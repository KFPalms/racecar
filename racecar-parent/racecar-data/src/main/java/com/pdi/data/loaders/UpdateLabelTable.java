package com.pdi.data.loaders;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
//import java.sql.SQLException;
//import java.util.HashMap;
import java.util.ArrayList;

/**
 * Updater to move the lael names into the database
 * Because I am effectively iterating over a table, I am unable to use a script (Sorry, Nate)
 *
 * @author kbeukelm
 *
 */
public class UpdateLabelTable
{
	// Static constants
    protected static final String DRIVER = "com.mysql.jdbc.Driver";
    protected static final String LAST_USER = "kbeukelm";

    protected static final String NEW_COLUMN_NAME = "defaultLabelTextId";

    protected static final String DEFAULT_LANG_CODE = "en";	// US English

    /*
	 * Runs as a headless app
	 */
	public static void main(String args[])
	{
		if (args.length == 0)
		{
			System.out.println(" No parameters were detected.");
			System.out.println(parmDescString());

			return;
		}

		if (args.length != 3)
		{
			System.out.println("Incorrect number of parameters (" + args.length + ")");
			System.out.println(parmDescString());
			return;
		}
		
		String url = args[0];
		String uid = args[1];
		String pw = args[2];
			
		String phase = null;
		Connection con = null;

		try
		{
			// Open the DB
			phase = "DB Open";
			con = openDb(url, uid, pw);
			System.out.println("Database opened.  url: " + url);

			// check
			phase = "Column existence checking";
			if (checkColumnExists(con, url, NEW_COLUMN_NAME))
			{
				System.out.println("Column \"" + NEW_COLUMN_NAME + "\" already exists.  Terminating...");
				return;
			}
			//System.out.println("DEBUG: Column doesn't exist... expected");

			// Get the language
			phase = "Getting default language";
			int langId = getDfltLangId(con, DEFAULT_LANG_CODE);
			//System.out.println("DEBUG: language ID = " + langId + ".  1 expected.");

			// Modify the table
			phase = "Adding the column.";
			addTextIdCol(con, NEW_COLUMN_NAME);
			//System.out.println("DEBUG: DB column added.");
			
			// Read the labels and IDs
			phase = "Fetching ids and labels.";
			ArrayList<IdAndLabel> list = getIdLabelList(con);
			//System.out.println("DEBUG: returned " + list.size() + " labels.");
			
			// Loop through the labels - insert to text and update the label table
			phase = "Inerting label text and ids.";
			insertLabels(con, langId, list);
			//System.out.println("DEBUG: labels added.");
		
			// Last table mod
			phase = "Dropping default_label column.";
			dropOldColumn(con);
			//System.out.println("DEBUG: column deleted.");

			System.out.println("Done...");
		}
		catch (Exception e)
		{
			System.out.println("ERROR Detected in " + phase + " phase...");
			e.printStackTrace();
			return;
		}
		finally
		{
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception e)  {  }
			}
		}
	}
	
	
	/*
	 * Method to display appropriate parameters.
	 * 
	 * Note that there is no closing nwe-line character; assumes that this method is called
	 * in a println() or that the user will handle the closing new-line himself.
	 */
	private static String parmDescString()
	{
		String str = "  This program requires three arguments as follows:\n";
		str += "    URL      - The URL where the database lies.  Examples:\n";
		str += "                 'jdbc:mysql://mspdbdev2:3306/pdi_certify' or\n";
		str += "                 'jdbc:mysql://sanfranqasql3:3306/scoring_reporting'\n";
		str += "    User id  - A user id for the specified database with the appropriate permissions.\n";
		str += "    Password - The password for the above user ID.";
		
		return str;
	}

	
	/*
	 * 
	 */
	private static Connection openDb(String url, String uid, String pw)
		throws Exception
	{
		Connection con = null;

		Class.forName(DRIVER);

		con = DriverManager.getConnection(url, uid, pw);
		//////con.setAutoCommit(false);
			
		return con;
	}


	/*
	 * checkColumnExists - Verify that the column does not yet exist
	 * 
	 * @param con - Connection object
	 * @param name - The name of the column whose existence we are testing
	 * @return boolean indicating existence
	 */
	private static boolean checkColumnExists(Connection con, String url, String name)
		throws Exception
	{
		Statement stmt = null;
		ResultSet rs = null;
		
		// Try to get the schema out of the URL.  That'll be the part just before the ?
		int end = url.indexOf('?');
		int start = url.lastIndexOf('/', end) + 1;
		String schema;
		if (end < 0 || start < 0)
		{
			schema = "";
		}
		else
		{
			schema = url.substring(start, end);
		}
		if (schema.length() < 1)
		{
			// Bad... bomb out
			System.out.println("Unable to parse schema:  URL=" + url + ".  Indices - start: " + start + ", end=" + end);
			return false;
		}

		try
		{
			StringBuffer q = new StringBuffer();
			q.append("SELECT COUNT(*) as colCnt");
			q.append("  FROM information_schema.columns ");
			q.append("  WHERE table_schema = '" + schema + "' ");
			q.append("    AND table_name = 'pdi_abd_rpt_label_lookup' ");
			q.append("    AND column_name = 'defaultLabelTextId'");
	
			stmt = con.createStatement();
			rs = stmt.executeQuery(q.toString());

			// get the row
			rs.next();

			return rs.getBoolean("colCnt");
		}
		finally
		{
			if(! (rs == null))
			{
				try  {  rs.close();  }
				catch (Exception e)  {  }
			}
			if(! (stmt == null))
			{
				try  {  stmt.close();  }
				catch (Exception e)  {  }
			}
		}
	}


	/*
	 * getDfltLangId - get the language ID of the specified language code
	 * 
	 * @param con - Connection object
	 * @param name - The name of the column whose existence we are testing
	 * @return boolean indicating existence
	 */
	private static int getDfltLangId(Connection con, String langCode)
		throws Exception
	{
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer q = new StringBuffer();
		q.append("SELECT ll.languageId ");
		q.append("  FROM pdi_abd_language ll ");
		q.append("  WHERE ll.languageCode = '" + langCode + "'");

		try
		{
			stmt = con.createStatement();
			rs = stmt.executeQuery(q.toString());

			if (! rs.isBeforeFirst())
			{
				throw new Exception("No data available for language code \"" + langCode + "\"");
			}
			
			// get the row
			rs.next();
			
			return rs.getInt("languageId");
		}
		finally
		{
			if(! (rs == null))
			{
				try  {  rs.close();  }
				catch (Exception e)  {  }
			}
			if(! (stmt == null))
			{
				try  {  stmt.close();  }
				catch (Exception e)  {  }
			}
		}
	}

	


	/*
	 * addTextIdCol - Add the specified column to the database table
	 * 
	 * @param con - Connection object
	 * @param colName - The name of the column to be added
	 */
	private static void addTextIdCol(Connection con, String colName)
		throws Exception
	{
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer q = new StringBuffer();
		q.append("ALTER TABLE pdi_abd_rpt_label_lookup ");
		q.append("  ADD COLUMN " + colName + " INTEGER(11) NOT NULL AFTER report_code");

		try
		{
			stmt = con.createStatement();
			stmt.executeUpdate(q.toString());

			return;
		}
		finally
		{
			if(! (rs == null))
			{
				try  {  rs.close();  }
				catch (Exception e)  {  }
			}
			if(! (stmt == null))
			{
				try  {  stmt.close();  }
				catch (Exception e)  {  }
			}
		}
	}


	/*
	 * getIdLabelList - get all of the existing label names and IDs
	 * 
	 * @param con - Connection object
	 * @return An ArrayList of objects containing the label ID and the label text
	 */
	private static ArrayList<IdAndLabel> getIdLabelList(Connection con)
		throws Exception
	{
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<IdAndLabel> ret = new ArrayList<IdAndLabel>();

		StringBuffer q = new StringBuffer();
		q.append("SELECT rll.report_label_id, ");
		q.append("       rll.default_label ");
		q.append("  FROM pdi_abd_rpt_label_lookup rll ");
		q.append("  ORDER BY rll.report_label_id");

		try
		{
			stmt = con.createStatement();
			rs = stmt.executeQuery(q.toString());

			if (! rs.isBeforeFirst())
			{
				throw new Exception("No label data available.");
			}

			// loop through the rows
			while(rs.next())
			{
				IdAndLabel il = new UpdateLabelTable().new IdAndLabel(rs.getLong("report_label_id"), rs.getString("default_label"));
				ret.add(il);
			}

			return ret;
		}
		finally
		{
			if(! (rs == null))
			{
				try  {  rs.close();  }
				catch (Exception e)  {  }
			}
			if(! (stmt == null))
			{
				try  {  stmt.close();  }
				catch (Exception e)  {  }
			}
		}
	}


	/*
	 * insertLabels - insert the label into the taxt table and update the label table with the id
	 * 
	 * @param con - Connection object
	 * @return An ArrayList of objects containing the label ID and the label text
	 */
	private static void insertLabels(Connection con, int dfltLang, ArrayList<IdAndLabel> list)
		throws Exception
	{
		String subphase = null;
		PreparedStatement insPS = null;
		PreparedStatement updtTxtPS = null;
		PreparedStatement updtLblPS = null;
		ResultSet rs = null;
		String tId = null;
		StringBuffer q = new StringBuffer();
		IdAndLabel il = null;
		
		// Set up the text prepared statement
		try
		{
			// Create the insert PS
			q.append("INSERT INTO pdi_abd_text ");
			q.append("  (languageID, text, isDefault, active, lastUserId, lastDate) ");
			q.append("  VALUES(?, ?, 1, 1, '" + LAST_USER + "', NOW())");
			insPS = con.prepareStatement(q.toString(),Statement.RETURN_GENERATED_KEYS);

			// Create the update PS
			q.setLength(0);
			q.append("UPDATE pdi_abd_text ");
			q.append("  SET textID = ? ");
			q.append("  WHERE uniqueID = ?");
			updtTxtPS = con.prepareStatement(q.toString());
			
			// Create the label update PS
			q.setLength(0);
			q.append("UPDATE pdi_abd_rpt_label_lookup ");
			q.append("  SET defaultLabelTextId = ? ");
			q.append("  WHERE report_label_id = ?");
			updtLblPS = con.prepareStatement(q.toString());

			try
			{
				// loop through the input list
				for (IdAndLabel ent : list)
				{
					il = ent;
					// write the text
					subphase = "Writing text.";
					insPS.setInt(1, dfltLang);	// language ID
					insPS.setString(2, ent.getLabel());	// Text itself
					insPS.executeUpdate();
	
					// get the id
					subphase = "Fetching text ID";
					rs = insPS.getGeneratedKeys();
					if (rs == null  ||  ! rs.isBeforeFirst())
					{
						//it didn't return an ID...
						throw new Exception("No text id returned.");
					}
					rs.next();
					tId = rs.getString(1);

					// Update the row so it has the textId
					subphase = "Updating textId (text table)";
					updtTxtPS.setString(1, tId);	// Text ID
					updtTxtPS.setString(2, tId);	// Unique ID (same for the default row)
					updtTxtPS.executeUpdate();
				
					// update the label table
					subphase = "Updatel label text ID";
					updtLblPS.setString(1, tId);
					updtLblPS.setLong(2, ent.getId());
					updtLblPS.executeUpdate();
				}	// End for loop
			}
			catch (Exception e)
			{
				String msg = "Error moving label text.  Subphase=" + subphase +
					 ", label ID=" + il.getId() +
					 ", label text=" + il.getLabel() +
					 ":  msg=" + e.getMessage();
				throw new Exception(msg, e.getCause());
			}
		}
		finally
		{
			if(! (rs == null))
			{
				try  {  rs.close();  }
				catch (Exception e)  {  }
			}
			if(! (insPS == null))
			{
				try  {  insPS.close();  }
				catch (Exception e)  {  }
			}
			if(! (updtTxtPS == null))
			{
				try  {  updtTxtPS.close();  }
				catch (Exception e)  {  }
			}
			if(! (updtLblPS == null))
			{
				try  {  updtLblPS.close();  }
				catch (Exception e)  {  }
			}
		}
	}


	/*
	 * dropOldColumn - Drop the default label column
	 * 
	 * @param con - Connection object
	 * @return An ArrayList of objects containing the label ID and the label text
	 */
	private static void dropOldColumn(Connection con)
		throws Exception
	{
		Statement stmt = null;

		StringBuffer q = new StringBuffer();
		q.append("ALTER TABLE pdi_abd_rpt_label_lookup ");
		q.append("  DROP COLUMN default_label");

		try
		{
			stmt = con.createStatement();
			stmt.executeUpdate(q.toString());

			return;
		}
		finally
		{
			if(! (stmt == null))
			{
				try  {  stmt.close();  }
				catch (Exception e)  {  }
			}
		}
	}




	/*
	 * Private class to hold the label id and label
	 */
	private class IdAndLabel
	{
		private long _id;
		private String _label;
		
		public IdAndLabel(long id, String label)
		{
			_id = id;
			_label = label;
		}
		
		// No setters, only getters
		public String getLabel()
		{
			return _label;
		}
		public long getId()
		{
			return _id;
		}
	}

}
