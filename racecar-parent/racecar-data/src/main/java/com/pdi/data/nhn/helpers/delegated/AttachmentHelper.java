/**
 * Copyright (c) 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.nhn.helpers.delegated;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.pdi.data.dto.Attachment;
import com.pdi.data.dto.AttachmentHolder;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.IAttachmentHelper;
import com.pdi.data.nhn.util.NhnDatabaseUtils;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.logging.LogWriter;
import com.pdi.properties.PropertyLoader;

/*
 * AttachmentHelper
 */
public class AttachmentHelper implements IAttachmentHelper
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//

	//
	// Constructors.
	//

	//
	// Instance methods.
	//

	/**
	 * fetch all Attachments for this project and participant, 
	 * and return them in the AttachmentHolder
	 *
	 * @param  session - A SessionUser object
	 * @param  projectId - A project Id
	 * @param  participantId - A participant Id
	 * @return AttachmentHolder
	 * @throws Exception
	 */
	public AttachmentHolder fromProjectIdParticipantId(SessionUser session,
			                                  String projectId,
			                                  String participantId)
		throws Exception
	{
		AttachmentHolder ret = new AttachmentHolder();

 		try
 		{
			ret.setParticipant(HelperDelegate.getParticipantHelper("com.pdi.data.nhn.helpers.delegated").fromId(session, participantId));
		}
 		catch (Exception e)
 		{
 			throw new Exception("Unable to fetch participant data attachments", e);
		}
		
		// Get the attacmentURL
		String attachmentUrl = PropertyLoader.getProperty("com.pdi.data.nhn.application", "attachment.url");

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("  SELECT ud.document_id as docId, ");
		sqlQuery.append("         fa.file_attachment_id as attId, ");
		sqlQuery.append("         fa.name as attName, ");
		sqlQuery.append("         dd.description as attDesc, ");
		sqlQuery.append("         " + Attachment.ASSOC_PART + " as assoc, ");
		sqlQuery.append("         dd.include_in_integration_grid as igInclude, ");
		sqlQuery.append("         ud.user_document_type_id as attTypeId, ");
		sqlQuery.append("         udt.name as attTypeName, ");
		sqlQuery.append("         fa.path as attPath ");
		sqlQuery.append("    FROM platform.dbo.user_document ud ");
		sqlQuery.append("      INNER JOIN platform.dbo.document dd ON dd.document_id = ud.document_id ");
		sqlQuery.append("      INNER JOIN platform.dbo.user_document_type udt ON udt.user_document_type_id = ud.user_document_type_id ");
		sqlQuery.append("      INNER JOIN platform.dbo.file_attachment fa ON fa.file_attachment_id = dd.file_attachment_id ");
		sqlQuery.append("    WHERE users_id = " + participantId + " ");
		sqlQuery.append("UNION ");
		sqlQuery.append("  SELECT pd.document_id as docId, ");
		sqlQuery.append("         fa.file_attachment_id as attId, ");
		sqlQuery.append("         fa.name as attName, ");
		sqlQuery.append("         dd.description as attDesc, ");
		sqlQuery.append("         " + Attachment.ASSOC_PROJ + " as assoc, ");
		sqlQuery.append("         dd.include_in_integration_grid as igInclude, ");
		sqlQuery.append("         pd.project_document_type_id as attTypeId, ");
		sqlQuery.append("         pdt.name as attTypeName, ");
		sqlQuery.append("         fa.path as attPath ");
		sqlQuery.append("    FROM platform.dbo.project_document pd ");
		sqlQuery.append("      INNER JOIN platform.dbo.document dd ON dd.document_id = pd.document_id ");
		sqlQuery.append("      INNER JOIN platform.dbo.project_document_type pdt ON pdt.project_document_type_id = pd.project_document_type_id ");
		sqlQuery.append("      INNER JOIN platform.dbo.file_attachment fa ON fa.file_attachment_id = dd.file_attachment_id ");
		sqlQuery.append("    WHERE project_id = "+ projectId + " ");
		sqlQuery.append("ORDER BY attName");

		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(sqlQuery);

		if (dlps.isInError())
		{
			LogWriter.logBasic(LogWriter.WARNING, this, "dlps.isInError()" + dlps.toString());
			return ret;
		}
		DataResult dr = null;
		ResultSet rs  = null;

		try
		{
			dr =  NhnDatabaseUtils.select(dlps);
			rs = dr.getResultSet();

			if (rs != null && rs.isBeforeFirst())
			{
				while (rs.next())
				{
					Attachment attachment = new Attachment();
					attachment.setAttachmentId(rs.getLong("attId"));
					attachment.setAttachmentName(rs.getString("attName"));
					attachment.setAttachmentDescription(rs.getString("attDesc"));
					attachment.setAttachmentAssociation(rs.getInt("assoc"));
					attachment.setDisplayInIg(rs.getBoolean("igInclude"));
					attachment.setAttachmentTypeId(rs.getInt("attTypeId"));
					attachment.setAttachmentTypeName(rs.getString("attTypeName"));
					attachment.setAttachmentPath(rs.getString("attPath"));
					attachment.setAttachmentUrl(attachmentUrl);

					ret.getAttachments().add(attachment);
				}
			}
			
			return ret;
		}
		finally
		{
//			if (rs != null)
//			{
//				try { rs.close();  }
//				catch (SQLException sqlEx)
//				{
//					LogWriter.logSQLWithException(LogWriter.ERROR, this, 
//							"Error in NHN AttachmentHelper fromProjectIdParticipantId() ", 
//							sqlQuery.toString(), sqlEx);
//				}
//				rs = null;
//			}
			if (dr != null) { dr.close(); dr = null; }
		}
	}


	/**
	 * A convenience method to fetch fetch Attachments for a
	 * participant but not for a project.
	 * 
	 * Note that the Participant object is empty in this case
	 *
	 * @param  session - A SessionUser object
	 * @param  participantId - A participant Id
	 * @return AttachmentHolder
	 * @throws Exception
	 */
	public AttachmentHolder fromParticipantId(SessionUser session, String participantId)
		throws Exception
	{
			return fromProjectIdParticipantId(session, "0", participantId);
	}


	/**
	 * A convenience method to fetch fetch Attachments for a
	 * project but not for a participant 
	 *
	 * @param  session - A SessionUser object
	 * @param  participantId - A participant Id
	 * @return AttachmentHolder
	 * @throws Exception
	 */
	public AttachmentHolder fromProjectId(SessionUser session, String projectId)
		throws Exception
	{
			return fromProjectIdParticipantId(session, projectId, "0");
	}
}
