package com.pdi.data.abyd.helpers.reportInput;

//import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
//import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import javax.xml.xpath.XPath;
//import javax.xml.xpath.XPathConstants;
//import javax.xml.xpath.XPathExpressionException;
//import javax.xml.xpath.XPathFactory;
//
//import org.w3c.dom.Node;
//import org.w3c.dom.NodeList;
//import org.xml.sax.InputSource;


import com.pdi.data.abyd.dto.common.CompGroupData;
import com.pdi.data.abyd.dto.common.DNACellDTO;
import com.pdi.data.abyd.dto.common.DNAStructureDTO;
import com.pdi.data.abyd.dto.common.ModDataDTO;
import com.pdi.data.abyd.dto.common.CompElementDTO;
import com.pdi.data.abyd.dto.intGrid.IGSummaryDataDTO;
import com.pdi.data.abyd.dto.reportInput.ReportInputDTO;
import com.pdi.data.abyd.helpers.intGrid.IGSummaryDataHelper;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.data.dto.Participant;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.IParticipantHelper;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;

/**
 * ResearchExtractHelper contains code used to write out the Research Extract
 * candidate row when a DRI is finalized (ReportInputDataHelper).
 *
 * @author		Ken Beukelman
 */
public class ResearchExtractHelper
{
	private static final Logger log = LoggerFactory.getLogger(ResearchExtractHelper.class);
	//
	//  Static data.
	//

	// Extract XML tags
	private static String ROOT_TAG = "AdditionalExtractData";
	
	// Extract status - Exposed
	public static int EXT_CAND = 0;	// Extract candidate (insert)
	public static int EXT_CPLT = 1;	// Extract complete
	public static int EXT_DMNT = 5;	// Extract candidate - generated from dormant data (insert)
	public static int EXT_DMNT_ERR = 7;	// Extract candidate - Error was detected
	public static int EXT_UPDT = 9;	// Extract candidate, existing row (update)
	//private static int EXT_ERR = -1;	// Extract attempted, but errors resulted
	
	// Constants for module data
	// Use the EXTERNAL name
	private static HashMap<String, String> MOD_MAP = new HashMap<String, String>();
	static
	{
		MOD_MAP.put("Interview",						"INT");
		MOD_MAP.put("In-Box",							"IBX");
		MOD_MAP.put("Direct Report Meeting",			"DRM");
		MOD_MAP.put("Boss Meeting",						"BSM");
		MOD_MAP.put("Peer Meeting",						"PRM");
		MOD_MAP.put("Team Meeting",						"TMM");
		MOD_MAP.put("Customer Meeting",					"CUM");
		MOD_MAP.put("Vendor Meeting",					"VNM");
		MOD_MAP.put("Financial Exercise",				"FEX");
		MOD_MAP.put("Analyst Meeting", 					"ANL");
		// Added modules
		MOD_MAP.put("Standard",							"STD");
		MOD_MAP.put("Direct Report Meeting Robinsons",	"DRMROB");
		MOD_MAP.put("CUSTOM Global Peer Meeting",		"CGPRM");
		MOD_MAP.put("Planning & Execution",				"PLEXSAC");
		MOD_MAP.put("Performance DRM",					"PDRMSAC");
		MOD_MAP.put("Resource Planning",				"RSPLNSAC");
		MOD_MAP.put("Coaching DRM",						"CDRMSAC");
		MOD_MAP.put("Business Mgmt. Meeting",			"BUSMGTM");
		MOD_MAP.put("District Manager Meeting",			"DMGRM");
		MOD_MAP.put("Leaderless Group Discussion",		"LLGDMAC");
		MOD_MAP.put("Business Mgmt. Exercise",			"BMEMAC");
		MOD_MAP.put("Group Exercise", 					"GRPEX");
		MOD_MAP.put("Subordinate Meeting", 				"SBM");
		MOD_MAP.put("Country Representative Meeting",	"CRM");
		MOD_MAP.put("Director Meeting",					"DIRM");
		
		MOD_MAP.put("Business Review Exercise",			"BRE");
		MOD_MAP.put("Town Hall",						"TNH");
		MOD_MAP.put("Joint Venture Meeting",			"JVM");
		MOD_MAP.put("Business Management",				"BMGT");
		MOD_MAP.put("GROUP Inbox",						"GIB");
		MOD_MAP.put("Testing (KFALP)",					"TALP");	// Manual Entry ALP
		MOD_MAP.put("Testing (KF4D)",					"TKF4D");	// Manual Entry KF4D
		MOD_MAP.put("KF360",							"TKF360");
		MOD_MAP.put("KF4D", 							"KF4D");
		MOD_MAP.put("In-Basket",						"IBSKT");
		
		// Novartis modules
		MOD_MAP.put("Manager Meeting",					"NMGRM");
		MOD_MAP.put("Coaching Role Play",				"NCCHRP");
		MOD_MAP.put("Counseling Role Play",				"NCSLRP");
		MOD_MAP.put("Commercial / Peer Meeting",		"NCPM");
		MOD_MAP.put("Indirect Report Meeting",			"NTIRM");
		MOD_MAP.put("External Stakeholder Meeting",		"NESM");

		// Special case
		MOD_MAP.put("Testing",							"TST");
		MOD_MAP.put("Testing (AUTO)",					"TSTALP");
		MOD_MAP.put("Testing (KF4D AUTO)",				"TSTKF4D");
		
		// ALP not present in list because it implies external test data not available here (kfp)
		
		// CEO-specific modules
		MOD_MAP.put("Strategy and Financial Exercise",			"STFINEX");
		MOD_MAP.put("Top 200",									"TOP200");
		MOD_MAP.put("Direct Report Meeting (Chris Wallace)",	"DRMCW");
		MOD_MAP.put("Board Meeting",							"BRDM");
		MOD_MAP.put("CFIN",										"CFIN");
		MOD_MAP.put("V360", 									"V360");
		
	}
	
	// Population type constants
	private static final int GENPOP = 1;
	private static final int SPECPOP = 2;
	
	// InstrumentId/VehId cross reference
//	// NOTE -- CHANGE/ADD TO THIS IF YOU ADD AN INSTRUMENT TO THE CONSTRUCT SPSS TABLE!!!
//	private static HashMap<String, String> INST_MAP = new HashMap<String, String>();
//	static
//	{
//		INST_MAP.put("FFGLQDEG",	"CS");		// Career Survey
//		INST_MAP.put("IVUVHYKM",	"CS");		// Career Survey
//		INST_MAP.put("ISWEGTQC",	"CS");		// Career Survey - Chinese
//		INST_MAP.put("BKUEDDNI",	"CS");		// Career Survey - French
//		INST_MAP.put("CPXPHWRY",	"CS");		// Career Survey - German
//		INST_MAP.put("FGGQYSFG",	"CS");		// Career Survey - Spanish
//		INST_MAP.put("FAGNDFAC",	"FEX");		// Financial Exercise - AbyD
//		INST_MAP.put("IXLGVIOQ",	"GPI");		// Global Personality Inventory (GPI)
//		INST_MAP.put("LKXOOMME",	"GPI");		// Global Personality Inventory (GPI) - Chinese
//		INST_MAP.put("FFWDUFWY",	"GPI");		// Global Personality Inventory (GPI) - French
//		INST_MAP.put("HOGEZINK",	"GPI");		// Global Personality Inventory (GPI) - German
//		INST_MAP.put("BMMLZKOK",	"GPI");		// Global Personality Inventory (GPI) - Spanish
//		INST_MAP.put("FDHIRHYW",	"GPI");		// Global Personality Inventory (GPI) UK English
//		INST_MAP.put("BMZLGGZL",	"LEI");		// Leadership Experience Inventory (LEI)
//		INST_MAP.put("KCIYQGYX",	"LEI");		// Leadership Experience Inventory (LEI) - Chinese
//		INST_MAP.put("DUJKAXPI",	"LEI");		// Leadership Experience Inventory (LEI) - French
//		INST_MAP.put("BHJAYRLB",	"LEI");		// Leadership Experience Inventory (LEI) - German
//		INST_MAP.put("CNBPPCGQ",	"LEI");		// Leadership Experience Inventory (LEI) - Spanish
//		INST_MAP.put("FFJUUSBA",	"WGE");		// Watson-Glaser II
//		
//		// Non-A by D instruments
//		INST_MAP.put("FFZFFOKV",	"WGA");		// Watson-Glaser A
//		INST_MAP.put("ISTVNYDL",	"WGAS");	// Watson-Glaser A - SHORT FORM
//		INST_MAP.put("DWGODHMX",	"WGC");		// Watson-Glaser C - Manual Entry
//		INST_MAP.put("HNZYMHDV",	"RAVB");	// Raven's APM Form B
//		INST_MAP.put("IYJOHVLX",	"RAVSF");	// Raven's APM Short Form
//		INST_MAP.put("DYMYDGHI",	"RAVSF");	// Raven's APM Short Form - Chinese
//		INST_MAP.put("ITVTIIPB",	"RAVSF");	// Raven's APM Short Form - French
//		INST_MAP.put("FDZAJOAY",	"RAVSF");	// Raven's APM Short Form - German
//		INST_MAP.put("DWWJAAHE",	"RAVSF");	// Raven's APM Short Form - Spanish
//		INST_MAP.put("ITAXVGGU",	"RAVT");	// Raven's Timed Long Form - Manual Entry
//		INST_MAP.put("LGHSTSHU",	"WES");		// Personnel Classification Test - Alexander G. Wesman (Wesman)
//	}

	private static HashMap<String, String> INST_MAP = new HashMap<String, String>();
	static
	{
		INST_MAP.put("cs",		"CS");		// Career Survey
		INST_MAP.put("cs2",		"CS");		// Career Survey - version 2
		INST_MAP.put("finex",	"FEX");		// Financial Exercise - AbyD
		INST_MAP.put("gpi",		"GPI");		// Global Personality Inventory (GPI) - Short (TLT)
		INST_MAP.put("gpil",	"GPI");		// Global Personality Inventory (GPI) - Standard
		INST_MAP.put("lei",		"LEI");		// Leadership Experience Inventory (LEI)
		INST_MAP.put("rv",		"RAVB");	// Raven's APM Form B
		INST_MAP.put("wg",		"WGE");		// Watson-Glaser II
		INST_MAP.put("wga",		"WGA");		// Watson-Glaser A
	}

	//
	//  Static methods.
	//

	/**
	 * Set up the research candidate row (if needed) -- Primary externally exposed method
	 *
	 * @param rptInp - A ReportInput object
	 * @throws Exception
	 */
	public static void setupResearchExtractCandidate(ReportInputDTO rptInp)
		throws Exception
	{
		// spawn thread? or after the ext cand status check?
		
		// see if the row is already extracted (if so return)
		int extStatus = getExtractStatus(rptInp.getParticipantId(), rptInp.getDnaId());
		if (extStatus == EXT_CPLT || extStatus == EXT_DMNT)
		{
			// It's already extracted... scram
			return;
		}

		// create the XML
		String xml = createExtractXML(rptInp);
		
		// write out the row
		writeExtractRow(extStatus, rptInp.getParticipantId(), rptInp.getDnaId(), xml);
	}


	/**
	 * Fetch the extract candidate row.
	 *
	 * @param dnaId - The dnaID
	 * @param dnaId - The participant ID
	 * @return The status of the extract row.  If the row is not present, return 0
	 * @throws Exception
	 */
	private static int getExtractStatus(String partId, long dnaId)
		throws Exception
	{
		DataResult dr = null;
		int ret = EXT_CAND;
		
		try
		{
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT ec.extractStatus ");
			sqlQuery.append("  FROM pdi_abd_extract_cand ec ");
			sqlQuery.append("  WHERE ec.participantId = '" + partId + "' ");
			sqlQuery.append("    AND ec.dnaId = " + dnaId);

			DataLayerPreparedStatement dlps =  AbyDDatabaseUtils.prepareStatement(sqlQuery);
			if (dlps.isInError())
			{
				throw new Exception("Error preparing checkExtractStatus.  Code=" + 
							dlps.getStatus().getExceptionCode() + 
							", Msg=" + dlps.getStatus().getExceptionMessage());
			}
	
			dr =  AbyDDatabaseUtils.select(dlps);
			if (dr.hasNoData())
			{
				ret = EXT_CAND;
			}
			else
			{
				if (dr.isInError())
				{
					throw new Exception("Error fetching Extract status.  Code=" + 
							dlps.getStatus().getExceptionCode() + 
							", Msg=" + dlps.getStatus().getExceptionMessage());
				}
				else
				{
					// check the status
					ResultSet rs = dr.getResultSet();
					// should be a single row
					rs.next();
					int stat = rs.getInt("extractStatus");
					// Transform an existing candidate not extracted status to an update status
					ret = (stat == EXT_CAND ? EXT_UPDT : stat);
				}
			}

			return ret;
		}
		finally
		{
			if (dr != null) {  dr.close();  dr = null; }
		}
	}


	/**
	 * Generate the extract XML from data in the ReportInput object
	 *
	 * @param rptInp - A ReportInput object
	 * @return An XML string
	 */
	public static String createExtractXML(ReportInputDTO rptInp)
		throws Exception
	{
		// For use in diverse places
		Connection con = null;
		
		try
		{
			con = AbyDDatabaseUtils.getDBConnection();
			String ret = "<" + ROOT_TAG + ">";
			
			// Get some general information
			ret += getGenInfo(rptInp);
			
			// Suggested DRI Ratings
			ret += getDriCalcValues(rptInp);
			
			// Get IG stuff
			IGSummaryDataHelper igHelper = new IGSummaryDataHelper(con, rptInp.getParticipantId(), rptInp.getDnaId());
			IGSummaryDataDTO ig = igHelper.getSummaryData();
	
			// Calc the suggested scores
			ret += getIgCalcValues(ig.getGridData());
	
			// Get score stuff (loop through the EGs and do it for each EG... and selected Testing modules)
			ret += getModCalcValues(rptInp.getModelName(), ig.getGridData());
			
			// get the norms
			ret += getNorms(con, rptInp.getDnaId(), rptInp.getParticipantId());
	
			// Finish it off
			ret += "</" + ROOT_TAG + ">";
			
			return ret;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception(e);
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)  {  /* Swallow the exception */  }
				con = null;
			}
		}
	}


	/*
	 * Get the Int grid XML data in a text string
	 */
	private static String getGenInfo(ReportInputDTO ri)
	{
		String ret = "";
		
		// Model level name
		ret += "<MOD_LEVEL>" + ri.getModelName() + "</MOD_LEVEL>";
		
		// get the participant name.
		Participant part;
		try
		{			
			IParticipantHelper iP = HelperDelegate.getParticipantHelper("com.pdi.data.nhn.helpers.delegated");
			part = iP.fromId(HelperDelegate.getSessionUserHelperHelper().createSessionUser(), ri.getParticipantId());
		}
		catch (Exception e)
		{
			e.printStackTrace();
			part = new Participant();
			part.setFirstName("Not");
			part.setLastName("Available");
		}
		ret += "<FNAME>" + part.getFirstName() + "</FNAME>";
		ret += "<LNAME>" + part.getLastName() + "</LNAME>";

		return ret;
	}


	/*
	 * Get the Int grid XML data in a text string
	 */
	private static String getDriCalcValues(ReportInputDTO rptInp)
	{
		String ret = "";
		
		ret += "<SCORES Veh=\"DRI\">";
		ret += "<SCORE Type=\"S\" Code=\"LSKILL\">" +
					getDisplaySuggested(rptInp.getLeadershipSkillRecommendedRatings()) +
					"</SCORE>";
		ret += "<SCORE Type=\"S\" Code=\"LEXP\">" +
					getDisplaySuggested(rptInp.getLeadershipExperienceRecommendedRatings()) +
					"</SCORE>";
		ret += "<SCORE Type=\"S\" Code=\"LSTYLE\">" + 
					getDisplaySuggested(rptInp.getLeadershipStyleRecommendedRatings()) +
					"</SCORE>";
		ret += "<SCORE Type=\"S\" Code=\"LINT\">" +
					getDisplaySuggested(rptInp.getLeadershipInterestRecommendedRatings()) +
					"</SCORE>";
		ret += "<SCORE Type=\"S\" Code=\"DERAIL\">" +
					rptInp.getDerailmentRecommendedRating() +
					"</SCORE>";
		ret += "<SCORE Type=\"S\" Code=\"LTERM\">" +
					getDisplaySuggested(rptInp.getLongtermRecommendedRatings()) +
					"</SCORE>";
		
		// Fit and Readiness (Calculated but not suggested... no "Entered" data exists)
		ret += "<SCORE Code=\"LSI\">" + rptInp.getLeadershipSkillIndex() + "</SCORE>";
		ret += "<SCORE Code=\"FITI\">" + rptInp.getCandidateFitIndex() + "</SCORE>";
		ret += "<SCORE Code=\"FITR\">" + rptInp.getFitRecommendedRating() + "</SCORE>";
		ret += "<SCORE Code=\"REDR\">" + rptInp.getReadinessRating() + "</SCORE>";
		ret += "<SCORE Code=\"AREDR\">" + rptInp.getAlrReadinessRating() + "</SCORE>";
		ret += "<SCORE Code=\"ARSUG\">" + rptInp.getAlrSugReadinessRating() + "</SCORE>";
		ret += "<SCORE Code=\"AFITR\">" + rptInp.getAlrCultureFitRating() + "</SCORE>";
		ret += "<SCORE Code=\"AFSUG\">" + rptInp.getAlrSugCultureFitRating() + "</SCORE>";
		ret += "</SCORES>";
		
		return ret;
	}


	/**
	 * Get the display suggested rating.
	 * 
	 * The business wanted multiple suggested ratings but to display the one closest to the
	 * "middle".  This routine pulls that out.
	 * Assumes a 1 - 4 rating scheme
	 *
	 * @param ratings - An ArrayList of Integer suggested ratings
	 * @return The suggested rating that is closest to the middle
	 */
	private static int getDisplaySuggested(ArrayList<Integer> ratings)
	{
		if(ratings == null || ratings.size() < 1)
		{
			return 0;
		}
		
		int ret = 0;
		int big = 0;
		int small=0;
		for (int i=0; i < ratings.size(); i++)
		{
			int num = ratings.get(i);
			if (i == 0)
			{
				big = small = num;
				continue;
			}
			
			if (num > big)
			{
				big = num;
			}
			else if (num < small)
			{
				small = num;
			}
		}
		
		if (big < 3)
			ret = big;
		else
			ret = small;
		
		return ret;
	}


	/*
	 * Get the Int grid XML data in a text string
	 */
	private static String getIgCalcValues(DNAStructureDTO struct)
	{
		String ret = "";
		
		// Make a competency HashMap...
		LinkedHashMap<Long, CompData> comps = new LinkedHashMap<Long, CompData>();
		// and loop through the groups, getting the competencies in a list.
		for (CompGroupData cg : struct.getFullCompArray())
		{
			for (CompElementDTO cmp : cg.getCgCompList())
			{
				comps.put(cmp.getCompId(), new ResearchExtractHelper().new CompData());
			}
		}
		
		// Go through the intersection data and pick up the score for each competency
		for (DNACellDTO cell : struct.getModCompIntersection())
		{
			double scor = Double.parseDouble(cell.getScore());
			if (scor == -1.00 || scor == 0.00)
			{
				// No score or score of zero are not included in the rollup
				continue;
			}
			comps.get(cell.getCompetencyId()).addToCum(scor);
		}
		
		// put out the suggested scores (competency averages)
		ret += "<SCORES Veh=\"IG\">";
		for (Iterator<Map.Entry<Long, CompData>> itr = comps.entrySet().iterator(); itr.hasNext(); )
		{
			Map.Entry<Long, CompData> ent = itr.next();
			ret += "<SCORE Type=\"S\" Code=\"" + ent.getKey() + "\">" +
					ent.getValue().getAvg() +
					"</SCORE>";
		}
		ret += "</SCORES>";

		return ret;
	}


	/*
	 * Get the Int grid XML data in a text string
	 */
	private static String getModCalcValues(String mdl, DNAStructureDTO struct) //ArrayList<ModDataDTO> mods)
	{
		//loop through the mod array and get the score data for each
		// score for the competencies
		String ret = "";
		
		for (ModDataDTO mod : struct.getModArray())
		{
			long modId = mod.getModId();
			String veh = "";
			veh = MOD_MAP.get(mod.getModName());
			if (veh == null)
			{
				//System.out.println("Unable to find vehicle name for module " + mod.getModName());
				continue;
			}

			ret += "<SCORES Veh=\"" + veh + "\">";
			
			// get the scores from the score array
			for(DNACellDTO cell : struct.getModCompIntersection())
			{
				if(cell.getModuleId() != modId)
				{
					continue;
				}
				// This only the calculated competency average score
				ret += "<SCORE Type=\"S\" Code=\"" + cell.getCompetencyId() + "\">" +
						cell.getScore() +
						"</SCORE>";
			}
			
			ret += "</SCORES>";
		}
			
		return ret;
	}


	/**
	 * Fetch norm info.  Includes putting out the synthetic norm
	 *
	 * @param con - A database connection
	 * @param dnaId - A DNA ID
	 * @param partId - The participant ID
	 * @return A status; 0 if no data yet, 1 if there is data, -1 if the row has already been extracted
	 * @throws Exception
	 */
	private static String getNorms(Connection con, long dnaId, String partId)
		throws Exception
	{
		String ret = "";
		
		ret += "<NORMS>";
		
		ret += getSyntheticNorm(con, dnaId);

		ret += getInstNorms(con, dnaId, partId);

		ret += "</NORMS>";

		return ret;
	}


	/**
	 * Fetch synthetic norm.
	 *
	 * @param con - A database connection
	 * @param dnaId - A DNA ID
	 * @return A String with the synthetic norm XML
	 * @throws Exception
	 */
	private static String getSyntheticNorm(Connection con, long dnaId)
		throws Exception
	{
		String ret = "";
		Statement stmt = null;
		ResultSet rs = null;
		
		// Get the synthetic norm
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT dna.synthMean, ");
		sqlQuery.append("       dna.synthStdDev ");
		sqlQuery.append("  FROM pdi_abd_dna dna ");
		sqlQuery.append("  WHERE dna.dnaId = " + dnaId);
		try
		{
			stmt = con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());
	
			if (rs.isBeforeFirst())
			{
				// we have at least one row (should be only 1, process only the first)
				rs.next();
	
				ret += "<NormSet Veh=\"DRI\" Type=\"S\" Name=\"DNA Synthetic Norm\">";
				ret += "<Norm Scale=\"dLCI\">";
				ret += "<Mean>" + rs.getString("synthMean") + "</Mean>";
				ret += "<StandardDeviation>" + rs.getString("synthStdDev") + "</StandardDeviation>";
				ret += "</Norm>";
				ret += "</NormSet>";
			}

			return ret;
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL error for getSyntheticNorm.  " +
							"SQLException: " + ex.getMessage() + ", " +
							"SQLState: " + ex.getSQLState() + ", " +
							"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (rs != null)
			{
			    try {  rs.close();  }
			    catch (SQLException sqlEx)
			    {
			    	System.out.println("ERROR: rs close in getSyntheticNorm - " + sqlEx.getMessage());
			    }
				rs = null;
			}
			if (stmt != null)
			{
			    try {  stmt.close();  }
			    catch (SQLException sqlEx)
			    {
			    	System.out.println("ERROR: stmt close in getSyntheticNorm - " + sqlEx.getMessage());
			    }
			    stmt = null;
			}
		}		   
	}


	/**
	 * Fetch instrument norm info.
	 *
	 * @param con - A database connection
	 * @param partId - The participant ID
	 * @return A String with the instrument norm XML
	 * @throws Exception
	 */
	private static String getInstNorms(Connection con, long dnaId, String partId)
		throws Exception
	{
		String ret = "";
		
// ------------- Old code - No longer used --------------------	
//		// Get the scales for which we have scores
//		// We pass an empty HashSet for normIds and an empty HashMpt for the constructs
//		HashSet<String> normIds = new HashSet<String>();
//		HashMap<String, String> conSpssXref = new HashMap<String, String>();
//		getScalesAndNormIds(con, partId, normIds, conSpssXref);
//		
//		// so now get all of the norm data for all of the norms
//		ArrayList<InstrumentNorm> normVals = getNorms(con, normIds, conSpssXref);
	// ---------------------------------------------------------	
		
		ArrayList<InstrumentNorm> normVals = getNormVals(con, dnaId, partId);
		
		// Generate the text
		for (InstrumentNorm ind : normVals)
		{
			if (ind == null){
				log.error("Instrument Norm Object was NULL.  KF4D Global Norms ?  Fix me.  Add norm data");
				continue;
			}

			ret += "<NormSet Veh=\"" + ind.getInstId() +
				   "\" Type=\"" + ind.getNormType() +
				   "\" Name=\"" + ind.getNormSetName() + "\">";
			for (ScaleNorm sn : ind.getScaleNorms())
			{
				ret += "<Norm Scale=\"" + sn.getScaleId() + "\">";
				ret += "<Mean>" + sn.getMean() + "</Mean>";
				ret += "<StandardDeviation>" + sn.getStdDev() + "</StandardDeviation>";
				ret += "</Norm>";
			}
			ret += "</NormSet>";
		}

		return ret;
	}


// pre-migration code - remove once the migration is complete
//	/**
//	 * Fetch Scales and Norm IDs
//	 *
//	 * @param con - A database connection
//	 * @param partId - The participant ID
//	 * @param norms - An empty Set in which to place norm ids
//	 * @param xref - An empty Map in which to place scale/construct cross references
//	 * @throws Exception
//	 */
//	private static void getScalesAndNormIds(Connection con,
//											String partId,
//			                                HashSet<String> norms,
//			                                HashMap<String, String> xref)
//		throws Exception
//	{
//		// Get the scales for which we have scores
//
//		Statement stmt = null;
//		ResultSet rs = null;
//
//		// Get the scores for this person
//		StringBuffer sqlQuery = new StringBuffer();
//		sqlQuery.append("SELECT csv.spssValue, ");
//		sqlQuery.append("       csv.constructId, ");
//		sqlQuery.append("       rslt.XML ");
//		sqlQuery.append("  FROM results rslt ");
//		sqlQuery.append("    LEFT JOIN pdi_construct_spss_value csv on csv.constructId = rslt.ConstructId ");
//		sqlQuery.append("  WHERE rslt.ConstructId IN (SELECT constructId ");
//		sqlQuery.append("                                FROM pdi_construct_spss_value ");
//		sqlQuery.append("                                WHERE spssValue in ('WG_A','WG_C','RAVENS_T','RAVENSSF','RAVENSB','WATSON','WG_AS','WGE_CT','WESMAN') ");
//		sqlQuery.append("                                  			OR spssValue like 'LEI3_%' ");
//		sqlQuery.append("                                  			OR spssValue like 'GPI_%') ");
//		sqlQuery.append("                                  AND lower(rslt.xml) like '%norm%' ");
//		sqlQuery.append("                                  AND rslt.CandidateID = '" + partId + "' ");
//		sqlQuery.append("  ORDER BY csv.spssValue, rslt.DateModifiedStamp");
//
//		try
//		{
//		    stmt = con.createStatement();
//			rs = stmt.executeQuery(sqlQuery.toString());
//
//			if (! rs.isBeforeFirst())
//			{
//				// no scores... nothing to do here
//				return;
//			}
//
//			while (rs.next())
//			{
//				String spssVal = rs.getString("spssValue");
//				String construct = rs.getString("constructId");
//				String xml = rs.getString("XML");
//				
//				// Map old spss keys to their new values
//				if (spssVal.equals("WATSON"))
//				{
//					spssVal = "WG_AS";
//				}
//				
//				// Save it
//				xref.put(construct, spssVal);
//
//				// Parse out the Norm IDs
//				XPathFactory  xpFactory = XPathFactory.newInstance();
//				XPath myXPath = (XPath) xpFactory.newXPath();
//				InputSource is;
//				
//				try
//				{
//					// Go to the 'Norms' node
//					String expression = "/XML_Fields/CScore.S0/Norms";
//					is = new InputSource(new ByteArrayInputStream(xml.getBytes()));
//					NodeList nodes = null;
//					nodes = (NodeList) myXPath.evaluate(expression, is, XPathConstants.NODESET);
//					if (nodes == null || nodes.getLength() < 1)
//					{
//						continue;
//					}
//
//					// The node called "Norms"
//					Node node = nodes.item(0);
//					NodeList nlist = node.getChildNodes();
//
//					// Get all of the norms in the 'Norms' node and put them in a Set...
//					for (int i=0; i < nlist.getLength(); i++)
//					{
//						Node normNode = nlist.item(i);
//						norms.add(normNode.getNodeName());
//					}
//				}
//				catch (XPathExpressionException ex)
//				{
//					throw new Exception("Unable to parse norm data:  part=" + partId +
//										", spss=" + spssVal + ", XML=" + xml);
//				}
//			}	// Got the norms and scales
//
//			return;
//		}
//		catch (SQLException ex)
//		{
//			// handle any errors
//			throw new Exception("SQL error for getInstNorms (scores).  " +
//							"SQLException: " + ex.getMessage() + ", " +
//							"SQLState: " + ex.getSQLState() + ", " +
//							"VendorError: " + ex.getErrorCode());
//		}
//		finally
//		{
//			if (rs != null)
//			{
//			    try {  rs.close();  }
//			    catch (SQLException sqlEx) { /* Ignore error */ }
//				rs = null;
//			}
//			if (stmt != null)
//			{
//			    try {  stmt.close();  }
//			    catch (SQLException sqlEx) { /* Ignore error */ }
//			    stmt = null;
//			}
//		}		   
//	}


	/**
	 * Fetch Norm data for this person in this project (project from DNA ID)
	 *
	 * @param con - A database connection
//	 * @param normIds - A set of norm ids
//	 * @param conSccaleXref - a construct/spss lookup map
	 * @return A list of InstrumentNorm objects
	 * @throws Exception
	 */
	private static ArrayList<InstrumentNorm> getNormVals(Connection con,
														 long dnaId,
														 String partId)
		throws Exception
	{
		ArrayList<InstrumentNorm> ret = new ArrayList<InstrumentNorm>();
		Statement stmt = null;
		ResultSet rs = null;

		// Get the norms in the list
		// The popType is hard coded here.  We could have got this from the normType column
		// in pdi_norm_group but would still have had to do a translation (genPop = 1,
		// specPop = 2) so we just did it in the query.
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("  SELECT pin.instrumentCode AS instCode, ");
		sqlQuery.append("         pin.genPopNormGroupId AS ngId, ");
		sqlQuery.append("         ng.normType AS popType, ");
		sqlQuery.append("         ng.name AS ngName, ");
		sqlQuery.append("         ng.normType, ");
		sqlQuery.append("         nrm.spssValue, ");
		sqlQuery.append("         nrm.mean, ");
		sqlQuery.append("         nrm.stdev, ");
		sqlQuery.append("         pin.participantId as pptId ");
		sqlQuery.append("    FROM pdi_abd_dna dna ");
		sqlQuery.append("      INNER JOIN pdi_participant_instrument_norm_map pin ON (pin.projectId = dna.projectId ");
		sqlQuery.append("                                                         AND pin.participantId IN (" + partId + ", 0)) ");
		sqlQuery.append("      INNER JOIN pdi_norm_group ng ON ng.normGroupId = pin.genPopNormGroupId ");
		sqlQuery.append("      LEFT JOIN pdi_norm nrm on nrm.normGroupId = ng.normGroupId ");
		sqlQuery.append("    WHERE dna.dnaId = " + dnaId +" ");
		sqlQuery.append("UNION ");
		sqlQuery.append("  SELECT pin.instrumentCode AS instCode, ");
		sqlQuery.append("         pin.specPopNormGroupId AS ngId, ");
		sqlQuery.append("         ng.normType AS popType, ");
		sqlQuery.append("         ng.name AS ngName, ");
		sqlQuery.append("         ng.normType, ");
		sqlQuery.append("         nrm.spssValue, ");
		sqlQuery.append("         nrm.mean, ");
		sqlQuery.append("         nrm.stdev, ");
		sqlQuery.append("         pin.participantId as pptId ");
		sqlQuery.append("    FROM pdi_abd_dna dna ");
		sqlQuery.append("      INNER JOIN pdi_participant_instrument_norm_map pin ON (pin.projectId = dna.projectId ");
		sqlQuery.append("                                                         AND pin.participantId IN (" + partId + ", 0)) ");
		sqlQuery.append("      INNER JOIN pdi_norm_group ng ON ng.normGroupId = pin.specPopNormGroupId ");
		sqlQuery.append("      INNER JOIN pdi_norm nrm on nrm.normGroupId = ng.normGroupId ");
		sqlQuery.append("    WHERE dna.dnaId = " + dnaId +" ");
		sqlQuery.append("ORDER BY instCode, popType, spssValue, pptId DESC");

		try
		{
		    stmt = con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			if (! rs.isBeforeFirst())
			{
				// no norm data found... nothing to do here
				return ret;
			}

			String curVeh = null;
			String curType = null;
			InstrumentNorm inst = null;
			String curSpss = "";
			String curSpssType = "";
			while (rs.next())
			{
				String instCode = rs.getString("instCode");
				String normSetName = rs.getString("ngName");
				int type = rs.getInt("popType");
				String popType;
				switch (type)
				{
					case GENPOP:
						popType = "G";
						break;
					case SPECPOP:
						popType = "S";
						break;
					default:
						// Invalid pop type - log it and skip it
						System.out.println("invalid population type code " + type + ".  Part=" + partId + ", DNA=" + dnaId);
						continue;
				}

				// If I can't find a vehicle code, skip it.
				String veh = INST_MAP.get(instCode);
				if (veh == null || veh.length() < 1)
				{
					System.out.println("Unable to find veh code for instCode " + instCode +
										".  norm Name=" + normSetName + ", popType=");
					continue;
				}

				if (! veh.equals(curVeh) || ! popType.equals(curType))
				{
					if (curVeh != null)
					{
						// Add the previous one to the output
						ret.add(inst);
						
					}

					// Making a new output object
					inst = new ResearchExtractHelper().new InstrumentNorm(veh, popType, normSetName);
					curVeh = veh;
					curType = popType;
				}
				
				// OK now process the norms
				String spss = rs.getString("spssValue");
				String mean = rs.getString("mean");
				String sd = rs.getString("stdev");

				if (spss.equals(curSpss) && curSpssType.equals(popType))
				{
					// Have one already... skip the build and continue
					continue;
				}
				else
				{
					// Add the norm to the list						
					inst.getScaleNorms().add(new ResearchExtractHelper().new ScaleNorm(spss, mean, sd));
					curSpss = spss;
					curSpssType = popType;
				}
			}	// while loop - Process the norms (run the result set of the norm query
				
			// Add the last inst object to the output list
			ret.add(inst);
			
			return ret;
		}
//		catch (SQLException ex)
//		{
//			// handle any errors
//			throw new Exception("SQL error for getInstNorms (scores).  " +
//							"SQLException: " + ex.getMessage() + ", " +
//							"SQLState: " + ex.getSQLState() + ", " +
//							"VendorError: " + ex.getErrorCode());
//		}
		finally
		{
			if (rs != null)
			{
			    try {  rs.close();  }
			    catch (SQLException sqlEx) { /* Ignore error */ }
				rs = null;
			}
			if (stmt != null)
			{
			    try {  stmt.close();  }
			    catch (SQLException sqlEx) { /* Ignore error */ }
			    stmt = null;
			}
		}		   
	}


//	/**
//	 * Fetch Norm data by instrument
//	 *
//	 * @param con - A database connection
//	 * @param normIds - A set of norm ids
//	 * @param conSccaleXref - a construct/spss lookup map
//	 * @return A list of InstrumentNorm objects
//	 * @throws Exception
//	 */
//	private static ArrayList<InstrumentNorm> getNorms(Connection con,
//			                                          HashSet<String> normIds,
//			                                          HashMap<String, String> conSccaleXref)
//		throws Exception
//	{
//		ArrayList<InstrumentNorm> ret = new ArrayList<InstrumentNorm>();
//
//		if (normIds.size() < 1)
//			return ret;
//	
//		String normList = "";
//		boolean first = true;
//		for (String str : normIds)
//		{
//			if (first)
//			{
//				first = false;
//			}
//			else
//			{
//				normList += ", ";
//			}
//			normList += "'" + str + "'";
//		}
//		
//		Statement stmt = null;
//		ResultSet rs = null;
//
//		// Get the norms in the list
//		StringBuffer sqlQuery = new StringBuffer();
//		sqlQuery.append("SELECT nn.TestId, ");
//		sqlQuery.append("       nn.NormName, ");
//		sqlQuery.append("       nn.XML ");
//		sqlQuery.append("  FROM norms nn ");
//		sqlQuery.append("  WHERE nn.UniqueIdStamp in (" + normList + ") ");
//		sqlQuery.append("  ORDER BY nn.TestId");
//
//		try
//		{
//		    stmt = con.createStatement();
//			rs = stmt.executeQuery(sqlQuery.toString());
//
//			if (! rs.isBeforeFirst())
//			{
//				// no norm data found... nothing to do here
//				return ret;
//			}
//
//			while (rs.next())
//			{
//				String tstIdV2 = rs.getString("TestId");
//				String normSetName = rs.getString("NormName");
//				String xml = rs.getString("XML");
//				//System.out.println(("Processing XML -->" + xml));
//				
//				// If I can't find an instrument code, skip it.
//				String veh = INST_MAP.get(tstIdV2);
//				if (veh == null || veh.length() < 1)
//				{
//					System.out.println("Unable to find instrument code for moduleId " + tstIdV2 +
//										".  norm Name=" + normSetName +
//										", XML-->" + xml);
//					continue;
//				}
//
//				// Set up for the XML parsing
//				XPathFactory  xpFactory = XPathFactory.newInstance();
//				XPath myXPath = (XPath) xpFactory.newXPath();
//				
//				try
//				{
//					// Parse the Group (pop type)
//					InputSource is = new InputSource(new ByteArrayInputStream(xml.getBytes()));
//					XPathExpression  xPathExpression = myXPath.compile("/XML_Fields/Group");			
//					String grp = xPathExpression.evaluate(is);
//					String popType = "";
//					if (grp.equals("1"))
//					{
//						popType = "G";	// GenPop
//					}
//					else if (grp.equals("2"))
//					{
//						popType = "S";	// SpecPop
//					}
//					else
//					{
//						// Invalid group - dump some info and skip it
//						System.out.println("Invalid population type - " + grp +
//											".  InstId=" + tstIdV2 +
//											", normset name=" + normSetName +
//											", XML-->" + xml);
//						continue;
//					}
//					//System.out.println("Pop type=" + popType);
//					
//					// Start making the output object
//					InstrumentNorm inst = new ResearchExtractHelper().new InstrumentNorm(veh, popType, normSetName);
//				
//					// Go to the 'Constructs' node
//					is = new InputSource(new ByteArrayInputStream(xml.getBytes()));
//					String expression = "/XML_Fields/Constructs/C";
//					NodeList nodes = (NodeList) myXPath.evaluate(expression, is, XPathConstants.NODESET);
//					if (nodes == null || nodes.getLength() < 1)
//					{
//						continue;
//					}
//
//					ArrayList<ScaleNorm> scaleNorms = new ArrayList<ScaleNorm>();
//					// We have the nodes called "C"... get the appropriate attributes
//					for (int i=0; i < nodes.getLength(); i++)
//					{
//						NamedNodeMap al = nodes.item(i).getAttributes();
//						
//						// Get the construct ID and transform it to an SPSS ID
//						String construct= al.getNamedItem("Id").getNodeValue();
//						String mean = al.getNamedItem("Avg").getNodeValue();
//						String sd = al.getNamedItem("StdDev").getNodeValue();
//						if (construct == null || construct.length() < 1 ||
//							mean      == null || mean.length() < 1 ||
//							sd        == null || sd.length() < 1)
//						{
//							// No data or invalid data... skip this node
//							continue;
//						}
//						//System.out.println("avg attribute:  val=" + ni.getNodeValue());
//						
//						String spss = conSccaleXref.get(construct);
//						if (spss == null || spss.length() < 1)
//						{
//							// Unrecognized construct name
//							continue;
//						}
//						
//						// Make the norm data object and add it to the list
//						scaleNorms.add(new ResearchExtractHelper().new ScaleNorm(spss, mean, sd));
//					}
//					
//					// Done with this normSet, add it to the enclosing instrument object
//					inst.setScaleNorms(scaleNorms);
//					
//					// And add that object to the output list
//					ret.add(inst);
//				}
//				catch (XPathExpressionException ex)
//				{
//					throw new Exception("Unable to parse norm XML:  testId=" + tstIdV2 +
//										", normsetName=" + normSetName + ", XML=" + xml);
//				}
//			}	// Process the norms (run the result set of the norm query
//			
//			return ret;
//		}
//		catch (SQLException ex)
//		{
//			// handle any errors
//			throw new Exception("SQL error for getInstNorms (scores).  " +
//							"SQLException: " + ex.getMessage() + ", " +
//							"SQLState: " + ex.getSQLState() + ", " +
//							"VendorError: " + ex.getErrorCode());
//		}
//		finally
//		{
//			if (rs != null)
//			{
//			    try {  rs.close();  }
//			    catch (SQLException sqlEx) { /* Ignore error */ }
//				rs = null;
//			}
//			if (stmt != null)
//			{
//			    try {  stmt.close();  }
//			    catch (SQLException sqlEx) { /* Ignore error */ }
//			    stmt = null;
//			}
//		}		   
//	}
	
	
	/**
	 * Write (either insert or update) the extract candidate row.
	 *
	 * @param stat - Extract completion status.
	 * @param partId - The participant ID
	 * @param dnaId - The dnaID
	 * @param xml - The XML string to write
	 * @throws Exception
	 */
	public static void writeExtractRow(int stat, String partId, long dnaId, String xml)
		throws Exception
	{
		// Generate the query
		StringBuffer sqlQuery = new StringBuffer();
		
		// Remove double quotes from the XML so it doesn't kill us in the query
		xml = xml.replace("'","''");
		
		if (stat == EXT_CPLT)
		{
			// Never update a completed extract (should also be checked earlier in the logic)
			return;
		}
		else if (stat == EXT_CAND || stat == EXT_DMNT)
		{
			sqlQuery.append("INSERT INTO pdi_abd_extract_cand ");
			sqlQuery.append("  (participantId, dnaId, finalizeDate, ");
			sqlQuery.append("   extractStatus, xmlData, ");
			sqlQuery.append("   lastUserId, lastDate) ");
			sqlQuery.append("  VALUES(" + partId + ", " + dnaId + ", GETDATE(), ");
			sqlQuery.append("         " + stat + ", N'" + xml + "', ");
			sqlQuery.append("         'inpDataHlpr', GETDATE()");
			sqlQuery.append(")");
		}
		else if (stat == EXT_DMNT_ERR)
		{
			// Note that there is no xml in an error row and the string called xml is really error text
			sqlQuery.append("INSERT INTO pdi_abd_extract_cand ");
			sqlQuery.append("  (participantId, dnaId, finalizeDate, ");
			sqlQuery.append("   extractStatus, xmlData, ");
			sqlQuery.append("   extractComment, ");
			sqlQuery.append("   lastUserId, lastDate) ");
			sqlQuery.append("  VALUES(" + partId + ", " + dnaId + ", GETDATE(), ");
			sqlQuery.append("         " + stat + ", N'', ");
			sqlQuery.append("         N'" + xml + "', ");
			sqlQuery.append("         'inpDataHlpr', GETDATE()");
			sqlQuery.append(")");
		}
		else
		{
			// Any other status is an update.
			sqlQuery.append("UPDATE pdi_abd_extract_cand ");
			sqlQuery.append("  SET finalizeDate = GETDATE(), ");
			sqlQuery.append("      extractStatus = 0, ");
			sqlQuery.append("      extractComment = null, ");
			sqlQuery.append("      xmlData = N'" + xml + "', ");
			sqlQuery.append("      lastUserId = 'inpDataHlpr', ");
			sqlQuery.append("      lastDate = GETDATE() ");
			sqlQuery.append("  WHERE participantId = '" + partId + "' ");
			sqlQuery.append("    AND dnaId = " + dnaId);
		}
		
		// prep it
		DataLayerPreparedStatement dlps =  AbyDDatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			throw new Exception("Error preparing extract query.  Query=" + sqlQuery.toString() +
					".  Code=" + dlps.getStatus().getExceptionCode() + 
					", Msg=" + dlps.getStatus().getExceptionMessage());
		}

		// do it
		DataResult dr = null;
		try
		{
			if (stat == 0)
			{
				dr =  AbyDDatabaseUtils.insert(dlps);
				if (dr.isInError())
				{
					throw new Exception("Error inserting extract data.  Code=" + 
							dr.getStatus().getExceptionCode() + 
							", Msg=" + dr.getStatus().getExceptionMessage());
				}
			}
			else
			{
				dr = AbyDDatabaseUtils.update(dlps);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally
		{
			if (dr != null) {  dr.close();  dr = null; }
		}
	}



	//
	// Instance data.
	//

		// No data or methods... meant to have a static invocation

	//
	// Instance methods.
	//
	//



	//
	// Private classes
	//
	//
	
	// Competency accumulator bucket
	private class CompData
	{
		private double _cum = 0;
		private int _count = 0;
		
		public void addToCum(double value)
		{
			_cum += value;
			_count++;
		}
		
		public double getAvg()
		{
			if (_cum == 0.0 || _count == 0)
				return 0.0;
			
			return Math.floor((_cum * 100 / (double)_count) + 0.5) / 100.0;
		}
	}

	
	// Holds all of the data for a normset for an instrument
	private class InstrumentNorm
	{
		private String _instId;		// veh code
		private String _normSetName;
		private String _normType;	// G or S (genPop or specPop)
		private ArrayList<ScaleNorm> _scaleNorms;
		
		public InstrumentNorm(String veh, String popType, String normName)
		{
			_instId = veh;
			_normSetName = normName;
			_normType = popType;
		}

		public String getInstId()
		{
			return _instId;
		}

		public String getNormSetName()
		{
			return _normSetName;
		}

		public String getNormType()
		{
			return _normType;
		}

//		public void setScaleNorms(ArrayList<ScaleNorm> value)
//		{
//			_scaleNorms = value;
//		}
		public ArrayList<ScaleNorm> getScaleNorms()
		{
			if (_scaleNorms == null)
				_scaleNorms = new ArrayList<ScaleNorm>();
			return _scaleNorms;
		}
	}

	
	// One norm with it's identifying SPSS scale id
	private class ScaleNorm
	{
		private String _scaleId;
		private String _mean;
		private String _stdDev;
		
		public ScaleNorm(String id, String m, String sd)
		{
			_scaleId = id;
			_mean = m;
			_stdDev = sd;
		}
		
		public String getScaleId()
		{
			return _scaleId;
		}
		
		public String getMean()
		{
			return _mean;
		}
		
		public String getStdDev()
		{
			return _stdDev;
		}
	}

}
