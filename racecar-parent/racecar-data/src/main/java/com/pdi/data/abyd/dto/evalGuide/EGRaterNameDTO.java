/**
 * Copyright (c) 2009,2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */

package com.pdi.data.abyd.dto.evalGuide;

import com.pdi.data.abyd.dto.common.EGRaterNameData;
import com.pdi.data.util.language.LanguageItemDTO;

/**
 * EGRaterNameDTO is a thin class containing all of the data
 * needed to add or update the rater name for an Eval Guide.
 * 
 * NOTE:  The enhancements requested by the business at migration time (Summer, 2011)
 * added a pair of language components; the name is now a little misleading.
 * 
 * @author		Ken Beukelman
 */
public class EGRaterNameDTO
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _participantId;
	private long _dnaId;
	private long _moduleId;
	private EGRaterNameData _raterNameData;
	private LanguageItemDTO _simLangItem;
	private String _simLangOther;
	private int _deliveryMethodId;


	//
	// Constructors.
	//
	public EGRaterNameDTO()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public String getParticipantId()
	{
		return _participantId;
	}
		
	public void setParticipantId(String value)
	{
		_participantId = value;
	}

	/*****************************************************************************************/
	public long getDnaId()
	{
		return _dnaId;
	}
		
	public void setDnaId(long value)
	{
		_dnaId = value;
	}

	/*****************************************************************************************/
	public long getModuleId()
	{
		return _moduleId;
	}
		
	public void setModuleId(long value)
	{
		_moduleId = value;
	}

	/*****************************************************************************************/
	public EGRaterNameData getRaterNameData()
	{
		return _raterNameData;
	}
		
	public void setRaterNameData(EGRaterNameData value)
	{
		_raterNameData = value;
	}

	/*****************************************************************************************/
	public LanguageItemDTO getSimLangItem()
	{
		return _simLangItem;
	}
		
	public void setSimLangItem(LanguageItemDTO value)
	{
		_simLangItem = value;
	}

	/*****************************************************************************************/
	public String getSimLangOther()
	{
		return _simLangOther;
	}
		
	public void setSimLangOther(String value)
	{
		_simLangOther = value;
	}

	/*****************************************************************************************/

	public int getDeliveryMethodId() {
		return _deliveryMethodId;
	}

	public void setDeliveryMethodId(int _deliveryMethodId) {
		this._deliveryMethodId = _deliveryMethodId;
	}
}
