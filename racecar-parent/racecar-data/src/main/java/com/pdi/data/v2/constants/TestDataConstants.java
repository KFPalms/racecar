package com.pdi.data.v2.constants;

import java.util.ArrayList;

public class TestDataConstants {
	
//	public static final transient ArrayList <String> ALL_ABYD_SCORED_INSTRUMENTS = new ArrayList <String> ();
//	static {
//		ALL_ABYD_SCORED_INSTRUMENTS.add("rv");		// Raven's APM Form B
//		ALL_ABYD_SCORED_INSTRUMENTS.add("lei");		// Leadership Experience Inventory (LEI)
//		//ALL_ABYD_SCORED_INSTRUMENTS.add("cs");		//Career Survey	
//		ALL_ABYD_SCORED_INSTRUMENTS.add("cs2");		// New Career Survey	
//		ALL_ABYD_SCORED_INSTRUMENTS.add("gpil");	// Global Personality Index (GPI) - Standard version (300 Questions) 
//		ALL_ABYD_SCORED_INSTRUMENTS.add("wg");		// Watson-Glaser II
//		ALL_ABYD_SCORED_INSTRUMENTS.add("finex");	// Financial Exercise
//
//	};
//	
//	public static final transient ArrayList <String> ALL_TLT_SCORED_INSTRUMENTS = new ArrayList <String> ();
//	static {
//		ALL_TLT_SCORED_INSTRUMENTS.add("rv");		// Raven's APM Form B
//		ALL_TLT_SCORED_INSTRUMENTS.add("lei");		// Leadership Experience Inventory (LEI)
//		ALL_TLT_SCORED_INSTRUMENTS.add("cs");		//Career Survey	
//		ALL_TLT_SCORED_INSTRUMENTS.add("cs2");		// New Career Survey	
//		ALL_TLT_SCORED_INSTRUMENTS.add("gpil");		// Global Personality Index (GPI) - Standard version (300 Questions) 
//		ALL_TLT_SCORED_INSTRUMENTS.add("gpi");		// Global Personality Index (GPI) - Short version 
//		ALL_TLT_SCORED_INSTRUMENTS.add("wg");		// Watson-Glaser II
//		ALL_TLT_SCORED_INSTRUMENTS.add("finex");	// Financial Exercise
//
//	};

	public static final transient ArrayList <String> TESTDATA_SCORED_INSTRUMENTS = new ArrayList <String> ();
	static {
		TESTDATA_SCORED_INSTRUMENTS.add("rv");		// Raven's APM Form B
		TESTDATA_SCORED_INSTRUMENTS.add("lei");		// Leadership Experience Inventory (LEI)
		TESTDATA_SCORED_INSTRUMENTS.add("cs");		//Career Survey	
		TESTDATA_SCORED_INSTRUMENTS.add("cs2");		// New Career Survey	
		TESTDATA_SCORED_INSTRUMENTS.add("gpil");	// Global Personality Index (GPI) - Standard version (300 Questions) 
		TESTDATA_SCORED_INSTRUMENTS.add("gpi");		// Global Personality Index (GPI) - Short version 
		TESTDATA_SCORED_INSTRUMENTS.add("wg");		// Watson-Glaser II
		TESTDATA_SCORED_INSTRUMENTS.add("finex");	// Financial Exercise

	};
}
