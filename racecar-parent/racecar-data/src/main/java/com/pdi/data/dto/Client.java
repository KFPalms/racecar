/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.data.dto;

import java.util.HashMap;
//import java.util.Iterator;
//import java.util.Map;
//import java.util.Map.Entry;

/**
 * Client is a basic data class that contains client data.
 * 
 * @author		Gavin Myers
 */
public class Client extends CoreDataObject
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	/**
	 * 
	 */
	private static final long serialVersionUID = 3978124464456011912L;
	//
	// Instance data.
	//
	private String id;
	private String name;
	private HashMap<String, String> metadata;

	//
	// Constructors.
	//
	public Client()
	{
		// does nothing right now
	}

	public Client(String idIn)
	{
		id = idIn;
	}

	//
	// Instance methods.
	//
	
	public String toString()
	{
		String ret = "Client:";
		ret += "  ID=" + id;
		ret += ", Name=" + name;
//		ret += "\n  Meta:";
//		for (Iterator<Entry<String, String>> itr=metadata.entrySet().iterator(); itr.hasNext(); )
//		{
//			Map.Entry<String, String> ent = itr.next();
//			ret += "\n     " + ent.getKey() + "=" + ent.getValue();
//		}
////		ret += "\n";
		
		return ret;
			
	}
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public void setId(String value)
	{
		id = value;
	}
	
	public String getId()
	{
		return id;
	}

	/*****************************************************************************************/
	public void setName(String value)
	{
		name = value;
	}
	
	public String getName()
	{
		return name;
	}

	/*****************************************************************************************/
	public void setMetadata(HashMap<String, String> value)
	{
		metadata = value;
	}
	
	public HashMap<String, String> getMetadata()
	{
		return metadata;
	}
}
