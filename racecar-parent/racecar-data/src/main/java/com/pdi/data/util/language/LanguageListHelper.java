/**
 * Copyright (c) 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.util.language;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.pdi.data.util.language.LanguageItemDTO;

/**
 * LanguageListHelper contains the code needed to fetch a list of available languages.
 *
 * @author		Ken Beukelman
 */
public class LanguageListHelper
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private Connection con;		// A database connection


	//
	// Constructors.
	//
	public LanguageListHelper(Connection connection)
		throws Exception
	{
		this.con = connection;
	}

	//
	// Instance methods.
	//


	/**
	 * Fetch A "Standard" list of language data items.  This list is all of the languages (or
	 * the report subset of languages) and is not affected by the Eval Guide flag
	 * NOTE:  The DTO objects contain the translation name as display text.
	 *
	 * @param excludeDflt - Flag telling the logic to return the default language in the list
	 * @return an ordered List of LanguageItemDTO objects
	 * @throws Exception
	 */
	public ArrayList<LanguageItemDTO> getLangList(boolean allLang, boolean includeDflt)
		throws Exception
	{
		long dfltLangId = (long) DefaultLanguageHelper.fetchDefaultLanguageId();
		
		ArrayList<LanguageItemDTO> ret = new ArrayList<LanguageItemDTO>();
		Statement stmt = null;
		ResultSet rs = null;
		
		String whereClause = "";
		if (! allLang)
			whereClause = ("  WHERE aln.rptLang = 1 ");

		StringBuffer sqlQuery = new StringBuffer();		
		sqlQuery.append("SELECT aln.languageId, ");
		sqlQuery.append("       aln.languageCode, ");
		sqlQuery.append("       txt.text as nameText ");
		sqlQuery.append("  FROM pdi_abd_language aln ");
		sqlQuery.append("    LEFT JOIN pdi_abd_text txt on (aln.textId = txt.textId and txt.languageId = " + dfltLangId + ") ");
		sqlQuery.append(whereClause);
		sqlQuery.append("  ORDER BY nameText");

		try
		{
			stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			if (! rs.isBeforeFirst())
			{
				throw new Exception("No language data available");
			}
			while (rs.next())
			{
				long langId = rs.getInt("languageId");

				if (! includeDflt  && langId == dfltLangId)
					continue;
				
				LanguageItemDTO itm = new LanguageItemDTO();
				itm.setId(rs.getLong("languageId"));
				itm.setCode(rs.getString("languageCode"));
				itm.setDisplayName(rs.getString("nameText"));
				ret.add(itm);
			}

			return ret;
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new SQLException("Fetching Languages.  msg=" + ex.getMessage(), ex);
		}
		finally
		{
			if (rs != null)
			{
				try  {  rs.close();  }
				catch (SQLException sqlEx)  {  }
				rs = null;
			}
			if (stmt != null)
			{
				try  {  stmt.close();  }
				catch (SQLException sqlEx)  {  }
				stmt = null;
			}
		}			
	}


	/**
	 * Fetch the eval Guide list of language data items.  This code does NOT add the
	 * "Other" item
	 * NOTE:  The DTO objects contain the spoken name as display text.
	 *
	 * @param excludeDflt - Flag telling the logic to return the default language in the list
	 * @return an ordered List of LanguageItemDTO objects
	 * @throws Exception
	 */
	public ArrayList<LanguageItemDTO> getEgLangList()
		throws Exception
	{
		long dfltLangId = (long) DefaultLanguageHelper.fetchDefaultLanguageId();
		
		ArrayList<LanguageItemDTO> ret = new ArrayList<LanguageItemDTO>();
		Statement stmt = null;
		ResultSet rs = null;
		
		StringBuffer sqlQuery = new StringBuffer();		
		sqlQuery.append("SELECT aln.languageId, ");
		sqlQuery.append("       aln.languageCode, ");
		sqlQuery.append("       txt.text as nameText ");
		sqlQuery.append("  FROM pdi_abd_language aln ");
		sqlQuery.append("    LEFT JOIN pdi_abd_text txt ON (txt.textId = aln.spokenTextId AND txt.languageId = " + dfltLangId + ") ");
		sqlQuery.append("  WHERE aln.egLang = 1 ");
		sqlQuery.append("  ORDER BY nameText");

		try
		{
			stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			if (! rs.isBeforeFirst())
			{
				throw new Exception("No Eval Guide language data available");
			}
			while (rs.next())
			{
				LanguageItemDTO itm = new LanguageItemDTO();
				itm.setId(rs.getLong("languageId"));
				itm.setCode(rs.getString("languageCode"));
				itm.setDisplayName(rs.getString("nameText"));
				ret.add(itm);
			}

			return ret;
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new SQLException("Fetching Languages.  msg=" + ex.getMessage(), ex);
		}
		finally
		{
			if (rs != null)
			{
				try  {  rs.close();  }
				catch (SQLException sqlEx)  {  }
				rs = null;
			}
			if (stmt != null)
			{
				try  {  stmt.close();  }
				catch (SQLException sqlEx)  {  }
				stmt = null;
			}
		}			
	}
}
