/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.dto.common;


/**
 * StatusDTO is a general purpose class generally used to return status from RPC
 * calls that don't return anything.  It can also be sub-classed by other DTOs
 * to allow them to return status information as well.
 * 
 * @author		Ken Beukelman
 */
public class StatusDTO
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private boolean _successStatus;
	private String _statusMsg;		// Generally null if successful, populated if not

	//
	// Constructors.
	//
	public StatusDTO()
	{
		// Assumes that the operation being monitored will be successful
		// and that no message will need to be passed.
		_successStatus = true;
		_statusMsg = null;
	}
	
	
	public StatusDTO(String errMsg)
	{
		// Assumes this is invoked upon failure.  Failure message is passed in
		_successStatus = false;
		_statusMsg = errMsg;
	}
	
	//
	// Instance methods.
	//

	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
	
	/*****************************************************************************************/
	public boolean getSuccessStatus()
	{
		return _successStatus;
	}
	
	public void setSuccessStatus(boolean value)
	{
		_successStatus = value;
	}
	
	/*****************************************************************************************/
	public String getStatusMsg()
	{
		return _statusMsg;
	}
	
	public void setStatusMsg(String value)
	{
		_statusMsg = value;
	}
}
