/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.helpers;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;

import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.util.RequestStatus;
import com.pdi.data.v3.dto.LanguageDTO;
import com.pdi.data.v3.dto.LanguageListDTO;
import com.pdi.data.v3.dto.SessionUserDTO;
import com.pdi.data.v3.util.V3DatabaseUtils;

/**
 * LanguageDataHelper provides functionality to add, 
 * change, delete, or fetch language information.
 *
 * @author		Ken Beukelman
 */
public class LanguageDataHelper
{
	//
	// Static data.
	//

	//
	// Static methods.
	//


	/**
	 * Insert a new language
	 * @param sessionUser
	 * @param An LanguageDTO object
	 * @return A new LanguageDTO object with the status and the new ID only
	 */
	public static LanguageDTO addLanguage(SessionUserDTO sessionUser, LanguageDTO newLang)
	{
		DataResult dr = null;
		LanguageDTO ret = new LanguageDTO();

		if(newLang.getName() == null         || newLang.getName().trim().length() == 0  ||
		   newLang.getIsoCode() == null      || newLang.getIsoCode().trim().length() == 0)
		{
			ret.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
					"An internalCode, displayName, and isoCode are required to add a language."));
			return ret;
		}

//TODO Modify query to eliminate hard-coded "xxxDate" fields once we have a DB with triggers installed

	// This is a test query until we get rights to add triggers.  At that time, only the
	// first two fields will be needed; this query should then be modified to reflect that.
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("INSERT INTO language_lookup ");
		sqlQuery.append("  (isoCode, displayName, createdBy, createdDate, modifiedBy, modifiedDate) ");
		sqlQuery.append("  VALUES( ?, ?, ?, GETDATE(), ?, GETDATE())");

		DataLayerPreparedStatement dlps = V3DatabaseUtils.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
		if (dlps.isInError())
		{
			ret.setStatus(dlps.getStatus());
			return ret;
		}

		try
		{
			dlps.getPreparedStatement().setString(1,newLang.getIsoCode());
			dlps.getPreparedStatement().setString(2,newLang.getName());
			dlps.getPreparedStatement().setString(3,sessionUser.getUsername());
			dlps.getPreparedStatement().setString(4,sessionUser.getUsername());

			dr = V3DatabaseUtils.insert(dlps);
			
			ret.setStatus(dr.getStatus());
			switch (ret.getStatus().getStatusCode())
			{
				case RequestStatus.RS_ERROR:
					break;
				case RequestStatus.DBS_DUP_KEY:
					// ISO code is only user changeable unique index
					ret.getStatus().setExceptionMessage("ISO code code (" + newLang.getIsoCode() + ") already exists.");
					break;
				case RequestStatus.RS_OK:
					ret.setId(dr.getNewId());
					break;
				default:
					ret.setStatus(unexpectedStatus(ret.getStatus()));
					break;
			}
	
			return ret;
		}
		catch (SQLException ex)
		{
			ret.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
											ex.getErrorCode(), "addLanguage:  " + ex.getMessage()));
			return ret;
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}


	/**
	 * Get a specific language
	 * @param sessionUser
	 * @param language id
	 * @return A LanguageDTO object
	 */
	public static LanguageDTO getLanguage(SessionUserDTO sessionUser, long langId)
	{
		DataResult dr = null;
		LanguageDTO ret = new LanguageDTO();

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT displayName, ");
		sqlQuery.append("       isoCode ");
		sqlQuery.append("  FROM language_lookup ");
		sqlQuery.append("  WHERE active = 1 ");
		sqlQuery.append("    AND languageId = ?");
		
		DataLayerPreparedStatement dlps = V3DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			ret.setStatus(dlps.getStatus());
			return ret;
		}

		try
		{
			dlps.getPreparedStatement().setLong(1,langId);

			dr = V3DatabaseUtils.select(dlps);
			
			ret.setStatus(dr.getStatus());
			switch (ret.getStatus().getStatusCode())
			{
				case RequestStatus.RS_ERROR:
					ret.setStatus(dr.getStatus());
					break;
				case RequestStatus.DBS_NO_DATA:
					ret.getStatus().setExceptionMessage("Language ID " + langId + " does not exist.");
					break;
				case RequestStatus.RS_OK:
					ResultSet rs = dr.getResultSet();
					rs.next();
					moveData(rs, ret);
					ret.setId(langId);
					break;
				default:
					ret.setStatus(unexpectedStatus(ret.getStatus()));
					break;
			}
	
			return ret;
		}
		catch (SQLException ex)
		{
			ret.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
											ex.getErrorCode(), "getLanguage:  " + ex.getMessage()));
			return ret;
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}

	
	/**
	 * Get all active languages in the language
	 * type lookup table in alphabetical order by name.
	 * 
	 * @param sessionUser
	 * @return An ArrayList of EmRecipientTypeDTO objects
	 */
	public static LanguageListDTO getAllLanguages(SessionUserDTO sessionUser)
	{
		DataResult dr = null;
		LanguageListDTO ret = new LanguageListDTO();
		
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT languageId, ");
		sqlQuery.append("       displayName, ");
		sqlQuery.append("       isoCode ");
		sqlQuery.append("  FROM language_lookup ");
		sqlQuery.append("  WHERE active = 1 ");
		sqlQuery.append("  ORDER BY displayName");

		DataLayerPreparedStatement dlps = V3DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			ret.setStatus(dlps.getStatus());
			return ret;
		}
		
		dr = V3DatabaseUtils.select(dlps);

		ret.setStatus(dr.getStatus());
		try
		{
			switch (ret.getStatus().getStatusCode())
			{
				case RequestStatus.RS_ERROR:
					break;
				case RequestStatus.DBS_NO_DATA:
					ret.setLangList(null);
					ret.getStatus().setExceptionMessage("No languages are present.");
					break;
				case RequestStatus.RS_OK:
					ResultSet rs = dr.getResultSet();
					try
					{
						while (rs.next())
						{
							LanguageDTO lang = new LanguageDTO(dr.getStatus());
							moveData(rs, lang);
							ret.getLangList().add(lang);
						}
					}
					catch (SQLException ex)
					{
						ret.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
								ex.getErrorCode(), "getAllLanguages:  " + ex.getMessage()));
						return ret;
					}
					break;
				default:
					ret.setStatus(unexpectedStatus(ret.getStatus()));
					break;
			}
			
			return ret;
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}


	/**
	 * Update a language
	 * 
	 * @param sessionUser
	 * @param An LanguageDTO object
	 * @return A RequestStatus object
	 */
	public static RequestStatus updateLanguage(SessionUserDTO sessionUser, LanguageDTO updtLang)
	{
		DataResult dr = null;
		
		if (updtLang.getId() == 0)
		{
			return new RequestStatus(RequestStatus.RS_ERROR,
					"Update requires a valid Language ID");
		}
		// We need at least one piece of data to update
		if ((updtLang.getName() == null     || updtLang.getName().trim().length() == 0)  &&
			(updtLang.getIsoCode() == null  || updtLang.getIsoCode().trim().length() == 0) )
		{
			return new RequestStatus(RequestStatus.RS_ERROR,
					"Update requires a valid Display Name, a valid ISO code or both");
		}

		String dn = null;
		String ic = null;
		int index = 0;
		String setStr = "";
		if (updtLang.getName() != null && updtLang.getName().trim().length() != 0)
		{
			dn = updtLang.getName().trim().replace("'","''");
			index += 1;
			setStr += "displayName = ?";
		}
		if (updtLang.getIsoCode() != null && updtLang.getIsoCode().trim().length() != 0)
		{
			ic = updtLang.getIsoCode().trim().replace("'","''");
			index += 2;
			if (setStr != null)
			{
				setStr += ", ";
			}
			setStr += "isoCode = ? ";
		}
		setStr += ", modifiedBy = ? ";

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("UPDATE language_lookup ");
		sqlQuery.append("  SET " + setStr + " ");
		sqlQuery.append("  WHERE LanguageId = ?");
		
		DataLayerPreparedStatement dlps = V3DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			return dlps.getStatus();
		}

		try
		{
			switch (index)
			{
				case 1:
					dlps.getPreparedStatement().setString(1, dn);
					dlps.getPreparedStatement().setString(2, sessionUser.getUsername());
					dlps.getPreparedStatement().setLong(3, updtLang.getId());
					break;
				case 2:
					dlps.getPreparedStatement().setString(1, ic);
					dlps.getPreparedStatement().setString(2, sessionUser.getUsername());
					dlps.getPreparedStatement().setLong(3, updtLang.getId());
					break;
				case 3:
					dlps.getPreparedStatement().setString(1, dn);
					dlps.getPreparedStatement().setString(2, ic);
					dlps.getPreparedStatement().setString(3, sessionUser.getUsername());
					dlps.getPreparedStatement().setLong(4, updtLang.getId());
					break;
			}

			dr = V3DatabaseUtils.update(dlps);
			
			switch (dr.getStatus().getStatusCode())
			{
				case RequestStatus.RS_ERROR:
					return dr.getStatus();
				case RequestStatus.RS_OK:
					if (dr.getRowCount() > 0)
					{
						return dr.getStatus();
					}
					else
					{
						return new RequestStatus(RequestStatus.DBS_NO_DATA, "No data to update for language Id " + updtLang.getId());
					}
				default:
					return unexpectedStatus(dr.getStatus());
			}
		}
		catch (SQLException ex)
		{
			return new RequestStatus(RequestStatus.RS_ERROR,
					ex.getErrorCode(), "updateLanguage:  " + ex.getMessage());
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}
	

	/**
	 * Delete a language - note that this is really not a true delete.  We simply
	 * update the active flag to 0.
	 * 
	 * @param sessionUser
	 * @param An LanguageDTO object
	 * @return A RequestStatus object
	 */
	public static RequestStatus deleteLanguage(SessionUserDTO sessionUser, long langId)
	{
		DataResult dr = null;
		
		if (langId == 0)
		{
			return new RequestStatus(RequestStatus.RS_ERROR,
					"Delete requires a valid Recipient Type ID");
		}

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("UPDATE language_lookup ");
		sqlQuery.append("  SET active = 0, modifiedBy = ?");
		sqlQuery.append("  WHERE languageId = ?");
		
		DataLayerPreparedStatement dlps = V3DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			return dlps.getStatus();
		}

		try
		{
			dlps.getPreparedStatement().setString(1, sessionUser.getUsername());
			dlps.getPreparedStatement().setLong(2, langId);

			dr = V3DatabaseUtils.update(dlps);
			
			switch (dr.getStatus().getStatusCode())
			{
				case RequestStatus.RS_ERROR:
				case RequestStatus.RS_OK:
					return dr.getStatus();
				default:
					return unexpectedStatus(dr.getStatus());
			}
		}
		catch (SQLException ex)
		{
			return new RequestStatus(RequestStatus.RS_ERROR,
					ex.getErrorCode(), "deleteLanguage:  " + ex.getMessage());
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}
	

	/////////////////////
	// Private methods //
	/////////////////////

	/**
	 * Process the input status object to add the "Unexpected..." message
	 * 
	 * @param The RequestStatus object 
	 * @return A RequestStatus object
	 */
	private static RequestStatus unexpectedStatus(RequestStatus inp)
	{
		RequestStatus out = new RequestStatus(inp);
		String str = "Unexpected Status code encountered";
		str += (out.getExceptionMessage() == null ? "." : "; msg=" + out.getExceptionMessage());
		inp.setExceptionMessage(str);
		return out;
	}
	
	
	/**
	 * Move data from a ResultSet object to a LanguageDTO object
	 * This function has been written to be "generic";  if the data is not
	 * in the result set, it won't be moved to the DTO.
	 * 
	 * @param rs A ResultSet object
	 * @param dto A LanguageDTO object
	 */
	private static void moveData(ResultSet rs, LanguageDTO dto)
	{
		try
		{
			ResultSetMetaData rsmd = rs.getMetaData();

			// Get all of the columns
			HashSet<String> set = new HashSet<String>();

			for(int i=1; i <= rsmd.getColumnCount(); i++)
			{
				//set.add(rsmd.getColumnName(i));
				set.add(rsmd.getColumnLabel(i));
			}
			
			//  Put them in the proper slot in the dto
			if (set.contains("languageId"))
			{
				dto.setId(rs.getInt("languageId"));
			}
			if (set.contains("displayName"))
			{
				dto.setName(rs.getString("displayName"));
			}
			if (set.contains("isoCode"))
			{
				dto.setIsoCode(rs.getString("isoCode"));
			}
			if (set.contains("active"))
			{
				dto.setActiveFlag(rs.getBoolean("active"));
			}
			
			return;
		}
		catch (SQLException ex)
		{
			dto.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
					ex.getErrorCode(), ex.getMessage()));
			
			// Log the error
			String exc = "\n************************************* " + 
				"\nError in LanguageDataHelper.moveData:  " +
				"\nSQLException: " + ex.getMessage() + " " +
				"\nSQLState: " + ex.getSQLState() + " " +
				"\nVendorError: " + ex.getErrorCode();
			System.err.println(exc);
			System.err.println("************************************* ");

			return;
		}
	}
	//
	// Instance data.
	//
	
	//
	// Constructors.
	//

	//
	// Instance methods.
	//

}
