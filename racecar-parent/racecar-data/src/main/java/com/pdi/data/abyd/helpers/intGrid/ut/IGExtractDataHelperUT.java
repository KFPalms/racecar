package com.pdi.data.abyd.helpers.intGrid.ut;

import java.sql.Connection;

import com.pdi.data.abyd.dto.intGrid.IGExtractDataDTO;
import com.pdi.data.abyd.helpers.intGrid.IGExtractDataHelper;
import com.pdi.ut.UnitTestUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;

import junit.framework.TestCase;

public class IGExtractDataHelperUT  extends TestCase
{
	//
	// Constructors
	//
	public IGExtractDataHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
	throws Exception
	{
		junit.textui.TestRunner.run(IGExtractDataHelperUT.class);
	}


	///*
	// * testGetIGExtractData
	// */
	//public void testGetIGExtractData()
	//	throws Exception
	//{
	//	UnitTestUtils.start(this);
	//	AbyDDatabaseUtils.setUnitTest(true);
	//	
	//	Connection con = AbyDDatabaseUtils.getDBConnection();
	//	String candId = "60254";
	//	long dnaId = 6;
	//
	//	IGExtractDataHelper helper = new IGExtractDataHelper(con, candId, dnaId);        
	//	IGExtractDataDTO ret =  helper.getIGExtractData();
	//	System.out.print(ret.toString());
	//
	//	UnitTestUtils.stop(this);
	//}

}
