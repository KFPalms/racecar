/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.helpers;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashSet;

import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.util.RequestStatus;
import com.pdi.data.v3.dto.EmailGroupDTO;
import com.pdi.data.v3.dto.EmailGroupListDTO;
import com.pdi.data.v3.dto.SessionUserDTO;
import com.pdi.data.v3.util.V3DatabaseUtils;


/**
 * EmailGroupsDataHelper provides functionality to fetch,
 * add, change, or delete Email Recipient Types.
 *
 * @author		Ken Beukelman
 */
public class EmailGroupDataHelper
{
	//
	// Static data.
	//

	//
	// Static methods.
	//
	//
	

	/////////////////////
	//Private classes  //
	/////////////////////
	
	/**
	* Process the input status object to add the "Unexpected..." message
	* 
	* @param The RequestStatus object 
	* @return A RequestStatus object
	*/
	private static RequestStatus unexpectedStatus(RequestStatus inp)
	{
		RequestStatus out = new RequestStatus(inp);
		String str = "Unexpected Status code encountered";
		str += (out.getExceptionMessage() == null ? "." : "; msg=" + out.getExceptionMessage());
		inp.setExceptionMessage(str);
		return out;
	}
	
	
	/**
	* Move data from a ResultSet object to a EmailGroupDTO object
	* This function has been written to be "generic";  if the data is not
	* in the result set, it won't be moved to the DTO.
	* 
	* NOTE:  This may not be needed here because we are moving all of the
	*		  data in the Result set.  This does allow us to have different
	*		  resultSets that return differing data and load only those
	*		  parts of the DTO that we have data for.
	*
	* @param rs A ResultSet object
	* @param dto An EmailGroupDTO object
	*/
	private static void moveData(ResultSet rs, EmailGroupDTO dto)
	{
		try
		{
			ResultSetMetaData rsmd = rs.getMetaData();
			
			// Get all of the columns
			HashSet<String> set = new HashSet<String>();
			
			for(int i=1; i <= rsmd.getColumnCount(); i++)
			{
				//set.add(rsmd.getColumnName(i));
				set.add(rsmd.getColumnLabel(i));
			}
			
			//  Put them in the proper slot in the dto
			if (set.contains("groupId"))
			{
				dto.setId(rs.getInt("groupId"));
			}
			if (set.contains("groupCode"))
			{
				dto.setCode(rs.getString("groupCode"));
			}
			if (set.contains("displayName"))
			{
				dto.setName(rs.getString("displayName"));
			}
			if (set.contains("description"))
			{
				dto.setDescription(rs.getString("description"));
			}
			
			return;
		}
		catch (SQLException ex)
		{
			dto.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
				ex.getErrorCode(), ex.getMessage()));
			
			// Log the error
			String exc = "\n************************************* " + 
						 "\nError in EmailGroupDataHelper.moveData: " +
						 "\nSQLException: " + ex.getMessage() + " " +
						 "\nSQLState: " + ex.getSQLState() + " " +
						 "\nVendorError: " + ex.getErrorCode();
			System.err.println(exc);
			System.err.println("************************************* ");
			
			return;
		}
	}


	///////////////////////////
	//Public static classes  //
	///////////////////////////

	/**
	 * Get all active e-mail groups in the e-mail group
	 * lookup table in natural order.
	 * 
	 * If a display order is required, it will have to be added
	 * to the data model and the logic at some future time.
	 * 
	 * @param sessionUser
	 * @return An ArrayList of EmailGroupListDTO objects
	 */
	public static EmailGroupListDTO getAllGroups(SessionUserDTO sessionUser)
	{
		DataResult dr = null;
		
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT groupId, ");
		sqlQuery.append("       groupCode, ");
		sqlQuery.append("       displayName, ");
		sqlQuery.append("       description ");
		sqlQuery.append("  FROM em_group_lookup ");
		sqlQuery.append("  WHERE active = 1");

		DataLayerPreparedStatement dlps = V3DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			return new EmailGroupListDTO(dlps.getStatus());
		}
		
		try
		{
			dr = V3DatabaseUtils.select(dlps);
			
			EmailGroupListDTO ret = new EmailGroupListDTO(dr.getStatus());
			switch (ret.getStatus().getStatusCode())
			{
				case RequestStatus.RS_ERROR:
					break;
				case RequestStatus.DBS_NO_DATA:
					ret.setGroupList(null);
					ret.getStatus().setExceptionMessage("No e-mail groups are present.");
					break;
				case RequestStatus.RS_OK:
					ResultSet rs = dr.getResultSet();
					try
					{
						while (rs.next())
						{
							EmailGroupDTO group = new EmailGroupDTO(dr.getStatus());
							moveData(rs, group);
							ret.getGroupList().add(group);
						}
					}
					catch (SQLException ex)
					{
						ret.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
								ex.getErrorCode(), "getAllGroups:  " + ex.getMessage()));
						return ret;
					}
					break;
				default:
					ret.setStatus(unexpectedStatus(ret.getStatus()));
					break;
			}
			
			return ret;
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}


	/**
	 * Get a specific group type
	 * @param group type id
	 * @return An EmaliGroupDTO object
	 */
	public static EmailGroupDTO getGroupInfo(long groupId)
	{
		DataResult dr = null;
		EmailGroupDTO ret = new EmailGroupDTO();

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT groupId, ");
		sqlQuery.append("       groupCode, ");
		sqlQuery.append("       displayName, ");
		sqlQuery.append("       description ");
		sqlQuery.append("  FROM em_group_lookup ");
		sqlQuery.append("  WHERE active = 1 ");
		sqlQuery.append("    AND groupId = ?");
			
		DataLayerPreparedStatement dlps = V3DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			ret.setStatus(dlps.getStatus());
			return ret;
		}

		try
		{
			dlps.getPreparedStatement().setLong(1,groupId);

			dr = V3DatabaseUtils.select(dlps);

			ret.setStatus(dr.getStatus());
			switch (ret.getStatus().getStatusCode())
			{
				case RequestStatus.RS_ERROR:
					break;
				case RequestStatus.DBS_NO_DATA:
					ret.getStatus().setExceptionMessage("Group ID " + groupId + " does not exist.");
					break;
				case RequestStatus.RS_OK:
					ResultSet rs = dr.getResultSet();
					rs.next();
					moveData(rs, ret);
					ret.setId(groupId);
					break;
				default:
					ret.setStatus(unexpectedStatus(ret.getStatus()));
					break;
			}
	
			return ret;
		}
		catch (SQLException ex)
		{
			ret.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
											ex.getErrorCode(), "getGroupInfo:  " + ex.getMessage()));
			return ret;
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}

	//
	// Instance data.
	//
	
	//
	// Constructors.
	//

	//
	// Instance methods.
	//

	public String toString()
	{
		return "EmailGroupDataHelper class:";
	}
}
