/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import com.pdi.data.util.RequestStatus;
import com.pdi.data.v3.dto.DocumentDTO;
import com.pdi.data.v3.dto.SessionUserDTO;
import com.pdi.properties.PropertyLoader;

/**
 * Helpful utilities for V3 data access
 * 
 *
 * @author		Gavin Myers
 */
public class V3WebserviceUtils
{

	//
	// Static data.
	//

	//
	// Static methods.
	//
	
	/**
	 * Request a particular URL from the webservice
	 * @param sessionUser
	 * @param path
	 * @param xml
	 * @return
	 * @throws Exception
	 */
	public static DocumentDTO request(SessionUserDTO sessionUser, String path, String xml)
	{
		return V3WebserviceUtils.authenticate(sessionUser, path);
	}
	
	
	/**
	 * Authenticate a particular user against a url
	 * @param sessionUser
	 * @param path
	 * @return
	 * @throws Exception
	 */
	private static DocumentDTO authenticate(SessionUserDTO sessionUser, String path)
	{
		DocumentDTO ret = new DocumentDTO();
		URL url = null;
		URLConnection uc = null;
		InputStream content = null;
		
		String surl = path;
		if(!path.contains("http"))
		{
			//this is a suburl... without hte header in there, so put the header in it
			surl = PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.url") + surl;
		}

		try {
			url = new URL (surl);
		}  catch (MalformedURLException e) {
			ret.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
					"Malformed URL - " + surl + ".  Msg=" + e.getMessage()));
			return ret;
		}

		String username=sessionUser.getUsername();
		String password = sessionUser.getPassword();
		String code = PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.code");
		String authentication = code+"|"+username+":"+password;
		String encoding = new sun.misc.BASE64Encoder().encode (authentication.getBytes());
			
		// open the connection and get the authorized user info
		try {
			uc = url.openConnection();
		} catch (IOException e) {
			ret.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
				"Unable to open authorization connection.  URL=" + surl + ".  Msg=" + e.getMessage()));
			return ret;
		}
		uc.setRequestProperty  ("Authorization", "Basic " + encoding);
		try {
			 content = (InputStream)uc.getInputStream();
		} catch (Exception e) {
			ret.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
				"Unable to open input stream from authorization connection.  Msg=" + e.getMessage()));
			return ret;
		}

		// Get the content
		BufferedReader br   = new BufferedReader (new InputStreamReader (content));
		StringBuffer result = new StringBuffer();
		String line;
		try {
			while ((line = br.readLine()) != null)
			{
				result.append(line);
			}
		} catch (IOException e) {
			ret.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
					"Unable to read User content.  Msg=" + e.getMessage()));
			return ret;
		}
		
		// suppress the unneeded headers
		String resultString = result.toString();
		resultString = resultString.replace("ns2:", "");
		resultString = resultString.replace("ns3:", "");
		
		// Put the content into a Document object
		try {
			DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
			////Document doc = docBuilder.parse(new InputSource(new StringReader(result.toString().replace("ns2:", ""))));
			Document doc = docBuilder.parse(new InputSource(new StringReader(resultString)));
				
			ret.setDocument(doc);
		} catch (Exception e) {
			ret.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
				"Unable to build authorization Document.  Msg=" + e.getMessage()));
			return ret;
		}

		return ret;
	}

	//
	// Instance data.
	//

	//
	// Constructors.
	//

	//
	// Instance methods.
	//
}
