/**
 * Copyright (c) 2008 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.dto.common;

import java.util.ArrayList;

import com.pdi.data.abyd.dto.common.DNACellDTO;
import com.pdi.data.abyd.dto.common.ModDataDTO;
import com.pdi.data.abyd.dto.common.KeyValuePair;

/**
 * DNAStructureDTO is a wrapper around a number of data items that are needed when the Design
 * Setup app needs to get its structure.
 * 
 * @author		Ken Beukelman
 */
public class DNAStructureDTO
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private ArrayList<KeyValuePair> _competencyArray;	// Display order list of competencies (setup only)
	private ArrayList<CompGroupData> _fullCompArray;	//Display order list of superfactors & comps (Int. Grid only)
	private ArrayList<ModDataDTO> _modArray;			// Display order list of modules
	private ArrayList<Long> _selectedModsArray;		// List of mods with comps selected
	private ArrayList<DNACellDTO> _modCompIntersection;	// List of intersection objects

	//
	// Constructors.
	//
	public DNAStructureDTO()
	{
		// initialize so that there is always an ArrayList (even if empty)
		// Someday, re-factor this so it uses the same structure as the Integration Grid
		_competencyArray = new ArrayList<KeyValuePair>();	// used only in setup
		_fullCompArray = new ArrayList<CompGroupData>();	// used only in int. grid
		_modArray = new ArrayList<ModDataDTO>();
		_selectedModsArray = new ArrayList<Long>();
		_modCompIntersection = new ArrayList<DNACellDTO>();
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	
	/*****************************************************************************************/
	// Currently used only for setup.  At some point, re-factor the code that
	// uses this data to use the full data competency array
	public ArrayList<KeyValuePair> getCompetencyArray()
	{
		return _competencyArray;
	}
	
	public void setCompetencyArray(ArrayList<KeyValuePair> value)
	{
		_competencyArray = value;
	}	
	
	/*****************************************************************************************/
	// Currently used only for integration grid.  At some point, re-factor the code that
	// uses the competencyArray to use this data (ignoring superfactors)
	public ArrayList<CompGroupData> getFullCompArray()
	{
		return _fullCompArray;
	}
	
	public void setFullCompArray(ArrayList<CompGroupData> value)
	{
		_fullCompArray = value;
	}	
			
	/*****************************************************************************************/
	public ArrayList<ModDataDTO> getModArray()
	{
		return _modArray;
	}
	
	public void setModArray(ArrayList<ModDataDTO> value)
	{
		_modArray = value;
	}
			
	/*****************************************************************************************/
	public ArrayList<Long> getSelectedModsArray()
	{
		return _selectedModsArray;
	}
	
	public void setSelectedModsArray(ArrayList<Long> value)
	{
		_selectedModsArray = value;
	}
	
	/*****************************************************************************************/
	public ArrayList<DNACellDTO> getModCompIntersection()
	{
		return _modCompIntersection;
	}
	
	public void setModCompIntersection(ArrayList<DNACellDTO> value)
	{
		_modCompIntersection = value;
	}

	/*
	 * Show me what you've got
	 */
	public String toString()
	{
		String ret = "DNAStructureDTO content (Java)\n";
		ret += "  All Competencies:\n";
		for (KeyValuePair kvp : _competencyArray)
		{
			ret += "    key=" + kvp.getTheKey() + ", val=" + kvp.getTheValue() + "\n";
		}
		ret += "  Competency Groups:\n";
		for (CompGroupData cgd : _fullCompArray)
		{
			ret += cgd.toString();
		}
		ret += "  Modules:\n";
		for (ModDataDTO mod : _modArray)
		{
			ret += mod.toString();
		}
		ret += "  Selected Module IDs:\n";
		for (Long selMod : _selectedModsArray)
		{
			ret += "    " + selMod.toString();
		}
		ret += "  Intersections:\n";
		for (DNACellDTO io : _modCompIntersection)
		{
			ret += "    " + io.toString();
		}
		
		return ret;
	}
}
