/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.dto;

import java.io.Serializable;

/**
 * Base class for all of the "core" data objects
 * 
 * @author Gavin Myers
 * @author Ken Beukelman
 */
public class CoreDataObject implements Serializable
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	/**
	 * 
	 */
	private static final long serialVersionUID = -6539006590497913780L;
	//
	// Instance data.
	//
	private boolean successStatus = true;	// default value says "Suceeded"/"Not in error"
	private String msg = "";				// contains value only if there is an error
	

	//
	// Constructors.
	//
	public CoreDataObject()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//

	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//
			
	/*****************************************************************************************/
	public boolean getSuccessStatus()
	{
		return successStatus;
	}

	public void setSuccessStatus(boolean value)
	{
		successStatus = value;
	}
	
	// Convenience method
	public boolean succeeded()
	{
		return successStatus;
	}
	
	/*****************************************************************************************/
	public String getMsg()
	{
		return msg;
	}
	
	public void setMsg(String value)
	{
		msg = value;
	}
}
