/**
 * Copyright (c) 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.util.data;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import com.pdi.data.dto.ProjectParticipantInstrument;
import com.pdi.data.nhn.util.NhnDatabaseUtils;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.logging.LogWriter;

public class DataUtils
{
	//
	// Static data.
	//
	
//	// Use this until we get the data from the database
//	static private final String[][] PARENT_CHILD_LIST =  new String[][]
//	{
//		(new String [] { "cs2",		"cs"}),
//		(new String [] { "cs3",		"cs2"}),	// Testing,,, not real yet
//		(new String [] { "gpil",	"gpi"})
//	};

	//
	// Static methods.
	//
	
	//
	// Instance data.
	//
	
	//
	// Constructors.
	//

	//
	// Instance methods.
	//
	
	/**
	 * This method is used to remove subordinate instruments from a list of ppi objects.
	 * It is called from each of the delegated objects that need to perform this
	 * functionality.
	 * 
	 * @param  ArrayList<ProjectParticipantInstrument> ary
	 * @return ArrayList<ProjectParticipantInstrument> 
	 */
	public ArrayList<ProjectParticipantInstrument> suppressSubInstUtil(ArrayList<ProjectParticipantInstrument> inpAry)
	{
		if (inpAry.size() < 1)
		{
			// Nothing to do, scram
			return inpAry;
		}
		
		// Pass 1 - set up initial Map (level 1 parent child relationships)
		HashMap<String,String> raw = new HashMap<String,String>();

		// first cut - from a table... replace this with a DB read when the data becomes available
//		for(int idx = 0; idx < PARENT_CHILD_LIST.length; idx++)
//		{
//			String value = PARENT_CHILD_LIST[idx][0];
//			String key = PARENT_CHILD_LIST[idx][1];
//			raw.put(key, value);
//		}
		
		// Second cut... Get it from the DB
		StringBuffer sb = new StringBuffer();		
		sb.append("SELECT c1.abbv AS parent, ");
		sb.append("       c2.abbv AS child ");
		sb.append("  FROM platform.dbo.course_association ca ");
		sb.append("    LEFT JOIN platform.dbo.course c1 ON c1.course = ca.course_id ");
		sb.append("    LEFT JOIN platform.dbo.course c2 ON c2.course = ca.associated_course_id ");
		sb.append("  WHERE ca.master_flag = 1");

		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(sb);
		if (dlps.isInError())
		{
			LogWriter.logSQL(LogWriter.WARNING, this, 
					"Error preparing Instrument fromId(SessionUser session, String instrumentId) query." + dlps.toString(), sb.toString());
		}

		DataResult dr = null;
		ResultSet rs = null;
		try
		{
			dr = NhnDatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			if (rs != null && rs.isBeforeFirst())
			{
				// Got data... set up the raw data map
				while (rs.next())
				{
					raw.put(rs.getString("child"), rs.getString("parent"));
				}
				rs.close();			
			}
		}
		catch(java.sql.SQLException se)
		{
			LogWriter.logSQLWithException(LogWriter.ERROR, this, se.getMessage(), sb.toString(), se);
		}
		finally
		{
			if (dr != null) {dr.close(); dr = null;}			
		}

		// We have the raw data... build the final product
		// Make a map - key = child, value = set of parents
		HashMap<String, HashSet<String>> cpMap= new HashMap<String, HashSet<String>>();
		
		// Loop through the raw data (once)
		for (Iterator<Map.Entry<String, String>> itr=raw.entrySet().iterator(); itr.hasNext(); )
		{
			// get the entry
			Map.Entry<String, String> ent = itr.next();
			
			// Make the parent set and put in the immediate parent
			HashSet<String> pSet = new HashSet<String>();
			String p = ent.getValue();
			
			// See if the parent is a child until it isn't
			while (p != null)
			{
				pSet.add(p);
				p = raw.get(p);
			}

			// create a cpMap entry
			cpMap.put(ent.getKey(), pSet);
		}

		// get a Set of all instruments in the list
		HashSet<String> allInst = new HashSet<String>();
		for(ProjectParticipantInstrument ppi: inpAry)
		{
			allInst.add(ppi.getInstrument().getCode());
		}

		// run through the input array and construct the output array
		ArrayList<ProjectParticipantInstrument> out = new ArrayList<ProjectParticipantInstrument>();
		for(ProjectParticipantInstrument ppi: inpAry)
		{
			String inst = ppi.getInstrument().getCode();
			
			HashSet<String> pars = cpMap.get(inst);
			if (pars != null)
			{
				// We have an instrument that is a child... is one of its parents present?
				boolean found = false;
				for(String p : pars)
				{
					// See if the parent is in the "all" list
					if (allInst.contains(p))
					{
						// It is... no need for more checks
						found = true;
						break;
					}
				}
				if (found)
				{
					// If the parent is there, let's not put out the child
					continue;
				}
			}
			
			// If here, we want to keep this ppi
			out.add(ppi);
		}
		
		return out;
	}
}
