/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.dto.setup;

import java.util.ArrayList;


/**
 * ReportModelStructureDTO contains the structure for a report model.
 * 
 * @author		Ken Beukelman
 */
public class ReportModelStructureDTO
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private long _dnaId;
	private String _participantId;
	private long _langId;
	private ArrayList<ReportModelSuperFactorDTO> _superFactorList = null;

	//
	// Constructors.
	//
	public ReportModelStructureDTO()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public long getDnaId()
	{
		return _dnaId;
	}

	public void setDnaId(long value)
	{
		_dnaId = value;
	}

	/*****************************************************************************************/
	public String getParticipantId()
	{
		return _participantId;
	}

	public void setParticipantId(String value)
	{
		_participantId = value;
	}

	/*****************************************************************************************/
	public long getLangId()
	{
		return _langId;
	}

	public void setLangId(long value)
	{
		_langId = value;
	}

	/*****************************************************************************************/
	public ArrayList<ReportModelSuperFactorDTO> getSuperFactorList()
	{
		if (_superFactorList == null)
			_superFactorList = new ArrayList<ReportModelSuperFactorDTO>();
		return _superFactorList;
	}

	public void setSuperFactorList(ArrayList<ReportModelSuperFactorDTO> value)
	{
		_superFactorList = value;
	}

	
	public String toString()
	{
		String str = "ReportModelStructureDTO:  DNA Id=" + _dnaId + "\n";
		if (_superFactorList == null  || _superFactorList.isEmpty())
		{
			str += "  No superfactor data...\n";
		}
		else
		{
			str += "Found " + _superFactorList.size() + " superfactors:\n";
			for(int i = 0; i < _superFactorList.size(); i++)
			{
				//str += "  SuperFactor " + (i+1) + ":\n";
				str += _superFactorList.get(i).toString();
			}
		}
		
		return str;
	}
}
