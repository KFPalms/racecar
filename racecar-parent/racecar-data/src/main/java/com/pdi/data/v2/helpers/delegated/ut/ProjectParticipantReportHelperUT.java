package com.pdi.data.v2.helpers.delegated.ut;

import java.util.ArrayList;

import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.IProjectParticipantReportHelper;
import com.pdi.properties.PropertyLoader;
import com.pdi.data.dto.ProjectParticipantReport;
import com.pdi.data.dto.SessionUser;
//import com.pdi.data.dto.Client;
import com.pdi.ut.UnitTestUtils;
import junit.framework.TestCase;


public class ProjectParticipantReportHelperUT extends TestCase
{
	private SessionUser _session = null;

	
	public ProjectParticipantReportHelperUT(String name)
	{
		super(name);
	}

	
	private void setUpSession()
	{
		if(_session == null)
		{
			this._session = new SessionUser();
			//_session.getMetadata().put("USERNAME", "cdunn");
			//_session.getMetadata().put("PASSWORD", "cdunn");
			//dev
			_session.getMetadata().put("USERNAME", "mbpanichi");
			_session.getMetadata().put("PASSWORD", "griffin12");
		}
		
		return;
	}


	public void testGetReportsfromProjectIdParticipantId_AbyD()
		throws Exception
	{
			UnitTestUtils.start(this);

			setUpSession();

			// Uses the same participant in both TLT and A by D test
			String participantId = "LFTRXIAT";	//  AbyD 2 Happy 051310 (certify)
			String engagementId = "IYKNYCNU";	// base testing -- A by D (1)

			IProjectParticipantReportHelper eprh = HelperDelegate.getProjectParticipantReportHelper(PropertyLoader.getProperty("com.pdi.data.v2.application","datasource.v2"));
			ArrayList<ProjectParticipantReport> eprList = eprh.fromProjectIdParticipantId(_session, engagementId, participantId);
			System.out.println("A by D test returned " + eprList.size() + " EPR objects...");
			for (ProjectParticipantReport epr : eprList)
			{
				System.out.println("\n" + epr.toString());
			}
			
			UnitTestUtils.stop(this);
	}


	public void testGetReportsfromProjectIdParticipantId_TLT()
		throws Exception
	{
		UnitTestUtils.start(this);

		setUpSession();

		// Uses the same participant in both TLT and A by D test
		String participantId = "LFTRXIAT";	//  AbyD 2 Happy 051310 (certify)
		String engagementId = "LFNHYXVB";	// TLT - NEW BASE 2 Project - English -- TLT (2)

		IProjectParticipantReportHelper eprh = HelperDelegate.getProjectParticipantReportHelper(PropertyLoader.getProperty("com.pdi.data.v2.application","datasource.v2"));
		ArrayList<ProjectParticipantReport> eprList = eprh.fromProjectIdParticipantId(_session, engagementId, participantId);
		System.out.println("TLT test returned " + eprList.size() + " EPR objects...");
		for (ProjectParticipantReport epr : eprList)
		{
			System.out.println("\n" + epr.toString());
		}
			
		UnitTestUtils.stop(this);
	}

/*
	public void testFromProjectParticipantList()
	throws Exception
	{
		UnitTestUtils.start(this);
	
		setUpSession();
	
		ArrayList<ProjectParticipant> inputList = new ArrayList<ProjectParticipant>();
	
		ProjectParticipant ep = new ProjectParticipant();
		ep.getParticipant().setId("LFTRXIAT");
		ep.getProject().setId("LFNHYXVB");	// TLT - NEW BASE 2 Project - English -- TLT (2)
		inputList.add(ep);
		ProjectParticipant ep2 = new ProjectParticipant();
		ep2.getParticipant().setId("HMJPOHSL");
		ep2.getProject().setId("IYKNYCNU");	// base testing -- A by D (1)
		inputList.add(ep2);
		
		IProjectParticipantReportHelper eprh = HelperDelegate.getProjectParticipantReportHelper(PropertyLoader.getProperty("com.pdi.data.v2.application","datasource.v2"));
		ArrayList<ProjectParticipantReport> eprList = eprh.fromProjectParticipantList(_session, inputList);
		System.out.println("ArrayList test returned " + eprList.size() + " EPR objects...");
		for (ProjectParticipantReport epr : eprList)
		{
			System.out.println("\n" + epr.toString());
		}
			
		UnitTestUtils.stop(this);
	}*/

}
