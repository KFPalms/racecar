/**
 * Copyright (c) 2008 Personnel Decisions International, Inc.
 * All rights reserved.
 *
 * $Header: /Platform/Java/sourceCode/com/pdicorp/app/tcm/export/vo/ExportDataVO.java 6     8/25/05 3:23p Kbeukelm $
 */
package com.pdi.data.abyd.dto.setup;

import java.util.ArrayList;

import com.pdi.data.abyd.dto.common.KeyValuePair;
import com.pdi.data.abyd.dto.setup.DNADescription;


/**
 * EntryStateDTO is a wrapper around a number of data items that are needed when the Design
 * Setup app is initially started.
 * 
 * @author		Ken Beukelman
  */
public class EntryStateDTO
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _clientName;						// The name of the client (from V2)
////	private String _clientMgrEmail;				// The email address of the client manager (from V2)
	private String _jobName;						// The V2 platform job (project) name
	private int _state;								// The setup app state
	private ArrayList<KeyValuePair> _modelArray;	// All of the available models; display order array of key/value pairs
	private DNADescription _dnaDesc;				// A DNADescription object containing information about the DNA associated with the V2 job (if present)
	private ArrayList<KeyValuePair> _modelTestArray;	// Test instruments for the models

	//
	// Constructors.
	//
	public EntryStateDTO()
	{
		// Does nothing presently
		_modelArray = new ArrayList<KeyValuePair>();
		_dnaDesc = new DNADescription();
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
	
	/*****************************************************************************************/
	public String getClientName()
	{
		return _clientName;
	}
	
	public void setClientName(String value)
	{
		_clientName = value;
	}	
////
////	/*****************************************************************************************/
////	public String getClientMgrEmail()
////	{
////		return _clientMgrEmail;
////	}
////	
////	public void setClientMgrEmail(String value)
////	{
////		_clientMgrEmail = value;
////	}	

	/*****************************************************************************************/
	public String getJobName()
	{
		return _jobName;
	}
	
	public void setJobName(String value)
	{
		_jobName = value;
	}	
			
	/*****************************************************************************************/
	public int getState()
	{
		return _state;
	}
	
	public void setState(int value)
	{
		_state = value;
	}
			
	/*****************************************************************************************/
	public ArrayList<KeyValuePair> getModelArray()
	{
		return _modelArray;
	}
	
	public void setModelArray(ArrayList<KeyValuePair> value)
	{
		_modelArray = value;
	}
			
	/*****************************************************************************************/
	public DNADescription getDnaDesc()
	{
		return _dnaDesc;
	}
	
	public void setDnaDesc(DNADescription value)
	{
		_dnaDesc = value;
	}
	
	/*****************************************************************************************/
	// Here's a freebie... Add an entry to the model array
	public void addToModelArray(KeyValuePair value)
	{
		_modelArray.add(value);
	}
	
	/*****************************************************************************************/
	public ArrayList<KeyValuePair> getModelTestArray()
	{
	return _modelTestArray;
	}
	
	public void setModelTestArray(ArrayList<KeyValuePair> value)
	{
		_modelTestArray = value;
	}
}