/**
 * Copyright (c) 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v2.helpers.delegated;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdi.data.dto.ProjectParticipantInstrument;
import com.pdi.data.dto.Instrument;
import com.pdi.data.dto.NormGroup;
//import com.pdi.data.dto.NormScore;
import com.pdi.data.dto.Participant;
import com.pdi.data.dto.Project;
import com.pdi.data.dto.ResponseGroup;
import com.pdi.data.dto.Score;
import com.pdi.data.dto.ScoreGroup;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.interfaces.IProjectParticipantInstrumentHelper;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.util.RequestStatus;
import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.logging.LogWriter;

public class ProjectParticipantInstrumentHelper  implements IProjectParticipantInstrumentHelper
{
	private static final Logger log = LoggerFactory.getLogger(ProjectParticipantInstrumentHelper.class);
	// default language id
	private static final int DEFAULT_LANG_ID = 1;
	private static final int DEFUALT_SCALE_TYPE_ID = 1;
	
	private static final int GEN_POP = 1;
	private static final int SPEC_POP = 2;
	
	private String dbUser = "epiHlpr";	
	private String dbUserTestData ="tdUser";
	
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	/*
	 * fromProjectIdParticipantId
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IProjectParticipantInstrumentHelper#fromProjectIdParticipantId(com.pdi.data.dto.SessionUser, int, java.lang.String, java.lang.String)
	 */
	@Override
	public ArrayList<ProjectParticipantInstrument> fromProjectIdParticipantId(
			SessionUser session, int mode, String projectId, String participantId) {
		
		// first get the instrument list
		ArrayList<ProjectParticipantInstrument> epis = new ArrayList<ProjectParticipantInstrument>();
		try {
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT DISTINCT r.instrumentCode ");
			sb.append("  FROM results r ");
			sb.append("  WHERE r.participantId = ? and r.projectId = ?");
				
			DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
			if (dlps.isInError())
			{
				//System.out.println("Error preparing writeAnswerDataToDatabase query." + dlps.toString());
				LogWriter.logSQL(LogWriter.WARNING, this,
						"Error preparing writeAnswerDataToDatabase query",
						sb.toString());
			}

			DataResult dr = null;
			try
			{

				// Fill sql parmeters with values and get the data
				dlps.getPreparedStatement().setString(1, participantId);
				dlps.getPreparedStatement().setString(2, projectId);

				dr = V2DatabaseUtils.select(dlps);
				ResultSet rs = dr.getResultSet();
				
				if (rs != null && rs.isBeforeFirst())
				{
					while(rs.next()) {
						String instrumentId = rs.getString("instrumentCode");
						
						// have instrument, now get the full EPI for this particular instrument/participant/project
						epis.add(new ProjectParticipantInstrumentHelper().fromProjectIdParticipantIdInstrumentId(session, mode, projectId, participantId, instrumentId, null));
					}
				}
			}
			catch(java.sql.SQLException se)
			{
				LogWriter.logSQL(LogWriter.ERROR, this,
						"Fetch error in fromProjectIdParticipantId", sb.toString());
				se.printStackTrace();
			}
			finally
			{
		        if (dr != null) { dr.close(); dr = null; }
			}
		} catch(Exception e) {
			LogWriter.logBasic(LogWriter.ERROR, this,
					"General error in fromProjectIdParticipantId");
			//se.printStackTrace();
		}
		return epis;
	}

	/**
	 * Used in Test Data, for AbyD type projects.
	 * For AbyD, need to get all instruments available, even if there
	 * is no participant data for that instrument.
	 * This method gets the Instrument, and also gets the ScoreGroup
	 * data for the unfinished PPI passed in, and returns the "completed"
	 * PPI with no data, but all scales/norms for that instrument.
	 * 
	 * @param  ProjectParticipantInstrument ppi
	 * @return ProjectParticipantInstrument	  
	 */
	public ProjectParticipantInstrument getPPIWithoutScoredData(SessionUser session, Project proj, Participant part, String instId) {
		
		ProjectParticipantInstrument ppi = new ProjectParticipantInstrument();
		ppi.setProject(proj);
		ppi.setParticipant(part);
		Instrument ins = new InstrumentHelper().fromId(session, instId);
		ppi.setInstrument(ins);

		DataResult dr = null;
		StringBuffer sb = new StringBuffer();
		try {
			sb.append("SELECT DISTINCT csv.instrumentCode, ");
			sb.append("                csv.spssValue, ");
			sb.append("                csv.scaleTextId, ");
			sb.append("                txt1.text as scaleName ");;
			sb.append("  FROM  pdi_construct_spss_value csv ");
			sb.append("    LEFT JOIN pdi_abd_text txt1 ON (txt1.textId = csv.scaleTextId ");
			sb.append("                                AND txt1.languageId = " + DEFAULT_LANG_ID + ") ");
			sb.append("  WHERE csv.instrumentCode = ? ");
			sb.append("  AND csv.scaleType = " + DEFUALT_SCALE_TYPE_ID + "    ");
			DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
			if (dlps.isInError())
			{
				//System.out.println("Error preparing writeAnswerDataToDatabase query." + dlps.toString());
				LogWriter.logSQL(LogWriter.WARNING, this,
						"Error preparing getPPIWithoutScoredData query.  " + dlps.toString(),
						sb.toString());
			}

			ScoreGroup scores = new ScoreGroup();
			
			// get NormGroups for the ScoreGroup
			// NOTE:  The returned list of norm groups may contain both ppt specific and project
			//        defaults for the project.  The data is ordered so that the last set of norms
			//        is the most relevant (th ppt norms.  If the processing loop for processing
			//        the output is changed we will still need to account for this behavior.
			NormGroupHelper ngh = new NormGroupHelper();		
			ArrayList<NormGroup> norms = ngh.fromParticipantInstrumentIdProjectId(session, part.getId(), ins.getCode(), proj.getId());
			for(NormGroup ng : norms){
				if(ng.getPopulation() == GEN_POP){
					scores.setGeneralPopulation(ng);
				}else if (ng.getPopulation() == SPEC_POP){
					scores.setSpecialPopulation(ng);
				}
			}
			
			// Fill sql parmeters with values and get the data
			dlps.getPreparedStatement().setString(1, ins.getCode());
			//System.out.println(" sql: " + sb.toString() + " instrument:  " + ins.getCode());			
			dr = V2DatabaseUtils.select(dlps);
			ResultSet rs = dr.getResultSet();
			
			if (rs != null && rs.isBeforeFirst())
			{
				//int index = 1;
				while (rs.next())
				{
					Score s = new Score();	
					
					s.setCode(rs.getString("spssValue"));
					s.setScaleName(rs.getString("scaleName"));
					
					//System.out.println("scaleName, spssCode :  " + s.getScaleName() + 
					//		", " + s.getCode());	
					
					scores.getScores().put(s.getCode(), s);
				}

				//System.out.println("scores.size.... " + scores.getScores().size());
				ppi.setScoreGroup(scores);
			}
		}
		catch(java.sql.SQLException se)
		{
			//se.printStackTrace();
           	LogWriter.logSQL(LogWriter.ERROR, this,
           			"Fetch error in getPPIWithoutScoredData:" +
           			" projID=" + proj.getId() +
           			", partID=" + part.getId() + ", instID=" + ins.getCode(),
           			sb.toString());
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}

		return ppi;
	}
	
	
	/**
	 * fromProjectIdParticipantIdInstrumentId
	 * Get an EPI object with score data only
	 * based on participantId, instrumentId, projectId
	 * 
	 * per NHN-3163 - adding callingProjectId so that 
	 * we can get norms from the current, calling project
	 * rather than the previous project. Business wants to
	 * see the norms from the current project when looking 
	 * at reports.
	 * 
	 */
	//@Override
	public ProjectParticipantInstrument fromProjectIdParticipantIdInstrumentId(
																	SessionUser session,
																	int mode,
																	String projectId,
																	String participantId,
																	String instrumentId, 
																	String callingProjectId)
	{
		
		ProjectParticipantInstrument ppi = new ProjectParticipantInstrument();
		Instrument ins = new InstrumentHelper().fromId(session, instrumentId);
		ppi.setInstrument(ins);
		
		//We will create dummy objects for Project and Participant, with just the 
		// id's available, as that is all we have available to us at this point.
		//We will, however, build a complete ScoreGroup.
		//Build a scoregroup from the database, populate the epi with the scores, return the epi
		
		Participant part = new Participant();
		part.setId(participantId);
		ppi.setParticipant(part);
		
		Project proj = new Project();
		if(projectId != null) {
			proj.setId(projectId);
		} else {
			proj.setId("0");
		}
		ppi.setProject(proj);
		
		StringBuffer sb = new StringBuffer();
		try {
			// When we added scale type we made minimal mods to the query (just added
			// the csv.scaleType to the selected columns).  Did this because the scale
			// type is not exclusionary in the first query.  Rather than have to process
			// the results differently from each query, we pull back the type and do a
			// compare on it later in the procees.
			if(projectId != null) {
				// Note that this query brings back every score in gpi (38) if present in results
				sb.append("SELECT DISTINCT r.scoreId, ");
				sb.append("                r.spssValue, ");
				sb.append("                r.rawScore, ");
				sb.append("                txt1.text as scaleName, ");
				sb.append("                csv.scaleType, ");
				sb.append("                r.lastDate ");
				sb.append("  FROM results r ");
				sb.append("    LEFT JOIN pdi_construct_spss_value csv ON csv.spssValue = r.spssValue ");
				sb.append("    LEFT JOIN pdi_abd_text txt1 ON (txt1.textId = csv.scaleTextId ");
				sb.append("                                AND txt1.languageId = " + DEFAULT_LANG_ID + ") ");
				sb.append("  WHERE r.participantId = ? ");
				sb.append("    AND r.projectId = ? ");
				sb.append("    AND r.instrumentCode = ?");
			} else {
				sb.append("SELECT DISTINCT ");
				sb.append("         r.scoreId, ");
				sb.append("         r.spssValue, ");
				sb.append("         r.rawScore, ");
				sb.append("         txt2.text as scaleName, ");
				sb.append("         csv.scaleType, ");
				sb.append("         r.lastDate ");
				sb.append("  FROM results r ");
				sb.append("    INNER JOIN pdi_instrument_code ic ON ic.instrumentCode = r.instrumentCode ");
				sb.append("    INNER JOIN pdi_construct_spss_value csv ON (csv.moduleId = ic.v2ModId ");
				sb.append("                                            AND csv.spssValue = r.spssValue) ");
				sb.append("    LEFT JOIN pdi_abd_text txt2 ON (txt2.textId = csv.scaleTextId ");
				sb.append("                              AND txt2.languageId = " + DEFAULT_LANG_ID + ") ");
				sb.append("  WHERE r.participantid = ? ");
				sb.append("    AND r.instrumentCode = ? ");
				sb.append("  ORDER BY r.spssValue");	// Probably not needed
			  }
			
			//System.out.println("fromProjectIdParticipantIdInstrumentId sql: " + sb.toString());
			
			DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
			if (dlps.isInError())
			{
				//System.out.println("Error preparing writeAnswerDataToDatabase query." + dlps.toString());
				LogWriter.logSQL(LogWriter.WARNING, this,
						"Error preparing writeAnswerDataToDatabase query.  " + dlps.toString(),
						sb.toString());
			}

			DataResult dr = null;
			ScoreGroup scores = new ScoreGroup();
						
			// get NormGroups for the ScoreGroup.  last ones in list are the ones used.
			// NOTE:  The returned list of norm groups may contain both ppt specific and project
			//        defaults for the project.  The data is ordered so that the last set of norms
			//        is the most relevant (th ppt norms.  If the processing loop for processing
			//        the output is changed we will still need to account for this behavior.

			NormGroupHelper ngh = new NormGroupHelper();
			ArrayList<NormGroup> norms;
			
			if (ppi.getInstrument().getCode().equals("rv2"))
			{
				// This is a cheat.  There are only 2 norms for rv2, a spec pop and a gen pop
				norms = ngh.fromInstrumentId(session, "rv2");
				log.debug("Loaded {} Norms for rv2", norms.size());
			}
			else
			{
				// Check to see if the callingProjectId is NULL -
				// if so, use the regular project ID
				// IF the calling projectID is NOT null,  use 
				// the callingProject ID instead.
				if(callingProjectId == null){
					norms = ngh.fromParticipantInstrumentIdProjectId(session, participantId, instrumentId, projectId);
				}else{
					norms = ngh.fromParticipantInstrumentIdProjectId(session, participantId, instrumentId, callingProjectId);
				}
			}
					
			
			for(NormGroup ng : norms){
				if(ng.getPopulation() == GEN_POP){
					scores.setGeneralPopulation(ng);
					log.debug("{}  GEN POP = {}", ng.getInstrumentCode(), ng.getName());
				}else if (ng.getPopulation() == SPEC_POP){
					scores.setSpecialPopulation(ng);
					log.debug("{}  SPEC POP = {}",ng.getInstrumentCode(), ng.getName());
				} else {
					log.debug("Failed to identify population: {}", ng.getPopulation());
				}
			}
			
			try
			{
				// Fill sql parmeters with values and get the data
				// this is getting score data, so use the regular projectId
				dlps.getPreparedStatement().setString(1, participantId);
				if(projectId != null) {
					dlps.getPreparedStatement().setString(2, projectId);
					dlps.getPreparedStatement().setString(3, instrumentId);
				} else {
					dlps.getPreparedStatement().setString(2, instrumentId);
				}

				dr = V2DatabaseUtils.select(dlps);
				ResultSet rs = dr.getResultSet();
				
				if (rs != null && rs.isBeforeFirst())
				{
					int index = 1;
					while (rs.next())
					{
						int typ = rs.getInt("scaleType");
						if (typ == Score.INTERNAL)
						{
							// Score is not editable... skip it
							continue;
						}
						Score s = new Score();
						s.setId(rs.getString("scoreId"));							
						s.setCode(rs.getString("spssValue"));
						s.setRawScore(rs.getString("rawScore"));
						s.setScaleName(rs.getString("scaleName"));
						scores.getScores().put(s.getCode(), s);
						if(index == 1){
							//ppi.setInstrumentTakenDate(rs.getString("dateCreatedStamp"));
							ppi.setInstrumentTakenDate(rs.getString("lastdate"));
														index++;
						}
						
						//System.out.println("id, rawScore, spssCode :  " + s.getId() + 
						//		", " + s.getRawScore() + ", " + s.getCode());							
					}
					
					ppi.setScoreGroup(scores);
				}
			}
			catch(java.sql.SQLException se)
			{
				//se.printStackTrace();
            	LogWriter.logSQL(LogWriter.ERROR, this,
            			"Error in fetch - fromProjectIdParticipantIdInstrumentId:" +
            			"  Mode=" + mode + ", projID=" + projectId +
            			", partID=" + participantId + ", instID=" + instrumentId,
            			sb.toString());
			}
			finally
			{
				if (dr != null) { dr.close(); dr = null; }
			}
		} catch(Exception e) {
			LogWriter.logSQL(LogWriter.ERROR, this,
					"Error in setup - fromProjectIdParticipantIdInstrumentId: " +
					"Mode=" + mode + ", projId=" + projectId +
					", PartID=" + participantId + ", instID=" + instrumentId,
					sb.toString());
			//e.printStackTrace();
		}
		
		
		return ppi;
		
	}
	
	
	/**
	 * fromProjectIdParticipantIdInstrumentIdIncludeProjectIdFromCallingProject
	 * Get an EPI object with score data only
	 * based on participantId, instrumentId, projectId
	 * but get norms from CURRENT, CALLING PROJECT
	 * and not from the project that the instrument may have
	 * been initially taken in.
	 * 
	 * NHN 1738: new use case, with participant in two
	 * different AbyD projects, so different sets of norms
	 * 
	 * * usecase: I the most recent GPI for this participant, but he
	 * is in two different projects, with different norms.  I want the
	 * norms for the current project.
	 * @param SessionUser session
	 * @param int mode
	 * @param String projectId
	 * @param String participantId
	 * @param String instrumentId
	 * @param String callingProjectId
	 * @return ArrayList<ProjectParticipantInstrument> ppiList
	 * 
	 */
	//@Override
	public ProjectParticipantInstrument fromProjectIdParticipantIdInstrumentIdIncludeProjectIdFromCallingProject(
																	SessionUser session,
																	int mode,
																	String projectId,
																	String participantId,
																	String instrumentId,
																	String callingProjectId)
	{
		
		ProjectParticipantInstrument ppi = new ProjectParticipantInstrument();
		Instrument ins = new InstrumentHelper().fromId(session, instrumentId);
		ppi.setInstrument(ins);
		
		//We will create dummy objects for Project and Participant, with just the 
		// id's available, as that is all we have available to us at this point.
		//We will, however, build a complete ScoreGroup.
		//Build a scoregroup from the database, populate the epi with the scores, return the epi
		
		Participant part = new Participant();
		part.setId(participantId);
		ppi.setParticipant(part);
		
		Project proj = new Project();
		if(projectId != null) {
			proj.setId(projectId);
		} else {
			proj.setId("0");
		}
		ppi.setProject(proj);
		
		StringBuffer sb = new StringBuffer();
		try {
			// When we added scale type we made minimal mods to the query (just added
			// the csv.scaleType to the selected columns).  Did this because the scale
			// type is not exclusionary in the first query.  Rather than have to process
			// the reults differently from each query, we pull back the type and do a
			// compare on it later in the procees.
			if(projectId != null) {
				// Note that this query brings back every score in gpi (38) if present in results
				sb.append("SELECT DISTINCT r.scoreId, ");
				sb.append("                r.spssValue, ");
				sb.append("                r.rawScore, ");
				sb.append("                txt1.text as scaleName, ");
				sb.append("                csv.scaleType, ");
				sb.append("                r.lastDate ");
				sb.append("  FROM results r ");
				sb.append("    LEFT JOIN pdi_construct_spss_value csv ON csv.spssValue = r.spssValue ");
				sb.append("    LEFT JOIN pdi_abd_text txt1 ON (txt1.textId = csv.scaleTextId ");
				sb.append("                                AND txt1.languageId = " + DEFAULT_LANG_ID + ") ");
				sb.append("  WHERE r.participantId = ? ");
				sb.append("    AND r.projectId = ? ");
				sb.append("    AND r.instrumentCode = ?");
			} else {
				// Note that this query brings back only those scores defined
				// for the GPI Short (27), even if more results are present
				sb.append("SELECT DISTINCT ");
				sb.append("         r.scoreId, ");
				sb.append("         r.spssValue, ");
				sb.append("         r.rawScore, ");
				sb.append("         txt2.text as scaleName, ");
				sb.append("         csv.scaleType, ");
				sb.append("         r.lastDate ");
				sb.append("  FROM results r ");
				sb.append("    INNER JOIN pdi_instrument_code ic ON ic.instrumentCode = r.instrumentCode ");
				sb.append("    INNER JOIN pdi_construct_spss_value csv ON (csv.moduleId = ic.v2ModId ");
				sb.append("                                            AND csv.spssValue = r.spssValue) ");
				sb.append("    LEFT JOIN pdi_abd_text txt2 ON (txt2.textId = csv.scaleTextId ");
				sb.append("                              AND txt2.languageId = " + DEFAULT_LANG_ID + ") ");
				sb.append("  WHERE r.participantid = ? ");
				sb.append("    AND r.instrumentCode = ? ");
				//sb.append("  ORDER BY csv.spssValue");	// Probably not needed
				sb.append("  ORDER BY r.spssValue");	// Probably not needed
			  }
			
			//System.out.println("fromProjectIdParticipantIdInstrumentIdIncludeProjectIdFromCallingProject sql: " + sb.toString());
			
			DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
			if (dlps.isInError())
			{
				//System.out.println("Error preparing writeAnswerDataToDatabase query." + dlps.toString());
				LogWriter.logSQL(LogWriter.WARNING, this,
						"Error preparing writeAnswerDataToDatabase query.  " + dlps.toString(),
						sb.toString());
			}

			DataResult dr = null;
			ScoreGroup scores = new ScoreGroup();
			
			// get NormGroups for the ScoreGroup.  Use the calling project Id to get the right norms.
			// NOTE:  The returned list of norm groups may contain both ppt specific and project
			//        defaults for the project.  The data is ordered so that the last set of norms
			//        is the most relevant (th ppt norms.  If the processing loop for processing
			//        the output is changed we will still need to account for this behavior.
			NormGroupHelper ngh = new NormGroupHelper();		
			ArrayList<NormGroup> norms = ngh.fromParticipantInstrumentIdProjectId(session, participantId, instrumentId, callingProjectId);
			for(NormGroup ng : norms){
				if(ng.getPopulation() == GEN_POP){
					scores.setGeneralPopulation(ng);
				}else if (ng.getPopulation() == SPEC_POP){
					scores.setSpecialPopulation(ng);
				}
			}
			
			try
			{
				// Fill sql parmeters with values and get the data
				dlps.getPreparedStatement().setString(1, participantId);
				if(projectId != null) {
					dlps.getPreparedStatement().setString(2, projectId);
					dlps.getPreparedStatement().setString(3, instrumentId);
				} else {
					dlps.getPreparedStatement().setString(2, instrumentId);
				}

				dr = V2DatabaseUtils.select(dlps);
				ResultSet rs = dr.getResultSet();
				
				if (rs != null && rs.isBeforeFirst())
				{
					int index = 1;
					while (rs.next())
					{
						int typ = rs.getInt("scaleType");
						if (typ == Score.INTERNAL)
						{
							// Score is not editable... skip it
							continue;
						}
						Score s = new Score();
						s.setId(rs.getString("scoreId"));							
						s.setCode(rs.getString("spssValue"));
						s.setRawScore(rs.getString("rawScore"));
						s.setScaleName(rs.getString("scaleName"));
						scores.getScores().put(s.getCode(), s);
						if(index == 1){
							//ppi.setInstrumentTakenDate(rs.getString("dateCreatedStamp"));
							ppi.setInstrumentTakenDate(rs.getString("lastdate"));
														index++;
						}
						
						//System.out.println("id, rawScore, spssCode :  " + s.getId() + 
						//		", " + s.getRawScore() + ", " + s.getCode());							
					}
					
					ppi.setScoreGroup(scores);
				}
			}
			catch(java.sql.SQLException se)
			{
				//se.printStackTrace();
            	LogWriter.logSQL(LogWriter.ERROR, this,
            			"SQL error in fromProjectIdParticipantIdInstrumentId:" +
            			"  Mode=" + mode + ", projID=" + projectId +
            			", partID=" + participantId + ", instID=" + instrumentId,
            			sb.toString());
			}
			finally
			{
				if (dr != null) { dr.close(); dr = null; }
			}
		} catch(Exception e) {
			LogWriter.logSQL(LogWriter.ERROR, this,
					"Error in fromProjectIdParticipantIdInstrumentId: " +
					"Mode=" + mode + ", projId=" + projectId +
					", PartID=" + participantId + ", instID=" + instrumentId,
					sb.toString());
			//e.printStackTrace();
		}

		return ppi;
	}

	/**
	 *  Get all Instruments for the participant, 
	 *  including duplicate instruments.
	 *  This populates score data for each instrument.
	 * 
	 * Following comment no longer viable (ProjectParticipantHelper has been dropped)
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IProjectParticipantInstrumentHelper#fromParticipantId(com.pdi.data.dto.SessionUser, int, java.lang.String)
	 *
	 *@param SessionUser session
	 *@param int mode
	 *@param String particpantId
	 *@param String callingProjectId -- this is a pass through value
	 *@return ArrayList<ProjectParticipantInstrument>
	 */
	//@Override
	public ArrayList<ProjectParticipantInstrument> fromParticipantId(SessionUser session,
																	 int mode,
																	 String participantId, 
																	 String callingProjectId)
	{
		ArrayList<ProjectParticipantInstrument> epiArray = new ArrayList<ProjectParticipantInstrument>();
		
		StringBuffer sb = new StringBuffer();
		try
		{
			// We need the date but that caused dups in project/inst entries.  Added the
			// order so that the last date is used and the rest of the dups are ignored.
			String joinType = null;
			if (mode == IProjectParticipantInstrumentHelper.MODE_FULL_COMPLETED)
			{
				// We want only the data for completed instruments
				joinType = "INNER JOIN";
			}
			else
			{
				// we want to get all instruments whether there is data or not
				joinType = "LEFT JOIN";
			}
			// NHN-3162 - It appears that in the evolution of the query that the last date was
			//            no longer being fetched.  Added the ASC/DESC qualifiers
			sb.append("SELECT DISTINCT ic.instrumentCode, ");
			sb.append("                r.projectId, ");
			sb.append("                CONVERT(date, r.lastDate) as lastDate ");
			sb.append("  FROM pdi_instrument_code ic ");
			sb.append("    LEFT JOIN pdi_construct_spss_value csv ON csv.instrumentCode = ic.instrumentCode ");
			sb.append("    " + joinType + " results r ON (r.instrumentCode = ic.instrumentCode ");
			sb.append("                               AND r.spssValue = csv.spssValue ");
			sb.append("                               AND r.participantid = ?) ");
			sb.append("  ORDER BY ic.instrumentCode ASC, lastDate DESC");
			
			log.debug("SQL: participantid = {} {}", participantId, sb.toString());
			
			
			//System.out.println("fromLastParrticipantId.. " + sb.toString());			
			DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
			if (dlps.isInError())
			{
				//System.out.println("Error preparing writeAnswerDataToDatabase query." + dlps.toString());
				LogWriter.logBasic(LogWriter.WARNING, this, "Error preparing fromParticipantId query." + dlps.toString());
			}

			DataResult dr = null;
			try
			{
				// Fill sql parmeters with values and get the data
				dlps.getPreparedStatement().setString(1, participantId);
				dr = V2DatabaseUtils.select(dlps);
				ResultSet rs = dr.getResultSet();
				
				if (rs != null && rs.isBeforeFirst())
				{
					String curInst = "";
					String curProj = "";
					while (rs.next())
					{
						String dbProj = rs.getString("projectId");
						String dbInst = rs.getString("instrumentCode");
						if (curInst.equals(dbInst) && curProj.equals(dbProj))
						{
							continue;
						}
						else
						{
							curInst = dbInst;
							curProj = dbProj;
						}
						// create new EPI to fill
						ProjectParticipantInstrument epi = new ProjectParticipantInstrument();
						
						//System.out.println(" projectID = " + dbProj + "    instrument = " + dbInst);
						
						// this gets us the ScoreGroup, but doesn't
						// populate the rest of the epi
						// the reason it doesn't populate the rest of the epi 
						// is that in most cases, we will be getting the data for
						// participant/project from the nhn side.
						////epi = this.fromProjectIdParticipantIdInstrumentId
						////			(session, mode, rs.getString("jobId"), participantId, rs.getString("instrumentCode"));
						epi = this.fromProjectIdParticipantIdInstrumentId(
								session, mode, dbProj, participantId, dbInst, callingProjectId);
						
						// should have an Instrument, and ScoreGroup...
						// add the date instrument taken
						//epi.setInstrumentTakenDate(rs.getString("dateCreatedStamp"));
						epi.setInstrumentTakenDate(rs.getString("lastDate"));
						
						//add the EPI to the epiArray
						epiArray.add(epi);
					}
				}
			}
			catch(java.sql.SQLException se)
			{
				//se.printStackTrace();
				LogWriter.logSQL(LogWriter.ERROR, this,
						"Error in data fetch in fromParticipantId."  +
						"  Mode=" + mode + ", partID=" + participantId,
						sb.toString());
			}
			finally
			{
				if (dr != null) { dr.close(); dr = null; }
			}
		} catch(Exception e) {
			//e.printStackTrace();
			LogWriter.logBasic(LogWriter.ERROR, this,
					"General error in fromParticipantId."  +
					"  Mode=" + mode + ", partID=" + participantId);
		}

		return epiArray;
	}


	/**
	 *  Get all Instruments for the participant, 
	 *  including duplicate instruments.
	 *  This populates score data for each instrument.
	 *  
	 *  The callingProjectId is just being passed along until 
	 *  it can be used to get the appropriate norms for the
	 *  instrument/project.
	 * 
	 * NHN 1738: new use case, with participant in two
	 * different AbyD projects, so different sets of norms
	 * 
	 * * usecase: I the most recent GPI for this participant, but he
	 * is in two different projects, with different norms.  I want the
	 * norms for the current project.
 
	 */
	//@Override
	public ArrayList<ProjectParticipantInstrument> fromParticipantIdIncludeProjectIdFromCallingProject(SessionUser session,
																	 int mode,
																	 String participantId,
																	 String callingProjectId)
	{
		ArrayList<ProjectParticipantInstrument> epiArray = new ArrayList<ProjectParticipantInstrument>();
		
		StringBuffer sb = new StringBuffer();
		try
		{
			// We need the date but that caused dups in project/inst entries.  Added the
			// order so that the last date is used and the rest of the dups are ignored.
			String joinType = null;
			if (mode == IProjectParticipantInstrumentHelper.MODE_FULL_COMPLETED)
			{
				// We want only the data for completed instruments
				joinType = "INNER JOIN";
			}
			else
			{
				// we want to get all instruments whether there is data or not
				joinType = "LEFT JOIN";
			}
			sb.append("SELECT DISTINCT ic.instrumentCode, ");
			sb.append("                r.projectId, ");
			sb.append("                CONVERT(date, r.lastDate) as lastDate ");
			sb.append("  FROM pdi_instrument_code ic ");
			sb.append("    LEFT JOIN pdi_construct_spss_value csv ON csv.instrumentCode = ic.instrumentCode ");
			sb.append("    " + joinType + " results r ON (r.instrumentCode = ic.instrumentCode ");
			sb.append("                               AND r.spssValue = csv.spssValue ");
			sb.append("                               AND r.participantid = ?) ");
			sb.append("  ORDER BY ic.instrumentCode ASC, lastDate DESC");
			
			// Modified the ordering in the above query to match that in fromParticipantId().  That way the logic in the calling routines can remain the same
			
			//System.out.println("fromParticipantIdIncludeProjectIdFromCallingProject.. " + sb.toString());			
			DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
			if (dlps.isInError())
			{
				//System.out.println("Error preparing writeAnswerDataToDatabase query." + dlps.toString());
				LogWriter.logBasic(LogWriter.WARNING, this, "Error preparing fromParticipantId query." + dlps.toString());
			}

			DataResult dr = null;
			try
			{
				// Fill sql parmeters with values and get the data
				dlps.getPreparedStatement().setString(1, participantId);
				dr = V2DatabaseUtils.select(dlps);
				ResultSet rs = dr.getResultSet();
				
				if (rs != null && rs.isBeforeFirst())
				{
					String curInst = "";
					String curProj = "";
					while (rs.next())
					{
						String dbProj = rs.getString("projectId");
						String dbInst = rs.getString("instrumentCode");
						if (curInst.equals(dbInst) && curProj.equals(dbProj))
						{
							continue;
						}
						else
						{
							curInst = dbInst;
							curProj = dbProj;
						}
						// create new EPI to fill
						ProjectParticipantInstrument epi = new ProjectParticipantInstrument();
						
						// this gets us the ScoreGroup, but doesn't
						// populate the rest of the epi
						// the reason it doesn't populate the rest of the epi 
						// is that in most cases, we will be getting the data for
						// participant/project from the nhn side.
						////epi = this.fromProjectIdParticipantIdInstrumentId
						////			(session, mode, rs.getString("jobId"), participantId, rs.getString("instrumentCode"));
						//epi = this.fromProjectIdParticipantIdInstrumentId(
						//		session, mode, dbProj, participantId, dbInst);
						// we must eventually be able to get the norm data from the calling project
						epi = this.fromProjectIdParticipantIdInstrumentIdIncludeProjectIdFromCallingProject(
								session, mode, dbProj, participantId, dbInst, callingProjectId);

						
						
						// should have an Instrument, and ScoreGroup...
						// add the date instrument taken
						//epi.setInstrumentTakenDate(rs.getString("dateCreatedStamp"));
						epi.setInstrumentTakenDate(rs.getString("lastDate"));
						
						//add the EPI to the epiArray
						epiArray.add(epi);
					}
				}
			}
			catch(java.sql.SQLException se)
			{
				//se.printStackTrace();
				LogWriter.logSQL(LogWriter.ERROR, this,
						"Error in data fetch in fromParticipantId."  +
						"  Mode=" + mode + ", partID=" + participantId,
						sb.toString());
			}
			finally
			{
				if (dr != null) { dr.close(); dr = null; }
			}
		} catch(Exception e) {
			//e.printStackTrace();
			LogWriter.logBasic(LogWriter.ERROR, this,
					"General error in fromParticipantId."  +
					"  Mode=" + mode + ", partID=" + participantId);
		}

		return epiArray;
	}

	/*
	 * fromParticipantIdInstrumentId
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IProjectParticipantInstrumentHelper#fromParticipantIdInstrumentId(com.pdi.data.dto.SessionUser, java.lang.String, java.lang.String)
	 */
	//@Override
	public ArrayList<ProjectParticipantInstrument> fromParticipantIdInstrumentId(
			SessionUser session, String participantId, String instrumentId) {
		// TODO Auto-generated method stub
		return null;
	}

	
	/*
	 * fromLastParticipantIdInstrumentId
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IProjectParticipantInstrumentHelper#fromLastParticipantIdInstrumentId(com.pdi.data.dto.SessionUser, java.lang.String, java.lang.String)
	 */
	//@Override
	public ProjectParticipantInstrument fromLastParticipantIdInstrumentId(
			SessionUser session, String participantId, String instrumentId) {
		// TODO Auto-generated method stub
		return null;
	}

	
	/**
	 * addUpdate(session, mode, epi)
	 * 
	 * Compare the incoming data with the existing data 
	 * Update items that have changed
	 * Updates Norm choices and scores from the manual entry/test data
	 * application. 
	 * 
	 * 	 This is a note related to how TestData updates
	 * scores.  At this time (3-07-2011) if ANY change is made
	 * to an instrument in TestData ( manual entry ) then ALL
	 * scale scores are updated.  This should keep all the "date taken"
	 * dates the same for a given instrument.  
	 * 		In that case, if we get the data sorted on instrument, 
	 * 		project (and probably scale), then we will put all of 
	 * 		the scales for a particular instance together and not 
	 * 		have to worry about different scale scores from different 
	 * 		instances/projects being jumbled together
	 * We need to make sure (eventually) that that is the way TestData
	 * 		 does updates/submits and when moved to SQL Server that 
	 * we will need do it in a transaction
	 * 
	 * 01-22-2013  -- adding boolean parameter, so that business can determine if the data is being	 
	 * added from the usual scoring routine, or from Test Data (Manual Entry) for billing purposes.
	 * 	 * 
	 * 	 * 
	 * 	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IProjectParticipantInstrumentHelper#addUpdate(com.pdi.data.dto.SessionUser, int, com.pdi.data.dto.ProjectParticipantInstrument)
	 */
	//@Override
	public void addUpdate(SessionUser session, int mode,
			ProjectParticipantInstrument epi,
			boolean fromTestData)
	{
		//TODO - compare both the objects, update only what has changed
//		ProjectParticipantInstrument epi2 = this.fromProjectIdParticipantIdInstrumentId
//															(session, 
//															mode, 
//															epi.getProject().getId(), 
//															epi.getParticipant().getId(), 
//															epi.getInstrument().getCode());
		
		String participantId = epi.getParticipant().getId();
		String instrumentCode = epi.getInstrument().getCode();
		String projectId = epi.getProject().getId();
				
		ScoreGroup sg = epi.getScoreGroup();
		//ScoreGroup sg2 = epi2.getScoreGroup();
		//System.out.println(" part, int, proj " + participantId + " " + instrumentCode + " " + projectId);
		//System.out.println("sg.getGeneralPopulation().getId() " + sg.getGeneralPopulation().getId());
		//System.out.println("sg2.getGeneralPopulation().getId()" + sg2.getGeneralPopulation().getId());

		//If norms are set or changed, update the linkage in the pdi_participant_instrument_norm_map table
		if(sg.getGeneralPopulation() != null && sg.getGeneralPopulation().getId() != 0) {
			// do an update
			NormGroupHelper ngh = new NormGroupHelper();
			ngh.update(session, sg.getGeneralPopulation(), projectId, participantId, instrumentCode);
		}
		if(sg.getSpecialPopulation() != null && sg.getSpecialPopulation().getId() != 0) {
			// do an update
			NormGroupHelper ngh = new NormGroupHelper();
			ngh.update(session, sg.getSpecialPopulation(), projectId, participantId, instrumentCode);
		}

		// For Scores, we're just going to do the update or insert no 
		// matter if there are changes or not.
		// Seems like over-kill to compare every score
		
//		for(String k : sg.getScores().keySet()) {
//			System.out.println("The SPSS is " + k);
//			Score s = sg.getScores().get(k);
//			System.out.println("The value is " + s.getRawScore());
//		}
		
		////////////////////////////////////////////
		// Set up the initial checking on it query.
		///////////////////////////////////////////
		StringBuffer sqlQuery1 = new StringBuffer();
		//sqlQuery1.append("SELECT rr.UniqueIdStamp ");
		sqlQuery1.append("SELECT rr.scoreId ");
		sqlQuery1.append("  FROM results rr ");
		//sqlQuery1.append("  WHERE rr.JobId = ? ");
		sqlQuery1.append("  WHERE rr.projectId = ? ");
		//sqlQuery1.append("    AND rr.CandidateId = ? ");
		sqlQuery1.append("    AND rr.participantId = ? ");
		sqlQuery1.append("    AND rr.instrumentCode = ? ");
		sqlQuery1.append("    AND rr.spssValue = ?");
	
		//System.out.println("sqlQuery1 " + sqlQuery1);	
		
		DataResult dr1 = null;
		DataResult drInsert = null;
		DataResult drUpdate = null;

		ResultSet rs1 = null;
		
		////////////////////////////////////////////
		// Set up the update query.
		///////////////////////////////////////////		
		// Note that we really only need score ID in the WHERE clause
		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE results ");  					
		//sb.append(" set  DateModifiedStamp = ?, ");  		// 1
		//sb.append(" ModifiedByStamp = ?,  ");  			// 2
		//sb.append(" CScore = ?  ");  						// 3						
		//sb.append(" WHERE candidateId = ? ");  		    // 4
		//sb.append(" AND jobId = ? ");  				    // 5
		//sb.append(" AND uniqueIdStamp = ?  ");		    // 7
		sb.append(" set  lastDate = ?, ");  			// 1
		sb.append(" lastUser = ?,  ");  				// 2
		sb.append(" rawScore = ?  ");  					// 3						
		sb.append(" WHERE scoreId	 = ? ");			// 4
	
		////////////////////////////////////////////////////////////////////////
		// spin through the scores, see if there's a row out there first... 
		////////////////////////////////////////////////////////////////////////	
		for(String k : sg.getScores().keySet())
		{
			try
			{
				Score s = sg.getScores().get(k);
			
				if(s.getRawScore() == null ||
				   s.getRawScore().trim().length() < 1)
				{
					continue;
				}
				DataLayerPreparedStatement dlps1 = V2DatabaseUtils.prepareStatement(sqlQuery1);
				if (dlps1.isInError())
				{
					//System.out.println("Error preparing writeResultsDataToDatabase query." + dlps1.toString());
					LogWriter.logSQL(LogWriter.ERROR, this,
							"Error preparing writeResultsDataToDatabase query." + dlps1.toString(),
							sqlQuery1.toString());
					return;
				}
				
				// fill in the ?'s for the initial query... 
				dlps1.getPreparedStatement().setString(1, projectId);
				dlps1.getPreparedStatement().setString(2, participantId);
				dlps1.getPreparedStatement().setString(3, instrumentCode);
				dlps1.getPreparedStatement().setString(4, k);

				// now do the initial select, to see if the row is there.... 
				dr1 = V2DatabaseUtils.select(dlps1);	
				
				if (dr1.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
				{	
					//System.out.println("INSERT INTO RESULTS.....");
					//do an insert.... 
					StringBuffer sbInsert = new StringBuffer();
					sbInsert.append("INSERT INTO results ");
					sbInsert.append("(lastDate, lastUser, participantId,");
					sbInsert.append(" projectId, instrumentCode, spssValue, rawScore) ");
					sbInsert.append(" VALUES ( ?, ?, ?, ?, ?, ?, ?) ");

					DataLayerPreparedStatement dlpsInsert = V2DatabaseUtils.prepareStatement(sbInsert);
					if (dlpsInsert.isInError())
					{
						//System.out.println("Error preparing doInsert query." + dlpsInsert.toString());
						LogWriter.logBasic(LogWriter.ERROR, this,
								"Error preparing doInsert query." + dlpsInsert.toString());
						return;
					}

					try // for drInsert
					{
							//String date = com.pdi.data.v2.util.Utils.getHRADateString();
							String date = sdf.format(new Date());
							//String uid = com.pdi.data.v2.util.Utils.getNumericLPKey();
							
							// Fill sql parameters with values and get the data
							dlpsInsert.getPreparedStatement().setString(1, date);
							// set appropriate user for billing purposes
							if(fromTestData){
								dlpsInsert.getPreparedStatement().setString(2, dbUserTestData);
							}else{
								dlpsInsert.getPreparedStatement().setString(2, dbUser);
							}
							dlpsInsert.getPreparedStatement().setString(3, participantId);
							dlpsInsert.getPreparedStatement().setString(4, projectId);
							dlpsInsert.getPreparedStatement().setString(5, instrumentCode);
							dlpsInsert.getPreparedStatement().setString(6, k);
							//dlpsInsert.getPreparedStatement().setString(7, s.getRawScore());
							dlpsInsert.getPreparedStatement().setDouble(7, s.scoreAsNumber());
							//System.out.println(k + " vs " + s.getRawScore());

							try {
								drInsert = V2DatabaseUtils.insert(dlpsInsert);
							} catch (Exception e) {
								LogWriter.logSQL(LogWriter.ERROR, this,
										"Insert error in result:" +
										"  PartId = " + participantId + 
										", projId = " + projectId + 
										", spssId = " + k,
										sbInsert.toString());
							}
					}
					catch(java.sql.SQLException se)
					{
						LogWriter.logSQL(LogWriter.ERROR, this, 
								"Insert setup error:" +
								"  Message = " + se.getMessage() +
								", PartId = " + participantId + 
								", projId = " + projectId + ",  "  +
								", spssId = " + k,
								sbInsert.toString());
					}
				}
				else if (dr1.getStatus().getStatusCode() == 0 )
				{
					//System.out.println("UPDATE RESULTS DATA.........");
					// THERE ALREADY IS a row, so do the update..				
					// Get the unique id for parameter 8
					rs1 = dr1.getResultSet();
					rs1.next();
					//String uid = rs1.getString("UniqueIdStamp");
					String uid = rs1.getString("scoreId");
					//String date = com.pdi.data.v2.util.Utils.getHRADateString();
					String date = sdf.format(new Date());
					
					DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
					if (dlps.isInError())
					{
						//System.out.println("Error preparing writeResultsDataToDatabase query." + dlps.toString());
						LogWriter.logBasic(LogWriter.ERROR, this, 
								"Insert setup error:  " + dlps.toString() + 
								"  PartId = " + participantId + 
								", projId = " + projectId + ",  "  +
								", spssId = " + k);
						return;
					}	
						
					//THIS IS THE UPDATE ...						
					// Fill sql parmeters with values and get the data
					dlps.getPreparedStatement().setString(1, date);
					if(fromTestData){
						dlps.getPreparedStatement().setString(2, dbUserTestData);
					}else{
						dlps.getPreparedStatement().setString(2, dbUser);
					}
					//dlps.getPreparedStatement().setString(3, s.getRawScore());
					String str = s.getRawScore().trim();
					double dbl;
					if (str == null || str.isEmpty())
					{
						// set to nothing
						dbl = 0.0;
					}
					else
					{
						try
						{
							dbl = Double.valueOf(str);
						}
						catch (NumberFormatException e)
						{
							System.out.println("PPI - Update results:  " + str + " is an invalid number.  Zero used.");
							dbl = 0.0;
						}
					}
					dlps.getPreparedStatement().setDouble(3, dbl);
					dlps.getPreparedStatement().setString(4, uid );

					try
					{
						drUpdate = V2DatabaseUtils.update(dlps);
					}
					catch(Exception e)
					{
						LogWriter.logSQL(LogWriter.ERROR, this, 
								"Update setup error:" +
								"  Message = " + e.getMessage() +
								", PartId = " + participantId + 
								", projId = " + projectId + ",  "  +
								", spssId = " + k,
								sb.toString());
					}
				}
				else
				{							
					LogWriter.logSQL(LogWriter.ERROR, this,
							"Update results." +
							"  PartId = " + participantId + 
							",  projId = " + projectId + 
							",  spssValue = " + k +
							", constructScore = " + s.getRawScore(),
							sb.toString());
				}
			} catch (Exception e) {
				// Does nothing
			}
			finally
			{
				if(dr1 != null) { dr1.close(); dr1 = null; }
				if(drInsert != null) { drInsert.close(); drInsert = null; }
				if(drUpdate != null) { drUpdate.close(); drUpdate = null; }
			}			
		}	// End of for loop
	}

	/**
	 * Return all instruments.
	 * Return the most recent instrument
	 * if there is a duplicate instrument in the list.
	 * 
	 * Note that the "last" instrument may be from a
	 * different project then the other instruments.  In
	 * fact, if you have various instruments from multiple
	 * projects, the latest may all be from different 
	 * projects.  You really just don't know until
	 * you get them.
	 * 
	 * TODO:  This is a note related to how TestData updates
	 * scores.  At this time (3-07-2011) if ANY change is made
	 * to an instrument in TestData ( manual entry ) then ALL
	 * scale scores are updated.  This should keep all the "date taken"
	 * dates the same for a given instrument.  
	 * 		In that case, if we get the data sorted on instrument, 
	 * 		project (and probably scale), then we will put all of 
	 * 		the scales for a particular instance together and not 
	 * 		have to worry about different scale scores from different 
	 * 		instances/projects being jumbled together
	 * We need to make sure (eventually) that that is the way TestData
	 * 		 does updates/submits and when moved to SQL Server that 
	 * we will need do it in a transaction
	 * 
	 * NOTE: NHN-3163 - added @param String callingProjectId -- this is the projectId, if available, of the project requesting the report. Thiis
	 * 										is added because the norms used to calc the report need to come from the 
	 * 										calling project, in the case where an instrument was taken in a previous 
	 * 										project possibly having different norms.  If the projectId for the calling 
	 * 										project is not available, null should be used.
	 * 									NOTE: this is a pass-through value, which will actually be used in the 
	 * 										method ProjectParticipantInstrumentHelper.fromParticipantId()
	 * 										Added per NHN-3163.
	 *
	 * * usecase: I need the most recent GPI for this participant
	 * @param SessionUser session
	 * @param int mode
	 * @param String instrumentId
	 * @param String callingProjectId  
	 * @return ArrayList<ProjectParticipantInstrument> epiList
	 */
	//@Override
	public ArrayList<ProjectParticipantInstrument> fromLastParticipantId(
			SessionUser session, int mode, String participantId, String callingProjectId)
	{
		ArrayList<ProjectParticipantInstrument> epiListLastDate = new  ArrayList<ProjectParticipantInstrument>();
		
		// call the greater one... . 
		ArrayList<ProjectParticipantInstrument> epiList = this.fromParticipantId(session, mode, participantId, callingProjectId);
		// then check for dups..
		
		// use this to hold the instrument code / dateCreated 
		// to compare against
		HashMap<String, ProjectParticipantInstrument> inst_date = new HashMap<String, ProjectParticipantInstrument>();
		
		for(ProjectParticipantInstrument epi : epiList){
			
			String instCode = epi.getInstrument().getCode();
			//String date = epi.getInstrumentTakenDate();
			
			if( inst_date.containsKey(instCode) ){
				/*
				//if it contains the key, check the date
				Float existingDate = new Float  (inst_date.get(instCode).getInstrumentTakenDate());
				//if the date on the epi in the list is greater than, just go on
				Float currentEpiDate = new Float (epi.getInstrumentTakenDate());
				
				// if the date on the existing epi is less than the current one, replace it
				if(existingDate < currentEpiDate){
					inst_date.put(instCode, epi);
				}*/
				
				// NHN-3162 found that his may not work to get the appropriate data.  The first one
				// is used, but if the date on that is older than the other(s) it is not appropriate.
				// Modified the fromParticipantId() method to put the newest first
				
			}else{
				// doesn't have it, add it to list
				inst_date.put(instCode, epi);			
			}
			
		}
		
		// add the epi's to the last date array list
		Iterator<String> iter = inst_date.keySet().iterator();
		while (iter.hasNext()){
			epiListLastDate.add(inst_date.get(iter.next()));		
		}
			
		// return un-duped one... 
		return epiListLastDate;
	}

	/**
	 * Return all instruments.
	 * Return the most recent instrument
	 * if there is a duplicate instrument in the list.
	 * 
	 * Note that the "last" instrument may be from a
	 * different project then the other instruments.  In
	 * fact, if you have various instruments from multiple
	 * projects, the latest may all be from different 
	 * projects.  You really just don't know until
	 * you get them.
	 * 
	 * NHN 1738: new use case, with participant in two
	 * different AbyD projects, so different sets of norms
	 * 
	 * * usecase: I the most recent GPI for this participant, but he
	 * is in two different projects, with different norms.  I want the
	 * norms for the current project.
	 * @param SessionUser session
	 * @param int mode
	 * @param String particpantId
	 * @param String callingProjectId
	 * @return ArrayList<ProjectParticipantInstrument> ppiList
	 */
	//@Override
	public ArrayList<ProjectParticipantInstrument> fromLastParticipantIdIncludeProjectIdForNorms(
			SessionUser session, int mode, String participantId, String callingProjectId)
	{
		ArrayList<ProjectParticipantInstrument> epiListLastDate = new  ArrayList<ProjectParticipantInstrument>();
		
		// call the greater one... that passes along the calling project id 
		ArrayList<ProjectParticipantInstrument> epiList = 
						this.fromParticipantIdIncludeProjectIdFromCallingProject(session, mode, participantId, callingProjectId);
		
		// then check for dups..
		
		// use this to hold the instrument code / dateCreated 
		// to compare against
		HashMap<String, ProjectParticipantInstrument> inst_date = new HashMap<String, ProjectParticipantInstrument>();
		
		for(ProjectParticipantInstrument epi : epiList){
			
			String instCode = epi.getInstrument().getCode();
			//String date = epi.getInstrumentTakenDate();
			
			// This works because the newest date for each instrument is first in the epiList
			if( inst_date.containsKey(instCode) ){
				// commented out because not needed if the newst date is first
				/*
				//if it contains the key, check the date
				Float existingDate = new Float  (inst_date.get(instCode).getInstrumentTakenDate());
				//if the date on the epi in the list is greater than, just go on
				Float currentEpiDate = new Float (epi.getInstrumentTakenDate());
				
				// if the date on the existing epi is less than the current one, replace it
				if(existingDate < currentEpiDate){
					inst_date.put(instCode, epi);
				}*/
				
			}else{
				// doesn't have it, add it to list
				inst_date.put(instCode, epi);			
			}
			
		}
		
		// add the epi's to the last date array list
		Iterator<String> iter = inst_date.keySet().iterator();
		while (iter.hasNext()){
			epiListLastDate.add(inst_date.get(iter.next()));		
		}
			
		// return un-duped one... 
		return epiListLastDate;
	}
	
	
	// * 	1-22-2013 - boolean added to determine if the update is from the usual scoring routine, or from Test Data (Manual Entry) for billing purposes.
	//@Override
	public void addUpdate(SessionUser session, int mode,
			ArrayList<ProjectParticipantInstrument> epis, boolean fromTestData) {
		for(ProjectParticipantInstrument ppi : epis) {
			addUpdate(session, mode, ppi, fromTestData);
		}
		
	}

	
	
	//@Override
	public ArrayList<ProjectParticipantInstrument> fromParticipantListInstrumentId(
			SessionUser session, ArrayList<Participant> participants,
			String instrumentId) {
		// TODO Auto-generated method stub
		return null;
	}

	
	//@Override
	public ArrayList<ProjectParticipantInstrument> fromProjectId(
			SessionUser session, int mode, String projectId) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Used in Test Data, for the FinEx only.
	 * NHN-1624 adds the ability to input FinEx response
	 * data to the TestData App.
	 * 
	 * This method already existed in the nhn helper, 
	 * but wasn't accessible as it was private.
	 *  
	 * @param  String participantId
	 * @return String instrumentId  
	 */
	//@Override
	public ResponseGroup getResponseGroupByParticipantId(String participantId, String instrumentId){
		return null;
	}

	
}
