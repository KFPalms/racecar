package com.pdi.data.helpers.interfaces;

import com.pdi.data.dto.AttachmentHolder;
import com.pdi.data.dto.SessionUser;


public interface IAttachmentHelper {
	
	public AttachmentHolder fromProjectIdParticipantId(SessionUser session, String projectId, String participantId) throws Exception;
	public AttachmentHolder fromParticipantId(SessionUser session, String participantId) throws Exception;
	public AttachmentHolder fromProjectId(SessionUser session, String projectId) throws Exception;
	
	// Following functionality removed when Attachment maintenance moved to the Admin tools
//	public void modify(AttachmentHolder holder) throws Exception;
//	public void delete(ArrayList<Attachment> attachmentArray)	throws Exception;
//	public void delete(Attachment attachment)	throws Exception;
}
