/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.data.dto;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

//import com.pdi.data.helpers.interfaces.IProjectHelper;

/**
 * Project is a basic data class that contains engagement (project) data.
 * 
 * NOTE:
 * As currently implemented, the contents of the rowguid, adminid, and templateid
 * columns in the database are not carried in the Project object as attributes, but
 * rather are contained as elements (with the same names)in the metadata HashMap.
 * If/When the object is re-factored to make them attributes of the object, the get
 * and create logic (and anywhere else that these items are referenced) will have to
 * be modified to reflect that change.
 * 
 * @author		Gavin Myers
 * @author      MB Panichi
 */
public class Project extends CoreDataObject
{

	//
	// Static data.
	//
	// The metadatatags of things that are not in the metadata in the database
	public static final String GUID = "rowguid";
	//public static final String PROJECTTYPE = "projecttype";
	
	//Project Type Constants  - these should be mirrored in the platform database.
	// They are used as the project_type code Name there.
	// IDs would have been preferable, but there are some projects multiply defined;
	// e.g., TLT w/Cogs and TLT w/o Cogs.
	// Eventually we will probably want to go to IDs, but not right now
	public static final String TLT = "TLT"; // Talent View of Transitions  (23)
	public static final String ABYD = "ABYD"; // Assessment By Design	(25, 26)
	public static final String ABYD_LONG = "Assessment By Design - Online"; // Assessment By Design	(25, 26)
	public static final String ASSESSMENT = "ASSESSMENT"; // Assessments  (2)
	public static final String TARGET_LP = "LP";  // Target Leadership Profile (3)
	public static final String TARGET_SHORT_LP = "SLP"; // Target Leadership Profile - short (32)
	public static final String TVOR = "TVOR"; // Talent View of Readiness (21)
	public static final String TCM = "TCM"; // Talent Content Management (30)
	public static final String JAV = "JAV"; // TCM Validation Questionaires
	public static final String SAC_PREWORK = "SAC_PRE"; // Shell prework
	public static final String SAC_PROCTORED = "SAC_PROC"; // Shell proctored 
	
	//
	// Static methods.
	//

	/**
	 * 
	 */
	private static final long serialVersionUID = -7223061453312517196L;
	//
	// Instance data.
	//
	private String id;
	private String name;
	private Client client;
	private String projectCode;
	private int projectTypeId;
	private String projectTypeName;
	private String adminId;
	private int targetLevelTypeId;
	private Date dateCreated;
	private int active;
	private Status status;
	private HashMap<String, String> metadata;
	private HashSet<String> participantCollection;
	private HashSet<String> contentCollection; 	
	private boolean participantsAdded;
	
	private String projectTypeCode;

	private String targetLevelName;

	private String targetLevelCode;

	private int targetLevelIndex;



	//
	// Constructors.
	//
	

	public Project()
	{
		// does nothing right now
	}

	public Project(String idIn)
	{
		id = idIn;
	}


	//
	// Instance methods.
	//
	
	
	public String toString()
	{
		String ret = "Project - ID=" + id + ", name=" + (name == null ? "-" : name) + ":";
		ret += "\n    typeId=" + projectTypeId + ", typeName=" + projectTypeName;
		ret += "\n    " + (client == null ? "Client null" : client.toString());
		ret += "\n    " + (status == null ? "Status null" : status.toString());
//		ret += "\n    Meta:";
//		for (Iterator<Entry<String, String>> itr=metadata.entrySet().iterator(); itr.hasNext(); )
//		{
//			Map.Entry<String, String> ent = itr.next();
//			ret += "\n     " + ent.getKey() + "=" + ent.getValue();
//		}
		
		return ret;
	}
	
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public void setId(String value)
	{
		id = value;
	}
	
	public String getId()
	{
		return id;
	}

	/*****************************************************************************************/
	public void setName(String value)
	{
		name = value;
	}
	
	public String getName()
	{
		return name;
	}

	/*****************************************************************************************/
	public void setClient(Client value)
	{
		client = value;
	}
	
	public Client getClient()
	{
		return client;
	}

	/*****************************************************************************************/
	public void setProjectCode(String value)
	{
		projectCode = value;
	}
	
	public String getProjectCode()
	{
		return projectCode;
	}

	/*****************************************************************************************/
	public void setProjectTypeId(int value)
	{
		projectTypeId = value;
	}

	public int getProjectTypeId()
	{
		return projectTypeId;
	}

	/*****************************************************************************************/
	public void setProjectTypeName(String projectType)
	{
		this.projectTypeName = projectType;
	}

	public String getProjectTypeName()
	{
		return projectTypeName;
	}

	/*****************************************************************************************/
	public void setAdminId(String adminId)
	{
		this.adminId = adminId;
	}

	public String getAdminId()
	{
		return adminId;
	}

	/*****************************************************************************************/
	public void setTargetLevelTypeId(int value)
	{
		this.targetLevelTypeId = value;
	}

	public int getTargetLevelTypeId()
	{
		return targetLevelTypeId;
	}
	
	/*****************************************************************************************/
	public void setTargetLevelName(String value)
	{
		this.targetLevelName = value;
	}

	public String getTargetLevelName()
	{
		return targetLevelName;
	}

	/*****************************************************************************************/
	public void setTargetLevelCode(String value)
	{
		this.targetLevelCode = value;
	}

	public String getTargetLevelCode()
	{
		return targetLevelCode;
	}
	/*****************************************************************************************/
	public void setDateCreated(Date dateCreated)
	{
		this.dateCreated = dateCreated;
	}

	public Date getDateCreated()
	{
		return dateCreated;
	}

	/*****************************************************************************************/
	public void setActive(int active)
	{
		this.active = active;
	}

	public int getActive()
	{
		return active;
	}

	/*****************************************************************************************/
	public void setStatus(Status value)
	{
		status = value;
	}
	
	public Status getStatus()
	{
		return status;
	}

	/*****************************************************************************************/
	public void setMetadata(HashMap<String, String> value)
	{
		metadata = value;
	}
	
	public HashMap<String, String> getMetadata()
	{
		return metadata;
	}

	/*****************************************************************************************/
	public void setParticipantCollection(HashSet<String> participantCollection)
	{
		this.participantCollection = participantCollection;
	}

	public HashSet<String> getParticipantCollection()
	{
		return participantCollection;
	}

		// Convenience participant methods
	public boolean addParticipantToProject(String participantId)
	{
		return participantCollection.add(participantId);
	}
	
	public boolean removeParticipantFromProject(Integer participantId)
	{
		return participantCollection.remove(participantId);
	}

	/*****************************************************************************************/
	public void setContentCollection(HashSet<String> contentCollection)
	{
		this.contentCollection = contentCollection;
	}

	public HashSet<String> getContentCollection() {
		return contentCollection;
	}
	
	/*****************************************************************************************/
    public void setParticipantsAdded(boolean value)
    {
           participantsAdded = value;
    }
    
    public boolean getParticipantsAdded()
    {
           return participantsAdded;
    }	

	/*****************************************************************************************/
	public void setProjectTypeCode(String projectTypeCode)
	{
		this.projectTypeCode = projectTypeCode;
	}

	public String getProjectTypeCode() {
		return projectTypeCode;
	}

	/*****************************************************************************************/
	public void setTargetLevelIndex(int value)
	{
		this.targetLevelIndex = value;
	}

	public int getTargetLevelIndex() {
		return this.targetLevelIndex;
	}
}
