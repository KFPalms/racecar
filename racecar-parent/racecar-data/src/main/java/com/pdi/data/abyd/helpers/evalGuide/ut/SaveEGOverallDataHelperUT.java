package com.pdi.data.abyd.helpers.evalGuide.ut;

import java.sql.Connection;

import com.pdi.data.abyd.dto.common.EGOverallData;
import com.pdi.data.abyd.dto.evalGuide.EGOverallDTO;
import com.pdi.data.abyd.helpers.evalGuide.SaveEGOverallDataHelper;
import com.pdi.ut.UnitTestUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;

import junit.framework.TestCase;

public class SaveEGOverallDataHelperUT extends TestCase
{
	//
	// Constructors
	//
	public SaveEGOverallDataHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
	throws Exception
	{
		junit.textui.TestRunner.run(SaveEGOverallDataHelperUT.class);
	}


	/*
	 * testSaveOverallData
	 */
	public void testSaveOverallData()
		throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);

		Connection con = AbyDDatabaseUtils.getDBConnection();
		EGOverallData ovDat = new EGOverallData();
		ovDat.setBriefDesc("Brief Description - HUT");
		ovDat.setKeyStrengths("Key Strengths - HUT");
		ovDat.setKeyDevNeeds("Key Development Needs - HUT");
		ovDat.setCoachNotes("Coaching Notes - HUT");

		EGOverallDTO inp = new EGOverallDTO();
    	inp.setParticipantId("KENSPART");
    	inp.setDnaId(999);
    	inp.setModuleId(888);
    	inp.setOverallData(ovDat);

    	SaveEGOverallDataHelper helper = new SaveEGOverallDataHelper(con, inp);    
		helper.saveOverallData();
		System.out.println("Check the db...");

		UnitTestUtils.stop(this);
	}
}
