package com.pdi.data.v2.helpers.delegated;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
//import java.util.HashMap;
import com.pdi.data.dto.Instrument;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.interfaces.IInstrumentHelper;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.logging.LogWriter;



public class InstrumentHelper implements IInstrumentHelper{

	//
	//  Static data.
	//

	
	//
	// Instance data.
	//

	//
	// Constructors.
	//
	
	
	//
	//  Instance methods.
	//
	
	/**
	 * fromId
	 * use case:  get instrument from instrumentId
	 * @param  SessionUser session
	 * @param  String instrumentId
	 * @return  Instrument
	 */
	//@Override
	public Instrument fromId(SessionUser session, String instrumentCode){
		
		Instrument instrument = new Instrument();
		instrument.setCode(instrumentCode);
		try {
			StringBuffer sb = new StringBuffer();
			sb.append("select * from pdi_instrument_code where instrumentCode = ?");
			
			DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
			if (dlps.isInError())
			{
				//TODO When this method returns something, this should be modified
				LogWriter.logSQL(LogWriter.WARNING, this, 
						"Error preparing fromId(SessionUser session, String instrumentCode) query." + dlps.toString(), 
						sb.toString());
			}

			DataResult dr = null;
			try
			{
				// Fill sql parmeters with values and get the data
				dlps.getPreparedStatement().setString(1, instrumentCode);

				dr = V2DatabaseUtils.select(dlps);
				ResultSet rs = dr.getResultSet();
					
				if (rs != null && rs.isBeforeFirst())
				{
					while(rs.next()) {
						instrument.setCode(rs.getString("instrumentCode"));
						instrument.setDescription(rs.getString("instrumentName"));
						instrument.setShortName(rs.getString("instrumentShortName"));
						
						instrument.getMetadata().put("nhnInstId", rs.getString("nhnInstID"));
						instrument.getMetadata().put("v2ModId", rs.getString("v2ModId"));
						instrument.getMetadata().put("researchInstId", rs.getString("researchInstId"));
						instrument.getMetadata().put("reportingInstId", rs.getString("reportingInstId"));
					}
				}
			}
			catch(java.sql.SQLException se)
			{
            	LogWriter.logSQLWithException(LogWriter.ERROR, this, 
            			"Error in com.pdi.data.v2.helpers.delegated.InstrumentHelper all()", 
            			sb.toString(), se);
			}
			finally
			{
				if (dr != null) { dr.close(); dr = null; }
			}
		} catch(Exception e) {
			LogWriter.logBasicWithException(LogWriter.ERROR, this, e.getMessage(), e);
		}
		return instrument;
	}


	/**
	 * all - Get all instruments by a type
	 * @param session
	 * @param type - Type is defined in IInstrumentHelper.  Use the call types not the db types.
	 * @return - ArrayList<Instrument>
	 */
	//@Override
	public ArrayList<Instrument> all(SessionUser session, int type)
		throws Exception
	{
		// Session not yet used
		// Check for valid type
		ArrayList<Instrument> ret = new ArrayList<Instrument>();
		
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT * FROM pdi_instrument_code ic ");
		sb.append("  WHERE ic.active = 1 ");
		if (type == IInstrumentHelper.TYPE_SCORABLE)
		{
			sb.append("    AND ic.scoreType = " + IInstrumentHelper.DB_SCORE_TYPE + " ");
		}
		sb.append("  ORDER BY ic.instrumentName");

		DataLayerPreparedStatement dlps = null;
		DataResult dr = null;
		ResultSet rs = null;

		try
		{
			dlps = V2DatabaseUtils.prepareStatement(sb);
			if (dlps.isInError())
			{
				LogWriter.logSQL(LogWriter.WARNING, this, "Error preparing 'all' query." + dlps.toString(), sb.toString());
				return ret;
			}
	
			dr = V2DatabaseUtils.select(dlps);
			if (dr.isInError())
			{
				LogWriter.logSQL(LogWriter.ERROR, this, "Error preparing 'all' query:  dr.isInError() " + dlps.toString(), sb.toString());
				return ret;
			}
			
			// Put them out
			rs = dr.getResultSet();

	       	if (rs == null || ! rs.isBeforeFirst())
	       	{
	       		// there are no instruments of the specified type
	       		return  ret;
	       	}

	       	while (rs.next())
	       	{
	       		Instrument inst = new Instrument();
	       		inst.setCode(rs.getString("instrumentCode"));
	       		inst.setDescription(rs.getString("instrumentName"));
	       		inst.setShortName(rs.getString("instrumentShortName"));
	       		//inst.setId(rs.getString("instrumentCode"));
	       		ret.add(inst);
	       		
	       	}

	       	return ret;
		}
		catch (SQLException se)
		{
			LogWriter.logSQLWithException(LogWriter.ERROR, this, se.getMessage(), sb.toString(), se);
			return new ArrayList<Instrument>();
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}		
	}

}
