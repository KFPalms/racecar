package com.pdi.data.nhn.helpers.delegated;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import com.pdi.logging.LogWriter;
import com.pdi.data.dto.Client;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.interfaces.IClientHelper;
import com.pdi.data.nhn.util.NhnDatabaseUtils;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;

public class ClientHelper implements IClientHelper {
		
	//@Override
	public ArrayList<Client> all(SessionUser session) {
		ArrayList<Client> clients = new ArrayList<Client>();
		StringBuffer sb = new StringBuffer();		
		sb.append("select " +
				"c.company_id, " +
				"c.coname, " +
				"c.rowguid, " +
				"c.enforce_secure_password, " +
				"c.randomize_passwords, " +
				"c.lang_pref, " +
				"c.show_reporting, " +
				"c.show_member_details, " +
				"c.iadvice_flag, " +
				"c.permitallprog, " +
				"c.show_lang_pref " +
				"from platform.dbo.company c " +
				"order by coname asc");

		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(sb);
		if (dlps.isInError()) {
			LogWriter.logSQL(LogWriter.WARNING, this, "dlps.isInError() " + dlps.toString(), sb.toString());
		}
		DataResult dr = null;
		ResultSet rs = null;
		try
		{
			dr = NhnDatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			if (rs != null && rs.isBeforeFirst())			{
				while (rs.next())				{
					Client client = new Client();
					client.setId(rs.getString("company_id"));
					client.setName(rs.getString("coname"));
					HashMap<String, String> hm = new HashMap<String, String>(); 
					hm.put("name", rs.getString(2)); //Why not?
					hm.put("rowguid", rs.getString(3)); 
					hm.put("enforce_secure_password", rs.getString(4)); 
					hm.put("randomize_passwords", rs.getString(5)); 
					hm.put("lang_pref", rs.getString(6)); 
					hm.put("show_reporting", rs.getString(7)); 
					hm.put("show_member_details", rs.getString(8)); 
					hm.put("iadvice_flag", rs.getString(9)); 
					hm.put("permitallprog", rs.getString(10)); 
					hm.put("show_lang_pref", rs.getString(11)); 
//					hm.put("active", (String)getClientIsActive(rs.getString(1)).toString());					
					client.setMetadata(hm);
					clients.add(client);
				}
				rs.close();				
			}
		}
		catch(java.sql.SQLException se)		{
			LogWriter.logSQLWithException(LogWriter.ERROR, this, 
					"Error in com.pdi.data.nhn.helpers.delegated.ClientHelper all()", 
					sb.toString(), se);
		
		}		finally		{
			if (dr != null) {dr.close(); dr = null;}			
		}		
		return clients;
	}

	//@Override
	public Client fromId(SessionUser session, String clientId) {
		Client client = new Client();		
		StringBuffer sb = new StringBuffer();		
		//System.out.println("Line 1.3.7");
		sb.append(" select " +
				"c.company_id, " +
				"c.coname, " +
				"c.rowguid, " +
				"c.enforce_secure_password, " +
				"c.randomize_passwords, " +
				"c.lang_pref, " +
				"c.show_reporting, " +
				"c.show_member_details, " +
				"c.iadvice_flag, " +
				"c.permitallprog, " +
				"c.show_lang_pref " +
				"from platform.dbo.company c " +
				"where company_id = " + Integer.parseInt(clientId));
		//System.out.println("Line 1.3.8");
		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(sb);
		if (dlps.isInError()) {
			LogWriter.logSQL(LogWriter.WARNING, this, "dlps.isInError() " + dlps.toString(), sb.toString());
		}
		DataResult dr = null;
		ResultSet rs = null;
		try		{
			//System.out.println("Line 1.3.9");
			dr = NhnDatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			if (rs != null && rs.isBeforeFirst())			{
				while (rs.next())				{
					//System.out.println("Line 1.3.10");
					client.setId(rs.getString("company_id"));
					client.setName(rs.getString("coname"));
					HashMap<String, String> hm = new HashMap<String, String>(); 
					hm.put("name", rs.getString(2)); //Why not?
					hm.put("rowguid", rs.getString(3)); 
					hm.put("enforce_secure_password", rs.getString(4)); 
					hm.put("randomize_passwords", rs.getString(5)); 
					hm.put("lang_pref", rs.getString(6)); 
					hm.put("show_reporting", rs.getString(7)); 
					hm.put("show_member_details", rs.getString(8)); 
					hm.put("iadvice_flag", rs.getString(9)); 
					hm.put("permitallprog", rs.getString(10)); 
					hm.put("show_lang_pref", rs.getString(11)); 
//					hm.put("active", (String)getClientIsActive(clientId).toString());
					client.setMetadata(hm);
				}
				rs.close();			
			}
		}catch(java.sql.SQLException se){
			LogWriter.logSQLWithException(LogWriter.ERROR, this, 
					"Error in com.pdi.data.nhn.helpers.delegated.ClientHelper fromId(SessionUser session, String clientId)" 
					+ "  clientId=" + clientId + "  ", 
					sb.toString(), se);
		}
		finally 		{
			if (dr != null) {dr.close(); dr = null;}			
		}
		return client;
	}
	
	public String getClientIsActive(String clientId)	{
		/**
		 * Get active status -- an extra step, we can see if we actually need this
		 */
		StringBuffer ab = new StringBuffer();
		ab.append("select c.company_id from company c join subterm s with (nolock)on s.company_id = c.company_id " +
				"where DATEADD(month, s.term, s.startdate ) < GETDATE() and c.company_id = " + clientId);
		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(ab);
		if (dlps.isInError()) {
			LogWriter.logSQL(LogWriter.WARNING, this, "dlps.isInError() " + dlps.toString(), ab.toString());
		}
		DataResult dr = null;
		ResultSet rs = null;
		String isActive = "0";
		try		{
			dr = NhnDatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			if (rs != null && rs.isBeforeFirst())			{
				isActive = "1";
				rs.close();							
			}
		}catch(java.sql.SQLException se){
			
			LogWriter.logSQLWithException(LogWriter.ERROR, this, 
					"Error in com.pdi.data.nhn.helpers.delegated.ClientHelper getClientIsActive(String clientId)" 
					+ "  clientId=" + clientId + "  ", 
					ab.toString(), se);

		}finally		{
			if (dr != null) {dr.close(); dr = null;}			
		}
		return isActive;
		
	}
	
	public Client getClientByParticipantId(String participantId){
		Client client = null;		
		StringBuffer sb = new StringBuffer();
		sb.append("select company_id from platform.dbo.users where users_id = " + participantId);		
		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(sb);
		if (dlps.isInError()) {
			LogWriter.logSQL(LogWriter.WARNING, this, "dlps.isInError() " + dlps.toString(), sb.toString());
		}
		DataResult dr = null;
		ResultSet rs = null;
		try		{
			dr = NhnDatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			if (rs != null && rs.isBeforeFirst())		
			{
				rs.next();  // gotta get the first row, or it blows up.
				client = fromId(null,rs.getString(1));
				rs.close();							
			}
		}catch(java.sql.SQLException se){
			
			LogWriter.logSQLWithException(LogWriter.ERROR, this, 
					"Error in com.pdi.data.nhn.helpers.delegated.ClientHelper getClientByParticipantId(String participantId)" 
					+ "  participantId=" + participantId + "  ", 
					sb.toString(), se);
			
		}finally		{
			if (dr != null) {dr.close(); dr = null;}			
		}
		return client;
	}

}
