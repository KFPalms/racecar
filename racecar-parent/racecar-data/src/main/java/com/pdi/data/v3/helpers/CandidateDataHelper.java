/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.helpers;

import java.util.ArrayList;
import org.w3c.dom.Document;

import com.pdi.data.util.RequestStatus;
import com.pdi.data.v3.dto.*;
import com.pdi.data.v3.util.V3WebserviceUtils;
import com.pdi.xml.XMLUtils;

import org.w3c.dom.*;

/**
 * DataHelper for Candidate Information
 * 
 * @author		Gavin Myers
 * @author		Ken Beukelman
 */
public class CandidateDataHelper
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//

	//
	// Constructors.
	//

	//
	// Instance methods.
	//


	/**
	 * Get all candidates that a particular user has access to
	 * NOTE:  This functionality appears to be applicable to admin users only.
	 *        All candidates should not be exposed to client-specific users
	 * @param sessionUser
	 * @return
	 */
	public static CandidateListDTO getCandidates(SessionUserDTO sessionUser)
	{
		DocumentDTO dto = V3WebserviceUtils.request(sessionUser,"candidate/list","");
		if (! dto.getStatus().isOk())
		{
			return new CandidateListDTO(dto.getStatus());
		}

		Document response = dto.getDocument();
		NodeList nodes = XMLUtils.getElements(response, "//candidateListLine");
		
		ArrayList<CandidateDTO> candidates = new ArrayList<CandidateDTO>();
		for(int i = 0; i < nodes.getLength(); i++)
		{
			Node node = nodes.item(i);
			candidates.add(new CandidateDTO(node));
		}

		return new CandidateListDTO(candidates);
	}


	/**
	 * Returns the completed participant information
	 * NOTE:  If the uri is present, it will be used.  The candidate id will be ignored.
	 *        If there is no uri, then the candidate id will be used. 
	 * @param candidate a candidate with at least the ID set
	 * @return a ClientDTO fully filled in
	 */
	public static CandidateDTO getCandidate(SessionUserDTO sessionUser, CandidateDTO candidate)
	{
		if (candidate.getUri() == null  &&
			candidate.getId() == null)
		{
			candidate.setStatus(new RequestStatus(RequestStatus.RS_ERROR, "A candidate ID or a URI is required to fetch a client."));
			return candidate;
		}

		DocumentDTO docDto;
		if (candidate.getUri() == null)
		{
			// get the data directly
			docDto = V3WebserviceUtils.request(sessionUser,"candidate/" + candidate.getId(),"");
		}
		else
		{
			// get the data using the uri
			docDto = V3WebserviceUtils.request(sessionUser,candidate.getUri(),"");
		}

		if (! docDto.getStatus().isOk())
		{
			candidate.setStatus(docDto.getStatus());
			return candidate;
		}

		Document response = docDto.getDocument();
		NodeList nodes = XMLUtils.getElements(response, "//resultCandidate");

		CandidateDTO newCand =  new CandidateDTO(nodes.item(0));
		// Preserve the incoming URI (if any)
		if (candidate.getUri() != null)
		{
			newCand.setUri(candidate.getUri());		
		}
		
		return newCand;
	}


	/**
	 * Get a specific candidate by id
	 * 
	 * @param sessionUser
	 * @param candidateId
	 * @return
	 */
	public static CandidateDTO getCandidate(SessionUserDTO sessionUser,String candidateId)
	{
		CandidateDTO inp = new CandidateDTO();
		inp.setId(candidateId);
		
		return getCandidate(sessionUser, inp);
	}

	
	
	/**
	 * Get all candidates for a particular organization - Currently returns empty list - should return CandidateListDTO
	 * @param sessionUser
	 * @param organization
	 * @return
	 */
	public static ArrayList<CandidateDTO> getCandidates(SessionUserDTO sessionUser, OrganizationDTO organization)
	{
		return new ArrayList<CandidateDTO>();
	}
	
	/**
	 * Get all candidates for a particular client - Currently returns empty list - should return CandidateListDTO
	 * @param sessionUser
	 * @param organization
	 * @return
	 */
	public static ArrayList<CandidateDTO> getCandidates(SessionUserDTO sessionUser, ClientDTO client)
	{
		// "candidate/list/" + client.getId() - Should return a list of proxy candidates
		return new ArrayList<CandidateDTO>();
	}
	
	
	

	//TODO Fill out the logic for the rest of these methods as needed
	//TODO Expose the rest of these methods as needed via the service

	//  All of the following methods will need to return some sort of status.
	//  The voids should probably return a RequestStatus object


	/**
	 * Create candidate
	 * @param sessionUser
	 * @param candidate
	 */
	public static void createCandidate(SessionUserDTO sessionUser, CandidateDTO candidate)
	{
		
	}


	/**
	 * Update candidate
	 * @param sessionUser
	 * @param candidate
	 */
	public static void updateCandidate(SessionUserDTO sessionUser, CandidateDTO candidate)
	{
		
	}


	/**
	 * Activate candidate
	 * @param sessionUser
	 * @param candidate
	 */
	public static void activateCandidate(SessionUserDTO sessionUser, CandidateDTO candidate)
	{
		
	}


	/**
	 * Deactivate candidate
	 * @param sessionUser
	 * @param candidate
	 */
	public static void deactivateCandidate(SessionUserDTO sessionUser, CandidateDTO candidate)
	{
		
	}
	
}
