package com.pdi.data.abyd.helpers.common.ut;

import java.sql.Connection;

//import com.pdi.data.abyd.dto.common.EGFullDataDTO;
//import com.pdi.data.abyd.helpers.common.EGDataHelper;
import com.pdi.data.abyd.helpers.common.ImportExportDataHelper;
import com.pdi.ut.UnitTestUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;

import junit.framework.TestCase;

public class ImportExportDataHelperUT  extends TestCase
{
	//
	// Constructors
	//
	public ImportExportDataHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
	throws Exception
	{
		junit.textui.TestRunner.run(ImportExportDataHelperUT.class);
	}


	/*
	 * testGetDnaNameFromV2
	 */
	public void testGetDnaNameFromV2()
		throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);
		
		Connection con = AbyDDatabaseUtils.getDBConnection();
		String jobId = "DXGNYGUF";

		ImportExportDataHelper helper = new ImportExportDataHelper();        
		String ret = helper.getDnaNameFromV2(con, jobId);
		
		System.out.println("DNA name for job " + jobId + "=" + (ret==null ? "NULL" : ret));
		
		UnitTestUtils.stop(this);
	}

}
