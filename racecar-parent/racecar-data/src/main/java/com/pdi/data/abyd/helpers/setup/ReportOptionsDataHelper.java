/**
 * Copyright (c) 2008 Personnel Decisions International, Inc.
 * All rights reserved.
 *
 * $Header: /com/pdi/data/abyd/helpers/setup/Setup2DataHelper 6     8/25/05 3:23p mpanichi $
 */

package com.pdi.data.abyd.helpers.setup;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import com.pdi.data.abyd.dto.setup.Setup2ReportOptionsDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportOptionValueDTO;


/**
 * Setup2DataHelper contains all the code needed to set up an A by D report
 *
 * @author		Ken Beukelman
 * @author		MB Panichi
 */
public class ReportOptionsDataHelper
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	
	//
	// Constructors.
	//
	
	//
	//  Instance methods.
	//
	
	/**
	 * Fetch the initial data needed for the By Design Setup app.
	 *
	 * @return the EntryStateDTO object
	 * @throws Exception
	 */
	public Setup2ReportOptionsDTO getReportOptionData(Connection con, long dnaId)
		throws Exception
	{
		// instantiate a return ArrayList
		Setup2ReportOptionsDTO ret = new Setup2ReportOptionsDTO();
		
		HashMap<Long, Setup2ReportOptionValueDTO> optionsMap = new HashMap<Long,Setup2ReportOptionValueDTO>();

		// get the default data
		optionsMap = getDefaultReportOptionsData(con);
		
		// update the default data with any custom options
		getReportOptionsDataForDnaId(con, dnaId, optionsMap);
		
		// build the return DTO....
		ret = buildReturnDTO(dnaId, optionsMap);
		
 		return ret;
	}      

	
	/**
	 * resetToDefaultReportOptionData
	 *
	 * @param   Connection con - the database connection
	 * @param   long dnaId - the dna id
	 * @return  Setup2ReportOptionsDTO object
	 * @throws Exception
	 */
	public Setup2ReportOptionsDTO resetToDefaultReportOptionData(Connection con, long dnaId)
		throws Exception
	{
		// instantiate a return ArrayList
		Setup2ReportOptionsDTO ret = new Setup2ReportOptionsDTO();
		
		HashMap<Long, Setup2ReportOptionValueDTO> optionsMap = new HashMap<Long,Setup2ReportOptionValueDTO>();

		// remove the updated options for this dna id... 
		resetReportOptionsDataForDnaId(con, dnaId);
		
		// get the default data only
		optionsMap = getDefaultReportOptionsData(con);
		
		// build the return DTO with just the default data....
		ret = buildReturnDTO(dnaId, optionsMap);
				
 		return ret;
	} 
	
	/**
	 * Update Options/configuration data in the Dashboard Report.
	 *
	 * @param Connection con - the database connection
	 * @param	Setup2ReportOptionsDTO dtoIn  - the incoming DTO with data to update
	 * @throws Exception
	 */
	public void updateReportOptionsData(Connection con, Setup2ReportOptionsDTO dtoIn )
		throws Exception
	{
	    String userId = "s2rptDataHlpr";

		Statement stmt = null;
		ResultSet rs = null;
		Statement stmt2 = null;
		ResultSet rs2 = null;
		Statement stmt3 = null;
		Statement stmt4 = null;
				
		try{			
			 // iterate through the dto's -- we need to look at each one... 
			 Iterator<Setup2ReportOptionValueDTO> it = dtoIn.getOptionValuesArray().iterator();
			 while(it.hasNext()){
				 
				 Setup2ReportOptionValueDTO dto = new Setup2ReportOptionValueDTO();
				 dto = it.next();
				 
				 // first, see if there exists a row in the 
				 // pdi_abd_rpt_options_dna table... 
				
				 // select sql:
					StringBuffer sqlQuery = new StringBuffer();
					sqlQuery.append("  SELECT option_value, option_string   ");
					sqlQuery.append("  FROM pdi_abd_rpt_options_dna  ");
					sqlQuery.append("  WHERE dna_id =  " + dtoIn.getDnaId() + "  ");
					sqlQuery.append("  AND report_options_id = " + dto.getReportOptionsId() + "  ");
					//System.out.println("sqlQuery = " + sqlQuery.toString());
					
					
				 // run the query
					stmt = con.createStatement();
					rs = stmt.executeQuery(sqlQuery.toString());
					
					int dtoValue = 0;
					String dtoString = dto.getOptionString();
				 // if the row DOES NOT EXIST, 
					if (!rs.isBeforeFirst() )
			       	{
						 // if the row DOES NOT EXIST, we will need to INSERT IT... 	
						
						// but, did it change from the original?
						// check against the lookup table... 
						// select sql:
						sqlQuery = new StringBuffer();
						sqlQuery.append("  SELECT is_default   ");
						sqlQuery.append("  FROM pdi_abd_rpt_options_lookup  ");
						sqlQuery.append("  WHERE report_options_id = " + dto.getReportOptionsId() + "  ");
						sqlQuery.append("    AND active = 1");
						//System.out.println("sqlQuery 2 = " + sqlQuery.toString());

						// run the query
						stmt2 = con.createStatement();
						rs2 = stmt2.executeQuery(sqlQuery.toString());
						
						rs2.next();
						// get the database value
						int databaseValue = rs2.getInt(1);
						// set the incoming dto value
						//System.out.println("dto.getOptionValue() " + dto.getOptionValue());
					 	if(dto.getOptionValue())
					 	{
							dtoValue = 1;
						}
						// IF the option_value is  DIFFERENT than the incoming value
						if(databaseValue == dtoValue && dtoString != "")
						{
							// they're the same... do nothing
							//System.out.println(dto.getReportOptionsId() + " CONTINUE: databaseValue == dtoValue : " + databaseValue + " == " + dtoValue);
							continue;
						} else {
							//System.out.println(dto.getReportOptionsId() + " INSERT: databaseValue == dtoValue : " + databaseValue + " == " + dtoValue);
							StringBuffer sqlInsert = new StringBuffer();
							
							sqlInsert.append(" INSERT into pdi_abd_rpt_options_dna    ");
							sqlInsert.append("  (dna_id, report_options_id, option_value, option_string, dateCreated, createdBy)  ");
							sqlInsert.append("   VALUES( " + dtoIn.getDnaId() +   ",  ");
							sqlInsert.append("  " + dto.getReportOptionsId()+  ",  ");
							sqlInsert.append("  " + dtoValue +  ",  ");
							sqlInsert.append("  N'" + dtoString.replace("'", "''") +  "',  ");
							sqlInsert.append("  GETDATE(),  ");
							sqlInsert.append("   '" + userId + "'  ");
							sqlInsert.append("   )  ");
							//System.out.println("sqlInsert = " + sqlInsert.toString());
							
							// run the query
							stmt3 = con.createStatement();
						    stmt3.executeUpdate(sqlInsert.toString());
						}
			       	} else {	
			       		//System.out.println(dto.getReportOptionsId() + " CONTINUE OR UPDATE:  if (rs != null )....");
			       		rs.next();  // this rs is looking at the rpt_options_dna table... 
			       					// be aware that this update could change the value here to be the same value
			       					// as what's in the lookup table...  this happens because there's already a row
			       					// here... so this value can be changed and then put back again.
			       		
			       		
						// get the database value
						int databaseValue = rs.getInt(1);
						String databaseString = rs.getString(2);
						// get the incoming dto value
						//System.out.println("dto.getOptionValue() " + dto.getOptionValue());
					 	if(dto.getOptionValue()){
							dtoValue = 1;
						}
						// AND the option_value is  DIFFERENT than the incoming value
					 	
					 	if(databaseValue == dtoValue && databaseString.equals(dtoString))
						{
							// they're the same... do nothing
							//System.out.println(dto.getReportOptionsId() + " CONTINUE: databaseValue == dtoValue : " + databaseValue + " == " + dtoValue);
							continue;
							
						} else {
							// UPDATE the value in the database
							//System.out.println(dto.getReportOptionsId() + " UPDATE: databaseValue == dtoValue : " + databaseValue + " == " + dtoValue);
							
							// updateSQL:
							StringBuffer sqlUpdate = new StringBuffer();														
							sqlUpdate.append("  UPDATE pdi_abd_rpt_options_dna   ");
							sqlUpdate.append("  SET option_value = " + dtoValue +",   ");
							sqlUpdate.append("   option_string = N'"+ dtoString.replace("'", "''") +"',  ");
							sqlUpdate.append("   dateCreated = GETDATE(),  ");
							sqlUpdate.append("   createdBy = '" + userId + "'  ");
							sqlUpdate.append("  WHERE dna_id = " + dtoIn.getDnaId() + "  ");
							sqlUpdate.append("  AND report_options_id = " + dto.getReportOptionsId() + "  ");
							//System.out.println("sqlUpdate = " + sqlUpdate.toString());
							
							// get a clean connection:
							stmt4 = con.createStatement();
							// run the query
							stmt4 = con.createStatement();
						    stmt4.executeUpdate(sqlUpdate.toString());
						}
			       	}
			 }// end main while iterator
		}
	    catch (SQLException ex)
		{
	        // handle any errors
	    	throw new Exception("updateReportOptionsData:  SQL updating V2 report options data.  " +
	    			"SQLException: " + ex.getMessage() + ", " +
	    			"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
	    }
	    finally
		{
	        if (rs != null)
	        {
	            try
				{
	                rs.close();
	            }
	            catch (SQLException sqlEx)
				{
	            	// swallow the error
	            }
	            rs = null;
	        }
	        if (stmt != null)
	        {
	            try
				{
	                stmt.close();
	            }
	            catch (SQLException sqlEx)
				{
	            	// swallow the error
				}
	            stmt = null;
	        }
	        if (stmt2 != null)
	        {
	            try
				{
	                stmt2.close();
	            }
	            catch (SQLException sqlEx)
				{
	            	// swallow the error
				}
	            stmt2 = null;
	        }
		}
	}  

	
	/**
	 * getDefaultReportOptionsData
	 * Fetch report options data from v2.  
	 * Adds the info to the instance DTO object, and
	 * fills the array
	 *
	 * @param  Connection con - the database connection 
	 * @return HashMap<Long,Setup2ReportOptionValueDTO>
	 * @throws Exception
	 */
	private HashMap<Long,Setup2ReportOptionValueDTO> getDefaultReportOptionsData(Connection connection)
		throws Exception
	{
		HashMap<Long, Setup2ReportOptionValueDTO> optionsMap = new HashMap<Long,Setup2ReportOptionValueDTO>();
		
		Statement stmt = null;
		ResultSet rs = null;

        StringBuffer sqlQuery = new StringBuffer();
        sqlQuery.append("SELECT report_options_id, option_code,  label, is_default, option_sequence ");
        sqlQuery.append("  FROM pdi_abd_rpt_options_lookup ");
        sqlQuery.append("  WHERE active = 1 ");
        sqlQuery.append("  ORDER BY option_sequence");
        //System.out.println("sql:  " + sqlQuery.toString());
        
        try
		{
	       	stmt = connection.createStatement();
	       	rs = stmt.executeQuery(sqlQuery.toString());
          	
	       	if (! rs.isBeforeFirst())
	       	{
	       		//System.out.println("No V2 data exists.");
	       		throw new Exception("No V2 data exists.");
	       	}
	     
	       	while(rs.next())
	       	{
	       		// fill up the DTO's and the array
	       		Setup2ReportOptionValueDTO dto = new Setup2ReportOptionValueDTO();
	       		//System.out.println(dto.toString());
	       		dto.setReportOptionsId(new Long(rs.getInt(1)).longValue()); 
	       		dto.setOptionCode(rs.getString(2));
	       		dto.setLabel(rs.getString(3));
	       		int defaultValue = rs.getInt(4);
	       		if (defaultValue == 0)
	       		{
	       			dto.setOptionValue(false);
	       		} else {
	       			dto.setOptionValue(true);
	       		}
	       		dto.setSequence(rs.getInt(5));
	       		
	       		optionsMap.put(new Long(dto.getReportOptionsId()), dto);	       		
	       	}
	       	
	       	return optionsMap;
		}
        catch (SQLException ex)
		{
            // handle any errors
        	throw new Exception("SQL fetching V2 data.  " +
        			"SQLException: " + ex.getMessage() + ", " +
        			"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
        }
        finally
		{
            if (rs != null)
            {
                try
				{
                    rs.close();
                }
                catch (SQLException sqlEx)
				{
                	// swallow the error
                }
                rs = null;
            }
            if (stmt != null)
            {
                try
				{
                    stmt.close();
                }
                catch (SQLException sqlEx)
				{
                	// swallow the error
				}
                stmt = null;
            }
        }
	}

	
	/**
	 * getReportOptionsDataForDnaId
	 * Fetch report options data from v2.  
	 * Adds the info to the instance DTO object, and
	 * updates the array with dna-specific choices, if any
	 *
	 * @param  Connection connection - the database connection
	 * @param  long dnaId -- the dnaId for this candidate's report
	 * @param   HashMap<Long, Setup2ReportOptionValueDTO> optionsMap
	 * @throws Exception
	 */
	private void getReportOptionsDataForDnaId(Connection connection, long dnaId, HashMap<Long, Setup2ReportOptionValueDTO> optionsMap)
		throws Exception
	{
		Statement stmt = null;
		ResultSet rs = null;

        StringBuffer sqlQuery = new StringBuffer();    
        sqlQuery.append("SELECT  d.report_options_id, d.option_value, d.option_string ");
        sqlQuery.append("  FROM pdi_abd_rpt_options_dna d ");
        sqlQuery.append("  WHERE dna_id = " + dnaId + "  " );
   
        try
		{
	       	stmt = connection.createStatement();
	       	rs = stmt.executeQuery(sqlQuery.toString());
          	
	       	if (rs.isBeforeFirst())
	       	{
		       	while(rs.next()){
		       		// if there's data, get the dto from the
		       		// hash map and update it... 	       		
		       		Long optionId = new Long( rs.getInt(1));		       		
		       		Setup2ReportOptionValueDTO dto = optionsMap.get(optionId);
		       		if (dto == null)
		       		{
		       			// option no lo9nger valid... ignore
		       			System.out.println("Option " + optionId + " No longer valid... ignored");
		       			continue;
		       		}
		       		
		       		// update the option value
		       		int customValue = rs.getInt(2);
		       		if (customValue == 0){
		       			dto.setOptionValue(false);
		       		}else{
		       			dto.setOptionValue(true);
		       		}
		       		dto.setOptionString(rs.getString(3));
		       	}
	       	}
		}
        catch (SQLException ex)
		{
            // handle any errors
        	throw new Exception("SQL fetching V2 data.  " +
        			"SQLException: " + ex.getMessage() + ", " +
        			"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
        }
        finally
		{
            if (rs != null)
            {
                try
				{
                    rs.close();
                }
                catch (SQLException sqlEx)
				{
                	// swallow the error
                }
                rs = null;
            }
            if (stmt != null)
            {
                try
				{
                    stmt.close();
                }
                catch (SQLException sqlEx)
				{
                	// swallow the error
				}
                stmt = null;
            }
        }
	}

	
	/**
	 * resetReportOptionsDataForDnaId
	 * reset the report options data from v2
	 * by removing any updated report options from
	 * the database, leaving only the default options.
	 *
	 * @param  Connection connection - the database connection
	 * @param  long dnaId -- the dnaId for this candidate's report
	 * @throws Exception
	 */
	private void resetReportOptionsDataForDnaId(Connection connection, long dnaId)
		throws Exception
	{
		Statement stmt = null;
		ResultSet rs = null;

        StringBuffer sqlQuery = new StringBuffer();    
        sqlQuery.append("DELETE FROM  pdi_abd_rpt_options_dna");
        sqlQuery.append("  WHERE dna_id = " + dnaId + "  " );
   
        try
		{
	       	stmt = connection.createStatement();
	        //int updated = stmt.executeUpdate(sqlQuery.toString());
	        stmt.executeUpdate(sqlQuery.toString());
  		}
        catch (SQLException ex)
		{
            // handle any errors
        	throw new Exception("SQL resetReportOptionsDataForDnaId  " +
        			"SQLException: " + ex.getMessage() + ", " +
        			"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
        }
        finally
		{
            if (rs != null)
            {
                try
				{
                    rs.close();
                }
                catch (SQLException sqlEx)
				{
                	// swallow the error
                }
                rs = null;
            }
            if (stmt != null)
            {
                try
				{
                    stmt.close();
                }
                catch (SQLException sqlEx)
				{
                	// swallow the error
				}
                stmt = null;
            }
        }
	}

	
	/**
	 * buildReturnDTO
	 *
	 * @param long dnaId
	 * @param HashMap<Long, Setup2ReportOptionValueDTO> optionsMap
	 * @return Setup2ReportOptionsDTO object
	 * @throws Exception
	 */
	private Setup2ReportOptionsDTO buildReturnDTO(long dnaId,HashMap<Long, Setup2ReportOptionValueDTO> optionsMap)
//		throws Exception
	{
		// instantiate a return DTO
		Setup2ReportOptionsDTO ret = new Setup2ReportOptionsDTO();

		// create an empty ArrayList as long as the optionsMap
		// have to create an ArrayList of null values
		int mysize = optionsMap.size();
		ArrayList<Setup2ReportOptionValueDTO> valueArray =
					new ArrayList<Setup2ReportOptionValueDTO>();
		for(int i = 0; i< mysize; i++)
		{
			valueArray.add(null);		
		}

		// use the optionsMap hash map to build the sequenced Array List that we'll add to the DTO
		Iterator<Setup2ReportOptionValueDTO> it = optionsMap.values().iterator();

		while (it.hasNext())
		{
			Setup2ReportOptionValueDTO dto = it.next();
			int sequence = dto.getSequence();
			// set the dto's into the correct sequence in the array.
			valueArray.set((sequence-1), dto);
		}
		
		// build the DTO
		ret.setDnaId(dnaId);
		ret.setOptionValuesArray(valueArray);
		
 		return ret;
	}   
}
