/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.dto.reportInput;

/**
 * ReportInputNoteDTO is a wrapper around the notes data for
 * the ReportInput piece of the EvalGuide/IntegrationGrid for
 * A by D.
 * 
 * @author      MB Panichi
 */
public class ReportInputNoteDTO
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private long _inputNoteId = 0;
    private String _noteText = "";   
    /*
     * Note Types:
     * 1 = operations
     * 2 = consultant
     */    
    private int _noteType = 0;  
 
	//
	// Constructors.
	//
	public ReportInputNoteDTO()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//
	
	public String toString()
	{
		return "  ReportInputNoteDTO:  ID=" + this._inputNoteId + ", type=" + this._noteType + ", text=" + this._noteText;
	}
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public long getInputNoteId()
	{
		return _inputNoteId;
	}
		
	public void setInputNoteId(long value)
	{
		_inputNoteId = value;
	}

	/*****************************************************************************************/
	public String getNoteText()
	{
		return _noteText;
	}
		
	public void setNoteText(String value)
	{
		_noteText = value;
	}

	/*****************************************************************************************/
	public int getNoteType()
	{
		return _noteType;
	}
		
	public void setNoteType(int value)
	{
		_noteType = value;
	}
	
}
