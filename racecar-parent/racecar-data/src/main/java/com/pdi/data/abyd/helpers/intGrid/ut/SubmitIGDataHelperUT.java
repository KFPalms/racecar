package com.pdi.data.abyd.helpers.intGrid.ut;

import java.sql.Connection;

import com.pdi.data.abyd.helpers.intGrid.SubmitIGDataHelper;
import com.pdi.ut.UnitTestUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;

import junit.framework.TestCase;

public class SubmitIGDataHelperUT  extends TestCase
{
	//
	// Constructors
	//
	public SubmitIGDataHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
	throws Exception
	{
		junit.textui.TestRunner.run(SubmitIGDataHelperUT.class);
	}


	/*
	 * testSubmitIG
	 */
	public void testSubmitIG()
		throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);
		
		Connection con = AbyDDatabaseUtils.getDBConnection();
		String partId = "9999";
    	long dnaId = 99;

    	SubmitIGDataHelper helper = new SubmitIGDataHelper(con, partId, dnaId);        
		helper.submitIG();
		System.out.println("Complete... check database");

		UnitTestUtils.stop(this);
	}

}
