/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.dto.reportInput;

import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.pdi.data.dto.NoteHolder;
import com.pdi.reporting.report.IndividualReport;

/*
 * REFACTOR THIS! Should be broken down by page at least for data maintenance.
 */
public class ReportInputDTO
{
	// Status ID definitions - exposed (public)
	public static int IN_PROGRESS = 1;
	public static int READY_FOR_EDITING = 2;
	public static int COMPLETED = 3;

	// Custom dashboard flag
	private boolean _showCustomReport = false;
	
	// "Header" data
	private long _inputId;	// The Report Input row ID (primary key)
	private long _lpId = 0;	// Link param ID of the IG with which the DRI is associated
	private long _dnaId;
	private String _dnaName;
	private String _modelName;
	private long _otxtStatusId;
	private long _ptxtStatusId;
	private long _rateStatusId;
	private String _participantId;
	private String _participantName;
	private String _participantNameInverse;
	private String _projectId;
	private String _projectName;
	private String _clientName;
	private String _consultantName;
	private String _adminDate;
	private NoteHolder _partNotes;
	private String _additionalNotes;
	
	// Added special for target
	private String interviewDateStr;
	
//TODO Refactor so that the calls in the participant data helper that do this
//	are exposed to the outside world (and these won't have to be exposed anymore)
	private IndividualReport _currentTLT;
	private IndividualReport _nextTLT;
	
	// Skills to Leverage
	private String _skillsToLeverageOrgText = "";
	private String _skillsToLeveragePartText = "";
	
	// Skills to develop
	private String _skillsToDevelopOrgText = "";
	private String _skillsToDevelopPartText = "";
	
	// Leadership competencies summary
	private String _leadershipSkillOrgText = "";
	private String _leadershipSkillPartText = "";
	private int _leadershipSkillRating = 0;
	private ArrayList<Integer> _leadershipSkillRecommendedRatings = new ArrayList<Integer>();
	
	private int _leadershipSkillIndex = 0;
	
	// Leadership experience summary
	private String _leadershipExperienceOrgText = "";
	private String _leadershipExperiencePartText = "";
	private int _leadershipExperienceRating = 0;
	private ArrayList<Integer> _leadershipExperienceRecommendedRatings = new ArrayList<Integer>();
	
	// Leadership Style and Aptitude
	private String _leadershipStyleOrgText = "";
	private String _leadershipStylePartText = "";
	private int _leadershipStyleRating = 0;
	private ArrayList<Integer> _leadershipStyleRecommendedRatings = new ArrayList<Integer>();
	
	// Leadership Interest
	private String _leadershipInterestOrgText = "";
	private String _leadershipInterestPartText = "";
	private int _leadershipInterestRating;
	private ArrayList<Integer> _leadershipInterestRecommendedRatings = new ArrayList<Integer>();
	
	// Derailment Risk
	private String _derailmentOrgText = "";
	private String _derailmentPartText = "";
	private int _derailmentRating = 0;
	private int _derailmentRecommendedRating = 0;
	
	//Long-term Advancement Potential
	private String _longtermOrgText = "";
	private int _longtermRating = 0;
	private ArrayList<Integer> _longtermRecommendedRatings = new ArrayList<Integer>();
	
	// Fit Summary
	private String _fitOrgText = "";
	private int _candidateFitIndex = 0;	// <-- calculated, not modifiable
	private int _fitRecommendedRating = 0;

	// Readiness Summary
	private String _readinessOrgText = "";
	private int _readinessRating = 0;	// <-- calculated, not modifiable
	private String _readinessFocus = "";
	
	// Development Summary
	private String _developmentPartText = "";
	
	// Pivotal Opportunities
	private String _pivotalPartText = "";
	
	// Individual Development Plan (IDP)
	private String _idpImagePath = "";
	private String _idpDocumentPath = "";
	
	// KFALR stuff
	private boolean _isAlr = false;
	private String _alrReadinessOrgText = "";
	private int _alrReadinessRating = 0;
	private int _alrSugReadinessRating = 0;
	private String _alrReadinessORAuth = null;
	private String _alrReadinessORJust = null;
	private String _alrReadinessFocus = "";
	private String _alrCultureFitOrgText = "";
	private String _alrCultureFitPartText = "";
	private int _alrCultureFitRating = 0;
	private int _alrSugCultureFitRating = 0;
	private String _alrCultureFitORAuth = null;
	private String _alrCultureFitORJust = null;
	
	
	public String toString() {
		String output = "";
		
		// Header data
		output += this.showHeaderString();
		
		// Strings and derived data
		output += "  Skills to Leverage:\n";
		output += "     Org Text=" + _skillsToLeverageOrgText + " \n";
		output += "    Part Text=" + _skillsToLeveragePartText + " \n";
		output += "  Skills to Develop:\n";
		output += "     Org Text=" + _skillsToDevelopOrgText + " \n";
		output += "    Part Text=" + _skillsToDevelopPartText + " \n";
		output += "  Leadership Competencies Summary:\n";
		output += "     Org Text=" + _leadershipSkillOrgText + " \n";
		output += "    Part Text=" + _leadershipSkillPartText + " \n";
		output += "    Skill Rating=" + _leadershipSkillRating + ", Sug/Rec Ratings=";
		for (int i=0; i < _leadershipSkillRecommendedRatings.size(); i++)
		{
			if(i > 0)
				output += ", ";
			output += _leadershipSkillRecommendedRatings.get(i);
		}
		output += "\n";
		output += "    Skill Index=" + _leadershipSkillIndex + "\n";
		output += "  Leadership Experience Summary:\n";
		output += "     Org Text=" + _leadershipExperienceOrgText + " \n";
		output += "    Part Text=" + _leadershipExperiencePartText + " \n";
		output += "    Experience Rating=" + _leadershipExperienceRating + ", Sug/Rec Ratings=";
		for (int i=0; i < _leadershipExperienceRecommendedRatings.size(); i++)
		{
			if(i > 0)
				output += ", ";
			output += _leadershipExperienceRecommendedRatings.get(i);
		}
		output += "\n";
		output += "  Leadership Style and Aptitude:\n";
		output += "     Org Text=" + _leadershipStyleOrgText + " \n";
		output += "    Part Text=" + _leadershipStylePartText + " \n";
		output += "    Style Rating=" + _leadershipStyleRating + ", Sug/Rec Ratings=";
		for (int i=0; i < _leadershipStyleRecommendedRatings.size(); i++)
		{
			if(i > 0)
				output += ", ";
			output += _leadershipStyleRecommendedRatings.get(i);
		}
		output += "\n";
		output += "  Leadership Interest:\n";
		output += "     Org Text=" + _leadershipInterestOrgText + " \n";
		output += "    Part Text=" + _leadershipInterestPartText + " \n";
		output += "    Interest Rating=" + _leadershipInterestRating + ", Sug/Rec Ratings=";
		for (int i=0; i < _leadershipInterestRecommendedRatings.size(); i++)
		{
			if(i > 0)
				output += ", ";
			output += _leadershipInterestRecommendedRatings.get(i);
		}
		output += "\n";
		output += "  Derailment Risk:\n";
		output += "     Org Text=" + _derailmentOrgText + " \n";
		output += "    Part Text=" + _derailmentPartText + " \n";
		output += "    Derailment Rating=" + _derailmentRating +
					", Sug/Rec Rating=" + _derailmentRecommendedRating;
		output += "\n";
		output += "  Long-term Advancement Potential:\n";
		output += "     Org Text=" + _longtermOrgText + " \n";
		output += "    AP Rating=" + _longtermRating + ", Sug/Rec Ratings=";
		for (int i=0; i < _longtermRecommendedRatings.size(); i++)
		{
			if(i > 0)
				output += ", ";
			output += _longtermRecommendedRatings.get(i);
		}
		output += "\n";
		output += "  Fit Summary:\n";
		output += "     Org Text=" + _fitOrgText + " \n";
		output += "    Fit Rating=" + _candidateFitIndex +
				  ", Sug/Rec Rating=" + _fitRecommendedRating;
		output += "\n";
		output += "  Readiness Summary:\n";
		output += "     Org Text=" + _readinessOrgText + " \n";
		output += "    Readiness Rating=" + _readinessRating + "\n";
		output += "    Focus statement =" + _readinessFocus;
		output += "\n";
		output += "  Development Summary:\n";
		output += "     Part Text=" + _developmentPartText + " \n";
		output += "  Pivotal Opportunities:\n";
		output += "     Part Text=" + _pivotalPartText + " \n";
		output += "  IDP Data:\n";
		output += "     Image Path=" + _idpImagePath + ", Doc Path" + _idpDocumentPath;

		// ALR stuff
		output += "\n";
		output += "  ALR Stuff:\n";
		output += "     isAlr=" + _isAlr + " \n";
		output += "     Readiness:\n";
		output += "         Org Text=" + _alrReadinessOrgText + " \n";
		output += "          Rating=" + _alrReadinessRating + "\n";
		output += "          Sug. Rating=" + _alrSugReadinessRating + "\n";
		output += "          Auth=" + _alrReadinessORAuth + "\n";
		output += "          Auth=" + _alrReadinessORJust + "\n";
		output += "      focus text=" + _alrReadinessFocus + "\n";
		output += "     Culture Fit:\n";
		output += "         Org Text=" + _alrCultureFitOrgText + " \n";
		output += "         Org Text=" + _alrCultureFitPartText + " \n";
		output += "          Rating=" + _alrCultureFitRating + "\n";
		output += "          Sug. Rating=" + _alrSugCultureFitRating + "\n";
		output += "          Auth=" + _alrCultureFitORAuth + "\n";
		output += "          Auth=" + _alrCultureFitORJust + "\n";

		return output;
	}
	
	
	public String showHeaderString()
	{
		String output = "";
		
		// Header data
		output += "ID=" + _inputId + ", statusIds: OT=" + _otxtStatusId + ", PT=" + _ptxtStatusId +  ", RR=" + _rateStatusId + "\n"; 
		output += "    Custom Dashboard Report flag = " + _showCustomReport + "\n"; 
		output += "    DNA - ID=" + _dnaId + ", Name=" + _dnaName + ", Model Name=" + _modelName + "\n";
		output += "    Associated IG linkparam ID=" + _lpId + "\n";
		output += "    Part ID=" + _participantId + ", part name=" + _participantName + "\n";
		output += "    projectId=" + _projectId + ", proj name=" + _projectName + ", client name=" + _clientName + "\n";
		output += "    cons name=" + _consultantName +
					", admin date=" + _adminDate + "\n";

		return output;
	}


	/**
	 * compositeRiStatus - Convenience method that returns the imputed DRI status for this instance of the DTO
	 * @return
	 */
	@JsonIgnore
	public long getCompositeStatusId()
	{
		int stat = 0;
		if (this.getOtxtStatusId() == COMPLETED &&
			this.getPtxtStatusId() == COMPLETED &&
			this.getRateStatusId() == COMPLETED)
		{
			// All done
			stat = ReportInputDTO.COMPLETED;
		}
		else if (this.getOtxtStatusId() == IN_PROGRESS &&
				 this.getPtxtStatusId() == IN_PROGRESS &&
				 this.getRateStatusId() == IN_PROGRESS)
		{
			// Nothing done
			stat = IN_PROGRESS;
		}
		else if (this.getOtxtStatusId() == READY_FOR_EDITING ||
				 this.getPtxtStatusId() == READY_FOR_EDITING)
		{
			// R & R should be done before the others can be edited;
			// if one text is in editing set the status to editing
			stat = READY_FOR_EDITING;
		}
		else
		{
			// rate can be done but neither DRI element is in editing
			stat = IN_PROGRESS;
		}
		
		return (long)stat;
	}
	
	
	/*****************************************************************************************/
	public boolean getShowCustomReport()
	{
		return _showCustomReport;
	}

	public void setShowCustomReport(boolean value)
	{
		_showCustomReport = value;
	}	
	
	/*****************************************************************************************/
	public long getInputId()
	{
		return _inputId;
	}

	public void setInputId(long value)
	{
		_inputId = value;
	}	
	
	/*****************************************************************************************/
	public void setLpId(long value)
	{
		_lpId = value;
	}
	
	public long getLpId()
	{
		return _lpId;
	}
	
	/*****************************************************************************************/
	public void setDnaId(long value)
	{
		_dnaId = value;
	}
	
	public long getDnaId()
	{
		return _dnaId;
	}

	/*****************************************************************************************/
	public void setDnaName(String value)
	{
		_dnaName = value;
	}
	
	public String getDnaName()
	{
		return _dnaName;
	}

	/*****************************************************************************************/
	public void setModelName(String value)
	{
		_modelName = value;
	}
	
	public String getModelName()
	{
		return _modelName;
	}

	/*****************************************************************************************/
	public void setOtxtStatusId(long value)
	{
		_otxtStatusId = value;
	}
	
	public long getOtxtStatusId()
	{
		return _otxtStatusId;
	}

	/*****************************************************************************************/
	public void setPtxtStatusId(long value)
	{
		_ptxtStatusId = value;
	}
	
	public long getPtxtStatusId()
	{
		return _ptxtStatusId;
	}

	/*****************************************************************************************/
	public void setRateStatusId(long value)
	{
		_rateStatusId = value;
	}
	
	public long getRateStatusId()
	{
		return _rateStatusId;
	}

	/*****************************************************************************************/
	public void setParticipantId(String value)
	{
		_participantId = value;
	}

	public String getParticipantId()
	{
		return _participantId;
	}

	/*****************************************************************************************/
	public void setParticipantName(String value)
	{
		_participantName = value;
	}

	public String getParticipantName()
	{
		return _participantName;
	}
	
	/*****************************************************************************************/
	public void setProjectId(String value)
	{
		_projectId = value;
	}
	
	public String getProjectId()
	{
		return _projectId;
	}

	/*****************************************************************************************/
	public void setProjectName(String value)
	{
		_projectName = value;
	}
	
	public String getProjectName()
	{
		return _projectName;
	}

	/*****************************************************************************************/
	public void setClientName(String value)
	{
		_clientName = value;
	}
	
	public String getClientName()
	{
		return _clientName;
	}

	/*****************************************************************************************/
	public void setConsultantName(String value)
	{
		_consultantName = value;
	}
	
	public String getConsultantName()
	{
		return _consultantName;
	}

	/*****************************************************************************************/
	public void setAdminDate(String value)
	{
		_adminDate = value;
	}
	
	public String getAdminDate()
	{
		return _adminDate;
	}
	
	/*****************************************************************************************/
	public void setSkillsToLeverageOrgText(String value)
	{
		_skillsToLeverageOrgText = value;
	}
	
	public String getSkillsToLeverageOrgText()
	{
		return _skillsToLeverageOrgText;
	}
	
	/*****************************************************************************************/
	public void setSkillsToLeveragePartText(String value)
	{
		_skillsToLeveragePartText = value;
	}
	
	public String getSkillsToLeveragePartText()
	{
		return _skillsToLeveragePartText;
	}
	
	/*****************************************************************************************/
	public void setSkillsToDevelopOrgText(String value)
	{
		_skillsToDevelopOrgText = value;
	}
	
	public String getSkillsToDevelopOrgText()
	{
		return _skillsToDevelopOrgText;
	}
	
	/*****************************************************************************************/
	public void setSkillsToDevelopPartText(String value)
	{
		_skillsToDevelopPartText = value;
	}
	
	public String getSkillsToDevelopPartText()
	{
		return _skillsToDevelopPartText;
	}
	
	/*****************************************************************************************/
	public void setLeadershipSkillOrgText(String value)
	{
		_leadershipSkillOrgText = value;
	}
	
	public String getLeadershipSkillOrgText()
	{
		return _leadershipSkillOrgText;
	}
	
	/*****************************************************************************************/
	public void setLeadershipSkillPartText(String value)
	{
		_leadershipSkillPartText = value;
	}
	public String getLeadershipSkillPartText()
	{
		return _leadershipSkillPartText;
	}
	
	/*****************************************************************************************/
	public void setLeadershipSkillRating(int value)
	{
		_leadershipSkillRating = value;
	}
	public int getLeadershipSkillRating()
	{
		return _leadershipSkillRating;
	}
	
	/*****************************************************************************************/
	public void setLeadershipSkillRecommendedRatings(ArrayList<Integer> value)
	{
		_leadershipSkillRecommendedRatings = value;
	}

	public ArrayList<Integer> getLeadershipSkillRecommendedRatings()
	{
		return _leadershipSkillRecommendedRatings;
	}
	
	/*****************************************************************************************/
	public void setLeadershipSkillIndex(int value)
	{
		_leadershipSkillIndex = value;
	}

	public int getLeadershipSkillIndex()
	{
		return _leadershipSkillIndex;
	}
	
	/*****************************************************************************************/
	public void setLeadershipExperienceOrgText(String value)
	{
		_leadershipExperienceOrgText = value;
	}
	
	public String getLeadershipExperienceOrgText()
	{
		return _leadershipExperienceOrgText;
	}
	
	/*****************************************************************************************/
	public void setLeadershipExperiencePartText(String value)
	{
		_leadershipExperiencePartText = value;
	}
	
	public String getLeadershipExperiencePartText()
	{
		return _leadershipExperiencePartText;
	}
	
	/*****************************************************************************************/
	public void setLeadershipExperienceRating(int value)
	{
		_leadershipExperienceRating = value;
	}
	
	public int getLeadershipExperienceRating()
	{
		return _leadershipExperienceRating;
	}
	
	/*****************************************************************************************/
	public void setLeadershipExperienceRecommendedRatings(ArrayList<Integer> value)
	{
		_leadershipExperienceRecommendedRatings = value;
	}

	public ArrayList<Integer> getLeadershipExperienceRecommendedRatings()
	{
		return _leadershipExperienceRecommendedRatings;
	}
	
	/*****************************************************************************************/
	public void setLeadershipStyleOrgText(String value)
	{
		_leadershipStyleOrgText = value;
	}
	
	public String getLeadershipStyleOrgText()
	{
		return _leadershipStyleOrgText;
	}
	
	/*****************************************************************************************/
	public void setLeadershipStylePartText(String value)
	{
		_leadershipStylePartText = value;
	}
	
	public String getLeadershipStylePartText()
	{
		return _leadershipStylePartText;
	}
	
	/*****************************************************************************************/
	public void setLeadershipStyleRating(int value)
	{
		_leadershipStyleRating = value;
	}
	
	public int getLeadershipStyleRating()
	{
		return _leadershipStyleRating;
	}
	
	/*****************************************************************************************/
	public void setLeadershipStyleRecommendedRatings(ArrayList<Integer> value)
	{
		_leadershipStyleRecommendedRatings = value;
	}
	
	public ArrayList<Integer> getLeadershipStyleRecommendedRatings()
	{
		return _leadershipStyleRecommendedRatings;
	}
	
	/*****************************************************************************************/
	public void setLeadershipInterestOrgText(String value)
	{
		_leadershipInterestOrgText = value;
	}
	
	public String getLeadershipInterestOrgText()
	{
		return _leadershipInterestOrgText;
	}

	public void setLeadershipInterestPartText(String value)
	{
		_leadershipInterestPartText = value;
	}
	
	public String getLeadershipInterestPartText()
	{
		return _leadershipInterestPartText;
	}
	
	/*****************************************************************************************/
	public void setLeadershipInterestRating(int value)
	{
		_leadershipInterestRating = value;
	}
	
	public int getLeadershipInterestRating()
	{
		return _leadershipInterestRating;
	}
	
	/*****************************************************************************************/
	public void setLeadershipInterestRecommendedRatings(ArrayList<Integer> value)
	{
		_leadershipInterestRecommendedRatings = value;
	}

	public ArrayList<Integer> getLeadershipInterestRecommendedRatings()
	{
		return _leadershipInterestRecommendedRatings;
	}
	
	/*****************************************************************************************/
	public void setDerailmentOrgText(String value)
	{
		_derailmentOrgText = value;
	}
	
	public String getDerailmentOrgText()
	{
		return _derailmentOrgText;
	}
	
	/*****************************************************************************************/
	public void setDerailmentPartText(String value)
	{
		_derailmentPartText = value;
	}
	
	public String getDerailmentPartText()
	{
		return _derailmentPartText;
	}
	
	/*****************************************************************************************/
	public void setDerailmentRating(int value)
	{
		_derailmentRating = value;
	}
	public int getDerailmentRating()
	{
		return _derailmentRating;
	}
	
	/*****************************************************************************************/
	public void setDerailmentRecommendedRating(int value)
	{
		_derailmentRecommendedRating = value;
	}

	public int getDerailmentRecommendedRating()
	{
		return _derailmentRecommendedRating;
	}
	
	/*****************************************************************************************/
	public void setLongtermOrgText(String value)
	{
		_longtermOrgText = value;
	}
	
	public String getLongtermOrgText()
	{
		return _longtermOrgText;
	}
	
	/*****************************************************************************************/
	public void setLongtermRating(int value)
	{
		_longtermRating = value;
	}
	
	public int getLongtermRating()
	{
		return _longtermRating;
	}
	
	/*****************************************************************************************/
	public void setlongtermRecommendedRatings(ArrayList<Integer> value)
	{
		_longtermRecommendedRatings = value;
	}

	public ArrayList<Integer> getLongtermRecommendedRatings()
	{
		return _longtermRecommendedRatings;
	}
	
	/*****************************************************************************************/
	public void setFitOrgText(String value)
	{
		_fitOrgText = value;
	}
	
	public String getFitOrgText()
	{
		return _fitOrgText;
	}
	
	/*****************************************************************************************/
	public void setCandidateFitIndex(int value)
	{
		_candidateFitIndex = value;
	}
	
	public int getCandidateFitIndex()
	{
		return _candidateFitIndex;
	}
	
	/*****************************************************************************************/
	public void setFitRecommendedRating(int value)
	{
		_fitRecommendedRating = value;
	}
	
	public int getFitRecommendedRating()
	{
		return _fitRecommendedRating;
	}
	
	/*****************************************************************************************/
	public void setReadinessOrgText(String value)
	{
		_readinessOrgText = value;
	}
	
	public String getReadinessOrgText()
	{
		return _readinessOrgText;
	}
	
	/*****************************************************************************************/
	public void setReadinessRating(int value)
	{
		_readinessRating = value;
	}
	
	public int getReadinessRating()
	{
		return _readinessRating;
	}
	
	/*****************************************************************************************/
	public void setReadinessFocus(String value)
	{
		_readinessFocus = value;
	}
	
	public String getReadinessFocus()
	{
		return _readinessFocus;
	}
	
	/*****************************************************************************************/
	public void setDevelopmentPartText(String value)
	{
		_developmentPartText = value;
	}

	public String getDevelopmentPartText()
	{
		return _developmentPartText;
	}

	/*****************************************************************************************/
	public void setPivotalPartText(String value)
	{
		_pivotalPartText = value;
	}

	public String getPivotalPartText()
	{
		return _pivotalPartText;
	}
	
	/*****************************************************************************************/
	public void setIdpImagePath(String value)
	{
		_idpImagePath = value;
	}
	
	public String getIdpImagePath()
	{
		return _idpImagePath;
	}
	
	/*****************************************************************************************/
	public void setIdpDocumentPath(String value)
	{
		_idpDocumentPath = value;
	}
	
	public String getIdpDocumentPath()
	{
		return _idpDocumentPath;
	}

	/*****************************************************************************************/
	public void setCurrentTLT(IndividualReport value)
	{
		_currentTLT = value;
	}
	
	public IndividualReport getCurrentTLT()
	{
		return _currentTLT;
	}

	/*****************************************************************************************/
	public void setNextTLT(IndividualReport value)
	{
		_nextTLT = value;
	}
	
	public IndividualReport getNextTLT()
	{
		return _nextTLT;
	}

	/*****************************************************************************************/
	public void setParticipantNameInverse(String _participantNameInverse) {
		this._participantNameInverse = _participantNameInverse;
	}

	public String getParticipantNameInverse() {
		return _participantNameInverse;
	}

	/*****************************************************************************************/

	public void setPartNotes(NoteHolder value)
	{
		_partNotes = value;
	}

	public NoteHolder getPartNotes()
	{
		return _partNotes;
	}

	/*****************************************************************************************/

	public void setAdditionalNotes(String value) {
		_additionalNotes = value;
	}
	public String getAdditionalNotes() {
		return _additionalNotes;
	}

	/*****************************************************************************************/

	public String getInterviewDateStr()
	{
		return this.interviewDateStr;
	}


	public void setInterviewDateStr(String interviewDateStr)
	{
		this.interviewDateStr = interviewDateStr;
	}

	/*****************************************************************************************/

	public boolean getIsAlr()
	{
		return this._isAlr;
	}


	public void setIsAlr(boolean value)
	{
		this._isAlr = value;
	}	

	/*****************************************************************************************/

	public String getAlrReadinessOrgText()
	{
		return this._alrReadinessOrgText;
	}


	public void setAlrReadinessOrgText(String value)
	{
		this._alrReadinessOrgText = value;
	}

	/*****************************************************************************************/

	public int getAlrReadinessRating()
	{
		return this._alrReadinessRating;
	}


	public void setAlrReadinessRating(int value)
	{
		this._alrReadinessRating = value;
	}

	/*****************************************************************************************/

	public int getAlrSugReadinessRating()
	{
		return this._alrSugReadinessRating;
	}


	public void setAlrSugReadinessRating(int value)
	{
		this._alrSugReadinessRating = value;
	}

	/*****************************************************************************************/

	public String getAlrReadinessORAuth()
	{
		return this._alrReadinessORAuth;
	}


	public void setAlrReadinessORAuth(String value)
	{
		this._alrReadinessORAuth = value;
	}

	/*****************************************************************************************/

	public String getAlrReadinessORJust()
	{
		return this._alrReadinessORJust;
	}


	public void setAlrReadinessORJust(String value)
	{
		this._alrReadinessORJust = value;
	}

	/*****************************************************************************************/

	public String getAlrReadinessFocus()
	{
		return this._alrReadinessFocus;
	}


	public void setAlrReadinessFocus(String value)
	{
		this._alrReadinessFocus = value;
	}

	/*****************************************************************************************/

	public String getAlrCultureFitOrgText()
	{
		return this._alrCultureFitOrgText;
	}


	public void setAlrCultureFitOrgText(String value)
	{
		this._alrCultureFitOrgText = value;
	}

	/*****************************************************************************************/

	public String getAlrCultureFitPartText()
	{
		return this._alrCultureFitPartText;
	}


	public void setAlrCultureFitPartText(String value)
	{
		this._alrCultureFitPartText = value;
	}

	/*****************************************************************************************/

	public int getAlrCultureFitRating()
	{
		return this._alrCultureFitRating;
	}


	public void setAlrCultureFitRating(int value)
	{
		this._alrCultureFitRating = value;
	}

	/*****************************************************************************************/

	public int getAlrSugCultureFitRating()
	{
		return this._alrSugCultureFitRating;
	}


	public void setAlrSugCultureFitRating(int value)
	{
		this._alrSugCultureFitRating = value;
	}

	/*****************************************************************************************/

	public String getAlrCultureFitORAuth()
	{
		return this._alrCultureFitORAuth;
	}


	public void setAlrCultureFitORAuth(String value)
	{
		this._alrCultureFitORAuth = value;
	}

	/*****************************************************************************************/

	public String getAlrCultureFitORJust()
	{
		return this._alrCultureFitORJust;
	}


	public void setAlrCultureFitORJust(String value)
	{
		this._alrCultureFitORJust = value;
	}
}
