/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.constants;

/**
 * V2InstrumentConstants contains the V2 module IDs for the
 * Raven's Form B, the Watson-Glazer Form E, and the
 * CHQ instruments.
 * Note that if we 'languageify" we may have to add more constants
 * (V2 has one id per module instance and each language is a new instance)
 */
public class V2InstrumentConstants
{
	public static final String RAVENS_B_V2_ID = "HNZYMHDV";
	public static final String WATSON_E_V2_ID = "FFJUUSBA";	// Called WG II Form B in the DB
	public static final String CHQ_V2_ID = "LIBPMLBJ";
	public static final String CS_ID = "FFGLQDEG";
}
