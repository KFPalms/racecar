/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.dto.intGrid;

/**
 * IGExtCogData is a thin class to hold certain information about
 * a single cognitive test for the Integration Grid Extract.
 * 
 * @author		Ken Beukelman
 */
public class IGExtCogData
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _cogName;
	private String _cogRating;
	private String _cogPctl;
	private String _cogNormName;

	//
	// Constructors.
	//
	public IGExtCogData()
	{
		// does nothing right now
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public String getCogName()
	{
		return _cogName;
	}
	
	public void setCogName(String value)
	{
		_cogName = value;
	}	

	/*****************************************************************************************/
	public String getCogRating()
	{
		return _cogRating;
	}
	
	public void setCogRating(String value)
	{
		_cogRating = value;
	}	

	/*****************************************************************************************/
	public String getCogPctl()
	{
		return _cogPctl;
	}
	
	public void setCogPctl(String value)
	{
		_cogPctl = value;
	}	

	/*****************************************************************************************/
	public String getCogNormName()
	{
		return _cogNormName;
	}
	
	public void setCogNormName(String value)
	{
		_cogNormName = value;
	}	



	/*
	 * Show me what you've got
	 */
	public String toString()
	{
		String ret = "IGExtCogData content (Java)\n";
		ret += "        Cog Name=" + _cogName + "\n";
		ret += "      Cog Rating=" + _cogRating + "\n";
		ret += "  Cog Percentile=" + _cogPctl + "\n";
		ret += "       Norm Name=" + _cogNormName + "\n";

		return ret;
	}
}
