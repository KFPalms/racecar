/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v2.ut;

import java.util.ArrayList;

import com.pdi.data.util.RequestStatus;
import com.pdi.data.v2.helpers.ExternalResultsDataHelper;
import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;


public class ExternalResultsDataHelperUT  extends TestCase
{

	//
	//		GPI_SCORES
	//
	private static final String[][] GPI_SCORES = new String[][]
	{
		(new String[] { "GPI_AD",   "5.50"}),
		(new String[] { "GPI_ADPT", "4.25"}),
		(new String[] { "GPI_AST",  "3.25"}),
		(new String[] { "GPI_COMP", "4.25"}),
		(new String[] { "GPI_CONS", "5.75"}),
		(new String[] { "GPI_DACH", "5.00"}),
		(new String[] { "GPI_DADV", "4.00"}),
		(new String[] { "GPI_DUT",  "3.50"}),
		(new String[] { "GPI_EGO",  "4.25"}),
		(new String[] { "GPI_EC",   "4.00"}),
		(new String[] { "GPI_EL",   "5.25"}),
		(new String[] { "GPI_EMP",  "5.50"}),
		(new String[] { "GPI_IMPR", "4.00"}),
		(new String[] { "GPI_IND",  "4.50"}),
		(new String[] { "GPI_INFL", "5.25"}),
		(new String[] { "GPI_INIT", "6.00"}),
		(new String[] { "GPI_INOV", "4.00"}),
		(new String[] { "GPI_INTD", "3.50"}),
		(new String[] { "GPI_INTI", "3.00"}),
		(new String[] { "GPI_MAN",  "6.25"}),
		(new String[] { "GPI_MIC",  "2.75"}),
		(new String[] { "GPI_NA",   "4.00"}),
		(new String[] { "GPI_OPEN", "4.50"}),
		(new String[] { "GPI_OPT",  "3.75"}),
		(new String[] { "GPI_PA",   "3.75"}),
		(new String[] { "GPI_RESP", "3.00"}),
		(new String[] { "GPI_RISK", "5.75"}),
		(new String[] { "GPI_SASI", "5.75"}),
		(new String[] { "GPI_SC",   "4.00"}),
		(new String[] { "GPI_SO",  " 6.00"}),
		(new String[] { "GPI_ST",   "3.50"}),
		(new String[] { "GPI_TA",   "5.25"}),
		(new String[] { "GPI_TC",   "6.50"}),
		(new String[] { "GPI_TF",   "4.50"}),
		(new String[] { "GPI_TR",   "3.75"}),
		(new String[] { "GPI_WF",   "6.25"}),
		(new String[] { "GPI_VIS",  "4.50"}),
		(new String[] { "PGPI_RDI", "99"})
	};


	//
	//		LEI_SCORES
	//
	private static final String[][] LEI_SCORES = new String[][]
	{
		// Regular scales
		(new String[] { "LEI3_BDV",     "69.525"}),
		(new String[] { "LEI3_BGR",     "67.325"}),
		(new String[] { "LEI3_CMT",     "51.625"}),
		(new String[] { "LEI3_DEV",     "53.9"}),
		(new String[] { "LEI3_EXT",     "56.55"}),
		(new String[] { "LEI3_FAL",     "51.675"}),
		(new String[] { "LEI3_FEX",     "40.45"}),
		(new String[] { "LEI3_FIN",     "93.075"}),
		(new String[] { "LEI3_FM",      "109.3"}),
		(new String[] { "LEI3_HR",      "90.725"}),
		(new String[] { "LEI3_INN",     "65.225"}),
		(new String[] { "LEI3_NEG",     "155.225"}),
		(new String[] { "LEI3_OAL",     "2050.25"}),
		(new String[] { "LEI3_OPX",     "87.125"}),
		(new String[] { "LEI3_PDT",     "29.975"}),
		(new String[] { "LEI3_PGT",     "137.4"}),
		(new String[] { "LEI3_PPL",     "208.9"}),
		(new String[] { "LEI3_PRB",     "53.675"}),
		(new String[] { "LEI3_RPR",     "130.1"}),
		(new String[] { "LEI3_SDV",     "74.2"}),
		(new String[] { "LEI3_SGY",     "108.3"}),
		(new String[] { "LEI3_STA",     "36.075"}),
		(new String[] { "LEI3_STF",     "92.525"}),
		(new String[] { "LEI3_VAS",     "187.375"}),
		// Work Orientation
		(new String[] { "LEI_WO_AVOID", "2.5"}),
		(new String[] { "LEI_WO_LEARN", "2.2"}),
		(new String[] { "LEI_WO_PROVE", "2.0"}),
		// Super Scales
		(new String[] { "LEI3_CHA",   "499.85"}),
		(new String[] { "LEI3_GMT",   "815.575"}),
		(new String[] { "LEI3_PCR",   "249.875"}),
		(new String[] { "LEI3_RC",    "484.95"}),
		// Dev Sugg Scale Standardization
		(new String[] { "LEI_DS_BDV",   "-0.17961045"}),
		(new String[] { "LEI_DS_BGR",   "-0.38190943"}),
		(new String[] { "LEI_DS_CMT",   "-2.0482512"}),
		(new String[] { "LEI_DS_DEV",   "-2.3578002"}),
		(new String[] { "LEI_DS_EXT",   "0.924441"}),
		(new String[] { "LEI_DS_FAL",   "-0.7503453"}),
		(new String[] { "LEI_DS_FEX",   "-2.5599597"}),
		(new String[] { "LEI_DS_FIN",   "-0.5238144"}),
		(new String[] { "LEI_DS_FM",    "-0.47135684"}),
		(new String[] { "LEI_DS_HR",    "-1.4575866"}),
		(new String[] { "LEI_DS_INN",   "0.72982633"}),
		(new String[] { "LEI_DS_NEG",   "0.06705717"}),
		(new String[] { "LEI_DS_OAL",   "-0.7557097"}),
		(new String[] { "LEI_DS_OPX",   "-0.96185356"}),
		(new String[] { "LEI_DS_PDT",   "-0.54882425"}),
		(new String[] { "LEI_DS_PGT",   "-0.1230591"}),
		(new String[] { "LEI_DS_PPL",   "-0.70161796"}),
		(new String[] { "LEI_DS_PRB",   "-2.150941"}),
		(new String[] { "LEI_DS_RPR",   "-0.06505605"}),
		(new String[] { "LEI_DS_SDV",   "-1.7362006"}),
		(new String[] { "LEI_DS_SGY",   "-0.32582602"}),
		(new String[] { "LEI_DS_STA",   "-0.35317743"}),
		(new String[] { "LEI_DS_STF",   "-1.0435572"}),
		(new String[] { "LEI_DS_VAS",   "-0.090408295"})
	};

	//
	//		CS_SCORES
	//
	private static final String[][] CS_SCORES = new String[][]
	{
		// Regular scales
		(new String[] { "CAREER_DRIVERS",    "19.812566951532048"}),
		(new String[] { "CAREER_GOALS",      "11.017235911122434"}),
		(new String[] { "EXPERIENCE_ORIENT", "7.56395602103121"}),
		(new String[] { "LEARNING_ORIENT",   "8.614133602487419"})
    };

	//
	//		FINEX_SCORES
	//
	private static final String[][] FINEX_SCORES = new String[][]
	{
		(new String[] { "FE_CORRECT",	"2"}),
		(new String[] { "FE_DAM",		"3"}),
		(new String[] { "FE_AFC",		"1"}),
		(new String[] { "FE_DAC",		"2"}),
		(new String[] { "FE_MPD",		"1"}),
	};

	//
	//		CHQ_ANSWERS
	//
	private static final String[][] CHQ_ANSWERS = new String[][]
	{

		(new String[] { "Q1",       "Ben Skywalker"}),
		(new String[] { "Q2",       "jedi knights inc"}),
		(new String[] { "Q3",       "jedi knight"}),
		(new String[] { "Q4",       "coruscant"}),
		(new String[] { "Q5",       "imperial city"}),
		(new String[] { "Q6",       "mn"}),
		(new String[] { "Q7",       "usa"}),
		(new String[] { "Q8",       "12345"}),
		(new String[] { "Q9",       "hpcc"}),
		(new String[] { "Q10",      "hp"}),
		(new String[] { "Q11",      "wfcc"}),
		(new String[] { "Q12",      "wp"}),
		(new String[] { "Q13",      "mpcc"}),
		(new String[] { "Q14",      "mp"}),
		(new String[] { "Q15",      "benS@jedi.com"}),
		(new String[] { "Q16",      "my dad"}),
		(new String[] { "Q17",      "manphncc"}),
		(new String[] { "Q18",      "manphno"}),
		(new String[] { "Q19",      "luke@jedi.com"}),
		(new String[] { "Q20",      "9"}),
		(new String[] { "Q21_1_1",  "edua"}),
		(new String[] { "Q21_1_2",  "edub"}),
		(new String[] { "Q21_1_3",  "educ"}),
		(new String[] { "Q21_1_4",  "edud"}),
		(new String[] { "Q21_1_5",  "edue"}),
		(new String[] { "Q21_1_6",  "eduf"}),
		(new String[] { "Q21_2_1",  "edug"}),
		(new String[] { "Q21_2_2",  "eduh"}),
		(new String[] { "Q21_2_3",  "edui"}),
		(new String[] { "Q21_2_4",  "eduj"}),
		(new String[] { "Q21_2_5",  "eduk"}),
		(new String[] { "Q21_2_6",  "edul"}),
		(new String[] { "Q21_3_1",  "edum"}),
		(new String[] { "Q21_3_2",  "edun"}),
		(new String[] { "Q21_3_3",  "eduo"}),
		(new String[] { "Q21_3_4",  "edup"}),
		(new String[] { "Q21_3_5",  "eduq"}),
		(new String[] { "Q21_3_6",  "edur"}),
		(new String[] { "Q21_4_1",  "edus"}),
		(new String[] { "Q21_4_2",  "edut"}),
		(new String[] { "Q21_4_3",  "eduu"}),
		(new String[] { "Q21_4_4",  "eduv"}),
		(new String[] { "Q21_4_5",  "eduw"}),
		(new String[] { "Q21_4_6",  "edux"}),
		(new String[] { "Q21_5_1",  "eduy"}),
		(new String[] { "Q21_5_2",  "eduz"}),
		(new String[] { "Q21_5_3",  "eduaa"}),
		(new String[] { "Q21_5_4",  "edubb"}),
		(new String[] { "Q21_5_5",  "educc"}),
		(new String[] { "Q21_5_6",  "edudd"}),
		(new String[] { "Q22",      "cuz my parents made me"}),
		(new String[] { "Q23",      "2887"}),
		(new String[] { "Q23_2",    ""}),
		(new String[] { "Q24",      "12"}),
		(new String[] { "Q25",      "2881"}),
		(new String[] { "Q25_2",    ""}),
		(new String[] { "Q26",      "2960"}),
		(new String[] { "Q26_2",    ""}),
		(new String[] { "Q27",      "2989|999"}),
		(new String[] { "Q27_2",    "fighting the good fight"}),
		(new String[] { "Q28",      "2787"}),
		(new String[] { "Q29",      "11"}),
		(new String[] { "Q30",      "12"}),
		(new String[] { "Q31",      "12"}),
		(new String[] { "Q32",      "2795"}),
		(new String[] { "Q33",      "2803"}),
		(new String[] { "Q33_2",    ""}),
		(new String[] { "Q34",      "2809"}),
		(new String[] { "Q34_2",    ""}),
		(new String[] { "Q35",      "123444"}),
		(new String[] { "Q36",      "18|3307"}),
		(new String[] { "Q36_01_1", "a"}),
		(new String[] { "Q36_01_2", "b"}),
		(new String[] { "Q36_01_3", "c"}),
		(new String[] { "Q36_01_4", "d"}),
		(new String[] { "Q36_01_5", "e"}),
		(new String[] { "Q36_01_6", "f"}),
		(new String[] { "Q36_02_1", "g"}),
		(new String[] { "Q36_02_2", "h"}),
		(new String[] { "Q36_02_3", "i"}),
		(new String[] { "Q36_02_4", "j"}),
		(new String[] { "Q36_02_5", "k"}),
		(new String[] { "Q36_02_6", "l"}),
		(new String[] { "Q36_03_1", "m"}),
		(new String[] { "Q36_03_2", "n"}),
		(new String[] { "Q36_03_3", "o"}),
		(new String[] { "Q36_03_4", "p"}),
		(new String[] { "Q36_03_5", "q"}),
		(new String[] { "Q36_03_6", "r"}),
		(new String[] { "Q36_04_1", "s"}),
		(new String[] { "Q36_04_2", "t"}),
		(new String[] { "Q36_04_3", "u"}),
		(new String[] { "Q36_04_4", "v"}),
		(new String[] { "Q36_04_5", "w"}),
		(new String[] { "Q36_04_6", "x"}),
		(new String[] { "Q36_05_1", "y"}),
		(new String[] { "Q36_05_2", "z"}),
		(new String[] { "Q36_05_3", "aa"}),
		(new String[] { "Q36_05_4", "bb"}),
		(new String[] { "Q36_05_5", "cc"}),
		(new String[] { "Q36_05_6", "dd"}),
		(new String[] { "Q36_06_1", "ee"}),
		(new String[] { "Q36_06_2", "ff"}),
		(new String[] { "Q36_06_3", "gg"}),
		(new String[] { "Q36_06_4", "hh"}),
		(new String[] { "Q36_06_5", "ii"}),
		(new String[] { "Q36_06_6", "jj"}),
		(new String[] { "Q36_07_1", "kk"}),
		(new String[] { "Q36_07_2", "ll"}),
		(new String[] { "Q36_07_3", "mm"}),
		(new String[] { "Q36_07_4", "nn"}),
		(new String[] { "Q36_07_5", "oo"}),
		(new String[] { "Q36_07_6", "pp"}),
		(new String[] { "Q36_08_1", "qq"}),
		(new String[] { "Q36_08_2", "rr"}),
		(new String[] { "Q36_08_3", "ss"}),
		(new String[] { "Q36_08_4", "tt"}),
		(new String[] { "Q36_08_5", "uu"}),
		(new String[] { "Q36_08_6", "vv"}),
		(new String[] { "Q36_09_1", "ww"}),
		(new String[] { "Q36_09_2", "xx"}),
		(new String[] { "Q36_09_3", "yy"}),
		(new String[] { "Q36_09_4", "zz"}),
		(new String[] { "Q36_09_5", "aaa"}),
		(new String[] { "Q36_09_6", "bbb"}),
		(new String[] { "Q36_10_1", "ccc"}),
		(new String[] { "Q36_10_2", "ddd"}),
		(new String[] { "Q36_10_3", "eee"}),
		(new String[] { "Q36_10_4", "fff"}),
		(new String[] { "Q36_10_5", "ggg"}),
		(new String[] { "Q36_10_6", "hhh"}),
		(new String[] { "Q37",      "blah blah blah"}),
		(new String[] { "Q38",      "59"}),
		(new String[] { "Q39",      "address the challenge"}),
		(new String[] { "Q40",      "accomplishment"}),
		(new String[] { "Q41",      "perform effectively"}),
		(new String[] { "Q42",      "i did it all right"}),
		(new String[] { "Q43",      "saving lives"}),
		(new String[] { "Q44",      "killing people"}),
		(new String[] { "Q45",      "it was my destiny"}),
		(new String[] { "Q46",      "squad leader"}),
		(new String[] { "Q47",      "padawan to knight"}),
		(new String[] { "Q48",      "none, really"}),
		(new String[] { "Q49",      "anythign involving boring stuff"}),
		(new String[] { "Q50",      "defeating the empire"}),
		(new String[] { "Q51",      "defeating the bad guys"}),
		(new String[] { "Q52",      "2817|999"}),
		(new String[] { "Q52_2",    "oh, all kinds of stuff"}),
		(new String[] { "Q53",      "valueable experiences"}),
		(new String[] { "Q54",      "rebellion monthly"}),
		(new String[] { "Q55",      "not really interested"}),
		(new String[] { "Q56",      "12"}),
		(new String[] { "Q57",      "if yes, here"}),
		(new String[] { "Q58",      "if no, there"}),
		(new String[] { "Q59",      "more excitement"}),
		(new String[] { "Q60",      "keeping alive"}),
		(new String[] { "Q61",      "still being alive and unharmed"}),
		(new String[] { "Q62",      "probably those meddling kids"}),
		(new String[] { "Q63",      "darth vader"}),
		(new String[] { "Q64",      "better training in telekinesis"}),
		(new String[] { "Q65",      "good at jedi meditation"}),
		(new String[] { "Q66",      "calm, you must remain calm"}),
		(new String[] { "Q67",      "i'm not like my father"}),
		(new String[] { "Q68",      "braver, stronger, faster"}),
		(new String[] { "Q69",      "killing the bad guys, saving the good guys"}),
		(new String[] { "Q70",      "stupid humans"}),
		(new String[] { "Q71",      "1|2|3|5|4||||||||||||||"}),
		(new String[] { "Q72",      "OH, this and that...."}),
		(new String[] { "Q73",      "asdf"}),
		(new String[] { "Q74",      "asdf"}),
		(new String[] { "Q75",      "asdf"}),
		(new String[] { "Q76",      "5"}),
		(new String[] { "Q77",      "2826"}),
		(new String[] { "Q78",      "2881"}),
		(new String[] { "Q78_2",    ""}),
		(new String[] { "Q79",      "2881"}),
		(new String[] { "Q79_2",    ""}),
		(new String[] { "Q80",      "2856"}),
		(new String[] { "Q80_2",    ""}),
		(new String[] { "Q81",      "1234"}),
		(new String[] { "Q82",      "01|1|1942"}),
		(new String[] { "Q83",      "20"}),
		(new String[] { "Q84",      "3016"}),
		(new String[] { "Q84_2",    ""})
	};
	
	
	
	public ExternalResultsDataHelperUT(String name)
	{
		super(name);
	}

	
	public void testSaveAdaptData() 
		throws Exception
	{
//    	if(true) {
//    		return;
//    	}
		
		UnitTestUtils.start(this);
		V2DatabaseUtils.setUnitTest(true);
		
		/*
		 * Build an IndividualReport data object
		 */
		IndividualReport ir = new IndividualReport();
		
		// ---- GPI score data ----
		ReportData gpiRd = new ReportData();
		for (int i=0; i < GPI_SCORES.length; i++)
		{
			gpiRd.getScoreData().put(GPI_SCORES[i][0], GPI_SCORES[i][1]);
		}
		ir.getReportData().put(ReportingConstants.RC_GPI, gpiRd);
		
		// ---- LEI score data ----
		ReportData leiRd = new ReportData();
		for (int i=0; i < LEI_SCORES.length; i++)
		{
			leiRd.getScoreData().put(LEI_SCORES[i][0], LEI_SCORES[i][1]);
		}
		ir.getReportData().put(ReportingConstants.RC_LEI, leiRd);
		
		// ---- FinEx score data ----
		ReportData finExRd = new ReportData();
		for (int i=0; i < FINEX_SCORES.length; i++)
		{
			finExRd.getScoreData().put(FINEX_SCORES[i][0], FINEX_SCORES[i][1]);
		}
		ir.getReportData().put(ReportingConstants.RC_FIN_EX, finExRd);
		
		// ---- CHQ answer data ----
		ReportData chqRd = new ReportData();
		for (int i=0; i < CHQ_ANSWERS.length; i++)
		{
			chqRd.getRawData().put(CHQ_ANSWERS[i][0], CHQ_ANSWERS[i][1]);
		}
		ir.getReportData().put(ReportingConstants.RC_CHQ, chqRd);
		
		String jobId;
		jobId = "IYKNYCNU";	// AbyD Testing Components Template
		ArrayList<String> mods = new ArrayList<String>();
		mods.add(ReportingConstants.RC_GPI);
		mods.add(ReportingConstants.RC_LEI);
		mods.add(ReportingConstants.RC_FIN_EX);
		mods.add(ReportingConstants.RC_CHQ);
		System.out.println("The following modules were selected to save:");
		for (String str : mods)
		{
			System.out.println("  " + str);
		}

		//RequestStatus rs = new ExternalResultsDataHelper().saveAdaptData(jobId, "FAKECAND", ir);
		ArrayList<String> lst  = new ExternalResultsDataHelper().saveAdaptData(jobId,
																			   "FAKECAND",
																			   mods,
																			   ir);
		if (lst.size() < 1)
		{
			System.out.println("Nothing saved from ADAPT");
		}
		else
		{
			System.out.println("The following modules were saved:");
			for (String str : lst)
			{
				System.out.println("  " + str);
			}
		}
//		System.out.println("Result---" + rs.toString());
//		assertEquals("eCode=" + rs.getExceptionCode() + ", eMsg=" + rs.getExceptionMessage() + ", status:",
//				RequestStatus.RS_OK, rs.getStatusCode());
			
			UnitTestUtils.stop(this);
	}	

	
	public void testSaveCheckboxData() 
		throws Exception
	{
//    	if(true) {
//    		return;
//    	}
		
		UnitTestUtils.start(this);
		V2DatabaseUtils.setUnitTest(true);
		
		
		// ---- CS score data ----
		IndividualReport ir = new IndividualReport();
		ReportData csRd = new ReportData();
		for (int i=0; i < CS_SCORES.length; i++)
		{
			csRd.getScoreData().put(CS_SCORES[i][0], CS_SCORES[i][1]);
		}
		ir.getReportData().put(ReportingConstants.RC_CAREER_SURVEY, csRd);
		
		String jobId;
		jobId = "IYKNYCNU";	// AbyD Testing Components Template

		RequestStatus rs = new ExternalResultsDataHelper().saveCheckboxData(jobId, "FAKECAND", ir);
		System.out.println("Result---" + rs.toString());
		//assertEquals("eCode=" + rs.getExceptionCode() + ", eMsg=" + rs.getExceptionMessage() + ", status:",
		//		RequestStatus.RS_OK, rs.getStatusCode());
		
		UnitTestUtils.stop(this);
	}	
}
