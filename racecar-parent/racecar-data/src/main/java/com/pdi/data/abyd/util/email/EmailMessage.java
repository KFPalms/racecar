/**
 * Copyright (c) 2008 Personnel Decisions International, Inc.
 * All rights reserved.
 *
 * $Header:
 */

package com.pdi.data.abyd.util.email;

import java.util.ArrayList;

// NOTE:  Eventually the email functionality in A By D may be migrated
// to the standard Oxcart functionality.  At that time, this module and
// its brethren in this folder can be dumped.

/**
 * EmailMessage contains the various pieces of data for an email. 
 *
 * @author	Mike Solomonson
 * @version	$Revision: 1 $  $Date: 12/04/08 12:23p $
 */
public class EmailMessage
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _toAddress;
	private String _fromAddress;
	private String _ccAddress = null;
	private String _subject;
	private String _text;
	private ArrayList<String> _textSubstitutions;
	private long _emailTemplateId;
	
	//
	// Constructors.
	//
	public EmailMessage()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//

	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public String getToAddress()
	{
		return _toAddress;
	}

	public void setToAddress(String value)
	{
	    _toAddress = value;
	}

	/*****************************************************************************************/
	public String getFromAddress()
	{
		return _fromAddress;
	}

	public void setFromAddress(String value)
	{
	    _fromAddress = value;
	}	

	/*****************************************************************************************/
	public String getCcAddress()
	{
		return _ccAddress;
	}

	public void setCcAddress(String value)
	{
	    _ccAddress = value;
	}

	/*****************************************************************************************/
	public String getSubject()
	{
		return _subject;
	}

	public void setSubject(String value)
	{
	    _subject = value;
	}	

	/*****************************************************************************************/
	public String getText()
	{
		return _text;
	}

	public void setText(String value)
	{
	    _text = value;
	}

	/*****************************************************************************************/
	public ArrayList<String> getTextSubstitutions()
	{
		if (_textSubstitutions == null)
		{
			_textSubstitutions = new ArrayList<String>();
		}
		return _textSubstitutions;
	}

	public void setTextSubstitutions(ArrayList<String> value)
	{
	    _textSubstitutions = value;
	}

	/*****************************************************************************************/
	public long getEmailTemplateId()
	{
		return _emailTemplateId;
	}

	public void setEmailTemplateId(long value)
	{
	    _emailTemplateId = value;	
	}
}