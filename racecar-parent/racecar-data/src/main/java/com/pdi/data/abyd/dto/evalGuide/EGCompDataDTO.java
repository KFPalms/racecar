/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.dto.evalGuide;

import java.util.ArrayList;

import com.pdi.data.abyd.dto.evalGuide.EGBarScoreData;


/**
 * EGCompDataDTO is a thin class containing all of the
 * data needed to add or update BAR scores for a
 * competency in an Eval Guide.
 * NOTE:  This class is incoming from the UI only and it contains
 *        data on the comp note and bar scores only.  Overide scores
 *        are handled elsewhere.
 * 
 * @author		Ken Beukelman
 */
public class EGCompDataDTO
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _participantId;
	private long _dnaId;
	private long _moduleId;
	private long _compId;
	private String _compNote;						// Consultant entered notes about the competency
	private ArrayList<EGBarScoreData> _compBarArray;	// Competency BAR score items
	private String _extCompScore;	// External competency score


	//
	// Constructors.
	//
	public EGCompDataDTO()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public String getParticipantId()
	{
		return _participantId;
	}
		
	public void setParticipantId(String value)
	{
		_participantId = value;
	}

	/*****************************************************************************************/
	public long getDnaId()
	{
		return _dnaId;
	}
		
	public void setDnaId(long value)
	{
		_dnaId = value;
	}

	/*****************************************************************************************/
	public long getModuleId()
	{
		return _moduleId;
	}
		
	public void setModuleId(long value)
	{
		_moduleId = value;
	}

	/*****************************************************************************************/
	public long getCompId()
	{
		return _compId;
	}
		
	public void setCompId(long value)
	{
		_compId = value;
	}

	/*****************************************************************************************/
	public String getCompNote()
	{
		return _compNote;
	}
		
	public void setCompNote(String value)
	{
		_compNote = value;
	}

	/*****************************************************************************************/
	public ArrayList<EGBarScoreData> getCompBarArray()
	{
		return _compBarArray;
	}
		
	public void setCompBarArray(ArrayList<EGBarScoreData> value)
	{
		_compBarArray = value;
	}

	/*****************************************************************************************/
	public String getExtCompScore()
	{
		return _extCompScore;
	}
		
	public void setExtCompScore(String value)
	{
		_extCompScore = value;
	}
}
