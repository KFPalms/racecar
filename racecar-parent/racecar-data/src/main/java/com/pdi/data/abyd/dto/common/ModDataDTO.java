/**
 * Copyright (c) 2008 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.dto.common;


/**
 * ModDataDTO is helper class to allow us to send the id, the name, and the module
 * display flag for a module back to the caller.  It is used in the DNAStructureDTO.
 * 
 * @author		Ken Beukelman
 */
public class ModDataDTO
{
	//
	// Static data.
	//


	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private long _modId;
	private String _modName;
	private boolean _modEditable;
	private boolean _modSubmitted;	// used in IG

	//
	// Constructors.
	//
	public ModDataDTO()
	{
		// does nothing at present
	}

	//
	// Instance methods.
	//

	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public long getModId()
	{
		return _modId;
	}
	
	public void setModId(long value)
	{
		_modId = value;
	}	

	/*****************************************************************************************/
	public String getModName()
	{
		return _modName;
	}

	public void setModName(String value)
	{
		_modName = value;
	}	

	/*****************************************************************************************/
	public boolean getModEditable()
	{
		return _modEditable;
	}

	public void setModEditable(boolean value)
	{
		_modEditable = value;
	}	

	/*****************************************************************************************/
	public boolean getModSubmitted()
	{
		return _modSubmitted;
	}

	public void setModSubmitted(boolean value)
	{
		_modSubmitted = value;
	}	



	/*
	 * Show me what you've got
	 */
	public String toString()
	{
		String ret = "DNAStructureDTO content (Java)\n";
		ret += "    Module ID=" + _modId + "\n";
		ret += "  Module Name=" + _modName + "\n";
		ret += "  is Editable=" + _modEditable + "\n";
		ret += "    Submitted=" + _modSubmitted + "\n";
		
		return ret;
	}
}
