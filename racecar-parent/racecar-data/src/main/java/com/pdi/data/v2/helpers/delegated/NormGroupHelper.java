package com.pdi.data.v2.helpers.delegated;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdi.data.dto.Norm;
import com.pdi.data.dto.NormGroup;
//import com.pdi.data.dto.Score;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.interfaces.INormGroupHelper;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.util.RequestStatus;
import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.logging.LogWriter;

public class NormGroupHelper implements INormGroupHelper {
	/*
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.INormGroupHelper#fromId(com.pdi.data.dto.SessionUser, java.lang.String)
	 */
	
	private static final Logger log = LoggerFactory.getLogger(NormGroupHelper.class);

	private final String UID = "nghlpr";

	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	/**
	 * fromId - get NormGroup for this normGroupId
	 *
	 * @param SessionUser
	 *            session
	 * @param String
	 *            normGroupId
	 * @return NormGroup
	 *
	 */
	@Override
	public NormGroup fromId(SessionUser session, int normGroupId) {

		NormGroup normGroup = new NormGroup();
		normGroup.setId(normGroupId);

		try {
			StringBuffer sb = new StringBuffer();
			// sb.append(" select normGroupId, instrumentCode, name, normType "
			// +
			// " from pdi_norm_group " +
			// " where normGroupId = ? ");
			sb.append("SELECT normGroupId, ");
			sb.append("       instrumentCode, ");
			sb.append("       name, ");
			sb.append("       normType, ");
			sb.append("       code, ");
			sb.append("       default_norm_group, ");
			sb.append("       sequence_order ");
			sb.append("  FROM pdi_norm_group ");
			sb.append("  WHERE normGroupId = ? ");
			DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
			if (dlps.isInError()) {
				// TODO When this method returns something, this should be
				// modified
				LogWriter.logSQL(LogWriter.WARNING, this,
						"Error preparing writeAnswerDataToDatabase query." + dlps.toString(), sb.toString());
			}
			DataResult dr = null;
			ResultSet rs = null;

			try {
				dlps.getPreparedStatement().setInt(1, normGroupId);
				dr = V2DatabaseUtils.select(dlps);
				rs = dr.getResultSet();

				NormHelper nh = new NormHelper();

				if (rs != null) {
					while (rs.next()) {
						if (rs.getInt("normType") == 1) {
							normGroup.setPopulation(NormGroup.GENERAL_POPULATION);
						} else if (rs.getInt("normType") == 2) {
							normGroup.setPopulation(NormGroup.SPECIAL_POPULATION);
						}
						normGroup.setName(rs.getString("name"));
						normGroup.setInstrumentCode(rs.getString("instrumentCode"));
						normGroup.setCode(rs.getString("code"));
						normGroup.setSequenceOrder(rs.getInt("sequence_order"));

						if (rs.getInt("default_norm_group") == 1) {
							normGroup.setDefaultNormGroup(true);
						} else {
							normGroup.setDefaultNormGroup(false);
						}

						// get Norms for the NormGroup
						normGroup.setNorms(nh.fromNormGroupId(session, normGroupId));
					}
				}
			} catch (java.sql.SQLException se) {
				LogWriter.logSQLWithException(LogWriter.ERROR, this, se.getMessage(), sb.toString(), se);
			} finally {
				if (dr != null) {
					dr.close();
					dr = null;
				}
			}
		} catch (Exception e) {
			LogWriter.logBasicWithException(LogWriter.ERROR, this, e.getMessage(), e);
		}

		return normGroup;
	}

	/**
	 * fromName - get NormGroup for this norm group name. NOTES: If the item is
	 * not found the returned NormGroup object has an id of 0 CAUUTION should be
	 * used because names can be the same from instrument to instrument
	 *
	 * @param SessionUser
	 *            session
	 * @param String
	 *            normGroupId
	 * @return NormGroup
	 */
	@Override
	public NormGroup fromName(SessionUser session, String normGroupName) {
		NormGroup ret = new NormGroup();
		ret.setId(0);
		ret.setName(normGroupName);

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT normGroupId, ");
		sb.append("       instrumentCode, ");
		sb.append("       name, ");
		sb.append("       normType, ");
		sb.append("       code, ");
		sb.append("       default_norm_group, ");
		sb.append("       sequence_order ");
		sb.append("  FROM pdi_norm_group ");
		sb.append("  WHERE name = ? ");
		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
		if (dlps.isInError()) {
			LogWriter.logSQL(LogWriter.WARNING, this,
					"Error preparing NormGroupHelper.fromName() query." + dlps.toString(), sb.toString());
			ret.setId(0);
			return ret;
		}
		DataResult dr = null;
		ResultSet rs = null;

		try {
			dlps.getPreparedStatement().setString(1, normGroupName);
			dr = V2DatabaseUtils.select(dlps);
			rs = dr.getResultSet();

			NormHelper nh = new NormHelper();

			if (rs != null) {
				// Assume a single return
				rs.next();
				if (rs.getInt("normType") == 1) {
					ret.setPopulation(NormGroup.GENERAL_POPULATION);
				} else if (rs.getInt("normType") == 2) {
					ret.setPopulation(NormGroup.SPECIAL_POPULATION);
				}
				ret.setName(rs.getString("name"));
				ret.setInstrumentCode(rs.getString("instrumentCode"));
				ret.setId(rs.getInt("normGroupId"));
				ret.setCode(rs.getString("code"));
				ret.setSequenceOrder(rs.getInt("sequence_order"));

				if (rs.getInt("default_norm_group") == 1) {
					ret.setDefaultNormGroup(true);
				} else {
					ret.setDefaultNormGroup(false);
				}

				// get Norms for the NormGroup
				ret.setNorms(nh.fromNormGroupId(session, ret.getId()));

				// See if there is more than one
				if (rs.next()) {
					LogWriter.logSQL(LogWriter.WARNING, this,
							"Too many norms from NormGroupHelper.fromName() query; norm name=" + normGroupName + ".  "
									+ dlps.toString(),
							sb.toString());
					ret.setId(0);
					ret.setNorms(null);
					return ret;
				}
			}
		} catch (java.sql.SQLException se) {
			LogWriter.logSQLWithException(LogWriter.ERROR, this, "Error in NormGroupHelper.fromName() fetching data.",
					sb.toString(), se);
			ret.setId(0);
			return ret;
		} finally {
			if (dr != null) {
				dr.close();
				dr = null;
			}
		}

		return ret;
	}

	/**
	 * fromInstrumentId - Convenience method to get the list of NormGroups
	 * available for a given instrument. No ordering performed.
	 *
	 * @param SessionUser
	 *            session
	 * @param String
	 *            instrumentCode
	 * @return ArrayList<NormGroup>
	 */
	// @Override
	@Override
	public ArrayList<NormGroup> fromInstrumentId(SessionUser session, String instrumentCode) {

		return (this.getNormGroups(session, instrumentCode, false));
	}

	/**
	 * fromInstrumentIdOrdered - Convenience method to get the list of
	 * NormGroups available for a given instrument. Order by name within type.
	 *
	 * @param SessionUser
	 *            session
	 * @param String
	 *            instrumentCode
	 * @return ArrayList<NormGroup>
	 */
	// @Override
	@Override
	public ArrayList<NormGroup> fromInstrumentIdOrdered(SessionUser session, String instrumentCode) {

		return (this.getNormGroups(session, instrumentCode, true));
	}

	/**
	 * getNormGroups - Core "fromInstrumentCode" method
	 *
	 * @param session
	 * @param instrumentCode
	 * @param ordered
	 * @return
	 */
	private ArrayList<NormGroup> getNormGroups(SessionUser session, String instrumentCode, boolean ordered) {
		ArrayList<NormGroup> normGroups = new ArrayList<NormGroup>();

		try {
			StringBuffer sb = new StringBuffer();
			// sb.append("select normGroupId, name, normType " +
			// "from pdi_norm_group " +
			// "where instrumentCode = ?");
			sb.append("SELECT normGroupId, ");
			sb.append("       name, ");
			sb.append("       normType, ");
			sb.append("       code, ");
			sb.append("       default_norm_group, ");
			sb.append("       sequence_order ");
			sb.append("  FROM pdi_norm_group ");
			sb.append("  WHERE instrumentCode = ? ");
			sb.append("    AND active = 1 ");
			if (ordered) {
				sb.append("  ORDER BY normType, sequence_order");
			}
			log.debug("SQL for getting Norm groups: {}, {}", sb, instrumentCode);
			DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
			if (dlps.isInError()) {
				// TODO When this method returns something, this should be
				// modified
				LogWriter.logSQL(LogWriter.WARNING, this,
						"Error preparing writeAnswerDataToDatabase query." + dlps.toString(), sb.toString());
			}
			DataResult dr = null;
			ResultSet rs = null;
			
			try {
				dlps.getPreparedStatement().setString(1, instrumentCode);
				dr = V2DatabaseUtils.select(dlps);
				rs = dr.getResultSet();
				
				NormHelper nh = new NormHelper();
				Map<Integer, HashMap<String, Norm>> batchNorms = nh.fromNormGroupIdBatch(session, instrumentCode);
				if (rs != null) {
					log.debug("Got resultset!");
					while (rs.next()) {
						// System.out.println("Found ONe");
						NormGroup ng = new NormGroup();
						ng.setId(rs.getInt("normGroupId"));
						ng.setInstrumentCode(instrumentCode);
						if (rs.getInt("normType") == 1) {
							ng.setPopulation(NormGroup.GENERAL_POPULATION);
						} else if (rs.getInt("normType") == 2) {
							ng.setPopulation(NormGroup.SPECIAL_POPULATION);
						}
						//ng.setNorms(nh.fromNormGroupId(session, ng.getId()));
						ng.setNorms(batchNorms.get(ng.getId()));
						ng.setName(rs.getString("name"));
						ng.setCode(rs.getString("code"));
						ng.setSequenceOrder(rs.getInt("sequence_order"));

						if (rs.getInt("default_norm_group") == 1) {
							ng.setDefaultNormGroup(true);
						} else {
							ng.setDefaultNormGroup(false);
						}
						normGroups.add(ng);
						// System.out.println(ng.getId() + " " +
						// ng.getInstrumentCode()+ " " + ng.getPopulation() + "
						// " + ng.getName());
					}
					log.debug("Finished building {} Norm Group Objects", normGroups.size());
					
				}
			} catch (java.sql.SQLException se) {
				LogWriter.logSQLWithException(LogWriter.ERROR, this, se.getMessage(), sb.toString(), se);
			} finally {
				if (dr != null) {
					dr.close();
					dr = null;
				}
			}
		} catch (Exception e) {
			LogWriter.logBasicWithException(LogWriter.ERROR, this, e.getMessage(), e);
		}

		// System.out.println("normGroups.size() " + normGroups.size());
		return normGroups;
	}

	// --------------------------------------------------------
	// Methods related to Participant - Instrument - Norm
	// --------------------------------------------------------

	/// **
	// * fromParticipantInstrumentId - get the list of NormGroups
	// * available for a given instrument and participant.
	// *
	// *
	// * @param SessionUser session
	// * @param String participantId
	// * @param String instrumentCode
	// * @return ArrayList<NormGroup>
	// */
	//// @Override
	// public ArrayList<NormGroup> fromParticipantInstrumentId(
	// SessionUser session, String participantId, String instrumentCode) {
	//
	// ArrayList<NormGroup> normGroupList = new ArrayList<NormGroup>();
	//
	// try {
	// StringBuffer sb = new StringBuffer();
	// //sb.append(" select pngp.normGroupId as gpId, pngp.normType as gpType,
	/// pngp.name as gpName, ");
	// //sb.append(" pnsp.normGroupId as spId, pnsp.normType as spType,
	/// pnsp.name as spName ");
	// //sb.append(" from pdi_participant_instrument_norm_map pin ");
	// //sb.append(" left join pdi_norm_group pngp on pngp.normGroupId =
	/// pin.genPopNormGroupId ");
	// //sb.append(" left join pdi_norm_group pnsp on pnsp.normGroupId =
	/// pin.specPopNormGroupId ");
	// //sb.append(" where pin.instrumentCode = ? ");
	// //sb.append(" and pin.participantId = ? ");
	// sb.append("SELECT pngp.normGroupId as gpId, ");
	// sb.append(" pngp.normType as gpType, ");
	// sb.append(" pngp.name as gpName, ");
	// sb.append(" pnsp.normGroupId as spId, ");
	// sb.append(" pnsp.normType as spType, ");
	// sb.append(" pnsp.name as spName ");
	// sb.append(" FROM pdi_participant_instrument_norm_map pin ");
	// sb.append(" LEFT JOIN pdi_norm_group pngp ON pngp.normGroupId =
	/// pin.genPopNormGroupId ");
	// sb.append(" LEFT JOIN pdi_norm_group pnsp on pnsp.normGroupId =
	/// pin.specPopNormGroupId ");
	// sb.append(" WHERE pin.instrumentCode = ? ");
	// sb.append(" AND pin.participantId = ? ");
	//
	// //System.out.println(" ArrayList<NormGroup> fromParticipantInstrumentId()
	/// sql: " + sb.toString());
	// System.out.println(" Fetched norms for instrument, candidate: " +
	/// instrumentCode + " " + participantId);
	// DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
	// if (dlps.isInError())
	// {
	// LogWriter.logSQL(LogWriter.WARNING, this,
	// "Error preparing writeAnswerDataToDatabase query." + dlps.toString(),
	// sb.toString());
	// }
	// DataResult dr = null;
	// ResultSet rs = null;
	//
	// try
	// {
	// dlps.getPreparedStatement().setString(1, instrumentCode);
	// dlps.getPreparedStatement().setString(2, participantId);
	// dr = V2DatabaseUtils.select(dlps);
	// rs = dr.getResultSet();
	//
	// NormHelper nh = new NormHelper();
	// if(rs != null) {
	// while (rs.next())
	// {
	// //TODO -- should only get one row back per participant/instrument???
	// // what about if someone has somehow taken the instrument for more
	// // than one project? or, in the new system, will that not be possible?
	// // we should probably talk about how we're handling that for sure....
	//
	// NormGroup genPop = new NormGroup();
	// genPop.setId(rs.getInt("gpId"));
	// genPop.setInstrumentCode(instrumentCode);
	// genPop.setPopulation(NormGroup.GENERAL_POPULATION);
	// genPop.setName(rs.getString("gpName"));
	// genPop.setNorms(nh.fromNormGroupId(session, genPop.getId()));
	//
	// NormGroup specPop = new NormGroup();
	// specPop.setId(rs.getInt("spId"));
	// specPop.setInstrumentCode(instrumentCode);
	// specPop.setPopulation(NormGroup.SPECIAL_POPULATION);
	// specPop.setName(rs.getString("spName"));
	// specPop.setNorms(nh.fromNormGroupId(session, specPop.getId()));
	//
	// normGroupList.add(genPop);
	// normGroupList.add(specPop);
	// }
	// }
	// }
	// catch(java.sql.SQLException se)
	// {
	// LogWriter.logSQLWithException(LogWriter.ERROR, this, se.getMessage(),
	/// sb.toString(), se);
	// }
	// finally
	// {
	// if (dr != null) { dr.close(); dr = null; }
	// }
	// } catch(Exception e) {
	// LogWriter.logBasicWithException(LogWriter.ERROR, this, e.getMessage(),
	/// e);
	// }
	//
	// return normGroupList;
	// }

	/**
	 * fromParticipantInstrumentIdProjectId - get the list of NormGroups
	 * available for a given instrument, participant, and project. All norm
	 * groups are returned. When both default and ppt norms are present, the
	 * defaults come out first. Within a particular set the gen pop norm will be
	 * first.
	 *
	 * NHN 1738: new use case, with participant in two different AbyD projects,
	 * so different sets of norms
	 *
	 * usecase: I have the most recent GPI for this participant, but he is in
	 * two different projects, with different norms. I want the norms for the
	 * current project.
	 *
	 * NOTE: Where project defaults are present they will come out first in the
	 * order. All current uses for this call loop through all of the norms with
	 * the last values overlaying the first in the destination. If there are
	 * both default and participant norms set the default norms are inserted
	 * then overlaid with the participant norms. If this method is called with a
	 * different processing method in mind, this fact should be taken into
	 * account.
	 *
	 * @param SessionUser
	 *            session
	 * @param String
	 *            participantId
	 * @param String
	 *            instrumentCode
	 * @param String
	 *            projectId
	 * @return ArrayList<NormGroup>
	 */
	// @Override
	@Override
	public ArrayList<NormGroup> fromParticipantInstrumentIdProjectId(SessionUser session, String participantId,
			String instrumentCode, String projectId) {

		ArrayList<NormGroup> normGroupList = new ArrayList<NormGroup>();

		try {
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT pngp.normGroupId as gpId, ");
			sb.append("       pngp.normType as gpType, ");
			sb.append("       pngp.name as gpName, ");
			sb.append("       pnsp.normGroupId as spId, ");
			sb.append("       pnsp.normType as spType, ");
			sb.append("       pnsp.name as spName ");
			sb.append("  FROM pdi_participant_instrument_norm_map pin ");
			sb.append("    LEFT JOIN pdi_norm_group pngp ON pngp.normGroupId = pin.genPopNormGroupId ");
			sb.append("    LEFT JOIN pdi_norm_group pnsp on pnsp.normGroupId = pin.specPopNormGroupId ");
			sb.append("  WHERE pin.instrumentCode = ? ");
			// sb.append(" AND pin.participantId = ? ");
			sb.append("    AND pin.participantId IN (?, 0) ");
			sb.append("    AND pin.projectId = ? ");
			sb.append("  ORDER BY pin.participantId ASC");

			// System.out.println(" ArrayList<NormGroup>
			// fromParticipantInstrumentId() sql: " + sb.toString());
			// System.out.println(" instrument, candidate, project : " +
			// instrumentCode + " " + participantId + " " + projectId);
			DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
			if (dlps.isInError()) {
				LogWriter.logSQL(LogWriter.WARNING, this,
						"Error preparing writeAnswerDataToDatabase query." + dlps.toString(), sb.toString());
			}
			DataResult dr = null;
			ResultSet rs = null;

			try {
				dlps.getPreparedStatement().setString(1, instrumentCode);
				dlps.getPreparedStatement().setString(2, participantId);
				dlps.getPreparedStatement().setString(3, projectId);
				dr = V2DatabaseUtils.select(dlps);
				rs = dr.getResultSet();

				NormHelper nh = new NormHelper();
				if (rs != null) {
					while (rs.next()) {

						NormGroup genPop = new NormGroup();
						genPop.setId(rs.getInt("gpId"));
						genPop.setInstrumentCode(instrumentCode);
						genPop.setPopulation(NormGroup.GENERAL_POPULATION);
						genPop.setName(rs.getString("gpName"));
						genPop.setNorms(nh.fromNormGroupId(session, genPop.getId()));

						NormGroup specPop = new NormGroup();
						specPop.setId(rs.getInt("spId"));
						specPop.setInstrumentCode(instrumentCode);
						specPop.setPopulation(NormGroup.SPECIAL_POPULATION);
						specPop.setName(rs.getString("spName"));
						specPop.setNorms(nh.fromNormGroupId(session, specPop.getId()));

						normGroupList.add(genPop);
						normGroupList.add(specPop);
					}
				}
			} catch (java.sql.SQLException se) {
				LogWriter.logSQLWithException(LogWriter.ERROR, this, se.getMessage(), sb.toString(), se);
			} finally {
				if (dr != null) {
					dr.close();
					dr = null;
				}
			}
		} catch (Exception e) {
			LogWriter.logBasicWithException(LogWriter.ERROR, this, e.getMessage(), e);
		}

		return normGroupList;
	}

	/**
	 * update updates the NormGrouppId(s) selected in the
	 * pdi_participant_instrument_norm_map
	 *
	 *
	 * @param SessionUser
	 *            session
	 * @param String
	 *            projectId
	 * @param String
	 *            participantId
	 * @param String
	 *            instrumentCode
	 *
	 */
	// @Override
	@Override
	public void update(SessionUser session, NormGroup ng, String projectId, String participantId,
			String instrumentCode) {

		////////////////////////////////////////////
		// Set up the initial checking on it query.
		///////////////////////////////////////////
		StringBuffer sqlQuery1 = new StringBuffer();
		sqlQuery1.append("SELECT pinm.pinId ");
		sqlQuery1.append("  FROM pdi_participant_instrument_norm_map pinm ");
		sqlQuery1.append("  WHERE pinm.projectId = ? "); // 1
		sqlQuery1.append("    AND pinm.participantid = ? "); // 2
		sqlQuery1.append("    AND pinm.instrumentCode = ?"); // 3

		// System.out.println("sqlQuery1 " + sqlQuery1);

		DataResult dr1 = null;
		ResultSet rs1 = null;

		////////////////////////////////////////////
		// Set up the update query.
		///////////////////////////////////////////
		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE pdi_participant_instrument_norm_map ");
		sb.append("  SET lastDate = ?, "); // 1
		sb.append("      lastUser = ?,  "); // 2
		if (ng.getPopulation() == 2) {
			sb.append("      specPopNormGroupId = ?  "); // 3
		} else if (ng.getPopulation() == 1) {
			sb.append("      genPopNormGroupId = ?  "); // 3
		}
		sb.append("  WHERE participantId = ? "); // 4
		sb.append("    AND projectId = ? "); // 5
		sb.append("    AND instrumentCode  = ?  "); // 6
		sb.append("    AND pinId = ?  "); // 7

		// System.out.println("sqlUpdate " + sb.toString());
		DataResult dr = null;
		DataResult dru = null;
		DataResult drInsert = null;

		try {
			////////////////////////////////////////////////////////////////////////
			// see if there's a row out there first...
			////////////////////////////////////////////////////////////////////////
			try {
				DataLayerPreparedStatement dlps1 = V2DatabaseUtils.prepareStatement(sqlQuery1);
				if (dlps1.isInError()) {
					LogWriter.logSQL(LogWriter.WARNING, this,
							"Error preparing NormGroupHelper update() query." + dlps1.toString(), sqlQuery1.toString());
					return;
				}

				// fill in the ?'s for the initial query...
				dlps1.getPreparedStatement().setString(1, projectId);
				dlps1.getPreparedStatement().setString(2, participantId);
				dlps1.getPreparedStatement().setString(3, instrumentCode);

				// now do the initial select, to see if the row is there....
				dr1 = V2DatabaseUtils.select(dlps1);

				// It's not there, do the insert
				if (dr1.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA) {
					// System.out.println("INSERT INTO
					// PDI_PARTICIPANT_INSTRUMENT_NORM_MAP.....");
					// do an insert....
					StringBuffer sbInsert = new StringBuffer();
					sbInsert.append("INSERT INTO pdi_participant_instrument_norm_map ");
					sbInsert.append("  (lastDate, lastUser, ");
					sbInsert.append("   participantId, instrumentCode, projectId, ");
					if (ng.getPopulation() == 2) {
						sbInsert.append(" specPopNormGroupId,  ");
					} else if (ng.getPopulation() == 1) {
						sbInsert.append(" genPopNormGroupId,  ");
					}
					sbInsert.append(" active ) ");
					sbInsert.append(" VALUES ( ?, ?, ?, ?, ?, ?, ?) ");

					DataLayerPreparedStatement dlpsInsert = V2DatabaseUtils.prepareStatement(sbInsert);
					if (dlpsInsert.isInError()) {
						LogWriter.logSQL(LogWriter.WARNING, this,
								"Error preparing doInsert query." + dlpsInsert.toString(), sbInsert.toString());
						return;
					}

					try {

						// java.util.Date today = new java.util.Date();
						// long t = today.getTime();
						// java.sql.Date dt = new java.sql.Date(t);
						String dtStr = sdf.format(new Date());

						// Fill sql parameters with values and get the data
						// dlpsInsert.getPreparedStatement().setDate(1, dt);
						dlpsInsert.getPreparedStatement().setString(1, dtStr);
						dlpsInsert.getPreparedStatement().setString(2, UID);
						dlpsInsert.getPreparedStatement().setString(3, participantId);
						dlpsInsert.getPreparedStatement().setString(4, instrumentCode);
						dlpsInsert.getPreparedStatement().setString(5, projectId);
						dlpsInsert.getPreparedStatement().setInt(6, ng.getId());
						dlpsInsert.getPreparedStatement().setInt(7, 1);

						try {
							drInsert = V2DatabaseUtils.insert(dlpsInsert);
						} catch (Exception e) {
							drInsert.close();
							LogWriter.logBasic(LogWriter.ERROR, this, e.getMessage());

						}
					} catch (java.sql.SQLException se) {
						LogWriter.logSQLWithException(
								LogWriter.ERROR, this, se.getMessage() + "  participantId = " + participantId
										+ ",  projectId = " + projectId + ",  " + ",  normId = " + ng.getId(),
								sbInsert.toString(), se);
					} finally {
						if (drInsert != null) {
							drInsert.close();
						}
					}

				} else if (dr1.getStatus().getStatusCode() == 0) {
					// System.out.println("UPDATE RESULTS DATA.........");
					// THERE ALREADY IS a row, so do the update..
					rs1 = dr1.getResultSet();
					rs1.next();
					// java.util.Date today = new java.util.Date();
					// long t = today.getTime();
					// java.sql.Date dt = new java.sql.Date(t);
					String dtStr = sdf.format(new Date());

					DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
					if (dlps.isInError()) {
						LogWriter.logSQL(LogWriter.WARNING, this,
								"Error preparing UPDATE pdi_participant_instrument_norm_map query." + dlps.toString(),
								sb.toString());
						return;
					}

					// THIS IS THE UPDATE ...
					// Fill sql parmeters with values and get the data
					// dlps.getPreparedStatement().setDate(1, dt);
					dlps.getPreparedStatement().setString(1, dtStr);
					dlps.getPreparedStatement().setString(2, UID);
					dlps.getPreparedStatement().setInt(3, ng.getId());
					dlps.getPreparedStatement().setString(4, participantId);
					dlps.getPreparedStatement().setString(5, projectId);
					dlps.getPreparedStatement().setString(6, instrumentCode);
					dlps.getPreparedStatement().setInt(7, rs1.getInt("pinId"));

					try {
						dru = V2DatabaseUtils.update(dlps);
						dru.getResultSet().close();
					} catch (Exception e) {
						// Does nothing
					}

					if (dru != null && !dru.getStatus().isOk()) {
						LogWriter.logSQL(LogWriter.WARNING, this,
								"Sql Exception : UPDATE pdi_participant_instrument_norm_map -- update data : "
										+ "  participantId = " + participantId + ",  projectId = " + projectId
										+ ",  normGroupId = " + ng.getId(),
								sb.toString());
					}
				} else {
					// this is a big bad error
					LogWriter.logSQL(LogWriter.ERROR, this,
							"Sql Exception : UPDATE pdi_participant_instrument_norm_map -- update data : "
									+ "  participantId = " + participantId + ",  projectId = " + projectId
									+ ",  normGroupId = " + ng.getId(),
							sb.toString());
				}
			} catch (Exception e) {

				LogWriter.logBasicWithException(LogWriter.ERROR, this, e.getMessage() + "  participantId = "
						+ participantId + ",  projectId = " + projectId + ",  normGroupId = " + ng.getId(), e);

			}
		} finally {
			if (dr != null) {
				dr.close();
				dr = null;
			}
			if (dr1 != null) {
				dr1.close();
				dr1 = null;
			}
			if (dru != null) {
				dru.close();
				dru = null;
			}
			if (drInsert != null) {
				drInsert.close();
				drInsert = null;
			}
		}
	}

	/**
	 * getParticipantSelectedNormGroups - Retrieve a list of norm group IDs
	 * currently selected by this participant in this project. NOTE: If there is
	 * no norm selected by this ppt but there is a project default, the default
	 * is returned.
	 *
	 * @param pptId
	 * @param projId
	 * @return
	 */
	@Override
	public HashMap<String, ArrayList<NormGroup>> getSelectedNormGroups(String pptId, String projId) {
		HashMap<String, ArrayList<NormGroup>> ret = new HashMap<String, ArrayList<NormGroup>>();

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT instrumentCode, ");
		sb.append("       specPopNormGroupId, ");
		sb.append("       genPopNormGroupId ");
		sb.append("  FROM  pdi_participant_instrument_norm_map pin ");
		sb.append("  WHERE pin.participantId in (0, " + pptId + ") ");
		sb.append("    AND pin.projectId = " + projId + " ");
		sb.append("    AND pin.active = 1  ");
		sb.append("  ORDER by instrumentCode ASC, participantId DESC ");

		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
		if (dlps.isInError()) {
			System.out.println("Warning:  NormGroupHelper.getSelectedNormGroups() failed to prepare the SQL.  "
					+ "projId=" + projId + ".  " + dlps.toString());
			return ret;
		}
		DataResult dr = null;
		ResultSet rs = null;

		try {
			dr = V2DatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			if (dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA) {
				return ret;
			}
			if (rs == null || !rs.isBeforeFirst()) {
				return ret;
			}

			// We have data of some sort...
			String curInst = "";
			while (rs.next()) {
				String inst = rs.getString("instrumentCode");
				int sp = rs.getInt("specPopNormGroupId");
				int gp = rs.getInt("genPopNormGroupId");
				if (sp == 0 && gp == 0)
					continue;
				if (curInst.equals(inst))
					continue;

				curInst = inst;
				ret.put(inst, new ArrayList<NormGroup>());

				if (sp != 0) {
					NormGroup spg = new NormGroup();
					spg.setId(sp);
					spg.setPopulation(NormGroup.SPECIAL_POPULATION);
					ret.get(inst).add(spg);
				}

				if (gp != 0) {
					NormGroup gpg = new NormGroup();
					gpg.setId(gp);
					gpg.setPopulation(NormGroup.GENERAL_POPULATION);
					ret.get(inst).add(gpg);
				}
			}

			return ret;
		} catch (java.sql.SQLException se) {
			System.out.println(
					"Error:  NormGroupHelper.getSelectedNormGroups() resultSet fetch.  msg=" + se.getMessage());
			ret.clear();
			return ret;
		} finally {
			if (dr != null) {
				dr.close();
				dr = null;
			}
		}
	}

	/**
	 * getDefaultNormGroupMap retrieves the default norm groups for a project in
	 * a HashMap keyed on instrument
	 *
	 * @param projId
	 * @return
	 */
	@Override
	public HashMap<String, ArrayList<NormGroup>> getDefaultNormGroupMap(String projId) {
		HashMap<String, ArrayList<NormGroup>> ret = new HashMap<String, ArrayList<NormGroup>>();

		StringBuffer sb = new StringBuffer();


		sb.append("SELECT inm.instrumentCode,  inm.genPopNormGroupId, inm.specPopNormGroupId, "
				+ "png.code, png.name, png.normType, png.normGroupId  ");
		sb.append("FROM pdi_participant_instrument_norm_map inm , pdi_norm_group png ");
		sb.append("WHERE inm.participantId = 0 AND inm.projectId = " + projId + " AND inm.active = 1 " );
		sb.append("and png.normGroupId in (inm.genPopNormGroupId, inm.specPopNormGroupId) ");
		sb.append("and png.active = 1");
		
		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
		if (dlps.isInError()) {
			System.out.println("Warning:  NormGroupHelper.getDefaultNormGroups() failed to prepare the SQL.  "
					+ "projId=" + projId + ".  " + dlps.toString());
			return ret;
		}
		DataResult dr = null;
		ResultSet rs = null;

		try {
			dr = V2DatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			if (dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA) {
				return ret;
			}
			if (rs == null || !rs.isBeforeFirst()) {
				return ret;
			}

			// We have data of some sort...
			//ArrayList<NormGroup> al = new ArrayList<NormGroup>();
			//String instrumentCode
			while (rs.next()) {
				
				if (rs.getInt("genPopNormGroupId") == rs.getInt("normGroupId")) {
					NormGroup ng = new NormGroup();
					ng.setPopulation(NormGroup.GENERAL_POPULATION);
					ng.setCode(rs.getString("code"));
					ng.setName(rs.getString("name"));
					ng.setId(rs.getInt("genPopNormGroupId"));
					//al.add(ng);
					if (ret.get(rs.getString("instrumentCode")) == null){
						ArrayList<NormGroup> ngList = new ArrayList<NormGroup>();
						ngList.add(ng);
						ret.put(rs.getString("instrumentCode"), ngList);
					} else {
						ret.get(rs.getString("instrumentCode")).add(ng);
					}
					log.debug("Creating Norm Group for GenPop, name: {}, code: {}, list size: {}", ng.getName(), 
							ng.getCode(), ret.get(rs.getString("instrumentCode")).size());
				}
				if (rs.getInt("specPopNormGroupId") == rs.getInt("normGroupId")) {
					NormGroup ng2 = new NormGroup();
					ng2.setPopulation(NormGroup.SPECIAL_POPULATION);
					ng2.setCode(rs.getString("code"));
					ng2.setName(rs.getString("name"));
					ng2.setId(rs.getInt("specPopNormGroupId"));
					//al.add(ng2);
					if (ret.get(rs.getString("instrumentCode")) == null){
						ArrayList<NormGroup> ngList = new ArrayList<NormGroup>();
						ngList.add(ng2);
						ret.put(rs.getString("instrumentCode"), ngList);
					} else {
						ret.get(rs.getString("instrumentCode")).add(ng2);
					}
					log.debug("Creating Norm Group for SpecPop, name: {}, code: {}, list size: {}", ng2.getName(), 
							ng2.getCode(), ret.get(rs.getString("instrumentCode")).size());
				}

				
			}
			//ret.put(rs.getString("instrumentCode"), al);

			return ret;
		} catch (java.sql.SQLException se) {
			System.out
					.println("Error:  NormGroupHelper.getDefaultNormGroups() resultSet fetch.  msg=" + se.getMessage());
			ret.clear();
			return ret;
		} finally {
			if (dr != null) {
				dr.close();
				dr = null;
			}
		}
	}

	/**
	 * getDefaultNormGroupSet retrieves the default norm group ids for a project
	 *
	 * @param projId
	 * @return
	 */
	@Override
	public HashSet<Integer> getDefaultNormGroupSet(String projId) {
		HashSet<Integer> ret = new HashSet<Integer>();

		// Get the data using the Map call
		HashMap<String, ArrayList<NormGroup>> map = getDefaultNormGroupMap(projId);

		// Move the bare IDs into a set
		for (Iterator<Map.Entry<String, ArrayList<NormGroup>>> itr = map.entrySet().iterator(); itr.hasNext();) {
			Map.Entry<String, ArrayList<NormGroup>> ent = itr.next();

			if (ent.getValue().isEmpty()) {
				int genNormGroupId = getDefaultNormGroupForInstrument(ent.getKey(), NormGroup.GENERAL_POPULATION);
				if (genNormGroupId != 0) {
					ret.add(genNormGroupId);
				}
				int specNormGroupId = getDefaultNormGroupForInstrument(ent.getKey(), NormGroup.SPECIAL_POPULATION);
				if (specNormGroupId != 0) {
					ret.add(specNormGroupId);
				}
			} else {
				for (NormGroup ng : ent.getValue()) {
					ret.add(ng.getId());
				}
			}
		}

		return ret;
	}

	/**
	 * getNormableCourseSet - Returns a Set of all possible courses that have
	 * norms. If this set is to be the basis for a further restricted set (valid
	 * courses in an engagement for example), that set must be generated from
	 * this set by further filtering.
	 */
	@Override
	public HashSet<String> getNormableCourseSet() {
		HashSet<String> ret = new HashSet<String>();

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT DISTINCT instrumentCode FROM pdi_norm_group");

		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
		if (dlps.isInError()) {
			LogWriter.logBasic(LogWriter.ERROR, this,
					"NormGroupHelper.getNormableCourseSet() failed to prepare the select SQL" + ".  Error code="
							+ dlps.getStatus().getExceptionCode() + ", Error msg="
							+ dlps.getStatus().getExceptionMessage());
			return null;
		}
		DataResult dr = null;
		ResultSet rs = null;

		try {
			dr = V2DatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			if (dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA || rs == null || !rs.isBeforeFirst()) {
				LogWriter.logBasic(LogWriter.WARNING, this,
						"NormGroupHelper.getNormableCourseSet() detected no normable courses.");
				return ret;
			}

			// We have data of some sort...
			while (rs.next()) {
				ret.add(rs.getString("instrumentCode"));
			}

			return ret;
		} catch (java.sql.SQLException se) {
			System.out
					.println("Error:  NormGroupHelper.getDefaultNormGroups() resultSet fetch.  msg=" + se.getMessage());
			return null;
		} finally {
			if (dr != null) {
				dr.close();
				dr = null;
			}
		}
	}

	/**
	 * updatePptNormByProjIdNormGroup - update all of the existing participants
	 * in a project/instrument (but not the project default) to the norm group
	 * id indicated in the NormGroup object.
	 */
	@Override
	public boolean updatePptNormByProjIdNormGroup(String projId, NormGroup ng) {
		DataLayerPreparedStatement dlps = null;

		if (projId == null) {
			LogWriter.logBasic(LogWriter.WARNING, this,
					"NormGroupHelper.updatePptNormByProjIdNormGroup(): no Project ID passed.");
			return false;
		}
		if (ng == null) {
			LogWriter.logBasic(LogWriter.WARNING, this,
					"NormGroupHelper.updatePptNormByProjIdNormGroup(): no NormGroup object passed.");
			return false;
		}

		String abbv = ng.getInstrumentCode();
		int id = ng.getId();
		int pop = ng.getPopulation();

		if (abbv == null || abbv.isEmpty()) {
			LogWriter.logBasic(LogWriter.WARNING, this,
					"NormGroupHelper.updatePptNormByProjIdNormGroup(): no instrument name found in NormGroup object.");
			return false;
		}
		if (id == 0) {
			LogWriter.logBasic(LogWriter.WARNING, this,
					"NormGroupHelper.updatePptNormByProjIdNormGroup(): no norm group id found in NormGroup object.");
			return false;
		}
		if (!ng.hasValidPop()) {
			LogWriter.logBasic(LogWriter.WARNING, this,
					"NormGroupHelper.updatePptNormByProjIdNormGroup(): invalid population type found in NormGroup object.");
			return false;
		}

		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE pdi_participant_instrument_norm_map ");
		if (pop == NormGroup.GENERAL_POPULATION)
			sb.append("  SET genPopNormGroupId = ? ");
		else
			sb.append("  SET specPopNormGroupId = ? ");
		sb.append("  WHERE projectId = ? ");
		sb.append("    AND instrumentCode = ? ");
		sb.append("    AND participantId != 0");

		dlps = V2DatabaseUtils.prepareStatement(sb);
		if (dlps.isInError()) {
			LogWriter.logBasic(LogWriter.WARNING, this,
					"NormGroupHelper.updatePptNormByProjIdNormGroup() failed to prepare the update SQL. projId="
							+ projId + ".  Error code=" + dlps.getStatus().getExceptionCode() + ", Error msg="
							+ dlps.getStatus().getExceptionMessage());
			return false;
		}

		DataResult dr = null;

		try {
			// Fill sql parameters with values and insert the data
			dlps.getPreparedStatement().setInt(1, id);
			dlps.getPreparedStatement().setString(2, projId);
			dlps.getPreparedStatement().setString(3, abbv);
			dr = V2DatabaseUtils.update(dlps);
			if (dr.isInError()) {
				LogWriter.logBasic(LogWriter.ERROR, this,
						"NormGroupHelper.updatePptNormByProjIdNormGroup() failed to update. projId=" + projId
								+ ", course=" + abbv + ", poptype=" + pop + ", ID=" + id + ".  Error code="
								+ dlps.getStatus().getExceptionCode() + ", Error msg="
								+ dlps.getStatus().getExceptionMessage());
				return false;
			}

			return true;
		} catch (SQLException se) {
			LogWriter.logBasicWithException(LogWriter.ERROR, this,
					"NormGroupHelper.updatePptNormByProjIdNormGroup():  update data.  inst Code=" + abbv + ", projId="
							+ projId + ".  Error code=" + dlps.getStatus().getExceptionCode() + ", Error msg="
							+ dlps.getStatus().getExceptionMessage(),
					se);
			return false;
		} finally {
			if (dr != null) {
				dr.close();
				dr = null;
			}
		}
	}

	/**
	 * getDefaultNormGroupForInstrument retrieves the default norm group for an
	 * instrument
	 *
	 * @param instrument
	 * @param normType
	 * @return
	 */
	@Override
	public int getDefaultNormGroupForInstrument(String instrumentCode, int normType) {
		int normGroupId = 0;

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT normGroupId ");
		sb.append("  FROM pdi_norm_group ");
		sb.append("  WHERE instrumentCode = ? ");
		sb.append("  AND normType = ? ");
		sb.append("  AND default_norm_group = 1");
		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
		if (dlps.isInError()) {
			LogWriter.logSQL(LogWriter.WARNING, this,
					"Error preparing NormGroupHelper.getDefaultNormGroupForInstrument() query." + dlps.toString(),
					sb.toString());
			return 0;
		}
		DataResult dr = null;
		ResultSet rs = null;

		try {
			dlps.getPreparedStatement().setString(1, instrumentCode);
			dlps.getPreparedStatement().setInt(2, normType);
			dr = V2DatabaseUtils.select(dlps);
			rs = dr.getResultSet();

			NormHelper nh = new NormHelper();

			if (rs != null) {
				// Assume a single return
				rs.next();
				normGroupId = rs.getInt("normGroupId");

				// See if there is more than one
				if (rs.next()) {
					LogWriter.logSQL(LogWriter.WARNING, this,
							"Too many norms from NormGroupHelper.getDefaultNormGroupForInstrument() query; norm instrumentCode="
									+ instrumentCode + ".  " + dlps.toString(),
							sb.toString());
					return normGroupId;
				}
			}
		} catch (java.sql.SQLException se) {
			LogWriter.logSQLWithException(LogWriter.ERROR, this,
					"Error in NormGroupHelper.getDefaultNormGroupForInstrument() fetching data.", sb.toString(), se);
			return 0;
		} finally {
			if (dr != null) {
				dr.close();
				dr = null;
			}
		}

		return normGroupId;
	}

}
