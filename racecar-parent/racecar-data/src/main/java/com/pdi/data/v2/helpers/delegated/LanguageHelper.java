/**
 * Copyright (c) 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v2.helpers.delegated;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.pdi.data.dto.Language;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.interfaces.ILanguageHelper;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.util.RequestStatus;
import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.logging.LogWriter;

/*
 * LanguageHelper provides language information in the V2 environment
 */
public class LanguageHelper implements ILanguageHelper 
{
	//
	// Static data
	//
	private static final int ALL = 0;
	private static final int ID = 1;
	private static final int CODE = 2;
	
	//
	// Static methods.
	//
	
	//
	// Instance data.
	//
	
	//
	// Instance methods.
	//
	
	/*
	 * fromId - Fetches a language by ID
	 * 
	 * @param sessionUser - A SessionUser object
	 * @param id - A language ID
	 * @returns A Language object for the specified ID
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.ILanguageHelper#fromId(com.pdi.data.dto.SessionUser, java.lang.String)
	 */
	@Override
	public Language fromId(SessionUser sessionUser, String id)
	{
		ArrayList<Language> al = fetchLanguageList(ID, id);
		return al.get(0);
	}


	/*
	 * fromCode - Fetches a language by language code
	 * 
	 * @param sessionUser - A SessionUser object
	 * @param code - A language code
	 * @returns A Language object for the specified code
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.ILanguageHelper#fromCode(com.pdi.data.dto.SessionUser, java.lang.String)
	 */
	@Override
	public Language fromCode(SessionUser su, String code)
	{
		ArrayList<Language> al = fetchLanguageList(CODE, code);
		return al.get(0);
	}


	/*
	 * all - Fetches an ArrayList of all languages
	 * 
	 * @param sessionUser - A SessionUser object
	 * @returns An ArrayList of Language objects representing all available, active languages
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.ILanguageHelper#all(com.pdi.data.dto.SessionUser)
	 */
	@Override
	public ArrayList<Language> all(SessionUser su)
	{
		return fetchLanguageList(ALL, null);
	}
	
	
	
	/*
	 * fetchLanguageList - Gets the requested language data and returns it in
	 * an ArrayList.  for the code and id fetches, the list should be exactly
	 * one entry long.
	 * 
	 * @param type - The type of query (CODE, ID, or ALL)
	 * @param key - The id or code used (CODE and ID queries - ignored in the ALL query)
	 * @return An ArrayList of Language objects
	 */
	private ArrayList<Language> fetchLanguageList(int type, String key)
	{
		ArrayList<Language> ret = new ArrayList<Language>();
		
		String andString = null;
		switch (type)
		{
		case ID:
			andString = "AND ll.languageId = " + key;
			break;
		case CODE:
			andString = "AND languageCode = '" + key + "'";
			break;
		default:
			andString = "";
			break;
		}
		
		StringBuffer q = new StringBuffer();		
		q.append("SELECT ll.languageId, ");
		q.append("       ll.languageCode, ");
		//q.append("       ll.textId, ");
		q.append("       txt1.text as engText, ");
		q.append("       txt2.text as transText ");
		q.append("  FROM pdi_abd_language ll ");
		q.append("    LEFT JOIN pdi_abd_text txt1 ON (txt1.textId = ll.textId AND txt1.languageId = " + Language.US_ENGLISH_ID + ") ");
		q.append("    LEFT JOIN pdi_abd_text txt2 ON (txt2.textId = ll.textId AND txt2.languageId = ll.languageId) ");
		q.append("  WHERE ll.active = 1 ");
		q.append("    " + andString + " ");
		q.append("  ORDER BY ll.languageId");

		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(q);
		if (dlps.isInError())
		{
			System.out.println("fetchLanguageList() 'prepare' error: type=" + type + ", key=" + key + "." +
								"  Error info: code=" + dlps.getStatus().getExceptionCode() +
								", msg=" + dlps.getStatus().getExceptionMessage());
			LogWriter.logBasic(LogWriter.ERROR, this, "fetchData - Creating DLPS. " + dlps.toString());
			return null;
		}
		DataResult dr = null;
		ResultSet rs = null;
		try		
		{
			dr = V2DatabaseUtils.select(dlps);
			if(dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
			{
				// No data to find... we are done here
				return ret;
			}
			
			// Get the rs and loop through it 
			rs = dr.getResultSet();
			while (rs.next())
			{
				Language lang = new Language();
				lang.setId(rs.getString("languageId"));
				lang.setCode(rs.getString("languageCode"));
				lang.setEnglishName(rs.getString("engText"));
				lang.setTranslatedName(rs.getString("transText"));
				ret.add(lang);
			}

			return ret;
		}
		catch(SQLException se)
		{	
			System.out.println("fetchLanguageList() exception: type=" + type + ", key=" + key + "; msg=" + se.getMessage());
			LogWriter.logBasicWithException(LogWriter.ERROR, this,
					"fetchLanguageList - Accessing data.", se);
			return null;
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}
}
