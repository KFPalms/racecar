/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.dto;

import java.util.ArrayList;

import com.pdi.data.util.BaseDTO;
import com.pdi.data.util.RequestStatus;

/**
 * LanguageListDTO is a class designed to convey the information for a number of
 * languages used in the PDI V3 extensions.  It contains an ArrayList of
 * LanguageDTO objects; the status in each of those may be redundant.
 * 
 * @author		Ken Beukelman
  */
public class LanguageListDTO extends BaseDTO
{
	//
	// Static data.
	//


	//
	// Static methods.
	//


	//
	// Instance data.
	//
	private ArrayList<LanguageDTO> _langList = new ArrayList<LanguageDTO>();


	//
	// Constructors.
	//
	public LanguageListDTO()
	{
		// does nothing right now
	}
	
	// Set up a new DTO with status but no data
	public LanguageListDTO(RequestStatus stat)
	{
		this.setStatus(stat);
	}
	
	//
	// Instance methods.
	//

	/**
	 * Generic toString method
	 */
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("LanguageListDTO:  ");
		sb.append("Status code=" + this.getStatus().getStatusCode() + ", count=" + _langList.size());
		for(int i=0; i < _langList.size(); i++)
		{
			sb.append("\n      id=" + _langList.get(i).getId());
			sb.append(", name=>" + _langList.get(i).getName() + "<");
		}
		return sb.toString();
	}
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public ArrayList<LanguageDTO> getLangList()
	{
		return _langList;
	}
	
	public void setLangList(ArrayList<LanguageDTO> value)
	{
		_langList = value;
	}	
}
