package com.pdi.data.dto;

import java.util.ArrayList;

/**
 * AttachmentHolder is a wrapper around the Attachment data for
 * a specific Participant.
 * 
 * Attachments are uploaded through the Test Data application.
 * Attachments are accessible in Test Data, and, read-only, 
 * through the DRI.
 * Attachments are .pdf, .doc, or .xls files, and that
 * is controlled via the UI.
 *
 * @author      MB Panichi
 */
public class AttachmentHolder extends CoreDataObject
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	/**
	 * 
	 */
	private static final long serialVersionUID = 7997544983918185526L;
	//
	// Instance data.
	//
	private Participant participant;
	private Project project;
    private ArrayList<Attachment> attachments = new ArrayList<Attachment>();
    //private ArrayList<AttachmentType> attachmentTypes = new ArrayList<AttachmentType>();

	//
	// Constructors.
	//
	public AttachmentHolder()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//
	
	public String toString()
	{
		String ret = "";
		
		ret += "AttachmentHolder:\n";
		ret += participant.toString() + "\n";
		ret += ((project == null) ? "  Project - null" : project.toString()) + "\n";
		ret += "    " + attachments.size() + " attachments:\n";
		for (Attachment att : attachments)
		{
			ret += att.toString() + "\n";
		}
		
		return ret;
	}

	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//



	/*****************************************************************************************/
	public void setParticipant(Participant participant)
	{
		this.participant = participant;
	}

	public Participant getParticipant()
	{
		return participant;
	}

	/*****************************************************************************************/
	public ArrayList<Attachment> getAttachments()
	{
		return attachments;
	}

	public void setAttachments(ArrayList<Attachment> value)
	{
		attachments = value;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Project getProject() {
		return project;
	}

//
//	/*****************************************************************************************/
//	public ArrayList<AttachmentType> getAttachmentTypes()
//	{
//		return attachmentTypes;
//	}
//
//	public void setAttachmentTypes(ArrayList<AttachmentType> value)
//	{
//		attachmentTypes = value;
//	}

}


