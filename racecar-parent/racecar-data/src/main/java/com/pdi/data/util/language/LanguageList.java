package com.pdi.data.util.language;

import java.util.ArrayList;

/**
 * Wrapper around an Arraylist of LanguageItemDTO elements
 * @author loa-kbeukelm
 *
 */
public class LanguageList
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private ArrayList<LanguageItemDTO> _list;

	//
	// Constructors.
	//
	
	// No paramater constructor (hidden
	private LanguageList()
	{
		// Does nothing presently
	}
	
	// 1 parameter constructor (only public constructor)
	public LanguageList(ArrayList<LanguageItemDTO> list)
	{
		_list = list;
	}
	
	//
	// Instance Methods
	//

	/*
	 * toString - return the content of this object as a string
	 */
		public String toString()
		{
			String ret = "";
			if(_list.isEmpty())
			{
				ret = "List is empty";
			}  else  {
				for(LanguageItemDTO dto : _list)
				{
					ret += dto.toString() + "\n";
				}
			}

			return ret;
		}

		//*******************************************************************************************//
		//*                                                                                         *//
		//*                                      Get/Set Functions                                  *//
		//*                                                                                         *//
		//*******************************************************************************************//
				
		/*****************************************************************************************/
		public ArrayList<LanguageItemDTO> getList()
		{
			return _list;
		}

		public void setList(ArrayList<LanguageItemDTO> value)
		{
			_list = value;
		}
}
