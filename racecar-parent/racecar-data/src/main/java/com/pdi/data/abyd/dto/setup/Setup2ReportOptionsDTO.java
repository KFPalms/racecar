/**
 * Copyright (c) 2008 Personnel Decisions International, Inc.
 * All rights reserved.
 *
 * $Header: com/pdi/data/abyd/dto/setup/Setup2ReportOptionsDTO.java 6     8/25/05 3:23p mpanichi $
 */
package com.pdi.data.abyd.dto.setup;

import java.util.ArrayList;

/**
 * Setup2ReportOptionsDTO is a wrapper around a number of data items that are needed when 
 * part 2 of the Design Setup app is initially started.
 * 
 * @author		Ken Beukelman
 * @author      MB Panichi
 */
public class Setup2ReportOptionsDTO
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//

    private long _dnaId = 0;
    private ArrayList<Setup2ReportOptionValueDTO> _optionValuesArray;

	//
	// Constructors.
	//
	public Setup2ReportOptionsDTO()
	{
		// Does nothing presently

		
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public long getDnaId()
	{
		return _dnaId;
	}
		
	public void setDnaId(long value)
	{
		_dnaId = value;
	}

	/*****************************************************************************************/
	public ArrayList<Setup2ReportOptionValueDTO> getOptionValuesArray()
	{
		if(_optionValuesArray == null){
			_optionValuesArray = new ArrayList<Setup2ReportOptionValueDTO>();
		}
		
		return _optionValuesArray;
	}
		
	public void setOptionValuesArray(ArrayList<Setup2ReportOptionValueDTO> value)
	{
		_optionValuesArray = value;
	}

	
	
	
	
	
}

