/**
 * Copyright (c) 2010, 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.setup;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import com.pdi.data.abyd.dto.setup.ReportModelCompChildDTO;
import com.pdi.data.abyd.dto.setup.ReportModelCompetencyDTO;
import com.pdi.data.abyd.dto.setup.ReportModelStructureDTO;
import com.pdi.data.abyd.dto.setup.ReportModelSuperFactorDTO;
import com.pdi.data.util.RequestStatus;
import com.pdi.data.util.language.DefaultLanguageHelper;

/**
 * ReportModelDataHelper contains all the methods associated with the Report Model tailoring
 * functionality in Setup2.
 *
 * @author		Ken Beukelman
 */
public class ReportModelDataHelper
{
	//
	//  Static data.
	//
	static public boolean DEL_NON_DFLT_TXT = true;	// Default setting
	static public boolean KEEP_ALL_LANG_TXT = false;	// Used in clone only

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	
	//
	// Constructors.
	//
	public ReportModelDataHelper()
	{
		// Does nothing at this time
	}
	
	//
	//  Instance methods.
	//

	/**
	 * createNewReportModelFromDna
	 * Create a new Report Model from the DNA setup.
	 * Used by setup1 code -- not accessed via the Service
	 * 
	 * NOTE:  This code DELETES an existing report model
	 *
	 * @param Connection con - the database connection
	 * @param long dnaId - the dna id
	 * @throws Exception
	 */
	 public void createNewReportModelFromDna(Connection con, long dnaId)
		throws Exception
	{
		// First, delete the old model if it exists
		this.deleteReportModel(con, dnaId);

		// Then create the new model
		this.createRMFromDna(con, dnaId);
	}


	/**
	 * Deletes all of the Report Model related data for
	 * a particular project.
	 *
	 * @param Connection con - A database connection
	 * @param lon dnaId - A dna Id
	 * @throws Exception
	 */
	 private void deleteReportModel(Connection con, long dnaId)
		throws Exception
	{
		Statement stmt = null;
		ResultSet rs = null;
		String phase = null;

		// Delete the text
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT rms.userTextId ");
		sqlQuery.append("  FROM pdi_abd_rm_superfactor rms ");
		sqlQuery.append("  WHERE rms.dnaId = " + dnaId + " ");
		sqlQuery.append("    AND rms.userTextId > 0 ");
		sqlQuery.append("UNION ");
		sqlQuery.append("SELECT rmc.userTextId ");
		sqlQuery.append("  FROM pdi_abd_rm_competency rmc ");
		sqlQuery.append("  WHERE rmc.dnaId = " + dnaId + " ");
		sqlQuery.append("    AND rmc.userTextId > 0");

		try
		{
			phase = "create text select Statement";
			stmt = con.createStatement();
	
			phase = "fetch texts";
			rs = stmt.executeQuery(sqlQuery.toString());
			if(rs.isBeforeFirst())
			{
				// Got texts to delete... do it
				// get the user text ids to delete
				String txtIds = "";
				boolean first = true;
				while (rs.next())
				{
					if (first)
						first = false;
					else
						txtIds += ", ";
					
					txtIds += rs.getString("userTextId");
				}
		       	sqlQuery.setLength(0);
				sqlQuery.append("DELETE FROM user_text ");
				sqlQuery.append("  WHERE typeCode = 'C' ");
				sqlQuery.append("    AND userTextId IN (" +txtIds + ")");
				phase = "delete texts";
				stmt.executeUpdate(sqlQuery.toString());
			}
	
			// Delete the structure from the bottom up
			sqlQuery.setLength(0);
			sqlQuery.append("DELETE FROM pdi_abd_rm_comp_children ");
			sqlQuery.append("  WHERE dnaId = " + dnaId);
			phase = "delete comp children";
			stmt.executeUpdate(sqlQuery.toString());
	
			sqlQuery.setLength(0);
			sqlQuery.append("DELETE FROM pdi_abd_rm_competency ");
			sqlQuery.append("  WHERE dnaId = " + dnaId);
			phase = "delete competencies";
			stmt.executeUpdate(sqlQuery.toString());
	
			sqlQuery.setLength(0);
			sqlQuery.append("DELETE FROM pdi_abd_rm_superfactor ");
			sqlQuery.append("  WHERE dnaId = " + dnaId);
			phase = "delete superfactors";
			stmt.executeUpdate(sqlQuery.toString());
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL deleteReportModel (" + phase + ").  " +
	    			"SQLException: " + ex.getMessage() + ", " +
	    			"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (rs != null)
			{
				try	{  rs.close();  }
				catch (SQLException sqlEx)
				{
					System.out.println("Error closing RM delete ResultSet.  Ignored.  " +
	    			"SQLException: " + sqlEx.getMessage() + ", " +
	    			"SQLState: " + sqlEx.getSQLState() + ", " +
					"VendorError: " + sqlEx.getErrorCode());
				}
				rs = null;
			}
			if (stmt != null)
			{
				try	{  stmt.close();  }
				catch (SQLException sqlEx)
				{
					System.out.println("Error closing RM delete Statement.  Ignored.  " +
	    			"SQLException: " + sqlEx.getMessage() + ", " +
	    			"SQLState: " + sqlEx.getSQLState() + ", " +
					"VendorError: " + sqlEx.getErrorCode());
				}
				stmt = null;
			}
		}
	}


	/**
	 * Create a new Report Model based upon the DNA.  All
	 * label texts are default texts in the default language.
	 *
	 * @param Connection con - A database connection
	 * @param lon dnaId - A dna Id
	 * @throws Exception
	 */
	private void createRMFromDna(Connection con, long dnaId)
		throws Exception
	{
		long dfltLangId = DefaultLanguageHelper.fetchDefaultLanguageId();
		
		// Get data from DNA and put it into a Remote Model Structure DTO
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT DISTINCT dl.groupId as sfId, ");
		sqlQuery.append("                txt1.textId as sfTxtId, ");
		sqlQuery.append("                txt1.text as sfName, ");
		sqlQuery.append("                dl.groupSeq, ");
		sqlQuery.append("                dl.competencyId as cmpId, ");
		sqlQuery.append("                txt2.textId as cmpTxtId, ");
		sqlQuery.append("                txt2.text as cmpName, ");
		sqlQuery.append("                dl.compSeq ");
		sqlQuery.append("  FROM pdi_abd_dna_link dl ");
		sqlQuery.append("    LEFT JOIN pdi_abd_dna_grouping_value dgv ON dgv.groupId = dl.groupId ");
		sqlQuery.append("    LEFT JOIN pdi_abd_text txt1 ON (txt1.textId = dgv.textId AND txt1.languageId = " + dfltLangId + ") ");
		sqlQuery.append("    LEFT JOIN pdi_abd_competency cmp ON cmp.competencyId = dl.competencyId ");
		sqlQuery.append("    LEFT JOIN pdi_abd_text txt2 ON (txt2.textId = cmp.textId AND txt2.languageId = " + dfltLangId + ") ");
		sqlQuery.append("  WHERE dl.dnaId = " + dnaId + " ");
		sqlQuery.append("    AND dl.active = 1 ");
		sqlQuery.append("    AND dl.isUsed = 1 ");
		sqlQuery.append("  ORDER BY dl.groupSeq, dl.compSeq");

		try
		{
			stmt = con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			if (! rs.isBeforeFirst())
			{
				// No data... nothing to do
				return;
			}
			
			// Build the Structure.  It can be simple here because each row is a new
			// competency and there will be a simgle comp child per competency.
			// Note that sequences are "1" based
			ReportModelStructureDTO rm = new ReportModelStructureDTO();
			rm.setDnaId(dnaId);
			rm.setLangId(dfltLangId);
			
			int curSfId = 0;
			int sfSeq = 0;
			int compSeq = 0;
			ReportModelSuperFactorDTO curSf = null;
			while(rs.next())
			{
				int sf = rs.getInt("sfId");
				if (sf != curSfId)
				{
					// New SF
					sfSeq++;	// increment the sequence
					curSf = new ReportModelSuperFactorDTO();
					curSf.setSuperFactorId(0);
					curSf.setDefaultTextId(rs.getLong("sfTxtId"));
					curSf.setUserTextId(0L);
					curSf.setDisplayName(rs.getString("sfName"));
					curSf.setSeq(sfSeq);
					rm.getSuperFactorList().add(curSf);
					curSfId = sf;
					compSeq = 0;	// reset the competency sequencer
				}

				// Build the competency and put it into the list
				compSeq++;
				ReportModelCompetencyDTO cmp = new ReportModelCompetencyDTO();
				cmp.setCompetencyId(0);
				cmp.setDefaultTextId(rs.getLong("cmpTxtId"));
				cmp.setUserTextId(0L);
				cmp.setDisplayName(rs.getString("cmpName"));
				cmp.setSeq(compSeq);
				ReportModelCompChildDTO cc = new ReportModelCompChildDTO();
				cc.setRmCompId(0);
				cc.setDnaCompId(rs.getLong("cmpId"));
				cmp.getChildren().add(cc);
				curSf.getCompList().add(cmp);
			}
			
			// OK now insert it into the rm tables using the standard update
			updateReportModelStructure(con, rm);
			return;
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL createReportModelFromDna.  " +
        			"SQLException: " + ex.getMessage() + ", " +
        			"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (rs != null)
			{
				try	{  rs.close();  }
				catch (SQLException sqlEx)  {  }
				rs = null;
			}
			if (stmt != null)
			{
				try	{  stmt.close();  }
				catch (SQLException sqlEx)  {  }
				stmt = null;
			}
		}
	}


	/**
	 * replaceReportModule - This updates the current Report Model.  The
	 *                       name is a carry over from when the model was
	 *                       bodily deleted and replaced every time it was updated.
	 * 
	 * @param con A database Connection object
	 * @param new Model A ReportModelStructureDTO containing a Report Model structure
	 * @throws Exception
	 */
	public void replaceReportModel(Connection con, ReportModelStructureDTO newModel)
	throws Exception
	{
		updateReportModelStructure(con, newModel, DEL_NON_DFLT_TXT);
	
		return;
	}
	
	public void replaceReportModel(Connection con, ReportModelStructureDTO newModel, boolean delFlg)
	throws Exception
	{
		updateReportModelStructure(con, newModel, delFlg);
	
		return;
	}


	/**
	 * updateReportModelStructure is the primary RM/CC
	 * data updater/creator.
	 * 
	 * @param Connection con - the database connection
	 * @param ReportModelStructureDTO struct
	 * @throws Exception
	 */
	 private void updateReportModelStructure(Connection con, ReportModelStructureDTO struct)
		throws Exception
	{
		 updateReportModelStructure(con, struct, DEL_NON_DFLT_TXT);
		 return;
	}
		 
	private void updateReportModelStructure(Connection con, ReportModelStructureDTO struct, boolean delFlg)
		throws Exception
	{
		long dfltLangId = DefaultLanguageHelper.fetchDefaultLanguageId();
		 // Set up stuff for later use
		long langId = struct.getLangId();
		if(langId == 0)
			langId = dfltLangId;
		long dnaId = struct.getDnaId();
		if (dnaId == 0L)
			throw new Exception("Cannot update a Report Model for DNA ID = 0");
		
		Statement stmt = null;
		ResultSet rs = null;
		try
		{
			stmt = con.createStatement();
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT COUNT(*) AS dCnt ");
			sql.append("  FROM pdi_abd_dna dna ");
			sql.append("  WHERE dna.dnaId = " + dnaId);
			rs = stmt.executeQuery(sql.toString());
			rs.next();
			int cnt = rs.getInt("dCnt");
			if (cnt < 1)
			{
				throw new Exception("Dna ID " + dnaId + " does not exist.  RM/CC ends.");
			}
		}
		catch (SQLException e)
		{
			throw new SQLException("Error on DNA fetch.  ID=" + dnaId + ".  msg=" + e.getMessage(), e);
		}
		finally
		{
			if (rs != null)
			{
				try  { rs.close();  }
				catch (Exception e) {  }
				rs = null;
			}
			if (stmt != null)
			{
				try  { stmt.close();  }
				catch (Exception e) {  }
				stmt = null;
			}
		}

		// Read up the existing data from the table into a reference structure.  This
		// structure contains the data as it currently exists for later reference.
		HashMap<Long, ReportModelSuperFactorDTO> refSfMap = new HashMap<Long, ReportModelSuperFactorDTO>();
		HashMap<Long, ReportModelCompetencyDTO> refCmpMap = new HashMap<Long, ReportModelCompetencyDTO>();
		HashMap<Long, Long> cmpSfXref = new HashMap<Long, Long>();	// What SF do the comps belong to
		ReportModelStructureDTO refDto = getReportModel(con, struct.getDnaId(), langId);
		refDto.getSuperFactorList();  // If there is a List, no harm.  Otherwise, one is made
		for (ReportModelSuperFactorDTO sf : refDto.getSuperFactorList())
		{
			// put it in the SF ref Map
			refSfMap.put(sf.getSuperFactorId(), sf);
			
			// and put its comps in the comp ref Map and save the comp/sf xref as well (by comp)
			for (ReportModelCompetencyDTO cmp : sf.getCompList())
			{
				refCmpMap.put(cmp.getCompetencyId(), cmp);
				cmpSfXref.put(cmp.getCompetencyId(), sf.getSuperFactorId());
			}
		}
		
		// Process the input data
		HashSet<Long> incSf = new HashSet<Long>();
		HashSet<Long> incCmp = new HashSet<Long>();
		for (ReportModelSuperFactorDTO sf : struct.getSuperFactorList())
		{
			long sfId = sf.getSuperFactorId();
			
			// Explanation for the error reported in NHN-1773:
			// If the user moves a superfactor from the right (active area) to the left (hold
			// area, all of the relevant information in the UI buffer (including IDs) is moved
			// with it.  If the user then presses the "Save" button, the SF and its associated
			// competencies are removed from the database.  The buffers in the UI, however, are
			// not updated, leaving the old IDs for removed  superfactors and thier competencies
			// in the UI hold area buffers.  If the user then moves a removed SF back into the
			// active area, all of those ids that have been retained are moved as well and when
			// this logic does its checks for new ("if(sfId == 0L)") or update (the else), it
			// thinks that there is data in the DB when there is not, causing it to try to
			// update a superfactor (and its competencies) when it should be re-inserting them.
			// Since the data is NOT in the db, the update fails, giving the error message noted
			// in the JIRA task.
			// SOLUTION:
			// if a superfactor ID exists, validate it against the sf IDs in the reference copy.
			// If it is not there, assume the above described situation and wipe out the id in
			// the sf and its competencies so that the pre-existing logic works.
			
			// If the ID is not zero, check it against the reference copy
			boolean foundSfId = false;
			if (sfId != 0L)
			{
				// See if it exists in the reference list
				for (ReportModelSuperFactorDTO sFac : refDto.getSuperFactorList())
				{
					if (sfId == sFac.getSuperFactorId())
					{
						foundSfId = true;
						break;
					}
				}
			}
			
			// If we did not find it, wipe the ids in the structure
			if (! foundSfId)
			{
				sfId = 0L;
				sf.setSuperFactorId(0L);
				for (ReportModelCompetencyDTO cc : sf.getCompList())
				{
					cc.setCompetencyId(0L);
				}
			}
			
			// Now resume the regular program
			if(sfId == 0L)
			{
				// insert the superfactor (returns the new sfId)
				sfId = insertSf(con, dnaId, langId, sf, delFlg);
				sf.setSuperFactorId(sfId);
			}
			else
			{
				// update the superfactor
				updateSf(con, dnaId, langId, sf, refSfMap.get(sf.getSuperFactorId()));
			}
			
			// loop through the competencies in this SF
			for (ReportModelCompetencyDTO cmp : sf.getCompList())
			{
				long cmpId = cmp.getCompetencyId();
				if (cmpId == 0L)
				{
					// Insert the competency
					cmpId = insertComp(con, dnaId, langId, sfId, cmp);
					cmp.setCompetencyId(cmpId);
				}
				else
				{
					// Update the competency
					updateComp(con, dnaId, langId, cmp, sfId, refCmpMap.get(cmpId), cmpSfXref.get(cmpId));
				}
				
				// stick the comp ID in the incoming reference set
				incCmp.add(cmpId);
			}	// End of competency loop
			
			// Stick the sf id in the incoming reference set
			incSf.add(sfId);
		}	// End of superfactor loop
		
		// Prepare for old stuff deletion
		deleteOldSFs(con, refSfMap, incSf);
		deleteOldComps(con, refCmpMap, incCmp);
	}


	/**
	 * insertSf - insert a new SuperFactor
	 *
	 * @param con A connection to the database
	 * @param dnaId A dnaId for which to get the report model
	 * @param langId A language ID for the text
	 * @param sf A ReportModelSuperFactorDTO object
	 * @return T
	 * @throws Exception
	 */
//	 private long insertSf(Connection con, long dnaId, long langId, ReportModelSuperFactorDTO sf)
//		throws Exception
//	{
//		 return insertSf(con, dnaId, langId, sf, DO_DEL_TXT);
//	}
		private long insertSf(Connection con, long dnaId, long langId, ReportModelSuperFactorDTO sf, boolean delFlg)
			throws Exception
		{
		long sfId = 0;
		Statement stmt = null;
		StringBuffer sql = new StringBuffer();
		ResultSet rs = null;
		String phase = "";

		// Play with the text
		long tid = textyStuff(con,
				              langId,
				              sf.getDefaultTextId(),
				              sf.getUserTextId(),
				              sf.getDisplayName(), delFlg);
		sf.setUserTextId(tid);

		try
		{
			 stmt = con.createStatement();

			// Done with the text stuff... Insert the actual new SF
			phase = "Insert the SF";
			sql.append("INSERT INTO pdi_abd_rm_superfactor ");
			sql.append("        (dnaId, defaultTextId, userTextId, sequence, lastUserId, lastDate) ");
	 		sql.append("  VALUES(" + dnaId + ", " + sf.getDefaultTextId() + ", " + sf.getUserTextId() + ", " + sf.getSeq() + ", SYSTEM_USER, GETDATE())");
			stmt.executeUpdate(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			rs = stmt.getGeneratedKeys();
			rs.next();
			sfId = rs.getLong(1);
			sf.setSuperFactorId(sfId);

			return sfId;
		}
		catch (SQLException e)
		{
			throw new SQLException("Error in phase: " + phase +".  Msg=" + e.getMessage(), e);
		}
		finally
		{
			if (rs != null)
			{
				try  { rs.close();  }
				catch (Exception rse) {  }
				rs = null;
			}
			if (stmt != null)
			{
				try  { stmt.close();  }
				catch (Exception rse) {  }
				rs = null;
			}
		}
 	}


 	/**
 	 * updateSf - Update a SuperFactor... Does not go down the tree to competencies
 	 *
 	 * @param con A connection to the database
 	 * @param dnaId A dnaId for which to get the report model
 	 * @param langId A language ID for the text
 	 * @param sf A ReportModelSuperFactorDTO object with (possible) changes
 	 * @param sf A ReportModelSuperFactorDTO object for reference
 	 * @return T
 	 * @throws Exception
 	 */
	 private void updateSf(Connection con,
			 			   long dnaId,
			 			   long langId,
						   ReportModelSuperFactorDTO sf,
						   ReportModelSuperFactorDTO refSf)
		throws Exception
	{
		 // First do text related stuff
		long tid = textyStuff(con,
							  langId,
							  sf.getDefaultTextId(),
							  sf.getUserTextId(),
							  sf.getDisplayName());
		sf.setUserTextId(tid);

		// Then do the update checking
		boolean changed = false;

		if (refSf == null)
		{
			// Shouldn't be, but check it
			throw new Exception("Unable to update... No reference SF found for id=" + sf.getSuperFactorId());
		}
		else
		{
			// Check to see if relevant data changed
			if (refSf.getUserTextId() != sf.getUserTextId()  ||
				refSf.getSeq() != sf.getSeq())
			{
				changed = true;
			}
		}
		 
		// data changed?
		if(! changed)
		{
			// No change... Our job is done here
			return;
		}
		
		// Yes it did.  Update the row...
		StringBuffer sql = new StringBuffer();
		Statement stmt = null;
		sql.append("UPDATE pdi_abd_rm_superfactor ");
		sql.append("  SET userTextId = " + sf.getUserTextId() + ", ");
		sql.append("      sequence = " + sf.getSeq() + ", ");
		sql.append("      lastDate = GETDATE() ");
		sql.append("  WHERE dnaId = " + dnaId + " ");
		sql.append("    AND sfId = " + sf.getSuperFactorId());
		
		try
		{
			stmt = con.createStatement();
			stmt.executeUpdate(sql.toString());  
		}
		catch (SQLException e)
		{
			throw new SQLException("Error updating superfactor ID " + sf.getSuperFactorId() + ".  Msg=" + e.getMessage(), e);
		}
		finally
		{
			if (stmt != null)
			{
				try  { stmt.close();  }
				catch (Exception rse) {  }
				stmt = null;
			}
		}
  	}


	/**
	 * insertComp - insert a new Competency
	 *
	 * @param con A connection to the database
	 * @param dnaId A dnaId for which to get the report model
	 * @param langId A language ID for the text
	 * @param sfId The ID of the superfactor to which this competency belongs
	 * @param sf A ReportModelSuperFactorDTO object
	 * @return T
	 * @throws Exception
	 */
	private long insertComp(Connection con, long dnaId, long langId, long sfId, ReportModelCompetencyDTO cmp)
		throws Exception
	{
		long cmpId = 0;
		Statement stmt = null;
		PreparedStatement ps = null;
 		StringBuffer sql = new StringBuffer();
		ResultSet rs = null;
		String phase = "";

		// Play with the texts if needed
		long tid = textyStuff(con,
									langId,
									cmp.getDefaultTextId(),
									cmp.getUserTextId(),
									cmp.getDisplayName());
		cmp.setUserTextId(tid);

		try
		{
			stmt = con.createStatement();
			
			// Do the competency
			phase = "Insert the competency";
			sql.append("INSERT INTO pdi_abd_rm_competency ");
			sql.append("       (dnaId, sfId, defaultTextId, userTextId, sequence, essential, lastUserId, lastDate) ");
			sql.append("VALUES (" + dnaId + ", " + sfId + ", " + cmp.getDefaultTextId() + ", " + cmp.getUserTextId() + ", " + cmp.getSeq() + ", " + (cmp.getEssential() ? 1 : 0) + ", SYSTEM_USER, GETDATE())");
			stmt.executeUpdate(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			rs = stmt.getGeneratedKeys();
			rs.next();
			cmpId = rs.getLong(1);
			cmp.setCompetencyId(cmpId);
			
			// Clean up
			sql.setLength(0);
			try {  rs.close();  }
			catch (Exception e) {  }
			rs = null;

			sql.append("INSERT INTO pdi_abd_rm_comp_children ");
			sql.append("       (dnaId, rmCompId, competencyId, lastUserId, lastDate) ");
			sql.append("VALUES (" + dnaId + ", " + cmpId + ", ?, SYSTEM_USER, GETDATE())");
			phase = "Comp Child Insert stmt";
			ps = con.prepareStatement(sql.toString());
			// Loop through the children
			for (ReportModelCompChildDTO cc : cmp.getChildren())
			{
				phase = "CC child parameter binding";
				ps.setLong(1, cc.getDnaCompId());
				phase = "CC child execute";
				ps.execute();
			}

			return cmpId;
		}
		catch (SQLException e)
		{
			throw new SQLException("Error in phase: " + phase +".  Msg=" + e.getMessage(), e);
		}
		finally
		{
			if (rs != null)
			{
				try  { rs.close();  }
				catch (Exception rse) {  }
				rs = null;
			}
			if (stmt != null)
			{
				try  { stmt.close();  }
				catch (Exception rse) {  }
				stmt = null;
			}
			if (ps != null)
			{
				try  { ps.close();  }
				catch (Exception rse) {  }
				ps = null;
			}
		}
 	}


 	/**
 	 * updateComp - Update a SuperFactor... Does not go down the tree to competencies
 	 *
 	 * @param con A connection to the database
 	 * @param dnaId A dnaId for which to get the report model
 	 * @param langId A language ID for the text
 	 * @param sf A ReportModelSuperFactorDTO object with possible changes
 	 * @param cmpSfId The superfactor to which the input competency belongs
 	 * @param sf A ReportModelSuperFactorDTO object for reference
 	 * @param refSfId The superfactor to which the reference competency belongs
 	 * @throws Exception
 	 */
	private void updateComp(Connection con,
			 			    long dnaId,
			 			    long langId,
			 			    ReportModelCompetencyDTO cmp,
			 			    long cmpSfId,
			 			    ReportModelCompetencyDTO refCmp,
			 			    long refSfId)
	throws Exception
	{
		long tid = textyStuff(con,
							  langId,
							  cmp.getDefaultTextId(),
							  cmp.getUserTextId(),
							  cmp.getDisplayName());
		cmp.setUserTextId(tid);

		boolean changed = false;

		if (refCmp == null)
		{
			// Shouldn't be, but check it
			System.out.println("no reference competency found for id=" + cmp.getCompetencyId());
			changed = true;
		}
		else
		{
			// Check to see if relevant data changed
			if (refCmp.getUserTextId() != cmp.getUserTextId() ||
			    refCmp.getSeq() != cmp.getSeq()  ||
				refCmp.getEssential() != cmp.getEssential()  ||
				refSfId != cmpSfId)
			{
				changed = true;
			}
		}

		Statement stmt = null;
		PreparedStatement ps = null;
		StringBuffer sql = new StringBuffer();
		String phase = "";
		try
		{
			stmt = con.createStatement();
			
			if(changed)
			{
				// Yes it did.  Update the row...
				phase = "Updating comp data for comp ID " + cmp.getCompetencyId();
				sql.append("UPDATE pdi_abd_rm_competency ");
				sql.append("  SET userTextId = " + cmp.getUserTextId() + ", ");
				sql.append("      sequence = " + cmp.getSeq() + ", ");
				sql.append("      essential = " + (cmp.getEssential() ? 1 : 0) + ", ");
				sql.append("      sfId = " + cmpSfId + " ");
				sql.append("  WHERE dnaId = " + dnaId + " ");
				sql.append("    AND rmCompId = " + cmp.getCompetencyId());
				stmt.executeUpdate(sql.toString());  
			}
		
			// Now do the comp children...  Just gonna replace in whole
			sql.setLength(0);
			phase = "Delete comp children for RM/CC comp ID " + cmp.getCompetencyId();
			sql.append("DELETE FROM pdi_abd_rm_comp_children ");
			sql.append("  WHERE dnaId = " + dnaId);
			sql.append("    AND rmCompId = " + cmp.getCompetencyId());
			stmt.executeUpdate(sql.toString());

			sql.setLength(0);
			phase = "Prepare insert for comp children";
			sql.append("INSERT INTO pdi_abd_rm_comp_children ");
			sql.append("       (dnaId, rmCompId, competencyId, lastUserId, lastDate) ");
			sql.append("VALUES (" + dnaId + ", " + cmp.getCompetencyId() + ", ?, SYSTEM_USER, GETDATE())");
			ps = con.prepareStatement(sql.toString());
			// Loop through the children
			for (ReportModelCompChildDTO cc : cmp.getChildren())
			{
				phase = "CC child parameter binding";
				ps.setLong(1, cc.getDnaCompId());
				phase = "CC child execute";
				ps.execute();
			}

		}
		catch (SQLException e)
		{
			throw new SQLException("Error updating competency ID " + cmp.getCompetencyId() + ".  Phase=" + phase + ", msg=" + e.getMessage(), e);
		}
		finally
		{
			if (stmt != null)
			{
				try  { stmt.close();  }
				catch (Exception rse) {  }
				stmt = null;
			}
			if (ps != null)
			{
				try  { ps.close();  }
				catch (Exception rse) {  }
				ps = null;
			}
		}
 	}


	/**
	 * textyStuff - Text logic associated with both
	 * superfactor and competency
	 *
	 * @param con A connection to the database
	 * @param langId A language ID for the text
	 * @param staticTextId The static text ID from the item
	 * @param userTextId The user text ID from the item
	 * @return the user text ID (used if update, generated in new insert
	 * @throws Exception
	 */
	// Convenience method - The old school way this was called.
	// Always deleted everything but the default language
	private long textyStuff(Connection con,
             long langId,
             long staticTextId,
             long userTextId,
             String theText)
	 	throws Exception
	{
		return textyStuff(con, langId, staticTextId, userTextId, theText, DEL_NON_DFLT_TXT);
	}
	
	// Actual method.  Allows us to NOT delete the other texts.  Used in clone only at this time.
	private long textyStuff(Connection con,
                 long langId,
                 long staticTextId,
                 long userTextId,
                 String theText,
                 boolean delFlg)
		throws Exception
	{
		long dfltLangId = DefaultLanguageHelper.fetchDefaultLanguageId();
		
		long newId = userTextId;	// Initialize as if there were no new id
		Statement stmt = null;
		StringBuffer sql = new StringBuffer();
		ResultSet rs = null;
		String phase = "";
		
		theText = theText.trim();
		// Handle embedded single quotes; replace with paired single quotes
		theText = theText.replace("'", "''");
		  
		// Add escapement to tags
			//Step 1, to prevent double escaping
		theText = theText.replaceAll("&amp;","&");
		theText = theText.replaceAll("&lt;","<");
		theText = theText.replaceAll("&gt;",">");
			//Step 2, escape
		theText = theText.replaceAll("&", "&amp;");
		theText = theText.replaceAll("<", "&lt;");
		theText = theText.replaceAll(">", "&gt;");

		try
		{
			stmt = con.createStatement();

			// First see if there is a user text id
			if (userTextId != 0L)
			{
				// There is... update it
				// But the update may be of a new language...
				// see if it exists in the language
				sql.setLength(0);
				phase = "Check user text existence";
				sql.append("SELECT COUNT(*) AS tCnt ");
				sql.append("  FROM user_text ut ");
				sql.append("  WHERE ut.userTextId = " + userTextId + "");
				sql.append("    AND languageId = " + langId);
				rs = stmt.executeQuery(sql.toString());
				rs.next();
				int cnt = rs.getInt("tCnt");
				// clean up
				sql.setLength(0);
				try {  rs.close();  }
				catch (Exception e) {  }
				rs = null;
				
				if (cnt > 0)
				{
					// It is there... update it
					phase = "Update user text.  Id=" + userTextId + ", langId=" + langId;
					sql.append("UPDATE user_text ");
					sql.append("  SET userText = N'" + theText + "' ");
					sql.append("  WHERE userTextId = " + userTextId + " ");
					sql.append("    AND languageId = " + langId);
					stmt.executeUpdate(sql.toString());  
					
					// If its default lang (en) delete the rest
					if (langId == dfltLangId && delFlg)
					{
						phase = "Delete texts on English update.  userTextId=" + userTextId;
						sql.setLength(0);
						sql.append("DELETE FROM user_text ");
						sql.append("  WHERE userTextId = " + userTextId + " ");
						sql.append("    AND languageId != " + dfltLangId);
						stmt.executeUpdate(sql.toString());
					}
				}
				else
				{
					int dflt = (langId == dfltLangId ? 1 : 0);
					// not there... insert it  (new language but existing item.  We have the userTextId
					phase = "Insert text.  Text=" + theText + ", langId=" + langId;
					sql.append("INSERT INTO user_text ");
					sql.append("  (userTextId, languageId, typeCode, userText, isDefault, lastUserId, lastDate) ");
					sql.append("  VALUES (" + userTextId + ", " + langId + ", 'C', N'" + theText + "', " + dflt + ", SYSTEM_USER, GETDATE())");
					stmt.executeUpdate(sql.toString());
				}
			}
			else
			{
				// userText ID IS == 0L... We have no user text... check the static
				if (staticTextId == 0L)
				{
					// insert a brand new user text.  We make that assumption because both
					// the user text and the static text ids were 0 (no text data available)
					int dflt = (langId == dfltLangId ? 1 : 0);
					// not there... insert it
					phase = "Insert new text.  Text=" + theText + ", langId=" + langId;
					sql.append("INSERT INTO user_text ");
					sql.append("  (languageId, typeCode, userText, isDefault, lastUserId, lastDate) ");
					sql.append("  VALUES (" + langId + ", 'C', N'" + theText + "', " + dflt + ", SYSTEM_USER, GETDATE())");
					stmt.executeUpdate(sql.toString(), Statement.RETURN_GENERATED_KEYS);
					rs = stmt.getGeneratedKeys();
					rs.next();
					long tid = rs.getLong(1);
					// clean up and put in the userTextId
					sql.setLength(0);
					try {  rs.close();  }
					catch (Exception e) {  }
					rs = null;
					phase = "Adding userTextId.  id=" + tid;
					sql.append("UPDATE user_text ");
					sql.append("  SET userTextId = " + tid + " ");
					sql.append("  WHERE uniqueId = " + tid);
					stmt.executeUpdate(sql.toString());
					
					// Set up ID for return
					newId = tid;
				}
				else
				{
					// get the original text string
					sql.append("SELECT txt.text AS txt");
					sql.append("  FROM pdi_abd_text txt ");
					sql.append("  WHERE txt.textId = " + staticTextId + " ");
					sql.append("    AND txt.languageId = " + langId);
					rs = stmt.executeQuery(sql.toString());
					rs.next();
					String orig = rs.getString("txt").trim();
					if (! orig.equals(theText))
					{
						// Not the same... create a new user text

						// clean up
						sql.setLength(0);
						try {  rs.close();  }
						catch (Exception e) {  }
						rs = null;
						
						// Insert it
						int dflt = (langId == dfltLangId ? 1 : 0);
						phase = "Insert new text.  Text=" + theText + ", langId=" + langId;
						sql.append("INSERT INTO user_text ");
						sql.append("  (languageId, typeCode, userText, isDefault, lastUserId, lastDate) ");
						sql.append("  VALUES (" + langId + ", 'C', N'" + theText + "', " + dflt + ", SYSTEM_USER, GETDATE())");
						stmt.executeUpdate(sql.toString(), Statement.RETURN_GENERATED_KEYS);
						rs = stmt.getGeneratedKeys();
						rs.next();
						long tid = rs.getLong(1);
						// clean up and put in the userTextId
						sql.setLength(0);
						try {  rs.close();  }
						catch (Exception e) {  }
						rs = null;
						phase = "Adding userTextId.  id=" + tid;
						sql.append("UPDATE user_text ");
						sql.append("  SET userTextId = " + tid + " ");
						sql.append("  WHERE uniqueId = " + tid);
						stmt.executeUpdate(sql.toString());
						
						// Set up ID for return
						newId = tid;
					}
				}
			}	// End of "if (userTextId != 0L)"

			return newId;
		}
		catch (SQLException e)
		{
			throw new Exception("Error in phase: " + phase +".  Msg=" + e.getMessage(), e);
		}
		finally
		{
			if (rs != null)
			{
				try  { rs.close();  }
				catch (Exception rse) {  }
				rs = null;
			}
			if (stmt != null)
			{
				try  { stmt.close();  }
				catch (Exception rse) {  }
				rs = null;
			}
		}
 	}


	/*
	 * deleteOldSFs checks and, if needed deletes RM Superfactors and associated text strings.
	 * 
	 * @param con - A database Connection object
	 * @param refSfMap  A HashMap of reference (old) ReportModelStructureDTO objects with
	 *                  a key of the superfactor id
	 * @param incSf A HashSet of the incoming (update) superfactor ids
	 * @throws Exception
	 */
	private void deleteOldSFs(Connection con,
							  HashMap<Long, ReportModelSuperFactorDTO> refSfMap,
							  HashSet<Long> incSf)
		throws Exception
	{
		PreparedStatement sfPs = null;
		PreparedStatement txtPs = null;
		String phase = "";
		
		try
		{
			StringBuffer sqlQuery = new StringBuffer();
			phase = "Preparing SF delete PreparedStatement";
			sqlQuery.append("DELETE FROM pdi_abd_rm_superfactor ");
			sqlQuery.append("  WHERE sfId = ?");
			sfPs = con.prepareStatement(sqlQuery.toString());
			
			sqlQuery.setLength(0);
			phase = "Preparing SF user text delete PreparedStatement";
			sqlQuery.append("DELETE FROM user_text ");
			sqlQuery.append("  WHERE typeCode = 'C' ");
			sqlQuery.append("    AND userTextId = ?");
			txtPs = con.prepareStatement(sqlQuery.toString());

			// See if old superfactors need to be deleted
			for (Iterator<Map.Entry<Long, ReportModelSuperFactorDTO>> itr = refSfMap.entrySet().iterator(); itr.hasNext();  )
			{
				Map.Entry<Long, ReportModelSuperFactorDTO> ent = itr.next();
				// See if the SF ID exists in the incoming data list
				if (incSf.contains(ent.getKey()))
				{
					// Nothing to do
					continue;
				}
				
				// Delete only the old SF and its relevant data.  Competencies could have
				// moved so we will delete only this SF and its related user texts.
				// Competencies will be dealt with separately
				ReportModelSuperFactorDTO dto = ent.getValue();
				long sfId = dto.getSuperFactorId();
				long txtId = dto.getUserTextId();
				
				// Delete the SF
				phase = "Superfactors delete binding";
				sfPs.setLong(1, sfId);
				phase = "SF delete execute";
				sfPs.execute();
				
				// Delete the text
				if (txtId != 0L)
				{
					phase = "Superfactors text delete binding";
					txtPs.setLong(1, txtId);
					phase = "SF text delete execute";
					txtPs.execute();
				}
			}
		}
		catch (SQLException e)
		{
			throw new SQLException("Error in phase: " + phase +".  Msg=" + e.getMessage(), e);
		}
		finally
		{
			if (sfPs != null)
			{
				try  { sfPs.close();  }
				catch (Exception e) {  }
				sfPs = null;
			}
			if (txtPs != null)
			{
				try  { txtPs.close();  }
				catch (Exception e) {  }
				txtPs = null;
			}
		}
	}


	/*
	 * deleteOldComps checks and, if needed deletes RM  competencies and associated text strings.
	 * 
	 * @param con - A database Connection object
	 * @param refSfMap  A HashMap of reference (old) ReportModelStructureDTO objects with
	 *                  a key of the superfactor id
	 * @param incSf A HashSet of the incoming (update) superfactor ids
	 * @throws Exception
	 */
	private void deleteOldComps(Connection con,
								HashMap<Long, ReportModelCompetencyDTO> refCmpMap,
								HashSet<Long> incCmp)
		throws Exception
	{
		PreparedStatement cmpPs = null;
		PreparedStatement ccPs = null;
		PreparedStatement txtPs = null;
		String phase = "";

		try
		{
			StringBuffer sqlQuery = new StringBuffer();
			phase = "Preparing comp delete PreparedStatement";
			sqlQuery.append("DELETE FROM pdi_abd_rm_competency ");
			sqlQuery.append("  WHERE rmCompId = ?");
			cmpPs = con.prepareStatement(sqlQuery.toString());

			sqlQuery.setLength(0);
			phase = "Preparing comp children delete PreparedStatement";
			sqlQuery.append("DELETE FROM pdi_abd_rm_comp_children ");
			sqlQuery.append("  WHERE rmCompId = ?");
			ccPs = con.prepareStatement(sqlQuery.toString());
			
			sqlQuery.setLength(0);
			phase = "Preparing comp user text delete PreparedStatement";
			sqlQuery.append("DELETE FROM user_text ");
			sqlQuery.append("  WHERE typeCode = 'C' ");
			sqlQuery.append("    AND userTextId = ?");
			txtPs = con.prepareStatement(sqlQuery.toString());

			// See if old competencies need to be deleted
			for (Iterator<Map.Entry<Long, ReportModelCompetencyDTO>> itr = refCmpMap.entrySet().iterator(); itr.hasNext();  )
			{
				Map.Entry<Long, ReportModelCompetencyDTO> ent = itr.next();
				// See if the SF ID exists in the incoming data list
				if (incCmp.contains(ent.getKey()))
				{
					// Nothing to do
					continue;
				}

				// Delete only the old competency and its relevant data.  That data is
				// the competency itself, its user text and its "child" competencies.
				ReportModelCompetencyDTO dto = ent.getValue();
				long cmpId = dto.getCompetencyId();
				long txtId = dto.getUserTextId();

				// Delete the Competency
				phase = "Competency delete binding";
				cmpPs.setLong(1, cmpId);
				phase = "Cmp delete execute";
				cmpPs.execute();
				
				// Delete the child competencies
				phase = "Competency child delete binding";
				ccPs.setLong(1, cmpId);
				phase = "Comp child delete execute";
				ccPs.execute();

				// Delete the text
				if (txtId != 0L)
				{
					phase = "Competency text delete binding";
					txtPs.setLong(1, txtId);
					phase = "Competency text delete execute";
					txtPs.execute();
				}
			}
		}
		catch (SQLException e)
		{
			throw new SQLException("Error in phase: " + phase +".  Msg=" + e.getMessage(), e);
		}
		finally
		{
			if (cmpPs != null)
			{
				try  { cmpPs.close();  }
				catch (Exception e) {  }
				cmpPs = null;
			}
			if (ccPs != null)
			{
				try  { ccPs.close();  }
				catch (Exception e) {  }
				ccPs = null;
			}
			if (txtPs != null)
			{
				try  { txtPs.close();  }
				catch (Exception e) {  }
				txtPs = null;
			}
		}
	}


 	/**
 	 * getReportModel - Convenience method
 	 *
 	 * @param con A connection to the database
 	 * @param dnaId A dnaId for which to get the report model
 	 * @return A ReportModelStructureDTO object
 	 * @throws Exception
 	 */
 	 public ReportModelStructureDTO getReportModel(Connection con, long dnaId)
 		throws Exception
 	{
 		long dfltLangId = DefaultLanguageHelper.fetchDefaultLanguageId();
 		
 		return fetchReportModel(con, dnaId, dfltLangId, null);
 	}


 	/**
 	 * getReportModel - Return the Report model for a particular DNA.
 	 * 					No scores are returned from this call.
 	 *
 	 * @param con A connection to the database
 	 * @param dnaId A dnaId for which to get the report model
 	 * @param langId An A by D language id
 	 * @return A ReportModelStructureDTO object
 	 * @throws Exception
 	 */
 	 public ReportModelStructureDTO getReportModel(Connection con, long dnaId, long langId)
 		throws Exception
 	{
 		return fetchReportModel(con, dnaId, langId, null);
 	}


	/**
	 * fetchReportModel - Return the Report model for a particular DNA.
	 *
	 * @param  Connection con - A connection to the database
	 * @param  long dnaId - A dnaId for which to get the report model
 	 * @param langId An A by D language id
	 * @param  HashMap<Long, Double> compScores
	 * @return A ReportModelStructureDTO object
	 * @throws Exception
	 */
	 private ReportModelStructureDTO fetchReportModel(Connection con, long dnaId, long langId, HashMap<Long, Double> compScores)
		throws Exception
	{
		long dfltLangId = DefaultLanguageHelper.fetchDefaultLanguageId();
		
		Statement stmt = null;
		ResultSet rs = null;
		String phase = "";
		
		// Set up the return object
		ReportModelStructureDTO ret = new ReportModelStructureDTO();
		ret.setDnaId(dnaId);
		ret.setLangId(langId);
	
		// get the data
		StringBuffer sql = new StringBuffer();
		phase = "Getting RM data";
		sql.append("SELECT sf.sfId, ");
		sql.append("       sf.defaultTextId as sfDid, ");
		sql.append("       sf.userTextId as sfUid, ");
		sql.append("       sf.sequence as sfSeq, ");
		sql.append("       cmp.rmCompId, ");
		sql.append("       cmp.defaultTextId as cmpDid, ");
		sql.append("       cmp.userTextId as cmpUid, ");
		sql.append("       cmp.sequence as cmpSeq, ");
		sql.append("       cmp.essential, ");
		sql.append("       cc.competencyId, ");
		sql.append("       txt.text as cmpRealName ");
		sql.append("  FROM pdi_abd_rm_superfactor sf ");
		sql.append("    INNER JOIN pdi_abd_rm_competency cmp ON (cmp.dnaId = sf.dnaId AND cmp.sfId = sf.sfId) ");
		sql.append("    INNER JOIN pdi_abd_rm_comp_children cc ON (cc.dnaId = cmp.dnaId AND cc.rmCompId = cmp.rmCompId) ");
		sql.append("    LEFT JOIN pdi_abd_competency co ON co.competencyId = cc.competencyId ");
		sql.append("    LEFT JOIN pdi_abd_text txt ON (txt.textId = co.textId AND txt.languageId = " + dfltLangId +") ");
		sql.append("  WHERE sf.dnaId = " + dnaId + " ");
		sql.append("  ORDER BY sf.sequence, sf.sfId, cmp.sequence, cmp.rmCompId");
		// Additional columns added to sort in case there are multiple copies
		// of the data... This should not happen, but it did at least once
		
		try
		{
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql.toString());
	
			if (! rs.isBeforeFirst())
			{
				// No data... nothing to do
				return ret;
			}
			
			ReportModelSuperFactorDTO curSf = null;
			ReportModelCompetencyDTO curCmp= null;
			int cmpCnt = 0;
			double cmpCum = 0.0;
			while(rs.next())
			{
				// Check the super-factor
				long sfId = rs.getLong("sfId");
				if (curSf == null || sfId != curSf.getSuperFactorId())
				{
					// Super-factor changed
					if (curCmp != null && cmpCnt != 0)
					{
						// calc the old (previous) comp score
						curCmp.setCompScore(calcScore(cmpCum, cmpCnt));
					}
					// Reset the buckets
					cmpCnt = 0;
					cmpCum = 0.0;

					// make a new sf
					curSf = new ReportModelSuperFactorDTO();
					curSf.setSuperFactorId(sfId);
					curSf.setDefaultTextId(rs.getLong("sfDid"));
					curSf.setUserTextId(rs.getLong("sfUid"));
					//curSf.setDisplayName(sfUid == 0L ? rs.getString("sfDtxt") : rs.getString("sfUtxt"));
					curSf.setSeq(rs.getInt("sfSeq"));
					ret.getSuperFactorList().add(curSf);
					curCmp = null;
				}	// End of if on curSf
				
				// check the competency
				long cmpId = rs.getLong("rmCompId");
				if (curCmp == null || cmpId != curCmp.getCompetencyId())
				{
					// Calc the old comp score
					if (curCmp != null && cmpCnt != 0)
					{
						curCmp.setCompScore(calcScore(cmpCum, cmpCnt));
					}
					// Reset the buckets
					cmpCnt = 0;
					cmpCum = 0.0;
					
					// make a new cmp
					curCmp = new ReportModelCompetencyDTO();
					curCmp.setCompetencyId(cmpId);
					curCmp.setDefaultTextId(rs.getLong("cmpDid"));
					curCmp.setUserTextId(rs.getLong("cmpUid"));
					curCmp.setSeq(rs.getInt("cmpSeq"));
					curCmp.setEssential(rs.getBoolean("essential"));
					curSf.getCompList().add(curCmp);
				}
				
				ReportModelCompChildDTO cc = new ReportModelCompChildDTO();
				cc.setRmCompId(cmpId);
				long cId = rs.getLong("competencyId");
				cc.setDnaCompId(cId);
				cc.setDnaCompName(rs.getString("cmpRealName"));	// Always use default real name text
				curCmp.getChildren().add(cc);
				if (compScores != null)
				{
					// Hmmm Why is the count updated even if there is no score?
					// Leaving it that way for now... missing comps will cause a really low score
					cmpCnt += 1;
					Double scor = compScores.get(new Long(cId));
					if (scor != null)
					{
						// changed for NHN-3003.  was this single line...
						//cmpCum += scor.doubleValue();
						// ...now is this if/else
						// Don't count competencies if they are N/A (the score is 0.0)
						if (scor == 0.0)
						{
							// Back out the cnt bump and don't add the 0.0
							cmpCnt -= 1;
						} else {
							// add in the score to the cumulative amount
							cmpCum += scor.doubleValue();
						}
					}
				}
			}	// End of while loop on the Result Set
			
			// calc the last comp score
			if (cmpCnt != 0)
				curCmp.setCompScore(calcScore(cmpCum, cmpCnt));
			
			// We have the data... now get the text
			// clean up
			phase = "Cleanup";
			rs.close();
			rs = null;
			sql.setLength(0);
			
			phase = "Getting texts";
			sql.append("SELECT 'S' AS typer, ");
			sql.append("       rms.sfId AS itemId, ");
			sql.append("       txt1.text AS enStxt, ");
			sql.append("       txt2.text AS lnStxt, ");
			sql.append("       txt3.userText AS enUtxt, ");
			sql.append("       txt4.userText AS lnUtxt ");
			sql.append("  FROM pdi_abd_rm_superfactor rms ");
			sql.append("    LEFT JOIN pdi_abd_text txt1 ON (txt1.textId = rms.defaultTextId AND txt1.languageId = " + dfltLangId + ") ");
			sql.append("    LEFT JOIN pdi_abd_text txt2 ON (txt2.textId = rms.defaultTextId and txt2.languageId = " + langId + ") ");
			sql.append("    LEFT JOIN user_text txt3 ON (txt3.userTextId = rms.userTextId AND txt3.languageId = " + dfltLangId + ") ");
			sql.append("    LEFT JOIN user_text txt4 ON (txt4.userTextId = rms.userTextId AND txt4.languageId = " + langId + ") ");
			sql.append("  WHERE rms.dnaId = " + dnaId + " ");
			sql.append("UNION ");
			sql.append("SELECT 'C' AS typer, ");
			sql.append("       rmc.rmCompId AS itemId, ");
			sql.append("       txt1.text AS enStxt, ");
			sql.append("       txt2.text AS lnStxt, ");
			sql.append("       txt3.userText AS enUtxt, ");
			sql.append("       txt4.userText AS lnUtxt ");
			sql.append("  FROM pdi_abd_rm_competency rmc ");
			sql.append("    LEFT JOIN pdi_abd_text txt1 ON (txt1.textId = rmc.defaultTextId AND txt1.languageId = " + dfltLangId + ") ");
			sql.append("    LEFT JOIN pdi_abd_text txt2 ON (txt2.textId = rmc.defaultTextId AND txt2.languageId = " + langId + ") ");
			sql.append("    LEFT JOIN user_text txt3 ON (txt3.userTextId = rmc.userTextId AND txt3.languageId = " + dfltLangId + ") ");
			sql.append("    LEFT JOIN user_text txt4 ON (txt4.userTextId = rmc.userTextId AND txt4.languageId = " + langId + ") ");
			sql.append("  WHERE rmc.dnaId = " + dnaId + " ");
			rs = stmt.executeQuery(sql.toString());
			
			if (! rs.isBeforeFirst())
			{
				// No data...
				throw new Exception("No text data found for DNA ID " + dnaId);
			}
			
			HashMap<Long, ItemTextData> sfItems = new HashMap<Long, ItemTextData>();
			HashMap<Long, ItemTextData> cmpItems = new HashMap<Long, ItemTextData>();
			while(rs.next())
			{
				ItemTextData itm =  new ItemTextData(rs.getLong("itemId"),
						 							 rs.getString("enStxt"),
						 							 rs.getString("lnStxt"),
						 							 rs.getString("enUtxt"),
						 							 rs.getString("lnUtxt"));
				if (rs.getString("typer").equals("S"))
				{
					// Superfactor item
					sfItems.put(itm.getItemId(), itm);
				}
				else
				{
					// Must be "C" - competency item
					cmpItems.put(itm.getItemId(), itm);
				}
			}			
			
			// Now put the appropriate text in
			ItemTextData itm = null;
			// Outer loop  - superfactors
			for (ReportModelSuperFactorDTO sf : ret.getSuperFactorList())
			{
				long sfId = sf.getSuperFactorId();
				itm = sfItems.get(sfId);
				if (sf.getUserTextId() == 0L)
				{
					// No user text... use the standard static text
					if (itm.getStaticLangTxt() == null)
					{
						// Fall all the way back to the default (English) static text
						sf.setDisplayName(itm.getStaticEnTxt());
					}
					else
					{
						// Use the static text in the specified language
						sf.setDisplayName(itm.getStaticLangTxt());
					}
				}
				else
				{
					// We have user entered text
					if (itm.getUserLangTxt() == null)
					{
						// Not available in the language.  Use the English.
						sf.setDisplayName(itm.getUserEnTxt());
					}
					else
					{
						// Use the user text in the language
						sf.setDisplayName(itm.getUserLangTxt());
					}
				}
				// Inner loop  - competencies within the superfactor
				for (ReportModelCompetencyDTO cmp : sf.getCompList())
				{
					long cmpId = cmp.getCompetencyId();
					itm = cmpItems.get(cmpId);
					if (cmp.getUserTextId() == 0L)
					{
						// No user text... use the standard static text
						if (itm.getStaticLangTxt() == null)
						{
							// Fall all the way back to the default (English) static text
							cmp.setDisplayName(itm.getStaticEnTxt());
						}
						else
						{
							// Use the static text in the specified language
							cmp.setDisplayName(itm.getStaticLangTxt());
						}
					}
					else
					{
						// We have user entered text
						if (itm.getUserLangTxt() == null)
						{
							// Not available in the language.  Use the English.
							cmp.setDisplayName(itm.getUserEnTxt());
						}
						else
						{
							// Use the user text in the language
							cmp.setDisplayName(itm.getUserLangTxt());
						}
					}
				}	// End of competency loop
			}	// End of superfactor loop

			return ret;
		}
		catch (SQLException ex)
		{
			// handle any errors
			String str = "SQL fetchReportModel, phase=" + phase + ".  " +
	    			"SQLException: " + ex.getMessage() + ", " +
	    			"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode();
			System.out.println(str);
			throw new Exception(str);
		}
		finally
		{
			if (rs != null)
			{
				try	{  rs.close();  }
				catch (SQLException sqlEx)
				{
					System.out.println("Error closing ResultSet in fetchReportModel.  Ignored.  " +
		        			"SQLException: " + sqlEx.getMessage() + ", " +
		        			"SQLState: " + sqlEx.getSQLState() + ", " +
							"VendorError: " + sqlEx.getErrorCode());
				}
				rs = null;
			}
			if (stmt != null)
			{
				try	{  stmt.close();  }
				catch (SQLException sqlEx)
				{
					System.out.println("Error closing RM Statement in fetchReportModel.  Ignored.  " +
		        			"SQLException: " + sqlEx.getMessage() + ", " +
		        			"SQLState: " + sqlEx.getSQLState() + ", " +
							"VendorError: " + sqlEx.getErrorCode());
				}
				stmt = null;
			}
		}
	}


	/**
	 * Calculate a score based upon the average of the scores  (cum and 
	 * count are passed in)
	 * The method is to calc the average, multiply it by 2, round it up by 1/2,
	 * take the highest integer at or under the current value, then divide it
	 * by 2 again.  This should produce numbers that are increments of 1/2.
	 * and 
	 * 
	 * @param double cum - cumulative score
	 * @param int cnt - the number of scores
	 * @return String - the score
	 */
	private  String calcScore(double cum, int cnt)
	{
		// Calc the average and multiply by 2
		double dbl = (cum / cnt) * 2;
		
		// round it and floor it, then divide it by 2
		dbl = (Math.floor(dbl + 0.5)) / 2.0;
	
		// Convert it to a string
		return "" + dbl;
	}


	/**
	 * getReportModelCompetencyChildren - Gets default data from the DNA.
	 * Note that the root children are always returned in English
	 * 
	 * @param Connection con - the database connection
	 * @param long dnaId - the dna Id
	 * @return ArrayList<ReportModelCompChildDTO>
	 * 
	 */
	public ArrayList<ReportModelCompChildDTO> getReportModelCompetencyChildren(Connection con, long dnaId)
		throws Exception
	{
		long dfltLangId = DefaultLanguageHelper.fetchDefaultLanguageId();

		ArrayList<ReportModelCompChildDTO>  compChildren = new ArrayList<ReportModelCompChildDTO> ();
		
		// Get data from DNA and put it into a Remote Model Structure DTO
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT DISTINCT dl.groupId as sfId, ");
		sqlQuery.append("                txt1.text as sfName,"); 
		sqlQuery.append("                dl.groupSeq, ");
		sqlQuery.append("                dl.competencyId as cmpId, ");
		sqlQuery.append("                txt2.text as cmpName, ");
		sqlQuery.append("                dl.compSeq ");
		sqlQuery.append("  FROM pdi_abd_dna_link dl ");
		sqlQuery.append("    LEFT JOIN pdi_abd_dna_grouping_value dgv ON dgv.groupId = dl.groupId ");
		sqlQuery.append("    LEFT JOIN pdi_abd_text txt1 ON (txt1.textId = dgv.textId AND txt1.languageId = " + dfltLangId + ") ");
		sqlQuery.append("    LEFT JOIN pdi_abd_competency cmp ON cmp.competencyId = dl.competencyId ");
		sqlQuery.append("    LEFT JOIN pdi_abd_text txt2 ON (txt2.textId = cmp.textId AND txt2.languageId = " + dfltLangId + ") ");
		sqlQuery.append("  WHERE dl.dnaId = " + dnaId + " ");
		sqlQuery.append("    AND dl.active = 1 ");
		sqlQuery.append("    AND dl.isUsed = 1 ");
		sqlQuery.append("  ORDER BY dl.groupSeq, dl.compSeq ");
		try
		{
			stmt = con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			if (! rs.isBeforeFirst())
			{
				// No data... nothing to do
				return compChildren;
			}

			while(rs.next())
			{
				ReportModelCompChildDTO compChild = new ReportModelCompChildDTO();
				compChild.setDnaCompId(rs.getInt("cmpId"));
				compChild.setDnaCompName(rs.getString("cmpName"));
				compChildren.add(compChild);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return compChildren;
	}

	
	/**
	 * getReportModelWithScores - Convenience method to enable use
	 *   until language is fully implemented
	 *
	 * @param con A connection to the database
	 * @param dnaId A dnaId for which to get the report model
	 * @param partId A participantId for whom to get the scores
	 * @return A ReportModelStructureDTO object
	 * @throws Exception
	 */
	 public ReportModelStructureDTO getReportModelWithScores(Connection con, long dnaId, String partId)
		throws Exception
	{
		long dfltLangId = DefaultLanguageHelper.fetchDefaultLanguageId();
		
		return fetchReportModelAndScores(con, dnaId, dfltLangId, partId, false);
	}


	/**
	 * getReportModelWithScores - Return the Report model for a particular DNA with
	 * 							  the scores for a particular participant.
	 * 							  No scores?  no model data is returned.
	 *
	 * @param con A connection to the database
	 * @param dnaId A dnaId for which to get the report model
	 * @param partId A participantId for whom to get the scores
	 * @return A ReportModelStructureDTO object
	 * @throws Exception
	 */
	 public ReportModelStructureDTO getReportModelWithScores(Connection con, long dnaId, int langId, String partId)
		throws Exception
	{
		return fetchReportModelAndScores(con, dnaId, langId, partId, false);
	}


	/**
	 * getReportModelForceScores - Convenience method to enable use
	 *   until language is fully implemented
	 *
	 * @param con A connection to the database
	 * @param dnaId A dnaId for which to get the report model
	 * @param partId A participantId for whom to get the scores
	 * @return A ReportModelStructureDTO object
	 * @throws Exception
	 */
	 public ReportModelStructureDTO getReportModelForceScores(Connection con, long dnaId, String partId)
		throws Exception
	{
		long dfltLangId = DefaultLanguageHelper.fetchDefaultLanguageId();
			
		return fetchReportModelAndScores(con, dnaId, dfltLangId, partId, true);
	}
		 
	/**
	 * getReportModelForceScores - Return the Report model for a particular DNA with
	 * 							   the scores for a particular participant.
	 * 							   No scores?  Model data still returns.
	 * 
	 * Name may be a bit of a misnomer
	 *
	 * @param con A connection to the database
	 * @param dnaId A dnaId for which to get the report model
	 * @param partId A participantId for whom to get the scores
	 * @return A ReportModelStructureDTO object
	 * @throws Exception
	 */
	 public ReportModelStructureDTO getReportModelForceScores(Connection con, long dnaId, int langId, String partId)
		throws Exception
	{
		 return fetchReportModelAndScores(con, dnaId, langId, partId, true);
	}


	/**
	 * fetchReportModelAndScores - Return the Report model for a particular DNA with
	 * 							  the scores for a particular participant.
	 *
	 * @param con A connection to the database
	 * @param dnaId A dnaId for which to get the report model
	 * @param langId A language ID
	 * @param partId A participantId for whom to get the scores
	 * @param forceScores Flag that, if set returns model data even if there are no scores
	 * @return A ReportModelStructureDTO object
	 * @throws Exception
	 */
	 public ReportModelStructureDTO fetchReportModelAndScores(Connection con, long dnaId, long langId, String partId, boolean forceScores)
		throws Exception
	{
		Statement stmt = null;
		ResultSet rs = null;
		StringBuffer sql = new StringBuffer();
		HashMap<Long, Double> scoreMap = new HashMap<Long, Double>();
		
		// Set up the return object
		ReportModelStructureDTO ret = new ReportModelStructureDTO();
		ret.setDnaId(dnaId);
		ret.setParticipantId(partId);
		ret.setLangId(langId);

		// Get the scores
		sql.append("SELECT icr.competencyId, ");
		sql.append("       icr.lcScore ");
		sql.append("  FROM pdi_abd_igrid_comp_resp icr ");
		sql.append("  WHERE icr.dnaId = " + dnaId + " ");
		sql.append("    AND icr.participantId = '" + partId + "'");			
		
		try
		{
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql.toString());

			if (forceScores || rs.isBeforeFirst())
			{
				// Drop on through
			}
			else
			{
				// No score data and none desired... no more to do
				return ret;
			}
	
			// put the scores in a map and get the model competency data
			while(rs.next())
			{
				long compId = rs.getLong("competencyId");
				int sIdx = rs.getInt("lcScore");
				// convert the index to a score
				double score = 0.0f;
				if (sIdx >= 1 && sIdx <= 9)
				{
					score = (sIdx + 1.0) / 2;
				}
				scoreMap.put(new Long(compId), new Double(score));
			}
			return fetchReportModel(con, dnaId, langId, scoreMap);
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL fetchReportModelWithScores(): dnaId=" + dnaId + ", partId=" + partId + ".  " +
	        			"SQLException: " + ex.getMessage() + ", " +
	        			"SQLState: " + ex.getSQLState() + ", " +
						"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (rs != null)
			{
				try	{  rs.close();  }
				catch (SQLException sqlEx)
				{
					System.out.println("Error closing RM read ResultSet.  Ignored.  " +
		        			"SQLException: " + sqlEx.getMessage() + ", " +
		        			"SQLState: " + sqlEx.getSQLState() + ", " +
							"VendorError: " + sqlEx.getErrorCode());
				}
				rs = null;
			}
			if (stmt != null)
			{
				try	{  stmt.close();  }
				catch (SQLException sqlEx)
				{
					System.out.println("Error closing RM read Statement.  Ignored.  " +
		        			"SQLException: " + sqlEx.getMessage() + ", " +
		        			"SQLState: " + sqlEx.getSQLState() + ", " +
							"VendorError: " + sqlEx.getErrorCode());
				}
				stmt = null;
			}
		}
	}


	 /**
	  * cloneModelUserText - We want to make copies of any user text because otherwise if they
	  *                      changed in either DNA it would change in the other. Also imagine
	  *                      a series of clones each descended from the previous... same thing
	  *                       would happen.  This will cut out that type of interaction.
	  * @param rms
	  * @return
	  */
	public RequestStatus cloneModelUserText(ReportModelStructureDTO rms)
	{
		RequestStatus ret = new RequestStatus();
		SetupUtils util = new SetupUtils();
		
		// Loop through the superfactors
		for (ReportModelSuperFactorDTO sf : rms.getSuperFactorList())
		{
			if (sf.getUserTextId() != 0)
			{
				// Clone the superfactor user text
				long newTextId = util.cloneUserText(sf.getUserTextId());
				if (newTextId == 0)
				{
					ret.setStatusCode(RequestStatus.RS_ERROR);
					ret.setExceptionMessage("Error cloning superfactor user text for superfactor ID " + sf.getSuperFactorId() + ", user text ID " + sf.getUserTextId() + ".");
					return ret;
				}
				sf.setUserTextId(newTextId);
			}
			
			// Loop through the competencies
			for (ReportModelCompetencyDTO cmp : sf.getCompList())
			{
				if (cmp.getUserTextId() != 0)
				{
					// Clone the competency user text
					long newTextId = util.cloneUserText(cmp.getUserTextId());
					if (newTextId == 0)
					{
						ret.setStatusCode(RequestStatus.RS_ERROR);
						ret.setExceptionMessage("Error cloning user text for competency ID " + cmp.getCompetencyId() + ", user text ID " + cmp.getUserTextId() + ".");
						return ret;
					}
					cmp.setUserTextId(newTextId);
				}
			}
		}
		
		return ret;
	}


	/**
	 * unescapeSfAndCompNames - We store the superfactor and competency name data as
	 *                          escaped in the DB... Unescape it for display purposes.
	 * @param inp
	 * @return
	 */
	public ReportModelStructureDTO unescapeSfAndCompNames(ReportModelStructureDTO inp)
	{
		  // Pull escapement from tags
		for(ReportModelSuperFactorDTO sfDto : inp.getSuperFactorList())
		{
			String sfName = sfDto.getDisplayName();
			sfName = sfName.replaceAll("&amp;","&");
			sfName = sfName.replaceAll("&lt;","<");
			sfName = sfName.replaceAll("&gt;",">");
			sfDto.setDisplayName(sfName);
			for(ReportModelCompetencyDTO compDto : sfDto.getCompList())
			{
				String compName = compDto.getDisplayName();
				compName = compName.replaceAll("&amp;","&");
				compName = compName.replaceAll("&lt;","<");
				compName = compName.replaceAll("&gt;",">");
				compDto.setDisplayName(compName);
			}
		}
		
		return inp;
	}



// ------- Private classes -------
	private class ItemTextData
	{
		private long _itemId;
		private String _staticEnTxt;
		private String _staticLangTxt;
		private String _userEnTxt;
		private String _userLangTxt;
		
		public ItemTextData(long itemId,
				            String staticEnTxt,
				            String staticLangTxt,
				            String userEnTxt,
				            String userLangTxt)
		{
			_itemId = itemId;
			_staticEnTxt = staticEnTxt;
			_staticLangTxt = staticLangTxt;
			_userEnTxt = userEnTxt;
			_userLangTxt = userLangTxt;
		}
		
		//Accessors only
		public long getItemId()
		{
			return _itemId;
		}

		public String getStaticEnTxt()
		{
			return _staticEnTxt;
		}

		public String getStaticLangTxt()
		{
			return _staticLangTxt;
		}

		public String getUserEnTxt()
		{
			return _userEnTxt;
		}

		public String getUserLangTxt()
		{
			return _userLangTxt;
		}
	}
}
