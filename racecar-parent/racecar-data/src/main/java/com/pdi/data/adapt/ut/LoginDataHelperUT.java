package com.pdi.data.adapt.ut;

import java.io.IOException;
//import java.io.StringReader;

//import javax.xml.parsers.DocumentBuilder;
//import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.PostMethod;
//import org.w3c.dom.Document;
//import org.xml.sax.InputSource;

import com.pdi.properties.PropertyLoader;

public class LoginDataHelperUT {
	public static String testLogin(String userid, String password) throws HttpException, IOException	{
		String req = PropertyLoader.getProperty("com.pdi.data.adapt.application", "castIron.userValidationUrl");
		req += "?userid=" + userid + "&password=" + password;
		
		PostMethod post = new PostMethod(req);
		HttpClient client = new HttpClient();
		int result = client.executeMethod(post);
		if (result != 200)
		{
			throw new HttpException("Data not retrieved.  Status = " + result);
		}
		if (result == 200)	{
			String strXMLResponse = post.getResponseBodyAsString();
			return strXMLResponse;
		}else{
			return result + "";
		}
//		DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
//		DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
//		Document doc = docBuilder.parse(new InputSource(new StringReader(strXMLResponse)));
//		
//		if (doc.hasAttributes() 
//				&& null != doc.getAttributes().getNamedItem("validUser") 
//				&& doc.getAttributes().item(0).getNodeValue().toString().equals("true"))	{
//			return true;
//		}	
//		return false;
	}
}
