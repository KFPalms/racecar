/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.helpers;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;

import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.util.RequestStatus;
import com.pdi.data.v3.dto.EmailRecipientTypeDTO;
import com.pdi.data.v3.dto.EmailRecipientTypeListDTO;
import com.pdi.data.v3.dto.SessionUserDTO;
import com.pdi.data.v3.util.V3DatabaseUtils;

/**
 * EmailRecipientTypeDataHelper provides functionality to
 * add, change, delete, or fetch Email Recipient Types.
 *
 * @author		Ken Beukelman
 */
public class EmailRecipientTypeDataHelper
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	/**
	 * Insert a new recipient type
	 * @param sessionUser
	 * @param An EmailRecipientTypeDTO object
	 * @return A new EmailRecipientTypeDTO object with the status and the ID only
	 */
	public static EmailRecipientTypeDTO addRecipientType(SessionUserDTO sessionUser, EmailRecipientTypeDTO newType)
	{
		DataResult dr = null;
		EmailRecipientTypeDTO ret = new EmailRecipientTypeDTO();

		if(newType.getName() == null || newType.getName().trim().length() == 0)
		{
			ret.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
					"An internalCode, displayName are required to add a recipient type."));
			return ret;
		}
		
//TODO Modify query to eliminate hard-coded "xxxDate" fields once we have a DB with triggers installed

	// This is a test query until we get rights to add triggers.  At that time, only the
	// first two fields will be needed; this query should then be modified to reflect that.
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("INSERT INTO em_recipient_type_lookup ");
		sqlQuery.append("  (displayName, createdBy,createdDate,modifiedBy,modifiedDate) ");
		sqlQuery.append("  VALUES( ?, ?, GETDATE(), ?, GETDATE())");
		
		DataLayerPreparedStatement dlps = V3DatabaseUtils.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
		if (dlps.isInError())
		{
			ret.setStatus(dlps.getStatus());
			return ret;
		}

		try
		{
			dlps.getPreparedStatement().setString(1,newType.getName());
			dlps.getPreparedStatement().setString(2,sessionUser.getUsername());
			dlps.getPreparedStatement().setString(3,sessionUser.getUsername());

			dr = V3DatabaseUtils.insert(dlps);
			
			ret.setStatus(dr.getStatus());
			switch (ret.getStatus().getStatusCode())
			{
				case RequestStatus.RS_ERROR:
					break;
				case RequestStatus.DBS_DUP_KEY:
					ret.getStatus().setExceptionMessage("Display name (" + newType.getName() + ") already exists.");
					break;
				case RequestStatus.RS_OK:
					ret.setId(dr.getNewId());
					break;
				default:
					ret.setStatus(unexpectedStatus(ret.getStatus()));
					break;
			}
	
			return ret;
		}
		catch (SQLException ex)
		{
			ret.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
									ex.getErrorCode(), "addRecipientType:  " + ex.getMessage()));
			return ret;
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}


	/**
	 * Get a specific recipient type
	 * @param recipient type id
	 * @return An EmRecipientTypeDTO object
	 */
	public static EmailRecipientTypeDTO getRecipientType(long typeId)
	{
		DataResult dr = null;
		EmailRecipientTypeDTO ret = new EmailRecipientTypeDTO();

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT recipientTypeId, ");
		sqlQuery.append("       displayName ");
		sqlQuery.append("  FROM em_recipient_type_lookup ");
		sqlQuery.append("  WHERE active = 1 ");
		sqlQuery.append("    AND recipientTypeId = ?");
		
		DataLayerPreparedStatement dlps = V3DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			ret.setStatus(dlps.getStatus());
			return ret;
		}

		try
		{
			dlps.getPreparedStatement().setLong(1,typeId);

			dr = V3DatabaseUtils.select(dlps);

			ret.setStatus(dr.getStatus());
			switch (ret.getStatus().getStatusCode())
			{
				case RequestStatus.RS_ERROR:
					break;
				case RequestStatus.DBS_NO_DATA:
					ret.getStatus().setExceptionMessage("Recipient Type ID " + typeId + " does not exist.");
					break;
				case RequestStatus.RS_OK:
					ResultSet rs = dr.getResultSet();
					rs.next();
					moveData(rs, ret);
					ret.setId(typeId);
					break;
				default:
					ret.setStatus(unexpectedStatus(ret.getStatus()));
					break;
			}
	
			return ret;
		}
		catch (SQLException ex)
		{
			ret.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
											ex.getErrorCode(), "getRecipientType:  " + ex.getMessage()));
			return ret;
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}

	
	/**
	 * Get all active recipient types in the recipient
	 * type lookup table in alphabetical order by name.
	 * 
	 * @param sessionUser
	 * @return An ArrayList of EmRecipientTypeDTO objects
	 */
	public static EmailRecipientTypeListDTO getAllRecipientTypes(SessionUserDTO sessionUser)
	{
		DataResult dr = null;
		EmailRecipientTypeListDTO ret = new EmailRecipientTypeListDTO();
		
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT recipientTypeId	, ");
		sqlQuery.append("       displayName");
		sqlQuery.append("  FROM em_recipient_type_lookup ");
		sqlQuery.append("  WHERE active = 1 ");
		sqlQuery.append("  ORDER BY displayName");

		DataLayerPreparedStatement dlps = V3DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			ret.setStatus(dlps.getStatus());
			return ret;
		}
		
		try
		{
			dr = V3DatabaseUtils.select(dlps);

			ret.setStatus(dr.getStatus());
			switch (ret.getStatus().getStatusCode())
			{
				case RequestStatus.RS_ERROR:
					break;
				case RequestStatus.DBS_NO_DATA:
					ret.setTypeList(null);
					ret.getStatus().setExceptionMessage("No recipient types are present.");
					break;
				case RequestStatus.RS_OK:
					ResultSet rs = dr.getResultSet();
					try
					{
						while (rs.next())
						{
							EmailRecipientTypeDTO type = new EmailRecipientTypeDTO(dr.getStatus());
							moveData(rs, type);
							ret.getTypeList().add(type);
						}
					}
					catch (SQLException ex)
					{
						ret.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
								ex.getErrorCode(), "getAllRecipientTypes:  " + ex.getMessage()));
						return ret;
					}
					break;
				default:
					ret.setStatus(unexpectedStatus(ret.getStatus()));
					break;
			}
			
			return ret;
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}


	/**
	 * Update a recipient type
	 * @param sessionUser
	 * @param An EmailRecipientTypeDTO object
	 * @return A RequestStatus object
	 */
	public static RequestStatus updateRecipientType(SessionUserDTO sessionUser, EmailRecipientTypeDTO updtType)
	{
		DataResult dr = null;
		
		if (updtType.getId() == 0 ||
			(updtType.getName() == null || updtType.getName().trim().length() == 0))
		{
			return new RequestStatus(RequestStatus.RS_ERROR,
					"Update requires a valid Recipient Type ID and Display Name");
		}

		String dn = updtType.getName().trim().replace("'","''");

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("UPDATE em_recipient_type_lookup ");
		sqlQuery.append("  SET displayName = ?, modifiedBy = ? ");
		sqlQuery.append("  WHERE recipientTypeId = ?");
		
		DataLayerPreparedStatement dlps = V3DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			return dlps.getStatus();
		}

		try
		{
			dlps.getPreparedStatement().setString(1, dn);
			dlps.getPreparedStatement().setString(2, sessionUser.getUsername());
			dlps.getPreparedStatement().setLong(3, updtType.getId());

			dr = V3DatabaseUtils.update(dlps);
			
			switch (dr.getStatus().getStatusCode())
			{
				case RequestStatus.RS_ERROR:
					return dr.getStatus();
				case RequestStatus.RS_OK:
					if (dr.getRowCount() >0)
					{
						return dr.getStatus();
					}
					else
					{
						return new RequestStatus(RequestStatus.DBS_NO_DATA, "No data to update for recipient type Id " + updtType.getId());
					}
				default:
					return unexpectedStatus(dr.getStatus());
			}
		}
		catch (SQLException ex)
		{
			return new RequestStatus(RequestStatus.RS_ERROR,
					ex.getErrorCode(), "updateRecipientType:  " + ex.getMessage());
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}

	
	/**
	 * Delete a recipient type - note that this is really not a true delete.  We simply
	 * update the active flag to 0.
	 * @param sessionUser
	 * @param An email recipient type ID
	 * @return A RequestStatus object
	 */
	public static RequestStatus deleteRecipientType(SessionUserDTO sessionUser, long typeId)
	{
		DataResult dr = null;
		
		if (typeId == 0)
		{
			return new RequestStatus(RequestStatus.RS_ERROR,
					"Delete requires a valid Recipient Type ID");
		}

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("UPDATE em_recipient_type_lookup ");
		sqlQuery.append("  SET active = 0, modifiedBy = ?");
		sqlQuery.append("  WHERE recipientTypeId = ?");
		
		DataLayerPreparedStatement dlps = V3DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			return dlps.getStatus();
		}

		try
		{
			dlps.getPreparedStatement().setString(1, sessionUser.getUsername());
			dlps.getPreparedStatement().setLong(2, typeId);

			dr = V3DatabaseUtils.update(dlps);
			
			switch (dr.getStatus().getStatusCode())
			{
				case RequestStatus.RS_ERROR:
				case RequestStatus.RS_OK:
					return dr.getStatus();
				default:
					return unexpectedStatus(dr.getStatus());
			}
		}
		catch (SQLException ex)
		{
			return new RequestStatus(RequestStatus.RS_ERROR,
					ex.getErrorCode(), "updateRecipientType:  " + ex.getMessage());
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}
	
	
/*
 * This code is for a "real" delete.  Please do not delete it for now in case we ever want to
 * do a "real" delete
 * 
 *	public static EmailRecipientTypeDTO deleteRecipientType(EmailRecipientTypeDTO delType)
 *		throws Exception
 *	{
 *		// Check for type id != 0 here (see current delete code)
 *		DataResult dr = null;
 *		StringBuffer sqlQuery = new StringBuffer();
 *		sqlQuery.append("DELETE FROM  em_recipient_type_lookup ");
 *		sqlQuery.append("  WHERE recipientTypeId = ?");
 *		
 *		PreparedStatement ps = V3DatabaseUtils.prepareStatement(sqlQuery);
 *
 *		ps.setLong(1,delType.getId());
 *
 *		try
 *		{
 *			dr = V3DatabaseUtils.delete(ps);
 *
 *			EmailRecipientTypeDTO ret = new EmailRecipientTypeDTO(dr.getStatus());
 *	
 *			switch (dr.getStatus().getStatusCode())
 *			{
 *				case RequestStatus.RS_ERROR:
 *					throw new Exception("deleteRecipientType:  " + dr.getStatus().getExceptionMessage());
 *				default:
 *					break;
 *			}
 *	
 *			return ret;
 *		}
 *		finally
 *		{
 *			if (dr != null) { dr.close(); dr = null; }
 *		}
 *	}
 */
	

	/**
	 * Process the input status object to add the "Unexpected..." message
	 * 
	 * @param The RequestStatus object 
	 * @return A RequestStatus object
	 */
	private static RequestStatus unexpectedStatus(RequestStatus inp)
	{
		RequestStatus out = new RequestStatus(inp);
		String str = "Unexpected Status code encountered";
		str += (out.getExceptionMessage() == null ? "." : "; msg=" + out.getExceptionMessage());
		inp.setExceptionMessage(str);
		return out;
	}


	/////////////////////
	// Private methods //
	/////////////////////

	/**
	 * Move data from a ResultSet object to a EmailRecipientTypeDTO object
	 * This function has been written to be "generic";  if the data is not
	 * in the result set, it won't be moved to the DTO.
	 * 
	 * NOTE:  This may not be needed here because we are moving all of the
	 *		  data in the Result set.  This does allow us to have different
	 *		  resultSets that return differing data and load only those
	 *		  parts of the DTO that we have data for.
	 *
	 * @param rs A ResultSet object
	 * @param dto An EmailRecipientTypeDTO object
	 */
	private static void moveData(ResultSet rs, EmailRecipientTypeDTO dto)
	{
		try
		{
			ResultSetMetaData rsmd = rs.getMetaData();

			// Get all of the columns
			HashSet<String> set = new HashSet<String>();

			for(int i=1; i <= rsmd.getColumnCount(); i++)
			{
				//set.add(rsmd.getColumnName(i));
				set.add(rsmd.getColumnLabel(i));
			}
			
			//  Put them in the proper slot in the dto
			if (set.contains("recipientTypeId"))
			{
				dto.setId(rs.getInt("recipientTypeId"));
			}
			if (set.contains("displayName"))
			{
				dto.setName(rs.getString("displayName"));
			}
			
			return;
		}
		catch (SQLException ex)
		{
			dto.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
					ex.getErrorCode(), ex.getMessage()));
			
			// Log the error
			String exc = "\n************************************* " + 
				"\nError in EmailRecipientTypeDataHelper.moveData: " +
				"\nSQLException: " + ex.getMessage() + " " +
				"\nSQLState: " + ex.getSQLState() + " " +
				"\nVendorError: " + ex.getErrorCode();
			System.err.println(exc);
			System.err.println("************************************* ");

			return;
		}
	}
	
	//
	// Instance data.
	//
	
	//
	// Constructors.
	//

	//
	// Instance methods.
	//

	public String toString()
	{
		return "EmailRecipientTypeDataHelper class:";
	}
}
