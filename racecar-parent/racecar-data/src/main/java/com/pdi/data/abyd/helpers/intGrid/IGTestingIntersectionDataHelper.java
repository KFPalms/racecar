/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.intGrid;

import java.sql.Connection;
import java.util.ArrayList;

import com.pdi.data.abyd.dto.common.DNACellDTO;
import com.pdi.data.abyd.dto.intGrid.IGIntersectionAndOtherDataDTO;


/**
 * IGTestingDataHelper returns the data for the "Testing" column on the Integration Grid with
 * the option of turning on/off the cognitive scores.  It returns an ArrayList of DNACellDTO
 * objects for that column only.
 *
 * @author		Ken Beukelman
 */
public class IGTestingIntersectionDataHelper
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private Connection con;
	private String partId;
	private long dnaId;
	private boolean cogsOn;
	private boolean hasAlp;
	private boolean hasKf4d;
	private IGKf4dDataHelper kf4dHelper;

	//
	// Constructors.
	//
	// ToDo Refactor to compine the ALP & KF4D flags (?)
	public IGTestingIntersectionDataHelper(Connection con, String partId, long dnaId, boolean cogsOn, boolean hasAlp, boolean hasKf4d)
		throws Exception
	{
		// ensure there is a candidate/participant id
		if (partId == null  || partId.length() < 1)
		{
			throw new Exception("A participant id is required to fetch Integration Grid data.");
		}
		if (dnaId == 0)
		{
			throw new Exception("A DNA id is required to fetch Integration Grid data.");
		}

		this.con = con;
		this.partId = partId;
		this.dnaId = dnaId;
		this.cogsOn = cogsOn;
		this.hasAlp = hasAlp;
		this.hasKf4d = hasKf4d;
		if (hasKf4d){
			this.kf4dHelper = new IGKf4dDataHelper(con, partId, dnaId);
		}
	}


	/**
	 * Controller method that gets the score data.
	 *
	 * @throws Exception
	 */
	public ArrayList<DNACellDTO> getTestingData()
		throws Exception
	{
		// The false id for ALP data... need to fix this to make it dynamic
		//TODO figure out if ALP data present; warning message set to note this fact that we need change
		IGIntersectionAndOtherDataDTO dat = IGScoring.fetchTestingIntersectionData(this.con,
																				   this.partId,
																				   this.dnaId,
																				   this.cogsOn,
																				   this.hasAlp,
																				   this.hasKf4d,
																				   this.kf4dHelper);
		
		return dat.getTestingCells();
	}
}
