/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */

package com.pdi.data.abyd.dto.evalGuide;

import com.pdi.data.abyd.dto.common.EGOverallData;


/**
 * EGOverallDTO is a thin class containing all of the data
 * needed to add or update the "overall" data (the stuff on
 * the last page with enterable data) for an Eval Guide.
 * 
 * @author		Ken Beukelman
 */
public class EGOverallDTO
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _participantId;
	private long _dnaId;
	private long _moduleId;
	private EGOverallData _overallData;

	//
	// Constructors.
	//
	public EGOverallDTO()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public String getParticipantId()
	{
		return _participantId;
	}
		
	public void setParticipantId(String value)
	{
		_participantId = value;
	}

	/*****************************************************************************************/
	public long getDnaId()
	{
		return _dnaId;
	}
		
	public void setDnaId(long value)
	{
		_dnaId = value;
	}

	/*****************************************************************************************/
	public long getModuleId()
	{
		return _moduleId;
	}
		
	public void setModuleId(long value)
	{
		_moduleId = value;
	}

	
	/*****************************************************************************************/
	public EGOverallData getOverallData()
	{
		return _overallData;
	}
		
	public void setOverallData(EGOverallData value)
	{
		_overallData = value;
	}

}
