/**
 * Copyright (c) 2008 Personnel Decisions International, Inc.
 * All rights reserved.
 *
 * $Header: /Platform/Java/sourceCode/com/pdicorp/app/tcm/export/vo/ExportDataVO.java 6     8/25/05 3:23p Kbeukelm $
 */
package com.pdi.data.abyd.dto.common;



/**
 * KeyValuePair is a container class for key/value pairs (e.g., id and name)
 * 
 * Note that while there is a corresponding class on the AS side, the data is
 * indistinguishable from a native AS3 Object and is, therefore mapped to that
 * when it is transported to Flex.
 *
 * @author		Ken Beukelman
 */
public class KeyValuePair
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
		private long _key;
		private String _value;


		//
		// Constructors.
		//
	public KeyValuePair()
	{
		// Does nothing presently
	}
	
	//
	// Instance methods.
	//

	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
		
	/*****************************************************************************************/
	public long getTheKey()
	{
		return _key;
	}

	public void setTheKey(long value)
	{
		_key = value;
	}

	/*****************************************************************************************/
	public String getTheValue()
	{
		return _value;
	}

	public void setTheValue(String value)
	{
		_value = value;
	}

}