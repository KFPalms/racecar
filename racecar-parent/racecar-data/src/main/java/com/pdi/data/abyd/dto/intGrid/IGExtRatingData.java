/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.dto.intGrid;


/**
 * IGExtRatingData is a container for name/rating data used in the IG Extract.
 * 
 * @author		Ken Beukelman
 */
public class IGExtRatingData
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _name;
	private String _rating;

	//
	// Constructors.
	//
	public IGExtRatingData()
	{
		// does nothing currently
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public String getName()
	{
		return _name;
	}
	
	public void setName(String value)
	{
		_name = value;
	}	

	/*****************************************************************************************/
	public String getRating()
	{
		return _rating;
	}
	
	public void setRating(String value)
	{
		_rating = value;
	}	



	/*
	 * Show me what you've got
	 */
	public String toString()
	{
		String ret = "IGExtRatingData content (Java)\n";
		ret += "        Scale Name=" + _name + "\n";
		ret += "      Scale Rating=" + _rating + "\n";

		return ret;
	}
}
