/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.dto.intGrid;

import java.util.ArrayList;

import com.pdi.data.abyd.dto.common.IGInstCompData;
import com.pdi.data.abyd.dto.intGrid.IGTestAndNormNameData;


/**
 * IGInstDisplayData is a thin class containing information about the instruments
 * (GPI and Cognitives) used to provide input into the Integration Grid.
 * 
 * @author		Ken Beukelman
 */
public class IGInstDisplayData
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private int _rdi;									// The GPI RDI associated with this participant
	private ArrayList<IGTestAndNormNameData> _normData;	// Norm data
	private ArrayList<IGInstCompData> _compArray;		// The data for the competency(ies)
	private boolean _cogsDone;							// flag for the COGSDONE construct
	private boolean _gpiIncluded;
	private boolean _hasKfp = false;
	private boolean _hasKfar = false;
	private String _calcedCompScore = "";
	private String _testInstrumentTitle;
	//
	// Constructors.
	//
	public IGInstDisplayData()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//
	
	
	public void calcCompScores()
	{
		// first calculate the individual competency scores
		double compTot = 0.0;
		int compCnt = 0;
		boolean unfinishedCog = false;	
		for (IGInstCompData comp : _compArray)
		{
			double tot = 0.0;
			int cnt = 0;
			// Do the cogs
			if (comp.getCogColumnScores() != null)
			{
				for (IGScaleScoreData dat : comp.getCogColumnScores())
				{
					double rate = 0.0;
					try
					{
						rate = Double.parseDouble(dat.getRating());
					}
					catch (NumberFormatException e)
					{
						//Skip it
						continue;
					}
					
					if (rate > 0.0)
					{
						if (dat.getIsNegative())
						{
							tot += (6.0 - rate);
						}
						else
						{
							tot += rate;
						}	
						cnt++;
					}
					else if(_cogsDone == false)
					{
						unfinishedCog = true;
					}
				}
			}
			// Now do the gpi
			if (comp.getGpiColumnScores() != null)
			{
				for (IGScaleScoreData dat : comp.getGpiColumnScores())
				{
					double rate = 0.0;
					try
					{
						rate = Double.parseDouble(dat.getRating());
					}
					catch (NumberFormatException e)
					{
						//Skip it
						continue;
					}
					if (rate > 0.0)
					{
						if (dat.getIsNegative())
						{
							tot += (6.0 - rate);
						}
						else
						{
							tot += rate;
						}	
						cnt++;
					// No cogsDone checks here
					}
				}
			}
			// Got the scores, calc the comp score
			double avg = 0.0;
			if(! unfinishedCog)
			{
				if(cnt > 0)
				{
					avg = tot / cnt;
					avg = Math.floor((avg * 100.0) + 0.505) / 100.0;
					compTot += avg;
					compCnt++;
				}
			}
			comp.setCalcedCompScore(avg == 0.0 ? "N/A" : String.format("%.2f", avg));
		}	// End of compArray loop
		
		// Calced all of the constituent scores... now do the overall
		double compAvg = 0.0;
		if (unfinishedCog)
		{
			compAvg = 0.0;	// Not really needed
		}
		else if(compCnt > 0)
		{
			compAvg = compTot / compCnt;
			compAvg = Math.floor((compAvg * 100.0) + 0.505) / 100.0;
		}
		_calcedCompScore = (compAvg == 0.0 ? "N/A" : String.format("%.2f", compAvg));
	}

	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public int getRdi()
	{
		return _rdi;
	}
		
	public void setRdi(int value)
	{
		_rdi = value;
	}
	
	/*****************************************************************************************/
	public ArrayList<IGTestAndNormNameData> getNormData()
	{
		return _normData;
	}
		
	public void setNormData(ArrayList<IGTestAndNormNameData> value)
	{
		_normData = value;
	}
	
	/*****************************************************************************************/
	public ArrayList<IGInstCompData> getCompArray()
	{
		return _compArray;
	}
		
	public void setCompArray(ArrayList<IGInstCompData> value)
	{
		_compArray = value;
	}
	
	/*****************************************************************************************/
	public boolean  getCogsDone()
	{
		return _cogsDone;
	}
		
	public void setCogsDone(boolean value)
	{
		_cogsDone = value;
	}	

	/*****************************************************************************************/
	public boolean  getGpiIncluded()
	{
		return _gpiIncluded;
	}
		
	public void setGpiIncluded(boolean value)
	{
		_gpiIncluded = value;
	}

	/*****************************************************************************************/
	public boolean  getHasKfp()
	{
		return _hasKfp;
	}
		
	public void setHasKfp(boolean value)
	{
		_hasKfp = value;
	}

	/*****************************************************************************************/
	public boolean  getHasKfar()
	{
		return _hasKfar;
	}
		
	public void setHasKfar(boolean value)
	{
		_hasKfar = value;
	}

	/*****************************************************************************************/
	public String getCalcedCompScore()
	{
		return _calcedCompScore;
	}
		
	public void setCalcedCompScore(String value)
	{
		_calcedCompScore = value;
	}
	
	public String getTestInstrumentTitle(){
		return _testInstrumentTitle;
	}
	
	public void setTestInstrumentTitle(String value){
		_testInstrumentTitle = value;
	}


	/*
	 * Show me what you've got
	 */
	public String toString()
	{
		String ret = "IGInstDisplayData content (Java)\n";
		ret += "       RDI=" + _rdi + "\n";
		ret += "  cogsDone=" + _cogsDone + "\n";
		if (_normData == null)
		{
			ret += "  No Norm data present";
		}
		else
		{
			for(IGTestAndNormNameData tnd : _normData)
			{
				ret += tnd.toString();
			}
		}
		if (_compArray == null)
		{
			ret += "No Competency data present";
		}
		else
		{
			for(IGInstCompData cd : _compArray)
			{
				ret += cd.toString();
			}
		}

		return ret;
	}
}