package com.pdi.data.abyd.helpers.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.properties.PropertyLoader;
import com.pdi.scoring.Norm;
import com.pdi.data.abyd.dto.common.PrnData;
import com.pdi.data.abyd.helpers.intGrid.IGAlrDataHelper;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.data.abyd.util.Utils;

public class Rv2Helper {
	
	private static final Logger log = LoggerFactory.getLogger(Rv2Helper.class);
	
	// Ravens Key
	private static List<Integer> RAVENS_KEY = new ArrayList<Integer>(
			Arrays.asList(8, 7, 1, 8, 4, 3, 1, 6, 5, 4, 4, 7, 8, 7, 2, 5, 6, 7, 8, 3, 6, 3, 2));
	
	private static final String RV2_RAW_SCORE_STR = "/scoringExtracts/participants/{pptId}/projects/{projId}/instruments/rv2";

	// Ravens norm
	private static double SPEC_RAV_MEAN = 14.3;
	private static double SPEC_RAV_STDEV = 4.1;
	
	/**
	 * captureRv2Scores - This is called because:
	 * 						1) We wish to generate a Ravens report
	 * 						2) We know we have an rv2 in the project.
	 *   This logic checks to see if there is a score for this ppt and
	 *   project and, if not, fetches the data and inserts it.
	 *	
	 *Note that all failures are logged, but no feedback is given to the caller.
	 * @param ppt
	 * @param proj
	 */
	public void captureRv2Scores(String ppt, String proj)
	{
		// See if there are results in the table for this ppt/proj;  if so, skip out
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT * ");
		sb.append("  FROM results ");
		sb.append("  WHERE instrumentCode = 'rv2' ");
		sb.append("    AND participantId = " + ppt + " ");
		sb.append("    AND projectId = " + proj);

		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
		if (dlps.isInError())
		{
			log.error("captureRv2Scores: Error preparing results read query. {}", dlps.toString());
			return;
		}

		DataResult dr = null;
		try
		{
			dr = V2DatabaseUtils.select(dlps);
			ResultSet rs = dr.getResultSet();

			if (rs != null && rs.isBeforeFirst())
			{
				// Data exists... we won't overwrite it
				return;
			}
			
			// no data... We need to get some
		}
		catch(java.sql.SQLException se)
		{
			log.error("captureRv2Scores: Error fetch results.  msg={}", se.getMessage());
			return;
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
		
		//		- Get the data type (alp or 4d)
		Utils utl = new Utils();
		//boolean hasKf4d = utl.isInstValid("kf4d", ppt, proj);
		boolean hasKfp = utl.isInstValid("kfp", ppt, proj);
		
		
		Connection con = null;
		long projId = Long.valueOf(proj);
		long dnaId = Utils.getDnaIdFromProjectId(projId);
		if (hasKfp)
		{
			try
			{
				// get the kfp score data
				con = AbyDDatabaseUtils.getDBConnection();
				IGAlrDataHelper alrHelper = new IGAlrDataHelper(con, ppt, dnaId);
				String xml = alrHelper.getAlrRgrData(ppt, Long.valueOf(proj));
				if (xml == null || xml.length() < 1)
				{
					// no data
					return;
				}

				// Pull the raw score out of the document
				DocumentBuilderFactory dbfac = null;
				DocumentBuilder docBuilder = null;
				Document doc = null;
				try {
					dbfac = DocumentBuilderFactory.newInstance();
					docBuilder = dbfac.newDocumentBuilder();
					doc = docBuilder.parse(new InputSource(new StringReader(xml)));
				} catch (ParserConfigurationException pce) {
					String err = "captureRv2Scores() - XML parser config ERROR: ppt=" + ppt + ", proj=" + proj+ ", xml=" + xml + ", msg=" + pce.getMessage();
					log.error(err);
					return;
				} catch (SAXException se) {
					String err = "captureRv2Scores() - XML SAX ERROR: ppt=" + ppt + ", proj=" + proj + ", xml=" + xml + ", msg=" + se.getMessage();
					log.error(err);
					return;
				}
			
				XPathFactory xPathfactory = XPathFactory.newInstance();
				XPath xpath = xPathfactory.newXPath();
				String raw = (String)xpath.compile("/kfarScoreData/scales/scale[@id='RV2']/raw").evaluate(doc, XPathConstants.STRING);
				if (raw == null || raw.isEmpty())
				{
					log.error("RV2 score is null or empty for ppt/proj={}", ppt + "/" + proj);
					return;
				}
				try
				{
					// Validate (just do the conversion to see if the exception happens)
					Float.valueOf(raw);
				}
				catch (NumberFormatException e)
				{
					log.error("RV2 score (" + raw + ") is ihvalid number.  ppt/proj=" + ppt + "/" + proj);
					return;
				}
				
				// persist it
				persistRv2Raw(raw, ppt, proj);

				// scram
				return;
			}
			catch (Exception e)
			{
				log.error("Exception in captureRv2Scores (kfp):  msg={}", e.getMessage());
				return;
			}
			finally
			{
				if (con != null)
				{
					try { con.close(); } catch (SQLException e){ /* swallow it */ }
					con = null;
				}
			}
		} else {
			//has KF4D or only RV2
			try{
				int rawRv2 = this.getRv2RawScore(ppt, projId);
				if (rawRv2 == -1)
				{
					// Nothing to persist... scram
					return;
				}
				
				// persist it
				persistRv2Raw(""+rawRv2, ppt, proj);

				// scram
				return;
			} catch (Exception e){
				log.error("Error getting RV2 Raw score: {}", e.getMessage());
				return;
			}
		}
	}
	
	/**
	 * getRv2RawScore returns the raw score for the ravens, a -1 (no scores
	 * available), or throws an exception
	 *
	 * @param partId
	 * @param projId
	 * @return
	 * @throws Exception
	 */
	public int getRv2RawScore(String partId, long projId) throws Exception {
		URL url = null;
		URLConnection uc = null;
		InputStream content = null;

		// Set up and callthe RESTful service
		String surl = PropertyLoader.getProperty("com.pdi.data.abyd.application", "palms.tincanRestServer.url");
		if (surl.charAt(surl.length() - 1) == '/') {
			surl = surl.substring(0, surl.length() - 2);
		}
		surl += RV2_RAW_SCORE_STR;

		surl = surl.replace("{pptId}", partId);
		surl = surl.replace("{projId}", "" + projId);

		try {
			url = new URL(surl);
		} catch (MalformedURLException e) {
			String err = "IGKf4dDataHelper.getRv2Rating() - Malformed URL - " + surl;
			log.error(err);
			throw new Exception(err);
		}

		// open the connection.
		try {
			uc = url.openConnection();
		} catch (IOException e) {
			String err = "IGKf4dDataHelper.getRv2Rating() - Unable to open connection.  URL=" + surl + ".  Msg="
					+ e.getMessage();
			log.error(err);
			throw new Exception(err);
		}

		// Get the data
		HttpURLConnection huc = (HttpURLConnection) uc;
		boolean hasScores = true;
		try {
			int statusCode = huc.getResponseCode();
			if (statusCode == HttpServletResponse.SC_NOT_FOUND || statusCode == HttpServletResponse.SC_NO_CONTENT
					|| statusCode == HttpServletResponse.SC_INTERNAL_SERVER_ERROR) {
				// String str = "There is no KF4D scoring information available
				// for participant " + this.partId + " in project " +
				// this.projId + " (" + statusCode + ").";
				hasScores = false;
			} else if (statusCode >= 200 && statusCode < 300) {
				content = huc.getInputStream();
			} else {
				String str = "Error returned from KF4D score fetch (getRv2Rating): Code=" + statusCode + " for URL="
						+ surl + ".  Message=" + huc.getResponseMessage();
				System.out.println(str);
				throw new Exception(str);
			}
		} catch (IOException e) {
			String err = "getRv2Rating: Error fetching score JSON string.  Msg=" + e.getMessage();
			log.error(err);
			throw new Exception(err);
		}

		StringBuffer xml = new StringBuffer();
		if (hasScores) {
			BufferedReader br = new BufferedReader(new InputStreamReader(content));
			String line;
			try {
				while ((line = br.readLine()) != null) {
					xml.append(line);
				}
			} catch (IOException e) {
				String err = "IGKf4dDataHelper.getRv2Rating() - Unable to read content.  URL=" + surl + ".  Msg="
						+ e.getMessage();
				log.error(err);
				throw new Exception(err);
			}
		} else {
			// No scores... send back a bogus score
			return -1;
		}

		// Get the responses
		DocumentBuilderFactory dbfac = null;
		DocumentBuilder docBuilder = null;
		Document doc = null;
		try {
			dbfac = DocumentBuilderFactory.newInstance();
			docBuilder = dbfac.newDocumentBuilder();
			doc = docBuilder.parse(new InputSource(new StringReader(xml.toString())));
		} catch (ParserConfigurationException pce) {
			String err = "IGKf4dDataHelper.getRv2Rating() - XML parser config ERROR: ppt=" + partId + ", proj=" + projId
					+ ", xml=" + xml + ", msg=" + pce.getMessage();
			log.error(err);
			throw new Exception(err);
		} catch (SAXException se) {
			String err = "IGKf4dDataHelper.getRv2Rating() - XML SAX ERROR: ppt=" + partId + ", proj=" + projId
					+ ", xml=" + xml + ", msg=" + se.getMessage();
			log.error(err);
			throw new Exception(err);
		} catch (IOException ie) {
			String err = "IGKf4dDataHelper.getRv2Rating() - XML I/O error: ppt=" + partId + ", proj=" + projId
					+ ", xml=" + xml + ", msg=" + ie.getMessage();
			log.error(err);
			throw new Exception(err);
		}

		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xpath = xPathfactory.newXPath();

		NodeList nodeList;
		try {
			nodeList = (NodeList) xpath.compile("/scoringExtract/dataset/integerDataArray").evaluate(doc,
					XPathConstants.NODESET);
		} catch (XPathExpressionException xe) {
			String err = "IGKf4dDataHelper.getRv2Rating() - XML I/O error: ppt=" + partId + ", proj=" + projId
					+ ", xml=" + xml + ", msg=" + xe.getMessage();
			log.error(err);
			throw new Exception(err);
		}

		// int size = nodeList.getLength();
		List<Integer> scores = new ArrayList<Integer>();
		for (int index = 0; index < nodeList.getLength(); index++) {
			Element e = (Element) nodeList.item(index);
			// String name = e.getAttribute("id").toUpperCase();
			NodeList nl = e.getChildNodes();

			for (int i2 = 0; i2 < nl.getLength(); i2++) {
				Element e2 = (Element) nl.item(i2);
				String val = e2.getTextContent();
				if (val == null || val.isEmpty() || val.equals("NaN")) {
					continue;
				}
				Integer sVal = Integer.parseInt(val);
				scores.add(i2, sVal);
			} // end of "value" loop
		} // end of ravens loop
		if (scores.size() != RAVENS_KEY.size()) {
			String err = "IGKf4dDataHelper.getRv2Rating() - Incomplete Ravens score data.  Only " + scores.size()
					+ " of " + RAVENS_KEY.size() + " responses available. Ravens Not Scored.";
			log.error(err);
			throw new Exception(err);
		}

		// Score
		int ravScore = 0;
		for (int idx = 0; idx < RAVENS_KEY.size(); idx++) {
			int val = scores.get(idx);
			if (RAVENS_KEY.get(idx) == val) {
				ravScore++;
			}
		}
		return ravScore;
	}
	
	public static PrnData doRavens2Norming(double scaleScore){
		// Norm it
		log.debug("Norming Ravens 2 Scale Score: {}", scaleScore);
		Norm nn = new Norm(SPEC_RAV_MEAN, SPEC_RAV_STDEV);
		log.debug("Using Mean: {}, and STD DEV: {}", SPEC_RAV_MEAN, SPEC_RAV_STDEV);
		double z = nn.calcZScore(scaleScore);
		log.debug("RV2 zScore: {}", z);
		int pctl = Norm.calcIntPctlFromZ(z);
		log.debug("RV2 Percentile: {}", pctl);
		String rate = Norm.calcPdiRating(pctl);

		log.debug("Calculated RV2 Rating: {}", rate);
		//this.setRv2Rating(rate);
		// OK
		PrnData ret = new PrnData();
		ret.setRate(Float.parseFloat(rate));
		return ret;
	}

	
	/**
	 * 	persistRv2Raw - Writes the data out to the results table
	 * 
	 * @param raw
	 * @param ppt
	 * @param proj
	 */
	private void persistRv2Raw(String raw, String ppt, String proj)
	{
		StringBuffer sb = new StringBuffer();
		sb.append("INSERT INTO results (participantId, projectId, instrumentCode, spssValue, rawScore, lastDate, lastUser) ");
		sb.append("  VALUES(" + ppt + ", " + proj +", 'rv2', 'RAVENSB', " + raw + ", GETDATE(), 'rv2hlpr') ");

		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
		if (dlps.isInError())
		{
			log.error("persistRv2Raw: Error preparing results insert query. {}", dlps.toString());
			return;
		}

		DataResult dr = null;
		try
		{
			dr = V2DatabaseUtils.insert(dlps);
			if (dr.getRowCount() != 1)
			{
				log.error("persistRv2Raw: row cnt (" + dr.getRowCount() + ") is not = 1. ppt=" + ppt + ", proj=" + proj);
			}
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}
	

	public static PrnData getRvRating(String partId, Connection con) {
		double scaleScore = -1;
		StringBuffer sqlQuery = new StringBuffer();
		Statement stmt = null;
		ResultSet rs = null;
		String phase = "";
		phase = "Score query execution";
		sqlQuery.append("SELECT rr.spssValue AS spssValue, ");
		sqlQuery.append("       rr.rawScore AS scaleScore ");
		sqlQuery.append("  FROM results rr ");
		sqlQuery.append("  WHERE rr.participantId = " + partId + " ");
		sqlQuery.append("    AND rr.instrumentCode IN ('rv') ");
		sqlQuery.append("  ORDER BY rr.spssValue, rr.lastDate");
		//Connection con = null;
		try {
			con = AbyDDatabaseUtils.getDBConnection();
			phase = "Norm query data retrieval";
			stmt = con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());
			if (! rs.isBeforeFirst()){
				return null;
			} else {
				while (rs.next())
				{
					scaleScore = rs.getDouble("scaleScore");
					break;
					
				}
			}
		} catch (Exception e) {
			log.error("Error occurred while attempting to retrieve RV score: {}", e.getMessage());
			e.printStackTrace();
			return null;
		}
		
		// Norm it

		PrnData prn = Rv2Helper.doRavens2Norming(scaleScore);
		//this.setRv2Rating(String.valueOf(prn.getRate()));
		return prn;
		
		
	}

}
