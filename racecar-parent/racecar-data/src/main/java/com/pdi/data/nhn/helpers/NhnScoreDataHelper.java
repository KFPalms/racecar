/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.nhn.helpers;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.pdi.data.constants.V2InstrumentConstants;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;
import com.pdi.reporting.request.ReportingRequest;
import com.pdi.reporting.response.ReportingResponse;
import com.pdi.webservice.NhnWebserviceUtils;
import com.pdi.xml.XMLUtils;

/*
 * The helper class that gets data back from Ninth House.net
 */
public class NhnScoreDataHelper
{
	//
	// Static data.
	//
	
	// don't know if I'll need these for sure, cuz I'm not certain what's
	// needing to be sent over... 
	private static final String RAVENS_B_NHN_ID = "ORG_RV_1.SCO_RV_1";
	private static final String WATSON_E_NHN_ID = "ORG_WG_1.SCO_WG_1";
	
	
	//
	// Static methods.
	//
	
	/*
	 * Gets data from 9th House.net, rolls up the results (generates raw scale scores),
	 * and return the data so it can be placed into the V2 results table.
	 *
	 * I think this is going to be called from the 
	 * pdi-web-participant/WebContent/track.jsp, around line 84.
	 *
	 * The overall process will be as follows:
	 * 1. Set up the request (passing )
	 * 		Dev URL:
			http://mspcidev1data/NH_SA?pid=XXXX&eid=XXXX
			Prod URL:
			http://mspci1data/NH_SA?pid=XXXX&eid=XXXX
			example URL:
			<http://mspcidev1data/NH_SA?pid=AbyDx009009&eid=ORG_WG_1.SCO_WG_1>
			
			Sameer's castIron url wants the NHN personId and the NHN module id, as
			noted in the example above.
			
	 * 2. Make the request (returns a Document object)
	 * 3. Set up the IndividualReport object with base data and raw (response) data from doc
	 * 4. Score the data
	 * 5. Return the data
	 * 
	 * @param String v2Cid - v2 candidate id
	 * @param String v2Pid - v2 project id
	 * @param String nhnMid - 9th house module id
	 */
	
	public static IndividualReport fetchNhnData(String v2Cid, String v2Pid, String v2ModId)
		throws Exception
	{
		
		//System.out.println("fetchNhnData : " + v2Cid + " : " + v2Pid + " : " + v2ModId);		
		
		// Create the return IndividualReport object and the
		// ReportData object that will contain the instrument data
		IndividualReport ir = new IndividualReport();
		ReportData	rd = new ReportData();
		
		// create the personId AND moduleId for nhn
		// to pass into the CastIron request...
		String pid = "AbyDx" + v2Cid +"-" + v2Pid; 
		String nhnMid = "";
		
		if(v2ModId.equals(V2InstrumentConstants.RAVENS_B_V2_ID))
			nhnMid = RAVENS_B_NHN_ID;
		if(v2ModId.equals(V2InstrumentConstants.WATSON_E_V2_ID))
			nhnMid = WATSON_E_NHN_ID;
		
		// set up the IndividualReport data for the appropriate module
		if(nhnMid.equals(RAVENS_B_NHN_ID)){
			ir.getReportData().put(ReportingConstants.RC_RAVENS_B, rd);
			ir.setReportCode(ReportingConstants.RC_RAVENS_B);
			rd.getDisplayData().put("V2_MODULE_ID", V2InstrumentConstants.RAVENS_B_V2_ID);		
			ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getDisplayData().put("CONSTRUCT_ID_CORRECT", "FCNEVHUL");
			// These construct ids are for the wrong module
			//ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getDisplayData().put("CONSTRUCT_ID_UNANSWERED", "HQAGQXCW");
			//ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getDisplayData().put("CONSTRUCT_ID_INCORRECT", "FDXKKUON");
			ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getDisplayData().put("CONSTRUCT_ID_UNANSWERED", "FDVTZYRA");
			ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getDisplayData().put("CONSTRUCT_ID_INCORRECT", "FFSALJFE");
			
		}else if(nhnMid.equals(WATSON_E_NHN_ID)){
			ir.getReportData().put(ReportingConstants.RC_WG_E, rd);
			ir.setReportCode(ReportingConstants.RC_WG_E);
			rd.getDisplayData().put("V2_MODULE_ID", V2InstrumentConstants.WATSON_E_V2_ID);
			// Construct ID was in the wrong place (was in the ir, now in the WG-E rd)
			//ir.getDisplayData().put("CONSTRUCT_ID", "FBGFXLHU");
			ir.getReportData().get(ReportingConstants.RC_WG_E).getDisplayData().put("CONSTRUCT_ID", "FBGFXLHU");
		}else {
			// this is  an error condition... 
			throw new Exception("Invalid module Id.  Cannot process incoming data.");
		}	

		// Get the raw response data from NHN (via Cast Iron)
		NhnWebserviceUtils util = new NhnWebserviceUtils();
		Document doc = util.requestNhnData(pid, nhnMid);
		
		//System.out.println(XMLUtils.nodeToString(doc.getFirstChild()));
		
		// Get the ScoreData node and parse out the V2 job (ENAGAGEMENT_ID)
		// and candidate (PERSON_ID) attributes
		NodeList partDataNodes = XMLUtils.getElements(doc, "//ScoreData");
		if (partDataNodes.getLength() < 1)
		{
			
			return null;
		}

		// set the Display Data  -- there will be only one row... 
		Node partData = partDataNodes.item(0);
		//System.out.println("--> ENGAGEMENT_ID=" + XMLUtils.getAttributeValue(partData, "ENGAGEMENT_ID") +
		//			"--> PERSON_ID=" + XMLUtils.getAttributeValue(partData, "PERSON_ID"));

		// Put the data into the ReportData object.
		// NOTE:  We put this data into the DisplayData bucket of the 
		// RerportData object because there is no explicit bucket into which it can
		// be placed.  Downstream processes will pick it up from there.
		rd.getDisplayData().put("ENGAGEMENT_ID", XMLUtils.getAttributeValue(partData, "ENGAGEMENT_ID"));
		rd.getDisplayData().put("PERSON_ID", XMLUtils.getAttributeValue(partData, "PERSON_ID"));
		
		// making sure that the v2 data is in place, because I'm not certain what's going
		// to be returned yet... 
		rd.getDisplayData().put("V2_CANDIDATE_ID", v2Cid);
		rd.getDisplayData().put("V2_PROJECT_ID", v2Pid);


		// Get the response data...
		// Get the Task nodes (XML is same schema as for ADAPT) - should be only 1
		NodeList taskNodes =  XMLUtils.getElements(doc, "//Task");
		//System.out.println("Task node count=" + nodes.getLength());
		if (taskNodes.getLength() < 1)
		{
			// nothing to do... just leave
			return null;
		}

		// Get the first task (should only be the one)
		Node task = taskNodes.item(0);
		// get the status
		String stat = XMLUtils.getAttributeValue(task, "COMPLETION_STATUS");
		if (stat == null || stat.length() < 1)
			stat = "";
		rd.getDisplayData().put("STATUS", stat);
		//System.out.println("Setting the NHN status to " + stat);
		
		NodeList answerNodes = XMLUtils.getElements(task, "Answer");
		if (answerNodes.getLength() < 1)
		{
			return null;
		}

		// Process the answers
		for (int i=0; i < answerNodes.getLength(); i++)
		{
			Node ans = answerNodes.item(i);
			//System.out.println("    " + XMLUtils.getAttributeValue(ans, "QUESTION_ID") +
			//			"--> resp=" + XMLUtils.getAttributeValue(ans, "RESPONSE"));

			// Put the data into the ReportData object
			rd.getRawData().put(XMLUtils.getAttributeValue(ans, "QUESTION_ID"), XMLUtils.getAttributeValue(ans, "RESPONSE"));
		}	// End of answers loop

		// Score the data... Scoring means generating the raw scale scores at this point.
		
		ir.setReportType(ReportingConstants.REPORT_TYPE_SCORE);
		ReportingRequest request = new ReportingRequest();
		request.addReport(ir);
		ReportingResponse response = request.generateReports();
		IndividualReport resp = (IndividualReport)response.getReports().get(0);
		
		// Now put the data into the v2 database.... 
		// use pdi-data-v2  com.pdi.data.v2.helpers.SaveConstructDataHelper
		//  in the construct data helper, the method calls are all at the end of the
		//  file.. starting around line 2168...
		//SaveConstructDataHelper dataHelper = new SaveConstructDataHelper();
		ir.getDisplayData().put("NHN_STATUS", stat);
		return resp;
	}
	
	//
	// Instance data.
	//

	//
	// Constructors.
	//

	//
	// Instance methods.
	//
	
}
