/**
 * Copyright (c) 2009, 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import com.pdi.data.abyd.dto.common.DeliveryMethodDTO;
import com.pdi.data.abyd.dto.common.EGBarDisplayData;
import com.pdi.data.abyd.dto.common.EGCompDisplayData;
import com.pdi.data.abyd.dto.common.EGFullDataDTO;
import com.pdi.data.abyd.dto.common.EGIrDisplayData;
import com.pdi.data.abyd.dto.common.EGOverallData;
import com.pdi.data.abyd.dto.common.EGRaterNameData;
import com.pdi.data.abyd.util.Utils;
import com.pdi.data.dto.Participant;
import com.pdi.data.dto.Project;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.IProjectHelper;
//import com.pdi.data.nhn.util.NhnDatabaseUtils;
//import com.pdi.data.util.DataLayerPreparedStatement;
//import com.pdi.data.util.DataResult;
//import com.pdi.data.util.RequestStatus;
import com.pdi.data.util.language.DefaultLanguageHelper;
import com.pdi.data.util.language.LanguageItemDTO;
import com.pdi.data.util.language.LanguageListHelper;
//import com.pdi.logging.LogWriter;

/**
 * EGFullDataHelper contains the code needed to extract and return an
 * EGFullDataDTO. It provides data base access and data reduction for the
 * Assessment-by-Design Evaluation Guide generation app
 *
 * @author Ken Beukelman
 */
public class EGDataHelper {
	//
	// Static data.
	//
	public static final long OTHER_LANG_ID = 9999;
	public static final String OTHER_LANG_CODE = "XX";
	public static final String OTHER_LANG_DISP_NAME = "Other";

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private final Connection con;
	private String lpId;

	// stuff from linkparms
	private String partId;
	private long projectId;
	private long dnaId;
	private long moduleId;
	private String partName; // Psrticipant name (Last, First)

	// default language ID
	private long dfltLangId = 0;

	// instantiate a return object
	private final EGFullDataDTO ret = new EGFullDataDTO();

	private Utils utl = null;

	//
	// Constructors.
	//

	/*
	 * "Standard" Constructor - Data from a linkParam
	 */
	public EGDataHelper(Connection con, String lpId) throws Exception {
		long longId = 0;
		// Ensure there is a linkparams id...
		if (lpId == null || lpId.length() < 1) {
			throw new Exception("A valid linkparms id is required to fetch Eval Guide data.");
		}
		// ...and that it is numeric...
		try {
			longId = Long.parseLong(lpId);
		} catch (NumberFormatException e) {
			throw new Exception("A valid linkparms id must be numeric.  Invalid lpId=" + lpId);
		}
		// ...and that it is not 0
		if (longId == 0) {
			throw new Exception("A valid linkparms id cannot be 0.");
		}

		this.con = con;
		this.lpId = lpId;

		// Get the linkparams data into the system
		fetchLinkparams();

		// Get the default language
		this.dfltLangId = DefaultLanguageHelper.fetchDefaultLanguageId();
	}

	/*
	 * Constructor - Explicitly passed data - limited usage
	 */
	public EGDataHelper(Connection con, String partId, long dnaId, long moduleId) throws Exception {
		long longId = 0;

		if (partId == null || partId.length() < 1) {
			throw new Exception("A valid Participant ID is required to fetch Eval Guide data.");
		}
		// ...and that it is numeric...
		try {
			longId = Long.parseLong(partId);
		} catch (NumberFormatException e) {
			throw new Exception("A valid Participant ID id must be numeric.  Invalid lpId=" + partId);
		}
		// ...and that it is not 0
		if (longId == 0) {
			throw new Exception("A valid Participant ID id cannot be 0.");
		}
		if (dnaId < 1) {
			throw new Exception("A valid DNA ID is required to fetch Eval Guide data.");
		}
		if (moduleId < 1) {
			throw new Exception("A valid Module ID is required to fetch Eval Guide data.");
		}

		this.con = con;
		this.partId = partId;
		this.dnaId = dnaId;
		this.moduleId = moduleId;

		// Get the default language
		this.dfltLangId = DefaultLanguageHelper.fetchDefaultLanguageId();

		// Get the project ID
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT dna.projectId ");
		sqlQuery.append("  FROM pdi_abd_dna dna ");
		sqlQuery.append("  WHERE dna.dnaId = " + this.dnaId + " ");
		sqlQuery.append("    AND dna.active = 1");

		try {
			stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			if (!rs.isBeforeFirst()) {
				throw new Exception("No valid data found for DNA ID " + this.dnaId);
			}

			rs.next();
			this.projectId = rs.getLong("projectId");
		} catch (SQLException ex) {
			throw new Exception("SQL in EGDataHelper constructor.  " + "SQLException: " + ex.getMessage() + ", "
					+ "SQLState: " + ex.getSQLState() + ", " + "VendorError: " + ex.getErrorCode());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					/*  Swallow the error */ }
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					/*  Swallow the error */ }
				stmt = null;
			}
		}

	}

	//
	// Instance methods.
	//

	/**
	 * Fetch the data from the linkparams table
	 *
	 * @throws Exception
	 */
	private void fetchLinkparams() throws Exception {
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT lp.participantId, ");
		sqlQuery.append("       lp.projectId, ");
		sqlQuery.append("       dna.dnaId, ");
		sqlQuery.append("       lp.moduleId ");
		sqlQuery.append("  FROM linkparams lp ");
		sqlQuery.append("    LEFT JOIN pdi_abd_dna dna ON dna.projectId = lp.projectId ");
		sqlQuery.append("  WHERE lp.linkparamId = " + this.lpId + " ");
		sqlQuery.append("    AND lp.active = 1");

		try {
			stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			if (!rs.isBeforeFirst()) {
				throw new Exception("No linkparams entry for key " + this.lpId);
			}

			// Get the first result row (only row)
			rs.next();

			this.partId = rs.getString("participantId");
			this.projectId = rs.getLong("projectId");
			this.dnaId = rs.getLong("dnaId");
			this.moduleId = rs.getLong("moduleId");

			if (dnaId == 0) {
				throw new Exception(
						"No DNA id found for project " + rs.getString("projectId") + ".  LPID=" + this.lpId);
			}

			// Get participant info
			SessionUser su = HelperDelegate.getSessionUserHelperHelper().createSessionUser();
			Participant participant = HelperDelegate.getParticipantHelper("com.pdi.data.nhn.helpers.delegated")
					.fromId(su, this.partId);

			this.partName = participant.getLastName();
			this.partName += ", ";
			this.partName += participant.getFirstName();

			return;
		} catch (SQLException ex) {
			// handle any errors
			throw new Exception("SQL in EG fetchLinkparams.  " + "SQLException: " + ex.getMessage() + ", "
					+ "SQLState: " + ex.getSQLState() + ", " + "VendorError: " + ex.getErrorCode());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					/*  Swallow the error */ }
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					/*  Swallow the error */ }
				stmt = null;
			}
		}
	}

	/**
	 * Main controller to fetch the data needed for the Eval Guide generation
	 * app.
	 *
	 * @return the EntryStateDTO object
	 * @throws Exception
	 */
	public EGFullDataDTO getEGImpactandOverallData() throws Exception {
		// Set up the key portion of the output
		this.ret.setParticipantId(this.partId);
		this.ret.setDnaId(this.dnaId);
		this.ret.setModuleId(this.moduleId);

		// Get high level response data (used on pag1, IR, and overall
		getEGRespData();

		// get the IR data
		getIrData();

		return this.ret;
	}

	/**
	 * Main controller to fetch the data needed for the Eval Guide generation
	 * app.
	 *
	 * @return the EntryStateDTO object
	 * @throws Exception
	 */
	public EGFullDataDTO getEGDisplayData() throws Exception {
		// Set up the key portion of the output
		this.ret.setParticipantId(this.partId);
		this.ret.setDnaId(this.dnaId);
		this.ret.setModuleId(this.moduleId);

		// Set the external data flag for this module

		// Get the languages list (used also in setting up the selected
		// language)
		getEgLanguages();

		// Get the delivery method list
		getEgDeliveryMethods();

		// Get high level response data (used on pag1, IR, and overall
		getEGRespData();

		// Get the client name
		getClientName();

		// Get the rest of the header data (not passed from V2 or invocation)
		getHeaderData();

		// get the IR data
		getIrData();

		// get the comp data (must be after getHeaderData() call as that sets
		// the external data flag)
		getCompData();

		return this.ret;
	}

	/*
	 * getLangList - Get the Eval Guide language List
	 */
	private void getEgLanguages() throws Exception {
		LanguageListHelper langHelper = new LanguageListHelper(this.con);
		ArrayList<LanguageItemDTO> list = langHelper.getEgLangList();

		// Add the "Other" item
		LanguageItemDTO itm = new LanguageItemDTO();
		itm.setId(OTHER_LANG_ID);
		itm.setCode(OTHER_LANG_CODE);
		itm.setDisplayName(OTHER_LANG_DISP_NAME);
		list.add(itm);

		this.ret.setLangList(list);
	}

	private void getEgDeliveryMethods() throws Exception {
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT deliveryMethodId, ");
		sqlQuery.append("       displayName, ");
		sqlQuery.append("       seq ");
		sqlQuery.append("  FROM pdi_abd_delivery_method;");
		try {
			stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());
			ArrayList<DeliveryMethodDTO> methods = new ArrayList<DeliveryMethodDTO>();
			while (rs.next()) {
				DeliveryMethodDTO methodDTO = new DeliveryMethodDTO();
				methodDTO.setDeliveryMethodId(rs.getInt("deliveryMethodId"));
				methodDTO.setDisplayName(rs.getString("displayName"));
				methodDTO.setSeq(rs.getInt("seq"));
				methods.add(methodDTO);
			}

			ret.setMethodList(methods);
		} catch (Exception e) {
			throw new Exception("SQL in EG getEgDeliveryMethods.  " + e.toString());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					/*  Swallow the error */ }
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					/*  Swallow the error */ }
				stmt = null;
			}
		}
	}

	/**
	 * Fetch response data that is in the "base" of the EGFullDataDTO. This
	 * includes data from the first page (consultant name), the IR section (IR
	 * notes), and pretty much all of the data from the Overall section
	 *
	 * @throws Exception
	 */

	private void getEGRespData() throws Exception {
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT egr.egSubmitted, ");
		sqlQuery.append("       egr.raterFirstName, ");
		sqlQuery.append("       egr.raterLastName, ");
		sqlQuery.append("       egr.simLangId, ");
		sqlQuery.append("       egr.simLangOther, ");
		sqlQuery.append("       egr.deliveryMethodId, ");
		sqlQuery.append("       egr.irNotes, ");
		sqlQuery.append("       egr.opr, ");
		sqlQuery.append("       egr.briefDesc, ");
		sqlQuery.append("       egr.keyStrengths, ");
		sqlQuery.append("       egr.keyNeeds, ");
		sqlQuery.append("       egr.coachNotes, ");
		sqlQuery.append("       egr.themesFromPeers, ");
		sqlQuery.append("       egr.themesFromDirectReports, ");
		sqlQuery.append("       egr.themesFromBoss ");
		sqlQuery.append("  FROM pdi_abd_eg_response egr ");
		sqlQuery.append("  WHERE egr.participantId = " + this.partId + " ");
		sqlQuery.append("    AND egr.dnaId = " + this.dnaId + " ");
		sqlQuery.append("AND egr.moduleId = " + this.moduleId);

		try {
			this.ret.setRaterNameData(new EGRaterNameData());
			this.ret.setOverallData(new EGOverallData());

			stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			if (!rs.isBeforeFirst()) {
				// Set numerics to default values
				this.ret.setSubmitted(false);
				this.ret.getOverallData().setOpr(0);
			} else {
				rs.next();
				this.ret.setSubmitted(rs.getBoolean("egSubmitted"));
				this.ret.getRaterNameData().setRaterFirstName(rs.getString("raterFirstName"));
				this.ret.getRaterNameData().setRaterLastName(rs.getString("raterLastName"));
				this.ret.setSimLangOther(rs.getString("simLangOther"));
				this.ret.setDeliveryMethodId(rs.getInt("deliveryMethodId"));
				this.ret.setIrNotes(rs.getString("irNotes"));
				this.ret.getOverallData().setOpr(rs.getInt("opr"));
				this.ret.getOverallData().setBriefDesc(rs.getString("briefDesc"));
				this.ret.getOverallData().setKeyStrengths(rs.getString("keyStrengths"));
				this.ret.getOverallData().setKeyDevNeeds(rs.getString("keyNeeds"));
				this.ret.getOverallData().setCoachNotes(rs.getString("coachNotes"));
				this.ret.getOverallData().setThemesFromPeers(rs.getString("themesFromPeers"));
				this.ret.getOverallData().setThemesFromDirectReports(rs.getString("themesFromDirectReports"));
				this.ret.getOverallData().setThemesFromBoss(rs.getString("themesFromBoss"));

				// Now do the selected language
				long simLangId = rs.getLong("simLangId");
				// Now set up the selected language
				for (LanguageItemDTO itm : ret.getLangList()) {
					if (itm.getId() == simLangId) {
						this.ret.setSimLangItem(itm.makeCopy());
						break;
					}
				}
			}
		} catch (SQLException ex) {
			throw new Exception("SQL in EG getEGRespData.  " + "SQLException: " + ex.getMessage() + ", " + "SQLState: "
					+ ex.getSQLState() + ", " + "VendorError: " + ex.getErrorCode());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					/*  Swallow the error */ }
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					/*  Swallow the error */ }
				stmt = null;
			}
		}
	}

	/**
	 * Fetch some external data. Adds the info to the instance DTO object.
	 *
	 * @throws Exception
	 */
	private void getClientName() throws Exception {
		// Create a dummy session
		SessionUser session = new SessionUser();

		// Instantiate a project helper
		IProjectHelper iproject = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated");

		// Get the Project object
		Project project = iproject.fromId(session, "" + this.projectId);

		// Fill the internal holder
		this.ret.setClientName(project.getClient().getName());

		return;
	}

	/**
	 * Fetch the rest of the data for the header.
	 *
	 * @throws Exception
	 */
	private void getHeaderData() throws Exception {
		String modIntName = "";

		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT dna.dnaName as jobName, ");
		sqlQuery.append("       t1.text as modelName, ");
		sqlQuery.append("       t2.text as simName, ");
		sqlQuery.append("       sim.specialType, ");
		sqlQuery.append("       sim.internalName, ");
		sqlQuery.append("       mdl.allowCso ");
		sqlQuery.append("  FROM pdi_abd_dna dna ");
		sqlQuery.append("    LEFT JOIN pdi_abd_model mdl ON mdl.modelId = dna.modelId ");
		sqlQuery.append("    LEFT JOIN pdi_abd_text t1 ON (t1.textId = mdl.textId AND t1.languageId = "
				+ this.dfltLangId + ") ");
		sqlQuery.append("    INNER JOIN pdi_abd_module sim On sim.moduleId = " + this.moduleId + " ");
		sqlQuery.append("    LEFT JOIN pdi_abd_text t2 ON (t2.textId = sim.textId AND t2.languageId = "
				+ this.dfltLangId + ") ");
		sqlQuery.append("  WHERE dna.dnaId = " + this.dnaId);
		// System.out.println("getHeaderData..." + sqlQuery.toString());
		try {
			stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			if (!rs.isBeforeFirst()) {
				throw new Exception("Unable to fetch header data:  DNA=" + this.dnaId + ", moduleId=" + this.moduleId);
			}

			// Get the result row
			rs.next();
			this.ret.setJobName(rs.getString("jobName"));
			this.ret.setModelName(rs.getString("modelName"));
			this.ret.setSimName(rs.getString("simName"));
			this.ret.setSpecialType(rs.getString("specialType"));
			this.ret.setAllowCso(rs.getBoolean("allowCso"));

			modIntName = rs.getString("internalName");

			// Add the participant name that we got on invocation
			this.ret.setPartName(this.partName);
		} catch (SQLException ex) {
			// handle any errors
			throw new Exception("SQL in EG getHeaderData.  " + "SQLException: " + ex.getMessage() + ", " + "SQLState: "
					+ ex.getSQLState() + ", " + "VendorError: " + ex.getErrorCode());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					/*  Swallow the error */ }
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					/*  Swallow the error */ }
				stmt = null;
			}
		}

		// Assumes that there will be only one inbox type in a project
		int cnt = 0;
		boolean hasBizsim = false;
		if (isInstValid("iblva")) {
			this.ret.setIblva(true);
			this.ret.setCrType("IBLVA");
			hasBizsim = true;
			cnt++;
		}
		if (isInstValid("nis")) {
			this.ret.setNis(true);
			this.ret.setCrType("NIS");
			hasBizsim = true;
			cnt++;
		}
		if (isInstValid("ismll")) {
			this.ret.setIsmll(true);
			this.ret.setCrType("ISMLL");
			hasBizsim = true;
			cnt++;
		}
		if (isInstValid("isbul")) {
			this.ret.setIsbul(true);
			this.ret.setCrType("ISBUL");
			hasBizsim = true;
			cnt++;
		}
		if (isInstValid("isz")) {
			this.ret.setIsz(true);
			this.ret.setCrType("ISZ");
			hasBizsim = true;
			cnt++;
		}
		if (isInstValid("eis")) {
			this.ret.setEis(true);
			this.ret.setCrType("EIS");
			hasBizsim = true;
			cnt++;
		}
		if (isInstValid("bre2")) {
			this.ret.setBre2(true);
			this.ret.setCrType("BRE2");
			hasBizsim = true;
			cnt++;
		}
		if (isInstValid("bresea")) {
			this.ret.setBresea(true);
			this.ret.setCrType("BRESEA");
			hasBizsim = true;
			cnt++;
		}
		if (isInstValid("sfeceo")) {
			this.ret.setSfeceo(true);
			this.ret.setCrType("SFECEO");
			// special circumstance... want the CRC viewable in the EG despite
			// the fact this is not an In-box
			// hasBizsim = true;
			this.ret.setHasCrc(true);
			cnt++;
		}
		if (isInstValid("bremed")) {
			this.ret.setBremed(true);
			this.ret.setCrType("BREMED");
			hasBizsim = true;
			cnt++;
		}
		if (cnt > 1) {
			throw new Exception(
					"Multiple Inbox types in EG getHeaderData(); " + "proj=" + this.projectId + ", ppt=" + this.partId);
		}

		if (hasBizsim) {
			if ("In-Box".equals(modIntName) || "Business Review Exercise".equals(modIntName)) // ||
			// "Strategy and FinEx".equals(modIntName))
			{
				this.ret.setHasCrc(true);
			}
		}

		return;
	}

	/**
	 * isInstValid - Probably should be "isInstPresent"
	 * 
	 * @return flag that says designated instrument is valid for this ppt/proj
	 */
	private boolean isInstValid(String abbv) {
		if (utl == null) {
			utl = new Utils();
		}

		return utl.isInstValid(abbv, this.partId, "" + this.projectId);
	}

	/**
	 * Fetch the Impact Rating data.
	 *
	 * @throws Exception
	 */
	private void getIrData() throws Exception {
		Statement stmt = null;
		ResultSet rs = null;
		StringBuffer sqlQuery = new StringBuffer();
		String context = "display";

		try {
			LinkedHashMap<Integer, EGIrDisplayData> irDat = new LinkedHashMap<Integer, EGIrDisplayData>();

			// get the display data objects into a linked hash table
			sqlQuery.append("SELECT ir.irSequence, ");
			sqlQuery.append("       txt.text as irText ");
			sqlQuery.append("  FROM pdi_abd_dna dna ");
			sqlQuery.append("    INNER JOIN pdi_abd_eg_ir ir ON (ir.modelId = dna.modelId ");
			sqlQuery.append("                                    AND ir.moduleId = " + this.moduleId + ") ");
			sqlQuery.append("    LEFT JOIN pdi_abd_text txt ON (txt.textId = ir.textId AND txt.languageId = "
					+ this.dfltLangId + ") ");
			sqlQuery.append("  WHERE dna.dnaId = " + this.dnaId + " ");
			sqlQuery.append("  ORDER BY ir.irSequence");

			stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			while (rs.next()) {
				EGIrDisplayData item = new EGIrDisplayData();
				item.setIrSeq(rs.getInt("irSequence"));
				item.setIrText(rs.getString("irText"));
				irDat.put(new Integer(item.getIrSeq()), item);
			}

			// clean up
			try {
				rs.close();
			} catch (SQLException sqlEx) {
				/*  Swallow the error */ }
			rs = null;
			try {
				stmt.close();
			} catch (SQLException sqlEx) {
				/*  Swallow the error */ }
			stmt = null;

			// get the score data and add it to the appropriate objects
			context = "score";
			sqlQuery.setLength(0);
			sqlQuery.append("SELECT irr.irSequence, ");
			sqlQuery.append("irr.irScore ");
			sqlQuery.append("  FROM pdi_abd_eg_ir_response irr ");
			sqlQuery.append("  WHERE irr.participantId = '" + this.partId + "' ");
			sqlQuery.append("    AND irr.dnaId = " + this.dnaId + " ");
			sqlQuery.append("    AND irr.moduleId = " + this.moduleId);

			stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			while (rs.next()) {
				Integer key = new Integer(rs.getInt("irSequence"));
				irDat.get(key).setIrScore(rs.getInt("irScore"));
			}

			// build the array in order
			ArrayList<EGIrDisplayData> al = new ArrayList<EGIrDisplayData>();
			for (Iterator<Map.Entry<Integer, EGIrDisplayData>> itr = irDat.entrySet().iterator(); itr.hasNext();) {
				Map.Entry<Integer, EGIrDisplayData> ent = itr.next();
				al.add(ent.getValue());
			}
			this.ret.setIrArray(al);

			return;
		} catch (SQLException ex) {
			throw new Exception("SQL in EG getIrData (" + context + ").  " + "SQLException: " + ex.getMessage() + ", "
					+ "SQLState: " + ex.getSQLState() + ", " + "VendorError: " + ex.getErrorCode());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					/*  Swallow the error */ }
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					/*  Swallow the error */ }
				stmt = null;
			}
		}
	}

	/**
	 * Fetch the Competency data.
	 *
	 * @throws Exception
	 */
	private void getCompData() throws Exception {
		Statement stmt = null;
		ResultSet rs = null;
		String context = "bars";
		HashMap<Long, String> compDesc = new HashMap<Long, String>();
		StringBuffer sqlQuery = new StringBuffer();

		try {
			HashMap<Long, ArrayList<EGBarDisplayData>> barMap = new HashMap<Long, ArrayList<EGBarDisplayData>>();

			// Note that hasExtCompScores is set in getHeaderData() so the call
			// to
			// getCompData() must be (and is) after the call to getHeaderData()
			if (this.ret.getSpecialType().equals("ES")) {
				// No Bars... don't fill the BAR data array
			} else {
				// Get all of the bar data, but not for externally scored
				// modules
				// NOTE: This query is much like the one for bar data in
				// IGCompLineDataHelper.
				// We replicate it here (with minor changes) because the data
				// required
				// is somewhat different and, therefore, the subsequent
				// processing is
				// different. Changes to the data model that require changes in
				// this
				// query may require changes in that one as well.
				sqlQuery.append("SELECT bar.competencyId, ");
				sqlQuery.append("       bar.barSequence, ");
				sqlQuery.append("       ISNULL(bresp.barScore,-1) as barScore, ");
				sqlQuery.append("       t1.text as hiText, ");
				sqlQuery.append("       t2.text as midText, ");
				sqlQuery.append("       t3.text as loText, ");
				sqlQuery.append("       t4.text as hiExText, ");
				sqlQuery.append("       t5.text as midExText, ");
				sqlQuery.append("       t6.text as loExText, ");
				sqlQuery.append("       t7.text as modCompDesc ");
				sqlQuery.append("  FROM pdi_abd_eg_bar bar ");
				sqlQuery.append("    INNER JOIN pdi_abd_dna dna ON dna.dnaId = " + this.dnaId + " ");
				sqlQuery.append(
						"    LEFT JOIN pdi_abd_eg_bar_response bresp ON (bresp.participantId = '" + this.partId + "' ");
				sqlQuery.append("                                            AND bresp.dnaId = dna.dnaId ");
				sqlQuery.append("                                            AND bresp.moduleId = bar.moduleId ");
				sqlQuery.append(
						"                                            AND bresp.competencyId = bar.competencyId ");
				sqlQuery.append(
						"                                            AND bresp.barSequence = bar.barSequence) ");
				sqlQuery.append("    LEFT JOIN pdi_abd_text t1 ON (t1.textId = bar.hiTextId AND t1.languageId = "
						+ this.dfltLangId + ") ");
				sqlQuery.append("    LEFT JOIN pdi_abd_text t2 ON (t2.textId = bar.medTextId AND t2.languageId = "
						+ this.dfltLangId + ") ");
				sqlQuery.append("    LEFT JOIN pdi_abd_text t3 ON (t3.textId = bar.loTextId AND t3.languageId = "
						+ this.dfltLangId + ") ");
				sqlQuery.append("    LEFT JOIN pdi_abd_text t4 ON (t4.textId = bar.hiExTextId AND t4.languageId = "
						+ this.dfltLangId + ") ");
				sqlQuery.append("    LEFT JOIN pdi_abd_text t5 ON (t5.textId = bar.medExTextId AND t5.languageId = "
						+ this.dfltLangId + ") ");
				sqlQuery.append("    LEFT JOIN pdi_abd_text t6 ON (t6.textId = bar.loExTextId AND t6.languageId = "
						+ this.dfltLangId + ") ");
				sqlQuery.append(
						"    INNER JOIN pdi_abd_mod_comp_desc mcd ON (mcd.modelId = bar.modelId and mcd.competencyId = bar.competencyId) ");
				sqlQuery.append("    LEFT JOIN pdi_abd_text t7 ON (t7.textId = mcd.textId AND t7.languageId = "
						+ this.dfltLangId + ") ");
				sqlQuery.append("  WHERE bar.modelId = dna.modelId ");
				sqlQuery.append("    AND bar.moduleId = " + this.moduleId + " ");
				sqlQuery.append("    AND bar.active = 1 ");
				sqlQuery.append("  ORDER BY bar.competencyId, bar.barSequence");
				stmt = this.con.createStatement();
				rs = stmt.executeQuery(sqlQuery.toString());

				// check for bar data - none is an error
				if (!rs.isBeforeFirst()) {
					throw new Exception("No bar data:  DNA=" + this.dnaId + ", moduleId=" + this.moduleId);
				}

				long curComp = 0;
				ArrayList<EGBarDisplayData> curBarArray = null;
				while (rs.next()) {
					// Is this a new Comp?
					long recComp = rs.getLong("competencyId");
					if (recComp != curComp) {
						// Set up for a new competency bar array
						curComp = recComp;
						curBarArray = new ArrayList<EGBarDisplayData>();
						barMap.put(new Long(curComp), curBarArray);

						// and save the description
						compDesc.put(new Long(recComp), rs.getString("modCompDesc"));
					}

					// Fill in the data for the bar...
					EGBarDisplayData newBar = new EGBarDisplayData();
					newBar.setBarSeq(rs.getInt("barSequence"));
					newBar.setBarScore(rs.getInt("barScore"));
					newBar.setBarHiText(rs.getString("hiText"));
					newBar.setBarMidText(rs.getString("midText"));
					newBar.setBarLoText(rs.getString("loText"));
					newBar.setBarHiExText(rs.getString("hiExText"));
					newBar.setBarMidExText(rs.getString("midExText"));
					newBar.setBarLoExText(rs.getString("loExText"));

					// ...and stick it in the current competency bar array
					curBarArray.add(newBar);
				} // End of BAR data rs loop

				// Done building bar arrays... clean up
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					/*  Swallow the error */ }
				rs = null;
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					/*  Swallow the error */ }
				stmt = null;
			} // End of extCompScores = false "if" statement

			// Now set up the comp query
			context = "competencies";
			sqlQuery.setLength(0);
			sqlQuery.append("SELECT dl.groupSeq, ");
			sqlQuery.append("       dl.competencyId, ");
			sqlQuery.append("       dl.compSeq, ");
			sqlQuery.append("       tc.text as compName, ");
			sqlQuery.append("       cresp.compNotes, ");
			sqlQuery.append("       cresp.orScoreIdx, ");
			sqlQuery.append("       cresp.extCompScore ");
			sqlQuery.append("  FROM pdi_abd_dna_link dl ");
			sqlQuery.append(
					"    LEFT JOIN pdi_abd_eg_comp_response cresp ON (cresp.participantId = '" + this.partId + "' ");
			sqlQuery.append("                                             AND cresp.dnaId = dl.dnaId ");
			sqlQuery.append("                                             AND cresp.moduleId = dl.moduleId ");
			sqlQuery.append("                                             AND cresp.competencyId = dl.competencyId) ");
			sqlQuery.append("    INNER JOIN pdi_abd_dna_grouping_value gv ON gv.groupId = dl.groupId ");
			sqlQuery.append("    INNER JOIN pdi_abd_competency cmp ON cmp.competencyId = dl.competencyId ");
			sqlQuery.append("    LEFT JOIN pdi_abd_text tc ON (tc.textId = cmp.textId AND tc.languageId = "
					+ this.dfltLangId + ") ");
			sqlQuery.append("  WHERE dl.dnaId = " + this.dnaId + " ");
			sqlQuery.append("    AND dl.moduleId = " + this.moduleId + " ");
			sqlQuery.append("    AND dl.active = 1 ");
			sqlQuery.append("    AND dl.isUsed = 1 ");
			sqlQuery.append("  ORDER BY dl.groupSeq, dl.compSeq ");

			stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			// check for comp data - none is an error
			if (!rs.isBeforeFirst()) {
				throw new Exception("No competency data:  DNA=" + this.dnaId + ", moduleId=" + this.moduleId);
			}

			// spin through the result set
			ArrayList<EGCompDisplayData> compArray = new ArrayList<EGCompDisplayData>();
			while (rs.next()) {
				// put the comp data into the comp object
				EGCompDisplayData item = new EGCompDisplayData();
				long compId = rs.getLong("competencyId");
				item.setCompId(compId);
				item.setCompName(rs.getString("compName"));
				item.setCompDesc(compDesc.get(new Long(compId)));
				item.setCompNotes(rs.getString("compNotes"));
				item.setOverideScoreIndex(rs.getInt("orScoreIdx"));
				if (this.ret.getSpecialType().equals("ES")) {
					// Externally scored... set the relevant data
					double es = rs.getDouble("extCompScore");
					if (rs.getObject("extCompScore") == null) {
						item.setExtCompScore("-1.00");
					} else if (es == 0.0) {
						item.setExtCompScore("0.00");
					} else {
						es = Math.floor((es * 100.0) + 0.505) / 100.0;
						item.setExtCompScore(String.format("%.2f", es));
					}
					item.setCompBars(new ArrayList<EGBarDisplayData>());
				} else {
					item.setExtCompScore(null);
					item.setCompBars(barMap.get(new Long(compId)));
				}

				// Add the comp to the com array
				compArray.add(item);
			}

			// set the comp array
			this.ret.setCompArray(compArray);

			// now calculate the competency score
			// Note that it may be overridden
			for (EGCompDisplayData comp : this.ret.getCompArray()) {
				if (this.ret.getSpecialType().equals("ES")) {
					// calced score remains empty
					comp.setCalcedCompScore(null);
				} else {
					// Set the calced score
					double sum = 0;
					int cnt = 0;
					double avg = 0;
					if (comp.getOverideScoreIndex() > 0) {
						// We have an override. process it and scram
						int idx = comp.getOverideScoreIndex();
						avg = (idx + 1.0) / 2.0;
					} else {
						// "Normal" scoring
						for (EGBarDisplayData bar : comp.getCompBars()) {
							int scor = bar.getBarScore();
							if (scor > 0) {
								sum += scor;
								cnt++;
							}
						}
						avg = (cnt < 2) ? 0.0 : sum / cnt;
					}
					avg = Math.floor((avg * 100.0) + 0.505) / 100.0;

					comp.setCalcedCompScore(avg == 0.0 ? "N/A" : String.format("%.2f", avg));
				}
			}

			return;
		} catch (SQLException ex) {
			throw new Exception("SQL in EG getCompData (" + context + ").  " + "SQLException: " + ex.getMessage() + ", "
					+ "SQLState: " + ex.getSQLState() + ", " + "VendorError: " + ex.getErrorCode());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					/*  Swallow the error */ }
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					/*  Swallow the error */ }
				stmt = null;
			}
		}
	}
}
