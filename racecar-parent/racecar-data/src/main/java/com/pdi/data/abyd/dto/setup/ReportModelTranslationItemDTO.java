/**
 * Copyright (c) 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.dto.setup;


/**
 * ReportModelTranslationItemDTO is a container for the data required
 * for a single Report Model item (SF or Comp) to be translated
 * when translating the texts in an RM/CC model.
 * 
 * @author		Ken Beukelman
 */
public class ReportModelTranslationItemDTO
{
	//
	// Static data.
	//
	public static final String IT_SF = "S";
	public static final String IT_CMP = "C";

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private long _itemId;
	private String _itemType;
	private long _staticTextId;
	private String _referenceText;
	private long _userTextId;
	private String _translatedText;
	private boolean _changed = false;

	//
	// Constructors.
	//
	public ReportModelTranslationItemDTO()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//
	
	public String toString()
	{
		String ret = "";
		ret += "  Xlate Item:  ItemID=" + _itemId + ", type=" + _itemType +
			   ", statTxtID=" + _staticTextId + ", userTxtID=" + _userTextId +
			   ", chgd=" + _changed + "\n";
		ret += "    Ref text   =" + _referenceText + "\n";
		ret += "    Xlated text=" + (_translatedText == null ? "null" : _translatedText);
		
		return ret;
	}
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public long getItemId()
	{
		return _itemId;
	}

	public void setItemId(long value)
	{
		_itemId = value;
	}

	/*****************************************************************************************/
	public String getItemType()
	{
		return _itemType;
	}

	public void setItemType(String value)
	{
		_itemType = value;
	}

	/*****************************************************************************************/
	public long getStaticTextId()
	{
		return _staticTextId;
	}

	public void setStaticTextId(long value)
	{
		_staticTextId = value;
	}

	/*****************************************************************************************/
	public String getReferenceText()
	{
		return _referenceText;
	}

	public void setReferenceText(String value)
	{
		_referenceText = value;
	}

	/*****************************************************************************************/
	public long getUserTextId()
	{
		return _userTextId;
	}

	public void setUserTextId(long value)
	{
		_userTextId = value;
	}

	/*****************************************************************************************/
	public String getTranslatedText()
	{
		return _translatedText;
	}

	public void setTranslatedText(String value)
	{
		_translatedText = value;
	}

	/*****************************************************************************************/
	public boolean getChanged()
	{
		return _changed;
	}

	public void setChanged(boolean value)
	{
		_changed = value;
	}

}
