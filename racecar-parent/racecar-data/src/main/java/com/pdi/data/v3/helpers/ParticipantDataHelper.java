/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.helpers;

//import java.io.StringWriter;
import java.util.ArrayList;

//import javax.xml.transform.Transformer;
//import javax.xml.transform.TransformerFactory;
//import javax.xml.transform.dom.DOMSource;
//import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.pdi.data.util.RequestStatus;
import com.pdi.data.v3.dto.DocumentDTO;
import com.pdi.data.v3.dto.ModuleDTO;
import com.pdi.data.v3.dto.ParticipantDTO;
import com.pdi.data.v3.dto.ParticipantListDTO;
import com.pdi.data.v3.dto.ParticipationDTO;
import com.pdi.data.v3.dto.ProjectDTO;
import com.pdi.data.v3.dto.ResultDTO;
import com.pdi.data.v3.dto.SessionUserDTO;
import com.pdi.data.v3.util.V3WebserviceUtils;
import com.pdi.xml.XMLUtils;
import java.util.ListIterator;

/**
 * DataHelper for Participant Information
 * 
 *
 * @author		Gavin Myers
 */
public class ParticipantDataHelper
{

	//
	// Static data.
	//

	//
	// Static methods.
	//
	
	/**
	 * Internal Thread Class used to get multiple participant results
	 * data back from the single participant list
	 */
	static class ParticipantDataHelperThread extends Thread
	{
		// Instance Data
		private SessionUserDTO _sessionUser;
		private ParticipantDTO _participant;

		// Constructor
		public ParticipantDataHelperThread()
		{
		}

		// Instance methods  
		public void run()
		{
			//hit the webservice
			try
			{
				this.setParticipant(ParticipantDataHelper.getParticipantResults(_sessionUser, _participant));
			}
			catch(Exception e)
			{
				//
			}
		}


		// Getters and setters
		
		//****************************************************************
		public void setParticipant(ParticipantDTO value)
		{
			_participant = value;
		}

		public ParticipantDTO getParticipant()
		{
			return _participant;
		}
		
		//****************************************************************
		public void setSessionUser(SessionUserDTO value)
		{
			_sessionUser = value;
		}

		public SessionUserDTO getSessionUser()
		{
			return _sessionUser;
		}
	}	// End locally used ParticipantDataHelperThread class

	
	/**
	 * Internal Thread Class used to get multiple participant module
	 * data back from the single participant list
	 */
	static class ParticipantModDataHelperThread extends Thread
	{
		// Instance Data
		private SessionUserDTO _sessionUser;
		private ParticipantDTO _participant;

		// Constructor
		public ParticipantModDataHelperThread()
		{
		}

		// Instance methods  
		public void run()
		{
			//hit the webservice
			try
			{
				this.setParticipant(ParticipantDataHelper.parseResultsForModules(_sessionUser, _participant));
			}
			catch(Exception e)
			{
				//
			}
		}


		// Getters and setters
		
		//****************************************************************
		public void setParticipant(ParticipantDTO value)
		{
			_participant = value;
		}

		public ParticipantDTO getParticipant()
		{
			return _participant;
		}
		
		//****************************************************************
		public void setSessionUser(SessionUserDTO value)
		{
			_sessionUser = value;
		}

		public SessionUserDTO getSessionUser()
		{
			return _sessionUser;
		}
	}	// End locally used ParticipantModDataHelperThread class

	
	/**
	 * Get all participants from a project - 1st level
	 *   Gets a list of participant proxies
	 * @param sessionUser
	 * @return
	 * @throws Exception 
	 */
	public static ParticipantListDTO getParticipants(SessionUserDTO sessionUser, ProjectDTO project)
	{
		//https://platform.iscopia.com/webservices1/rest/v1/project/PCOJ-VNSK/participant/list
		
		DocumentDTO dto = V3WebserviceUtils.request(sessionUser,"project/"+project.getId()+"/participant/list","");
		if (! dto.getStatus().isOk())
		{
			return new ParticipantListDTO(dto.getStatus());
		}

		Document response = dto.getDocument();
		NodeList nodes = XMLUtils.getElements(response, "//participantListLine");

		ArrayList<ParticipantDTO> results = new ArrayList<ParticipantDTO>();
		for(int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);
			ParticipantDTO participant = new ParticipantDTO(node);
			results.add(participant);
		}
		
		//Thread out to get all the participant data
		// Note that this is different from the getModules thread
		int count = results.size();
		ParticipantDataHelperThread	threads[] = new ParticipantDataHelperThread[count];
			
		for(int i = 0; i < count; i++){
			ParticipantDTO participant = results.get(i);
			threads[i] = new ParticipantDataHelperThread();
			threads[i].setSessionUser(sessionUser);
			threads[i].setParticipant(participant);
			threads[i].start();
		}
		
		//Join the threads
		for(int i = 0; i < count; i++){
			try {
	            threads[i].join();
	         }
	         catch (InterruptedException e) {
	            System.out.print("Join interrupted\n");
	         }
		}
		
		//Populate with the new participant data
		results = new ArrayList<ParticipantDTO>();
		for(int i = 0; i < count; i++){
			results.add(threads[i].getParticipant());
		}
		
		return new ParticipantListDTO(results);
	}


	/**
	 * Gets the full details of a participant - 2nd level
	 * Currently depends upon the URI only (You can't use the id only to get a participant)
	 * This allows us to assume that we are filling a proxy
	 * @param sessionUser
	 * @param participant
	 * @return
	 * 
	 */
	public static ParticipantDTO getParticipant(SessionUserDTO sessionUser, ParticipantDTO participant)
	{
		if(participant.getUri() == null)
		{
			participant.setStatus(new RequestStatus(RequestStatus.RS_ERROR, "URI required to fill participant"));
			return participant;
		}
		//https://platform.iscopia.com/webservices1/rest/v1/project/PCOJ-VNSK/participant/TOXPN-OKJTW
		//https://platform.iscopia.com/webservices1/rest/v1/project/PEIX-EYWU/participant/TQRQK-SEZRL
		
		DocumentDTO dto = V3WebserviceUtils.request(sessionUser,participant.getUri(),"");
		if (! dto.getStatus().isOk())
		{
			participant.setStatus(dto.getStatus());
			return participant;
		}

		Document response = dto.getDocument();
		participant.refresh(response.getFirstChild());
        /*
	       DOMSource domSource = new DOMSource(response);
	       StringWriter writer = new StringWriter();
	       StreamResult result = new StreamResult(writer);
	       TransformerFactory tf = TransformerFactory.newInstance();
	       Transformer transformer = tf.newTransformer();
	       transformer.transform(domSource, result);
	       System.out.println(writer.toString());
        */
		
		return participant;
	}


	/**
	 * Get result data from a participant - 3rd level
	 * @param sessionUser
	 * @return
	 */
	public static ParticipantDTO getParticipantResults(SessionUserDTO sessionUser, ParticipantDTO participant)
	{
		//No participations? Let's try and get the full info
		if(participant.getParticipations() == null || participant.getParticipations().size() == 0)
		{
			participant = ParticipantDataHelper.getParticipant(sessionUser, participant);
			if (! participant.getStatus().isOk())
				return participant;
		}
		//https://platform.iscopia.com/webservices1/rest/v1/project/PCOJ-VNSK/participant/list

		for(ParticipationDTO participation : participant.getParticipations())
		{
			DocumentDTO dto = V3WebserviceUtils.request(sessionUser,participation.getResultsUri(),"");
			if (! dto.getStatus().isOk())
			{
				participant.setStatus(dto.getStatus());
			}

			Document response = dto.getDocument();
			NodeList nodes = XMLUtils.getElements(response, "//participantResultModuleListLine");
			if(participation.getResults() == null)
			{
				participation.setResults(new ArrayList<ResultDTO>());
			}
			for(int i = 0; i < nodes.getLength(); i++)
			{				
				Node node = nodes.item(i);
				String modid = XMLUtils.getElementValue(node, "moduleIdentifier/.");
				ResultDTO result = new ResultDTO(node, modid);
				if (result.getStatus().getStatusCode() != RequestStatus.WB_NO_DATA)
					participation.getResults().add(result);
			}
		}
		
		return participant;
	}
	
	
	/**
	 * Get module data for a participant
	 * @param sessionUser
	 * @param project
	 * @return ParticipantListDTO
	 */
	public static ParticipantListDTO getParticipantsWithModules(SessionUserDTO sessionUser, ProjectDTO project)
	{
		DocumentDTO dto = null;
		Document response = null;
		NodeList nodes = null;
		
		// Get the project results for module list
		if (project.isProxy())
		{
			// Fill it
			project = ProjectDataHelper.getProject(sessionUser, project);
		}

// I discovered the following:
//		1) It appears that all of the instruments are listed in the results for the people in a project
//		2) Within an activity the results for the participants appear to be in the same order
//		3) The order is not in presentation order
// Skip this for now until we know more
//
//		dto = V3WebserviceUtils.request(sessionUser,project.getResultsUri(),"");
//		if (! dto.getStatus().isOk())
//		{
//			return new ParticipantListDTO(dto.getStatus());
//		}
//		response = dto.getDocument();
//		nodes = XMLUtils.getElements(response, "//participantResultModuleListLine");
//
//		// Assumes that the module list is in presentation order
//		//   Consent forms (or any sort of logic that can skip a module) blows this assumption up
////TODO Find an assumption that works
//		ArrayList<ModuleDTO> modules = new ArrayList<ModuleDTO>();
//		for(int i = 0; i < nodes.getLength(); i++)
//		{
//			Node node = nodes.item(i);
//			ModuleDTO mod = new ModuleDTO(node);
//			modules.add(mod);
//		}
//		
//		// clean up; Discard the old objects so we can use them for the next query
//		dto = null;
//		response = null;
//		nodes = null;
		
		// Get a list of participant proxies
		dto = V3WebserviceUtils.request(sessionUser,"project/"+project.getId()+"/participant/list","");
		if (! dto.getStatus().isOk())
		{
			return new ParticipantListDTO(dto.getStatus());
		}
		response = dto.getDocument();
		nodes = XMLUtils.getElements(response, "//participantListLine");

		ArrayList<ParticipantDTO> partList = new ArrayList<ParticipantDTO>();
		for(int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);
			ParticipantDTO participant = new ParticipantDTO(node);
			if (participant.isProxy())
			{
				// Should be... so fill it
				participant = ParticipantDataHelper.getParticipant(sessionUser, participant);
			}

			partList.add(participant);
		}
		
		//Thread out to get all the participant module data
		// Note that this is different from the getResults thread
		int count = partList.size();
		ParticipantModDataHelperThread	threads[] = new ParticipantModDataHelperThread[count];
			
		for(int i = 0; i < count; i++)
		{
			ParticipantDTO participant = partList.get(i);
			threads[i] = new ParticipantModDataHelperThread();
			threads[i].setSessionUser(sessionUser);
			threads[i].setParticipant(participant);
			threads[i].start();
		}

		//Join the threads
		for(int i = 0; i < count; i++)
		{
			try
			{
	            threads[i].join();
			}
			catch (InterruptedException e)
			{
				System.out.print("Module Join interrupted\n");
			}
		}
		
		//Populate with the new participant data
		ArrayList<ParticipantDTO> results = new ArrayList<ParticipantDTO>();
		for(int i = 0; i < count; i++){
			results.add(threads[i].getParticipant());
		}
		
		return new ParticipantListDTO(results);
	}
	
	
	/**
	 * Parse module data for a participant
	 * @param sessionUser
	 * @param project
	 * @return ParticipantDTO
	 */
	private static ParticipantDTO parseResultsForModules(SessionUserDTO sessionUser, ParticipantDTO participant)
	{
		// Get the results data for each participation
		for (ListIterator<ParticipationDTO> itr = participant.getParticipations().listIterator(); itr.hasNext();  )
		{
			ParticipationDTO participation = itr.next();
			DocumentDTO docDto = V3WebserviceUtils.request(sessionUser,participation.getResultsUri(),"");
			if (! docDto.getStatus().isOk())
			{
				participant.setStatus(docDto.getStatus());
				return participant;
			}
			
			Document respDoc = docDto.getDocument();
			NodeList nodes = XMLUtils.getElements(respDoc, "//participantResultModuleListLine");

			ArrayList<ResultDTO> results = new ArrayList<ResultDTO>();
			for(int i = 0; i < nodes.getLength(); i++)
			{
				Node node = nodes.item(i);
				ResultDTO res = new ResultDTO();
				res.setModule(new ModuleDTO(node));
				// derive status for this module
				res.getModule().setModuleStatus(deriveModuleStatus(node));
				
				results.add(res);
			}
			participation.setResults(results);
		}
		
		participant.setStatus(new RequestStatus());
		return participant;
	}
	
	/**
	 * Parse module data for a participant
	 * @param sessionUser
	 * @param project
	 * @return ParticipantDTO
	 */
	private static String deriveModuleStatus(Node node)
	{
		// Just try to get some element that won't be there if they haven't started the module.
		// The lowest tag on an un-started module is the empty <participantResultList> tag.  The
		// stuff to the right of that (below) to find an element that will have a value.
		String content = XMLUtils.getElementValue(node, "resultList/resultListLine/participantResultList/participantResultListLine/candidateIdentifier.");
		
		return (content == null ? "Not Started" : "Started");
	}

	//
	// Instance data.
	//

	//
	// Constructors.
	//

	//
	// Instance methods.
	//
}
 