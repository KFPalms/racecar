/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 *
 * $Header: /Platform/Java/sourceCode/com/pdicorp/app/tcm/export/vo/ExportDataVO.java 6     8/25/05 3:23p Kbeukelm $
 */
package com.pdi.data.abyd.dto.common;


/**
 * EGRaterNameData is a container for the consultant name.  It is used in the full data and name DTOs.
 * 
 * @author		Ken Beukelman
  */
public class EGRaterNameData
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _raterLastName;		// Consultant last name
	private String _raterFirstName;		// Consultant first name

	//
	// Constructors.
	//
	public EGRaterNameData()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public String getRaterLastName()
	{
		return _raterLastName;
	}
		
	public void setRaterLastName(String value)
	{
		_raterLastName = value;
	}

	/*****************************************************************************************/
	public String getRaterFirstName()
	{
		return _raterFirstName;
	}
		
	public void setRaterFirstName(String value)
	{
		_raterFirstName = value;
	}


	/*
	 * Show me what you've got
	 */
	public String toString()
	{
		String ret = "EGRaterNameData (Java) - " + _raterLastName + ", " + _raterFirstName + "\n";
		
		return ret;
	}
}
