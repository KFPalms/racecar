/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.dto;

import com.pdi.data.util.BaseDTO;

/**
 * ParticipantReportListDTO is a class designed 
 * to bring a list of participant/report pairs
 * 
 * @author		Gavin Myers
  */
public class ParticipantEmailListDTO  extends BaseDTO
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private ParticipantListDTO _participantList;
	private EmailDTO _email;


	//
	// Constructors.
	//

	/**
	 * "Standard" (no-parameter) constructor
	 */
	public ParticipantEmailListDTO()
	{
		super();
		_participantList = new ParticipantListDTO();
	}
	

	//
	// Instance methods.
	//
	

	
	/**
	 * Generic toString method
	 */
	public String toString()
	{
		String str = "\n --- ParticipantEmailListDTO --- \n";
		str += this.getStatus().toString();
		str += "\nProxy:      " + this.getProxy();
		str += this.getParticipantList().toString();
		str += this.getEmail().toString();
		
		return str;
	}	


	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
	
	/*****************************************************************************************/
	public void setParticipantList(ParticipantListDTO participantList)
	{
		_participantList = participantList;
	}
	
	public ParticipantListDTO getParticipantList()
	{
		return _participantList;
	}
	
	/*****************************************************************************************/
	public void setEmail(EmailDTO email)
	{
		_email = email;
	}
	
	public EmailDTO getEmail()
	{
		return _email;
	}
}
