package com.pdi.data.helpers.interfaces;

//import java.sql.Connection;

import com.pdi.data.dto.NoteHolder;
import com.pdi.data.dto.SessionUser;


public interface INoteHelper {
	
	public NoteHolder all(SessionUser session, String participantId);
	public void addNotes( NoteHolder noteHolder) throws Exception;
	
}
