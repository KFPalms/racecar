/**
 * Copyright (c) 2008 Personnel Decisions International, Inc.
 * All rights reserved.
 *
 * $Header: /com/pdicorp/app/ABD/dnaDesigner/AbdDnaService 6     8/25/05 3:23p Kbeukelm $
 */

package com.pdi.data.abyd.helpers.setup;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import com.pdi.data.abyd.dto.common.DNACellDTO;

/**
 * SaveDNAStructureHelper contains the code needed to save the DNA structure data.
 *
 * @author		Ken Beukelman
 * @version	$Revision: 6 $  $Date: 8/25/05 3:23p $
 */
public class SaveDNAStructureHelper
{
	//
	// Static data.
	//
	/** The source revision. */
	public static final String REVISION = "$Revision: 5 $";

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private Connection connection;
	private long dnaId;
	private ArrayList<DNACellDTO> cells;
	
	HashMap<String, TmpltData> tpltCells = null;

	
	//
	// Constructors.
	//
	public SaveDNAStructureHelper(Connection con, long dnaId, ArrayList<DNACellDTO> cells)
	{
		this.connection = con;
		this.dnaId = dnaId;
		this.cells = cells;
	}

	//
	// Instance methods.
	//

	/**
	 * saveStructure
	 * Update the structure database from the incoming data.
	 * The logic assumes that modules turned off do not return data.
	 * The value returned indicates whether or not link params were deleted.
	 *
	 * @throws Exception
	 */
	public boolean saveStructure()
		throws Exception
	{
		// This functionality is implemented by constructing two collections
		// of data - one the state as currently resides in the database and
		// the other as sent from the UI.
		//
		// Assumptions:
		//		- The data from the UI does not contain any data for turned off
		//		  modules.

		HashMap<String, DNACellDTO> inpCells = new HashMap<String, DNACellDTO>();
		HashMap<String, DNACellDTO> dbCells = null;
		
		// Set up the incoming data for analysis;  move the data to a
		// HashMap for easy access
		for(int i=0; i < this.cells.size(); i++)
		{
			DNACellDTO cell = this.cells.get(i);
			String key = "" + cell.getModuleId() + "|" + cell.getCompetencyId();
			inpCells.put(key, cell);
		}
		
		// get the existing DNA structure from the database and set it up the same way
		dbCells = getDBStructMap();

		/* NOTE: per defect meeting of 4/7/09, Business logic around e-mail
		 *  sending will be moved to the UI and they will call a mail RPC
		 */
////		// Before we can send an email to the client mgr indicating that the DNA has changed,
////		// we need to verify that something truly is changing. We use the boolean structureHasChanged
////		// to track if something, anything has changed through updates, inserts and deletes below.
////////		boolean structureHasChanged = false;
		
		// We want to track a change in structure so we can manage link params
		boolean delLinkParams = false;
		
		// Run the incoming rows to see if the data is in the db.  If it is,
		// this is an update.  If not then this is an insert.
		for (Iterator<String> itr = inpCells.keySet().iterator(); itr.hasNext(); )
		{
			// get the key
			String key = itr.next();
			// look it up in the existing
			if(dbCells.containsKey(key))
			{
				// There... is the data the same?
				if(((DNACellDTO)dbCells.get(key)).getIsUsed() != ((DNACellDTO)inpCells.get(key)).getIsUsed())
				{
					// No.  Do the update
					updateDnaLink((DNACellDTO) inpCells.get(key));
					delLinkParams = true;
				}
			}  else  {
				//Not there; insert a new link
				insertDnaLink((DNACellDTO) inpCells.get(key));
				delLinkParams = true;
			}
		}
		
		// Delete the rows in the database not in the incoming data
		for (Iterator<String> itr = dbCells.keySet().iterator(); itr.hasNext(); )
		{
			// get the key
			String key = itr.next();
			// look it up in the input cells
			if(! inpCells.containsKey(key))
			{
				// Nope, not there.  Delete it.
				deleteDnaLink((DNACellDTO) dbCells.get(key));
				delLinkParams = true;
			}
		}		
		
////		if (structureHasChanged)
////		{
////		    // We need some of the values in a DNADescription object to continue.
////		    DNADescription dnaDesc = getLimitedDNADescriptionData();
////		    
////		    if (dnaDesc.getDnaComplete().booleanValue() && dnaDesc.getSpComplete().booleanValue())
////		    {
////		        // The DNA structure has truly changed so we now need to send an email to the client mgr.
////		        
////	   		    String host = this.properties.getProperty("abd.emailServer");
////	   		    EmailRequestService emailService = new EmailRequestService(host);
////	
////	   	        EmailMessage email = new EmailMessage();
////	   	        email.setToAddress(dnaDesc.getcMgrEmail());
////	   	        String fromAddr = this.properties.getProperty("abd.dna.clientMgr.fromAddress");
////	   	        email.setFromAddress(fromAddr);
////	   	        String emailSubject = this.properties.getProperty("abd.dna.clientMgr.emailSubject");
////	   	        email.setSubject(emailSubject);
////	   	        String emailText = this.properties.getProperty("abd.dna.clientMgr.emailText");
////	   	        email.setText(emailText);
////	   	        email.setTextSubstitutions("");
////	   	        
////	   	        emailService.sendEmail(email);
////		    }
////		}
		
		// So now we also need to create a report model based upon this data
		new ReportModelDataHelper().createNewReportModelFromDna(this.connection, this.dnaId);
		
		boolean lpDeleted = false;
		if (delLinkParams)
		{
			lpDeleted = deleteLinkparams();
		}
		
		return lpDeleted;
	}

	
	/**
	 * getDBStructMap returns a Map of DNACellDTO objects that represents the current
	 * intersections in a dna.  Its key string is the mod id + "|" + the comp id
	 *
	 * @return HashMap<String, DNACellDTO>
	 * @throws Exception
	 */
	private HashMap<String, DNACellDTO> getDBStructMap()
		throws Exception
	{
		HashMap<String, DNACellDTO> results = new HashMap<String, DNACellDTO>();
		
		Statement stmt = null;
		ResultSet rs = null;
		
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT moduleId, ");
		sqlQuery.append("       competencyId, ");
		sqlQuery.append("       isUsed ");
		sqlQuery.append("  FROM pdi_abd_dna_link ");
		sqlQuery.append("  WHERE dnaId = " + this.dnaId);

		try
		{

	       	stmt = this.connection.createStatement();
	       	rs = stmt.executeQuery(sqlQuery.toString());
	       	
	       	while (rs.next())
	       	{
	       		DNACellDTO entry = new DNACellDTO();
	       		entry.setModuleId(rs.getLong("moduleId"));
	       		entry.setCompetencyId(rs.getLong("competencyId"));
	       		entry.setIsUsed(rs.getInt("isUsed") == 0 ? false : true);
	       		String key = "" + entry.getModuleId() + "|" + entry.getCompetencyId();
	       		results.put(key, entry);
	       	}
		}
        catch (SQLException ex)
		{
            // handle any errors
        	throw new Exception("SQL getDBStructMap.  " +
        			"SQLException: " + ex.getMessage() + ", " +
        			"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
        finally
		{
        	if (rs != null)
        	{
        		try {  rs.close();  }
        		catch (SQLException sqlEx)	{  /* swallow the error */  }
        		rs = null;
        	}
        	if (stmt != null)
        	{
        		try	{  stmt.close();  }
        		catch (SQLException sqlEx)	{  /* swallow the error */  }
        		stmt = null;
        	}
		}

        return results;
	}


	/**
	 * getTpltDataMap returns a Map of objects that contains data from the
	 * template that is not supplied in a DNACellDTO object.
	 *
	 * @return HashMap<String, TmpltData>
	 * @throws Exception
	 */
	private HashMap<String, TmpltData> getTpltDataMap()
		throws Exception
	{
		HashMap<String, TmpltData> results = new HashMap<String, TmpltData>();
		
		Statement stmt = null;
		ResultSet rs = null;
		
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT dl.moduleId, ");
		sqlQuery.append("       dl.modSeq, ");
		sqlQuery.append("       dl.groupId, ");
		sqlQuery.append("       dl.groupSeq, ");
		sqlQuery.append("       dl.competencyId, ");
		sqlQuery.append("       dl.compSeq, ");
		sqlQuery.append("       dl.modEdit ");
		sqlQuery.append("  FROM pdi_abd_dna dna ");
		sqlQuery.append("    INNER JOIN pdi_abd_model mdl ON mdl.modelId = dna.modelId ");
		sqlQuery.append("    INNER JOIN pdi_abd_dna_link dl ON dl.dnaId = mdl.dnaId ");
		sqlQuery.append("  WHERE dna.dnaId = " + this.dnaId);
	
		try
		{
	       	stmt = this.connection.createStatement();
	       	rs = stmt.executeQuery(sqlQuery.toString());
	       	
	       	while (rs.next())
	       	{
	       		TmpltData entry = new TmpltData();
	       		entry.setModuleId(rs.getLong("moduleId"));
	       		entry.setModuleSequence(rs.getInt("modSeq"));
	       		entry.setGroupId(rs.getLong("groupId"));
	       		entry.setGroupSequence(rs.getInt("groupSeq"));
	       		entry.setCompetencyId(rs.getLong("competencyId"));
	       		entry.setCompetencySequence(rs.getInt("compSeq"));
	       		entry.setModEdit(rs.getInt("modEdit"));

	       		String key = "" + entry.getModuleId() + "|" + entry.getCompetencyId();
	       		results.put(key, entry);
	       	}
		}
	    catch (SQLException ex)
		{
	        // handle any errors
	    	throw new Exception("SQL getTpltDataMap.  " +
	    			"SQLException: " + ex.getMessage() + ", " +
	    			"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
	    finally
		{
	    	if (rs != null)
	    	{
	    		try {  rs.close();  }
	    		catch (SQLException sqlEx)	{  /* swallow the error */  }
	    		rs = null;
	    	}
	    	if (stmt != null)
	    	{
	    		try	{  stmt.close();  }
	    		catch (SQLException sqlEx)	{  /* swallow the error */  }
	    		stmt = null;
	    	}
		}
	
	    return results;
	}

	/**
	 * getGPIModuleIds
	 *
	 * @return  ArrayList<Long>
	 * @throws Exception
	 */
//	private ArrayList<Long> getGPIModuleIds()
//		throws Exception
//	{
//		ArrayList<Long> ret = new ArrayList<Long>();
//		
//		Statement stmt = null;
//		ResultSet rs = null;
//
//		StringBuffer sqlQuery = new StringBuffer();
//		sqlQuery.append("SELECT moduleId ");
//		sqlQuery.append("  FROM pdi_abd_module ");
//		sqlQuery.append("  WHERE internalName like '%GPI%'  ");
//
//		try
//		{
//
//	       	stmt = this.connection.createStatement();
//	       	rs = stmt.executeQuery(sqlQuery.toString());
//	       	
//	       	while (rs.next())
//	       	{
//	       		ret.add(new Long(rs.getLong("moduleId")));
//
//	       	}
//
//		}
//        catch (SQLException ex)
//		{
//            // handle any errors
//        	throw new Exception("SQL getGPIModuleIds.  " +
//        			"SQLException: " + ex.getMessage() + ", " +
//        			"SQLState: " + ex.getSQLState() + ", " +
//					"VendorError: " + ex.getErrorCode());
//		}
//        finally
//		{
//        	if (rs != null)
//        	{
//        		try {  rs.close();  }
//        		catch (SQLException sqlEx)	{  /* swallow the error */  }
//        		rs = null;
//        	}
//        	if (stmt != null)
//        	{
//        		try	{  stmt.close();  }
//        		catch (SQLException sqlEx)	{  /* swallow the error */  }
//        		stmt = null;
//        	}
//		}
//
//        return ret;
//	}
	
	/**
	 * Update a DNA link, based off of data in a DNACellDTO object.
	 *
	 * @param cell a DNACellDTO object
	 * @throws Exception
	 */
	private void updateDnaLink(DNACellDTO cell)
		throws Exception
	{
		Statement stmt = null;
		
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("UPDATE pdi_abd_dna_link ");
		sqlQuery.append("  SET isUsed = " + (cell.getIsUsed() ? 1 : 0) + " ");
		sqlQuery.append("  WHERE dnaId = " + this.dnaId + " ");
		sqlQuery.append("    AND moduleId = " + cell.getModuleId() + " ");
		sqlQuery.append("    AND competencyId = " + cell.getCompetencyId() + " "); 	
		
		try
		{
			stmt = this.connection.createStatement();
			stmt.executeUpdate(sqlQuery.toString());
		}
        catch (SQLException ex)
		{
            // handle any errors
        	throw new Exception("SQL updateDnaLink.  " +
        			"SQLException: " + ex.getMessage() + ", " +
        			"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
        }
        finally
		{
            if (stmt != null)
            {
                try  {  stmt.close();  }
                catch (SQLException sqlEx)  {  /* Swallow the error */  }
                stmt = null;
            }
        }
	}


	/**
	 * Insert a DNA link, based off of data in a DNACellDTO object.
	 *
	 * @param cell a DNACellDTO object
	 * @throws Exception
	 */
	private void insertDnaLink(DNACellDTO cell)
		throws Exception
	{
		Statement stmt = null;
		
		// if we are here, we need some template info; see if we have it.
		if (this.tpltCells == null)
		{
			// Lazy init the tplt data
			this.tpltCells = getTpltDataMap();
		}
		
		// get the template data for the cell
		String key = "" + cell.getModuleId() + "|" + cell.getCompetencyId();
		//TmpltData tdat = (TmpltData) this.tpltCells.get(key);
		TmpltData tdat = this.tpltCells.get(key);

		// Set up the insert
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("INSERT INTO pdi_abd_dna_link ");
		sqlQuery.append("  (dnaId, ");
		sqlQuery.append("   moduleId, ");
		sqlQuery.append("   modSeq, ");
		sqlQuery.append("   groupId, ");
		sqlQuery.append("   groupSeq, ");
		sqlQuery.append("   competencyId, ");
		sqlQuery.append("   compSeq, ");
		sqlQuery.append("   isUsed, ");
		sqlQuery.append("   modEdit, ");
		sqlQuery.append("   active, ");
		sqlQuery.append("   lastUserId, ");
		sqlQuery.append("   lastDate) ");
		sqlQuery.append("  VALUES(" + this.dnaId + ", "); 
		sqlQuery.append("         " + cell.getModuleId() + ", ");
		sqlQuery.append("         " + tdat.getModuleSequence() + ", ");
		sqlQuery.append("         " + tdat.getGroupId() + ", ");
		sqlQuery.append("         " + tdat.getGroupSequence() + ", ");
		sqlQuery.append("         " + cell.getCompetencyId() + ", ");
		sqlQuery.append("         " + tdat.getCompetencySequence() + ", ");
		sqlQuery.append("         " + (cell.getIsUsed() ? 1 : 0) + ", ");
		sqlQuery.append("         " + tdat.getModEdit() + ", ");
		sqlQuery.append("         1, ");
		sqlQuery.append("         SYSTEM_USER," );
		sqlQuery.append("         GETDATE())");

		try
		{
	       	stmt = this.connection.createStatement();
	       	stmt.executeUpdate(sqlQuery.toString());
		}
        catch (SQLException ex)
		{
            // handle any errors
        	throw new Exception("SQL insertDnaLink.  " +
        			"SQLException: " + ex.getMessage() + ", " +
        			"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
        finally
		{
        	if (stmt != null)
        	{
        		try  {  stmt.close();  }
        		catch (SQLException sqlEx)  { /* Swallow the error */}
        		stmt = null;
        	}
		}
	}


	/**
	 * Delete a DNA link, based off of data in a DNACellDTO object.
	 *
	 * @param cell a DNACellDTO object
	 * @throws Exception
	 */
	private void deleteDnaLink(DNACellDTO cell)
		throws Exception
	{
		Statement stmt = null;
		//int cnt = 0;
		
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("DELETE FROM pdi_abd_dna_link ");
		sqlQuery.append("  WHERE dnaId = " + this.dnaId + " ");
		sqlQuery.append("    AND moduleId = " + cell.getModuleId() + " ");
		sqlQuery.append("    AND competencyId = " + cell.getCompetencyId() + " "); 	

		try
		{
			stmt = this.connection.createStatement();
			//cnt = stmt.executeUpdate(sqlQuery.toString());
			stmt.executeUpdate(sqlQuery.toString());
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL deleteDnaLink.  " +
	        			"SQLException: " + ex.getMessage() + ", " +
	        			"SQLState: " + ex.getSQLState() + ", " +
						"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (stmt != null)
			{
				try  {  stmt.close();  }
				catch (SQLException sqlEx)  { /* Swallow the error */}
				stmt = null;
			}
		}
	}


	/**
	 * deleteLinkparams - Deletes all of the linkparams entries for a project
	 * @throws Exception
	 */
	private boolean deleteLinkparams()
		throws Exception
	{
		Statement stmt = null;
		
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("DELETE FROM linkparams ");
		sqlQuery.append("  WHERE projectId = (SELECT projectId ");
		sqlQuery.append("                       FROM pdi_abd_dna ");
		sqlQuery.append("                       WHERE dnaId = " + this.dnaId + ")");

		try
		{
			int cnt = 0;
			stmt = this.connection.createStatement();
			cnt = stmt.executeUpdate(sqlQuery.toString());
			return (cnt > 0) ? true : false;
		}
		catch (SQLException ex)
		{
			throw new Exception("SQL deleteLinkparams, dna=" + this.dnaId + ".  " +
	        			"SQLException: " + ex.getMessage() + ", " +
	        			"SQLState: " + ex.getSQLState() + ", " +
						"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (stmt != null)
			{
				try  {  stmt.close();  }
				catch (SQLException sqlEx)  { /* Swallow the error */}
				stmt = null;
			}
		}


	}


/*****************************  Private class TmpltData  *****************************/
	
	/**
	 * TmpltData is a thin, private class used to contain ancillary
	 *  information from the template that is used in inserting DNA
	 *  link rows.  Each item represents one row in the template.
	 *
	 * @author		Ken Beukelman
	 * @version	$Revision: 6 $  $Date: 8/25/05 3:23p $
	 */
	private class TmpltData
	{

		//
		// Instance data.
		//
		private long _modId;
		private int _modSeq;
		private long _grpId;
		private int _grpSeq;
		private long _cmpId;
		private int _cmpSeq;
		private int _modEdit;
		
		//
		// Constructors.
		//
		protected TmpltData()
			throws Exception
		{
			// Nothing happens here
		}

		//
		// Instance methods.
		//

		/*****************************************************************************************/
		public long getModuleId()
		{
			return _modId;
		}
	
		public void setModuleId(long value)
		{
			_modId = value;
		}

		/*****************************************************************************************/
		public int getModuleSequence()
		{
			return _modSeq;
		}
	
		public void setModuleSequence(int value)
		{
			_modSeq = value;
		}

		/*****************************************************************************************/
		public long getGroupId()
		{
			return _grpId;
		}
	
		public void setGroupId(long value)
		{
			_grpId = value;
		}

		/*****************************************************************************************/
		public int getGroupSequence()
		{
			return _grpSeq;
		}
	
		public void setGroupSequence(int value)
		{
			_grpSeq = value;
		}

		/*****************************************************************************************/
		public long getCompetencyId()
		{
			return _cmpId;
		}
	
		public void setCompetencyId(long value)
		{
			_cmpId = value;
		}

		/*****************************************************************************************/
		public int getCompetencySequence()
		{
			return _cmpSeq;
		}
	
		public void setCompetencySequence(int value)
		{
			_cmpSeq = value;
		}

		/*****************************************************************************************/
		public int getModEdit()
		{
			return _modEdit;
		}
	
		public void setModEdit(int value)
		{
			_modEdit = value;
		}
		
	}	// End private class tmpltData
}
