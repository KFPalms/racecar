package com.pdi.data.nhn.helpers.delegated.ut;

import java.util.ArrayList;

import com.pdi.data.helpers.interfaces.IClientHelper;

import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.nhn.util.NhnDatabaseUtils;
//import com.pdi.properties.PropertyLoader;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.dto.Client;
import com.pdi.ut.UnitTestUtils;
//import com.pdi.ut.UnitTestUtils;
import junit.framework.TestCase;

public class ClientHelperUT extends TestCase {

	public ClientHelperUT(String name) 	{
		super(name);
	}
	
	public void testGetClient() throws Exception {
		UnitTestUtils.start(this);
		NhnDatabaseUtils.setUnitTest(true);
		
		SessionUser session = new SessionUser();
		
		IClientHelper iclient = HelperDelegate.getClientHelper("com.pdi.data.nhn.helpers.delegated");

		ArrayList<Client> clients = iclient.all(session);
		for(Client client : clients) {
			System.out.println(client.getId() + " - " + client.getName());
		}

		String clientId = "2";  //  generic tpot client          
		Client singleClient = iclient.fromId(session, clientId);
		
		try{
			System.out.println("singleClient.getId = " + singleClient.getId());
			System.out.println("singleClient.getName = " + singleClient.getName());
			System.out.println("singleClient.getMetadata = " + singleClient.getMetadata().toString());
		}catch (Exception e) {
			System.out.println("clientId " + clientId + " did not return a valid client.");
		}

	}
	
	
}
