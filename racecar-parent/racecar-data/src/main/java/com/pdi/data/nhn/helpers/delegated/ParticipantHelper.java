/**
 * Copyright (c) 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.nhn.helpers.delegated;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import com.pdi.data.dto.Note;
import com.pdi.data.dto.NoteHolder;
import com.pdi.data.dto.Participant;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.interfaces.IParticipantHelper;
import com.pdi.data.nhn.util.NhnDatabaseUtils;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.util.RequestStatus;
import com.pdi.logging.LogWriter;

public class ParticipantHelper implements IParticipantHelper {

	//
	// Static Variables
	//
	private static final String SUPER_ADMIN = "4";
	
	// "Standard" SQL prefix -- Used in fromId, fromProjectId, fromClientId
	//                          and in fromAdminId where the admin IS a superuser
	private static String SQL_PREFIX =
		"SELECT " +
			"u.users_id, " +
			"u.firstname, " +
			"u.lastname, " +
			"u.email, " +
			"o.rolename, " +
			"u.cosubid, " +
			"u.lang_pref, " +
			"u.employeeid, " +
			"u.username, " +
			"u.type, " +
			"u.active, " +
			"u.company_id, " +
			"u.optional_1, " +
			"u.optional_2, " +
			"u.optional_3, " +
			"u.supervisor_name, " +
			"u.supervisor_email, " +
			"u.lang_pref " +
		"FROM platform.dbo.users u " +
		"LEFT JOIN platform.dbo.orgroles o ON u.company_id = o.company_id AND u.jobclass = o.roleid ";
	
	// Admin SQL prefix -- Used in fromAdminId where the admin is NOT a superuser
	private static String SQL_ADMIN_PREFIX = 
	"SELECT " +
		"u.users_id, " +
		"u.firstname, " +
		"u.lastname, " +
		"u.email, " +
		"o.rolename, " +
		"u.cosubid, " +
		"u.lang_pref, " +
		"u.employeeid, " +
		"u.username, " +
		"u.type, " +
		"u.active, " +
		"u.company_id, " +
		"u.optional_1, " +
		"u.optional_2, " +
		"u.optional_3, " +
		"u.supervisor_name, " +
		"u.supervisor_email, " + 
		"u.lang_pref " +
	"FROM platform.dbo.users u " +
	"LEFT JOIN platform.dbo.orgroles o ON u.company_id = o.company_id AND u.jobclass = o.roleid " +
	"JOIN platform.dbo.hchylvl h ON h.hchyid = u.cosubid AND h.company_id = u.company_id " +
	"WHERE u.active = 1 " + 
	"  AND  h.levelid like (SELECT h.levelid " +
							" FROM platform.dbo.users u " +
							" JOIN platform.dbo.hchylvl h on h.hchyid = u.ahlevel " +
							" WHERE u.users_id = ?) + '%'  " +
	"  AND u.company_id =  ? AND u.active = 1 ";

	//
	// Static Methods
	//
	
	//
	// Instance Variables
	//
	
	//
	// Instance Methods
	//

	/*
	 * fromId
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IParticipantHelper#fromId(com.pdi.data.dto.SessionUser, java.lang.String)
	 */
	//@Override
	public Participant fromId(SessionUser session, String participantId) {
		Participant participant = new Participant();		
		StringBuffer sb = new StringBuffer();		
		sb.append(SQL_PREFIX +
				"WHERE u.users_id = ?");

		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(sb);
		if (dlps.isInError()) {
			LogWriter.logSQL(LogWriter.WARNING, this, 
					"Error preparing ParticipantHelper.fromId query." + dlps.toString(),
					sb.toString());
		}
		DataResult dr = null;
		ResultSet rs = null;
		try		{
			dlps.getPreparedStatement().setString(1, participantId);
			dr = NhnDatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			if (rs != null && rs.isBeforeFirst()) {
				while (rs.next())				{
					participant = getParticipant(rs);
				}	
			}
		}catch(java.sql.SQLException se){
			LogWriter.logSQLWithException(LogWriter.ERROR, this, se.getMessage(), sb.toString(), se);
		}
		finally 		{
			if (dr != null) {dr.close();  dr = null;}			
		}
		return participant;
	}

	/*
	 * fromProjectId
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IParticipantHelper#fromProjectId(com.pdi.data.dto.SessionUser, java.lang.String)
	 */
	//@Override
	public ArrayList<Participant> fromProjectId(SessionUser session,
			String projectId) {
		ArrayList<Participant> participants = new ArrayList<Participant>();
		StringBuffer sb = new StringBuffer();		
		sb.append(SQL_PREFIX +
				"where u.active = 1 AND u.users_id in (SELECT users_id FROM platform.dbo.project_user WHERE project_id = ?) order by u.lastname, u.firstname");
		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(sb);
		if (dlps.isInError()) {
			LogWriter.logSQL(LogWriter.WARNING, this, 
					"Error preparing ParticipantHeper.fromProjectId query." + dlps.toString(), 
					sb.toString());
		}
		DataResult dr = null;
		ResultSet rs = null;
		try		{
			dlps.getPreparedStatement().setString(1, projectId);
			dr = NhnDatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			if (rs != null && rs.isBeforeFirst())			{
				while (rs.next())				{
					Participant participant = getParticipant(rs);
					participants.add(participant);
				}
				rs.close();			
			}
		}catch(java.sql.SQLException se){
			LogWriter.logSQLWithException(LogWriter.ERROR, this, se.getMessage(), sb.toString(), se);
		}
		finally 		{
			if (dr != null) {dr.close(); dr = null;}			
		}
		return participants;
	}
	
	
	/*
	 * fromClientId
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IParticipantHelper#fromClientId(com.pdi.data.dto.SessionUser, java.lang.String)
	 */
	//@Override
	public ArrayList<Participant> fromClientId(SessionUser session, String clientId){
		ArrayList<Participant> participants = new ArrayList<Participant>();
		StringBuffer sb = new StringBuffer();		
		sb.append(SQL_PREFIX +
				"WHERE u.company_id = ? and u.active = 1 order by u.lastname, u.firstname");
		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(sb);
		if (dlps.isInError()) {
			LogWriter.logSQL(LogWriter.WARNING, this, 
					"Error preparing ParticipantHeper.fromClientId query." + dlps.toString(), 
					sb.toString());
		}
		DataResult dr = null;
		ResultSet rs = null;
		try		{
			dlps.getPreparedStatement().setString(1, clientId);
			dr = NhnDatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			if (rs != null && rs.isBeforeFirst())			{
				while (rs.next())				{
					Participant participant = getParticipant(rs);
					participants.add(participant);
				}
				rs.close();			
			}
		}catch(java.sql.SQLException se){
			LogWriter.logSQLWithException(LogWriter.ERROR, this, se.getMessage(), sb.toString(), se);
		}
		finally 		{
			if (dr != null) {dr.close(); dr = null;}			
		}
		return participants;
	}
	
	
	/*
	 * fromAdminId
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IParticipantHelper#fromAdminId(com.pdi.data.dto.SessionUser, java.lang.String)
	 */
	//@Override
	public ArrayList<Participant> fromAdminId(SessionUser session, String adminId){
		ArrayList<Participant> participants = new ArrayList<Participant>();
		Participant admin = fromId(session, adminId);
		String clientId = admin.getMetadata().get("company_id");
		/**
		 * For now we will just get all by client;
		 * The admin filtering is some really crazy stuff.
		 */
		StringBuffer sb = new StringBuffer();		
		
		if(admin.getMetadata().get("type").equals(SUPER_ADMIN)){
			sb.append(SQL_PREFIX + "WHERE u.company_id = ? and active = 1 order by u.lastname ASC");
		}else{
			sb.append(SQL_ADMIN_PREFIX + " order by u.lastname ASC");
			
		}
		
		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(sb);
		if (dlps.isInError()) {
			LogWriter.logSQL(LogWriter.WARNING, this, 
					"Error preparing ParticipantHeper.fromAdminId query." + dlps.toString(), 
					sb.toString());
		}
		DataResult dr = null;
		ResultSet rs = null;
		try		{
			if(admin.getMetadata().get("type").equals(SUPER_ADMIN)){
			dlps.getPreparedStatement().setString(1, clientId);
			}else{
				dlps.getPreparedStatement().setString(1, admin.getId());
				dlps.getPreparedStatement().setString(2, clientId);
			}
			dr = NhnDatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			if (rs != null && rs.isBeforeFirst())			{
				while (rs.next())				{
					Participant participant = getParticipant(rs);
					participants.add(participant);
				}
				rs.close();			
			}
		}catch(java.sql.SQLException se){
			LogWriter.logSQLWithException(LogWriter.ERROR, this, 
					"addminId=" + adminId + " \n" + se.getMessage(), sb.toString(), 
					se);
		}
		finally 		{
			if (dr != null) {dr.close(); dr = null;}			
		}
		return participants;
	}
		
	
	
	/*
	 * getParticipant -- moves the data from the rs into a participant object
	 */
	private Participant getParticipant(ResultSet rs){
		Participant participant = new Participant();
		try {
			participant.setId(rs.getString(1));
			participant.setFirstName(rs.getString(2));
			participant.setLastName(rs.getString(3));
			participant.setEmail(rs.getString(4));
			HashMap<String, String> hm = new HashMap<String, String>(); 
			hm.put("jobTitle",rs.getString(5));
			hm.put("orgUnit",rs.getString(6));
			hm.put("lang_pref", rs.getString(7)); 
			hm.put("employeeid", rs.getString(8)); 
			hm.put("username", rs.getString(9)); 
			hm.put("type", rs.getString(10)); 
			hm.put("active", rs.getString(11)); 
			hm.put("company_id", rs.getString(12)); 
			hm.put("optional_1", rs.getString(13)); 
			hm.put("optional_2", rs.getString(14)); 
			hm.put("optional_3", rs.getString(15)); 
			hm.put("supervisor_name", rs.getString(16));
			hm.put("supervisor_email", rs.getString(17));
			hm.put("lang_pref", rs.getString(18));
			participant.setMetadata(hm);
		}catch (Exception e){
			LogWriter.logBasicWithException(LogWriter.ERROR, this, e.getMessage(), e);
		}
		return participant;		
	}

	
	/*
	 * Get a list of Participant objects that contain the input string in their
	 * first name, last name, or e-mail address.  Always returns an ArrayList,
	 * even if there are no results (size() = 0 - n).
	 * 
	 * @param session A SessionUser object
	 * @param adminId A user ID; assumed to be an admin
	 * @param target The string to look for (in firstName, lastName, or email)
	 * @return An ArrayList of Participant objects
	 */
	@Override
	public ArrayList<Participant> find(SessionUser session, String adminId, String target)
	{
		// attempt 2
		ArrayList<Participant> ret = new ArrayList<Participant>();

		// Get admin info
		Participant pp = fromId(session, adminId);
		if(pp.getId() == null)
		{
			// This works because the getId constructor set up a null ID
			return ret;
		}
		String clientId = pp.getMetadata().get("company_id");
		boolean isNormalAdmin = ! SUPER_ADMIN.equals(pp.getMetadata().get("type"));

		// Note that the query appears to be case insensitive.  If that proves not
		// to be the case, then add LOWER() around name and the comparison string. 
		StringBuffer query = new StringBuffer();
		query.append(SQL_PREFIX);
		if (isNormalAdmin)
		{
			query.append("  JOIN platform.dbo.hchylvl h ON h.hchyid = u.cosubid AND h.company_id = u.company_id ");
		}
		query.append(" WHERE u.active = 1 ");
		query.append("    AND u.company_id = " + clientId + " ");
		query.append("    AND (   u.firstname like '%" + target + "%' ");
		query.append("         OR u.lastname like '%" + target + "%' ");
		query.append("         OR u. email like '%" + target + "%') ");
		if (isNormalAdmin)
		{
			query.append("    AND h.levelid LIKE (SELECT h.levelid ");
			query.append("                          FROM platform.dbo.users u, platform.dbo.hchylvl h ");
			query.append("                          WHERE u.users_id = " + adminId + " ");
			query.append("                            AND h.hchyid = u.cosubid) + '%' ");
		}
		query.append("  ORDER BY u.lastname, u.firstname");

		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(query);
		if (dlps.isInError())
		{
			System.out.println();
			LogWriter.logSQL(LogWriter.ERROR, this, 
					"Error preparing ParticipantHelper find query." + dlps.toString(), 
					query.toString());
			return ret;
		}

		DataResult dr = null;
		ResultSet rs = null;
		try
		{
			// Do the select
			dr = NhnDatabaseUtils.select(dlps);
			if (dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
			{
				// No problem... we are done here
				return ret;
			}
			if (dr.isInError())
			{
				LogWriter.logSQL(LogWriter.ERROR, this, "dr.IsInError()" 
						+ "Error finding in ParticipantHelper:  " +
						  "target=" + target +
						  "status="+ dr.getStatus().getStatusCode() +
						  ", exceptionCode=" + dr.getStatus().getExceptionCode() +
						  ", exceptionMsg=" + dr.getStatus().getExceptionMessage(), 
						  query.toString());
				return ret;
			}
			
			// Process the results
			rs = dr.getResultSet();
			try
			{
				if (rs == null || ! rs.isBeforeFirst())	
				{
					// No data.  Done here.
					return ret;
				}
			
				while (rs.next())
				{
					Participant part = getParticipant(rs);
					ret.add(part);
				}
			
				return ret;
			}
			catch (Exception e)
			{
				System.out.println();
				LogWriter.logBasicWithException(LogWriter.WARNING, this, 
						"Error processing data in ParticipantHelper find:  \n" +
						e.getMessage(), e);
				return new ArrayList<Participant>();
			}
			finally { /* Place holder */ }
		}
		finally
		{
//			if (rs != null)
//			{
//				try { rs.close(); }
//				catch (Exception e)	{ 
//					LogWriter.logBasicWithException(LogWriter.WARNING, this, 
//							"Error processing data in ParticipantHelper find:  \n" +
//							e.getMessage(), e);
//				}
//			}
			if (dr != null) { dr.close(); dr = null; }
		}
	}

	
	/*
	 * Get The admin notes for a participant.
	 * 
	 * @param session A SessionUser object
	 * @param adminId A participant ID
	 * @return A NoteHolder object
	 */
	@Override
	public NoteHolder getNotes(SessionUser session, String participantId)
	{
		StringBuffer query = new StringBuffer();
		query.append("SELECT un.user_note_id, ");
		query.append("       un.note, ");
		query.append("       un.date_added ");
		query.append("  FROM platform.dbo.user_note un ");
		query.append("  WHERE un.users_id = " + participantId + " ");
		query.append("  ORDER BY un.user_note_id");

		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(query);
		if (dlps.isInError())
		{
			System.out.println();
			LogWriter.logSQL(LogWriter.ERROR, this, 
					"Error preparing ParticipantHelper getNotes query." + dlps.toString(), 
					query.toString());
			return null;
		}

		DataResult dr = null;
		ResultSet rs = null;
		try
		{
			// Do the select
			dr = NhnDatabaseUtils.select(dlps);
			if (dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
			{
				// No problem... we are done here
				return null;
			}
			if (dr.isInError())
			{
				LogWriter.logSQL(LogWriter.ERROR, this, "dr.IsInError()" 
						+ "Error in getNotes in ParticipantHelper:  " +
						  "partId=" + participantId +
						  "status="+ dr.getStatus().getStatusCode() +
						  ", exceptionCode=" + dr.getStatus().getExceptionCode() +
						  ", exceptionMsg=" + dr.getStatus().getExceptionMessage(), 
						  query.toString());
				return null;
			}
			
			// Process the results
			rs = dr.getResultSet();
			try
			{
				if (rs == null || ! rs.isBeforeFirst())	
				{
					// No data.  Done here. (This should be a redundant check)
					return null;
				}
			
				NoteHolder nh = new NoteHolder(participantId);
				ArrayList<Note> ary = new ArrayList<Note>();
				while (rs.next())
				{
					ary.add(new Note(rs.getLong("user_note_id"), rs.getString("note"), rs.getString("date_added")));
				}
	
				nh.setNotesArray(ary);
				return nh;
			}
			catch (SQLException e)
			{
				LogWriter.logBasicWithException(LogWriter.WARNING, this, 
						"Error processing data in ParticipantHelper getNotes:  \n" +
						e.getMessage(), e);
				return null;
			}
			finally { /* Do nothing */ }
		}
		finally
		{
//			if (rs != null)
//			{
//				try { rs.close(); }
//				catch (Exception e)
//				{ 
//					LogWriter.logBasicWithException(LogWriter.WARNING, this, 
//							"Error processing data in ParticipantHelper getNotes:  \n" +
//							e.getMessage(), e);
//				}
//			}

			if (dr != null) { dr.close(); dr = null; }
		}
	}

}
