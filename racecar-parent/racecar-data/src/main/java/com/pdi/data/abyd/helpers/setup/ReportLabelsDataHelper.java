/**
 * Copyright (c) 2008, 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.setup;

//import java.sql.Connection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.pdi.data.abyd.dto.setup.Setup2ReportLabelValueDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportLabelsDTO;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.util.RequestStatus;
import com.pdi.data.util.language.DefaultLanguageHelper;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.data.constants.DataConstants;


/**
 * ReportLabelDataHelper contains all the code needed to set up an A by D report
 *
 * @author		Ken Beukelman
 @author		    MB Panichi
 */
public class ReportLabelsDataHelper
{
	//
	//  Static data.
	//

	// label types
	public static final String LEADERSHIP_LEGEND = "LEG_LEAD";
	public static final String TLT_LEGEND = "LEG_TLT";
	public static final String COVER_PAGE = "COV_PAGE";
	public static final String CUSTOM_LEGEND_LS = "CL_LS";
	public static final String CUSTOM_LEGEND_TL = "CL_TL";

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private long dfltLangId = 0;
	
	//
	// Constructors.
	//
	public ReportLabelsDataHelper()
		throws Exception
	{
		// Get the default language
		Connection con = AbyDDatabaseUtils.getDBConnection();
		try
		{
			
			dfltLangId = DefaultLanguageHelper.fetchDefaultLanguageId();
		}
		catch (Exception ex)
		{
			throw new Exception("Unable to retrieve default language in ReportLabelsDataHelper", ex);
		}finally{
			if(con != null){
				con.close();
			}
		}
	}
	
	//
	//  Instance methods.
	//

	/**
	 * Fetch label data.  Called by the A by D Design Setup app only.
	 * 
	 * ASSUMPTION:  The customizable labels are only customized in English.
	 *              That means that there is no language awareness (other than
	 *              it needs to use the default language).
	 * At such time as the users decide to allow customization in all languages,
	 * this method and the UI that uses it will have to be changed.
	 * 
	 * @param dnaId - the dna id
	 * @param editableLabels - types of labels to include in the fetch
	 * @return the EntryStateDTO object
	 * @throws Exception
	 */
	public  Setup2ReportLabelsDTO getSetupReportLabelData(long dnaId, int editableLabels)
		throws Exception
	{
		// Get the ALR model type flag
		boolean isAlr = chkAlrProj(dnaId);
		
		Setup2ReportLabelsDTO ret = new Setup2ReportLabelsDTO();
		ret.setDnaId(dnaId);
		
		String editable = "";

		switch(editableLabels){
			
			//case 1:
			case SetupConstants.EDITABLE:
				editable = "WHERE ll.editable = ? ";
				break;
			//case 2: 
			case SetupConstants.NOT_EDITABLE:
				editable = "WHERE ll.editable = ? ";
				break;
			//case 3:
			case SetupConstants.ALL_LABELS:
				editable = "";
				break;
			
		}
		
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT utxt.userText as userLabel, ");
		sqlQuery.append("       stxt.text as defaultLabel, ");
		sqlQuery.append("       ll.report_code, ");
		sqlQuery.append("       ll.report_label_id, ");
		sqlQuery.append("       ll.sequence ");
		sqlQuery.append("  FROM pdi_abd_rpt_label_lookup ll ");
		sqlQuery.append("    LEFT JOIN pdi_abd_text stxt ON (stxt.textId = ll.defaultLabelTextId ");
		sqlQuery.append("                                AND stxt.languageId = " + this.dfltLangId + ") ");
		sqlQuery.append("    LEFT JOIN pdi_abd_rpt_label_options_dna ld ON (ld.report_label_id = ll.report_label_id ");
		sqlQuery.append("                                               AND ld.dna_id = ?) ");
		sqlQuery.append("    LEFT JOIN user_text utxt ON (utxt.userTextId = ld.userTextId ");
		sqlQuery.append("                                AND utxt.languageId = " + this.dfltLangId + ")");
		sqlQuery.append("  " + editable + " ");
		sqlQuery.append("  ORDER BY ll.sequence");
	  
		DataLayerPreparedStatement dlps = AbyDDatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			return null;
		}
		
		DataResult dr = null;
		
		try
		{
			//Fill sql with values
			dlps.getPreparedStatement().setLong(1, dnaId);
			if( editableLabels == SetupConstants.EDITABLE ){
				dlps.getPreparedStatement().setInt(2, 1);
			}else if(editableLabels == SetupConstants.NOT_EDITABLE ){
				dlps.getPreparedStatement().setInt(2, 0);
			}
			
			
			dr = AbyDDatabaseUtils.select(dlps);
			
			ResultSet rs = dr.getResultSet();
			
			// Process returned data
			while(rs.next()) {
				String code = rs.getString("report_code");
				// Here be a kludge.  This requires that all ALR labels start 
				// with the sequence "ALR_". Is there a better way to do this?
				if (! isAlr && code.startsWith("ALR_"))
				{
					// Skip me
					continue;
				}
				Setup2ReportLabelValueDTO label = new Setup2ReportLabelValueDTO();
				label.setCustomLabel(rs.getString("userLabel") != null ? rs.getString("userLabel") : "");
				label.setLabel(rs.getString("defaultLabel"));
				label.setReportCode(code);
				label.setReportLabelId(rs.getLong("report_label_id"));
				label.setSequence(rs.getInt("sequence"));
				
				if(label.getReportCode().equals(LEADERSHIP_LEGEND))
				{
					ret.setLeadershipLegend(label);
				}
				else if(label.getReportCode().equals(TLT_LEGEND))
				{
					ret.setTltLegend(label);
				}
				else if(label.getReportCode().equals(COVER_PAGE))
				{
					ret.setCoverPage(label);
				} else if (label.getReportCode().equals(CUSTOM_LEGEND_LS) || 
						   label.getReportCode().equals(CUSTOM_LEGEND_TL))
				{
					// don't want these in the array for the grid... think we don't need them... not sure just yet... 
					// probably I could pull these out of the database, but that worries me, so gonna do it 
					// this way for now... 
				} else {
					ret.getLabelValuesArray().add(label);					
				}
			}
		}
		catch (SQLException ex)
		{
			// Do something real here.  Be sure you pass the status back
			ex.printStackTrace();
			return null;
		}
		finally
		{
			if (dr != null) {  dr.close();  dr = null; }
		}
 		return ret;
	}      

	
	/**
	 * Update label data in the Dashboard Report (User input changes).
	 * 
	 * NOTE:  At this time this code is not language aware.  It assumes that all
	 *  inputs are in US English.  If/When translations are needed for user input
	 *  label data, this logic will need to be reworked (can't delete everything...
	 *  it will need to be language aware AND need to add language to static text fetch).
	 *
	 * @param Connection con - the database connection
	 * @param Setup2ReportLabelsDTO dtoIn -- the incoming DTO that contains the updated data
	 * @throws Exception
	 */
	public void updateReportLabelData(Setup2ReportLabelsDTO dtoIn )
		throws Exception
	{
	    //System.out.println("updateReportLabelData :  dtoIn.getLabelValuesArray().size()" + dtoIn.getLabelValuesArray().size());
        String userId = "s2rptDataHlpr";
        
        labelTextDelete(dtoIn);
        labelTextInsert(userId, dtoIn);
	}


		/**
		 * Delete all relevant user label data (map rows and texts).
		 * 
		 * NOTE:  This code is NOT language aware
		 *
		 * @param Setup2ReportLabelsDTO dtoIn -- the incoming DTO; only DNA ID used here
		 * @throws Exception
		 */
		private void labelTextDelete(Setup2ReportLabelsDTO dtoIn )
			throws Exception
		{
		StringBuffer sqlQuery = new StringBuffer();
		DataResult dr = null;
		
		sqlQuery.append("SELECT userTextId ");
		sqlQuery.append("  FROM pdi_abd_rpt_label_options_dna lod ");
		sqlQuery.append("  WHERE lod.dna_id = " + dtoIn.getDnaId());
		DataLayerPreparedStatement dlps = AbyDDatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			throw new Exception("Error preparing to fetch text data.  " + dlps.getStatus().toString());
		}
		
		try
		{
			// Get the current text ids
			dr = AbyDDatabaseUtils.select(dlps);
			if (dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
			{
				// nothing to do... "Our job is done here"
				return;
			}
			if (dlps.isInError())
			{
				throw new Exception("Error fetching text data.  " + dlps.getStatus().toString());
			}

			// Save off the text IDs
			ArrayList<Integer> txtIds = new ArrayList<Integer>();
			ResultSet rs = dr.getResultSet();
			while(rs.next())
			{
				int id = rs.getInt("userTextId");
				txtIds.add(id);
			}
			
			// Delete the map rows
			sqlQuery.setLength(0);
			sqlQuery.append("DELETE FROM pdi_abd_rpt_label_options_dna ");
			sqlQuery.append("  WHERE dna_id = " + dtoIn.getDnaId());
			dlps = AbyDDatabaseUtils.prepareStatement(sqlQuery);
			if (dlps.isInError())
			{
				throw new Exception("Error preparing to delete label map data.  " + dlps.getStatus().toString());
			}
			AbyDDatabaseUtils.delete(dlps);
			if (dlps.isInError())
			{
				throw new Exception("Error deleting label map data.  " + dlps.getStatus().toString());
			}
			
			// Delete the text rows
			String ids = "";
			boolean first = true;
			for (Integer id : txtIds)
			{
				if (first)
					first = false;
				else
					ids += ", ";
				ids += id.toString();
			}
			sqlQuery.setLength(0);
			sqlQuery.append("DELETE FROM user_text ");
			sqlQuery.append("  WHERE userTextId in (" + ids + ")");
			dlps = AbyDDatabaseUtils.prepareStatement(sqlQuery);
			if (dlps.isInError())
			{
				throw new Exception("Error preparing to delete label texts.  " + dlps.getStatus().toString());
			}
			AbyDDatabaseUtils.delete(dlps);
			if (dlps.isInError())
			{
				throw new Exception("Error deleting label texts.  " + dlps.getStatus().toString());
			}
			
			return;
		}
		catch (SQLException ex)
		{
			throw new Exception("SQL Error in user text delete", ex);
		}
		finally
		{
			if (dr != null) {  dr.close();  dr = null; }
		}
	}


	/**
	 * Insert all relevant user label data (map rows and texts).
	 * 
	 * NOTE:  This code is NOT language aware
	 *
	 * @param userId --The user ID hat get put into the inserted rows
	 * @param dtoIn -- A Setup2ReportLabelsDTO object containing the updated data
	 * @throws Exception
	 */
	private void labelTextInsert(String userId, Setup2ReportLabelsDTO dtoIn )
		throws Exception
	{
		//ResultSet rs = null;
		DataLayerPreparedStatement insTxtDlps = null;
		DataLayerPreparedStatement updtTxtDlps = null;
		DataLayerPreparedStatement insMapDlps = null;
		DataResult drIt = null;
		DataResult drUt = null;
		DataResult drIm = null;
		
		// Create a comprehensive list of the candidates for insertion
		ArrayList<Setup2ReportLabelValueDTO> cand = new ArrayList<Setup2ReportLabelValueDTO>(dtoIn.getLabelValuesArray());
		cand.add(dtoIn.getCoverPage());
		cand.add(dtoIn.getLeadershipLegend());
		cand.add(dtoIn.getTltLegend());
		
		// Loop through all of the texts and see which ones to update
		for(Setup2ReportLabelValueDTO value : cand)
		{
			try
			{
				if (value == null  ||  value.getCustomLabel() == null  ||  value.getCustomLabel().length() < 1)
				{
					// nothing to do
					continue;
				}
				
				// Create the text insert PS
				StringBuffer q = new StringBuffer();
				q.append("INSERT INTO user_text ");
				q.append("  (languageId, typeCode, userText, isDefault, lastUserId, lastDate) ");
				q.append("  VALUES(" + dfltLangId + ", '" + DataConstants.UTT_LABEL + "', ?, 1, '" + userId + "', GETDATE())");
				insTxtDlps = AbyDDatabaseUtils.prepareStatement(q, Statement.RETURN_GENERATED_KEYS );
				if (insTxtDlps.isInError())
				{
					throw new Exception("Error preparing to insert texts.  " + insTxtDlps.getStatus().toString());
				}
		
				// Create the text update PS
				q.setLength(0);
				q.append("UPDATE user_text ");
				q.append("  SET userTextId = ? ");
				q.append("  WHERE uniqueId = ?");
				updtTxtDlps = AbyDDatabaseUtils.prepareStatement(q);
				if (updtTxtDlps.isInError())
				{
					throw new Exception("Error preparing to insert texts.  " + updtTxtDlps.getStatus().toString());
				}
		
				// Create the label insert PS
				q.setLength(0);
				q.append("INSERT INTO pdi_abd_rpt_label_options_dna ");
				q.append("       (dna_id, report_label_id, userTextId, dateCreated, createdBy) ");
				q.append("  VALUES (?, ?, ?, GETDATE(), '" + userId + "')");
				insMapDlps = AbyDDatabaseUtils.prepareStatement(q);
				if (insMapDlps.isInError())
				{
					throw new Exception("Error preparing to insert map rows.  " + insMapDlps.getStatus().toString());
				}
				
				// Insert the text
				insTxtDlps.getPreparedStatement().setString(1, value.getCustomLabel().replace("'", "''"));
				drIt = AbyDDatabaseUtils.insert(insTxtDlps);
				if (drIt.isInError())
				{
					throw new Exception("Error inserting label text (" + value.getCustomLabel() + ").  " + insTxtDlps.getStatus().toString());
				}
				
				updtTxtDlps.getPreparedStatement().setLong(1, drIt.getNewId());	// Text ID
				updtTxtDlps.getPreparedStatement().setLong(2, drIt.getNewId());	// Unique ID (same for the default row)
				drUt = AbyDDatabaseUtils.update(updtTxtDlps);
				if (drUt.isInError())
				{
					throw new Exception("Error updating label text (" + value.getCustomLabel() + ").  " + updtTxtDlps.getStatus().toString());
				}
				
				// Insert the map row
				insMapDlps.getPreparedStatement().setLong(1, dtoIn.getDnaId());	// DNA ID
				insMapDlps.getPreparedStatement().setLong(2, value.getReportLabelId());	// Report label ID
				insMapDlps.getPreparedStatement().setLong(3, drIt.getNewId());	// User text ID
				drIm = AbyDDatabaseUtils.insert(insMapDlps);
				if (drIm.isInError())
				{
					throw new Exception("Error inserting map row for " + dtoIn.getDnaId() + "|" + value.getReportLabelId() + ".  " + insMapDlps.getStatus().toString());
				}
			}
			finally
			{
				if (drIt != null) {  drIt.close();  drIt = null; }
				if (drUt != null) {  drUt.close();  drUt = null; }
				if (drIm != null) {  drIm.close();  drIm = null; }
			}
		}	// End of for loop
	}


	/**
	 * Fetch language aware label data.  Called by consumer methods requiring
	 * translations on the labels.  Do NOT use for setup.  The setup method
	 * (getTranslatedReportLabelData) should be modified to handle that.
	 * 
	 * ASSUMPTION:  The current rules associated with labels say that only US
	 * English labels are customizable.  Thus, when we go get data here we will
	 * attempt to fetch the label in the designated language first and if no
	 * data exists, then go for the text in the default language (first the
	 * custom string, then the default string).  At such time as the business
	 * decides to allow customization in all languages, this method should be
	 * examined (but may not have to be changed).
	 * 
	 * @param dnaId - the dna id
	 * @param languageId - the desired language id
	 * @param editableLabels - types of labels to include in the fetch
	 * @return the EntryStateDTO object
	 * @throws Exception
	 */
	public  Setup2ReportLabelsDTO getTranslatedReportLabelData(long dnaId, long langId, int editableLabels)
		throws Exception
	{
		Setup2ReportLabelsDTO ret = new Setup2ReportLabelsDTO();
		ret.setDnaId(dnaId);
		
		String editable = "";

		switch(editableLabels)
		{
			case SetupConstants.EDITABLE:
			case SetupConstants.NOT_EDITABLE:
				editable = "WHERE ll.editable = ? ";
				break;
			case SetupConstants.ALL_LABELS:
				editable = "";
				break;
		}
		
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT detxt.text as dfltEnStr, ");
		sqlQuery.append("	    dltxt.text as dfltLangStr, ");
		sqlQuery.append("	    uetxt.userText as usrEnStr, ");
		sqlQuery.append("	    ultxt.userText as userLangStr, ");
		sqlQuery.append("       ll.report_code, ");
		sqlQuery.append("       ll.report_label_id, ");
		sqlQuery.append("       ll.sequence ");
		sqlQuery.append("  FROM pdi_abd_rpt_label_lookup ll ");
		sqlQuery.append("	 LEFT JOIN pdi_abd_text detxt ON ");
		sqlQuery.append("        (detxt.textId = ll.defaultLabelTextId AND detxt.languageId = " + dfltLangId + ") ");
		sqlQuery.append("	 LEFT JOIN pdi_abd_text dltxt ON ");
		sqlQuery.append("        (dltxt.textId = ll.defaultLabelTextId AND dltxt.languageId = " + langId + ") ");
		sqlQuery.append("	 LEFT JOIN pdi_abd_rpt_label_options_dna ld ON ");
		sqlQuery.append("        (ld.report_label_id = ll.report_label_id AND ld.dna_id = " + dnaId + ") ");
		sqlQuery.append("	 LEFT JOIN user_text uetxt ON ");
		sqlQuery.append("        (uetxt.userTextId = ld.userTextId AND uetxt.languageId = " + dfltLangId + ") ");
		sqlQuery.append("	 LEFT JOIN user_text ultxt ON ");
		sqlQuery.append("        (ultxt.userTextId = ld.userTextId AND ultxt.languageId = " + langId + ") ");
		sqlQuery.append("  " + editable + " ");
		sqlQuery.append("  ORDER BY ll.sequence");

		DataLayerPreparedStatement dlps = AbyDDatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			return null;
		}
		
		DataResult dr = null;
		
		try
		{
			//Fill sql with values
			if( editableLabels == SetupConstants.EDITABLE ){
				dlps.getPreparedStatement().setInt(1, 1);
				//System.out.println("sql query: "  + sqlQuery.toString() + "   lang id: " + langId +  ",  dnaId: " + dnaId + ", editable: " + "1");
			}else if(editableLabels == SetupConstants.NOT_EDITABLE ){
				dlps.getPreparedStatement().setInt(1, 0);
				//System.out.println("sql query: "  + sqlQuery.toString() + "   lang id: " + langId +  ",  dnaId: " + dnaId + ", editable: " + "0");
			}

			dr = AbyDDatabaseUtils.select(dlps);
			ResultSet rs = dr.getResultSet();
			
			// Process returned data
			while(rs.next())
			{
				Setup2ReportLabelValueDTO label = new Setup2ReportLabelValueDTO();
				label.setReportCode(rs.getString("report_code").trim());
				label.setReportLabelId(rs.getLong("report_label_id"));
				label.setSequence(rs.getInt("sequence"));
				
				// Get the label strings.  Get the string in the specified language.  If
				// it doesn't exist, get it in the default language (English).  If it
				// still doesn't exist, set it to an empty string
				// First do the user/custom string
				String str = rs.getString("userLangStr");
				if (str == null || str.length() < 1)
					str = rs.getString("usrEnStr");
				label.setCustomLabel(str == null ? "" : str);
				// Then do the default label
				str = rs.getString("dfltLangStr");
				if (str == null || str.length() < 1)
					str = rs.getString("dfltEnStr");
				label.setLabel(str == null ? "" : str);
				//System.out.println("sql:  label: " + label.getLabel() +  ",  rpt code:  " + label.getReportCode());			
				if(label.getReportCode().equals(LEADERSHIP_LEGEND))
				{
					ret.setLeadershipLegend(label);
				}
				else if(label.getReportCode().equals(TLT_LEGEND))
				{
					ret.setTltLegend(label);
				}
				else if(label.getReportCode().equals(COVER_PAGE))
				{
					ret.setCoverPage(label);
				} else if (label.getReportCode().equals(CUSTOM_LEGEND_LS) || 
						   label.getReportCode().equals(CUSTOM_LEGEND_TL))
				{
					// don't want these in the array for the grid... think we don't need them... not sure just yet... 
					// probably I could pull these out of the database, but that worries me, so gonna do it 
					// this way for now... 
				} else {
					ret.getLabelValuesArray().add(label);					
				}
			}

			return ret;
		}
		catch (SQLException ex)
		{
			// Do something real here.
			ex.printStackTrace();
			throw new Exception("Exception processing SQL data in getTranslatedReportLabelData", ex);
		}
		finally
		{
			if (dr != null) {  dr.close();  dr = null; }
		}
	}      


	/**
	 * Fetch language aware label data using a language code.  A convenience method that
	 * converts a language code into a language id and then calls the function that uses
	 * the language ID. See a full discussion of the functionality in the method of the
	 * same name but with the language ID instead (above).
	 * 
	 * @param dnaId - the dna id
	 * @param langCode - the language code of the desired language
	 * @param editableLabels - types of labels to include in the fetch
	 * @return the EntryStateDTO object
	 * @throws Exception
	 */
	public  Setup2ReportLabelsDTO getTranslatedReportLabelData(long dnaId, String langCode, int editableLabels)
		throws Exception
	{
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT lan.languageId ");
		sqlQuery.append("  FROM pdi_abd_language lan ");
		sqlQuery.append("  WHERE languageCode = '" + langCode + "'");

		DataLayerPreparedStatement dlps = AbyDDatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			throw new Exception("Prepare SQL in getTranslatedReportLabelData (langCode); " +
					"code=" + dlps.getStatus().getExceptionCode() + ", "+
					"msg=" + dlps.getStatus().getExceptionMessage());
		}
		
		DataResult dr = null;
		try
		{
			dr = AbyDDatabaseUtils.select(dlps);
			ResultSet rs = dr.getResultSet();
			
			if (! rs.isBeforeFirst())
			{
				throw new Exception("No data returned in getTranslatedReportLabelData (langCode); " +
						"langCode=" + langCode);
			}

			// Process returned data
			rs.next();	// Should be single row returned
			long langId = rs.getLong("languageId");

			return getTranslatedReportLabelData(dnaId, langId, editableLabels);
		}
		catch (SQLException ex)
		{
			// Do something real here.
			ex.printStackTrace();
			throw new Exception("Exception processing SQL data in getTranslatedReportLabelData (code)", ex);
		}
		finally
		{
			if (dr != null) {  dr.close();  dr = null; }
		}
	}


	/**
	 * chkAlrProj -  Checks to see if this is an ALR model by checking to see if the first 4 characters of the internalName are "ALR ".
	 * 				 Figure out a better way to do this as it restricts all ALR models to internal names starting with that sequence.
	 * 				 With the labels, err on the side of caution and show 'em all.
	 * @param dnaId
	 * @return
	 */
	private boolean chkAlrProj(long dnaId)
	{
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT mm.internalName ");
		sqlQuery.append("  FROM pdi_abd_model mm ");
		sqlQuery.append("  WHERE mm.modelId = (SELECT modelId ");
		sqlQuery.append("                        FROM pdi_abd_dna ");
		sqlQuery.append("                        WHERE dnaId = " + dnaId + ") ");
 
		DataLayerPreparedStatement dlps = AbyDDatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			System.out.println("chkAlrProj: Error retrieving model name for DNA " + dnaId + ".  Returned ALR = true.");
			return true;
		}
		
		DataResult dr = null;

		try
		{
			boolean ret = false;
			dr = AbyDDatabaseUtils.select(dlps);
			if (dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
			{
				System.out.println("chkAlrProj: No data returned for DNA " + dnaId + " (possible new model?).  Returned ALR = true...");
				return true;
			}
			
			ResultSet rs = dr.getResultSet();
			
			// Process returned data - should be a single row
			if (! rs.isBeforeFirst())
			{
				System.out.println("chkAlrProj: Empty result set.  Shouldn't get here.  DNA " + dnaId + " (possible new model?).  Returned ALR = true...");
				return true;
			}
			
			// Get the row
			rs.next();
			String iName = rs.getString("internalName");
			// Here be a kludge.  Is there a better way to do this?
			if (iName.startsWith("ALR "))
			{
				ret = true;
			}
			return ret;
		}
		catch (SQLException ex)
		{
			System.out.println("Error fetching internal model name for DNA " + dnaId + ".  Returned ALR = true.");
			ex.printStackTrace();
			return true;
		}
		finally
		{
			if (dr != null) {  dr.close();  dr = null; }
		}
 	}
}

