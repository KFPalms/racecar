/**
 * Copyright (c) 2009, 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.intGrid;

//import java.math.BigDecimal;
//import java.math.MathContext;
//import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
//import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.pdi.data.abyd.dto.intGrid.IGTestAndNormNameData;
import com.pdi.data.abyd.dto.intGrid.IGScaleScoreData;
import com.pdi.data.abyd.dto.intGrid.IGIntersectionAndOtherDataDTO;
import com.pdi.data.abyd.dto.common.DNACellDTO;
import com.pdi.data.abyd.helpers.intGrid.IGConstants;
import com.pdi.data.abyd.util.Utils;
import com.pdi.data.dto.NormGroup;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.INormGroupHelper;
//import com.pdi.properties.PropertyLoader;
import com.pdi.scoring.Norm;
import com.pdi.webservice.NhnWebserviceUtils;


/**
 * IGScoring contains the scoring logic associated exclusively with the Integration Grid.
 * As such, it is primarily concerned with the data in the testing column as the scores
 * for the Eval Guides are calculated in the Eval Guide code.
 * 
 * This module contains some code that is used in more than one of the data helpers in
 * the Integration Grid app.  Insofar as possible, this code has been implemented as
 * static methods to avoid having to instantiate a separate class.
 *
 * @author		Ken Beukelman
 */
public class IGScoring
{
	//
	// Static data.
	//
	
	// SAC II model name
	static private String SAC_II_MODEL_NAME = "Saudi Aramco SAC II";
	static private String SAC_II_GP_NORM_NAME = "North America";
	static private String SAC_II_SP_NORM_NAME = "Aramco Supervisor";

	// List of all IG competencies from all models
	static private String All_COMP_LIST =
			// mll competencies
		"'Act Strategically'," +					// mll
		"'Build Realistic Plans'," +				// mll
		"'Build Support'," +						// mll
		"'Develop Others'," +						// mll
		"'Establish Relationships'," +				// mll
		"'Establish Trust'," +						// mll
		"'Foster Open Communication'," +			// mll
		"'Make Sound Decisions'," +					// mll
		"'Manage Execution'," +						// mll
		"'Meet Customer Needs'," +					// mll
		"'Motivate Others'," +						// mll
		"'Promote Teamwork'," +						// mll
		"'Show Adaptability'," +					// mll
		"'Show Drive and Initiative'," +			// mll
		"'Think Creatively'," +						// mll
		"'Use Financial Data'," +					// mll
		"'Display Global Awareness'," +				// mll
		"'Humility - MOHG'," +						// mll
			// bul competencies
		"'Adapt and Learn'," +						// bul
		"'Apply Financial Acumen'," +				// bul
		"'Build Relationships'," +					// bul
		"'Build Talent'," +							// bul
		"'Display Global Perspective'," +			// bul
		"'Drive for Results'," +					// bul
		"'Engage and Inspire'," +					// bul
		"'Ensure Execution'," +						// bul
		"'Focus on Customers'," +					// bul
		"'Influence Others'," +						// bul
		"'Innovate'," +								// bul
		"'Inspire Trust'," +						// bul
		"'Lead Courageously'," +					// bul
		"'Promote Collaboration'," +				// bul
		"'Think Strategically'," +					// bul
		"'Use Insightful Judgment'," +				// bul
		// Humility looks like exactly the same one as for the mll,
		// but on the off chance that the scoring may change, let's
		// follow the pattern used in every other model/competency
		// combo and give it a different internal name
		"'Humility - MOHG BUL'," +					// bul
		// P66 BUL has 3 competencies that are new or scored
		// differently or from other models
		"'Foster Novel Thinking - P66 BUL'," +		// bul - P66
		"'Foster Collaborative Relationships - P66 BUL'," +		// bul - P66
		"'Demonstrate Agility - P66 BUL'," +		// bul - P66
		// sea competencies
		"'Align the Organization'," +				// sea
		"'Apply Financial Insights'," +				// sea
		"'Build Organizational Relationships'," +	// sea
		"'Demonstrate Agility'," +					// sea
		"'Develop Organizational Talent'," +		// sea
		"'Display Vision'," +						// sea
		"'Drive Global Integration'," +				// sea
		"'Drive Organizational Success'," +			// sea
		"'Earn Unwavering Trust'," +				// sea
		"'Energize the Organization'," +			// sea
		"'Ensure Collaboration'," +					// sea
		"'Ensure Customer Focus'," +				// sea
		"'Lead Boldly'," +							// sea
		"'Optimize Execution'," +					// sea
		"'Shape Strategy'," +						// sea
		"'Use Astute Judgment'," +					// sea
		"'Use Organizational Influence'," +			// sea
		// Secial SEA for J&J (No WG-E in the Cogs scoring
		"'Use Astute Judgement - NoWG'," +			// sea (J&J)
		"'Shape Strategy - NoWG'," +				// sea (J&J)
		"'Display Vision - NoWG'," +				// sea (J&J)

		// fll competencies
		"'Analyze Issues and Solve Problems'," +	// fll
		"'Understand Strategies'," +				// fll
		"'Identify Improvements'," +				// fll
		"'Seek Customer Satisfaction'," +			// fll
		"'Establish Plans'," +						// fll
		"'Execute Efficiently'," +					// fll
		"'Show Initiative'," +						// fll
		"'Solicit Support'," +						// fll
		"'Encourage Commitment'," +					// fll
		"'Select and Develop'," +					// fll
		"'Communicate Effectively'," +				// fll
		"'Relate Well to Others'," +				// fll
		"'Demonstrate Credibility'," +				// fll
		"'Readily Adapt'," +						// fll
		"'Learn and Grow'," +						// fll
		"'Act with Integrity'," +					// fll
		
		// New FLL competencies
		"'Analyze Issues and Solve Problems - II'," +	// fll II
		"'Understand Strategies - II'," +				// fll II
		"'Identify Improvements - II'," +				// fll II
		"'Seek Customer Satisfaction - II'," +			// fll II
		"'Establish Plans - II'," +						// fll II
		"'Execute Efficiently - II'," +					// fll II
		"'Show Initiative - II'," +						// fll II
		"'Solicit Support - II'," +						// fll II
		"'Encourage Commitment - II'," +				// fll II
		"'Select and Develop - II'," +					// fll II
		"'Communicate Effectively - II'," +				// fll II
		"'Relate Well to Others - II'," +				// fll II
		"'Demonstrate Credibility - II'," +				// fll II
		"'Readily Adapt - II'," +						// fll II
		"'Learn and Grow - II'," +						// fll II
		"'Act with Integrity - II'," +					// fll II
		
		// Ericsson Executive
		"'Build Customer Orientation - EEA'," +			// Ericsson Executive
		"'Ensure Outstanding Performance - EEA'," +		// Ericsson Executive
		"'Shapes Strategy - EEA'," +					// Ericsson Executive
		"'Creates a Culture of Innovation - EEA'," +	// Ericsson Executive
		"'Builds Capabilities - EEA'," +				// Ericsson Executive
		"'Shapes View - EEA'," +						// Ericsson Executive
		"'Removes Obstacles & Maintains Focus - EEA'," +	// Ericsson Executive
		"'Leads Across Boundaries - EEA'," +			// Ericsson Executive
		"'Pushes for Radical Change - EEA'," +			// Ericsson Executive
		"'Thrives on Curiosity - EEA'," +				// Ericsson Executive
		"'Learns and Grows - EEA'," +					// Ericsson Executive
		"'Demonstrates Resilience - EEA'," +			// Ericsson Executive
		"'Speed, Quality, Integrity - EEA'," +			// Ericsson Executive
		
		// Ericsson EEA/EAC 2015
		"'Competence Drive - EEA/EAC'," +				// Ericsson EEA/EAC 2015
		"'Master Strategy - EEA/EAC'," +				// Ericsson EEA/EAC 2015
		"'Driving Innovation - EEA/EAC'," +				// Ericsson EEA/EAC 2015
		"'Business Acumen - EEA/EAC'," +				// Ericsson EEA/EAC 2015
		"'Enabling People - EEA/EAC'," +				// Ericsson EEA/EAC 2015
		"'Courageous Leadership - EEA/EAC'," +			// Ericsson EEA/EAC 2015
		"'Uncompromising Integrity - EEA/EAC'," +		// Ericsson EEA/EAC 2015
		"'Customer Excellence - EEA/EAC'," +			// Ericsson EEA/EAC 2015
		"'Excelling Execution - EEA/EAC'," +			// Ericsson EEA/EAC 2015
		"'Embracing Change - EEA/EAC'," +				// Ericsson EEA/EAC 2015		
		
		// Ericsson L1
		"'Focuses on Customer Needs'," +			// Ericsson L1
		"'Demands Excellence'," +					// Ericsson L1
		"'Outside-In Thinking'," +					// Ericsson L1
		"'Embraces New Ideas'," +					// Ericsson L1
		"'Results Leadership'," +					// Ericsson L1
		"'Develops People'," +						// Ericsson L1
		"'Listens and Shares Information'," +		// Ericsson L1
		"'People Leadership'," +					// Ericsson L1
		"'Prioritizes'," +							// Ericsson L1
		"'Leverages Diversity'," +					// Ericsson L1
		"'Personal Leadership'," +					// Ericsson L1
		"'Learns and Grows'," +						// Ericsson L1
		"'Shows Flexibility'," +					// Ericsson L1
		"'Acts with Integrity'," +					// Ericsson L1
		
		// Ericsson Competency structure
		//	This structure was set up for the Ericsson L1 v2 model but it matches the
		//	more recent (than the original L1) Ericsson models EEA/EAC and Ericsson L3
		//	Business/Sales & R&D.  This should be checked for scoring consistency with
		//	any new Erricson models come in and used where possible (the above mentioned
		//	model's competencies are scored with the same scales... no need to duplicate
		//	it over and over).
		"'Competence Drive - ECS'," +				// New Ericsson competency structure
		"'Master Strategy - ECS'," +				// New Ericsson competency structure
		"'Driving Innovation - ECS'," +				// New Ericsson competency structure
		"'Business Acumen - ECS'," +				// New Ericsson competency structure
		"'Enabling People - ECS'," +				// New Ericsson competency structure
		"'Courageous Leadership - ECS'," +			// New Ericsson competency structure
		"'Uncompromising Integrity - ECS'," +		// New Ericsson competency structure
		"'Customer Excellence - ECS'," +			// New Ericsson competency structure
		"'Excelling Execution - ECS'," +			// New Ericsson competency structure
		"'Embracing Change - ECS'," +				// New Ericsson competency structure	
		
		// Ericsson L3
		"'Anticipates Customer Needs - EL3'," +			// Ericsson L3
		"'Creates Business Value - EL3'," +				// Ericsson L3
		"'Demonstrates Foresight - EL3'," +				// Ericsson L3
		"'Demonstrates Entrepreneurship - EL3'," +		// Ericsson L3
		"'Ensures Organizational Commitment - EL3'," +	// Ericsson L3
		"'Ensures Impact - EL3'," +						// Ericsson L3
		"'Simplifies - EL3'," +							// Ericsson L3
		"'Drives Organizational Alignment - EL3'," +	// Ericsson L3
		"'Champions Change - EL3'," +					// Ericsson L3
		"'Thrives on Curiosity - EL3'," +				// Ericsson L3
		"'Learns and Grows - EL3'," +					// Ericsson L3
		"'Handles Ambiguity - EL3'," +					// Ericsson L3
		"'Acts with Integrity - EL3'," +				// Ericsson L3

		// Ericsson Sales Competencies.
		"'Creating-Innovating - ES'," +				// Ericsson Sales
		"'Entrepreneurial Thinking - ES'," +		// Ericsson Sales
		"'Formulating Strategies - ES'," +			// Ericsson Sales
		"'Persuading-Influencing - ES'," +			// Ericsson Sales
		"'Applying Expertise - ES'," +				// Ericsson Sales
		"'Relating-Networking - ES'," +				// Ericsson Sales
		"'Coping - ES'," +							// Ericsson Sales
		"'Delivering Results - ES'," +				// Ericsson Sales
		"'Deciding-Initiating - ES'," +				// Ericsson Sales
		"'Achieving Goals-Objectives - ES'," +		// Ericsson Sales

		// Ericsson L3 Business/Sales & R&D
		"'Competence Drive - EBSRD'," +				// Ericsson L3 Business/Sales & R&D
		"'Master Strategy - EBSRD'," +				// Ericsson L3 Business/Sales & R&D
		"'Driving Innovation - EBSRD'," +			// Ericsson L3 Business/Sales & R&D
		"'Business Acumen - EBSRD'," +				// Ericsson L3 Business/Sales & R&D
		"'Enabling People - EBSRD'," +				// Ericsson L3 Business/Sales & R&D
		"'Courageous Leadership - EBSRD'," +		// Ericsson L3 Business/Sales & R&D
		"'Uncompromising Integrity - EBSRD'," +		// Ericsson L3 Business/Sales & R&D
		"'Customer Excellence - EBSRD'," +			// Ericsson L3 Business/Sales & R&D
		"'Excelling Execution - EBSRD'," +			// Ericsson L3 Business/Sales & R&D
		"'Embracing Change - EBSRD'," +				// Ericsson L3 Business/Sales & R&D
		
		// Saudi Aramco MAC competencies
		"'Use Sound Judgment - MAC'," +					// Saudi Aramco MAC
		"'Strategic Insight - MAC'," +					// Saudi Aramco MAC
		"'Business Acumen - MAC'," +					// Saudi Aramco MAC
		"'Foster Innovation - MAC'," +					// Saudi Aramco MAC
		"'Display Global Perspective - MAC'," +			// Saudi Aramco MAC
		"'Motivate Engage Inspire - MAC'," +			// Saudi Aramco MAC
		"'Influence Others - MAC'," +					// Saudi Aramco MAC
		"'Manage and Develop Talent - MAC'," +			// Saudi Aramco MAC
		"'Foster Open Communication - MAC'," +			// Saudi Aramco MAC
		"'Build Relationships - MAC'," +				// Saudi Aramco MAC
		"'Foster Teamwork and Collaboration - MAC'," +	// Saudi Aramco MAC
		"'Drive for Results - MAC'," +					// Saudi Aramco MAC
		"'Manage Execution - MAC'," +					// Saudi Aramco MAC
		"'Adaptability and Resilience - MAC'," +		// Saudi Aramco MAC
		"'Inspire Trust - MAC'," +						// Saudi Aramco MAC
		
		// Saudi Aramco SAC competencies
		"'Analyze & Solve Problems - SAC'," +			// Saudi Aramco SAC
		"'Use Business Metrics - SAC'," +				// Saudi Aramco SAC
		"'Show Drive & Initiative - SAC'," +			// Saudi Aramco SAC
		"'Direct Staff & Delegate - SAC'," +			// Saudi Aramco SAC
		"'Plan & Organize Work - SAC'," +				// Saudi Aramco SAC
		"'Encourage & Motivate - SAC'," +				// Saudi Aramco SAC
		"'Build Support - SAC'," +						// Saudi Aramco SAC
		"'Coach & Develop Talent - SAC'," +				// Saudi Aramco SAC
		"'Communicate Openly - SAC'," +					// Saudi Aramco SAC
		"'Build Relationships - SAC'," +				// Saudi Aramco SAC
		"'Foster Teamwork - SAC'," +					// Saudi Aramco SAC
		"'Adapt & Learn - SAC'," +						// Saudi Aramco SAC

		// Saudi Aramco SAC II competencies
		"'Analyze & Solve Problems - SAC II'," +		// Saudi Aramco SAC II
		"'Use Business Metrics - SAC II'," +			// Saudi Aramco SAC II
		"'Show Drive & Initiative - SAC II'," +			// Saudi Aramco SAC II
		"'Direct Staff & Delegate - SAC II'," +			// Saudi Aramco SAC II
		"'Plan & Organize Work - SAC II'," +			// Saudi Aramco SAC II
		"'Encourage & Motivate - SAC II'," +			// Saudi Aramco SAC II
		"'Build Support - SAC II'," +					// Saudi Aramco SAC II
		"'Coach & Develop Talent - SAC II'," +			// Saudi Aramco SAC II
		"'Communicate Openly - SAC II'," +				// Saudi Aramco SAC II
		"'Build Relationships - SAC II'," +				// Saudi Aramco SAC II
		"'Foster Teamwork - SAC II'," +					// Saudi Aramco SAC II
		"'Adapt & Learn - SAC II'," +					// Saudi Aramco SAC II

		// Saudi Aramco CAC competencies
		"'Think Strategically - CAC'," +				// Saudi Aramco CAC
		"'Use Seasoned Judgment - CAC'," +				// Saudi Aramco CAC
		"'Employ Financial Acumen - CAC'," +			// Saudi Aramco CAC
		"'Display Visionary Thinking - CAC'," +			// Saudi Aramco CAC
		"'Drive Global Integration - CAC'," +			// Saudi Aramco CAC
		"'Engage and Inspire - CAC'," +					// Saudi Aramco CAC
		"'Display Organizational Influence - CAC'," +	// Saudi Aramco CAC
		"'Develop Organizational Talent - CAC'," +		// Saudi Aramco CAC
		"'Foster Open Communication - CAC'," +			// Saudi Aramco CAC
		"'Build Relationships - CAC'," +				// Saudi Aramco CAC
		"'Drive Organizational Success - CAC'," +		// Saudi Aramco CAC
		"'Drive Execution - CAC'," +					// Saudi Aramco CAC
		"'Align the Organization - CAC'," +				// Saudi Aramco CAC
		"'Lead Courageously - CAC'," +					// Saudi Aramco CAC
		"'Adaptability and Resilience - CAC'," +		// Saudi Aramco CAC
		"'Earn Unwavering Trust - CAC'," +				// Saudi Aramco CAC		

		// Saudi Aramco DAC competencies
		"'Make Sound Decisions - DAC'," +				// Saudi Aramco DAC	
		"'Understand Strategies - DAC'," +				// Saudi Aramco DAC	
		"'Use Business Metrics - DAC'," +				// Saudi Aramco DAC	
		"'Motivate Others - DAC'," +					// Saudi Aramco DAC	
		"'Influence Others - DAC'," +					// Saudi Aramco DAC	
		"'Coach and Develop Talent - DAC'," +			// Saudi Aramco DAC	
		"'Foster Open Communication - DAC'," +			// Saudi Aramco DAC	
		"'Build Relationships - DAC'," +				// Saudi Aramco DAC	
		"'Promote teamwork - DAC'," +					// Saudi Aramco DAC	
		"'Drive for Results - DAC'," +					// Saudi Aramco DAC	
		"'Manage Execution - DAC'," +					// Saudi Aramco DAC	
		"'Build Realistic Plans - DAC'," +				// Saudi Aramco DAC	
		"'Establish Trust - DAC'," +					// Saudi Aramco DAC	
		"'Show Adaptability - DAC'," +					// Saudi Aramco DAC	

		// SAM competencies
		"'Identify Innovative Targeted Solutions - SAM'," +	// SAM
		"'Think Strategically - SAM'," +					// SAM
		"'Plan and Manage Execution - SAM'," +				// SAM
		"'Customer Focus - SAM'," +							// SAM
		"'Drive for Results - SAM'," +						// SAM
		"'Influence and Negotiate - SAM'," +				// SAM
		"'Lead the Team - SAM'," +							// SAM
		"'Communicate Effectively - SAM'," +				// SAM
		"'Enhance Professional Skills - SAM'," +			// SAM

		// Target MLL competencies
		"'Make Sound Decisions - Target'," +		// Target mll
		"'Act Strategically - Target'," +			// Target mll
		"'Think Creatively - Target'," +			// Target mll
		"'Build Realistic Plans - Target'," +		// Target mll
		"'Manage Execution - Target'," +			// Target mll
		"'Show Drive and Initiative - Target'," +	// Target mll
		"'Build Support - Target'," +				// Target mll
		"'Motivate Others - Target'," +				// Target mll
		"'Develop Others - Target'," +				// Target mll
		"'Promote Teamwork - Target'," +			// Target mll
		"'Foster Open Communication - Target'," +	// Target mll
		"'Establish Relationships - Target'," +		// Target mll
		"'Establish Trust - Target'," +				// Target mll
		"'Show Adaptability - Target'," +			// Target mll
		//Target BUL competencies
		"'Use Insightful Judgment - Target'," +		// Target bll
		"'Think Strategically - Target'," +			// Target bll
		"'Innovate - Target'," +					// Target bll
		"'Ensure Execution - Target'," +			// Target bll
		"'Drive for Results - Target'," +			// Target bll
		"'Lead Courageously - Target'," +			// Target bll
		"'Influence Others - Target'," +			// Target bll
		"'Engage and Inspire - Target'," +			// Target bll
		"'Promote Collaboration - Target'," +		// Target bll
		"'Build Talent - Target'," +				// Target bll
		"'Build Relationships - Target'," +			// Target bll
		"'Inspire Trust - Target'," +				// Target bll
		"'Adapt and Learn - Target'," +				// Target bll

		// All KFLA competencies
		"'Business Insight - KFLA'," +				// KFLA
		"'Customer Focus - KFLA'," +				// KFLA
		"'Financial Acumen - KFLA'," +				// KFLA
		"'Tech Savvy - KFLA'," +					// KFLA
		"'Manages Complexity - KFLA'," +			// KFLA
		"'Decision Quality - KFLA'," +				// KFLA
		"'Balances Stakeholders - KFLA'," +			// KFLA
		"'Global Perspective - KFLA'," +			// KFLA
		"'Cultivates Innovation - KFLA'," +			// KFLA
		"'Strategic Mindset - KFLA'," +				// KFLA
		"'Action Oriented - KFLA'," +				// KFLA
		"'Resourcefulness - KFLA'," +				// KFLA
		"'Directs Work - KFLA'," +					// KFLA
		"'Plans and Aligns - KFLA'," +				// KFLA
		"'Optimizes Work Processes - KFLA'," +		// KFLA
		"'Ensures Accountability - KFLA'," +		// KFLA
		"'Drives Results - KFLA'," +				// KFLA
		"'Collaborates - KFLA'," +					// KFLA
		"'Manages Conflict - KFLA'," +				// KFLA
		"'Interpersonal Savvy - KFLA'," +			// KFLA
		"'Builds Networks - KFLA'," +				// KFLA
		"'Attracts Top Talent - KFLA'," +			// KFLA
		"'Develops Talent - KFLA'," +				// KFLA
		"'Values Differences - KFLA'," +			// KFLA
		"'Builds Effective Teams - KFLA'," +		// KFLA
		"'Communicates Effectively - KFLA'," +		// KFLA
		"'Drives Engagement - KFLA'," +				// KFLA
		"'Organization Savvy - KFLA'," +			// KFLA
		"'Persuades - KFLA'," +						// KFLA
		"'Drives Vision and Purpose - KFLA'," +		// KFLA
		"'Courage - KFLA'," +						// KFLA
		"'Instills Trust - KFLA'," +				// KFLA
		"'Demonstrates Self-Awareness - KFLA'," +	// KFLA
		"'Self-Development - KFLA'," +				// KFLA
		"'Manages Ambiguity - KFLA'," +				// KFLA
		"'Nimble Learning - KFLA'," +				// KFLA
		"'Being Resilient - KFLA'," +				// KFLA
		"'Situational Adaptability - KFLA'," +		// KFLA

		// All SABIC MAC competencies
		"'Business Acumen - SABIC MAC'," +				// SABIC MAC
		"'Drive for Results - SABIC MAC'," +			// SABIC MAC
		"'Planning and Organizing - SABIC MAC'," +		// SABIC MAC
		"'Strategic Focus - SABIC MAC'," +				// SABIC MAC
		"'Develop and Empower People - SABIC MAC'," +	// SABIC MAC
		"'Inclusiveness - SABIC MAC'," +				// SABIC MAC
		"'Leading Change - SABIC MAC'," +				// SABIC MAC
		"'Motivate Others - SABIC MAC'," +				// SABIC MAC
		"'Personal Leadership - SABIC MAC'," +			// SABIC MAC
	
		// J&J BUL Competencies.  Same as J&J SEA compentencies (later) but requires
		// unique name because scoring is different between the two models
		"'Cultivate external relationships - JnJ BUL'," +		// J&J BUL
		"'Be insight-driven - JnJ BUL'," +						// J&J BUL
		"'Forge internal collaboration - JnJ BUL'," +			// J&J BUL
		"'Translate insights - JnJ BUL'," +						// J&J BUL
		"'Challenge the status-quo - JnJ BUL'," +				// J&J BUL
		"'Take and manage risks - JnJ BUL'," +					// J&J BUL
		"'Take ownership for talent - JnJ BUL'," +				// J&J BUL
		"'Diversity and inclusion - JnJ BUL'," +				// J&J BUL
		"'Transparent constructive conversations - JnJ BUL'," +	// J&J BUL
		"'Empower people - JnJ BUL'," +							// J&J BUL
		"'Global and enterprise-wide mindset - JnJ BUL'," +		// J&J BUL
		"'Balance strategic choices - JnJ BUL'," +				// J&J BUL
		"'Credo - JnJ BUL'," +									// J&J BUL

		// J&J SEA Competencies.  See note on J&J BUL compentencies (above)
		"'Cultivate external relationships - JnJ SEA'," +		// J&J SEA
		"'Be insight-driven - JnJ SEA'," +						// J&J SEA
		"'Forge internal collaboration - JnJ SEA'," +			// J&J SEA
		"'Translate insights - JnJ SEA'," +						// J&J SEA
		"'Challenge the status-quo - JnJ SEA'," +				// J&J SEA
		"'Take and manage risks - JnJ SEA'," +					// J&J SEA
		"'Take ownership for talent - JnJ SEA'," +				// J&J SEA
		"'Diversity and inclusion - JnJ SEA'," +				// J&J SEA
		"'Transparent constructive conversations - JnJ SEA'," +	// J&J SEA
		"'Empower people - JnJ SEA'," +							// J&J SEA
		"'Global and enterprise-wide mindset - JnJ SEA'," +		// J&J SEA
		"'Balance strategic choices - JnJ SEA'," +				// J&J SEA
		"'Credo - JnJ SEA'," +									// J&J SEA
		"'Innovation - JnJ SEA'," +								// J&J SEA
	
		// BGB Level 6 Competencies
		"'Exceed Customer Needs - BGB_L6'," +					// BGB Level 6
		"'Plans for Delivery - BGB_L6'," +						// BGB Level 6
		"'Effective Decisionss - BGB_L6'," +					// BGB Level 6
		"'Develops Others - BGB_L6'," +							// BGB Level 6
		"'Communicates Vision - BGB_L6'," +						// BGB Level 6
		"'Feedback - BGB_L6'," +								// BGB Level 6
		"'Leads - BGB_L6'," +									// BGB Level 6
		"'Accountability - BGB_L6'," +							// BGB Level 6
		"'Establishes Trust - BGB_L6'," +						// BGB Level 6
		
		// Coca-Cola MLL model
		"'Attracts Top Talent - CCM'," +						// Coca-Cola Hellenic MLL
		"'Develops Talent - CCM'," +							// Coca-Cola Hellenic MLL
		"'Builds Effective Teams - CCM'," +						// Coca-Cola Hellenic MLL
		"'Drives Engagement - CCM'," +							// Coca-Cola Hellenic MLL
		"'Strategic Mindset - CCM'," +							// Coca-Cola Hellenic MLL
		"'Drives Vision and Purpose - CCM'," +					// Coca-Cola Hellenic MLL
		"'Courage - CCM'," +									// Coca-Cola Hellenic MLL
		"'Financial Acumen - CCM'," +							// Coca-Cola Hellenic MLL
		"'Decision Quality - CCM'," +							// Coca-Cola Hellenic MLL
		"'Drives Results - CCM'," +								// Coca-Cola Hellenic MLL
		"'Manages Complexity - CCM'," +							// Coca-Cola Hellenic MLL
		"'Resourcefulness - CCM'," +							// Coca-Cola Hellenic MLL
		"'Plans and Aligns - CCM'," +							// Coca-Cola Hellenic MLL
		"'Ensures Accountability - CCM'," +						// Coca-Cola Hellenic MLL
		"'Business Insight - CCM'," +							// Coca-Cola Hellenic MLL
		"'Cultivates Innovation - CCM'," +						// Coca-Cola Hellenic MLL
		"'Customer Focus - CCM'," +								// Coca-Cola Hellenic MLL
		"'Collaborates - CCM'," +								// Coca-Cola Hellenic MLL		
	
		// ALR Competencies - Probably should have used KFLA competencies
		"'Business Insight - ALR'," +							// ALR; factor - 'Thought', cluster - 'Understanding the Business'
		"'Financial Acumen - ALR'," +							// ALR; factor - 'Thought', cluster - 'Understanding the Business'
		"'Manages Complexity - ALR'," +							// ALR; factor - 'Thought', cluster - 'Making Complex Decisions'
		"'Cultivates Innovation - ALR'," +						// ALR; factor - 'Thought', cluster - 'Creating the New and Different'
		"'Strategic Mindset - ALR'," +							// ALR; factor - 'Thought', cluster - 'Creating the New and Different'
		"'Plans and Aligns - ALR'," +							// ALR; factor - 'Results', cluster - 'Managing Execution'
		"'Ensures Accountability - ALR'," +						// ALR; factor - 'Results', cluster - 'Focusing on Performance'
		"'Drives Results - ALR'," +								// ALR; factor - 'Results', cluster - 'Focusing on Performance'
		"'Collaborates - ALR'," +								// ALR; factor - 'People', cluster - 'Building Collaborative Relationships'
		"'Develops Talent - ALR'," +							// ALR; factor - 'People', cluster - 'Optimizing Diverse Talent'
		"'Builds Effective Teams - ALR'," +						// ALR; factor - 'People', cluster - 'Optimizing Diverse Talent'
		"'Drives Engagement - ALR'," +							// ALR; factor - 'People', cluster - 'Influencing People'
		"'Persuades - ALR'," +									// ALR; factor - 'People', cluster - 'Influencing People'
		"'Courage - ALR'," +									// ALR; factor - 'Self', cluster - 'Being Authentic'
		"'Instills Trust - ALR'," +								// ALR; factor - 'Self', cluster - 'Being Authentic'
		"'Manages Ambiguity - ALR'," +							// ALR; factor - 'Self', cluster - 'Being Flexible and Adaptable'
		// There are two additional competencies that were not in the first model setup.  Factor assessments are a guess and the cluster assignments are unkown at the time of this load.
		"'Taking Initiative - ALR'," +							// ALR; factor - 'Reults' (?), cluster - (?)
		"'Being Open - ALR'," +										// ALR; factor - 'Self' (?), cluster - (?)

		// ALR CEO scoring.  Competency name and test scoring are different from the other ALR
		"'Customer Focus - CEO'," +								// ALR; factor - 'Thought'
		"'Financial Acumen - CEO'," +							// ALR; factor - 'Thought'
		"'Manages Complexity - CEO'," +							// ALR; factor - 'Thought'
		"'Balances Stakeholders - CEO'," +						// ALR; factor - 'Thought'
		"'Global Perspective - CEO'," +							// ALR; factor - 'Thought'
		"'Cultivates Innovation - CEO'," +						// ALR; factor - 'Thought'
		"'Strategic Vision - CEO'," +							// ALR; factor - 'Thought'
		"'Aligns Execution - CEO'," +							// ALR; factor - 'Results'
		"'Ensures Accountability - CEO'," +						// ALR; factor - 'Results'
		"'Drives Results - CEO'," +								// ALR; factor - 'Results'
		"'Manages Conflict - CEO'," +							// ALR; factor - 'People'
		"'Navigates Networks - CEO'," +							// ALR; factor - 'People'
		"'Develops Talent - CEO'," +							// ALR; factor - 'People'
		"'Builds Effective Teams - CEO'," +						// ALR; factor - 'People'
		"'Communicates Effectively - CEO'," +					// ALR; factor - 'People'
		"'Engages and Inspires - CEO'," +						// ALR; factor - 'People'
		"'Persuades - CEO'," +									// ALR; factor - 'People'
		"'Courage - CEO'," +									// ALR; factor - 'Self'
		"'Instills Trust - CEO'," +								// ALR; factor - 'Self'
		"'Demonstrates Self-Awareness - CEO'," +				// ALR; factor - 'Self'
		"'Manages Ambiguity - CEO'," +							// ALR; factor - 'Self'
		"'Nimble Learning - CEO'," +							// ALR; factor - 'Self'
		"'Being Resilient - CEO'," +							// ALR; factor - 'Self'
		"'Situational Adaptability - CEO'";						// ALR; factor - 'Self'

	/*
	 * Map associating a bunch of information about the components for each
	 * competency that is derived from the cognitive tests and the GPI.
	 * 
	 * 1. competency internal name
	 * 2. component type (GPI or Cognitive)
	 * 3. spss id
	 * 4. isNegative (pos = false, neg = true, default = false)
	 */
	static private final Object[][] COMPETENCY_SPSS_MAP =  new Object[][]
 	{
		// ------------- MLL -------------
		//(new Object [] { "Make Sound Decisions",					"Cogs",	"RAVENSSF",	new Boolean(false) }),
		//(new Object [] { "Make Sound Decisions",					"Cogs",	"RAVENS_T",	new Boolean(false) }),
		(new Object [] { "Make Sound Decisions",				"Cogs",	"RAVENSB",	new Boolean(false) }),
		//(new Object [] { "Make Sound Decisions",					"Cogs", "WG_A",		new Boolean(false) }),
		//(new Object [] { "Make Sound Decisions",					"Cogs", "WG_C",		new Boolean(false) }),
		//(new Object [] { "Make Sound Decisions",					"Cogs", "WG_AS",	new Boolean(false) }),
		(new Object [] { "Make Sound Decisions",				"Cogs", "WGE_CT",	new Boolean(false) }),
		//(new Object [] { "Make Sound Decisions",					"Cogs", "WESMAN",	new Boolean(false) }),
		(new Object [] { "Make Sound Decisions",				"GPI",  "GPI_TF",	new Boolean(false) }),
		(new Object [] { "Make Sound Decisions",		 		"GPI",	"GPI_VIS",	new Boolean(false) }),
		(new Object [] { "Make Sound Decisions",				"GPI",  "GPI_AD",	new Boolean(true)  }),
		                                                          		
		//(new Object [] { "Act Strategically",	 					"Cogs", "RAVENSSF", new Boolean(false)  }),
		//(new Object [] { "Act Strategically",						"Cogs",	"RAVENS_T",	new Boolean(false) }),
		(new Object [] { "Act Strategically",					"Cogs",	"RAVENSB",	new Boolean(false) }),
		//(new Object [] { "Act Strategically",						"Cogs", "WG_A",		new Boolean(false) }),
		//(new Object [] { "Act Strategically",						"Cogs", "WG_C",		new Boolean(false) }),
		//(new Object [] { "Act Strategically",						"Cogs", "WG_AS", 	new Boolean(false)  }),
		(new Object [] { "Act Strategically",					"Cogs", "WGE_CT", 	new Boolean(false) }),
		//(new Object [] { "Act Strategically",						"Cogs", "WESMAN", 	new Boolean(false)  }),
		(new Object [] { "Act Strategically", 					"GPI", 	"GPI_TF", 	new Boolean(false) }),
		(new Object [] { "Act Strategically",			 		"GPI", 	"GPI_VIS", 	new Boolean(false) }),
		 		
		(new Object [] { "Think Creatively",	 				"GPI", "GPI_INOV", 	new Boolean(false) }),
		(new Object [] { "Think Creatively",					"GPI", "GPI_TF",  	new Boolean(false) }),
		(new Object [] { "Think Creatively", 					"GPI", "GPI_VIS", 	new Boolean(false) }),
		(new Object [] { "Think Creatively",					"GPI", "GPI_RISK", 	new Boolean(false) }),
		(new Object [] { "Think Creatively",		 			"GPI", "GPI_ADPT", 	new Boolean(false) }),
		
		(new Object [] { "Show Drive and Initiative",		 	"GPI", "GPI_WF", 	new Boolean(false) }),
		(new Object [] { "Show Drive and Initiative",	 		"GPI", "GPI_TC", 	new Boolean(false) }),
		(new Object [] { "Show Drive and Initiative",		 	"GPI", "GPI_EL", 	new Boolean(false) }),
		(new Object [] { "Show Drive and Initiative",	 		"GPI", "GPI_INIT", 	new Boolean(false) }),
		(new Object [] { "Show Drive and Initiative",	 		"GPI", "GPI_DACH", 	new Boolean(false) }),
		(new Object [] { "Show Drive and Initiative", 			"GPI", "GPI_COMP", 	new Boolean(false) }),
		(new Object [] { "Show Drive and Initiative",		 	"GPI", "GPI_RESP", 	new Boolean(false) }), 		
		
		(new Object [] { "Build Support", 						"GPI", "GPI_INFL", 	new Boolean(false) }), 
		(new Object [] { "Build Support",		 				"GPI", "GPI_SO", 	new Boolean(false) }), 
		(new Object [] { "Build Support", 						"GPI", "GPI_AST", 	new Boolean(false) }), 
		(new Object [] { "Build Support",				 		"GPI", "GPI_DACH", 	new Boolean(false) }), 
		(new Object [] { "Build Support",						"GPI", "GPI_INTI", 	new Boolean(true)  }), 
		
		(new Object [] { "Motivate Others",				 		"GPI", "GPI_INFL", 	new Boolean(false) }),
		(new Object [] { "Motivate Others",						"GPI", "GPI_SO", 	new Boolean(false) }),
		(new Object [] { "Motivate Others",						"GPI", "GPI_OPT", 	new Boolean(false) }),
		
		(new Object [] { "Promote Teamwork",					"GPI", "GPI_INTI", 	new Boolean(true)  }),
		(new Object [] { "Promote Teamwork",					"GPI", "GPI_MIC", 	new Boolean(true)  }),
		(new Object [] { "Promote Teamwork",					"GPI", "GPI_NA", 	new Boolean(true)  }),
		(new Object [] { "Promote Teamwork",					"GPI", "GPI_TR", 	new Boolean(false) }),
		(new Object [] { "Promote Teamwork",					"GPI", "GPI_IND", 	new Boolean(true)  }),
		
		(new Object [] { "Establish Relationships",				"GPI", "GPI_INTI", 	new Boolean(true)  }),
		(new Object [] { "Establish Relationships",				"GPI", "GPI_SO", 	new Boolean(false) }),
		(new Object [] { "Establish Relationships",				"GPI", "GPI_CONS", 	new Boolean(false) }),
		(new Object [] { "Establish Relationships",				"GPI", "GPI_EMP", 	new Boolean(false) }),
		(new Object [] { "Establish Relationships",				"GPI", "GPI_TR", 	new Boolean(false) }),
		(new Object [] { "Establish Relationships",				"GPI", "GPI_IND", 	new Boolean(true)  }),
		(new Object [] { "Establish Relationships",				"GPI", "GPI_NA", 	new Boolean(true)  }),
		
		(new Object [] { "Establish Trust",						"GPI", "GPI_EGO", 	new Boolean(true)  }),
		(new Object [] { "Establish Trust",						"GPI", "GPI_MAN", 	new Boolean(true)  }),
		(new Object [] { "Establish Trust",						"GPI", "GPI_INTI", 	new Boolean(true)  }),
		(new Object [] { "Establish Trust",						"GPI", "GPI_PA", 	new Boolean(true)  }),
		(new Object [] { "Establish Trust",						"GPI", "GPI_DUT", 	new Boolean(false) }),
		
		(new Object [] { "Show Adaptability",					"GPI", "GPI_ADPT", 	new Boolean(false) }),
		(new Object [] { "Show Adaptability",					"GPI", "GPI_NA", 	new Boolean(true)  }),
		(new Object [] { "Show Adaptability",			 		"GPI", "GPI_OPT", 	new Boolean(false) }),
		(new Object [] { "Show Adaptability",					"GPI", "GPI_EC", 	new Boolean(false) }),
		(new Object [] { "Show Adaptability",					"GPI", "GPI_ST", 	new Boolean(false) }),
		
		(new Object [] { "Humility - MOHG",						"GPI", "GPI_EGO", 	new Boolean(true)  }),
		(new Object [] { "Humility - MOHG",						"GPI", "GPI_MAN", 	new Boolean(true)  }),
		(new Object [] { "Humility - MOHG",						"GPI", "GPI_CONS", 	new Boolean(false) }),
		(new Object [] { "Humility - MOHG",						"GPI", "GPI_OPEN", 	new Boolean(false) }),


		// ------------- BUL -------------
		//(new Object [] { "Use Insightful Judgment",				"Cogs", "RAVENSSF",	new Boolean(false)  }),
		//(new Object [] { "Use Insightful Judgment",				"Cogs",	"RAVENS_T",	new Boolean(false) }),
		(new Object [] { "Use Insightful Judgment",				"Cogs",	"RAVENSB",	new Boolean(false) }),
		//(new Object [] { "Use Insightful Judgment",				"Cogs", "WG_A",		new Boolean(false) }),
		//(new Object [] { "Use Insightful Judgment",				"Cogs", "WG_C",		new Boolean(false) }),
		//(new Object [] { "Use Insightful Judgment",				"Cogs", "WG_AS", 	new Boolean(false)  }),
		(new Object [] { "Use Insightful Judgment",				"Cogs", "WGE_CT", 	new Boolean(false) }),
		//(new Object [] { "Use Insightful Judgment",				"Cogs", "WESMAN", 	new Boolean(false)  }),
		
		//(new Object [] { "Think Strategically",					"Cogs", "RAVENSSF",	new Boolean(false)  }),
		//(new Object [] { "Think Strategically",					"Cogs",	"RAVENS_T",	new Boolean(false) }),
		(new Object [] { "Think Strategically",					"Cogs",	"RAVENSB",	new Boolean(false) }),
		//(new Object [] { "Think Strategically",					"Cogs", "WG_A",		new Boolean(false) }),
		//(new Object [] { "Think Strategically",					"Cogs", "WG_C",		new Boolean(false) }),
		//(new Object [] { "Think Strategically",					"Cogs", "WG_AS", 	new Boolean(false)  }),
		(new Object [] { "Think Strategically",					"Cogs", "WGE_CT", 	new Boolean(false) }),
		//(new Object [] { "Think Strategically",					"Cogs", "WESMAN", 	new Boolean(false)  }),
		(new Object [] { "Think Strategically",					"GPI", "GPI_TF", 	new Boolean(false) }),
		(new Object [] { "Think Strategically",					"GPI", "GPI_VIS", 	new Boolean(false) }),
		(new Object [] { "Think Strategically",					"GPI", "GPI_AD", 	new Boolean(true)  }),
		
		(new Object [] { "Innovate",							"GPI", "GPI_INOV",	new Boolean(false) }),
		(new Object [] { "Innovate",							"GPI", "GPI_TF", 	new Boolean(false) }),
		(new Object [] { "Innovate", 							"GPI", "GPI_VIS", 	new Boolean(false) }),
		
		(new Object [] { "Influence Others",					"GPI", "GPI_TF", 	new Boolean(false) }),
		(new Object [] { "Influence Others",	 				"GPI", "GPI_INFL", 	new Boolean(false) }),
		(new Object [] { "Influence Others", 					"GPI", "GPI_SO", 	new Boolean(false) }),
		
		(new Object [] { "Engage and Inspire",					"GPI", "GPI_INFL", 	new Boolean(false) }),
		(new Object [] { "Engage and Inspire",					"GPI", "GPI_SO", 	new Boolean(false) }),
		(new Object [] { "Engage and Inspire",			 		"GPI", "GPI_OPT", 	new Boolean(false) }),
		
		(new Object [] { "Build Relationships",					"GPI", "GPI_INTI", 	new Boolean(true)  }),
		(new Object [] { "Build Relationships",					"GPI", "GPI_SO", 	new Boolean(false) }),
		(new Object [] { "Build Relationships",					"GPI", "GPI_CONS", 	new Boolean(false) }),
		(new Object [] { "Build Relationships",					"GPI", "GPI_EMP", 	new Boolean(false) }),
		(new Object [] { "Build Relationships",					"GPI", "GPI_TR", 	new Boolean(false) }),
		
		(new Object [] { "Drive for Results",					"GPI", "GPI_WF", 	new Boolean(false) }),
		(new Object [] { "Drive for Results",					"GPI", "GPI_TC", 	new Boolean(false) }),
		(new Object [] { "Drive for Results",					"GPI", "GPI_EL", 	new Boolean(false) }),
		(new Object [] { "Drive for Results",					"GPI", "GPI_INIT", 	new Boolean(false) }),
		(new Object [] { "Drive for Results",					"GPI", "GPI_DACH", 	new Boolean(false) }),
		(new Object [] { "Drive for Results",					"GPI", "GPI_COMP", 	new Boolean(false) }),
		
		(new Object [] { "Lead Courageously",					"GPI", "GPI_TC", 	new Boolean(false) }),
		(new Object [] { "Lead Courageously",					"GPI", "GPI_INFL", 	new Boolean(false) }),
		(new Object [] { "Lead Courageously",					"GPI", "GPI_INIT", 	new Boolean(false) }),
		(new Object [] { "Lead Courageously",					"GPI", "GPI_DACH", 	new Boolean(false) }),
		(new Object [] { "Lead Courageously",					"GPI", "GPI_RISK", 	new Boolean(false) }),
		
		(new Object [] { "Inspire Trust",						"GPI", "GPI_EGO", 	new Boolean(true)  }),
		(new Object [] { "Inspire Trust",						"GPI", "GPI_MAN", 	new Boolean(true)  }),
		(new Object [] { "Inspire Trust",						"GPI", "GPI_INTI", 	new Boolean(true)  }),
		(new Object [] { "Inspire Trust",						"GPI", "GPI_PA", 	new Boolean(true)  }),
		(new Object [] { "Inspire Trust",						"GPI", "GPI_DUT", 	new Boolean(false) }),
		
		(new Object [] { "Adapt and Learn",						"GPI", "GPI_ADPT", 	new Boolean(false) }),
		(new Object [] { "Adapt and Learn",						"GPI", "GPI_NA", 	new Boolean(true)  }),
		(new Object [] { "Adapt and Learn",						"GPI", "GPI_OPT",	new Boolean(false) }),
		(new Object [] { "Adapt and Learn",						"GPI", "GPI_EC", 	new Boolean(false) }),
		(new Object [] { "Adapt and Learn",						"GPI", "GPI_ST", 	new Boolean(false) }),
		(new Object [] { "Adapt and Learn",						"GPI", "GPI_SASI", 	new Boolean(false) }),
		
		// Same calcs as the one in MLL, but we are separating them just in case something changes in
		// the scoring (and to maintain parallelism with the calculation definitions that already exist)
		(new Object [] { "Humility - MOHG BUL",					"GPI", "GPI_EGO", 	new Boolean(true)  }),
		(new Object [] { "Humility - MOHG BUL",					"GPI", "GPI_MAN", 	new Boolean(true)  }),
		(new Object [] { "Humility - MOHG BUL",					"GPI", "GPI_CONS", 	new Boolean(false) }),
		(new Object [] { "Humility - MOHG BUL",					"GPI", "GPI_OPEN", 	new Boolean(false) }),
		
		// Phillips 66 has a combination of "standard" competencies (scvored the same as the baseline BUL)
		// and a number of additional BUL competencies unique to them, which follow
		(new Object [] { "Foster Novel Thinking - P66 BUL",		"GPI", "GPI_INOV", 	new Boolean(false) }),
		(new Object [] { "Foster Novel Thinking - P66 BUL",		"GPI", "GPI_TF", 	new Boolean(false) }),
		(new Object [] { "Foster Novel Thinking - P66 BUL",		"GPI", "GPI_VIS", 	new Boolean(false) }),

		(new Object [] { "Foster Collaborative Relationships - P66 BUL",	"GPI", "GPI_SO", 	new Boolean(false) }),
		(new Object [] { "Foster Collaborative Relationships - P66 BUL",	"GPI", "GPI_INTI", 	new Boolean(true) }),
		(new Object [] { "Foster Collaborative Relationships - P66 BUL",	"GPI", "GPI_CONS", 	new Boolean(false) }),
		(new Object [] { "Foster Collaborative Relationships - P66 BUL",	"GPI", "GPI_EMP", 	new Boolean(false) }),
		(new Object [] { "Foster Collaborative Relationships - P66 BUL",	"GPI", "GPI_TR", 	new Boolean(false) }),
		
		(new Object [] { "Demonstrate Agility - P66 BUL",		"GPI", "GPI_ADPT", 	new Boolean(false) }),
		(new Object [] { "Demonstrate Agility - P66 BUL",		"GPI", "GPI_NA", 	new Boolean(true) }),
		(new Object [] { "Demonstrate Agility - P66 BUL",		"GPI", "GPI_OPT", 	new Boolean(false) }),
		(new Object [] { "Demonstrate Agility - P66 BUL",		"GPI", "GPI_EC", 	new Boolean(false) }),
		(new Object [] { "Demonstrate Agility - P66 BUL",		"GPI", "GPI_ST", 	new Boolean(false) }),
		(new Object [] { "Demonstrate Agility - P66 BUL",		"GPI", "GPI_SASI", 	new Boolean(false) }),


		// ------------- SEA -------------
		//(new Object [] { "Use Astute Judgment",					"Cogs", "RAVENSSF",	new Boolean(false)  }),
		//(new Object [] { "Use Astute Judgment",					"Cogs", "RAVENS_T",	new Boolean(false)  }),
		(new Object [] { "Use Astute Judgment",					"Cogs", "RAVENSB",	new Boolean(false) }),
		//(new Object [] { "Use Astute Judgment",					"Cogs", "WG_A",		new Boolean(false)  }),
		//(new Object [] { "Use Astute Judgment",					"Cogs", "WG_C",		new Boolean(false)  }),
		//(new Object [] { "Use Astute Judgment",					"Cogs", "WG_AS",	new Boolean(false)  }),
		(new Object [] { "Use Astute Judgment",					"Cogs", "WGE_CT",	new Boolean(false) }),
		(new Object [] { "Use Astute Judgment",					"GPI", "GPI_INOV",	new Boolean(false) }),
		(new Object [] { "Use Astute Judgment",					"GPI", "GPI_TF",	new Boolean(false) }),
		
		(new Object [] { "Shape Strategy",						"GPI", "GPI_TF",	new Boolean(false) }),
		(new Object [] { "Shape Strategy",						"GPI", "GPI_VIS",	new Boolean(false) }),
		//(new Object [] { "Shape Strategy",						"Cogs", "RAVENSSF",	new Boolean(false)  }),
		//(new Object [] { "Shape Strategy",						"Cogs",	"RAVENS_T",	new Boolean(false) }),
		(new Object [] { "Shape Strategy",						"Cogs",	"RAVENSB",	new Boolean(false) }),
		//(new Object [] { "Shape Strategy",						"Cogs", "WG_A",		new Boolean(false) }),
		//(new Object [] { "Shape Strategy",						"Cogs", "WG_C",		new Boolean(false) }),
		//(new Object [] { "Shape Strategy",		 				"Cogs", "WG_AS", 	new Boolean(false)  }),
		(new Object [] { "Shape Strategy",				 		"Cogs", "WGE_CT",	new Boolean(false) }),
		
		(new Object [] { "Display Vision",						"GPI", "GPI_INOV",	new Boolean(false) }),
		(new Object [] { "Display Vision",						"GPI", "GPI_TF",	new Boolean(false) }),
		(new Object [] { "Display Vision",						"GPI", "GPI_VIS",	new Boolean(false) }),
		//(new Object [] { "Display Vision",						"Cogs", "RAVENSSF",	new Boolean(false)  }),
		//(new Object [] { "Display Vision",						"Cogs", "RAVENS_T",	new Boolean(false)  }),
		(new Object [] { "Display Vision",						"Cogs", "RAVENSB",	new Boolean(false) }),
		//(new Object [] { "Display Vision",						"Cogs", "WG_A",		new Boolean(false)  }),
		//(new Object [] { "Display Vision",						"Cogs", "WG_C",		new Boolean(false)  }),
		//(new Object [] { "Display Vision",						"Cogs", "WG_AS",	new Boolean(false)  }),
		(new Object [] { "Display Vision",						"Cogs", "WGE_CT",	new Boolean(false) }),

		(new Object [] { "Drive Organizational Success",		"GPI", "GPI_TC",	new Boolean(false) }),
		(new Object [] { "Drive Organizational Success",		"GPI", "GPI_EL",	new Boolean(false) }),
		(new Object [] { "Drive Organizational Success",		"GPI", "GPI_INIT",	new Boolean(false) }),
		(new Object [] { "Drive Organizational Success",		"GPI", "GPI_DACH",	new Boolean(false) }),

		(new Object [] { "Lead Boldly",							"GPI", "GPI_TC",	new Boolean(false) }),
		(new Object [] { "Lead Boldly",							"GPI", "GPI_INFL",	new Boolean(false) }),
		(new Object [] { "Lead Boldly",							"GPI", "GPI_INIT",	new Boolean(false) }),
		(new Object [] { "Lead Boldly",							"GPI", "GPI_DACH",	new Boolean(false) }),
		(new Object [] { "Lead Boldly",							"GPI", "GPI_RISK",	new Boolean(false) }),

		(new Object [] { "Use Organizational Influence",		"GPI", "GPI_TF",	new Boolean(false) }),
		(new Object [] { "Use Organizational Influence",		"GPI", "GPI_INFL",	new Boolean(false) }),

		(new Object [] { "Energize the Organization",			"GPI", "GPI_INFL",	new Boolean(false) }),
		(new Object [] { "Energize the Organization",			"GPI", "GPI_OPT",	new Boolean(false) }),

		(new Object [] { "Earn Unwavering Trust",				"GPI", "GPI_EGO",	new Boolean(true)  }),
		(new Object [] { "Earn Unwavering Trust",				"GPI", "GPI_MAN",	new Boolean(true)  }),
		(new Object [] { "Earn Unwavering Trust",				"GPI", "GPI_INTI",	new Boolean(true)  }),
		(new Object [] { "Earn Unwavering Trust",				"GPI", "GPI_PA",	new Boolean(true)  }),
		(new Object [] { "Earn Unwavering Trust",				"GPI", "GPI_DUT",	new Boolean(false) }),

		(new Object [] { "Demonstrate Agility",					"GPI", "GPI_ADPT",	new Boolean(false) }),
		(new Object [] { "Demonstrate Agility",					"GPI", "GPI_NA",	new Boolean(true)  }),
		(new Object [] { "Demonstrate Agility",					"GPI", "GPI_ST",	new Boolean(false) }),


		// ------------- FLL -------------
		(new Object [] { "Analyze Issues and Solve Problems",	"Cogs",	"RAVENSB",	new Boolean(false) }),
		(new Object [] { "Analyze Issues and Solve Problems",	"Cogs", "WGE_CT",	new Boolean(false) }),
		(new Object [] { "Analyze Issues and Solve Problems",	"GPI", "GPI_TF",	new Boolean(false) }),
		(new Object [] { "Analyze Issues and Solve Problems",	"GPI", "GPI_VIS",	new Boolean(false) }),
		
		(new Object [] { "Understand Strategies",				"Cogs",	"RAVENSB",	new Boolean(false) }),
		(new Object [] { "Understand Strategies",				"Cogs", "WGE_CT",	new Boolean(false) }),
		(new Object [] { "Understand Strategies",				"GPI", "GPI_TF",	new Boolean(false) }),
		(new Object [] { "Understand Strategies",				"GPI", "GPI_VIS",	new Boolean(false) }),
		
		(new Object [] { "Identify Improvements",				"GPI", "GPI_TA",	new Boolean(false) }),
		(new Object [] { "Identify Improvements",				"GPI", "GPI_INOV",	new Boolean(false) }),
		(new Object [] { "Identify Improvements",				"GPI", "GPI_RISK",	new Boolean(false) }),
		(new Object [] { "Identify Improvements",				"GPI", "GPI_ADPT",	new Boolean(false) }),
		
		(new Object [] { "Seek Customer Satisfaction",			"GPI", "GPI_INFL",	new Boolean(false) }),
		(new Object [] { "Seek Customer Satisfaction",			"GPI", "GPI_WF",	new Boolean(false) }),
		(new Object [] { "Seek Customer Satisfaction",			"GPI", "GPI_SO",	new Boolean(false) }),
		(new Object [] { "Seek Customer Satisfaction",			"GPI", "GPI_RESP",	new Boolean(false) }),
		
		// Establish Plans - No testing
		
		(new Object [] { "Execute Efficiently",					"GPI", "GPI_TC",	new Boolean(false) }),
		(new Object [] { "Execute Efficiently",					"GPI", "GPI_EL",	new Boolean(false) }),
		(new Object [] { "Execute Efficiently",					"GPI", "GPI_DACH",	new Boolean(false) }),
		(new Object [] { "Execute Efficiently",					"GPI", "GPI_RESP",	new Boolean(false) }),
		
		// Show Initiative - No testing
		
		(new Object [] { "Solicit Support",						"GPI", "GPI_INFL",	new Boolean(false) }),
		(new Object [] { "Solicit Support",						"GPI", "GPI_AST",	new Boolean(false) }),
		(new Object [] { "Solicit Support",						"GPI", "GPI_DACH",	new Boolean(false) }),
		(new Object [] { "Solicit Support",						"GPI", "GPI_INTI",	new Boolean(true)  }),

		(new Object [] { "Encourage Commitment",				"GPI", "GPI_INFL",	new Boolean(false) }),
		(new Object [] { "Encourage Commitment",				"GPI", "GPI_SO",	new Boolean(false) }),
		(new Object [] { "Encourage Commitment",				"GPI", "GPI_OPT",	new Boolean(false) }),

		// Select and Develop - No testing
		
		// Communicate Effectively - No testing
		
		(new Object [] { "Relate Well to Others",				"GPI", "GPI_EGO",	new Boolean(true)  }),
		(new Object [] { "Relate Well to Others",				"GPI", "GPI_MAN",	new Boolean(true)  }),
		(new Object [] { "Relate Well to Others",				"GPI", "GPI_INTI",	new Boolean(true)  }),
		(new Object [] { "Relate Well to Others",				"GPI", "GPI_PA",	new Boolean(true)  }),
		(new Object [] { "Relate Well to Others",				"GPI", "GPI_DUT",	new Boolean(false) }),
		
		// Demonstrate Credibility - No testing
		
		(new Object [] { "Readily Adapt",						"GPI", "GPI_ADPT",	new Boolean(false) }),
		(new Object [] { "Readily Adapt",						"GPI", "GPI_NA",	new Boolean(true)  }),
		(new Object [] { "Readily Adapt",						"GPI", "GPI_OPT",	new Boolean(false) }),
		(new Object [] { "Readily Adapt",						"GPI", "GPI_EC",	new Boolean(false) }),
		(new Object [] { "Readily Adapt",						"GPI", "GPI_ST",	new Boolean(false) }),
		
		(new Object [] { "Learn and Grow",						"Cogs",	"RAVENSB",	new Boolean(false) }),
		(new Object [] { "Learn and Grow",						"GPI", "GPI_TA",	new Boolean(false) }),
		(new Object [] { "Learn and Grow",						"GPI", "GPI_OPEN",	new Boolean(false) }),
		(new Object [] { "Learn and Grow",						"GPI", "GPI_ADPT",	new Boolean(false) }),
		(new Object [] { "Learn and Grow",						"GPI", "GPI_SASI",	new Boolean(false) }),
		(new Object [] { "Learn and Grow",						"GPI", "GPI_EGO",	new Boolean(true)  }),

		(new Object [] { "Act with Integrity",					"GPI", "GPI_MAN",	new Boolean(true)  }),
		(new Object [] { "Act with Integrity",					"GPI", "GPI_PA",	new Boolean(true)  }),
		(new Object [] { "Act with Integrity",					"GPI", "GPI_TR",	new Boolean(false) }),
		(new Object [] { "Act with Integrity",					"GPI", "GPI_EL",	new Boolean(false) }),
		(new Object [] { "Act with Integrity",					"GPI", "GPI_DUT",	new Boolean(false) }),

		// SEA competencies Reworked for no WG in cogs scoring
		(new Object [] { "Use Astute Judgement - NoWG",			"Cogs", "RAVENSB",	new Boolean(false) }),
		
		(new Object [] { "Shape Strategy - NoWG",				"GPI", "GPI_TF",	new Boolean(false) }),
		(new Object [] { "Shape Strategy - NoWG",				"GPI", "GPI_VIS",	new Boolean(false) }),
		(new Object [] { "Shape Strategy - NoWG",				"Cogs",	"RAVENSB",	new Boolean(false) }),
		
		(new Object [] { "Display Vision - NoWG",				"GPI", "GPI_INOV",	new Boolean(false) }),
		(new Object [] { "Display Vision - NoWG",				"GPI", "GPI_TF",	new Boolean(false) }),
		(new Object [] { "Display Vision - NoWG",				"GPI", "GPI_VIS",	new Boolean(false) }),
		(new Object [] { "Display Vision - NoWG",				"Cogs", "RAVENSB",	new Boolean(false) }),

		// ------------- FLL II -------------
		(new Object [] { "Analyze Issues and Solve Problems - II",	"Cogs",	"RAVENSB",	new Boolean(false) }),
		(new Object [] { "Analyze Issues and Solve Problems - II",	"Cogs", "WGE_CT",	new Boolean(false) }),
		(new Object [] { "Analyze Issues and Solve Problems - II",	"GPI", "GPI_TF",	new Boolean(false) }),
		(new Object [] { "Analyze Issues and Solve Problems - II",	"GPI", "GPI_VIS",	new Boolean(false) }),

		(new Object [] { "Understand Strategies - II",			"Cogs",	"RAVENSB",	new Boolean(false) }),
		(new Object [] { "Understand Strategies - II",			"Cogs", "WGE_CT",	new Boolean(false) }),
		(new Object [] { "Understand Strategies - II",			"GPI", "GPI_TF",	new Boolean(false) }),
		(new Object [] { "Understand Strategies - II",			"GPI", "GPI_VIS",	new Boolean(false) }),
		
		(new Object [] { "Identify Improvements - II",			"GPI", "GPI_TA",	new Boolean(false) }),
		(new Object [] { "Identify Improvements - II",			"GPI", "GPI_INOV",	new Boolean(false) }),
		(new Object [] { "Identify Improvements - II",			"GPI", "GPI_RISK",	new Boolean(false) }),
		(new Object [] { "Identify Improvements - II",			"GPI", "GPI_ADPT",	new Boolean(false) }),
		
		(new Object [] { "Seek Customer Satisfaction - II",		"GPI", "GPI_INFL",	new Boolean(false) }),
		(new Object [] { "Seek Customer Satisfaction - II",		"GPI", "GPI_WF",	new Boolean(false) }),
		(new Object [] { "Seek Customer Satisfaction - II",		"GPI", "GPI_SO",	new Boolean(false) }),
		(new Object [] { "Seek Customer Satisfaction - II",		"GPI", "GPI_RESP",	new Boolean(false) }),
		
		// Establish Plans - II - No testing
		
		(new Object [] { "Execute Efficiently - II",			"GPI", "GPI_TC",	new Boolean(false) }),
		(new Object [] { "Execute Efficiently - II",			"GPI", "GPI_EL",	new Boolean(false) }),
		(new Object [] { "Execute Efficiently - II",			"GPI", "GPI_DACH",	new Boolean(false) }),
		(new Object [] { "Execute Efficiently - II",			"GPI", "GPI_RESP",	new Boolean(false) }),
		
		// Show Initiative - II - No testing
		
		(new Object [] { "Solicit Support - II",				"GPI", "GPI_INFL",	new Boolean(false) }),
		(new Object [] { "Solicit Support - II",				"GPI", "GPI_AST",	new Boolean(false) }),
		(new Object [] { "Solicit Support - II",				"GPI", "GPI_DACH",	new Boolean(false) }),
		(new Object [] { "Solicit Support - II",				"GPI", "GPI_INTI",	new Boolean(true)  }),

		(new Object [] { "Encourage Commitment - II",			"GPI", "GPI_INFL",	new Boolean(false) }),
		(new Object [] { "Encourage Commitment - II",			"GPI", "GPI_SO",	new Boolean(false) }),
		(new Object [] { "Encourage Commitment - II",			"GPI", "GPI_OPT",	new Boolean(false) }),

		// Select and Develop - II - No testing
		
		// Communicate Effectively - II - No testing
		
		(new Object [] { "Relate Well to Others - II",			"GPI", "GPI_EGO",	new Boolean(true)  }),
		(new Object [] { "Relate Well to Others - II",			"GPI", "GPI_MAN",	new Boolean(true)  }),
		(new Object [] { "Relate Well to Others - II",			"GPI", "GPI_INTI",	new Boolean(true)  }),
		(new Object [] { "Relate Well to Others - II",			"GPI", "GPI_PA",	new Boolean(true)  }),
		(new Object [] { "Relate Well to Others - II",			"GPI", "GPI_DUT",	new Boolean(false) }),
		
		// Demonstrate Credibility - II - No testing
		
		(new Object [] { "Readily Adapt - II",					"GPI", "GPI_ADPT",	new Boolean(false) }),
		(new Object [] { "Readily Adapt - II",					"GPI", "GPI_NA",	new Boolean(true)  }),
		(new Object [] { "Readily Adapt - II",					"GPI", "GPI_OPT",	new Boolean(false) }),
		(new Object [] { "Readily Adapt - II",					"GPI", "GPI_EC",	new Boolean(false) }),
		(new Object [] { "Readily Adapt - II",					"GPI", "GPI_ST",	new Boolean(false) }),

		(new Object [] { "Learn and Grow - II",					"GPI", "GPI_TA",	new Boolean(false) }),
		(new Object [] { "Learn and Grow - II",					"GPI", "GPI_OPEN",	new Boolean(false) }),
		(new Object [] { "Learn and Grow - II",					"GPI", "GPI_ADPT",	new Boolean(false) }),
		(new Object [] { "Learn and Grow - II",					"GPI", "GPI_SASI",	new Boolean(false) }),
		(new Object [] { "Learn and Grow - II",					"GPI", "GPI_EGO",	new Boolean(true)  }),

		(new Object [] { "Act with Integrity - II",				"GPI", "GPI_MAN",	new Boolean(true)  }),
		(new Object [] { "Act with Integrity - II",				"GPI", "GPI_PA",	new Boolean(true)  }),
		(new Object [] { "Act with Integrity - II",				"GPI", "GPI_TR",	new Boolean(false) }),
		(new Object [] { "Act with Integrity - II",				"GPI", "GPI_EL",	new Boolean(false) }),
		(new Object [] { "Act with Integrity - II",				"GPI", "GPI_DUT",	new Boolean(false) }),


		// ------------- Ericsson Executive -------------
		// Build Customer Orientation - EEA - No testing
		
		// Ensure Outstanding Performance - EEA - No testing per NHN-3022
		//(new Object [] { "Ensure Outstanding Performance - EEA",	"Cogs",	"RAVENSB",	new Boolean(false) }),
		//(new Object [] { "Ensure Outstanding Performance - EEA",	"GPI",	"GPI_DACH",	new Boolean(false) }),
		
		(new Object [] { "Shapes Strategy - EEA",					"Cogs",	"RAVENSB",	new Boolean(false) }),
		(new Object [] { "Shapes Strategy - EEA",					"GPI",	"GPI_INOV",	new Boolean(false) }),
		(new Object [] { "Shapes Strategy - EEA",					"GPI",	"GPI_TF",	new Boolean(false) }),
		(new Object [] { "Shapes Strategy - EEA",					"GPI",	"GPI_VIS",	new Boolean(false) }),
		(new Object [] { "Shapes Strategy - EEA",					"GPI",	"GPI_AD",	new Boolean(true) }),
		
		(new Object [] { "Creates a Culture of Innovation - EEA",	"Cogs",	"RAVENSB",	new Boolean(false) }),
		(new Object [] { "Creates a Culture of Innovation - EEA",	"GPI",	"GPI_INOV",	new Boolean(false) }),
		(new Object [] { "Creates a Culture of Innovation - EEA",	"GPI",	"GPI_TA",	new Boolean(false) }),
		(new Object [] { "Creates a Culture of Innovation - EEA",	"GPI",	"GPI_INIT",	new Boolean(false) }),
		(new Object [] { "Creates a Culture of Innovation - EEA",	"GPI",	"GPI_RISK",	new Boolean(false) }),

		(new Object [] { "Builds Capabilities - EEA",				"GPI",	"GPI_CONS",	new Boolean(false) }),
		(new Object [] { "Builds Capabilities - EEA",				"GPI",	"GPI_SO",	new Boolean(false) }),
		(new Object [] { "Builds Capabilities - EEA",				"GPI",	"GPI_TR",	new Boolean(false) }),
		(new Object [] { "Builds Capabilities - EEA",				"GPI",	"GPI_OPT",	new Boolean(false) }),
		
		(new Object [] { "Shapes View - EEA",						"GPI",	"GPI_INFL",	new Boolean(false) }),
		(new Object [] { "Shapes View - EEA",						"GPI",	"GPI_TC",	new Boolean(false) }),
		(new Object [] { "Shapes View - EEA",						"GPI",	"GPI_CONS",	new Boolean(false) }),
		(new Object [] { "Shapes View - EEA",						"GPI",	"GPI_EMP",	new Boolean(false) }),
		(new Object [] { "Shapes View - EEA",						"GPI",	"GPI_AST",	new Boolean(false) }),
		(new Object [] { "Shapes View - EEA",						"GPI",	"GPI_OPEN",	new Boolean(false) }),
		
		(new Object [] { "Removes Obstacles & Maintains Focus - EEA",	"GPI",	"GPI_TF",	new Boolean(false) }),
		(new Object [] { "Removes Obstacles & Maintains Focus - EEA",	"GPI",	"GPI_WF",	new Boolean(false) }),
		(new Object [] { "Removes Obstacles & Maintains Focus - EEA",	"GPI",	"GPI_INIT",	new Boolean(false) }),
		(new Object [] { "Removes Obstacles & Maintains Focus - EEA",	"GPI",	"GPI_TC",	new Boolean(false) }),
		(new Object [] { "Removes Obstacles & Maintains Focus - EEA",	"GPI",	"GPI_DACH",	new Boolean(false) }),
		
		(new Object [] { "Leads Across Boundaries - EEA",			"GPI",	"GPI_SO",	new Boolean(false) }),
		(new Object [] { "Leads Across Boundaries - EEA",			"GPI",	"GPI_CONS",	new Boolean(false) }),
		(new Object [] { "Leads Across Boundaries - EEA",			"GPI",	"GPI_OPEN",	new Boolean(false) }),
		(new Object [] { "Leads Across Boundaries - EEA",			"GPI",	"GPI_INTD",	new Boolean(false) }),
		(new Object [] { "Leads Across Boundaries - EEA",			"GPI",	"GPI_IND",	new Boolean(true) }),
		
		(new Object [] { "Pushes for Radical Change - EEA",			"GPI",	"GPI_TC",	new Boolean(false) }),
		(new Object [] { "Pushes for Radical Change - EEA",			"GPI",	"GPI_INIT",	new Boolean(false) }),
		(new Object [] { "Pushes for Radical Change - EEA",			"GPI",	"GPI_DACH",	new Boolean(false) }),
		(new Object [] { "Pushes for Radical Change - EEA",			"GPI",	"GPI_RISK",	new Boolean(false) }),
		(new Object [] { "Pushes for Radical Change - EEA",			"GPI",	"GPI_RESP",	new Boolean(false) }),
		(new Object [] { "Pushes for Radical Change - EEA",			"GPI",	"GPI_PA",	new Boolean(true) }),
		
		(new Object [] { "Thrives on Curiosity - EEA",				"GPI",	"GPI_TA",	new Boolean(false) }),
		(new Object [] { "Thrives on Curiosity - EEA",				"GPI",	"GPI_INOV",	new Boolean(false) }),
		(new Object [] { "Thrives on Curiosity - EEA",				"GPI",	"GPI_AST",	new Boolean(false) }),
		(new Object [] { "Thrives on Curiosity - EEA",				"GPI",	"GPI_OPEN",	new Boolean(false) }),
		
		// Learns and Grows - EEA - No testing
		
		(new Object [] { "Demonstrates Resilience - EEA",			"GPI",	"GPI_ADPT",	new Boolean(false) }),
		(new Object [] { "Demonstrates Resilience - EEA",			"GPI",	"GPI_OPT",	new Boolean(false) }),
		(new Object [] { "Demonstrates Resilience - EEA",			"GPI",	"GPI_EC",	new Boolean(false) }),
		(new Object [] { "Demonstrates Resilience - EEA",			"GPI",	"GPI_ST",	new Boolean(false) }),
		(new Object [] { "Demonstrates Resilience - EEA",			"GPI",	"GPI_DUT",	new Boolean(false) }),
		
		(new Object [] { "Speed, Quality, Integrity - EEA",			"GPI",	"GPI_PA",	new Boolean(true) }),
		(new Object [] { "Speed, Quality, Integrity - EEA",			"GPI",	"GPI_MAN",	new Boolean(true) }),
		(new Object [] { "Speed, Quality, Integrity - EEA",			"GPI",	"GPI_EMP",	new Boolean(false) }),
		(new Object [] { "Speed, Quality, Integrity - EEA",			"GPI",	"GPI_DUT",	new Boolean(false) }),


		// ------------- Ericsson EEA/EAC -------------
		// This is the same mapping for EEA/EAC 2
		(new Object [] { "Competence Drive - EEA/EAC",			"Cogs",	"RAVENSB",	new Boolean(false) }),
		(new Object [] { "Competence Drive - EEA/EAC",			"GPI",	"GPI_TA",	new Boolean(false) }),
		(new Object [] { "Competence Drive - EEA/EAC",			"GPI",	"GPI_TF",	new Boolean(false) }),
		(new Object [] { "Competence Drive - EEA/EAC",			"GPI",	"GPI_SASI",	new Boolean(false) }),
		(new Object [] { "Competence Drive - EEA/EAC",			"GPI",	"GPI_DACH",	new Boolean(false) }),

		(new Object [] { "Master Strategy - EEA/EAC",			"Cogs",	"RAVENSB",	new Boolean(false) }),
		(new Object [] { "Master Strategy - EEA/EAC",			"GPI",	"GPI_TA",	new Boolean(false) }),
		(new Object [] { "Master Strategy - EEA/EAC",			"GPI",	"GPI_TF",	new Boolean(false) }),
		(new Object [] { "Master Strategy - EEA/EAC",			"GPI",	"GPI_VIS",	new Boolean(false) }),
		(new Object [] { "Master Strategy - EEA/EAC",			"GPI",	"GPI_AD",	new Boolean(true) }),

		(new Object [] { "Driving Innovation - EEA/EAC",		"Cogs",	"RAVENSB",	new Boolean(false) }),
		(new Object [] { "Driving Innovation - EEA/EAC",		"GPI",	"GPI_TA",	new Boolean(false) }),
		(new Object [] { "Driving Innovation - EEA/EAC",		"GPI",	"GPI_INOV",	new Boolean(false) }),
		(new Object [] { "Driving Innovation - EEA/EAC",		"GPI",	"GPI_RISK",	new Boolean(false) }),
		(new Object [] { "Driving Innovation - EEA/EAC",		"GPI",	"GPI_OPT",	new Boolean(false) }),

		// Business Acumen - EEA/EAC has no testing
		
		(new Object [] { "Enabling People - EEA/EAC",			"GPI",	"GPI_SO",	new Boolean(false) }),
		(new Object [] { "Enabling People - EEA/EAC",			"GPI",	"GPI_TR",	new Boolean(false) }),
		(new Object [] { "Enabling People - EEA/EAC",			"GPI",	"GPI_CONS",	new Boolean(false) }),
		(new Object [] { "Enabling People - EEA/EAC",			"GPI",	"GPI_EMP",	new Boolean(false) }),
		(new Object [] { "Enabling People - EEA/EAC",			"GPI",	"GPI_AST",	new Boolean(false) }),
		(new Object [] { "Enabling People - EEA/EAC",			"GPI",	"GPI_SASI",	new Boolean(false) }),
		(new Object [] { "Enabling People - EEA/EAC",			"GPI",	"GPI_IND",	new Boolean(true) }),
		
		(new Object [] { "Courageous Leadership - EEA/EAC",		"GPI",	"GPI_TC",	new Boolean(false) }),
		(new Object [] { "Courageous Leadership - EEA/EAC",		"GPI",	"GPI_DACH",	new Boolean(false) }),
		(new Object [] { "Courageous Leadership - EEA/EAC",		"GPI",	"GPI_INFL",	new Boolean(false) }),
		(new Object [] { "Courageous Leadership - EEA/EAC",		"GPI",	"GPI_WF",	new Boolean(false) }),
		(new Object [] { "Courageous Leadership - EEA/EAC",		"GPI",	"GPI_OPT",	new Boolean(false) }),
		(new Object [] { "Courageous Leadership - EEA/EAC",		"GPI",	"GPI_MIC",	new Boolean(true) }),
		
		(new Object [] { "Uncompromising Integrity - EEA/EAC",	"GPI",	"GPI_DUT",	new Boolean(false) }),
		(new Object [] { "Uncompromising Integrity - EEA/EAC",	"GPI",	"GPI_EGO",	new Boolean(true) }),
		(new Object [] { "Uncompromising Integrity - EEA/EAC",	"GPI",	"GPI_MAN",	new Boolean(true) }),
		(new Object [] { "Uncompromising Integrity - EEA/EAC",	"GPI",	"GPI_INTI",	new Boolean(true) }),
		(new Object [] { "Uncompromising Integrity - EEA/EAC",	"GPI",	"GPI_PA",	new Boolean(true) }),

		// Customer Excellence - EEA/EAC has no testing

		(new Object [] { "Excelling Execution - EEA/EAC",		"GPI",	"GPI_TC",	new Boolean(false) }),
		(new Object [] { "Excelling Execution - EEA/EAC",		"GPI",	"GPI_RESP",	new Boolean(false) }),
		(new Object [] { "Excelling Execution - EEA/EAC",		"GPI",	"GPI_INIT",	new Boolean(false) }),
		(new Object [] { "Excelling Execution - EEA/EAC",		"GPI",	"GPI_DACH",	new Boolean(false) }),
		(new Object [] { "Excelling Execution - EEA/EAC",		"GPI",	"GPI_EL",	new Boolean(false) }),
		(new Object [] { "Excelling Execution - EEA/EAC",		"GPI",	"GPI_WF",	new Boolean(false) }),

		(new Object [] { "Embracing Change - EEA/EAC",			"GPI",	"GPI_OPEN",	new Boolean(false) }),
		(new Object [] { "Embracing Change - EEA/EAC",			"GPI",	"GPI_ADPT",	new Boolean(false) }),
		(new Object [] { "Embracing Change - EEA/EAC",			"GPI",	"GPI_AST",	new Boolean(false) }),
		(new Object [] { "Embracing Change - EEA/EAC",			"GPI",	"GPI_ST",	new Boolean(false) }),
		(new Object [] { "Embracing Change - EEA/EAC",			"GPI",	"GPI_NA",	new Boolean(true) }),

		// ------------- Ericsson L1 -------------
		(new Object [] { "Focuses on Customer Needs",			"GPI", "GPI_INFL",	new Boolean(false) }),
		(new Object [] { "Focuses on Customer Needs",			"GPI", "GPI_WF",	new Boolean(false) }),
		(new Object [] { "Focuses on Customer Needs",			"GPI", "GPI_SO",	new Boolean(false) }),
		(new Object [] { "Focuses on Customer Needs",			"GPI", "GPI_RESP",	new Boolean(false) }),
		
		// Demands Excellence - No testing
		
		(new Object [] { "Outside-In Thinking",					"Cogs",	"RAVENSB",	new Boolean(false) }),
		(new Object [] { "Outside-In Thinking",					"Cogs",	"WGE_CT",	new Boolean(false) }),
		(new Object [] { "Outside-In Thinking",					"GPI",	"GPI_TF",	new Boolean(false) }),
		(new Object [] { "Outside-In Thinking",					"GPI",	"GPI_VIS",	new Boolean(false) }),
		
		(new Object [] { "Embraces New Ideas",					"GPI",	"GPI_TA",	new Boolean(false) }),
		(new Object [] { "Embraces New Ideas",					"GPI",	"GPI_INOV",	new Boolean(false) }),
		(new Object [] { "Embraces New Ideas",					"GPI",	"GPI_RISK",	new Boolean(false) }),
		(new Object [] { "Embraces New Ideas",					"GPI",	"GPI_ADPT",	new Boolean(false) }),
		
		(new Object [] { "Develops People",						"GPI",	"GPI_INFL",	new Boolean(false) }),
		(new Object [] { "Develops People",						"GPI",	"GPI_SO",	new Boolean(false) }),
		(new Object [] { "Develops People",						"GPI",	"GPI_OPT",	new Boolean(false) }),
		
		// Listens and Shares Information - No testing
		
		(new Object [] { "Prioritizes",							"GPI",	"GPI_TC",	new Boolean(false) }),
		(new Object [] { "Prioritizes",							"GPI",	"GPI_EL",	new Boolean(false) }),
		(new Object [] { "Prioritizes",							"GPI",	"GPI_DACH",	new Boolean(false) }),
		(new Object [] { "Prioritizes",							"GPI",	"GPI_RESP",	new Boolean(false) }),
		
		// Leverages Diversity - No testing
		
		(new Object [] { "Learns and Grows",					"Cogs",	"RAVENSB",	new Boolean(false) }),
		(new Object [] { "Learns and Grows",					"GPI",	"GPI_TA",	new Boolean(false) }),
		(new Object [] { "Learns and Grows",					"GPI",	"GPI_OPEN",	new Boolean(false) }),
		(new Object [] { "Learns and Grows",					"GPI",	"GPI_ADPT",	new Boolean(false) }),
		(new Object [] { "Learns and Grows",					"GPI",	"GPI_SASI",	new Boolean(false) }),
		(new Object [] { "Learns and Grows",					"GPI",	"GPI_EGO",	new Boolean(true)  }),
		
		(new Object [] { "Shows Flexibility",					"GPI",	"GPI_ADPT",	new Boolean(false) }),
		(new Object [] { "Shows Flexibility",					"GPI",	"GPI_NA",	new Boolean(true)  }),
		(new Object [] { "Shows Flexibility",					"GPI",	"GPI_OPT",	new Boolean(false) }),
		(new Object [] { "Shows Flexibility",					"GPI",	"GPI_EC",	new Boolean(false) }),
		(new Object [] { "Shows Flexibility",					"GPI",	"GPI_ST",	new Boolean(false) }),
		
		(new Object [] { "Acts with Integrity",					"GPI",	"GPI_MAN",	new Boolean(true)  }),
		(new Object [] { "Acts with Integrity",					"GPI",	"GPI_PA",	new Boolean(true)  }),
		(new Object [] { "Acts with Integrity",					"GPI",	"GPI_TR",	new Boolean(false) }),
		(new Object [] { "Acts with Integrity",					"GPI",	"GPI_EL",	new Boolean(false) }),
		(new Object [] { "Acts with Integrity",					"GPI",	"GPI_DUT",	new Boolean(false) }),


		// ------------- Ericsson L3 -------------
			// Anticipates Customer Needs - EL3 - No testing
		
		//Creates Business Value - EL3 - No testing (per NHN-3020)
		//(new Object [] { "Creates Business Value - EL3",		"Cogs",	"RAVENSB",	new Boolean(false) }),
		//(new Object [] { "Creates Business Value - EL3",		"GPI",	"GPI_DACH",	new Boolean(false) }),
		
		(new Object [] { "Demonstrates Foresight - EL3",		"Cogs",	"RAVENSB",	new Boolean(false) }),
		(new Object [] { "Demonstrates Foresight - EL3",		"GPI",	"GPI_INOV",	new Boolean(false) }),
		(new Object [] { "Demonstrates Foresight - EL3",		"GPI",	"GPI_TF",	new Boolean(false) }),
		(new Object [] { "Demonstrates Foresight - EL3",		"GPI",	"GPI_VIS",	new Boolean(false) }),
		(new Object [] { "Demonstrates Foresight - EL3",		"GPI",	"GPI_AD",	new Boolean(true) }),
		
		(new Object [] { "Demonstrates Entrepreneurship - EL3",	"Cogs",	"RAVENSB",	new Boolean(false) }),
		(new Object [] { "Demonstrates Entrepreneurship - EL3",	"GPI",	"GPI_INOV",	new Boolean(false) }),
		(new Object [] { "Demonstrates Entrepreneurship - EL3",	"GPI",	"GPI_TA",	new Boolean(false) }),
		(new Object [] { "Demonstrates Entrepreneurship - EL3",	"GPI",	"GPI_INIT",	new Boolean(false) }),
		(new Object [] { "Demonstrates Entrepreneurship - EL3",	"GPI",	"GPI_RISK",	new Boolean(false) }),
		
		(new Object [] { "Ensures Organizational Commitment - EL3",	"GPI",	"GPI_CONS",	new Boolean(false) }),
		(new Object [] { "Ensures Organizational Commitment - EL3",	"GPI",	"GPI_SO",	new Boolean(false) }),
		(new Object [] { "Ensures Organizational Commitment - EL3",	"GPI",	"GPI_TR",	new Boolean(false) }),
		(new Object [] { "Ensures Organizational Commitment - EL3",	"GPI",	"GPI_OPT",	new Boolean(false) }),
		
		(new Object [] { "Ensures Impact - EL3",				"GPI",	"GPI_INFL",	new Boolean(false) }),
		(new Object [] { "Ensures Impact - EL3",				"GPI",	"GPI_TC",	new Boolean(false) }),
		(new Object [] { "Ensures Impact - EL3",				"GPI",	"GPI_CONS",	new Boolean(false) }),
		(new Object [] { "Ensures Impact - EL3",				"GPI",	"GPI_EMP",	new Boolean(false) }),
		(new Object [] { "Ensures Impact - EL3",				"GPI",	"GPI_AST",	new Boolean(false) }),
		(new Object [] { "Ensures Impact - EL3",				"GPI",	"GPI_OPEN",	new Boolean(false) }),
		
		(new Object [] { "Simplifies - EL3",					"GPI",	"GPI_TF",	new Boolean(false) }),
		(new Object [] { "Simplifies - EL3",					"GPI",	"GPI_WF",	new Boolean(false) }),
		(new Object [] { "Simplifies - EL3",					"GPI",	"GPI_INIT",	new Boolean(false) }),
		(new Object [] { "Simplifies - EL3",					"GPI",	"GPI_TC",	new Boolean(false) }),
		(new Object [] { "Simplifies - EL3",					"GPI",	"GPI_DACH",	new Boolean(false) }),
		
		(new Object [] { "Drives Organizational Alignment - EL3",	"GPI",	"GPI_SO",	new Boolean(false) }),
		(new Object [] { "Drives Organizational Alignment - EL3",	"GPI",	"GPI_CONS",	new Boolean(false) }),
		(new Object [] { "Drives Organizational Alignment - EL3",	"GPI",	"GPI_OPEN",	new Boolean(false) }),
		(new Object [] { "Drives Organizational Alignment - EL3",	"GPI",	"GPI_INTD",	new Boolean(false) }),
		(new Object [] { "Drives Organizational Alignment - EL3",	"GPI",	"GPI_IND",	new Boolean(true) }),
		
		(new Object [] { "Champions Change - EL3",				"GPI",	"GPI_TC",	new Boolean(false) }),
		(new Object [] { "Champions Change - EL3",				"GPI",	"GPI_INIT",	new Boolean(false) }),
		(new Object [] { "Champions Change - EL3",				"GPI",	"GPI_DACH",	new Boolean(false) }),
		(new Object [] { "Champions Change - EL3",				"GPI",	"GPI_RISK",	new Boolean(false) }),
		(new Object [] { "Champions Change - EL3",				"GPI",	"GPI_RESP",	new Boolean(false) }),
		(new Object [] { "Champions Change - EL3",				"GPI",	"GPI_PA",	new Boolean(true) }),
		
		(new Object [] { "Thrives on Curiosity - EL3",			"GPI",	"GPI_TA",	new Boolean(false) }),
		(new Object [] { "Thrives on Curiosity - EL3",			"GPI",	"GPI_INOV",	new Boolean(false) }),
		(new Object [] { "Thrives on Curiosity - EL3",			"GPI",	"GPI_AST",	new Boolean(false) }),
		(new Object [] { "Thrives on Curiosity - EL3",			"GPI",	"GPI_OPEN",	new Boolean(false) }),
		
			// Learns and Grows - EL3 - EL3 - No testing
		
		(new Object [] { "Handles Ambiguity - EL3",				"GPI",	"GPI_ADPT",	new Boolean(false) }),
		(new Object [] { "Handles Ambiguity - EL3",				"GPI",	"GPI_OPT",	new Boolean(false) }),
		(new Object [] { "Handles Ambiguity - EL3",				"GPI",	"GPI_EC",	new Boolean(false) }),
		(new Object [] { "Handles Ambiguity - EL3",				"GPI",	"GPI_ST",	new Boolean(false) }),
		(new Object [] { "Handles Ambiguity - EL3",				"GPI",	"GPI_DUT",	new Boolean(false) }),
		
		(new Object [] { "Acts with Integrity - EL3",			"GPI",	"GPI_PA",	new Boolean(true) }),
		(new Object [] { "Acts with Integrity - EL3",			"GPI",	"GPI_MAN",	new Boolean(true) }),
		(new Object [] { "Acts with Integrity - EL3",			"GPI",	"GPI_EMP",	new Boolean(false) }),
		(new Object [] { "Acts with Integrity - EL3",			"GPI",	"GPI_DUT",	new Boolean(false) }),


		// ------------- Ericsson Sales -------------
		(new Object [] { "Creating-Innovating - ES",			"Cogs",	"RAVENSB",	new Boolean(false)   }),
		(new Object [] { "Creating-Innovating - ES",			"GPI",	"GPI_INOV",	new Boolean(false)   }),
		(new Object [] { "Creating-Innovating - ES",			"GPI",	"GPI_TA",	new Boolean(false)   }),

		// No testing for Entrepreneurial Thinking - ES
		
		(new Object [] { "Formulating Strategies - ES",			"Cogs",	"RAVENSB",	new Boolean(false)   }),
		
		(new Object [] { "Persuading-Influencing - ES",			"GPI",	"GPI_TC",	new Boolean(false)   }),
		(new Object [] { "Persuading-Influencing - ES",			"GPI",	"GPI_EL",	new Boolean(false)   }),
		(new Object [] { "Persuading-Influencing - ES",			"GPI",	"GPI_INFL",	new Boolean(false)   }),
		(new Object [] { "Persuading-Influencing - ES",			"GPI",	"GPI_TR",	new Boolean(false)   }),
		(new Object [] { "Persuading-Influencing - ES",			"GPI",	"GPI_AST",	new Boolean(false)   }),
		
		// No testing for Applying Expertise - ES

		(new Object [] { "Relating-Networking - ES",			"GPI",	"GPI_SO",	new Boolean(false)   }),
		(new Object [] { "Relating-Networking - ES",			"GPI",	"GPI_EMP",	new Boolean(false)   }),
		(new Object [] { "Relating-Networking - ES",			"GPI",	"GPI_TR",	new Boolean(false)   }),
		(new Object [] { "Relating-Networking - ES",			"GPI",	"GPI_MAN",	new Boolean(true)    }),
		(new Object [] { "Relating-Networking - ES",			"GPI",	"GPI_EGO",	new Boolean(true)    }),

		(new Object [] { "Coping - ES",							"GPI",	"GPI_ST",	new Boolean(false)   }),
		(new Object [] { "Coping - ES",							"GPI",	"GPI_EC",	new Boolean(false)   }),
		(new Object [] { "Coping - ES",							"GPI",	"GPI_OPT",	new Boolean(false)   }),
		(new Object [] { "Coping - ES",							"GPI",	"GPI_NA",	new Boolean(true)    }),
		(new Object [] { "Coping - ES",							"GPI",	"GPI_ADPT",	new Boolean(false)   }),

		(new Object [] { "Delivering Results - ES",				"GPI",	"GPI_DACH",	new Boolean(false)   }),
		(new Object [] { "Delivering Results - ES",				"GPI",	"GPI_WF",	new Boolean(false)   }),
		(new Object [] { "Delivering Results - ES",				"GPI",	"GPI_RESP",	new Boolean(false)   }),
	
		(new Object [] { "Deciding-Initiating - ES",			"Cogs",	"RAVENSB",	new Boolean(false)   }),
		(new Object [] { "Deciding-Initiating - ES",			"GPI",	"GPI_TC",	new Boolean(false)   }),
		(new Object [] { "Deciding-Initiating - ES",			"GPI",	"GPI_INIT",	new Boolean(false)   }),
		(new Object [] { "Deciding-Initiating - ES",			"GPI",	"GPI_SC",	new Boolean(false)   }),
		(new Object [] { "Deciding-Initiating - ES",			"GPI",	"GPI_RISK",	new Boolean(false)   }),

		(new Object [] { "Achieving Goals-Objectives - ES",		"GPI",	"GPI_DACH",	new Boolean(false)   }),
		(new Object [] { "Achieving Goals-Objectives - ES",		"GPI",	"GPI_EL",	new Boolean(false)   }),
		(new Object [] { "Achieving Goals-Objectives - ES",		"GPI",	"GPI_COMP",	new Boolean(false)   }),
		(new Object [] { "Achieving Goals-Objectives - ES",		"GPI",	"GPI_RESP",	new Boolean(false)   }),


		// ------------- Ericsson L3 Business/Sales & R&D -------------
		(new Object [] { "Competence Drive - EBSRD",			"Cogs",	"RAVENSB",	new Boolean(false) }),		
		(new Object [] { "Competence Drive - EBSRD",			"GPI",	"GPI_TA",	new Boolean(false) }),
		(new Object [] { "Competence Drive - EBSRD",			"GPI",	"GPI_TF",	new Boolean(false) }),
		(new Object [] { "Competence Drive - EBSRD",			"GPI",	"GPI_SASI",	new Boolean(false) }),
		(new Object [] { "Competence Drive - EBSRD",			"GPI",	"GPI_DACH",	new Boolean(false) }),

		(new Object [] { "Master Strategy - EBSRD",				"Cogs",	"RAVENSB",	new Boolean(false) }),		
		(new Object [] { "Master Strategy - EBSRD",				"GPI",	"GPI_TA",	new Boolean(false) }),
		(new Object [] { "Master Strategy - EBSRD",				"GPI",	"GPI_TF",	new Boolean(false) }),
		(new Object [] { "Master Strategy - EBSRD",				"GPI",	"GPI_VIS",	new Boolean(false) }),
		(new Object [] { "Master Strategy - EBSRD",				"GPI",	"GPI_AD",	new Boolean(true) }),

		(new Object [] { "Driving Innovation - EBSRD",			"Cogs",	"RAVENSB",	new Boolean(false) }),		
		(new Object [] { "Driving Innovation - EBSRD",			"GPI",	"GPI_TA",	new Boolean(false) }),
		(new Object [] { "Driving Innovation - EBSRD",			"GPI",	"GPI_INOV",	new Boolean(false) }),
		(new Object [] { "Driving Innovation - EBSRD",			"GPI",	"GPI_RISK",	new Boolean(false) }),
		(new Object [] { "Driving Innovation - EBSRD",			"GPI",	"GPI_OPT",	new Boolean(false) }),

		// No testing for Business Acumen - EBSRD

		(new Object [] { "Enabling People - EBSRD",				"GPI",	"GPI_SO",	new Boolean(false) }),
		(new Object [] { "Enabling People - EBSRD",				"GPI",	"GPI_TR",	new Boolean(false) }),
		(new Object [] { "Enabling People - EBSRD",				"GPI",	"GPI_CONS",	new Boolean(false) }),
		(new Object [] { "Enabling People - EBSRD",				"GPI",	"GPI_EMP",	new Boolean(false) }),
		(new Object [] { "Enabling People - EBSRD",				"GPI",	"GPI_AST",	new Boolean(false) }),
		(new Object [] { "Enabling People - EBSRD",				"GPI",	"GPI_SASI",	new Boolean(false) }),
		(new Object [] { "Enabling People - EBSRD",				"GPI",	"GPI_IND",	new Boolean(true) }),

		(new Object [] { "Courageous Leadership - EBSRD",		"GPI",	"GPI_TC",	new Boolean(false) }),
		(new Object [] { "Courageous Leadership - EBSRD",		"GPI",	"GPI_DACH",	new Boolean(false) }),
		(new Object [] { "Courageous Leadership - EBSRD",		"GPI",	"GPI_INFL",	new Boolean(false) }),
		(new Object [] { "Courageous Leadership - EBSRD",		"GPI",	"GPI_WF",	new Boolean(false) }),
		(new Object [] { "Courageous Leadership - EBSRD",		"GPI",	"GPI_OPT",	new Boolean(false) }),
		(new Object [] { "Courageous Leadership - EBSRD",		"GPI",	"GPI_MIC",	new Boolean(true) }),

		(new Object [] { "Uncompromising Integrity - EBSRD",	"GPI",	"GPI_DUT",	new Boolean(false) }),
		(new Object [] { "Uncompromising Integrity - EBSRD",	"GPI",	"GPI_EGO",	new Boolean(true) }),
		(new Object [] { "Uncompromising Integrity - EBSRD",	"GPI",	"GPI_MAN",	new Boolean(true) }),
		(new Object [] { "Uncompromising Integrity - EBSRD",	"GPI",	"GPI_INTI",	new Boolean(true) }),
		(new Object [] { "Uncompromising Integrity - EBSRD",	"GPI",	"GPI_PA",	new Boolean(true) }),

		// No testing for Customer Excellence - EBSRD

		(new Object [] { "Excelling Execution - EBSRD",			"GPI",	"GPI_TC",	new Boolean(false) }),
		(new Object [] { "Excelling Execution - EBSRD",			"GPI",	"GPI_RESP",	new Boolean(false) }),
		(new Object [] { "Excelling Execution - EBSRD",			"GPI",	"GPI_INIT",	new Boolean(false) }),
		(new Object [] { "Excelling Execution - EBSRD",			"GPI",	"GPI_DACH",	new Boolean(false) }),
		(new Object [] { "Excelling Execution - EBSRD",			"GPI",	"GPI_EL",	new Boolean(false) }),
		(new Object [] { "Excelling Execution - EBSRD",			"GPI",	"GPI_WF",	new Boolean(false) }),

		(new Object [] { "Embracing Change - EBSRD",			"GPI",	"GPI_OPEN",	new Boolean(false) }),
		(new Object [] { "Embracing Change - EBSRD",			"GPI",	"GPI_ADPT",	new Boolean(false) }),
		(new Object [] { "Embracing Change - EBSRD",			"GPI",	"GPI_AST",	new Boolean(false) }),
		(new Object [] { "Embracing Change - EBSRD",			"GPI",	"GPI_ST",	new Boolean(false) }),
		(new Object [] { "Embracing Change - EBSRD",			"GPI",	"GPI_NA",	new Boolean(true) }),



		// ------------- Ericsson New competency structure -------------
		// This has the same competencies and uses the same scale scores as the Ericsson L3 
		// Business/Sales & R&D (suffix EBSRD) and Ericsson EEA/EAC.  If those models are updated or if a
		// new Ericsson model is created with the same competencies and the same scale scores then use
		// this as the scoring model.
		(new Object [] { "Competence Drive - ECS",				"Cogs",	"RAVENSB",	new Boolean(false) }),		
		(new Object [] { "Competence Drive - ECS",				"GPI",	"GPI_TA",	new Boolean(false) }),
		(new Object [] { "Competence Drive - ECS",				"GPI",	"GPI_TF",	new Boolean(false) }),
		(new Object [] { "Competence Drive - ECS",				"GPI",	"GPI_SASI",	new Boolean(false) }),
		(new Object [] { "Competence Drive - ECS",				"GPI",	"GPI_DACH",	new Boolean(false) }),

		(new Object [] { "Master Strategy - ECS",				"Cogs",	"RAVENSB",	new Boolean(false) }),		
		(new Object [] { "Master Strategy - ECS",				"GPI",	"GPI_TA",	new Boolean(false) }),
		(new Object [] { "Master Strategy - ECS",				"GPI",	"GPI_TF",	new Boolean(false) }),
		(new Object [] { "Master Strategy - ECS",				"GPI",	"GPI_VIS",	new Boolean(false) }),
		(new Object [] { "Master Strategy - ECS",				"GPI",	"GPI_AD",	new Boolean(true) }),

		(new Object [] { "Driving Innovation - ECS",			"Cogs",	"RAVENSB",	new Boolean(false) }),		
		(new Object [] { "Driving Innovation - ECS",			"GPI",	"GPI_TA",	new Boolean(false) }),
		(new Object [] { "Driving Innovation - ECS",			"GPI",	"GPI_INOV",	new Boolean(false) }),
		(new Object [] { "Driving Innovation - ECS",			"GPI",	"GPI_RISK",	new Boolean(false) }),
		(new Object [] { "Driving Innovation - ECS",			"GPI",	"GPI_OPT",	new Boolean(false) }),

		// No testing for Business Acumen - ECS

		(new Object [] { "Enabling People - ECS",				"GPI",	"GPI_SO",	new Boolean(false) }),
		(new Object [] { "Enabling People - ECS",				"GPI",	"GPI_TR",	new Boolean(false) }),
		(new Object [] { "Enabling People - ECS",				"GPI",	"GPI_CONS",	new Boolean(false) }),
		(new Object [] { "Enabling People - ECS",				"GPI",	"GPI_EMP",	new Boolean(false) }),
		(new Object [] { "Enabling People - ECS",				"GPI",	"GPI_AST",	new Boolean(false) }),
		(new Object [] { "Enabling People - ECS",				"GPI",	"GPI_SASI",	new Boolean(false) }),
		(new Object [] { "Enabling People - ECS",				"GPI",	"GPI_IND",	new Boolean(true) }),

		(new Object [] { "Courageous Leadership - ECS",			"GPI",	"GPI_TC",	new Boolean(false) }),
		(new Object [] { "Courageous Leadership - ECS",			"GPI",	"GPI_DACH",	new Boolean(false) }),
		(new Object [] { "Courageous Leadership - ECS",			"GPI",	"GPI_INFL",	new Boolean(false) }),
		(new Object [] { "Courageous Leadership - ECS",			"GPI",	"GPI_WF",	new Boolean(false) }),
		(new Object [] { "Courageous Leadership - ECS",			"GPI",	"GPI_OPT",	new Boolean(false) }),
		(new Object [] { "Courageous Leadership - ECS",			"GPI",	"GPI_MIC",	new Boolean(true) }),

		(new Object [] { "Uncompromising Integrity - ECS",		"GPI",	"GPI_DUT",	new Boolean(false) }),
		(new Object [] { "Uncompromising Integrity - ECS",		"GPI",	"GPI_EGO",	new Boolean(true) }),
		(new Object [] { "Uncompromising Integrity - ECS",		"GPI",	"GPI_MAN",	new Boolean(true) }),
		(new Object [] { "Uncompromising Integrity - ECS",		"GPI",	"GPI_INTI",	new Boolean(true) }),
		(new Object [] { "Uncompromising Integrity - ECS",		"GPI",	"GPI_PA",	new Boolean(true) }),

		// No testing for Customer Excellence - ECS

		(new Object [] { "Excelling Execution - ECS",			"GPI",	"GPI_TC",	new Boolean(false) }),
		(new Object [] { "Excelling Execution - ECS",			"GPI",	"GPI_RESP",	new Boolean(false) }),
		(new Object [] { "Excelling Execution - ECS",			"GPI",	"GPI_INIT",	new Boolean(false) }),
		(new Object [] { "Excelling Execution - ECS",			"GPI",	"GPI_DACH",	new Boolean(false) }),
		(new Object [] { "Excelling Execution - ECS",			"GPI",	"GPI_EL",	new Boolean(false) }),
		(new Object [] { "Excelling Execution - ECS",			"GPI",	"GPI_WF",	new Boolean(false) }),

		(new Object [] { "Embracing Change - ECS",				"GPI",	"GPI_OPEN",	new Boolean(false) }),
		(new Object [] { "Embracing Change - ECS",				"GPI",	"GPI_ADPT",	new Boolean(false) }),
		(new Object [] { "Embracing Change - ECS",				"GPI",	"GPI_AST",	new Boolean(false) }),
		(new Object [] { "Embracing Change - ECS",				"GPI",	"GPI_ST",	new Boolean(false) }),
		(new Object [] { "Embracing Change - ECS",				"GPI",	"GPI_NA",	new Boolean(true) }),



		// ------------- Saudi Aramco MAC -------------
		(new Object [] { "Use Sound Judgment - MAC",			"Cogs",	"RAVENSB",	new Boolean(false) }),
		(new Object [] { "Use Sound Judgment - MAC",			"Cogs",	"WGE_CT",	new Boolean(false) }),
		
		(new Object [] { "Strategic Insight - MAC",				"Cogs",	"RAVENSB",	new Boolean(false) }),
		(new Object [] { "Strategic Insight - MAC",				"Cogs",	"WGE_CT",	new Boolean(false) }),
		(new Object [] { "Strategic Insight - MAC",				"GPI",	"GPI_TF",	new Boolean(false) }),
		(new Object [] { "Strategic Insight - MAC",				"GPI",	"GPI_VIS",	new Boolean(false) }),
		(new Object [] { "Strategic Insight - MAC",				"GPI",	"GPI_AD",	new Boolean(true) }),

		// No testing for Business Acumen - MAC
		
		(new Object [] { "Foster Innovation - MAC",				"GPI",	"GPI_INOV",	new Boolean(false) }),
		(new Object [] { "Foster Innovation - MAC",				"GPI",	"GPI_TF",	new Boolean(false) }),
		(new Object [] { "Foster Innovation - MAC",				"GPI",	"GPI_VIS",	new Boolean(false) }),
		(new Object [] { "Foster Innovation - MAC",				"GPI",	"GPI_RISK",	new Boolean(false) }),

		// No testing for Display Global Perspective - MAC
		
		(new Object [] { "Motivate Engage Inspire - MAC",		"GPI",	"GPI_INFL",	new Boolean(false) }),
		(new Object [] { "Motivate Engage Inspire - MAC",		"GPI",	"GPI_SO",	new Boolean(false) }),
		(new Object [] { "Motivate Engage Inspire - MAC",		"GPI",	"GPI_OPT",	new Boolean(false) }),
		
		(new Object [] { "Influence Others - MAC",				"GPI",	"GPI_TF",	new Boolean(false) }),
		(new Object [] { "Influence Others - MAC",				"GPI",	"GPI_INFL",	new Boolean(false) }),
		(new Object [] { "Influence Others - MAC",				"GPI",	"GPI_SO",	new Boolean(false) }),
		(new Object [] { "Influence Others - MAC",				"GPI",	"GPI_EL",	new Boolean(false) }),

		// No testing for Manage and Develop Talent - MAC

		// No testing for Foster Open Communication - MAC
		
		(new Object [] { "Build Relationships - MAC",			"GPI",	"GPI_INTI",	new Boolean(true) }),
		(new Object [] { "Build Relationships - MAC",			"GPI",	"GPI_SO",	new Boolean(false) }),
		(new Object [] { "Build Relationships - MAC",			"GPI",	"GPI_EMP",	new Boolean(false) }),
		(new Object [] { "Build Relationships - MAC",			"GPI",	"GPI_CONS",	new Boolean(false) }),
		(new Object [] { "Build Relationships - MAC",			"GPI",	"GPI_TR",	new Boolean(false) }),
		(new Object [] { "Build Relationships - MAC",			"GPI",	"GPI_OPEN",	new Boolean(false) }),
		
		(new Object [] { "Foster Teamwork and Collaboration - MAC",	"GPI",	"GPI_MIC",	new Boolean(true) }),
		(new Object [] { "Foster Teamwork and Collaboration - MAC",	"GPI",	"GPI_IND",	new Boolean(true) }),
		(new Object [] { "Foster Teamwork and Collaboration - MAC",	"GPI",	"GPI_TR",	new Boolean(false) }),
		(new Object [] { "Foster Teamwork and Collaboration - MAC",	"GPI",	"GPI_DACH",	new Boolean(false) }),
		
		(new Object [] { "Drive for Results - MAC",				"GPI",	"GPI_WF",	new Boolean(false) }),
		(new Object [] { "Drive for Results - MAC",				"GPI",	"GPI_EL",	new Boolean(false) }),
		(new Object [] { "Drive for Results - MAC",				"GPI",	"GPI_DACH",	new Boolean(false) }),
		(new Object [] { "Drive for Results - MAC",				"GPI",	"GPI_RISK",	new Boolean(false) }),
		(new Object [] { "Drive for Results - MAC",				"GPI",	"GPI_DADV",	new Boolean(false) }),
		
		(new Object [] { "Manage Execution - MAC",				"GPI",	"GPI_AD",	new Boolean(false) }),
		(new Object [] { "Manage Execution - MAC",				"GPI",	"GPI_INFL",	new Boolean(false) }),
		(new Object [] { "Manage Execution - MAC",				"GPI",	"GPI_DACH",	new Boolean(false) }),
		
		(new Object [] { "Adaptability and Resilience - MAC",	"GPI",	"GPI_ADPT",	new Boolean(false) }),
		(new Object [] { "Adaptability and Resilience - MAC",	"GPI",	"GPI_OPEN",	new Boolean(false) }),
		(new Object [] { "Adaptability and Resilience - MAC",	"GPI",	"GPI_EC",	new Boolean(false) }),
		(new Object [] { "Adaptability and Resilience - MAC",	"GPI",	"GPI_ST",	new Boolean(false) }),
		
		(new Object [] { "Inspire Trust - MAC",					"GPI",	"GPI_OPEN",	new Boolean(false) }),
		(new Object [] { "Inspire Trust - MAC",					"GPI",	"GPI_EC",	new Boolean(false) }),
		(new Object [] { "Inspire Trust - MAC",					"GPI",	"GPI_CONS",	new Boolean(false) }),
		(new Object [] { "Inspire Trust - MAC",					"GPI",	"GPI_MAN",	new Boolean(true) }),
		(new Object [] { "Inspire Trust - MAC",					"GPI",	"GPI_PA",	new Boolean(true) }),
		

		// ------------- Saudi Aramco SAC -------------
		(new Object [] { "Analyze & Solve Problems - SAC",		"Cogs",	"RAVENSB",	new Boolean(false) }),
		(new Object [] { "Analyze & Solve Problems - SAC",		"GPI",  "GPI_INOV",	new Boolean(false) }),
		(new Object [] { "Analyze & Solve Problems - SAC",		"GPI",  "GPI_TF",	new Boolean(false) }),
		(new Object [] { "Analyze & Solve Problems - SAC",		"GPI",  "GPI_VIS",	new Boolean(false) }),
		(new Object [] { "Analyze & Solve Problems - SAC",		"GPI",  "GPI_AD",	new Boolean(false) }),
		
		// No testing for Use Business Metrics - SAC
		
		(new Object [] { "Show Drive & Initiative - SAC",		"GPI",  "GPI_WF",	new Boolean(false) }),
		(new Object [] { "Show Drive & Initiative - SAC",		"GPI",  "GPI_TC",	new Boolean(false) }),
		(new Object [] { "Show Drive & Initiative - SAC",		"GPI",  "GPI_EL",	new Boolean(false) }),
		(new Object [] { "Show Drive & Initiative - SAC",		"GPI",  "GPI_DACH",	new Boolean(false) }),
		(new Object [] { "Show Drive & Initiative - SAC",		"GPI",  "GPI_RESP",	new Boolean(false) }),
		(new Object [] { "Show Drive & Initiative - SAC",		"GPI",  "GPI_COMP",	new Boolean(false) }),
		(new Object [] { "Show Drive & Initiative - SAC",		"GPI",  "GPI_INIT",	new Boolean(false) }),

		// No testing for Direct Staff & Delegate - SAC
		
		(new Object [] { "Plan & Organize Work - SAC",			"GPI",  "GPI_AD",	new Boolean(false) }),
		(new Object [] { "Plan & Organize Work - SAC",			"GPI",  "GPI_WF",	new Boolean(false) }),
		(new Object [] { "Plan & Organize Work - SAC",			"GPI",  "GPI_VIS",	new Boolean(false) }),

		(new Object [] { "Encourage & Motivate - SAC",			"GPI",  "GPI_INFL",	new Boolean(false) }),
		(new Object [] { "Encourage & Motivate - SAC",			"GPI",  "GPI_SO",	new Boolean(false) }),
		(new Object [] { "Encourage & Motivate - SAC",			"GPI",  "GPI_OPT",	new Boolean(false) }),

		(new Object [] { "Build Support - SAC",					"GPI",  "GPI_INFL",	new Boolean(false) }),
		(new Object [] { "Build Support - SAC",					"GPI",  "GPI_SO",	new Boolean(false) }),
		(new Object [] { "Build Support - SAC",					"GPI",  "GPI_AST",	new Boolean(false) }),
		(new Object [] { "Build Support - SAC",					"GPI",  "GPI_DACH",	new Boolean(false) }),
		(new Object [] { "Build Support - SAC",					"GPI",  "GPI_INTI",	new Boolean(true) }),

		// No testing for Coach & Develop Talent - SAC
		
		// No testing for Communicate Openly - SAC
		
		(new Object [] { "Build Relationships - SAC",			"GPI",  "GPI_INTI",	new Boolean(true) }),
		(new Object [] { "Build Relationships - SAC",			"GPI",  "GPI_IND",	new Boolean(true) }),
		(new Object [] { "Build Relationships - SAC",			"GPI",  "GPI_NA",	new Boolean(true) }),
		(new Object [] { "Build Relationships - SAC",			"GPI",  "GPI_SO",	new Boolean(false) }),
		(new Object [] { "Build Relationships - SAC",			"GPI",  "GPI_CONS",	new Boolean(false) }),
		(new Object [] { "Build Relationships - SAC",			"GPI",  "GPI_EMP",	new Boolean(false) }),
		(new Object [] { "Build Relationships - SAC",			"GPI",  "GPI_TR",	new Boolean(false) }),

		(new Object [] { "Foster Teamwork - SAC",				"GPI",  "GPI_INTI",	new Boolean(true) }),
		(new Object [] { "Foster Teamwork - SAC",				"GPI",  "GPI_MIC",	new Boolean(true) }),
		(new Object [] { "Foster Teamwork - SAC",				"GPI",  "GPI_NA",	new Boolean(true) }),
		(new Object [] { "Foster Teamwork - SAC",				"GPI",  "GPI_TR",	new Boolean(false) }),
		(new Object [] { "Foster Teamwork - SAC",				"GPI",  "GPI_IND",	new Boolean(true) }),

		(new Object [] { "Adapt & Learn - SAC",					"GPI",  "GPI_ADPT",	new Boolean(false) }),
		(new Object [] { "Adapt & Learn - SAC",					"GPI",  "GPI_NA",	new Boolean(true) }),
		(new Object [] { "Adapt & Learn - SAC",					"GPI",  "GPI_OPT",	new Boolean(false) }),
		(new Object [] { "Adapt & Learn - SAC",					"GPI",  "GPI_EC",	new Boolean(false) }),
		(new Object [] { "Adapt & Learn - SAC",					"GPI",  "GPI_ST",	new Boolean(false) }),
		

		// ------------- Saudi Aramco SAC -------------
		(new Object [] { "Analyze & Solve Problems - SAC II",	"Cogs",	"RAVENSB",	new Boolean(false) }),
		(new Object [] { "Analyze & Solve Problems - SAC II",	"GPI",  "GPI_INOV",	new Boolean(false) }),
		(new Object [] { "Analyze & Solve Problems - SAC II",	"GPI",  "GPI_TF",	new Boolean(false) }),
		(new Object [] { "Analyze & Solve Problems - SAC II",	"GPI",  "GPI_VIS",	new Boolean(false) }),
		(new Object [] { "Analyze & Solve Problems - SAC II",	"GPI",  "GPI_AD",	new Boolean(false) }),

		// No testing for Use Business Metrics - SAC II

		(new Object [] { "Show Drive & Initiative - SAC II",	"GPI",  "GPI_TC",	new Boolean(false) }),
		(new Object [] { "Show Drive & Initiative - SAC II",	"GPI",  "GPI_EL",	new Boolean(false) }),
		(new Object [] { "Show Drive & Initiative - SAC II",	"GPI",  "GPI_DACH",	new Boolean(false) }),

		// No testing for Direct Staff & Delegate - SAC II

		(new Object [] { "Plan & Organize Work - SAC II",		"GPI",  "GPI_AD",	new Boolean(false) }),
		(new Object [] { "Plan & Organize Work - SAC II",		"GPI",  "GPI_VIS",	new Boolean(false) }),

		(new Object [] { "Encourage & Motivate - SAC II",		"GPI",  "GPI_INFL",	new Boolean(false) }),
		(new Object [] { "Encourage & Motivate - SAC II",		"GPI",  "GPI_SO",	new Boolean(false) }),
		(new Object [] { "Encourage & Motivate - SAC II",		"GPI",  "GPI_OPT",	new Boolean(false) }),

		(new Object [] { "Build Support - SAC II",				"GPI",  "GPI_INFL",	new Boolean(false) }),
		(new Object [] { "Build Support - SAC II",				"GPI",  "GPI_SO",	new Boolean(false) }),
		(new Object [] { "Build Support - SAC II",				"GPI",  "GPI_AST",	new Boolean(false) }),
		(new Object [] { "Build Support - SAC II",				"GPI",  "GPI_DACH",	new Boolean(false) }),

		// No testing for Coach & Develop Talent - SAC II

		// No testing for Communicate Openly - SAC II

		(new Object [] { "Build Relationships - SAC II",		"GPI",  "GPI_IND",	new Boolean(true) }),
		(new Object [] { "Build Relationships - SAC II",		"GPI",  "GPI_NA",	new Boolean(true) }),
		(new Object [] { "Build Relationships - SAC II",		"GPI",  "GPI_SO",	new Boolean(false) }),
		(new Object [] { "Build Relationships - SAC II",		"GPI",  "GPI_CONS",	new Boolean(false) }),
		(new Object [] { "Build Relationships - SAC II",		"GPI",  "GPI_EMP",	new Boolean(false) }),
		(new Object [] { "Build Relationships - SAC II",		"GPI",  "GPI_TR",	new Boolean(false) }),

		(new Object [] { "Foster Teamwork - SAC II",			"GPI",  "GPI_MIC",	new Boolean(true) }),
		(new Object [] { "Foster Teamwork - SAC II",			"GPI",  "GPI_NA",	new Boolean(true) }),
		(new Object [] { "Foster Teamwork - SAC II",			"GPI",  "GPI_TR",	new Boolean(false) }),
		(new Object [] { "Foster Teamwork - SAC II",			"GPI",  "GPI_IND",	new Boolean(true) }),
		(new Object [] { "Foster Teamwork - SAC II",			"GPI",  "GPI_INTD",	new Boolean(false) }),

		(new Object [] { "Adapt & Learn - SAC II",				"GPI",  "GPI_ADPT",	new Boolean(false) }),
		(new Object [] { "Adapt & Learn - SAC II",				"GPI",  "GPI_NA",	new Boolean(true) }),
		(new Object [] { "Adapt & Learn - SAC II",				"GPI",  "GPI_OPT",	new Boolean(false) }),
		(new Object [] { "Adapt & Learn - SAC II",				"GPI",  "GPI_SASI",	new Boolean(false) }),
		(new Object [] { "Adapt & Learn - SAC II",				"GPI",  "GPI_ST",	new Boolean(false) }),


		// ------------- Saudi Aramco CAC -------------
		(new Object [] { "Use Seasoned Judgment - CAC",			"Cogs", "RAVENSB",	new Boolean(false) }),
		(new Object [] { "Use Seasoned Judgment - CAC",			"Cogs", "WGE_CT",	new Boolean(false) }),

		(new Object [] { "Think Strategically - CAC",			"Cogs",	"RAVENSB",	new Boolean(false) }),
		(new Object [] { "Think Strategically - CAC",			"GPI",	"GPI_TF",	new Boolean(false) }),
		(new Object [] { "Think Strategically - CAC",			"GPI",	"GPI_VIS",	new Boolean(false) }),
		(new Object [] { "Think Strategically - CAC",			"GPI",	"GPI_AD",	new Boolean(true) }),

		// No testing for Employ Financial Acumen - CAC
		
		(new Object [] { "Display Visionary Thinking - CAC",	"GPI",	"GPI_INOV",	new Boolean(false) }),
		(new Object [] { "Display Visionary Thinking - CAC",	"GPI",	"GPI_TF",	new Boolean(false) }),
		(new Object [] { "Display Visionary Thinking - CAC",	"GPI",	"GPI_VIS",	new Boolean(false) }),
		(new Object [] { "Display Visionary Thinking - CAC",	"GPI",	"GPI_RISK",	new Boolean(false) }),

		// No testing for Drive Global Integration - CAC

		(new Object [] { "Engage and Inspire - CAC",			"GPI",	"GPI_INFL",	new Boolean(false) }),
		(new Object [] { "Engage and Inspire - CAC",			"GPI",	"GPI_MIC",	new Boolean(true) }),
		(new Object [] { "Engage and Inspire - CAC",			"GPI",	"GPI_SO",	new Boolean(false) }),
		(new Object [] { "Engage and Inspire - CAC",			"GPI",	"GPI_EL",	new Boolean(false) }),
		(new Object [] { "Engage and Inspire - CAC",			"GPI",	"GPI_OPT",	new Boolean(false) }),

		(new Object [] { "Display Organizational Influence - CAC",	"GPI",	"GPI_WF",	new Boolean(false) }),
		(new Object [] { "Display Organizational Influence - CAC",	"GPI",	"GPI_TC",	new Boolean(false) }),
		(new Object [] { "Display Organizational Influence - CAC",	"GPI",	"GPI_EL",	new Boolean(false) }),
		(new Object [] { "Display Organizational Influence - CAC",	"GPI",	"GPI_INIT",	new Boolean(false) }),
		(new Object [] { "Display Organizational Influence - CAC",	"GPI",	"GPI_DACH",	new Boolean(false) }),
		(new Object [] { "Display Organizational Influence - CAC",	"GPI",	"GPI_ADPT",	new Boolean(false) }),

		// No testing for Develop Organizational Talent - CAC

		// No testing for Foster Open Communication - CAC

		(new Object [] { "Build Relationships - CAC",			"GPI",	"GPI_INTI",	new Boolean(true) }),
		(new Object [] { "Build Relationships - CAC",			"GPI",	"GPI_SO",	new Boolean(false) }),
		(new Object [] { "Build Relationships - CAC",			"GPI",	"GPI_CONS",	new Boolean(false) }),
		(new Object [] { "Build Relationships - CAC",			"GPI",	"GPI_TR",	new Boolean(false) }),
		(new Object [] { "Build Relationships - CAC",			"GPI",	"GPI_AST",	new Boolean(false) }),
		(new Object [] { "Build Relationships - CAC",			"GPI",	"GPI_OPEN",	new Boolean(false) }),
		(new Object [] { "Build Relationships - CAC",			"GPI",	"GPI_NA",	new Boolean(true) }),

		(new Object [] { "Drive Organizational Success - CAC",	"GPI",	"GPI_WF",	new Boolean(false) }),
		(new Object [] { "Drive Organizational Success - CAC",	"GPI",	"GPI_TC",	new Boolean(false) }),
		(new Object [] { "Drive Organizational Success - CAC",	"GPI",	"GPI_EL",	new Boolean(false) }),
		(new Object [] { "Drive Organizational Success - CAC",	"GPI",	"GPI_INIT",	new Boolean(false) }),
		(new Object [] { "Drive Organizational Success - CAC",	"GPI",	"GPI_DACH",	new Boolean(false) }),
		(new Object [] { "Drive Organizational Success - CAC",	"GPI",	"GPI_ADPT",	new Boolean(false) }),

		(new Object [] { "Drive Execution - CAC",				"GPI",	"GPI_AD",	new Boolean(false) }),
		(new Object [] { "Drive Execution - CAC",				"GPI",	"GPI_INFL",	new Boolean(false) }),
		(new Object [] { "Drive Execution - CAC",				"GPI",	"GPI_DACH",	new Boolean(false) }),
		(new Object [] { "Drive Execution - CAC",				"GPI",	"GPI_MIC",	new Boolean(true) }),

		// No testing for Align the Organization - CAC

		(new Object [] { "Lead Courageously - CAC",				"GPI",	"GPI_TC",	new Boolean(false) }),
		(new Object [] { "Lead Courageously - CAC",				"GPI",	"GPI_INFL",	new Boolean(false) }),
		(new Object [] { "Lead Courageously - CAC",				"GPI",	"GPI_INIT",	new Boolean(false) }),
		(new Object [] { "Lead Courageously - CAC",				"GPI",	"GPI_PA",	new Boolean(true) }),
		(new Object [] { "Lead Courageously - CAC",				"GPI",	"GPI_DACH",	new Boolean(false) }),
		(new Object [] { "Lead Courageously - CAC",				"GPI",	"GPI_RISK",	new Boolean(false) }),

		(new Object [] { "Adaptability and Resilience - CAC",	"GPI",	"GPI_ADPT",	new Boolean(false) }),
		(new Object [] { "Adaptability and Resilience - CAC",	"GPI",	"GPI_NA",	new Boolean(true) }),
		(new Object [] { "Adaptability and Resilience - CAC",	"GPI",	"GPI_OPT",	new Boolean(false) }),
		(new Object [] { "Adaptability and Resilience - CAC",	"GPI",	"GPI_EC",	new Boolean(false) }),
		(new Object [] { "Adaptability and Resilience - CAC",	"GPI",	"GPI_ST",	new Boolean(false) }),
		(new Object [] { "Adaptability and Resilience - CAC",	"GPI",	"GPI_SASI",	new Boolean(false) }),

		(new Object [] { "Earn Unwavering Trust - CAC",			"GPI",	"GPI_OPEN",	new Boolean(false) }),
		(new Object [] { "Earn Unwavering Trust - CAC",			"GPI",	"GPI_EC",	new Boolean(false) }),
		(new Object [] { "Earn Unwavering Trust - CAC",			"GPI",	"GPI_CONS",	new Boolean(false) }),
		(new Object [] { "Earn Unwavering Trust - CAC",			"GPI",	"GPI_MAN",	new Boolean(true) }),
		(new Object [] { "Earn Unwavering Trust - CAC",			"GPI",	"GPI_PA",	new Boolean(true) }),

		
		// ------------- Saudi Aramco DAC -------------
		(new Object [] { "Make Sound Decisions - DAC",			"Cogs", "RAVENSB",	new Boolean(false) }),
		(new Object [] { "Make Sound Decisions - DAC",			"Cogs", "WGE_CT",	new Boolean(false) }),

		(new Object [] { "Understand Strategies - DAC",			"Cogs",	"RAVENSB",	new Boolean(false) }),
		(new Object [] { "Understand Strategies - DAC",			"GPI",	"GPI_TF",	new Boolean(false) }),
		(new Object [] { "Understand Strategies - DAC",			"GPI",	"GPI_VIS",	new Boolean(false) }),
		(new Object [] { "Understand Strategies - DAC",			"GPI",	"GPI_AD",	new Boolean(true) }),

		// No testing for Use Business Metrics - DAC

		(new Object [] { "Motivate Others - DAC",				"GPI",	"GPI_INFL",	new Boolean(false) }),
		(new Object [] { "Motivate Others - DAC",				"GPI",	"GPI_SO",	new Boolean(false) }),
		(new Object [] { "Motivate Others - DAC",				"GPI",	"GPI_OPT",	new Boolean(false) }),

		(new Object [] { "Influence Others - DAC",				"GPI",	"GPI_TF",	new Boolean(false) }),
		(new Object [] { "Influence Others - DAC",				"GPI",	"GPI_INFL",	new Boolean(false) }),
		(new Object [] { "Influence Others - DAC",				"GPI",	"GPI_SO",	new Boolean(false) }),
		(new Object [] { "Influence Others - DAC",				"GPI",	"GPI_EL",	new Boolean(false) }),

		// No testing for Coach and Develop Talent - DAC

		// No testing for Foster Open Communication - DAC

		(new Object [] { "Build Relationships - DAC",			"GPI",	"GPI_INTI",	new Boolean(true) }),
		(new Object [] { "Build Relationships - DAC",			"GPI",	"GPI_SO",	new Boolean(false) }),
		(new Object [] { "Build Relationships - DAC",			"GPI",	"GPI_EMP",	new Boolean(false) }),
		(new Object [] { "Build Relationships - DAC",			"GPI",	"GPI_CONS",	new Boolean(false) }),
		(new Object [] { "Build Relationships - DAC",			"GPI",	"GPI_TR",	new Boolean(false) }),
		(new Object [] { "Build Relationships - DAC",			"GPI",	"GPI_OPEN",	new Boolean(false) }),

		(new Object [] { "Promote teamwork - DAC",				"GPI",	"GPI_MIC",	new Boolean(true) }),
		(new Object [] { "Promote teamwork - DAC",				"GPI",	"GPI_IND",	new Boolean(true) }),
		(new Object [] { "Promote teamwork - DAC",				"GPI",	"GPI_TR",	new Boolean(false) }),
		(new Object [] { "Promote teamwork - DAC",				"GPI",	"GPI_DACH",	new Boolean(false) }),

		(new Object [] { "Drive for Results - DAC",				"GPI",	"GPI_WF",	new Boolean(false) }),
		(new Object [] { "Drive for Results - DAC",				"GPI",	"GPI_EL",	new Boolean(false) }),
		(new Object [] { "Drive for Results - DAC",				"GPI",	"GPI_DACH",	new Boolean(false) }),
		(new Object [] { "Drive for Results - DAC",				"GPI",	"GPI_RISK",	new Boolean(false) }),
		(new Object [] { "Drive for Results - DAC",				"GPI",	"GPI_DADV",	new Boolean(false) }),

		(new Object [] { "Manage Execution - DAC",				"GPI",	"GPI_AD",	new Boolean(false) }),
		(new Object [] { "Manage Execution - DAC",				"GPI",	"GPI_INFL",	new Boolean(false) }),
		(new Object [] { "Manage Execution - DAC",				"GPI",	"GPI_DACH",	new Boolean(false) }),

		// No testing for Build Realistic Plans - DAC

		(new Object [] { "Establish Trust - DAC",				"GPI",	"GPI_OPEN",	new Boolean(false) }),
		(new Object [] { "Establish Trust - DAC",				"GPI",	"GPI_EC",	new Boolean(false) }),
		(new Object [] { "Establish Trust - DAC",				"GPI",	"GPI_CONS",	new Boolean(false) }),
		(new Object [] { "Establish Trust - DAC",				"GPI",	"GPI_MAN",	new Boolean(true) }),
		(new Object [] { "Establish Trust - DAC",				"GPI",	"GPI_PA",	new Boolean(true) }),

		(new Object [] { "Show Adaptability - DAC",				"GPI",	"GPI_ADPT",	new Boolean(false) }),
		(new Object [] { "Show Adaptability - DAC",				"GPI",	"GPI_OPEN",	new Boolean(false) }),
		(new Object [] { "Show Adaptability - DAC",				"GPI",	"GPI_EC",	new Boolean(false) }),
		(new Object [] { "Show Adaptability - DAC",				"GPI",	"GPI_ST",	new Boolean(false) }),


		// ------------- SAM -------------
		(new Object [] { "Identify Innovative Targeted Solutions - SAM", "Cogs", "RAVENSB",	new Boolean(false) }),
		(new Object [] { "Identify Innovative Targeted Solutions - SAM", "Cogs", "WGE_CT", new Boolean(false) }),
		
		(new Object [] { "Think Strategically - SAM",			"Cogs",	"RAVENSB",	new Boolean(false) }),
		(new Object [] { "Think Strategically - SAM",			"GPI",  "GPI_INOV",	new Boolean(false) }),
		(new Object [] { "Think Strategically - SAM",			"GPI",  "GPI_TF",	new Boolean(false) }),
		(new Object [] { "Think Strategically - SAM",			"GPI",  "GPI_VIS",	new Boolean(false) }),
		
		(new Object [] { "Plan and Manage Execution - SAM",		"GPI",  "GPI_VIS",	new Boolean(false) }),
		(new Object [] { "Plan and Manage Execution - SAM",		"GPI",  "GPI_AD",	new Boolean(false) }),
		(new Object [] { "Plan and Manage Execution - SAM",		"GPI",  "GPI_WF",	new Boolean(false) }),
		
		(new Object [] { "Customer Focus - SAM",				"GPI",  "GPI_VIS",	new Boolean(false) }),
		(new Object [] { "Customer Focus - SAM",				"GPI",  "GPI_MAN",	new Boolean(true) }),
		(new Object [] { "Customer Focus - SAM",				"GPI",  "GPI_INTI",	new Boolean(true) }),
		(new Object [] { "Customer Focus - SAM",				"GPI",  "GPI_PA",	new Boolean(true) }),
		(new Object [] { "Customer Focus - SAM",				"GPI",  "GPI_SO",	new Boolean(false) }),
		(new Object [] { "Customer Focus - SAM",				"GPI",  "GPI_AST",	new Boolean(false) }),
		(new Object [] { "Customer Focus - SAM",				"GPI",  "GPI_INIT",	new Boolean(false) }),
		(new Object [] { "Customer Focus - SAM",				"GPI",  "GPI_NA",	new Boolean(true) }),
		
		(new Object [] { "Drive for Results - SAM",				"GPI",  "GPI_WF",	new Boolean(false) }),
		(new Object [] { "Drive for Results - SAM",				"GPI",  "GPI_INFL",	new Boolean(false) }),
		(new Object [] { "Drive for Results - SAM",				"GPI",  "GPI_EL",	new Boolean(false) }),
		(new Object [] { "Drive for Results - SAM",				"GPI",  "GPI_INIT",	new Boolean(false) }),
		(new Object [] { "Drive for Results - SAM",				"GPI",  "GPI_DACH",	new Boolean(false) }),
		(new Object [] { "Drive for Results - SAM",				"GPI",  "GPI_DADV",	new Boolean(false) }),
		(new Object [] { "Drive for Results - SAM",				"GPI",  "GPI_RESP",	new Boolean(false) }),
		
		(new Object [] { "Influence and Negotiate - SAM",		"GPI",  "GPI_TF",	new Boolean(false) }),
		(new Object [] { "Influence and Negotiate - SAM",		"GPI",  "GPI_INFL",	new Boolean(false) }),
		(new Object [] { "Influence and Negotiate - SAM",		"GPI",  "GPI_AST",	new Boolean(false) }),
		(new Object [] { "Influence and Negotiate - SAM",		"GPI",  "GPI_INTI",	new Boolean(true) }),
		
		(new Object [] { "Lead the Team - SAM",					"GPI",  "GPI_INFL",	new Boolean(false) }),
		
		// Communicate Effectively - SAM - No testing
		
		(new Object [] { "Enhance Professional Skills - SAM",	"GPI",  "GPI_INIT",	new Boolean(false) }),


		// ------------- Target MLL -------------
		(new Object [] { "Make Sound Decisions - Target",		"Cogs",	"RAVENSB",	new Boolean(false) }),
		(new Object [] { "Make Sound Decisions - Target",		"Cogs", "WGE_CT",	new Boolean(false) }),
		(new Object [] { "Make Sound Decisions - Target",		"GPI",  "GPI_TF",	new Boolean(false) }),

		(new Object [] { "Act Strategically - Target",			"Cogs",	"RAVENSB",	new Boolean(false) }),
		(new Object [] { "Act Strategically - Target",			"Cogs", "WGE_CT", 	new Boolean(false) }),
		(new Object [] { "Act Strategically - Target",			"GPI", 	"GPI_VIS", 	new Boolean(false) }),

		(new Object [] { "Think Creatively - Target",	 		"GPI", "GPI_INOV", 	new Boolean(false) }),

		(new Object [] { "Manage Execution - Target",	 		"GPI", "GPI_WF", 	new Boolean(false) }),
		
		(new Object [] { "Show Drive and Initiative - Target",	"GPI", "GPI_INIT", 	new Boolean(false) }),
		(new Object [] { "Show Drive and Initiative - Target",	"GPI", "GPI_DACH", 	new Boolean(false) }),

		(new Object [] { "Build Support - Target", 				"GPI", "GPI_PA", 	new Boolean(true)  }), 
		(new Object [] { "Build Support - Target",		 		"GPI", "GPI_RISK", 	new Boolean(false) }), 

		(new Object [] { "Motivate Others - Target",			"GPI", "GPI_INFL", 	new Boolean(false) }),
		(new Object [] { "Motivate Others - Target",			"GPI", "GPI_MIC", 	new Boolean(true)  }),
		(new Object [] { "Motivate Others - Target",			"GPI", "GPI_TR", 	new Boolean(false) }),
		
		(new Object [] { "Develop Others - Target",				"GPI", "GPI_TC", 	new Boolean(false) }),
		(new Object [] { "Develop Others - Target",				"GPI", "GPI_MIC", 	new Boolean(true)  }),
		
		(new Object [] { "Promote Teamwork - Target",			"GPI", "GPI_INFL", 	new Boolean(false) }),
		(new Object [] { "Promote Teamwork - Target",			"GPI", "GPI_EGO", 	new Boolean(true)  }),
		(new Object [] { "Promote Teamwork - Target",			"GPI", "GPI_OPEN", 	new Boolean(false) }),
		(new Object [] { "Promote Teamwork - Target",			"GPI", "GPI_IND", 	new Boolean(true)  }),
		(new Object [] { "Promote Teamwork - Target",			"GPI", "GPI_COMP", 	new Boolean(true)  }),
		// Adding per JIRA task NHN-2458
		(new Object [] { "Promote Teamwork - Target",			"GPI", "GPI_INTD", 	new Boolean(false) }),
		
		(new Object [] { "Establish Relationships - Target",	"GPI", "GPI_INFL", 	new Boolean(false) }),
		(new Object [] { "Establish Relationships - Target",	"GPI", "GPI_EGO", 	new Boolean(true)  }),
		(new Object [] { "Establish Relationships - Target",	"GPI", "GPI_MAN", 	new Boolean(true)  }),
		(new Object [] { "Establish Relationships - Target",	"GPI", "GPI_INTI", 	new Boolean(true)  }),
		(new Object [] { "Establish Relationships - Target",	"GPI", "GPI_PA", 	new Boolean(true)  }),
		(new Object [] { "Establish Relationships - Target",	"GPI", "GPI_SO", 	new Boolean(false) }),
		(new Object [] { "Establish Relationships - Target",	"GPI", "GPI_CONS", 	new Boolean(false) }),
		(new Object [] { "Establish Relationships - Target",	"GPI", "GPI_EMP", 	new Boolean(false) }),
		(new Object [] { "Establish Relationships - Target",	"GPI", "GPI_AST", 	new Boolean(false) }),
		(new Object [] { "Establish Relationships - Target",	"GPI", "GPI_OPEN", 	new Boolean(false) }),
		(new Object [] { "Establish Relationships - Target",	"GPI", "GPI_INTD", 	new Boolean(false) }),

		(new Object [] { "Establish Trust - Target",			"GPI", "GPI_MAN", 	new Boolean(true)  }),
		(new Object [] { "Establish Trust - Target",			"GPI", "GPI_DUT", 	new Boolean(false) }),
		(new Object [] { "Establish Trust - Target",			"GPI", "GPI_RESP", 	new Boolean(false) }),

		(new Object [] { "Show Adaptability - Target",			"GPI", "GPI_ADPT", 	new Boolean(false) }),
		(new Object [] { "Show Adaptability - Target",			"GPI", "GPI_ST", 	new Boolean(false) }),


		// ------------- Target BUL -------------
		(new Object [] { "Use Insightful Judgment - Target",	"Cogs",	"RAVENSB",	new Boolean(false) }),
		(new Object [] { "Use Insightful Judgment - Target",	"Cogs", "WGE_CT", 	new Boolean(false) }),
		(new Object [] { "Use Insightful Judgment - Target",	"GPI", "GPI_TF", 	new Boolean(false) }),

		(new Object [] { "Think Strategically - Target",		"Cogs",	"RAVENSB",	new Boolean(false) }),
		(new Object [] { "Think Strategically - Target",		"Cogs", "WGE_CT", 	new Boolean(false) }),
		(new Object [] { "Think Strategically - Target",		"GPI", "GPI_VIS", 	new Boolean(false) }),

		(new Object [] { "Innovate - Target",					"GPI", "GPI_INOV",	new Boolean(false) }),

		(new Object [] { "Ensure Execution - Target",			"GPI", "GPI_WF", 	new Boolean(false) }),

		(new Object [] { "Drive for Results - Target",			"GPI", "GPI_INIT", 	new Boolean(false) }),
		(new Object [] { "Drive for Results - Target",			"GPI", "GPI_DACH", 	new Boolean(false) }),

		(new Object [] { "Lead Courageously - Target",			"GPI", "GPI_PA", 	new Boolean(true)  }),
		(new Object [] { "Lead Courageously - Target",			"GPI", "GPI_RISK", 	new Boolean(false) }),

		(new Object [] { "Engage and Inspire - Target",			"GPI", "GPI_INFL", 	new Boolean(false) }),
		(new Object [] { "Engage and Inspire - Target",			"GPI", "GPI_MIC", 	new Boolean(true)  }),
		(new Object [] { "Engage and Inspire - Target",			"GPI", "GPI_TR", 	new Boolean(false) }),

		(new Object [] { "Build Talent - Target",				"GPI", "GPI_TC", 	new Boolean(false) }),
		(new Object [] { "Build Talent - Target",				"GPI", "GPI_MIC", 	new Boolean(true)  }),

		(new Object [] { "Promote Collaboration - Target",		"GPI", "GPI_INFL", 	new Boolean(false) }),
		(new Object [] { "Promote Collaboration - Target",		"GPI", "GPI_EGO", 	new Boolean(true)  }),
		(new Object [] { "Promote Collaboration - Target",		"GPI", "GPI_OPEN", 	new Boolean(false) }),
		(new Object [] { "Promote Collaboration - Target",		"GPI", "GPI_IND", 	new Boolean(true)  }),
		(new Object [] { "Promote Collaboration - Target",		"GPI", "GPI_COMP", 	new Boolean(true)  }),
		(new Object [] { "Promote Collaboration - Target",		"GPI", "GPI_INTD", 	new Boolean(false) }),

//		(new Object [] { "Influence Others - Target",						"GPI", "GPI_EGO", 	new Boolean(true)  }),

		(new Object [] { "Build Relationships - Target",		"GPI", "GPI_INFL", 	new Boolean(false) }),
		(new Object [] { "Build Relationships - Target",		"GPI", "GPI_EGO", 	new Boolean(true)  }),
		(new Object [] { "Build Relationships - Target",		"GPI", "GPI_MAN", 	new Boolean(true)  }),
		(new Object [] { "Build Relationships - Target",		"GPI", "GPI_INTI", 	new Boolean(true)  }),
		(new Object [] { "Build Relationships - Target",		"GPI", "GPI_PA", 	new Boolean(true)  }),
		(new Object [] { "Build Relationships - Target",		"GPI", "GPI_SO", 	new Boolean(false) }),
		(new Object [] { "Build Relationships - Target",		"GPI", "GPI_CONS",	new Boolean(false) }),
		(new Object [] { "Build Relationships - Target",		"GPI", "GPI_EMP", 	new Boolean(false) }),
		(new Object [] { "Build Relationships - Target",		"GPI", "GPI_AST", 	new Boolean(false) }),
		(new Object [] { "Build Relationships - Target",		"GPI", "GPI_OPEN", 	new Boolean(false) }),
		(new Object [] { "Build Relationships - Target",		"GPI", "GPI_INTD", 	new Boolean(false) }),

		(new Object [] { "Adapt and Learn - Target",			"GPI", "GPI_ADPT", 	new Boolean(false) }),
		(new Object [] { "Adapt and Learn - Target",			"GPI", "GPI_ST",	new Boolean(false) }),

		(new Object [] { "Inspire Trust - Target",				"GPI", "GPI_MAN", 	new Boolean(true)  }),
		(new Object [] { "Inspire Trust - Target",				"GPI", "GPI_DUT", 	new Boolean(false) }),
		(new Object [] { "Inspire Trust - Target",				"GPI", "GPI_RESP", 	new Boolean(false) }),


		// ------------- Readiness Assessment - BUL -------------
		(new Object [] { "Manages Complexity - KFLA",			"Cogs",	"RAVENSB", 	new Boolean(false)  }),
		(new Object [] { "Manages Complexity - KFLA",			"Cogs",	"WGE_CT", 	new Boolean(false)  }),
		
		(new Object [] { "Decision Quality - KFLA",				"Cogs",	"RAVENSB", 	new Boolean(false)  }),
		(new Object [] { "Decision Quality - KFLA",				"Cogs",	"WGE_CT", 	new Boolean(false)  }),
		
		(new Object [] { "Global Perspective - KFLA",			"GPI",  "GPI_INOV",	new Boolean(false) }),
		(new Object [] { "Global Perspective - KFLA",			"GPI",  "GPI_TF",	new Boolean(false) }),
		(new Object [] { "Global Perspective - KFLA",			"GPI",  "GPI_VIS",	new Boolean(false) }),
		(new Object [] { "Global Perspective - KFLA",			"GPI",  "GPI_RISK",	new Boolean(false) }),
		(new Object [] { "Global Perspective - KFLA",			"GPI",  "GPI_ADPT",	new Boolean(false) }),
		
		// No testing for Cultivates Innovation - KFLA

		(new Object [] { "Strategic Mindset - KFLA",			"Cogs",	"RAVENSB", 	new Boolean(false)  }),
		(new Object [] { "Strategic Mindset - KFLA",			"GPI",  "GPI_TF",	new Boolean(false) }),
		(new Object [] { "Strategic Mindset - KFLA",			"GPI",  "GPI_VIS",	new Boolean(false) }),
		(new Object [] { "Strategic Mindset - KFLA",			"GPI",  "GPI_AD",	new Boolean(true) }),
		
		// No testing for Financial Acumen - KFLA (Competency for Becton Dickinson only)
		
		(new Object [] { "Plans and Aligns - KFLA",				"GPI",  "GPI_WF",	new Boolean(false) }),
		(new Object [] { "Plans and Aligns - KFLA",				"GPI",  "GPI_TC",	new Boolean(false) }),
		(new Object [] { "Plans and Aligns - KFLA",				"GPI",  "GPI_EL",	new Boolean(false) }),
		(new Object [] { "Plans and Aligns - KFLA",				"GPI",  "GPI_INIT",	new Boolean(false) }),
		(new Object [] { "Plans and Aligns - KFLA",				"GPI",  "GPI_DACH",	new Boolean(false) }),
		(new Object [] { "Plans and Aligns - KFLA",				"GPI",  "GPI_COMP",	new Boolean(false) }),
		
		(new Object [] { "Ensures Accountability - KFLA",		"GPI",  "GPI_TC",	new Boolean(false) }),
		(new Object [] { "Ensures Accountability - KFLA",		"GPI",  "GPI_INFL",	new Boolean(false) }),
		(new Object [] { "Ensures Accountability - KFLA",		"GPI",  "GPI_INIT",	new Boolean(false) }),
		(new Object [] { "Ensures Accountability - KFLA",		"GPI",  "GPI_DACH",	new Boolean(false) }),
		(new Object [] { "Ensures Accountability - KFLA",		"GPI",  "GPI_RISK",	new Boolean(false) }),
		
		(new Object [] { "Drives Results - KFLA",				"GPI",  "GPI_WF",	new Boolean(false) }),
		(new Object [] { "Drives Results - KFLA",				"GPI",  "GPI_TC",	new Boolean(false) }),
		(new Object [] { "Drives Results - KFLA",				"GPI",  "GPI_EL",	new Boolean(false) }),
		(new Object [] { "Drives Results - KFLA",				"GPI",  "GPI_INIT",	new Boolean(false) }),
		(new Object [] { "Drives Results - KFLA",				"GPI",  "GPI_DACH",	new Boolean(false) }),
		(new Object [] { "Drives Results - KFLA",				"GPI",  "GPI_COMP",	new Boolean(false) }),
		
		(new Object [] { "Develops Talent - KFLA",				"GPI",  "GPI_INTI",	new Boolean(true) }),
		(new Object [] { "Develops Talent - KFLA",				"GPI",  "GPI_CONS",	new Boolean(false) }),
		(new Object [] { "Develops Talent - KFLA",				"GPI",  "GPI_EMP",	new Boolean(false) }),
		(new Object [] { "Develops Talent - KFLA",				"GPI",  "GPI_TR",	new Boolean(false) }),
		(new Object [] { "Develops Talent - KFLA",				"GPI",  "GPI_INTD",	new Boolean(false) }),
		(new Object [] { "Develops Talent - KFLA",				"GPI",  "GPI_AST",	new Boolean(false) }),
		
		// No testing for Builds Effective Teams - KFLA
		
		(new Object [] { "Drives Engagement - KFLA",			"GPI",  "GPI_INFL",	new Boolean(false) }),
		(new Object [] { "Drives Engagement - KFLA",			"GPI",  "GPI_TC",	new Boolean(false) }),
		(new Object [] { "Drives Engagement - KFLA",			"GPI",  "GPI_AST",	new Boolean(false) }),
		
		(new Object [] { "Persuades - KFLA",					"GPI",  "GPI_TF",	new Boolean(false) }),
		(new Object [] { "Persuades - KFLA",					"GPI",  "GPI_INFL",	new Boolean(false) }),
		
		(new Object [] { "Drives Vision and Purpose - KFLA",	"GPI",  "GPI_INFL",	new Boolean(false) }),
		(new Object [] { "Drives Vision and Purpose - KFLA",	"GPI",  "GPI_OPT",	new Boolean(false) }),
		
		// Becton Dickinson only
		(new Object [] { "Collaborates - KFLA",					"GPI",  "GPI_INTD",	new Boolean(false) }),
		(new Object [] { "Collaborates - KFLA",					"GPI",  "GPI_RESP",	new Boolean(false) }),
		(new Object [] { "Collaborates - KFLA",					"GPI",  "GPI_CONS",	new Boolean(false) }),
		(new Object [] { "Collaborates - KFLA",					"GPI",  "GPI_IND",	new Boolean(true) }),
		
		(new Object [] { "Courage - KFLA",						"GPI",  "GPI_EGO",	new Boolean(true) }),
		(new Object [] { "Courage - KFLA",						"GPI",  "GPI_MAN",	new Boolean(true) }),
		(new Object [] { "Courage - KFLA",						"GPI",  "GPI_INTI",	new Boolean(true) }),
		(new Object [] { "Courage - KFLA",						"GPI",  "GPI_PA",	new Boolean(true) }),
		(new Object [] { "Courage - KFLA",						"GPI",  "GPI_INIT",	new Boolean(false) }),
		(new Object [] { "Courage - KFLA",						"GPI",  "GPI_TC",	new Boolean(false) }),
		(new Object [] { "Courage - KFLA",						"GPI",  "GPI_ADPT",	new Boolean(false) }),
		
		(new Object [] { "Manages Ambiguity - KFLA",			"GPI",  "GPI_ADPT",	new Boolean(false) }),
		(new Object [] { "Manages Ambiguity - KFLA",			"GPI",  "GPI_NA",	new Boolean(true) }),
		(new Object [] { "Manages Ambiguity - KFLA",			"GPI",  "GPI_OPT",	new Boolean(false) }),
		(new Object [] { "Manages Ambiguity - KFLA",			"GPI",  "GPI_EC",	new Boolean(false) }),
		(new Object [] { "Manages Ambiguity - KFLA",			"GPI",  "GPI_ST",	new Boolean(false) }),

		// ------------- SABIC MAC -------------
		(new Object [] { "Personal Leadership - SABIC MAC",		"GPI",	"GPI_EGO", 	new Boolean(true)   }),
		(new Object [] { "Personal Leadership - SABIC MAC",		"GPI",	"GPI_MAN", 	new Boolean(true)   }),
		(new Object [] { "Personal Leadership - SABIC MAC",		"GPI",	"GPI_RISK", new Boolean(false)  }),
		(new Object [] { "Personal Leadership - SABIC MAC",		"GPI",	"GPI_OPT",  new Boolean(false)  }),
		(new Object [] { "Personal Leadership - SABIC MAC",		"GPI",	"GPI_EC",   new Boolean(false)  }),
		(new Object [] { "Personal Leadership - SABIC MAC",		"GPI",	"GPI_ST",   new Boolean(false)  }),
		(new Object [] { "Personal Leadership - SABIC MAC",		"GPI",	"GPI_SASI", new Boolean(false)  }),
		 
		(new Object [] { "Motivate Others - SABIC MAC",			"GPI",	"GPI_SO",   new Boolean(false)  }),
		(new Object [] { "Motivate Others - SABIC MAC",			"GPI",	"GPI_OPT",  new Boolean(false)  }),
		(new Object [] { "Motivate Others - SABIC MAC",			"GPI",	"GPI_TR",   new Boolean(false)  }),
		(new Object [] { "Motivate Others - SABIC MAC",			"GPI",	"GPI_AST",  new Boolean(false)  }),

		// No testing for Develop and Empower People - SABIC MAC 

		(new Object [] { "Inclusiveness - SABIC MAC",			"GPI",	"GPI_TA",   new Boolean(false)  }),
		(new Object [] { "Inclusiveness - SABIC MAC",			"GPI",	"GPI_INTI", new Boolean(true)   }),
		(new Object [] { "Inclusiveness - SABIC MAC",			"GPI",	"GPI_TR",   new Boolean(false)  }),
		(new Object [] { "Inclusiveness - SABIC MAC",			"GPI",	"GPI_OPEN", new Boolean(false)  }),
		(new Object [] { "Inclusiveness - SABIC MAC",			"GPI",	"GPI_INTD", new Boolean(false)  }),
		 
		(new Object [] { "Leading Change - SABIC MAC",			"GPI",	"GPI_TC",   new Boolean(false)  }),
		(new Object [] { "Leading Change - SABIC MAC",			"GPI",	"GPI_INFL", new Boolean(false)  }),
		(new Object [] { "Leading Change - SABIC MAC",			"GPI",	"GPI_INIT", new Boolean(false)  }),
		(new Object [] { "Leading Change - SABIC MAC",			"GPI",	"GPI_RISK", new Boolean(false)  }),

		(new Object [] { "Strategic Focus - SABIC MAC",			"Cogs",	"RAVENSB", 	new Boolean(false)  }),
		(new Object [] { "Strategic Focus - SABIC MAC",			"GPI",	"GPI_TF",   new Boolean(false)  }),
		(new Object [] { "Strategic Focus - SABIC MAC",			"GPI",	"GPI_INOV", new Boolean(false)  }),
		(new Object [] { "Strategic Focus - SABIC MAC",			"GPI",	"GPI_VIS",  new Boolean(false)  }),

		// No testing for Business Acumen - SABIC MAC
		 
		(new Object [] { "Drive for Results - SABIC MAC",		"GPI",	"GPI_TA",   new Boolean(false)  }),
		(new Object [] { "Drive for Results - SABIC MAC",		"GPI",	"GPI_WF",   new Boolean(false)  }),
		(new Object [] { "Drive for Results - SABIC MAC",		"GPI",	"GPI_TC",   new Boolean(false)  }),
		(new Object [] { "Drive for Results - SABIC MAC",		"GPI",	"GPI_EL",   new Boolean(false)  }),
		(new Object [] { "Drive for Results - SABIC MAC",		"GPI",	"GPI_INIT", new Boolean(false)  }),
		(new Object [] { "Drive for Results - SABIC MAC",		"GPI",	"GPI_DACH", new Boolean(false)  }),
		(new Object [] { "Drive for Results - SABIC MAC",		"GPI",	"GPI_COMP", new Boolean(false)  }),
		 
		// No testing for Planning and Organizing - SABIC MAC

		// ------------- J&J BUL -------------
		(new Object [] { "Cultivate external relationships - JnJ BUL",	"GPI",	"GPI_TF",	new Boolean(false)   }),
		(new Object [] { "Cultivate external relationships - JnJ BUL",	"GPI",	"GPI_INFL",	new Boolean(false)   }),
		(new Object [] { "Cultivate external relationships - JnJ BUL",	"GPI",	"GPI_SO",	new Boolean(false)   }),
		(new Object [] { "Cultivate external relationships - JnJ BUL",	"GPI",	"GPI_INTI",	new Boolean(true)    }),
		(new Object [] { "Cultivate external relationships - JnJ BUL",	"GPI",	"GPI_CONS",	new Boolean(false)   }),
		(new Object [] { "Cultivate external relationships - JnJ BUL",	"GPI",	"GPI_EMP",	new Boolean(false)   }),
		(new Object [] { "Cultivate external relationships - JnJ BUL",	"GPI",	"GPI_TR",	new Boolean(false)   }),

		(new Object [] { "Be insight-driven - JnJ BUL",					"GPI",	"GPI_TF",	new Boolean(false)   }),
		(new Object [] { "Be insight-driven - JnJ BUL",					"GPI",	"GPI_VIS",	new Boolean(false)   }),
		(new Object [] { "Be insight-driven - JnJ BUL",					"GPI",	"GPI_AD",	new Boolean(true)    }),
		(new Object [] { "Be insight-driven - JnJ BUL",					"GPI",	"GPI_INOV",	new Boolean(false)   }),
				
		(new Object [] { "Forge internal collaboration - JnJ BUL",		"GPI",	"GPI_TF",	new Boolean(false)   }),
		(new Object [] { "Forge internal collaboration - JnJ BUL",		"GPI",	"GPI_INFL",	new Boolean(false)   }),
		(new Object [] { "Forge internal collaboration - JnJ BUL",		"GPI",	"GPI_SO",	new Boolean(false)   }),
				
		(new Object [] { "Translate insights - JnJ BUL",				"GPI",	"GPI_TF",	new Boolean(false)   }),
		(new Object [] { "Translate insights - JnJ BUL",				"GPI",	"GPI_VIS",	new Boolean(false)   }),
		(new Object [] { "Translate insights - JnJ BUL",				"GPI",	"GPI_AD",	new Boolean(true)    }),
		(new Object [] { "Translate insights - JnJ BUL",				"GPI",	"GPI_INOV",	new Boolean(false)   }),
		
		(new Object [] { "Challenge the status-quo - JnJ BUL",			"GPI",	"GPI_TC",	new Boolean(false)   }),
		(new Object [] { "Challenge the status-quo - JnJ BUL",			"GPI",	"GPI_INFL",	new Boolean(false)   }),
		(new Object [] { "Challenge the status-quo - JnJ BUL",			"GPI",	"GPI_INIT",	new Boolean(false)   }),
		(new Object [] { "Challenge the status-quo - JnJ BUL",			"GPI",	"GPI_DACH",	new Boolean(false)   }),
		(new Object [] { "Challenge the status-quo - JnJ BUL",			"GPI",	"GPI_RISK",	new Boolean(false)   }),
		
		// No testing for Take and manage risks - JnJ BUL		
		
		(new Object [] { "Take ownership for talent - JnJ BUL",			"GPI",	"GPI_ADPT",	new Boolean(false)   }),
		(new Object [] { "Take ownership for talent - JnJ BUL",			"GPI",	"GPI_NA",	new Boolean(true)    }),
		(new Object [] { "Take ownership for talent - JnJ BUL",			"GPI",	"GPI_OPT",	new Boolean(false)   }),
		(new Object [] { "Take ownership for talent - JnJ BUL",			"GPI",	"GPI_EC",	new Boolean(false)   }),
		(new Object [] { "Take ownership for talent - JnJ BUL",			"GPI",	"GPI_ST",	new Boolean(false)   }),
		(new Object [] { "Take ownership for talent - JnJ BUL",			"GPI",	"GPI_SASI",	new Boolean(false)   }),
		
		(new Object [] { "Diversity and inclusion - JnJ BUL",			"GPI",	"GPI_TF",	new Boolean(false)   }),
		(new Object [] { "Diversity and inclusion - JnJ BUL",			"GPI",	"GPI_INFL",	new Boolean(false)   }),
		(new Object [] { "Diversity and inclusion - JnJ BUL",			"GPI",	"GPI_SO",	new Boolean(false)   }),
		
		(new Object [] { "Transparent constructive conversations - JnJ BUL",	"GPI",	"GPI_TF",	new Boolean(false)   }),
		(new Object [] { "Transparent constructive conversations - JnJ BUL",	"GPI",	"GPI_INFL",	new Boolean(false)   }),
		(new Object [] { "Transparent constructive conversations - JnJ BUL",	"GPI",	"GPI_SO",	new Boolean(false)   }),
		(new Object [] { "Transparent constructive conversations - JnJ BUL",	"GPI",	"GPI_INTI",	new Boolean(true)    }),
		(new Object [] { "Transparent constructive conversations - JnJ BUL",	"GPI",	"GPI_CONS",	new Boolean(false)   }),
		(new Object [] { "Transparent constructive conversations - JnJ BUL",	"GPI",	"GPI_EMP",	new Boolean(false)   }),
		(new Object [] { "Transparent constructive conversations - JnJ BUL",	"GPI",	"GPI_TR",	new Boolean(false)   }),
		
		(new Object [] { "Empower people - JnJ BUL",					"GPI",	"GPI_WF",	new Boolean(false)   }),
		(new Object [] { "Empower people - JnJ BUL",					"GPI",	"GPI_TC",	new Boolean(false)   }),
		(new Object [] { "Empower people - JnJ BUL",					"GPI",	"GPI_EL",	new Boolean(false)   }),
		(new Object [] { "Empower people - JnJ BUL",					"GPI",	"GPI_INIT",	new Boolean(false)   }),
		(new Object [] { "Empower people - JnJ BUL",					"GPI",	"GPI_DACH",	new Boolean(false)   }),
		(new Object [] { "Empower people - JnJ BUL",					"GPI",	"GPI_COMP",	new Boolean(false)   }),
		(new Object [] { "Empower people - JnJ BUL",					"GPI",	"GPI_INFL",	new Boolean(false)   }),
		(new Object [] { "Empower people - JnJ BUL",					"GPI",	"GPI_SO",	new Boolean(false)   }),
		(new Object [] { "Empower people - JnJ BUL",					"GPI",	"GPI_OPT",	new Boolean(false)   }),
		
		// No testing for Global and enterprise-wide mindset - JnJ BUL
		
		(new Object [] { "Balance strategic choices - JnJ BUL",			"GPI",	"GPI_TF",	new Boolean(false)   }),
		(new Object [] { "Balance strategic choices - JnJ BUL",			"GPI",	"GPI_VIS",	new Boolean(false)   }),
		(new Object [] { "Balance strategic choices - JnJ BUL",			"GPI",	"GPI_AD",	new Boolean(true)    }),
		
		// No testing for Credo - JnJ BUL

		// ------------- J&J SEA -------------
		
		// No testing for Cultivate external relationships - JnJ SEA
		
		(new Object [] { "Be insight-driven - JnJ SEA",					"Cogs",	"RAVENSB",	new Boolean(false)	 }),
		
		// No testing for Forge internal collaboration - JnJ SEA
		
		(new Object [] { "Translate insights - JnJ SEA",				"Cogs",	"RAVENSB",	new Boolean(false)	 }),
		(new Object [] { "Translate insights - JnJ SEA",				"GPI",	"GPI_TF",	new Boolean(false)   }),
		(new Object [] { "Translate insights - JnJ SEA",				"GPI",	"GPI_VIS",	new Boolean(false)   }),
		(new Object [] { "Translate insights - JnJ SEA",				"GPI",	"GPI_INOV",	new Boolean(false)   }),

		(new Object [] { "Challenge the status-quo - JnJ SEA",			"GPI",	"GPI_TC",	new Boolean(false)   }),
		(new Object [] { "Challenge the status-quo - JnJ SEA",			"GPI",	"GPI_INFL",	new Boolean(false)   }),
		(new Object [] { "Challenge the status-quo - JnJ SEA",			"GPI",	"GPI_INIT",	new Boolean(false)   }),
		(new Object [] { "Challenge the status-quo - JnJ SEA",			"GPI",	"GPI_DACH",	new Boolean(false)   }),
		(new Object [] { "Challenge the status-quo - JnJ SEA",			"GPI",	"GPI_RISK",	new Boolean(false)   }),
		(new Object [] { "Challenge the status-quo - JnJ SEA",			"GPI",	"GPI_ADPT",	new Boolean(false)   }),
		(new Object [] { "Challenge the status-quo - JnJ SEA",			"GPI",	"GPI_NA",	new Boolean(true)    }),
		(new Object [] { "Challenge the status-quo - JnJ SEA",			"GPI",	"GPI_ST",	new Boolean(false)   }),
		
		(new Object [] { "Take and manage risks - JnJ SEA",				"Cogs",	"RAVENSB",	new Boolean(false)	 }),
		
		(new Object [] { "Take ownership for talent - JnJ SEA",			"GPI",	"GPI_ADPT",	new Boolean(false)   }),
		(new Object [] { "Take ownership for talent - JnJ SEA",			"GPI",	"GPI_NA",	new Boolean(true)    }),
		(new Object [] { "Take ownership for talent - JnJ SEA",			"GPI",	"GPI_ST",	new Boolean(false)   }),
		
		// No testing for Diversity and inclusion - JnJ SEA
		
		(new Object [] { "Transparent constructive conversations - JnJ SEA",	"GPI",	"GPI_TF",	new Boolean(false)   }),
		(new Object [] { "Transparent constructive conversations - JnJ SEA",	"GPI",	"GPI_INFL",	new Boolean(false)   }),
		
		(new Object [] { "Empower people - JnJ SEA",					"GPI",	"GPI_TC",	new Boolean(false)   }),
		(new Object [] { "Empower people - JnJ SEA",					"GPI",	"GPI_EL",	new Boolean(false)   }),
		(new Object [] { "Empower people - JnJ SEA",					"GPI",	"GPI_INIT",	new Boolean(false)   }),
		(new Object [] { "Empower people - JnJ SEA",					"GPI",	"GPI_DACH",	new Boolean(false)   }),
		(new Object [] { "Empower people - JnJ SEA",					"GPI",	"GPI_INFL",	new Boolean(false)   }),
		(new Object [] { "Empower people - JnJ SEA",					"GPI",	"GPI_OPT",	new Boolean(false)   }),
		
		// No testing for Global and enterprise-wide mindset - JnJ SEA
		
		(new Object [] { "Balance strategic choices - JnJ SEA",			"GPI",	"GPI_TC",	new Boolean(false)   }),
		(new Object [] { "Balance strategic choices - JnJ SEA",			"GPI",	"GPI_EL",	new Boolean(false)   }),
		(new Object [] { "Balance strategic choices - JnJ SEA",			"GPI",	"GPI_INIT",	new Boolean(false)   }),
		(new Object [] { "Balance strategic choices - JnJ SEA",			"GPI",	"GPI_DACH",	new Boolean(false)   }),

		(new Object [] { "Credo - JnJ SEA",								"GPI",	"GPI_EGO",	new Boolean(true)    }),
		(new Object [] { "Credo - JnJ SEA",								"GPI",	"GPI_MAN",	new Boolean(true)    }),
		(new Object [] { "Credo - JnJ SEA",								"GPI",	"GPI_INTI",	new Boolean(true)    }),
		(new Object [] { "Credo - JnJ SEA",								"GPI",	"GPI_PA",	new Boolean(true)    }),
		(new Object [] { "Credo - JnJ SEA",								"GPI",	"GPI_DUT",	new Boolean(false)   }),

		(new Object [] { "Innovation - JnJ SEA",						"GPI",	"GPI_INOV",	new Boolean(false)    }),
		(new Object [] { "Innovation - JnJ SEA",						"GPI",	"GPI_INIT",	new Boolean(false)    }),
		(new Object [] { "Innovation - JnJ SEA",						"GPI",	"GPI_DACH",	new Boolean(false)    }),
		(new Object [] { "Innovation - JnJ SEA",						"GPI",	"GPI_SC",	new Boolean(false)    }),

		
		// ------------- Coca-Cola Hellenic MLL -------------
		// No testing for Attracts Top Talent - CCM
		
		(new Object [] { "Develops Talent - CCM",						"GPI",	"GPI_CONS",	new Boolean(false)   }),
		(new Object [] { "Develops Talent - CCM",						"GPI",	"GPI_RISK",	new Boolean(false)   }),
		(new Object [] { "Develops Talent - CCM",						"GPI",	"GPI_MIC",	new Boolean(true)    }),

		(new Object [] { "Builds Effective Teams - CCM",				"GPI",	"GPI_CONS",	new Boolean(false)   }),
		(new Object [] { "Builds Effective Teams - CCM",				"GPI",	"GPI_OPEN",	new Boolean(false)   }),
		(new Object [] { "Builds Effective Teams - CCM",				"GPI",	"GPI_INFL",	new Boolean(false)   }),

		(new Object [] { "Drives Engagement - CCM",						"GPI",	"GPI_CONS",	new Boolean(false)   }),
		(new Object [] { "Drives Engagement - CCM",						"GPI",	"GPI_TR",	new Boolean(false)   }),
		(new Object [] { "Drives Engagement - CCM",						"GPI",	"GPI_MIC",	new Boolean(true)    }),

		(new Object [] { "Strategic Mindset - CCM",						"GPI",	"GPI_INOV",	new Boolean(false)   }),
		(new Object [] { "Strategic Mindset - CCM",						"GPI",	"GPI_RISK",	new Boolean(false)   }),
		(new Object [] { "Strategic Mindset - CCM",						"GPI",	"GPI_VIS",	new Boolean(false)   }),
		(new Object [] { "Strategic Mindset - CCM",						"Cogs",	"RAVENSB",	new Boolean(false)   }),

		(new Object [] { "Drives Vision and Purpose - CCM",				"GPI",	"GPI_INFL",	new Boolean(false)   }),
		(new Object [] { "Drives Vision and Purpose - CCM",				"GPI",	"GPI_VIS",	new Boolean(false)   }),
		(new Object [] { "Drives Vision and Purpose - CCM",				"GPI",	"GPI_OPT",	new Boolean(false)   }),

		(new Object [] { "Courage - CCM",								"GPI",	"GPI_DACH",	new Boolean(false)   }),
		(new Object [] { "Courage - CCM",								"GPI",	"GPI_SC",	new Boolean(false)   }),
		(new Object [] { "Courage - CCM",								"GPI",	"GPI_PA",	new Boolean(true)    }),

		// No testing for Financial Acumen - CCM

		(new Object [] { "Decision Quality - CCM",						"Cogs",	"RAVENSB",	new Boolean(false)   }),
		(new Object [] { "Decision Quality - CCM",						"GPI",	"GPI_TF",	new Boolean(false)   }),

		(new Object [] { "Drives Results - CCM",						"GPI",	"GPI_DACH",	new Boolean(false)   }),
		(new Object [] { "Drives Results - CCM",						"GPI",	"GPI_INIT",	new Boolean(false)   }),
		(new Object [] { "Drives Results - CCM",						"GPI",	"GPI_EL",	new Boolean(false)   }),

		(new Object [] { "Manages Complexity - CCM",					"Cogs",	"RAVENSB",	new Boolean(false)   }),
		(new Object [] { "Manages Complexity - CCM",					"GPI",	"GPI_TA",	new Boolean(false)   }),

		(new Object [] { "Resourcefulness - CCM",						"GPI",	"GPI_WF",	new Boolean(false)   }),
		(new Object [] { "Resourcefulness - CCM",						"GPI",	"GPI_AD",	new Boolean(false)   }),

		(new Object [] { "Plans and Aligns - CCM",						"GPI",	"GPI_VIS",	new Boolean(false)   }),
		(new Object [] { "Plans and Aligns - CCM",						"GPI",	"GPI_WF",	new Boolean(false)   }),
		(new Object [] { "Plans and Aligns - CCM",						"GPI",	"GPI_ADPT",	new Boolean(false)   }),

		// No testing for Ensures Accountability - CCM

		// No testing for Business Insight - CCM

		(new Object [] { "Cultivates Innovation - CCM",					"GPI",	"GPI_INOV",	new Boolean(false)   }),
		(new Object [] { "Cultivates Innovation - CCM",					"GPI",	"GPI_TA",	new Boolean(false)   }),

		// No testing for Customer Focus - CCM

		(new Object [] { "Collaborates - CCM",							"GPI",	"GPI_INTD",	new Boolean(false)   }),
		(new Object [] { "Collaborates - CCM",							"GPI",	"GPI_IND",	new Boolean(true)    }),
		(new Object [] { "Collaborates - CCM",							"GPI",	"GPI_MAN",	new Boolean(true)    }),


		// ------------- BGB Level 6 -------------
		// No testing for Exceed Customer Needs - BGB_L6
		
		(new Object [] { "Plans for Delivery - BGB_L6",					"Cogs",	"RAVENSB",	new Boolean(false)   }),
		(new Object [] { "Plans for Delivery - BGB_L6",					"GPI",	"GPI_TF",	new Boolean(false)   }),
		(new Object [] { "Plans for Delivery - BGB_L6",					"GPI",	"GPI_VIS",	new Boolean(false)   }),
		
		(new Object [] { "Effective Decisionss - BGB_L6",				"Cogs",	"RAVENSB",	new Boolean(false)   }),
		(new Object [] { "Effective Decisionss - BGB_L6",				"GPI",	"GPI_TF",	new Boolean(false)   }),
		(new Object [] { "Effective Decisionss - BGB_L6",				"GPI",	"GPI_VIS",	new Boolean(false)   }),
		(new Object [] { "Effective Decisionss - BGB_L6",				"GPI",	"GPI_AD",	new Boolean(true)    }),
		
		// No testing for Develops Others - BGB_L6
		
		// No testing for Communicates Vision - BGB_L6
		
		(new Object [] { "Feedback - BGB_L6",							"GPI",	"GPI_ADPT",	new Boolean(false)   }),
		(new Object [] { "Feedback - BGB_L6",							"GPI",	"GPI_NA",	new Boolean(true)    }),
		(new Object [] { "Feedback - BGB_L6",							"GPI",	"GPI_OPT",	new Boolean(false)   }),
		(new Object [] { "Feedback - BGB_L6",							"GPI",	"GPI_EC",	new Boolean(false)   }),
		(new Object [] { "Feedback - BGB_L6",							"GPI",	"GPI_ST",	new Boolean(false)   }),
		
		(new Object [] { "Leads - BGB_L6",								"GPI",	"GPI_ADPT",	new Boolean(false)   }),
		(new Object [] { "Leads - BGB_L6",								"GPI",	"GPI_NA",	new Boolean(true)    }),
		(new Object [] { "Leads - BGB_L6",								"GPI",	"GPI_OPT",	new Boolean(false)   }),
		(new Object [] { "Leads - BGB_L6",								"GPI",	"GPI_EC",	new Boolean(false)   }),
		(new Object [] { "Leads - BGB_L6",								"GPI",	"GPI_ST",	new Boolean(false)   }),
		
		(new Object [] { "Accountability - BGB_L6",						"GPI",	"GPI_TC",	new Boolean(false)   }),
		(new Object [] { "Accountability - BGB_L6",						"GPI",	"GPI_INFL",	new Boolean(false)   }),
		(new Object [] { "Accountability - BGB_L6",						"GPI",	"GPI_INIT",	new Boolean(false)   }),
		(new Object [] { "Accountability - BGB_L6",						"GPI",	"GPI_DACH",	new Boolean(false)   }),
		(new Object [] { "Accountability - BGB_L6",						"GPI",	"GPI_RISK",	new Boolean(false)   }),
		
		(new Object [] { "Establishes Trust - BGB_L6",					"GPI",	"GPI_EGO",	new Boolean(true)    }),
		(new Object [] { "Establishes Trust - BGB_L6",					"GPI",	"GPI_MAN",	new Boolean(true)    }),
		(new Object [] { "Establishes Trust - BGB_L6",					"GPI",	"GPI_INTI",	new Boolean(true)    }),
		(new Object [] { "Establishes Trust - BGB_L6",					"GPI",	"GPI_PA",	new Boolean(true)    }),
		(new Object [] { "Establishes Trust - BGB_L6",					"GPI",	"GPI_DUT",	new Boolean(false)   }),

		// ------------- CEO (ALR with GPI) -------------
		// No testing for Customer Focus - CEO
		
		// No testing for Financial Acumen - CEO
		
		(new Object [] { "Manages Complexity - CEO",					"Cogs",	"RAVENSB",	new Boolean(false)   }),
		(new Object [] { "Manages Complexity - CEO",					"Cogs", "WGE_CT",	new Boolean(false)   }),
		(new Object [] { "Manages Complexity - CEO",					"GPI",	"GPI_TF",	new Boolean(false)   }),
		(new Object [] { "Manages Complexity - CEO",					"GPI",	"GPI_INOV",	new Boolean(false)   }),
		
		(new Object [] { "Balances Stakeholders - CEO",					"GPI",	"GPI_INTD",	new Boolean(false)   }),
		(new Object [] { "Balances Stakeholders - CEO",					"GPI",	"GPI_INFL",	new Boolean(false)   }),
		
		(new Object [] { "Global Perspective - CEO",					"GPI",	"GPI_OPEN",	new Boolean(false)   }),
		(new Object [] { "Global Perspective - CEO",					"GPI",	"GPI_VIS",	new Boolean(false)   }),
		
		(new Object [] { "Cultivates Innovation - CEO",					"GPI",	"GPI_INOV",	new Boolean(false)   }),
		(new Object [] { "Cultivates Innovation - CEO",					"GPI",	"GPI_VIS",	new Boolean(false)   }),
		(new Object [] { "Cultivates Innovation - CEO",					"GPI",	"GPI_RISK",	new Boolean(false)   }),

		(new Object [] { "Strategic Vision - CEO",						"GPI",	"GPI_TF",	new Boolean(false)   }),
		(new Object [] { "Strategic Vision - CEO",						"GPI",	"GPI_INOV",	new Boolean(false)   }),
		(new Object [] { "Strategic Vision - CEO",						"GPI",	"GPI_VIS",	new Boolean(false)   }),

		(new Object [] { "Aligns Execution - CEO",						"GPI",	"GPI_DACH",	new Boolean(false)   }),

		(new Object [] { "Ensures Accountability - CEO",				"GPI",	"GPI_AD",	new Boolean(false)   }),
		(new Object [] { "Ensures Accountability - CEO",				"GPI",	"GPI_DACH",	new Boolean(false)   }),
		(new Object [] { "Ensures Accountability - CEO",				"GPI",	"GPI_RESP",	new Boolean(false)   }),

		(new Object [] { "Drives Results - CEO",						"GPI",	"GPI_DACH",	new Boolean(false)   }),
		(new Object [] { "Drives Results - CEO",						"GPI",	"GPI_INIT",	new Boolean(false)   }),
		(new Object [] { "Drives Results - CEO",						"GPI",	"GPI_EL",	new Boolean(false)   }),
		(new Object [] { "Drives Results - CEO",						"GPI",	"GPI_COMP",	new Boolean(false)   }),

		(new Object [] { "Manages Conflict - CEO",						"GPI",	"GPI_EGO",	new Boolean(true)    }),
		(new Object [] { "Manages Conflict - CEO",						"GPI",	"GPI_EMP",	new Boolean(false)   }),
		(new Object [] { "Manages Conflict - CEO",						"GPI",	"GPI_OPEN",	new Boolean(false)   }),
		(new Object [] { "Manages Conflict - CEO",						"GPI",	"GPI_AST",	new Boolean(false)   }),

		(new Object [] { "Navigates Networks - CEO",					"GPI",	"GPI_INTD",	new Boolean(false)   }),
		(new Object [] { "Navigates Networks - CEO",					"GPI",	"GPI_SO",	new Boolean(false)   }),

		(new Object [] { "Develops Talent - CEO",						"GPI",	"GPI_TR",	new Boolean(false)   }),
		(new Object [] { "Develops Talent - CEO",						"GPI",	"GPI_EMP",	new Boolean(false)   }),
		(new Object [] { "Develops Talent - CEO",						"GPI",	"GPI_OPEN",	new Boolean(false)   }),
		(new Object [] { "Develops Talent - CEO",						"GPI",	"GPI_INFL",	new Boolean(false)   }),

		(new Object [] { "Builds Effective Teams - CEO",				"GPI",	"GPI_MIC",	new Boolean(true)    }),
		(new Object [] { "Builds Effective Teams - CEO",				"GPI",	"GPI_CONS",	new Boolean(false)   }),
		(new Object [] { "Builds Effective Teams - CEO",				"GPI",	"GPI_TR",	new Boolean(false)   }),
		(new Object [] { "Builds Effective Teams - CEO",				"GPI",	"GPI_INTD",	new Boolean(false)   }),

		(new Object [] { "Communicates Effectively - CEO",				"GPI",	"GPI_NA",	new Boolean(true)    }),
		(new Object [] { "Communicates Effectively - CEO",				"GPI",	"GPI_OPEN",	new Boolean(false)   }),
		(new Object [] { "Communicates Effectively - CEO",				"GPI",	"GPI_SO",	new Boolean(false)   }),

		(new Object [] { "Engages and Inspires - CEO",					"GPI",	"GPI_SC",	new Boolean(false)   }),
		(new Object [] { "Engages and Inspires - CEO",					"GPI",	"GPI_AST",	new Boolean(false)   }),
		(new Object [] { "Engages and Inspires - CEO",					"GPI",	"GPI_INFL",	new Boolean(false)   }),
		(new Object [] { "Engages and Inspires - CEO",					"GPI",	"GPI_TC",	new Boolean(false)   }),

		(new Object [] { "Persuades - CEO",								"GPI",	"GPI_OPEN",	new Boolean(false)   }),
		(new Object [] { "Persuades - CEO",								"GPI",	"GPI_INFL",	new Boolean(false)   }),

		(new Object [] { "Courage - CEO",								"GPI",	"GPI_RESP",	new Boolean(false)   }),
		(new Object [] { "Courage - CEO",								"GPI",	"GPI_ADPT",	new Boolean(false)   }),
		(new Object [] { "Courage - CEO",								"GPI",	"GPI_RISK",	new Boolean(false)   }),

		(new Object [] { "Instills Trust - CEO",						"GPI",	"GPI_TR",	new Boolean(false)   }),

		// No testing for Demonstrates Self-Awareness - CEO

		(new Object [] { "Manages Ambiguity - CEO",						"GPI",	"GPI_EC",	new Boolean(false)   }),
		(new Object [] { "Manages Ambiguity - CEO",						"GPI",	"GPI_OPT",	new Boolean(false)   }),
		(new Object [] { "Manages Ambiguity - CEO",						"GPI",	"GPI_ST",	new Boolean(false)   }),
		(new Object [] { "Manages Ambiguity - CEO",						"GPI",	"GPI_ADPT",	new Boolean(false)   }),

		(new Object [] { "Nimble Learning - CEO",						"GPI",	"GPI_TF",	new Boolean(false)   }),
		(new Object [] { "Nimble Learning - CEO",						"GPI",	"GPI_INOV",	new Boolean(false)   }),
		(new Object [] { "Nimble Learning - CEO",						"GPI",	"GPI_ADPT",	new Boolean(false)   }),

		(new Object [] { "Being Resilient - CEO",						"GPI",	"GPI_SC",	new Boolean(false)   }),
		(new Object [] { "Being Resilient - CEO",						"GPI",	"GPI_OPT",	new Boolean(false)   }),
		(new Object [] { "Being Resilient - CEO",						"GPI",	"GPI_ADPT",	new Boolean(false)   }),

		(new Object [] { "Situational Adaptability - CEO",				"GPI",	"GPI_ADPT",	new Boolean(false)   }),
		(new Object [] { "Situational Adaptability - CEO",				"GPI",	"GPI_RISK",	new Boolean(false)   })

	};

	
//	static private final String SPSS_VALUES_STRING = "'WG_A','WG_C','RAVENS_T','RAVENSSF','RAVENSB','WATSON','WG_AS','WGE_CT','WESMAN','PGPI_RDI','COGSDONE'";
//	static private final String IG_INST_NO_LEI = "'rv', 'wg', 'gpil'";
//	static private final String IG_INST_WITH_LEI = "'rv', 'wg', 'gpil', 'lei'";

	static private HashMap<String, String> INST_SPSS_MAP =  new HashMap<String, String>();
	static
	{
		INST_SPSS_MAP.put("gpil", "GPI");
		INST_SPSS_MAP.put("lei",  "LEI");
		INST_SPSS_MAP.put("rv",   "RAVENSB");
		INST_SPSS_MAP.put("wg",   "WGE_CT");
	}

	//
	// Static methods.
	//


	/**
	 * QnD method that converts a float to a 2 decimal String
	 *
	 * @param input - A double containing a score
	 * @return A String containing the formated score (n.nn)
	 * @throws Exception
	 */
	static public String scoreFloat2String(double input)
	{
//		MathContext mc = new MathContext(3, RoundingMode.HALF_UP);
//		DecimalFormat df = new DecimalFormat("0.00");
//
//		BigDecimal rslt = new BigDecimal(input, mc);
//		double scr = rslt.doubleValue();
//		
//		return df.format(scr);
		double score = Math.floor((input * 100.0) + 0.505) / 100.0;
		return String.format("%.2f", score);

	}


//	/**
//	 * Convenience method.  Allows legacy calls to the function (assumes that there is no ALP data)
//	 * @param con
//	 * @param partId
//	 * @param dnaId
//	 * @param cogsOn
//	 * @return
//	 * @throws Exception
//	 */
//	static public IGIntersectionAndOtherDataDTO fetchTestingIntersectionData(Connection con,
//																			 String partId,
//																		 	 long dnaId,
//																		 	 boolean cogsOn)
//		throws Exception
//	{
//		return fetchTestingIntersectionData(con, partId, dnaId, cogsOn, false);
//	}

	
	/*
	 * fetchTestingIntersectionData is used to get intersection related data for the
	 * summary and for the testing column.
	 * 
	 * It returns the following participant score-related data:
	 * -- Integration Grid intersections for the testing column in an ArrayList object.
	 * -- Norm Info Map (3 chars of spss name as key, V2 norm id as value)
	 * -- the RDI value
	 * 
	 * @param con - A database connection
	 * @param partId - A V2 participant ID
	 * @param dnaId - An A By D DNA ID
	 * @param cogsOn - A flag indicating if the Cogs are on in the score calcs
	 * @return An IGIntersectionAndOtherDataDTO object
	 * @throws Exception
	 */
	static public IGIntersectionAndOtherDataDTO fetchTestingIntersectionData(Connection con,
																			 String partId,
																			 long dnaId,
																			 boolean cogsOn,
																			 boolean hasAlp,
																			 boolean hasKf4d,
																			 IGKf4dDataHelper kf4dHelper)
		throws Exception
	{ 
		IGIntersectionAndOtherDataDTO ret = new IGIntersectionAndOtherDataDTO();
		
		Map<Long,String> tScores = null;
		if( hasAlp)
		{
			IGAlrDataHelper alrh = new IGAlrDataHelper(con, partId, dnaId);
			tScores = alrh.getAlrTestCompScores();
			alrh = null;
			ret.setRdi(Integer.parseInt(IGConstants.NO_RDI)); 
		}
		else if( hasKf4d)
		{
			//IGKf4dDataHelper alrh = new IGKf4dDataHelper(con, partId, dnaId);
			tScores = kf4dHelper.getKf4dTestCompScores();
			//alrh = null;
			ret.setRdi(Integer.parseInt(IGConstants.NO_RDI)); 
		}
		else
		{
			ScoreAndNormData sDat = IGScoring.fetchIGTestScoresAndNorms(con, partId, dnaId, IGConstants.NO_LEI);
			
			// Get the appropriate Testing competency scores from the constituent scores fetched above
			tScores = IGScoring.calcTestingScores(con, partId, dnaId, cogsOn, sDat);
	
			//Gavin: Adding this in for now, sometimes the RDI is returning null isntead of a number, 
			//this is causing a number format exception
			if(sDat.getRdi() != null)
			{
				ret.setRdi(Integer.parseInt(sDat.getRdi())); 
			}
			else
			{
				/* The application appears to work without the RDI */
				//throw new Exception("RDI IS NULL");
			}
		}

		// create the Testing column intersection cells
		ArrayList<DNACellDTO> tCells = new ArrayList<DNACellDTO>();
		for (Iterator<Map.Entry<Long, String>> itr=tScores.entrySet().iterator(); itr.hasNext(); )
		{
			Map.Entry<Long, String> entry = itr.next();
			DNACellDTO newCell = new DNACellDTO();
			newCell.setModuleId(IGConstants.TESTING_MOD_ID);
			newCell.setCompetencyId(entry.getKey().longValue());
			newCell.setScore(entry.getValue());
			tCells.add(newCell);
		}
		ret.setTestingCells(tCells);

		return ret;
	}


	/**
	 * fetchIGTestScoresAndNorms is a convenience method that sets up to pull only
	 * cog/GPI score data and test norm data for this participant.  Used in the
	 * Integration Grid only (no LEI data desired).
	 * 
	 * @param con - A database connection
	 * @param partId - A V2 participant ID
	 * @return A ScoreAndNormData object
	 * @throws Exception
	 */
	static public ScoreAndNormData fetchIGTestScoresAndNorms(Connection con,
															 String partId,
															 long dnaId)
		throws Exception
	{
			ScoreAndNormData ret = IGScoring.getScoreAndNormData(con, partId, dnaId, IGConstants.NO_LEI);
			
			return ret;
	}


	/**
	 * fetchIGExtractScoresAndNorms Fetch scores and norms for use in the data extract.
	 * This includes the LEI for this participant.
	 * 
	 * @param con - A database connection
	 * @param partId - A V2 participant ID
	 * @return A ScoreAndNormData object
	 * @throws Exception
	 */
	static protected ScoreAndNormData fetchIGExtractScoresAndNorms(Connection con,
																   String partId,
																   long dnaId)
		throws Exception
	{
		ScoreAndNormData ret = getScoreAndNormData(con, partId, dnaId, IGConstants.INCLUDE_LEI);
		
		return ret;
	}	


	/**
	 * fetchIGTestScoresAndNorms is a convenience method that sets up to pull cog/GPI
	 * score data and test norm data for this participant.  This method makes no
	 * assumptions about LEI data and passes through the flag.
	 * Note that this method is used internally only.
	 * 
	 * @param con - A database connection
	 * @param partId - A V2 participant ID
	 * @param leiFlag - Indicate if the LEI scores should be fetched
	 * @return A ScoreAndNormData object
	 * @throws Exception
	 */
	static private ScoreAndNormData fetchIGTestScoresAndNorms(Connection con,
															  String partId,
															  long dnaId,
															  boolean leiFlag)
		throws Exception
	{
			ScoreAndNormData ret = IGScoring.getScoreAndNormData(con, partId, dnaId, leiFlag);
			
			return ret;
	}


	/**
	 * Fetch percentile, PDI rating, and norm name for the GPI, Cognitive tests,
	 * and perhaps the LEI  for this participant.
	 * 
	 * NOTES:
	 * The data will be retrieved from the results table and only the last raw score value
	 * will be used (even if it is invalid and there are valid scores earlier).  The
	 * scores are normed and the PDI rating is calculated and both values are returned.
	 * 
	 * This routine gets all scores available, whether needed or not.  Dropping
	 * un-needed scores should be accomplished in the calling code.
	 * 
	 * If at some point we need only those scores in certain projects or modules or we
	 * need to cross-check to ensure that the qre is done, then we will have to refine
	 * this logic
	 *
	 * We pick up the spec pop norm name used for every instrument and return all of them
	 * in a map.  The key is the SPSS Value and the value is the norm name used.  They
	 * may or may not be used in the caller.
	 *  
	 * @param con - A database connection
	 * @param partId - A V2 participant ID
	 * @param leiFlag - Indicate if the LEI scores should be fetched
	 * @return A ScoreAndNormData object
	 * @throws Exception
	 */
	/*
	 * This is now a convenience method signature.  all of the "old" calls use this signature.
	 * Only the ALP logic uses the new signature directly
	 */
	static protected ScoreAndNormData getScoreAndNormData(Connection con,
			String partId,
			long dnaId,
			boolean includeLEI)
	throws Exception
	{
		return getScoreAndNormData(con, partId, dnaId, includeLEI, false, false);
	}


	/**
	 * getScoreAndNormData - This is the place where all of the work gets done.  The above signature was created to
	 *                       support all of the existing calls when the "inAlp" parameter was added to support ALP.
	 * @param con
	 * @param partId
	 * @param dnaId
	 * @param includeLEI
	 * @param inAlp
	 * @return
	 * @throws Exception
	 */
	static protected ScoreAndNormData getScoreAndNormData(Connection con,
														String partId,
														long dnaId,
														boolean includeLEI,
														boolean inAlp,
														boolean inK4d)
		throws Exception
	{
		ScoreAndNormData ret = new ScoreAndNormData();
		StringBuffer sqlQuery = new StringBuffer();
		Statement stmt = null;
		ResultSet rs = null;
		String phase = "";

		// Go get the instrument list for the project
		// need to get project Id from dna id
		SessionUser su = HelperDelegate.getSessionUserHelperHelper().createSessionUser();	
		String projectId = new Long( Utils.getProjectIdFromDNAId(su, dnaId)).toString();
		
		// Added to allow SAC II projects to collect short GPI data and score it using
		// specific norms.  This first bit sets a flag to indicate if the project is
		// SAC II.  If it is, it sets the norms for this person/project/instrument in
		// the pdi_participant_instrument_norm_map table (the pin map).  Note that this code uses the 
		// existing gpil norms and sets the pin map using "gpil" as the istrument code.
		boolean isSacII = isProjectSacII(con, projectId);
		if(isSacII)
		{
			// Set norms to defined defaults
			INormGroupHelper ngh = HelperDelegate.getNormGroupHelper("com.pdi.data.v2.helpers.delegated");
	
			// get the Defined SAC II GP & SP
			NormGroup gp = ngh.fromName(null, SAC_II_GP_NORM_NAME);
			NormGroup sp = ngh.fromName(null, SAC_II_SP_NORM_NAME);
			
			// Set the pin map row
			ngh.update(null, gp, projectId, partId, gp.getInstrumentCode());
			ngh.update(null, sp, projectId, partId, sp.getInstrumentCode());
		}
		
		// then get course list from webservice
		//ArrayList<String> courseListFromProj = new NhnWebserviceUtils()
		//					.fetchCourseListForProject(PropertyLoader.getProperty("com.pdi.listener.portal.application", "courseList.url"), projectId);
		//Only returns courses with Test Data, i think - nw
		ArrayList<String> courseListFromProj = new NhnWebserviceUtils().getFilteredProjectCourseList(projectId);
		if (courseListFromProj.size() < 1)
		{
			if (inAlp)
			{
				// Alp may or may not have a scoreable instrument (wg).  If <1 then the course is not selected
				ret.setRdi("0");
				ret.setCogsDone(true);
				ret.setNormMap(new HashMap<String, String>());
				ret.setScoreMap(new HashMap<String, ScorePair>());
				return ret;
			}
			else if (inK4d)
			{
				// KF4D may or may not have a scoreable instrument (wg/rav).  If <1 then the course is not selected
				ret.setRdi("0");
				ret.setCogsDone(true);
				ret.setNormMap(new HashMap<String, String>());
				ret.setScoreMap(new HashMap<String, ScorePair>());
				return ret;
			}
			else
			{
				// "Normal" usage
				String s = "Empty course list for project " + projectId + ".  Unable to fetch norms.";
				System.out.println ("getScoreAndNormData:  " + s + "  (NhnWebserviceUtils.fetchCourseListForProject() call)");
				throw new Exception(s);
			}
		}
		
		// has instruments... check 'em
		// build the pretty string... 
		String codeList = "";
		int index = 0;
		for(String s : courseListFromProj){;
			index++;
			codeList += "'" +s+"'";
			if(index < courseListFromProj.size()){
				codeList += ",";
			}
		}

		try
		{
			phase = "Create the statement";
		    stmt = con.createStatement();

			HashMap<String, String> normNames = new HashMap<String, String>();
			HashMap<String, ScorePair> scores = new HashMap<String, ScorePair>();
			int rdi = 0;
			// Get all the norms
			HashMap<String, Norm> norms = new HashMap<String, Norm>();
			phase = "Norm query execution";
			sqlQuery.append("SELECT pin.instrumentCode AS instCode, ");
			sqlQuery.append("       ng.name AS sPopName, ");
			sqlQuery.append("       nn.spssValue, ");
			sqlQuery.append("       nn.mean, ");
			sqlQuery.append("       nn.stdev ");
			sqlQuery.append("  FROM pdi_participant_instrument_norm_map pin ");
			sqlQuery.append("    INNER JOIN pdi_abd_dna dna ON (dna.projectId = pin.projectId ");
			sqlQuery.append("                               AND dna.dnaId = " + dnaId + " ");
			sqlQuery.append("                               AND dna.active = 1) ");
			sqlQuery.append("    LEFT JOIN pdi_norm_group ng ON (ng.normGroupId = pin.specPopNormGroupId ");
			sqlQuery.append("                               AND ng.active = 1) ");
			sqlQuery.append("    LEFT JOIN pdi_norm nn ON (nn.normGroupId = ng.normGroupId AND nn.active = 1) ");
			sqlQuery.append("      WHERE pin.participantId IN (" + partId + ", 0) ");
			sqlQuery.append("        AND pin.instrumentCode in (" + codeList + ") ");
			sqlQuery.append("        AND pin.active = 1 ");
			sqlQuery.append("  ORDER BY nn.spssValue ASC, pin.participantId DESC");

			//System.out.println("sqlQuery.. " + sqlQuery.toString());
			
			rs = stmt.executeQuery(sqlQuery.toString());
			phase = "Norm query data retrieval";
			if (! rs.isBeforeFirst())
			{
				// no norms... still nothing to do here
				// return the empty output data object
				//return ret;	// Used to fail soft.  Now fails with an Exception.
				String str = courseListFromProj.get(0).toString();
				if(courseListFromProj.size() == 1 && str.equals(IGConstants.I_CODE_FINEX)){
					// only FinX IG is the only time we want to not throw an exception. NHN-1763
					str = "";
				}else{
					throw new Exception("No norms have been set. Please set norms for all instruments in Test Data or project setup.");
				}
				
			}
			// Put the Norms and norm names into Maps
			while (rs.next())
			{
				// Save the norm name
				String instSpss =  INST_SPSS_MAP.get(rs.getString("instCode"));
				if (normNames.get(instSpss) == null)
				{
					// Not there stick it in.  Should get the first and ignore the second
					normNames.put(instSpss, rs.getString("sPopName"));
				}
				
				// Save the norm.  Same idea... picks up the first and ignores subsequent.
				String normKey = rs.getString("spssValue");
				if (norms.get(normKey) == null)
				{
					norms.put(normKey, new Norm(rs.getDouble("mean"), rs.getDouble("stdev")));
				}
			}	// End of while loop on norm result processing	

			phase = "closing ResultSet (1st time)";
			rs.close();
			sqlQuery.setLength(0);
			
			// Need to pick up the gpi scores.  The short GPI is not in the data setup for the A by D
			// (only gpil) so let's add it to the code list.  This works downstream because this is
			// the last place intsrment code is used/referenced in the scoring
			if (isSacII)
			{
				// add gpi to list of codes (shouldn't be there)
				if (codeList.length() > 0)
					codeList += ",";
				codeList += "'gpi'";
			}

		    // Get all the raw scores, score them, and put them into a Map
			phase = "Score query execution";
			sqlQuery.append("SELECT rr.spssValue AS spssValue, ");
			sqlQuery.append("       rr.rawScore AS scaleScore ");
			sqlQuery.append("  FROM results rr ");
			sqlQuery.append("  WHERE rr.participantId = " + partId + " ");
			sqlQuery.append("    AND rr.instrumentCode IN (" + codeList + ") ");
			sqlQuery.append("  ORDER BY rr.spssValue, rr.lastDate");

			rs = stmt.executeQuery(sqlQuery.toString());
			phase = "Score query data retrieval";
			if (! rs.isBeforeFirst())
			{
				// no scores... nothing to do here
				// return the empty output data object
				return ret;
			}
			// Put the scores into a Map for later use
			while (rs.next())
			{
				String spssVal = rs.getString("spssValue");

				if (spssVal.equals("PGPI_RDI"))
				{
					rdi = rs.getInt("scaleScore");
					continue;
				}
				
				Norm norm = norms.get(spssVal);
				if (norm == null)
				{
					// Cannot score... skip it
					continue;
				}
				
				// So score it already
				ScorePair sp = new ScorePair();
				double z = norm.calcZScore(rs.getDouble("scaleScore"));
				int pctl = Norm.calcIntPctlFromZ(z);
				sp.setPctl("" + pctl);
				sp.setRating(Norm.calcPdiRating(pctl));

				scores.put(spssVal, sp);
			}	// End of while loop

			// Get cogsdone
			phase = "closing ResultSet (2nd time)";
			rs.close();
			sqlQuery.setLength(0);

		    phase = "cogsDone query";
			sqlQuery.append("SELECT igr.cogsDone AS cogsDone ");
			sqlQuery.append("  FROM pdi_abd_igrid_response igr ");
			sqlQuery.append("  WHERE igr.dnaId = " + dnaId + " ");
			sqlQuery.append("    AND igr.participantId = " + partId + " ");
			
			rs = stmt.executeQuery(sqlQuery.toString());
			phase = "cogsDone retrieval";
			
			if (rs.isBeforeFirst())
			{
				rs.next();	// Single result row
				ret.setCogsDone(rs.getBoolean("cogsDone"));
			}
			else
			{
				// If there is no igr yet, default to cogs not done
				ret.setCogsDone(false);
			}
			
			// set up the rest of the return buckets
			ret.setRdi(rdi == 0 ? null : "" + rdi);
			ret.setNormMap(normNames);
			ret.setScoreMap(scores);

			return ret;
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL error for getScoreAndNormData, (" +
							"DNA ID=" + dnaId + ", " + ", partID=" + partId + ").  " +
							"phase=" + phase + ".  " +
							"SQLException: " + ex.getMessage() + ", " +
							"SQLState: " + ex.getSQLState() + ", " +
							"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (rs != null)
			{
			    try {  rs.close();  }
			    catch (SQLException sqlEx)
			    {
			    	System.out.println("ERROR: rs close in getScoreAndNormData 'finally' - " + sqlEx.getMessage());
			    }
				rs = null;
			}
			if (stmt != null)
			{
			    try {  stmt.close();  }
			    catch (SQLException sqlEx)
			    {
			    	System.out.println("ERROR: stmt close in getScoreAndNormData'finally' - " + sqlEx.getMessage());
			    }
			    stmt = null;
			}
		}		   
	}

	
	
	
	//----------------------------------------------------
	// Private classes - Used elsewhere in the IG code
	//----------------------------------------------------


	/*****************************  Score And Norm data  *****************************/

	/**
	 * ScoreAndNormData is a thin class used to transport score and norm data to
	 * classes external to within the scoring code.
	 * 
	 * Note:  This is only implemented on the server side
	 *
	 * @author		Ken Beukelman
	 */
	static protected class ScoreAndNormData
	{

		//
		// Instance data.
		//
		private String _rdi;
		private boolean _cogsDone = false;
		private Map<String, String> _normMap = new HashMap<String, String>();
		private Map<String, ScorePair> _scoreMap = new HashMap<String, ScorePair>();

		//
		// Constructors.
		//

		//
		// Instance methods.
		//

		/*****************************************************************************************/
		public String getRdi()
		{
			return _rdi;
		}

		public void setRdi(String value)
		{
			_rdi = value;
		}

		/*****************************************************************************************/
		public boolean getCogsDone()
		{
			return _cogsDone;
		}

		public void setCogsDone(boolean value)
		{
			_cogsDone = value;
		}

		/*****************************************************************************************/
		public Map<String, String> getNormMap()
		{
			return _normMap;
		}

		public void setNormMap(Map<String, String> value)
		{
			_normMap = value;
		}

		/*****************************************************************************************/
		public Map<String, ScorePair> getScoreMap()
		{
			return _scoreMap;
		}

		public void setScoreMap(Map<String, ScorePair> value)
		{
			_scoreMap = value;
		}
	}	// End protected class ScoreAndNormData



	/*****************************  Protected class ScorePair  *****************************/

	/**
	 * ScorePair is a thin class used to contain two score data - percentile and
	 *  rating - for the cogs and for the gpi
	 * 
	 * Note:  This is only implemented on the server side
	 *
	 * @author		Ken Beukelman
	 */
	static protected class ScorePair
	{

		//
		// Instance data.
		//
		private String _pctl;
		private String _rating;

		//
		// Constructors.
		//

		//
		// Instance methods.
		//

		/*****************************************************************************************/
		public String getPctl()
		{
			return _pctl;
		}

		public void setPctl(String value)
		{
			_pctl = value;
		}

		/*****************************************************************************************/
		public String getRating()
		{
			return _rating;
		}

		public void setRating(String value)
		{
			_rating = value;
		}
	}	// End protected class ScorePairs



	/*****************************  Protected class ScoreLists  *****************************/

	/**
	 * ScoreLists is a thin class used to contain two lists of
	 * IGScaleScoreData objects, one for the cogs and one for the gpi
	 * 
	 * Note:  This is only implemented on the server side
	 *
	 * @author		Ken Beukelman
	 */
	protected class ScoreLists
	{

		//
		// Instance data.
		//
		
		private ArrayList<IGScaleScoreData> _cogScores;
		private ArrayList<IGScaleScoreData> _gpiScores;

		//
		// Constructors.
		//
		protected ScoreLists()
		{
			// Nothing happens here
		}

		//
		// Instance methods.
		//

		/*****************************************************************************************/
		public ArrayList<IGScaleScoreData> getCogScores()
		{
			if (_cogScores == null)
			{
				_cogScores = new ArrayList<IGScaleScoreData>();
			}
			return _cogScores;
		}

		public void setCogScores(ArrayList<IGScaleScoreData> value)
		{
			_cogScores = value;
		}

		/*****************************************************************************************/
		public ArrayList<IGScaleScoreData> getGpiScores()
		{
			if (_gpiScores == null)
			{
				_gpiScores = new ArrayList<IGScaleScoreData>();
			}
			return _gpiScores;
		}

		public void setGpiScores(ArrayList<IGScaleScoreData> value)
		{
			_gpiScores = value;
		}
	}	// End class ScoreLists




	// Private classes - Used only on IGScoring
	

	/*****************************  Private class SpssAndFlag  *****************************/

	/**
	 * SpssAndFlag is a thin, private class used to contain some needed
	 * information about the scoring components (Cog names, GPI scale
	 * scores) for a particular competency.
	 *
	 * @author		Ken Beukelman
	 */
	static private class SpssAndFlag
	{

		//
		// Instance data.
		//
		private String _componentKey;
		private boolean _negativeFlag = false;

		//
		// Constructors.
		//
		protected SpssAndFlag()
		{
			// Nothing happens here
		}

		//
		// Instance methods.
		//

		/*****************************************************************************************/
		public String getComponentKey()
		{
			return _componentKey;
		}

		public void setComponentKey(String value)
		{
			_componentKey = value;
		}

		/*****************************************************************************************/
		public boolean getNegativeFlag()
		{
			return _negativeFlag;
		}

		public void setNegativeFlag(boolean value)
		{
			_negativeFlag = value;
		}
	}	// End private class SpssAndFlag



	/*****************************  Private class ScoreParts  *****************************/

	/**
	 * SpssAndFlag is a thin, private class used to contain some needed
	 * information about the scoring components (Cog names, GPI scale
	 * scores) for a particular competency.
	 *
	 * @author		Ken Beukelman
	 */
	static private class ScoreParts
	{

		//
		// Instance data.
		//
		private double _cum = 0.0;
		private int _cnt = 0;
		private boolean _missingParts = false;

		//
		// Constructors.
		//
		protected ScoreParts()
		{
			// Nothing happens here
		}

		//
		// Instance methods.
		//

		/*****************************************************************************************/
		public double getCum()
		{
			return _cum;
		}

		public void setCum(double value)
		{
			_cum = value;
		}

		/*****************************************************************************************/
		public int getCnt()
		{
			return _cnt;
		}

		public void setCnt(int value)
		{
			_cnt = value;
		}

		/*****************************************************************************************/
		public boolean getMissingParts()
		{
			return _missingParts;
		}

		public void setMissingParts(boolean value)
		{
			_missingParts = value;
		}
	}	// End private class SpssFlag
	
	
	
	
	/**
	 *  Fetch the data for the "Testing" column.  Does the following:
	 *  -- Gets the relevant competencies
	 *  -- Eliminate those not turned on (no intersection)
	 *  -- Eliminate those not needed (due to RDI or Cog flag issues)
	 *  -- Create the comp scores
	 *  NOTE:  This appears to handle "legacy"scoring only.  Scorring
	 *         for the ALR Auto projects is handled elsewhere.
	 * @param con - A database connection
	 * @param partId - A V2 participant ID
	 * @param displayCogs - Indicate if the cognitive scores will be displayed
	 * @param scoreData - A ScoreAndNormData object
	 * @return A map (key = compId, val = score as a string) of calculated scores
	 * @throws Exception
	 */
	static private Map<Long,String> calcTestingScores(Connection con,
													  String partId,
													  long dnaId,
			                                          boolean displayCogs,
			                                          ScoreAndNormData scoreData)
		throws Exception
	{
		// Get the relevant competencies.  This gets the intersections,
		// so intersections not checked in Setup do not appear in this list.
		
		// Check the RDI
		String rdiValue = scoreData.getRdi();
		if (rdiValue == null)
		{
			rdiValue = IGConstants.NO_RDI;
		}

		Statement stmt = null;
		ResultSet rs = null;
		StringBuffer sqlQuery = new StringBuffer();

		String context = "competency list";
		sqlQuery.append("SELECT mdule.moduleId, "); 
		sqlQuery.append("       mdule.internalName as mIntName, ");
		sqlQuery.append("       dl.competencyId ");
		sqlQuery.append("  FROM pdi_abd_module mdule ");
		sqlQuery.append("    INNER JOIN pdi_abd_dna_link dl ON (dl.moduleId = mdule.moduleId ");
		sqlQuery.append("                                   AND dl.dnaId = " + dnaId + ") ");		
		sqlQuery.append("  WHERE mdule.internalName in ('GPI','Cogs') ");
		sqlQuery.append("    AND dl.isUsed = 1");

		List<Long> cogComps = new ArrayList<Long>();
		List<Long> gpiComps = new ArrayList<Long>();

		try
		{
		    stmt = con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			// Get the competencies for the lists.  If there are no
			// competencies, at least we'll get the module IDs (used later).
			while (rs.next())
			{
				if (rs.getString("mIntName").equals("GPI"))
				{
					gpiComps.add(rs.getLong("competencyId"));
				}
				else
				{
					cogComps.add(rs.getLong("competencyId"));
				}
			}

			// Now see if there are any GPI-related intersections
			if (gpiComps.size() < 1)
			{
				rdiValue = IGConstants.NO_GPI_SCORES;
			}
			
			// Eliminate the competencies not needed (due to RDI or Cog flag issues)
			// Changed - Don't make a change in scoring if RDI >= 98
			//double rdi = Double.parseDouble(rdiValue);
			//if (rdi >= 98.0)
			//{
			//	gpiComps.clear();
			//}
			if (! displayCogs)
			{			
				cogComps.clear();
			}

			// Create the comp scores...
			// first, clean up the sql elements
			if (rs != null)
			{
			    try {  rs.close();  }
			    catch (SQLException sqlEx)
			    {
			    	System.out.println("ERROR: rs close(1) in fetchTestingScores - " + sqlEx.getMessage());
			    }
				rs = null;
			}
			if (stmt != null)
			{
			    try {  stmt.close();  }
			    catch (SQLException sqlEx)
			    {
			    	System.out.println("ERROR: stmt close(1) in fetchTestingScores - " + sqlEx.getMessage());
			    }
			    stmt = null;
			}
			sqlQuery.setLength(0);
			
			// Then get the internal names and comp ids for the Cogs/GPI competencies
			// TODO Refactor so it reads the active competencies and then uses the list to knock out any non-complient results in
			// the rs.next() loop.  More processing but fewer moving parts in the query (esp. when the list gets really long).
			sqlQuery.append("SELECT cmp.internalName, ");
			sqlQuery.append("       cmp.competencyId ");
			sqlQuery.append("  FROM pdi_abd_competency cmp ");
			sqlQuery.append("  WHERE cmp.internalName in (" + All_COMP_LIST + ")");

		    stmt = con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			// Put the names and ids into a temporary map.
			Map<String, Long> compNameId = new HashMap<String, Long>();
			while (rs.next())
			{
				compNameId.put(rs.getString("internalName"),new Long(rs.getLong("competencyId")));
			}
			
			// Use the map just built and the static list of components defined as static constants
			// above to build 2 maps of competency component lists (one for Cogs and 1 for GPI)
			//  key=compId, value=list of spssId/flag objects (spss name and neg flag)
			Map<Long, ArrayList<SpssAndFlag>> cogCompComponents = new HashMap<Long, ArrayList<SpssAndFlag>>();
			Map<Long, ArrayList<SpssAndFlag>> gpiCompComponents = new HashMap<Long, ArrayList<SpssAndFlag>>();
			for (int idx= 0; idx < COMPETENCY_SPSS_MAP.length; idx++)
			{
				SpssAndFlag component = new SpssAndFlag();
				component.setComponentKey((String)COMPETENCY_SPSS_MAP[idx][2]);
				component.setNegativeFlag(((Boolean)COMPETENCY_SPSS_MAP[idx][3]).booleanValue());
				Long cid = compNameId.get(COMPETENCY_SPSS_MAP[idx][0]);
				ArrayList<SpssAndFlag> curComp;
				if (COMPETENCY_SPSS_MAP[idx][1].equals("GPI"))
				{
					curComp = gpiCompComponents.get(cid);
					if (curComp == null)
					{
						curComp = new ArrayList<SpssAndFlag>();
						gpiCompComponents.put(cid, curComp);
					}
				}
				else
				{
					// must be Cogs
					curComp = cogCompComponents.get(cid);
					if (curComp == null)
					{
						curComp = new ArrayList<SpssAndFlag>();
						cogCompComponents.put(cid, curComp);
					}
				}
				curComp.add(component);
			}

			// Now generate a Map (key = comp id, value = ScoreParts object... cum, cnt, valid)
			// this accumulates the score data needed to calculate competency scores
			Map<Long, ScoreParts> compScoreRollup = new HashMap<Long, ScoreParts>();
			scoreAccumulator(compScoreRollup, gpiComps, gpiCompComponents, scoreData.getScoreMap(), false);
			if (scoreData.getCogsDone())
			{			
				// cogs are done... send the cog scores
				scoreAccumulator(compScoreRollup, cogComps, cogCompComponents,
								 scoreData.getScoreMap(), true);
				// changing the boolean to False, so that it ignores missing 
				// items, per JIRA task 194 in A by D. 
				// the only thing that I can see being an issue here is that
				// if there truly ARE missing items, it's not going to care
//				scoreAccumulator(compScoreRollup, cogComps, cogCompComponents,
//						 scoreData.getScoreMap(), false);
			}
			else
			{
				// Cogs not done... Send an empty score map (pretend there are no cogs scores)
				scoreAccumulator(compScoreRollup, cogComps, cogCompComponents,
								 new HashMap<String, ScorePair>(), false);
			}
		
			// OK... calculate the scores
			Map<Long,String> testingScores = new HashMap<Long,String>();
			for (Iterator<Map.Entry<Long, ScoreParts>> itr=compScoreRollup.entrySet().iterator(); itr.hasNext(); )
			{
				double score;
				Map.Entry<Long, ScoreParts> ent = itr.next();
				if (ent.getValue().getMissingParts())
				{			
					score = -1;
				}
				else
				{
					score = ent.getValue().getCum() / (double) ent.getValue().getCnt();
				}
				
				testingScores.put(ent.getKey(), scoreFloat2String(score));
			}
			
			return testingScores;
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL error for fetchTestingScores (" + context + ").  " +
							"SQLException: " + ex.getMessage() + ", " +
							"SQLState: " + ex.getSQLState() + ", " +
							"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (rs != null)
			{
			    try {  rs.close();  }
			    catch (SQLException sqlEx)
			    {
			    	System.out.println("ERROR: rs close(2) in fetchTestingScores - " + sqlEx.getMessage());
			    }
				rs = null;
			}
			if (stmt != null)
			{
			    try {  stmt.close();  }
			    catch (SQLException sqlEx)
			    {
			    	System.out.println("ERROR: stmt close(2) in fetchTestingScores - " + sqlEx.getMessage());
			    }
			    stmt = null;
			}
		}		   
	}	

	
	/**
	 *  Accumulate the score data, 
	 *
	 * @param rollup - the map I am rolling the score into (the "output")
	 * @param compList - The list of competencies to accumulate
	 * @param compComponents - The Map of component lists (key = compId)
	 * @param scoreMap - The map of scores by scale
	 */
	static private void scoreAccumulator(Map<Long, ScoreParts> rollup,
			                      List<Long> compList,
			                      Map<Long, ArrayList<SpssAndFlag>> compComponents,
			                      Map<String, ScorePair> scoreMap,
			                      boolean ignoreMissingParts)
	{
		for(int i= 0; i < compList.size(); i++)
		{
			// Get or make the scoreparts entry
			Long compId = compList.get(i);
			
			ScoreParts parts = rollup.get(compId);
			if (parts == null)
			{
				// doesn't exist
				parts = new ScoreParts();
				rollup.put(compId, parts);
			}
		
			// We have the entry for this comp, get each component and add it to the entry
			ArrayList<SpssAndFlag> ary = compComponents.get(compId);
			if (ary == null)
			{
				// Happens when a project is set up and the definition of the project subsequently changes (in the template)
				// Also if the new competency is not in the list built into IGScoring (or if the updated program was not deployed)
				throw new IllegalStateException("Invalid setup.  No testing components exist for competency " + compId);
			}
			for (int j=0; j < ary.size(); j++)
			{
				String key = ary.get(j).getComponentKey();

				if (parts.getMissingParts())
				{
					// if one of the previous components was missing... don't even bother
					continue;
				}
				ScorePair sp = scoreMap.get(key);
				String scoreStr = (sp == null) ? null : sp.getRating();
				
				if(scoreStr == null )
				{
					if(ignoreMissingParts){
						// do nothing...
					}else{
						parts.setMissingParts(true);
					}
					
				}else {
					
					double score = Double.parseDouble(scoreStr);
					if (ary.get(j).getNegativeFlag())
					{
						score = 6.0 - score;
					}
					parts.setCum(parts.getCum() + score);
					parts.setCnt(parts.getCnt() + 1);
				}
			}
		}
	}


	/**
	 * Get the test and norm names in an arrayList of TestNormNameData objects.
	 *
	 * @param con - A database connection
	 * @param normMap - A Map of instrumentkeys and norm name values
	 * @param include LEI - A flag that indicates if the LEI info should be returned
	 * @return An arraylist of IGTestAndNormNameData objects - test name is the key and norm name is the value 
	 * @throws Exception
	 */
	static protected ArrayList<IGTestAndNormNameData> getTestAndNormNames(Connection con,
																	  Map<String, String> normMap,
																	  boolean includeLEI)
		throws Exception
	{
		ArrayList<IGTestAndNormNameData> ret = new ArrayList<IGTestAndNormNameData>();
		for (int i=0; i < IGConstants.IG_INST_KEYS.size(); i++)
		{
			IGTestAndNormNameData elt = new IGTestAndNormNameData();
			String key = IGConstants.IG_INST_KEYS.get(i);
			elt.setTestName(IGConstants.INST_NAMES.get(key));
			String normName = normMap.get(key);	// the V2 norm id
			elt.setNormName(normName == null ? "Not Available" : normName);		
			ret.add(elt);
		}

		if(includeLEI)
		{
			IGTestAndNormNameData elt = new IGTestAndNormNameData();
			String key = IGConstants.LEI_INST;
			elt.setNormName(IGConstants.INST_NAMES.get(key));
			String normName = normMap.get(key);	// the V2 norm id
			elt.setNormName(normName == null ? "Not Available" : normName);
		}

		return ret;
	}

	
	/**
	 *  Return a list of score keys for a competency. 
	 *
	 * @param con - a database connection
	 * @param competencyId - the map I am rolling the score into
	 * @return An ArrayList of IGScaleScoreData objects
	 * @throws Exception
	 */
	static public ArrayList<IGScaleScoreData> getAScoreKeyList(Connection con, long competencyId)
		throws Exception
	{
		ArrayList<IGScaleScoreData> ret = new ArrayList<IGScaleScoreData>();
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT cmp.internalName ");
		sqlQuery.append("  FROM pdi_abd_competency cmp ");
		sqlQuery.append("  WHERE cmp.competencyId = " + competencyId);

		try
		{
		    stmt = con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());
			
			if (! rs.isBeforeFirst())
			{
				// Nothing to do
				return ret;
			}
			
			// go looking for the score keys
			rs.next();
			String comp = rs.getString("internalName");
			for(int i=0; i < COMPETENCY_SPSS_MAP.length; i++)
			{
				if(((String)COMPETENCY_SPSS_MAP[i][0]).equals(comp))
				{
					String key = (String)COMPETENCY_SPSS_MAP[i][2];
					String val = IGConstants.SCALE_NAMES.get(key);
					Boolean isNeg = (Boolean) COMPETENCY_SPSS_MAP[i][3];
					if (val == null)
					{
						val = "N/A - " + key;
					}
				
					IGScaleScoreData ssd = new IGScaleScoreData();
					ssd.setRating("");	// PDINH rating (empty)
					ssd.setScaleName(val);	// Name of the scale
					ssd.setIsNegative(isNeg);
					ssd.setSpssVal(key);	// spss key
					
					ret.add(ssd);
				}
			}
			
			return ret;
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL error for getAScoreKeyList.  " +
							"SQLException: " + ex.getMessage() + ", " +
							"SQLState: " + ex.getSQLState() + ", " +
							"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (rs != null)
			{
			    try {  rs.close();  }
			    catch (SQLException sqlEx)
			    {
			    	System.out.println("ERROR: rs close in getAScoreKeyList - " + sqlEx.getMessage());
			    }
				rs = null;
			}
			if (stmt != null)
			{
			    try {  stmt.close();  }
			    catch (SQLException sqlEx)
			    {
			    	System.out.println("ERROR: stmt close in getAScoreKeyList - " + sqlEx.getMessage());
			    }
			    stmt = null;
			}
		}		   
	}


	/**
	 *  Return a map of score keys for a competency list. 
	 *
	 * @param compMap - a Map of competency ids by internal name
	 * @param scoreMap - a Map of scale scores
	 * @return A Map of ZLists of the score keys (with names) by competency Id
	 */
	static public Map<Long, ScoreLists> getScoreLists(Map<String, Long> compMap,
													  Map<String, IGScoring.ScorePair> scoreMap)
		throws Exception
	{
		Map<Long, ScoreLists> ret = new HashMap<Long, IGScoring.ScoreLists>();

		// For each item in the SPSS key list...
		for (int idx= 0; idx < COMPETENCY_SPSS_MAP.length; idx++)
		{
			// get the internal name and look up the comp id
			String name = (String)COMPETENCY_SPSS_MAP[idx][0];
			Long compId = compMap.get(name);
			if (compId == null)
			{
				// The comp id is not in the input list... ignore it
				continue;
			}
			
			// see if the id exists in the output map
			if (! ret.containsKey(compId))
			{
				// NO - create an new entry in the ret map (compID, new arraylist
				ret.put(compId, new IGScoring().new ScoreLists());
			}
			
			// Get the spss key and the name of the instrument/scale
			String spssKey = (String)COMPETENCY_SPSS_MAP[idx][2];
			String scaleName = IGConstants.SCALE_NAMES.get(spssKey) == null ? ("N/A - " + spssKey) : IGConstants.SCALE_NAMES.get(spssKey);
			
			// Get the score
			String rating = "Not Available";
			if (scoreMap.containsKey(spssKey) && scoreMap.get(spssKey).getRating() != null)
			{
				rating = scoreMap.get(spssKey).getRating();
			}
			
			// Create the IGScaleScoreData object
			IGScaleScoreData ssd = new IGScaleScoreData();
			ssd.setScaleName(scaleName);
			ssd.setRating(rating);
			ssd.setIsNegative((Boolean) COMPETENCY_SPSS_MAP[idx][3]);
			
			// put it into the appropriate ret map entry
			String type = (String) COMPETENCY_SPSS_MAP[idx][1];
			if (type.equals("Cogs"))
			{
				ret.get(compId).getCogScores().add(ssd);
			}
			else if (type.equals("GPI"))
			{
				ret.get(compId).getGpiScores().add(ssd);
			}
			else
			{
				throw new Exception("Invalid score type in comp/spss map - " + type);
			}
		}
			
		return ret;
	}


	/**
	 *  Return a boolean indicating if there are cogs selected in the DNA. 
	 *
	 * @param compMap - a Map of competency ids by internal name
	 * @param scoreMap - a Map of scale scores
	 * @return A boolean idicateing if cogs are present (false) or not (true)
	 */
	static public boolean noCogsInDna(Connection con, long dnaId)
		throws Exception
	{
		boolean ret = false;	// default value says that there are cogs
		
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT COUNT(*) AS cogsCnt ");
		sqlQuery.append("  FROM pdi_abd_dna_link dl ");
		sqlQuery.append("  WHERE dl.dnaId = " + dnaId + " ");
		sqlQuery.append("    AND dl.moduleId IN (SELECT mm.moduleId ");
		sqlQuery.append("                          FROM pdi_abd_module mm ");
		sqlQuery.append("                          WHERE mm.internalName = 'Cogs')");
		//System.out.println("noCogsInDna SQL " + sqlQuery.toString());
		
		try
		{
		    stmt = con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			// "SELECT COUNT() here always returns exactly 1 row... get it
			rs.next();

			int cnt = rs.getInt("cogsCnt");
			
			ret = cnt == 0 ? true : false;

			return ret;	// DEBUG - Replace with desirted output
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL error for noCogsInDna.  " +
							"SQLException: " + ex.getMessage() + ", " +
							"SQLState: " + ex.getSQLState() + ", " +
							"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (rs != null)
			{
			    try {  rs.close();  }
			    catch (SQLException sqlEx)
			    {
			    	System.out.println("ERROR: rs close in noCogsInDna - " + sqlEx.getMessage());
			    }
				rs = null;
			}
			if (stmt != null)
			{
			    try {  stmt.close();  }
			    catch (SQLException sqlEx)
			    {
			    	System.out.println("ERROR: stmt close in noCogsInDna - " + sqlEx.getMessage());
			    }
			    stmt = null;
			}
		}
	}


		/**
		 *  Return a boolean indicating if there are GPI scales selected in the DNA. 
		 *
		 * @param compMap - a Map of competency ids by internal name
		 * @param scoreMap - a Map of scale scores
		 * @return A boolean idicating if GPI scales are present (false) or not (true)
		 */
		static public boolean noGpiInDna(Connection con, long dnaId)
			throws Exception
		{
			boolean ret = false;	// default value says that there are GPI scales
			
			Statement stmt = null;
			ResultSet rs = null;

			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT COUNT(*) AS gpiCnt ");
			sqlQuery.append("  FROM pdi_abd_dna_link dl ");
			sqlQuery.append("  WHERE dl.dnaId = " + dnaId + " ");
			sqlQuery.append("    AND dl.moduleId IN (SELECT mm.moduleId ");
			sqlQuery.append("                          FROM pdi_abd_module mm ");
			sqlQuery.append("                          WHERE mm.internalName = 'GPI')");
			//System.out.println("noGpiInDna SQL " + sqlQuery.toString());
			
			try
			{
			    stmt = con.createStatement();
				rs = stmt.executeQuery(sqlQuery.toString());

				// "SELECT COUNT() here always returns exactly 1 row... get it
				rs.next();

				int cnt = rs.getInt("gpiCnt");
				
				ret = cnt == 0 ? true : false;

				return ret;	// DEBUG - Replace with desired output
			}
			catch (SQLException ex)
			{
				// handle any errors
				throw new Exception("SQL error for noGPIInDna.  " +
								"SQLException: " + ex.getMessage() + ", " +
								"SQLState: " + ex.getSQLState() + ", " +
								"VendorError: " + ex.getErrorCode());
			}
			finally
			{
				if (rs != null)
				{
				    try {  rs.close();  }
				    catch (SQLException sqlEx)
				    {
				    	System.out.println("ERROR: rs close in noCogsInDna - " + sqlEx.getMessage());
				    }
					rs = null;
				}
				if (stmt != null)
				{
				    try {  stmt.close();  }
				    catch (SQLException sqlEx)
				    {
				    	System.out.println("ERROR: stmt close in noCogsInDna - " + sqlEx.getMessage());
				    }
				    stmt = null;
				}
			}
		}
	
	
	static private boolean isProjectSacII(Connection con, String projectId)
		throws Exception
	{
		boolean ret = false;
		Statement stmt = null;
		ResultSet rs = null;
		
		try
		{
			stmt = con.createStatement();
		}
		catch (SQLException ex)
		{
			throw new Exception("Unable to create Statement for isProjectSacII().  msg=" + ex.getMessage(), ex);
		}
//			    
//		HashMap<String, String> normNames = new HashMap<String, String>();
//		HashMap<String, ScorePair> scores = new HashMap<String, ScorePair>();
//		int rdi = 0;
//		// Get all the norms
//		HashMap<String, Norm> norms = new HashMap<String, Norm>();
//		phase = "Norm query execution";
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT 'x' ");
		sqlQuery.append("  FROM pdi_abd_dna dna, pdi_abd_model mdl ");
		sqlQuery.append("  WHERE dna.projectId = " + projectId + " ");
		sqlQuery.append("    AND mdl.internalName = '" + SAC_II_MODEL_NAME + "' ");
		sqlQuery.append("    AND dna.modelId = mdl.modelId");
		//System.out.println("sqlQuery.. " + sqlQuery.toString());
		
		try
		{
			rs = stmt.executeQuery(sqlQuery.toString());
			if (rs.isBeforeFirst())
			{
				// we have data
				ret = true;
			}
		}
		catch (SQLException ex)
		{
			throw new Exception("SQLException in isProjectSacII().  msg=" + ex.getMessage(), ex);
		}

		return ret;
	}

}