package com.pdi.data.helpers.interfaces;

import java.util.ArrayList;

import com.pdi.data.dto.Language;
import com.pdi.data.dto.SessionUser;

public interface ILanguageHelper {
	public Language fromId(SessionUser su, String id);
	public Language fromCode(SessionUser su, String code);
	public ArrayList<Language> all(SessionUser su);
}
