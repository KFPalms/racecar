/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v2.util;

/**
 * All rights reserved.
 * 
 * This class is very V2 specific, and allows fetching and parsing of norms
 * from the V2 database.  Norms for V2 are in the module XML, so very specific
 * methods are used to access them.
 * 
 */
import java.io.StringReader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.v2.constants.ManualEntryConstants;
import com.pdi.scoring.Norm;

/**
 * NormUtils is a class that has methods to do things like
 * fetching a parsing norms. 
 *  
 * @author		MB Panichi
 * @author		Ken Beukelman
 * 
 */
public class NormUtils
{
	//
	// Static data.
	//

	//
	// Static methods.
	//
	
	/**
	 * Fetch the XML from the norms table for
	 * this normId.
	 *
	 * @param Connection con - the database connection
	 * @param String normId - the norm Id 
	 * @return a String - the norms XML
	 * @throws Exception
	 */
	public static String fetchNormsXML(String normId)
		throws Exception
	{
		
		String normsXML = "";
	    
	    StringBuffer sqlQuery = new StringBuffer();
	    sqlQuery.append("SELECT  uniqueIdStamp, XML  ");
	    sqlQuery.append("  FROM  norms  ");
	    sqlQuery.append("  WHERE uniqueIdStamp = ?  ");
	    //System.out.println("fetchNormsXML SQL for norm Id " + normId + "  :  " +sqlQuery.toString());
	    
		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
		
		if (dlps.isInError())
		{
			//TODO When this method returns something, this should be modified
			System.out.println("Error preparing fetchNormsXML query." + dlps.toString());
			return normsXML;
		}

		DataResult dr = null;
		ResultSet rs = null;
		
	    try
		{
			// Fill sql parmeters with values and get the data
			dlps.getPreparedStatement().setString(1, normId);
			
			dr = V2DatabaseUtils.select(dlps);
			rs = dr.getResultSet();
	       	
	       	if (! rs.isBeforeFirst())
	       	{
	       		throw new Exception("SQL error obtaining norms xml.  ");
	       	}

	       	while (rs.next())
	       	{     		
	       		normsXML = (String) rs.getString("XML");
	       		//System.out.println("norms XML = " + normsXML);
	       	}
	       	
	       return normsXML;	
	       
		}
	    catch (SQLException ex)
		{
	    	// handle any errors
	    	throw new Exception("SQL fetching norm XML data.  " +
	    			"SQLException: " + ex.getMessage() + ", " +
					"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
	    finally
		{
			if (dr != null) { dr.close(); dr = null; }
	    }
	}	
	
	
	/**
	 * This method will parse the XML from the
	 * results table to get the norm ID used 
	 * to norm this instrument
	 * 
	 * @param String resultsXML - the xml to be parsed
	 * @param HashMap - the normsMap to be updated
	 * @param String - score - the score to be updated
	 * 
	 */
	public static boolean  parseNormsXML( String normsXML, HashMap<String, HashMap<String, String>> normsMap, String constId)
		throws Exception
	{
			//System.out.println("parseNormsXML().....creating the norms map...");	
		String matchConstId = constId;
		try
		{
			//System.out.println("normsXML  " + normsXML);	
			DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
			dbfac.setNamespaceAware(true);
			DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
			Document doc = docBuilder.parse(new InputSource(new StringReader(normsXML)));
			
			XPath xpath = XPathFactory.newInstance().newXPath(); 
			NodeList nodes = (NodeList) xpath.evaluate("//Constructs", doc, XPathConstants.NODESET); 	
			Node node = nodes.item(0);
			NodeList nodes2 = node.getChildNodes();	
			
			String mean = "";
			String stddev = "";
			String myConstId = "";
			//System.out.println("nodes2.getLength() " + nodes2.getLength());
			boolean done = false;
			for(int i = 0; i < nodes2.getLength(); i++)
			{
				Node node2 = nodes2.item(i);
				NamedNodeMap atts = node2.getAttributes();				
				if(done)
				{
					break;
				}
				
				// for each node, let's check the attributes to find the
				// construct we're looking for..... 
				for(int j = 0; j<3; j++)
				{
					Node attNode = atts.item(j);
				
					if(j == 0)
					{
						mean = attNode.getNodeValue();
						//if(mean != null)
						//{
						//	System.out.println(i +" mean = " + attNode.getNodeValue());
						//}
					}
					if(j == 1)
					{
						myConstId = attNode.getNodeValue();
						//if(myConstId != null)
						//{
						//	System.out.println(j +" constID = " + attNode.getNodeValue());
						//}
					}
					if(j == 2)
					{
						stddev = attNode.getNodeValue();
						//if(stddev != null)
						//{
						//	System.out.println(i +" stddev = " + attNode.getNodeValue());
						//}
					}
					
					if(myConstId.equals(matchConstId)&& 
							(mean != "" || !mean.equals("")) && 
							(stddev != "" || !stddev.equals("")) )
					{
						//if( (mean != "" || !mean.equals("")) && (stddev != "" || !stddev.equals("")) ){
						done = true;
						//}
						
						boolean usedConstruct = true;
		       			Iterator<String> iter = ManualEntryConstants.NON_NORMED_CONSTRUCTS.keySet().iterator();	
		       			//System.out.println("myConstId.equals(constId) " +  matchConstId);
		       			while(iter.hasNext())
		       			{
		       				String id = iter.next();
		       				if(id.equals(matchConstId))
		       				{
		       					//System.out.println("CONTAINED IN: ManualEntryConstants.NON_NORMED_CONSTRUCTS : " + matchConstId);
		       					done = true;
		       					usedConstruct = false;
		       					break;
		       				}	       					
		       			}
						
						// if the constId is the same as we're looking for 
						// (remember that the norm XML has mean/stdev data for each 
						// construct that is normed (so, for example, GPI has 37 constructs
						if(usedConstruct)
						{
							// only put the normed constructs into this map.... 
							if( (mean == "" ||
								 mean.equals("")) ||
								 (stddev == "" || stddev.equals("")) )
							{
//								System.out.println("not normed: " + matchConstId);
								done = false;
								return done;
							} else {		
								HashMap<String, String> map = new HashMap<String, String>();
								map.put(mean, stddev);
								normsMap.put(matchConstId, map);			
								//System.out.println("constId, Mean, stddev :  " + matchConstId + " | " + mean + " | " + stddev);
								done = true;
								return done;
							}
						} else {
							done = false;
							return done;
						}
					}				
				}
			}
			
			return done;
		}
		catch (Exception ex)
		{
	       	throw new Exception("Unable to parse the Norms XML:  " + ex.getMessage());
		}
	}	
	
	
	/**
	 * Fetch the XML from the norms table for
	 * this normId.
	 *
	 * @param Connection con - the database connection
	 * @param String normId - the norm Id 
	 * @return a String - the norms XML
	 * @throws Exception
	 */
	public static Norm getNormAndParseForConstruct(String normId, String constructId)
		throws Exception
	{
		String normsXML = NormUtils.fetchNormsXML(normId);
		
		Norm norm = new Norm();
		norm.setId(normId);
	
		try
		{
			//System.out.println("getNormAndParseForConstruct  - normsXML  " + normsXML);	
			DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
			dbfac.setNamespaceAware(true);
			DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
			Document doc = docBuilder.parse(new InputSource(new StringReader(normsXML)));
			
			XPath xpath = XPathFactory.newInstance().newXPath(); 
			NodeList nodes = (NodeList) xpath.evaluate("//Constructs", doc, XPathConstants.NODESET); 	
			Node node = nodes.item(0);
			NodeList nodes2 = node.getChildNodes();	
			
			String mean = "";
			String stddev = "";
			String myConstId = "";
			//System.out.println("nodes2.getLength() " + nodes2.getLength());
			boolean done = false;
			for(int i = 0; i < nodes2.getLength(); i++)
			{
				Node node2 = nodes2.item(i);
				NamedNodeMap atts = node2.getAttributes();				
				if(done)
				{
					break;
				}
				
				// for each node, let's check the attributes to find the
				// construct we're looking for..... 
				//for(int j = 0; j<3; j++){
				//Node attNode = atts.item(1);
				if(atts.item(1).getNodeValue().equals(constructId))
				{
					mean = atts.item(0).getNodeValue();
					//if(mean != null)
					//{
					//	System.out.println(" mean = " + mean);
					//}
					
					myConstId = atts.item(1).getNodeValue();
					//if(myConstId != null)
					//{
					//	System.out.println(" constID = " + myConstId);
					//}

					stddev = atts.item(2).getNodeValue();
					//if(stddev != null)
					//{
					//	System.out.println(" stddev = " + stddev);
					//}
					
					if(myConstId.equals(constructId)&& 
							(mean != "" || !mean.equals("")) && 
							(stddev != "" || !stddev.equals("")) )
					{
						done = true;
						
						boolean usedConstruct = true;
		       			Iterator<String> iter = ManualEntryConstants.NON_NORMED_CONSTRUCTS.keySet().iterator();	
		       			//System.out.println("myConstId.equals(constId) " +  matchConstId);
		       			while(iter.hasNext())
		       			{
		       				String id = iter.next();
		       				if(id.equals(constructId))
		       				{
		       					//System.out.println("CONTAINED IN: ManualEntryConstants.NON_NORMED_CONSTRUCTS : " + matchConstId);
		       					done = true;
		       					usedConstruct = false;
		       					break;
		       				}	       					
		       			}
						
						// if the constId is the same as we're looking for 
						// (remember that the norm XML has mean/stdev data for each 
						// construct that is normed (so, for example, GPI has 37 constructs
						if(usedConstruct){
							// only put the normed constructs into this map.... 
							if( (mean == "" || mean.equals("")) || (stddev == "" || stddev.equals("")) )
							{
								//System.out.println("not normed: " + matchConstId);
								
								done = false;
								// return the norm with only the id.
								return norm;
							} else {		
								norm.setMean(new Double(mean));
								norm.setStdDev(new Double(stddev));
//								System.out.println("constId, Mean, stddev :  " + constructId + " | " + mean + " | " + stddev);
								done = true;
								return norm;
							}
						} else {
							done = false;
							return norm;
						}
					}				
				}
			}
			
			return norm;
		}
		catch (Exception ex)
		{
	       	throw new Exception("Unable to parse the norm/contruct XML:  " + ex.getMessage());
		}
	}		
}
