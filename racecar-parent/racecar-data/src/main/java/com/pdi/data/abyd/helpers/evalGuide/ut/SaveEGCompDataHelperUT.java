package com.pdi.data.abyd.helpers.evalGuide.ut;

import java.sql.Connection;
import java.util.ArrayList;

import com.pdi.data.abyd.dto.evalGuide.EGBarScoreData;
import com.pdi.data.abyd.dto.evalGuide.EGCompDataDTO;
import com.pdi.data.abyd.dto.evalGuide.EGCompOverideDataDTO;
import com.pdi.data.abyd.helpers.evalGuide.SaveEGCompDataHelper;
import com.pdi.ut.UnitTestUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;

import junit.framework.TestCase;

public class SaveEGCompDataHelperUT extends TestCase
{
	//
	// Constructors
	//
	public SaveEGCompDataHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
	throws Exception
	{
		junit.textui.TestRunner.run(SaveEGCompDataHelperUT.class);
	}


	/*
	 * testSaveBarScore
	 * NOTE:  The name of the method is saveBarScore, but it saves the note
	 *        as well.  It probably should have been named saveCompData
	 */
//	public void testSaveBarScore()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//		
//		Connection con = AbyDDatabaseUtils.getDBConnection();
//    	ArrayList<EGBarScoreData> barDat = new ArrayList<EGBarScoreData>();
//    	for (int i=0; i < 4; i++)
//    	{
//    		EGBarScoreData cur = new EGBarScoreData();
//    		cur.setBarSeq(i + 1);
//    		cur.setBarScore((i + 1) * 2);
//    		barDat.add(cur);
//    	}
//    	
//    	EGCompDataDTO inp = new EGCompDataDTO();
//    	inp.setParticipantId("999999");
//    	inp.setDnaId(999);
//    	inp.setModuleId(888);
//    	inp.setCompId(14);
//    	inp.setCompNote("Comp.Note - HUT");
//    	inp.setCompBarArray(barDat);
//		
//		SaveEGCompDataHelper helper = new SaveEGCompDataHelper(con);    
//		helper.saveBarScoreData(inp);
//		System.out.println("Check the db...");
//
//		UnitTestUtils.stop(this);
//	}


	/*
	 * testSaveOverideScore
	 */
	public void testSaveOverideScore()
		throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);
		
		Connection con = AbyDDatabaseUtils.getDBConnection();
    	
    	EGCompOverideDataDTO inp = new EGCompOverideDataDTO();
    	inp.setParticipantId("999999");
    	inp.setDnaId(999);
    	inp.setModuleId(888);
    	inp.setCompId(14);
    	inp.setOrScoreIdx(5);
		
		SaveEGCompDataHelper helper = new SaveEGCompDataHelper(con);    
		helper.saveCsoData(inp);
		System.out.println("Check the db...");

		UnitTestUtils.stop(this);
	}
}
