/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.dto;

import java.util.ArrayList;
import java.util.Date;

import org.w3c.dom.Node;

import com.pdi.data.util.BaseDTO;
import com.pdi.data.util.RequestStatus;
import com.pdi.xml.XMLUtils;

/**
 * DTO for participation information
 * 
 * @author		Gavin Myers
 * @author		Ken Beukelman
 */
public class ParticipationDTO extends BaseDTO
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _extId;
	private ItemStatusDTO _itemStatus;
	private Date _dateStarted;
	private String _resultsUri;
	private String _answersUri;
	private String _activityId;
	private String _productId;
	
	private ArrayList<ResultDTO> _results;

	// 
	// Constructors.
	//

	/** 
	 * "Standard" (no-parameter) constructor
	 */
	public ParticipationDTO()
	{ 
		// Does nothing presently
		this.setResults(new ArrayList<ResultDTO>());
	}
	
	/** 
	 * Builds the object based off an xml node
	 * @param node
	 */
	public ParticipationDTO(Node node)
	{
		this.setResults(new ArrayList<ResultDTO>());
		
		/*
		 * The XML looks something like this:
		 * <ns3:participation>
		 *   <status>IN_PROGRESS</status>
		 *   <outcome>NOT_AVAILABLE</outcome>
		 *   <startedOn>2009-08-18T07:10:38.000-0400</startedOn>
		 *   <externalReferenceId>85946D39-5F94-4629-AF16-9754EFE44652</externalReferenceId>
		 *   <answers>https://platform.iscopia.com/webservices1/rest/v1/project/PCOJ-VNSK/participant/TOXPN-OKJTW/answers/c6b06dca-acd9-44ec-ab9d-ede1533c963f</answers>
		 *   <results>https://platform.iscopia.com/webservices1/rest/v1/project/PCOJ-VNSK/participant/TOXPN-OKJTW/results/c6b06dca-acd9-44ec-ab9d-ede1533c963f</results>
		 *   <ns2:activity>
		 *     <identifier>c6b06dca-acd9-44ec-ab9d-ede1533c963f</identifier>
		 *     <type>ASSESS</type>
		 *     <productIdentifier>4F97F776-A007-4387-8146-A5386D6BEFCC</productIdentifier>
		 *   </ns2:activity>
		 * </ns3:participation>
		 */
		this.setExtId(XMLUtils.getElementValue(node, "externalReferenceId/."));
		this.setItemStatus(new ItemStatusDTO(node));
		this.setAnswersUri(XMLUtils.getElementValue(node, "answers/."));
		this.setResultsUri(XMLUtils.getElementValue(node, "results/."));
		//this.setDateStarted(XMLUtils.getElementValue(node, "startedOn/."));
		this.setProductId(XMLUtils.getElementValue(node, "activity/productIdentifier/."));
		
		if (this.getExtId() == null &&
			(this.getItemStatus().getStatus().getStatusCode() == RequestStatus.WB_NO_DATA) &&
			this.getAnswersUri() == null &&
			this.getResultsUri() == null)
		{
			this.getStatus().setStatusCode(RequestStatus.WB_NO_DATA);
		}
	}

	//
	// Instance methods.
	//
	
	/**
	 * Generic toString method
	 */
	public String toString()
	{
		String str = "\n --- ParticipationDTO --- \n";
		str += "\nExternalID:               " + this.getExtId();
		str += "\nResults URI:  			" + this.getResultsUri();
		str += "\nAnswers URI:  			" + this.getAnswersUri();
		str += "\nProduct ID:  			" + this.getProductId();
		str += "\nActivity ID:  			" + this.getActivityId();

		str += this.getItemStatus().toString();
		for(ResultDTO result : this.getResults())
		{
			str += result.toString();
		}
		
		return str;
	}	
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
	
	/*****************************************************************************************/
	public void setExtId(String value)
	{
		_extId = value;
	}
	
	public String getExtId()
	{
		return _extId;
	}
	
	/*****************************************************************************************/
	public void setItemStatus(ItemStatusDTO value)
	{
		_itemStatus = value;
	}
	
	public ItemStatusDTO getItemStatus()
	{
		return _itemStatus;
	}
	
	/*****************************************************************************************/
	public void setDateStarted(Date value)
	{
		_dateStarted = value;
	}
	
	public Date getDateStarted()
	{
		return _dateStarted;
	}
	
	/*****************************************************************************************/
	public void setResults(ArrayList<ResultDTO> value)
	{
		_results = value;
	}
	
	public ArrayList<ResultDTO> getResults()
	{
		return _results;
	}
	
	/*****************************************************************************************/
	public void setResultsUri(String value)
	{
		_resultsUri = value;
	}
	
	public String getResultsUri()
	{
		return _resultsUri;
	}
	
	/*****************************************************************************************/
	public void setAnswersUri(String value)
	{
		_answersUri = value;
	}
	
	public String getAnswersUri()
	{
		return _answersUri;
	}
	
	/*****************************************************************************************/
	public void setProductId(String value)
	{
		_productId = value;
	}
	
	public String getProductId()
	{
		return _productId;
	}
	
	/*****************************************************************************************/
	public void setActivityId(String value)
	{
		_activityId = value;
	}
	
	public String getActivityId()
	{
		return _activityId;
	}
}
