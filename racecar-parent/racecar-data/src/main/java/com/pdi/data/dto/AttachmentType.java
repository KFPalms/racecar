package com.pdi.data.dto;

/**
 * AttachmentType is a wrapper around the attachments data for
 * a specific Participant.
 * Attachments are uploaded through the Test Data application.
 * Attachments are accessible in Test Data, and, read-only, 
 * through the DRI.
 * Attachments are .pdf, .doc, or .xls files, and that
 * is controlled via the UI.
 *
 * @author      MB Panichi
 */
public class AttachmentType extends CoreDataObject
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	/**
	 * 
	 */
	private static final long serialVersionUID = -1379522361334851074L;
	//
	// Instance data.
	//
    private String name = "";
    private int id = 0;

	//
	// Constructors.
	//
	public AttachmentType()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//

	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public String getName()
	{
		return name;
	}

	public void setName(String value)
	{
		name = value;
	}

	/*****************************************************************************************/
	public int getId()
	{
		return id;
	}

	public void setId(int value)
	{
		id = value;
	}

}



