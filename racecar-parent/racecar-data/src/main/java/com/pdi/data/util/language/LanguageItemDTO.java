/**
 * Copyright (c) 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.util.language;

/**
 * LanguageItemDTO is a container for information about a language.
 * Used primarily in Language dropdowns.
 * 
 * @author		Ken Beukelman
  */
public class LanguageItemDTO
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private long _id;
	private String _code;
	private String _displayName;
	

	//
	// Constructors.
	//
	
	// No paramater constructor
	public LanguageItemDTO()
	{
		// Does nothing presently
	}
	
	// 3 parameter constructor (used in makeCopy())
	private LanguageItemDTO(long id, String code, String dispName)
	{
		_id = id;
		_code = code;
		_displayName = dispName;
	}

	//
	// Instance methods.
	//

	/*
	 * makeCopy - make a copy of the data in this object
	 */
	public LanguageItemDTO makeCopy()
	{
		return new LanguageItemDTO(_id, _code, _displayName);
	}


	/*
	 * toString - return the content of this object as a string
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		String ret = "";
		ret += _id + "/" + _code + " - " + _displayName;
		
		return ret;
	}
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//
			
	/*****************************************************************************************/
	public long getId()
	{
		return _id;
	}

	public void setId(long value)
	{
		_id = value;
	}
	
	/*****************************************************************************************/
	public String getCode()
	{
		return _code;
	}

	public void setCode(String value)
	{
		_code = value.trim();
	}
	
	/*****************************************************************************************/
	public String getDisplayName()
	{
		return _displayName;
	}

	public void setDisplayName(String value)
	{
		_displayName = value;
	}
}
