/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.dto;

/**
 * EqInfo is a wrapper around a number of data items that are used
 * in instrument score equivalency tracking.
 * 
 * @author		Ken Beukelman
  */
public class EqInfo
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String participantId;	// The participant ID
	private String projectId;		// The project ID
	private String instCode;		// The "new" (target) instrument code
	private String eqScore;			// The "new" (equivalent) score
	private String oldInstCode;		// The "old" (original instrument code.
									// NOTE - these are bogus and are defined in DataConstants
	private String oldScore;		// The score of the old instrument (participant entered)

	//
	// Constructors.
	//
	public EqInfo()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
	
	/*****************************************************************************************/
	public String getParticipantId()
	{
		return this.participantId;
	}
	
	public void setParticipantId(String value)
	{
		this.participantId = value;
	}	
	
	/*****************************************************************************************/
	public String getProjectId()
	{
		return this.projectId;
	}
	
	public void setProjectId(String value)
	{
		this.projectId = value;
	}	
	
	/*****************************************************************************************/
	public String getInstCode()
	{
		return this.instCode;
	}
	
	public void setInstCode(String value)
	{
		this.instCode = value;
	}	
	
	/*****************************************************************************************/
	public String getEqScore()
	{
		return this.eqScore;
	}
	
	public void setEqScore(String value)
	{
		this.eqScore = value;
	}	
	
	/*****************************************************************************************/
	public String getOldInstCode()
	{
		return this.oldInstCode;
	}
	
	public void setOldInstCode(String value)
	{
		this.oldInstCode = value;
	}	
	
	/*****************************************************************************************/
	public String getOldScore()
	{
		return this.oldScore;
	}
	
	public void setOldScore(String value)
	{
		this.oldScore = value;
	}	

}
