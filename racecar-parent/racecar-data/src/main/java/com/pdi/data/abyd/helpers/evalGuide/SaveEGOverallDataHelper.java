/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.evalGuide;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import com.pdi.data.abyd.dto.evalGuide.EGOverallDTO;

/**
 * SaveEGOverallDataHelper contains the code needed to save the "overall" data
 * as found on the last data page of the Eval Guide.
 *
 * @author Ken Beukelman
 */
public class SaveEGOverallDataHelper {
	private final Connection con;
	private final String participantId;
	private final long dnaId;
	private final long moduleId;
	private final int opr; // Overall Performance Rating
	private String briefDesc; // Brief Description
	private String keyStrengths; // Key Strengths
	private String keyDevNeeds; // Key Development Needs
	private String coachNotes; // Real-time Coaching Notes
	private String themesFromPeers; // Themes From Peers
	private String themesFromDirectReports; // Themes From Direct Reports
	private String themesFromBoss; // Themes From Boss

	//
	// Constructors.
	//
	public SaveEGOverallDataHelper(Connection con, EGOverallDTO overallData) throws Exception {
		// Check key data
		if (overallData.getParticipantId() == null || overallData.getParticipantId().length() == 0
				|| overallData.getDnaId() == 0 || overallData.getModuleId() == 0) {
			throw new Exception("Invalid Overall Data key:  part="
					+ (overallData.getParticipantId() == null ? "null"
							: (overallData.getParticipantId().length() == 0 ? "<empty string>"
									: overallData.getParticipantId()))
					+ ", dna=" + overallData.getDnaId() + ", module=" + overallData.getModuleId());
		}

		this.con = con;
		this.participantId = overallData.getParticipantId();
		this.dnaId = overallData.getDnaId();
		this.moduleId = overallData.getModuleId();
		this.opr = overallData.getOverallData().getOpr();
		this.briefDesc = overallData.getOverallData().getBriefDesc();
		this.keyStrengths = overallData.getOverallData().getKeyStrengths();
		this.keyDevNeeds = overallData.getOverallData().getKeyDevNeeds();
		this.coachNotes = overallData.getOverallData().getCoachNotes();
		this.themesFromPeers = overallData.getOverallData().getThemesFromPeers();
		this.themesFromDirectReports = overallData.getOverallData().getThemesFromDirectReports();
		this.themesFromBoss = overallData.getOverallData().getThemesFromBoss();
	}

	//
	// Instance methods.
	//

	/**
	 * Update the EG response row with the "overall" data. Returns nothing.
	 *
	 * @throws Exception
	 */
	public void saveOverallData() throws Exception {
		Statement stmt = null;

		// Change any double quotes to single quotes
		// This prevents "Bad SQL" messages for strings that include double
		// quotes
		this.briefDesc = this.briefDesc.trim().replace("'", "''");
		this.keyStrengths = this.keyStrengths.trim().replace("'", "''");
		this.keyDevNeeds = this.keyDevNeeds.trim().replace("'", "''");
		if (this.themesFromPeers != null) {
			this.themesFromPeers = this.themesFromPeers.trim().replace("'", "''");
		}
		if (this.themesFromDirectReports != null) {
			this.themesFromDirectReports = this.themesFromDirectReports.trim().replace("'", "''");
		}
		if (this.themesFromBoss != null) {
			this.themesFromBoss = this.themesFromBoss.trim().replace("'", "''");
		}
		if (this.coachNotes != null) {
			this.coachNotes = this.coachNotes.trim().replace("'", "''");
		}

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("UPDATE pdi_abd_eg_response ");
		sqlQuery.append("  SET opr = " + this.opr + ", ");
		sqlQuery.append("      briefDesc = N'" + this.briefDesc + "', ");
		sqlQuery.append("      keyStrengths = N'" + this.keyStrengths + "', ");
		sqlQuery.append("      keyNeeds = N'" + this.keyDevNeeds + "', ");
		sqlQuery.append(
				"      coachNotes = " + (this.coachNotes == null ? "null" : "N'" + this.coachNotes + "'") + ", ");
		sqlQuery.append("      themesFromPeers = "
				+ (this.themesFromPeers == null ? "null" : "N'" + this.themesFromPeers + "'") + ", ");
		sqlQuery.append("      themesFromDirectReports = "
				+ (this.themesFromDirectReports == null ? "null" : "N'" + this.themesFromDirectReports + "'") + ", ");
		sqlQuery.append("      themesFromBoss = "
				+ (this.themesFromBoss == null ? "null" : "N'" + this.themesFromBoss + "'") + ", ");
		sqlQuery.append("  	   lastUserId = SYSTEM_USER, ");
		sqlQuery.append("  	   lastDate = GETDATE() ");
		sqlQuery.append("  WHERE participantId = " + this.participantId + " ");
		sqlQuery.append("    AND dnaId = " + this.dnaId + " ");
		sqlQuery.append("    AND moduleId = " + this.moduleId);

		try {
			stmt = this.con.createStatement();
			stmt.executeUpdate(sqlQuery.toString());

			return;
		} catch (SQLException ex) {
			// handle any errors
			throw new Exception("SQL saveOverallData.  " + "SQLException: " + ex.getMessage() + ", " + "SQLState: "
					+ ex.getSQLState() + ", " + "VendorError: " + ex.getErrorCode());
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					/* swallow the error */ }
				stmt = null;
			}
		}
	}
}
