/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v2.dto;


/**
 * SessionUserthis is the session user as returned from the V2 system
 * 
 * @author		Gavin Myers
 */
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.*;

public class SessionUserDTO
{

	
	
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//	
	
	private String _sessionId;
	private String _userId;
	private String _branch;
	private String _clientType;
	private String _accessLevel;
	private String _clientId;
	private String _userType;
	private String _active;
	private String _name;
	private String _clientActive;
	private String _id;
	private String _spt;
	private String _sdt;
	private String _ipAddress;
	private String _status;
	
	//
	// Constructors.
	//

	/**
	 * "Standard" (no-parameter) constructor
	 */
	public SessionUserDTO()
	{
		// Does nothing presently
	}
	
	public SessionUserDTO(Document doc)
		throws Exception
	{
		XPath xpath = XPathFactory.newInstance().newXPath(); 
		Node result = ((NodeList) xpath.evaluate("//Result", doc, XPathConstants.NODESET)).item(0); 
		Node record = ((NodeList) xpath.evaluate("//Record", doc, XPathConstants.NODESET)).item(0); 

		this.setSessionId(result.getAttributes().getNamedItem("SessionId").getNodeValue());
		this.setId(record.getAttributes().getNamedItem("UserID").getNodeValue());
		
		return;
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//
	
	/*****************************************************************************************/
	public String getStatus()
	{
		return _status;
	}

	public void setStatus(String value)
	{
		_status = value;
	}
	/*****************************************************************************************/
	
	
	/*****************************************************************************************/
	public String getSessionId()
	{
		return _sessionId;
	}

	public void setSessionId(String value)
	{
		_sessionId = value;
	}
	/*****************************************************************************************/
	

	/*****************************************************************************************/
	public String getUserId()
	{
		return _userId;
	}

	public void setUserId(String value)
	{
		_userId = value;
	}
	/*****************************************************************************************/
	
	
	/*****************************************************************************************/
	public String getBranch()
	{
		return _branch;
	}

	public void setBranch(String value)
	{
		_branch = value;
	}
	/*****************************************************************************************/
	
	/*****************************************************************************************/
	public String getClientType()
	{
		return _clientType;
	}

	public void setClientType(String value)
	{
		_clientType = value;
	}
	/*****************************************************************************************/
	
	/*****************************************************************************************/
	public String getAccessLevel()
	{
		return _accessLevel;
	}

	public void setAccessLevel(String value)
	{
		_accessLevel = value;
	}
	/*****************************************************************************************/
	
	/*****************************************************************************************/
	public String getClientId()
	{
		return _clientId;
	}

	public void setClientId(String value)
	{
		_clientId = value;
	}
	/*****************************************************************************************/
	
	/*****************************************************************************************/
	public String getUserType()
	{
		return _userType;
	}

	public void setUserType(String value)
	{
		_userType = value;
	}
	/*****************************************************************************************/
	
	/*****************************************************************************************/
	public String getActive()
	{
		return _active;
	}

	public void setActive(String value)
	{
		_active = value;
	}
	/*****************************************************************************************/
	
	/*****************************************************************************************/
	public String getName()
	{
		return _name;
	}

	public void setName(String value)
	{
		_name = value;
	}
	/*****************************************************************************************/
	
	/*****************************************************************************************/
	public String getClientActive()
	{
		return _clientActive;
	}

	public void setClientActive(String value)
	{
		_clientActive = value;
	}
	/*****************************************************************************************/
	
	
	/*****************************************************************************************/
	public String getId()
	{
		return _id;
	}

	public void setId(String value)
	{
		_id = value;
	}
	/*****************************************************************************************/
	
	
	/*****************************************************************************************/
	public String getSPT()
	{
		return _spt;
	}

	public void setSPT(String value)
	{
		_spt = value;
	}
	/*****************************************************************************************/
	
	
	/*****************************************************************************************/
	public String getSDT()
	{
		return _sdt;
	}

	public void setSDT(String value)
	{
		_sdt = value;
	}
	/*****************************************************************************************/
	
	
	/*****************************************************************************************/
	public String getIpAddress()
	{
		return _ipAddress;
	}

	public void setIpAddress(String value)
	{
		_ipAddress = value;
	}
	/*****************************************************************************************/
	
	
	
}
