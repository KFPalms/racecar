package com.pdi.data.helpers.interfaces;

import java.util.ArrayList;

import com.pdi.data.dto.SessionUser;
import com.pdi.data.dto.TestData;

public interface ITestDataHelper {
	
	public void saveCogsDoneFlag(SessionUser session, TestData testData);
	public boolean getCogsDoneFlag(SessionUser session, TestData testData);
	public long getDnaIdFromProjectId(SessionUser session, TestData testData);
	public ArrayList<String> getUserCourseListFromStoredProc(String clientId, String participantId);
}
