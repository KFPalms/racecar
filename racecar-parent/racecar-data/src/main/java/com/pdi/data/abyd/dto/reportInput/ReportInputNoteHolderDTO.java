/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.dto.reportInput;

import java.util.ArrayList;

import com.pdi.data.dto.Attachment;

/**
 * ReportInputNoteHolderDTO is a wrapper around the 
 * array list of notes data for
 * the ReportInput piece of the EvalGuide/IntegrationGrid for
 * A by D.
 * 
 * @author      MB Panichi
 */
public class ReportInputNoteHolderDTO
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
    private long _reportInputId = 0; // foreign key to ReportInputDTO  
    private ArrayList<ReportInputNoteDTO> _inputNotesArray = new ArrayList<ReportInputNoteDTO>();
 
	//
	// Constructors.
	//
	public ReportInputNoteHolderDTO()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//
	
	public String toString()
	{
		String ret = "";
		
		ret += "ReportInputNoteHolderDTO:\n";
		ret += "Id = " + this._reportInputId + "\n";
		ret += "    " + this._inputNotesArray.size() + " notes:\n";
		for (ReportInputNoteDTO note : _inputNotesArray)
		{
			ret += note.toString() + "\n";
		}
		
		return ret;
	}
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public long getReportInputId()
	{
		return _reportInputId;
	}
		
	public void setReportInputId(long value)
	{
		_reportInputId = value;
	}

	/*****************************************************************************************/
	public ArrayList<ReportInputNoteDTO> getInputNotesArray()
	{
		return _inputNotesArray;
	}
		
	public void setInputNotesArray(ArrayList<ReportInputNoteDTO> value)
	{
		_inputNotesArray = value;
	}
	
}
