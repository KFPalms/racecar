package com.pdi.data.v2.ut;

//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.Map;

//import com.pdi.data.v2.dataObjects.KeyValuePairStrings;
//import com.pdi.data.v2.dto.ConstructDTO;
//import com.pdi.data.v2.dto.ManualEntryDTO;
//import com.pdi.data.v2.dto.ModConstsDTO;
import com.pdi.data.v2.dto.ManualEntryDTO;
import com.pdi.data.v2.dto.SessionUserDTO;
import com.pdi.data.v2.helpers.ManualEntryDataHelper;
import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.data.v2.util.V2WebserviceUtils;
import com.pdi.properties.PropertyLoader;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

public class ManualEntryDataHelperUT  extends TestCase{

	public ManualEntryDataHelperUT(String name)
	{
		super(name);
	}
	
	public void testProperties()
	throws Exception
	{
		UnitTestUtils.start(this);
		
		//Does the local property work?
		String applicationId = PropertyLoader.getProperty("com.pdi.data.v2.application", "application.code");
		assertEquals(applicationId != null, true);
		assertEquals(applicationId.equals("PDI-DATA-V2"), true);
		
		UnitTestUtils.stop(this);
	}
	
	public void testLogin()
	throws Exception
	{
		UnitTestUtils.start(this);
		
		String username = "cdunn";
		String password = "cdunn";
		V2WebserviceUtils v2web = new V2WebserviceUtils();
		SessionUserDTO user = v2web.registerUser(username, password);
		System.out.println("The user name is " + user.getName());
		
		UnitTestUtils.stop(this);
	}


//	public void testGetChqAnswerData() 
//		throws Exception 
//	{
//	
//		UnitTestUtils.start(this);
//		
//		V2DatabaseUtils.setUnitTest(true);
//		
////		String username = "cdunn";
////		String password = "cdunn";
////		V2WebserviceUtils v2web = new V2WebserviceUtils();
////		SessionUserDTO sessionUser = v2web.registerUser(username, password);
////		System.out.println("The user name is " + sessionUser.getName());
//
//		HashMap<String, String> hm = LinkParamsDataHelper.getAdaptV2Answers("CTFVWQWE", "LIBPMLBJ");
//		for (Iterator<Map.Entry<String, String>> itr=hm.entrySet().iterator(); itr.hasNext();  )
//		{
//			Map.Entry<String, String> ent = itr.next();
//			System.out.println(ent.getKey() + "=" + ent.getValue());
//		}
//		
//		UnitTestUtils.stop(this);
//}



	public void testGetLinkParamsData() 
		throws Exception
	{
		UnitTestUtils.start(this);
		V2DatabaseUtils.setUnitTest(true);

		String username = "cdunn";
		String password = "cdunn";
		V2WebserviceUtils v2web = new V2WebserviceUtils();
		SessionUserDTO sessionUser = v2web.registerUser(username, password);
		System.out.println("The user name is " + sessionUser.getName());
//		
//		/*
//		 * note:  doing these all in the same test, because the DTO's build
//		 * on each other as it goes... 		 * 
//		 */
//		System.out.println(" TEST 1.....getLinkParamsData ");

		ManualEntryDataHelper lpHelper = new ManualEntryDataHelper();
		//ManualEntryDTO lpDto = lpHelper.getLinkParamsData("DVHBIOYN"); // certify test 
		//ManualEntryDTO lpDto = lpHelper.getLinkParamsData("BJSDNQPY"); // a by d 2010 certify
		ManualEntryDTO lpDto = lpHelper.getLinkParamsData("BIARSGTM"); // a by d 2010 certify
		System.out.println("LP data:");
		System.out.println("  ID=" + lpDto.getLinkParamId() + ", proj=" + lpDto.getProjectId());
		System.out.println("  Candidate:  ID=" + lpDto.getCandidateId() +
				", fname=" +lpDto.getFirstName() + ", lname=" + lpDto.getLastName());
		System.out.println("  ADAPT: uid=" + lpDto.getAdaptUserId() + ", eid=" + lpDto.getAdaptProjectId());
		System.out.println("         fname=" + lpDto.getAdaptFirstName() + ", lname=" + lpDto.getAdaptLastName());

		UnitTestUtils.stop(this);
	}	

		
//public ManualEntryDTO fetchLinkParamsDataForVendorService(String linkParamId)  
/*	DVHBIOYN
 * 
	<?xml version="1.0" encoding="UTF-8"?>
	<XML_Fields>
	    <JobId>KECLMUBZ</JobId>
	    <CandList>HNKGFQFW</CandList>
	    <FirstName>MB</FirstName>
	    <LastName>vendor test 03-23-09a</LastName>
	</XML_Fields>
*/	

//	System.out.println(" TEST 2.....getModulesForProjectId ");
//			
//	ArrayList<KeyValuePairStrings> moduleArray = new ArrayList<KeyValuePairStrings>();	
//	//LinkParamsDataHelper lpHelper = new LinkParamsDataHelper();
//	//moduleArray =  lpHelper.getModulesForProjectId("KECLMUBZ");	 // certify test
//	moduleArray =  lpHelper.getModulesForProjectId("IYKNYCNU");	 // a by d 2010 certify
//	Iterator<KeyValuePairStrings> iter = moduleArray.iterator();
//	
//	while(iter.hasNext()){
//		KeyValuePairStrings kvps = new KeyValuePairStrings();
//		kvps = iter.next();
//		System.out.println("key = " + kvps.getTheKey() + "   value = " + kvps.getTheValue());		
//	}
//
//	// use the moduleArray from previous test to get the constructs.... 
//	ArrayList<ModConstsDTO> modConstsArray = new ArrayList<ModConstsDTO>();
//
//	System.out.println(" TEST 3..... getConstructsforModules ");
//	
//	modConstsArray = lpHelper.getConstructsforModules(moduleArray);
//	Iterator<ModConstsDTO> i2 = modConstsArray.iterator();
//	
//	while(i2.hasNext()){
//		ModConstsDTO mcDto = new ModConstsDTO();
//		mcDto = i2.next();
//		System.out.println("121  module name = " + mcDto.getModuleName());
//		System.out.println("122  constructsarray.size() = " + mcDto.getConstructsAry().size());
//		
//		ArrayList<ConstructDTO> constArray = new ArrayList<ConstructDTO>();
//		constArray =  mcDto.getConstructsAry();
//		Iterator<ConstructDTO> i3 = constArray.iterator();
//		while(i3.hasNext()){
//			ConstructDTO cDto = new ConstructDTO();
//			cDto = i3.next();
//			//Norm norm = new Norm();			
//			System.out.println("     " + cDto.getConstructId()+ " : "  + cDto.getConstructName() );
//		}
//		mcDto = null;
//		constArray = null;
//	}
//
//	System.out.println(" TEST 4..... getResultsForConstructs ");
//	
//	//modConstsArray = lpHelper.getResultsForConstructs(modConstsArray, "CPHIPWGI", "IYKNYCNU"); // certify a by d 2010
//	modConstsArray = lpHelper.getResultsForConstructs(modConstsArray, "HNKGFQFW", "KECLMUBZ");  // certify
//	Iterator<ModConstsDTO> i4 = modConstsArray.iterator(); 
//	while(i4.hasNext()){
//		ModConstsDTO mcDto = new ModConstsDTO();
//		mcDto = i4.next();
//		System.out.println("module name = " + mcDto.getModuleName());
//		
//		ArrayList<ConstructDTO> constArray =  mcDto.getConstructsAry();
//		Iterator<ConstructDTO> i5 = constArray.iterator();
//		while(i5.hasNext()){
//			ConstructDTO cDto = new ConstructDTO();
//			cDto = i5.next();
//			//Norm norm = new Norm();			
//			System.out.println("     " + cDto.getConstructId()+ " : "  + cDto.getConstructName() +
//					" : "  + cDto.getConstructScore() + " : "  + cDto.getConstructScore2());
//		}
//		mcDto = null;
//		constArray = null;
//	}
//	
	
	
//	UnitTestUtils.stop(this);
//}	
	
	
}
