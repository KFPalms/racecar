/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 *
 * $Header: /Platform/Java/sourceCode/com/pdicorp/app/tcm/export/vo/ExportDataVO.java 6     8/25/05 3:23p Kbeukelm $
 */
package com.pdi.data.abyd.dto.common;


/**
* EGIrDisplayData is a container for the displayable Impact Rating data for a single
* IR in an Eval Guide.  It is a superset of the container used to return data for
* persistence in the database (EGIrScoreData).  It is used in the full data DTO.
* 
* @author		Ken Beukelman
* @version	$Revision: 1 $  $Date: 8/25/05 3:23p $
*/
public class EGIrDisplayData
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private int _irSeq;			// The sequence that this IR is in the list
	private String _irText;		// The display text for this IR
	private int _irScore;		// The score associated with this IR

	//
	// Constructors.
	//
	public EGIrDisplayData()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public int getIrSeq()
	{
		return _irSeq;
	}
		
	public void setIrSeq(int value)
	{
		_irSeq = value;
	}

	/*****************************************************************************************/
	public String getIrText()
	{
		return _irText;
	}
		
	public void setIrText(String value)
	{
		_irText = value;
	}

	/*****************************************************************************************/
	public int getIrScore()
	{
		return _irScore;
	}
		
	public void setIrScore(int value)
	{
		_irScore = value;
	}



	/*
	 * Show me what you've got
	 */
	public String toString()
	{
		String ret = "EGIrDisplayData content (Java)\n";
		ret += "  IR Sequence=" + _irSeq + "\n";
		ret += "      IR Text=" + _irText + "\n";
		ret += "     IR Score=" + _irScore + "\n";
		
		return ret;
	}
}
