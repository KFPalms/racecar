/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.dto.setup;

import java.util.ArrayList;


/**
 * ReportModelCompetencyDTO is a container for a report model competency.
 * Note that the RM (report model) designation has morphed into the CC
 * (custom competency) designation
 * 
 * @author		Ken Beukelman
 */
public class ReportModelCompetencyDTO
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private long _rmCompId;
	private long _defaultTextId;
	private long _userTextId;
	private String _displayName;
	private int _seq;
	private boolean _essential = false;
	private String _compScore = "0.0";
	private ArrayList<ReportModelCompChildDTO> _children;

	//
	// Constructors.
	//
	public ReportModelCompetencyDTO()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
	
	/*****************************************************************************************/
	public long getCompetencyId()
	{
		return _rmCompId;
	}

	public void setCompetencyId(long value)
	{
		_rmCompId = value;
	}
	
	/*****************************************************************************************/
	public long getDefaultTextId()
	{
		return _defaultTextId;
	}
	
	public void setDefaultTextId(long value)
	{
		_defaultTextId = value;
	}
	
	/*****************************************************************************************/
	public long getUserTextId()
	{
		return _userTextId;
	}
	
	public void setUserTextId(long value)
	{
		_userTextId = value;
	}
	
	/*****************************************************************************************/
	public String getDisplayName()
	{
		return _displayName;
	}

	public void setDisplayName(String value)
	{
		_displayName = value;
	}
	
	/*****************************************************************************************/
	public int getSeq()
	{
		return _seq;
	}

	public void setSeq(int value)
	{
		_seq = value;
	}
	
	/*****************************************************************************************/
	public boolean getEssential()
	{
		return _essential;
	}

	public void setEssential(boolean value)
	{
		_essential = value;
	}
	
	/*****************************************************************************************/
	public String getCompScore()
	{
		return _compScore;
	}

	public void setCompScore(String value)
	{
		_compScore = value;
	}
	
	/*****************************************************************************************/
	public ArrayList<ReportModelCompChildDTO> getChildren()
	{
		if (_children == null)
			_children = new ArrayList<ReportModelCompChildDTO>();
		return _children;
	}

	public void setChildren(ArrayList<ReportModelCompChildDTO> value)
	{
		_children = value;
	}
	
	
	public String toString()
	{
		String str = "    ReportModelCompetencyDTO:  Comp ID=" + _rmCompId + " - " + _displayName + " (" + _seq + "), " + (_essential ? "IS" : "Is NOT") + " essential, score=" + _compScore + "\n";
		if (_children == null  || _children.isEmpty())
		{
			str += "      No competency child data...\n";
		}
		else
		{
			for(int i = 0; i < _children.size(); i++)
			{
				//str += "      comp iter " + i + ":\n";
				str += _children.get(i).toString();
			}
		}
		
		return str;
	}
}
