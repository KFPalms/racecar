/*
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.setup.ut;

import java.sql.Connection;

import com.pdi.data.abyd.dto.common.KeyValuePair;
import com.pdi.data.abyd.dto.setup.EntryStateDTO;
import com.pdi.data.abyd.helpers.setup.EntryStateHelper;
import com.pdi.ut.UnitTestUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.data.nhn.util.NhnDatabaseUtils;

import junit.framework.TestCase;

public class EntryStateHelperUT extends TestCase
{
	//
	// Constructors
	//

	public EntryStateHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
		throws Exception
    {
		junit.textui.TestRunner.run(EntryStateHelperUT.class);
    }


	/*
	 * testGetEntryStateData
	 */
	public void testGetEntryStateData()
		throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);
		NhnDatabaseUtils.setUnitTest(true);

		Connection con = AbyDDatabaseUtils.getDBConnection();
		//String jobId = "LKLVXNGM";
		String projId = "329";
		
		System.out.println("EntryStateHelperUT:");
		System.out.println("    Input project ID=" + projId);
		
		EntryStateHelper helper = new EntryStateHelper(con, projId);
		EntryStateDTO dto = helper.getEntryStateData();
		
// TODO  What can I do to validate this?
		System.out.println("    Output:");
		System.out.println("         projectName=" + dto.getJobName());
		System.out.println("      ClientName=" + dto.getClientName());
		System.out.println("        DNA Desc=" + dto.getState());
		System.out.println("           State=" + dto.getState());
		System.out.println("        Models:=");
		for (KeyValuePair kvp : dto.getModelArray())
		{
			System.out.println("          " + kvp.getTheKey() + " - " + kvp.getTheValue());
		}

		UnitTestUtils.stop(this);
	}
}
