/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.dto;

//import java.util.ArrayList;
//import java.util.Date;

import org.w3c.dom.Node;

import com.pdi.data.util.BaseDTO;
import com.pdi.data.util.RequestStatus;
import com.pdi.xml.XMLUtils;

/**
 * DTO for client information
 * 
 * @author		Gavin Myers
 * @author		Ken Beukelman
 */
public class ClientDTO extends BaseDTO
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _id;
	private String _name;
	private String _uri;
	private String _abbreviation;
//	private String _parentId;
//	private String _adminEmail;
//	private String _preferredLanguage;
//	private ArrayList<String> _availableLanguages;
//	private String _version;
//	private String _clientStatus;
//	private String _createdBy;
//	private Date _createdDate;


	//
	// Constructors.
	//

	/**
	 * "Standard" (no-parameter) constructor
	 */
	public ClientDTO()
	{
		super();
	}

	/**
	 * Set up a new DTO with status but no data
	 */
	public ClientDTO(RequestStatus stat)
	{
		super();
		this.setStatus(stat);
	}

	/**
	 * Builds the object based off an xml node
	 * 		NOTE:  This does not fill the 'languages' list
	 * @param node
	 */
	public ClientDTO(Node node)
	{
		// We could be sending a list node or a result node.  Check for both.
		/*
		 * From the client list, the XML looks like this:
		 * <clientListLine>
		 *   <name>PDI Test Client</name>
		 *   <abbreviation>PDIDEV</abbreviation>
		 *   <identifier>OVNX-JPFM</identifier>
		 *   <uri>https://platform.iscopia.com/webservices1/rest/v1/client/OVNX-JPFM</uri>
		 * <clientListLine>
		 * 
		 * Note the the URI is unique here and tells us if we are filling a proxy or the real McCoy
		 * 
		 * From the client uri, the XML looks like this:
		 * <resultClient>
		 *   <name>Get R Dunn Prod</name>
		 *   <abbreviation>GRD</abbreviation>
		 *   <parentIdentifier>OSYS-STEM</parentIdentifier>
		 *   <adminEmail>cdunn@pdininthhouse.com</adminEmail>
		 *   <preferredLanguage>en</preferredLanguage>
		 *   <availableLanguages>en</availableLanguages>
		 *   <availableLanguages>fr</availableLanguages>
		 *   <identifier>OKLU-MJGG</identifier>
		 *   <version>1</version>
		 *   <status>Active</status>
		 *   <createdByUser>chauncey.dunn.dev</createdByUser>
		 *   <createdDate>2009-09-02T16:26:09.000-0400</createdDate>
		 *   <workflowsOffering/>
		 * <resultClient>
		 */

		super();
		
		// This works, because the participant list has proxy data only plus the URI.  If the
		// URI attribute is present, the Node must be from the list and contains proxy data only.
		String uri = XMLUtils.getElementValue(node, "uri/.");
		if (uri == null)
		this.setProxy((uri == null ? FILLED : PROXY));

		this.setId(XMLUtils.getElementValue(node, "identifier/."));
		this.setName(XMLUtils.getElementValue(node, "name/."));
		this.setUri(uri);
		this.setAbbreviation(XMLUtils.getElementValue(node, "abbreviation/."));

//		// the rest will be filled only if this is not a proxy
//		this.setParentId(XMLUtils.getElementValue(node, "parentIdentifier/."));
//		this.setAdminEmail(XMLUtils.getElementValue(node, "adminEmail/."));
//		this.setPreferredLanguage(XMLUtils.getElementValue(node, "preferredLanguage/."));
////		private ArrayList<String> _availableLanguages;
//		this.setVersion(XMLUtils.getElementValue(node, "version/."));
//		this.setClientStatus(XMLUtils.getElementValue(node, "status/."));
//		this.setCreatedBy(XMLUtils.getElementValue(node, "createdByUser/."));
////		private Date _createdDate;
		
		// check for data
		boolean cmnData = ! (this.getId() == null  &&
		              	   this.getName() == null  &&
		                   this.getAbbreviation() == null);
		
		if (isProxy())
		{
			if (! cmnData && this.getUri() == null)
			{
				this.getStatus().setStatusCode(RequestStatus.WB_NO_DATA);
			}
		}
		else
		{
			// Not proxy... check all of the data
			if (! cmnData)
//			if (! cmnData  &&
//				this.getParentId() == null  &&
//				this.getAdminEmail() == null  &&
//				this.getPreferredLanguage() == null  &&
//				this.getAvailableLanguages() == null  &&
//				this.getVersion() == null  &&
//				this.getClientStatus() == null  &&
//				this.getCreatedBy() == null  &&
//				this.getCreatedDate() == null)
			{
				this.getStatus().setStatusCode(RequestStatus.WB_NO_DATA);
			}
		}
//			if (this.getId() == null &&
//				this.getName() == null &&
//				this.getCode() == null &&
//				this.getDescription() == null &&
//				this.getAdminEmail() == null &&
//				this.getLogo() == null &&
//				this.getWatermark() == null &&
//				this.getTelephone() == null &&
//				this.getFax() == null &&
//				this.getLanguage() == null &&
//				this.getUrl() == null &&
//				this.getAddress().getStatus().getStatusCode() == RequestStatus.WB_NO_DATA)
//			{
//				this.getStatus().setStatusCode(RequestStatus.WB_NO_DATA);
//			}
	}


	// Instance methods.
	//
	//	
	/**
	 * Generic toString method
	 */
	public String toString()
	{
			String str = "\n --- ClientDTO --- \n";
			str += this.getStatus().toString();
			str += "\nProxy:             " + this.getProxy();
			str += "\nID:                 " + this.getId();
			str += "\nName:               " + this.getName();
			str += "\nURI:                " + this.getUri();
//			str += "\nAbbreviation:        " + this.getAbbreviation();
//			str += "\nParentId:           " + this.getParentId();
//			str += "\nAdminEmail:         " + this.getAdminEmail();
//			str += "\nPreferredLanguage:  " + this.getPreferredLanguage();
//			str += "\nAvaliableLanguages: ";
//			for (int i = 0; i < this.getAvailableLanguages().size(); i++)
//			{
//				str += "\n      " + this.getAvailableLanguages().get(i);
//			}
//			str += "\nVersion:            " + this.getVersion();
//			str += "\nClientStatus:       " + this.getClientStatus();
//			str += "\nCreatedBy:          " + this.getCreatedBy();

			return str;
	}
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public void setId(String id)
	{
		_id = id;
	}

	public String getId()
	{
		return _id;
	}

	/*****************************************************************************************/
	public void setName(String value)
	{
		_name = value;
	}

	public String getName()
	{
		return _name;
	}

	/*****************************************************************************************/
	public void setUri(String value)
	{
		_uri = value;
	}

	public String getUri()
	{
		return _uri;
	}

	/*****************************************************************************************/
	public void setAbbreviation(String value)
	{
		_abbreviation = value;
	}
	
	public String getAbbreviation()
	{
		return _abbreviation;
	}
//	
//	/*****************************************************************************************/
//	public void setParentId(String value)
//	{
//		_parentId = value;
//	}
//	
//	public String getParentId()
//	{
//		return _parentId;
//	}
//	
//	/*****************************************************************************************/
//	public void setAdminEmail(String value)
//	{
//		_adminEmail = value;
//	}
//	
//	public String getAdminEmail()
//	{
//		return _adminEmail;
//	}
//	
//	/*****************************************************************************************/
//	public void setPreferredLanguage(String value)
//	{
//		_preferredLanguage = value;
//	}
//	
//	public String getPreferredLanguage()
//	{
//		return _preferredLanguage;
//	}
//	
//	/*****************************************************************************************/
//	public void setAvailableLanguages(ArrayList<String> value)
//	{
//		_availableLanguages = value;
//	}
//	
//	public ArrayList<String> getAvailableLanguages()
//	{
//		return _availableLanguages;
//	}
//	
//	/*****************************************************************************************/
//	public void setVersion(String value)
//	{
//		_version = value;
//	}
//	
//	public String getVersion()
//	{
//		return _version;
//	}
//	
//	/*****************************************************************************************/
//	public void setClientStatus(String value)
//	{
//		_clientStatus = value;
//	}
//	
//	public String getClientStatus()
//	{
//		return _clientStatus;
//	}
//	
//	/*****************************************************************************************/
//	public void setCreatedBy(String value)
//	{
//		_createdBy = value;
//	}
//	
//	public String getCreatedBy()
//	{
//		return _createdBy;
//	}
//	
//	/*****************************************************************************************/
//	public void setCreatedDate(Date value)
//	{
//		_createdDate = value;
//	}
//	
//	public Date getCreatedDate()
//	{
//		return _createdDate;
//	}


		

	
	
	
	
///////////////////
// Original code //
///////////////////
//
//	//
//	// Static data.
//	//
//
//	//
//	// Static methods.
//	//
//
//	//
//	// Instance data.
//	//
//	private String _id;
//	private String _name;
//	private String _code;
//	private String _description;
//	private String _adminEmail;
//	private String _logo;
//	private String _watermark;
//	private String _telephone;
//	private String _fax;
//	private String _language;
//	private ArrayList<String> _languages;
//	private String _version;
//	private String _url;
//	
//	private AddressDTO _address;
//
//
//	//
//	// Constructors.
//	//
//
//	/**
//	 * "Standard" (no-parameter) constructor
//	 */
//	public ClientDTO()
//	{
//		super();
//	}
//
//	/**
//	 * Set up a new DTO with status but no data
//	 */
//	public ClientDTO(RequestStatus stat)
//	{
//		super();
//		this.setProxy(PROXY);	// Default to partial data
//		this.setStatus(stat);
//	}
//
//	/**
//	 * Builds the object based off an xml node
//	 * 		NOTE:  This does not fill the 'languages' list
//	 * @param node
//	 */
//	public ClientDTO(Node node)
//	{
//		/*
//		 * The XML looks something like this:
//		 * <client:Client xmlns:client="urn:iscopia.com:client-1.0">
//		 *   <name>Aviland HR</name>
//		 *   <abbreviation>AHR</abbreviation>
//		 *   <description>Avilan Human Resources inc.</description>
//		 *   <adminEmail>admin@avilandhr.com</adminEmail>
//		 *   <logoName>avilandLogo</logoName>
//		 *   <watermarkName>avilandWatermark</watermarkName>
//		 *   <address>123 Main Street</address>
//		 *   <city>Montreal</city>
//		 *   <stateOrProvince>QC</stateOrProvince>
//		 *   <country>Canada</country>
//		 *   <zip>H2S-2P2</zip>
//		 *   <telephone>514-555-1234</telephone>
//		 *   <fax>514-555-5555</fax>
//		 *   <preferredLanguage>fr</preferredLanguage>
//		 *   <availableLanguages>fr</availableLanguages>
//		 *   <availableLanguages>en</availableLanguages>
//		 *   <identifier>AAAA-BBBB</identifier >
//		 *   <version>1</version>
//		 * </client:Client>
//		 */
//		this.setId(XMLUtils.getElementValue(node, "identifier/."));
//		this.setName(XMLUtils.getElementValue(node, "name/."));
//		this.setCode(XMLUtils.getElementValue(node, "abbreviation/."));
//		this.setDescription(XMLUtils.getElementValue(node, "description/."));
//		this.setAdminEmail(XMLUtils.getElementValue(node, "adminEmail/."));
//		this.setLogo(XMLUtils.getElementValue(node, "logoName/."));
//		this.setWatermark(XMLUtils.getElementValue(node, "watermarkName/."));
//		this.setTelephone(XMLUtils.getElementValue(node, "telephone/."));
//		this.setFax(XMLUtils.getElementValue(node, "fax/."));
//		this.setLanguage(XMLUtils.getElementValue(node, "preferredLanguage/."));
//		this.setUrl(XMLUtils.getElementValue(node, "uri/."));
//		this.setAddress(new AddressDTO(node));
//		
//		// check for data
//		if (this.getId() == null &&
//			this.getName() == null &&
//			this.getCode() == null &&
//			this.getDescription() == null &&
//			this.getAdminEmail() == null &&
//			this.getLogo() == null &&
//			this.getWatermark() == null &&
//			this.getTelephone() == null &&
//			this.getFax() == null &&
//			this.getLanguage() == null &&
//			this.getUrl() == null &&
//			this.getAddress().getStatus().getStatusCode() == RequestStatus.WB_NO_DATA)
//		{
//			this.getStatus().setStatusCode(RequestStatus.WB_NO_DATA);
//		}
//	}
//
//	//
//	// Instance methods.
//	//
//	
//	/**
//	 * Generic toString method
//	 */
//	public String toString()
//	{
//		String str = "\n --- ClientDTO --- \n";
//		str += this.getStatus().toString();
//		str += "\nid:            " + this.getId();
//		str += "\nName:          " + this.getName();
//		str += "\nCode:          " + this.getCode();
//		str += "\nDescription:   " + this.getDescription();
//		str += "\nURL:           " + this.getUrl();
//		str += this.getAddress().toString();
//		return str;
//	}
//	
//	//*******************************************************************************************//
//	//																							 //
//	//                                       Get/Set Functions                                   //
//	//																							 //
//	//*******************************************************************************************//
//	
//	/*****************************************************************************************/
//	public void setId(String id)
//	{
//		_id = id;
//	}
//	
//	public String getId()
//	{
//		return _id;
//	}
//	
//	/*****************************************************************************************/
//	public void setName(String value)
//	{
//		_name = value;
//	}
//	
//	public String getName()
//	{
//		return _name;
//	}
//	
//	/*****************************************************************************************/
//	public void setCode(String value)
//	{
//		_code = value;
//	}
//	
//	public String getCode()
//	{
//		return _code;
//	}
//	
//	/*****************************************************************************************/
//	public void setDescription(String value)
//	{
//		_description = value;
//	}
//	
//	public String getDescription()
//	{
//		return _description;
//	}
//	
//	/*****************************************************************************************/
//	public void setAdminEmail(String value)
//	{
//		_adminEmail = value;
//	}
//	
//	public String getAdminEmail()
//	{
//		return _adminEmail;
//	}
//	
//	/*****************************************************************************************/
//	public void setLogo(String value)
//	{
//		_logo = value;
//	}
//	
//	public String getLogo()
//	{
//		return _logo;
//	}
//	
//	/*****************************************************************************************/
//	public void setWatermark(String value)
//	{
//		_watermark = value;
//	}
//	
//	public String getWatermark()
//	{
//		return _watermark;
//	}
//	
//	/*****************************************************************************************/
//	public void setAddress(AddressDTO value)
//	{
//		_address = value;
//	}
//	
//	public AddressDTO getAddress()
//	{
//		return _address;
//	}
//	
//	/*****************************************************************************************/
//	public void setTelephone(String value)
//	{
//		_telephone = value;
//	}
//	
//	public String getTelephone()
//	{
//		return _telephone;
//	}
//	
//	/*****************************************************************************************/
//	public void setFax(String value)
//	{
//		_fax = value;
//	}
//	
//	public String getFax()
//	{
//		return _fax;
//	}
//	
//	/*****************************************************************************************/
//	public void setLanguage(String value)
//	{
//		_language = value;
//	}
//	
//	public String getLanguage()
//	{
//		return _language;
//	}
//	
//	/*****************************************************************************************/
//	public void setLanguages(ArrayList<String> value)
//	{
//		_languages = value;
//	}
//	
//	public ArrayList<String> getLanguages()
//	{
//		return _languages;
//	}
//	
//	/*****************************************************************************************/
//	public void setVersion(String value)
//	{
//		_version = value;
//	}
//	
//	public String getVersion()
//	{
//		return _version;
//	}
//	
//	/*****************************************************************************************/
//	public void setUrl(String value)
//	{
//		_url = value;
//	}
//
//	public String getUrl()
//	{
//		return _url;
//	}
}
