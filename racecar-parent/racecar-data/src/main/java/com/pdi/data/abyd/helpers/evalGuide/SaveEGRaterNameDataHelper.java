/**
 * Copyright (c) 2009, 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.evalGuide;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.pdi.data.abyd.dto.common.DeliveryMethodDTO;
import com.pdi.data.abyd.dto.evalGuide.EGRaterNameDTO;
import com.pdi.data.abyd.helpers.common.EGDataHelper;
import com.pdi.data.util.language.LanguageItemDTO;


/**
 * SaveEGRaterNameDataHelper contains the code needed to save the rater name
 * as found on the first page of the Eval Guide.
 *
 * @author		Ken Beukelman
 */
public class SaveEGRaterNameDataHelper
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private Connection con;
	private String participantId;
	private long dnaId;
	private long moduleId;
	private String fName;
	private String lName;
	private LanguageItemDTO simLangItem ;
	private String simLangOther;
	private int deliveryMethodId;

	//
	//
	// Constructors.
	//
	public SaveEGRaterNameDataHelper(Connection con, EGRaterNameDTO nameData)
		throws Exception
	{
		// Check key data
		if (nameData == null)
		{
			throw new Exception("Input data DTO is null; no name data to save");
		}

		if (nameData.getParticipantId() == null || nameData.getParticipantId().length() == 0  ||
			nameData.getDnaId()	== 0 ||
			nameData.getModuleId() == 0)
		{
			String err = "";
			if (nameData.getRaterNameData() == null)
			{
				err = "Rater Name data is null";
			}
			else
			{
				err = "Invalid Comp Data key:  part=" +  (nameData.getParticipantId() == null ? "null" : (nameData.getParticipantId().length() == 0 ? "<empty string>" : nameData.getParticipantId())) +
								", name=" + nameData.getRaterNameData().getRaterLastName() == null ? "null" : nameData.getRaterNameData().getRaterLastName() +
								", " + nameData.getRaterNameData().getRaterFirstName() == null ? "null" : nameData.getRaterNameData().getRaterFirstName() +
					            ", dna=" + nameData.getDnaId() +
					            ", module=" + nameData.getModuleId();
			}
			throw new Exception(err);
		}

		this.con = con;
		this.participantId = nameData.getParticipantId();
		this.dnaId = nameData.getDnaId();
		this.moduleId = nameData.getModuleId();
		this.fName = nameData.getRaterNameData().getRaterFirstName() == null ? "<none>" : nameData.getRaterNameData().getRaterFirstName();
		this.lName = nameData.getRaterNameData().getRaterLastName() == null ? "<nane>" : nameData.getRaterNameData().getRaterLastName();
		this.simLangItem = nameData.getSimLangItem();
		this.simLangOther = nameData.getSimLangOther();
		this.deliveryMethodId = nameData.getDeliveryMethodId();
	}

	//
	// Instance methods.
	//

	/**
	 * Insert or update the first set of EG data (the rater name).  Returns nothing.
	 *
	 * @throws Exception
	 */
	public void saveRaterName()
		throws Exception
	{
		// Sorry, can't get by with it, I have to hit the DB to see if the EG
		// is out there.
		Statement stmt = null;
		ResultSet rs = null;
		String context;
		StringBuffer sqlQuery = new StringBuffer();
        
        // look for the row
		context = "count";
		sqlQuery.append("SELECT COUNT(*) as cnt ");
		sqlQuery.append("  FROM pdi_abd_eg_response egr ");
		sqlQuery.append("  WHERE egr.participantId = " + this.participantId + " ");
		sqlQuery.append("    AND egr.dnaId = " + this.dnaId + " ");
		sqlQuery.append("    AND egr.moduleId = " + this.moduleId);

		try
		{
			stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());
			
			// count always returns a row
			rs.next();
			int cnt = rs.getInt("cnt");

			// Clean up
			if (rs != null)
			{
				try  {  rs.close();  }
				catch (SQLException sqlEx)  {  /* swallow the error */  }
				rs = null;
			}
			//if (stmt != null)
			//{
			//    try  {  stmt.close();  }
			//    catch (SQLException sqlEx)  {  /* swallow the error */  }
			//    stmt = null;
			//}

			// Change any double quotes to single quotes
			// This prevents "Bad SQL" messages for strings that include double quotes
			this.fName = this.fName.trim().replace("'","''");
			this.lName = this.lName.trim().replace("'","''");
			if (this.simLangOther != null)
			{
				this.simLangOther = this.simLangOther.trim().replace("'","''");
				if (this.simLangOther.length() < 1)
					this.simLangOther = null;
			}

			// Do some language checking...
			// if the lang == other, needs text else kill the text here
			if (this.simLangItem.getId() == EGDataHelper.OTHER_LANG_ID)
			{
				if (this.simLangOther == null)
				{
					// Needs input here
					throw new Exception("'Other' language type needs text in the data field.");
				}
			}
			else
			{
				// Just make sure...
				this.simLangOther = null;
			}

			// Now process as required
			if (cnt == 0)
			{
				// Do the insert
				context = "insert";
				sqlQuery.setLength(0);
				sqlQuery.append("INSERT INTO pdi_abd_eg_response ");
				sqlQuery.append("  (participantId, dnaId, moduleId, raterFirstName, raterLastName, simLangId, simLangOther, deliveryMethodId, lastUserId, lastDate) ");
				sqlQuery.append("  VALUES(" + this.participantId + ", " +
											  this.dnaId + ", " +
											  this.moduleId + ", " +
											  "N'" + this.fName + "', " +
											  "N'" + this.lName + "', " +
											  this.simLangItem.getId() + ", " +
											  "N'" + this.simLangOther + "', " +
											  this.deliveryMethodId + ", " +
											  "SYSTEM_USER, " +
											  "GETDATE())");
		       	//stmt = this.con.createStatement();
		       	stmt.executeUpdate(sqlQuery.toString());
			}
			else
			{
				// Do the update
				context = "update";

                // update the name fields
				sqlQuery.setLength(0);
				sqlQuery.append("UPDATE pdi_abd_eg_response ");
				sqlQuery.append("  SET raterFirstName = N'" + this.fName + "', " +
									  "raterLastName = N'" + this.lName + "', " +
									  "simLangId = " + this.simLangItem.getId() + ", " +
									  "simLangOther = N'" + this.simLangOther + "', " +
									  "deliveryMethodId = " + this.deliveryMethodId + ", " +
									  "lastUserId = SYSTEM_USER, " +
									  "lastDate = GETDATE() ");
				sqlQuery.append("  WHERE participantId = " + this.participantId + " ");
				sqlQuery.append("    AND dnaId = " + this.dnaId + " ");
				sqlQuery.append("    AND moduleId = " + this.moduleId);
				
		       	stmt = this.con.createStatement();
		       	stmt.executeUpdate(sqlQuery.toString());
			}

			return;
		}
        catch (SQLException ex)
		{
            // handle any errors
        	throw new Exception("SQL saveRaterName (" + context + ").  " +
        			"SQLException: " + ex.getMessage() + ", " +
        			"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
        }
        finally
		{
            if (rs != null)
            {
                try  {  rs.close();  }
                catch (SQLException sqlEx)  {  /* swallow the error */  }
                rs = null;
            }
            if (stmt != null)
            {
                try  {  stmt.close();  }
                catch (SQLException sqlEx)  {  /* swallow the error */  }
                stmt = null;
            }
		}
	}
}
