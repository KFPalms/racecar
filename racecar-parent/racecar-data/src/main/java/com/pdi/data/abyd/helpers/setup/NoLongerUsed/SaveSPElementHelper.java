/**
 * Copyright (c) 2008 Personnel Decisions International, Inc.
 * All rights reserved.
 */

package com.pdi.data.abyd.helpers.setup.NoLongerUsed;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
////import java.util.Properties;

import com.pdi.data.abyd.dto.common.CompElementDTO;
////import com.pdicorp.enterprise.EmailMessage;
////import com.pdicorp.enterprise.EmailRequestService;

/**
 * SaveSPTextHelper contains the code needed to save the text portion of the
 * SP data.  The elements data is saved in a different class.
 * 
 * See additional comments at each of the function calls.
 *
 * @author		Ken Beukelman
 * @version	$Revision: 6 $  $Date: 8/25/05 3:23p $
 */
public class SaveSPElementHelper
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private Connection connection;
	private long dnaId;
	private ArrayList<CompElementDTO> eltAry;
////	private Properties properties;	
	
	//
	// Constructors.
	//
////////	protected SaveSPElementHelper(Connection con, long dnaId, ArrayList<SPCompElementDTO> ary, Properties props)
	public SaveSPElementHelper(Connection con, long dnaId, ArrayList<CompElementDTO> ary)
		throws Exception
	{
		this.connection = con;
		this.dnaId = dnaId;
		this.eltAry = ary;
////		this.properties = props;
	}

	//
	// Instance methods.
	//

	/**
	 * Insert, update, or delete the SP elements.  This logic determines which 
	 * function to perform depending upon if the text is already in the database.
	 * 
	 * NOTE:
	 * This function assumes that all of the competency elements are being returned,
	 * whether modified or not.  If this is not the case, then the logic will
	 * erroneously delete the competency elements that are not present.
	 *
////	 * @param clientMgrEmailAddr an email address for the project's client manager
	 * @throws Exception
	 */
////////	protected void saveSPElements(String clientMgrEmailAddr)
	public void saveSPElements()
		throws Exception
	{
		Statement stmt = null;
		ResultSet rs = null;
		String action = "";
		StringBuffer sqlQuery = new StringBuffer();
		HashMap<String, Boolean> inpElts = new HashMap<String, Boolean>();
		HashMap<String, Boolean> dbElts = new HashMap<String, Boolean>();
		
////////		// Before we can send an email to the client mgr indicating that the SP has changed,
////////		// we need to verify that something truly is changing. We use the boolean spElementsHaveChanged
////////		// to track if something, anything has changed through updates, inserts and deletes below.
////////		boolean spElementsHaveChanged = false;		

		// Set up the incoming data for analysis;  move the data to a
		// HashMap for easy access
		for(int i=0; i < this.eltAry.size(); i++)
		{
			CompElementDTO elt = this.eltAry.get(i);
			String key = "" + elt.getCompId();
			inpElts.put(key, new Boolean(elt.getIsEssential()));
			//System.out.println("input key:  " + elt.getCompId() + "  bool: " + elt.getIsEssential());
		}
		
		try
		{
			// Get the info from the db and set it up in the same fashion as the  input data
			action = "getting existing elements";
			sqlQuery.append("SELECT competencyId as compId, ");
			sqlQuery.append("       spValue as eltVal ");
			sqlQuery.append("  FROM pdi_abd_dna_sp_comp_elements elts ");
			sqlQuery.append("  WHERE dnaId = " + this.dnaId);
			sqlQuery.append("    AND active = 1");

			stmt = this.connection.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());
			if (rs.isBeforeFirst())
	       	{
				// Got something... make the list
				while(rs.next())
				{
					dbElts.put(rs.getString("compId"), new Boolean(rs.getBoolean("eltVal")));
				}
	       	}

			// loop through the input (do we insert or update)
			for (Iterator<String> itr = inpElts.keySet().iterator(); itr.hasNext();  )
			{
				String key = itr.next();
				Boolean inpVal = (Boolean) inpElts.get(key);
				Boolean dbVal = (Boolean) dbElts.get(key);
				if (dbVal == null)
				{
					// Not there... Insert to db
					action = "inserting";
					sqlQuery.setLength(0);
					sqlQuery.append("INSERT INTO pdi_abd_dna_sp_comp_elements ");
					sqlQuery.append("  (dnaId, competencyId, spValue, active, lastUserId, lastDate) ");
					sqlQuery.append("  VALUES(" + this.dnaId + ", ");
					sqlQuery.append("         " + key + ", ");
					sqlQuery.append("         " + (inpVal.booleanValue() ? 1 : 0) + ", ");
					sqlQuery.append("         1, ");
					sqlQuery.append("         (SELECT SUBSTRING_INDEX(USER(),'@',1)), ");
					sqlQuery.append("         now())");

			       	stmt = this.connection.createStatement();
			       	stmt.executeUpdate(sqlQuery.toString(), Statement.RETURN_GENERATED_KEYS);
////////			       	spElementsHaveChanged = true;
				}  else  {
					// It's there... Update db.  But only if ift needs it.
					if (! inpVal.equals(dbVal))
					{
						action = "updating";
						sqlQuery.setLength(0);
						sqlQuery.append("UPDATE pdi_abd_dna_sp_comp_elements ");
						sqlQuery.append("  SET spValue = " + (inpVal.booleanValue() ? 1 : 0) + " ");
						sqlQuery.append("  WHERE dnaId = " + this.dnaId + " ");
						sqlQuery.append("    AND competencyId = " + key);
	
				       	stmt = this.connection.createStatement();
				       	stmt.executeUpdate(sqlQuery.toString());
////////				       	spElementsHaveChanged = true;
					}
				}
			}

			// Now loop through the original db values (do we delete?)
			for (Iterator<String> itr = dbElts.keySet().iterator(); itr.hasNext();  )
			{
				action = "deleteing";
				
				// get the key
				String cmpId = itr.next();

				// does it exist?
				if(! inpElts.containsKey(cmpId))
				{
					// no, delete it from the db
					sqlQuery.setLength(0);
					sqlQuery.append("DELETE FROM pdi_abd_dna_sp_comp_elements ");
					sqlQuery.append("  WHERE dnaId = " + this.dnaId + " ");
					sqlQuery.append("    AND competencyId = " + cmpId); 	
	
					stmt = this.connection.createStatement();
					stmt.executeUpdate(sqlQuery.toString());
////////					spElementsHaveChanged = true;
				}
			}
			
			// set the SP Complete flag, if needed
////////			setSPComplete(clientMgrEmailAddr, spElementsHaveChanged);
			setSPComplete();
				
			return;
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL saveSPElements (" + action + ").  " +
    			"SQLException: " + ex.getMessage() + ", " +
    			"SQLState: " + ex.getSQLState() + ", " +
				"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (rs != null)
			{
				try  {  rs.close();  }
				catch (SQLException sqlEx)  {  /* Swallow the error  */  }
				rs = null;
			}
			if (stmt != null)
			{
				try  {  stmt.close();  }
				catch (SQLException sqlEx)  {  /* Swallow the error */  }
				stmt = null;
			}
		}
	}
	
	/**
	 * Update the SP complete flag, if needed.
////	 * Also determine if we need to notify the client mgr with an email.
	 * 
////	 * @param clientMgrEmailAddr an email address for the project's client manager
	 * @param spElementsHaveChanged flag indicating if the SP elements have changed
	 * @throws Exception
	 */
////////	private void setSPComplete(String clientMgrEmailAddr, boolean spElementsHaveChanged)
	private void setSPComplete()
		throws Exception
	{
////		if (getSPSubmittedFlag())
////		{
////		    //
////		    // We need to send an email to the client mgr if either
////		    // SP Element (essential) of SP Text (notes) have changed.
////		    //
////	       	if ( spElementsHaveChanged || getSpTextChangedFlag() )
////	       	{
////	       	    sendEmail(clientMgrEmailAddr);
////	       	}
////	       	
////			return;
////		}
		
////		// We have to set the flag and send the e-mail...
		// We have to set the flag
		Statement stmt = null;
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("UPDATE pdi_abd_dna ");
		sqlQuery.append("  SET spSubmitted = 1 ");
		sqlQuery.append("  WHERE dnaId = " + this.dnaId + " ");

		try
		{
	       	stmt = this.connection.createStatement();
	       	stmt.executeUpdate(sqlQuery.toString());
	       	
////	       	// The SpSubmitted flag has now been set to true for the first time so we 
////	       	// need to send an email to the client mgr.
////	       	if ( spElementsHaveChanged || getSpTextChangedFlag() )
////	       	{
////	       	    sendEmail(clientMgrEmailAddr);
////	       	}
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL setSPComplete (" + this.dnaId + ").  " +
				"SQLException: " + ex.getMessage() + ", " +
				"SQLState: " + ex.getSQLState() + ", " +
				"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (stmt != null)
			{
				try  {  stmt.close();  }
				catch (SQLException sqlEx)  {  /* Swallow the error */  }
				stmt = null;
			}
		}
	}
	
////	
////	/**
////	 * Get the SP complete flag
////	 * 
////	 * @throws Exception
////	 */
////	private boolean getSPSubmittedFlag()
////		throws Exception
////	{
////		Statement stmt = null;
////		ResultSet rs = null;
////		
////		StringBuffer sqlQuery = new StringBuffer();
////		sqlQuery.append("SELECT spSubmitted ");
////		sqlQuery.append("  FROM pdi_abd_dna ");
////		sqlQuery.append("  WHERE dnaId = " + this.dnaId);
////		
////		try
////		{
////			stmt = this.connection.createStatement();
////			rs = stmt.executeQuery(sqlQuery.toString());
////			
////			if (! rs.isBeforeFirst())
////	       	{
////				throw new Exception("getSPSubmittedFlag - Information for DNA " + this.dnaId + " does not exist.");
////	       	}  else  {
////				// Got something... get the flag
////				rs.next();
////				{
////					return rs.getBoolean("spSubmitted");
////				}
////	       	}
////		}
////		catch (SQLException ex)
////		{
////			// handle any errors
////			throw new Exception("SQL getSPSubmittedFlag.  " +
////				"SQLException: " + ex.getMessage() + ", " +
////				"SQLState: " + ex.getSQLState() + ", " +
////				"VendorError: " + ex.getErrorCode());
////		}
////		finally
////		{
////				if (rs != null)
////				{
////					try  {  rs.close();  }
////					catch (SQLException sqlEx)  {  /* Swallow the error  */  }
////					rs = null;
////				}
////				if (stmt != null)
////				{
////					try  {  stmt.close();  }
////					catch (SQLException sqlEx)  {  /* Swallow the error */  }
////					stmt = null;
////				}
////		}
////	}
////	
////	/**
////	 * Set the spTextChanged flag to false. When true this flag indicates that a user has
////	 * made a change to the SuccessProfile text. It is set to true in the SaveSPTextHelper class 
////	 * and reset to false here.
////	 *
////	 * @throws Exception
////	 */
////	private void setSpTextChangedFlagToFalse()
////		throws Exception
////	{
////		Statement stmt = null;
////		
////		StringBuffer sqlUpdate = new StringBuffer();
////		sqlUpdate.append("UPDATE pdi_abd_dna ");
////		sqlUpdate.append("  SET spTextChanged = 0 ");
////		sqlUpdate.append("  WHERE dnaId = " + this.dnaId + " "); 	
////		
////		try
////		{
////			stmt = this.connection.createStatement();
////			stmt.executeUpdate(sqlUpdate.toString());
////		}
////        catch (SQLException ex)
////		{
////            // handle any errors
////        	throw new Exception("SQL updateSpTextChangedFlag.  " +
////        			"SQLException: " + ex.getMessage() + ", " +
////        			"SQLState: " + ex.getSQLState() + ", " +
////					"VendorError: " + ex.getErrorCode());
////        }
////        finally
////		{
////            if (stmt != null)
////            {
////                try  {  stmt.close();  }
////                catch (SQLException sqlEx)  {  /* Swallow the error */  }
////                stmt = null;
////            }
////        }
////	}	
////	
////	/**
////	 * Get the SP Text Changed flag
////	 *
////	 * @throws Exception
////	 */
////	private boolean getSpTextChangedFlag()
////		throws Exception
////	{
////		Statement stmt = null;
////		ResultSet rs = null;
////		
////		StringBuffer sqlQuery = new StringBuffer();
////		sqlQuery.append("SELECT spTextChanged ");
////		sqlQuery.append("  FROM pdi_abd_dna ");
////		sqlQuery.append("  WHERE dnaId = " + this.dnaId);
////		
////		try
////		{
////			stmt = this.connection.createStatement();
////			rs = stmt.executeQuery(sqlQuery.toString());
////			rs.next();
////			return rs.getBoolean("spTextChanged");
////		}
////		catch (SQLException ex)
////		{
////			// handle any errors
////			throw new Exception("SQL getSpTextChangedFlag.  " +
////				"SQLException: " + ex.getMessage() + ", " +
////				"SQLState: " + ex.getSQLState() + ", " +
////				"VendorError: " + ex.getErrorCode());
////		}
////		finally
////		{
////				if (rs != null)
////				{
////					try  {  rs.close();  }
////					catch (SQLException sqlEx)  {  /* Swallow the error  */  }
////					rs = null;
////				}
////				if (stmt != null)
////				{
////					try  {  stmt.close();  }
////					catch (SQLException sqlEx)  {  /* Swallow the error */  }
////					stmt = null;
////				}
////		}
////	}	
////	
////	/**
////	 * Build a request for an email to be sent.
////	 * Set SpTextChanged Flag to false. 
////	 *
////	 * @param clientMgrEmailAddr an email address for the project's client manager
////	 * @throws Exception
////	 */
////	private void sendEmail(String clientMgrEmailAddr)
////		throws Exception
////	{
////	    String host = this.properties.getProperty("abd.emailServer");
////	    EmailRequestService emailService = new EmailRequestService(host);
////
////        EmailMessage email = new EmailMessage();
////        email.setToAddress(clientMgrEmailAddr);
////        String fromAddr = this.properties.getProperty("abd.dna.clientMgr.fromAddress");
////        email.setFromAddress(fromAddr);
////        String emailSubject = this.properties.getProperty("abd.dna.clientMgr.emailSubject");
////        email.setSubject(emailSubject);
///        String emailText = this.properties.getProperty("abd.dna.clientMgr.emailText");
////        email.setText(emailText);
////        email.setTextSubstitutions("");
////        
////        emailService.sendEmail(email);
////        
////        setSpTextChangedFlagToFalse();	    
////	}
}
