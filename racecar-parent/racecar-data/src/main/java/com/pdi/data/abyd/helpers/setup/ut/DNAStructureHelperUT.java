/*
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.setup.ut;

import java.sql.Connection;

import com.pdi.data.abyd.dto.common.CompGroupData;
import com.pdi.data.abyd.dto.common.DNACellDTO;
import com.pdi.data.abyd.dto.common.DNAStructureDTO;
import com.pdi.data.abyd.dto.common.KeyValuePair;
import com.pdi.data.abyd.dto.common.ModDataDTO;
import com.pdi.data.abyd.dto.common.CompElementDTO;
import com.pdi.data.abyd.helpers.setup.DNAStructureHelper;
import com.pdi.ut.UnitTestUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;

import junit.framework.TestCase;

public class DNAStructureHelperUT extends TestCase
{
	//
	// Constructors
	//

	public DNAStructureHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
		throws Exception
    {
		junit.textui.TestRunner.run(DNAStructureHelperUT.class);
    }


	/*
	 * testDNAStructure
	 */
	public void testDNAStructure()
		throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);

		Connection con = AbyDDatabaseUtils.getDBConnection();
		//long dnaId = 41;
		long dnaId = 19;
		
		System.out.println("DNAStructureHelperUT:");
		System.out.println("    DNA ID=" + dnaId);
		
		DNAStructureHelper helper = new DNAStructureHelper(con, dnaId);
		DNAStructureDTO dto = helper.getSetupDNAStructData();
		
		System.out.println("    Output:");
		System.out.println("        Competencies:");
		for (KeyValuePair kvp : dto.getCompetencyArray())
		{
			System.out.println("          " + kvp.getTheKey() + " - " + kvp.getTheValue());
		}
		System.out.println("        Competency Group Data:");
		for (CompGroupData grp : dto.getFullCompArray())
		{
			System.out.println("          ID=" + grp.getCgId() + ", Name=" + grp.getCgName());
			for (CompElementDTO cmp : grp.getCgCompList())
			{
				System.out.println("              ID=" + cmp.getCompId() + ",Name=" + cmp.getCompName());
			}
		}
		System.out.println("        Modules:");
		for (ModDataDTO mod : dto.getModArray())
		{
			System.out.println("          ID=" + mod.getModId() + ", editted=" + mod.getModEditable() + ", sumitted=" + mod.getModSubmitted() + ", Name=" + mod.getModName());
		}
		System.out.println("        Selected Modules:");
		for (Long sel : dto.getSelectedModsArray())
		{
			System.out.println("          " + sel.longValue());
		}
		System.out.println("        Intersections:");
		for (DNACellDTO mc : dto.getModCompIntersection())
		{
			System.out.println("          mod=" + mc.getModuleId() + ", comp=" + mc.getCompetencyId() + ", used=" + mc.getIsUsed() + ", score=" + mc.getScore());
		}

		UnitTestUtils.stop(this);
	}
}
