/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.dto;

import java.io.StringWriter;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

import com.pdi.data.util.BaseDTO;

/**
 * DocumentDTO is a wrapper class designed to allow status 
 * information to be passed along with a Document.
 * 
 * NOTE: There is no corresponding class on the client side
 *       as the Document object is not exposed there.
 * 
 * @author		Ken Beukelman
 * 
 */
public class DocumentDTO extends BaseDTO
{

	//
	// Static data.
	//


	//
	// Static methods.
	//


	//
	// Instance data.
	//

	private Document _document;


	//
	// Constructors.
	//
	public DocumentDTO()
	{
		super();
		// does nothing right now
	}
	
	
	//
	// Instance methods.
	//

	/**
	 * Generic toString method
	 */
	public String toString()
	{
		String temp = null;
		
		String str = "DocumentDTO - \n" +
					 "   " + this.getStatus().toString()+ "\n";
		
		DOMSource domSource = new DOMSource(_document);
		StringWriter writer = new StringWriter();
		StreamResult result = new StreamResult(writer);
		TransformerFactory tf = TransformerFactory.newInstance();
		try
		{
			Transformer transformer = tf.newTransformer();
			transformer.transform(domSource, result);
			temp = writer.toString();
		} catch (Exception e)
		{
			
		}
		str += temp;

		return str;
	}
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public Document getDocument()
	{
		return _document;
	}
	
	public void setDocument(Document value)
	{
		_document = value;
	}	
}
