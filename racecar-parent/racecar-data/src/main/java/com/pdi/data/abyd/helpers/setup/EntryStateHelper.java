/**
 * Copyright (c) 2008, 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 *
 * $Header: /com/pdicorp/app/ABD/dnaDesigner/AbdDnaService 6     8/25/05 3:23p Kbeukelm $
 */
package com.pdi.data.abyd.helpers.setup;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdi.data.abyd.dto.common.KeyValuePair;
import com.pdi.data.abyd.dto.setup.DNADescription;
import com.pdi.data.abyd.dto.setup.EntryStateDTO;
import com.pdi.data.abyd.helpers.setup.SetupConstants;
import com.pdi.data.dto.Project;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.IProjectHelper;
import com.pdi.data.util.language.DefaultLanguageHelper;
import com.pdi.webservice.NhnWebserviceUtils;

/**
 * EntryStateHelper contains all the code needed to extract and return an EntryStateDTO.  It
 * provides data base access and data reduction for the Assessment-by-Design Project Setup app
 *
 * @author		Ken Beukelman
 */
public class EntryStateHelper
{
	private static final Logger log = LoggerFactory.getLogger(EntryStateHelper.class);
	//
	// Static data.
	//
	// These are special circumstance project type codes (Palms) used in Entry State
	private static final String RED_ASMT_PROJ_TYPE = "RED_ASMT";
	private static final String ERICSSON_PROJ_TYPE = "ERICSSON";
	private static final String ALR_PROJ_TYPE = "ALR";
	
	// And these are special circumstance model names (Oxcart) used in Entry State
	// These are the ACTUAL (display/external) name of the model, NOT the internal name
	//
	// If adding a model name (external/display name) here also add it to the
	// ALR list in ReportInputDataHelper but note that THAT uses internal name
	// 2-step process!  Declare the name here and add it to the Set below
	private static final String BUL_REFRESH_MODEL = "BUL Refresh";
	private static final String BD_BUL_REFRESH_MODEL = "BUL Refresh - Becton Dickinson";
	private static final String ERICSSON_BUL_REFRESH_MODEL = "Ericsson EEA-EAC 2015";
	private static final String ERICSSON_BUL_REFRESH_2_MODEL = "Ericsson EEA-EAC 2016 TolTek";
//	private static final String ERICSSON_EEA_EAC_2017 = "Ericsson EEA-EAC 2017 TolTek";	// The new Ericsson model is an ALR model

//	private static final String ALR_BUL_MODEL = "ALR BUL (Tol Tech)";
//	private static final String ALR_BUL_STD_26 = "ALR BUL (TolTek 26 Std)";
//	private static final String ALR_SEA_STD = "ALR SEA (Infoworks 24 Std)";
//	private static final String ALR_BUL_AUTO = "AUTO - ALR BUL (TolTek 26 Std)";
//	private static final String ALR_BUL_AUTO_2 = "AUTO - ALR BUL (TolTek 26 Std) V2";
//	private static final String ALR_SEA_AUTO = "AUTO - ALR SEA (Infoworks 24 Std)";
//	private static final String ALR_CEO_TS = "CEO";		// ALR CEO model with traditional scoring (GPI & Cogs)
//	private static final String NOV_SMDP ="Novartis SMDP Center";
//	private static final String NOV_SMDP_2 ="Novartis SMDP Center v2";
//	private static final String NOV_SMDP_3 ="Novartis SMDP Center v3";
//	private static final String NOV_GPH ="Novartis GPH";
//	private static final String NOV_AZAAMTEK = "AzaamTek";
	private static final String MLL_MODEL = "MLL";
//	private static final String KFLA_MLL_MODEL = "KFLA MLL Zenio";
//	private static final String KFLA_MLL_MODEL_10 = "KFLA MLL Zenio (v 1.0)";
//	private static final String JJ_SEA_MODEL = "J&J SEA (KF4D)";
//	private static final String JJ_BUL_MODEL = "J&J BUL (KF4D)";
//	private static final String MLL_KF4D_MAN_MODEL = "KFRA MLL - KF4D - Manual";
//	private static final String BUL_KF4D_MAN_MODEL = "KFRA BUL - KF4D - Manual";
//	private static final String SEA_KF4D_MAN_MODEL = "KFRA SEA - KF4D - Manual";
//	private static final String CEO_KF4D_MAN_MODEL = "KFRA CEO - KF4D - Manual";
//	private static final String SEA_KF4D_AUTO_MODEL = "KFRA SEA - KF4D - Auto";
//	private static final String BUL_KF4D_AUTO_MODEL = "KFRA BUL - KF4D - Auto";
//	private static final String CEO_KF4D_AUTO_MODEL = "KFRA CEO - KF4D - Auto";
//	private static final String CEO_KF4D_AUTO_V2_MODEL = "KFRA CEO - KF4D - Auto V2";
//	//private static final String CAPGEM_VP_MODEL = "Capgemini - VP";
//	//private static final String CAPGEM_EVP_MODEL = "Capgemini - EVP";
//	private static final String TOLMED_MODEL = "TolMed";
//	private static final String MLL_KF4D_AUTO_MODEL = "KFRA MLL - KF4D - Auto";
//	private static final String MLL_KF4D_AUTO_360_MODEL = "KFRA SEA - KF4D - Auto (optional 360)";

//	// Uses the external names set above
//	private static Set<String> ALR_MODELS_EXT = new HashSet<String>();
//	static {
//		ALR_MODELS_EXT.add(ALR_BUL_MODEL);
//		ALR_MODELS_EXT.add(ALR_BUL_STD_26);
//		ALR_MODELS_EXT.add(ALR_SEA_STD);
//		ALR_MODELS_EXT.add(ALR_BUL_AUTO);
//		ALR_MODELS_EXT.add(ALR_BUL_AUTO_2);
//		ALR_MODELS_EXT.add(ALR_SEA_AUTO);
//		ALR_MODELS_EXT.add(ALR_CEO_TS);
//		ALR_MODELS_EXT.add(NOV_SMDP);
//		ALR_MODELS_EXT.add(NOV_SMDP_2);
//		ALR_MODELS_EXT.add(NOV_SMDP_3);
//		ALR_MODELS_EXT.add(NOV_GPH);
//		ALR_MODELS_EXT.add(NOV_AZAAMTEK);
//		ALR_MODELS_EXT.add(KFLA_MLL_MODEL);
//		ALR_MODELS_EXT.add(KFLA_MLL_MODEL_10);
//		ALR_MODELS_EXT.add(ERICSSON_EEA_EAC_2017);
//		ALR_MODELS_EXT.add(JJ_SEA_MODEL);
//		ALR_MODELS_EXT.add(JJ_BUL_MODEL);
//		ALR_MODELS_EXT.add(MLL_KF4D_MAN_MODEL);
//		ALR_MODELS_EXT.add(BUL_KF4D_MAN_MODEL);
//		ALR_MODELS_EXT.add(SEA_KF4D_MAN_MODEL);
//		ALR_MODELS_EXT.add(CEO_KF4D_MAN_MODEL);
//		ALR_MODELS_EXT.add(SEA_KF4D_AUTO_MODEL);
//		ALR_MODELS_EXT.add(BUL_KF4D_AUTO_MODEL);
//		ALR_MODELS_EXT.add(CEO_KF4D_AUTO_MODEL);
//		ALR_MODELS_EXT.add(CEO_KF4D_AUTO_V2_MODEL);
//		//ALR_MODELS_EXT.add(CAPGEM_VP_MODEL);
//		//ALR_MODELS_EXT.add(CAPGEM_EVP_MODEL);
//		ALR_MODELS_EXT.add(TOLMED_MODEL);
//		ALR_MODELS_EXT.add(MLL_KF4D_AUTO_MODEL);
//		ALR_MODELS_EXT.add(MLL_KF4D_AUTO_360_MODEL);
//	}

//	private static final int FLL_TARGET_LEVEL = 1;
	private static final int MLL_TARGET_LEVEL = 2;
//	private static final int BUL_TARGET_LEVEL = 3;
//	private static final int SEA_TARGET_LEVEL = 4;
	
	// Testing type abbreviations
	private static final String KF4D_TEST = "KF4D";
	private static final String KFP_TEST = "KFP";
	private static final String GPI_TEST = "GPI";


	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private Connection connection;
	private String projectId;
	
	// holder for the project type... used to determine the model list
	private String projectTypeCode = null;
	
//		// This a holder for participant count data so that
//		// we don't have to hit the V2 database twice.
//	private int partCount = 0;
	
	private long dfltLangId = 0;
	
//	    // This a flag, from the Project, to indicate if
//	    // participants have **ever** been added to the project
//	    // because if they have been added, even if they have been
//	    // subsequently removed, the setup needs to be locked.
//	private boolean participantsAdded = false;
	
	// Flag to indicate that there are Eval. Guides present for this project.
	private boolean hasEgs = false;
	
	
	//
	// Constructors.
	//
	public EntryStateHelper(Connection con, String projectId)
		throws Exception
	{
		// ensure there is a project id
		if (projectId == null  || projectId.length() < 1)
		{
			throw new Exception("A project id is required to fetch DNA setup data.");
		}

		this.connection = con;
		this.projectId = projectId;
		this.projectTypeCode = null;
		
		// Get the default language
		this.dfltLangId = DefaultLanguageHelper.fetchDefaultLanguageId();
	}

	//
	// Instance methods.
	//

	/**
	 * Fetch the initial data needed for the By Design Setup app.
	 *
	 * @return the EntryStateDTO object
	 * @throws Exception
	 */
	public EntryStateDTO getEntryStateData()
		throws Exception
	{
		EntryStateDTO ret = new EntryStateDTO();

//		// Reset partCount just to be sure...
//		this.partCount = 0;

		// Get externally sourced data
		log.debug("Getting Externally sourced data for project: {}", projectId);
		ret = getExternalData(ret);
		log.debug("Finished Getting Externally sourced data for project {}", projectId);

		//get the model list
		log.debug("Getting the model list for project: {}", projectId);
		ret = getModelListData(ret);
		log.debug("Finished Getting the model list for project: {}", projectId);
		
		// Get the DNA description
		log.debug("Getting the DNA description for project: {}", projectId);
		ret = getDNADescriptionData(ret);
		log.debug("Finished Getting the DNA description for project: {}", projectId);

		//get the state - done last because now the data is all in and available
		log.debug("Getting the state for project {}", projectId);
		ret = getState(ret);
		log.debug("Finished Getting the state for project {}", projectId);
        
		// get the model/test data
		log.debug("Getting the model/test data for project {}", projectId);
		ret = fetchModelTestList(ret);
		log.debug("Finished Getting the model/test data for project {}", projectId);
		
  		return ret;
	}       


	/**
	 * Fetch some External data.  Adds the info to the instance DTO object.
	 *
	 * @throws Exception
	 */
	private EntryStateDTO getExternalData(EntryStateDTO ret)
		throws Exception
	{
		// Get the project name, the client name, and the participant count
		
		// Create a dummy session
		SessionUser session = new SessionUser();
		
		// Instantiate a project helper
		IProjectHelper iproject = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated"); 

		// Get the Project object
		Project project = iproject.fromId(session, this.projectId);

		// Fill the internal holder
		ret.setJobName(project.getName());
		ret.setClientName(project.getClient().getName());
//
//		// Save the participant count for this project for later use (state)
//		this.partCount = project.getParticipantCollection().size();
        
		// This is no longer used per SBA-334
//		//Set the participantsAdded flag for this project for later use (state)
//        this.participantsAdded = project.getParticipantsAdded();
        
		// Save the project type code for later use
        this.projectTypeCode = project.getProjectTypeCode();
        
        // the new standard for when to lock (SBA-334)
        int egCnt = getEgCnt(project.getId());
        this.hasEgs = (egCnt != 0);

		
		return ret;
	}
	
	
	/**
	 * Fetch a model list.
	 *
	 * @return an updated EntryStateDTO object
	 * @throws Exception
	 */
	private EntryStateDTO getModelListData(EntryStateDTO ret)
		throws Exception
	{
		//ArrayList ary = new ArrayList();
		
        Statement stmt = null;
        ResultSet rs = null;
        
        StringBuffer sqlQuery = new StringBuffer();
        sqlQuery.append("SELECT pam.modelId as kvKey, ");
        sqlQuery.append("      pat.text as kvValue, ");
        sqlQuery.append("      pam.targetLevelTypeId as targ, ");
        sqlQuery.append("      pam.modelSeq as modSeq, ");
        sqlQuery.append("      pam.isRa ");
        sqlQuery.append("  FROM pdi_abd_model pam ");
        sqlQuery.append("    LEFT JOIN pdi_abd_text pat ON (pat.textId = pam.textId ");
        sqlQuery.append("                               AND pat.languageId = " + this.dfltLangId + ") ");
		sqlQuery.append("  WHERE pam.active = 1 ");
		sqlQuery.append("  ORDER BY modSeq");
		//System.out.println("getModelListData query: " + sqlQuery.toString());
		try
		{
			stmt = this.connection.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			if (! rs.isBeforeFirst())
			{
				throw new Exception("No model data exists");
			}

			ArrayList<KeyValuePair> orderedModelList = new ArrayList<KeyValuePair>();
			while (rs.next())
			{
				String val = rs.getString("kvValue");
				boolean isReadAssmnt = rs.getBoolean("isRa");
				// We now wish to put out only certain models for certain project types
				// We probably should abstract this a bit (possibly with database values
				// or something) when we port this to PALMS
				if (this.projectTypeCode.equals(ERICSSON_PROJ_TYPE))
				{
					int typ = rs.getInt("targ");
					if (typ != MLL_TARGET_LEVEL)
					{
						// don't put it out if it isn't an MLL
						// Note that this excludes the Ericsson L1 model as well
						continue;
					}
					if (val.equalsIgnoreCase(MLL_MODEL))
					{
						// Nope, we don't want the standard MLL in Ericsson
						continue;
					}
				}
				else if (this.projectTypeCode.equals(RED_ASMT_PROJ_TYPE))
				{
					// Project type from PALMS is RED_ASMT
					if (! (val.equalsIgnoreCase(BUL_REFRESH_MODEL)				||
						   val.equalsIgnoreCase(BD_BUL_REFRESH_MODEL)			||
						   val.equalsIgnoreCase(ERICSSON_BUL_REFRESH_MODEL)		||
						   val.equalsIgnoreCase(ERICSSON_BUL_REFRESH_2_MODEL) ))
					{
						// Don't put if not an "BUL Refresh" related model
						continue;
					}
				}
				else if (this.projectTypeCode.equals(ALR_PROJ_TYPE))
				{
					// Project type from PALMS is ALR
					// If adding a model name (external/display name) here also add it below AND
					// to the ALR list in ReportInputDataHelper but note that THAT uses internal name
					//if (! ALR_MODELS_EXT.contains(val))
					if (! isReadAssmnt)
					{
						// Don't put if NOT an ALR related model
						continue;
					}
				}
				else
				{
					// Project type code from PALMS does NOT equal one of the exceptions (ERICSSON, RED_ASMT, ALR)
					if (val.equalsIgnoreCase(BUL_REFRESH_MODEL)				||
						val.equalsIgnoreCase(BD_BUL_REFRESH_MODEL)			||
						val.equalsIgnoreCase(ERICSSON_BUL_REFRESH_MODEL)	||
						val.equalsIgnoreCase(ERICSSON_BUL_REFRESH_2_MODEL) )
					{
						// Don't put out the BUL Refresh models
						continue;
					}
					// If adding a model name (external/display name) here also add it above AND
					// to the ALR list in ReportInputDataHelper but note that THAT uses internal name
					//else if (ALR_MODELS_EXT.contains(val))
					else if (isReadAssmnt)
					{
						// Don't put out the ALR models
						continue;
					}
				}
				// The result set should contain all of the models
				// available for the project type in sequence order
				KeyValuePair entry = new KeyValuePair();
				entry.setTheKey(rs.getLong("kvKey"));
				entry.setTheValue(val);
				orderedModelList.add(entry);
			}
			ret.setModelArray(orderedModelList);

			return ret;
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL fetching model list.  " +
					"SQLException: " + ex.getMessage() + ", " +
					"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (rs != null)
			{
				try  {  rs.close();  }
				catch (SQLException sqlEx) {  /* Swallow the error */  }
				rs = null;
			}
			if (stmt != null)
			{
				try  {  stmt.close();  }
				catch (SQLException sqlEx) {  /* Swallow the error */  }
                stmt = null;
            }
        }
	}


	/**
	 * Fetch a DNA description based upon the V2 project id.
	 *
	 * @throws Exception
	 */
	private EntryStateDTO getDNADescriptionData(EntryStateDTO ret)
		throws Exception
	{
		// get a pointer to the DNA description object
		DNADescription dd = ret.getDnaDesc();

		// The project id will always be the one input
		dd.setJobId(this.projectId);

		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT dna.dnaId, ");
		sqlQuery.append("       dna.dnaName, ");
		sqlQuery.append("       dna.projectId, ");
		sqlQuery.append("       dna.modelId, ");
		sqlQuery.append("       txt.text AS mdlName, ");
		sqlQuery.append("       dna.cMgrName, ");
		sqlQuery.append("       dna.cMgrEmail, ");
		sqlQuery.append("       dna.dnaSubmitted, ");
		sqlQuery.append("       dna.synthMean, ");
		sqlQuery.append("       dna.synthStdDev, ");
		sqlQuery.append("       dna.wbVersion, ");
		sqlQuery.append("       dna.dnaNotes, ");
		sqlQuery.append("       mdl.active ");
		sqlQuery.append("  FROM pdi_abd_dna dna ");
		sqlQuery.append("    LEFT JOIN pdi_abd_model mdl ON mdl.modelId = dna.modelId ");
		sqlQuery.append("    LEFT JOIN pdi_abd_text txt ON (txt.textId = mdl.textId) ");
		sqlQuery.append("  WHERE dna.projectId = " + this.projectId);  
		//System.out.println("get dna description sql :  " + sqlQuery);
		try
		{
			stmt = this.connection.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			// If there is data (isBeforeFirst = true), fill the DNADescription object
        	// from it.  If there isn't data (isBeforeFirst = false), assume no DNA in
			// the data base for this project.  The (nearly) empty DNADescription
			// object will be returned.
			if ( rs.isBeforeFirst())
			{
				// Fill up the DNADescription
				// Get the first result row
				rs.next();

				dd.setDnaId(rs.getLong("dnaId"));
				dd.setDnaName(rs.getString("dnaName"));
				dd.setJobId(rs.getString("projectId"));
				dd.setModelId(rs.getLong("modelId"));
				dd.setModelName(rs.getString("mdlName"));
				dd.setcMgrName(rs.getString("cMgrName"));
				dd.setcMgrEmail(rs.getString("cMgrEmail"));
				dd.setDnaComplete(new Boolean(rs.getBoolean("dnaSubmitted")));
				dd.getSynthNorm().setMean(rs.getDouble("synthMean"));
				dd.getSynthNorm().setStdDev(rs.getDouble("synthStdDev"));
				dd.setWbVersion(rs.getString("wbVersion") == null ? "" : rs.getString("wbVersion"));
				dd.setDnaNotes(rs.getString("dnaNotes") == null ? "" : rs.getString("dnaNotes"));
				dd.set_modelActive(rs.getLong("active") == 1 ? true : false);
			}
			
			if (ret.getDnaDesc().getDnaName() == null  || ret.getDnaDesc().getDnaId() == 0)
			{
				ret.getDnaDesc().setDnaName(ret.getJobName());
			}
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL fetching DNA.  " +
					"SQLException: " + ex.getMessage() + ", " +
					"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (rs != null)
			{
				try  {  rs.close();  }
				catch (SQLException sqlEx) {  /* Swallow the error */  }
				rs = null;
			}
			if (stmt != null)
			{
				try  {  stmt.close();  }
				catch (SQLException sqlEx) {  /* Swallow the error */  }
		        stmt = null;
		    }
		}
		
		// Now do the test instrument
		String inst = "";
		ArrayList<String> courseList = new NhnWebserviceUtils().getUnfilteredProjectCourseList(this.projectId);
		if (courseList.contains("kf4d") || courseList.contains("dla") || courseList.contains("dpa"))
		{
			inst = KF4D_TEST;
		}
		else if (courseList.contains("kfp"))
		{
			inst = KFP_TEST;
		}
		else if (courseList.contains("gpi") || courseList.contains("gpil"))
		{
			inst = GPI_TEST;
		}
		dd.setTestInstrument(inst);

		// ...and scram
		return ret;
		
	}
	
	
	/**
	 * Get the state.  Based upon the following rules:
	 * State 0 - No DNA exists in the database
	 * State 1 - DNA exists but is not 'Complete'
	 * State 2 - DNA exists and is Complete.  No participants associated with project
	 * State 3 - DNA exists and is Complete.  Participants do exist for the project
	 * 
	 * Because of previous action, data is available in the system to generate the state
	 *
	 * @throws Exception
	 */
	private EntryStateDTO getState(EntryStateDTO ret)
//		throws Exception
	{
		// partCount is set previously in the getExternalData() method by checking the size of the 
        // participantCollection
		// we have also set the participantsAdded flag in the getExternalData() method, 
		//		which remains set even if all participants have been removed -- business doesn't 
		// 		want setup 1 accessible, even when existing participants are removed, because it
		//		mucks things up.


		ret.setState(SetupConstants.SS_DNA_INVALID);  // default to invalid state
		if (ret.getDnaDesc().getDnaId() == 0)
		{
			// Initial entry
			ret.setState(SetupConstants.SS_DNA_NEW);
		}
		else if (ret.getDnaDesc().getDnaComplete().booleanValue() == false)
		{
			// DNA portion not yet submitted (no check on SP)
			ret.setState(SetupConstants.SS_DNA_STARTED);
		}
//		else if (ret.getDnaDesc().getDnaComplete().booleanValue() == true && this.participantsAdded == false)
		else if (ret.getDnaDesc().getDnaComplete().booleanValue() == true && this.hasEgs == false)
		{
			// DNA portion of design submitted; no participants have been processed
			ret.setState(SetupConstants.SS_DNA_SUBMITTED);
		}
		else
		{
			// DNA design complete and participants are active (locked down)
			ret.setState(SetupConstants.SS_DNA_LOCKED);
		}
		
		return ret;
	}       
	
	
	/**
	 * Fetch the EG count for the project/dna
	 *
	 * @return count
	 * @throws Exception
	 */
	private int getEgCnt(String projId)
		throws Exception
	{
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT count(*) as cnt ");
		sqlQuery.append("  FROM pdi_abd_eg_response ");
		sqlQuery.append("  WHERE dnaId = (SELECT Top 1 dnaId ");
		sqlQuery.append("                   FROM pdi_abd_dna ");
		sqlQuery.append("                   WHERE projectId = " + projId + " AND active=1)");
		//System.out.println("getEgCnt query: " + sqlQuery.toString());
		try
		{
			stmt = this.connection.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			// Assume a single row
			rs.next();
			
			return rs.getInt("cnt");
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL fetching EG count.  " +
					"SQLException: " + ex.getMessage() + ", " +
					"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (rs != null)
			{
				try  {  rs.close();  }
				catch (SQLException sqlEx) {  /* Swallow the error */  }
				rs = null;
			}
			if (stmt != null)
			{
				try  {  stmt.close();  }
				catch (SQLException sqlEx) {  /* Swallow the error */  }
                stmt = null;
            }
		}
	}


	/**
	 * getModelTestList - Array list of key value pairs of modelID and the test associated with them
	 * 					  Note that the test is simplified; auto and manual tests are set to the same
	 * 					  testing indicator for both (simplifies the check in the UI)
	 * 
	 * @param ret
	 * @return
	 */
	private EntryStateDTO fetchModelTestList(EntryStateDTO ret)
		throws Exception
	{
		ArrayList<KeyValuePair> mtl = new ArrayList<KeyValuePair>();
		ret.setModelTestArray(mtl);
		
		Map<Long,String> modelTests = new HashMap<Long,String>();
		
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT DISTINCT pam.modelId, mm.internalName ");
		sqlQuery.append("  FROM pdi_abd_model pam ");
		sqlQuery.append("    LEFT JOIN pdi_abd_dna dna ON (dna.modelId = pam.modelId AND projectId = 0) ");
		sqlQuery.append("    LEFT JOIN pdi_abd_dna_link dl on (dl.dnaId = dna.dnaId) ");
		sqlQuery.append("    INNER JOIN pdi_abd_module mm on( mm.moduleId = dl.moduleId AND ");
		sqlQuery.append("									  mm.internalName in ('GPI','Testing (KFALP)','ALP','Testing (KF4D)','KF4D') ) ");
		sqlQuery.append("  WHERE pam.active = 1");
		//System.out.println("get model test sql :  " + sqlQuery);
		try
		{
			stmt = this.connection.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			if (! rs.isBeforeFirst())
			{
				// note this but continue
				System.out.println("No test instruments detected.. ever... anywhere.");
			}
			else
			{
				while(rs.next())
				{
					long key = rs.getLong("modelId");
					String value = rs.getString("internalName");
					if (value.equals("Testing (KF4D)") || value.equals("KF4D"))
					{
						// Manual or auto KF4D
						value = KF4D_TEST;
					}
					else if (value.equals("Testing (KFALP)") || value.equals("ALP"))
					{
						// Manual or auto ALP
						value = KFP_TEST;
					}
					else if (value.equals("GPI"))
					{
						//  GPI (duh!)
						value = GPI_TEST;
					}
					else
					{
						value = "";
					}
					
					modelTests.put(key, value);
				}
			}
			
			// filter the full list (only put out the ones that exist in the model list)
			for (KeyValuePair kvp : ret.getModelArray())
			{
				long key = kvp.getTheKey();
				String tst = modelTests.get(key);
				if (tst == null)
				{
					continue;
				}
				KeyValuePair entry = new KeyValuePair();
				entry.setTheKey(key);
				entry.setTheValue(tst);
				mtl.add(entry);
			}

			return ret;
		}
		catch (SQLException ex)
		{
			// handle any errors
			String errStr = "SQL fetching model test list.  " +
					"SQLException: " + ex.getMessage() + ", " +
					"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode();
			System.out.println(errStr);
			throw new Exception(errStr);
		}
		finally
		{
			if (rs != null)
			{
				try  {  rs.close();  }
				catch (SQLException sqlEx) {  /* Swallow the error */  }
				rs = null;
			}
			if (stmt != null)
			{
				try  {  stmt.close();  }
				catch (SQLException sqlEx) {  /* Swallow the error */  }
		        stmt = null;
		    }
		}
	}
}
