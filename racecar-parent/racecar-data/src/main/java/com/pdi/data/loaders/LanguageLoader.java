/**
 * Copyright (c) 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.loaders;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Loader for the language definition table
 * 
 * NOTE:  Unless the ids in the SQL Server table platform.dbo.Languages
 * are identical from environment to environment, the data in this loader
 * MUST BE MODIFIED in order to load the correct data!
 *  
 * @author kbeukelm
 *
 */
public class LanguageLoader
{
	// Static constants
    protected static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String LAST_USER = "kbeukelm";
    private static final String US_ENGLISH_ID = "1";

	private static final String[][] LANGUAGE_DATA = new String[][] 
   	{
   		//				id	code		name (default lang)		name (in self)										rptLang		egLang
		(new String[] { "1", "en",		"English",				"English (United States)",								"1",	"1"}),
		(new String[] { "2", "en_GB",	"British English",		"English (United Kingdom)",								"0",	"1"}),
		(new String[] { "3", "fr",		"French",				"Fran&#0231;ais",										"1",	"1"}),
		(new String[] { "4", "es",		"Spanish",				"Espa&#0241;ol",										"1",	"1"}),
		(new String[] { "5", "it",		"Italian",				"Italiano",												"1",	"1"}),
		(new String[] { "6", "de",		"German",				"Deutsch",												"1",	"1"}),
		(new String[] { "7", "pt_BR",	"Portuguese (Brazil)",	"Portuguese (Brazil)",									"1",	"0"}),
		(new String[] { "8", "sv",		"Swedish",				"Svenska",												"0",	"1"}),
		(new String[] { "9", "ja",		"Japanese",				"&#26085;&#26412;&#35486; (&#26085;&#26412;)",			"1",	"1"}),
		(new String[] {"10", "zh_CN",	"Simplified Chinese",	"&#20013;&#25991; (&#20013;&#22269;)",					"1",	"1"}),
		(new String[] {"11", "pl",		"Polish",				"Polski",												"0",	"0"}),
		(new String[] {"12", "ru",		"Russian",				"&#1088;&#1091;&#1089;&#1089;&#1082;&#1080;&#1081;",	"0",	"0"}),
		(new String[] {"13", "nl",		"Dutch",				"Nederlands",											"0",	"1"}),
		(new String[] {"14", "id",		"Bahasa Indonesia",		"Bahasa Indonesia",										"0",	"0"}),
		(new String[] {"15", "cs",		"Czech",				"Če&#353;tina",											"0",	"0"}),
		(new String[] {"16", "hu",		"Hungarian",			"Magyar",												"0",	"1"}),
		(new String[] {"17", "no",		"Norwegian",			"Norsk",												"0",	"1"}),
		(new String[] {"18", "ko",		"Korean",				"&#54620;&#44397;&#50612;",								"1",	"1"})
	};
	

	/*
	 * Runs as a headless app
	 */
	public static void main(String args[])
	{
		if (args.length == 0)
		{
			System.out.println(" No parameters were detected.");
			System.out.println(parmDescString());

			return;
		}

		if (args.length != 3)
		{
			System.out.println("Incorrect number of parameters (" + args.length + ")");
			System.out.println(parmDescString());
			return;
		}
		
		String url = args[0];
		String uid = args[1];
		String pw = args[2];
		
//		String phase = null;
		Connection con = null;
//		boolean stat = false;
//		PreparedStatement ngIns = null;
//		PreparedStatement ngId = null;
//		PreparedStatement nIns = null;

		try
		{
			// Open the DB
			con = openDb(url, uid, pw);
			if (con == null)
			{
				return;
			}
			System.out.println("Database opened.  url: " + url);
			
			// Delete the old text data and the language data
			if (! deleteOldLanguages(con))
			{
				return;
			}
			
			// Insert the new data
			if (! insertAllLanguages(con))
			{
				return;
			}
			
			// Commit the transaction  --  NOT APPLICABLE IN MYSQL
			//commit(con);

			System.out.println("Completed...");
		}
		finally
		{
			if (con != null)
			{
				try
				{
					con.close();
				}
				catch (Exception e)  { /* swallow it */ }
			}
		}
	}
	
	
	/*
	 * Method to display appropriate parameters.
	 * 
	 * Note that there is no closing nwe-line character; assumes that this method is called
	 * in a println() or that the user will handle the closing new-line himself.
	 */
	private static String parmDescString()
	{
		String str = "  This program requires three arguments as follows:\n";
		str += "    URL      - The URL where the database lies.  Examples:\n";
		str += "                 'jdbc:mysql://mspdbdev2:3306/pdi_certify' or\n";
		str += "                 'jdbc:mysql://sanfranqasql3:3306/scoring_reporting'\n";
		str += "    User id  - A user id for the specified database with the appropriate permissions.\n";
		str += "    Password - The password for the above user ID.";
		
		return str;
	}
	
	/*
	 * Open the db connection
	 */
	private static Connection openDb(String url, String uid, String pw)
	{
		Connection con = null;
		
		try
		{
			Class.forName(DRIVER);
			con = DriverManager.getConnection(url, uid, pw);
			con.setAutoCommit(false);
		}
		catch (Exception e)
		{
			System.out.println("Unable to open database.");
			e.printStackTrace();
		}
			
		return con;
	}
	
	
	/*
	 * delete the current content of the pdi_abd_lang table and the associated texts
	 */
	private static boolean deleteOldLanguages(Connection con)
	{
		String phase = "";
		Statement langStmt = null;
		ResultSet rs = null;
		PreparedStatement delTextPS = null;
		PreparedStatement delLangPS = null;
		StringBuffer q = new StringBuffer();
		
		try
		{
			// Create the prepared statements
			phase = "Create DELETE prepared statements";
			q.setLength(0);
			q.append("DELETE FROM pdi_abd_text ");
			q.append("  WHERE textId = ?");
			delTextPS = con.prepareStatement(q.toString());

			q.setLength(0);
			q.append("DELETE FROM pdi_abd_language ");
			q.append("  WHERE languageId = ?");
			delLangPS = con.prepareStatement(q.toString());

			// Get the data
			phase = "Fetch language data.";
			q.setLength(0);
			q.append("SELECT ll.languageId, ");
			q.append("       ll.languageCode, ");
			q.append("       ll.textId ");
			q.append("  FROM pdi_abd_language ll");
			langStmt = con.createStatement();
	       	rs = langStmt.executeQuery(q.toString());
	       	
	       	phase = "Retrieve language data";
	       	if (! rs.isBeforeFirst())
	       	{
	       		// No data... Done (success)
	       		return true;
	       	}
	       	while (rs.next())
	       	{
	       		phase = "fetching result data";
				// Delete the text
	       		String id = rs.getString("languageId");
	       		String txtId = rs.getString("textId");
	       		String langCode = rs.getString("languageCode");
	       		
	       		phase = "Deleting texts for langId " + id + " (" + txtId + ")";
	       		delTextPS.setString(1, txtId);
	       		delTextPS.executeUpdate();

	       		// Delete the language
	       		phase = "Deleting language " + langCode + " (" + id + ")";
	       		delLangPS.setString(1, id);
	       		delLangPS.executeUpdate();
	       		
	       		System.out.println(langCode + " deleted");
	       	}
	       	
	       	// Made it here.. it's OK
			return true;
		}
		catch (Exception e)
		{
			System.out.println("Delete failed -- Phase = " + phase);
			e.printStackTrace();
			try
			{
				con.rollback();
			} catch (Exception f) { /* Swallow it */ }
			return false;
		}
		finally
		{
			if (rs != null)
			{
				try {  	rs.close(); }  catch (SQLException e) { /* Swallow it */ };
				rs = null;
			}
			if (langStmt != null)
			{
				try {  	langStmt.close(); }  catch (SQLException e) { /* Swallow it */ };
				langStmt = null;
			}
			if (delTextPS != null)
			{
				try {  	delTextPS.close(); }  catch (SQLException e) { /* Swallow it */ };
				delTextPS = null;
			}
			if (delLangPS != null)
			{
				try {  	delLangPS.close(); }  catch (SQLException e) { /* Swallow it */ };
				delLangPS = null;
			}
		}
	}

	
	/*
	 * Insert the new language data from the list
	 */
	private static boolean insertAllLanguages(Connection con)
	{
		String phase = "";
		PreparedStatement insTextPS = null;
		PreparedStatement updtTextPS = null;
		PreparedStatement insLangPS = null;
		ResultSet rs = null;
		StringBuffer q = new StringBuffer();
		
		try
		{
			// Create the prepared statements
			phase = "Create INSERT prepared statements";
			// insert text PS
			q.setLength(0);
			q.append("INSERT INTO pdi_abd_text ");
			q.append("  (textId, languageID, text, isDefault, active, lastUserId, lastDate) ");
			q.append("  VALUES(?, ?, ?, ?, 1, '" + LAST_USER + "', NOW())");
			insTextPS = con.prepareStatement(q.toString(),Statement.RETURN_GENERATED_KEYS);

			// update text PS
			q.setLength(0);
			q.append("UPDATE pdi_abd_text ");
			q.append("  SET textID = ? ");
			q.append("  WHERE uniqueID = ?");
			updtTextPS = con.prepareStatement(q.toString());

			// insert language PS
			q.setLength(0);
			q.append("INSERT INTO pdi_abd_language ");
			q.append("  (languageId, languageCode, textId, rptLang, egLang, active, lastUserId, lastDate) ");
			q.append("  VALUES(?, ?, ?, ?, ?, 1, '" + LAST_USER + "', NOW())");			
			insLangPS = con.prepareStatement(q.toString());

			// loop through insert data
			String[] ent = null;
			for (int i=0; i < LANGUAGE_DATA.length; i++)
			{
				ent = LANGUAGE_DATA[i];
				String langId = ent[0];
				String langCode = ent[1];
				String dfltName = ent[2];
				String transName = ent[3];
				String rptLang = ent[4];
				String egLang = ent[5];
				String dfltId = null;
				
				// Insert the name in US English (default language)
				phase = "Insert default text for " + langCode;
				insTextPS.setString(1, "0");	// Don't have the right textId yet
				insTextPS.setString(2, US_ENGLISH_ID);
				insTextPS.setString(3, dfltName);
				insTextPS.setString(4, "1");	// US English is the default for here
				insTextPS.executeUpdate();
				phase = "Fetching default text id for " + langCode;
				rs = insTextPS.getGeneratedKeys();
				if (rs == null  ||  ! rs.isBeforeFirst())
				{
					//it didn't return an ID...
					throw new Exception("No default definition id returned;  lang=" + langCode);
				}
				rs.next();
				dfltId = rs.getString(1);
				
				// Update the row so it has the textId
				phase = "Updating textID";
				updtTextPS.setString(1, dfltId);	// Text ID
				updtTextPS.setString(2, dfltId);	// Unique ID (same for the default row)
				updtTextPS.executeUpdate();
				
				// Insert the name in the language itself (but not if US English)
				if (! langId.equals(US_ENGLISH_ID))
				{
					phase = "Insert translated text for " + langCode;
					insTextPS.setString(1, dfltId);
					insTextPS.setString(2, langId);
					insTextPS.setString(3, transName);
					insTextPS.setString(4, "0");
					insTextPS.executeUpdate();
				}

				// Insert the language row
				phase = "Insert language row for " + langCode;
				insLangPS.setString(1, langId);
				insLangPS.setString(2, langCode);
				insLangPS.setString(3, dfltId);
				insLangPS.setString(4, rptLang);
				insLangPS.setString(5, egLang);
				insLangPS.executeUpdate();
	       		
	       		System.out.println(langCode + " inserted");
	       	}
	       	
	       	// Made it here.. it's OK
			return true;
		}
		catch (Exception e)
		{
			System.out.println("Insert failed -- Phase = " + phase);
			e.printStackTrace();
			try
			{
				con.rollback();
			} catch (Exception f) { /* Swallow it */ }
			return false;
		}
		finally
		{
			if (rs != null)
			{
				try {  	rs.close(); }  catch (SQLException e) { /* Swallow it */ };
				rs = null;
			}
			if (insTextPS != null)
			{
				try {  	insTextPS.close(); }  catch (SQLException e) { /* Swallow it */ };
				insTextPS = null;
			}
			if (insLangPS != null)
			{
				try {  	insLangPS.close(); }  catch (SQLException e) { /* Swallow it */ };
				insLangPS = null;
			}
		}
	}
	
	
//	/*
//	 * commit the transaction -- Doesn't work for My SQL
//	 */
//	private static void commit(Connection con)
//	{
//		try
//		{
//			con.rollback();
//			System.out.println("Rollback enabled... Changes won't take.");
//			//con.commit();
//		}
//		catch (Exception e)
//		{
//			System.out.println("Commit failed.");
//			e.printStackTrace();
//			try
//			{
//				con.rollback();
//			} catch (Exception f) { /* Swallow it */ }
//		}
//	}
}
