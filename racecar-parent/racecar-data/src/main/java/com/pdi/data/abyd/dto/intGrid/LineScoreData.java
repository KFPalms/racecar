package com.pdi.data.abyd.dto.intGrid;

import java.util.ArrayList;

/**
* LineScoreData is a thin class used to contain some needed
* information about the scoring components (Cog names, GPI scale
* scores, etc.) for a particular IG line.
*
* @author		Ken Beukelman
*/
public class LineScoreData {
	//
	// Instance data.
	//
	private ArrayList<IGScaleScoreData> _cogColumnScores; // need the isNeg for the edit button data
	private ArrayList<IGScaleScoreData> _gpiColumnScores; // so that we can calc the averages correctly

	//
	// Constructors.
	//
	public LineScoreData()
	{
		// Nothing much happens here
		_cogColumnScores = new ArrayList<IGScaleScoreData>();
		_gpiColumnScores = new ArrayList<IGScaleScoreData>();
	}

	//
	// Instance methods.
	//
	
	/*****************************************************************************************/
	public ArrayList<IGScaleScoreData> getCogColumnScores()
	{
		return _cogColumnScores;
	}

	public void setCogColumnScores(ArrayList<IGScaleScoreData> value)
	{
		_cogColumnScores = value;
	}

	/*****************************************************************************************/
	public ArrayList<IGScaleScoreData> getGpiColumnScores()
	{
		return _gpiColumnScores;
	}

	public void setGpiColumnScores(ArrayList<IGScaleScoreData> value)
	{
		_gpiColumnScores = value;
	}		
}
