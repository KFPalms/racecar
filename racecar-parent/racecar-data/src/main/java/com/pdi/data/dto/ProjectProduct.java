/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.dto;

import java.util.HashMap;


/**
 * ProjectProduct is a basic data class that contains related engagement and product data.
 * 
 * @author		Gavin Myers
 */
public class ProjectProduct extends CoreDataObject
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	/**
	 * 
	 */
	private static final long serialVersionUID = 3288727981899057980L;
	//
	// Instance data.
	//
	private Project engagement;
	private Product product;
	private NormGroup generalPopulation;
	private NormGroup specialPopulation;
	
	private HashMap<String, String> metadata;

	//
	// Constructors.
	//
	public ProjectProduct()
	{
		// does nothing right now
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public void setProject(Project value)
	{
		engagement = value;
	}
	
	public Project getProject()
	{
		return engagement;
	}

	/*****************************************************************************************/
	public void setProduct(Product value)
	{
		product = value;
	}
	
	public Product getProduct()
	{
		return product;
	}

	/*****************************************************************************************/
	public void setGeneralPopulation(NormGroup value)
	{
		generalPopulation = value;
	}
	
	public NormGroup getGeneralPopulation()
	{
		return generalPopulation;
	}

	/*****************************************************************************************/
	public void setSpecialPopulation(NormGroup value)
	{
		specialPopulation = value;
	}
	
	public NormGroup getSpecialPopulation()
	{
		return specialPopulation;
	}

	/*****************************************************************************************/
	public void setMetadata(HashMap<String, String> value)
	{
		metadata = value;
	}
	
	public HashMap<String, String> getMetadata()
	{
		return metadata;
	}
}
