package com.pdi.data.v2.helpers.delegated.ut;

import java.util.ArrayList;

import com.pdi.data.helpers.interfaces.IClientHelper;

import com.pdi.data.helpers.HelperDelegate;
//import com.pdi.properties.PropertyLoader;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.dto.Client;
//import com.pdi.ut.UnitTestUtils;
import junit.framework.TestCase;

public class ClientHelperUT extends TestCase {

	public ClientHelperUT(String name)
	{
		super(name);
	}
	
	public void testGetClient()
	throws Exception
	{
		SessionUser session = new SessionUser();
		session.getMetadata().put("USERNAME", "cdunn");
		session.getMetadata().put("PASSWORD", "cdunn");
////		HelperDelegate.setEnvrionment(PropertyLoader.getProperty("com.pdi.data.v2.application","global.application"));
		// v2
		//System.out.println(PropertyLoader.getProperty("com.pdi.data.v2.application","datasource"));

		//TODO: Identify if it is possible to pull strings from outside projects PropertyLoader.getProperty("com.pdi.data.application","datasource.v2")
		
		IClientHelper iclient = HelperDelegate.getClientHelper();
		ArrayList<Client> clients = iclient.all(session);
		for(Client client : clients) {
			System.out.println(client.getId() + " - " + client.getName());
		}
		
		String clientId = "IYBWJXCM";  //  generic tpot client          
		Client singleClient = iclient.fromId(session, clientId);
		System.out.println("singleClient.getid = " + singleClient.getId());
		
//		// adapt
//		iclient = HelperDelegate.getClientHelper(PropertyLoader.getProperty("com.pdi.data.application", "datasource.adapt"));
//		clients = iclient.all(session);
//		for(Client client : clients) {
//			System.out.println(client.getId() + " - " + client.getName());
//		}
//		
//		//nhn
//		iclient = HelperDelegate.getClientHelper(PropertyLoader.getProperty("com.pdi.data.application","datasource.nhn"));
//		clients = iclient.all(session);
//		for(Client client : clients) {
//			System.out.println(client.getId() + " - " + client.getName());
//		}
//		
//		
//		// generic
//		iclient = HelperDelegate.getClientHelper();
//		clients = iclient.all(session);
//		for(Client client : clients) {
//			System.out.println(client.getId() + " - " + client.getName());
//		}
		
		
	}
	
	
}
