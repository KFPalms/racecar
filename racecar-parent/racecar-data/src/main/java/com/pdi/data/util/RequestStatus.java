/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.util;

/**
 * BaseStatus is a container for status information about a query.
 * It is designed for use both within the database utilities and
 * for transfer of data to the Business layer.
 *
 * @author		Ken Beukelman
 */
public class RequestStatus
{
	//
	// Static data.
	//
	
	// Generic status types
	public static final int RS_ERROR = -1;		// An error was encountered in fetching the DTO
	public static final int RS_OK = 0;			// DTO fetch succeeded
	// DB Status types
	public static final int DBS_NO_DATA = 1;	// No data returned from the DB fetch (May be expected)
	public static final int DBS_DUP_KEY = 2;	// Duplicate key encountered on DB insert (May be anticipated)
	// Web Service status types
	public static final int WB_NO_DATA = 3;		// No data returned from web service
////	public static final int WB_LIST_ERROR = 4;	// One or more elements in the list was returned in error
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private int _statusCode = RS_OK;	// DBS_xxx value
	private int _exceptionCode = 0;
	private String _exceptionMessage = null;

	
	//
	// Constructors.
	//
	public RequestStatus()
	{
		// Nothing done on a no-parameter constructor
	}
	
	public RequestStatus(int sCode, int eCode, String eMsg)
	{
		// initialize with all three content parameters
		_statusCode = sCode;
		_exceptionCode = eCode;
		_exceptionMessage = eMsg;
	}
	
	public RequestStatus(int sCode, String eMsg)
	{
		//initialize with a status and a message
		_statusCode = sCode;
		_exceptionMessage = eMsg;
	}
	
	public RequestStatus(int sCode)
	{
		//initialize with a status (usually used for "no data" situations)
		_statusCode = sCode;
	}
	
	public RequestStatus(RequestStatus rs)
	{
		//initialize from another RequestStaus object
		_statusCode = rs.getStatusCode();
		_exceptionCode = rs.getExceptionCode();
		_exceptionMessage = rs.getExceptionMessage();
	}

	//
	// Instance methods.
	//
	
	/**
	 * Generic toString method
	 */
	public String toString() {
		String str = "RequestStatus data - " +
					 "statusCcode=" + this._statusCode +
					 ", exceptionCode=" + this._exceptionCode +
					 ", exceptionMessage=" + this._exceptionMessage;
		return str;
	}

	/*
	 * Checks if the OK status is present
	 */
	public boolean isOk()
	{
		return (this._statusCode == RequestStatus.RS_OK);
	}
	
	//////////////////////////////////////
	//        Setters and Getters       //
	//////////////////////////////////////
	
	/********************************************************/
	public void setStatusCode(int value)
	{
		this._statusCode = value;
	}

	public int getStatusCode()
	{
		return this._statusCode;
	}
	
	/********************************************************/
	public void setExceptionCode(int value)
	{
		this._exceptionCode = value;
	}

	public int getExceptionCode()
	{
		return this._exceptionCode;
	}
	
	/********************************************************/
	public void setExceptionMessage(String value)
	{
		this._exceptionMessage = value;
	}

	public String getExceptionMessage()
	{
		return this._exceptionMessage;
	}
}
