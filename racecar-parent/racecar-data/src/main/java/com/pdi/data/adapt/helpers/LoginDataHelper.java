/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.adapt.helpers;

import java.io.IOException;
import java.io.StringReader;
//import java.sql.Connection;
//import java.sql.ResultSet;
//import java.sql.SQLException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.PostMethod;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

//import com.mysql.jdbc.PreparedStatement;
import com.pdi.properties.PropertyLoader;


public class LoginDataHelper
{
	/**
	 * Validates the login
	 * 
	 * @param userid The user ID to validate
	 * @param password The password to validate
	 * @throws HttpException, IOException, ParserConfigurationException, SAXException
	 * @return The login validity (boolean)
	 */
	public static boolean validateLogin(String userid, String password)
		throws HttpException, IOException, ParserConfigurationException, SAXException
	{
		String req = PropertyLoader.getProperty("com.pdi.data.adapt.application", "castIron.userValidationUrl");
		req += "?userid=" + userid + "&password=" + password;
		
		PostMethod post = new PostMethod(req);
		HttpClient client = new HttpClient();
		int result = client.executeMethod(post);
		if (result != 200)		{
			throw new HttpException("Data not retrieved.  Status = " + result);
		}
		
		String strXMLResponse = post.getResponseBodyAsString();

		DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
		Document doc = docBuilder.parse(new InputSource(new StringReader(strXMLResponse)));
		
		try {
			return doc.getElementsByTagName("result").item(0).getAttributes().item(0).getNodeValue().equalsIgnoreCase("true");
		}catch (Exception e)	{
			return false;
		}
	}
	

	
}
