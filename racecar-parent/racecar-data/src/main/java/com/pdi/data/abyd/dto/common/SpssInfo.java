package com.pdi.data.abyd.dto.common;

/**
 * SpssInfo is a thin class to hold iformation about a single spss/scale item associated with competency scoring
 */
public class SpssInfo
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data
	//
	private String _spssId;
	private boolean _inverted = false;
	
	//
	// Constructors
	//
	
	// Default constructor is hidden
	@SuppressWarnings("unused")
	private SpssInfo()
	{
		// Default constructor
	}
	
	// Only public constructor
	public SpssInfo(String spssId, boolean isInverted)
	{
		this._spssId = spssId;
		this._inverted = isInverted;
	}

	//
	// Instance methods
	//

	/*****************************************************************************************/
	public String getSpssId()
	{
		return this._spssId;
	}

	// No setter.  Set in constructor only
//	public void setSpss(String value)
//	{
//		this._spssId = value;
//	}

	/*****************************************************************************************/
	// Getter without a get for boolean
	public boolean isInverted()
	{
		return this._inverted;
	}

	// No setter.  Set in constructor only
//	public void setIsInverted(boolean value)
//	{
//		this._inverted = value;
//	}
}
