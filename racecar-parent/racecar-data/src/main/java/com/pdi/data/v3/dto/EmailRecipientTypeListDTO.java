/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.dto;

import java.util.ArrayList;

import com.pdi.data.util.BaseDTO;
import com.pdi.data.util.RequestStatus;

/**
 * EmailRecipientTypeListDTO is a class designed to convey the information for a number of
 * recipient types used in the PDI V3 extensions.  It contains an ArrayList of
 * EmailRecipientTypeDTO objects; the status in each of those may be redundant.
 * 
 * @author		Ken Beukelman
  */
public class EmailRecipientTypeListDTO extends BaseDTO
{
	//
	// Static data.
	//


	//
	// Static methods.
	//


	//
	// Instance data.
	//
	private ArrayList<EmailRecipientTypeDTO> _typeList = new ArrayList<EmailRecipientTypeDTO>();


	//
	// Constructors.
	//
	public EmailRecipientTypeListDTO()
	{
		// does nothing right now
	}
	
	// Set up a new DTO with status but no data
	public EmailRecipientTypeListDTO(RequestStatus stat)
	{
		this.setStatus(stat);
	}
//	
//	// Set up a new DTO with status but no data
//	public EmailRecipientTypeListDTO(int sc, int ec, String em)
//	{
//		this.setStatusCode(sc);
//		this.setExceptionCode(ec);
//		this.setExceptionMessage(em);
//	}
	
//	public EmailRecipientTypeListDTO(int sc, int ec, String em, ArrayList<EmailRecipientType> list)
//	{
//		this.setStatusCode(sc);
//		this.setExceptionCode(ec);
//		this.setExceptionMessage(em);
//		_typeList = list;
//	}
	
	
	//
	// Instance methods.
	//

	/**
	 * Generic toString method
	 */
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("EmailRecipientTypeListDTO:  ");
		sb.append("Status code=" + this.getStatus().getStatusCode() + ", count=" + _typeList.size());
		for(int i=0; i < _typeList.size(); i++)
		{
			sb.append("\n      id=" + _typeList.get(i).getId());
			sb.append(", name=>" + _typeList.get(i).getName() + "<");
		}
		return sb.toString();
	}
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public ArrayList<EmailRecipientTypeDTO> getTypeList()
	{
		return _typeList;
	}
	
	public void setTypeList(ArrayList<EmailRecipientTypeDTO> value)
	{
		_typeList = value;
	}	
}
