/*
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.setup.ut;

import java.sql.Connection;
//import java.util.ArrayList;

//import com.pdi.data.abyd.dto.setup.ReportModelCompChildDTO;
//import com.pdi.data.abyd.dto.setup.ReportModelCompetencyDTO;
//import com.pdi.data.abyd.dto.setup.ReportModelStructureDTO;
//import com.pdi.data.abyd.dto.setup.ReportModelSuperFactorDTO;
import com.pdi.data.abyd.helpers.setup.ReportModelDataHelper;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

public class ReportModelDataHelperUT extends TestCase
{
	//
	// Constructors
	//

	public ReportModelDataHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
		throws Exception
    {
		junit.textui.TestRunner.run(ReportModelDataHelperUT.class);
    }


	// NO LONGER USED!!!
//	/*
//	 * testDeleteReportModel
//	 */
////	public void testDeleteReportModel()
////		throws Exception
////	{
////		UnitTestUtils.start(this);
////		AbyDDatabaseUtils.setUnitTest(true);
////
////		Connection con = AbyDDatabaseUtils.getDBConnection();
////		// Recommended that this be the same as the dnaId in the next test or data will double up
////		//long dnaId = 20;
////		//long dnaId = 41;
////		long dnaId = 5;
////			
////		ReportModelDataHelper helper = new ReportModelDataHelper();
////		helper.deleteReportModel(con, dnaId);
////		//System.out.println("Check the data on the database for Report Model " + dnaId);
////
////		UnitTestUtils.stop(this);
////	}


	/*
	 * testCreateNewReportModelFromDna
	 */
	public void testCreateNewReportModelFromDna()
		throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);

		Connection con = AbyDDatabaseUtils.getDBConnection();
		long dnaId = 6;	

		ReportModelDataHelper helper = new ReportModelDataHelper();
		helper.createNewReportModelFromDna(con, dnaId);
		System.out.println("Function returns nothing... check the database.");

		UnitTestUtils.stop(this);
	}


	/*
	 * testGetReportModel
	 */
//	public void testGetReportModel()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//
//		Connection con = AbyDDatabaseUtils.getDBConnection();
//		
//		long dnaId = 6;
//
//		ReportModelStructureDTO dto = new ReportModelDataHelper().getReportModel(con, dnaId);
//		System.out.println(dto.toString());
//
//		UnitTestUtils.stop(this);
//	}


	/*
	 * testGetReportModelCompetencyChildren
	 */
//	public void testGetReportModelCompetencyChildren()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//
//		Connection con = AbyDDatabaseUtils.getDBConnection();
//		
//		long dnaId = 6;
//
//		ArrayList<ReportModelCompChildDTO> dtoList = new ReportModelDataHelper().getReportModelCompetencyChildren(con, dnaId);
//		System.out.println("Child comp count = " + dtoList.size());
//		for (ReportModelCompChildDTO dto : dtoList)
//		{
//			System.out.println(dto.toString());
//		}
//
//		UnitTestUtils.stop(this);
//	}
	
	
	/*
	 * testGetReportModelWithScores -- Move the to Report Input... the functionality has gone there
	 */
//	public void testGetReportModelWithScores()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//
//		Connection con = AbyDDatabaseUtils.getDBConnection();
//		
//		
//		long dnaId = 6;	// dev
//		String pId = "59639";
//		ReportModelDataHelper reportModelDataHelper = new ReportModelDataHelper();
//		ReportModelStructureDTO dto = reportModelDataHelper.getReportModelWithScores(con, dnaId, pId);
//		System.out.println(dto.toString());
//
//		UnitTestUtils.stop(this);
//	}

	
	/*
	 * testReplaceReportModule
	 */
//	public void testReplaceReportModule()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//
//		Connection con = AbyDDatabaseUtils.getDBConnection();
//
//		// Don't use thei bogus structure... See below
//			//// Make a bogus model structure
//			//ReportModelStructureDTO struct = new ReportModelStructureDTO();
//			//struct.setDnaId(9999);
//			//ReportModelSuperFactorDTO sf1 = new ReportModelSuperFactorDTO();
//			//sf1.setSuperFactorId(99991);
//			//sf1.setDisplayName("SF1");
//			//struct.getSuperFactorList().add(sf1);
//			//ReportModelCompetencyDTO cmp1_1 = new ReportModelCompetencyDTO();
//			////cmp1_1.setRmCompId(999911);
//			//cmp1_1.setDisplayName("Comp 1 for SF 1");
//			//sf1.getCompList().add(cmp1_1);
//			//ReportModelCompChildDTO cc1_1_1 =  new ReportModelCompChildDTO();
//			//cc1_1_1.setDnaCompId(9999111);
//			//cmp1_1.getChildren().add(cc1_1_1);
//			//ReportModelCompChildDTO cc1_1_2 =  new ReportModelCompChildDTO();
//			//cc1_1_2.setDnaCompId(9999112);
//			//cmp1_1.getChildren().add(cc1_1_2);
//			//ReportModelCompetencyDTO cmp1_2 = new ReportModelCompetencyDTO();
//			////cmp1_2.setRmCompId(999912);
//			//cmp1_2.setDisplayName("Comp 2 for SF 1");
//			//sf1.getCompList().add(cmp1_2);
//			//ReportModelCompChildDTO cc1_2_1 =  new ReportModelCompChildDTO();
//			//cc1_2_1.setDnaCompId(9999121);
//			//cmp1_2.getChildren().add(cc1_2_1);
//			//ReportModelSuperFactorDTO sf2 = new ReportModelSuperFactorDTO();
//			//sf2.setSuperFactorId(99992);
//			//sf2.setDisplayName("SF2");
//			//struct.getSuperFactorList().add(sf2);
//			//ReportModelCompetencyDTO cmp2_1 = new ReportModelCompetencyDTO();
//			////cmp2_1.setRmCompId(999921);
//			//cmp2_1.setDisplayName("Comp 1 for SF 2");
//			//sf2.getCompList().add(cmp2_1);
//			//ReportModelCompChildDTO cc2_1_1 =  new ReportModelCompChildDTO();
//			//cc2_1_1.setDnaCompId(9999211);
//			//cmp2_1.getChildren().add(cc2_1_1);
//			//ReportModelCompChildDTO cc2_1_2 =  new ReportModelCompChildDTO();
//			//cc2_1_2.setDnaCompId(9999212);
//			//cmp2_1.getChildren().add(cc2_1_2);
//			//ReportModelCompetencyDTO cmp2_2 = new ReportModelCompetencyDTO();
//			////cmp2_2.setRmCompId(999922);
//			//cmp2_2.setDisplayName("Comp 2 for SF 2");
//			//sf2.getCompList().add(cmp2_2);
//			//ReportModelCompChildDTO cc2_2_1 =  new ReportModelCompChildDTO();
//			//cc2_2_1.setDnaCompId(9999221);
//			//cmp2_2.getChildren().add(cc2_2_1);
//			////System.out.println("Input - " + struct.toString());
//		
//		// Run this right after the create tes... That way we have a known model structure
//		long dnaId = 6;	// Use the same DNA ID as in the setup test
//		
//		// Play with the data
//		ReportModelStructureDTO dto = new ReportModelDataHelper().getReportModel(con, dnaId);
//		for (ReportModelSuperFactorDTO sf : dto.getSuperFactorList())
//		{
//			// Change the sequence
//			sf.setSeq(5 - sf.getSeq());
//			// Change the display text
//			String str = sf.getDisplayName();
//			int idx = str.indexOf("-XYZ");
//			if (idx > -1)
//			{
//				//Strip it
//				str = str.substring(0, idx);
//			} else {
//				str += "-KDXYZB";
//			}
//			sf.setDisplayName(str);
//			
//			for(ReportModelCompetencyDTO cmp : sf.getCompList())
//			{
//				cmp.setSeq(5 - cmp.getSeq());
//				if (cmp.getEssential())
//				{
//					cmp.setEssential(false);
//				}
//				else
//				{
//					cmp.setEssential(true);
//				}
//				String str2 = cmp.getDisplayName();
//				int idx2 = str2.indexOf("-XYZ");
//				if (idx2 > -1)
//				{
//					//Strip it
//					str2 = str2.substring(0, idx2);
//				} else {
//					str2 += "-XYZ";
//				}
//				cmp.setDisplayName(str2);
//			}
//		}
//		
////		// OK, delete an SF from the input DTO
////		int siz = dto.getSuperFactorList().size();
////		dto.getSuperFactorList().remove(siz - 1);
//
//		 new ReportModelDataHelper().replaceReportModel(con, dto);
//		System.out.println("No output... check database for effects");
//
//		UnitTestUtils.stop(this);
//	}

	 
	//	/*
	////	 * testDNAStructure
	////	 */
	////	public void testDNAStructure()
	////		throws Exception
	////	{
	////		UnitTestUtils.start(this);
	////		AbyDDatabaseUtils.setUnitTest(true);
	////
	////		Connection con = AbyDDatabaseUtils.getDBConnection();
	////		long dnaId = 41;
	////		
	////		System.out.println("DNAStructureHelperUT:");
	////		System.out.println("    DNA ID=" + dnaId);
	////		
	////		DNAStructureHelper helper = new DNAStructureHelper(con, dnaId);
	////		DNAStructureDTO dto = helper.getSetupDNAStructData();
	////		
	////		System.out.println("    Output:");
	////		System.out.println("        Competencies:");
	////		for (KeyValuePair kvp : dto.getCompetencyArray())
	////		{
	////			System.out.println("          " + kvp.getTheKey() + " - " + kvp.getTheValue());
	////		}
	////		System.out.println("        Competency Group Data:");
	////		for (CompGroupData grp : dto.getFullCompArray())
	////		{
	////			System.out.println("          ID=" + grp.getCgId() + ", Name=" + grp.getCgName());
	////			for (CompElementDTO cmp : grp.getCgCompList())
	////			{
	////				System.out.println("              ID=" + cmp.getCompId() + ", essential=" + cmp.getIsEssential() + ",Name=" + cmp.getCompName());
	////			}
	////		}
	////		System.out.println("        Modules:");
	////		for (ModDataDTO mod : dto.getModArray())
	////		{
	////			System.out.println("          ID=" + mod.getModId() + ", edited=" + mod.getModEditable() + ", sumitted=" + mod.getModSubmitted() + ", Name=" + mod.getModName());
	////		}
	////		System.out.println("        Selected Modules:");
	////		for (Long sel : dto.getSelectedModsArray())
	////		{
	////			System.out.println("          " + sel.longValue());
	////		}
	////		System.out.println("        Intersections:");
	////		for (DNACellDTO mc : dto.getModCompIntersection())
	////		{
	////			System.out.println("          mod=" + mc.getModuleId() + ", comp=" + mc.getCompetencyId() + ", used=" + mc.getIsUsed() + ", score=" + mc.getScore());
	////		}
	////
	////		UnitTestUtils.stop(this);
	////	}
}
