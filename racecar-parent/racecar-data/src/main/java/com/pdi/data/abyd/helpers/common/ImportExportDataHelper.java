/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


/**
 * Class used to take data to and from abyd, for example, how to get a dna id with just v2 job/part info, or 
 * how to link an abyd instrument from an adapt user/pass
 * 
 * @author gmyers
 */
public class ImportExportDataHelper
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	
	//
	// Constructors.
	//

	//
	// Instance methods.
	//

	/*
	 * getDnaFromV2
	 * 
	 * Wont return the DNA id unless there is IG response data
	 */
	public long getDnaFromV2(Connection con, String jobId, String participantId) 
	throws Exception 
	{
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT dna.dnaId ");
		sqlQuery.append("  FROM pdi_abd_dna dna ");
		sqlQuery.append("    INNER JOIN pdi_abd_igrid_response igr ");
		sqlQuery.append("  WHERE igr.dnaId = dna.dnaId ");
		sqlQuery.append("    AND dna.jobId = '" + jobId + "' ");
		sqlQuery.append("    AND igr.participantId = '" + participantId + "'");

		try
		{
			stmt = con.createStatement();
        	rs = stmt.executeQuery(sqlQuery.toString());
        	
        	if(rs == null || !rs.isBeforeFirst())
        	{
        		return -1;
        	}

        	// Get the text (should be one and only one row)
        	rs.next();

			return rs.getLong("dnaId");
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("getReportNotes SQL.  " +
					"SQLException: " + ex.getMessage() + ", " +
					"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (rs != null)
			{
				try
				{
					rs.close();
				}
				catch (SQLException ex)
				{
					System.out.println("Closing rs in getReportNotes.  " +
							"SQLException: " + ex.getMessage() + ", " +
							"SQLState: " + ex.getSQLState() + ", " +
							"VendorError: " + ex.getErrorCode());
				}
				rs = null;
			}
			if (stmt != null)
			{
				try
				{
					stmt.close();
				}
				catch (SQLException ex)
				{
					System.out.println("Closing stmt in getReportNotes.  " +
							"SQLException: " + ex.getMessage() + ", " +
							"SQLState: " + ex.getSQLState() + ", " +
							"VendorError: " + ex.getErrorCode());
				}
				stmt = null;
			}
		}
	}
	

	/*
	 * getDnaFromV2
	 * 
	 * Returns the DNA ID associated with a job id
	 */
	public long getDnaFromV2(Connection con, String projectId) 
	throws Exception 
	{
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT dna.dnaId ");
		sqlQuery.append("  FROM pdi_abd_dna dna ");
		sqlQuery.append("    WHERE dna.projectId = '" + projectId + "' ");

		try
		{
			stmt = con.createStatement();
        	rs = stmt.executeQuery(sqlQuery.toString());
        	
        	if(rs == null || !rs.isBeforeFirst())
        	{
        		return -1;
        	}

        	// Get the text (should be one and only one row)
        	rs.next();

			return rs.getLong("dnaId");
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("getReportNotes SQL.  " +
					"SQLException: " + ex.getMessage() + ", " +
					"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (rs != null)
			{
				try
				{
					rs.close();
				}
				catch (SQLException ex)
				{
					System.out.println("Closing rs in getReportNotes.  " +
							"SQLException: " + ex.getMessage() + ", " +
							"SQLState: " + ex.getSQLState() + ", " +
							"VendorError: " + ex.getErrorCode());
				}
				rs = null;
			}
			if (stmt != null)
			{
				try
				{
					stmt.close();
				}
				catch (SQLException ex)
				{
					System.out.println("Closing stmt in getReportNotes.  " +
							"SQLException: " + ex.getMessage() + ", " +
							"SQLState: " + ex.getSQLState() + ", " +
							"VendorError: " + ex.getErrorCode());
				}
				stmt = null;
			}
		}
	}


	/*
	 * getDnaNameFromV2
	 * 
	 * Returns the DNA name associated with a job id
	 */
	public String getDnaNameFromV2(Connection con, String jobId) 
	throws Exception 
	{
		Statement stmt = null;
		ResultSet rs = null;
		String ret = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT dna.dnaName ");
		sqlQuery.append("  FROM pdi_abd_dna dna ");
		sqlQuery.append("    WHERE dna.jobId = '" + jobId + "'");

		try
		{
			stmt = con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			if(rs == null || !rs.isBeforeFirst())
			{
				return ret;
			}

			// Get the text (should be one and only one row)
			rs.next();

			ret = rs.getString("dnaName");
			return ret;
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("getDnaNameFromV2 SQL; job=" + jobId + ".  " +
					"SQLException: " + ex.getMessage() + ", " +
					"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (rs != null)
			{
				try
				{
					rs.close();
				}
				catch (SQLException ex)
				{
					System.out.println("Closing rs in getDnaNameFromV2.  " +
							"SQLException: " + ex.getMessage() + ", " +
							"SQLState: " + ex.getSQLState() + ", " +
							"VendorError: " + ex.getErrorCode());
				}
				rs = null;
			}
			if (stmt != null)
			{
				try
				{
					stmt.close();
				}
				catch (SQLException ex)
				{
					System.out.println("Closing stmt in getDnaNameFromV2.  " +
							"SQLException: " + ex.getMessage() + ", " +
							"SQLState: " + ex.getSQLState() + ", " +
							"VendorError: " + ex.getErrorCode());
				}
				stmt = null;
			}
		}
	}
}
