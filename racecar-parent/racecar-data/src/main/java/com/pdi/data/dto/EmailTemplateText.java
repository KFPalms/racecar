/**
 * Copyright (c) 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.dto;

public class EmailTemplateText {
	private Language language;
	private String emailConstant;
	private String subject;
	private String body;
	
	public EmailTemplateText(Language l, String c, String s, String b)
	{
		language = l;
		emailConstant = c;
		subject = s;
		body = b;
	}
	
//	public void setLanguage(Language language) {
//		_language = language;
//	}
	public Language getLanguage() {
		return language;
	}

//	public void setEmailConstant(String v)
//	{
//		_emailConstant = v;
//	}
	public String getEmailConstant()
	{
		return emailConstant;
	}

//	public void setSubject(String v) {
//		_subject = v;
//	}
	public String getSubject() {
		return subject;
	}
	
//	public void setBody(String v) {
//		_body = v;
//	}
	public String getBody() {
		return body;
	}

}
