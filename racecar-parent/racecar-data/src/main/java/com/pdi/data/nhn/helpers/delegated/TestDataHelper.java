package com.pdi.data.nhn.helpers.delegated;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.CallableStatement;
//import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.pdi.data.dto.SessionUser;
import com.pdi.data.dto.TestData;
import com.pdi.data.helpers.interfaces.ITestDataHelper;
import com.pdi.data.nhn.util.NhnDatabaseUtils;
import com.pdi.logging.LogWriter;

public class TestDataHelper implements ITestDataHelper{
	
	//private String dbUser = "TestDataHlpr";	
	//private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	/**
	 * In v2 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.ITestDataHelper#saveCogsDoneFlag(com.pdi.data.dto.SessionUser, TestData testData)
	 */
	public void saveCogsDoneFlag(SessionUser session, TestData testData){
		
	}
	
	
	/**
	 * In v2
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.ITestDataHelper#getCogsDoneFlag(com.pdi.data.dto.SessionUser, TestData testData)
	 */
	
	public boolean getCogsDoneFlag(SessionUser session, TestData testData){
		return false;
	}
	
	
	/** 
	 * In v2
	 * CALLS stored procedure User_course_list_status_assessment 
	 * CREATE PROCEDURE [dbo].[user_course_list_status_assessment]
     *			@company_id int,
     *			@users_id int
     *
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.ITestDataHelper#getDnaIdFromProjectId(com.pdi.data.dto.SessionUser, com.pdi.data.dto.TestData)
	 */
	public long getDnaIdFromProjectId(SessionUser session, TestData testData){		
		return new Long(0).longValue();
	}

	/**
	 * The method getUserCourseListFromStoredProc calls the stored procedure 
	 * user_course_list_status_testdata.
	 * This method is used by the Test Data application in determining which
	 * instruments should be shown.  The stored proc is also used by the Launch Page
	 * for Participants.
	 * 
	 * @param String clientId
	 * @param String participantId
	 * @return ArrayList<String> - the list of course abbreviations 
	 * 								that can be shown
	 */
	public ArrayList<String> getUserCourseListFromStoredProc(String clientId, String participantId){
	
		//System.out.println("getUserCourseListFromStoredProc " + clientId + "  " + participantId);
		Connection con = null;
		CallableStatement proc_stmt = null;
		ResultSet rs  = null;
		ArrayList<String> courseAbbrvs = new ArrayList<String>();
		
		try {
			
			con = NhnDatabaseUtils.getDBConnection();
			proc_stmt = con.prepareCall("{ call platform.dbo.user_course_list_status_testdata(?,?) }");
            

			try
			{
	            proc_stmt.setInt(1, (new Long(clientId)).intValue());
	            proc_stmt.setInt(2, (new Long(participantId)).intValue());
	            rs = proc_stmt.executeQuery();	
				
				try {
					
					if (rs != null && rs.isBeforeFirst())
					{
							while(rs.next()){								
								//System.out.println("abbrv :  " + rs.getString("abbv"));
								courseAbbrvs.add(rs.getString("abbv"));
							}
						
					}else{
						//System.out.println("Error in getUserCourseListFromStoredProc() for Participant: " + participantId + ", client: " + clientId );
						throw new SQLException("Error in getUserCourseListFromStoredProc() for Participant: " + participantId + ", client: " + clientId );
						
					}
				} finally {
			        if (rs != null)
			        {
			            try
						{
			                rs.close();
			            }
			            catch (SQLException sqlEx)
						{
			            	sqlEx.printStackTrace();
			            }
			            rs = null;
			        }
			        if (con != null)
			        {
			            try
						{
			                con.close();
			            }
			            catch (SQLException sqlEx)
						{
			            	sqlEx.printStackTrace();
			            }
			            con = null;
			        }
			        
				}
				
			}
			catch(java.sql.SQLException se)
			{
				LogWriter.logSQL(LogWriter.ERROR, this,
						" error in getUserCourseListFromStoredProc() for Participant: " + participantId + ", client: " + clientId , 
						"stored_proc: user_course_list_status_testdata(?,?)  ");
				se.printStackTrace();
			}

				
			
		} catch(Exception e) {
			LogWriter.logBasic(LogWriter.ERROR, this,
					" error in getUserCourseListFromStoredProc() for Participant: " + participantId + ", client: " + clientId );
			//se.printStackTrace();
		}
		finally
		{
	        if (con != null)
	        {
	            try
				{
	                con.close();
	            }
	            catch (SQLException sqlEx)
				{
	            	sqlEx.printStackTrace();
	            }
	            con = null;
	        }
		}
		
		return courseAbbrvs;
	}
	
}

