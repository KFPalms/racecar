/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.pdi.data.abyd.dto.common.CompElementDTO;
import com.pdi.data.abyd.dto.common.CompGroupData;
import com.pdi.data.abyd.dto.common.DNACellDTO;
import com.pdi.data.abyd.dto.common.EGBarDisplayData;
import com.pdi.data.abyd.dto.common.EGCompDisplayData;
import com.pdi.data.abyd.dto.common.EGFullDataDTO;
import com.pdi.data.abyd.dto.common.IGInstCompData;
import com.pdi.data.abyd.dto.intGrid.IGFinalScoreDTO;
import com.pdi.data.abyd.dto.intGrid.IGInstDisplayData;
import com.pdi.data.abyd.dto.intGrid.IGIntersectionAndOtherDataDTO;
import com.pdi.data.abyd.dto.intGrid.IGSummaryDataDTO;
import com.pdi.data.abyd.dto.reportInput.ReportInputDTO;
import com.pdi.data.abyd.dto.setup.ReportModelCompChildDTO;
import com.pdi.data.abyd.dto.setup.ReportModelCompetencyDTO;
import com.pdi.data.abyd.dto.setup.ReportModelStructureDTO;
import com.pdi.data.abyd.dto.setup.ReportModelSuperFactorDTO;
import com.pdi.data.abyd.helpers.common.EGDataHelper;
import com.pdi.data.abyd.helpers.intGrid.IGAlrDataHelper;
import com.pdi.data.abyd.helpers.intGrid.IGKf4dDataHelper;
import com.pdi.data.abyd.helpers.intGrid.IGScoring;
import com.pdi.data.abyd.helpers.intGrid.IGSummaryDataHelper;
import com.pdi.data.abyd.helpers.intGrid.IGTestColumnDataHelper;
import com.pdi.data.abyd.helpers.setup.ReportModelDataHelper;
//import com.pdi.data.dto.Participant;
//import com.pdi.data.dto.SessionUser;
//import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.nhn.util.NhnDatabaseUtils;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.util.RequestStatus;
//import com.pdi.data.v2.dto.SessionUserDTO;
import com.pdi.properties.PropertyLoader;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.GroupReport;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.xml.XMLUtils;

/*
 * AbdExtractHelper provides a repository for methods used by the ABYD_EXTRACT.groovy
 * scripts in the listener-portal (data collection) environment
 */
public class AlrExtractHelper {

	private static final Logger log = LoggerFactory.getLogger(AlrExtractHelper.class);
	//
	// Static data
	//
	private static final String SCORE_PARM_STR = "/KfapRawScoresServlet?pptId={pptId}&projId={projId}&targIdx={tgtLvl}&nVersion={normVer}";

	//
	// Statics used in extract data fetch
	//

	// Leadership rating (also Long Term Potential)
	private static String[] LEADERSHIP = new String[5];
	static {
		LEADERSHIP[0] = "Insufficient Data";
		LEADERSHIP[1] = "Weak";
		LEADERSHIP[2] = "Mixed";
		LEADERSHIP[3] = "Strong";
		LEADERSHIP[4] = "Very Strong";
	}

	// Fit Recommendation
	private static String[] FIT = new String[5];
	static {
		FIT[0] = "Insufficient Data";
		FIT[1] = "Not Recommended";
		FIT[2] = "Recommended w/Reservations";
		FIT[3] = "Recommended";
		FIT[4] = "Strongly Recommended";
	}

	// Readiness Rating
	private static String[] READINESS = new String[5];
	static {
		READINESS[0] = "Insufficient Data";
		READINESS[1] = "Develop in Place";
		READINESS[2] = "Broaden";
		READINESS[3] = "Prepare";
		READINESS[4] = "Ready";
	}

	// Derailment Rating
	private static String[] DERAIL = new String[5];
	static {
		DERAIL[0] = "Insufficient Data";
		DERAIL[1] = "High";
		DERAIL[2] = "Moderate";
		DERAIL[3] = "Low";
		DERAIL[4] = "Minimal";
	}

	// Mapping to derailment strings
	private static int DR_NO_DAT = 0;
	private static int DR_HIGH = 1;
	private static int DR_MODERATE = 2;
	private static int DR_LOW = 3;
	private static int DR_MINIMAL = 4;

	private static Map<String, Integer> KFLA_COMPS_MAP = new HashMap<String, Integer>();
	static {
		// All generic KFLA competencies. Order is fixed.
		// If order changes for generic model then it becomes a custom model
		KFLA_COMPS_MAP.put("Business Insight - KFLA", 1);
		KFLA_COMPS_MAP.put("Customer Focus - KFLA", 2);
		KFLA_COMPS_MAP.put("Financial Acumen - KFLA", 3);
		KFLA_COMPS_MAP.put("Tech Savvy - KFLA", 4);
		KFLA_COMPS_MAP.put("Manages Complexity - KFLA", 5);
		KFLA_COMPS_MAP.put("Decision Quality - KFLA", 6);
		KFLA_COMPS_MAP.put("Balances Stakeholders - KFLA", 7);
		KFLA_COMPS_MAP.put("Global Perspective - KFLA", 8);
		KFLA_COMPS_MAP.put("Cultivates Innovation - KFLA", 9);
		KFLA_COMPS_MAP.put("Strategic Mindset - KFLA", 10);
		KFLA_COMPS_MAP.put("Action Oriented - KFLA", 11);
		KFLA_COMPS_MAP.put("Resourcefulness - KFLA", 12);
		KFLA_COMPS_MAP.put("Directs Work - KFLA", 13);
		KFLA_COMPS_MAP.put("Plans and Aligns - KFLA", 14);
		KFLA_COMPS_MAP.put("Optimizes Work Processes - KFLA", 15);
		KFLA_COMPS_MAP.put("Ensures Accountability - KFLA", 16);
		KFLA_COMPS_MAP.put("Drives Results - KFLA", 17);
		KFLA_COMPS_MAP.put("Collaborates - KFLA", 18);
		KFLA_COMPS_MAP.put("Manages Conflict - KFLA", 19);
		KFLA_COMPS_MAP.put("Interpersonal Savvy - KFLA", 20);
		KFLA_COMPS_MAP.put("Builds Networks - KFLA", 21);
		KFLA_COMPS_MAP.put("Attracts Top Talent - KFLA", 22);
		KFLA_COMPS_MAP.put("Develops Talent - KFLA", 23);
		KFLA_COMPS_MAP.put("Values Differences - KFLA", 24);
		KFLA_COMPS_MAP.put("Builds Effective Teams - KFLA", 25);
		KFLA_COMPS_MAP.put("Communicates Effectively - KFLA", 26);
		KFLA_COMPS_MAP.put("Drives Engagement - KFLA", 27);
		KFLA_COMPS_MAP.put("Organization Savvy - KFLA", 28);
		KFLA_COMPS_MAP.put("Persuades - KFLA", 29);
		KFLA_COMPS_MAP.put("Drives Vision and Purpose - KFLA", 30);
		KFLA_COMPS_MAP.put("Courage - KFLA", 31);
		KFLA_COMPS_MAP.put("Instills Trust - KFLA", 32);
		KFLA_COMPS_MAP.put("Demonstrates Self-Awareness - KFLA", 33);
		KFLA_COMPS_MAP.put("Self-Development - KFLA", 34);
		KFLA_COMPS_MAP.put("Manages Ambiguity - KFLA", 35);
		KFLA_COMPS_MAP.put("Nimble Learning - KFLA", 36);
		KFLA_COMPS_MAP.put("Being Resilient - KFLA", 37);
		KFLA_COMPS_MAP.put("Situational Adaptability - KFLA", 38);

		// ALR scored competencies
		KFLA_COMPS_MAP.put("Business Insight - ALR", 1);
		KFLA_COMPS_MAP.put("Financial Acumen - ALR", 3);
		KFLA_COMPS_MAP.put("Manages Complexity - ALR", 5);
		KFLA_COMPS_MAP.put("Cultivates Innovation - ALR", 9);
		KFLA_COMPS_MAP.put("Strategic Mindset - ALR", 10);
		KFLA_COMPS_MAP.put("Plans and Aligns - ALR", 14);
		KFLA_COMPS_MAP.put("Ensures Accountability - ALR", 16);
		KFLA_COMPS_MAP.put("Drives Results - ALR", 17);
		KFLA_COMPS_MAP.put("Collaborates - ALR", 18);
		KFLA_COMPS_MAP.put("Develops Talent - ALR", 23);
		KFLA_COMPS_MAP.put("Builds Effective Teams - ALR", 25);
		KFLA_COMPS_MAP.put("Drives Engagement - ALR", 27);
		KFLA_COMPS_MAP.put("Persuades - ALR", 29);
		KFLA_COMPS_MAP.put("Courage - ALR", 31);
		KFLA_COMPS_MAP.put("Instills Trust - ALR", 32);
		KFLA_COMPS_MAP.put("Manages Ambiguity - ALR", 35);

		// CEO scored competencies
		KFLA_COMPS_MAP.put("Customer Focus - CEO", 2);
		KFLA_COMPS_MAP.put("Financial Acumen - CEO", 3);
		KFLA_COMPS_MAP.put("Manages Complexity - CEO", 5);
		KFLA_COMPS_MAP.put("Balances Stakeholders - CEO", 7);
		KFLA_COMPS_MAP.put("Global Perspective - CEO", 8);
		KFLA_COMPS_MAP.put("Cultivates Innovation - CEO", 9);
		KFLA_COMPS_MAP.put("Strategic Vision - CEO", 10); // Mapped to
															// "Strategic
															// Mindset"
		KFLA_COMPS_MAP.put("Aligns Execution - CEO", 14); // Mapped to "Plans
															// and Aligns"
		KFLA_COMPS_MAP.put("Ensures Accountability - CEO", 16);
		KFLA_COMPS_MAP.put("Drives Results - CEO", 17);
		KFLA_COMPS_MAP.put("Manages Conflict - CEO", 19);
		KFLA_COMPS_MAP.put("Navigates Networks - CEO", 21); // Mapped to "Builds
															// Networks"
		KFLA_COMPS_MAP.put("Develops Talent - CEO", 23);
		KFLA_COMPS_MAP.put("Builds Effective Teams - CEO", 25);
		KFLA_COMPS_MAP.put("Communicates Effectively - CEO", 26);
		KFLA_COMPS_MAP.put("Engages and Inspires - CEO", 27); // Mapped to
																// "Drives
																// Engagement"
		KFLA_COMPS_MAP.put("Persuades - CEO", 29);
		KFLA_COMPS_MAP.put("Courage - CEO", 31);
		KFLA_COMPS_MAP.put("Instills Trust - CEO", 32);
		KFLA_COMPS_MAP.put("Demonstrates Self-Awareness - CEO", 33);
		KFLA_COMPS_MAP.put("Manages Ambiguity - CEO", 35);
		KFLA_COMPS_MAP.put("Nimble Learning - CEO", 36);
		KFLA_COMPS_MAP.put("Being Resilient - CEO", 37);
		KFLA_COMPS_MAP.put("Situational Adaptability - CEO", 38);

		// Custom Models start here
		// Note that the position numbers are arbitrary (1 - n)

		// Ericsson EEA/EAC competencies
		KFLA_COMPS_MAP.put("Competence Drive - EEA/EAC", 1);
		KFLA_COMPS_MAP.put("Master Strategy - EEA/EAC", 2);
		KFLA_COMPS_MAP.put("Driving Innovation - EEA/EAC", 3);
		KFLA_COMPS_MAP.put("Business Acumen - EEA/EAC", 4);
		KFLA_COMPS_MAP.put("Enabling People - EEA/EAC", 5);
		KFLA_COMPS_MAP.put("Courageous Leadership - EEA/EAC", 6);
		KFLA_COMPS_MAP.put("Uncompromising Integrity - EEA/EAC", 7);
		KFLA_COMPS_MAP.put("Customer Excellence - EEA/EAC", 8);
		KFLA_COMPS_MAP.put("Excelling Execution - EEA/EAC", 9);
		KFLA_COMPS_MAP.put("Embracing Change - EEA/EAC", 10);

		// J&J BUL Competencies
		KFLA_COMPS_MAP.put("Cultivate external relationships - JnJ BUL", 1);
		KFLA_COMPS_MAP.put("Be insight-driven - JnJ BUL", 2);
		KFLA_COMPS_MAP.put("Forge internal collaboration - JnJ BUL", 3);
		KFLA_COMPS_MAP.put("Translate insights - JnJ BUL", 4);
		KFLA_COMPS_MAP.put("Challenge the status-quo - JnJ BUL", 5);
		KFLA_COMPS_MAP.put("Take and manage risks - JnJ BUL", 6);
		KFLA_COMPS_MAP.put("Take ownership for talent - JnJ BUL", 7);
		KFLA_COMPS_MAP.put("Diversity and inclusion - JnJ BUL", 8);
		KFLA_COMPS_MAP.put("Transparent constructive conversations - JnJ BUL", 9);
		KFLA_COMPS_MAP.put("Empower people - JnJ BUL", 10);
		KFLA_COMPS_MAP.put("Global and enterprise-wide mindset - JnJ BUL", 11);
		KFLA_COMPS_MAP.put("Balance strategic choices - JnJ BUL", 12);
		KFLA_COMPS_MAP.put("Credo - JnJ BUL", 13);

		// J&J SEA Competencies
		KFLA_COMPS_MAP.put("Cultivate external relationships - JnJ SEA", 1);
		KFLA_COMPS_MAP.put("Be insight-driven - JnJ SEA", 2);
		KFLA_COMPS_MAP.put("Forge internal collaboration - JnJ SEA", 3);
		KFLA_COMPS_MAP.put("Translate insights - JnJ SEA", 4);
		KFLA_COMPS_MAP.put("Challenge the status-quo - JnJ SEA", 5);
		KFLA_COMPS_MAP.put("Take and manage risks - JnJ SEA", 6);
		KFLA_COMPS_MAP.put("Take ownership for talent - JnJ SEA", 7);
		KFLA_COMPS_MAP.put("Diversity and inclusion - JnJ SEA", 8);
		KFLA_COMPS_MAP.put("Transparent constructive conversations - JnJ SEA", 9);
		KFLA_COMPS_MAP.put("Empower people - JnJ SEA", 10);
		KFLA_COMPS_MAP.put("Global and enterprise-wide mindset - JnJ SEA", 11);
		KFLA_COMPS_MAP.put("Balance strategic choices - JnJ SEA", 12);
		KFLA_COMPS_MAP.put("Credo - JnJ SEA", 13);

		// Capgemini Competencies
		KFLA_COMPS_MAP.put("La Niaque - CG", 1);
		KFLA_COMPS_MAP.put("Business Builder - CG", 2);
		KFLA_COMPS_MAP.put("Profit Shaper - CG", 3);
		KFLA_COMPS_MAP.put("People Developer - CG", 4);
		KFLA_COMPS_MAP.put("Active Connector - CG", 5);
		KFLA_COMPS_MAP.put("Agile Player - CG", 6);

	}

	private static Set<String> CUSTOM_SUFFIX_SET = new HashSet<String>();
	static {
		CUSTOM_SUFFIX_SET.add(" - EEA/EAC");
		CUSTOM_SUFFIX_SET.add(" - JnJ BUL");
		CUSTOM_SUFFIX_SET.add(" - JnJ SEA");
		CUSTOM_SUFFIX_SET.add(" - CG");
	}

	// List of the "standard" KFLA Competency Names
	// TODO Refactor to put in DB? If so will need a helper to build the base
	// list.
	private static Map<String, String> STD_KFLA_COMP_NAMES = new HashMap<String, String>();
	static {
		STD_KFLA_COMP_NAMES.put("KFLA_COMP1_Name", "Business Insight");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP2_Name", "Customer Focus");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP3_Name", "Financial Acumen");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP4_Name", "Tech Savvy");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP5_Name", "Manages Complexity");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP6_Name", "Decision Quality");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP7_Name", "Balance Stakeholders");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP8_Name", "Global Perspective");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP9_Name", "Cultivates Innovation");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP10_Name", "Strategic Mindset");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP11_Name", "Action Oriented");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP12_Name", "Resourcefulness");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP13_Name", "Directs Work");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP14_Name", "Plans and Aligns");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP15_Name", "Optimizes Work Processes");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP16_Name", "Ensures Accountability");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP17_Name", "Drives Results");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP18_Name", "Collaborates");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP19_Name", "Manages Conflict");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP20_Name", "Interpersonal Savvy");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP21_Name", "Builds Networks");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP22_Name", "Attracts Talent");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP23_Name", "Develops Talent");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP24_Name", "Values Differences");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP25_Name", "Builds Effective Teams");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP26_Name", "Communicates Effectively");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP27_Name", "Drives Engagement");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP28_Name", "Organizational Savvy");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP29_Name", "Persuades");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP30_Name", "Drives Vision and Purpose");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP31_Name", "Courage");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP32_Name", "Instills Trust");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP33_Name", "Demonstrates Self-Awareness");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP34_Name", "Self-Development");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP35_Name", "Manages Ambiguity");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP36_Name", "Nimble Learning");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP37_Name", "Being Resilient");
		STD_KFLA_COMP_NAMES.put("KFLA_COMP38_Name", "Situational Adaptability");
	}

	//
	// Static methods.
	//

	//
	// These methods are called from the groovy script in portal.
	// They extract the data that will be needed for the extract.
	//

	// /**
	// * Put the candidate info into the IR
	// *
	// *
	// * @throws Exception
	// */
	// public static IndividualReport addPartInfo(SessionUserDTO sessionUser,
	// IndividualReport ir, String partId)
	// throws Exception
	// {
	// ir.getDisplayData().put("ParticipantId", partId);
	//
	// SessionUser su =
	// HelperDelegate.getSessionUserHelperHelper().createSessionUser();
	// Participant participant =
	// HelperDelegate.getParticipantHelper().fromId(su, partId);
	//
	// ir.getDisplayData().put("ParticipantFname", participant.getFirstName() ==
	// null ? "Not Available" : participant.getFirstName());
	// ir.getDisplayData().put("ParticipantLname", participant.getLastName() ==
	// null ? "Not Available" : participant.getLastName());
	//
	// return ir;
	// }

	//
	// Instance data.
	//
	private Connection conn = null;

	//
	// Constructors.
	//

	// Make the default constructor invisible
	private AlrExtractHelper() {
		// Does nothing
	}

	public AlrExtractHelper(Connection con) {
		// con cannot be null. Current calling context implies that it will not
		// be.
		this.conn = con;
	}

	//
	// Instance methods.
	//

	/**
	 * Fetch DNA info and client name based upon the V2 job (project) id.
	 *
	 * @throws Exception
	 */
	public void getGroupData(String jobId, GroupReport gr) throws Exception {
		Statement stmt = null;
		ResultSet rs = null;
		// GroupReport gr = new GroupReport();

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT dna.dnaId, ");
		sqlQuery.append("       dna.dnaName, ");
		sqlQuery.append("       dna.modelId, ");
		sqlQuery.append("       modl.internalName as modelName, ");
		sqlQuery.append("       dna.synthMean, ");
		sqlQuery.append("       dna.synthStdDev, ");
		sqlQuery.append("       dna.dnaNotes, ");
		sqlQuery.append("       dna.reportNote, ");
		sqlQuery.append("       dna.wbVersion ");
		// sqlQuery.append(" cc.Name as clientName ");
		sqlQuery.append("  FROM pdi_abd_dna dna ");
		sqlQuery.append("    LEFT JOIN pdi_abd_model modl ON modl.modelId = dna.modelId ");
		// sqlQuery.append(" LEFT JOIN jobs jj on jj.UniqueIdStamp = dna.jobId
		// ");
		// sqlQuery.append(" LEFT JOIN clients cc on cc.UniqueIdStamp =
		// jj.ClientID ");
		sqlQuery.append("  WHERE dna.projectId = '" + jobId + "'");
		// System.out.println("GRP Data SQL=" + sqlQuery.toString());

		try {
			stmt = this.conn.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			// If there is no row, return a null
			if (!rs.isBeforeFirst()) {
				// return null;
			}

			// Get the first result row and put the data into the gr
			rs.next();

			gr.getDisplayData().put("DNA_ID", rs.getString("dnaId"));
			gr.getDisplayData().put("ProjectName", rs.getString("dnaName"));
			gr.getDisplayData().put("Model", rs.getString("modelName") == null ? "" : rs.getString("modelName"));
			gr.getDisplayData().put("dlciMean", rs.getString("synthMean"));
			gr.getDisplayData().put("dlciStdDev", rs.getString("synthStdDev"));
			gr.getDisplayData().put("DnaNotes",
					rs.getString("dnaNotes") == null ? "" : XMLUtils.suppressHtmlTags(rs.getString("dnaNotes")));
			gr.getDisplayData().put("Rpt_Note",
					rs.getString("reportNote") == null ? "" : XMLUtils.suppressHtmlTags(rs.getString("reportNote")));
			gr.getDisplayData().put("wbVersion", rs.getString("wbVersion"));
			// gr.getDisplayData().put("ClientName", rs.getString("clientName")
			// == null ? "" : rs.getString("clientName"));

			return;
		} catch (SQLException ex) {
			// handle any errors
			String str = "getGroupData():  SQL fetching DNA for project " + jobId + ".  " + "SQLException: "
					+ ex.getMessage() + ", " + "SQLState: " + ex.getSQLState() + ", " + "VendorError: "
					+ ex.getErrorCode();
			System.out.println(str);
			throw new Exception(str);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					// swallow the error
				}
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					// swallow the error
				}
				stmt = null;
			}
		}
	}

	/**
	 * Put the Client Competency (Report Model) structure into the GroupReport
	 * object
	 *
	 * This code esentially duplicates the score code, except it saves structure
	 * (but not scores) and into the GroupReport object.
	 *
	 * Perhaps we could find some way to abstract the functionality somehow...
	 *
	 * @throws Exception
	 */
	public void addClientCompetencyStructureALR(GroupReport gr, long dnaId) throws Exception {
		// Get the data (including scores)
		ReportModelStructureDTO ccStruct = new ReportModelDataHelper().getReportModel(this.conn, dnaId);

		// Using indices so that holes in the sequence don't hurt
		int i = 0;
		int j = 0;
		int k = 0;
		String strComp = "";
		String strCompName = "";
		gr.getDisplayData().put("CC_SF_Count", "" + ccStruct.getSuperFactorList().size());
		for (ReportModelSuperFactorDTO sf : ccStruct.getSuperFactorList()) {
			j = 0; // Init the competency counter
			i++;
			gr.getDisplayData().put("CC_SF" + i + "_Name", sf.getDisplayName());
			gr.getDisplayData().put("CC_SF" + i + "_Comp_Count", "" + sf.getCompList().size());
			for (ReportModelCompetencyDTO cmp : sf.getCompList()) {
				k = 0; // Initialize the child comp counter
				j++;
				gr.getDisplayData().put("CC_SF" + i + "_Comp" + j + "_Name", cmp.getDisplayName());
				// gr.getDisplayData().put("CC_SF" + i + "_Comp" + j +
				// "_Essential", cmp.getEssential() ? "True" : "False");

				gr.getDisplayData().put("CC_SF" + i + "_Comp" + j + "_Child_Count", "" + cmp.getChildren().size());

				// combine multiple Child DNA NAmes if they exist into one
				// string
				strCompName = "";
				for (ReportModelCompChildDTO cc : cmp.getChildren()) {
					k++;

					strComp = cc.getDnaCompName();

					if (k > 1) {
						strCompName = strCompName + "; " + strComp;

					} else {
						strCompName = strComp;
					}
					// gr.getDisplayData().put("CC_SF" + i + "_Comp" + j +
					// "_Child" + k + "_Dna_Name", cc.getDnaCompName());
					// gr.getDisplayData().put("CC_SF" + i + "_Comp" + j +
					// "_Child" + k + "_Dna_Essential", cc.isDnaCompEssential()
					// ? "True" : "False");
				}
				gr.getDisplayData().put("CC_SF" + i + "_Comp" + j + "_Child1_Dna_Name", strCompName);
			}
		}
	}

	/**
	 * Fetch Internal Competency name for later use
	 *
	 * @throws Exception
	 */

	public Map<Integer, String> getCompInternalNames(Long dnaId) throws Exception {
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT co.competencyId, ");
		sqlQuery.append(" co.internalName ");
		sqlQuery.append(" FROM pdi_abd_rm_comp_children cc ");
		sqlQuery.append(" INNER JOIN pdi_abd_competency co ON co.competencyId = cc.competencyId ");
		sqlQuery.append(" WHERE cc.dnaId = " + dnaId);
		// System.out.println("CompInternalNames SQL=" + sqlQuery.toString());

		try {
			stmt = this.conn.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());
			HashMap<Integer, String> cmpInternalNames = new HashMap<Integer, String>();

			// Fill Hash map with values
			while (rs.next()) {
				cmpInternalNames.put(rs.getInt("competencyId"), rs.getString("internalName"));
			}
			return cmpInternalNames;
		} catch (SQLException ex) {
			// handle any errors
			String str = "getCompInternalNames():  " + "SQLException: " + ex.getMessage() + ", " + "SQLState: "
					+ ex.getSQLState() + ", " + "VendorError: " + ex.getErrorCode();
			System.out.println(str);
			throw new Exception(str);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					// swallow the error
				}
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					// swallow the error
				}
				stmt = null;
			}
		}
	}

	/**
	 * Put the DRI stuff into the IR object
	 *
	 * //throws Exception
	 */
	public IndividualReport addDRIandTLT(IndividualReport ir, long dnaId, String partId, ReportInputDTO ri)
	// throws Exception
	{

		// ReportInputDTO ri = new
		// ReportInputDataHelper().getReportInput(partId, dnaId);
		// Get the dLCi/LeadershipSkills Index. Save it twice to satisfy either
		// naming convention
		ir.getDisplayData().put("IG_dLCI", "" + ri.getLeadershipSkillIndex());
		ir.getDisplayData().put("DRI_LSkillIndex", "" + ri.getLeadershipSkillIndex());

		// Consultant selected ratings
		ir.getDisplayData().put("DRILeadSkillRating", LEADERSHIP[ri.getLeadershipSkillRating()]);
		ir.getDisplayData().put("DRILeadExperRating", LEADERSHIP[ri.getLeadershipExperienceRating()]);
		ir.getDisplayData().put("DRILeadStyleRating", LEADERSHIP[ri.getLeadershipStyleRating()]);
		ir.getDisplayData().put("DRILeadIntrstRating", LEADERSHIP[ri.getLeadershipInterestRating()]);
		ir.getDisplayData().put("DRIDerailmentRating", DERAIL[ri.getDerailmentRating()]);
		ir.getDisplayData().put("DRILongTermRating", LEADERSHIP[ri.getLongtermRating()]);
		ir.getDisplayData().put("DRILeadSkillRatingNumber", "" + ri.getLeadershipSkillRating());
		ir.getDisplayData().put("DRILeadExperRatingNumber", "" + ri.getLeadershipExperienceRating());
		ir.getDisplayData().put("DRILeadStyleRatingNumber", "" + ri.getLeadershipStyleRating());
		ir.getDisplayData().put("DRILeadIntrstRatingNumber", "" + ri.getLeadershipInterestRating());
		ir.getDisplayData().put("DRIDerailmentRatingNumber", "" + ri.getDerailmentRating());
		ir.getDisplayData().put("DRILongTermRatingNumber", "" + ri.getLongtermRating());

		// Fit index and recomendation
		ir.getDisplayData().put("DRIFitIndex", "" + ri.getCandidateFitIndex()); // %tile
		ir.getDisplayData().put("DRIFitRating", FIT[ri.getFitRecommendedRating()]);

		// Readiness Recomendation and focus statement
		ir.getDisplayData().put("DRIReadinessRating", READINESS[ri.getReadinessRating()]);
		ir.getDisplayData().put("DRIReadinessRatingNumber", "" + ri.getReadinessRating());
		ir.getDisplayData().put("DRIDevelopmentFocus", ri.getReadinessFocus());

		// Counsultant entered text
		ir.getDisplayData().put("DRISkillsToLeverageOrgText",
				XMLUtils.suppressHtmlTags(ri.getSkillsToLeverageOrgText()));
		ir.getDisplayData().put("DRISkillsToLeveragePartText",
				XMLUtils.suppressHtmlTags(ri.getSkillsToLeveragePartText()));
		ir.getDisplayData().put("DRISkillsToDevelopOrgText", XMLUtils.suppressHtmlTags(ri.getSkillsToDevelopOrgText()));
		ir.getDisplayData().put("DRISkillsToDevelopPartText",
				XMLUtils.suppressHtmlTags(ri.getSkillsToDevelopPartText()));
		ir.getDisplayData().put("DRILeadershipSkillOrgText", XMLUtils.suppressHtmlTags(ri.getLeadershipSkillOrgText()));
		ir.getDisplayData().put("DRILeadershipSkillPartText",
				XMLUtils.suppressHtmlTags(ri.getLeadershipSkillPartText()));
		ir.getDisplayData().put("DRILeadershipExpOrgText",
				XMLUtils.suppressHtmlTags(ri.getLeadershipExperienceOrgText()));
		ir.getDisplayData().put("DRILeadershipExpPartText",
				XMLUtils.suppressHtmlTags(ri.getLeadershipExperiencePartText()));
		ir.getDisplayData().put("DRILeadershipStyleOrgText", XMLUtils.suppressHtmlTags(ri.getLeadershipStyleOrgText()));
		ir.getDisplayData().put("DRILeadershipStylePartText",
				XMLUtils.suppressHtmlTags(ri.getLeadershipStylePartText()));
		ir.getDisplayData().put("DRILeadershipInterestOrgText",
				XMLUtils.suppressHtmlTags(ri.getLeadershipInterestOrgText()));
		ir.getDisplayData().put("DRILeadershipInterestPartText",
				XMLUtils.suppressHtmlTags(ri.getLeadershipInterestPartText()));
		ir.getDisplayData().put("DRIDerailmentOrgText", XMLUtils.suppressHtmlTags(ri.getDerailmentOrgText()));
		ir.getDisplayData().put("DRIDerailmentPartText", XMLUtils.suppressHtmlTags(ri.getDerailmentPartText()));
		ir.getDisplayData().put("DRILongTermPotentialOrgText", XMLUtils.suppressHtmlTags(ri.getLongtermOrgText()));
		ir.getDisplayData().put("DRIFitOrgText", XMLUtils.suppressHtmlTags(ri.getFitOrgText()));
		ir.getDisplayData().put("DRIReadinessOrgText", XMLUtils.suppressHtmlTags(ri.getReadinessOrgText()));
		ir.getDisplayData().put("DRIDevelopmentPartText", XMLUtils.suppressHtmlTags(ri.getDevelopmentPartText()));
		ir.getDisplayData().put("DRIPivotalPartText", XMLUtils.suppressHtmlTags(ri.getPivotalPartText()));

		// Put out the TLT stuff
		IndividualReport tlt = ri.getCurrentTLT();

		// System.out.println(tlt.toString());
		ir.getDisplayData().put("TLTLeadershipInterest",
				tlt.getScriptedGroupData("LE_INTER_FINALSCORE") == 1 ? "On" : "Off");
		// per NHN-1646, do not show leadership experience if no lei or no cs
		if (ir.getDisplayData().get("CS_MISSING") != null || ir.getDisplayData().get("LEI_MISSING") != null) {
			// skip these
			// System.out.println("do not show leadership experience if no lei
			// or no cs.....");
		} else {
			// do these
			ir.getDisplayData().put("TLTLeadershipExperience",
					tlt.getScriptedGroupData("LE_EXPER_FINALSCORE") == 1 ? "On" : "Off");
		}
		ir.getDisplayData().put("TLTLeadershipFoundations",
				tlt.getScriptedGroupData("LE_FOUND_FINALSCORE") == 1 ? "On" : "Off");

		// For derailment Risk, make the following assumption (based on the
		// color of the box on the
		// TLT Group Detail Report):
		// Stanine Box Color String
		// 1 - 3 Blue "Minimal"
		// 4 - 7 Green "Low"
		// 8 Yellow "Moderate"
		// 9 Red "High"
		String str = tlt.getScriptedData("DERAIL_FACTORS");
		int num = -1;
		boolean valid = true;
		if (str == null) {
			num = DR_NO_DAT;
		} else if (str.equals("1") || str.equals("2") || str.equals("3")) {
			num = DR_MINIMAL;
		} else if (str.equals("4") || str.equals("5") || str.equals("6") || str.equals("7")) {
			num = DR_LOW;
		} else if (str.equals("8")) {
			num = DR_MODERATE;
		} else if (str.equals("9")) {
			num = DR_HIGH;
		} else {
			num = DR_NO_DAT;
			valid = false;
		}
		if (valid) {
			str = DERAIL[num];
		} else {
			str = "Invalid Value (" + str + ")";
		}
		ir.getDisplayData().put("TLTDerailmentRisk", str);
		ir.getDisplayData().put("TLTDerailmentRiskNumber", "" + num);

		// Interest
		// per NHN 1646 - if cs is not present, do not show interest scores
		if (ir.getDisplayData().get("CS_MISSING") == null) {
			// do these
			ir.getDisplayData().put("TLTLeadershipAspiration", tlt.getScriptedData("CAREER_GOALS"));
			ir.getDisplayData().put("TLTCareerDrivers", tlt.getScriptedData("CAREER_DRIVERS"));
			ir.getDisplayData().put("TLTLearningOrientation", tlt.getScriptedData("LEARNING_ORIENT"));
			ir.getDisplayData().put("TLTExperienceOrientation", tlt.getScriptedData("EXPERIENCE_ORIENT"));
		} else {
			// skip these
			// System.out.println("if cs is not present, do not show interest
			// scores.....");
		}

		// Experience
		// per NHN 1646 - if cs or lei are not present do not show Experience
		// scores
		if (ir.getDisplayData().get("CS_MISSING") != null || ir.getDisplayData().get("LEI_MISSING") != null) {
			// skip these
			// System.out.println("if cs or lei are not present do not show
			// Experience scores.....");
		} else {
			// do these
			ir.getDisplayData().put("TLTBusinessOperations", tlt.getScriptedData("LEI_OPERATIONS"));
			ir.getDisplayData().put("TLTHandlingToughChallenges", tlt.getScriptedData("LEI_SIT_TOUGH_CHAL"));
			ir.getDisplayData().put("TLTHighVisibility", tlt.getScriptedData("LEI_HIGH_VIS"));
			ir.getDisplayData().put("TLTGrowingTheBusiness", tlt.getScriptedData("LEI_BD_GROW_BUS"));
			ir.getDisplayData().put("TLTPersonalDevelopment", tlt.getScriptedData("LEI_PERSONAL"));
		}

		// Foundations
		if (tlt.getDisplayData().get("COGNITIVES_INCLUDED").equals("1")) {
			str = tlt.getScriptedData("ZGEST");
		} else {
			str = "Not available";
		}
		// per NHN 1646 - if cs is not present do not show the following
		// foundations scores
		if (ir.getDisplayData().get("CS_MISSING") == null) {
			// do these
			ir.getDisplayData().put("TLTProblemSolving", str);
			ir.getDisplayData().put("TLTIntellectualEngagement", tlt.getScriptedData("INTELLECTUAL_ENGMT"));
			ir.getDisplayData().put("TLTAttentionToDetail", tlt.getScriptedData("DETAIL_ORIENT"));
			ir.getDisplayData().put("TLTImpactInfluence", tlt.getScriptedData("FACIL_LEADERSHP"));
			ir.getDisplayData().put("TLTInterpersonalEngagement", tlt.getScriptedData("INTER_PERSONAL_ENGMT"));
		} else {
			// skip these
			// System.out.println(" if cs is not present do not show the
			// following foundations scores.....");
		}
		ir.getDisplayData().put("TLTAchievementDrive", tlt.getScriptedData("DRIVE"));
		ir.getDisplayData().put("TLTAdvancementDrive", tlt.getScriptedData("INDIV_ORIENT"));
		ir.getDisplayData().put("TLTCollectiveOrientation", tlt.getScriptedData("COLLECTIVE_ORIENT"));
		ir.getDisplayData().put("TLTFlexibilityAdaptability", tlt.getScriptedData("POSITIVITY"));

		return ir;
	}

	/**
	 * Put the Client Competency (Report Model) scores into the IR object
	 *
	 * This code esentially duplicates the structure code, except it saves
	 * scores only and into the IndividualReport object.
	 *
	 * Perhaps we could find some way to abstract the functionality somehow...
	 */
	public IndividualReport addClientCompetencyScores(IndividualReport ir, String partId, long dnaId,
			ReportModelStructureDTO ccStruct) {
		// Get the data (including scores)
		// ReportModelStructureDTO ccStruct =
		// new ReportModelDataHelper().getReportModelWithScores(con, dnaId,
		// partId);
		// Using indices so that holes in the sequence don't hurt
		int i = 0;
		int j = 0;
		for (ReportModelSuperFactorDTO sf : ccStruct.getSuperFactorList()) {
			j = 0; // Init the competency counter
			i++;
			for (ReportModelCompetencyDTO cmp : sf.getCompList()) {
				j++;
				ir.getDisplayData().put("CC_SF" + i + "_Comp" + j + "_Score", "" + cmp.getCompScore());
				// System.out.println("CC_SF" + i + "_Comp" + j + "_Score" +
				// cmp.getCompScore());
			}
		}

		return ir;
	}

	/**
	 * Put the IntegrationGrig (IG) info into the IR object Note that the dLCI
	 * is calculated in a different module and the data is inserted there
	 *
	 * @throws Exception
	 */
	public IndividualReport addIGDataforALR(IndividualReport ir, String partId, long dnaId,
			Map<Integer, String> CompInternalNames) throws Exception {
		IGSummaryDataDTO ig = new IGSummaryDataHelper(this.conn, partId, dnaId).getSummaryData();

		boolean isSubmitted = ig.getSubmitted();

		if (isSubmitted) {

			ir.getDisplayData().put("IG_WorkingNotes", ig.getWorkingNotes() == null ? "" : ig.getWorkingNotes());

			// adding the IG Submitted dates per NHN-2199
			ir.getDisplayData().put("IGDate",
					new IGSummaryDataHelper(this.conn).getIGSubmittedDateWithFormat(dnaId, partId, "dd/MM/yyyy"));
			ir.getDisplayData().put("ReportDate", new IGSummaryDataHelper(this.conn).getIGSubmittedDate(dnaId, partId));

			// get the list of comps in order (need name and save Id)
			int i = 0;
			int j = 0;
			int intCompID = 0;
			String internalName = "";
			LinkedHashMap<Long, CompInfo> cmps = new LinkedHashMap<Long, CompInfo>();
			// Loop through the superfactors
			for (CompGroupData cg : ig.getGridData().getFullCompArray()) {
				// No sf info needed... Loop through the competencies
				for (CompElementDTO comp : cg.getCgCompList()) {
					i++;

					intCompID = (int) comp.getCompId();
					// get the internal name
					internalName = CompInternalNames.get(intCompID);

					// get the position of this competency
					j = convertToKFLACompNumber(internalName);

					cmps.put(new Long(comp.getCompId()), new AlrExtractHelper().new CompInfo(j));
					ir.getDisplayData().put("IG_Comp" + j + "_Name", comp.getCompName());

					// cmps.put(new Long(comp.getCompId()), new
					// AlrExtractHelper().new CompInfo(i));
					// ir.getDisplayData().put("IG_Comp" + i + "_Name",
					// comp.getCompName());
				}
			}
			ir.getDisplayData().put("IG_Comp_Count", "" + i);

			// Final (consultant entered)scores (and notes)
			double finCum = 0.0;
			int finCnt = 0;
			for (IGFinalScoreDTO finScores : ig.getCompFinData()) {
				// get the compId
				long id = finScores.getCompId();
				// Note that the value here is an index into a list, not the
				// value to be used
				int idx = finScores.getCompScoreIdx();
				// get the actual score value as a string
				// use simplifying assumptions 1=1.0, 2=1.5, 3=2.0,...9=5.0
				String scor = null;
				if (idx < 1) {
					scor = "N/A";
				} else {
					double raw = (idx + 1.0) / 2.0;
					raw = Math.floor((raw * 100.0) + 0.505) / 100.0;
					scor = "" + raw;
					finCum += raw;
					finCnt++;
				}
				// Get the position of the competency and put out the score
				int posn = cmps.get(new Long(id)).getPosition();
				ir.getDisplayData().put("IG_Comp" + posn + "_FinalScore", scor);
				// and don't forget the note
				ir.getDisplayData().put("IG_Comp" + posn + "_Note",
						finScores.getCompNote() == null ? "" : finScores.getCompNote());
			}
			// Total score
			double tot = 0.0;
			if (finCnt != 0.0) {
				tot = finCum / finCnt;
				tot = Math.floor((tot * 100.0) + 0.505) / 100.0;
			}
			ir.getDisplayData().put("IG_TotalScore", "" + tot);

			// gen data for calculated comp scores
			for (DNACellDTO cell : ig.getGridData().getModCompIntersection()) {
				// Loop through accumulating data
				long id = cell.getCompetencyId();
				String sc = cell.getScore();
				double scor = Double.parseDouble(sc);
				cmps.get(new Long(id)).addCum(scor);
			}
			// Calculate and display calculated comp scores
			for (Iterator<CompInfo> itr = cmps.values().iterator(); itr.hasNext();) {
				String str;
				CompInfo cur = itr.next();
				if (cur.getCount() == 0.0) {
					str = "0.0";
				} else {
					double scor = cur.getCum() / cur.getCount();
					scor = Math.floor((scor * 100.0) + 0.505) / 100.0;
					str = "" + scor;
				}
				ir.getDisplayData().put("IG_Comp" + cur.getPosition() + "_CalcScore", str);
			}
		} // End of "isSubmitted"
		return ir;
	}

	/**
	 * Put the EG info into the IR
	 *
	 * @throws Exception
	 */
	public IndividualReport addEGDataforALR(IndividualReport ir, String partId, long dnaId,
			Map<Integer, String> CompInternalNames) throws Exception {
		Statement stmt = null;
		ResultSet rs = null;

		String strModuleName = "";
		String strInterview = "Off";
		String strBRE = "Off";
		String strDRM = "Off";
		String strPeer = "Off";
		String strBoss = "Off";
		String strTownHall = "Off";
		String strCustomer = "Off";
		String strStratFinEx = "Off";
		String strTop200 = "Off";
		String strBrdMtg = "Off";
		String strCfin = "Off";
		String strInbox = "Off";
		String strTeam = "Off";
		String str360 = "Off";
		String strAnalystMeeting = "Off";
		String strModuleAbbr = ""; // Gets filled with the actual module
									// abbreviation
		String strTesting = ""; // Used to tell what sort of testing is being
								// displayed

		// Get the module IDs in order (don't need the module internal name
		// here)
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT DISTINCT dl.moduleId, dl.modSeq ");
		sqlQuery.append("  FROM pdi_abd_dna_link dl, pdi_abd_module amod ");
		sqlQuery.append("  WHERE dl.dnaId = " + dnaId + " ");
		sqlQuery.append("    AND dl.isUsed = 1 ");
		sqlQuery.append("    AND amod.moduleId = dl.moduleId ");
		sqlQuery.append("    AND amod.hasEG = 1 ");
		sqlQuery.append("  ORDER BY dl.modSeq");

		try {
			stmt = this.conn.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			if (!rs.isBeforeFirst()) {
				System.out.println("No EGs for DNA " + dnaId);
				return null;
			}

			@SuppressWarnings("unused")
			int i = 0;
			while (rs.next()) {
				boolean skip = false;
				i++;
				EGFullDataDTO eg = new EGDataHelper(this.conn, partId, dnaId, rs.getLong("moduleId"))
						.getEGDisplayData();
				// Put out sim level data

				strModuleName = eg.getSimName();

				switch (strModuleName) {

				case "Interview":
					strInterview = "On";
					strModuleAbbr = "Interview";
					break;

				case "Testing (KFALP)": // This is the EG version (manual
										// scoring)
					strTesting = "KFALP - Manual";
					strModuleAbbr = "Testing";
					break;

				case "Testing (KF4D)": // This is the EG version (manual
										// scoring)
					strTesting = "KF4D - Manual";
					strModuleAbbr = "Testing";
					break;

				case "Direct Report Meeting":
					strDRM = "On";
					strModuleAbbr = "DRM";
					break;

				case "Direct Report Meeting (Chris Wallace)": // This duplicates
																// DRM because
																// it is mapped
																// to its
																// columns
					strDRM = "On";
					strModuleAbbr = "DRM";
					break;

				case "Boss Meeting":
					// case "Manager Meeting": // Not yet (but it will work)
					strBoss = "On";
					strModuleAbbr = "Boss";
					break;

				case "Peer Meeting":
					strPeer = "On";
					strModuleAbbr = "Peer";
					break;

				case "Customer Meeting":
					strCustomer = "On";
					strModuleAbbr = "Customer";
					break;

				case "Business Review Exercise":
					strBRE = "On";
					strModuleAbbr = "BRE";
					break;

				case "Town Hall":
					strTownHall = "On";
					strModuleAbbr = "TownHall";
					break;

				case "Strategy and Financial Exercise":
					strStratFinEx = "On";
					strModuleAbbr = "StratFinEx";
					break;

				case "Top 200":
					strTop200 = "On";
					strModuleAbbr = "Top200";
					break;

				case "Board Meeting":
					strBrdMtg = "On";
					strModuleAbbr = "BrdMtg";
					break;

				case "CFIN":
					strCfin = "On";
					strModuleAbbr = "Cfin";
					break;

				case "In-Box":
					strInbox = "On";
					strModuleAbbr = "Inbox";
					break;

				case "Team Meeting":
					strTeam = "On";
					strModuleAbbr = "Team";
					break;

				case "KF360":
					str360 = "On";
					strModuleAbbr = "KF360";
					break;
				case "Analyst Meeting":
					strAnalystMeeting = "On";
					strModuleAbbr = "AnalystMeeting";
					break;

				default:
					skip = true;
					break;
				}

				// Go to the next module without processing the data?
				if (skip) {
					continue;
				}

				// ir.getDisplayData().put("EG" + i + "_Name", eg.getSimName());
				// ir.getDisplayData().put("EG" + i + "_OPR", "" +
				// eg.getOverallData().getOpr());
				// ir.getDisplayData().put("EG" + i + "_BriefDesc",
				// eg.getOverallData().getBriefDesc() == null ? "" :
				// eg.getOverallData().getBriefDesc());
				// ir.getDisplayData().put("EG" + i + "_KeyStrengths",
				// eg.getOverallData().getKeyStrengths() == null ? "" :
				// eg.getOverallData().getKeyStrengths());
				// ir.getDisplayData().put("EG" + i + "_KeyNeeds",
				// eg.getOverallData().getKeyDevNeeds() == null ? "" :
				// eg.getOverallData().getKeyDevNeeds());
				// ir.getDisplayData().put("EG" + i + "_CoachNotes",
				// eg.getOverallData().getCoachNotes() == null ? "" :
				// eg.getOverallData().getCoachNotes());

				// Put out the comps for this sim
				int j = 0;
				int intCompID = 0;
				String internalName = "";
				int zeroCnt;
				double avgScore;
				for (EGCompDisplayData comp : eg.getCompArray()) {
					zeroCnt = 0;
					avgScore = 0.0;

					intCompID = (int) comp.getCompId();
					// get the internal name
					internalName = CompInternalNames.get(intCompID);

					// get the position of the competency
					j = convertToKFLACompNumber(internalName);

					if ("KF360".equals(strModuleName)) {
						// This is a special case. Use the extCompScore
						String score = comp.getExtCompScore();
						if (score == null || score.isEmpty() || score.equals("-1.00")) {
							score = "0.0";
						}
						avgScore = Double.valueOf(score);
						if (avgScore == 0.0) {
							zeroCnt = 2; // Fake it into putting an empty string
											// in for score
						}
					} else {
						// Normal case... comp the score
						// Get the EG competency score
						if (comp.getOverideScoreIndex() == 0) {
							// do it the regular way
							// Get the average score (N/A if less than 2, but FE
							// passes through)
							double cum = 0;
							int cnt = 0;
							for (EGBarDisplayData bar : comp.getCompBars()) {
								int score = bar.getBarScore();
								if (score > 0) {
									cum += score;
									cnt++;
								} else if (score == 0) {
									zeroCnt++;
								}
							}
							// calc and save the score
							if (cnt > 0) {
								avgScore = (zeroCnt > 1) ? 0.0f : (cum / cnt);
							}
						} else {
							// There is a non-zero value for override... use it
							// (zeroCnt has no meaning here; overide assumes
							// that the number of behaviors scored is
							// irrelevant)
							avgScore = (comp.getOverideScoreIndex() + 1.0) / 2.0;
						}
					} // End of else (this is a normal comp)

					avgScore = Math.floor((avgScore * 100.0) + 0.505) / 100.0;
					ir.getDisplayData().put("EG_" + strModuleAbbr + "_Comp" + j + "_Score",
							zeroCnt > 1 ? "" : String.format("%.2f", avgScore));
					ir.getDisplayData().put("EG_" + strModuleAbbr + "_Comp" + j + "_" + comp.getCompName() + "_Score",
							zeroCnt > 1 ? "" : String.format("%.2f", avgScore));
				} // End of competency For loop
					// ir.getDisplayData().put("EG" + i + "_Comp_Count", "" +
					// j);
			} // End of rs while loop
				// ir.getDisplayData().put("EG_Count", "" + i);
			ir.getDisplayData().put("Interview", "" + strInterview);
			ir.getDisplayData().put("Testing", "" + strTesting);
			ir.getDisplayData().put("DRM", "" + strDRM);
			ir.getDisplayData().put("Boss", "" + strBoss);
			ir.getDisplayData().put("Peer", "" + strPeer);
			ir.getDisplayData().put("Customer", "" + strCustomer);
			ir.getDisplayData().put("BRE", "" + strBRE);
			ir.getDisplayData().put("TownHall", "" + strTownHall);
			ir.getDisplayData().put("StratFinEx", "" + strStratFinEx);
			ir.getDisplayData().put("Top200", "" + strTop200);
			ir.getDisplayData().put("BrdMtg", "" + strBrdMtg);
			ir.getDisplayData().put("Cfin", "" + strCfin);
			ir.getDisplayData().put("InBox", "" + strInbox);
			ir.getDisplayData().put("Team", "" + strTeam);
			ir.getDisplayData().put("KF360", "" + str360);
			ir.getDisplayData().put("AnalystMeeting", "" + strAnalystMeeting);

			return ir;
		} catch (SQLException ex) {
			String str = "addEGDataforALR(): SQL error.  " + "SQLException: " + ex.getMessage() + ", " + "SQLState: "
					+ ex.getSQLState() + ", " + "VendorError: " + ex.getErrorCode();
			System.out.println(str);
			throw new Exception(str);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					/*  Swallow the error */ }
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					/*  Swallow the error */ }
				stmt = null;
			}
		}
	}

	/**
	 * getAlpDataforALR - Gets the ALR scoring data for the auto scored case
	 *
	 * @param ir
	 * @param partId
	 * @param dnaId
	 * @param compInternalNames
	 * @return
	 */
	public IndividualReport getAlrCompScoreData(IndividualReport ir, String partId, long dnaId,
			Map<Integer, String> compInternalNames, String type) {
		try {
			Map<Long, String> scores = null;
			if (type == ReportingConstants.SRC_KFP) {
				IGAlrDataHelper helper = new IGAlrDataHelper(conn, partId, dnaId);
				scores = helper.getAlrTestCompScores();

				ir.getDisplayData().put("Testing", "KFALP - Auto"); // This is
																	// the auto
																	// scoring
																	// (ALP
																	// scores
																	// from the
																	// scoring
																	// engine)
			} else if (type == ReportingConstants.SRC_KF4D) {
				IGKf4dDataHelper helper = new IGKf4dDataHelper(conn, partId, dnaId);
				scores = helper.getKf4dTestCompScores();
				/* -- This adds capacity values to extract from RV2 scores
				 * -- This might be done wrong, need to verify when the time comes.
								if (!StringUtils.isEmpty(helper.getRv2Rating())){
									ir.getDisplayData().put("capacityScore", helper.getRv2Rating());
									ir.getDisplayData().put("capacityRaw", String.valueOf(helper.getRv2Rawscore()));
								}
								*/
				ir.getDisplayData().put("Testing", "KF4D or DLA"); // This is
																	// the auto
																	// scoring
																	// (ALP
																	// scores
																	// from the
																	// scoring
																	// engine)
			} else {
				return null;
			}

			// Put out the comps for this sim
			for (Map.Entry<Long, String> ent : scores.entrySet()) {
				int comp = ent.getKey().intValue();
				String score = ent.getValue();
				if (score.equals("-1.00")) {
					score = "";
				}

				String intName = compInternalNames.get(comp);

				log.trace("Got Testing competency: {} with ID: {}, with score: {}", intName, comp, score);

				// get the mapping for the competency
				int j = convertToKFLACompNumber(intName);

				// It isn't an EG but we are going to stick it there anyway
				// (compatibility)
				ir.getDisplayData().put("EG_Testing_Comp" + j + "_Score", score);
				ir.getDisplayData().put("EG_Testing_Comp" + j + "_" + intName + "_Score", score);
			}
		} catch (Exception e) {
			// Report the error but don't kill the process if upstream error
			log.error("getAlrCompScoreData():  Unable to fetch auto ALP data, continuing.  Part={}, dnaId={}, msg={}",
					partId, dnaId, e.getMessage());
			return ir;
		}

		return ir;
	}

	/**
	 * getAlpRawData - Returns the raw alp data for ALR input
	 *
	 * @param ir
	 * @param partId
	 * @param projId
	 * @return
	 */
	public IndividualReport getAlpRawData(IndividualReport ir, String partId, String projId) throws Exception {
		// Get required ancilliary info
		// ----------------------------------------------------------------------------------------------//
		// TODO Eventually we may be able to get the info over in the target by
		// getting a Project
		// object and fetching the data (no need to send it across)
		// Get the project related data needed for queries
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT  nn.version, ");
		sb.append(" 	   tlt.code, ");
		sb.append(" 	   tlt.target_level_index ");
		sb.append("  FROM platform.dbo.project pp WITH(NOLOCK) ");
		sb.append(
				"  LEFT JOIN platform.dbo.target_level_type tlt ON tlt.target_level_type_id = pp.target_level_type_id ");
		sb.append(
				"  LEFT JOIN platform.dbo.norm nn ON nn.project_type_id = pp.project_type_id and nn.default_norm = 1 ");
		sb.append("  WHERE pp.project_id = " + projId);

		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(sb);
		if (dlps.isInError()) {
			String err = "AlrExtractHelper.getAlpRawData() - Error preparing project query.  " + dlps.toString();
			System.out.println(err);
			throw new Exception(err);
		}

		// Go and get the project related info
		DataResult dr = null;
		ResultSet rs = null;
		String nVer = null;
		String tlCode = null;
		int tlIdx;

		try {
			dr = NhnDatabaseUtils.select(dlps);
			int stat = dr.getStatus().getStatusCode();
			if (stat == RequestStatus.RS_ERROR) {
				String err = "AlrExtractHelper.getAlpRawData() - Error executing select.  Ppt=" + partId + ", proj="
						+ projId + ". " + dlps.toString();
				System.out.println(err);
				throw new Exception(err);
			} else {
				if (stat == RequestStatus.DBS_NO_DATA) {
					// No data to find...
					String err = "AlrExtractHelper.getAlpRawData() - No data found for project " + projId + ". ";
					System.out.println(err);
					throw new Exception(err);
				} else {
					// Get the data
					rs = dr.getResultSet();
					if (rs == null || !rs.isBeforeFirst()) {
						String err = "AlrExtractHelper.getAlpRawData() - No ResultSet for project " + projId + ". ";
						System.out.println(err);
						throw new Exception(err);
					} else {
						// Get the data -- assume that there is a single row
						rs.next();
						nVer = rs.getString("version");
						tlCode = rs.getString("code");
						tlIdx = rs.getInt("target_level_index");
					}
				}
			}
		} finally {
			if (dr != null) {
				dr.close();
				dr = null;
			}
		}

		// Check version
		if (nVer == null || nVer.length() < 1) {
			String err = "AlrExtractHelper.getAlpRawData - No norm version data for project " + projId;
			System.out.println(err);
			throw new Exception(err);
		}
		if (tlCode == null || tlCode.length() < 1) {
			String err = "AlrExtractHelper.getAlpRawData - No target level code found for project " + projId;
			System.out.println(err);
			throw new Exception(err);
		}
		if (tlIdx == 0) {
			String err = "AlrExtractHelper.getAlpRawData - Invalid target level inde (0).  Target Level code=" + tlCode
					+ ",  project= " + projId + ".";
			System.out.println(err);
			throw new Exception(err);
		}
		// ----------------------------------------------------------------------------------------------//

		// get the data
		URL url = null;
		URLConnection uc = null;
		InputStream content = null;

		// Call the servlet
		String surl = PropertyLoader.getProperty("com.pdi.data.abyd.application", "palms.reportServer.url");
		if (surl.charAt(surl.length() - 1) == '/') {
			surl = surl.substring(0, surl.length() - 2);
		}
		surl += SCORE_PARM_STR;

		surl = surl.replace("{pptId}", partId);
		surl = surl.replace("{projId}", "" + projId);
		surl = surl.replace("{tgtLvl}", "" + tlIdx);
		surl = surl.replace("{normVer}", nVer);

		try {
			url = new URL(surl);
		} catch (MalformedURLException e) {
			String err = "AlrExtractHelper.getAlpRawData - Malformed URL - " + surl;
			System.out.println(err);
			throw new Exception(err);
		}

		// open the connection.
		try {
			uc = url.openConnection();
		} catch (IOException e) {
			String err = "AlrExtractHelper.getAlpRawData - Unable to open connection.  URL=" + surl + ".  Msg="
					+ e.getMessage();
			System.out.println(err);
			throw new Exception(err);
		}

		// Get the data
		HttpURLConnection huc = (HttpURLConnection) uc;
		boolean hasAlpContent = true; // assume data there
		try {
			int statusCode = huc.getResponseCode();
			if (statusCode == HttpServletResponse.SC_NO_CONTENT
					|| statusCode == HttpServletResponse.SC_INTERNAL_SERVER_ERROR) {
				// String str = "The KFALP instrument has not been completed for
				// participant " + partId + ". The IG will not be available
				// until it is complete.";
				// System.out.println(str); // Don't track this in the log
				// throw new Exception(str); // Don't throw an error
				System.out.println("getAlpRawData():  No kfp content for part " + partId);
				hasAlpContent = false;
			} else if (statusCode >= 200 && statusCode < 300) {
				content = huc.getInputStream();
			} else {
				String str = "Error returned from score fetch: Code=" + statusCode + " for URL=" + surl + ".  Message="
						+ huc.getResponseMessage();
				System.out.println(str);
				throw new Exception(str);
			}
		} catch (IOException e) {
			String err = "getAlpRawData: Error fetching score RGR data.  Msg=" + e.getMessage();
			System.out.println(err);
			throw new Exception(err);
		}

		if (hasAlpContent) {
			BufferedReader br = new BufferedReader(new InputStreamReader(content));
			StringBuffer result = new StringBuffer();
			String line;
			try {
				while ((line = br.readLine()) != null) {
					result.append(line);
				}
			} catch (IOException e) {
				String err = "AlrExtractHelper.getAlpRawData() - Unable to read content.  URL=" + surl + ".  Msg="
						+ e.getMessage();
				System.out.println(err);
				throw new Exception(err);
			}

			// convert the XML to a hashMap
			DocumentBuilderFactory dbfac = null;
			DocumentBuilder docBuilder = null;
			Document doc = null;
			String xml = "";
			try {
				dbfac = DocumentBuilderFactory.newInstance();
				docBuilder = dbfac.newDocumentBuilder();
				xml = result.toString();
				doc = docBuilder.parse(new InputSource(new StringReader(xml)));
			} catch (ParserConfigurationException pce) {
				String err = "AlrExtractHelper.getAlpRawData() - XML parser config ERROR.  msg=" + pce.getMessage();
				System.out.println(err);
				throw new Exception(err);
			} catch (SAXException se) {
				String err = "AlrExtractHelper.getAlpRawData() - XML SAX ERROR.  msg=" + se.getMessage() + " \n"
						+ " ppt=" + partId + ", proj=" + projId + ", tgtLvl=" + tlIdx + ", nVer=" + nVer + " \n"
						+ " XML=" + xml;
				System.out.println(err);
				throw new Exception(err);
			} catch (IOException e) { // everything else
				String err = "AlrExtractHelper.getAlpRawData() - IOException ERROR.  msg=" + e.getMessage();
				System.out.println(err);
				throw new Exception(err);
			}

			Map<String, String> temp = new HashMap<String, String>();
			Element root = doc.getDocumentElement();
			NodeList children = root.getChildNodes();
			for (int i = 0; i < children.getLength(); i++) {
				Node child = children.item(i);
				NamedNodeMap anm = child.getAttributes();
				String keyVal = anm.getNamedItem("key").getNodeValue();
				if (keyVal.equalsIgnoreCase("JF17") || keyVal.equalsIgnoreCase("JF18")
						|| keyVal.equalsIgnoreCase("JF19") || keyVal.equalsIgnoreCase("JF20")) {
					// suppress the data
				} else {
					String valVal = anm.getNamedItem("value").getNodeValue();
					temp.put(keyVal, valVal);
				}
			}

			// stuff it into the Individual Report
			ir.getDisplayData().putAll(temp);
		}

		// Scram
		return ir;
	}

	/**
	 * getGpiCogsCompScoreData - Gets the test scoring data for ALRs using
	 * traditional (GPI/Cogs) data
	 *
	 * @param ir
	 * @param partId
	 * @param dnaId
	 * @param compInternalNames
	 * @return
	 */
	public IndividualReport getGpiCogsCompScoreData(IndividualReport ir, String partId, long dnaId,
			Map<Integer, String> compInternalNames) {
		Connection con = null;
		IGTestColumnDataHelper igTestColHlpr = null;
		IGInstDisplayData igDisplayData = null;
		IGIntersectionAndOtherDataDTO testingIntersection = null;
		try {
			con = AbyDDatabaseUtils.getDBConnection();
			igTestColHlpr = new IGTestColumnDataHelper(con, partId, dnaId);
			igDisplayData = igTestColHlpr.getTestingColumnData();

			// The false is for ALP data... We called this from ALR after
			// checking for ALP data and finding none.
			// the second false is for KF4D info, also patently false here
			// testingIntersection = IGScoring.fetchTestingIntersectionData(con,
			// partId, dnaId, true, false, false);
			testingIntersection = IGScoring.fetchTestingIntersectionData(con, partId, dnaId, true, false, false, null);
		} catch (Exception e) {
			System.out.println(
					"Error: AlrExtractHelper.getGpiCogsCompScoreData().  Error fetching test column data; no data fetched.  msg="
							+ e.getMessage());
			return ir;
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception se) {
			}
			con = null;
		}

		for (IGInstCompData compData : igDisplayData.getCompArray()) {
			// Find comp in the testing intersection cells
			for (DNACellDTO cell : testingIntersection.getTestingCells()) {
				if (cell.getCompetencyId() == compData.getCompId()) {
					// Got a match... put it out
					int comp = (int) compData.getCompId();
					String score = cell.getScore();
					String intName = compInternalNames.get(comp);
					// get the mapping for the competency
					int j = convertToKFLACompNumber(intName);
					// It isn't an EG but we are going to stick it there anyway
					// (compatibility)
					ir.getDisplayData().put("EG_Testing_Comp" + j + "_Score", score);
					ir.getDisplayData().put("EG_Testing_Comp" + j + "_" + intName + "_Score", score);
				}
			}
		}
		ir.getDisplayData().put("Testing", "GPI / Cogs");

		return ir;
	}

	/**
	 * convertToKFLACompNumber - Put in an internal name and get a position
	 * index
	 *
	 * @param compName
	 *            - the key to the name/number lookup
	 * @return the index number of the name
	 */
	private int convertToKFLACompNumber(String compName) {
		int compNumber = 0;

		if (KFLA_COMPS_MAP.containsKey(compName)) {
			compNumber = KFLA_COMPS_MAP.get(compName);
		}

		return compNumber;
	}

	/**
	 *
	 * @param intNames
	 * @return
	 */
	public boolean checkCustom(Map<Integer, String> intNames) {
		boolean ret = false; // default to false

		if (intNames == null || intNames.isEmpty() || intNames.entrySet().isEmpty()
				|| !intNames.entrySet().iterator().hasNext()) {
			return ret;
		}

		Map.Entry<Integer, String> ent = intNames.entrySet().iterator().next();
		String intCompName = ent.getValue();
		String suffix = intCompName.substring(intCompName.indexOf(" - "));
		if (CUSTOM_SUFFIX_SET.contains(suffix)) {
			ret = true;
		}

		return ret;
	}

	/**
	 * getCustCompNames - Get the custom competency names and their keys. Note
	 * that the keys are the same as for regular ALR projects (start with
	 * "KFLA_COMP" and end with "_Name"
	 *
	 * @param dnaId
	 *            - DNA ID of the project in question
	 * @return Hashmap of keys and names
	 * @throws Exception
	 */
	public Map<String, String> getCustCompNames(long dnaId) throws Exception {
		// ret.put("KFLA_COMP1_Name", "Business Insight");
		HashMap<String, String> ret = new HashMap<String, String>();

		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT DISTINCT tt.text, dl.groupSeq, dl.compSeq ");
		sqlQuery.append("  FROM pdi_abd_dna_link dl ");
		sqlQuery.append("    LEFT JOIN pdi_abd_competency cc ON cc.competencyId = dl.competencyId ");
		sqlQuery.append("    LEFT JOIN pdi_abd_text tt ON tt.textId = cc.textId AND tt.languageId = 1 ");
		sqlQuery.append("  WHERE dl.dnaId = " + dnaId + " ");
		sqlQuery.append("  ORDER BY dl.groupSeq, dl.compSeq");
		// System.out.println("Cust comp name Data SQL=" + sqlQuery.toString());

		try {
			stmt = this.conn.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			// process results
			int idx = 0;
			while (rs.next()) {
				idx++;
				String key = "KFLA_COMP" + idx + "_Name";
				String val = rs.getString("text");
				ret.put(key, val);
			}

			return ret;
		} catch (SQLException ex) {
			// handle any errors
			String str = "getCustCompNames():  SQL fetching names for DNA " + dnaId + ".  SQLException: "
					+ ex.getMessage();
			System.out.println(str);
			throw new Exception(str);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					// swallow the error
				}
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					// swallow the error
				}
				stmt = null;
			}
		}
	}

	/**
	 * getCompNamesWSyn - Get a list of all of the standard competency names in
	 * order, then override the standard names and positions with any synonyms.
	 * It actually overwrites ALL used competencies with their DNA specified
	 * name. (start with "KFLA_COMP" and end with "_Name"
	 *
	 * @param dnaId
	 *            - DNA ID of the project in question
	 * @return Hashmap of keys and names
	 * @throws Exception
	 */
	public Map<String, String> getCompNamesWSyn(long dnaId) throws Exception {
		HashMap<String, String> ret = new HashMap<String, String>();
		ret.putAll(STD_KFLA_COMP_NAMES);

		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT DISTINCT dl.competencyId, cc.internalName, tt.text ");
		sqlQuery.append("  FROM pdi_abd_dna_link dl ");
		sqlQuery.append("    LEFT JOIN pdi_abd_competency cc ON cc.competencyId = dl.competencyId ");
		sqlQuery.append("    LEFT JOIN pdi_abd_text tt ON (tt.textId = cc.textId AND languageId = 1) ");
		sqlQuery.append("  WHERE dl.dnaId = " + dnaId);
		// System.out.println("Cust comp name Data SQL=" + sqlQuery.toString());

		try {
			stmt = this.conn.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			// process results
			while (rs.next()) {
				String internalName = rs.getString("internalName");
				int idx = convertToKFLACompNumber(internalName);
				String key = "KFLA_COMP" + idx + "_Name";
				String val = rs.getString("text");
				ret.put(key, val);
			}

			return ret;
		} catch (SQLException ex) {
			// handle any errors
			String str = "getCompNamesWSyn():  SQL fetching names for DNA " + dnaId + ".  SQLException: "
					+ ex.getMessage();
			System.out.println(str);
			throw new Exception(str);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					// swallow the error
				}
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					// swallow the error
				}
				stmt = null;
			}
		}
	}

	/**
	 * Fetch the Impact Rating data.
	 *
	 * @throws Exception
	 */
	public IndividualReport addIrData(GroupReport gr, IndividualReport ir, String partId, long dnaId) throws Exception {
		Statement stmt = null;
		ResultSet rs = null;
		StringBuffer sqlQuery = new StringBuffer();
		String context = "display";

		try {
			// get the display data objects into a linked hash table
			sqlQuery.append("SELECT ir.moduleId, ");
			sqlQuery.append("       txt.text as moduleText, ");
			sqlQuery.append("       ir.irSequence, ");
			sqlQuery.append("       irr.irScore ");
			sqlQuery.append("  FROM pdi_abd_dna dna ");
			sqlQuery.append("    INNER JOIN pdi_abd_eg_ir ir ON (ir.modelId = dna.modelId) ");
			sqlQuery.append("    LEFT JOIN pdi_abd_module mod ON (mod.moduleId = ir.moduleId)");
			sqlQuery.append("    LEFT JOIN pdi_abd_text txt ON (txt.textId = mod.textId AND txt.languageId = 1)");
			sqlQuery.append("    LEFT JOIN pdi_abd_eg_ir_response irr ON (irr.moduleId = ir.moduleId AND");
			sqlQuery.append("    irr.irSequence = ir.irSequence AND irr.dnaId = dna.dnaId AND");
			sqlQuery.append("    irr.participantId= " + partId + ")");
			sqlQuery.append("  WHERE dna.dnaId = " + dnaId + " ");
			sqlQuery.append("  ORDER BY ir.moduleId, ir.irSequence");

			stmt = this.conn.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			String key = "";
			StringBuffer impactRatingIds = new StringBuffer("");
			while (rs.next()) {
				key = "IR_" + new Integer(rs.getInt("moduleId")) + "_" + new Integer(rs.getInt("irSequence"));
				if (gr != null){
					gr.addDisplayData(key + "_Name",
							rs.getString("moduleText") + " Impact Rating #" + rs.getInt("irSequence"));
				}
				if (ir != null){
					ir.addDisplayData(key + "_Score", rs.getInt("irScore") == 0 ? "" : "" + rs.getInt("irScore"));
				}

				if (impactRatingIds.length() == 0) {
					impactRatingIds.append(key);
				} else {
					impactRatingIds.append(",");
					impactRatingIds.append(key);
				}
			}
			if (gr != null){
				gr.getDisplayData().put("IR_Rating_Ids", impactRatingIds.toString());
			}

			return ir;

		} catch (SQLException ex) {
			throw new Exception("SQL in EG getIrData (" + context + ").  " + "SQLException: " + ex.getMessage() + ", "
					+ "SQLState: " + ex.getSQLState() + ", " + "VendorError: " + ex.getErrorCode());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					/*  Swallow the error */ }
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					/*  Swallow the error */ }
				stmt = null;
			}
		}
	}
	
	public void addIrDataGroup(GroupReport gr, long dnaId) throws Exception {
		Statement stmt = null;
		ResultSet rs = null;
		StringBuffer sqlQuery = new StringBuffer();
		String context = "display";

		try {
			// get the display data objects into a linked hash table
			sqlQuery.append("SELECT ir.moduleId, ");
			sqlQuery.append("       txt.text as moduleText, ");
			sqlQuery.append("       ir.irSequence ");
			sqlQuery.append("  FROM pdi_abd_dna dna ");
			sqlQuery.append("    INNER JOIN pdi_abd_eg_ir ir ON (ir.modelId = dna.modelId) ");
			sqlQuery.append("    LEFT JOIN pdi_abd_module mod ON (mod.moduleId = ir.moduleId)");
			sqlQuery.append("    LEFT JOIN pdi_abd_text txt ON (txt.textId = mod.textId AND txt.languageId = 1)");
			
			sqlQuery.append("  WHERE dna.dnaId = " + dnaId + " ");
			sqlQuery.append("  ORDER BY ir.moduleId, ir.irSequence");

			stmt = this.conn.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			String key = "";
			StringBuffer impactRatingIds = new StringBuffer("");
			while (rs.next()) {
				key = "IR_" + new Integer(rs.getInt("moduleId")) + "_" + new Integer(rs.getInt("irSequence"));
				
				gr.addDisplayData(key + "_Name",
						rs.getString("moduleText") + " Impact Rating #" + rs.getInt("irSequence"));
				


				if (impactRatingIds.length() == 0) {
					impactRatingIds.append(key);
				} else {
					impactRatingIds.append(",");
					impactRatingIds.append(key);
				}
			}
			
			gr.getDisplayData().put("IR_Rating_Ids", impactRatingIds.toString());
			

		} catch (SQLException ex) {
			throw new Exception("SQL in EG getIrData (" + context + ").  " + "SQLException: " + ex.getMessage() + ", "
					+ "SQLState: " + ex.getSQLState() + ", " + "VendorError: " + ex.getErrorCode());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					/*  Swallow the error */ }
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					/*  Swallow the error */ }
				stmt = null;
			}
		}
	}

	// ------------------------------------------------------------------------------------------------------------

	/*
	 * Private class that holds data about a competency.  Used for accumulating scores in addIGDataforALR()
	 */
	private class CompInfo {
		// Instance variables
		private final int _posn;
		private int _cnt = 0;
		private double _cum = 0.0;

		public CompInfo(int pos) {
			_posn = pos;
		}

		// methods
		public int getPosition() {
			return _posn;
		}

		// There is no setCount method. The count is bumped when a
		// valid score is added
		public int getCount() {
			return _cnt;
		}

		// Adds the value to the cum bucket and bumps the count bucket per the
		// rules implemented in the UI (view.controls.home ~ 700 & ~715)
		public void addCum(double value) {
			if (value == -1.0)
				return;
			else if (value == 0.0)
				return;
			else {
				_cum += value;
				_cnt++;
			}
		}

		public double getCum() {
			return _cum;
		}
	}
}
