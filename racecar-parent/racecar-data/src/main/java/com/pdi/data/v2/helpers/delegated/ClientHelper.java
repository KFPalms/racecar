package com.pdi.data.v2.helpers.delegated;

import java.util.ArrayList;

import com.pdi.data.dto.Client;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.interfaces.IClientHelper;

public class ClientHelper implements IClientHelper {

	/**
	 * all
	 * use case:  get all clients
	 * @param SessionUser session
	 * @return ArrayList<Client> 
	 */
	//@Override
	public ArrayList<Client> all(SessionUser session)
	{	
		// Replace ETR calls with jdbc calls if this function is needed any more
//		ArrayList<Client> clients = new ArrayList<Client>();
//		try {
//			
//			//TODO: Make this USERID instead of USERNAME
//			String username = session.getMetadata().get("USERNAME");
//			String password = session.getMetadata().get("PASSWORD");
//			V2WebserviceUtils v2web = new V2WebserviceUtils();
//			SessionUserDTO sessionUser = v2web.registerUser(username, password);
//			
//			//TODO: Need to create sql here to get Clients listed depending on user access level... ARRRGHHH!
//			String xml = "";
//			xml += "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
//			xml += "		<ETR SessionId=\"${sessionId}\">";
//			xml += "			<Rqt Cmd=\"OpenSQL\" AN=\"JobListSQL\" FA=\"Y\" FR=\"1\" RC=\"N\" SQ=\"SELECT UniqueIdStamp, Name, Parent, ClientType FROM Clients where ( UniqueIdStamp = (select ClientID from users where UniqueIDStamp = '${userId}') or 'EXT_ROOT' = (select ClientID from users where UniqueIDStamp = '${userId}') or 'INT_ROOT' = (select ClientID from users where UniqueIDStamp = '${userId}') ) ORDER BY Name\" />";		
//			xml += "			<Rqt Cmd=\"Read\" AN=\"JobListSQL\" Sub=\"JobList\" Rpt=\"*\" RQ=\"N\">";
//			xml += "				<Fld FN=\"UniqueIdStamp\" Fmt=\"A\" />";
//			xml += "				<Fld FN=\"Name\" Fmt=\"A\" />";
//			xml += "				<Fld FN=\"Parent\" Fmt=\"A\" />";
//			xml += "				<Fld FN=\"ClientType\" Fmt=\"A\" />";
//	
//			xml += "			</Rqt>";
//			xml += "			<Rqt Cmd=\"Close\" AN=\"JobListSQL\" />";
//			xml += "		</ETR>";
//			xml = xml.replace("${sessionId}", sessionUser.getSessionId());
//			xml = xml.replace("${userId}", sessionUser.getId());
//			XPath xpath = XPathFactory.newInstance().newXPath(); 
//				
//			Document doc = v2web.sendData(xml);
//			NodeList records = (NodeList) xpath.evaluate("//Record", doc, XPathConstants.NODESET);
//			
//			for(int i = 0; i < records.getLength(); i++) {
//				Node node = records.item(i);
//				Client client = new Client();
//				client.setId(XMLUtils.getAttributeValue(node, "UniqueIdStamp"));
//				client.setName(XMLUtils.getAttributeValue(node, "Name"));
//				clients.add(client);
//			}
//			
//		} catch(Exception e) {
//			e.printStackTrace();
//		}
//		return clients;
		
		return null;
	}

	//@Override
	public Client fromId(SessionUser session, String clientId)
	{
		// Replace ETR calls with jdbc calls if this function is needed any more
//		Client client = new Client();
//		
//		try {
//			String username = session.getMetadata().get("USERNAME");
//			String password = session.getMetadata().get("PASSWORD");
//			V2WebserviceUtils v2web = new V2WebserviceUtils();
//			SessionUserDTO sessionUser = v2web.registerUser(username, password);
//			
//			//TODO: Need to create sql here to get Clients listed depending on user access level... ARRRGHHH!
//			String xml = "";
//			xml += "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
//			xml += "		<ETR SessionId=\"${sessionId}\">";
//			xml += "			<Rqt Cmd=\"OpenSQL\" AN=\"JobListSQL\" FA=\"Y\" FR=\"1\" RC=\"N\" SQ=\"SELECT UniqueIdStamp, Name, Parent, ClientType FROM Clients where UniqueIdStamp = '${clientId}'\" />";		
//			xml += "			<Rqt Cmd=\"Read\" AN=\"JobListSQL\" Sub=\"JobList\" Rpt=\"*\" RQ=\"N\">";
//			xml += "				<Fld FN=\"UniqueIdStamp\" Fmt=\"A\" />";
//			xml += "				<Fld FN=\"Name\" Fmt=\"A\" />";
//			xml += "				<Fld FN=\"Parent\" Fmt=\"A\" />";
//			xml += "				<Fld FN=\"ClientType\" Fmt=\"A\" />";
//			xml += "			</Rqt>";
//			xml += "			<Rqt Cmd=\"Close\" AN=\"JobListSQL\" />";
//			xml += "		</ETR>";
//			xml = xml.replace("${sessionId}", sessionUser.getSessionId());
//			xml = xml.replace("${clientId}", clientId);
//			
//			XPath xpath = XPathFactory.newInstance().newXPath(); 		
//			Document doc = v2web.sendData(xml);
//			NodeList records = (NodeList) xpath.evaluate("//Record", doc, XPathConstants.NODESET);
//			for(int i = 0; i < records.getLength(); i++) {				
//				Node node = records.item(i);
//				
//				client.setId(XMLUtils.getAttributeValue(node, "UniqueIdStamp"));
//				client.setName(XMLUtils.getAttributeValue(node, "Name"));
//			}
//			
//		} catch(Exception e) {
//			e.printStackTrace();
//		}
//		return client;
		
		return null;
	}

	//@Override
	public Client getClientByParticipantId(String participantId) {
		// TODO Auto-generated method stub
		return null;
	}
}
