package com.pdi.data.helpers.interfaces;

import java.util.ArrayList;

import com.pdi.data.dto.Project;
import com.pdi.data.dto.ProjectParticipantInstrument;
import com.pdi.data.dto.Participant;
import com.pdi.data.dto.ResponseGroup;
import com.pdi.data.dto.SessionUser;
//import com.pdi.data.dto.Status;

public interface IProjectParticipantInstrumentHelper {
	
	public static int MODE_DEFAULT = 0;
	public static int MODE_RESPONSE = 1;
	public static int MODE_SCORE = 2;
	public static int MODE_FULL = 3;
	public static int MODE_FULL_COMPLETED = 4;
	
	/**
	 * Add/Update an EPI
	 * @param session
	 * @param mode what to update (header info, response info, score info, everything)
	 * @param epi
	 * @param updateFromTestDataApp  (if the call is being made from the TestDataApp)
	 * 1-22-2013 - boolean added to determine if the update is from the usual scoring routine, or from Test Data (Manual Entry) for billing purposes.
	 */
	public void addUpdate(SessionUser session, int mode, ProjectParticipantInstrument epi, boolean updateFromTestDataApp);
	
	/**
	 * Add/Update an EPI
	 * @param session
	 * @param mode what to update (header info, response info, score info, everything)
	 * @param epi
	 * @param updateFromTestDataApp  (if the call is being made from the TestDataApp)
	 * 1-22-2013 - boolean added to determine if the update is from the usual scoring routine, or from Test Data (Manual Entry) for billing purposes.
	 */
	public void addUpdate(SessionUser session, int mode, ArrayList<ProjectParticipantInstrument> epis, boolean updateFromTestDataApp);
	
	
	/**
	 * Get all instruments by a participant/engagement link
	 * usecase: I've selected a engagement/participant and I want to see instruments from that link
	 * @param session
	 * @param engagementId
	 * @param participantId
	 * @return
	 */
	public ArrayList<ProjectParticipantInstrument> fromProjectIdParticipantId(SessionUser session, int mode, String engagementId, String participantId);
	
	
	/**
	 * Get all instruments by a participant/engagement link
	 * usecase: I've selected a engagement/participant and I want to see instruments from that link
	 * @param session
	 * @param engagementId
	 * @return
	 */
	public ArrayList<ProjectParticipantInstrument> fromProjectId(SessionUser session, int mode, String engagementId);
	
	
	/**
	 * Get a specific instrument from a participant/engagement
	 * usecase: Is there a valid instrument given an engagement/participant/instrument link
	 * @param session
	 * @param engagementId
	 * @param participantId
	 * @param instrumentId
	 * @return
	 */
	public ProjectParticipantInstrument fromProjectIdParticipantIdInstrumentId(SessionUser session, int mode, String engagementId, String participantId, String instrumentId, String callingProjectId);
	
	/**
	 * Get all instruments that this participant has ever done
	 * usecase: I need to know all instruments this participant has compelted
	 * @param session
	 * @param participantId
	 * @param String callingProjectId -- this is the projectId, if available, of the project requesting the report. Thiis
	 * 										is added because the norms used to calc the report need to come from the 
	 * 										calling project, in the case where an instrument was taken in a previous 
	 * 										project possibly having different norms.  If the projectId for the calling 
	 * 										project is not available, null should be used.
	 * 									NOTE: this is a pass-through value, which will actually be used in the 
	 * 										method ProjectParticipantInstrumentHelper.fromParticipantId()
	 * 										Added per NHN-3163.

	 * @return
	 */
	public ArrayList<ProjectParticipantInstrument> fromParticipantId(SessionUser session, int mode, String participantId, String callingProjectId);
	
	/**
	 * Get a specific participant/instrument regardless of the engagement it came from
	 * usecase: I need all of the GPIs for this participant, regardless of how many
	 * @param session
	 * @param participantId
	 * @param instrumentId
	 * @return
	 */
	public ArrayList<ProjectParticipantInstrument>  fromParticipantIdInstrumentId(SessionUser session, String participantId, String instrumentId);
	
	
	/**
	 * Filter the passed list and return EPI objects of people that 
	 * are in that list
	 * and
	 * have that instrument
	 * @param session
	 * @param participantId
	 * @param instrumentId
	 * @return
	 */
	public ArrayList<ProjectParticipantInstrument>  fromParticipantListInstrumentId(SessionUser session, ArrayList<Participant> participants, String instrumentId);
	
	
	/**
	 * Get all instruments that this participant has ever done, and
	 * grab the most recent if there are duplicates
	 * usecase: I need to know all instruments this participant has compelted
	 * @param session
	 * @param int mode -- determines if full answer data is gathered or not
	 * @param participantId
	 * @param String callingProjectId -- this is the projectId, if available, of the project requesting the report. Thiis
	 * 										is added because the norms used to calc the report need to come from the 
	 * 										calling project, in the case where an instrument was taken in a previous 
	 * 										project possibly having different norms.  If the projectId for the calling 
	 * 										project is not available, null should be used.
	 * 									NOTE: this is a pass-through value, which will actually be used in the 
	 * 										method ProjectParticipantInstrumentHelper.fromParticipantId()
	 * 										Added per NHN-3163.
	 * @return
	 */
	public ArrayList<ProjectParticipantInstrument> fromLastParticipantId(SessionUser session, int mode, String participantId, String callingProjectId);
	
	/**
	 * Get a specific participant/instrument regardless of the engagement it came from, and get the most recent!
	 * usecase: I the most recent GPI for this participant
	 * @param session
	 * @param participantId
	 * @param instrumentId
	 * @return
	 */
	public ProjectParticipantInstrument  fromLastParticipantIdInstrumentId(SessionUser session, String participantId, String instrumentId);
	
	/**
	 * Used in Test Data, for AbyD type projects.
	 * For AbyD, need to get all instruments available, even if there
	 * is no participant data for that instrument.
	 * This method gets the Instrument, and also gets the ScoreGroup
	 * data for the unfinished PPI passed in, and returns the "completed"
	 * PPI with no data, but all scales/norms for that instrument.
	 * 
	 * @param  ProjectParticipantInstrument ppi
	 * @return ProjectParticipantInstrument	  
	 */
	public ProjectParticipantInstrument getPPIWithoutScoredData(SessionUser session, Project proj, Participant part, String instId);
	
	
	/**
	 * Return all instruments.
	 * Return the most recent instrument
	 * if there is a duplicate instrument in the list.
	 * 
	 * Note that the "last" instrument may be from a
	 * different project then the other instruments.  In
	 * fact, if you have various instruments from multiple
	 * projects, the latest may all be from different 
	 * projects.  You really just don't know until
	 * you get them.
	 * 
	 * NHN 1738: new use case, with participant in two
	 * different AbyD projects, so different sets of norms
	 * 
	 * * usecase: I the most recent GPI for this participant, but he
	 * is in two different projects, with different norms.  I want the
	 * norms for the current project.
	 * @param SessionUser session
	 * @param int mode
	 * @param String particpantId
	 * @param String callingProjectId
	 * @return ArrayList<ProjectParticipantInstrument> ppiList
	 */
	public ArrayList<ProjectParticipantInstrument> fromLastParticipantIdIncludeProjectIdForNorms
							(SessionUser session,
							 int mode, 
							 String participantId,
							 String callingProjectId);
	
	
	/**
	 * Used in Test Data, for the FinEx only.
	 * NHN-1624 adds the ability to input FinEx response
	 * data to the TestData App.
	 * 
	 * This method already existed in the nhn helper, 
	 * but wasn't accessible as it was private.
	 *  
	 * @param  String participantId
	 * @return String instrumentId  
	 */
	public ResponseGroup getResponseGroupByParticipantId(String participantId, String instrumentId);
	
	
//	/**
//	 * Get the completed status for a person / engagement / instrument intersection
//   *  *****NOTE***** Because this is a private method, and is v2 specific, it is not included in the Interface
//	 * usecase: I need to know if this person has has completed a given instrument in a given engagement
//	 * @param session
//	 * @param participantId
//	 * @param engagementId
//	 * @param instrumentId
//	 * @return
//	 */
//	private Status  getStatusFromParticipantIdProjectIdInstrumentId(SessionUser session, String participantId, String engagementId, String instrumentId);
}
