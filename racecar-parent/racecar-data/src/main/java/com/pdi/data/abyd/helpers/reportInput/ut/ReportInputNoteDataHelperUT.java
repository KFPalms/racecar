package com.pdi.data.abyd.helpers.reportInput.ut;

import java.sql.Connection;
import java.util.Iterator;

import com.pdi.data.abyd.dto.reportInput.ReportInputNoteDTO;
import com.pdi.data.abyd.dto.reportInput.ReportInputNoteHolderDTO;
import com.pdi.data.abyd.helpers.reportInput.ReportInputNoteDataHelper;
import com.pdi.data.abyd.helpers.setup.ut.EntryStateHelperUT;
import com.pdi.ut.UnitTestUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;

import junit.framework.TestCase;

public class ReportInputNoteDataHelperUT  extends TestCase
{
	
	//
	// Constructors
	//

	public ReportInputNoteDataHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
		throws Exception
    {
		junit.textui.TestRunner.run(EntryStateHelperUT.class);
    }


	/*
	 * testGetSetup2ReportOptionsData
	 */
	public void testGetReportInputNotes()
		throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);

		//Connection con = AbyDDatabaseUtils.getDBConnection();
		//String ret = "database URL=" + con.getMetaData().getURL() + "\n";
	 
		ReportInputNoteDataHelper reportInputNoteDataHelper = new ReportInputNoteDataHelper();
		ReportInputNoteHolderDTO noteHolderDto = reportInputNoteDataHelper.getReportInputNotes(2434);
		
		Iterator<ReportInputNoteDTO> it = noteHolderDto.getInputNotesArray().iterator();
		System.out.println("------------------------------" );
		while (it.hasNext()){
			
			ReportInputNoteDTO dto = it.next();
			System.out.println("inputNoteId: " + dto.getInputNoteId());
			System.out.println("noteText: " + dto.getNoteText());
			System.out.println("noteType: " + dto.getNoteType());
			System.out.println("------------------------------" );
		}
	
		
		UnitTestUtils.stop(this);
	}

	
	public void testAddReportInputNotes()
	throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);
	
		Connection con = AbyDDatabaseUtils.getDBConnection();
		//String ret = "database URL=" + con.getMetaData().getURL() + "\n";
	
		ReportInputNoteDataHelper reportInputNoteDataHelper = new ReportInputNoteDataHelper();
		// do a fetch to get the notes
		ReportInputNoteHolderDTO noteHolderDto = reportInputNoteDataHelper.getReportInputNotes(2434);
		ReportInputNoteHolderDTO noteHolderDto2 = reportInputNoteDataHelper.getReportInputNotes(2434);

		// make a change		
		ReportInputNoteDTO note1 = new ReportInputNoteDTO();
		note1.setInputNoteId(1);
		note1.setNoteType(1);
		if(noteHolderDto.getInputNotesArray().get(0).getNoteText().equals("testing this note 1")){
			note1.setNoteText("changing the note 1 text");
		}else{
			note1.setNoteText("testing this note 1");
		}
		noteHolderDto2.getInputNotesArray().set(0, note1);
		
		ReportInputNoteDTO note2 = new ReportInputNoteDTO();
		note2.setInputNoteId(2);
		note2.setNoteType(2);
		if(noteHolderDto.getInputNotesArray().get(1).getNoteText().equals("testing this note 2")){
			note2.setNoteText("changing the note 2 text");
		}else{
			note2.setNoteText("testing this note 2");
		}
		noteHolderDto2.getInputNotesArray().set(1, note2);
		
		
		reportInputNoteDataHelper.addReportInputNotes(con, noteHolderDto2);
		
		// fetch them again
		ReportInputNoteHolderDTO noteHolderDto3 = reportInputNoteDataHelper.getReportInputNotes(2434);
		
		// show comparison
		System.out.println(" ------------------- ");
		System.out.println("id:  " + noteHolderDto.getInputNotesArray().get(0).getInputNoteId());
		System.out.println("type: " + noteHolderDto.getInputNotesArray().get(0).getNoteType());
		System.out.println("text 1: " + noteHolderDto.getInputNotesArray().get(0).getNoteText());
		System.out.println("text 2:  " + noteHolderDto3.getInputNotesArray().get(0).getNoteText());
		System.out.println(" ------------------- ");
		System.out.println("id:  " + noteHolderDto.getInputNotesArray().get(1).getInputNoteId());
		System.out.println("type: " + noteHolderDto.getInputNotesArray().get(1).getNoteType());
		System.out.println("text 1: " + noteHolderDto.getInputNotesArray().get(1).getNoteText());
		System.out.println("text 2:  " + noteHolderDto3.getInputNotesArray().get(1).getNoteText());		
		System.out.println(" ------------------- ");
		
		UnitTestUtils.stop(this);
	}
	
}
