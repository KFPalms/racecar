/*
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.helpers.ut;

import com.pdi.data.util.RequestStatus;
import com.pdi.data.v3.dto.EmailGroupDTO;
import com.pdi.data.v3.dto.EmailGroupListDTO;
import com.pdi.data.v3.dto.SessionUserDTO;
import com.pdi.data.v3.helpers.EmailGroupDataHelper;
import com.pdi.data.v3.util.V3DatabaseUtils;
import com.pdi.properties.PropertyLoader;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

public class EmailGroupDataHelperUT extends TestCase
{

	//
	// Constructors
	//

	public EmailGroupDataHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args) throws Exception
	{
		junit.textui.TestRunner.run(EmailGroupDataHelperUT.class);
	}

	/*
	 * testProperties
	 */
	public void testProperties()
		throws Exception
	{
		UnitTestUtils.start(this);

		//Does the local property work?
		String applicationId = PropertyLoader.getProperty("com.pdi.data.v3.application", "application.code");
		assertEquals(applicationId != null, true);
		assertEquals(applicationId.equals("PDI-DATA-V3"), true);

		UnitTestUtils.stop(this);
	}

	// When we add the other functionality, these methods will have to be fleshed out

	/*
	 * test Insert
	 */
	public void testInsertGroup()
	{
		UnitTestUtils.start(this);
		V3DatabaseUtils.setUnitTest(true);

//		// Check the insert
//		EmailRecipientTypeDTO ins = new EmailRecipientTypeDTO();
//		ins.setName("Unit Test type");
//		EmailRecipientTypeDTO insOut = EmailRecipientTypeDataHelper.addRecipientType(ins);
//		//assertEquals("addRecipientType status != OK",
//		//			 RequestStatus.RS_OK, insOut.getStatus().getStatusCode());
//		assertFalse("addRecipientType id not returned", (0 == insOut.getId()));
//		System.out.println("Insert succeeded; New ID = " + insOut.getId());
		
		UnitTestUtils.stop(this);
	}

	/*
	 * test get
	 */
	public void testGetGroup()
	{
		UnitTestUtils.start(this);
		V3DatabaseUtils.setUnitTest(true);

		//
//		// Now check the select for that item
//		EmailRecipientTypeDTO singleType = EmailRecipientTypeDataHelper.getRecipientType(insOut.getId());
//		assertEquals("getRecipientType status != OK",
//					 RequestStatus.RS_OK, singleType.getStatus().getStatusCode());
//		assertEquals("getRecipientType returned incorrect name",
//				ins.getName(), singleType.getName());
//		System.out.println("Get succeeded");
		
		UnitTestUtils.stop(this);
	}


	/*
	 * test getAllGroups
	 */
	public void testGetAllGroups()
	{
		UnitTestUtils.start(this);
		V3DatabaseUtils.setUnitTest(true);
    	
    	SessionUserDTO sessionUser = new SessionUserDTO();
    	sessionUser.setUsername(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.username")); 
    	sessionUser.setPassword(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.password"));

		EmailGroupListDTO glDto = EmailGroupDataHelper.getAllGroups(sessionUser);
		assertEquals("getAllGroups status != OK - returns " + glDto.getStatus().getStatusCode(),
					 RequestStatus.RS_OK, glDto.getStatus().getStatusCode());
		for (EmailGroupDTO group : glDto.getGroupList())
		{
			System.out.println(group.toString());
		}
	
		// Redo the following when the get of a single group is done
//		// Test the "Get All Types" cross-referenced to the get single group
//		EmailRecipientTypeListDTO tlDto = EmailRecipientTypeDataHelper.getAllRecipientTypes();
//		assertEquals("getAllRecipientTypes status != OK - returns " + tlDto.getStatus().getStatusCode(),
//					 RequestStatus.RS_OK, tlDto.getStatus().getStatusCode());
//
//		ArrayList<EmailRecipientTypeDTO> typeList = tlDto.getTypeList();
//		for (EmailRecipientTypeDTO type : typeList)
//		{
//			//	//Print out the recipient type
//			//	System.out.println(type.toString());
//
//			//For fun... let's ask the db to get the same thing AGAIN
//			singleType = null;
//			singleType = EmailRecipientTypeDataHelper.getRecipientType(type.getId());
//
//			//Are they the same object... well they should be!
//			assertEquals(singleType.getId(), type.getId());
//		}
//		System.out.println("Get All succeeded");

		UnitTestUtils.stop(this);
	}
	
	
	/*
	 * test update
	 */
	public void testUpdateGroup()
	{
		UnitTestUtils.start(this);
		V3DatabaseUtils.setUnitTest(true);
	//
	//	// Check the update on that item
	//	EmailRecipientTypeDTO updt = new EmailRecipientTypeDTO();
	//	updt.setId(insOut.getId());
	//	updt.setName("Unit Test type (updated-" + insOut.getId() + ")");
	//	RequestStatus updtOut = EmailRecipientTypeDataHelper.updateRecipientType(updt);
	//	assertEquals("updateRecipientType status != OK.  msg=" + updtOut.getExceptionMessage() + ".  ",
	//				 RequestStatus.RS_OK, updtOut.getStatusCode());
	//	EmailRecipientTypeDTO updtChk = EmailRecipientTypeDataHelper.getRecipientType(insOut.getId());
	//	assertEquals("updateRecipientType check (get) status != OK.  msg=" + updtChk.getStatus().getExceptionMessage(),
	//				 RequestStatus.RS_OK, updtChk.getStatus().getStatusCode());
	//	assertEquals("getRecipientType returned incorrect name",
	//				 updt.getName(), updtChk.getName());
	//	System.out.println("update succeeded");
		
		UnitTestUtils.stop(this);
	}
	
	
	/*
	 * test delete
	 */
	public void testDeleteGroup()
	{
		UnitTestUtils.start(this);
		V3DatabaseUtils.setUnitTest(true);

	//	//Check that the item deletes
	//	RequestStatus delOut = EmailRecipientTypeDataHelper.deleteRecipientType(insOut.getId());
	//	assertEquals("deleteRecipientType status != OK.  msg=" + delOut.getExceptionMessage() + ".  ",
	//				 RequestStatus.RS_OK, delOut.getStatusCode());
	//	EmailRecipientTypeDTO delChk = EmailRecipientTypeDataHelper.getRecipientType(insOut.getId());
	//	assertEquals("deleteRecipientType check status != OK.  msg=" + delChk.getStatus().getExceptionMessage(),
	//				 RequestStatus.DBS_NO_DATA, delChk.getStatus().getStatusCode());
	//	System.out.println("delete succeeded");
	//
	//	// Actual test code ends here
		
		UnitTestUtils.stop(this);
	}
}