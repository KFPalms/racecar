package com.pdi.data.abyd;

public class ApplicationData {
	private static final ApplicationData singleton = new ApplicationData();
	
	private static ApplicationData getInstance() {
		return singleton;
	}
	public static void setId(String id) {
		ApplicationData.getInstance().id = id;
	}
	public static String getId() {
		return ApplicationData.getInstance().id;
	}
	private String id = "ABYD";
}
