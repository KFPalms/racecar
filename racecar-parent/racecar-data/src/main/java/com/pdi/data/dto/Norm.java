/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.data.dto;



/**
 * Norm is a basic data class that contains norm data.
 * 
 * Note that most of the actual calculations occur in a related utils class.
 * 
 * @author		Gavin Myers
 */
public class Norm extends CoreDataObject
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	/**
	 * 
	 */
	private static final long serialVersionUID = 1931774212363194816L;
	//
	// Instance data.
	//
	private int id;
	private int normGroupId;
	private double mean;
	private double standardDeviation;
	private String spssValue;	

	
	//
	// Constructors.
	//
	
	/*
	 * No-parameter constructor
	 */
	public Norm()
	{
		// does nothing right now
	}
	
	/*
	 * 2 parameter constructor
	 */
	public Norm(double meanIn, double stDevIn)
	{
		mean = meanIn;
		standardDeviation = stDevIn;
	}

	
	public String toString()
	{
		String ret = "Norm - ID=" + id;
		ret += ",    mean=" + mean + ", stdev=" + standardDeviation;
		
		return ret;
	}
	
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public void setId(int value)
	{
		id = value;
	}
	
	public int getId()
	{
		return id;
	}

	/*****************************************************************************************/
	public void setMean(double value)
	{
		mean = value;
	}
	
	public double getMean()
	{
		return mean;
	}

	/*****************************************************************************************/
	public void setStandardDeviation(double value)
	{
		standardDeviation = value;
	}
	
	public double getStandardDeviation()
	{
		return standardDeviation;
	}

	/*****************************************************************************************/
	public void setSpssValue(String value)
	{
		spssValue = value;
	}
	
	public String getSpssValue()
	{
		return spssValue;
	}

	public int getNormGroupId() {
		return normGroupId;
	}

	public void setNormGroupId(int normGroupId) {
		this.normGroupId = normGroupId;
	}

}

