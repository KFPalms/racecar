/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.dto.intGrid;


/**
 * IGExtSPData is a container for Success Profile category name/entered text data used in the IG Extract.
 * 
 * @author		Ken Beukelman
 */
public class IGExtSPData
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _spName;
	private String _spText;

	//
	// Constructors.
	//
	public IGExtSPData()
	{
		// does nothing currently
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public String getSpName()
	{
		return _spName;
	}
	
	public void setSpName(String value)
	{
		_spName = value;
	}	

	/*****************************************************************************************/
	public String getSpText()
	{
		return _spText;
	}
	
	public void setSpText(String value)
	{
		_spText = value;
	}	



	/*
	 * Show me what you've got
	 */
	public String toString()
	{
		String ret = "IGExtSPData content (Java)\n";
		ret += "  SP Name=" + _spName + "\n";
		ret += "  SP Text=" + _spText + "\n";

		return ret;
	}
}
