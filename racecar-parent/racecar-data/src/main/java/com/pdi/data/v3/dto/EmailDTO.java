/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.dto;

import java.util.HashMap;

import com.pdi.data.util.BaseDTO;
import com.pdi.data.util.RequestStatus;


/**
 * EmailDTO is a class designed to convey the information for one
 * e-mail object and its request status used in the PDI V3 extensions.
 * 
 * @author		Ken Beukelman
 * @author 		MB Panichi
 * 
  */
public class EmailDTO  extends BaseDTO 
{

	//
	// Static data.
	//


	//
	// Static methods.
	//


	//
	// Instance data.
	//
	private long _emailId;
	private String _displayName = null;
	private String _description = null;
	private long _recipientTypeId;
	private String _recipientTypeName;
	private HashMap<String, EmailTextDTO> _textMap; // key = ISO Code
	
	private long _groupId;
	private String _groupName;
	
	private String _clientId;
	private long _productId;


	//
	// Constructors.
	//
	public EmailDTO()
	{
		super();
		// does nothing right now
	}
	
	// Status setup constructor - email data remains unset
	public EmailDTO(RequestStatus stat)
	{
		super();
		this.setStatus(stat);
	}
	
	
	//
	// Instance methods.
	//

	/**
	 * Generic toString method
	 */
	public String toString() {
		String str = "EmailDTO - " +
					 "Status code=" + this.getStatus().getStatusCode() +
					 ", id=" + this.getEmailId() +
					 ", name=>" + this.getDisplayName() + "<";
		return str;
	}
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public long getEmailId()
	{
		return _emailId;
	}
	
	public void setEmailId(long value)
	{
		_emailId = value;
	}	
//	
//	/*****************************************************************************************/
//	public long getBaselineLangId()
//	{
//		return _baselineLangId;
//	}
//	
//	public void setBaselineLangId(long value)
//	{
//		_baselineLangId = value;
//	}
	
	/*****************************************************************************************/
	public String getDisplayName()
	{
		return _displayName;
	}
	
	public void setDisplayName(String value)
	{
		_displayName = value;
	}
	
	/*****************************************************************************************/
	public String getDescription()
	{
		return _description;
	}
	
	public void setDescription(String value)
	{
		_description = value;
	}
	
	/*****************************************************************************************/
	public long getRecipientTypeId()
	{
		return _recipientTypeId;
	}
	
	public void setRecipientTypeId(long value)
	{
		_recipientTypeId = value;
	}

	/*****************************************************************************************/
	public String getRecipientTypeName()
	{
		return _recipientTypeName;
	}
	
	public void setRecipientTypeName(String value)
	{
		_recipientTypeName = value;
	}	
	
	/*****************************************************************************************/
	public HashMap<String, EmailTextDTO> getTextMap()
	{
		if(_textMap == null)
		{
			_textMap = new HashMap<String, EmailTextDTO>();
		}
		return _textMap;
	}
	
	public void setTextMap(HashMap<String, EmailTextDTO> value)
	{
		_textMap = value;
	}		
	
	/*****************************************************************************************/
	public long getGroupId()
	{
		return _groupId;
	}
	
	public void setGroupId(long value)
	{
		_groupId = value;
	}

	/*****************************************************************************************/
	public String getGroupName()
	{
		return _groupName;
	}
	
	public void setGroupName(String value)
	{
		_groupName = value;
	}	

	/*****************************************************************************************/
	public String getClientId()
	{
		return _clientId;
	}
	
	public void setClientId(String value)
	{
		_clientId = value;
	}	
	
	/*****************************************************************************************/
	public long getProductId()
	{
		return _productId;
	}
	
	public void setProductId(long value)
	{
		_productId = value;
	}
}
