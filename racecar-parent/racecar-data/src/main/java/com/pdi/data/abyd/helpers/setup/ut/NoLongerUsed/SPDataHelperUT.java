package com.pdi.data.abyd.helpers.setup.ut.NoLongerUsed;

import java.sql.Connection;

import com.pdi.data.abyd.dto.common.KeyValuePair;
import com.pdi.data.abyd.dto.common.CompElementDTO;
import com.pdi.data.abyd.dto.setup.NoLongerUsed.SPFullDataDTO;
import com.pdi.data.abyd.helpers.setup.NoLongerUsed.SPDataHelper;
import com.pdi.ut.UnitTestUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;

import junit.framework.TestCase;

public class SPDataHelperUT extends TestCase
{
	//
	// Constructors
	//

	public SPDataHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
		throws Exception
    {
		junit.textui.TestRunner.run(SPDataHelperUT.class);
    }


	/*
	 * testGetAllSPData
	 */
	public void testGetAllSPData()
		throws Exception
	{
		System.out.println("SPDataHelperUT:");
		
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);

		Connection con = AbyDDatabaseUtils.getDBConnection();
		long dnaId = 18;
			
		System.out.println("    DNA ID=" + dnaId);

		SPDataHelper helper = new SPDataHelper(con, dnaId);
		SPFullDataDTO dto = helper.getAllSPData();
			
		System.out.println("    Output:");
		System.out.println("      Competencies:");
		for (KeyValuePair kvp : dto.getCompList())
		{
			System.out.println("        " + kvp.getTheKey() + " - " + kvp.getTheValue());
		}
		System.out.println("      Comp elements:");
		for (CompElementDTO elt : dto.getCompElts())
		{
			System.out.println("        ID=" + elt.getCompId() + ", essential=" + elt.getIsEssential());
		}
		System.out.println("      Text elts:");
		for (KeyValuePair kvp : dto.getTextElts())
		{
			System.out.println("        " + kvp.getTheKey() + " - " + kvp.getTheValue());
		}

		UnitTestUtils.stop(this);
	}
}
