package com.pdi.data.v2.ut;

//import java.util.Iterator;
//import java.util.Set;

import junit.framework.TestCase;

import com.pdi.data.v2.helpers.ParticipantDataHelper;
import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.GroupReport;
import com.pdi.reporting.report.IndividualReport;
//import com.pdi.reporting.report.ReportData;
import com.pdi.reporting.request.ReportingRequest;
import com.pdi.reporting.response.ReportingResponse;
import com.pdi.ut.UnitTestUtils;

public class TLTDataGetterUT extends TestCase
{

	//
	// Constructor
	//
	public TLTDataGetterUT(String name)
	{
		super(name);
	} 


	//
	// Test Main
	//
	public static void main(String[] args)
		throws Exception
	{
		junit.textui.TestRunner.run(TLTDataGetterUT.class);
	}

	//
	// Tests
	//
	
	/*
	 * testGroovy
	 */
	public void testGroovy()
	{
		UnitTestUtils.start(this);
		
		UnitTestUtils.stop(this);
	}

	/*
	 * testTLT get
	 */
	public void testTltGet()
		throws Exception
	{
    	UnitTestUtils.start(this);
		V2DatabaseUtils.setUnitTest(true);

    	
    	// Cert
    	String partId = "LLCQIBLV";
    	int targetLvl = 8;
    	
    	// Get the score data
    	ParticipantDataHelper participantDataHelper = new ParticipantDataHelper();
    	IndividualReport ir = participantDataHelper.getIndividualReportScoreData(partId);
    	//System.out.println("Score Data:\n" + ir.toString());

    	// Norm data
		String trans;
		switch (targetLvl)
		{
			case 7:		// MLL
				trans = "2";
				break;
			case 8:		// EXEC/Sr. Exec/BUL
				trans = "4";
				break;
			case 9:		// SEA
				trans = "10";
				break;
			default:
				throw new Exception("Invalid transition level (" + targetLvl + ")");
		}
		ir.getDisplayData().put("TRANSITION_LEVEL", trans);
		ir.getDisplayData().put("TARGET_LEVEL", targetLvl + "");
		ir.getDisplayData().put(ReportingConstants.RC_COGNITIVES_INCLUDED, "2");  // Include the Raven's
		ir.getDisplayData().put("LAST_NAME", "");
		ir.getDisplayData().put("FIRST_NAME", "");
			 
		// Get the norms
		
		participantDataHelper.getIndivTransNormData(ir);
    	//System.out.println("Score Plus norm data:\n" + ir.toString());
	
		// Calculate the needed values.  We "generate" the report data for the
		// TLT_GROUP_DETAIL_REPORT, then extract the information needed
		GroupReport gr = new GroupReport();
		gr.getIndividualReports().add(ir);
		gr.setReportCode(ReportingConstants.REPORT_CODE_TLT_GROUP_DETAIL);
		gr.setReportType(ReportingConstants.REPORT_TYPE_GROUP_DATA);
		gr.setTargetTransitionLevel(targetLvl);
	
		ReportingRequest request = new ReportingRequest();
		request.addReport(gr);
		ReportingResponse response = request.generateReports();
		
		GroupReport resp = (GroupReport)response.getReports().get(0);
		IndividualReport out = resp.getIndividualReports().get(0);
    	System.out.println("TLT results:\n" + out.toString());

    	UnitTestUtils.stop(this);
	}

}
