/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.reportInput;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;

import com.pdi.data.abyd.dto.reportInput.ReportInputNoteDTO;
import com.pdi.data.abyd.dto.reportInput.ReportInputNoteHolderDTO;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.util.RequestStatus;
import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.xml.XMLUtils;

/*
 * ReportInputNoteDataHelper
 */
public class ReportInputNoteDataHelper
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	
	//
	// Constructors.
	//

	//
	// Instance methods.
	//

	/**
	 * 
	 * 
	 * @param  long reportInputId
	 * @return ArrayList<ReportInputNoteDTO>
	 * @throws Exception
	 */
	public ReportInputNoteHolderDTO getReportInputNotes(long reportInputId) 
		throws Exception 
	{
		ReportInputNoteHolderDTO noteHolder = new ReportInputNoteHolderDTO();
		//noteHolder.setInputNotesArray(new ArrayList<ReportInputNoteDTO>());
		ArrayList<ReportInputNoteDTO> notesArray = new ArrayList<ReportInputNoteDTO>();
		noteHolder.setInputNotesArray(notesArray);
		noteHolder.setReportInputId(reportInputId);
		
		if (reportInputId == 0)
		{
			// 0 is an invalid input (it means that the DRI data doesn't yet exist)
			// just return the DTO with the empty list at this point
			// This should no longer happen
			System.out.println("INCONSISTENT DATA! Fetching notes, the ReportInputID = 0");
			return noteHolder;
		}
		
		//ArrayList<ReportInputNoteDTO> notesArray = new ArrayList<ReportInputNoteDTO>();
		//noteHolder.setInputNotesArray(notesArray);
		
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append(" SELECT  0, reportNote, '3' FROM pdi_abd_dna p, pdi_abd_rpt_input r where p.dnaid = r.dnaid and r.inputId = ? ");

		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);

		dlps.getPreparedStatement().setLong(1,reportInputId);
		
		if (dlps.isInError())
		{
			return noteHolder;
		}
		
		// Go get the rest of thedata
		DataResult dr = null;
		ResultSet rs  = null;
		try
		{
			dr =  V2DatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			if (rs != null && rs.isBeforeFirst())
			{
				while (rs.next())
				{
					ReportInputNoteDTO dto = new ReportInputNoteDTO();
					dto.setInputNoteId(rs.getInt(1));
					dto.setNoteText(XMLUtils.xmlEscapeString(rs.getString(2)));
					dto.setNoteType(rs.getInt(3));
					
					notesArray.add(dto);
				}
			}
		} finally {
	        if (dr != null) { dr.close(); dr = null; }
		}
		try {		
			dr = null;
			
			sqlQuery = new StringBuffer();
			sqlQuery.append(" SELECT inputNoteId, noteText, noteType  ");
			sqlQuery.append(" FROM pdi_abd_rpt_input_notes  ");
			sqlQuery.append(" WHERE inputId =  ? ");
	
			dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
			
			dlps.getPreparedStatement().setLong(1,reportInputId);
			
			dr =  V2DatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			
			if (rs == null || ! rs.isBeforeFirst())
			{
				// No data... return
				return noteHolder;
			}
			
			while (rs.next())
			{
				ReportInputNoteDTO dto = new ReportInputNoteDTO();
				dto.setInputNoteId(rs.getInt(1));
				dto.setNoteText(XMLUtils.xmlEscapeString(rs.getString(2)));
				dto.setNoteType(rs.getInt(3));
				
				notesArray.add(dto);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (dr != null) { dr.close(); dr = null; }
		}
		return noteHolder;
	}

	
	/**
	 * addReportInputNotes 
	 * Insert or update a note into pdi_abd_rpt_input_notes
	 * 
	 * @param  Connection - the database connection
	 * @param  ReportInputNoteHolderDTO holderDto - holds the report notes
	 * @throws Exception
	 */
	
	public void addReportInputNotes(Connection con, ReportInputNoteHolderDTO holderDto) 
		throws Exception 
	{
		
		ArrayList<ReportInputNoteDTO> notesArray = holderDto.getInputNotesArray();
		
	    String userId = "rptInptNtHlpr";

		Statement stmt = null;
		ResultSet rs = null;
				
		try
		{			
			// iterate through the dto's -- we need to look at each one... 
			Iterator<ReportInputNoteDTO> it = notesArray.iterator();
			while(it.hasNext())
			{
				 ReportInputNoteDTO dto = new ReportInputNoteDTO();
				 dto = it.next();
				 
				 if(dto.getNoteType() == 3)
				 {
					 //this is a project note from the other table... 
					 //don't add it to this one.... 
					 // skip it and keep going.... 
					 continue;
				 }
				 
				 // look at the dto... if it's a new one, it's not
				 // going to have an inputNoteId, because we need to
				 // get that from the database... 
				 boolean insert = false;
				 if(dto.getInputNoteId() == 0)
				 {
					 insert = true;					
				 }
				 else
				 {
					 // see if there exists a row in the 
					 // pdi_abd_rpt_input_notes table... 
					 // i'm leaving this check here, because I'm
					 // betting that they're going to allow updates
					 // to previous notes, even if they said they're 
					 // not right now... 
					
					 // select sql:
						StringBuffer sqlQuery = new StringBuffer();
						sqlQuery.append("  SELECT noteText   ");
						sqlQuery.append("  FROM pdi_abd_rpt_input_notes  ");
						sqlQuery.append("  WHERE inputId =  " + holderDto.getReportInputId() + "  ");
						sqlQuery.append("  AND inputNoteId = " + dto.getInputNoteId() + "  ");
						//System.out.println("sqlQuery = " + sqlQuery.toString());
						
						// run the query
						stmt = con.createStatement();
						rs = stmt.executeQuery(sqlQuery.toString());
				 }

				 // Clean up note text
				 dto.setNoteText(dto.getNoteText().replace("'","''"));
				 // if the row DOES NOT EXIST, 
				 if ((insert || !rs.isBeforeFirst()) && (dto.getNoteText().length() > 0)) 
				 {
					 //...we will need to INSERT IT... 					
					DataResult dr = null;
					try
					{
						StringBuffer sqlInsert = new StringBuffer();
						sqlInsert.append("INSERT into pdi_abd_rpt_input_notes ");
						sqlInsert.append("  (  inputId, noteText, noteType, lastUserId, lastDate ) VALUES( ");
						sqlInsert.append("  ?,  ");
						sqlInsert.append("  ?,  ");
						sqlInsert.append("  ?,  ");
						sqlInsert.append("  ?,  ");
						sqlInsert.append("  GETDATE()  ");							
						sqlInsert.append("   )  ");
							
						DataLayerPreparedStatement dlps = AbyDDatabaseUtils.prepareStatement(sqlInsert);
							
						dlps.getPreparedStatement().setLong(1,holderDto.getReportInputId());
						dlps.getPreparedStatement().setString(2, dto.getNoteText());
						dlps.getPreparedStatement().setInt(3, dto.getNoteType());
						dlps.getPreparedStatement().setString(4, userId);
						// OK.  We make a new one each time
						dr = AbyDDatabaseUtils.insert(dlps);
					}
					finally
					{
						if (dr != null) {dr.close(); dr = null; }
					}
		       	} else if (dto.getNoteText().length() > 0){	
		       		//... we just need to UPDATE IT....
		       		
		       		//System.out.println(dto.getReportOptionsId() + " CONTINUE OR UPDATE:  if (rs != null )....");
		       		// need to see what the noteText value is from the select at the top of the main loop.... 
		       		rs.next(); 	       		
		       		
					// get the database value
					String databaseText = rs.getString(1);
					
					// get the incoming dto value
					String incomingText = dto.getNoteText();
					
					// AND the option_value is DIFFERENT from the incoming value
					if(databaseText.equals(incomingText))
					{
						// they're the same... do nothing
						//System.out.println(dto.getReportOptionsId() + " CONTINUE: databaseValue == dtoValue : " + databaseValue + " == " + dtoValue);
						continue;
					} else {
						incomingText = incomingText.replace("'", "''");
						// UPDATE the value in the database
						//System.out.println(dto.getReportOptionsId() + " UPDATE: databaseValue == dtoValue : " + databaseValue + " == " + dtoValue);
							
						DataResult dr = null;
						try
						{
							StringBuffer sqlUpdate = new StringBuffer();
							sqlUpdate.append("UPDATE pdi_abd_rpt_input_notes ");
							sqlUpdate.append("  SET noteText = ?, ");
							sqlUpdate.append("  lastDate = GETDATE(),  ");
							sqlUpdate.append("  lastUserId = ?  ");
							sqlUpdate.append("  WHERE inputNoteId = ?  ");
	
							DataLayerPreparedStatement dlps = AbyDDatabaseUtils.prepareStatement(sqlUpdate);
								
							dlps.getPreparedStatement().setString(1,incomingText);
							dlps.getPreparedStatement().setString(2, userId);
							dlps.getPreparedStatement().setLong(3,dto.getInputNoteId());
							//AbyDDatabaseUtils.update(dlps).close();	// OK we do a new one each loop
							dr = AbyDDatabaseUtils.update(dlps);
							if (dr.getStatus().getStatusCode() != RequestStatus.RS_OK)
							{
								System.out.println("Error updating notes.  msg=" + dr.getStatus().getExceptionMessage());
							}
						}
						finally
						{
							// OK we do a new one each loop
							if (dr != null) {dr.close(); dr = null; }	
						}
					}
		       	}
			 }// end main while iterator
		}
	    catch (SQLException ex)
		{
	        // handle any errors
	    	throw new Exception("addReportInputNotes:  SQL updating V2 report options data.  " +
	    			"SQLException: " + ex.getMessage() + ", " +
	    			"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
	    }
	    finally
		{
	        if (rs != null)
	        {
	            try
				{
	                rs.close();
	            }
	            catch (SQLException sqlEx)
				{
	            	// swallow the error
	            }
	            rs = null;
	        }
	        if (stmt != null)
	        {
	            try
				{
	                stmt.close();
	            }
	            catch (SQLException sqlEx)
				{
	            	// swallow the error
				}
	            stmt = null;
	        }
		}
	}
}
