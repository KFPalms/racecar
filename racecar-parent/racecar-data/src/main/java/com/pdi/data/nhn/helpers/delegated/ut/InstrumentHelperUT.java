package com.pdi.data.nhn.helpers.delegated.ut;

//import java.util.ArrayList;

import com.pdi.data.helpers.interfaces.IInstrumentHelper;

import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.nhn.util.NhnDatabaseUtils;

import com.pdi.data.dto.Instrument;
import com.pdi.data.dto.SessionUser;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

public class InstrumentHelperUT extends TestCase {

	public InstrumentHelperUT(String name) 	{
		super(name);
	}
	
	public void testGetInstrument() throws Exception {
		UnitTestUtils.start(this);
		NhnDatabaseUtils.setUnitTest(true);
		
		SessionUser session = new SessionUser();
		
		IInstrumentHelper iInstrument = HelperDelegate.getInstrumentHelper();
//		ArrayList<Instrument> instruments = iInstrument.all(session);
//		for(Instrument instrument : instruments) {
//			System.out.println(instrument.getId() + " - " + instrument.getName());
//		}

		String instrumentId = "RV";  //  Ravens instrument/course abbv          
		Instrument singleInstrument = iInstrument.fromId(session, instrumentId);
		
		try{
			System.out.println("singleInstrument.getId = " + singleInstrument.getCode());
			System.out.println("singleInstrument.getDescription = " + singleInstrument.getDescription());
			System.out.println("singleInstrument.getMetadata = " + singleInstrument.getMetadata().toString());
		}catch (Exception e) {
			System.out.println("instrumentId " + instrumentId + " did not return a valid instrument.");
		}

	}
	
	
}
