package com.pdi.data.loaders;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/*
 * Update dates from HRA to DATETIME format
 */
public class LastUserUpdater
{
	// Static constants
	// Gonna hard code the params as this is throw-away code
    protected static final String DRIVER = "com.mysql.jdbc.Driver";
    protected static final String URL = "jdbc:mysql://mspdbdev2:3306/pdi_certify";
    protected static final String UID = "kbeukelm";
    protected static final String PW = "mysql";

//    // Instance variables
//    private static int readCnt = 0;
//    private static int writeCnt = 0;
    
    /*
	 * Runs as a headless app
	 */
	public static void main(String args[])
	{
		String phase = null;
		Connection con = null;
		int readCnt = 0;
		int writeCnt = 0;
		PreparedStatement readStmt = null;
		PreparedStatement writeStmt = null;
		LinkedHashMap<String, String> names = new LinkedHashMap<String, String>();

		try
		{
			// Open the DB
			phase = "DB Open";
			con = openDb(URL, UID, PW);
			System.out.println("Database opened.  url: " + URL);
		
			// Set up the two prepared statements
			// Two calls 'cause you can't return multiple objects (without a List or a Map)
			phase = "Read PreparedStatement setup";
			readStmt = prepareReadStmt(con);
			phase = "Write PreparedStatement setup";
			writeStmt = prepareWriteStmt(con);

			//Load the data
			phase= "Loading User data";
			readCnt = loadUsers(con, readStmt, names);

			// Update the users
			phase = "Updating users";
			writeCnt = updateUsers(con, names, writeStmt);
		}
		catch (Exception e)
		{
			System.out.println("ERROR Detected in " + phase + " phase...");
			e.printStackTrace();
			return;
		}
		finally
		{
			if (readStmt != null)
			{
				try
				{
					readStmt.close();
				}
				catch (Exception e) {  }
			}
			if (writeStmt != null)
			{
				try
				{
					writeStmt.close();
				}
				catch (Exception e) {  }
			}
			if (con != null)
			{
				try
				{
					con.close();
				}
				catch (Exception e)  {  }
			}
		}
		
		System.out.println("Done:  read " + readCnt + " rows, " +
				           "converted " + names.size() + " rows, " +
				           "wrote " + writeCnt + " rows.");
	}

	
	/*
	 * 
	 */
	private static Connection openDb(String url, String uid, String pw)
		throws Exception
	{
		Connection con = null;

		Class.forName(DRIVER);

		con = DriverManager.getConnection(url, uid, pw);
		//////con.setAutoCommit(false);
			
		return con;
	}


	/*
	 * Prepare the read stmt
	 */
	private static PreparedStatement prepareReadStmt(Connection con)
		throws Exception
	{
		StringBuffer q = new StringBuffer();
		q.append("SELECT rr.scoreId, ");
		q.append("       rr.CreatedByStamp, ");
		q.append("       rr.ModifiedByStamp ");
		q.append("  FROM results rr");
		q.append("  ORDER BY rr.scoreId");
		return con.prepareStatement(q.toString());
	}


	/*
	 * Prepare the write stmt
	 */
	private static PreparedStatement prepareWriteStmt(Connection con)
		throws Exception
	{
		StringBuffer q = new StringBuffer();
		q.append("UPDATE results ");
		q.append("  SET lastUser = ? ");
		q.append("  WHERE scoreId = ?");
		return con.prepareStatement(q.toString());
	}


	/*
	 * Get the appropriate user data
	 */
	private static int loadUsers(Connection con,
			 PreparedStatement read,
			 LinkedHashMap<String, String> users)
		throws Exception
	{
		int cnt = 0;

		// Read and save 'em
		ResultSet rs = read.executeQuery();
		//int dbcnt = 0;
		//while (rs.next() && dbcnt < 10)
		while (rs.next())
		{
			//dbcnt++;
			cnt++;
			String id = rs.getString(1);
			String name = rs.getString(3);
			if (name == null || name.length() < 1)
			{
				name = rs.getString(2);
			}
			if (name == null || name.length() < 1)
			{
				System.out.println("ID " + id + " has no valid name");
				continue;
			}

			users.put(id, name);
		}

		return cnt;
	}


	/*
	 * 	Load the user data into the results table
	 */
	private static int updateUsers(Connection con,
								   LinkedHashMap<String, String> users,
								   PreparedStatement write)
		throws Exception
	{
		int cnt = 0;
		Map.Entry<String, String> ent = null;

		// Loop through the users
		System.out.println("Preparing to update " + users.size() + " User entries...");
		for (Iterator<Map.Entry<String, String>> itr=users.entrySet().iterator(); itr.hasNext(); )
		{
			ent = itr.next();
			
			String id = ent.getKey();
			String user = ent.getValue();
			if (id == null || id.length() < 1)
			{
				System.out.println("Bad Key:  null.  User=" + user);
			}
			if (user == null || user.length() < 1)
			{
				System.out.println("Bad Date:  null.  ID=" + id);
			}

			write.setString(1, user);
			write.setString(2, id);

			write.executeUpdate();
			cnt++;
		}
			
		return cnt;
	}
}
