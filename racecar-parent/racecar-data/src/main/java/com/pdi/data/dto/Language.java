/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.data.dto;

public class Language {
	public static final String US_ENGLISH_ID = "1";

	private String id;
	private String code;
	private String englishName;
	private String translatedName;
	
	// Constructor
	public Language()
	{
		// No parm constructor does nothing
	}
	
	// Multi parm constructor
	public Language(String idIn, String codeIn, String nameIn, String transNameIn)
	{
		id = idIn;
		code = codeIn;
		englishName = nameIn;
		translatedName = transNameIn;
	}
	
	public void setCode(String code) {
		this.code = code.trim();
	}
	public String getCode() {
		return code;
	}
	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}
	public String getEnglishName() {
		return englishName;
	}
	public void setTranslatedName(String translatedName) {
		this.translatedName = translatedName;
	}
	public String getTranslatedName() {
		return translatedName;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getId() {
		return id;
	}
	
	public String toString()
	{
		String str = "Language:  ";
		str += "id=" + id + " ";
		str += ", code=" + code + " ";
		str += ", engName=" + englishName + " ";
		str += ", transName=" + translatedName + " ";
		
		return str;
	}
}
