/**
 * Copyright (c) 2008 Personnel Decisions International, Inc.
 * All rights reserved.
 *
 * $Header: com/pdi/data/abyd/dto/setup/Setup2ReportLabelDTO.java 6     8/25/05 3:23p mpanichi $
 */
package com.pdi.data.abyd.dto.setup;

import java.util.ArrayList;

/**
 * Setup2ReportLabelDTO is a wrapper around a number of data items that are needed when 
 * part 2 of the Design Setup app is initially started.
 * 
 * @author		Ken Beukelman
 * @author      MB Panichi
 */
public class Setup2ReportLabelsDTO
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//

	private long _dnaId = 0;
    private ArrayList<Setup2ReportLabelValueDTO> _labelValuesArray;
    private Setup2ReportLabelValueDTO _leadershipLegend;
    private Setup2ReportLabelValueDTO _tltLegend;
    private Setup2ReportLabelValueDTO _coverPage;
	
	//
	// Constructors.
	//
	public Setup2ReportLabelsDTO()
	{
		// Does nothing presently

		
	}

	//
	// Instance methods.
	//
	
	public String toString()
	{
		String ret = "Setup2ReportLabelsDTO:\n";
		
		ret += "DNA ID=" + _dnaId + "\n";
		ret += "Label Array contains " + _labelValuesArray.size() + " entries --\n";
		for (Setup2ReportLabelValueDTO ent : _labelValuesArray)
		{
			ret += ent.toString() + "\n";
		}
		ret += "Leadership Legend:"  + (_leadershipLegend == null ? "  null" : "\n" + _leadershipLegend.toString()) + "\n";
		ret += "TLT Legend:" + (_tltLegend == null ? "  null" : "\n" + _tltLegend.toString()) + "\n";
		ret += "Cover Page:"  + ( _coverPage == null ? "  null" : "\n" + _coverPage.toString()) + "\n";
		
		return ret;
	}
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//


	/*****************************************************************************************/
	public long getDnaId()
	{
		return _dnaId;
	}
		
	public void setDnaId(long value)
	{
		_dnaId = value;
	}	

	/*****************************************************************************************/
	public ArrayList<Setup2ReportLabelValueDTO> getLabelValuesArray()
	{
		if(_labelValuesArray == null){
			_labelValuesArray = new ArrayList<Setup2ReportLabelValueDTO>();
		}
		
		return _labelValuesArray;
	}
		
	public void setLabelValuesArray(ArrayList<Setup2ReportLabelValueDTO> value)
	{
		_labelValuesArray = value;
	}	

	/*****************************************************************************************/
	public Setup2ReportLabelValueDTO getLeadershipLegend()
	{
		
		return _leadershipLegend;
	}
		
	public void setLeadershipLegend(Setup2ReportLabelValueDTO value)
	{
		_leadershipLegend = value;
	}
	/*****************************************************************************************/
	public Setup2ReportLabelValueDTO getTltLegend()
	{

		
		return _tltLegend;
	}
		
	public void setTltLegend(Setup2ReportLabelValueDTO value)
	{
		_tltLegend = value;
	}
	/*****************************************************************************************/
	public Setup2ReportLabelValueDTO getCoverPage()
	{

		
		return _coverPage;
	}
		
	public void setCoverPage(Setup2ReportLabelValueDTO value)
	{
		_coverPage = value;
	}
}