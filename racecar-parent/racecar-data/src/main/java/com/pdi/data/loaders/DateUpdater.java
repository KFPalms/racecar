package com.pdi.data.loaders;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TimeZone;

/*
 * Update dates from HRA to DATETIME format
 * NOT critical to production release
 */
public class DateUpdater
{
	// Static constants
	// Gonna hard code the params as this is throw-away code
    protected static final String DRIVER = "com.mysql.jdbc.Driver";
    protected static final String URL = "jdbc:mysql://mspdbdev2:3306/pdi_certify";
    protected static final String UID = "kbeukelm";
    protected static final String PW = "mysql";

//    // Instance variables
//    private static int readCnt = 0;
//    private static int writeCnt = 0;
    
    /*
	 * Runs as a headless app
	 */
	public static void main(String args[])
	{
		String phase = null;
		Connection con = null;
		int readCnt = 0;
		int writeCnt = 0;
		PreparedStatement readStmt = null;
		PreparedStatement writeStmt = null;
		LinkedHashMap<String, String> dates = new LinkedHashMap<String, String>();

		try
		{
			// Open the DB
			phase = "DB Open";
			con = openDb(URL, UID, PW);

			System.out.println("Database opened.  url: " + URL);
		
			// Set up the two prepared statements
			// Two calls 'cause you can't return multiple objects (without a List or a Map)
			phase = "Read PreparedStatement setup";
			readStmt = prepareReadStmt(con);
			phase = "Write PreparedStatement setup";
			writeStmt = prepareWriteStmt(con);

			//Load the data
			phase= "Loading Date data";
			readCnt = loadDates(con, readStmt, dates);

			// Update the dates
			phase = "Updating dates";
			writeCnt = updateDates(con, dates, writeStmt);
		}
		catch (Exception e)
		{
			System.out.println("ERROR Detected in " + phase + " phase...");
			e.printStackTrace();
			return;
		}
		finally
		{
			if (readStmt != null)
			{
				try
				{
					readStmt.close();
				}
				catch (Exception e) {  }
			}
			if (writeStmt != null)
			{
				try
				{
					writeStmt.close();
				}
				catch (Exception e) {  }
			}
			if (con != null)
			{
				try
				{
					con.close();
				}
				catch (Exception e)  {  }
			}
		}
		
		//System.out.println("Done:  Wrote " + NORM_DATA.length + " norms in " + NORM_GROUP_DATA.length + " norm groups.");
		System.out.println("Done:  read " + readCnt + " rows, " +
				           "converted " + dates.size() + " rows, " +
				           "wrote " + writeCnt + " rows.");
	}

	
	/*
	 * 
	 */
	private static Connection openDb(String url, String uid, String pw)
		throws Exception
	{
		Connection con = null;

		Class.forName(DRIVER);

		con = DriverManager.getConnection(url, uid, pw);
		//////con.setAutoCommit(false);
			
		return con;
	}


	/*
	 * Prepare the read stmt
	 */
	private static PreparedStatement prepareReadStmt(Connection con)
		throws Exception
	{
		StringBuffer q = new StringBuffer();
		q.append("SELECT rr.scoreId, ");
		q.append("       rr.DateCreatedStamp, ");
		q.append("       rr.DateModifiedStamp ");
		q.append("  FROM results rr");
		q.append("  ORDER BY rr.scoreId");
		return con.prepareStatement(q.toString());
	}


	/*
	 * Prepare the write stmt
	 */
	private static PreparedStatement prepareWriteStmt(Connection con)
		throws Exception
	{
		StringBuffer q = new StringBuffer();
		q.append("UPDATE results ");
		q.append("  SET lastDate = ? ");
		q.append("  WHERE scoreId = ?");
		return con.prepareStatement(q.toString());
	}


	private static int loadDates(Connection con,
			 PreparedStatement read,
			 LinkedHashMap<String, String> dates)
		throws Exception
	{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateStr = null;
		int cnt = 0;

		// Read and save 'em
		ResultSet rs = read.executeQuery();
		//int dbcnt = 0;
		//while (rs.next() && dbcnt < 100)
		while (rs.next())
		{
			dateStr = null;
			//dbcnt++;
			cnt++;
			String id = rs.getString(1);
			String date = rs.getString(3);
			if (date == null || date.length() < 1)
			{
				date = rs.getString(2);
			}

			if (date == null || date.length() < 1)
			{
				System.out.println("ID " + id + " has no valid date");
				continue;
			}

			// Convert the date
			float since70 = 0.0f;
			try
			{
			since70 = Float.parseFloat(date) - 25569f; // Subtract the number of days from 1/1/1900 to 1/1/1970
			if (since70 < 0)
			{
				System.out.println("Date " + date + " is before 1/1/1970.  Conversion cannot handle dates before then");
				continue;
			}
			}catch(Exception e)
			{
				System.out.println("Error in parse:  ID=" + id + ", date=" + date);
				continue;
			}

			long ts = (long) ((since70 * 86400000f) + .5f);
			TimeZone tz = TimeZone.getTimeZone("America/Montreal");
			dateStr = sdf.format(new Date(ts-tz.getOffset(ts)));

			dates.put(id, dateStr);
		}

		return cnt;
	}


	/*
	 * 	Load the norm data into the norm table
	 */
	private static int updateDates(Connection con,
								   LinkedHashMap<String, String> dates,
								   PreparedStatement write)
		throws Exception
	{
		int cnt = 0;
		Map.Entry<String, String> ent = null;

		// Loop through the dates
		System.out.println("  " + dates.size() + " Date entries...");
		for (Iterator<Map.Entry<String, String>> itr=dates.entrySet().iterator(); itr.hasNext(); )
		{
			ent = itr.next();
			
			String id = ent.getKey();
			String date = ent.getValue();
			if (id == null || id.length() < 1)
			{
				System.out.println("Bad Key:  null.  Date=" + date);
			}
			if (date == null || date.length() < 1)
			{
				System.out.println("Bad Date:  null.  ID=" + id);
			}

			write.setString(1, date);
			write.setString(2, id);

			write.executeUpdate();
			cnt++;
		}
			
		return cnt;
	}
}
