/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.dto;



/**
 * DTO to transport data specific 
 * to the Test Data application in 
 * an RPC call.
 * 
 * @author MB Panichi
 * 
 */
public class TestData {

	//
	// Static data.
	//
	
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	
	private long projectId;    
	private long dnaId = -1;  // want to initialize this so I can check for null
	private long participantId;
	private boolean cogsCompleted;
	

	//
	// Constructors.
	//
	public TestData()
	{
		// does nothing right now
	}

	//
	// Instance methods.
	//
	
	public String toString()
	{
		String ret = " TestData ";	
		ret += "  projectId=" + projectId;
		ret += ", dnaId=" + dnaId;
		ret += ", participantId=" + participantId;
		ret += ", cogsCompleted=" + cogsCompleted;		
		return ret;
	}
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public void setProjectId(long value)
	{
		projectId = value;
	}
	
	public long getProjectId()
	{
		return projectId;
	}

	/*****************************************************************************************/
	public void setDnaId(long value)
	{
		dnaId = value;
	}
	
	public long getDnaId()
	{
		return dnaId;
	}
	
	/*****************************************************************************************/
	public void setParticipantId(long value)
	{
		participantId = value;
	}
	
	public long getParticipantId()
	{
		return participantId;
	}
	
	/*****************************************************************************************/
	public void setCogsCompleted(boolean value)
	{
		cogsCompleted = value;
	}
	
	public boolean getCogsCompleted()
	{
		return cogsCompleted;
	}
}
