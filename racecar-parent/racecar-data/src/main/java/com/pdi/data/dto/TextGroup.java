/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.data.dto;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/*
 * TextGroup - A text group holds all of the strings for a text group in a particular language.
 * 		The container within it ais a Map -- key = labelCode, value = text string
 */
public class TextGroup extends CoreDataObject
{
	//
	// Static data.
	//
	private static final long serialVersionUID = 1L;

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String groupCode;
	private String languageCode;
	private HashMap<String, Text> labelMap;

	//
	// Constructors.
	//
	public TextGroup(String groupCodeIn, String languageCodeIn)
	{
		groupCode = groupCodeIn;
		languageCode = languageCodeIn;
		labelMap = new HashMap<String, Text>();
	}

	//
	// Instance methods.
	//

	/*
	 * clone
	 */
	public TextGroup clone()
	{
		// Make a new one
		TextGroup ret = new TextGroup(this.groupCode, this.languageCode);
		// And copy the labels
		ret.setLabelMap(new HashMap<String, Text>(this.labelMap));
		
		return ret;
	}

	/*
	 * toString
	 */
	public String toString()
	{
		String str = "TextGroup:  ";
		str += "grpCode=" + groupCode;
		str += ", langCode=" + languageCode;
		for (Iterator<Map.Entry<String, Text>> itr=labelMap.entrySet().iterator(); itr.hasNext(); )
		{
			Map.Entry<String, Text> ent = itr.next();
			str += "\n";
			str+= "  " + ent.getKey() + "--" + ent.getValue().getText() + "  (" + ent.getValue().getLanguage().getCode() + ")";
		}

		return str;
	}
	
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
	
	/*****************************************************************************************/
	public String getGroupCode()
	{
		return groupCode;
	}
	
	public void setGroupCode(String value)
	{
		groupCode = value;
	}	
	
	/*****************************************************************************************/
	public String getLanguageCode()
	{
		return languageCode;
	}
	
	public void setLanguageCode(String value)
	{
		languageCode = value;
	}	
	
	/*****************************************************************************************/
	public HashMap<String, Text> getLabelMap()
	{
		return labelMap;
	}
	
	public void setLabelMap(HashMap<String, Text> value)
	{
		labelMap = value;
	}
	
	/*
	 * getLabelHashMap - Convenience method to return just the labels and their values
	 */
	public HashMap<String, String> getLabelHashMap()
	{
		HashMap<String, String> ret = new HashMap<String, String>();
		
		for(Iterator<Map.Entry<String, Text>> itr=labelMap.entrySet().iterator(); itr.hasNext(); )
		{
			Map.Entry<String, Text> ent = itr.next();
			ret.put(ent.getKey(), ent.getValue().getText());
		}
		
		return ret;
	}
}
