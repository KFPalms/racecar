/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.checkbox.helpers;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.pdi.data.checkbox.util.CheckboxWebserviceUtils;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;
import com.pdi.reporting.request.ReportingRequest;
import com.pdi.reporting.response.ReportingResponse;
import com.pdi.xml.XMLUtils;

/*
 * Helper clas to get data back from Checkbox
 */
public class CheckboxScoreDataHelper
{
	//
	// Static data.
	//
	
	//
	// Static methods.
	//
	
	/*
	 * Gets data from Checkbox, rolls up the results (generates raw scale scores),
	 * and return the data so it can eventually be placed into the V2 results table.
	 *
	 * The overall process will be as follows:
	 * 1. Set up the request (passing Checkbox response id)
	 * 2. Make the request (returns a Document object)
	 * 3. Set up the IndividualReport object with base data and raw (response) data from doc
	 * 4. Score the data
	 * 5. Return the data
	 * 
	 * @param rid The Checkbox
	 */
	public static IndividualReport fetchCheckboxData(long rid)
		throws Exception
	{
		
		//System.out.println("fetchCheckboxData -- RID " + rid);		
		
		// Create the return IndividualReport object and the
		// ReportData object that will contain the Career Survey data
		IndividualReport ir = new IndividualReport();
		ReportData	rd = new ReportData();
		ir.getReportData().put(ReportingConstants.RC_CAREER_SURVEY, rd);

		// Get the raw response data from Checkbox (via Cast Iron)
		CheckboxWebserviceUtils util = new CheckboxWebserviceUtils();
		Document doc = util.requestCheckboxData(rid);

		// Get the ScoreData node and parse out the V2 job (ENAGAGEMENT_ID)
		// and candidate (PERSON_ID) attributes
		NodeList partDataNodes = XMLUtils.getElements(doc, "//ScoreData");
		if (partDataNodes.getLength() < 1)
		{
			return null;
		}

		// set the Display Data  -- there will be only one row... 
		Node partData = partDataNodes.item(0);
//		System.out.println("--> ENGAGEMENT_ID=" + XMLUtils.getAttributeValue(partData, "ENGAGEMENT_ID") +
//					"--> PERSON_ID=" + XMLUtils.getAttributeValue(partData, "PERSON_ID"));

		// Put the data into the ReportData object.
		// NOTE:  We put this data into the DisplayData bucket of the Career Survey
		// RerportData object because there is no explicit bucket into which it can
		// be placed.  Downstream processes wil pick it up from there.
		rd.getDisplayData().put("ENGAGEMENT_ID", XMLUtils.getAttributeValue(partData, "ENGAGEMENT_ID"));
		rd.getDisplayData().put("PERSON_ID", XMLUtils.getAttributeValue(partData, "PERSON_ID"));

		// Get the response data...
		// Get the Task nodes (XML is same schema as for ADAPT) - should be only 1
		NodeList taskNodes =  XMLUtils.getElements(doc, "//Task");
		//System.out.println("Task node count=" + nodes.getLength());
		if (taskNodes.getLength() < 1)
		{
			// nothing to do... just leave
			return null;
		}

		// Get the first task (should only be the one)
		Node task = taskNodes.item(0);
		NodeList answerNodes = XMLUtils.getElements(task, "Answer");
		if (answerNodes.getLength() < 1)
		{
			return null;
		}

		// Process the answers
		for (int i=0; i < answerNodes.getLength(); i++)
		{
			Node ans = answerNodes.item(i);
//			System.out.println("    " + XMLUtils.getAttributeValue(ans, "QUESTION_ID") +
//						"--> resp=" + XMLUtils.getAttributeValue(ans, "RESPONSE"));

			// Put the data into the ReportData object
			rd.getRawData().put(XMLUtils.getAttributeValue(ans, "QUESTION_ID"), XMLUtils.getAttributeValue(ans, "RESPONSE"));
		}	// End of answers loop

		// Score the data
		ir.setReportCode(ReportingConstants.RC_CAREER_SURVEY);
		ir.setReportType(ReportingConstants.REPORT_TYPE_SCORE);
		ReportingRequest request = new ReportingRequest();
		request.addReport(ir);
		ReportingResponse response = request.generateReports();
		IndividualReport resp = (IndividualReport)response.getReports().get(0);

		return resp;
	}
	
	//
	// Instance data.
	//

	//
	// Constructors.
	//

	//
	// Instance methods.
	//

}
