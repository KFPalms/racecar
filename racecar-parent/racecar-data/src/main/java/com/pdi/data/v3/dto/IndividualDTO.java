/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.dto;

import org.w3c.dom.Node;

import com.pdi.data.util.BaseDTO;
import com.pdi.data.util.RequestStatus;
import com.pdi.xml.XMLUtils;

/**
 * DTO for individual information
 * 
 *
 * @author		Gavin Myers
 */
public class IndividualDTO extends BaseDTO
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _familyName;
	private String _givenName;
	private String _email;
	private String _telephone;
	private String _mobile;

	//
	// Constructors.
	//
	
	/**
	 * "Standard" (no-parameter) constructor
	 */
	public IndividualDTO()
	{
		// Does no additional functionality
	}
	
	/**
	 * Builds the object based off an xml node
	 * @param node
	 * @throws Exception
	 */
	public IndividualDTO(Node node)
	{
		/*
		 *	The XML should look something like this:
		 *	<familyName>Smith</familyName>
		 *	<givenName>John</givenName>
		 *	<email>johnsmith@iscopia.com</email>
		 *	<mobile>418-444-4446</mobile>	
		 */
		
		//Some times they use familyName... some times they use lastName
		// Try one and if it produces nothing try the other
		String famName = XMLUtils.getElementValue(node, "familyName/.");
		if (famName == null)
		{
			famName = XMLUtils.getElementValue(node, "lastName/.");
		}
		this.setFamilyName(famName);
		
		//Sometimes they use givenName sometimes they use firstName
		// Again, try one and if it produces nothing try the other
		String gvnName = XMLUtils.getElementValue(node, "givenName/.");
		if (gvnName == null)
		{
			gvnName = XMLUtils.getElementValue(node, "firstName/.");
		}
		this.setGivenName(gvnName);
		
		this.setEmail(XMLUtils.getElementValue(node, "email/."));
		this.setMobile(XMLUtils.getElementValue(node, "mobile/."));
		this.setTelephone(XMLUtils.getElementValue(node, "telephone/."));
		
		if (this.getFamilyName() == null &&
			this.getGivenName() == null &&
			this.getEmail() == null &&
			this.getTelephone() == null &&
			this.getMobile() == null)
		{
			this.getStatus().setStatusCode(RequestStatus.WB_NO_DATA);
		}

	}

	//
	// Instance methods.
	//
	
	/**
	 * Generic toString method
	 */
	public String toString()
	{
		String str = "\n --- IndividualDTO --- \n";
		str += "\nFamily Name:   " + this.getFamilyName();
		str += "\nGiven Name:    " + this.getGivenName();
		str += "\nEmail:         " + this.getEmail();
		str += "\nTelephone:     " + this.getMobile();
		str += "\nMobile:        " + this.getTelephone();
		
		return str;
	}
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
	
	/*****************************************************************************************/
	public void setFamilyName(String value)
	{
		_familyName = value;
	}
	
	public String getFamilyName()
	{
		return _familyName;
	}
	
	/*****************************************************************************************/
	public void setGivenName(String value)
	{
		_givenName = value;
	}
	
	public String getGivenName()
	{
		return _givenName;
	}
	
	/*****************************************************************************************/
	public void setEmail(String value)
	{
		_email = value;
	}
	
	public String getEmail()
	{
		return _email;
	}
	
	/*****************************************************************************************/
	public void setTelephone(String value)
	{
		_telephone = value;
	}
	
	public String getTelephone()
	{
		return _telephone;
	}
	
	/*****************************************************************************************/
	public void setMobile(String value)
	{
		_mobile = value;
	}
	
	public String getMobile()
	{
		return _mobile;
	}
}
