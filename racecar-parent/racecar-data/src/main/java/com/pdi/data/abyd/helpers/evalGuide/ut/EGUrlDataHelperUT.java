/*
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.evalGuide.ut;

import java.sql.Connection;

import com.pdi.data.abyd.dto.evalGuide.EGUrlDTO;
import com.pdi.data.abyd.helpers.evalGuide.EGUrlDataHelper;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.ut.UnitTestUtils;
import com.pdi.data.nhn.util.NhnDatabaseUtils;

import junit.framework.TestCase;

public class EGUrlDataHelperUT extends TestCase
{
	//
	// Constructors
	//

	public EGUrlDataHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
		throws Exception
	{
		junit.textui.TestRunner.run(EGUrlDataHelperUT.class);
    }


	/*
	 * testURL Generation
	 */
	public void testURLGen()
		throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);
		NhnDatabaseUtils.setUnitTest(true);

		Connection con = AbyDDatabaseUtils.getDBConnection();
		//String projId = "1";
		//String partId = "60254";
		//String projId = "863";
		//String projId = "860";
		//String partId = "372425";
		String projId = "400";
		String partId = "367924";
		
		EGUrlDataHelper helper = new EGUrlDataHelper(con);    
		EGUrlDTO dto = helper.getUrlData(projId, partId);
		
		System.out.println(dto.toString());

		UnitTestUtils.stop(this);
	}
}
