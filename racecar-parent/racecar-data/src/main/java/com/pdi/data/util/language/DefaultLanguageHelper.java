/**
 * Copyright (c) 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.util.language;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * DefaultLanguageHelper contains methods associated with the default language.
 *
 * @author		Ken Beukelman
 */
public class DefaultLanguageHelper
{
	//
	//  Static data.
	//
	private static long _dfltLang = 0;

	//
	// Static methods.
	//

	/**
	 * Fetch the default language ID and stuff it into a static (used in the constructor)
	 * ID is pulled from the database.  Default language is defined in LanguageConstants
	 */
	public static long fetchDefaultLanguageId()
		throws Exception
	{
		
		//Getting rid of this sillyness.  It will always be 1
		return 1;
		/*if (con == null)
			throw new Exception("fetchDefaultLanguage requires a Connection object");

		// See if we already have the language
		if (_dfltLang != 0)
			return _dfltLang;

		// Not there... get it
		StringBuffer sql = new StringBuffer();
		Statement stmt = null;
		ResultSet rs = null;

		sql.append("SELECT ll.languageId ");
		sql.append("  FROM pdi_abd_language ll ");
		sql.append("  WHERE ll.languageCode = '" + LanguageConstants.DEFAULT_LANG_CODE + "'");

		try
		{
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql.toString());

			if (! rs.isBeforeFirst())
			{
				throw new Exception("No data available for default language code " +  LanguageConstants.DEFAULT_LANG_CODE);
			}
			
			// Assume a single row
			rs.next();
			_dfltLang = rs.getLong("languageId");

			return _dfltLang;
		}
		catch (SQLException ex)
		{
			// Pass it on
			throw new SQLException(ex);
		}
		finally
		{
			if (rs != null)
			{
				try {  rs.close();  }
				catch (SQLException e)  {  }
				rs = null;
			}
			if (stmt != null)
			{
				try {  stmt.close();  }
				catch (SQLException e)  {  }
				stmt = null;
			}
		}
		*/
	}


	/**
	 * Fetch the default language code as defined in LanguageConstants
	 */
	public static String fetchDefaultLanguageCode()
	{
		return LanguageConstants.DEFAULT_LANG_CODE;
	}

	//
	// Instance data.
	//
	
	//
	// Constructors.
	//

	//
	// Instance methods.
	//
}
