package com.pdi.data.dto;

import java.util.HashMap;

public class ResponseGroup  extends CoreDataObject{
	
	private static final long serialVersionUID = 6208668431685677293L;
	private HashMap<String, Response> responses = new HashMap<String, Response>();

	
	/**
	 * 
	 */
	public ResponseGroup()  {
		this.setResponses(new HashMap<String, Response>());
	}

	public void setResponses(HashMap<String, Response> responses) {
		this.responses = responses;
	}

	public HashMap<String, Response> getResponses() {
		return responses;
	}
}
