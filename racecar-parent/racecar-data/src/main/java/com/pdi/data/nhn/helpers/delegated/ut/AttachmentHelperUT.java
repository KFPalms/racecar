package com.pdi.data.nhn.helpers.delegated.ut;

import com.pdi.data.dto.AttachmentHolder;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.IAttachmentHelper;
import com.pdi.data.nhn.util.NhnDatabaseUtils;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

public class AttachmentHelperUT extends TestCase
{
	public AttachmentHelperUT(String name)
	{
		super(name);
	}

	/*
	 * testFromProjectIdParticipantId
	 */
	public void testFromProjectIdParticipantId()
		throws Exception
	{
		UnitTestUtils.start(this);
		NhnDatabaseUtils.setUnitTest(true);

		SessionUser session = new SessionUser();
		String projectId = "330";
		String participantId = "367574";
	
		IAttachmentHelper hlpr = HelperDelegate.getAttachmentHelper("com.pdi.data.nhn.helpers.delegated");

		try
		{
			AttachmentHolder ret = hlpr.fromProjectIdParticipantId(session, projectId, participantId);
			System.out.println(ret.toString());
		}
		catch (Exception e)
		{
			System.out.println("ProjId " + projectId + ", Part " + participantId + " threw an exception.");
			e.printStackTrace();
		}

		UnitTestUtils.stop(this);
	}

	/*
	 * testFromProjectId
	 */
	public void testFromProjectId()
		throws Exception
	{
		UnitTestUtils.start(this);
		NhnDatabaseUtils.setUnitTest(true);

		SessionUser session = new SessionUser();
		String projectId = "330";
	
		IAttachmentHelper hlpr = HelperDelegate.getAttachmentHelper("com.pdi.data.nhn.helpers.delegated");

		try
		{
			AttachmentHolder ret = hlpr.fromProjectId(session, projectId);
			System.out.println(ret.toString());
		}
		catch (Exception e)
		{
			System.out.println("ProjId " + projectId + " threw an exception.");
			e.printStackTrace();
		}

		UnitTestUtils.stop(this);
	}

	/*
	 * testFromParticipantId
	 */
	public void testFromParticipantId()
		throws Exception
	{
		UnitTestUtils.start(this);
		NhnDatabaseUtils.setUnitTest(true);

		SessionUser session = new SessionUser();
		String participantId = "367574";
	
		IAttachmentHelper hlpr = HelperDelegate.getAttachmentHelper("com.pdi.data.nhn.helpers.delegated");

		try
		{
			AttachmentHolder ret = hlpr.fromParticipantId(session, participantId);
			System.out.println(ret.toString());
		}
		catch (Exception e)
		{
			System.out.println("PartID " + participantId + " threw an exception.");
			e.printStackTrace();
		}

		UnitTestUtils.stop(this);
	}
}
