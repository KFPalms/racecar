/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v2.dto;

import org.w3c.dom.Node;

import com.pdi.xml.XMLUtils;

/**
 * ClientDTO is a wrapper around a number of data items
 * relaying information about a client.
 * 
 * @author      MB Panichi
 * @author		Ken Beukelman
  */
public class ClientDTO
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _id;
	private String _name;
	private String _uri;
	private String _abbreviation;

	//
	// Constructors.
	//

	/**
	 * "Standard" (no-parameter) constructor
	 */
	public ClientDTO()
	{
		super();
	}
	
	/*
	 * Constructor with a Node
	 */
	public ClientDTO(Node node)
	{
		this.setId(XMLUtils.getAttributeValue(node, "UniqueIdStamp"));
		this.setName(XMLUtils.getAttributeValue(node, "Name"));
	}

	//	
	// Instance methods.
	//
	/**
	 * Generic toString method
	 */
	public String toString()
	{
			String str = "\n --- ClientDTO --- \n";
			str += "\nID:                 " + this.getId();
			str += "\nName:               " + this.getName();
			str += "\nURI:                " + this.getUri();

			return str;
	}
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public void setId(String id)
	{
		_id = id;
	}

	public String getId()
	{
		return _id;
	}

	/*****************************************************************************************/
	public void setName(String value)
	{
		_name = value;
	}

	public String getName()
	{
		return _name;
	}

	/*****************************************************************************************/
	public void setUri(String value)
	{
		_uri = value;
	}

	public String getUri()
	{
		return _uri;
	}

	/*****************************************************************************************/
	public void setAbbreviation(String value)
	{
		_abbreviation = value;
	}
	
	public String getAbbreviation()
	{
		return _abbreviation;
	}

}
