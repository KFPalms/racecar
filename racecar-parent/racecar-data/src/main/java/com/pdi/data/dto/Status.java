/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.dto;

/**
 * Status is a basic data class that contains status about a query.
 * Not according to the latest thinking (see below)
 * "I saw the Core object status as event status and the Status object
 *  as data status. For example: A Status object on the Client will
 *  track if that Client is Deleted, Archived, Available. The Core
 *  status of the Client would track if adding/deleting/updating that
 *  Client failed. To put it easier. The Status object would track to
 *  a status column on the Client table. Where as the Core status wouldn't
 *  track to anything in the database, it would be built around the request"
 * 
 * 
 * @author		Gavin Myers
 * @author      MB Panichi
 */
public class Status extends CoreDataObject
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5386311119081459454L;
	//
	// Static data.
	//
	// These must be defined the same as on the Flex side
	public static int COMPLETED = 1;
	public static int CANCELLED = 2;
	public static int IN_PROGRESS = 3; 
	public static int NOTSTARTED = 4;

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String name;
	private int code;

	//
	// Constructors.
	//
	public Status()
	{
		// does nothing right now
	}

	//
	// Instance methods.
	//
	
	public String toString()
	{
		String ret = "Status " + name + " (" + code + ")";

		return ret;
	}
	
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public void setName(String value)
	{
		name = value;
	}
	
	public String getName()
	{
		return name;
	}

	/*****************************************************************************************/
	public void setCode(int value)
	{
		code = value;
	}
	
	public int getCode()
	{
		return code;
	}
}
