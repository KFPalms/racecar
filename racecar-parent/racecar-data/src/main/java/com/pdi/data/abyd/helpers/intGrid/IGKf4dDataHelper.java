package com.pdi.data.abyd.helpers.intGrid;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import com.pdi.data.abyd.dto.common.IGInstCompData;
import com.pdi.data.abyd.dto.common.PrnData;
import com.pdi.data.abyd.dto.common.SpssInfo;
import com.pdi.data.abyd.dto.intGrid.IGScaleScoreData;
import com.pdi.data.abyd.dto.intGrid.LineScoreData;
import com.pdi.data.abyd.helpers.common.HelperUtils;
import com.pdi.data.abyd.helpers.common.Rv2Helper;
import com.pdi.data.abyd.helpers.intGrid.IGScoring.ScoreAndNormData;
//import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.data.abyd.util.Utils;
import com.pdi.data.util.language.DefaultLanguageHelper;
import com.pdi.properties.PropertyLoader;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.scoring.Norm;
import com.pdi.webservice.NhnWebserviceUtils;

public class IGKf4dDataHelper {
	
	private static final Logger log = LoggerFactory.getLogger(IGKf4dDataHelper.class);
	
	private static final String KF4D_SCORE_STR = "/kf4dScores/{pptId}/KF4D_JSON/{projId}";
	//private static final String RV2_RAW_SCORE_STR = "/scoringExtracts/participants/{pptId}/projects/{projId}/instruments/rv2";
	private static final String WG_SCALE = "WGE_CT";
	private static final String RAV_SCALE = "RAVENSB";

	// Instance variables
	private Connection con;
	private String partId;
	private long dnaId;

	// derived values
	private long dfltLangId;
	private long projId;

	private Kf4dScoreData holder = null;

	// Ravens norm -- Moved to Rv2Helper
	//private static double SPEC_RAV_MEAN = 14.3;
	//private static double SPEC_RAV_STDEV = 4.1;

	// Ravens Key -- moved to Rv2Helper
	//private static List<Integer> RAVENS_KEY = new ArrayList<Integer>(
		//	Arrays.asList(8, 7, 1, 8, 4, 3, 1, 6, 5, 4, 4, 7, 8, 7, 2, 5, 6, 7, 8, 3, 6, 3, 2));

	Map<Long, List<SpssInfo>> compSpssInfo = null;
	
	ArrayList<String> courseList = new ArrayList<String>();
	
	String kf4dJSONData = null;
	
	Document projectInfo = null;
	
	int rv2Rawscore;
	
	String rv2Rating;

	private String testInstrument = "KF4D";

	//
	// Constructors
	//
	@SuppressWarnings("unused")
	private IGKf4dDataHelper() {
		// Does nothing... invalid for use (needs the 3 param constructor)
	}

	/**
	 * Constructor.
	 *
	 * @param eCon
	 * @param partId
	 * @param dnaId
	 * @throws Exception
	 */
	public IGKf4dDataHelper(Connection eCon, String partId, long dnaId) throws Exception {
		log.debug("Constructing a new IGKf4dDataHelper instance");
		if (eCon == null) {
			throw new Exception("Invalid connection parameter for getAlrScoring");
		}
		if (partId == null || partId.length() < 1) {
			throw new Exception("Invalid participant ID parameter for getAlrScoring");
		}
		if (dnaId == 0) {
			throw new Exception("Invalid dna ID parameter for getAlrScoring");
		}

		this.con = eCon;
		this.partId = partId;
		this.dnaId = dnaId;

		this.dfltLangId = DefaultLanguageHelper.fetchDefaultLanguageId();
		this.projId = HelperUtils.getProjIdFromDna(this.dnaId);
		
	}
	
	public IGKf4dDataHelper(Connection eCon, String partId, long dnaId, Document projectInfo) throws Exception {
		log.debug("Constructing a new IGKf4dDataHelper instance");
		if (eCon == null) {
			throw new Exception("Invalid connection parameter for getAlrScoring");
		}
		if (partId == null || partId.length() < 1) {
			throw new Exception("Invalid participant ID parameter for getAlrScoring");
		}
		if (dnaId == 0) {
			throw new Exception("Invalid dna ID parameter for getAlrScoring");
		}

		this.con = eCon;
		this.partId = partId;
		this.dnaId = dnaId;

		this.dfltLangId = DefaultLanguageHelper.fetchDefaultLanguageId();
		this.projId = HelperUtils.getProjIdFromDna(this.dnaId);
		this.projectInfo = projectInfo;
		
	}

	public IGKf4dDataHelper(Connection conn, String partId, String projId) throws Exception {
		if (partId == null || partId.length() < 1) {
			throw new Exception("Invalid participant ID parameter for IGKf4dDataHelper");
		}
		if (projId == null) {
			throw new Exception("Invalid proj ID parameter (null) for IGKf4dDataHelper");
		}
		try {
			this.projId = Long.parseLong(projId);
		} catch (NumberFormatException e) {
			throw new Exception("Invalid proj ID parameter (" + projId + ") for IGKf4dDataHelper");
		}
		if (this.projId == 0) {
			throw new Exception("Invalid proj ID parameter (0) for IGKf4dDataHelper");
		}
		this.partId = partId;

		if (conn == null) {
			throw new Exception("Invalid DB connection (null) for IGKf4dDataHelper");
		}
		this.con = conn;

		this.dfltLangId = DefaultLanguageHelper.fetchDefaultLanguageId();
		this.dnaId = Utils.getDnaIdFromProjectId(this.projId);
	}

	/**
	 * getKf4dTestCompScores - Simple converter to take the generated KF4D comp
	 * scores from the internal data object and convert them to the string
	 * objects required by the IG display
	 *
	 * @return
	 * @throws Exception
	 */
	public Map<Long, String> getKf4dTestCompScores() throws Exception {
		HashMap<Long, String> ret = new HashMap<Long, String>();

		// Get the score data (scores in an object of arrays)
		Kf4dScoreData ksd = getKf4dScoreData();

		// Put it out into a Map of competency scores
		for (Iterator<Map.Entry<Long, Float>> itr = ksd.getCompScores().entrySet().iterator(); itr.hasNext();) {
			Map.Entry<Long, Float> ent = itr.next();

			// round the comp scores to 2 decimals
			// BigDecimal bd = new BigDecimal(ent.getValue());
			// bd = bd.setScale(2, RoundingMode.HALF_UP);
			// //ret.put(ent.getKey(), "" + bd.floatValue());
			// ret.put(ent.getKey(), String.format("%.2f", bd.floatValue())); //
			// Need 2 decimals in result string

			double score = Math.floor((ent.getValue() * 100.0) + 0.505) / 100.0;
			ret.put(ent.getKey(), String.format("%.2f", score)); // Need 2
																	// decimals
																	// in result
																	// string
		}

		return ret;
	}

	/**
	 * getKf4dScoreData - get the constituent scores from the KF4D data
	 *
	 * @return Kf4dScoreData object
	 */
	private Kf4dScoreData getKf4dScoreData() throws Exception {
		if (this.holder != null) {
			return this.holder;
		}

		// Get the raw JSON data
		String json;
		try {
			json = getKf4dJsonScoreData();
		} catch (Exception e) {
			String err = "Unable to fetch KF4D score JSON for ppt " + this.partId + ", proj " + this.projId + ".  Msg="
					+ e.getMessage();
			System.out.println(err);
			throw new Exception(err);
		}
		boolean hasScores = true;
		if (StringUtils.isEmpty(json)) {
			// String err = "KF4D score data not available for ppt " +
			// this.partId + ", proj " + this.projId + ".";
			log.debug("KF4D score data not available for ppt {}, proj {}.", this.partId, this.projId);
			// throw new Exception(err);
			hasScores = false;
		}

		// Pop score data into the output
		Kf4dScoreData ret = new Kf4dScoreData();

		if (hasScores) {
			// Read up the JSON tree
			ObjectMapper mapper = new ObjectMapper();
			JsonNode root;
			try {
				root = mapper.readTree(json);
			} catch (JsonProcessingException e) {
				String err = "Unable to process KF4D score JSON for ppt " + this.partId + ", proj " + this.projId
						+ ".  Msg=" + e.getMessage();
				log.error(err);
				throw new Exception(err);
			} catch (IOException e) {
				String err = "IO error mapping KF4D score JSON for ppt " + this.partId + ", proj " + this.projId
						+ ".  Msg=" + e.getMessage();
				log.error(err);
				throw new Exception(err);
			}

			ret = fillTraits(root, ret);
			ret = fillDrivers(root, ret);
			ret = fillRecommended(root, ret);
			ret = fillDerailers(root, ret);
		}

		// Add the Cogs
		// ArrayList<String> courseList = new NhnWebserviceUtils()
		// .fetchUnfilteredCourseListForProject(PropertyLoader.getProperty("com.pdi.listener.portal.application",
		// "courseList.url"), ""+this.projId);
		
		if (projectInfo == null){
			projectInfo = new NhnWebserviceUtils().fetchProjectInfo(String.valueOf(projId));
		}
		if (this.courseList.isEmpty()){
			log.debug("Getting course list from project info document");
			courseList = new NhnWebserviceUtils().getUnfilteredProjectCourseList(projectInfo);
		}
		
		if (courseList.contains(IGConstants.I_CODE_DLA)){
			log.debug("Setting Test Instrument to DLA");
			setTestInstrument("Leadership Assessment: DLA");
		} else if (courseList.contains(IGConstants.I_CODE_DPA)){
			log.debug("Setting Test Instrument to DPA");
			setTestInstrument("Potential Assessment: DPA");
		} 
		//ArrayList<String> courseList = new NhnWebserviceUtils().getUnfilteredProjectCourseList(this.projId);

		// WG and Ravens are both associated with KF4D
		ScoreAndNormData sn;
		if (courseList.contains(IGConstants.I_CODE_WGE) || courseList.contains(IGConstants.I_CODE_RAVB)) {
			// Get the WG scale score and dump it into Traits
			sn = IGScoring.getScoreAndNormData(this.con, this.partId, this.dnaId, false, false, true);
			// initialize cogsDone to its derived value
			ret.setCogsDone(sn.getCogsDone());

			if (sn.getCogsDone()) {
				if (courseList.contains(IGConstants.I_CODE_WGE)) {
					String wgRating = sn.getScoreMap().get(WG_SCALE).getRating();
					if (wgRating != null) {
						PrnData pd = new PrnData();
						pd.setRate(Float.parseFloat(wgRating));
						ret.getTraits().put(WG_SCALE, pd);
					}
				}
				if (courseList.contains(IGConstants.I_CODE_RAVB)) {
					String wgRating = sn.getScoreMap().get(RAV_SCALE).getRating();
					if (wgRating != null) {
						PrnData pd = new PrnData();
						pd.setRate(Float.parseFloat(wgRating));
						ret.getTraits().put(RAV_SCALE, pd);
						
					}
				}
			}
		} else {
			// No "normal" cogs selected. Initialize to "all done"
			ret.setCogsDone(true);
		}

		if (courseList.contains(IGConstants.I_CODE_RAVB2)) {
			if (HelperUtils.courseComplete(this.partId, IGConstants.I_CODE_RAVB2)) {
				// TODO Replace this with something that uses canned scoring
				PrnData pd = getRv2Rating(this.partId, this.projId);
				// Don't overide the cogsDone... let the "normal" ones drive
				// put the rating into the traits
				ret.getTraits().put(RAV_SCALE, pd);
				if (!(pd == null)){
					log.debug("Added RV2 Rating: {} to KF4D Traits scores with key: {}", pd.getRate(), RAV_SCALE);
				} 
			//Ravens Reuse dictates we need to use RV score for RV2 if available
			} else if (!courseList.contains(IGConstants.I_CODE_RAVB) && HelperUtils.courseComplete(this.partId, IGConstants.I_CODE_RAVB)) {
				PrnData pd = Rv2Helper.getRvRating(this.partId, con);
				ret.getTraits().put(RAV_SCALE, pd);
				if (!(pd == null)){
					log.debug("Added RV2 Rating: {} to KF4D Traits scores with key: {}.  USING RV1 score!!!!", pd.getRate(), RAV_SCALE);
				} 
			} else {
				// Not done; overide the "normal" cogsDone if needed
				if (ret.getCogsDone()) {
					ret.setCogsDone(false);
				}
			}
		}

		// Calc the competency scores
		// Get the list of competencies with constituent scales
		Map<Long, List<SpssInfo>> compScales = getSpssForComps();
		
		log.debug("Got {} competency scales", compScales.size());

		// get the normed scale score ratings (Traits + Drivers)
		Map<String, PrnData> scores = ret.getTraits();
		for (Iterator<Map.Entry<String, PrnData>> itr = (ret.getDrivers()).entrySet().iterator(); itr.hasNext();) {
			// Drivers has values only without ALR prefix... make them "scales"
			Map.Entry<String, PrnData> ent = itr.next();
			String key = "ALR_" + ent.getKey();
			scores.put(key, ent.getValue());
		}

		// generate the competency scores
		for (Iterator<Map.Entry<Long, List<SpssInfo>>> itr = compScales.entrySet().iterator(); itr.hasNext();) {
			// get the next competency
			Map.Entry<Long, List<SpssInfo>> ent = itr.next();
			long compId = ent.getKey();
			float compScore = 0.0f; // Default value = 0

			if (scores.isEmpty()) {
				compScore = -1f;
			} else {
				// get the constituent scores and average them
				int cnt = 0;
				float cumScore = 0.0f;
				boolean scram = false;
				for (SpssInfo ci : ent.getValue()) {
					if (scram) {
						// Missing at least one trait/driver... no score
						// possible
						cnt = 0;
						compScore = -1f;
						break;
					}
					// Get the rating for the named spss key. If it isn't
					// present, skip it
					String spssKey = ci.getSpssId();
					PrnData pd = scores.get(spssKey);
					if (pd == null) {
						if (spssKey.equals(WG_SCALE) || spssKey.equals(RAV_SCALE)) {
							continue;
						} else {
							// throw new Exception("Score data for trait " +
							// spssKey + " does not exist; ppt=" + this.partId +
							// ", project=" + this.projId);
							scram = true;
							continue;
						}
					}
					Float score = scores.get(spssKey).getRate();
					log.trace("Got score: {}, for spss: {}, competency: {}", score, spssKey, compId);
					if (score == null || score == 0.0f) {
						// if(spssKey.equals("RAVENSB"))
						// {
						// noRav = true;
						// }
						continue;
					}

					// got a live one
					cnt++;
					// No rounding being done here
					if (ci.isInverted()) {
						log.trace("Inverting score for {}.  6.0 minus {} equals {}", spssKey, score, 6.0 - score);
						cumScore += (6.0 - score);
					} else {
						cumScore += score;
					}
				}

				// Finished the list of scales for the competency. Calculate the
				// score and save it for output.
				if (cnt > 0) {
					compScore = cumScore / cnt;
					log.trace("Calculated score: {} / {} = {} for competency id: {}", cumScore, cnt, compScore, compId);
				}
			}

			ret.getCompScores().put(compId, compScore);
		}

		// Scram
		this.holder = ret;

		return ret;
	}


	/**
	 * fillTraits
	 *
	 * @param root
	 * @param ret
	 * @return
	 */
	private Kf4dScoreData fillTraits(JsonNode root, Kf4dScoreData ret) {
		JsonNode traitsNode = root.path("results").path("candidate").path("traits");
		if (traitsNode != null){
			log.trace("Traits node found: {}", traitsNode.asText());
		}
		if (traitsNode.isArray()) {
			for (JsonNode node : traitsNode) {
				PrnData prn = new PrnData();
				String key = node.path("Ckey").asText();
				key = "ALR_" + key.toUpperCase();
				int pctl = node.path("percentile").asInt();
				pctl = cleanPctl(pctl);
				prn.setPctl(pctl);
				prn.setRate(Float.parseFloat(Norm.calcPdiRating(pctl)));
				prn.setNarr(getNarrIdx(pctl));
				ret.getTraits().put(key, prn);
			}
		}

		return ret;
	}

	/**
	 * fillDrivers
	 *
	 * @param root
	 * @param ret
	 * @return
	 */
	private Kf4dScoreData fillDrivers(JsonNode root, Kf4dScoreData ret) {
		JsonNode driversNode = root.path("results").path("candidate").path("drivers");
		if (driversNode != null){
			log.trace("Drivers node found: {}", driversNode.asText());
		}
		if (driversNode.isArray()) {
			for (JsonNode node : driversNode) {
				PrnData prn = new PrnData();
				String key = node.path("Ckey").asText();
				key = key.toUpperCase();
				int pctl = node.path("percentile").asInt();
				pctl = cleanPctl(pctl);
				prn.setPctl(pctl);
				prn.setRate(Float.parseFloat(Norm.calcPdiRating(pctl)));
				prn.setNarr(getNarrIdx(pctl));
				ret.getDrivers().put(key, prn);
			}
		}

		return ret;
	}

	/**
	 * fillRecommended
	 *
	 * @param root
	 * @param ret
	 * @return
	 */
	private Kf4dScoreData fillRecommended(JsonNode root, Kf4dScoreData ret) {
		JsonNode recNode = root.path("results").path("candidate").path("recommended");
		if (recNode != null){
			log.trace("Recommended node found: {}", recNode.asText());
		}
		if (recNode.isArray()) {
			for (JsonNode node : recNode) {
				String key = node.path("Ckey").asText();
				key = key.toUpperCase();
				int val = node.path("score").asInt();
				ret.getRecommended().put(key, val);
			}
		}

		return ret;
	}

	/**
	 * fillDerailers
	 *
	 * @param root
	 * @param ret
	 * @return
	 */
	private Kf4dScoreData fillDerailers(JsonNode root, Kf4dScoreData ret) {
		JsonNode derailNode = root.path("results").path("candidate").path("derailers");
		if (derailNode != null){
			log.trace("Derailers node found: {}", derailNode.asText());
		}
		if (derailNode.isArray()) {
			for (JsonNode node : derailNode) {
				String key = node.path("Ckey").asText();
				key = key.toUpperCase();
				int pctl = node.path("percentile").asInt();
				pctl = cleanPctl(pctl);
				ret.getDerailers().put(key, pctl);
			}
		}

		return ret;
	}

	/**
	 * calcPctlFromRaw - Does what it says (integer from 1 to 99)
	 *
	 * @param rawPctl
	 * @return
	 */
	private int cleanPctl(int inpPctl) {
		if (inpPctl < 1) {
			log.trace("Percentile value {} is less than 1.  Setting to 1", inpPctl);
			inpPctl = 1;
		} else if (inpPctl > 99) {
			log.trace("Percentile value {} is greater than 99.  Setting to 99", inpPctl);
			inpPctl = 99;
		}

		return inpPctl;
	}

	/**
	 * getNarrIdx - calculate the narrative index based upon the percentile
	 * (cutpoints from Jeff Jones)
	 *
	 * @param pctl
	 * @return narrative index
	 */
	private int getNarrIdx(int pctl) {
		int ret = 0;

		if (pctl < 16) {
			ret = 1;
		} else if (pctl >= 16 && pctl < 31) {
			ret = 2;
		} else if (pctl >= 31 && pctl < 70) {
			ret = 3;
		} else if (pctl >= 70 && pctl < 85) {
			ret = 4;
		} else if (pctl >= 85) {
			ret = 5;
		}

		return ret;
	}

	/**
	 * getKf4dScoreData - get the JSON string that contains the KF4D score data
	 *
	 * @param pptId
	 * @param projId
	 * @return JSON score string
	 */
	private String getKf4dJsonScoreData() throws Exception {
		
		if (this.kf4dJSONData != null){
			return this.kf4dJSONData;
		}
		URL url = null;
		URLConnection uc = null;
		InputStream content = null;

		// Call the RESTful service
		String surl = PropertyLoader.getProperty("com.pdi.data.abyd.application", "palms.reportRestServer.url");
		if (surl.charAt(surl.length() - 1) == '/') {
			surl = surl.substring(0, surl.length() - 2);
		}
		surl += KF4D_SCORE_STR;

		surl = surl.replace("{pptId}", partId);
		surl = surl.replace("{projId}", "" + projId);

		log.debug("Getting KF4D Json from URL: {}", surl);
		try {
			url = new URL(surl);
		} catch (MalformedURLException e) {
			String err = "IGKf4dDataHelper.getKf4dJsonScoreData() - Malformed URL - " + surl;
			System.out.println(err);
			throw new Exception(err);
		}

		// open the connection.
		try {
			uc = url.openConnection();
		} catch (IOException e) {
			String err = "IGKf4dDataHelper.getKf4dJsonScoreData() - Unable to open connection.  URL=" + surl + ".  Msg="
					+ e.getMessage();
			System.out.println(err);
			throw new Exception(err);
		}

		// Get the data
		HttpURLConnection huc = (HttpURLConnection) uc;
		boolean hasScores = true;
		try {
			int statusCode = huc.getResponseCode();
			// Kaz wants to send 404 instead of 204
			// I am conflicted as 404 is also the response if the service is not
			// available
			// Putting in both
			if (statusCode == HttpServletResponse.SC_NOT_FOUND || statusCode == HttpServletResponse.SC_NO_CONTENT
					|| statusCode == HttpServletResponse.SC_INTERNAL_SERVER_ERROR) {
				// String str = "There is no KF4D scoring information available
				// for participant " + this.partId + " in project " +
				// this.projId + ".";
				hasScores = false;
			} else if (statusCode >= 200 && statusCode < 300) {
				content = huc.getInputStream();
			} else {
				String str = "Error returned from KF4D score fetch: Code=" + statusCode + " for URL=" + surl
						+ ".  Message=" + huc.getResponseMessage();
				log.error(str);
				throw new Exception(str);
			}
		} catch (IOException e) {
			String err = "getKf4dJsonScoreData(): Error fetching score JSON for URL=" + surl + ".  Message="
					+ e.getMessage();
			log.error(err);
			throw new Exception(err);
		}

		StringBuffer result = new StringBuffer();
		if (hasScores) {
			BufferedReader br = new BufferedReader(new InputStreamReader(content));
			String line;
			try {
				while ((line = br.readLine()) != null) {
					result.append(line);
				}
			} catch (IOException e) {
				String err = "IGKf4dDataHelper.getKf4dJsonScoreData() - Unable to read content.  URL=" + surl
						+ ".  Msg=" + e.getMessage();
				System.out.println(err);
				throw new Exception(err);
			}
		}
		this.kf4dJSONData = result.toString();
		// OK
		return kf4dJSONData;
	}

	/**
	 * getSpssForComps - Fetch a map (key = comp ID) that contains lists of spss
	 * keys that are valid for the key competency.
	 *
	 * @return
	 * @throws Exception
	 */
	private Map<Long, List<SpssInfo>> getSpssForComps() throws Exception {
		if (compSpssInfo == null) {
			compSpssInfo = (new Utils()).getCompSpssInfo(this.dnaId);
		}
		return compSpssInfo;
	}

	/**
	 * hasKf4dInDna - Return a boolean indicating if there are ALP items in
	 * selected DNA. NOTE: This method does not use the underlying private data
	 * class.
	 *
	 * @return
	 * @throws Exception
	 */
	// TODO is there a way to check both ALP and K4D at the same time?
	public boolean hasKf4dInDna() throws Exception {
		boolean ret = false; // default value says that there are no KF4D items

		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT COUNT(*) AS k4dCnt ");
		sqlQuery.append("  FROM pdi_abd_dna_link dl ");
		sqlQuery.append("  WHERE dl.dnaId = " + this.dnaId + " ");
		sqlQuery.append("    AND dl.moduleId IN (SELECT mm.moduleId ");
		sqlQuery.append("                          FROM pdi_abd_module mm ");
		sqlQuery.append("                          WHERE mm.internalName = 'KF4D')");
		// System.out.println("noCogsInDna SQL " + sqlQuery.toString());

		try {
			stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			// "SELECT COUNT()..." here always returns exactly 1 row... get it
			rs.next();

			int cnt = rs.getInt("k4dCnt");

			ret = cnt == 0 ? false : true;

			return ret;
		} catch (SQLException ex) {
			// handle any errors
			throw new Exception("SQL error for hasKf4dInDna.  " + "SQLException: " + ex.getMessage() + ", "
					+ "SQLState: " + ex.getSQLState() + ", " + "VendorError: " + ex.getErrorCode());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					System.out.println("ERROR: rs close in noCogsInDna - " + sqlEx.getMessage());
				}
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					System.out.println("ERROR: stmt close in noCogsInDna - " + sqlEx.getMessage());
				}
				stmt = null;
			}
		}
	}

	/**
	 * getKf4dCompData - gets the data about constituent norm scores for the
	 * testing column
	 *
	 * @return
	 * @throws Exception
	 */
	// TODO Refactor to leverage the same code in IGALRDataHelper
	public ArrayList<IGInstCompData> getKf4dCompData() throws Exception {
		ArrayList<IGInstCompData> ret = new ArrayList<IGInstCompData>();

		// Get the competency info
		Map<Long, IGInstCompData> holder = new LinkedHashMap<Long, IGInstCompData>();
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT DISTINCT dl.competencyId, ");
		sqlQuery.append("                txt1.text as compName, ");
		sqlQuery.append("                txt2.text as compDesc, ");
		sqlQuery.append("                cmp.internalName as intName, ");
		sqlQuery.append("                dl.groupSeq, ");
		sqlQuery.append("                dl.compSeq ");
		sqlQuery.append("  FROM pdi_abd_dna_link dl ");
		sqlQuery.append("  INNER JOIN pdi_abd_module sims ON (sims.moduleId = dl.moduleId ");
		sqlQuery.append("                                 AND sims.internalName in ('KF4D')) ");
		sqlQuery.append("  LEFT JOIN pdi_abd_competency cmp ON cmp.competencyId = dl.competencyId ");
		sqlQuery.append("  LEFT JOIN pdi_abd_text txt1 ON (txt1.textId = cmp.textId AND txt1.languageId = "
				+ this.dfltLangId + ") ");
		sqlQuery.append("  INNER JOIN pdi_abd_dna dna on dna.dnaId = dl.dnaId ");
		sqlQuery.append("  LEFT JOIN pdi_abd_mod_comp_desc mcd on (mcd.modelId = dna.modelId ");
		sqlQuery.append("                                      AND mcd.competencyId = dl.competencyId) ");
		sqlQuery.append("  LEFT JOIN pdi_abd_text txt2 ON (txt2.textId = mcd.textId AND txt2.languageId = "
				+ this.dfltLangId + ") ");
		sqlQuery.append("  WHERE dl.dnaId = " + this.dnaId + " ");
		sqlQuery.append("  and dl.isUsed = 1 ");
		sqlQuery.append("  ORDER BY dl.groupSeq, dl.compSeq");

		try {
			stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			// Get the competencies - generate IGInstCompData objects
			// (no score data) and a map of compId/intName
			Map<String, Long> nameMap = new HashMap<String, Long>();
			while (rs.next()) {
				IGInstCompData elt = new IGInstCompData();
				elt.setCompId(rs.getLong("competencyId"));
				elt.setCompName(rs.getString("compName"));
				elt.setCompDesc(rs.getString("compDesc"));
				Long cmpId = new Long(rs.getLong("competencyId"));

				holder.put(cmpId, elt);
				nameMap.put(rs.getString("intName"), cmpId);
			}

			Map<Long, LineScoreData> sl = getScoreLists();
			
			// Stick the score lists into the IGInstCompData objects
			// and put the result in the output array
			for (Iterator<Map.Entry<Long, IGInstCompData>> itr = holder.entrySet().iterator(); itr.hasNext();) {
				Map.Entry<Long, IGInstCompData> ent = itr.next();
				log.trace("Getting item '{}' from scorelists", ent.getKey());
				LineScoreData scoreLists = sl.get(ent.getKey());
				IGInstCompData compData = ent.getValue();

				log.trace("Competency {} ({}).  Calculated Score: {}", compData.getCompName(), compData.getCompId(), compData.getCalcedCompScore());
				
				if (scoreLists == null){
					log.error("Score Lists is null.  Bad news.  Goodbye");
				}
				
				compData.setCogColumnScores(scoreLists.getCogColumnScores());
				compData.setGpiColumnScores(scoreLists.getGpiColumnScores());

				ret.add(compData);
			}

			return ret;
		} catch (SQLException ex) {
			// handle any errors
			throw new Exception("SQL in getKf4dCompData.  " + "SQLException: " + ex.getMessage() + ", " + "SQLState: "
					+ ex.getSQLState() + ", " + "VendorError: " + ex.getErrorCode());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					System.out.println("ERROR: rs close in IG getCompData - " + sqlEx.getMessage());
				}
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					System.out.println("ERROR: stmt close in IG getCompData - " + sqlEx.getMessage());
				}
				stmt = null;
			}
		}
	}

	/**
	 * getScoreLists - gets a map (key = competency Id) of score containers
	 * (scores separated by "cog" vs. "kf4d" with kf4d carried in the "gpi"
	 * container). Used to get the normed scale scores for display in the "Edit"
	 * and "View" data in the IG
	 *
	 * @return
	 * @throws Exception
	 */
	private Map<Long, LineScoreData> getScoreLists() throws Exception {
		Map<Long, LineScoreData> ret = new HashMap<Long, LineScoreData>();

		// Get the scale scores
		Map<String, String> scaleScoreMap = getKf4dScaleScores();
		//
		// // Get the cogs, if present
		// Map<String, String> cogScoreMap = getCogScaleScores();
		// if (! cogScoreMap.isEmpty())
		// {
		// scaleScoreMap.putAll(cogScoreMap);
		// }

		// Get the comp/scale cross reference
		Map<Long, List<SpssInfo>> spssLists = getSpssForComps();

		// For each item in the SPSS list...
		for (Iterator<Map.Entry<Long, List<SpssInfo>>> itr = spssLists.entrySet().iterator(); itr.hasNext();) {
			Map.Entry<Long, List<SpssInfo>> ent = itr.next();
			long compId = ent.getKey();
			if (!ret.containsKey(compId)) {
				// NO - create an new entry in the ret map (compID, new
				// arraylist
				ret.put(compId, new LineScoreData());
			}

			// For each item in the list, get the score associated with it
			List<SpssInfo> val = ent.getValue();
			for (SpssInfo ci : val) {
				// get the score
				String spssId = ci.getSpssId();
				String score = scaleScoreMap.get(spssId);
				if (score == null) {
					continue;
				}

				// build the comp score info container data object
				IGScaleScoreData ssd = new IGScaleScoreData();
				ssd.setSpssVal(spssId);
				ssd.setRating(score);
				ssd.setIsNegative(ci.isInverted());
				ssd.setScaleName(IGConstants.SCALE_NAMES.get(spssId));

				// Stick the data object into the output object
				if (spssId.substring(0, 3).equals("ALR")) {
					ret.get(compId).getGpiColumnScores().add(ssd);
				} else {
					ret.get(compId).getCogColumnScores().add(ssd);
				}
			}
		}

		return ret;
	}

	/**
	 * getAlrScaleScores - Simple converter object to take the ALR scale ratings
	 * (ALP scales normed for ALR and converted to ratings) from the internal
	 * data object and convert them to the string objects required by the IG (a
	 * String object)
	 *
	 * @return - Map. key = Scale key, value = value (as a string)
	 */
	private Map<String, String> getKf4dScaleScores() throws Exception {
		HashMap<String, String> ret = new HashMap<String, String>();

		Kf4dScoreData sd = getKf4dScoreData();

		// Traits
		for (Iterator<Map.Entry<String, PrnData>> itr = sd.getTraits().entrySet().iterator(); itr.hasNext();) {
			Map.Entry<String, PrnData> ent = itr.next();

			log.debug("Rounding the scale score for: {}", ent.getKey());
			if (ent.getValue() == null){
				log.debug("Value for {} is null", ent.getKey());
				continue;
			}
			// round the scale ratings to 1 decimal
			BigDecimal bd = new BigDecimal(ent.getValue().getRate());
			bd = bd.setScale(1, RoundingMode.HALF_UP);
			ret.put(ent.getKey(), "" + bd.floatValue());
		}

		// Drivers
		for (Iterator<Map.Entry<String, PrnData>> itr = sd.getDrivers().entrySet().iterator(); itr.hasNext();) {
			Map.Entry<String, PrnData> ent = itr.next();

			// round the scale ratings to 1 decimal
			BigDecimal bd = new BigDecimal(ent.getValue().getRate());
			bd = bd.setScale(1, RoundingMode.HALF_UP);
			ret.put(ent.getKey(), "" + bd.floatValue());
		}

		return ret;
	}

	/**
	 * getTraits - used by the reporting code... Gets the trait (scale) data for
	 * reporting
	 *
	 * @return
	 * @throws Exception
	 */
	public Map<String, PrnData> getTraits() throws Exception {
		Map<String, PrnData> ret = new HashMap<String, PrnData>();
		Kf4dScoreData sd = getKf4dScoreData();

		ret.putAll(sd.getTraits());

		return ret;
	}

	/**
	 * getDrivers - used by the reporting code... Gets the Driver data for
	 * reporting
	 *
	 * @return
	 * @throws Exception
	 */
	public Map<String, PrnData> getDrivers() throws Exception {
		Map<String, PrnData> ret = new HashMap<String, PrnData>();
		Kf4dScoreData sd = getKf4dScoreData();

		ret.putAll(sd.getDrivers());

		return ret;
	}

	/**
	 * getRiskFactors - used by the reporting code... Gets the Risk Factor
	 * (derailer) data for reporting
	 *
	 * @return
	 * @throws Exception
	 */
	public Map<String, Integer> getRiskFactors() throws Exception {
		Map<String, Integer> ret = new HashMap<String, Integer>();
		Kf4dScoreData sd = getKf4dScoreData();

		ret.putAll(sd.getDerailers());

		return ret;
	}

	/**
	 * getStyleRecommendation - publicly expose the Leadership Style
	 * recommendation
	 *
	 * @return
	 * @throws Exception
	 */
	public int getStyleRecommendation() throws Exception {
		Kf4dScoreData sd = getKf4dScoreData();

		int val = 0;
		if (sd.getRecommended() != null && sd.getRecommended().get("RLS") != null) {
			val = sd.getRecommended().get("RLS");
		}

		return val;
	}

	/**
	 * getInterestRecommendation - publicly expose the Leadership Interest
	 * recommendation
	 *
	 * @return
	 * @throws Exception
	 */
	public int getInterestRecommendation() throws Exception {
		Kf4dScoreData sd = getKf4dScoreData();

		int val = 0;
		if (sd.getRecommended() != null && sd.getRecommended().get("RLI") != null) {
			val = sd.getRecommended().get("RLI");
		}

		return val;
	}

	/**
	 * getDerailmentRecommendation - publicly expose the Derailment Risk
	 * recommendation
	 *
	 * @return
	 * @throws Exception
	 */
	public int getDerailmentRecommendation() throws Exception {
		Kf4dScoreData sd = getKf4dScoreData();

		int val = 0;
		if (sd.getRecommended() != null && sd.getRecommended().get("RDR") != null) {
			val = sd.getRecommended().get("RDR");
		}

		return val;
	}

	/**
	 * getAdvancementRecommendation - publicly expose the Long-term Advancement
	 * Potential recommendation
	 *
	 * @return
	 * @throws Exception
	 */
	public int getAdvancementRecommendation() throws Exception {
		Kf4dScoreData sd = getKf4dScoreData();

		int val = 0;
		if (sd.getRecommended() != null && sd.getRecommended().get("RAP") != null) {
			val = sd.getRecommended().get("RAP");
		}

		return val;
	}

	/**
	 * getKf4dCogsDone - public method to expose the cogsDone flag
	 *
	 * @return
	 * @throws Exception
	 */
	public boolean getKf4dCogsDone() throws Exception {
		Kf4dScoreData sd = getKf4dScoreData();

		return sd.getCogsDone();
	}

	/**
	 * getKf4dCompScore - Gets the scale scores for the testing column for the
	 * detail data when "Edit" is pressed. Sticks the KF4D scores for a single
	 * competency into the GPI collection.
	 *
	 * @param compId
	 * @return
	 * @throws Exception
	 */
	public IGInstCompData getKf4dCompScore(long compId) throws Exception {
		// Create the comp array and the single IGInstCompData object in it
		IGInstCompData icData = new IGInstCompData();

		// set the comp id only... All the data should be available already
		// including the id
		icData.setCompId(compId);

		// Get the scale scores from the KF4D scoring.
		LineScoreData lsd = getKf4dDetailScores(compId);
		icData.setCogColumnScores(lsd.getCogColumnScores());
		icData.setGpiColumnScores(lsd.getGpiColumnScores());

		return icData;
	}

	/**
	 * getAlrInstScores - Gets the ALR scale scores (ALP scores normed for ALR)
	 * for a single competency. Called from getAlrCompScore().
	 *
	 * @param compId
	 * @return
	 * @throws Exception
	 */
	private LineScoreData getKf4dDetailScores(long compId) throws Exception {
		LineScoreData ret = new LineScoreData();

		// Get the scale scores
		Map<String, String> scores = getKf4dScaleScores();

		// need to add the cog scores if needed

		// get the key list for this comp
		List<SpssInfo> comps = getSpssForComp(compId);

		// loop through the list and get the ones for this comp
		ArrayList<IGScaleScoreData> cogs = new ArrayList<IGScaleScoreData>();
		ArrayList<IGScaleScoreData> kf4d = new ArrayList<IGScaleScoreData>();
		for (SpssInfo ci : comps) {
			String key = ci.getSpssId();
			boolean inverted = ci.isInverted();
			String val = IGConstants.SCALE_NAMES.get(key);
			if (val == null) {
				val = "N/A - " + key;
			}
			// Enclose the name in parenthesis if inverted
			if (inverted) {
				val = "(" + val + ")";
			}

			IGScaleScoreData ssd = new IGScaleScoreData();

			// truncate it and make it a string
			String rat = scores.get(key);
			if (rat == null) {
				rat = "N/A - " + key;
			}

			ssd.setRating(rat); // PDINH rating (empty)
			ssd.setScaleName(val); // Name of the scale
			ssd.setIsNegative(inverted);
			ssd.setSpssVal(key); // spss key

			if (key.substring(0, 3).equals("ALR")) {
				kf4d.add(ssd);
			} else {
				cogs.add(ssd);
			}
		}

		// Scram
		ret.setGpiColumnScores(kf4d);
		ret.setCogColumnScores(cogs);
		return ret;
	}

	/**
	 * getSpssForComp - Fetch the list of spss keys that are valid for the
	 * specified competency
	 *
	 * @param compId
	 * @return
	 * @throws Exception
	 */
	private List<SpssInfo> getSpssForComp(long compId) throws Exception {
		if (compSpssInfo == null) {
			this.compSpssInfo = (new Utils()).getCompSpssInfo(this.dnaId);
		}

		List<SpssInfo> ret = this.compSpssInfo.get(compId);
		if (ret == null) {
			// Make an empty one
			ret = new ArrayList<SpssInfo>();
		}

		return ret;
	}

	/**
	 * getKf4dRawData - get the raw score data for the extract
	 *
	 * @param ir
	 * @param partId
	 * @param projId
	 * @return
	 * @throws Exception
	 */
	public IndividualReport getKf4dRawData(IndividualReport ir, String partId, String projId) throws Exception {

		// Gather the Raw data to populate the displayData bucket in the IR
		Map<String, String> temp = new HashMap<String, String>();

		String json;
		try {
			json = getKf4dJsonScoreData();
		} catch (Exception e) {
			String err = "Unable to fetch KF4D score JSON for ppt " + this.partId + ", proj " + this.projId + ".  Msg="
					+ e.getMessage();
			System.out.println(err);
			throw new Exception(err);
		}

		if (!json.isEmpty()) {
			// Read up the JSON tree
			ObjectMapper mapper = new ObjectMapper();
			JsonNode root;
			try {
				root = mapper.readTree(json);
			} catch (JsonProcessingException e) {
				String err = "Unable to process raw KF4D score JSON for ppt " + this.partId + ", proj " + this.projId
						+ ".  Msg=" + e.getMessage();
				System.out.println(err);
				throw new Exception(err);
			} catch (IOException e) {
				String err = "IO error mapping raw KF4D score JSON for ppt " + this.partId + ", proj " + this.projId
						+ ".  Msg=" + e.getMessage();
				System.out.println(err);
				throw new Exception(err);
			}

			// Traits
			JsonNode traitsNode = root.path("results").path("candidate").path("traits");
			// System.out.println("Traits node: " + traitsNode.asText());
			if (traitsNode.isArray()) {
				for (JsonNode node : traitsNode) {
					String key = node.path("Ckey").asText();
					key = "r_" + key;
					String rScore = node.path("rawScore").asText();
					temp.put(key, rScore);
					log.debug("Traits node: {}, Score: {}", key, rScore);
				}
			}

			// Drivers
			JsonNode driversNode = root.path("results").path("candidate").path("drivers");
			// System.out.println("Drivers node: " + driversNode.asText());
			if (traitsNode.isArray()) {
				for (JsonNode node : driversNode) {
					String key = node.path("Ckey").asText();
					key = "r_" + key;
					String rScore = node.path("rawScore").asText();
					temp.put(key, rScore);
				}
			}

			// Competencies
			JsonNode compsNode = root.path("results").path("candidate").path("competencies");
			// System.out.println("Competencies node: " + compsNode.asText());
			if (traitsNode.isArray()) {
				for (JsonNode node : compsNode) {
					String key = node.path("Ckey").asText();
					key = "r_" + key;
					String rScore = node.path("rawScore").asText();
					temp.put(key, rScore);
				}
			}

			// TraitsSuperFactors
			JsonNode tsfNode = root.path("results").path("candidate").path("traitsSuperFactors");
			// System.out.println("TraitsSuperFactors node: " +
			// tsfNode.asText());
			if (traitsNode.isArray()) {
				for (JsonNode node : tsfNode) {
					String key = node.path("Ckey").asText();
					key = "r_" + key;
					String rScore = node.path("rawScore").asText();
					temp.put(key, rScore);
				}
			}
		}
		// stuff the raw score data into the Individual Report
		ir.getDisplayData().putAll(temp);

		// Scram
		return ir;
	}

	

	private PrnData getRv2Rating(String partId, long projId) throws Exception {
		int ravScore = 0;
		Rv2Helper rv2Helper = new Rv2Helper();
		try {
			//ravScore = getRv2RawScore(partId, projId);
			ravScore = rv2Helper.getRv2RawScore(partId, projId);
			log.debug("Got RV2 Raw Score: {}", ravScore);
			this.setRv2Rawscore(ravScore);
		} catch (Exception e) {
			// Pass it on along
			throw new Exception(e);
		}
		if (ravScore == -1) {
			return null;
		}

		// Norm it
		PrnData prn = Rv2Helper.doRavens2Norming(ravScore);
		this.setRv2Rating(String.valueOf(prn.getRate()));
		return prn;
	}
	
/*
	private PrnData getRvRating(String partId2, long projId2) {
		double scaleScore = -1;
		StringBuffer sqlQuery = new StringBuffer();
		Statement stmt = null;
		ResultSet rs = null;
		String phase = "";
		phase = "Score query execution";
		sqlQuery.append("SELECT rr.spssValue AS spssValue, ");
		sqlQuery.append("       rr.rawScore AS scaleScore ");
		sqlQuery.append("  FROM results rr ");
		sqlQuery.append("  WHERE rr.participantId = " + partId + " ");
		sqlQuery.append("    AND rr.instrumentCode IN ('rv') ");
		sqlQuery.append("  ORDER BY rr.spssValue, rr.lastDate");
		
		try {
			phase = "Norm query data retrieval";
			stmt = con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());
			if (! rs.isBeforeFirst()){
				return null;
			} else {
				while (rs.next())
				{
					scaleScore = rs.getDouble("scaleScore");
					break;
					
				}
			}
		} catch (SQLException e) {
			log.error("Error occurred while attempting to retrieve RV score: {}", e.getMessage());
			e.printStackTrace();
			return null;
		}
		
		// Norm it

		PrnData prn = Rv2Helper.doRavens2Norming(scaleScore);
		this.setRv2Rating(String.valueOf(prn.getRate()));
		return prn;
		
		
	}
	*/
	/*
	private PrnData doRavens2Norming(double scaleScore){
		// Norm it
		Norm nn = new Norm(SPEC_RAV_MEAN, SPEC_RAV_STDEV);
		log.debug("Using Mean: {}, and STD DEV: {}", SPEC_RAV_MEAN, SPEC_RAV_STDEV);
		double z = nn.calcZScore(scaleScore);
		log.debug("RV2 zScore: {}", z);
		int pctl = Norm.calcIntPctlFromZ(z);
		log.debug("RV2 Percentile: {}", pctl);
		String rate = Norm.calcPdiRating(pctl);

		log.debug("Calculated RV2 Rating: {}", rate);
		this.setRv2Rating(rate);
		// OK
		PrnData ret = new PrnData();
		ret.setRate(Float.parseFloat(rate));
		return ret;
	}
	*/

	/**
	 * getRv2RawScore returns the raw score for the ravens, a -1 (no scores
	 * available), or throws an exception
	 *
	 * @param partId
	 * @param projId
	 * @return
	 * @throws Exception
	 */
	/*
	public int getRv2RawScore(String partId, long projId) throws Exception {
		URL url = null;
		URLConnection uc = null;
		InputStream content = null;

		// Set up and callthe RESTful service
		String surl = PropertyLoader.getProperty("com.pdi.data.abyd.application", "palms.tincanRestServer.url");
		if (surl.charAt(surl.length() - 1) == '/') {
			surl = surl.substring(0, surl.length() - 2);
		}
		surl += RV2_RAW_SCORE_STR;

		surl = surl.replace("{pptId}", partId);
		surl = surl.replace("{projId}", "" + projId);

		try {
			url = new URL(surl);
		} catch (MalformedURLException e) {
			String err = "IGKf4dDataHelper.getRv2Rating() - Malformed URL - " + surl;
			System.out.println(err);
			throw new Exception(err);
		}

		// open the connection.
		try {
			uc = url.openConnection();
		} catch (IOException e) {
			String err = "IGKf4dDataHelper.getRv2Rating() - Unable to open connection.  URL=" + surl + ".  Msg="
					+ e.getMessage();
			System.out.println(err);
			throw new Exception(err);
		}

		// Get the data
		HttpURLConnection huc = (HttpURLConnection) uc;
		boolean hasScores = true;
		try {
			int statusCode = huc.getResponseCode();
			if (statusCode == HttpServletResponse.SC_NOT_FOUND || statusCode == HttpServletResponse.SC_NO_CONTENT
					|| statusCode == HttpServletResponse.SC_INTERNAL_SERVER_ERROR) {
				// String str = "There is no KF4D scoring information available
				// for participant " + this.partId + " in project " +
				// this.projId + " (" + statusCode + ").";
				hasScores = false;
			} else if (statusCode >= 200 && statusCode < 300) {
				content = huc.getInputStream();
			} else {
				String str = "Error returned from KF4D score fetch (getRv2Rating): Code=" + statusCode + " for URL="
						+ surl + ".  Message=" + huc.getResponseMessage();
				System.out.println(str);
				throw new Exception(str);
			}
		} catch (IOException e) {
			String err = "getRv2Rating: Error fetching score JSON string.  Msg=" + e.getMessage();
			System.out.println(err);
			throw new Exception(err);
		}

		StringBuffer xml = new StringBuffer();
		if (hasScores) {
			BufferedReader br = new BufferedReader(new InputStreamReader(content));
			String line;
			try {
				while ((line = br.readLine()) != null) {
					xml.append(line);
				}
			} catch (IOException e) {
				String err = "IGKf4dDataHelper.getRv2Rating() - Unable to read content.  URL=" + surl + ".  Msg="
						+ e.getMessage();
				System.out.println(err);
				throw new Exception(err);
			}
		} else {
			// No scores... send back a bogus score
			return -1;
		}

		// Get the responses
		DocumentBuilderFactory dbfac = null;
		DocumentBuilder docBuilder = null;
		Document doc = null;
		try {
			dbfac = DocumentBuilderFactory.newInstance();
			docBuilder = dbfac.newDocumentBuilder();
			doc = docBuilder.parse(new InputSource(new StringReader(xml.toString())));
		} catch (ParserConfigurationException pce) {
			String err = "IGKf4dDataHelper.getRv2Rating() - XML parser config ERROR: ppt=" + partId + ", proj=" + projId
					+ ", xml=" + xml + ", msg=" + pce.getMessage();
			System.out.println(err);
			throw new Exception(err);
		} catch (SAXException se) {
			String err = "IGKf4dDataHelper.getRv2Rating() - XML SAX ERROR: ppt=" + partId + ", proj=" + projId
					+ ", xml=" + xml + ", msg=" + se.getMessage();
			System.out.println(err);
			throw new Exception(err);
		} catch (IOException ie) {
			String err = "IGKf4dDataHelper.getRv2Rating() - XML I/O error: ppt=" + partId + ", proj=" + projId
					+ ", xml=" + xml + ", msg=" + ie.getMessage();
			System.out.println(err);
			throw new Exception(err);
		}

		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xpath = xPathfactory.newXPath();

		NodeList nodeList;
		try {
			nodeList = (NodeList) xpath.compile("/scoringExtract/dataset/integerDataArray").evaluate(doc,
					XPathConstants.NODESET);
		} catch (XPathExpressionException xe) {
			String err = "IGKf4dDataHelper.getRv2Rating() - XML I/O error: ppt=" + partId + ", proj=" + projId
					+ ", xml=" + xml + ", msg=" + xe.getMessage();
			System.out.println(err);
			throw new Exception(err);
		}

		// int size = nodeList.getLength();
		List<Integer> scores = new ArrayList<Integer>();
		for (int index = 0; index < nodeList.getLength(); index++) {
			Element e = (Element) nodeList.item(index);
			// String name = e.getAttribute("id").toUpperCase();
			NodeList nl = e.getChildNodes();

			for (int i2 = 0; i2 < nl.getLength(); i2++) {
				Element e2 = (Element) nl.item(i2);
				String val = e2.getTextContent();
				if (val == null || val.isEmpty() || val.equals("NaN")) {
					continue;
				}
				Integer sVal = Integer.parseInt(val);
				scores.add(i2, sVal);
			} // end of "value" loop
		} // end of ravens loop
		if (scores.size() != RAVENS_KEY.size()) {
			String err = "IGKf4dDataHelper.getRv2Rating() - Incomplete Ravens score data.  Only " + scores.size()
					+ " of " + RAVENS_KEY.size() + " responses available. Ravens Not Scored.";
			System.out.println(err);
			throw new Exception(err);
		}

		// Score
		int ravScore = 0;
		for (int idx = 0; idx < RAVENS_KEY.size(); idx++) {
			int val = scores.get(idx);
			if (RAVENS_KEY.get(idx) == val) {
				ravScore++;
			}
		}
		return ravScore;
	}
	*/

	// ******************** Private classes *******************

	/**
	 * Kf4dScoreData - A thin class to carry data needed by the IG and related
	 * screens when using KF4D data.
	 */
	private class Kf4dScoreData {

		//
		// Instance data.
		//

		private boolean cogsDone = false;

		private final Map<String, PrnData> traits = new HashMap<String, PrnData>();
		private final Map<String, PrnData> drivers = new HashMap<String, PrnData>();
		private final Map<String, Integer> recommended = new HashMap<String, Integer>();
		private final Map<String, Integer> derailers = new HashMap<String, Integer>();

		private final Map<Long, Float> compScores = new HashMap<Long, Float>();

		//
		// Constructors.
		//
		public Kf4dScoreData() {
			// default constructor
		}

		//
		// Instance methods.
		//

		/*****************************************************************************************/
		public boolean getCogsDone() {
			return this.cogsDone;
		}

		public void setCogsDone(boolean value) {
			this.cogsDone = value;
		}

		/*****************************************************************************************/
		public Map<String, PrnData> getTraits() {
			return this.traits;
		}
		//
		// public void setTraits(Map<String, PrnData> value)
		// {
		// this.traits = value;
		// }

		/*****************************************************************************************/
		public Map<String, PrnData> getDrivers() {
			return this.drivers;
		}
		//
		// public void setDrivers(Map<String, PrnData> value)
		// {
		// this.drivers = value;
		// }

		/*****************************************************************************************/
		public Map<String, Integer> getRecommended() {
			return this.recommended;
		}
		//
		// public void setRecommended(Map<String, Integer> value)
		// {
		// this.recommended = value;
		// }

		/*****************************************************************************************/
		public Map<String, Integer> getDerailers() {
			return this.derailers;
		}
		//
		// public void setDerailers(Map<String, Integer> value)
		// {
		// this.derailers = value;
		// }

		/*****************************************************************************************/
		public Map<Long, Float> getCompScores() {
			return this.compScores;
		}
		//
		// public void setCompScores(Map<Long, Float> value)
		// {
		// this.compScores = value;
		// }
	} // end of private class

	public int getRv2Rawscore() {
		return rv2Rawscore;
	}

	public void setRv2Rawscore(int rv2Rawscore) {
		this.rv2Rawscore = rv2Rawscore;
	}

	public String getRv2Rating() {
		return rv2Rating;
	}

	public void setRv2Rating(String rv2Rating) {
		this.rv2Rating = rv2Rating;
	}

	public String getTestInstrument() {
		return testInstrument;
	}

	public void setTestInstrument(String testInstrument) {
		this.testInstrument = testInstrument;
	}
}
