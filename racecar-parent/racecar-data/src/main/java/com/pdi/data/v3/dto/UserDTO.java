/**
 * Copyright (c) 2008 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.dto;

import org.w3c.dom.Node;

import com.pdi.data.util.BaseDTO;
import com.pdi.data.util.RequestStatus;
import com.pdi.data.v3.dto.IndividualDTO;
import com.pdi.xml.XMLUtils;

/**
 * UserDTO is a wrapper around some user data
 * 
 * @author		Gavin Meyers
  */
public class UserDTO extends BaseDTO
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _id;
	private String _username;
	private IndividualDTO _individual;
	private String _language;
	private String _version;

	//
	// Constructors.
	//
	
	/**
	 * "Standard" no param constructor - no additional functionality
	 */
	public UserDTO()
	{
		super();
		// Does nothing presently
	}
	
	/**
	 * "Standard" no param constructor - no additional functionality
	 */
	public UserDTO(RequestStatus stat)
	{
		super(stat);
		// Does nothing presently
	}
	
	/**
	 * Builds the object based off an xml node
	 * @param node
	 */
	public UserDTO(Node node)
	{
		/*
		 * The XML should look something like this:
		 * <user:user xmlns:user="urn:iscopia.com:user-1.0">
		 *   <username>jsmith</username>
		 *   <familyName>Smith</familyName>
		 *   <givenName>John</givenName>
		 *   <telephone>514-555-1212</telephone>
		 *   <mobile>514-555-8989</mobile>
		 *   <email>jsmith@iscopia.com</email>
		 *   <languageCode>fr</languageCode>
		 *   <version>1</version>
		 * </user:user>
		 */

		this.setUsername(XMLUtils.getElementValue(node, "username/."));
		this.setLanguage(XMLUtils.getElementValue(node, "languageCode/."));
		this.setVersion(XMLUtils.getElementValue(node, "version/."));
		this.setIndividual(new IndividualDTO(node));

		if (this.getUsername() == null &&
			this.getLanguage() == null &&
			this.getVersion() == null &&
			this.getIndividual().getStatus().getStatusCode() == RequestStatus.WB_NO_DATA)
		{
			this.getStatus().setStatusCode(RequestStatus.WB_NO_DATA);
		}
	}
	
	
	/**
	 * Converts a session user into a regular user
	 * @param sessionUser
	 */
	public UserDTO(SessionUserDTO sessionUser)
	{
		this.setStatus(sessionUser.getStatus());
		this.setId(sessionUser.getId());
		this.setIndividual(sessionUser.getIndividual());
		this.setLanguage(sessionUser.getLanguage());
		this.setUsername(sessionUser.getUsername());
		this.setVersion(sessionUser.getVersion());
	}

	//
	// Instance methods.
	//
	
	/**
	 * Generic toString method
	 */
	public String toString()
	{
		String str = "\n --- UserDTO --- \n";
		str += "\nusername:            " + this.getUsername();
		str += this.getIndividual().toString();
		return str;
	}
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
	
	/*****************************************************************************************/
	public void setId(String value)
	{
		_id = value;
	}
	
	public String getId()
	{
		return _id;
	}
	
	/*****************************************************************************************/
	public void setUsername(String value)
	{
		_username = value;
	}
	
	public String getUsername()
	{
		return _username;
	}
	
	/*****************************************************************************************/
	public void setIndividual(IndividualDTO value)
	{
		_individual = value;
	}
	
	public IndividualDTO getIndividual()
	{
		return _individual;
	}
	
	/*****************************************************************************************/
	public void setLanguage(String value)
	{
		_language = value;
	}
	
	public String getLanguage()
	{
		return _language;
	}
	
	/*****************************************************************************************/
	public void setVersion(String value)
	{
		_version = value;
	}
	
	public String getVersion()
	{
		return _version;
	}
}
