/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.adapt.util;

//import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.w3c.dom.Document;
//import org.xml.sax.InputSource;

import com.pdi.properties.PropertyLoader;

public class AdaptWebserviceUtils
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//

	//
	// Constructors.
	//

	//
	// Instance methods.
	//
	
	/*
	 * Sends request to the Cast Iron server for ADAPT data
	 * @param xml - the xml to send
	 * @param engagement - ADAPT engagement id
	 * @param - ADAPT person id
	 * @return A Document object containing the response
	 */
	public Document requestAdaptData(long engagement, long person)
		throws Exception
	{
		String req = PropertyLoader.getProperty("com.pdi.data.adapt.application", "castIron.answerUrl");
		req += "?pid=" + person + "&eid=" + engagement;
		System.out.println("Request string = " + req);
		PostMethod post = new PostMethod(req);

		HttpClient client = new HttpClient();
		int result = client.executeMethod(post);
		if (result != 200)
		{
			throw new Exception("Data not retrieved.  Status = " + result);
		}
		
		String strXMLResponse = post.getResponseBodyAsString();
		//in error situations v2 returns an invalid version
		strXMLResponse = strXMLResponse.replace("version=\"\"", "version=\"1.0\"");

//		System.out.println("-- COMING BACK --");
//		System.out.println(strXMLResponse);
//		System.out.println("-- DONE COMING BACK --");
		
		DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
		//Document doc = docBuilder.parse(new InputSource(new StringReader(strXMLResponse)));
		Document doc = docBuilder.parse(post.getResponseBodyAsStream());

		
		return doc;
	}
}
