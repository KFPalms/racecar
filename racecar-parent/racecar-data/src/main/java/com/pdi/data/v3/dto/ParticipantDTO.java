/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.dto;

import java.util.ArrayList;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.pdi.data.util.BaseDTO;
import com.pdi.data.util.RequestStatus;
import com.pdi.xml.XMLUtils;

/**
 * DTO for participant information
 * 
 * @author		Gavin Myers
 * @author		Ken Beukelman
 */
public class ParticipantDTO extends BaseDTO
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _id;
	private CandidateDTO _candidate;
	private String _projectId;
	private String _uri;
	private String _version;
	private ArrayList<ParticipationDTO> _participations;
	private String _businessUnit;

	//
	// Constructors.
	//

	/**
	 * "Standard" (no-parameter) constructor
	 */
	public ParticipantDTO()
	{
		super();
		// Does nothing presently
	}
	
	/**
	 * Builds the object based off an xml node
	 * @param node
	 */
	public ParticipantDTO(Node node)
	{
		super();
		refresh(node);
	}


	//
	// Instance methods.
	//
	
	/**
	 * Refresh the data in this object based upon a node input.
	 * @param node
	 */
	public void refresh(Node node)
	{
		// We could be sending a list node or a result node.  Check for both.
		/*
		 * From the Participant list, the XML looks like this:
		 * <ns2:participantListLine>
		 *   <identifier>TSJSP-MFTXY</identifier>
		 *   <candidateIdentifier>COLH-HHNR</candidateIdentifier>
		 *   <projectIdentifier>PWDQ-KPQN</projectIdentifier>
		 *   <firstName>Chauncey</firstName>
		 *   <lastName>Testuser</lastName>
		 *   <email>cdunn@pdi-corp.com</email>
		 *   <uri>https://platform.iscopia.com/webservices1/rest/v1/project/PWDQ-KPQN/participant/TSJSP-MFTXY</uri>
		 * </ns2:participantListLine>
		 * 
		 * Note the the URI is unique here and tells us if we are filling a proxy or the real McCoy
		 * 
		 * From the project uri (result node), the XML looks like this:
		 * <ns3:resultParticipant>
		 *   <projectIdentifier>PWDQ-KPQN</projectIdentifier>
		 *   <candidateIdentifier>COLH-HHNR</candidateIdentifier>
		 *   <firstName>Chauncey</firstName>
		 *   <lastName>Testuser</lastName>
		 *   <email>cdunn@pdi-corp.com</email>
		 *   <identifier>TSJSP-MFTXY</identifier>
		 *   <version>1</version>
		 *   <ns3:participationList>
		 *     <ns3:participation>
		 *       <status>IN_PROGRESS</status>
		 *       <outcome>NOT_AVAILABLE</outcome>
		 *       <startedOn>2009-09-02T16:30:15.000-0400</startedOn>
		 *       <externalReferenceId>4825A19C-E936-47E9-93B8-8907D25C5D18</externalReferenceId>
		 *       <answers>https://platform.iscopia.com/webservices1/rest/v1/project/PWDQ-KPQN/participant/TSJSP-MFTXY/answers/13f5fcdd-7f0c-4b5f-96af-a92d4c010086</answers>
		 *       <results>https://platform.iscopia.com/webservices1/rest/v1/project/PWDQ-KPQN/participant/TSJSP-MFTXY/results/13f5fcdd-7f0c-4b5f-96af-a92d4c010086</results>
		 *       <ns2:activity>
		 *         <identifier>13f5fcdd-7f0c-4b5f-96af-a92d4c010086</identifier>
		 *         <type>ASSESS</type>
		 *         <productIdentifier>4F97F776-A007-4387-8146-A5386D6BEFCC</productIdentifier>
		 *         <ns2:reportList/>
		 *       </ns2:activity>
		 *     </ns3:participation>
		 *   </ns3:participationList>
		 * </ns3:resultParticipant>
		 */
		
		// initialize the status to OK
		this.setStatus(new RequestStatus());
		
		// This works, because the participant list has proxy data only plus the URI.  If the
		// URI attribute is present, the Node must be from the list and contains proxy data only.
		String uri = XMLUtils.getElementValue(node, "uri/.");
		this.setProxy((uri == null ? FILLED : PROXY));
		
		this.setId(XMLUtils.getElementValue(node, "identifier/."));
		this.setCandidate(new CandidateDTO(node));	// CandidateDTO recognizes that I am coming from here
		this.setProjectId(XMLUtils.getElementValue(node, "projectIdentifier/."));
		if (this.isProxy())
		{
			this.setUri(uri);
		}
		else
		{
			//If we are filling this we need to add the participations proxies... set 'em up
			NodeList nodes = XMLUtils.getElements(node, "participationList/participation");
			if(nodes != null)
			{
				// create the list
				this.setParticipations(new ArrayList<ParticipationDTO>());
				
				// Put the participations into the list, but not if they are empty
				for(int i = 0; i < nodes.getLength(); i++)
				{
					Node node2 = nodes.item(i);
					ParticipationDTO dto = new ParticipationDTO(node2);
					if (dto.getStatus().getStatusCode() != RequestStatus.WB_NO_DATA)
					{
						this.getParticipations().add(dto);
					}
				}
			}
		}
		
		if (this.getId() == null &&
				this.getCandidate().getStatus().getStatusCode() == RequestStatus.WB_NO_DATA &&
				this.getProjectId() == null &&
				this.getUri() == null &&
				(this.getParticipations() == null || this.getParticipations().size() == 0))
		{
			this.getStatus().setStatusCode(RequestStatus.WB_NO_DATA);
		}
		
		return;
	}

	
	/**
	 * Generic toString method
	 */
	public String toString()
	{
		String str = "\n --- ParticipantDTO --- \n";
		str += this.getStatus().toString();
		str += "\nProxy:        " + this.getProxy();
		str += "\nID:           " + _id;
		str += this.getCandidate().toString();
		str += "\nProjectId:    " + _projectId;
		str += "\nURI:          " + _uri;
		str += "\nVersion:      " + _version;
		if (this.getParticipations() == null)
		{
			str += "\nNo Participations available";
		}
		else
		{
			for(ParticipationDTO participation : _participations)
			{
				str += participation.toString();
			}
		}
		
		return str;
	}	


	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
	
	/*****************************************************************************************/
	public void setId(String value)
	{
		_id = value;
	}
	
	public String getId()
	{
		return _id;
	}
	
	/*****************************************************************************************/
	public void setCandidate(CandidateDTO value)
	{
		_candidate = value;
	}
	
	public CandidateDTO getCandidate()
	{
		return _candidate;
	}
	
	/*****************************************************************************************/
	public void setProjectId(String value)
	{
		_projectId = value;
	}
	
	public String getProjectId()
	{
		return _projectId;
	}
	
	/*****************************************************************************************/
	public void setUri(String value)
	{
		_uri = value;
	}
	
	public String getUri()
	{
		return _uri;
	}
	
	/*****************************************************************************************/
	public void setVersion(String value)
	{
		_version = value;
	}
	
	public String getVersion()
	{
		return _version;
	}
	
	/*****************************************************************************************/
	public void setParticipations(ArrayList<ParticipationDTO> value)
	{
		_participations = value;
	}
	
	public ArrayList<ParticipationDTO> getParticipations()
	{
		return _participations;
	}

	public void setBusinessUnit(String _businessUnit) {
		this._businessUnit = _businessUnit;
	}

	public String getBusinessUnit() {
		return _businessUnit;
	}
	
	
	
	
/////////////////////	
//  Original Code  //
/////////////////////	
//	//
//	// Static data.
//	//
//
//	//
//	// Static methods.
//	//
//
//	//
//	// Instance data.
//	//
//	private String _id;
//	private String _uri;
//	private ProjectDTO _project;
//	private CandidateDTO _candidate;
//	private ArrayList<ParticipationDTO> _participations = new ArrayList<ParticipationDTO>();
//
//	//
//	// Constructors.
//	//
//
//	/**
//	 * "Standard" (no-parameter) constructor
//	 */
//	public ParticipantDTO()
//	{
//		super();
//		// Does nothing presently
//	}
//	
//	/**
//	 * Builds the object based off an xml node
//	 * @param node
//	 * @throws Exception 
//	 * @throws Exception
//	 */
//	public ParticipantDTO(Node node)
//	{
//		refresh(node);
//	}
//	
//	public void refresh(Node node)
//	{
//		/*
//		 * The XML looks something like this:
//		 * <?xml version="1.0" encoding="utf-8" standalone="yes"?>
//		 * <ns3:resultParticipant xmlns:ns2="urn:iscopia.com:business-commons-1.0"
//		 * 					      xmlns:ns3="urn:iscopia.com:participant-1.0">
//		 *   <projectIdentifier>PCOJ-VNSK</projectIdentifier>
//		 *   <candidateIdentifier>CWNL-QWAA</candidateIdentifier>
//		 *   <firstName>q</firstName>
//		 *   <lastName>q</lastName>
//		 *   <email>q@iscopia.com</email>
//		 *   <telephone>q</telephone>
//		 *   <identifier>TOXPN-OKJTW</identifier>
//		 *   <version>1</version>
//		 *   <ns3:participationList>
//		 *     <ns3:participation>
//		 *       <status>IN_PROGRESS</status>
//		 *       <outcome>NOT_AVAILABLE</outcome>
//		 *       <startedOn>2009-08-18T07:10:38.000-0400</startedOn>
//		 *       <externalReferenceId>85946D39-5F94-4629-AF16-9754EFE44652</externalReferenceId>
//		 *       <answers>https://platform.iscopia.com/webservices1/rest/v1/project/PCOJ-VNSK/participant/TOXPN-OKJTW/answers/c6b06dca-acd9-44ec-ab9d-ede1533c963f</answers>
//		 *       <results>https://platform.iscopia.com/webservices1/rest/v1/project/PCOJ-VNSK/participant/TOXPN-OKJTW/results/c6b06dca-acd9-44ec-ab9d-ede1533c963f</results>
//		 *       <ns2:activity>
//		 *         <identifier>c6b06dca-acd9-44ec-ab9d-ede1533c963f</identifier>
//		 *         <type>ASSESS</type>
//		 *         <productIdentifier>4F97F776-A007-4387-8146-A5386D6BEFCC</productIdentifier>
//		 *       </ns2:activity>
//		 *     </ns3:participation>
//		 *   </ns3:participationList>
//		 * </ns3:resultParticipant>
//		 */
//
//		this.setId(XMLUtils.getElementValue(node, "identifier/."));
//		if(XMLUtils.getElementValue(node, "uri/.") != null) {
//			this.setUri(XMLUtils.getElementValue(node, "uri/."));
//		}
//		this.setProject(new ProjectDTO(node, "xxx"));
//		this.setCandidate(new CandidateDTO(node));	// CandidateDTO recognizes that I am coming from here
//		
//		NodeList nodes = XMLUtils.getElements(node, "participationList/participation");
//		
//		//System.out.println(XMLUtils.nodeToString(node));
//		
//		if(nodes != null)
//		{
//			// Put the participations into the list, but not if they are empty
//			for(int i = 0; i < nodes.getLength(); i++)
//			{
//				Node node2 = nodes.item(i);
//				ParticipationDTO dto = new ParticipationDTO(node2);
//				if (dto.getStatus().getStatusCode() != RequestStatus.WB_NO_DATA)
//				{
//					this.getParticipations().add(dto);
//				}
//			}
//		}
//		
//		if (this.getId() == null &&
//			this.getUri() == null &&
//			this.getProject().getStatus().getStatusCode() == RequestStatus.WB_NO_DATA &&
//			this.getCandidate().getStatus().getStatusCode() == RequestStatus.WB_NO_DATA &&
//			(this.getParticipations() == null || this.getParticipations().size() == 0))
//		{
//			this.getStatus().setStatusCode(RequestStatus.WB_NO_DATA);
//		}
//	}
//
//	//
//	// Instance methods.
//	//
//	
//	/**
//	 * Generic toString method
//	 */
//	public String toString() {
//		String str = "\n --- ParticipantDTO --- \n";
//		str += "\nID:               " + this.getId();
//		str += "\nURI:  			" + this.getUri();
//		str += this.getProject().toString();
//		str += this.getCandidate().toString();
//		for(ParticipationDTO participation : this.getParticipations())
//		{
//			str += participation.toString();
//		}
//		
//		return str;
//	}	
//	
//	//*******************************************************************************************//
//	//																							 //
//	//                                       Get/Set Functions                                   //
//	//																							 //
//	//*******************************************************************************************//
//	
//	/*****************************************************************************************/
//	public void setUri(String value)
//	{
//		_uri = value;
//	}
//	
//	public String getUri()
//	{
//		return _uri;
//	}
//	
//	/*****************************************************************************************/
//	public void setProject(ProjectDTO value)
//	{
//		_project = value;
//	}
//	
//	public ProjectDTO getProject()
//	{
//		return _project;
//	}
//	
//	/*****************************************************************************************/
//	public void setId(String value)
//	{
//		_id = value;
//	}
//	
//	public String getId()
//	{
//		return _id;
//	}
//	
//	/*****************************************************************************************/
//	public void setCandidate(CandidateDTO value)
//	{
//		_candidate = value;
//	}
//	
//	public CandidateDTO getCandidate()
//	{
//		return _candidate;
//	}
//	
//	/*****************************************************************************************/
//	public void setParticipations(ArrayList<ParticipationDTO> value)
//	{
//		_participations = value;
//	}
//	
//	public ArrayList<ParticipationDTO> getParticipations()
//	{
//		return _participations;
//	}
}
