/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.dto;

import com.pdi.data.util.BaseDTO;
import com.pdi.data.util.RequestStatus;

/**
 * EmailGroupDTO is a class designed to convey the information for one
 * e-mail group type and its request status used in the PDI V3 extensions.
 * 
 * @author		Ken Beukelman
  */
public class EmailGroupDTO extends BaseDTO
{
	//
	// Static data.
	//

	// Group type codes
	public static final String GLOBAL = "GL";		// GLobal
	public static final String CLIENT = "CL";		// CLient
	public static final String PRODUCT = "PD";		// ProDuct
	public static final String PROD_CLIENT = "PC";  // Product-Client


	//
	// Static methods.
	//


	//
	// Instance data.
	//
	private long _id;
	private String _code;
	private String _name = null;
	private String _description = null;


	//
	// Constructors.
	//
	public EmailGroupDTO()
	{
		super();
		// does nothing right now
	}
	
	// Status setup constructor - group data remains unset
	public EmailGroupDTO(RequestStatus stat)
	{
		super();
		this.setStatus(stat);
	}
	
	
	//
	// Instance methods.
	//

	/**
	 * Generic toString method
	 */
	public String toString()
	{
		String str = "\nEmailGroupDTO:";
		str +=  this.getStatus().toString();
		str += "\nID:           " + _id;
		str += "\nCode:         " + _code;
		str += "\nName:         " + _name;
		str += "\nDescription:  " + _description;
		
		return str;
	}
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public long getId()
	{
		return _id;
	}
	
	public void setId(long value)
	{
		_id = value;
	}	

	/*****************************************************************************************/
	public String getCode()
	{
		return _code;
	}
	
	public void setCode(String value)
	{
		_code = value;
	}	

	/*****************************************************************************************/
	public String getName()
	{
		return _name;
	}
	
	public void setName(String value)
	{
		_name = value;
	}	

	/*****************************************************************************************/
	public String getDescription()
	{
		return _description;
	}
	
	public void setDescription(String value)
	{
		_description = value;
	}	
}
