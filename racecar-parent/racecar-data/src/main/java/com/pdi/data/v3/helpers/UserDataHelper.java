/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.helpers;

import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.pdi.data.util.RequestStatus;
import com.pdi.data.v3.dto.DocumentDTO;
import com.pdi.data.v3.dto.SessionUserDTO;
import com.pdi.data.v3.dto.UserDTO;
import com.pdi.data.v3.dto.UserListDTO;
import com.pdi.data.v3.util.V3WebserviceUtils;
import com.pdi.properties.PropertyLoader;
import com.pdi.xml.XMLUtils;

/**
 * UserDataHelper provides functionality to fetch a user or a list of users.
 *
 * @author		Ken Beukelman
 * @author		MB Panichi
 */
public class UserDataHelper
{

	//
	// Static data.
	//

	//
	// Static methods.
	//
	
	/**
	 * Get all projects a user has access to
	 * @param sessionUser
	 * @return
	 */
//TODO 10/6/09-Note that this code is untested as it is not currently exposed in PortalFlexService
	public static UserListDTO getUsers(SessionUserDTO sessionUser)
	{
		UserListDTO ret = new UserListDTO();
		
		DocumentDTO dto = V3WebserviceUtils.request(sessionUser,"user/"+PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.code")+"/list","");
		if (! dto.getStatus().isOk())
		{
			ret.setStatus(dto.getStatus());
			return ret;
		}
		
		
		Document response = dto.getDocument();
		if(response == null)
		{
			ret.setStatus(new RequestStatus(RequestStatus.WB_NO_DATA, "No users were found"));
			return ret;
		}
		
		ArrayList<UserDTO> results = new ArrayList<UserDTO>();
		NodeList nodes = XMLUtils.getElements(response, "//userListLine");
		for(int i = 0; i < nodes.getLength(); i++)
		{
			Node node = nodes.item(i);
			results.add(new UserDTO(node));
		}
		ret.setUserList(results);
		
		return ret;
	}


	/**
	 * Builds a session user object
	 * @param username
	 * @param password
	 * @return
	 */
	public static SessionUserDTO getUser(String username, String password)
	{
		//https://pre-prod.iscopia.com/webservices1/rest/v1/user/OPDI-OPDI/gmyers
		
		SessionUserDTO sessionUser = new SessionUserDTO();
		sessionUser.setUsername(username);
		sessionUser.setPassword(password);
		DocumentDTO dto = V3WebserviceUtils.request(sessionUser,"user/"+PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.code")+"/"+username,"");
		if (! dto.getStatus().isOk())
		{
			sessionUser.setStatus(dto.getStatus());
			return sessionUser;
		}
		
		Document response = dto.getDocument();
		if (response == null)
		{
			sessionUser.setStatus(new RequestStatus(RequestStatus.WB_NO_DATA,"No User data available."));
			return sessionUser;
		}
		
		// Replace the session user with one using the data we just got
		try
		{
			NodeList nodes = XMLUtils.getElements(response, "//ResultUser");
			sessionUser  = new SessionUserDTO(nodes.item(0));
		}
		catch (Exception e)
		{
			sessionUser.setStatus(new RequestStatus(RequestStatus.RS_ERROR, "Unable to extract data from User XML.  Msg=" + e.getMessage()));
		}
		sessionUser.setUsername(username);
		sessionUser.setPassword(password);
		
		return sessionUser;
	}

	//
	// Instance data.
	//

	//
	// Constructors.
	//

	//
	// Instance methods.
	//
}
