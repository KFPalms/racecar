package com.pdi.data.nhn.helpers.delegated;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import com.pdi.data.dto.Instrument;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.interfaces.IInstrumentHelper;
import com.pdi.data.nhn.util.NhnDatabaseUtils;

import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;

import com.pdi.logging.LogWriter;

public class InstrumentHelper implements IInstrumentHelper {

	//@Override
	public Instrument fromId(SessionUser session, String instrumentId) {
		Instrument instrument = new Instrument();		
		StringBuffer sb = new StringBuffer();		
		sb.append("select " +
				"c.course, " +
				"c.abbv, " +
				"c.title " +
				"from platform.dbo.course c " +
				"where abbv = '" + instrumentId + "'");

		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(sb);
		if (dlps.isInError()) {
			LogWriter.logSQL(LogWriter.WARNING, this, 
					"Error preparing Instrument fromId(SessionUser session, String instrumentId) query." + dlps.toString(), sb.toString());
		}
		DataResult dr = null;
		ResultSet rs = null;
		try		{
			dr = NhnDatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			if (rs != null && rs.isBeforeFirst())			{
				while (rs.next())				{
					//instrument.setId(rs.getString(2));
					instrument.setCode(rs.getString(2));
					instrument.setDescription(rs.getString(3));
					HashMap<String, String> hm = new HashMap<String, String>(); 
					hm.put("numericId", rs.getString(1));
					instrument.setMetadata(hm);
					/**
					 * TODO: Figure out what to do with the scores and responses members of Instrument;
					 * NHN Courses don't seem to have analogous intrinsic fields. 
					 */
				}
				rs.close();			
			}
		}catch(java.sql.SQLException se){
			LogWriter.logSQLWithException(LogWriter.ERROR, this, se.getMessage(), sb.toString(), se);
		}
		finally 		{
			if (dr != null) {dr.close(); dr = null;}			
		}
		return instrument;
	}

	/**
	 * Send in the string code (i.e., "finex")
	 * and get back the actual nhn instrument id (i.e., 51)
	 * 
	 * @param String instrumentCode
	 * @return int ( the numeric instrument id)
	 */
	public int fromInstrumentCode(String instrumentCode) {
		int instrumentId = 0;		
		StringBuffer sb = new StringBuffer();		
		sb.append("select " +
				"c.course " +
				"from platform.dbo.course c " +
				"where c.abbv = '" + instrumentCode + "'");

		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(sb);
		if (dlps.isInError()) {
			LogWriter.logSQL(LogWriter.WARNING, this, 
					"Error preparing Instrument fromId(SessionUser session, String instrumentId) query." + dlps.toString(), sb.toString());
		}
		DataResult dr = null;
		ResultSet rs = null;
		try		{
			dr = NhnDatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			if (rs != null && rs.isBeforeFirst())			{
				while (rs.next())				{
					instrumentId = rs.getInt(1);
				}
				rs.close();			
			}
		}catch(java.sql.SQLException se){
			LogWriter.logSQLWithException(LogWriter.ERROR, this, se.getMessage(), sb.toString(), se);
		}
		finally 		{
			if (dr != null) {dr.close(); dr = null;}			
		}
		return instrumentId;
	}


	//@Override
	public ArrayList<Instrument> all(SessionUser session, int type) {
		// TODO Auto-generated method stub
		return null;
	}

}
