package com.pdi.data.nhn.helpers.delegated;

import java.sql.ResultSet;
import java.util.ArrayList;

import com.pdi.data.dto.Language;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.interfaces.ILanguageHelper;
import com.pdi.data.nhn.util.NhnDatabaseUtils;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.util.RequestStatus;
import com.pdi.logging.LogWriter;

public class LanguageHelper implements ILanguageHelper {

	@Override
	public ArrayList<Language> all(SessionUser su) {
		ArrayList<Language> languages = new ArrayList<Language>();
		StringBuffer sb = new StringBuffer();		
		sb.append("select * from platform.dbo.languages l with(NOLOCK) ");
		
		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(sb);
		if (dlps.isInError()) {
			System.out.println("Error preparing Language all query." + dlps.toString());
			LogWriter.logSQL(LogWriter.ERROR, this,
					"Error preparing Project fromId query." + dlps.toString(),
					sb.toString());
		}
		DataResult dr = null;
		ResultSet rs = null;
		try		
		{
			dr = NhnDatabaseUtils.select(dlps);
			if(dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
			{
				// No data to find... we are done here
				return languages;
			}
			rs = dr.getResultSet();
			if (rs != null && rs.isBeforeFirst())			
			{
				while (rs.next())				
				{
					Language language = new Language();
					language.setId(rs.getString("id"));
					language.setCode(rs.getString("code"));
					language.setEnglishName(rs.getString("name"));
					language.setTranslatedName(rs.getString("trans_name"));
					languages.add(language);
				}
			}
		}
		catch(java.sql.SQLException se)
		{	
			LogWriter.logBasic(LogWriter.INFO, this, "LanguageHelper all error " + se.getMessage());
		}
		//catch (Exception e)
		//{
		//	LogWriter.logBasic(LogWriter.ERROR, this,
		//			"General error in ProjectHelper fromId (projId=" + projectId + ").  Msg=" + e.getMessage());
		//}
		finally
		{
//			try {  rs.close();  }
//			catch(Exception e)
//			{
//				LogWriter.logBasic(LogWriter.WARNING, this,
//						"Problem closing ResultSet.  Msg=" + e.getMessage());
//			}
			if (dr != null) { dr.close(); dr = null; }
		}
		return languages;
	}

	@Override
	public Language fromId(SessionUser su, String id) {
		Language language = new Language();
		StringBuffer sb = new StringBuffer();		
		sb.append("select * from platform.dbo.languages l with(NOLOCK) where id = ?");
		
		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(sb);
		
		if (dlps.isInError()) {
			System.out.println("Error preparing Language all query." + dlps.toString());
			LogWriter.logSQL(LogWriter.ERROR, this,
					"Error preparing Project fromId query." + dlps.toString(),
					sb.toString());
		}
		DataResult dr = null;
		ResultSet rs = null;
		try		
		{
			dlps.getPreparedStatement().setString(1, id);
			
			dr = NhnDatabaseUtils.select(dlps);
			if(dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
			{
				// No data to find... we are done here
				return language;
			}
			rs = dr.getResultSet();
			if (rs != null && rs.isBeforeFirst())			
			{
				while (rs.next())				
				{
					
					language.setId(rs.getString("id"));
					language.setCode(rs.getString("code"));
					language.setEnglishName(rs.getString("name"));
					language.setTranslatedName(rs.getString("trans_name"));

				}
			}
		}
		catch(java.sql.SQLException se)
		{	
			LogWriter.logBasic(LogWriter.INFO, this, "LanguageHelper all error " + se.getMessage());
		}
		//catch (Exception e)
		//{
		//	LogWriter.logBasic(LogWriter.ERROR, this,
		//			"General error in ProjectHelper fromId (projId=" + projectId + ").  Msg=" + e.getMessage());
		//}
		finally
		{
//			try {  rs.close();  }
//			catch(Exception e)
//			{
//				LogWriter.logBasic(LogWriter.WARNING, this,
//						"Problem closing ResultSet.  Msg=" + e.getMessage());
//			}
			if (dr != null) { dr.close(); dr = null; }
		}
		return language;
	}

	@Override
	public Language fromCode(SessionUser su, String code) {
		Language language = new Language();
		StringBuffer sb = new StringBuffer();		
		sb.append("select * from platform.dbo.languages l with(NOLOCK) where code = ?");
		
		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(sb);
		
		if (dlps.isInError()) {
			System.out.println("Error preparing Language all query." + dlps.toString());
			LogWriter.logSQL(LogWriter.ERROR, this,
					"Error preparing Project fromId query." + dlps.toString(),
					sb.toString());
		}
		DataResult dr = null;
		ResultSet rs = null;
		try		
		{
			dlps.getPreparedStatement().setString(1, code);
			
			dr = NhnDatabaseUtils.select(dlps);
			if(dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
			{
				// No data to find... we are done here
				return language;
			}
			rs = dr.getResultSet();
			if (rs != null && rs.isBeforeFirst())			
			{
				while (rs.next())				
				{
					
					language.setId(rs.getString("id"));
					language.setCode(rs.getString("code"));
					language.setEnglishName(rs.getString("name"));
					language.setTranslatedName(rs.getString("trans_name"));

				}
			}
		}
		catch(java.sql.SQLException se)
		{	
			LogWriter.logBasic(LogWriter.INFO, this, "LanguageHelper all error " + se.getMessage());
		}
		//catch (Exception e)
		//{
		//	LogWriter.logBasic(LogWriter.ERROR, this,
		//			"General error in ProjectHelper fromId (projId=" + projectId + ").  Msg=" + e.getMessage());
		//}
		finally
		{
//			try {  rs.close();  }
//			catch(Exception e)
//			{
//				LogWriter.logBasic(LogWriter.WARNING, this,
//						"Problem closing ResultSet.  Msg=" + e.getMessage());
//			}
			if (dr != null) { dr.close(); dr = null; }
		}
		return language;
	}

}
