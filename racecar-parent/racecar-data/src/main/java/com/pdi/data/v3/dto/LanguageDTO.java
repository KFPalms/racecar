/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.dto;

import com.pdi.data.util.BaseDTO;
import com.pdi.data.util.RequestStatus;


/**
 * LanguageDTO is a class designed to convey the information for one
 * language used in the PDI V3 extensions.
 * 
 * @author		Ken Beukelman
  */
public class LanguageDTO extends BaseDTO
{
	//
	// Static data.
	//


	//
	// Static methods.
	//


	//
	// Instance data.
	//
	private long _id;
	private String _isoCode;
	private String _name;
	private boolean _activeFlag;


	//
	// Constructors.
	//
	public LanguageDTO()
	{
		// does nothing right now
	}
	
	// Status setup constructor - language data remains unset
	public LanguageDTO(RequestStatus stat)
	{
		this.setStatus(stat);
	}

	//
	// Instance methods.
	//

	/**
	 * Generic toString method
	 */
	public String toString() {
		String str = "LanguageDTO:  " +
					 "Status code=" + this.getStatus().getStatusCode() +
					 ", id=" + this.getId() +
					 ", ISO code=" + this.getIsoCode() +
		 			 ", active flag=" + this.getActiveFlag() +
					 ", name=>" + this.getName() + "<";
		return str;
	}
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public long getId()
	{
		return _id;
	}
	
	public void setId(long value)
	{
		_id = value;
	}	

	/*****************************************************************************************/
	public String getIsoCode()
	{
		return _isoCode;
	}
	
	public void setIsoCode(String value)
	{
		_isoCode = value;
	}	

	/*****************************************************************************************/
	public boolean getActiveFlag()
	{
		return _activeFlag;
	}
	
	public void setActiveFlag(boolean value)
	{
		_activeFlag = value;
	}	

	/*****************************************************************************************/
	public String getName()
	{
		return _name;
	}
	
	public void setName(String value)
	{
		_name = value;
	}	
}
