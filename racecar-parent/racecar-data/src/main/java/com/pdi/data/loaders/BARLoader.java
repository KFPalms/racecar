/**
 * Copyright (c) 2012 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.loaders;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;


/**
* BarLoader loads the BARs for Eval Guides.  It reads a spreadsheet to get the 
* model, module, and competency name, then fetches their respective IDs to insert
* the data.
* 
* NOTE*NOTE*NOTE*NOTE*NOTE*NOTE*NOTE*NOTE*NOTE*NOTE*NOTE*NOTE*NOTE*NOTE*NOTE*NOTE*NOTE
* Note that you need to have a "clean" spreadsheet.  Deleting data from a spreadsheet
* apparently leaves stuff lying around which are picked up at load time and can give
* bogus errors (duplicate keys).
* 
* A "clean" spreadsheet can be generated in the following fashion:
* 	1) Do any cleanup in the source spreadsheet
* 	2) Open a new spreadsheet.
* 	3) copy each page of the source spreadsheet separately to the new spreadsheet
* 	4) Delete the rows following the last row in each sheet in the workbook
* 	5) Delete the columns following the last column in each sheet in the workbook
* 	6) Save the result as a .xls (Office 97-2003) workbook
* 
* Note also that this setup can read .xls files only at this time.
* 
* Runs in Debug or Run mode in Eclipse for testing
* 
* @author		Ken Beukelman
*/
public class BARLoader
{
	//
	// Static variables
	//
	private static final String DRIVER = "com.mysql.jdbc.Driver";

	//
	// Static classes
	//
	public static void main(String args[])
	{
		// The arguments are specified in the Run program Arguments in eclipse.
		if (args.length == 0)
		{
			System.out.println(" No parameters were detected.");
			System.out.println(parmDescString());

			return;
		}

		if (args.length != 4)
		{
			System.out.println("Incorrect number of parameters (" + args.length + ")");
			System.out.println(parmDescString());
			return;
		}
		
		String url = args[0];
		String uid = args[1];
		String pw = args[2];
		String xlFile = args[3];
		Connection con;
		
		System.out.println("BARLoader has been discontinued.  Please use BARLoaderII.");
		
//		// connect to the database
//		try
//		{
//			Class.forName(DRIVER);
//			con = DriverManager.getConnection(url, uid, pw);
//			con.setAutoCommit(false);	// This is not operative in My SQL as currently configured
//		}
//		catch (Exception e)
//		{
//			System.out.println("Unable to open database connection");
//			e.printStackTrace();
//			return;
//		}
//		System.out.println("Connected to " + url);
//
//		BARLoader loader = new BARLoader();
//		loader.run(con, uid, xlFile);
		
		return;
	}
	
	
	/*
	 * Method to display appropriate parameters.
	 * 
	 * Note that there is no closing new-line character; assumes that this method is called
	 * in a println() or that the user will handle the closing new-line himself.
	 */
	private static String parmDescString()
	{
		String str = "  This program requires four arguments as follows:\n";
		str += "    URL      - The URL where the database lies.  Examples:\n";
		str += "                 'jdbc:mysql://mspdbdev2:3306/pdi_certify' or\n";
		str += "                 'jdbc:mysql://sanfranqasql3:3306/scoring_reporting'\n";
		str += "       DON'T FORGET THE CHARACTER SET PARAMS AFTER THE URL! Use the following:\n";
		str += "         \"?useUnicode=true&characterEncoding=UTF-8&characterSetResults=UTF-8\"\n";
		str += "    User id  - A user id for the specified database with the appropriate permissions.\n";
		str += "    Password - The password for the above user ID.\n";
		str += "    Worksheet - The Path and name of the worksheet that the BAR data is in (enclose in double quotes if it conatins embedded spaces).\n";
		
		return str;
	}

	//
	// Instance variables
	//
	private String lastUser = "";
	private Map<String, Long> modelMap = null;
	private Map<String, Long> simMap = null;
	private Map<String, Long> compMap = null;
	private ArrayList<BarData> barArray = null;
	private PreparedStatement txtInsPs = null;
	private PreparedStatement getSeqPs = null;
	private PreparedStatement txtUpdtPs = null;
	
	
		//
		// Instance methods
		//
		/**
		 * Controller method for the load
		 */
		protected void run(Connection con, String uid, String fileName)
		{
			this.lastUser = uid;
			
			// Get models
			this.modelMap = getModelData(con);
			if (this.modelMap == null)
				return;

			// Get modules/sims/instruments
			this.simMap = getSimData(con);
			if (this.simMap == null)
				return;

			// Get competencies
			this.compMap = getCompData(con);
			if (this.compMap == null)
				return;

			// Fetch the BARs
			this.barArray = readBarData(fileName);
			if (barArray == null)
			{
				System.out.print("Error detected fetching bar data.  Exiting.");
				return;
			}
			////Debug
			//for(BarData bar : barArray)
			//{
			//	System.out.println(bar.toString());
			//}

			// Load the BARs
			if (loadBars(con))
				System.out.println("FLL BAR load completed successfully.");
		}
			
	
		/**
		 * Fetches the model data to a map for later lookup
		 */
		private Map<String, Long> getModelData(Connection con)
		{
			System.out.println("  Fetching model info");
			
			Map<String, Long> ret = new HashMap<String, Long>();

			Statement stmt = null;
		    ResultSet rs = null;

		    StringBuffer query = new StringBuffer();
		    query.append("SELECT mdl.internalName, ");
		    query.append("       mdl.modelId");
		    query.append("  FROM pdi_abd_model mdl ");
		    query.append("  WHERE mdl.active = 1");

		    try
		    {
		    	stmt = con.createStatement();
		        rs = stmt.executeQuery(query.toString());
	
				while (rs.next())
				{
					ret.put(rs.getString("internalName").trim(), new Long(rs.getLong("modelId")));
				}

				return ret;
		     }
		    catch (SQLException se)
		    {
				System.out.println("Error detected in getModelData.");
				se.printStackTrace();
				return null;
		    }
		    finally
		    {
		    	if (stmt != null)
		    	{
		        	try { stmt.close(); }
		        	catch (SQLException se) { /* swallow the exception */ }
		    		stmt = null;
		    	}
		    	if (rs != null)
		    	{
		        	try { rs.close(); }
		        	catch (SQLException se) { /* swallow the exception */ }
		    		rs = null;
		    	}
		    }
		}


		/**
		 * Fetches the module/sim/instrument data to a map for later lookup
		 */
		private Map<String, Long> getSimData(Connection con)
		{
			System.out.println("  Fetching sim info");

			Map<String, Long> ret = new HashMap<String, Long>();

			Statement stmt = null;
			ResultSet rs = null;
	
			StringBuffer query = new StringBuffer();
			query.append("SELECT sim.internalName, ");
			query.append("       sim.moduleId ");
			query.append("  FROM pdi_abd_module sim");
			try
			{
				stmt = con.createStatement();
				rs = stmt.executeQuery(query.toString());
	
				while (rs.next())
				{
					ret.put(rs.getString("internalName").trim(), new Long(rs.getLong("moduleId")));
				}
				
				return ret;
			}
			catch (SQLException se)
			{
				System.out.println("Error detected in getSimData.");
				se.printStackTrace();
				return null; 
			}
			finally
			{
				try { stmt.close(); }
				catch (SQLException se) { /* swallow the exception */ }
				stmt = null;
				try { rs.close(); }
				catch (SQLException se) { /* swallow the exception */ }
				rs = null;
			}
		}
	

		/**
		 * Fetches the competency data to a map for later lookup
		 */
		private Map<String, Long> getCompData(Connection con)
		{
			System.out.println("  Fetching comp info");
	
			Map<String, Long> ret = new HashMap<String, Long>();
	
			Statement stmt = null;
			ResultSet rs = null;
		
			StringBuffer query = new StringBuffer();
			query.append("SELECT cmp.internalName, ");
			query.append("       cmp.competencyId ");
			query.append("  FROM pdi_abd_competency cmp");
			try
			{
				stmt = con.createStatement();
				rs = stmt.executeQuery(query.toString());
		
				while (rs.next())
				{
					ret.put(rs.getString("internalName").trim(), new Long(rs.getLong("competencyId")));
				}
				
				return ret;
			}
			catch (SQLException se)
			{
				System.out.println("Error detected in getCompData.");
				se.printStackTrace();
				return null;
			}
			finally
			{
				try { stmt.close(); }
				catch (SQLException se) { /* swallow the exception */ }
				stmt = null;
				try { rs.close(); }
				catch (SQLException se) { /* swallow the exception */ }
				rs = null;
			}
		}
	
	
		/*
		 * Read the bars.
		 * 
		 * Assumes an old-format (.xls) spreadsheet
		 */
		private ArrayList<BarData> readBarData(String fileName)
		{
			ArrayList<BarData> ret = new ArrayList<BarData>();
	
			try
			{
				// Open the spreadsheet
				POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(fileName));
				HSSFWorkbook wb = new HSSFWorkbook(fs);
				System.out.println("  " + fileName + " has "+ wb.getNumberOfSheets() + " sheets");
	
				//for (int i=1; i < wb.getNumberOfSheets(); i++)
				for (int i=0; i < wb.getNumberOfSheets(); i++)
				{
					HSSFSheet sheet = wb.getSheetAt(i);
					System.out.println("  Sheet " + i + ":  " + sheet.getSheetName() + " (" + sheet.getLastRowNum()+ " rows)");
	
					for (Row row : sheet)
					{
						// Always skip row 0 in the sheet (it is the header)
						if (row.getRowNum() == 0)
						{
							continue;
						}
						
						BarData bd = new BARLoader().new BarData();
						for (Cell cell : row)
						{
							int idx = cell.getColumnIndex();
							switch (idx)
							{
								case 0:		// Model name - String
									if (cell.getCellType() != Cell.CELL_TYPE_STRING)
									{
										throw new Exception("Sheet: " + sheet.getSheetName() + ", row " + row.getRowNum() + ", dataType=" + cell.getCellType() + ": Model name expected");
									}
									bd.setModel(cell.getStringCellValue().trim());
									break;
								case 1:		// Module name
									if (cell.getCellType() != Cell.CELL_TYPE_STRING)
									{
										throw new Exception("Sheet: " + sheet.getSheetName() + ", row " + row.getRowNum() + ", dataType=" + cell.getCellType() + ": Module name expected");
									}
									bd.setModule(cell.getStringCellValue().trim());
									break;
								case 2:		// Competency name
									if (cell.getCellType() != Cell.CELL_TYPE_STRING)
									{
										throw new Exception("Sheet: " + sheet.getSheetName() + ", row " + row.getRowNum() + ", dataType=" + cell.getCellType() + ": Competency name expected");
									}
									bd.setComp(cell.getStringCellValue().trim());
									break;
								case 3:		// BAR sequence
									if (cell.getCellType() != Cell.CELL_TYPE_NUMERIC)
									{
										throw new Exception("Sheet: " + sheet.getSheetName() + ", row " + row.getRowNum() + ", dataType=" + cell.getCellType() + ": Sequence number expected");
									}
					    			bd.setSeq((int)cell.getNumericCellValue());
									break;
								case 4:		// Behavior id
									if (cell.getCellType() != Cell.CELL_TYPE_NUMERIC)
									{
										throw new Exception("Sheet: " + sheet.getSheetName() + ", row " + row.getRowNum() + ", dataType=" + cell.getCellType() + ": Behavior ID expected");
									}
					    			bd.setBehId((long)cell.getNumericCellValue());
									break;
								case 5:		// High value (5) text
									if (cell.getCellType() != Cell.CELL_TYPE_STRING)
									{
										throw new Exception("Sheet: " + sheet.getSheetName() + ", row " + row.getRowNum() + ", dataType=" + cell.getCellType() + ": High (5) text expected");
									}
					    			bd.setHi(cell.getStringCellValue().trim());
									break;
								case 6:		// Middle value (3) text
									if (cell.getCellType() != Cell.CELL_TYPE_STRING)
									{
										throw new Exception("Sheet: " + sheet.getSheetName() + ", row " + row.getRowNum() + ", dataType=" + cell.getCellType() + ": Mid (3) text expected");
									}
					    			bd.setMid(cell.getStringCellValue().trim());
									break;
								case 7:		// Low value (1) text
									if (cell.getCellType() != Cell.CELL_TYPE_STRING)
									{
										throw new Exception("Sheet: " + sheet.getSheetName() + ", row " + row.getRowNum() + ", dataType=" + cell.getCellType() + ": Low (1) text expected");
									}
					    			bd.setLo(cell.getStringCellValue().trim());
									break;
								case 8:		// High value (5) example text
									if (cell == null || cell.getCellType() == Cell.CELL_TYPE_BLANK)
									{
										// Do nothing... leave the entry as null in the BarData object
									}
									else if (cell.getCellType() == Cell.CELL_TYPE_STRING)
									{
										bd.setHiE(cell.getStringCellValue().trim());
									}
									else
									{
										// Bad data
										throw new Exception("Sheet: " + sheet.getSheetName() + ", row " + row.getRowNum() + ", dataType=" + cell.getCellType() + ": High (5) extended text expected");
									}
									break;
								case 9:		// Middle value (3) example text
									if (cell == null || cell.getCellType() == Cell.CELL_TYPE_BLANK)
									{
										// Do nothing... leave the entry as null in the BarData object
									}
									else if (cell.getCellType() == Cell.CELL_TYPE_STRING)
									{
										bd.setMidE(cell.getStringCellValue().trim());
									}
									else
									{
										// Bad data
										throw new Exception("Sheet: " + sheet.getSheetName() + ", row " + row.getRowNum() + ", dataType=" + cell.getCellType() + ": Mid (3) extended text expected");
									}
									break;
								case 10:		// Low value (1) example text
									if (cell == null || cell.getCellType() == Cell.CELL_TYPE_BLANK)
									{
										// Do nothing... leave the entry as null in the BarData object
									}
									else if (cell.getCellType() == Cell.CELL_TYPE_STRING)
									{
										bd.setLoE(cell.getStringCellValue().trim());
									}
									else
									{
										// Bad data
										throw new Exception("Sheet: " + sheet.getSheetName() + ", row " + row.getRowNum() + ", dataType=" + cell.getCellType() + ": Low (1) extended text expected");
									}
									break;
								default:
									System.out.println("Sheet: " + sheet.getSheetName() + ", row " + row.getRowNum() + ", column=" + cell.getColumnIndex() + ", dataType=" + cell.getCellType() + ": No data expected... ignored");
							}	// End of the Cell switch
						}	// End of the Cell loop
						
						//Save the BarData object
						ret.add(bd);
					}	// End of Row loop
				}	// End of sheet FOR loop
				
				return ret;
			}
			catch (Exception e)
			{
				System.out.println("Error in BAR data fetch.");
				e.printStackTrace();  
				return null;
			}
		}


		/**
		 * Loads the BAR data into the database.
		 * Assumes that the data in the barArray is sorted by model, module, and competency
		 */
		private boolean loadBars(Connection con)
		{
			StringBuffer query = new StringBuffer();
			String context = null;
			PreparedStatement barInsPs = null;
	
	 		String curModel = null;
	 		String curModule= null;
	 		int curBehCnt = 0;
			String locStr = "";
	
			try
			{
			   	// Set up the text insert...
				context = "txtInsPs";
			    query.setLength(0);
				query.append("INSERT INTO pdi_abd_text ");
				query.append("  (languageId,  text, isDefault, active, lastUserId, lastDate) ");
				query.append("  VALUES(1, ?, 1, 1, '" + this.lastUser + "', NOW())");
				this.txtInsPs = con.prepareStatement(query.toString());
	
				// ...the ID value query...
				context = "getSeqPs";
		    	query.setLength(0);
				query.append("SELECT LAST_INSERT_ID()");
				this.getSeqPs = con.prepareStatement(query.toString());
	
			   	// ...the text update...
				context = "txtUpdtPs";
			    query.setLength(0);
				query.append("UPDATE pdi_abd_text ");
				query.append("  SET textId = ?");
				query.append("  WHERE uniqueId = ?");
				this.txtUpdtPs = con.prepareStatement(query.toString());
	
				// ...and the BAR insert
				context = "barInsPs";
			    query.setLength(0);
				query.append("INSERT INTO pdi_abd_eg_bar ");
				query.append("  (modelId, moduleId, competencyId, barSequence, extBehaviorId, ");
				query.append("   hiTextId, medTextId, loTextId, ");
				query.append("   hiExTextId, medExTextId, loExTextId, ");
				query.append("   active, lastUserId, lastDate) ");
				query.append("  VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 1,'" + this.lastUser + "', NOW())");
				barInsPs = con.prepareStatement(query.toString());

				// loop through the bar array
				for (BarData theBar : this.barArray)
				{ 
					locStr = "Model=" + theBar.getModel() +
					", Sim=" + theBar.getModule() +
					", Comp=" + theBar.getComp() +
					". Seq=" + theBar.getSeq() +
					", beh ID=" + theBar.getBehId();
	
					if (curModel == null  ||  (! curModel.equals(theBar.getModel())) )
					{
						// Got a new model
						if (curModel != null)
						{
							// Not the first time... put out the count
							System.out.println("");
							System.out.println("        Total BARs in module:  " + curBehCnt);
							curBehCnt = 0;
							curModule = null;
						}
						curModel = theBar.getModel();
						System.out.println("  Loading data for model:  " + curModel);
					}
					if (curModule == null  ||  (! curModule.equals(theBar.getModule())) )
					{
						if (curModule != null)
						{
							System.out.println("");
							System.out.println("        Total BARs in module:  " + curBehCnt);
							curBehCnt = 0;
						}
						curModule = (String) theBar.getModule();
						System.out.print("    Loading Module:  " + curModule);
					}

					// Insert the 6 strings as needed - save the ids
					context = "High text";
					long HiId = insertText(theBar.getHi());
	
					context = "Middle text";
					long MidId = insertText(theBar.getMid());
	
					context = "Low text";
					long LoId = insertText(theBar.getLo());
	
					context = "High example text";
					long HiEId = insertText(theBar.getHiE());
	
					context = "Middle example text";
					long MidEId = insertText(theBar.getMidE());
	
					context = "Low example text";
					long LoEId = insertText(theBar.getLoE());
	
					context = "BAR insert";
					barInsPs.setLong(1, ((Long)this.modelMap.get(theBar.getModel())).longValue() );
					barInsPs.setLong(2, ((Long)this.simMap.get(theBar.getModule())).longValue() );
					barInsPs.setLong(3, ((Long)this.compMap.get(theBar.getComp())).longValue() );
					barInsPs.setInt(4, theBar.getSeq());
					barInsPs.setLong(5, theBar.getBehId());
					barInsPs.setLong(6, HiId);
					barInsPs.setLong(7, MidId);
					barInsPs.setLong(8, LoId);
					barInsPs.setLong(9, HiEId);
					barInsPs.setLong(10, MidEId);
					barInsPs.setLong(11, LoEId);
	
					barInsPs.executeUpdate();
	
					System.out.print(".");
					curBehCnt++;
		    	}	// end of for loop

				// put out the last count
				System.out.println("");
				System.out.println("        Total BARs in module:  " + curBehCnt);
				
				return true;
			}
			catch (SQLException se)
			{
				System.out.println("\nSQL error in loadBars(" + context + ").");
				se.printStackTrace();
				return false; 
			}
			catch (Exception e)
			{
				System.out.println("\nGeneral error in loadBars(" + context + ").");
				System.out.println("  Source data = " + locStr);
				e.printStackTrace();
				return false; 
			}
		}


		/**
		 * Writes out data to the text file and returns the ID
		 */
		private long insertText(String txt)
			throws SQLException
		{
	    	ResultSet rs = null;
	    	
			if (txt == null)
			{
				return 0;
			}

			// Clean the string and escape the single quote character
	    	txt = txt.trim();
			if (txt.contains("'"))
			{
				txt = txt.replace("'", "\'");
			}

			// Insert the text...
			this.txtInsPs.setString(1, txt);
			this.txtInsPs.executeUpdate();
	
			// ...get the id...
			rs = this.getSeqPs.executeQuery();
			rs.next();
			long id = rs.getLong(1);
			
			// ...and update the text ID with the new unique ID
			this.txtUpdtPs.setLong(1, id);
			this.txtUpdtPs.setLong(2, id);
			this.txtUpdtPs.executeUpdate();
	
			if (rs != null)
			{
				try
				{
					rs.close();
				}
				catch (Exception e)  {  /* Swallow it */  }
				rs = null;
			}
			
			return id;
		}
	
		
	
		/**
		 * Copyright (c) 2009, 2011, 2012 Personnel Decisions International, Inc.
		 * All rights reserved.
		 * 
		 * @author		Ken Beukelman
		 */
		private class BarData
		{
		    //
		    // Static variables
		    //
			   
		    //
		    // Instance variables
		    //
			private String _model = null;
			private String _module = null;
			private String _comp = null;
			private int _seq = 0;
			private long _bId = 0;
			private String _hi = null;
			private String _mid = null;
			private String _lo = null;
			private String _hiE = null;
			private String _midE = null;
			private String _loE = null;
	
		    //
		    // Class methods
		    //
	
		    //
		    // Instance methods
		    //
			public String toString()
			{
				String ret = "";
				ret += _model;
				ret += "|" + _module;
				ret += "|" + _comp;
				ret += "|" + _seq;
				ret += "|" + _bId;
				ret += "\n   Hi-" + _hi;
				ret += "\n  Mid-" + _mid;
				ret += "\n  Low-" + _lo;
				ret += "\n   HiE-" + _hiE;
				ret += "\n  MidE-" + _midE;
				ret += "\n  LowE-" + _loE;
				
				return ret;
			}
	
			/******************************************/
			public String getModel()
			{
				return _model;
			}
	
			public void setModel(String value)
			{
				_model = value;
			}
	
			/******************************************/
			public String getModule()
			{
				return _module;
			}
	
			public void setModule(String value)
			{
				_module = value;
			}
	
			/******************************************/
			public String getComp()
			{
				return _comp;
			}
	
			public void setComp(String value)
			{
				_comp = value;
			}
	
			/******************************************/
			public int getSeq()
			{
				return _seq;
			}
	
			public void setSeq(int value)
			{
				_seq = value;
			}
	
			/******************************************/
			public long getBehId()
			{
				return _bId;
			}
	
			public void setBehId(long value)
			{
				_bId = value;
			}
	
			/******************************************/
			public String getHi()
			{
				return _hi;
			}
	
			public void setHi(String value)
			{
				_hi = value;
			}
	
			/******************************************/
			public String getMid()
			{
				return _mid;
			}
	
			public void setMid(String value)
			{
				_mid = value;
			}
	
			/******************************************/
			public String getLo()
			{
				return _lo;
			}
	
			public void setLo(String value)
			{
				_lo = value;
			}
	
			/******************************************/
			public String getHiE()
			{
				return _hiE;
			}
	
			public void setHiE(String value)
			{
				_hiE = value;
			}
	
			/******************************************/
			public String getMidE()
			{
				return _midE;
			}
	
			public void setMidE(String value)
			{
				_midE = value;
			}
	
			/******************************************/
			public String getLoE()
			{
				return _loE;
			}
	
			public void setLoE(String value)
			{
				_loE = value;
			}
		}	// end of BarData private class
}
