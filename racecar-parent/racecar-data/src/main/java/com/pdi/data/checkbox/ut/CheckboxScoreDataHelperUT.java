/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.checkbox.ut;

import java.util.Iterator;
import java.util.Map;

import com.pdi.data.checkbox.helpers.CheckboxScoreDataHelper;
import com.pdi.properties.PropertyLoader;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

public class CheckboxScoreDataHelperUT  extends TestCase
{
	public CheckboxScoreDataHelperUT(String name)
	{
		super(name);
	}

	public void testProperties()
	{
		UnitTestUtils.start(this);
		
		//Does the local property work?
		String applicationId = PropertyLoader.getProperty("com.pdi.data.checkbox.application", "application.code");
		assertEquals(applicationId != null, true);
		assertEquals(applicationId.equals("PDI-DATA-CHECKBOX"), true);
		
		UnitTestUtils.stop(this);
	}

	public void testCheckbox()
		throws Exception
	{
		UnitTestUtils.start(this);
	
		// Initial Checkbox test (Bogus job/cand ids
		//long rid= 89671;	
		// Second Checkbox test (has "real" V2 data in it)
		long rid = 89830;
	
		IndividualReport ir = CheckboxScoreDataHelper.fetchCheckboxData(rid); 
	
		// Dump the results
		System.out.println("-----------------------------------------------------------------------");
		System.out.println("                      Results Dump from UT");
		System.out.println("-----------------------------------------------------------------------");
		if (ir == null)	// Shouldn't happen
		{
			System.out.println("ERROR:  No IndividualReport object returned.");
		}
		if (ir.getReportData().isEmpty())
		{
			System.out.println("No report data found");
		}

		ReportData rd = ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY);
		
		// raw data - input scores
		System.out.println("  Display data:");
		for (Iterator<Map.Entry<String, String>> itr = rd.getDisplayData().entrySet().iterator(); itr.hasNext(); )
		{
			Map.Entry<String, String> me = itr.next();
			System.out.println("    " + me.getKey() + "=" + me.getValue());
		}
		
		System.out.println("  Raw data:");
		for (Iterator<Map.Entry<String, String>> itr = rd.getRawData().entrySet().iterator(); itr.hasNext(); )
		{
			Map.Entry<String, String> me = itr.next();
			System.out.println("    " + me.getKey() + "=" + me.getValue());
		}
		
		// score data - raw scale scores
		System.out.println("  Score data:");
		for (Iterator<Map.Entry<String, String>> itr = rd.getScoreData().entrySet().iterator(); itr.hasNext(); )
		{
			Map.Entry<String, String> me = itr.next();
			System.out.println("    " + me.getKey() + "=" + me.getValue());
		}

		UnitTestUtils.stop(this);
	}
}
