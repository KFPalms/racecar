/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */

package com.pdi.data.abyd.helpers.setup;

import com.pdi.data.abyd.util.email.EmailMessage;
import com.pdi.data.abyd.util.email.EmailRequestService;
import com.pdi.properties.PropertyLoader;

/**
 * SetupSendEmailHelper contains the code needed to send an e-mail from
 * setup to a designated recipient with one of a number of texts.
 * 
 * @author		Ken Beukelman
 */
public class SetupSendEmailHelper
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String emailAddr;
	private String emTextType;
	private String cName;
	
	//
	// Constructors.
	//
	public SetupSendEmailHelper(String address, String textFlag, String clientName)
		throws Exception
	{
		if (address ==null || address.length() < 1)
		{
			throw new Exception("The e-mail address must exist.");
		}
		
		if (textFlag ==null || textFlag.length() < 1)
		{
			throw new Exception("The text indicator must exist.");
		}
		
		if (! (textFlag.equals("modelChange")	||
			   textFlag.equals("dnaChange")		||
			   textFlag.equals("spChange")		||
			   textFlag.equals("dnaSubmitted") ) )
		{
			throw new Exception("Unrecogniged textFlag (" + textFlag + ").");
		}
		
		if (clientName ==null || clientName.length() < 1)
		{
			throw new Exception("The client name must exist.");
		}
		
		this.emailAddr = address;
		this.emTextType = textFlag;
		this.cName = clientName;
	}

	//
	// Instance methods.
	//
	
	/**
	 * Build a request for an email to be sent.
	 *
	 * @throws Exception
	 */
	public void sendEmail()
		throws Exception
	{
		// Check for subject and text
		String propName = "abd.dna.emailSubject." + this.emTextType;
		String emailSubject = PropertyLoader.getProperty("com.pdi.data.abyd.application", propName);

		if (emailSubject == null)
		{
			throw new Exception("No subject text for text type " + this.emTextType);
		}
		propName = "abd.dna.emailText." + this.emTextType;
		String emailBody = PropertyLoader.getProperty("com.pdi.data.abyd.application", propName);
		if (emailBody == null)
		{
			throw new Exception("No body text for text type " + this.emTextType);
		}
		
		// Get the from address and the server name
		String fromAddr = PropertyLoader.getProperty("com.pdi.data.abyd.application", "abd.dna.fromAddress");
		String host = PropertyLoader.getProperty("com.pdi.data.abyd.application", "abd.emailServer");

		// Set up the message object
		EmailMessage email = new EmailMessage();
		email.setToAddress(this.emailAddr);
		email.setFromAddress(fromAddr);
		email.setSubject(emailSubject);
		email.setText(emailBody);
		email.getTextSubstitutions().add(this.cName);

		EmailRequestService emailService = new EmailRequestService(host);
		emailService.sendEmail(email);

		return;
	}
}
