package com.pdi.data.abyd.helpers.intGrid.ut;

import java.sql.Connection;

import com.pdi.data.abyd.dto.intGrid.IGSummaryDataDTO;
import com.pdi.data.abyd.helpers.intGrid.IGSummaryDataHelper;
import com.pdi.ut.UnitTestUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.data.nhn.util.NhnDatabaseUtils;

import junit.framework.TestCase;

public class IGSummaryDataHelperUT  extends TestCase
{
	//
	// Constructors
	//
	public IGSummaryDataHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
	throws Exception
	{
		junit.textui.TestRunner.run(IGSummaryDataHelperUT.class);
	}


	/*
	 * testGetSummaryData
	 */
	public void testGetSummaryData()
		throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);
		NhnDatabaseUtils.setUnitTest(true);
		
		Connection con = AbyDDatabaseUtils.getDBConnection();
		
		String lpId = "84681894";	// dev

		// get the data
		IGSummaryDataHelper helper = new IGSummaryDataHelper(con, lpId, false);        
		IGSummaryDataDTO dto = helper.getSummaryData();
		System.out.print(dto.toString());

		UnitTestUtils.stop(this);
	}

}
