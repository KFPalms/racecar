package com.pdi.data.v2.helpers.delegated.ut;

import com.pdi.data.dto.TextGroup;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.ITextHelper;
import com.pdi.data.v2.helpers.delegated.TextHelper;
import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.properties.PropertyLoader;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

public class TextHelperUT extends TestCase
{
	public TextHelperUT(String name)
	{
		super(name);
	}	

//	/*
//	 * Ignore this for now... not doing the initialization thing anymore
//	 * testInit - ALWAYS KEEP THIS EXPOSED.  If you don't, the
//	 * subsequent tests will break because there is no cache set up
//	 */
//	public void testInit()
//		throws Exception
//	{
//			UnitTestUtils.start(this);
//			V2DatabaseUtils.setUnitTest(true);
//
//			// Initialize the cache
//			TextHelper th = new TextHelper();
//			th.initializeCache();
//
//			System.out.println(th.showCache());
//			
//			UnitTestUtils.stop(this);
//	}


	/*
	 * testFromGroupCodeLanguageCode
	 */
//	public void testFromGroupCodeLanguageCode()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		V2DatabaseUtils.setUnitTest(true);
//
//		ITextHelper helper = HelperDelegate.getTextHelper(PropertyLoader.getProperty("com.pdi.data.v2.application","datasource.v2"));
//		
//		// Get a TextGroup object
//		TextGroup tg =  helper.fromGroupCodeLanguageCode(null, "TEST_2", "nl");
////		if (tg == null)
////			System.out.println("No data for the group/language...");
////		else
////			System.out.println(tg.toString());
////		System.out.println("-----------------------");
//		
//		//Try a second
//		tg =  helper.fromGroupCodeLanguageCode(null, "TEST_1", "nl");
////		if (tg == null)
////			System.out.println("No data for the group/language...");
////		else
////			System.out.println(tg.toString());
////		System.out.println("-----------------------");
//		
//		// Try one with a bogus language
//		tg = helper.fromGroupCodeLanguageCode(null, "TEST_1", "aa");
//		if (tg == null)
//			System.out.println("No data for the group/language...");
//		else
//			System.out.println(tg.toString());
//		System.out.println("-----------------------");
//		
//		// Try one with a bogus group code
//		tg = helper.fromGroupCodeLanguageCode(null, "KEN_B", "en");
//		if (tg == null)
//			System.out.println("No data for the group/language...");
//		else
//			System.out.println(tg.toString());
//
//		
//		UnitTestUtils.stop(this);
//	}


	/*
	 * testFromGroupCodeLanguageCodeWithDefault
	 */
	public void testFromGroupCodeLanguageCodeWithDefault()
		throws Exception
	{
		UnitTestUtils.start(this);
		V2DatabaseUtils.setUnitTest(true);

		ITextHelper helper = HelperDelegate.getTextHelper(PropertyLoader.getProperty("com.pdi.data.v2.application","datasource.v2"));
		TextGroup tg = null;
		
		// Get a TextGroup object
		tg =  helper.fromGroupCodeLanguageCodeWithDefault(null, "TEST_2", "nl");
		//TextGroup tg =  helper.fromGroupCodeLanguageCode(null, "TEST_2", "es");
		if (tg == null)
			System.out.println("No data for the language... (should be data for nl)");
		else
			System.out.println(tg.toString());
		System.out.println("-----------------------");
		
		// Try one with a bogus language
		tg = helper.fromGroupCodeLanguageCodeWithDefault(null, "TEST_1", "aa");
		if (tg == null)
			System.out.println("No data for the group/language...");
		else
			System.out.println(tg.toString());
		System.out.println("-----------------------");
		
		// Try one with a bogus group code
		tg = helper.fromGroupCodeLanguageCode(null, "KEN_B", "en");
		if (tg == null)
			System.out.println("No data for the group/language...");
		else
			System.out.println(tg.toString());
		
		// OK now lets get different languages
		System.out.println("----------- lang changes ------------");
		System.out.println("------------(SB nl)-----------");
		tg =  helper.fromGroupCodeLanguageCodeWithDefault(null, "TEST_2", "nl");
		if (tg == null)
			System.out.println("No data for the language... (should be data for nl)");
		else
			System.out.println(tg.toString());
		System.out.println("------------(SB en)-----------");
		tg =  helper.fromGroupCodeLanguageCodeWithDefault(null, "TEST_2", "en");
		if (tg == null)
			System.out.println("No data for the language... (should be data for en)");
		else
			System.out.println(tg.toString());
		System.out.println("------------(SB nl)-----------");
		tg =  helper.fromGroupCodeLanguageCodeWithDefault(null, "TEST_2", "nl");
		if (tg == null)
			System.out.println("No data for the language... (should be data for nl)");
		else
			System.out.println(tg.toString());
		
		UnitTestUtils.stop(this);
	}


	/*
	 * testFromGroupCode
	 */
//	public void testFromGroupCode()
//	throws Exception
//	{
//		UnitTestUtils.start(this);
//		V2DatabaseUtils.setUnitTest(true);
//
//		ITextHelper helper = HelperDelegate.getTextHelper(PropertyLoader.getProperty("com.pdi.data.v2.application","datasource.v2"));
//		
//		// Get all text for a text group
//		HashMap<String, TextGroup> hm =  helper.fromGroupCode(null, "TEST_1");
//		if (hm == null)
//			System.out.println("No data for the group code...");
//		else
//		{
//			System.out.println("Found " + hm.size() + " languages in group");
//			for (Iterator<Map.Entry<String, TextGroup>> itr=hm.entrySet().iterator(); itr.hasNext();  )
//			{
//				Map.Entry<String, TextGroup> ent = itr.next();
//				System.out.println("  lang=" + ent.getKey() + " (" + ent.getValue().getLanguageCode() + ")");
//			}
//		}
//		System.out.println("----------  ----------");
//		
//		// Get all text for another text group
//		hm =  helper.fromGroupCode(null, "TEST_2");
//		if (hm == null)
//			System.out.println("No data for the group code...");
//		else
//		{
//			System.out.println("Found " + hm.size() + " languages in group");
//			for (Iterator<Map.Entry<String, TextGroup>> itr=hm.entrySet().iterator(); itr.hasNext();  )
//			{
//				Map.Entry<String, TextGroup> ent = itr.next();
//				System.out.println("  lang=" + ent.getKey() + " (" + ent.getValue().getLanguageCode() + ")");
//			}
//		}
//		System.out.println("----------  ----------");
//		
//		// Try a bogus group code
//		hm =  helper.fromGroupCode(null, "KEN_B");
//		if (hm == null)
//			System.out.println("No data for the bogus group code (expected)...");
//		else
//			System.out.println("Found data for bogus group - Error");
//		System.out.println("----------  ----------");
//		
//		// Try an empty group code
//		hm =  helper.fromGroupCode(null, "");
//		if (hm == null)
//			System.out.println("No data for empty group code (expected)...");
//		else
//			System.out.println("Found data for empty group - Error");
//		
//		UnitTestUtils.stop(this);
//	}


	/*
	 * testFromGroupCodeLanguageCodeLabelCode
	 */
//	public void testFromGroupCodeLanguageCodeLabelCode()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		V2DatabaseUtils.setUnitTest(true);
//
//		ITextHelper helper = HelperDelegate.getTextHelper(PropertyLoader.getProperty("com.pdi.data.v2.application","datasource.v2"));
//
//		// Get a single text from a text group in a language
//		Text txt =  helper.fromGroupCodeLanguageCodeLabelCode(null, "TEST_1", "hu", "TESTING_1");
//		if (txt == null)
//			System.out.println("No data for the group/lang/label...");
//		else
//			System.out.println(txt.toString());
//		System.out.println("----------  ----------");
//			
//		// Get another text from another text group (should be null... not in that language)
//		txt =  helper.fromGroupCodeLanguageCodeLabelCode(null, "TEST_2", "hu", "MLL_LABEL");
//		if (txt == null)
//			System.out.println("No data for the group/lang/label... expected result");
//		else
//			System.out.println(txt.toString());
//		System.out.println("----------  ----------");
//			
//		// Try a bogus label
//		txt =  helper.fromGroupCodeLanguageCodeLabelCode(null, "TEST_2", "hu", "KEN_B");
//		if (txt == null)
//			System.out.println("No data for the group/lang/label... expected result");
//		else
//			System.out.println(txt.toString());
//			
//		UnitTestUtils.stop(this);
//	}


	/*
	 * testFromGroupCodeLanguageCodeLabelCodeWithDefault
	 */
//	public void testFromGroupCodeLanguageCodeLabelCodeWithDefault()
//	throws Exception
//	{
//		UnitTestUtils.start(this);
//		V2DatabaseUtils.setUnitTest(true);
//
//		ITextHelper helper = HelperDelegate.getTextHelper(PropertyLoader.getProperty("com.pdi.data.v2.application","datasource.v2"));
//
//		// Get a single text from a text group in a language (with default)
//		Text txt =  helper.fromGroupCodeLanguageCodeLabelCodeWithDefault(null, "TEST_1", "hu", "TESTING_1");
//		if (txt == null)
//			System.out.println("No data for the group/lang/label (error)...");
//		else
//			System.out.println(txt.toString());
//		System.out.println("----------  ----------");
//
//		// Get another text from another text group (should return the default language)
//		txt =  helper.fromGroupCodeLanguageCodeLabelCodeWithDefault(null, "TEST_2", "hu", "MLL_LABEL");
//		if (txt == null)
//			System.out.println("No data for the group/lang/label... error");
//		else
//			System.out.println(txt.toString());
//		System.out.println("----------  ----------");
//		
//		// Try a bogus label
//		txt =  helper.fromGroupCodeLanguageCodeLabelCode(null, "TEST_2", "hu", "KEN_B");
//		if (txt == null)
//			System.out.println("No data for the group/lang/label... expected result");
//		else
//			System.out.println(txt.toString());
//		
//		UnitTestUtils.stop(this);
//	}


	/*
	 * testFromTextIdLanguageCode
	 */
//	public void testFromTextIdLanguageCode()
//	throws Exception
//	{
//		UnitTestUtils.start(this);
//		V2DatabaseUtils.setUnitTest(true);
//
//		String id = "";
//		String lang = "";
//		ITextHelper helper = HelperDelegate.getTextHelper(PropertyLoader.getProperty("com.pdi.data.v2.application","datasource.v2"));
//
//		// Get a single text string in a language
//		id = "3333";
//		lang = "fr";
//		String str =  helper.fromTextIdLanguageCode(null, id, lang);
//		if (str == null)
//			System.out.println("No data for ID " + id + " in " + lang + ".  Error...");
//		else
//			System.out.println(id + "/" + lang + " - ret=" + str);
//		System.out.println("----------  ----------");
//
//		// Get another text (should return null - only available in English)
//		id = "3210";
//		str =  helper.fromTextIdLanguageCode(null, id, lang);
//		if (str == null)
//			System.out.println("No data for ID " + id + " in " + lang + ".  Expected");
//		else
//			System.out.println(id + "/" + lang + " - ret=" + str + ".  Error.");
//		System.out.println("----------  ----------");
//
//		// Try a bogus language
//		id = "3333";
//		lang = "es_ME";
//		str =  helper.fromTextIdLanguageCode(null, id, lang);
//		if (str == null)
//			System.out.println("No data for ID " + id + " in " + lang + ".  Expected result");
//		else
//			System.out.println(id + "/" + lang + " - ret=" + str + ".  Error.");
//		System.out.println("----------  ----------");
//
//		// Try a bogus label
//		id = "99999";
//		lang = "en";
//		str =  helper.fromTextIdLanguageCode(null, id, lang);
//		if (str == null)
//			System.out.println("No data for ID " + id + " in " + lang + ".  Expected result");
//		else
//			System.out.println(id + "/" + lang + " - ret=" + str + ".  Error.");
//		
//		UnitTestUtils.stop(this);
//	}


	/*
	 * testFromTextIdLanguageCodeWithDefault
	 */
//	public void testFromTextIdLanguageCodeWithDefault()
//	throws Exception
//	{
//		UnitTestUtils.start(this);
//		V2DatabaseUtils.setUnitTest(true);
//
//		String id = "";
//		String lang = "";
//		ITextHelper helper = HelperDelegate.getTextHelper(PropertyLoader.getProperty("com.pdi.data.v2.application","datasource.v2"));
//
//		// Get a single text string in a language (with default)
//		id = "3333";
//		lang = "fr";
//		String str =  helper.fromTextIdLanguageCodeWithDefault(null, id, lang);
//		if (str == null)
//			System.out.println("No data for ID " + id + " in " + lang + ".  Error...");
//		else
//			System.out.println(id + "/" + lang + " - ret=" + str);
//		System.out.println("----------  ----------");
//
//		// Get another text (not in language, available in English)
//		id = "3210";
//		str =  helper.fromTextIdLanguageCodeWithDefault(null, id, lang);
//		if (str == null)
//			System.out.println("No data for ID " + id + " in " + lang + ".  Error");
//		else
//			System.out.println(id + "/" + lang + " - ret=" + str + ".  Expected result (English)");
//		System.out.println("----------  ----------");
//
//		// Try a bogus language
//		id = "3333";
//		lang = "es_ME";
//		str =  helper.fromTextIdLanguageCodeWithDefault(null, id, lang);
//		if (str == null)
//			System.out.println("No data for ID " + id + " in " + lang + ".  Error");
//		else
//			System.out.println(id + "/" + lang + " - ret=" + str + ".  Expected result (english.");
//		System.out.println("----------  ----------");
//
//		// Try a bogus label
//		id = "99999";
//		lang = "en";
//		str =  helper.fromTextIdLanguageCodeWithDefault(null, id, lang);
//		if (str == null)
//			System.out.println("No data for ID " + id + " in " + lang + ".  Expected result");
//		else
//			System.out.println(id + "/" + lang + " - ret=" + str + ".  Error.");
//		
//		UnitTestUtils.stop(this);
//	}

	
	/*
	 * testLast - A last look at the cache
	 */
	public void testLast()
		throws Exception
	{
			UnitTestUtils.start(this);
			V2DatabaseUtils.setUnitTest(true);

			TextHelper th = new TextHelper();
			System.out.println(th.showCache());
			
			UnitTestUtils.stop(this);
	}

}
