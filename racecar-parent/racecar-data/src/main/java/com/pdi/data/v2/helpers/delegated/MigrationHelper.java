package com.pdi.data.v2.helpers.delegated;

import java.util.HashMap;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.interfaces.IMigrationHelper;
import com.pdi.data.v2.helpers.ParticipantDataHelper;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.GroupReport;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.scoring.Norm;
import com.pdi.scoring.NormSet;

public class MigrationHelper implements IMigrationHelper {

	private static final Logger log = LoggerFactory.getLogger(MigrationHelper.class);
	//@Override
	public void tltIndividualReportMigration(SessionUser session, int currentLevel,
			int targetLevel, IndividualReport ir, boolean includeCogs) {
		log.debug("indiv. migration helper... {}", targetLevel);
  		int indivNormsetId = new ParticipantDataHelper().getIndivNormsetId(targetLevel + "");	// Destination level adjusted transition level

		NormSet normset;
		normset = new ParticipantDataHelper().getPdiTransitionNormset(indivNormsetId);
		normset = new ParticipantDataHelper().populateNormset(normset);

		for(Norm norm : normset.getNorms())
		{
			//System.out.println("NORMNAMES:  " + norm.getName()+ "_MEAN  " + norm.getMean());
			ir.addDisplayData(norm.getName()+ "_MEAN", (new Double(norm.getMean())).toString());
			ir.addDisplayData(norm.getName()+ "_STDEV", (new Double(norm.getStdDev())).toString());
		}
		if(includeCogs) {
			ir.getDisplayData().put(ReportingConstants.RC_COGNITIVES_INCLUDED, "1");
		} else {
			ir.getDisplayData().put(ReportingConstants.RC_COGNITIVES_INCLUDED, "0");
		}

	}

	//@Override
	public void tltGroupReportMigration(SessionUser session,
			int currentLevel, int targetLevel, GroupReport ret, boolean includeCogs) {
		//System.out.println("group. migration helper...");
		//  Get the group normset
		HashMap<String, NormSet> groupNormSets = new HashMap<String, NormSet>();
		groupNormSets = new ParticipantDataHelper().getPdiNormsets(currentLevel, targetLevel);
		//System.out.println("currentLevel: " + currentLevel + "  targetLevel: " + targetLevel);		
		Iterator<String> it = groupNormSets.keySet().iterator();
		while (it.hasNext()) 
		{		
			String s = it.next();
			 NormSet ns = groupNormSets.get(s);
			//System.out.println("ns display name: " + ns.getDisplayName() + "  genPop?: " + ns.getGeneralPopulation()
			//					+ "  transLevel: " + ns.getTransitionLevel() + " transType: " + ns.getTransitionType() 
			//					+ " nsId: " + ns.getNormsetId().toString());			 
			 for(Norm n : ns.getNorms())
			 {			 
				ret.addDisplayData(ns.getDisplayName() + "_" + n.getName()+ "_MEAN", new Double(n.getMean()).toString());
				ret.addDisplayData(ns.getDisplayName() + "_" + n.getName()+ "_STDEV", new Double(n.getStdDev()).toString());
				ret.addDisplayData(ns.getDisplayName() + "_" + n.getName()+ "_WEIGHT", new Double(n.getWeight()).toString());
			 }
		}
		
		if(includeCogs) {
			ret.getDisplayData().put(ReportingConstants.RC_COGNITIVES_INCLUDED, "1");
		} else {
			ret.getDisplayData().put(ReportingConstants.RC_COGNITIVES_INCLUDED, "0");
		}
	}

}
