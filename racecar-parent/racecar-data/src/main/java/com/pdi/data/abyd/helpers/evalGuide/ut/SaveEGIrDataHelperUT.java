package com.pdi.data.abyd.helpers.evalGuide.ut;

import java.sql.Connection;
import java.util.ArrayList;

import com.pdi.data.abyd.dto.evalGuide.EGIrDataDTO;
import com.pdi.data.abyd.dto.evalGuide.EGIrScoreData;
import com.pdi.data.abyd.helpers.evalGuide.SaveEGIrDataHelper;
import com.pdi.ut.UnitTestUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;

import junit.framework.TestCase;

public class SaveEGIrDataHelperUT extends TestCase
{
	//
	// Constructors
	//
	public SaveEGIrDataHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
	throws Exception
	{
		junit.textui.TestRunner.run(SaveEGIrDataHelperUT.class);
	}


	/*
	 * testSaveIrData
	 */
	public void testSaveIrData()
		throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);
		
		Connection con = AbyDDatabaseUtils.getDBConnection();
    	ArrayList<EGIrScoreData> irDat = new ArrayList<EGIrScoreData>();
    	for (int i=0; i < 4; i++)
    	{
    		EGIrScoreData cur = new EGIrScoreData();
    		cur.setIrSeq(i + 1);
    		cur.setIrScore((i + 1) * 2);
    		irDat.add(cur);
    	}
    	
		EGIrDataDTO inp = new EGIrDataDTO();
    	inp.setParticipantId("KENSPART");
    	inp.setDnaId(999);
    	inp.setModuleId(888);
    	inp.setIrArray(irDat);

		SaveEGIrDataHelper helper = new SaveEGIrDataHelper(con, inp);    
		helper.saveIrData();
		System.out.println("Check the db...");

		UnitTestUtils.stop(this);
	}
}
