/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.intGrid;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import com.pdi.data.abyd.dto.intGrid.IGExtCogData;
import com.pdi.data.abyd.dto.intGrid.IGExtCompData;
import com.pdi.data.abyd.dto.intGrid.IGExtEGCompletionData;
import com.pdi.data.abyd.dto.intGrid.IGExtractDataDTO;
import com.pdi.data.abyd.dto.intGrid.IGExtRatingData;
import com.pdi.data.abyd.helpers.intGrid.IGConstants;
import com.pdi.data.abyd.helpers.intGrid.IGScoring.ScoreAndNormData;
import com.pdi.data.util.language.DefaultLanguageHelper;


/**
 * IGExtractDataHelper contains the code needed to extract and return an IGExtractDataDTO.
 * NOTES:
 * 	-- This method is invoked from within the Integration grid.
 * 	-- Much of the data may be present there.
 * 	-- The business desires all possible data to be reported, even if there is nothing
 * 		to report there (e.g., the intersections are not selected for a competency).
 * 	-- This means that we will to go to the database to generate all of the data (and
 * 		not just those extra bits).  That way the UI doesn't have to interleave them in.
 * 	-- Certain other bits of data remain in the purview of the UI and will not be
 * 		returned in this version, even though many of them could be derived.  If, in the
 * 		future it is determined that it is more desirable to include them here, the design
 * 		will be changed at that time to accommodate that change.  Those bits of data not
 * 		currently being returned include the following:
 * 			- Candidate last name
 * 			- Candidate first name
 * 			- Client name
 * 			- Job (DNA) name
 * 			- Overall competency score
 * 			- dLCI
 *
 * @author		Ken Beukelman
 */
public class IGExtractDataHelper
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private Connection con;
	private String partId;
	private long dnaId;
	
	private long dfltLangId;

	// The return data
	IGExtractDataDTO ret = new IGExtractDataDTO();
	
	// The norm name map
	// Norm names by abbreviation
	Map<String, String> normNameMap;
	
	//
	//
	// Constructors.
	//
	public IGExtractDataHelper(Connection con, String partId, long dnaId)
		throws Exception
	{
		if (con == null)
			throw new Exception("A vaild connection is required for the IG extract function.");
		
		// check for key data
		if (partId == null || partId.length() == 0  ||  dnaId == 0)
		{
			throw new Exception("Invalid IG Data key:  part=" +  (partId == null ? "null" : (partId.length() == 0 ? "<empty string>" : partId)) +
					            ", dna=" + dnaId);
		}

		this.con = con;
		this.partId = partId;
		this.dnaId = dnaId;
		
		this.dfltLangId = DefaultLanguageHelper.fetchDefaultLanguageId();
	}


	//
	// Instance methods.
	//
	//

	// It appears that this is no longer used.  At this juncture, it appears that this method is not called from active code
	/**
	 * Get the extract data
	 *
	 *@return An IGExtractDataDTO object
	 * @throws Exception
	 */
	public IGExtractDataDTO getIGExtractData()
		throws Exception
	{
		// See comment above
		System.out.println("---> Questionable call to getIGExtractData; trace this out if this message appears.");
		
		// Put in pass-through data
		this.ret.setPartId(this.partId);

		// Get model name, Eval Guide list, and eval guide completion
		getModLevelData();

		// get competency scores
		getCompScoreData();
		
		ScoreAndNormData dat = IGScoring.fetchIGExtractScoresAndNorms(this.con, this.partId, this.dnaId);

		// Set up norm names internally for later use
		this.normNameMap = setUpNormNames(dat.getNormMap());
		if (this.normNameMap == null || this.normNameMap.size() == 0)
		{
			throw new Exception("No norm names available in getIGExtractData");
		}
		
		// get cognitive score data
		getCogScoreData(dat.getScoreMap());

		// Get LEI scores
		getLeiScoreData(dat.getScoreMap());

		// Get GPI scores
		getGpiScoreData(dat.getRdi(), dat.getScoreMap());

		// Success Profile no longer exists
		//// Success Profile
		//getSuccessProfile();
		
		return this.ret;
	}


	/**
	 * Get some header and eg related data.
	 * 
	 * NOTE: This is a 3 part query.  It gets the following items:
	 * 			- model name,
	 * 			- the eval guide names and dates, and
	 * 			- the integration grid date and the working notes.
	 *
	 * @throws Exception
	 */
	private void getModLevelData()
		throws Exception
	{
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT DISTINCT dna.modelId as dnaModelId, m.modelId modelId, ");
		sqlQuery.append("                t1.text as modelName, ");
		sqlQuery.append("                dl1.moduleId, ");
		sqlQuery.append("                t2.text as moduleName, ");
		sqlQuery.append("                ISNULL(dl2.moduleId, 0) as assignedFlag, ");
		sqlQuery.append("                egr.lastDate as egDate, ");
		sqlQuery.append("                igr.lastDate as igLast, ");
		sqlQuery.append("                igr.igLockDate as igLock, ");
		sqlQuery.append("                igr.workingNotes, ");
		sqlQuery.append("                dl1.modSeq ");
		sqlQuery.append("  FROM pdi_abd_dna dna ");
		sqlQuery.append("    JOIN pdi_abd_model m ");
		sqlQuery.append("    LEFT JOIN pdi_abd_text t1 ON (t1.textId = m.textId AND t1.languageId = " + this.dfltLangId + ") ");
		sqlQuery.append("    LEFT JOIN pdi_abd_dna_link dl1 ON dl1.dnaId = m.dnaId ");
		sqlQuery.append("    LEFT JOIN pdi_abd_module mdul ON (mdul.moduleId = dl1.moduleId)  ");  
		sqlQuery.append("    LEFT JOIN pdi_abd_text t2 ON (t2.textId = mdul.textId AND t2.languageId = " + this.dfltLangId + ") ");
		sqlQuery.append("    LEFT JOIN pdi_abd_dna_link dl2 ON (dl2.dnaId = dna.dnaId ");
		sqlQuery.append("                                   AND dl2.moduleId = dl1.moduleId) ");
		sqlQuery.append("    LEFT JOIN pdi_abd_eg_response egr ON (egr.participantId = '"+ this.partId + "' ");                                     
		sqlQuery.append("                                      	AND egr.dnaId = dna.dnaId ");                                     
		sqlQuery.append("                                      	AND egr.moduleId = dl2.moduleId) ");		
		sqlQuery.append("    LEFT JOIN pdi_abd_igrid_response igr ON (igr.participantId = '" + this.partId + "' ");
		sqlQuery.append("                                         AND igr.dnaId = dna.dnaId ");
		sqlQuery.append("                                         AND igr.igSubmitted = 1) ");
		sqlQuery.append("  WHERE dna.dnaId = " + this.dnaId + " ");
		sqlQuery.append("  ORDER BY modelName, dl1.modSeq");
		//System.out.println("getModLevelData SQL: " + sqlQuery.toString());
		try
		{
		    stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			// process into the module list
			boolean first = true;
			SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
			Date date, lock, last;
			String dnaModelId = "";
			while (rs.next())
			{
				if (first)
				{
					this.ret.setModelName(rs.getString("modelName"));
					last = rs.getDate("igLast");
					lock = rs.getDate("igLock");
					date = lock != null ? lock : last;
					this.ret.setIgCompletionDate(date == null ? "" : fmt.format(date));
					this.ret.setWorkingNotes(rs.getString("workingNotes"));
					
					dnaModelId = rs.getString("dnaModelId");					
					first = false;
				}

				String curModelId = rs.getString("modelId");
				
				IGExtEGCompletionData dateElt = new IGExtEGCompletionData();
				dateElt.setModelName(rs.getString("modelName"));
				dateElt.setModuleName(rs.getString("moduleName"));
				
				date = rs.getDate("egDate");
				if(curModelId.equals(dnaModelId) && date != null)
				{
					// If the current model id matches the DNA model id,
					// this is the date to use for EG date values...
					dateElt.setDateString(fmt.format(date));
				}

				this.ret.getEgCompletionDates().add(dateElt);
			}
			
			return;
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL getModLevelData.  " +
							"SQLException: " + ex.getMessage() + ", " +
							"SQLState: " + ex.getSQLState() + ", " +
							"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (rs != null)
			{
			    try {  rs.close();  }
			    catch (SQLException sqlEx)
			    {
			    	System.out.println("ERROR: rs close in getModLevelData - " + sqlEx.getMessage());
			    }
				rs = null;
			}
			if (stmt != null)
			{
			    try {  stmt.close();  }
			    catch (SQLException sqlEx)
			    {
			    	System.out.println("ERROR: stmt close in getModLevelData - " + sqlEx.getMessage());
			    }
			    stmt = null;
			}
		}		    
	}


	/**
	 * Get the competency final score data
	 *
	 * @throws Exception
	 */
	private void getCompScoreData()
		throws Exception
	{
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();

		// first get all competencies in order
		sqlQuery.append("  SELECT DISTINCT 1 AS typer, ");
		sqlQuery.append("                  mdl.internalName AS modName, ");
		sqlQuery.append("                  mdl.dnaId AS dnaId, ");
		sqlQuery.append("                  dl.competencyId AS compId, ");
		sqlQuery.append("                  t1.text AS compName, ");
		sqlQuery.append("                  0 AS scorIdx, ");
		//sqlQuery.append("                  0 AS essential, ");
		sqlQuery.append("                  mdl.modelSeq AS seq1, ");
		sqlQuery.append("                  dl.groupSeq AS seq2, ");
		sqlQuery.append("                  dl.compSeq AS seq3 ");
		sqlQuery.append("    FROM pdi_abd_model mdl ");
		sqlQuery.append("      INNER JOIN pdi_abd_dna_link dl ON dl.dnaId = mdl.dnaId ");
		sqlQuery.append("      LEFT JOIN pdi_abd_competency cmp ON cmp.competencyId = dl.competencyId ");
		sqlQuery.append("      LEFT JOIN pdi_abd_text t1 ON (t1.textId = cmp.textId AND t1.languageId = " + this.dfltLangId + ") ");
		sqlQuery.append("UNION ");
		// Then get score indices completed for this project
		sqlQuery.append("  SELECT 2 AS typer, ");
		sqlQuery.append("              NULL AS modName,  ");
		sqlQuery.append("              icr.dnaId AS dnaId, ");
		sqlQuery.append("              icr.competencyId AS compId, ");
		sqlQuery.append("              NULL AS compNane, ");
		sqlQuery.append("              icr.lcscore AS scorIdx, ");
		//sqlQuery.append("              false AS essential, ");
		sqlQuery.append("              0 AS seq1, ");
		sqlQuery.append("              0 AS seq2, ");
		sqlQuery.append("              0 AS seq3 ");
		sqlQuery.append("    FROM pdi_abd_igrid_comp_resp icr ");
		sqlQuery.append("    WHERE icr.participantId = '" + this.partId + "' ");
		sqlQuery.append("      AND icr.dnaId = " + this.dnaId + " ");
		sqlQuery.append("ORDER BY typer, seq1, seq2, seq3");
		//	System.out.println("getCompScoreData SQL " + sqlQuery.toString());

		try
		{
		    stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());
			
			LinkedHashMap<String, IGExtCompData> comps = new LinkedHashMap<String, IGExtCompData>();
			while (rs.next())
			{
				int type = rs.getInt("typer");
				String key = rs.getString("compId");
				switch (type)
				{
					case 1:
						// create the elt in a map
						IGExtCompData elt1 = new IGExtCompData();
						elt1.setCompName(rs.getString("modName") + " " + rs.getString("compName"));
						comps.put(key, elt1);
						
						break;
					case 2:
						// stuff the score index into the existing map elt
						IGExtCompData elt2 = comps.get(key);
						if (elt2 == null)
						{
							throw new Exception("Invalid key (" + key + ") fetching comp score data (2)");
						}
						elt2.setCompFinalScoreIdx(rs.getInt("scorIdx"));
						break;
						// No default - only types 1 or 2 can appear
				}
			}
			
			// Put away the score data			
			this.ret.setCompData(new ArrayList<IGExtCompData>(comps.values()));
			
			return;
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL getCompScoreData.  " +
							"SQLException: " + ex.getMessage() + ", " +
							"SQLState: " + ex.getSQLState() + ", " +
							"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (rs != null)
			{
			    try {  rs.close();  }
			    catch (SQLException sqlEx)
			    {
			    	System.out.println("ERROR: rs close in getCompScoreData - " + sqlEx.getMessage());
			    }
				rs = null;
			}
			if (stmt != null)
			{
			    try {  stmt.close();  }
			    catch (SQLException sqlEx)
			    {
			    	System.out.println("ERROR: stmt close in getCompScoreData - " + sqlEx.getMessage());
			    }
			    stmt = null;
			}
		}		    
	}


	/**
	* Get norm names for all instruments into the output map
	* 
	* @throws Exception
	*/
	private Map<String, String> setUpNormNames(Map<String, String> normMap)
	{
		Map<String, String> ret = new HashMap<String, String>();
		
		// Get all possible norm names.  If not in the map, the norm is "Not Available"
		for (Iterator<String> itr=IGConstants.IG_INST_KEYS.iterator(); itr.hasNext(); )
		{
			// get the output key
			String key = itr.next();

			// Get the key from the normMap
			String normName = normMap.get(key);
			String name = (normName == null) ? "Not Available" : normName;

			ret.put(key, name);
		}
		
		// now add the LEI
		String normName = normMap.get(IGConstants.LEI_INST);
		String name = (normName == null) ? "Not Available" : normName;
		ret.put(IGConstants.LEI_INST, name);
		
		return ret;
	}


	/**
	 * Get the Cognitive test scores into the DTO
	 * 
	 * @param scoreMap - Map of PDI ratings and percentiles containing all ratings available for this person
	 * @param normMap - Map of norms used by all available instruments done by this person
	 * @throws Exception
	 */
	private void getCogScoreData(Map<String, IGScoring.ScorePair> scoreMap)
//		throws Exception
	{
		// do the cog instrument scores in order...
		for(Iterator<String> itr=IGConstants.COG_INST_KEYS.iterator(); itr.hasNext(); )
		{
			IGExtCogData curEnt = new IGExtCogData();
			
			String key = itr.next();
			
			String name = IGConstants.INST_NAMES.get(key);
			curEnt.setCogName(name);
			
			String rating = "Not Available";
			if (scoreMap.get(key) != null && scoreMap.get(key).getRating() != null)
			{
				rating = scoreMap.get(key).getRating();
			}
			curEnt.setCogRating(rating);

			String pctl = "Not Available";
			if (scoreMap.get(key) != null && scoreMap.get(key).getPctl() != null)
			{
				pctl = scoreMap.get(key).getPctl();
			}
			curEnt.setCogPctl(pctl);
			
			String norm = this.normNameMap.get(key) == null ? "Not Available" : this.normNameMap.get(key);
			curEnt.setCogNormName(norm);
			
			this.ret.getCogData().add(curEnt);
		}
		
		return;
	}


	/**
	 * Get the LEI scores into the DTO
	 * 
	 * @param ratingMap - Map of PDI ratings containing all ratings available for this person
	 * @param normMap - Map of norms used by all available instruments done by this person
	 */
	private void getLeiScoreData(Map<String, IGScoring.ScorePair> scoreMap)
	{
			// get the scores in order...
			for(Iterator<String> itr=IGConstants.LEI_SCALE_ORDER.iterator(); itr.hasNext(); )
			{
				String id = itr.next();
				
				IGExtRatingData leiRating = new IGExtRatingData();
				leiRating.setName(IGConstants.SCALE_NAMES.get(id));
				String rating = "Not Available";
				if (scoreMap.get(id) != null && scoreMap.get(id).getRating() != null)
				{
					rating = scoreMap.get(id).getRating();
				}

				leiRating.setRating(rating);
				this.ret.getLeiRatings().add(leiRating);
			}
			
			// get the norm label
			String normName = this.normNameMap.get("LEI3");
			this.ret.setLeiNormName(normName);
	}


	/**
	 * Get the GPI scores into the DTO
	 * 
	 * @param ratingMap - Map of PDI ratings containing all ratings available for this person
	 * @param normMap - Map of norms used by all available instruments done by this person
	 */
	private void getGpiScoreData(String rdi, Map<String, IGScoring.ScorePair> scoreMap)
	{
		// get the scores in order...
		for(Iterator<String> itr=IGConstants.GPI_SCALE_ORDER.iterator(); itr.hasNext(); )
		{
			String id = itr.next();

			IGExtRatingData gpi = new IGExtRatingData();
			gpi.setName(IGConstants.SCALE_NAMES.get(id));
			String rating = "Not Available";
			if (scoreMap.get(id) != null && scoreMap.get(id).getRating() != null)
			{
				rating = scoreMap.get(id).getRating();
			}
			gpi.setRating(rating);
			this.ret.getGpiRatings().add(gpi);
		}
		
		// get the RDI
		this.ret.setRdi(rdi);
		
		// get the norm label
		String normName = this.normNameMap.get("GPI");
		this.ret.setGpiNormName(normName);
	}


// Success Profile no longer exists
//	/**
//	 * Get the success profile data
//	 * 
//	 * All of the data is required in setup, so we assume that all data is
//	 * present and will make no checks here for completeness of the data.
//	 *
//	 * @throws Exception
//	 */
//	private void getSuccessProfile()
//		throws Exception
//	{
//		// before I do anything, I need to set up this array, so I can 
//		// put the stuff into it in the right order... so I'm going to
//		// put dummy objects (which will be overwritten) into it.... 
//		for(int i = 0; i < (IGConstants.SUCCESS_PROFILE_FIELDS_INDEXES.length + 1); i++){
//			IGExtSPData elt = new IGExtSPData();
//			elt.setSpName("name");  
//			elt.setSpText("text");
//			this.ret.getSuccessProfile().add(elt);
//		}
//		
//		Statement stmt = null;
//		ResultSet rs = null;
//		
//		StringBuffer sqlQuery = new StringBuffer();
//		sqlQuery.append("SELECT spt.spTextType, ");
//		sqlQuery.append("       spt.spText ");
//		sqlQuery.append("  FROM pdi_abd_dna_sp_comp_text spt ");
//		sqlQuery.append("  WHERE spt.dnaId = " + this.dnaId);
//		//System.out.println("getSuccessProfile SQL  " + sqlQuery.toString());
//		try
//		{
//		    stmt = this.con.createStatement();
//			rs = stmt.executeQuery(sqlQuery.toString());
//
//			while (rs.next())
//			{
//				Long type = rs.getLong("spTextType");
//				Integer index = (Integer) IGConstants.SUCCESS_PROFILE_FIELDS_INDEXES[(type.intValue()-1)][0];
//				String fieldName = (String)IGConstants.SUCCESS_PROFILE_FIELDS_INDEXES[type.intValue()-1][1];
//				//System.out.println("index " + index + "  fieldName " + fieldName);
//				
//				IGExtSPData elt = new IGExtSPData();
//				elt.setSpName(fieldName);  
//				elt.setSpText(rs.getString("spText"));
//				
//				this.ret.getSuccessProfile().set(index.intValue(), elt);
//			}
//			
//			return;
//		}
//		catch (SQLException ex)
//		{
//			// handle any errors
//			throw new Exception("SQL getSuccessProfile.  " +
//							"SQLException: " + ex.getMessage() + ", " +
//							"SQLState: " + ex.getSQLState() + ", " +
//							"VendorError: " + ex.getErrorCode());
//		}
//		finally
//		{
//			if (rs != null)
//			{
//			    try {  rs.close();  }
//			    catch (SQLException sqlEx)
//			    {
//			    	System.out.println("ERROR: rs close in getSuccessProfile - " + sqlEx.getMessage());
//			    }
//				rs = null;
//			}
//			if (stmt != null)
//			{
//			    try {  stmt.close();  }
//			    catch (SQLException sqlEx)
//			    {
//			    	System.out.println("ERROR: stmt close in getSuccessProfile - " + sqlEx.getMessage());
//			    }
//			    stmt = null;
//			}
//		}		    
//	}
}