package com.pdi.data.abyd.helpers.intGrid.ut;

import java.sql.Connection;

import com.pdi.data.abyd.dto.intGrid.IGLcNameDTO;
import com.pdi.data.abyd.helpers.intGrid.SaveIGLcNameDataHelper;
import com.pdi.ut.UnitTestUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;

import junit.framework.TestCase;

public class SaveIGLcNameDataHelperUT  extends TestCase
{
	//
	// Constructors
	//
	public SaveIGLcNameDataHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
	throws Exception
	{
		junit.textui.TestRunner.run(SaveIGLcNameDataHelperUT.class);
	}


	/*
	 * testSaveLcName
	 */
	public void testSaveLcName()
		throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);
		
		Connection con = AbyDDatabaseUtils.getDBConnection();
		IGLcNameDTO inp = new IGLcNameDTO();
		inp.setPartId("9999");
    	inp.setDnaId(99);
    	inp.setLcFirstName("Timotheus");
    	inp.setLcLastName("O'Reilly - HUT");

		// get the data
		SaveIGLcNameDataHelper helper = new SaveIGLcNameDataHelper(con, inp);        
		helper.saveLcName();
		System.out.println("Complete... check database");

		UnitTestUtils.stop(this);
	}

}
