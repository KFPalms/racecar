/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.evalGuide;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;

import com.pdi.data.abyd.dto.evalGuide.EGBarScoreData;
import com.pdi.data.abyd.dto.evalGuide.EGCompDataDTO;
import com.pdi.data.abyd.dto.evalGuide.EGCompOverideDataDTO;

/**
 * SaveEGBarScoreDataHelper contains the code needed to save the BAR
 * score data for a single competency.  It includes the data for the
 * BAR scores and the competency note.  Competency overide scores are
 * handled elsewhere.
 *
 * @author		Ken Beukelman
 */
public class SaveEGCompDataHelper
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private Connection con;

	//
	//
	// Constructors.
	//
	public SaveEGCompDataHelper(Connection con)
		throws Exception
	{
		// Save connection for later use
		this.con = con;
	}

	//
	// Instance methods.
	//

	/**
	 * Update the competency note field in the EG Response table and
	 * the scores in the eg_ir_response table.  Returns nothing.
	 *
	 * @throws Exception
	 */
	public void saveEgScoreData(EGCompDataDTO scoreData)
		throws Exception
	{
		// check for key data
		if (scoreData.getParticipantId() == null || scoreData.getParticipantId().length() == 0  ||
			scoreData.getDnaId()	== 0 ||
			scoreData.getModuleId() == 0 ||
			scoreData.getCompId() == 0)
		{
			throw new Exception("Invalid Comp Data key:  part=" +  (scoreData.getParticipantId() == null ? "null" : (scoreData.getParticipantId().length() == 0 ? "<empty string>" : scoreData.getParticipantId())) +
					            ", dna=" + scoreData.getDnaId() +
					            ", module=" + scoreData.getModuleId() +
					            ", comp=" + scoreData.getCompId());
		}

		// Do the note...
		saveCompNote(scoreData);
		
		// ...then do the scores
		if (scoreData.getExtCompScore() == null || scoreData.getExtCompScore().isEmpty())
		{
			// Assume that bars are in sight here
			saveBarScores(scoreData);
		}
		else
		{
			// The external score is in sight
			saveExtScore(scoreData);
		}
		
		return;
	}


	/**
	 * Update the competency score index field in the EG Response table
	 *
	 * @throws Exception
	 */
	public void saveCsoData(EGCompOverideDataDTO csoData)
		throws Exception
	{
		// check for key data
		if (csoData.getParticipantId() == null			||
			csoData.getParticipantId().length() == 0	||
			csoData.getDnaId()	== 0					||
			csoData.getModuleId() == 0					||
			csoData.getCompId() == 0  )
		{
			throw new Exception("Invalid Comp Data key (SCO):  part=" +  (csoData.getParticipantId() == null ? "null" : (csoData.getParticipantId().length() == 0 ? "<empty string>" : csoData.getParticipantId())) +
					            ", dna=" + csoData.getDnaId() +
					            ", module=" + csoData.getModuleId() +
					            ", comp=" + csoData.getCompId());
		}

		// Keyss are OK... do it
		saveCso(csoData);
		
		return;
	}


	private boolean doesCompRowExist(String pptId, long dnaId, long moduleId, long compId)
		throws Exception
	{
		Statement stmt = null;
		ResultSet rs = null;
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT count(*) as cnt ");
		sqlQuery.append("  FROM pdi_abd_eg_comp_response cr ");
		sqlQuery.append("  WHERE cr.participantId = '" + pptId + "' ");
		sqlQuery.append("    AND cr.dnaId = " + dnaId + " ");
		sqlQuery.append("    AND cr.moduleId = " + moduleId + " ");
		sqlQuery.append("    AND cr.competencyId = " + compId);

		try
		{
			stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());
			
			// There should always be 1 and only 1 row here
			rs.next();
			boolean exists = rs.getBoolean("cnt");
			
			return exists;
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL doesCompRowExist().  " +
	        						"SQLException: " + ex.getMessage() + ", " +
	        						"SQLState: " + ex.getSQLState() + ", " +
	        						"VendorError: " + ex.getErrorCode());
		}
		finally
		{
		// clean up
			if (rs != null)
			{
				try  {  rs.close();  }
				catch (SQLException sqlEx)  {  /* swallow the error */  }
				rs = null;
			}
			if (stmt != null)
			{
				try  {  stmt.close();  }
				catch (SQLException sqlEx)  {  /* swallow the error */  }
				stmt = null;
			}
		}
	}
	
	/**
	 * Insert/Update the competency note field.
	 *
	 * @throws Exception
	 */
	private void saveCompNote(EGCompDataDTO barScoreData)
		throws Exception
	{
		Statement stmt = null;
		ResultSet rs = null;
		String context = null;
		
		// Transform embedded quotes in the note, if needed
		String noteStr =  barScoreData.getCompNote();
		if(noteStr == null)
		{
			noteStr = "";
		}
		if (noteStr.contains("'"))
		{
			noteStr = noteStr.replace("'", "''");
		}
		
		boolean exists = doesCompRowExist(barScoreData.getParticipantId(), barScoreData.getDnaId(), barScoreData.getModuleId(), barScoreData.getCompId());
		
		StringBuffer sqlQuery = new StringBuffer();			
		if (exists)
		{
			// Update
			context = "update";
			sqlQuery.append("UPDATE pdi_abd_eg_comp_response ");
			sqlQuery.append("  SET compNotes = N'" + noteStr + "', ");
			sqlQuery.append("      lastUserId = SYSTEM_USER, ");
			sqlQuery.append("      lastDate = GETDATE()");
			sqlQuery.append("  WHERE participantId = '" + barScoreData.getParticipantId() + "' " );
			sqlQuery.append("    AND dnaId = " + barScoreData.getDnaId() + " ");
			sqlQuery.append("    AND moduleId = " + barScoreData.getModuleId() + " ");
			sqlQuery.append("    AND competencyId = " + barScoreData.getCompId());
		}
		else
		{
			// Insert
			context = "insert";
			sqlQuery.append("INSERT INTO pdi_abd_eg_comp_response ");
			sqlQuery.append("  (participantId, ");
			sqlQuery.append("   dnaId, ");
			sqlQuery.append("   moduleId,");
			sqlQuery.append("   competencyId,");
			sqlQuery.append("   compNotes,");
			sqlQuery.append("   lastUserId, ");
			sqlQuery.append("   lastDate) ");
			sqlQuery.append("VALUES(" + barScoreData.getParticipantId() + ", ");
			sqlQuery.append("       " + barScoreData.getDnaId() + ", ");
			sqlQuery.append("       " + barScoreData.getModuleId() + ", ");
			sqlQuery.append("       " + barScoreData.getCompId() + ", ");
			sqlQuery.append("       N'" + noteStr + "', ");
			sqlQuery.append("         SYSTEM_USER," );
			sqlQuery.append("         GETDATE())");
		}
		
		try
		{
	       	stmt = this.con.createStatement();
	       	stmt.executeUpdate(sqlQuery.toString());
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL saveCompNote (" + context + ").  " +
	        						"SQLException: " + ex.getMessage() + ", " +
	        						"SQLState: " + ex.getSQLState() + ", " +
	        						"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (rs != null)
			{
				try  {  rs.close();  }
				catch (SQLException sqlEx)  {  /* swallow the error */  }
				rs = null;
			}
			if (stmt != null)
			{
				try  {  stmt.close();  }
				catch (SQLException sqlEx)  {  /* swallow the error */  }
				stmt = null;
			}
		}
	}

	
	/**
	 * Insert/Update the BAR scores
	 *
	 * @throws Exception
	 */
	private void saveBarScores(EGCompDataDTO barScoreData)
		throws Exception
	{
		String context = null;
		ResultSet rs = null;
		Statement selStmt = null;
		PreparedStatement insPstmt = null;
		PreparedStatement updPstmt = null;

		// Certain portions of the key (participant, DNA, module, and competency)
		// remain constant for the life of the prepared statement; they are being
		// inserted at creation time rather than being set every time
		StringBuffer insQuery = new StringBuffer();
		insQuery.append("INSERT INTO pdi_abd_eg_bar_response ");
		insQuery.append("  (participantId, ");
		insQuery.append("   dnaId, ");
		insQuery.append("   moduleId, ");
		insQuery.append("   competencyId, ");
		insQuery.append("   barScore, ");
		insQuery.append("   barSequence, ");
		insQuery.append("   lastUserId, ");
		insQuery.append("   lastDate) ");
		insQuery.append("  VALUES(" + barScoreData.getParticipantId() + ", ");
		insQuery.append("         " + barScoreData.getDnaId() + ", ");
		insQuery.append("         " + barScoreData.getModuleId() + ", ");
		insQuery.append("         " + barScoreData.getCompId() + ", ");
		insQuery.append("         ?, ");
		insQuery.append("         ?, ");
		insQuery.append("         SYSTEM_USER," );
		insQuery.append("         GETDATE())");

		// see comment above about embedded information
		StringBuffer updQuery = new StringBuffer();
		updQuery.append("UPDATE pdi_abd_eg_bar_response ");
		updQuery.append("  SET barScore = ?, ");
		updQuery.append("      lastUserId = SYSTEM_USER, ");
		updQuery.append("      lastDate = GETDATE() ");
		updQuery.append("  WHERE participantId = " + barScoreData.getParticipantId() + " ");
		updQuery.append("    AND dnaId = " + barScoreData.getDnaId() + " ");
		updQuery.append("    AND moduleId = " + barScoreData.getModuleId() + " ");
		updQuery.append("    AND competencyId = " + barScoreData.getCompId() + " ");
		updQuery.append("    AND barSequence = ?");
		
		try
		{
			// prep the insert statement
			context = "prep - Insert";
			insPstmt = con.prepareStatement(insQuery.toString());

			// prep the update statement
			context = "prep - Update";
			updPstmt = con.prepareStatement(updQuery.toString());
			
			// get the existing sequence numbers for this part/dna/mod/comp
			StringBuffer selQuery = new StringBuffer();
			selQuery.append("SELECT br.barSequence ");
			selQuery.append("  FROM pdi_abd_eg_bar_response br ");
			selQuery.append("  WHERE br.participantId = '" + barScoreData.getParticipantId() + "' ");
			selQuery.append("    AND br.dnaId = " + barScoreData.getDnaId() + " ");
			selQuery.append("    AND br.moduleId = " + barScoreData.getModuleId() + " ");
			selQuery.append("    AND br.competencyId = " + barScoreData.getCompId() + " ");

			selStmt = this.con.createStatement();
			rs = selStmt.executeQuery(selQuery.toString());
			
			ArrayList<Integer> seqList = new ArrayList<Integer>();
			while (rs.next())
			{
				seqList.add(new Integer(rs.getInt("barSequence")));
			}
			
			// Now loop through the scores passed in and either insert or
			// update, based upon the bar sequences.
			for(Iterator<EGBarScoreData> itr = barScoreData.getCompBarArray().iterator(); itr.hasNext(); )
			{
				EGBarScoreData cur = itr.next();
				Integer chkIt = new Integer(cur.getBarSeq());
				if(seqList.contains(chkIt))
				{
					// The sequence exists, update the score row.
					context = "update";
					updPstmt.clearParameters();
					updPstmt.clearWarnings();
					context = "result update";
					updPstmt.setInt (1, cur.getBarScore());
					updPstmt.setInt (2, cur.getBarSeq());
					
					updPstmt.executeUpdate();
				}
				else
				{
					// The sequence does not exist, insert a score row.
					// (careful, barSeq & barScore are backwards of the insert)
					context = "insert";
					insPstmt.clearParameters();
					insPstmt.clearWarnings();
					insPstmt.setInt (1, cur.getBarScore());
					insPstmt.setInt (2, cur.getBarSeq());
					
					insPstmt.executeUpdate();
				}
			}	// end of the for loop
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL saveBarScores (" + context + ").  " +
	        			"SQLException: " + ex.getMessage() + ", " +
	        			"SQLState: " + ex.getSQLState() + ", " +
						"VendorError: " + ex.getErrorCode());
		}
		finally
		{
        	if (rs != null)
        	{
        		try {  rs.close();  }
        		catch (SQLException sqlEx)	{  /* swallow the error */  }
        		rs = null;
        	}
        	if (selStmt != null)
        	{
        		try	{ selStmt.close();  }
        		catch (SQLException sqlEx)	{  /* swallow the error */  }
        		selStmt = null;
        	}
        	if (insPstmt != null)
        	{
        		try	{ insPstmt.close();  }
        		catch (SQLException sqlEx)	{  /* swallow the error */  }
        		insPstmt = null;
        	}
        	if (updPstmt != null)
        	{
        		try	{ updPstmt.close();  }
        		catch (SQLException sqlEx)	{  /* swallow the error */  }
        		updPstmt = null;
        	}
		}
	}


	/**
	 * Insert/Update the external competency score
	 * @param scoreData
	 * @throws Exception
	 */
	private void saveExtScore(EGCompDataDTO scoreData)
		throws Exception
		{
			if (scoreData.getParticipantId() == null	||
				scoreData.getParticipantId().isEmpty()	||
				scoreData.getDnaId()	== 0			||
				scoreData.getModuleId() == 0			||
				scoreData.getCompId() == 0  )
		{
			throw new Exception("Invalid Comp Data key (ext comp score):  part=" +  (scoreData.getParticipantId() == null ? "null" : (scoreData.getParticipantId().length() == 0 ? "<empty string>" : scoreData.getParticipantId())) +
					            ", dna=" + scoreData.getDnaId() +
					            ", module=" + scoreData.getModuleId() +
					            ", comp=" + scoreData.getCompId());
		}

		Statement stmt = null;
		ResultSet rs = null;
		String context = null;
		
		boolean exists = doesCompRowExist(scoreData.getParticipantId(), scoreData.getDnaId(), scoreData.getModuleId(), scoreData.getCompId());

		StringBuffer sqlQuery = new StringBuffer();			
		if (exists)
		{
			// Update
			context = "update";
			sqlQuery.append("UPDATE pdi_abd_eg_comp_response ");
			sqlQuery.append("  SET extCompScore = " + scoreData.getExtCompScore() + ", ");
			sqlQuery.append("      lastUserId = SYSTEM_USER, ");
			sqlQuery.append("      lastDate = GETDATE()");
			sqlQuery.append("  WHERE participantId = '" + scoreData.getParticipantId() + "' " );
			sqlQuery.append("    AND dnaId = " + scoreData.getDnaId() + " ");
			sqlQuery.append("    AND moduleId = " + scoreData.getModuleId() + " ");
			sqlQuery.append("    AND competencyId = " + scoreData.getCompId());
		}
		else
		{
			// Insert
			context = "insert";
			sqlQuery.append("INSERT INTO pdi_abd_eg_comp_response ");
			sqlQuery.append("  (participantId, ");
			sqlQuery.append("   dnaId, ");
			sqlQuery.append("   moduleId,");
			sqlQuery.append("   competencyId,");
			sqlQuery.append("   compNotes,");	// Required because a TEXT field cannot have a default value in this version of MySQL
			sqlQuery.append("   extCompScore,");
			sqlQuery.append("   lastUserId, ");
			sqlQuery.append("   lastDate) ");
			sqlQuery.append("VALUES(" + scoreData.getParticipantId() + ", ");
			sqlQuery.append("       " + scoreData.getDnaId() + ", ");
			sqlQuery.append("       " + scoreData.getModuleId() + ", ");
			sqlQuery.append("       " + scoreData.getCompId() + ", ");
			sqlQuery.append("       N'', ");
			sqlQuery.append("       " + scoreData.getExtCompScore() + ", ");
			sqlQuery.append("         SYSTEM_USER," );
			sqlQuery.append("         GETDATE())");
		}

		try
		{
	       	stmt = this.con.createStatement();
	       	stmt.executeUpdate(sqlQuery.toString());
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL saveExtScore (" + context + ").  " +
	        						"SQLException: " + ex.getMessage() + ", " +
	        						"SQLState: " + ex.getSQLState() + ", " +
	        						"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (rs != null)
			{
				try  {  rs.close();  }
				catch (SQLException sqlEx)  {  /* swallow the error */  }
				rs = null;
			}
			if (stmt != null)
			{
				try  {  stmt.close();  }
				catch (SQLException sqlEx)  {  /* swallow the error */  }
				stmt = null;
			}
		}
	}


	/**
	 * Insert/Update the competency overide field.
	 *
	 * @throws Exception
	 */
	private void saveCso(EGCompOverideDataDTO csoData)
		throws Exception
	{
		Statement stmt = null;
		ResultSet rs = null;
		String context = null;

		boolean exists = doesCompRowExist(csoData.getParticipantId(), csoData.getDnaId(), csoData.getModuleId(), csoData.getCompId());
		
		StringBuffer sqlQuery = new StringBuffer();			
		if (exists)
		{
			// Update
			context = "update";
			sqlQuery.append("UPDATE pdi_abd_eg_comp_response ");
			sqlQuery.append("  SET orScoreIdx = " + csoData.getOrScoreIdx() + ", ");
			sqlQuery.append("      lastUserId = SYSTEM_USER, ");
			sqlQuery.append("      lastDate = GETDATE()");
			sqlQuery.append("  WHERE participantId = " + csoData.getParticipantId() + " " );
			sqlQuery.append("    AND dnaId = " + csoData.getDnaId() + " ");
			sqlQuery.append("    AND moduleId = " + csoData.getModuleId() + " ");
			sqlQuery.append("    AND competencyId = " + csoData.getCompId());
		}
		else
		{
			// Insert
			context = "insert";
			sqlQuery.append("INSERT INTO pdi_abd_eg_comp_response ");
			sqlQuery.append("  (participantId, ");
			sqlQuery.append("   dnaId, ");
			sqlQuery.append("   moduleId,");
			sqlQuery.append("   competencyId,");
			sqlQuery.append("   compNotes,");	// Required because a TEXT field cannot have a default value in this version of MySQL
			sqlQuery.append("   orScoreIdx,");
			sqlQuery.append("   lastUserId, ");
			sqlQuery.append("   lastDate) ");
			sqlQuery.append("VALUES('" + csoData.getParticipantId() + "', ");
			sqlQuery.append("       " + csoData.getDnaId() + ", ");
			sqlQuery.append("       " + csoData.getModuleId() + ", ");
			sqlQuery.append("       " + csoData.getCompId() + ", ");
			sqlQuery.append("       N'', ");
			sqlQuery.append("       " + csoData.getOrScoreIdx() + ", ");
			sqlQuery.append("         SYSTEM_USER," );
			sqlQuery.append("         GETDATE())");
		}

		try
		{
	       	stmt = this.con.createStatement();
	       	stmt.executeUpdate(sqlQuery.toString());
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL saveCso (" + context + ").  " +
	        						"SQLException: " + ex.getMessage() + ", " +
	        						"SQLState: " + ex.getSQLState() + ", " +
	        						"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (rs != null)
			{
				try  {  rs.close();  }
				catch (SQLException sqlEx)  {  /* swallow the error */  }
				rs = null;
			}
			if (stmt != null)
			{
				try  {  stmt.close();  }
				catch (SQLException sqlEx)  {  /* swallow the error */  }
				stmt = null;
			}
		}
	}
}
