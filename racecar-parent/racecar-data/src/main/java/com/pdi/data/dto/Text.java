/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.data.dto;


/**
 * Text is a basic data class that contains information about a text string.
 * 
 * @author		Ken Beukelman
 */
public class Text extends CoreDataObject
{
	//
	// Static data.
	//
	private static final long serialVersionUID = 1L;

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private int id;
	private String labelCode;
	private Language lang;
	private String text;

	//
	// Constructors.
	//
	public Text()
	{
		// Does nothing presently
	}

	public Text(int idIn, String lblIn, Language langIn, String txtIn)
	{
		id = idIn;
		labelCode = lblIn;
		lang = langIn;
		text = txtIn;
	}

	//
	// Instance methods.
	//
	
	/*
	 * clone
	 */
	public Text clone()
	{
		return new Text(this.id, this.labelCode, this.lang, this.text);
	}
	
	/*
	 * toString
	 */
	public String toString()
	{
		String str = "Text:  ";
		str += "id=" + id;
		str += ", lang=" + lang.getCode() + " (" + lang.getId() + ")";
		str += ", label=" + labelCode;
		str += ", txt=" + text;
		
		return str;
	}
	
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
	
	/*****************************************************************************************/
	public int getId()
	{
		return id;
	}
	
	public void setId(int value)
	{
		id = value;
	}	
	
	/*****************************************************************************************/
	public String getLabelCode()
	{
		return labelCode;
	}
	
	public void setLabelCode(String value)
	{
		labelCode = value;
	}	
	
	/*****************************************************************************************/
	public Language getLanguage()
	{
		return lang;
	}
	
	public void setLanguage(Language value)
	{
		lang = value;
	}	
	
	/*****************************************************************************************/
	public String getText()
	{
		return text;
	}
	
	public void setText(String value)
	{
		text = value;
	}	
}
