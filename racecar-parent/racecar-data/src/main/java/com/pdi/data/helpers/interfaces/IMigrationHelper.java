package com.pdi.data.helpers.interfaces;

import com.pdi.data.dto.SessionUser;
import com.pdi.reporting.report.GroupReport;
import com.pdi.reporting.report.IndividualReport;

/**
 * The migration helper is a pseudo helper
 * it is designed to support one-off choices
 * that we must make to easily transition our code away from
 * one place to another
 * 
 * nothing here is permanent, everything here should be replaced by more concrete solutions
 * everything in here must be documented and explained why we need it
 * 
 * @author gmyers
 *
 */
public interface IMigrationHelper {
	
	/**
	 * This will populate a group report with tlt group norm data
	 * Why do we need this: We need this because the current process of
	 * getting tlt specific norm data is not defined in our class structure
	 * the data model is very rough to work with
	 * we have no elegant solution
	 * 
	 * how can we replace this
	 * We need a concept of Product/Collection norms, these are norms that
	 * are applied to a product or collection of objects, these norms
	 * are part of project configuration
	 * 
	 * currently we have none of this 
	 * @param session
	 * @param currentLevel
	 * @param targetLevel
	 * @param ir
	 */
	public void tltIndividualReportMigration(SessionUser session, int currentLevel, int targetLevel, IndividualReport ir, boolean includeCogs);
	
	/**
	 * This will populate an individual report with tlt group norm data
	 * Why do we need this: We need this because the current process of
	 * getting tlt specific norm data is not defined in our class structure
	 * the data model is very rough to work with
	 * we have no elegant solution
	 * 
	 * how can we replace this
	 * We need a concept of Product/Collection norms, these are norms that
	 * are applied to a product or collection of objects, these norms
	 * are part of project configuration
	 * 
	 * currently we have none of this 
	 * @param session
	 * @param currentLevel
	 * @param targetLevel
	 * @param gr
	 */
	public void tltGroupReportMigration(SessionUser session, int currentLevel, int targetLevel, GroupReport gr, boolean includeCogs );
	
}
