/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.util;

import java.sql.SQLException;

import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.util.DatabaseUtils;
import com.pdi.data.util.RequestStatus;
import com.pdi.properties.PropertyLoader;

/**
 * V3DatabaseUtils is a helper class that provides the helper classes with
 * a V3 specific interface to the common data layer execution methods.
 * 
 * Note that the data access methods are static.
 *
 * @author		Ken Beukelman
 */
public class V3DatabaseUtils
{
	//
	// Static data.
	//
	
	private static boolean _isUnitTest = false;

	//
	// Static methods.
	//

	//////////////////////////////////////
	//    static Setters and Getters    //
	//////////////////////////////////////

	/**
	 * Is the request actually a unit test?
	 */
	public static void setUnitTest(boolean value) {
		_isUnitTest = value;
	}

	public static boolean isUnitTest()
	{
		return _isUnitTest;
	}

	//////////////////////////////////////
	//    static data methods           //
	//////////////////////////////////////
	
	/**
	 * Private function, used to determine what database
	 * we are connecting to based on the environment passed 
	 * (development,preproduciton,produciton)
	 * 
	 * @return The environment string
	 */
	private static String determineEnvironment()
	{	
		return PropertyLoader.getProperty("com.pdi.data.v3.application", "datasource");
	}
	
	/**
	 * 
	 */
	public static void determineUnitTest()
	{
		if(_isUnitTest)
		{
			DatabaseUtils.prepareUnitTest(
					PropertyLoader.getProperty("com.pdi.data.v3.application", "database.url"), 
					PropertyLoader.getProperty("com.pdi.data.v3.application", "database.username"), 
					PropertyLoader.getProperty("com.pdi.data.v3.application", "database.password"),
					"");
		}
	}
	
	
	/**
	 * Tests a connection to the V3 database
	 * @param the environment (development, preproduction, production)
	 * @return a boolean
	 * @throws Exception
	 */
	public static boolean testConnection(String environment) 
		{
		V3DatabaseUtils.determineUnitTest();
		//build sql
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT NOW1() AS dbTime FROM DUAL");
		DataLayerPreparedStatement dlps = V3DatabaseUtils.prepareStatement(sql);
		if (dlps.isInError())
		{
			return false;
		}

		DataResult dr = null;
		try
		{
			//get result set
			dr = select(dlps);
			if (dr.isInError())
			{
				return false;
			}
			
			//do normal resultset stuff
			dr.getResultSet().next();
			
			//get value from result set
			String time = dr.getResultSet().getString("dbTime");
			
			//do stuff with value
			System.out.println("*** Testing Connection for V3 database and the result is " + time);
			
			//close connection
			dr.close();
		}
		catch (SQLException ex)
		{
			return false;
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}

		return true;
	}


	/**
	 * Get a DB connection for use by a call
	 * @param the environment (development, preproduction, production)
	 * @param the SQL to execute
	 * @return a DataLayerPreparedStatement object
	 */
	public static DataLayerPreparedStatement prepareStatement(StringBuffer sql)
	{
		V3DatabaseUtils.determineUnitTest();	
		return DatabaseUtils.prepareStatement(determineEnvironment(), sql);
	}

	
	/**
	 * Get a DB connection for use by a call
	 * @param the environment (development, preproduction, production)
	 * @param the SQL to execute
	 * @param an auto-generate constant
	 * @return a DataLayerPreparedStatement object
	 */
	public static DataLayerPreparedStatement prepareStatement(StringBuffer sql, int autogen)
	{
		V3DatabaseUtils.determineUnitTest();	
		return DatabaseUtils.prepareStatement(determineEnvironment(), sql, autogen);
	}

	
// NOTE that the following functions are intermediate functions used within the data layer.
//	    The actual functionality performed in the helpers may be different from the function
//		called here; e.g., a delete in a helper may actually call an update function to set
//		the active flag off.  
	
	/**
	 * The insert interface function.  Note that there is no checking that the
	 * prepared statement is an insert statement.  If the user calls this with
	 * some other functionality, data will be lost at a minimum and the
	 * (properly formated) statement could error out.
	 *
	 * @param A DataLayerPreparedStatement with the PreparedStatement to execute
	 * @return a DataResult object
	 */
	public static DataResult insert(DataLayerPreparedStatement dlps)
	{
		V3DatabaseUtils.determineUnitTest();
		return statusTransform(DatabaseUtils.execute(determineEnvironment(),
							   dlps,
							   DatabaseUtils.DBA_INSERT));
	}

	
	/**
	 * The select interface function.  Note that there is no checking that the
	 * prepared statement is a select statement.  If the user calls this with
	 * some other functionality, data will be lost at a minimum and the
	 * (properly formated) statement could error out.
	 *
	 * @param A DataLayerPreparedStatement with the PreparedStatement to execute
	 * @return a DataResult object
	 */
	public static DataResult select(DataLayerPreparedStatement dlps)
	{
		V3DatabaseUtils.determineUnitTest();
		return statusTransform(DatabaseUtils.execute(determineEnvironment(),
							   dlps,
							   DatabaseUtils.DBA_SELECT));
	}

	
	/**
	 * The update interface function.  Note that there is no checking that the
	 * prepared statement is an update statement.  If the user calls this with
	 * some other functionality, data will be lost at a minimum and the
	 * (properly formated) statement could error out.
	 *
	 * @param A DataLayerPreparedStatement with the PreparedStatement to execute
	 * @return a DataResult object
	 */
	public static DataResult update(DataLayerPreparedStatement dlps)
	{
		V3DatabaseUtils.determineUnitTest();
		return statusTransform(DatabaseUtils.execute(determineEnvironment(),
							   dlps,
							   DatabaseUtils.DBA_UPDATE));
	}

	
	/**
	 * The delete interface function.  Note that there is no checking that the
	 * prepared statement is a delete statement.  If the user calls this with
	 * some other functionality, data will be lost at a minimum and the
	 * (properly formated) statement could error out.
	 *
	 * @param A DataLayerPreparedStatement with the PreparedStatement to execute
	 * @return a DataResult object
	 */
	public static DataResult delete(DataLayerPreparedStatement dlps)
	{
		V3DatabaseUtils.determineUnitTest();
		return statusTransform(DatabaseUtils.execute(determineEnvironment(),
							   dlps,
							   DatabaseUtils.DBA_DELETE));
	}

	
	/**
	 * A private method to transform certain db error conditions
	 * to possible "soft fail" status codes.
	 *
	 * @param the DataResult object to transform
	 * @return the transformed DataResult object
	 */
	private static DataResult statusTransform(DataResult inp)
	{
		// Transform errors that may be "soft" errors to status codes here.
		// Currently there is only one status that is transformed, but
		// others could be added as needed.
		DataResult ret = inp;
		if (inp.isInError())
		{
			// Transform the error code as required
			// TODO Refactor so that the update does not depend upon the error code (do a query)
			// Duplicate key error triggers update; error code = 1062 on MySQL, 2627 on SQL Server
			if (inp.getStatus().getExceptionCode() == 2627)		// MySQL error code for dup keys
			{
				ret.getStatus().setStatusCode(RequestStatus.DBS_DUP_KEY);
			}
		}

		return ret;
	}
	//
	// Instance data.
	//


	//
	// Constructors.
	//


	//
	// Instance methods.
	//

}
