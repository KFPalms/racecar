package com.pdi.data.v2.helpers.delegated.ut;

//import java.util.ArrayList;
//import java.util.Iterator;
//import java.util.Map;

import com.pdi.data.dto.ProjectParticipantInstrument;
import com.pdi.data.dto.Score;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.IProjectParticipantInstrumentHelper;
import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.properties.PropertyLoader;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

public class ProjectParticipantInstrumentHelperUT extends TestCase
{
	public ProjectParticipantInstrumentHelperUT(String name)
	{
		super(name);
	}	

	
	private SessionUser _session = null;


	private void setUpSession()
	{
		if(_session == null)
		{
			this._session = new SessionUser();
			//_session.getMetadata().put("USERNAME", "cdunn");
			//_session.getMetadata().put("PASSWORD", "cdunn");
			//dev
			_session.getMetadata().put("USERNAME", "mbpanichi");
			_session.getMetadata().put("PASSWORD", "griffin12");
		}
		
		return;
	}
	
	
//	public void testAddUpdate()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		
//		System.out.println("Unable to test addUpdate() here.  Use test in PortalDelegatedServiceUT");
//		
////		//Get a PPI from NHN
////		//Score the PPI
////		
////		//Phase 1: Take Participant Data from the database (response data)
////		IProjectParticipantInstrumentHelper ippi = HelperDelegate.getProjectParticipantInstrumentHelper("com.pdi.data.nhn.helpers.delegated");
////		String engagementId = "2";         
////		String participantId = "553269";
////		String participantId = "51081";
////		String instrumentId = "rv";
////		int mode = 1;
////		ProjectParticipantInstrument ppi = ippi.fromProjectIdParticipantIdInstrumentId(null, mode, engagementId, participantId, instrumentId);
////		
////		//Phase 2: SCORE the response data
////		IScoringHelper iscoring = HelperDelegate.getScoringHelper();
////		ScoreGroup sg = iscoring.score(ppi.getInstrument(), ppi.getResponseGroup());
////		
////		for(Score s : sg.getScores().values()) {
////			System.out.println(s.getCode() + " = " + s.getRawScore());
////		}
////		
////		//Phase 3: Save scored data to the V2 database
////		UnitTestUtils.start(this);
////		V2DatabaseUtils.setUnitTest(true);
//
//		UnitTestUtils.stop(this);
//	}

	
//	public void testFromLastParticipantId() throws Exception
//	{
//		UnitTestUtils.start(this);
//		
//		System.out.println("FromLastParticipantId() no longer operative");
//
////		V2DatabaseUtils.setUnitTest(true);
////		setUpSession();
////		String participantId = "XXXXXXX2";
////		
////		IProjectParticipantInstrumentHelper ppiHelper = HelperDelegate.getProjectParticipantInstrumentHelper();
////		ArrayList<ProjectParticipantInstrument> ppiList = ppiHelper.fromLastParticipantId(_session, 1, participantId);
////		System.out.println(" ppiList.size() : " + ppiList.size());
////		
////		for(ProjectParticipantInstrument ppi : ppiList){
////			
////			System.out.println("instrumentId: " + ppi.getInstrument().getCode());
////			System.out.println("instrumentTakenDate: " + ppi.getInstrumentTakenDate());
////			
////			System.out.println("genPopNorm:  " + ppi.getScoreGroup().getGeneralPopulation().getPopulation());
////			System.out.println("specPopNorm: " + ppi.getScoreGroup().getSpecialPopulation().getPopulation());
////			HashMap<String, Score> scores = ppi.getScoreGroup().getScores();
////			for(String str : scores.keySet()){
////				
////				System.out.println("      key, Score " + str + " : " + scores.get(str).getRawScore());
////				
////			}
////			System.out.println("-------------------- ");
////		}
//		
//		UnitTestUtils.stop(this);
//	}	


	public void testFromProjectIdParticipantIdInstrumentId()
		throws Exception
	{
		UnitTestUtils.start(this);
		V2DatabaseUtils.setUnitTest(true);

		setUpSession();	// May be commented out above

		////// Old stuff
		////String engagementId = "HOCAQLAH";	// AbyD Template Testing Components_SAVE AS & RENAME 
		////String participantId = "FAMWFZPM";	// One of the MB test candidates
		////String instrumentId = "IXLGVIOQ";	// GPI
		// New stuff
//		String projectId = "1";	// AbyD Template Testing Components_SAVE AS & RENAME 
//		String participantId = "60232";	// One of the MB test candidates
//		String instrumentCode = "gpil";	// GPI
//		// Bring back a WG_E (Should be a single score... the CT
//		projectId = "FBODOOSY";
//		participantId = "BLMXLQVJ";
//		instrumentCode = "wg";	// GPI
		String projectId = "60";	// AbyD Template Testing Components_SAVE AS & RENAME 
		String participantId = "364005";	// One of the MB test candidates
		String instrumentCode = "gpi";	// GPI

		IProjectParticipantInstrumentHelper ppiHI = HelperDelegate.getProjectParticipantInstrumentHelper(PropertyLoader.getProperty("com.pdi.data.v2.application","datasource.v2"));
		
		ProjectParticipantInstrument ppi = ppiHI.fromProjectIdParticipantIdInstrumentId(_session, IProjectParticipantInstrumentHelper.MODE_DEFAULT, projectId, participantId, instrumentCode, null);
		
		//ScoreGroup.calculateNormScores(scores, normGroup);
		//ppi.getScoreGroup().calculateGeneralPopulation().get("GPI_EGO").toRating();
		
		System.out.println(ppi.toString());
		System.out.println("Scores:");
		for (Score s : ppi.getScoreGroup().getScores().values())
		{
			System.out.println("  " + s.toString());
		}
		
		//ppi.getScoreGroup().getScores();
		//ppi.getScoreGroup().calculateGeneralPopulation();
		//ScoreGroup.calculateNormScores(scores, normGroup);
		
		// Now try one with no project id
		System.out.println("-----------------:");
		ppi = ppiHI.fromProjectIdParticipantIdInstrumentId(_session, IProjectParticipantInstrumentHelper.MODE_DEFAULT, null, participantId, instrumentCode, null);
		System.out.println(ppi.toString());
		System.out.println("Scores:");
		for (Score s : ppi.getScoreGroup().getScores().values())
		{
			System.out.println("  " + s.toString());
		}

		UnitTestUtils.stop(this);
	}


//	public void testFromParticipantIdInstrumentId()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//
//		System.out.println("ppiHI.fromParticipantIdInstrumentId() no longer operative");
////		setUpSession();	// May be commented out above
////
////		//// Cert combo
////		//String participantId = "CPHIPWGI";	// One of the MB test candidates
////		////String instrumentId = "IXLGVIOQ";	// GPI
////		//String instrumentId = "LIBPMLBJ";	// CHQ
////		
////		String participantId = "60232";	// One of the MB test candidates - 4 instruments
////		String instrumentId = "gpil";	// GPI
////
////
////		IProjectParticipantInstrumentHelper ppiHI = HelperDelegate.getProjectParticipantInstrumentHelper(PropertyLoader.getProperty("com.pdi.data.v2.application","datasource.v2"));
////		ArrayList<ProjectParticipantInstrument> ppiList = ppiHI.fromParticipantIdInstrumentId(_session, participantId, instrumentId);
////		System.out.println("PartInst returned " + ppiList.size() + " entries:");
////		for (ProjectParticipantInstrument ppi : ppiList)
////		{
////			System.out.println(ppi.toString() + "\n");
////		}
//
//		UnitTestUtils.stop(this);
//	}


//	public void testFromProjectIdParticipantId()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		V2DatabaseUtils.setUnitTest(true);
//
//		setUpSession();	// May be commented out above
//
//		////// Cert combo - Old data
//		////String projectId = "IYKNYCNU";	// AbyD Template Testing Components_SAVE AS & RENAME 
//		////String participantId = "CPHIPWGI";	// One of the MB test candidates
//		// Cert combo - New data
//		String projectId = "1";	// 
//		//String participantId = "59639";	// One of the MB test candidates - 3 instruments
//		String participantId = "60232";	// One of the MB test candidates - 4 instruments
//
//		IProjectParticipantInstrumentHelper ppiHI = HelperDelegate.getProjectParticipantInstrumentHelper(PropertyLoader.getProperty("com.pdi.data.v2.application","datasource.v2"));
//		ArrayList<ProjectParticipantInstrument> ppiList = ppiHI.fromProjectIdParticipantId(_session, IProjectParticipantInstrumentHelper.MODE_DEFAULT, projectId, participantId);
//		System.out.println("EngInst returned " + ppiList.size() + " entries:");
//		for (ProjectParticipantInstrument ppi : ppiList)
//		{
//			System.out.println(ppi.toString() + "\n");
//		}
//
//		UnitTestUtils.stop(this);
//	}


//	public void testFromParticipantId()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		V2DatabaseUtils.setUnitTest(true);
//
//		setUpSession();	// May be commented out above
//
//		////// Cert - old (V2) data
//		////String participantId = "CPHIPWGI";	// One of the MB test candidates
//		// Cert - new (mixed platform) data
//		//String participantId = "60232";	// One of the MB test candidates - 6 entries
//		String participantId = "59639";	// One of the MB test candidates - 3 instruments
//		
//		IProjectParticipantInstrumentHelper ppiHI = HelperDelegate.getProjectParticipantInstrumentHelper(PropertyLoader.getProperty("com.pdi.data.v2.application","datasource.v2"));
//		ArrayList<ProjectParticipantInstrument> ppiList = ppiHI.fromLastParticipantId(_session, IProjectParticipantInstrumentHelper.MODE_DEFAULT, participantId);
//
//		for (ProjectParticipantInstrument ppi : ppiList)
//		{
//			System.out.println(ppi.toString() + "\n");
//		}
//
//		UnitTestUtils.stop(this);
//	}

}
