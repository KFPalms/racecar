package com.pdi.data.v2.helpers.delegated.ut;


import java.util.ArrayList;

import com.pdi.data.helpers.interfaces.IProjectHelper;

import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.properties.PropertyLoader;
import com.pdi.ut.UnitTestUtils;
import com.pdi.data.dto.Project;
import com.pdi.data.dto.SessionUser;


import junit.framework.TestCase;

public class ProjectHelperUT extends TestCase
{
	private SessionUser _session = null;
	
	public ProjectHelperUT(String name)
	{
		super(name);
	}
	
	private void setUpSession()
	{
		if(_session == null)
		{
			this._session = new SessionUser();
			//_session.getMetadata().put("USERNAME", "cdunn");
			//_session.getMetadata().put("PASSWORD", "cdunn");
			//dev
			_session.getMetadata().put("USERNAME", "mbpanichi");
			_session.getMetadata().put("PASSWORD", "griffin12");
		}
		
		return;
	}
	
	
	
		// v2	
		//** NOTE ** 
		//In V2, the engagementId is the jobId.  
		//Passing in an engagement Id is the same as passing in a project/job Id
	
	
//	public void testFromId()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//
//		setUpSession();
//		////IProjectHelper iProject = HelperDelegate.getProjectHelper(PropertyLoader.getProperty("com.pdi.data.application","datasource.v2"));
//		IProjectHelper iProject = HelperDelegate.getProjectHelper(PropertyLoader.getProperty("com.pdi.data.v2.application","datasource.v2"));
//		
//		//String projectId = "HOCAQLAH";    // generic tpot
//		//String projectId = "HSYKSUUY"; 	 // base consultant
//		String projectId = "IYKNYCNU"; 	 // base testing
//		Project project = iProject.fromId(_session, projectId);
//		if (project != null)
//		{
//			System.out.println(project.toString());
//		}
//		else
//		{
//			System.out.println("fromId() commented out.... returns null");
//		}
//
//		UnitTestUtils.stop(this);
//	}
		
	
//	public void testFromClientId()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//
//		setUpSession();
//		
//		////IProjectHelper iProject = HelperDelegate.getProjectHelper(PropertyLoader.getProperty("com.pdi.data.application","datasource.v2"));
//		IProjectHelper iProject = HelperDelegate.getProjectHelper(PropertyLoader.getProperty("com.pdi.data.v2.application","datasource.v2"));
//
//		String clientId = "IYBWJXCM";  //  generic tpot client 
//		ArrayList<Project> projects = iProject.fromClientId(_session, clientId);
//		if (projects != null)
//		{
//			System.out.println("Projects for client ID " + clientId + " (" + projects.size() + "):");
//			for(Project project : projects)
//			{
//				System.out.println(project.toString());
//			}
//		}
//		else
//		{
//			System.out.println("fromClientId() commented out.... returns null");
//		}
//
//		UnitTestUtils.stop(this);
//	}		

	
	public void testFromParticipantId()
		throws Exception
	{
		UnitTestUtils.start(this);
		V2DatabaseUtils.setUnitTest(true);

		setUpSession();
		
		////IProjectHelper iProject = HelperDelegate.getProjectHelper(PropertyLoader.getProperty("com.pdi.data.application","datasource.v2"));
		IProjectHelper iProject = HelperDelegate.getProjectHelper(PropertyLoader.getProperty("com.pdi.data.v2.application","datasource.v2"));

		//String participantId = "KEYVOFZF";	//  Joe Blough (dev)
		//String participantId = "ITHXPFWA";	// Cert - 2 AbyD
		//String participantId = "BKVMUKQV";	// Cert - 1 TLT
		String participantId = "KEOEPSOT";		// Cert - 2 AbyD
		ArrayList<Project> projects = iProject.fromParticipantId(_session, participantId);
		if (projects != null)
		{
			System.out.println("Projects (" + projects.size() + "):");
			for(Project myProject : projects)
			{
				System.out.println(myProject.toString());
			}
		}
		else
		{
			System.out.println("fromParticipantId() commented out.... returns null");
		}

		UnitTestUtils.stop(this);
	}
}
