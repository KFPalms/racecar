package com.pdi.data.abyd.helpers.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.pdi.data.abyd.dto.common.PrnData;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.data.nhn.util.NhnDatabaseUtils;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.util.RequestStatus;
import com.pdi.scoring.Norm;

public class HelperUtils
{
	/**
	 * alpCheck - sees if there is a KFALP/kfp/ALP instrument associated with this project
	 * @param dnaId
	 * @return
	 * @throws Exception
	 */
	public static boolean alpCheck(long dnaId)
		throws Exception
	{
	// See if we have an ALP
		Statement stmt = null;
		ResultSet rs = null;
		boolean ret = false;
		Connection con = null;
	
		StringBuffer sqlQuery = new StringBuffer(); 
		sqlQuery.append("SELECT COUNT(*) as cnt ");
		sqlQuery.append("  FROM pdi_abd_dna_link ll ");
		sqlQuery.append("    LEFT JOIN pdi_abd_module mm ON mm.moduleId = ll.moduleId ");
		sqlQuery.append("  WHERE dnaId = " + dnaId + " ");
		sqlQuery.append("    AND ll.moduleId IN (SELECT moduleId ");
		sqlQuery.append("                          FROM pdi_abd_module ");
		sqlQuery.append("                          WHERE internalName IN ('ALP')) ");

		try
		{
			con = AbyDDatabaseUtils.getDBConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());
			
			// count always returns a row
			rs.next();
			if (rs.getInt("cnt") > 0)
			{
				ret = true;
			}
			return ret;
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL error in alpCheck (dnaId=" + dnaId + ").  msg=" + ex.getMessage());
		}
		finally
		{
			if (rs != null)
			{
				try {  rs.close();  }
				catch (SQLException sqlEx) {    /*  Swallow the error */ }
				rs = null;
			}
			if (stmt != null)
			{
				try {  stmt.close();  }
				catch (SQLException sqlEx) {  /*  Swallow the error */  }
				stmt = null;
			}
			if (con != null)
			{
				try {  con.close();  }
				catch (SQLException sqlEx) {  /*  Swallow the error */  }
				stmt = null;
			}
		}
	}


	// TODO Refactor so that these checks aren't dupes with a single line change
	/**
	 * kfarCheck - sees if there is a KF4D/fkar instrument associated with this project
	 * @param dnaId
	 * @return
	 * @throws Exception
	 */
	public static boolean kfarCheck(long dnaId)
		throws Exception
	{
		// See if we have a KF4D
		Statement stmt = null;
		ResultSet rs = null;
		boolean ret = false;
		Connection con = null;
	
		StringBuffer sqlQuery = new StringBuffer(); 
		sqlQuery.append("SELECT COUNT(*) as cnt ");
		sqlQuery.append("  FROM pdi_abd_dna_link ll ");
		sqlQuery.append("    LEFT JOIN pdi_abd_module mm ON mm.moduleId = ll.moduleId ");
		sqlQuery.append("  WHERE dnaId = " + dnaId + " ");
		sqlQuery.append("    AND ll.moduleId IN (SELECT moduleId ");
		sqlQuery.append("                          FROM pdi_abd_module ");
		sqlQuery.append("                          WHERE internalName IN ('KF4D')) ");

		try
		{
			con = AbyDDatabaseUtils.getDBConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());
			
			// count always returns a row
			rs.next();
			if (rs.getInt("cnt") > 0)
			{
				ret = true;
			}
			return ret;
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL error in alpCheck (dnaId=" + dnaId + ").  msg=" + ex.getMessage());
		}
		finally
		{
			if (rs != null)
			{
				try {  rs.close();  }
				catch (SQLException sqlEx) {    /*  Swallow the error */ }
				rs = null;
			}
			if (stmt != null)
			{
				try {  stmt.close();  }
				catch (SQLException sqlEx) {  /*  Swallow the error */  }
				stmt = null;
			}
			if (con != null)
			{
				try {  con.close();  }
				catch (SQLException sqlEx) {  /*  Swallow the error */  }
				stmt = null;
			}
		}
	}


	/**
	 * gpiCogsCheck - sees if there GPI or Cog instruments associated with this project
	 * @param dnaId
	 * @return
	 * @throws Exception
	 */
	public static boolean gpiCogsCheck(long dnaId)
		throws Exception
	{
	// See if we have a GPI or Cogs
		Statement stmt = null;
		ResultSet rs = null;
		boolean ret = false;
		Connection con = null;
	
		StringBuffer sqlQuery = new StringBuffer(); 
		sqlQuery.append("SELECT COUNT(*) as cnt ");
		sqlQuery.append("  FROM pdi_abd_dna_link ll ");
		sqlQuery.append("    LEFT JOIN pdi_abd_module mm ON mm.moduleId = ll.moduleId ");
		sqlQuery.append("  WHERE dnaId = " + dnaId + " ");
		sqlQuery.append("    AND ll.moduleId IN (SELECT moduleId ");
		sqlQuery.append("                          FROM pdi_abd_module ");
		sqlQuery.append("                          WHERE internalName IN ('GPI', 'Cogs')) ");

		try
		{
			con = AbyDDatabaseUtils.getDBConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());
			
			// count always returns a row
			rs.next();
			if (rs.getInt("cnt") > 0)
			{
				ret = true;
			}
			return ret;
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL error in gpiCogsCheck (dnaId=" + dnaId + ").  msg=" + ex.getMessage());
		}
		finally
		{
			if (rs != null)
			{
				try {  rs.close();  }
				catch (SQLException sqlEx) {    /*  Swallow the error */ }
				rs = null;
			}
			if (stmt != null)
			{
				try {  stmt.close();  }
				catch (SQLException sqlEx) {  /*  Swallow the error */  }
				stmt = null;
			}
			if (con != null)
			{
				try {  con.close();  }
				catch (SQLException sqlEx) {  /*  Swallow the error */  }
				stmt = null;
			}
		}
	}


	/**
	 * getProjIdFromDna
	 * @param dnaId
	 * @return
	 * @throws Exception
	 */
	public static synchronized long getProjIdFromDna(long dnaId)
		throws Exception
	{
		long ret = 0;

		Statement stmt = null;
		ResultSet rs = null;
		
		if(dnaId == 0)
		{
			throw new Exception("Invalid dna ID parameter for HelperUtils.getProjIdFromDna()");
		}

		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("SELECT projectId ");
		sqlQuery.append("  FROM pdi_abd_dna ");
		sqlQuery.append("  WHERE dnaId = " + dnaId);

		Connection con = null;
		try
		{
			con = AbyDDatabaseUtils.getDBConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			if (! rs.isBeforeFirst())
			{
				String err = "HelperUtils.getProjIdFromDna() - No project ID available for dnaID=" + dnaId;
				System.out.println(err);
				throw new Exception(err);
			}

			rs.next();
			ret = rs.getLong("projectId");

			return ret;
		}
		catch (SQLException ex)
		{
			// handle any errors
			String err = "HelperUtils.getProjIdFromDna() - SQL in getProjIdFromDna().  dnaId=" + dnaId + ".  " +
							"SQLException: " + ex.getMessage() + ", " +
							"SQLState: " + ex.getSQLState() + ", " +
							"VendorError: " + ex.getErrorCode();
			System.out.println(err);
			throw new Exception(err);
		}
		finally
		{
			if (rs != null)
			{
			    try {  rs.close();  }
			    catch (SQLException sqlEx)
			    {
			    	System.out.println("ERROR: rs close in getProjIdFromDna - " + sqlEx.getMessage());
			    }
				rs = null;
			}
			if (stmt != null)
			{
			    try {  stmt.close();  }
			    catch (SQLException sqlEx)
			    {
			    	System.out.println("ERROR: stmt close in getProjIdFromDna - " + sqlEx.getMessage());
			    }
			    stmt = null;
			}
			if (con != null)
			{
			    try {  con.close();  }
			    catch (SQLException sqlEx)
			    {
			    	System.out.println("ERROR: con close in getProjIdFromDna - " + sqlEx.getMessage());
			    }
			    con = null;
			}
		}
	}
	
	public static boolean courseComplete(String partId, String courseAbbv) throws Exception {
		boolean ret = false;

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT  'x' AS ecks ");
		sb.append("  FROM platform.dbo.position pp WITH(NOLOCK)");
		sb.append("  WHERE pp.users_id = " + partId + " ");
		sb.append("    AND pp.course = (SELECT course FROM platform.dbo.course WITH(NOLOCK) WHERE abbv = '" + courseAbbv + "') ");
		sb.append("    AND pp.completion_status = 2");

		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(sb);
		if (dlps.isInError()) {
			String err = "Error preparing rv2Complete() query." + dlps.toString();
			System.out.println(err);
			throw new Exception(err);
		}
		DataResult dr = null;
		try {
			dr = NhnDatabaseUtils.select(dlps);
			int stat = dr.getStatus().getStatusCode();
			if (stat == RequestStatus.RS_ERROR) {
				String err = "Error executing select in rv2Complete(). " + dlps.toString();
				System.out.println(err);
				throw new Exception(err);
			} else {
				if (stat == RequestStatus.DBS_NO_DATA) {
					// No data to find... we are done here... default is false
				} else {
					ret = true;
				}
			}
		} finally {
			if (dr != null) {
				dr.close();
				dr = null;
			}
		}

		return ret;
	}
	
	public static PrnData getRv2Rating(String partId, long projId) throws Exception {
		int ravScore = 0;
		Rv2Helper rv2Helper = new Rv2Helper();
		try {
			//ravScore = getRv2RawScore(partId, projId);
			
			ravScore = rv2Helper.getRv2RawScore(partId, projId);
			//log.debug("Got RV2 Raw Score: {}", ravScore);
			//this.setRv2Rawscore(ravScore);
		} catch (Exception e) {
			// Pass it on along
			throw new Exception(e);
		}
		if (ravScore == -1) {
			return null;
		}

		// Norm it

		return Rv2Helper.doRavens2Norming(ravScore);
	}
	

}
