/**
 * Copyright (c) 2008 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.dto.common;


/**
 * DNACellDTO is a container for information about a module/competency intersection.
 * Used in setup and Integration Grids.
 * 
 * @author		Ken Beukelman
  */
public class DNACellDTO
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private long _moduleId;	  	// The internal id of the module
	private long _competencyId;	// The internal id of the competency
	private boolean _isUsed;	// Flag indicating whether the intersection is on or off - Used in setup
	private String _score;		// Intersection score value - used in Integration Grid
	

	//
	// Constructors.
	//
	public DNACellDTO()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//

	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//
			
	/*****************************************************************************************/
	public long getModuleId()
	{
		return _moduleId;
	}

	public void setModuleId(long value)
	{
		_moduleId = value;
	}

	/*****************************************************************************************/
	public long getCompetencyId()
	{
		return _competencyId;
	}

	public void setCompetencyId(long value)
	{
		_competencyId = value;
	}	
	
	/*****************************************************************************************/
	public boolean getIsUsed()
	{
		return _isUsed;
	}

	public void setIsUsed(boolean value)
	{
		_isUsed = value;
	}
	
	/*****************************************************************************************/
	public String getScore()
	{
		return _score;
	}

	public void setScore(String value)
	{
		_score = value;
	}



	/*
	 * Show me what you've got
	 */
	public String toString()
	{
		String ret = "DNACellDTO content (Java)\n";
		ret += "           Module ID=" + _moduleId + "\n";
		ret += "       Competency ID=" + _competencyId + "\n";
		ret += "   Intersection Used=" + _isUsed + "\n";
		ret += "  Intersection Score=" + _score + "\n";
		
		return ret;
	}
}
