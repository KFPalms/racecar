/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 *
 * $Header: /Platform/Java/sourceCode/com/pdicorp/app/tcm/export/vo/ExportDataVO.java 6     8/25/05 3:23p Kbeukelm $
 */
package com.pdi.data.abyd.dto.evalGuide;

import java.util.ArrayList;

import com.pdi.data.abyd.dto.common.StatusDTO;

/**
* EGUrlDTO is a thin class containing a list of generated Eval Guide URLs for a participant
* 
* @author		Ken Beukelman
*/
public class EGUrlDTO extends StatusDTO
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _formattedCandName;			// The full participant name ("Last, First")
	private String _clientName;					// The name of the client
	private String _dnaName;					// The name of the DNA/Project
	private ArrayList<EGUrlData> _urlDataArray;	// List of generated URLs
	private String _candidateId;				// Candidate Id
	private String _projectName;				// Project name
	
	
	//
	// Constructors.
	//
	public EGUrlDTO()
	{
		// Does nothing currently
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public String getFormattedCandName()
	{
		return this._formattedCandName;
	}
		
	public void setFormattedCandName(String value)
	{
		this._formattedCandName = value;
	}

	/*****************************************************************************************/
	public String getClientName()
	{
		return this._clientName;
	}
		
	public void setClientName(String value)
	{
		this._clientName = value;
	}

	/*****************************************************************************************/
	public String getDnaName()
	{
		return this._dnaName;
	}
		
	public void setDnaName(String value)
	{
		this._dnaName = value;
	}

	/*****************************************************************************************/
	public ArrayList<EGUrlData>  getUrlDataArray()
	{
		if (this._urlDataArray == null)
			this._urlDataArray = new ArrayList<EGUrlData>();
		return this._urlDataArray;
	}
		
	public void setUrlDataArray(ArrayList<EGUrlData>  value)
	{
		this._urlDataArray = value;
	}

	/*****************************************************************************************/
	public String getCandidateId()
	{
		return this._candidateId;
	}
		
	public void setCandidateId(String value)
	{
		this._candidateId = value;
	}
	
	/*****************************************************************************************/
	public String getProjectName()
	{
		return this._projectName;
	}
		
	public void setProjectName(String value)
	{
		this._projectName = value;
	}
	
	/*
	 * Show me what you've got
	 */
	public String toString()
	{
		String ret = "EGUrlDTO content (Java)\n";
		ret += "  Part.  Name=" + _formattedCandName + "\n";
		ret += "  Client Name=" + _clientName + "\n";
		ret += " Project Name=" + _projectName + "\n";
		ret += "     DNA Name=" + _dnaName + "\n";
		if(_urlDataArray == null)
		{
			ret += "No URLs available";
		}
		else
		{
			for (EGUrlData url : _urlDataArray)
			{
				ret += "  Genned URL info:\n";
				ret += url.toString();
			}
		}
		
		return ret;
	}
}
