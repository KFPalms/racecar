/**
 * Copyright (c) 2009, 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v2.helpers;

/**
 * SaveConstructDataHelper contains all the code needed to save the construct score data
 * from the Manual Entry Application.
 *
 * @author		MB Panichi 
 * @author		Ken Beukelman (original code)
 * @version	$Revision: 6 $  $Date: 8/25/05 3:23p $
 */


import java.io.StringReader;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TimeZone;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.util.RequestStatus;
import com.pdi.data.v2.constants.ManualEntryConstants;
import com.pdi.data.v2.dto.*;

import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.scoring.Norm;
import com.pdi.xml.XMLUtils;
import com.pdi.data.v2.util.NormUtils;
import com.pdi.data.v2.util.Utils;
import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.data.v2.util.V2WebserviceUtils;

/*
 * SaveConstructDataHelper contains all the code needed to save the construct score data
 */
public class SaveConstructDataHelper {
	//
	//  Static data.
	//
	private static final String XML_HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	private static final String TAG_XML_FIELDS_OPEN = "<XML_Fields>";
	private static final String TAG_XML_FIELDS_CLOSE = "</XML_Fields>";
	private static final String TAG_CSCORE_OPEN = "<CScore.S0>";
	private static final String TAG_CSCORE_CLOSE = "</CScore.S0>";
	private static final String TAG_CREATION_ATTMPT_OPEN = "<CreationAttempt>";
	private static final String TAG_CREATION_ATTMPT_CLOSE = "</CreationAttempt>";
	private static final String TAG_ANSWERS_OPEN = "<Answers>";
	private static final String TAG_ANSWERS_CLOSE = "</Answers>";
	private static final String TAG_A_OPEN_1 = "<A N = \"";
	private static final String TAG_A_OPEN_2 = "\">";
	private static final String TAG_A_CLOSE = "</A>";
	private static final String TAG_START_DATE_OPEN = "<StartDates>";
	private static final String TAG_START_DATE_CLOSE = "</StartDates>";
	private static final String TAG_OBJ_RESTORE_OPEN = "<ObjectRestore>";	
	private static final String TAG_OBJ_RESTORE_CLOSE = "</ObjectRestore>";	
	private static final String TAG_NORM_OPEN = "<Norms>";
	private static final String TAG_NORM_CLOSE = "</Norms>";
	private static final String TAG_OPEN_START = "<";	
	private static final String TAG_CLOSE_START ="</";
	private static final String TAG_END = ">";
	
	private static final String CPP_VENDOR_MODULE_ID = "ISXQYKIH";
	private static final String CPP_INSTRUMENT_MODULE_ID = "HSJRIKLO";
	private  final String COMPLETED_VALUE = "1";
	
	private static final String VENDOR_MODULES_STRING = "('DTYAQNYJ','IZFTQXVI')";
	private static final String PAUSE_MESSAGE_MODULE = "FAEVOVQQ";
////	private static final String COGS_DONE_MODULE = "KFVDJXHR";
	private static final int NUMBER_OF_VENDOR_MODULES = 2;

	//
	// Instance data.
	//

	// instantiate a return object
	private ManualEntryDTO lpDTO = new ManualEntryDTO();

	// database variables
	private HashMap<String, String> resultsXML = new HashMap<String, String>();
	private String answersXML = "";
	private String createdByStamp = "ManEntryApp";  // varchar(15)
	private String dateCreatedStamp = "";
	private String uniqueIdStamp = "";
	
////	// if there are answers present for vendor integration
////	private String answerUniqueId = "";
	
	//
	// Constructors.
	//
	public SaveConstructDataHelper()
		throws Exception
	{

	}

	//
	//  Instance methods.
	//

	/*
	 * Convenience method
	 */
	public Boolean trackInstrumentStatus(SaveConstructDataHelper saveConstructDataHelper,
										  String projectId,
										  String participantId,
										  String instrumentId,
										  String instrumentStatus)
			throws Exception
	{
		return saveConstructDataHelper.trackInstrumentStatus(projectId,
				participantId, instrumentId, instrumentStatus);
	}
	

	/**
	 * setUpSaveData is the controlling method to write
	 * answer/results data back to the V2 database.  It calls
	 * the other methods to do the actual work.
	 * 
	 * @param ManualEntryDTO dtoIn,  the data being passed 
	 * 			from the Submit button in the Manual Entry App
	 * @return 
	 * @throws Exception
	 */
	public void setUpSaveData(ManualEntryDTO dtoIn)
		throws Exception
	{        
		lpDTO = dtoIn;
		
		//System.out.println("lpDTO.getModConstDTOList().get(0).getHasScores()  " + lpDTO.getModConstDTOList().get(0).getHasScores());
		
		if(dtoIn.getManualEntryType().equals(ManualEntryConstants.ME_A_BY_D_TYPE)){
			buildResultsXMLforAbyD();
		} else {
			buildResultsXML();
		}		
		buildAnswersXML();
		
		writeResultsDataToDatabase();
		
		writeAnswerDataToDatabase();
		
		/*
			If this is the cpp module, then 
			we need to set the vendor integration pass-through
			module answers to complete as well.
		*/		
		if(lpDTO.getModConstDTOList().get(0).getModuleId().equals(CPP_INSTRUMENT_MODULE_ID))
		{
			insertAnswerDataforVendorPassThroughModule(CPP_VENDOR_MODULE_ID);			
		}
		lpDTO.getModConstDTOList().get(0).setHasScores(true);
		
		/*
			If all the Vendor modules have been set to completed
			in the ManualEntry app 
			for this participant/project, we need to also shut off
			the Pause Message Module  (FAEVOVQQ)
		*/		
		
		Boolean vendorModsComplete = checkForCompletedVendorModules(lpDTO.getProjectId(), lpDTO.getCandidateId(), VENDOR_MODULES_STRING);
		if(vendorModsComplete)
		{
			// see if the pause module is turned off... 
			Boolean doIt = checkForCompletedVendorModules(lpDTO.getProjectId(), lpDTO.getCandidateId(), "('"+PAUSE_MESSAGE_MODULE+"')");
			
			// if it's not turned off, turn off the pause module
			if(!doIt){
				insertAnswerDataforVendorPassThroughModule(PAUSE_MESSAGE_MODULE);
			}
		}
	}  	 

	
	/**
	 * setUpSaveAdaptUserData is the controlling method to write
	 * answer/results data back to the V2 database.  It calls
	 * the other methods to do the actual work.
	 * 
	 * @param ManualEntryDTO dtoIn,  the data being passed 
	 * 			from the Submit button in the Manual Entry App
	 * @return 
	 * @throws Exception
	 */
	public void setUpSaveAdaptUserData(ManualEntryDTO dtoIn)
		throws Exception
	{        
		lpDTO = dtoIn;
//		System.out.println("   lpDTO.getAdaptProjectId().... " + lpDTO.getAdaptProjectId());
//		System.out.println("   lpDTO.getAdaptProjectId().... " + lpDTO.getAdaptProjectId());
		
	} 
	
	
	/**
	 * buildResultsXML creates the XML string to 
	 * be inserted into the V2 database
	 * This is called by the Manual Entry application
	 * 
	 * @throws Exception
	 */
	public void buildResultsXML()
		throws Exception
	{        
		// there's only ever going to be one mcDTO in this array, 
		// so I'm safe to just get the first one
		ModConstsDTO mcDTO = (ModConstsDTO) lpDTO.getModConstDTOList().get(0);

		Iterator<ConstructDTO> it = mcDTO.getConstructsAry().iterator();
		while(it.hasNext())
		{
			ConstructDTO cDTO = new ConstructDTO();
			cDTO = it.next();
			
			if(cDTO.getConstructScore() == null)
			{
				continue;
			}
			
			String xml = "";			
			xml += XML_HEADER;
			xml += TAG_XML_FIELDS_OPEN;
			xml += TAG_CSCORE_OPEN;
			xml += cDTO.getConstructScore().toString();
			xml += TAG_CSCORE_CLOSE;
			xml += TAG_XML_FIELDS_CLOSE;		
			resultsXML.put(cDTO.getConstructId(), xml);
		}
		
		return;
	} 	

	
	/**
	 * buildResultsXMLForSHLIntegration creates the XML string to 
	 * be inserted into the V2 database
	 * this is specific to the SHL Integration data
	 * 
	 * @throws Exception
	 */
	public void  buildResultsXMLForSHLIntegration(HashMap<String,
												   String> tScore,
												   ManualEntryDTO dtoIn)
		throws Exception
	{    
		String xml = "";
		// there's only ever going to be one mcDTO in this array, 
		// so I'm safe to just get the first one
		//System.out.println("dtoIn.getModConstDTOList().size " + dtoIn.getModConstDTOList().size());		
		ModConstsDTO mcDTO = (ModConstsDTO) dtoIn.getModConstDTOList().get(0);

		Iterator<ConstructDTO> it = mcDTO.getConstructsAry().iterator();
		while(it.hasNext()){
			ConstructDTO cDTO = new ConstructDTO();
			cDTO = it.next();
			
			
			if(cDTO.getConstructScore() == null)
			{
				continue;
			}
			
			xml = "";			
			xml += XML_HEADER;
			xml += TAG_XML_FIELDS_OPEN;
			xml += TAG_CSCORE_OPEN;
			xml += cDTO.getConstructScore().toString();
			xml += TAG_CSCORE_CLOSE;
			xml += TAG_XML_FIELDS_CLOSE;		
			resultsXML.put(cDTO.getConstructId(), xml);
		}
		
		return;
	} 
	
	
	/**
	 * buildResultsXMLforAbyD creates the XML string to 
	 * be inserted into the V2 database.
	 * This is called by the Manual Entry application,
	 * and is specific to the data needed by the A by D 
	 * application and extract.
	 * 
	 * @param Connection  - the database connection
	 * @throws Exception
	 */
	public  void buildResultsXMLforAbyD()
		throws Exception
	{        
			//System.out.println("buildResultsXMLforAbyD()....260");	
		// there's only ever going to be one mcDTO in this array, 
		// so I'm safe to just get the first one
		ModConstsDTO mcDTO = (ModConstsDTO) lpDTO.getModConstDTOList().get(0);
			//System.out.println("buildResultsXMLforAbyD().... 264....");
		
		//boolean cogsDone = false;
		Double zScore = null;
		Double zScoreGenPop = null;
		Double pctlScore = null;
		Double pctlScoreGenPop = null;
		/*
			if this is the COGSDONE module (KFVDJXHR)
			construct id = COGSDONE
			there aren't any norms for this.  results should show a value of 1.
			really, if it's there at all, we need to just set that cogsdone flag.
			
			the flag for this is simply the existence of the construct for this 
			candidate/project????  
 		 */
		
		if(mcDTO.getModuleId() == null)
		{
			//System.out.println("dto moduleId == null");
			throw new Exception("dto moduleId == null");
		} else {
				//System.out.println("buildResultsXMLforAbyD().... 286....");			
			//if(mcDTO.getModuleId().equals(COGS_DONE_MODULE))
			//{
			//	//System.out.println("it's COGSDONE!!!!");
			//	cogsDone = true;
			//}
			
			if(mcDTO.getNormId() != null)
			{
				//System.out.println("mcDTO.getNormId() " + mcDTO.getNormId());
			}
			
			if(mcDTO.getGenPopNormId() != null)
			{
				//System.out.println("getGenPopNormId()  " + mcDTO.getGenPopNormId());
			}			
				//System.out.println("buildResultsXMLforAbyD().... 299....");				
			Iterator<ConstructDTO> it = mcDTO.getConstructsAry().iterator();
			while(it.hasNext())
			{
					//System.out.println("buildResultsXMLforAbyD().... 302....");					
				ConstructDTO cDTO = new ConstructDTO();
				cDTO = it.next();
				if(cDTO == null)
				{
					//System.out.println("(cDTO == null!!!!");
				}
					//System.out.println("cDTO.id = " + cDTO.getConstructId() + "  " + cDTO.getConstructName());
				
				//Norm norm = cDTO.getConstructNorm();
				String normId = mcDTO.getNormId();
				String genPopNormId = mcDTO.getGenPopNormId();
				if(cDTO.getConstructScore() == null)
				{
					//System.out.println("cDTO.getConstructScore() == null ");
					continue;
				}
				
				if(normId == null)
				{
					//System.out.println("mcDTO.getNormId() norm ==  null ");
				}
				
				if(genPopNormId == null)
				{
					//System.out.println("mcDTO.getGenPopNormId() norm ==  null ");
				}
				
				// okay, get the mean/stddev for this norm and construct, so we can get the zscore
				// spec-pop
				if( normId != null )
				{
					Norm updatedNorm = NormUtils.getNormAndParseForConstruct(normId, cDTO.getConstructId());
					zScore = updatedNorm.calcZScore(new Double(cDTO.getConstructScore()));	
					//System.out.println("normed Score = " + zScore.toString() + "  CSCORE:  " + cDTO.getConstructScore().toString());
					pctlScore = Norm.calcDoublePctlFromZ(zScore);
					//System.out.println("calcDoublePctlFromZ " + pctlScore);
				}
				
				// gen pop
				if( genPopNormId != null )
				{
					Norm updatedGenPopNorm = NormUtils.getNormAndParseForConstruct(genPopNormId, cDTO.getConstructId());
					zScoreGenPop = updatedGenPopNorm.calcZScore(new Double(cDTO.getConstructScore()));	
					//System.out.println("normed Score zScoreGenPop = " + zScoreGenPop.toString() + "  CSCORE:  " + cDTO.getConstructScore().toString());
					pctlScoreGenPop = Norm.calcDoublePctlFromZ(zScoreGenPop);
					//System.out.println("calcDoublePctlFromZ pctlScoreGenPop " + pctlScoreGenPop);
				}
				/*
				<?xml version="1.0" encoding="UTF-8"?>
				<XML_Fields>
    				<CScore.S0>
        			3.75
        				<Norms>
            				<ISXORYVM></ISXORYVM>
            				<CRHUHSFC></CRHUHSFC>
        				</Norms>
    				</CScore.S0>
    				<ISXORYVM>12.4045212999999988</ISXORYVM>
    				<CRHUHSFC>9.42549509999999757</CRHUHSFC>
				</XML_Fields>
				*/
				
				String xml = "";			
				xml += XML_HEADER;
				xml += TAG_XML_FIELDS_OPEN;
				xml += TAG_CSCORE_OPEN;
				xml += cDTO.getConstructScore().toString();

				if(normId == null && genPopNormId == null){
					xml += TAG_NORM_OPEN;						
					xml += TAG_NORM_CLOSE;
					//xml += TAG_CSCORE_CLOSE;
				}
				else if (normId == null || genPopNormId == null)
				{
					xml += TAG_NORM_OPEN;
				}
				else
				{
					xml += TAG_NORM_OPEN;
					
					if(normId != null)
					{
						xml += TAG_OPEN_START;
						xml += normId;  // norm id
						xml += TAG_END;
						xml += TAG_CLOSE_START;
						xml += normId;   // norm id
						xml += TAG_END;
						//xml += TAG_NORM_CLOSE;
						//xml += TAG_CSCORE_CLOSE;
					}
				}
				
				if(genPopNormId == null)
				{
					//xml += TAG_NORM_OPEN;						
					xml += TAG_NORM_CLOSE;
					xml += TAG_CSCORE_CLOSE;
				} else {
					//xml += TAG_CSCORE_OPEN;
					//xml += TAG_NORM_OPEN;						
					xml += TAG_OPEN_START;
					xml += genPopNormId;  // norm id
					xml += TAG_END;
					xml += TAG_CLOSE_START;
					xml += genPopNormId;   // norm id
					xml += TAG_END;
					xml += TAG_NORM_CLOSE;
					xml += TAG_CSCORE_CLOSE;
				}
				
				if(pctlScore != null)
				{
					xml += TAG_OPEN_START;
					xml += normId;   // norm id
					xml += TAG_END;
					xml += pctlScore;   // normed score 
					xml += TAG_CLOSE_START;
					xml += normId;   // norm id
					xml += TAG_END;
				}
				if(pctlScoreGenPop != null)
				{
					xml += TAG_OPEN_START;
					xml += genPopNormId;   // norm id
					xml += TAG_END;
					xml += pctlScoreGenPop;   // normed score 
					xml += TAG_CLOSE_START;
					xml += genPopNormId;   // norm id
					xml += TAG_END;						
				}
				
				//xml += TAG_CSCORE_CLOSE;
				xml += TAG_XML_FIELDS_CLOSE;
				//System.out.println("results xml = " + xml);
				resultsXML.put(cDTO.getConstructId(), xml);				
			}			
		}
	}
	
	
	/**
	 * buildAnswersXML builds the XML for 
	 * answer data in the V2 database
	 *
	 * @throws Exception
	 */
	public  void buildAnswersXML()
		throws Exception
	{        
		// there's only ever going to be one mcDTO in this array, 
		// so I'm safe to just get the first one
		ModConstsDTO mcDTO = lpDTO.getModConstDTOList().get(0); 
		
		answersXML = XML_HEADER;
		answersXML += TAG_XML_FIELDS_OPEN;
		answersXML += TAG_CREATION_ATTMPT_OPEN + "1" + TAG_CREATION_ATTMPT_CLOSE;
		answersXML += TAG_ANSWERS_OPEN;		
		
		ConstructDTO cDTO = new ConstructDTO();
		int index = 1;
		Iterator<ConstructDTO> it = mcDTO.getConstructsAry().iterator();
		while(it.hasNext())
		{	
			cDTO = it.next();
			String score = cDTO.getConstructScore() == null?" ":cDTO.getConstructScore();
			
			answersXML += TAG_A_OPEN_1 + index + TAG_A_OPEN_2;			
			answersXML += score;
			answersXML += TAG_A_CLOSE;
			
			index++;
		}		
		answersXML += TAG_ANSWERS_CLOSE;
		answersXML += TAG_START_DATE_OPEN + dateCreatedStamp + TAG_START_DATE_CLOSE;
		answersXML += TAG_OBJ_RESTORE_OPEN + "Cleared" + TAG_OBJ_RESTORE_CLOSE;
		answersXML += TAG_XML_FIELDS_CLOSE;
	}
	
	
	/**
	 * writeResultsDataToDatabase writes data to 
	 * the results table in the V2 database
	 *
	 *	If update is true, then the admin is updating 
	 * 	the existing scores, and we will update.
	 *  If update is false, there are no existing 
	 *  scores, so we'll just do an insert.
	 *
	 * @param Connection con, the database connection
	 * @throws Exception
	 */
	public  void writeResultsDataToDatabase()
		throws Exception
	{        
		StringBuffer sb = new StringBuffer();

		// need to see if this is going to be an insert or an
		// update!
		//Boolean update = lpDTO.getModConstDTOList().get(0).getHasScores();	
			
		ArrayList<ConstructDTO> doInsert = new ArrayList<ConstructDTO>();
		
		Iterator<ConstructDTO> iter = lpDTO.getModConstDTOList().get(0).getConstructsAry().iterator();

		ConstructDTO cdto = null;
		
		////////////////////////////////////////////
		// Set up the initial checking on it query.
		///////////////////////////////////////////
		StringBuffer sqlQuery1 = new StringBuffer();
		sqlQuery1.append("SELECT rr.UniqueIdStamp ");
		sqlQuery1.append("  FROM results rr ");
		sqlQuery1.append("  WHERE rr.JobId = ? ");
		sqlQuery1.append("    AND rr.CandidateId = ? ");
		sqlQuery1.append("    AND rr.ConstructId = ?");
	
		//System.out.println("sqlQuery1 " + sqlQuery1);	
		
		ResultSet rs1 = null;
		
		////////////////////////////////////////////
		// Set up the update query.
		///////////////////////////////////////////			
		sb.append("UPDATE results ");  					
		sb.append(" set  DateModifiedStamp = ?, ");  	// 1
		sb.append(" ModifiedByStamp = ?,  ");  			// 2
		sb.append(" CScore = ?,  ");  					// 3
		sb.append(" XML = ?  "); 						// 4
		sb.append(" WHERE candidateId = ? ");  		    // 5
		sb.append(" AND jobId = ? ");  				    // 6
		sb.append(" AND constructId = ?  ");  			// 7
		sb.append(" AND uniqueIdStamp = ?  ");		    // 8
	
		////////////////////////////////////////////////////////////////////////
		// spin through the constructs, see if there's a row out there first... 
		////////////////////////////////////////////////////////////////////////	
		while(iter.hasNext())
		{
			DataResult dr1 = null;
			DataResult dr = null;
			DataResult dru = null;

			try {
				cdto = iter.next();
				//System.out.println(testIndex + " :  597 WRITE DATA ITERATOR - NEXT CDTO = " + cdto.getConstructId() + " " + cdto.getConstructName());					
				if(cdto.getConstructScore() == null)
				{
					continue;
				}
				DataLayerPreparedStatement dlps1 = V2DatabaseUtils.prepareStatement(sqlQuery1);
				if (dlps1.isInError())
				{
					System.out.println("Error preparing writeResultsDataToDatabase query." + dlps1.toString());
					return;
				}
					
				// fill in the ?'s for the initial query... 
				dlps1.getPreparedStatement().setString(1, this.lpDTO.getProjectId());
				dlps1.getPreparedStatement().setString(2, this.lpDTO.getCandidateId());
				dlps1.getPreparedStatement().setString(3, cdto.getConstructId());

				// now do the initial select, to see if the row is there.... 
				dr1 = V2DatabaseUtils.select(dlps1);	
					
				if (dr1.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
				{						
					//do an insert.... 
					doInsert.add(cdto);	
					doInsert(doInsert);
					// reset the array... 
					doInsert = new ArrayList<ConstructDTO>();
				}
				else if (dr1.getStatus().getStatusCode() == 0 )
				{
					// THERE ALREADY IS a row, so do the update..cdto.					
					// Get the unique id for parameter 8
					rs1 = dr1.getResultSet();
					rs1.next();
					String uid = rs1.getString("UniqueIdStamp");
						
					DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
					if (dlps.isInError())
					{
						//TODO When this method returns something, this should be modified
						System.out.println("Error preparing writeResultsDataToDatabase query." + dlps.toString());
						return;
					}	
						
					//THIS IS THE UPDATE ...						
					// Fill sql parmeters with values and get the data
					dlps.getPreparedStatement().setString(1, this.dateCreatedStamp);
					dlps.getPreparedStatement().setString(2, this.createdByStamp);
					dlps.getPreparedStatement().setString(3, cdto.getConstructScore().toString());
					dlps.getPreparedStatement().setString(4, this.resultsXML.get(cdto.getConstructId()));
					dlps.getPreparedStatement().setString(5, this.lpDTO.getCandidateId());
					dlps.getPreparedStatement().setString(6, this.lpDTO.getProjectId());
					dlps.getPreparedStatement().setString(7, cdto.getConstructId());
					dlps.getPreparedStatement().setString(8, uid );

					try {
						dru = V2DatabaseUtils.update(dlps);
					} catch(Exception e) {
						
					}
					//rs = dr.getResultSet();
						
					//System.out.println("dr.getStatus().getExceptionCode() =  " + dr.getStatus().getExceptionCode());
					//System.out.println("dr.getStatus().getStatusCode() =  " + dr.getStatus().getStatusCode());
					//System.out.println("690 success: writeResultsDataToDatabase -- UPDATE results : ");
					//System.out.println("  candidateId = " + this.lpDTO.getCandidateId() + 
					//		",  projectId = " + this.lpDTO.getProjectId() + 
					//		",  constructId = " + cdto.getConstructId() +
					//		", constructScore = " + cdto.getConstructScore().toString());
					    
					if (dru != null && ! dru.getStatus().isOk() )
					{
						 System.out.println("Sql Exception : writeResultsDataToDatabase -- update data : ");
					    System.out.println("  candidateId = " + this.lpDTO.getCandidateId() + 
							    			",  projectId = " + this.lpDTO.getProjectId() + ",  "  +
							    			",  constructId = " + cdto.getConstructId());
					}
				}
				else
				{							
					// this is a big bad error
				    System.out.println("Sql Exception : writeResultsDataToDatabase -- update data : ");
				    System.out.println("  candidateId = " + this.lpDTO.getCandidateId() + 
				    			",  projectId = " + this.lpDTO.getProjectId() + ",  "  +
				    			",  constructId = " + cdto.getConstructId());
				}
			} catch (Exception e) {
				//Does nothing
			}
			finally
			{
		        if (dr != null) { dr.close(); dr = null; }
		        if (dr1 != null) { dr1.close(); dr1 = null; }
		        if (dru != null) { dru.close(); dru = null; }
			}
		}	// End of while loop
	}

	
	/**
	 * writeResultsDataToDatabase writes data to 
	 * the results table in the V2 database
	 *
	 *	If update is true, then the admin is updating 
	 * 	the existing scores, and we will update.
	 *  If update is false, there are no existing 
	 *  scores, so we'll just do an insert.
	 *
	 * @param Connection con, the database connection
	 * @throws Exception
	 */
	public  void writeResultsDataToDatabase(ManualEntryDTO dtoIn)
		throws Exception
	{        
		StringBuffer sb = new StringBuffer();

		// need to see if this is going to be an insert or an
		// update!
		Boolean update = dtoIn.getModConstDTOList().get(0).getHasScores();
		
		ArrayList<ConstructDTO> doInsert = new ArrayList<ConstructDTO>(); 		
		Iterator<ConstructDTO> iter = dtoIn.getModConstDTOList().get(0).getConstructsAry().iterator();
		
		if(update)
		{
			ConstructDTO cdto = null;
			sb.append("UPDATE results ");  					
			sb.append(" set  DateModifiedStamp = ?, ");  	// 1
			sb.append(" ModifiedByStamp = ?,  ");  			// 2
			sb.append(" CScore = ?,  ");  					// 3
			sb.append(" XML = ?  "); 						// 4
			sb.append(" WHERE candidateId = ? ");  		    // 5
			sb.append(" AND jobId = ? ");  				    // 6
			sb.append(" AND constructId = ?  ");  			// 7

			DataResult dr = null;
			try
			{
				while(iter.hasNext())
				{
					cdto = iter.next();
					
					if(cdto.getConstructScore() == null)
					{
						continue;
					}			
					
					DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
					if (dlps.isInError())
					{
						//TODO When this method returns something, this should be modified
						System.out.println("Error preparing writeResultsDataToDatabase query." + dlps.toString());
						return;
					}
					// Fill sql parmeters with values and get the data
					dlps.getPreparedStatement().setString(1, this.dateCreatedStamp);
					dlps.getPreparedStatement().setString(2, this.createdByStamp);
					dlps.getPreparedStatement().setString(3, cdto.getConstructScore().toString());
					dlps.getPreparedStatement().setString(4, this.resultsXML.get(cdto.getConstructId()));
					dlps.getPreparedStatement().setString(5, dtoIn.getCandidateId());
					dlps.getPreparedStatement().setString(6, dtoIn.getProjectId());
					dlps.getPreparedStatement().setString(7, cdto.getConstructId());
					
					try {
						dr = V2DatabaseUtils.update(dlps);
						
						int updated = dr.getStatus().getExceptionCode(); 
						//System.out.println("updated status =  " + updated);
						//System.out.println("success: writeResultsDataToDatabase -- insert data : ");
						//System.out.println("  candidateId = " + dtoIn.getCandidateId() + 
						//		",  projectId = " + dtoIn.getProjectId() + ",  "  +
						//		",  constructId = " + cdto.getConstructId());
						
						if(updated != RequestStatus.RS_OK)
						{
							//System.out.println("(updated != RequestStatus.RS_OK) " + updated);
							doInsert.add(cdto);							
						}
					} catch(Exception e) {
						// Does nothing
					}
					finally
					{
						if (dr != null) { dr.close(); dr = null; }
					}			
				}	//
				
				if(doInsert.size() > 0)
				{
					// there are some inserts to do...
					doInsert(doInsert, dtoIn);
				}
			}
			catch(java.sql.SQLException se)
			{
			    System.out.println("Sql Exception : writeResultsDataToDatabase -- update data : ");
			    System.out.println("message = " + se.getMessage());
			    System.out.println("  candidateId = " + dtoIn.getCandidateId() + 
			    			",  projectId = " + dtoIn.getProjectId() + ",  "  +
			    			",  constructId = " + cdto.getConstructId());
			}
		} else {  // do insert
			ConstructDTO cdto = null;
			sb.append("INSERT INTO results ");
			sb.append("(UniqueIdStamp, DateCreatedStamp, DateModifiedStamp, CreatedByStamp, ");
			sb.append(" ModifiedByStamp, CandidateId, JobId, ConstructId, CScore, XML) ");
			sb.append(" VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");			

			DataResult dru = null;
			try
			{
				while(iter.hasNext())
				{
					cdto = iter.next();
					
					if(cdto.getConstructScore() == null)
					{
						continue;
					}
					
					// I've created my own unique Id, 
					// while we wait for the actual HRA code
					String uid = buildHRADateAndUID();

					DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
					if (dlps.isInError())
					{
						//TODO When this method returns something, this should be modified
						System.out.println("Error preparing writeResultsDataToDatabase query." + dlps.toString());
						return;
					}

					// Fill sql parameters with values and get the data
					dlps.getPreparedStatement().setString(1, uid);
					dlps.getPreparedStatement().setString(2, this.dateCreatedStamp);
					dlps.getPreparedStatement().setString(3, this.dateCreatedStamp);
					dlps.getPreparedStatement().setString(4, this.createdByStamp);
					dlps.getPreparedStatement().setString(5, this.createdByStamp);
					dlps.getPreparedStatement().setString(6, dtoIn.getCandidateId());
					dlps.getPreparedStatement().setString(7, dtoIn.getProjectId());
					dlps.getPreparedStatement().setString(8, cdto.getConstructId());
					dlps.getPreparedStatement().setString(9, cdto.getConstructScore().toString());
					dlps.getPreparedStatement().setString(10, this.resultsXML.get(cdto.getConstructId()));

					try {
						dru = V2DatabaseUtils.update(dlps);
					} catch (Exception e) {
						
					}
					finally
					{
						if (dru != null) { dru.close(); dru = null; }
					}			
				}  // end while    
			}
			catch(java.sql.SQLException se)
			{
			    System.out.println("Sql Exception : writeResultsDataToDatabase -- insert data : ");
			    System.out.println("message = " + se.getMessage());
			    System.out.println("message = " + se.getStackTrace());
			    System.out.println("  candidateId = " + this.lpDTO.getCandidateId() + 
			    			",  projectId = " + this.lpDTO.getProjectId() + ",  "  +
			    			",  constructId = " + cdto.getConstructId());
			}
		}// end else
	}	
	
	
	/**
	 * doInsert writes data to 
	 * the results table in the V2 database.
	 * It's called from the if(update) in the 
	 * writeResultsDataToDatabase method, if there are
	 * constructs that have been added that don't already
	 * exist in the database.  This would only affect the
	 * CPI, where there are 3 "un-required" constructs.
	 *
	 * @param ArrayList arList, the array of constructDTO's that 
	 * 			need to be inserted
	 * @throws Exception
	 */
	public  void doInsert(ArrayList<ConstructDTO> arList)
		throws Exception
	{        		
		StringBuffer sb = new StringBuffer();
	
		Iterator<ConstructDTO> iter = arList.iterator();
		ConstructDTO cdto = null;
		sb.append("INSERT INTO results ");
		sb.append("(UniqueIdStamp, DateCreatedStamp, DateModifiedStamp, CreatedByStamp, ");
		sb.append(" ModifiedByStamp, CandidateId, JobId, ConstructId, CScore, XML) ");
		sb.append(" VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");

		DataResult dr = null;
		//ResultSet rs = null;
		
		try
		{
			while(iter.hasNext())
			{
				cdto = iter.next();					

				// I've created my own unique Id, 
				// while we wait for the actual HRA code
				String uid = buildHRADateAndUID();

				DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
				if (dlps.isInError())
				{
					//TODO When this method returns something, this should be modified
					System.out.println("Error preparing doInsert query." + dlps.toString());
					return;
				}

				// Fill sql parameters with values and get the data
				dlps.getPreparedStatement().setString(1, uid);
				dlps.getPreparedStatement().setString(2, this.dateCreatedStamp);
				dlps.getPreparedStatement().setString(3, this.dateCreatedStamp);
				dlps.getPreparedStatement().setString(4, this.createdByStamp);
				dlps.getPreparedStatement().setString(5, this.createdByStamp);
				dlps.getPreparedStatement().setString(6, this.lpDTO.getCandidateId());
				dlps.getPreparedStatement().setString(7, this.lpDTO.getProjectId());
				dlps.getPreparedStatement().setString(8, cdto.getConstructId());
				dlps.getPreparedStatement().setString(9, cdto.getConstructScore().toString());
				dlps.getPreparedStatement().setString(10, this.resultsXML.get(cdto.getConstructId()));

				try {
					dr = V2DatabaseUtils.insert(dlps);
				} catch (Exception e) {
					//dr.close();
				}
				finally
				{
					if (dr != null) { dr.close(); dr = null; }
				}			
			}  // end while    
		}
		catch(java.sql.SQLException se)
		{
		    System.out.println("Sql Exception : writeResultsDataToDatabase -- insert data : ");
		    System.out.println("message = " + se.getMessage());
		    System.out.println("message = " + se.getStackTrace());
		    System.out.println("  candidateId = " + this.lpDTO.getCandidateId() + 
		    			",  projectId = " + this.lpDTO.getProjectId() + ",  "  +
		    			",  constructId = " + cdto.getConstructId());
		}
	}	

	
	/**
	 * doInsert writes data to 
	 * the results table in the V2 database.
	 * It's called from the if(update) in the 
	 * writeResultsDataToDatabase method, if there are
	 * constructs that have been added that don't already
	 * exist in the database.  This would only affect the
	 * CPI, where there are 3 "un-required" constructs.
	 *
	 * @param ArrayList arList, the array of constructDTO's that 
	 * 			need to be inserted
	 * @throws Exception
	 */
	public  void doInsert(ArrayList<ConstructDTO> arList, ManualEntryDTO dtoIn )
		throws Exception
	{        
		StringBuffer sb = new StringBuffer();
	
		Iterator<ConstructDTO> iter = arList.iterator();
		ConstructDTO cdto = null;
		sb.append("INSERT INTO results ");
		sb.append("(UniqueIdStamp, DateCreatedStamp, DateModifiedStamp, CreatedByStamp, ");
		sb.append(" ModifiedByStamp, CandidateId, JobId, ConstructId, CScore, XML) ");
		sb.append(" VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");

		DataResult dr = null;
		try
		{
			while(iter.hasNext())
			{
				cdto = iter.next();					

				// I've created my own unique Id, 
				// while we wait for the actual HRA code
				String uid = buildHRADateAndUID();

				DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
				if (dlps.isInError())
				{
					//TODO When this method returns something, this should be modified
					System.out.println("Error preparing doInsert query." + dlps.toString());
					return;
				}

				// Fill sql parmeters with values and get the data
				dlps.getPreparedStatement().setString(1, uid);
				dlps.getPreparedStatement().setString(2, this.dateCreatedStamp);
				dlps.getPreparedStatement().setString(3, this.dateCreatedStamp);
				dlps.getPreparedStatement().setString(4, this.createdByStamp);
				dlps.getPreparedStatement().setString(5, this.createdByStamp);
				dlps.getPreparedStatement().setString(6, dtoIn.getCandidateId());
				dlps.getPreparedStatement().setString(7, dtoIn.getProjectId());
				dlps.getPreparedStatement().setString(8, cdto.getConstructId());
				dlps.getPreparedStatement().setString(9, cdto.getConstructScore().toString());
				dlps.getPreparedStatement().setString(10, this.resultsXML.get(cdto.getConstructId()));
				
				try {
					dr = V2DatabaseUtils.insert(dlps);
					dr.getResultSet().close();
				} catch(Exception e) {
					
				}
				finally
				{
					if (dr != null) { dr.close(); dr = null; }
				}			
			}  // end while    
		}
		catch(java.sql.SQLException se)
		{
		    System.out.println("Sql Exception : writeResultsDataToDatabase -- insert data : ");
		    System.out.println("message = " + se.getMessage());
		    System.out.println("message = " + se.getStackTrace());
		    System.out.println("  candidateId = " + this.lpDTO.getCandidateId() + 
		    			",  projectId = " + this.lpDTO.getProjectId() + ",  "  +
		    			",  constructId = " + cdto.getConstructId());
		}
	}	

	
	/**
	 * writeAnswerDataToDatabase writes data to 
	 * the answers table in the V2 database
	 *
	 * @throws Exception
	 */
	public  void writeAnswerDataToDatabase()
		throws Exception
	{        
		StringBuffer sb = new StringBuffer();

		// need to see if this is going to be an insert or an
		// update! 
		Boolean update = lpDTO.getModConstDTOList().get(0).getHasScores();
		
		// don't trust that the update is set right for manual entry.... .so checking it
		// manually....
		//if(lpDTO.getManualEntryType().equals(ManualEntryConstants.ME_A_BY_D_TYPE)){
			update = checkForAnswerData(this.lpDTO.getProjectId(), this.lpDTO.getCandidateId(), this.lpDTO.getModConstDTOList().get(0).getModuleId() );
			//System.out.println("1093 setting UPDATE.... " + update);
		//}

		Iterator<ConstructDTO> iter = lpDTO.getModConstDTOList().get(0).getConstructsAry().iterator();
		
		if(update)
		{
			ConstructDTO cdto = null;
			
			sb.append("UPDATE answers ");  					
			sb.append(" set  DateModifiedStamp = ?, ");  // 1
			sb.append(" ModifiedByStamp = ?,  ");  		 // 2
			sb.append(" Completed = ?,  ");  			 // 3
			sb.append(" XML = ?  ");  					 // 4
			sb.append(" WHERE CandidateId = ? ");  	     // 5
			sb.append(" AND ModuleId = ? ");  			 // 6
			sb.append(" AND  JobId = ?  ");  			 // 7

			DataResult dr = null;
			try
			{
				while(iter.hasNext())
				{
					//System.out.println("this.dateCreatedStamp " + this.dateCreatedStamp);
					//String uid = buildHRADateAndUID(); // actually using this to get date created
					buildHRADateAndUID(); // actually using this to get date created
					//System.out.println("this.dateCreatedStamp " + this.dateCreatedStamp);
					
					cdto = iter.next();	
					
					DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
					if (dlps.isInError())
					{
						//TODO When this method returns something, this should be modified
						System.out.println("Error preparing writeAnswerDataToDatabase query." + dlps.toString());
						return;
					}

					// Fill sql parmeters with values and get the data
					dlps.getPreparedStatement().setString(1, this.dateCreatedStamp);
					dlps.getPreparedStatement().setString(2, this.createdByStamp);
					dlps.getPreparedStatement().setString(3, "1");
					dlps.getPreparedStatement().setString(4, this.answersXML);
					dlps.getPreparedStatement().setString(5, this.lpDTO.getCandidateId());
					dlps.getPreparedStatement().setString(6, this.lpDTO.getModConstDTOList().get(0).getModuleId());
					dlps.getPreparedStatement().setString(7, this.lpDTO.getProjectId());

					try {
						dr = V2DatabaseUtils.update(dlps);
						dr.getResultSet().close();
					} catch(Exception e) {
						
					}
					finally
					{
						if (dr != null) { dr.close(); dr = null; }
					}		
					// for Answers, we only have to do this ONCE - 
					//  there is only one answer row per module, and all
					// the construct answers are included in that one XML
					break;
				}
				lpDTO.getModConstDTOList().get(0).setHasScores(true);
			}
			catch(java.sql.SQLException se)
			{
			    System.out.println("Sql Exception : writeAnswerDataToDatabase -- update data : ");
			    System.out.println("message = " + se.getMessage());
			    System.out.println("  candidateId = " + this.lpDTO.getCandidateId() + 
			    			",  projectId = " + this.lpDTO.getProjectId() + ",  "  +
			    			",  constructId = " + cdto.getConstructId());

			}
		} else { // do insert
			String uid = buildHRADateAndUID();

			sb.append("INSERT INTO answers ");
			sb.append("(UniqueIdStamp, DateCreatedStamp, DateModifiedStamp, CreatedByStamp, ModifiedByStamp, ");
			sb.append(" CandidateId, ModuleId, JobId, StartDate, Completed, XML) ");
			sb.append(" VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");

			DataResult dr = null;
			try
			{
				while(iter.hasNext())
				{

					DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
					if (dlps.isInError())
					{
						//TODO When this method returns something, this should be modified
						System.out.println("Error preparing writeAnswerDataToDatabase query." + dlps.toString());
						return;
					}
					// Fill sql parmeters with values and get the data
					dlps.getPreparedStatement().setString(1, uid);
					dlps.getPreparedStatement().setString(2, this.dateCreatedStamp);
					dlps.getPreparedStatement().setString(3, this.dateCreatedStamp);
					dlps.getPreparedStatement().setString(4, this.createdByStamp);
					dlps.getPreparedStatement().setString(5, this.createdByStamp);
					dlps.getPreparedStatement().setString(6, this.lpDTO.getCandidateId());
					dlps.getPreparedStatement().setString(7, this.lpDTO.getModConstDTOList().get(0).getModuleId());
					dlps.getPreparedStatement().setString(8, this.lpDTO.getProjectId());
					dlps.getPreparedStatement().setString(9, this.dateCreatedStamp);
					dlps.getPreparedStatement().setString(10, "1");
					dlps.getPreparedStatement().setString(11, this.answersXML);

					try {
						dr = V2DatabaseUtils.insert(dlps);
						dr.getResultSet().close();
					} catch(Exception e) {
						
					}
					finally
					{
						if (dr != null) { dr.close(); dr = null; }
					}			
					// for Answers, we only have to do this ONCE - 
					//  there is only one answer row per module, and all
					// the construct answers are included in that one XML
					break;
				}  // end while
				lpDTO.getModConstDTOList().get(0).setHasScores(true);
			}
			catch(java.sql.SQLException se)
			{
			    System.out.println("Sql Exception : writeAnswersDataToDatabase -- insert data : ");
			    System.out.println("message = " + se.getMessage());
			    System.out.println("  candidateId = " + this.lpDTO.getCandidateId() + 
			    			",  projectId = " + this.lpDTO.getProjectId() + ",  "  +
			    			",  moduleId = " + this.lpDTO.getModConstDTOList().get(0).getModuleId());
			}
		}// end else
	}

	
	/**
	 * writeAnswerDataToDatabaseForVendorIntegration writes data to 
	 * the answers table in the V2 database
	 * I've written this specifically for the needs of the SHL Vendor integration, 
	 * though it may be used by other integrations as well.
	 * Also, as this is only to set the answers to completed or not completed, 
	 * there is no XML to be put into the database.
	 * 
	 * @param Connection con, the database connection
	 * @throws Exception
	 */
	public  void writeAnswerDataToDatabaseForVendorIntegration(ManualEntryDTO dtoIn, boolean success)
		throws Exception
	{        		
		StringBuffer sb = new StringBuffer();

		// need to see if this is going to be an insert or an
		// update!  Connection con, String projId, String candId, String modId 
		Boolean update = checkForAnswerData(dtoIn.getProjectId(), dtoIn.getCandidateId(), dtoIn.getModuleId());	
		
		double completed;
		String completedString = "";
		if(success)
		{
			completed = 1;
			completedString = Double.toString(completed).substring(0, 1);
		} else {
			completed = .5;
			completedString = Double.toString(completed);
		}
		
		String xmlString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><XML_Fields>" +
				"<CreationAttempt>1</CreationAttempt><Answers></Answers>" +
				"<StartDates>"+ this.dateCreatedStamp + "</StartDates><ObjectRestore>" +
				"<N N=\"PreviousSaveTime\">61327</N><N N=\"ModAdminStart\">8161</N>" +
				"<N N=\"SaveTime\">181972</N></ObjectRestore></XML_Fields>";
		
		if(update)
		{
			sb.append("UPDATE answers ");  					
			sb.append(" set  DateModifiedStamp = ?, ");  // 1
			sb.append(" ModifiedByStamp = ?,  ");  		 // 2
			sb.append(" Completed = ?,  ");  			 // 3
			sb.append(" XML = ?  ");  					 // 4
			sb.append(" WHERE CandidateId = ? ");  	     // 5
			sb.append(" AND ModuleId = ? ");  			 // 6
			sb.append(" AND  JobId = ?  ");  			 // 7

			DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
			if (dlps.isInError())
			{
				//TODO When this method returns something, this should be modified
				System.out.println("Error preparing writeAnswerDataToDatabaseForVendorIntegration query." + dlps.toString());
				return;
			}

			DataResult dr = null;
			try
			{
				buildHRADateAndUID(); // actually using this to get date created

				// Fill sql parmeters with values and get the data
				dlps.getPreparedStatement().setString(1, this.dateCreatedStamp);
				dlps.getPreparedStatement().setString(2, this.createdByStamp);
				dlps.getPreparedStatement().setString(3, completedString);
				dlps.getPreparedStatement().setString(4, xmlString);
				dlps.getPreparedStatement().setString(5, dtoIn.getCandidateId());
				dlps.getPreparedStatement().setString(6, dtoIn.getModuleId());
				dlps.getPreparedStatement().setString(7, dtoIn.getProjectId());
				
				try {
					dr = V2DatabaseUtils.update(dlps);
					dr.getResultSet().close();
				} catch(Exception e) {
					
				}
			}
			catch(java.sql.SQLException se)
			{
			    System.out.println("Sql Exception : writeAnswerDataToDatabaseForVendorIntegration -- update data : ");
			    System.out.println("message = " + se.getMessage());
			    System.out.println("  candidateId = " + dtoIn.getCandidateId() + 
			    			",  projectId = " + dtoIn.getProjectId() + ",  moduleId = "  + dtoIn.getModuleId());
			}
			finally
			{
				if (dr != null) { dr.close(); dr = null; }
			}		
		} else { // do insert
			String uid = buildHRADateAndUID();

			sb.append("INSERT INTO answers ");
			sb.append("(UniqueIdStamp, DateCreatedStamp, DateModifiedStamp, CreatedByStamp, ModifiedByStamp, ");
			sb.append(" CandidateId, ModuleId, JobId, StartDate, Completed, XML) ");
			sb.append(" VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");
			
			DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
			if (dlps.isInError())
			{
				//TODO When this method returns something, this should be modified
				System.out.println("Error preparing writeAnswerDataToDatabaseForVendorIntegration query." + dlps.toString());
				return;
			}

			DataResult dr = null;
			try
			{
				// Fill sql parmeters with values and get the data
				dlps.getPreparedStatement().setString(1, uid);
				dlps.getPreparedStatement().setString(2, this.dateCreatedStamp);
				dlps.getPreparedStatement().setString(3, this.dateCreatedStamp);
				dlps.getPreparedStatement().setString(4, this.createdByStamp);
				dlps.getPreparedStatement().setString(5, this.createdByStamp);
				dlps.getPreparedStatement().setString(6, dtoIn.getCandidateId());
				dlps.getPreparedStatement().setString(7, dtoIn.getModuleId());
				dlps.getPreparedStatement().setString(8, dtoIn.getProjectId());
				dlps.getPreparedStatement().setString(9, this.dateCreatedStamp);
				dlps.getPreparedStatement().setString(10, Double.toString(completed));
				dlps.getPreparedStatement().setString(11, xmlString);

				try {
					dr = V2DatabaseUtils.insert(dlps);
					dr.getResultSet().close();
				} catch(Exception e) {
					
				}
			}
			catch(java.sql.SQLException se)
			{
			    System.out.println("Sql Exception : writeAnswerDataToDatabaseForVendorIntegration -- insert data : ");
			    System.out.println("message = " + se.getMessage());
			    System.out.println("  candidateId = " + this.lpDTO.getCandidateId() + 
			    			",  projectId = " + this.lpDTO.getProjectId() + ",  "  +
			    			",  moduleId = " + this.lpDTO.getModuleId());
			}
			finally
			{
				if (dr != null) { dr.close(); dr = null; }
			}			
		}// end else
	}	

	
	/**
	 * checkForAnswerData checks to see if there is data already
	 * in existence in the answers table
	 * 
	 * @param Connection con, the database connection
	 * @param String projId, the project/job id
	 * @param String candId, the candidate Id
	 * @param String modId, the module Id
	 * @return Boolean  - if the data exists, true, if not, false
	 */
	public Boolean checkForAnswerData(String projId, String candId, String modId )
		throws Exception 
	{
		Boolean doesExist = false;
        
        StringBuffer sqlQuery = new StringBuffer();
        sqlQuery.append("SELECT  uniqueIdStamp ");
        sqlQuery.append("  FROM  answers ");
        sqlQuery.append("  WHERE candidateId = ?  ");
        sqlQuery.append("  AND moduleId = ?  ");
        sqlQuery.append("  AND jobId = ?  ");
        //System.out.println(" checkForAnswerData sql : " + sqlQuery.toString());
        
		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			//TODO When this method returns something, this should be modified
			System.out.println("Error preparing checkForAnswerData query." + dlps.toString());
			return doesExist;
		}

		DataResult dr = null;
		ResultSet rs = null;
        try
		{
			// Fill sql parameters with values and get the data
			dlps.getPreparedStatement().setString(1, candId);
			dlps.getPreparedStatement().setString(2, modId);
			dlps.getPreparedStatement().setString(3, projId);

			dr = V2DatabaseUtils.select(dlps);
			rs = dr.getResultSet();
           	
	       	if (rs == null || ! rs.isBeforeFirst())
	       	{
	       		return doesExist; 
	       	}
	       	
	       	while (rs.next())
	       	{
	       		//System.out.println("rs.getString(uniqueIdStamp)" + rs.getString("uniqueIdStamp"));
	       		//answerUniqueId = (String) rs.getString("uniqueIdStamp");
	       		doesExist = true;
	       		
	       	}
	       	
	       	return doesExist;
		}
        catch (SQLException ex)
		{
        	// handle any errors
        	throw new Exception("SQL checking for Answer Data.  " +
        			"SQLException: " + ex.getMessage() + ", " +
					"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
        finally
		{
			if (dr != null) { dr.close(); dr = null; }
        }
	}

	
	/**
	 * checkForAnswerData checks to see if there is data already
	 * in existence in the answers table
	 * 
	 * @param Connection con, the database connection
	 * @param String projId, the project/job id
	 * @param String candId, the candidate Id
	 * @param String modId, the module Id
	 * @return Boolean  - if the data exists, true, if not, false
	 */
	public Boolean checkForCompletedVendorModules(String projId, String candId, String mods )
		throws Exception 
	{
		Boolean doesExist = false;
        
        StringBuffer sqlQuery = new StringBuffer();
        sqlQuery.append("SELECT  completed, moduleId ");
        sqlQuery.append("  FROM  answers ");
        sqlQuery.append("  WHERE candidateId = ?  ");
        sqlQuery.append("  AND moduleId IN  " + mods + "  ");
        sqlQuery.append("  AND jobId = ?  ");        
       //System.out.println("1482  checkForCompletedVendorModules sql : " + sqlQuery.toString());
 
		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			//TODO When this method returns something, this should be modified
			System.out.println("Error preparing checkForCompletedVendorModules query." + dlps.toString());
			return doesExist;
		}

		DataResult dr = null;
		ResultSet rs = null;
        try
		{
			// Fill sql parmeters with values and get the data
			dlps.getPreparedStatement().setString(1, candId);
			dlps.getPreparedStatement().setString(2, projId);

			dr = V2DatabaseUtils.select(dlps);
			rs = dr.getResultSet();

           	
	       	if (rs == null || ! rs.isBeforeFirst())
	       	{
	       		return doesExist; 
	       	}
	       	
	       	int count = 0;
	       	while (rs.next())
	       	{
	       		//System.out.println("rs.getString(completed)  " + rs.getString("completed") + "  " + rs.getString("moduleId") );
	       		count++;
	       	}	


	       //	System.out.println("count  " + count);
	       	if(count == NUMBER_OF_VENDOR_MODULES){
	       		doesExist = true;
	       	}
	       	return doesExist;
		}
        catch (SQLException ex)
		{
        	// handle any errors
        	throw new Exception("SQL checking for Answer Data.  " +
        			"SQLException: " + ex.getMessage() + ", " +
					"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
        finally
		{
			if (dr != null) { dr.close(); dr = null; }
        }
		
	}	
	

	/**
	 * checkForResultsData checks to see if there is data already
	 * in existence in the results table
	 * 
	 * @param Connection con, the database connection
	 * @param String projId, the project/job id
	 * @param String candId, the candidate Id
	 * @param String constructId, the contstruct Id
	 * @return Boolean  - if the data exists, true, if not, false
	 */
	public Boolean checkForResultsData(String projId, String candId, String constructId )
		throws Exception 
	{
		Boolean doesExist = false;
        
        StringBuffer sqlQuery = new StringBuffer();
        sqlQuery.append("SELECT  uniqueIdStamp ");
        sqlQuery.append("  FROM  results ");
        sqlQuery.append("  WHERE candidateId = ?  ");
        sqlQuery.append("  AND constructId = ?  ");
        sqlQuery.append("  AND jobId = ?  ");
        //System.out.println(" checkForResultsData sql : " + sqlQuery.toString());
        
		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
		
		if (dlps.isInError())
		{
			//TODO When this method returns something, this should be modified
			System.out.println("Error preparing checkForResultsData query." + dlps.toString());
			return doesExist;
		}

		DataResult dr = null;
		ResultSet rs = null;
        try
		{
			// Fill sql parameters with values and get the data
			dlps.getPreparedStatement().setString(1, candId);
			dlps.getPreparedStatement().setString(2, constructId);
			dlps.getPreparedStatement().setString(3, projId);

			dr = V2DatabaseUtils.select(dlps);
			rs = dr.getResultSet();
           	
	       	if (rs == null || ! rs.isBeforeFirst())
	       	{
	       		return doesExist; 
	       	}
	       	
	       	while (rs.next())
	       	{
	       		//System.out.println("rs.getString(uniqueIdStamp)   " + rs.getString("uniqueIdStamp"));
	       		//answerUniqueId = (String) rs.getString("uniqueIdStamp");
	       		doesExist = true;
	       	}
	       	
	       	return doesExist;
		}
        catch (SQLException ex)
		{
        	// handle any errors
        	throw new Exception("SQL checking for Answer Data.  " +
        			"SQLException: " + ex.getMessage() + ", " +
					"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
        finally
		{
			if (dr != null) { dr.close(); dr = null; }
        }
	}

	
	/**
	 * insertAnswerDataforVendorPassThroughModule
	 *  inserts a row to set the completed
	 * flag in the answer data for the vendor
	 * integration pass-through module
	 *
	 * @param Connection con, the database connection
	 * @throws Exception
	 */
	public  void insertAnswerDataforVendorPassThroughModule(String modId)
		throws Exception
	{        
		String xmlString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><XML_Fields>" +
		"<CreationAttempt>1</CreationAttempt><Answers></Answers>" +
		"<StartDates>"+ this.dateCreatedStamp + "</StartDates><ObjectRestore>" +
		"<N N=\"PreviousSaveTime\">61327</N><N N=\"ModAdminStart\">8161</N>" +
		"<N N=\"SaveTime\">181972</N></ObjectRestore></XML_Fields>";
		
		StringBuffer sqlQuery = new StringBuffer();
		String key = Utils.getNumericLPKey();
		
		boolean update = checkForAnswerData(this.lpDTO.getProjectId(), this.lpDTO.getCandidateId(), modId );
		
		if(update)
		{
			sqlQuery.append("UPDATE answers ");
			sqlQuery.append("  set DateModifiedStamp = ?,  ");
			sqlQuery.append("  ModifiedByStamp = ?, ");
			sqlQuery.append("  completed = ?, ");
			sqlQuery.append("  XML =  ?  ");
			sqlQuery.append("  WHERE CandidateID = ?  ");
			sqlQuery.append("  AND ModuleID = ?  ");
			sqlQuery.append("  AND JobID = ?  ");
		} else {
			sqlQuery.append("INSERT INTO answers ");
			sqlQuery.append("  (UniqueIdStamp, UniqueVsStamp, ");
			sqlQuery.append("   DateCreatedStamp, DateModifiedStamp, CreatedByStamp, ");
			sqlQuery.append("   CandidateID, ModuleID, JobID, ");
			sqlQuery.append("   Completed, XML) ");
			sqlQuery.append("  VALUES( ?, ?,  ?, ?, ?,  ?, ?, ?, ?, ?)  ");
		}

		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			//TODO When this method returns something, this should be modified
			System.out.println("Error preparing insertAnswerDataforVendorPassThroughModule query." + dlps.toString());
			return;
		}

		DataResult dr = null;
		//ResultSet rs = null;
		try
		{
			if(update)
			{
				// Fill sql parmeters with values and get the data
				dlps.getPreparedStatement().setString(1, Utils.getHRADateString());
				dlps.getPreparedStatement().setString(2, createdByStamp);
				dlps.getPreparedStatement().setString(3, COMPLETED_VALUE);
				dlps.getPreparedStatement().setString(4, xmlString);
				dlps.getPreparedStatement().setString(5, lpDTO.getCandidateId());
				dlps.getPreparedStatement().setString(6, modId);
				dlps.getPreparedStatement().setString(7, lpDTO.getProjectId());
	
				dr = V2DatabaseUtils.update(dlps);
				//rs = dr.getResultSet();
			} else {
				// Fill sql parmeters with values and get the data
				dlps.getPreparedStatement().setString(1, key);
				dlps.getPreparedStatement().setString(2, key);
				dlps.getPreparedStatement().setString(3, Utils.getHRADateString());
				dlps.getPreparedStatement().setString(4, Utils.getHRADateString());
				dlps.getPreparedStatement().setString(5, createdByStamp);
				dlps.getPreparedStatement().setString(6, lpDTO.getCandidateId());
				dlps.getPreparedStatement().setString(7, modId);
				dlps.getPreparedStatement().setString(8, lpDTO.getProjectId());
				dlps.getPreparedStatement().setString(9, COMPLETED_VALUE);
				dlps.getPreparedStatement().setString(10, xmlString);				
	
				dr = V2DatabaseUtils.insert(dlps);
				//rs = dr.getResultSet();
			}
			//System.out.println("insertAnswerDataforVendorPassThroughModule: success:   candidateId = " + lpDTO.getCandidateId() + 
			//		",  projectId = " + lpDTO.getProjectId() + ", moduleId = " + modId);
			
		}
		catch (SQLException ex)
		{
			throw new Exception("SQL in setCompleted. \n " +
					"Cand=" + lpDTO.getCandidateId() + ", job=" + lpDTO.getProjectId() + ". \n " +
					"SQLException: " + ex.getMessage() + ", " +
					"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}
	
	
	/**
	 * writeAdaptV2UserData writes data to 
	 * the pdi_adapt_user_map table in the V2 database.
	 *
	 * @param 
	 * @throws Exception
	 */
	public void writeAdaptV2UserData(ManualEntryDTO dto )
		throws Exception
	{ 
		// Set the time zone.  We use that of our Montreal Partners to
		// avoid confusion with other similar dates in the database. 
		TimeZone tz = TimeZone.getTimeZone("America/Montreal");
		GregorianCalendar gc = new GregorianCalendar(tz);			
		long timeInMillis = gc.getTimeInMillis();
		java.sql.Date date = new java.sql.Date(timeInMillis);
	
		// check for data.... 
		StringBuffer sb = new StringBuffer();
		
		sb.append("select  v2CandidateId   ");
		sb.append("from pdi_adapt_user_map  ");
		sb.append(" where v2CandidateId =  '" + dto.getCandidateId() + "'  ");
		sb.append(" and v2JobId =  '" + dto.getProjectId() + "'  ");
		//System.out.println("sql... " + sb.toString());
		
		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
		if (dlps.isInError())
		{
			//TODO When this method returns something, this should be modified
			System.out.println("Error preparing writeAdaptV2UserData query." + dlps.toString());
			return;
		}
		
		DataResult dr = null;
		DataResult dr2 = null;
		ResultSet rs = null;
		
		try
		{
			dr = V2DatabaseUtils.select(dlps);
			rs = dr.getResultSet();
					
			if (rs != null && rs.getFetchSize() < 1)
			{
				//System.out.println("updating data.... ");
				//////////////////////////////////////////////////////////////
				//   UPDATE
				//////////////////////////////////////////////////////////////
				this.udpateAdaptV2UserData(dto, date, "manualEntry");
			}
			else
			{
				//System.out.println("inserting data.... ");
				//////////////////////////////////////////////////////////////
				//   INSERT
				//////////////////////////////////////////////////////////////
				
				sb = new StringBuffer();
			
				sb.append("INSERT INTO pdi_adapt_user_map ");
				sb.append("(v2CandidateId, v2JobId, AdaptUserId, AdaptProjectId, ");
				sb.append(" AdaptFirstName, AdaptLastName, dateCreated, createdBy) ");
				sb.append(" VALUES ( ?, ?, ?, ?, ?, ?, ?, ?) ");
		
				DataLayerPreparedStatement dlps2 = V2DatabaseUtils.prepareStatement(sb);
				if (dlps2.isInError())
				{
					//TODO When this method returns something, this should be modified
					System.out.println("Error preparing writeAdaptV2UserData query." + dlps2.toString());
					return;
				}
				
				// Fill SQL parameters with values and get the data
				dlps2.getPreparedStatement().setString(1,dto.getCandidateId());
				dlps2.getPreparedStatement().setString(2, dto.getProjectId());
				dlps2.getPreparedStatement().setInt(3, dto.getAdaptUserId());
				dlps2.getPreparedStatement().setInt(4, dto.getAdaptProjectId());
				dlps2.getPreparedStatement().setString(5, dto.getAdaptFirstName());
				dlps2.getPreparedStatement().setString(6, dto.getAdaptLastName());
				dlps2.getPreparedStatement().setDate(7, date);
				dlps2.getPreparedStatement().setString(8, "manualEntry" );
	
				dr2 = V2DatabaseUtils.insert(dlps2);
				//rs = dr.getResultSet();
					
				if (dr2.isInError())
				{
						// this is a big bad error
					    System.out.println("Sql Exception : writeAdaptV2UserData  : ");
					    System.out.println("  candidateId = " + dto.getCandidateId() + 
					    			",  projectId = " + dto.getProjectId());
				}
				else
				{
					// no error, so okay... 
					if (! dr2.getStatus().isOk() )
					{
						System.out.println("Sql Exception : writeAdaptV2UserData : ");
					    System.out.println("  candidateId = " + dto.getCandidateId() + 
					    			",  projectId = " + dto.getProjectId());
					}
					//System.out.println("1890 success: writeAdaptV2UserData : ");
					//System.out.println("  candidateId = " + v2CandId + 
					//		",  projectId = " +v2JobId);
				}
			}
		}
		catch(java.sql.SQLException se)
		{
		    System.out.println("Sql Exception : writeAdaptV2UserData : ");
		    System.out.println("message = " + se.getMessage());
		    System.out.println("message = " + se.getStackTrace());
		    System.out.println("  candidateId = " + this.lpDTO.getCandidateId() + 
		    			",  projectId = " + this.lpDTO.getProjectId());
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
			if (dr2 != null) { dr2.close(); dr2 = null; }
		}	
	}		

	
	/**
	 * udpateAdaptV2UserData updates data to 
	 * the pdi_adapt_user_map table in the V2 database.
	 *
	 * @param 
	 * @throws Exception
	 */
	private void udpateAdaptV2UserData(ManualEntryDTO dto, java.sql.Date date, String uid)
		throws Exception
	{ 
		StringBuffer sb = new StringBuffer();

		sb.append("UPDATE pdi_adapt_user_map ");		
		sb.append(" SET AdaptUserId = ?,  ");		//1 
		sb.append("     AdaptProjectId = ?, ");	//2
		sb.append("     AdaptFirstName = ?, ");		//3
		sb.append("     AdaptLastName = ?, ");		//4
		sb.append("     dateCreated = ?,  ");		//5 
		sb.append("     createdBy = ?  ");			//6
		sb.append("     WHERE v2CandidateId = ? ");	//7
		sb.append("       AND  v2JobId = ? ");		//8 

		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
		if (dlps.isInError())
		{
			//TODO When this method returns something, this should be modified
			System.out.println("Error preparing udpateAdaptV2UserData query." + dlps.toString());
			return;
		}

		DataResult dr = null;
		try
		{
			// Fill sql parameters with values and get the data
			dlps.getPreparedStatement().setInt(1, dto.getAdaptUserId());
			dlps.getPreparedStatement().setInt(2, dto.getAdaptProjectId());			
			dlps.getPreparedStatement().setString(3, dto.getAdaptFirstName());			
			dlps.getPreparedStatement().setString(4, dto.getAdaptLastName());			
			dlps.getPreparedStatement().setDate(5, date);
			dlps.getPreparedStatement().setString(6, uid );
			dlps.getPreparedStatement().setString(7, dto.getCandidateId());
			dlps.getPreparedStatement().setString(8, dto.getProjectId());			
			
			dr = V2DatabaseUtils.update(dlps);
			//rs = dr.getResultSet();
	
			if (dr.isInError())
			{
				// this is a big bad error
			    System.out.println("Sql Exception : udpateAdaptV2UserData : ");
			    System.out.println("  candidateId = " + dto.getCandidateId() + 
			    			",  projectId = " + dto.getProjectId());
			}
			else
			{
				// no error, so okay... 				
				//System.out.println("1975 success: udpateAdaptV2UserData : ");
				//System.out.println("  candidateId = " + dto.getCandidateId() + 
				//			",  projectId = " + dto.getProjectId());
			}				
		}
		catch(java.sql.SQLException se)
		{
		    System.out.println("Sql Exception : udpateAdaptV2UserData : ");
		    System.out.println("message = " + se.getMessage());
		    System.out.println("message = " + se.getStackTrace());
		    System.out.println("  candidateId = " + dto.getCandidateId() + 
	    			",  projectId = " +  dto.getProjectId());
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}			
	}			

	
	/**
	 * writeConsentAnswerRow writes data to 
	 * the answer data table in the V2 database.
	 *
	 * @param 
	 * @throws Exception
	 */
	public  void writeConsentAnswerRow(String consentModuleId, String v2CandId, String v2JobId )
		throws Exception
	{ 
		// v2 admin username and password
		String username = "KWIDJSKL";
		String password = "WKEIDSLA";
		
		V2WebserviceUtils utils = new V2WebserviceUtils();
		
		try
		{
			// got to see if this is an UPDATE first....
			SaveConstructDataHelper helper = new SaveConstructDataHelper();
			Boolean update = helper.checkForAnswerData(v2JobId, v2CandId, consentModuleId);
		
			if(update)
			{
				// the answer data row already exists, so don't add one... 
				// the data in the row doesn't actually change, so don't even bother
				// with an update
			} else {
				// insert
				SessionUserDTO sessionDto = utils.registerUser(username, password);
				utils.insertAnswerRow(sessionDto.getSessionId(), username, password, v2CandId, consentModuleId, v2JobId);
			}
		}
		catch(Exception e)
		{
		    System.out.println("Exception : writeConsentAnswerRow : ");
		    e.printStackTrace();
		}
	}		
	

	/*
	 * buildHRADateAndUID
	 */
	public String buildHRADateAndUID()
	{	
		this.dateCreatedStamp = Utils.getHRADateString();
		return Utils.getNumericLPKey();
	}
	

	/*
	 * trackInstrumentStatus
	 */
	public Boolean trackInstrumentStatus(String projectId,
										 String participantId,
										 String instrumentId,
										 String instrumentStatus)
		throws Exception
	{
		String answerValue;
		try	{			
			instrumentStatus = instrumentStatus.toLowerCase();
			if (instrumentStatus.equals("completed") || instrumentStatus.equals("passed"))
			{
				answerValue = "1";
			} else if (instrumentStatus.equals("not_started"))	{
				answerValue = "0";
			} else { //incomplete
				answerValue = "0.0999";
			}
		}
		catch (Exception e)
		{
			answerValue = "0.0999"; //assumption is that if we're tracking, there must be some sort of progress
		}
    
		StringBuffer sb = new StringBuffer();

		// need to see if this is going to be an insert or an update! 
		
		Boolean update;// = this.lpDTO.getModConstDTOList().get(0).getHasScores();	
		update = checkForAnswerData(projectId, participantId, instrumentId );
		buildHRADateAndUID(); // actually using this to get date created
		System.out.println("this.dateCreatedStamp " + this.dateCreatedStamp);
		this.answersXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><XML_Fields><Answers></Answers><StartDates>" + this.dateCreatedStamp + "</StartDates></XML_Fields>";

		
		if(update)
		{			
			sb.append("UPDATE answers ");  					
			sb.append(" set  DateModifiedStamp = ?, ");  // 1
			sb.append(" ModifiedByStamp = ?,  ");  		 // 2
			sb.append(" Completed = ?,  ");  			 // 3
			sb.append(" XML = ?  ");  					 // 4
			sb.append(" WHERE CandidateId = ? ");  	     // 5
			sb.append(" AND ModuleId = ? ");  			 // 6
			sb.append(" AND  JobId = ?  ");  			 // 7
	
			DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
			if (dlps.isInError())
			{
				System.out.println("Error preparing trackInstrumentStatus query." + dlps.toString());
				return false;
			}

			DataResult dr = null;
			try
			{				
				// Fill sql parmeters with values and get the data
				dlps.getPreparedStatement().setString(1, this.dateCreatedStamp);
				dlps.getPreparedStatement().setString(2, this.createdByStamp);
				dlps.getPreparedStatement().setString(3, answerValue);
				dlps.getPreparedStatement().setString(4, this.answersXML);
				dlps.getPreparedStatement().setString(5, participantId);
				dlps.getPreparedStatement().setString(6, instrumentId);
				dlps.getPreparedStatement().setString(7, projectId);

				dr = V2DatabaseUtils.update(dlps);
			}
			catch(java.sql.SQLException se)
			{
				System.out.println("Error preparing writeAnswerDataToDatabase UPDATE query. " + dlps.toString());
				return false;
			}
			finally
			{
				if (dr != null) { dr.close(); dr = null; }
			}		
		} else { // do insert			
			String uid = buildHRADateAndUID();
			//ConstructDTO cdto = null; 

			sb.append("INSERT INTO answers ");
			sb.append("(UniqueIdStamp, DateCreatedStamp, DateModifiedStamp, CreatedByStamp, ModifiedByStamp, ");
			sb.append(" CandidateId, ModuleId, JobId, StartDate, Completed, XML) ");
			sb.append(" VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");

			DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
			if (dlps.isInError())
			{
				//TODO When this method returns something, this should be modified
				System.out.println("Error preparing writeAnswerDataToDatabase INSERT query. " + dlps.toString());
				return false;
			}

			DataResult dr = null;
			try
			{
				// Fill sql parmeters with values and get the data
				dlps.getPreparedStatement().setString(1, uid);
				dlps.getPreparedStatement().setString(2, this.dateCreatedStamp);
				dlps.getPreparedStatement().setString(3, this.dateCreatedStamp);
				dlps.getPreparedStatement().setString(4, this.createdByStamp);
				dlps.getPreparedStatement().setString(5, this.createdByStamp);
				dlps.getPreparedStatement().setString(6, participantId);
				dlps.getPreparedStatement().setString(7, instrumentId);
				dlps.getPreparedStatement().setString(8, projectId);
				dlps.getPreparedStatement().setString(9, this.dateCreatedStamp);
				dlps.getPreparedStatement().setString(10, answerValue);
				dlps.getPreparedStatement().setString(11, this.answersXML);

				dr = V2DatabaseUtils.insert(dlps);
			}	catch(java.sql.SQLException se)
			{
			    System.out.println("Sql Exception : trackInstrumentStatus -- insert data : ");
			    System.out.println("message = " + se.getMessage());
			    return false;
			}
			finally
			{
				if (dr != null) { dr.close(); dr = null; }
			}					
		}// end else
		return true;
	}
	
//*********************************************************************	
//	THIS SECTION IS ALL FOR SAVING NINTH HOUSE.NET DATA TO V2
//*********************************************************************	
	
/*
 * From the ManualEntryDataHelper, I think I can get the WG_E_CONSTRUCTS, 
 * and the RAVENS_2010_CONSTRUCTS...
 *  
 * 
 *  I'll need to pass in the Individual Report
 * individualReport.getReportData().get(ReportingConstants.RC_WG_E).getScoreData().put("WGE_CT", ct.toString());
 * individualReport.getReportData().get(ReportingConstants.RC_RAVENS_B).getScoreData().put("CORRECT_ANSWERS", correct.toString());
 * 
 * 	I can get the results, then, from the scoreData hash maps
 *  and put it into v2.
 *  We will be saving only rollup scores in the results table.  We won't be doing
 *  	norming, etc... that will be done at report time... 
 *  will need to build the results xml
 *  do a writeresults call... 
 *  
 */

	
	/**
	 * buildResultsXMLFromIR creates the XML string to 
	 * be inserted into the V2 database
	 * This version of the build results xml was created
	 * specifically for a by d -- so that we can get the
	 * data from the two modules we're running on Reflex on
	 * 9th house .net.  
	 * 
	 * @param score - the raw score as a String
	 * @param norms - An ArrayList of the norms used in this XML
	 * @return String - the results XML string
	 * @throws Exception
	 */
	public  String buildResultsXML(String score, ArrayList<Norm> norms)
//		throws Exception
	{        
		String ret = "";
		
		ret += XML_HEADER;
		ret += TAG_XML_FIELDS_OPEN;
		ret += TAG_CSCORE_OPEN;
		ret += score;
		if (norms != null)
		{
			// Add norm list
			ret += TAG_NORM_OPEN;
			for (Norm n : norms)
			{
				ret += TAG_OPEN_START;
				ret += n.getId();
				ret += TAG_END;
				ret += TAG_CLOSE_START;
				ret += n.getId();
				ret += TAG_END;
			}
			ret += TAG_NORM_CLOSE;
		}
		ret += TAG_CSCORE_CLOSE;
		if (norms != null)
		{
			// normed values here
			for (Norm n : norms)
			{
				double pctl;
				if (score == null || score.length() < 1)
				{
					pctl = 0.0;
				} else {
					double dd = Double.parseDouble(score);
					pctl = Norm.calcDoublePctlFromZ(n.calcZScore(dd));
				}

				ret += TAG_OPEN_START;
				ret += n.getId();
				ret += TAG_END;
				ret += pctl;
				ret += TAG_CLOSE_START;
				ret += n.getId();
				ret += TAG_END;
			}
		}
		
		ret += TAG_XML_FIELDS_CLOSE;		

		return ret;
	} 

	
	/**
	 * buildScoreOnlyResultsXML - Convenience method to create
	 * an XML string with no norm data to be inserted into the
	 * V2 database.  This version writes the score only to the
	 * database
	 * 
	 * @param  score - the score to write out to the database
	 * @return String - the results XML string
	 * @throws Exception
	 */
	private String buildResultsXML(String score)
	{        
		return buildResultsXML(score, null);
	} 
	

	/**
	 * writerResultsDataToDatabaseFromIR writes data to 
	 * the results table in the V2 database
	 * and gets the data from the IndividualReport
	 * This was written specifically for the a by d
	 * application, which is getting data remotely from
	 * our modules on Reflex on 9th house .net
	 *
	 * @param IndividualReport - the report data to be saved
	 * @throws Exception
	 */
	public void writeResultsDataToDatabaseFromIR(IndividualReport ir)
		throws Exception
	{        
		//System.out.println("writeResultsDataToDatabaseFromIR......");	
		//System.out.println("ir.getReportCode()......" + ir.getReportCode());
		String resultsXML = "";
		
		if(ir.getReportCode().equals(ReportingConstants.RC_RAVENS_B))
		{
			//System.out.println("RAVENS!");
			// Not complate
			HashMap<String, ArrayList<Norm>> nbc =
				getNormsByConstruct(ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getDisplayData().get("V2_PROJECT_ID"),
									ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getDisplayData().get("V2_MODULE_ID"));
			resultsXML = buildResultsXML(ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getScoreData().get("CORRECT_ANSWERS"),
										 nbc.get(ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getDisplayData("CONSTRUCT_ID_CORRECT")));
			//System.out.println("ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getDisplayData(CONSTRUCT_ID_CORRECT)" + ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getDisplayData("CONSTRUCT_ID_CORRECT"));
			//System.out.println(ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getScoreData("CORRECT_ANSWERS"));
			//System.out.println(resultsXML);
			insertResultsDataToDatabase(
					ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getDisplayData("V2_PROJECT_ID"),
					ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getDisplayData("V2_CANDIDATE_ID"),
					ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getDisplayData("CONSTRUCT_ID_CORRECT"),
					ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getScoreData("CORRECT_ANSWERS"),
					resultsXML
					);
			resultsXML = buildResultsXML(ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getScoreData().get("INCORRECT_ANSWERS"),
										 nbc.get(ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getDisplayData("CONSTRUCT_ID_INCORRECT")));
			//System.out.println("ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getDisplayData(CONSTRUCT_ID_INCORRECT)" + ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getDisplayData("CONSTRUCT_ID_INCORRECT"));			
			//System.out.println(ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getScoreData("INCORRECT_ANSWERS"));
			//System.out.println(resultsXML);
			insertResultsDataToDatabase(
					ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getDisplayData("V2_PROJECT_ID"),
					ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getDisplayData("V2_CANDIDATE_ID"),
					ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getDisplayData("CONSTRUCT_ID_INCORRECT"),
					ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getScoreData("INCORRECT_ANSWERS"),
					resultsXML
					);
			resultsXML = buildResultsXML(ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getScoreData().get("NUMBER_UNANSWERED"),
										 nbc.get(ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getDisplayData("CONSTRUCT_ID_UNANSWERED")));
			//System.out.println("ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getDisplayData(CONSTRUCT_ID_UNANSWERED)" + ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getDisplayData("CONSTRUCT_ID_UNANSWERED"));
			//System.out.println(ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getScoreData("NUMBER_UNANSWERED"));
			//System.out.println(resultsXML);
			insertResultsDataToDatabase(
					ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getDisplayData("V2_PROJECT_ID"),
					ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getDisplayData("V2_CANDIDATE_ID"),
					ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getDisplayData("CONSTRUCT_ID_UNANSWERED"),
					ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getScoreData("NUMBER_UNANSWERED"),
					resultsXML
					);
		} else if (ir.getReportCode().equals(ReportingConstants.RC_WG_E)) {
			//System.out.println("WG-E!");
			HashMap<String, ArrayList<Norm>> nbc =
				getNormsByConstruct(ir.getReportData().get(ReportingConstants.RC_WG_E).getDisplayData().get("V2_PROJECT_ID"),
									ir.getReportData().get(ReportingConstants.RC_WG_E).getDisplayData().get("V2_MODULE_ID"));
			//System.out.println("got " + nbc.size() + " constructs:");
			//for (Iterator<String> itr= nbc.keySet().iterator(); itr.hasNext(); )
			//{
			//	String str = itr.next();
			//	System.out.println("   " + str);
			//}
			//System.out.println("score=" + ir.getReportData().get(ReportingConstants.RC_WG_E).getScoreData().get("WGE_CT"));
			//System.out.println("construct=" + ir.getReportData().get(ReportingConstants.RC_WG_E).getDisplayData().get("CONSTRUCT_ID"));
			resultsXML = buildResultsXML(ir.getReportData().get(ReportingConstants.RC_WG_E).getScoreData().get("WGE_CT"),
										 nbc.get(ir.getReportData().get(ReportingConstants.RC_WG_E).getDisplayData("CONSTRUCT_ID")));
			insertResultsDataToDatabase(
					ir.getReportData().get(ReportingConstants.RC_WG_E).getDisplayData("V2_PROJECT_ID"),
					ir.getReportData().get(ReportingConstants.RC_WG_E).getDisplayData("V2_CANDIDATE_ID"),
					ir.getReportData().get(ReportingConstants.RC_WG_E).getDisplayData("CONSTRUCT_ID"),
					ir.getReportData().get(ReportingConstants.RC_WG_E).getScoreData().get("WGE_CT"),	//"WGE_CT",
					resultsXML
					);
		} else if (ir.getReportCode().equals(ReportingConstants.RC_CAREER_SURVEY)) {
			//System.out.println(   ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().get("CAREER_GOALS"));
			//System.out.println(    ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().get("CAREER_DRIVERS"));
			//System.out.println(	ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().get("LEARNING_ORIENT"));
			//System.out.println(	ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().get("EXPERIENCE_ORIENT"));
			//WHATEVER THE CS SCORE IS WHEN IT IS SCORED
			// CS is scored like an Eval Guide and the data
			// should be in appropriate format at this point
			//
			// Change (9/2/10)... Pick up the raw score only... no norms, no norm score
			//
			//System.out.println("score = " + ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().get("CAREER_GOALS"));
			//resultsXML = buildResultsXML(ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().get("CAREER_GOALS"));
			resultsXML = buildResultsXML(ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().get("CAREER_GOALS_RAW"));
			//System.out.println("results xml = " + resultsXML);
			insertResultsDataToDatabase(
				ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getDisplayData("V2_PROJECT_ID"),
				ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getDisplayData("V2_CANDIDATE_ID"),
				ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getDisplayData("CONSTRUCT_ID_CG"),
				//ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().get("CAREER_GOALS"), 
				ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().get("CAREER_GOALS_RAW"), 
				resultsXML
				);
			//resultsXML = buildResultsXML(ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().get("CAREER_DRIVERS"));
			resultsXML = buildResultsXML(ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().get("CAREER_DRIVERS_RAW"));
			//System.out.println("results xml = " + resultsXML);
			insertResultsDataToDatabase(
				ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getDisplayData("V2_PROJECT_ID"),
				ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getDisplayData("V2_CANDIDATE_ID"),
				ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getDisplayData("CONSTRUCT_ID_CD"),
				//ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().get("CAREER_DRIVERS"), 
				ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().get("CAREER_DRIVERS_RAW"), 
				resultsXML
				);
			//resultsXML = buildResultsXML(ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().get("LEARNING_ORIENT"));
			resultsXML = buildResultsXML(ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().get("LEARNING_ORIENT_RAW"));
			//System.out.println("results xml = " + resultsXML);
			insertResultsDataToDatabase(
				ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getDisplayData("V2_PROJECT_ID"),
				ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getDisplayData("V2_CANDIDATE_ID"),
				ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getDisplayData("CONSTRUCT_ID_LO"),
				//ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().get("LEARNING_ORIENT"), 
				ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().get("LEARNING_ORIENT_RAW"), 
				resultsXML
				);
			//resultsXML = buildResultsXML(ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().get("EXPERIENCE_ORIENT"));
			resultsXML = buildResultsXML(ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().get("EXPERIENCE_ORIENT_RAW"));
			//System.out.println("results xml = " + resultsXML);
			insertResultsDataToDatabase(
				ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getDisplayData("V2_PROJECT_ID"),
				ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getDisplayData("V2_CANDIDATE_ID"),
				ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getDisplayData("CONSTRUCT_ID_EO"),
				//ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().get("EXPERIENCE_ORIENT"),
				ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().get("EXPERIENCE_ORIENT_RAW"),
				resultsXML
				);
		}
	}
	

	/**
	 * getNormsByConstruct gets all of the norms for a project and builds a norm
	 * for each, placing the norm in a Map keyed by the desired construct id.
	 *
	 * @param modId - the module id
	 * @return a HashMap of ArrayLists of Norms (empty map if no data)
	 * @throws Exception
	 */
	private HashMap<String, ArrayList<Norm>> getNormsByConstruct(String jobId, String modId)
	{
		HashMap<String, ArrayList<Norm>> ret = new HashMap<String, ArrayList<Norm>>();
		
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT jm.XML ");
		sb.append("  FROM job_mod jm ");
		sb.append("  WHERE jm.JobID = '" + jobId + "' ");
		sb.append("    AND jm.ModuleID = '" + modId + "'");
		
		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
		if (dlps.isInError())
		{
			//TODO When this method returns something, this should be modified
			System.out.println("Error preparing getNormsByConstruct norm names query." + dlps.toString());
			return ret;
		}

		DataResult dr = null;
		ResultSet rs = null;
		String normList = "";
		
		// Get the NormId tags
		try
		{
			dr = V2DatabaseUtils.select(dlps);
			if (dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
			{
				// No data to process
				return ret;
			}
			if (dr.isInError())
			{
			    System.out.println("Sql Exception : getNormsByConstruct -- select norm names : ");
			    System.out.println(dr.getStatus().toString());
				return ret;
			}
			
			rs = dr.getResultSet();
			if (! rs.isBeforeFirst())
			{
				// No data... scram
				return ret;
			}
			rs.next();	// Should be only the one; may have multiple norms (GP & SP for example)
			String xml = rs.getString("XML");
			
			DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
			Document doc = docBuilder.parse(new InputSource(new StringReader(xml)));
			NodeList normNodes = XMLUtils.getElements(doc, "//NormList/N");

			if (normNodes.getLength() < 1)
			{
				// nothing to do... just leave
				return ret;
			}
			
			for (int i=0; i < normNodes.getLength(); i++)
			{
				Node norm = normNodes.item(i);
				if (i > 0)
				{
					normList += ", ";
				}
				
				normList += "'" + XMLUtils.getAttributeValue(norm, "NormId") + "'";
			}	// End of norms loop
			
			dr.close();
			dr = null;
			rs = null;
		}
		catch(java.sql.SQLException se)
		{
		    System.out.println("Sql Exception : getNormsByConstruct -- select norm names : ");
		    System.out.println("message = " + se.getMessage());
		    System.out.println("  projectId = " + jobId + ",  "  +
		    				   ",  moduleId = " + modId);
		    return ret;
		}
		catch(Exception ex)
		{
		    System.out.println("Parse/doc handilng exception : getNormsByConstruct -- select norm names  : ");
		    System.out.println("message = " + ex.getMessage());
		    System.out.println("  projectId = " + jobId + ",  "  +
		    				   ",  moduleId = " + modId);
		    return ret;
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}		
		
		// Now get the norms
		dlps = null;
		dr = null;
		if (normList.length() < 1)
		{
			return ret;
		}
		sb.setLength(0);
		sb.append("SELECT nn.UniqueIdStamp as normId, ");
		sb.append("       nn.XML ");
		sb.append("  FROM norms nn ");
		sb.append("  WHERE nn.UniqueIdStamp in (" + normList + ")");
		
		dlps = V2DatabaseUtils.prepareStatement(sb);
		if (dlps.isInError())
		{
			//TODO When this method returns something, this should be modified
			System.out.println("Error preparing getNormsByConstruct norm data query." + dlps.toString());
			return ret;
		}

		try
		{
			dr = V2DatabaseUtils.select(dlps);
			if (dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
			{
				// No data to process
				return ret;
			}
			if (dr.isInError())
			{
			    System.out.println("Sql Exception : getNormsByConstruct -- select norms : ");
			    System.out.println(dr.getStatus().toString());
				return ret;
			}
			
			rs = dr.getResultSet();
			if (! rs.isBeforeFirst())
			{
				// No data... scram
				return ret;
			}
			while (rs.next())
			{
				String nId = rs.getString("normId");
				String xml = rs.getString("XML");
				
				// get the constructs and mean/stdv available in this norm
				DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
				DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
				Document doc = docBuilder.parse(new InputSource(new StringReader(xml)));
				NodeList cNodes = XMLUtils.getElements(doc, "//Constructs/C");

				if (cNodes.getLength() < 1)
				{
					// nothing to do... skip to next norm
					continue;
				}

				for (int i=0; i < cNodes.getLength(); i++)
				{
					Node con = cNodes.item(i);
					String conId = XMLUtils.getAttributeValue(con, "Id");
					String mean = XMLUtils.getAttributeValue(con, "Avg");
					String sd = XMLUtils.getAttributeValue(con, "StdDev");
					if (mean == null || mean.length() < 1 ||
						sd == null   || sd.length() < 1)
					{
						// Skip the construct... no valid norm
						continue;
					}
					Norm norm = new Norm(nId, Double.parseDouble(mean), Double.parseDouble(sd));
					ArrayList<Norm> curList = ret.get(conId);
					if (curList == null)
					{
						curList = new ArrayList<Norm>();
						ret.put(conId, curList);
					}
					curList.add(norm);
				}	// End of constructs loop
			}
			
			return ret;
		}
		catch(java.sql.SQLException se)
		{
		    System.out.println("Sql Exception : getNormsByConstruct -- select norms : ");
		    System.out.println("message = " + se.getMessage());
		    System.out.println("  projectId = " + jobId + ",  "  +
		    				   ",  moduleId = " + modId);
		    return ret;
		}
		catch(Exception ex)
		{
		    System.out.println("Parse/doc handilng exception : getNormsByConstruct -- select norms  : ");
		    System.out.println("message = " + ex.getMessage());
		    System.out.println("  projectId = " + jobId + ",  "  +
		    				   ",  moduleId = " + modId);
		    return ret;
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}		
	} 


	/**
	 * insertResultsDataToDatabase does the actual writes of data to 
	 * the results table in the V2 database.
	 *
	 * @param projectId - the project id of the data to be saved
	 * @param candidateId - the candidate id of the data to be saved
	 * @param constructId - the construct id of the data to be saved
	 * @param cscore - the raw score of the data to be saved
	 * @param resultsXML - the fully formed XML column data to be saved
	 * @throws Exception
	 */
	private void insertResultsDataToDatabase(String projectId, String candidateId, String constructId, String cscore, String resultsXML)
		throws Exception
	{
		StringBuffer sb = new StringBuffer();
		
		// need to see if this is going to be an insert or an
		// update! 		
		Boolean update = checkForResultsData(projectId,candidateId,constructId );
		
		uniqueIdStamp = this.buildHRADateAndUID(); // also sets the dateCreatedStamp variable internally
		
		if(update)
		{
			//System.out.println("UPDATE! ");
			//ConstructDTO cdto = null;
			sb.append("UPDATE results ");  					
			sb.append(" set  DateModifiedStamp = ?, ");  // 1
			sb.append(" ModifiedByStamp = ?,  ");  		 // 2
			sb.append(" XML = ?,  ");  					 // 3
			sb.append(" CScore = ? ");					 // 4
			sb.append(" WHERE CandidateId = ? ");  	     // 5
			sb.append(" AND constructId = ? ");  		 // 6
			sb.append(" AND  JobId = ?  ");  			 // 7
	
			DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
			if (dlps.isInError())
			{
				//TODO When this method returns something, this should be modified
				System.out.println("Error preparing writeAnswerDataToDatabase query." + dlps.toString());
				return;
			}

			DataResult dr = null;
			try
			{
				// Fill sql parmeters with values and get the data
				dlps.getPreparedStatement().setString(1, this.dateCreatedStamp);
				dlps.getPreparedStatement().setString(2, this.createdByStamp);
				dlps.getPreparedStatement().setString(3, resultsXML);
					
				dlps.getPreparedStatement().setString(4, cscore);
				
					
				dlps.getPreparedStatement().setString(5, candidateId);
				dlps.getPreparedStatement().setString(6, constructId);
				dlps.getPreparedStatement().setString(7, projectId);

				dr = V2DatabaseUtils.update(dlps);				
			}
			catch(java.sql.SQLException se)
			{
			    System.out.println("Sql Exception : writeResultsDataToDatabaseFromIR -- update data : ");
			    System.out.println("message = " + se.getMessage());
			    System.out.println("  candidateId = " + candidateId + 
			    			",  projectId = " + projectId + ",  "  +
			    			",  constructId = " + constructId);
			}
			finally
			{
				if (dr != null) { dr.close(); dr = null; }
			}		
		} else { // do insert
			sb.append("INSERT INTO results ");
			sb.append("(UniqueIdStamp, DateCreatedStamp, DateModifiedStamp, CreatedByStamp, ModifiedByStamp, ");
			sb.append(" CandidateId, ConstructId, JobId, CScore, XML) ");
			sb.append(" VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");

			DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
			if (dlps.isInError())
			{
				//TODO When this method returns something, this should be modified
				System.out.println("Error preparing writeAnswerDataToDatabase query." + dlps.toString());
				return;
			}

			DataResult dr = null;
			try
			{										
				dlps.getPreparedStatement().setString(1, this.uniqueIdStamp);
				dlps.getPreparedStatement().setString(2, this.dateCreatedStamp);
				dlps.getPreparedStatement().setString(3, this.dateCreatedStamp);
				dlps.getPreparedStatement().setString(4, this.createdByStamp);
				dlps.getPreparedStatement().setString(5, this.createdByStamp);
				dlps.getPreparedStatement().setString(6, candidateId);
				dlps.getPreparedStatement().setString(7, constructId);
				dlps.getPreparedStatement().setString(8, projectId);
				dlps.getPreparedStatement().setString(9, cscore);

				dlps.getPreparedStatement().setString(10, resultsXML);
					
				dr = V2DatabaseUtils.insert(dlps);					
			}
			catch(java.sql.SQLException se)
			{
			    System.out.println("Sql Exception : writeResultsDataToDatabaseFromIR -- insert data : ");
			    System.out.println("message = " + se.getMessage());
			    System.out.println("  candidateId = " + candidateId + 
			    			",  projectId = " + projectId + ",  "  +
			    			",  moduleId = " + constructId);
			}
			finally
			{
				if (dr != null) { dr.close(); dr = null; }
			}			
		}// end else
	}
}
