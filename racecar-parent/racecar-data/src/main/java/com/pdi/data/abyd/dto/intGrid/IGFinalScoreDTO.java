/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */

package com.pdi.data.abyd.dto.intGrid;


/**
* IGFinalScoreDTO is a helper class to allow us to pass the final score information
* to and from the server.  It is used in an array in DNAStructureDTO and individuals
* are passed back to the server to update the database.
* 
* @author		Ken Beukelman
*/
public class IGFinalScoreDTO
{
	//
	// Static data.
	//


	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _partId;
	private long _dnaId;
	private long _compId;
	private int _compScoreIdx;	// index for dropdown
	private String _compNote;

	//
	// Constructors.
	//
	public IGFinalScoreDTO()
	{
		// does nothing at present
	}

	//
	// Instance methods.
	//

	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public String getPartId()
	{
		return _partId;
	}

	public void setPartId(String value)
	{
		_partId = value;
	}	

	/*****************************************************************************************/
	public long getDnaId()
	{
		return _dnaId;
	}
	
	public void setDnaId(long value)
	{
		_dnaId = value;
	}	

	/*****************************************************************************************/
	public long getCompId()
	{
		return _compId;
	}
	
	public void setCompId(long value)
	{
		_compId = value;
	}	

	/*****************************************************************************************/
	public int getCompScoreIdx()
	{
		return _compScoreIdx;
	}
	
	public void setCompScoreIdx(int value)
	{
		_compScoreIdx = value;
	}	

	/*****************************************************************************************/
	public String getCompNote()
	{
		return _compNote;
	}

	public void setCompNote(String value)
	{
		_compNote = value;
	}	



	/*
	 * Show me what you've got
	 */
	public String toString()
	{
		String ret = "IGFinalScoreDTO content (Java)\n";
		ret += "   Participant ID=" + _partId + "\n";
		ret += "           DNA ID=" + _dnaId + "\n";
		ret += "    Competency ID=" + _compId + "\n";
		ret += "      Score Index=" + _compScoreIdx + "\n";
		ret += "  Competency Note=" + _compNote + "\n";
		
		return ret;
	}
}
