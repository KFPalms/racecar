package com.pdi.data.abyd.helpers.setup.ut.NoLongerUsed;

import java.sql.Connection;
import java.util.ArrayList;

import com.pdi.data.abyd.dto.common.CompElementDTO;
import com.pdi.data.abyd.helpers.setup.NoLongerUsed.SaveSPElementHelper;
import com.pdi.ut.UnitTestUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;

import junit.framework.TestCase;

public class SaveSPElementHelperUT extends TestCase
{
	//
	// Constructors
	//

	public SaveSPElementHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
		throws Exception
    {
		junit.textui.TestRunner.run(SaveSPElementHelperUT.class);
    }

	/*
	 * testSaveSPElement
	 * NOTE:  The functionality testing is incomplete; this merely tests updates.
	 *        For inserts we would need a longer array (to add new ones in) and
	 *        for deletes we would need a shorter array (delete happens when the
	 *        element is there in db but not in passed data)
	 */
	public void testSaveSPElement()
		throws Exception
	{
		System.out.println("SaveSPElementHelperUT:");
		
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);

		Connection con = AbyDDatabaseUtils.getDBConnection();
		long dnaId = 9;
		ArrayList<CompElementDTO> ary = new ArrayList<CompElementDTO>();
		long[] compAry = {508,509};
		
    	for (int i = 0; i < compAry.length; i++)
    	{
    		CompElementDTO ce = new CompElementDTO();
    		ce.setCompId(compAry[i]);
    		ce.setIsEssential(i == 0 ? false : true);
    		ary.add(ce);
    	}

		System.out.println("    DNA ID=" + dnaId);

    	SaveSPElementHelper helper = new SaveSPElementHelper(con, dnaId, ary);
		helper.saveSPElements();

		System.out.println("    No data returned.");

		UnitTestUtils.stop(this);
	}
}
