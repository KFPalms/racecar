package com.pdi.data.dto;

import java.util.ArrayList;

public class EmailTemplate {
	private long id;
	private String name;
	private String code;
	private ArrayList<EmailTemplateText> emailTemplateTexts;
	
	public void setId(long id) {
		this.id = id;
	}
	public long getId() {
		return id;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getCode() {
		return code;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setEmailTemplateTexts(ArrayList<EmailTemplateText> emailTemplateTexts) {
		this.emailTemplateTexts = emailTemplateTexts;
	}
	public ArrayList<EmailTemplateText> getEmailTemplateTexts() {
		return emailTemplateTexts;
	}
	
}
