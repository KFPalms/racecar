/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.dto;

import java.util.ArrayList;

import com.pdi.data.util.BaseDTO;
import com.pdi.data.util.RequestStatus;

/**
 * EmailGroupListDTO is a class designed to convey the information for a number of
 * email groups used in the PDI V3 extensions.  It contains an ArrayList of
 * EmailGroupDTO objects; the status in each of those may be redundant.
 * 
 * @author		Ken Beukelman
  */
public class EmailGroupListDTO extends BaseDTO
{
	//
	// Static data.
	//


	//
	// Static methods.
	//


	//
	// Instance data.
	//
	private ArrayList<EmailGroupDTO> _groupList = new ArrayList<EmailGroupDTO>();


	//
	// Constructors.
	//
	public EmailGroupListDTO()
	{
		super();
		// does nothing right now
	}
	
	// Set up a new DTO with status but no data
	public EmailGroupListDTO(RequestStatus stat)
	{
		super();
		this.setStatus(stat);
	}
	
	
	//
	// Instance methods.
	//

	/**
	 * Generic toString method
	 */
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append("EmailGroupListDTO:  ");
		sb.append("Status code=" + this.getStatus().getStatusCode() + ", count=" + _groupList.size());
		for(EmailGroupDTO group : _groupList)
		{
			sb.append("\n      id=" + group.getId());
			sb.append(", name=>" + group.getName() + "<");
			sb.append(", description=" + group.getName());
		}
		
		return sb.toString();
	}
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public ArrayList<EmailGroupDTO> getGroupList()
	{
		return _groupList;
	}
	
	public void setGroupList(ArrayList<EmailGroupDTO> value)
	{
		_groupList = value;
	}	
}
