package com.pdi.data.v3.helpers.ut;

import com.pdi.data.v3.dto.CandidateDTO;
import com.pdi.data.v3.dto.CandidateListDTO;
import com.pdi.data.v3.dto.SessionUserDTO;
import com.pdi.data.v3.helpers.CandidateDataHelper;
import com.pdi.properties.PropertyLoader;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

public class CandidateDataHelperUT extends TestCase
{
	public CandidateDataHelperUT(String name)
	{
		super(name);
	} 
    
    
    public void testProperties()
    	throws Exception
    {
    	UnitTestUtils.start(this);
    	//Does the local property work?
    	String applicationId = PropertyLoader.getProperty("com.pdi.data.v3.application", "application.code");
    	assertEquals(applicationId != null, true);
    	assertEquals(applicationId.equals("PDI-DATA-V3"), true);
    	UnitTestUtils.stop(this);
    }
    

    
    /*
     * Tests the default List Candidates
     * 
     */
    public void testGetCandidates()
    	throws Exception
    {
    	UnitTestUtils.start(this);
    	/*
    	System.out.println("HELLO WORLD!");
    	SessionUserDTO sessionUser = new SessionUserDTO();
    	sessionUser.setUsername(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.username")); 
    	sessionUser.setPassword(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.password"));

    	ArrayList<CandidateDTO> candidates = CandidateDataHelper.getCandiates(sessionUser);
    	for(CandidateDTO candidate : candidates) {
    		System.out.println(candidate.toString());
    	}
		//System.out.println("UT testCandidateList " + response);
    	//assertEquals(response != null, true);
    	*/
    	
    	
    	SessionUserDTO sessionUser = new SessionUserDTO();
    	sessionUser.setUsername(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.username")); 
    	sessionUser.setPassword(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.password"));

    	CandidateListDTO candidates = CandidateDataHelper.getCandidates(sessionUser);
    	for(CandidateDTO candidate : candidates.getCandidateList())
    	{
    		System.out.println("-----------------------");
    		System.out.println(candidate.toString());
    	}
    	
    	UnitTestUtils.stop(this);
    }

    
    /*
     * Tests the a single Candidates get
     * 
     */
    public void testGetCandidate()
    	throws Exception
    {
    	UnitTestUtils.start(this);
     	
    	SessionUserDTO sessionUser = new SessionUserDTO();
    	sessionUser.setUsername(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.username")); 
    	sessionUser.setPassword(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.password"));

		System.out.println("-----------------  Testing getCandidate (id input)  -----------------");
    	CandidateDTO candidate = CandidateDataHelper.getCandidate(sessionUser, "CJJO-WIBY");
   		System.out.println(candidate.toString());
   		
   		candidate = null;
   		System.out.println("-----------------  Testing getCandidate (uri input)  -----------------");
   		CandidateDTO inp = new CandidateDTO();
   		inp.setUri("https://platform.iscopia.com/webservices1/rest/v1/candidate/CNFE-UWVF");
    	candidate = CandidateDataHelper.getCandidate(sessionUser, inp);
   		System.out.println(candidate.toString());
    	
    	UnitTestUtils.stop(this);
    }

    
    public static void main(String[] args)
    	throws Exception
    {

        junit.textui.TestRunner.run(CandidateDataHelperUT.class);
    }
}