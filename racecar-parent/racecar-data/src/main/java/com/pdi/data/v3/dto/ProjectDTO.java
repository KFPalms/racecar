/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.pdi.data.util.BaseDTO;
import com.pdi.data.util.RequestStatus;
import com.pdi.xml.XMLUtils;

//import javax.swing.text.html.HTMLDocument.Iterator;
//
//import org.w3c.dom.Node;
//
//import com.pdi.data.util.RequestStatus;
//import com.pdi.data.v3.helpers.ProjectDataHelper;
//import com.pdi.xml.XMLUtils;

/**
 * DTO for project information
 * 
 * NOTE:  this clas does not conform to the usual proxy/filled model in that
 * 		  even the proxy has data in the participant
 * 
 * @author		Gavin Myers
 * @author		Ken Beukelman
 */
public class ProjectDTO extends BaseDTO
{
	//
	//
	// Static data.
	//	
	// These constants are not replicated on the Flex side
	// as the data they represent is not exposed there
	public static final String TRANSITION_TYPE_KEY = "type.transition.t_pdi_dev";
	public static final String TRANSITION_CURRENT_LEVEL_KEY = "current.level.transition.t_pdi_dev";
	public static final String TRANSITION_TARGET_LEVEL_KEY = "target.level.transition.t_pdi_dev";

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	
	// mb added _customFields:
	//    used for project fields like transition level, transition level labels, business unit
	//Keith added _selected:
	//    Used in CandidateDTO
	private String _id;
	private String _name;
	private ClientDTO _client;
	private String _supplierId;
	private String _supplierName;
	private String _uri;
	private ItemStatusDTO _itemStatus;
	private String _version;
	private String _productId;
	private String _resultsUri;
	private String _answersUri;
	private String _activityId;
	private String _transitionLevel;
	private String _transitionLevelName;
	private String _TPOTTransLevelCurrent;
	private String _TPOTTransLevelTarget;
	
	private HashMap<String, String> _customFields = new HashMap<String,String>();
	private ArrayList<ParticipantDTO> _participantList = null;
	private int _participantCount = 0;
	private int _participantCompleteCount = 0;	// Not available in release 1.0
	
	private boolean _selected = false;

	
	//
	// Constructors.
	//

	/**
	 * "Standard" (no-parameter) constructor
	 */
	public ProjectDTO()
	{
		super();
		// Does nothing presently
	}
	
	/**
	 * Builds the object based off an xml node
	 * @param node
	 * @throws Exception
	 */
	public ProjectDTO(Node node)
	{	
		// We could be sending a list node or a result node.  Check for both.
		/*
		 * From the Project list, the XML looks like this:
		 * <ns2:projectListLine>
		 *   <identifier>PWDQ-KPQN</identifier>
		 *   <name>GRD Talentview</name>
		 *   <clientName>Get R Dunn Prod</clientName>
		 *   <supplierName>T_PDI_dev</supplierName>
		 *   <uri>https://platform.iscopia.com/webservices1/rest/v1/project/PWDQ-KPQN</uri>
		 * </ns2:projectListLine>
		 * 
		 * Note the the URI is unique here and tells us if we are filling a proxy or the real McCoy
		 * 
		 * From the project uri (result node), the XML looks like this:
		 * <ns3:resultProject>
		 *   <identifier>PWDQ-KPQN</identifier>
		 *   <name>GRD Talentview</name>
		 *   <clientIdentifier>OKLU-MJGG</clientIdentifier>
		 *   <supplierIdentifier>OZUN-PUAX</supplierIdentifier>
		 *   <status>ACTIVE</status>
		 *   <version>0</version>
		 *   <ns2:customFields>
		 *     <ns2:customField>
		 *       <code>type.transition.t_pdi_dev</code>
		 *       <type>PICKLIST</type>
		 *       <required>0</required>
		 *     </ns2:customField>
		 *     <ns2:customField>
		 *       <code>current.level.transition.t_pdi_dev</code>
		 *       <type>STRING</type>
		 *       <required>0</required>
		 *     </ns2:customField>
		 *   </ns2:customFields>
		 *   <ns3:activityWorkflow>
		 *     <ns2:activity>
		 *       <identifier>13f5fcdd-7f0c-4b5f-96af-a92d4c010086</identifier>
		 *       <type>ASSESS</type>
		 *       <productIdentifier>4F97F776-A007-4387-8146-A5386D6BEFCC</productIdentifier>
		 *       <results>https://platform.iscopia.com/webservices1/rest/v1/project/PWDQ-KPQN/participant/results/13f5fcdd-7f0c-4b5f-96af-a92d4c010086</results>
		 *       <answers>https://platform.iscopia.com/webservices1/rest/v1/project/PWDQ-KPQN/participant/answers/13f5fcdd-7f0c-4b5f-96af-a92d4c010086</answers>
		 *       <ns2:reportList/>
		 *     </ns2:activity>
		 *   </ns3:activityWorkflow>
		 * </ns3:resultProject>
		 */
		
		super();
		
		// This works, because the project list has proxy data only plus the URI.  If the
		// URI attribute is present, the Node must be from the list and contains proxy data only.
		String uri = XMLUtils.getElementValue(node, "uri/.");
		this.setProxy((uri == null ? FILLED : PROXY));
		
		this.setId(XMLUtils.getElementValue(node, "identifier/."));
		this.setName(XMLUtils.getElementValue(node, "name/."));
		if (noClient())
		{
			this.setClient(new ClientDTO());
		}
		this.getClient().setId(XMLUtils.getElementValue(node, "clientIdentifier/."));
		this.getClient().setName(XMLUtils.getElementValue(node, "clientName/."));
		this.setSupplierId(XMLUtils.getElementValue(node, "supplierIdentifier/."));
		this.setSupplierName(XMLUtils.getElementValue(node, "supplierName/."));
		this.setUri(uri);
		this.setItemStatus(new ItemStatusDTO(node));
		this.setVersion(XMLUtils.getElementValue(node, "version/."));
		this.setProductId(XMLUtils.getElementValue(node, "activityWorkflow/activity/productIdentifier/."));
		this.setActivityId(XMLUtils.getElementValue(node, "activityWorkflow/activity/identifier/."));
		
		this.setResultsUri(XMLUtils.getElementValue(node, "activityWorkflow/activity/results/."));
		this.setAnswersUri(XMLUtils.getElementValue(node, "activityWorkflow/activity/answers/."));
		
		// Get the custom fields
		NodeList nodes = XMLUtils.getElements(node, "//resultProject/customFields/customField");			
		if(nodes != null)
		{
			HashMap<String, String> cf = new HashMap<String, String>();				
			// Put the custom fields into the list, but not if they are empty
			for(int i = 0; i < nodes.getLength(); i++)
			{			
				Node node2 = nodes.item(i);
				String key = XMLUtils.getElementValue(node2, "code/.");
				String val = XMLUtils.getElementValue(node2, "value/.");
				cf.put(key,val);
			}
			
			this.setCustomFields(cf);
		}
		
		// If you want the participants for a project, call ParticipantDataHelper.getParticipants
		// from the project data helper

		// check for data
		if (this.isProxy())
		{
			if (this.getId() == null  &&
				this.getName() == null  &&
				(this.getClient() == null || this.getClient().getName() == null)  &&
				this.getSupplierName() == null  &&
				this.getUri() == null)
			{
				this.getStatus().setStatusCode(RequestStatus.WB_NO_DATA);
			}
		}
		else
		{
			// This is not a proxy project 
			if (this.getId() == null &&
				this.getName() == null &&
				(this.getClient() == null || this.getClient().getId() == null)  &&
				this.getSupplierId() == null &&
				(this.getItemStatus().getStatus().getStatusCode() == RequestStatus.WB_NO_DATA) &&
				this.getVersion() == null &&
				this.getProductId() == null  &&
				(this.getCustomFields() == null || this.getCustomFields().size() < 1))
			{
				this.getStatus().setStatusCode(RequestStatus.WB_NO_DATA);
			}
		}
		
		return;
	}


	//
	// Instance methods.
	//
//
//	/*
//	 * 
//	 */
//	private ArrayList<ParticipantDTO> getMyParticipantProxies()
//	{
//		DocumentDTO dto = V3WebserviceUtils.request(sessionUser,"project/"+project.getId()+"/participant/list","");
//		if (! dto.getStatus().isOk())
//		{
//			return new ParticipantListDTO(dto.getStatus());
//		}
//
//		Document response = dto.getDocument();
//		NodeList nodes = XMLUtils.getElements(response, "//participantListLine");
//
//		ArrayList<ParticipantDTO> results = new ArrayList<ParticipantDTO>();
//		for(int i = 0; i < nodes.getLength(); i++) {
//			Node node = nodes.item(i);
//			ParticipantDTO participant = new ParticipantDTO(node);
//			results.add(participant);
//		}
//		
//	}

	/**
	 * Generic toString method
	 */
	public String toString()
	{
		String str = "\n --- ProjectDTO --- ";
		str += this.getStatus().toString();
		str += "\nProxy:               " + this.getProxy();
		str += "\nid:                  " + this.getId();
		str += "\nName:                " + this.getName();
		str += this.getClient().toString();
		str += "\nSupplier ID:         " + this.getSupplierId();
		str += "\nSupplier Name:       " + this.getSupplierName();
		str += "\nURI:                 " + this.getUri();
		str += this.getItemStatus().toString();
		str += "\nVersion:            " + this.getVersion();
		str += "\nProduct ID:         " + this.getProductId();
		str += "\nCustomFields ("+ this.getCustomFields().size() + " entries):";
		for (Iterator<Map.Entry<String, String>> itr=this.getCustomFields().entrySet().iterator(); itr.hasNext(); )
		{
			Map.Entry<String, String> ent = itr.next();
			str +="\n    " + ent.getKey() + " = " + ent.getValue();
		}
		str += "\nParticipant cnt:    " + this.getParticipantCount();
////		str += "\nPart Complete cnt:  " + this.getParticipantCompleteCount();
		if (this.getParticipantList() == null)
		{
			str += "Participant List is not initialized";
		}
		else
		{
			str += "\nParticipant Array (" + this.getParticipantList().size() + " entries):";
			for (int i = 0; i < this.getParticipantList().size(); i++)
			{
				this.getParticipantList().get(i).toString();
			}
		}
		str += "\nSelected:      " + this.getSelected();

		return str;
	}

	/**
	 * Generic toString method
	 */
	private boolean noClient()
	{
		return (this.getClient() == null);
	}


	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
	
	
	/*****************************************************************************************/
	public void setId(String value)
	{
		_id = value;
	}
	
	public String getId()
	{
		return _id;
	}
	
	/*****************************************************************************************/
	public void setName(String value)
	{
		_name = value;
	}
	
	public String getName()
	{
		return _name;
	}
	
	/*****************************************************************************************/
	public void setClient(ClientDTO value)
	{
		_client = value;
	}
	
	public ClientDTO getClient()
	{
		return _client;
	}
	
	/*****************************************************************************************/
	public void setSupplierId(String value)
	{
		_supplierId = value;
	}
	
	public String getSupplierId()
	{
		return _supplierId;
	}
	
	/*****************************************************************************************/
	public void setSupplierName(String value)
	{
		_supplierName = value;
	}
	
	public String getSupplierName()
	{
		return _supplierName;
	}
	
	/*****************************************************************************************/
	public void setUri(String value)
	{
		_uri = value;
	}
	
	public String getUri()
	{
		return _uri;
	}
	
	/*****************************************************************************************/
	public void setItemStatus(ItemStatusDTO value)
	{
		_itemStatus = value;
	}
	
	public ItemStatusDTO getItemStatus()
	{
		return _itemStatus;
	}
	
	/*****************************************************************************************/
	public void setVersion(String value)
	{
		_version = value;
	}
	
	public String getVersion()
	{
		return _version;
	}
	
	/*****************************************************************************************/
	public void setProductId(String value)
	{
		_productId = value;
	}
	
	public String getProductId()
	{
		return _productId;
	}
	
	/*****************************************************************************************/
	public void setResultsUri(String value)
	{
		_resultsUri = value;
	}
	
	public String getResultsUri()
	{
		return _resultsUri;
	}
	
	/*****************************************************************************************/
	public void setAnswersUri(String value)
	{
		_answersUri = value;
	}
	
	public String getAnwersUri()
	{
		return _answersUri;
	}
	
	/*****************************************************************************************/
	public void setCustomFields(HashMap<String, String> value)
	{
		_customFields = value;
	}
	
	public HashMap<String, String> getCustomFields()
	{
		return _customFields;
	}
	
	/*****************************************************************************************/
	public void setParticipantList(ArrayList<ParticipantDTO> value)
	{
		_participantList = value;
	}
	
	public ArrayList<ParticipantDTO> getParticipantList()
	{
		return _participantList;
	}

	/*****************************************************************************************/
	public void setParticipantCount(int value)
	{
		_participantCount = value;
	}

	public int getParticipantCount()
	{
		return _participantCount;
	}

	/*****************************************************************************************/
	public void setParticipantCompleteCount(int value)
	{
		_participantCompleteCount = value;
	}

	public int getParticipantCompleteCount()
	{
		return _participantCompleteCount;
	}

	/*****************************************************************************************/
	public void setSelected(boolean value)
	{
		_selected = value;
	}

	public boolean getSelected()
	{
		return _selected;
	}

	public void setActivityId(String _activityId) {
		this._activityId = _activityId;
	}

	public String getActivityId() {
		return _activityId;
	}

	/*****************************************************************************************/
	public void setTransitionLevel(String value)
	{
		_transitionLevel = value;
	}
	
	public String getTransitionLevel()
	{
		return _transitionLevel;
	}
	
	/*****************************************************************************************/
	public void setTPOTTransLevelCurrent(String value)
	{
		_TPOTTransLevelCurrent = value;
	}
	
	public String getTPOTTransLevelCurrent()
	{
		return _TPOTTransLevelCurrent;
	}
	
	/*****************************************************************************************/
	public void setTPOTTransLevelTarget(String value)
	{
		_TPOTTransLevelTarget = value;
	}
	
	public String getTPOTTransLevelTarget()
	{
		return _TPOTTransLevelTarget;
	}

	public void setTransitionLevelName(String _transitionLevelName) {
		this._transitionLevelName = _transitionLevelName;
	}

	public String getTransitionLevelName() {
		return _transitionLevelName;
	}	
	
	
//	/////////////////////
//	//  Original Code  //
//	/////////////////////
//	//
//	// Static data.
//	//
//
//	//
//	// Static methods.
//	//
//
//	//
//	// Instance data.
//	//
//	private String _id;
//	private String _supplierId;
//	private String _name;
//	private ClientDTO _client;
//	private ItemStatusDTO _itemStatus;
//	private String _version;
//	private String _projectURI;  // mb added -- also adding method in projecthelper to fill the product 
//	private ProductDTO _product;   
//	private boolean _selected = false;	//Keith added selected + in CandidateDTO
//
//	// mb added:  used for project fields like transition level, transition level labels, business unit
//	private HashMap<String, String> _customFields = new HashMap<String,String>(); 
//	
//	private ArrayList<ParticipantDTO> _participants = new ArrayList<ParticipantDTO>();
//	
//	//
//	// Constructors.
//	//
//
//	/**
//	 * "Standard" (no-parameter) constructor
//	 */
//	public ProjectDTO()
//	{
//		super();
//		// Does nothing presently
//	}
//	
//	/**
//	 * Builds the object based off an xml node
//	 * @param node
//	 * @throws Exception
//	 */
//	public ProjectDTO(Node node)
//	{
//		/*
//		 * The XML looks something like this:
//		 * <ns2:resultProject xmlns:ns2="urn:iscopia.com:project-1.0">
//		 *   <identifier>PBJN-ZVOM</identifier>
//		 *   <name>QA_VM-067-FX</name>
//		 *   <clientIdentifier>OAIL-HLSC</clientIdentifier>
//		 *   <supplierIdentifier>OAIL-HLSC</supplierIdentifier>
//		 *   <status>ACTIVE</status>
//		 *   <version>0</version>
//		 * </ns2:resultProject>
//		 */
//		
//		super();
//		
//		this.setId(XMLUtils.getElementValue(node, "identifier/."));
//		this.setName(XMLUtils.getElementValue(node, "name/."));
//		this.setProjectURI(XMLUtils.getElementValue(node, "uri/."));
//		
//		this.setProduct(ProjectDataHelper.getProductFromProject(this.getProjectURI()));
//		
//		// Rework this:  Create the client and fill it in (not using a node)
//		////		this.setClient(new ClientDTO(node));		
//		this.setItemStatus(new ItemStatusDTO(node));
//		
//		// check for data
//		if (this.getId() == null &&
//			this.getName() == null &&
//			this.getProjectURI() == null &&
//			this.getClient().getStatus().getStatusCode() == RequestStatus.WB_NO_DATA &&
//			this.getItemStatus().getStatus().getStatusCode() == RequestStatus.WB_NO_DATA)
//		{
//			this.getStatus().setStatusCode(RequestStatus.WB_NO_DATA);
//		}
//}
//
//	//
//	// Instance methods.
//	//
//	
//	/**
//	 * Generic toString method
//	 */
//	public String toString() {
//		String str = "\n --- ProjectDTO --- ";
//		str += this.getStatus().toString();
//		str += "\nid:            " + this.getId();
//		str += "\nName:          " + this.getName();
//		str += this.getClass().toString();
//		str += this.getItemStatus().toString();
//		str += "\nVersion:       " + this.getVersion();
//		str += "\n Participant Array goes here";
//		return str;
//	}
//	
//	//*******************************************************************************************//
//	//																							 //
//	//                                       Get/Set Functions                                   //
//	//																							 //
//	//*******************************************************************************************//
//	
//	/*****************************************************************************************/
//	public void setId(String value)
//	{
//		_id = value;
//	}
//	
//	public String getId()
//	{
//		return _id;
//	}
//	
//	/*****************************************************************************************/
//	public void setSupplierId(String value)
//	{
//		_supplierId = value;
//	}
//	
//	public String getSupplierId()
//	{
//		return _supplierId;
//	}
//	
//	/*****************************************************************************************/
//	public void setName(String value)
//	{
//		_name = value;
//	}
//	
//	public String getName()
//	{
//		return _name;
//	}
//	
//	/*****************************************************************************************/
//	public void setClient(ClientDTO value)
//	{
//		_client = value;
//	}
//	
//	public ClientDTO getClient()
//	{
//		return _client;
//	}
//	
//	/*****************************************************************************************/
//	public void setItemStatus(ItemStatusDTO value)
//	{
//		_itemStatus = value;
//	}
//	
//	public ItemStatusDTO getItemStatus()
//	{
//		return _itemStatus;
//	}
//	
//	/*****************************************************************************************/
//	public void setVersion(String value)
//	{
//		_version = value;
//	}
//	
//	public String getVersion()
//	{
//		return _version;
//	}
//
//	/*****************************************************************************************/
//	public void setProjectURI(String value)
//	{
//		_projectURI = value;
//	}
//	
//	public String getProjectURI()
//	{
//		return _projectURI;
//	}
//	
//	/*****************************************************************************************/
//	public void setProduct(ProductDTO value)
//	{
//		_product = value;
//	}
//
//	public ProductDTO getProduct()
//	{
//		return _product;
//	}
//	
//	/*****************************************************************************************/
//	public void setSelected(boolean value)
//	{
//		_selected = value;
//	}
//
//	public boolean getSelected()
//	{
//		return _selected;
//	}
//
//	/*****************************************************************************************/
//	public void setCustomFields(HashMap<String, String> value)
//	{
//		_customFields = value;
//	}
//	
//	public HashMap<String, String> getCustomFields()
//	{
//		return _customFields;
//	}
//	
//	/*****************************************************************************************/
//	public void setParticipants(ArrayList<ParticipantDTO> value)
//	{
//		_participants = value;
//	}
//	
//	public ArrayList<ParticipantDTO> getParticipants()
//	{
//		return _participants;
//	}
}
