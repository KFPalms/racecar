/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */

package com.pdi.data.abyd.dto.intGrid;

import java.util.ArrayList;
import java.util.Map;

import com.pdi.data.abyd.dto.common.DNACellDTO;

/**
* IGIntersectionAndOtherDataDTO is a helper class to allow us to pass DNA cell scores,
* instrument norm names, and the RDI.  For internal (server-side) use only.
* 
* NOTE:  This is currently used only on the server side.
*        NO DATA IS PASSED TO THE CLIENT SIDE WITH THIS DTO>
* 
* @author		Ken Beukelman
*/
public class IGIntersectionAndOtherDataDTO
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private Map<String, String> _normNameMap;	// key = spss key, value is norm name
	private ArrayList<DNACellDTO> _testingCells;
	private int _rdi;

	//
	// Constructors.
	//
	public IGIntersectionAndOtherDataDTO()
	{
		// does nothing at present
	}

	//
	// Instance methods.
	//

	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public Map<String, String> getNormNameMap()
	{
		return _normNameMap;
	}

	public void setNormNameMap(Map<String, String> value)
	{
		_normNameMap = value;
	}	

	/*****************************************************************************************/
	public ArrayList<DNACellDTO> getTestingCells()
	{
		return _testingCells;
	}

	public void setTestingCells(ArrayList<DNACellDTO> value)
	{
		_testingCells = value;
	}	

	/*****************************************************************************************/
	public int getRdi()
	{
		return _rdi;
	}

	public void setRdi(int value)
	{
		_rdi = value;
	}	
}
