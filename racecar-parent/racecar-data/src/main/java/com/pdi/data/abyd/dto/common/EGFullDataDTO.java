/**
 * Copyright (c) 2009, 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */

package com.pdi.data.abyd.dto.common;

import java.util.ArrayList;

import com.pdi.data.abyd.dto.common.EGCompDisplayData;
import com.pdi.data.abyd.dto.common.EGIrDisplayData;
import com.pdi.data.abyd.dto.common.EGOverallData;
import com.pdi.data.abyd.dto.common.EGRaterNameData;
import com.pdi.data.util.language.LanguageItemDTO;


/**
 * EGFullDataDTO is a thin class containing all of the data needed to create an Eval Guide.
 * 
 * @author		Ken Beukelman
 */
public class EGFullDataDTO
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
		// The primary key for much of the data
	private String _participantId;					// The participant ID as a String
	private long _dnaId;							// The internal id of the DNA
	private long _moduleId;			  				// The internal id of the module

		// Header info
	private String _modelName;						// The name of the model used in the project/DNA
	private String _simName;						// The name of the module/sim
	private String _partName;						// The name of the participant in "last, first" format (V2)
	private String _clientName;						// The name of the client associated with the project
	private String _jobName;						// The name of the job (from DNA)
	private boolean _allowCso;						// The flag for allowing Comp Scoring Overrides

		// Page 1 info (consultant name)
	private EGRaterNameData _raterNameData;			// Consultant name data

		// Competency page data (multiple)
	private ArrayList<EGCompDisplayData> _compArray;		// Array of comp data objects

		// Importance Ratings
	private String _irNotes;						// Consultant entered notes about Importance Ratings
	private ArrayList<EGIrDisplayData> _irArray;	// Importance Rating display items

		// Overall data
	private EGOverallData _overallData;				// EG overall data
	private boolean _submitted = false;				// The 'submitted' flag for the eval guide
	private String _specialType;					// The flag indicating this module requires special processing

		// Language data
	private LanguageItemDTO _simLangItem;			// The language selected for this sim
	private String _simLangOther;					// The language name if not on the list
	private ArrayList<LanguageItemDTO> _langList;	// The list of languages to display
		
		// Delivery Method data
	private int _deliveryMethodId;
	private ArrayList<DeliveryMethodDTO> _methodList;
	
		// flags
	// TODO Re-factor (?) - Something like a single flag and a String with the type
	private boolean _iblva = false;					// Denotes that this is an iblva type (GL-GPS)with completed data
	private boolean _nis = false;					// Denotes that this is an nis type (Novartis) with completed data
	private boolean _ismll = false;					// Denotes that this is an ismll type (legacy port of MLL) with completed data
	private boolean _isbul = false;					// Denotes that this is an isbul type (legacy port of BUL) with completed data
	private boolean _isz = false;					// Denotes that this is an isz type (MLL Zenio) with completed data
	private boolean _eis = false;					// Denotes that this is an eis type (Ecolog) with completed data
	private boolean _bre2 = false;					// Denotes that this is a bre2 type (BRE presented in Inbox logic) with completed data
	private boolean _bresea = false;				// Denotes that this is a bresea type (BRE presented in Inbox logic) with completed data
	private boolean _bremed = false;				// Denotes that this is a bremed type (BRE presented in Inbox logic) with completed data
	private boolean _sfeceo = false;				// Denotes that this is a sfeceo type (BRE presented in Inbox logic) with completed data
	private String _crType = null;					// Content Review type
	private boolean _hasCrc = false;				// Flag indicating that there is a CRC involved (this is an Inbox)

	// Roxcart specific data - No Oxcart functionality
	// TODO Remove this as Roxcart has been replaced by scorekeeper
	private ArrayList<Object> _savedTabs = new ArrayList<Object>();
	//
	// Constructors.
	//
	public EGFullDataDTO()
	{
		// Does nothing presentlyS
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	// -----------------> Key fields <-----------------
	/*****************************************************************************************/
	public String getParticipantId()
	{
		return _participantId;
	}
		
	public void setParticipantId(String value)
	{
		_participantId = value;
	}

	/*****************************************************************************************/
	public long getDnaId()
	{
		return _dnaId;
	}
		
	public void setDnaId(long value)
	{
		_dnaId = value;
	}

	/*****************************************************************************************/
	public long getModuleId()
	{
		return _moduleId;
	}
		
	public void setModuleId(long value)
	{
		_moduleId = value;
	}

	
	// -----------------> Header info <-----------------
	/*****************************************************************************************/
	public String getModelName()
	{
		return _modelName;
	}
		
	public void setModelName(String value)
	{
		_modelName = value;
	}

	/*****************************************************************************************/
	public String getSimName()
	{
		return _simName;
	}
		
	public void setSimName(String value)
	{
		_simName = value;
	}

	/*****************************************************************************************/
	public String getPartName()
	{
		return _partName;
	}
		
	public void setPartName(String value)
	{
		_partName = value;
	}

	/*****************************************************************************************/
	public String getClientName()
	{
		return _clientName;
	}
		
	public void setClientName(String value)
	{
		_clientName = value;
	}

	/*****************************************************************************************/
	public String getJobName()
	{
		return _jobName;
	}
		
	public void setJobName(String value)
	{
		_jobName = value;
	}

	/*****************************************************************************************/
	public boolean getAllowCso()
	{
		return _allowCso;
	}
		
	public void setAllowCso(boolean value)
	{
		_allowCso = value;
	}

	// -----------------> Page 1 info (consultant name) <-----------------
	/*****************************************************************************************/
	public EGRaterNameData getRaterNameData()
	{
		return _raterNameData;
	}
		
	public void setRaterNameData(EGRaterNameData value)
	{
		_raterNameData = value;
	}

	
	// -----------------> Competency page data (multiple) <-----------------

	/*****************************************************************************************/
	public ArrayList<EGCompDisplayData> getCompArray()
	{
		return _compArray;
	}
		
	public void setCompArray(ArrayList<EGCompDisplayData> value)
	{
		_compArray = value;
	}

	
	// -----------------> Importance Ratings <-----------------
	/*****************************************************************************************/
	public String getIrNotes()
	{
		return _irNotes;
	}
		
	public void setIrNotes(String value)
	{
		_irNotes = value;
	}

	/*****************************************************************************************/
	public ArrayList<EGIrDisplayData> getIrArray()
	{
		return _irArray;
	}
		
	public void setIrArray(ArrayList<EGIrDisplayData> value)
	{
		_irArray = value;
	}

	
	// -----------------> Overall data <-----------------
	/*****************************************************************************************/
	public EGOverallData getOverallData()
	{
		return _overallData;
	}
		
	public void setOverallData(EGOverallData value)
	{
		_overallData = value;
	}

	/*****************************************************************************************/
	public boolean getSubmitted()
	{
		return _submitted;
	}
		
	public void setSubmitted(boolean value)
	{
		_submitted = value;
	}

	/*****************************************************************************************/
	public String getSpecialType()
	{
		return _specialType;
	}
		
	public void setSpecialType(String value)
	{
		_specialType = value;
	}

	
	// -----------------> Language data <-----------------
	/*****************************************************************************************/
	public LanguageItemDTO getSimLangItem()
	{
		return _simLangItem;
	}
		
	public void setSimLangItem(LanguageItemDTO value)
	{
		_simLangItem = value;
	}

	/*****************************************************************************************/
	public String getSimLangOther()
	{
		return _simLangOther;
	}
		
	public void setSimLangOther(String value)
	{
		_simLangOther = value;
	}

	/*****************************************************************************************/
	public ArrayList<LanguageItemDTO> getLangList()
	{
		if (_langList == null)
			_langList = new ArrayList<LanguageItemDTO>();
		return _langList;
	}
		
	public void setLangList(ArrayList<LanguageItemDTO> value)
	{
		_langList = value;
	}
	/*****************************************************************************************/

	public int getDeliveryMethodId() {
		return _deliveryMethodId;
	}

	
	public void setDeliveryMethodId(int _deliveryMethodId) {
		this._deliveryMethodId = _deliveryMethodId;
	}

	/*****************************************************************************************/

	public ArrayList<DeliveryMethodDTO> getMethodList() {
		return _methodList;
	}


	public void setMethodList(ArrayList<DeliveryMethodDTO> _methodList) {
		this._methodList = _methodList;
	}

	// -----------------> Flags <-----------------
	/*****************************************************************************************/
	public boolean getIblva()
	{
		return _iblva;
	}
		
	public void setIblva(boolean value)
	{
		_iblva = value;
	}

	/*****************************************************************************************/
	public boolean getNis()
	{
		return _nis;
	}
		
	public void setNis(boolean value)
	{
		_nis = value;
	}

	/*****************************************************************************************/
	public boolean getIsmll()
	{
		return _ismll;
	}
		
	public void setIsmll(boolean value)
	{
		_ismll = value;
	}

	/*****************************************************************************************/
	public boolean getIsbul()
	{
		return _isbul;
	}
		
	public void setIsbul(boolean value)
	{
		_isbul = value;
	}

	/*****************************************************************************************/
	public boolean getIsz()
	{
		return _isz;
	}
		
	public void setIsz(boolean value)
	{
		_isz = value;
	}

	/*****************************************************************************************/
	public boolean getEis()
	{
		return _eis;
	}
		
	public void setEis(boolean value)
	{
		_eis = value;
	}

	/*****************************************************************************************/
	public boolean getBre2()
	{
		return _bre2;
	}
		
	public void setBre2(boolean value)
	{
		_bre2 = value;
	}

	/*****************************************************************************************/
	public boolean getBresea()
	{
		return _bresea;
	}
		
	public void setBresea(boolean value)
	{
		_bresea = value;
	}

	/*****************************************************************************************/
	public boolean getSfeceo()
	{
		return _sfeceo;
	}
		
	public void setSfeceo(boolean value)
	{
		_sfeceo = value;
	}

	/*****************************************************************************************/
	public String getCrType()
	{
		return this._crType;
	}
		
	public void setCrType(String value)
	{
		this._crType = value;
	}

	/*****************************************************************************************/
	public boolean getHasCrc()
	{
		return _hasCrc;
	}
		
	public void setHasCrc(boolean value)
	{
		_hasCrc = value;
	}

	/*****************************************************************************************/
	public ArrayList<Object> getSavedTabs()
	{
		return _savedTabs;
	}
		
	public void setSavedTabs(ArrayList<Object> value)
	{
		_savedTabs = value;
	}


	/*
	 * Show me what you've got
	 */
	public String toString()
	{
		String ret = "EGUrlDTO content (Java)\n";
		ret += "      Part. ID=" + _participantId + "\n";
		ret += "        DNA ID=" + _dnaId + "\n";
		ret += "     Module ID=" + _dnaId + "\n";
		ret += "    Model Name=" + _modelName + "\n";
		ret += "      Sim Name=" + _simName + "\n";
		ret += "     Part Name=" + _partName + "\n";
		ret += "   Client Name=" + _clientName + "\n";
		ret += "  Project Name=" + _jobName + "\n";
		ret += _raterNameData.toString();
		ret += "  Competency Array:\n";
		for (EGCompDisplayData cmp : _compArray)
		{
			ret += cmp.toString();
		}
		ret += "         Notes=" + _irNotes + "\n";
		ret += "  Importance Rating Array:\n";
		for (EGIrDisplayData ir : _irArray)
		{
			ret += ir.toString();
		}
		ret += _overallData.toString();
		ret += "  is Submitted=" + _submitted + "\n";
		ret += "  Special Type=" + _specialType + "\n";
		ret += "  Language selected=" + (_simLangItem == null ? "None (null)" : _simLangItem.toString()) + "\n";
		ret += "       \"Other\" text=" + (_simLangOther == null ? "None" : _simLangOther) + "\n";
		ret += "  Lang list not output yet\n";
		ret += "  Delivery Method not output yet\n";
		ret += "  Flags: iblva=" + _iblva + ", nis=" + _nis + ", ismll=" + _ismll + ", isbul=" + _isbul + ", isz=" + _isz + ", eis=" + _eis+ ", bre2=" + _bre2 + ", bresea2=" + _bresea + ", sfeceo=" + _sfeceo + "\n";
		ret += "  Type (for Roxcart)=" + _crType;
		
		return ret;
	}

	public boolean getBremed() {
		return _bremed;
	}

	public void setBremed(boolean _bremed) {
		this._bremed = _bremed;
	}

	
}
