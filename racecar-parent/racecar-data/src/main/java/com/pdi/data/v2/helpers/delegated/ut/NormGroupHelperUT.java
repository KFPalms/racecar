package com.pdi.data.v2.helpers.delegated.ut;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import com.pdi.data.dto.Norm;
import com.pdi.data.dto.NormGroup;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.INormGroupHelper;
//import com.pdi.data.helpers.interfaces.INormHelper;
import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

public class NormGroupHelperUT extends TestCase {
	public NormGroupHelperUT(String name)
	{
		super(name);
	}
	
//	// Neds to be redone with NHN IDs
//	public void testFromId()
//	throws Exception
//	{
//		
//		SessionUser session = new SessionUser();
//		session.getMetadata().put("USERNAME", "mbpanichi");
//		session.getMetadata().put("PASSWORD", "griffin12");
//		UnitTestUtils.start(this);
//		V2DatabaseUtils.setUnitTest(true);
//	
//		INormGroupHelper iNormGroup = HelperDelegate.getNormGroupHelper("com.pdi.data.v2.helpers.delegated");
//		//normGroupId	V2NormGroupId		instrumentCode	name						normType	archived
//		//208			EZZHNWMJ			BHMGTDEV		North American Executive	2			0
//		
//		NormGroup ng = iNormGroup.fromId(session, 208);
//		
//		System.out.println("test from Id....");
//		System.out.println("    id: " + ng.getId());
//		System.out.println("  inst: " + ng.getInstrumentCode());
//		System.out.println("  name: " + ng.getName());
//		System.out.println("  type: " + ng.getPopulation());
//		UnitTestUtils.stop(this);
//
//	}
	
	
	// Needs to be redone with NHN IDs
//	public void testFromInstrumentId()
//	throws Exception
//	{
//		SessionUser session = new SessionUser();
//		session.getMetadata().put("USERNAME", "mbpanichi");
//		session.getMetadata().put("PASSWORD", "griffin12");
//		UnitTestUtils.start(this);
//		V2DatabaseUtils.setUnitTest(true);
//		
//		INormGroupHelper iNormGroup = HelperDelegate.getNormGroupHelper("com.pdi.data.v2.helpers.delegated");
//		
//		ArrayList<NormGroup> groups = iNormGroup.fromInstrumentId(session, "lei");
//		
//		//normGroupId	V2NormGroupId	instrumentCode	name									normType	archived
//		//345			KCHTUXMZ		HNZYMHDV		Saudi Aramco - Mid-Level Manager, 2006	2			0
//		
//		System.out.println("test from Instrument Id....");
//		System.out.println("groups.size: " + groups.size());
//		for(NormGroup ng : groups){	
//			
//			System.out.println("NORM GROUP");
//			System.out.println("    id: " + ng.getId());
//			System.out.println("  inst: " + ng.getInstrumentCode());
//			System.out.println("  name: " + ng.getName());
//			System.out.println("  type: " + ng.getPopulation());
//			HashMap<String, Norm> norms = ng.getNorms();
//			Iterator<String> it = norms.keySet().iterator();
//			System.out.println("     NORMS");
//			while(it.hasNext()){
//				String e = it.next();
//				Norm norm = norms.get(e);		
//				System.out.println("     " + norm.getId());
//				System.out.println("     " + norm.getMean());
//				System.out.println("     " + norm.getStandardDeviation());
//				System.out.println("     " + norm.getSpssValue());
//			}
//			System.out.println("--------------------");
//		}
//
//		UnitTestUtils.stop(this);		
//	}
	
//	// This method has been retired... test method has been replaced by fromParticipantInstrumentIdProjectId
//	//public void testFromParticipantInstrumentId()
//	//throws Exception
//	//{
//	//	SessionUser session = new SessionUser();
//	//	session.getMetadata().put("USERNAME", "mbpanichi");
//	//	session.getMetadata().put("PASSWORD", "griffin12");
//	//	UnitTestUtils.start(this);
//	//	V2DatabaseUtils.setUnitTest(true);
//	//	
//	//	INormGroupHelper iNormGroup = HelperDelegate.getNormGroupHelper("com.pdi.data.v2.helpers.delegated");
//	//	
//	//	ArrayList<NormGroup> groups = iNormGroup.fromParticipantInstrumentId(session, "XXXXXXXX", "BMZLGGZL");
//	//	
//	//	//pinId	participantId	instrumentCode	projectId	specPopNormGroupId	genPopNormGroupId	archived
//	//	//1		XXXXXXXX		BHMGTDEV		XXXXXX		221					218				0		
//	//	
//	//	System.out.println("test from Participant Instrument Id....");
//	//	for(NormGroup ng : groups){	
//	//		
//	//		System.out.println("NORM GROUP");
//	//		System.out.println("    id: " + ng.getId());
//	//		System.out.println("  inst: " + ng.getInstrumentCode());
//	//		System.out.println("  name: " + ng.getName());
//	//		System.out.println("  type: " + ng.getPopulation());
//	//		HashMap<String, Norm> norms = ng.getNorms();
//	//		Iterator<String> it = norms.keySet().iterator();
//	//		System.out.println("     NORMS");
//	//		while(it.hasNext()){
//	//			String e = it.next();
//	//			Norm norm = norms.get(e);		
//	//			System.out.println("     " + norm.getId());
//	//			System.out.println("     " + norm.getMean());
//	//			System.out.println("     " + norm.getStandardDeviation());
//	//			System.out.println("     " + norm.getSpssValue());
//	//		}
//	//		System.out.println("--------------------");
//	//	}
//
//	//	UnitTestUtils.stop(this);		
//	//}


	public void testFromParticipantInstrumentIdProjectId()
	throws Exception
	{
		SessionUser session = new SessionUser();
		//session.getMetadata().put("USERNAME", "mbpanichi");
		//session.getMetadata().put("PASSWORD", "griffin12");
		UnitTestUtils.start(this);
		V2DatabaseUtils.setUnitTest(true);
		
		String instId ="rv";
		String pptId = "368084";
		String projId = "591";

		INormGroupHelper iNormGroup = HelperDelegate.getNormGroupHelper("com.pdi.data.v2.helpers.delegated");
		
		ArrayList<NormGroup> groups = iNormGroup.fromParticipantInstrumentIdProjectId(session, pptId, instId, projId);
		
		System.out.println("test from Participant, Instrument Id, ProjectId....");
		for(NormGroup ng : groups)
		{	
			System.out.println("NORM GROUP");
			System.out.println("    id: " + ng.getId());
			System.out.println("  inst: " + ng.getInstrumentCode());
			System.out.println("  name: " + ng.getName());
			System.out.println("  type: " + ng.getPopulation());
			HashMap<String, Norm> norms = ng.getNorms();
			Iterator<String> it = norms.keySet().iterator();
			System.out.println("     NORMS");
			while(it.hasNext()){
				String e = it.next();
				Norm norm = norms.get(e);		
				System.out.println("     " + norm.getId());
				System.out.println("     " + norm.getMean());
				System.out.println("     " + norm.getStandardDeviation());
				System.out.println("     " + norm.getSpssValue());
			}
			System.out.println("--------------------");
		}

		UnitTestUtils.stop(this);		
	}
}
