/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v2.helpers.delegated;

//import java.util.ArrayList;

import java.util.ArrayList;

//import javax.xml.xpath.XPath;
//import javax.xml.xpath.XPathConstants;
//import javax.xml.xpath.XPathFactory;
//
//import org.w3c.dom.Document;
//import org.w3c.dom.Node;
//import org.w3c.dom.NodeList;

//import com.pdi.data.dto.Project;
//import com.pdi.data.dto.ProjectParticipant;
import com.pdi.data.dto.NoteHolder;
import com.pdi.data.dto.Participant;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.interfaces.IParticipantHelper;
//import com.pdi.data.v2.dto.SessionUserDTO;
//import com.pdi.data.v2.util.V2WebserviceUtils;
//import com.pdi.xml.XMLUtils;

public class ParticipantHelper implements IParticipantHelper{
	
	/**
	 * fromId
	 * use case:  get participant object from participantId
	 * @param SessionUser session
	 * @param String participantId
	 * @return Participant
	 */
	//@Override
	public Participant fromId(SessionUser session, String participantId)
	{
		// Either remove from here and interface or rework using JDBC calls.
		// Use participantId instead of candidate id
//		
//		Participant participant = new Participant();
//		
//		try {
//			
//			String username = session.getMetadata().get("USERNAME");
//			String password = session.getMetadata().get("PASSWORD");
//			V2WebserviceUtils v2web = new V2WebserviceUtils();
//			SessionUserDTO sessionUser = v2web.registerUser(username, password);
//			
//			String xml = "";
//			xml += "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
//			xml += "<ETR SessionId=\"${sessionId}\">";
//			xml += "<Rqt Cmd=\"OpenSQL\" AN=\"CandModSQL\" FA=\"Y\" FR=\"1\" RC=\"N\" SQ=\"SELECT C.UniqueIdStamp, C.FirstName, C.LastName, C.Email, C.XML FROM Candidates as C INNER JOIN Cand_Jobs as CJ on (CJ.CandidateID=C.UniqueIdStamp and C.UniqueIdStamp = &apos;${candidateId}&apos; and CJ.Archived=&apos;0&apos;)   \" />";
//			xml += "<Rqt Cmd=\"Read\" AN=\"CandModSQL\" Sub=\"CandidateModules\" Rpt=\"*\" RQ=\"N\">";
//			xml += "	<Fld FN=\"UniqueIdStamp\" Fmt=\"A\" />";
//			xml += "	<Fld FN=\"FirstName\" Fmt=\"A\" />";
//			xml += "	<Fld FN=\"LastName\" Fmt=\"A\" />";
//			xml += "	<Fld FN=\"Email\" Fmt=\"A\" />";
//			xml += "    <Fld FN=\"xml_BU\" Fmt=\"A\" />";
//			xml += "    <Fld FN=\"xml_JobTitle\" Fmt=\"A\" />";
//			xml += "</Rqt>";
//			xml += "<Rqt Cmd=\"Close\" AN=\"CandModSQL\" />";
//			xml += "</ETR>";
//			xml = xml.replace("${candidateId}", participantId);
//			xml = xml.replace("${sessionId}", sessionUser.getSessionId());
//			
//			XPath xpath = XPathFactory.newInstance().newXPath(); 
//			Document doc = v2web.sendData(xml);
//			
//			NodeList records = (NodeList) xpath.evaluate("//Record", doc, XPathConstants.NODESET); 
//			
//			// Returns the last participant
//			for(int i = 0; i < records.getLength(); i++) {
//				Node node = records.item(i);
//				
//				participant.setId(XMLUtils.getAttributeValue(node, "UniqueIdStamp"));
//				//participant.setFirstName(XMLUtils.xmlFormatString(XMLUtils.getAttributeValue(node, "FirstName")));
//				//participant.setLastName(XMLUtils.xmlFormatString(XMLUtils.getAttributeValue(node, "LastName")));
//				//participant.setEmail(XMLUtils.xmlFormatString(XMLUtils.getAttributeValue(node, "Email")));
//				//participant.setBusinessUnit(XMLUtils.xmlFormatString(XMLUtils.getAttributeValue(node, "xml_BU")));
//				//participant.setJobTitle(XMLUtils.xmlFormatString(XMLUtils.getAttributeValue(node, "xml_JobTitle")));
//				participant.setFirstName(XMLUtils.getAttributeValue(node, "FirstName"));
//				participant.setLastName(XMLUtils.getAttributeValue(node, "LastName"));
//				participant.setEmail(XMLUtils.getAttributeValue(node, "Email"));
//				participant.getMetadata().put("OPTIONAL_1",XMLUtils.getAttributeValue(node, "xml_BU"));
//				participant.getMetadata().put("OPTIONAL_2",XMLUtils.getAttributeValue(node, "xml_JobTitle"));
//
//
//			}
//			
//		} catch(Exception e) {
//			e.printStackTrace();
//		}
//		
//		
//		return participant;
		
		return null;
	}

	//@Override
	public ArrayList<Participant> fromProjectId(SessionUser session,
			String engagementId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<Participant> fromClientId(SessionUser session,
			String clientId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<Participant> fromAdminId(SessionUser session, String adminId) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public ArrayList<Participant> find(SessionUser session, String adminId, String target)
	{
		return new ArrayList<Participant>();
	}


	@Override
	public NoteHolder getNotes(SessionUser session, String participantId)
	{
		return null;
	}
	

}
