package com.pdi.data.loaders;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * Loader to put the correct normId values into the norm element table
 * Based upon the old V2 norm ids
 * 
 * NOT NEEDED for prod (contains all the relevant info)
 * 
 * @author kbeukelm
 *
 */
public class LoadPdiNormStuff
{
	// Static constants
    protected static final String DRIVER = "com.mysql.jdbc.Driver";
    
    private static final boolean USE_TEST_FILES = false;

	/*
	 * The active flag probably could have been hard-coded.  In future loads,
	 * the date could be the system date (captured as a String somewhere) and
	 * the user could be passed in as a parameter.
	 */
	private static final String[][] NORM_GROUP_DATA = new String[][] 
	{
		//				inst	name												Type active	lastDate			 lastUser
		(new String [] {"cs",	"General Population Norm",							"1", "1", "2010-03-04 13:43:55", "mbpanichi"}),
		(new String [] {"cs2",	"General Population Norm",							"1", "1", "2011-09-07 12:16:55", "mbpanichi"}),
		(new String [] {"gpil",	"Aramco Supervisor",								"2", "1", "2006-09-27 08:49:43", "kbeukelman"}),
		(new String [] {"gpil",	"Asia",												"1", "1", "2006-09-23 11:56:58", "kbeukelman"}),
		(new String [] {"gpil",	"Asia All Management (does not include Japan)",		"2", "1", "2006-09-25 12:46:26", "mbpanichi"}),
		(new String [] {"gpil",	"Asia First-Level Leader (includes Australia)",		"2", "1", "2006-09-25 12:28:57", "mbpanichi"}),
		(new String [] {"gpil",	"Asia Individual Contributor (includes Australia)",	"2", "1", "2006-09-25 11:34:20", "mbpanichi"}),
		(new String [] {"gpil",	"Australia All Management",							"2", "1", "2008-09-16 11:06:31", "mbpanichi"}),
		(new String [] {"gpil",	"Europe",											"1", "1", "2006-09-25 07:25:18", "kbeukelman"}),
		(new String [] {"gpil",	"European Exec., Sr. Exec, and CEO",				"2", "1", "2006-09-26 08:45:44", "mbpanichi"}),
		(new String [] {"gpil",	"European First-Level Leader",						"2", "1", "2006-09-28 12:58:21", "kbeukelman"}),
		(new String [] {"gpil",	"European Individual Contributor",					"2", "1", "2006-09-26 08:21:42", "mbpanichi"}),
		(new String [] {"gpil",	"European Mid-Level Leader",						"2", "1", "2006-09-28 15:09:17", "kbeukelman"}),
		(new String [] {"gpil",	"Freddie Mac Sr Executive",							"2", "1", "2006-09-28 15:42:12", "kbeukelman"}),
		(new String [] {"gpil",	"Global CEO",										"2", "1", "2010-06-17 07:37:12", "kbeukelman"}),
		(new String [] {"gpil",	"Global Senior Executive",							"2", "1", "2010-06-17 07:37:12", "kbeukelman"}),
		(new String [] {"gpil",	"Japan",											"1", "1", "2006-09-25 08:17:44", "kbeukelman"}),
		(new String [] {"gpil",	"Japan All Management",								"2", "1", "2006-09-26 08:17:20", "kbeukelman"}),
		(new String [] {"gpil",	"Japan First-Level Leader",							"2", "1", "2006-09-25 15:34:38", "mbpanichi"}),
		(new String [] {"gpil",	"Japan Individual Contributor",						"2", "1", "2006-09-25 14:44:24", "mbpanichi"}),
		(new String [] {"gpil",	"Latin America All Management",						"2", "1", "2008-05-05 09:34:33", "msolomonson"}),
		(new String [] {"gpil",	"Middle East All Management",						"2", "1", "2008-09-16 12:12:03", "mbpanichi"}),
		(new String [] {"gpil",	"NA Executive",										"2", "1", "2006-09-25 08:39:35", "mbpanichi"}),
		(new String [] {"gpil",	"NA First-Level Leader",							"2", "1", "2006-09-25 11:16:52", "mbpanichi"}),
		(new String [] {"gpil",	"NA Individual Contributor",						"2", "1", "2006-09-25 10:55:01", "mbpanichi"}),
		(new String [] {"gpil",	"NA Mid-Level Leader",								"2", "1", "2006-09-25 10:37:32", "mbpanichi"}),
		(new String [] {"gpil",	"NA Supervisor",									"2", "1", "2006-09-28 15:37:41", "kbeukelman"}),
		(new String [] {"gpil",	"North America",									"1", "1", "2006-09-25 08:22:06", "mbpanichi"}),
		(new String [] {"gpil",	"Saudi Aramco All Management",						"2", "1", "2006-09-28 15:29:05", "kbeukelman"}),
		(new String [] {"gpil",	"Saudi Aramco Mid-Level Manager",					"2", "1", "2006-09-27 12:58:45", "kbeukelman"}),
		(new String [] {"lei",	"All Leaders",										"1", "1", "2006-10-02 07:59:41", "msolomonson"}),
		(new String [] {"lei",	"Executive",										"2", "1", "2006-10-02 08:17:10", "msolomonson"}),
		(new String [] {"lei",	"First-Level Leader",								"2", "1", "2006-10-02 09:35:49", "msolomonson"}),
		(new String [] {"lei",	"Freddie Mac Sr Executive",							"2", "1", "2006-10-02 10:43:32", "msolomonson"}),
		(new String [] {"lei",	"Global CEO",										"2", "1", "2010-06-16 13:59:53", "kbeukelman"}),
		(new String [] {"lei",	"Global Senior Executive",							"2", "1", "2010-06-16 13:53:20", "kbeukelman"}),
		(new String [] {"lei",	"Mid-Level Leader",									"2", "1", "2006-10-02 09:46:44", "msolomonson"}),
		(new String [] {"rv",	"Australia - All Mgmt Levels, 2008",				"2", "1", "2010-03-05 09:30:07", "mbpanichi"}),
		(new String [] {"rv",	"Europe - All Mgmt Levels",							"2", "1", "2010-02-25 08:27:43", "mbpanichi"}),
		(new String [] {"rv",	"Global - All Mgmt Levels",							"1", "1", "2010-02-25 08:27:43", "mbpanichi"}),
		(new String [] {"rv",	"Japan - All Mgmt Levels, 2008",					"2", "1", "2010-03-05 09:30:07", "mbpanichi"}),
		(new String [] {"rv",	"Latin America - All Mgmt Levels",					"2", "1", "2010-03-05 09:30:07", "mbpanichi"}),
		(new String [] {"rv",	"Middle East - All Mgmt Levels, 2008",				"2", "1", "2010-03-05 09:36:40", "mbpanichi"}),
		(new String [] {"rv",	"Non-Japan Asia - All Mgmt Levels, 2008",			"2", "1", "2010-03-05 09:30:07", "mbpanichi"}),
		(new String [] {"rv",	 "North America - All Mgmt Levels",					"2", "1", "2010-03-05 09:30:07", "mbpanichi"}),
		(new String [] {"rv",	"Saudi Aramco - All Mgmt Levels, 2004",				"2", "1", "2010-03-05 09:30:07", "mbpanichi"}),
		(new String [] {"rv",	"Saudi Aramco - Mid-Level Manager, 2006",			"2", "1", "2010-03-05 09:30:07", "mbpanichi"}),
		(new String [] {"wg",	"Europe - All Management Levels",					"2", "1", "2010-04-26 09:14:04", "mbpanichi"}),
		(new String [] {"wg",	"First Level Leader",								"2", "1", "2010-03-05 10:20:21", "mbpanichi"}),
		(new String [] {"wg",	"General Population",								"1", "1", "2010-03-05 10:02:53", "mbpanichi"}),
		(new String [] {"wg",	"Global CEO/Senior Executive",						"2", "1", "2010-06-16 13:27:07", "kbeukelman"}),
		(new String [] {"wg",	"Mid-Level Leader",									"2", "1", "2010-03-05 10:26:54", "mbpanichi"}),
		(new String [] {"wg",	"NA Executive",										"2", "1", "2010-03-05 10:26:54", "mbpanichi"}),
		(new String [] {"wg",	"Non-Management",									"2", "1", "2010-03-05 10:02:53", "mbpanichi"}),
		(new String [] {"wg",	"Saudi Aramco - All Mgmt Levels, 2004",				"2", "1", "2010-04-26 09:14:04", "mbpanichi"}),
		(new String [] {"wg",	"Saudi Aramco - Mid-Level Manager, 2006",			"2", "1", "2010-04-26 09:14:04", "mbpanichi"}),
		(new String [] {"wg",	"Saudi Aramco - Supervisor, 2006",					"2", "1", "2010-04-26 09:14:04", "mbpanichi"}),
		(new String [] {"wg",	"Supervisor",										"2", "1", "2010-03-05 10:20:21", "mbpanichi"}),
		(new String [] {"wga",	"General Population",								"1", "1", "2006-09-26 14:44:00", "tmartinson"}),
		(new String [] {"wga",	"Global CEO/Senior Executive",						"2", "1", "2010-06-16 13:20:34", "kbeukelman"}),
		(new String [] {"wga",	"NA Executive",										"2", "1", "2006-09-26 14:44:00", "tmartinson"}),
		(new String [] {"wga",	"NA First-Level Leader",							"2", "1", "2006-09-26 14:50:33", "tmartinson"}),
		(new String [] {"wga",	"NA Individual Contributor",						"2", "1", "2006-09-26 14:50:33", "tmartinson"}),
		(new String [] {"wga",	"NA Mid-Level Leader",								"2", "1", "2006-09-26 14:50:33", "tmartinson"}),
		(new String [] {"wga",	"NA Supervisor",									"2", "1", "2006-09-26 14:50:33", "tmartinson"}),
		(new String [] {"wga",	"Saudi Aramco All Management",						"1", "1", "2006-09-26 14:50:33", "tmartinson"}),
		(new String [] {"wga",	"Saudi Aramco Mid-Level Manager",					"2", "1", "2006-09-26 14:50:33", "tmartinson"}),
		(new String [] {"wga",	"Saudi Aramco Supervisor",							"2", "1", "2006-09-26 14:50:33", "tmartinson"})
	};
	
	private static final String[][] NORM_DATA = new String[][] 
	{
		//				Key								mean		stdev		spss value				active
		(new String [] {"cs|General Population Norm",	"2.7488",	"0.88267",	"CAREER_DRIVERS",		"1"}),
		(new String [] {"cs|General Population Norm",	"3.5145",	"0.62377",	"CAREER_GOALS",			"1"}),
		(new String [] {"cs|General Population Norm",	"3.6382",	"0.44473",	"EXPERIENCE_ORIENT",	"1"}),
		(new String [] {"cs|General Population Norm",	"3.1826",	"0.55244",	"LEARNING_ORIENT",		"1"}),
		(new String [] {"cs2|General Population Norm",	"2.7488",	"0.88267",	"CAREER_DRIVERS",		"1"}),
		(new String [] {"cs2|General Population Norm",	"3.5145",	"0.62377",	"CAREER_GOALS",			"1"}),
		(new String [] {"cs2|General Population Norm",	"3.6382",	"0.44473",	"EXPERIENCE_ORIENT",	"1"}),
		(new String [] {"cs2|General Population Norm",	"3.1826",	"0.55244",	"LEARNING_ORIENT",		"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"6.516",	"1.114",	"GPI_AD",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"5.117",	"0.991",	"GPI_ADPT",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"5.466",	"0.944",	"GPI_AST",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"4.488",	"1.105",	"GPI_COMP",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"7.604",	"1.102",	"GPI_CONS",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"6.094",	"1.066",	"GPI_DACH",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"4.812",	"0.962",	"GPI_DADV",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"5.631",	"0.923",	"GPI_DUT",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"4.512",	"1.086",	"GPI_EC",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"3.251",	"0.918",	"GPI_EGO",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"5.97",		"1.164",	"GPI_EL",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"5.215",	"0.815",	"GPI_EMP",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"4.666",	"0.911",	"GPI_IMPR",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"3.466",	"1.003",	"GPI_IND",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"6.065",	"1.071",	"GPI_INFL",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"6.379",	"1.104",	"GPI_INIT",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"6.519",	"1.039",	"GPI_INOV",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"5.88",		"1.074",	"GPI_INTD",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"1.893",	"0.91",		"GPI_INTI",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"3.984",	"1.278",	"GPI_MAN",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"2.175",	"0.911",	"GPI_MIC",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"1.836",	"0.892",	"GPI_NA",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"4.946",	"0.975",	"GPI_OPEN",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"6.594",	"1.112",	"GPI_OPT",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"2.595",	"0.885",	"GPI_PA",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"5.797",	"0.839",	"GPI_RESP",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"5.583",	"1.27",		"GPI_RISK",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"7.368",	"1.008",	"GPI_SASI",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"5.172",	"0.834",	"GPI_SC",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"6.399",	"1.232",	"GPI_SO",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"4.69",		"1.157",	"GPI_ST",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"7.401",	"1.034",	"GPI_TA",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"6.931",	"1.267",	"GPI_TC",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"4.978",	"0.832",	"GPI_TF",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"5.318",	"0.866",	"GPI_TR",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"6.293",	"1.132",	"GPI_VIS",	"1"}),
		(new String [] {"gpil|Aramco Supervisor",		"6.59",		"1.085",	"GPI_WF",	"1"}),
		(new String [] {"gpil|Asia",					"5.423",	"1.248",	"GPI_AD",	"1"}),
		(new String [] {"gpil|Asia",					"4.646",	"0.983",	"GPI_ADPT",	"1"}),
		(new String [] {"gpil|Asia",					"4.774",	"0.942",	"GPI_AST",	"1"}),
		(new String [] {"gpil|Asia",					"3.795",	"1.098",	"GPI_COMP",	"1"}),
		(new String [] {"gpil|Asia",					"7.146",	"1.151",	"GPI_CONS",	"1"}),
		(new String [] {"gpil|Asia",					"5.295",	"1.176",	"GPI_DACH",	"1"}),
		(new String [] {"gpil|Asia",					"3.154",	"1.092",	"GPI_DADV",	"1"}),
		(new String [] {"gpil|Asia",					"4.812",	"1.038",	"GPI_DUT",	"1"}),
		(new String [] {"gpil|Asia",					"3.81",		"1.089",	"GPI_EC",	"1"}),
		(new String [] {"gpil|Asia",					"3.073",	"0.919",	"GPI_EGO",	"1"}),
		(new String [] {"gpil|Asia",					"4.817",	"1.23",		"GPI_EL",	"1"}),
		(new String [] {"gpil|Asia",					"4.332",	"0.854",	"GPI_EMP",	"1"}),
		(new String [] {"gpil|Asia",					"3.471",	"0.82",		"GPI_IMPR",	"1"}),
		(new String [] {"gpil|Asia",					"3.976",	"0.957",	"GPI_IND",	"1"}),
		(new String [] {"gpil|Asia",					"4.966",	"1.23",		"GPI_INFL",	"1"}),
		(new String [] {"gpil|Asia",					"5.122",	"1.152",	"GPI_INIT",	"1"}),
		(new String [] {"gpil|Asia",					"5.483",	"1.454",	"GPI_INOV",	"1"}),
		(new String [] {"gpil|Asia",					"4.552",	"0.972",	"GPI_INTD",	"1"}),
		(new String [] {"gpil|Asia",					"2.268",	"0.884",	"GPI_INTI",	"1"}),
		(new String [] {"gpil|Asia",					"4.353",	"1.203",	"GPI_MAN",	"1"}),
		(new String [] {"gpil|Asia",					"2.545",	"0.857",	"GPI_MIC",	"1"}),
		(new String [] {"gpil|Asia",					"2.496",	"0.895",	"GPI_NA",	"1"}),
		(new String [] {"gpil|Asia",					"3.848",	"0.981",	"GPI_OPEN",	"1"}),
		(new String [] {"gpil|Asia",					"5.29",		"1.187",	"GPI_OPT",	"1"}),
		(new String [] {"gpil|Asia",					"3.192",	"0.857",	"GPI_PA",	"1"}),
		(new String [] {"gpil|Asia",					"4.748",	"0.944",	"GPI_RESP",	"1"}),
		(new String [] {"gpil|Asia",					"5.296",	"1.195",	"GPI_RISK",	"1"}),
		(new String [] {"gpil|Asia",					"6.155",	"1.124",	"GPI_SASI",	"1"}),
		(new String [] {"gpil|Asia",					"4.184",	"0.936",	"GPI_SC",	"1"}),
		(new String [] {"gpil|Asia",					"5.09",		"1.453",	"GPI_SO",	"1"}),
		(new String [] {"gpil|Asia",					"4.052",	"1.2",		"GPI_ST",	"1"}),
		(new String [] {"gpil|Asia",					"6.505",	"1.067",	"GPI_TA",	"1"}),
		(new String [] {"gpil|Asia",					"5.568",	"1.576",	"GPI_TC",	"1"}),
		(new String [] {"gpil|Asia",					"4.078",	"0.963",	"GPI_TF",	"1"}),
		(new String [] {"gpil|Asia",					"4.735",	"0.854",	"GPI_TR",	"1"}),
		(new String [] {"gpil|Asia",					"5.267",	"1.217",	"GPI_VIS",	"1"}),
		(new String [] {"gpil|Asia",					"5.377",	"1.204",	"GPI_WF",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"5.218",	"1.258",	"GPI_AD",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"5.185",	"0.966",	"GPI_ADPT",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"5.103",	"0.834",	"GPI_AST",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"3.859",	"1.143",	"GPI_COMP",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"7.438",	"1.202",	"GPI_CONS",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"5.742",	"1.036",	"GPI_DACH",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"3.421",	"1.03",		"GPI_DADV",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"5.024",	"0.972",	"GPI_DUT",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"4.129",	"1.055",	"GPI_EC",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"3.23",		"0.926",	"GPI_EGO",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"5.419",	"1.169",	"GPI_EL",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"4.631",	"0.865",	"GPI_EMP",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"3.714",	"0.764",	"GPI_IMPR",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"3.909",	"0.913",	"GPI_IND",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"5.825",	"1.009",	"GPI_INFL",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"5.802",	"1.1",		"GPI_INIT",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"6.169",	"1.295",	"GPI_INOV",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"4.97",		"1.062",	"GPI_INTD",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"2.347",	"0.905",	"GPI_INTI",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"4.218",	"1.312",	"GPI_MAN",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"2.228",	"0.891",	"GPI_MIC",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"2.069",	"0.899",	"GPI_NA",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"4.204",	"0.953",	"GPI_OPEN",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"5.788",	"1.173",	"GPI_OPT",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"2.94",		"0.896",	"GPI_PA",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"5.147",	"0.877",	"GPI_RESP",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"5.831",	"1.178",	"GPI_RISK",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"6.616",	"0.958",	"GPI_SASI",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"4.571",	"0.806",	"GPI_SC",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"5.361",	"1.338",	"GPI_SO",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"4.459",	"1.223",	"GPI_ST",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"6.947",	"0.974",	"GPI_TA",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"6.859",	"1.065",	"GPI_TC",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"4.623",	"0.934",	"GPI_TF",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"4.909",	"0.882",	"GPI_TR",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"6.046",	"1.049",	"GPI_VIS",	"1"}),
		(new String [] {"gpil|Asia All Management (does not include Japan)",	"5.619",	"1.256",	"GPI_WF",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"5.423",	"1.206",	"GPI_AD",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"4.697",	"0.98",		"GPI_ADPT",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"4.89",		"0.864",	"GPI_AST",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"3.895",	"1.117",	"GPI_COMP",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"7.245",	"0.997",	"GPI_CONS",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"5.487",	"1.039",	"GPI_DACH",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"3.242",	"1.115",	"GPI_DADV",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"4.952",	"1.028",	"GPI_DUT",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"4.033",	"1.052",	"GPI_EC",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"3.114",	"0.865",	"GPI_EGO",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"4.97",		"1.154",	"GPI_EL",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"4.401",	"0.794",	"GPI_EMP",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"3.522",	"0.784",	"GPI_IMPR",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"3.966",	"0.907",	"GPI_IND",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"5.208",	"1.113",	"GPI_INFL",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"5.268",	"1.126",	"GPI_INIT",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"5.665",	"1.44",		"GPI_INOV",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"4.666",	"0.918",	"GPI_INTD",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"2.27",		"0.889",	"GPI_INTI",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"4.399",	"1.113",	"GPI_MAN",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"2.146",	"0.818",	"GPI_MIC",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"2.344",	"0.781",	"GPI_NA",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"3.927",	"0.976",	"GPI_OPEN",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"5.32",		"1.141",	"GPI_OPT",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"3.09",		"0.818",	"GPI_PA",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"4.889",	"0.835",	"GPI_RESP",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"5.434",	"1.161",	"GPI_RISK",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"6.365",	"0.984",	"GPI_SASI",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"4.277",	"0.859",	"GPI_SC",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"5.079",	"1.456",	"GPI_SO",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"4.215",	"1.186",	"GPI_ST",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"6.678",	"0.917",	"GPI_TA",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"6.013",	"1.364",	"GPI_TC",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"4.19",		"0.885",	"GPI_TF",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"4.825",	"0.808",	"GPI_TR",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"5.494",	"1.089",	"GPI_VIS",	"1"}),
		(new String [] {"gpil|Asia First-Level Leader (includes Australia)",	"5.533",	"1.149",	"GPI_WF",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"5.407",	"1.252",	"GPI_AD",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"4.469",	"0.939",	"GPI_ADPT",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"4.551",	"1.013",	"GPI_AST",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"3.621",	"1.076",	"GPI_COMP",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"7.044",	"1.232",	"GPI_CONS",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"5.2",		"1.207",	"GPI_DACH",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"2.822",	"1.008",	"GPI_DADV",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"4.763",	"1.06",		"GPI_DUT",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"3.684",	"1.091",	"GPI_EC",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"2.797",	"0.832",	"GPI_EGO",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"4.629",	"1.274",	"GPI_EL",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"4.179",	"0.899",	"GPI_EMP",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"3.292",	"0.807",	"GPI_IMPR",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"3.86",		"1.005",	"GPI_IND",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"4.559",	"1.191",	"GPI_INFL",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"4.958",	"1.136",	"GPI_INIT",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"5.416",	"1.458",	"GPI_INOV",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"4.483",	"0.947",	"GPI_INTD",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"2.087",	"0.833",	"GPI_INTI",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"4.205",	"1.166",	"GPI_MAN",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"2.497",	"0.785",	"GPI_MIC",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"2.605",	"0.864",	"GPI_NA",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"3.792",	"1.001",	"GPI_OPEN",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"5.038",	"1.187",	"GPI_OPT",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"3.105",	"0.835",	"GPI_PA",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"4.655",	"0.961",	"GPI_RESP",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"5.042",	"1.134",	"GPI_RISK",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"6.1",		"1.158",	"GPI_SASI",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"3.992",	"0.971",	"GPI_SC",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"4.904",	"1.482",	"GPI_SO",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"3.957",	"1.199",	"GPI_ST",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"6.531",	"1.045",	"GPI_TA",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"5.051",	"1.499",	"GPI_TC",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"4.007",	"0.942",	"GPI_TF",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"4.654",	"0.873",	"GPI_TR",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"5.172",	"1.196",	"GPI_VIS",	"1"}),
		(new String [] {"gpil|Asia Individual Contributor (includes Australia)",	"5.307",	"1.21",		"GPI_WF",	"1"}),
		(new String [] {"gpil|Australia All Management",	"5.472159",	"1.469486",	"GPI_AD",	"1"}),
		(new String [] {"gpil|Australia All Management",	"5.533523",	"0.84245",	"GPI_ADPT",	"1"}),
		(new String [] {"gpil|Australia All Management",	"5.536932",	"0.821081",	"GPI_AST",	"1"}),
		(new String [] {"gpil|Australia All Management",	"4.406818",	"1.161311",	"GPI_COMP",	"1"}),
		(new String [] {"gpil|Australia All Management",	"7.6375",	"1.160454",	"GPI_CONS",	"1"}),
		(new String [] {"gpil|Australia All Management",	"6.147727",	"0.900231",	"GPI_DACH",	"1"}),
		(new String [] {"gpil|Australia All Management",	"3.885227",	"0.988492",	"GPI_DADV",	"1"}),
		(new String [] {"gpil|Australia All Management",	"5.860227",	"0.978445",	"GPI_DUT",	"1"}),
		(new String [] {"gpil|Australia All Management",	"4.846591",	"0.990151",	"GPI_EC",	"1"}),
		(new String [] {"gpil|Australia All Management",	"2.886364",	"0.882077",	"GPI_EGO",	"1"}),
		(new String [] {"gpil|Australia All Management",	"6.236932",	"1.038263",	"GPI_EL",	"1"}),
		(new String [] {"gpil|Australia All Management",	"5.032386",	"0.850567",	"GPI_EMP",	"1"}),
		(new String [] {"gpil|Australia All Management",	"3.891477",	"0.698608",	"GPI_IMPR",	"1"}),
		(new String [] {"gpil|Australia All Management",	"3.285227",	"0.77486",	"GPI_IND",	"1"}),
		(new String [] {"gpil|Australia All Management",	"6.25625",	"0.953109",	"GPI_INFL",	"1"}),
		(new String [] {"gpil|Australia All Management",	"6.328409",	"0.869038",	"GPI_INIT",	"1"}),
		(new String [] {"gpil|Australia All Management",	"6.411932",	"1.112186",	"GPI_INOV",	"1"}),
		(new String [] {"gpil|Australia All Management",	"5.315909",	"0.908657",	"GPI_INTD",	"1"}),
		(new String [] {"gpil|Australia All Management",	"1.963068",	"0.79375",	"GPI_INTI",	"1"}),
		(new String [] {"gpil|Australia All Management",	"3.127273",	"1.091224",	"GPI_MAN",	"1"}),
		(new String [] {"gpil|Australia All Management",	"1.949432",	"0.849008",	"GPI_MIC",	"1"}),
		(new String [] {"gpil|Australia All Management",	"1.348295",	"0.777951",	"GPI_NA",	"1"}),
		(new String [] {"gpil|Australia All Management",	"4.772727",	"0.951288",	"GPI_OPEN",	"1"}),
		(new String [] {"gpil|Australia All Management",	"6.823864",	"1.02902",	"GPI_OPT",	"1"}),
		(new String [] {"gpil|Australia All Management",	"2.053977",	"0.860955",	"GPI_PA",	"1"}),
		(new String [] {"gpil|Australia All Management",	"5.818182",	"0.781265",	"GPI_RESP",	"1"}),
		(new String [] {"gpil|Australia All Management",	"5.896023",	"1.149359",	"GPI_RISK",	"1"}),
		(new String [] {"gpil|Australia All Management",	"7.219318",	"1.033974",	"GPI_SASI",	"1"}),
		(new String [] {"gpil|Australia All Management",	"5.188068",	"0.750284",	"GPI_SC",	"1"}),
		(new String [] {"gpil|Australia All Management",	"6.483523",	"1.403253",	"GPI_SO",	"1"}),
		(new String [] {"gpil|Australia All Management",	"5.150568",	"1.116976",	"GPI_ST",	"1"}),
		(new String [] {"gpil|Australia All Management",	"7.0375",	"0.951418",	"GPI_TA",	"1"}),
		(new String [] {"gpil|Australia All Management",	"7.303977",	"1.005005",	"GPI_TC",	"1"}),
		(new String [] {"gpil|Australia All Management",	"5.040909",	"0.753524",	"GPI_TF",	"1"}),
		(new String [] {"gpil|Australia All Management",	"5.35",		"0.811155",	"GPI_TR",	"1"}),
		(new String [] {"gpil|Australia All Management",	"6.15",		"0.982014",	"GPI_VIS",	"1"}),
		(new String [] {"gpil|Australia All Management",	"6.346591",	"1.100212",	"GPI_WF",	"1"}),
		(new String [] {"gpil|Europe",	 		"5.95",		"1.362",	"GPI_AD",	"1"}),
		(new String [] {"gpil|Europe",			"4.947",	"1.031",	"GPI_ADPT",	"1"}),
		(new String [] {"gpil|Europe",			"5.205",	"0.924",	"GPI_AST",	"1"}),
		(new String [] {"gpil|Europe",			"3.692",	"1.329",	"GPI_COMP",	"1"}),
		(new String [] {"gpil|Europe",			"7.467",	"1.242",	"GPI_CONS",	"1"}),
		(new String [] {"gpil|Europe",			"4.998",	"1.273",	"GPI_DACH",	"1"}),
		(new String [] {"gpil|Europe",			"3.87",		"1.107",	"GPI_DADV",	"1"}),
		(new String [] {"gpil|Europe",			"5.414",	"1.102",	"GPI_DUT",	"1"}),
		(new String [] {"gpil|Europe",			"4.229",	"1.18",		"GPI_EC",	"1"}),
		(new String [] {"gpil|Europe",			"2.832",	"0.891",	"GPI_EGO",	"1"}),
		(new String [] {"gpil|Europe",			"5.68",		"1.291",	"GPI_EL",	"1"}),
		(new String [] {"gpil|Europe",			"4.803",	"0.87",		"GPI_EMP",	"1"}),
		(new String [] {"gpil|Europe",			"4.209",	"1.062",	"GPI_IMPR",	"1"}),
		(new String [] {"gpil|Europe",			"3.733",	"1.023",	"GPI_IND",	"1"}),
		(new String [] {"gpil|Europe",			"5.488",	"1.217",	"GPI_INFL",	"1"}),
		(new String [] {"gpil|Europe",			"5.903",	"1.165",	"GPI_INIT",	"1"}),
		(new String [] {"gpil|Europe",			"6.077",	"1.214",	"GPI_INOV",	"1"}),
		(new String [] {"gpil|Europe",			"4.952",	"1.071",	"GPI_INTD",	"1"}),
		(new String [] {"gpil|Europe",			"2.406",	"0.926",	"GPI_INTI",	"1"}),
		(new String [] {"gpil|Europe",			"3.291",	"1.357",	"GPI_MAN",	"1"}),
		(new String [] {"gpil|Europe",			"2.407",	"0.899",	"GPI_MIC",	"1"}),
		(new String [] {"gpil|Europe",			"2.04",		"1",		"GPI_NA",	"1"}),
		(new String [] {"gpil|Europe",			"4.674",	"0.976",	"GPI_OPEN",	"1"}),
		(new String [] {"gpil|Europe",			"6.31",		"1.282",	"GPI_OPT",	"1"}),
		(new String [] {"gpil|Europe",			"2.604",	"0.996",	"GPI_PA",	"1"}),
		(new String [] {"gpil|Europe",			"5.683",	"0.8",		"GPI_RESP",	"1"}),
		(new String [] {"gpil|Europe",			"5.482",	"1.338",	"GPI_RISK",	"1"}),
		(new String [] {"gpil|Europe",			"6.75",		"1.084",	"GPI_SASI",	"1"}),
		(new String [] {"gpil|Europe",			"4.649",	"0.891",	"GPI_SC",	"1"}),
		(new String [] {"gpil|Europe",			"5.894",	"1.402",	"GPI_SO",	"1"}),
		(new String [] {"gpil|Europe",			"4.805",	"1.343",	"GPI_ST",	"1"}),
		(new String [] {"gpil|Europe",			"6.727",	"1.11",		"GPI_TA",	"1"}),
		(new String [] {"gpil|Europe",			"6.312",	"1.515",	"GPI_TC",	"1"}),
		(new String [] {"gpil|Europe",			"4.704",	"0.895",	"GPI_TF",	"1"}),
		(new String [] {"gpil|Europe",			"4.563",	"1.097",	"GPI_TR",	"1"}),
		(new String [] {"gpil|Europe",			"5.561",	"1.136",	"GPI_VIS",	"1"}),
		(new String [] {"gpil|Europe",			"6.137",	"1.267",	"GPI_WF",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"4.965",	"1.418",	"GPI_AD",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"5.477",	"0.917",	"GPI_ADPT",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"5.325",	"0.823",	"GPI_AST",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"4.622",	"1.169",	"GPI_COMP",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"7.041",	"1.189",	"GPI_CONS",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"5.927",	"1.059",	"GPI_DACH",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"4.135",	"1.03",		"GPI_DADV",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"5.462",	"1.064",	"GPI_DUT",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"4.64",		"0.979",	"GPI_EC",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"2.764",	"0.896",	"GPI_EGO",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"6.504",	"1.112",	"GPI_EL",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"4.733",	"0.854",	"GPI_EMP",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"3.637",	"0.856",	"GPI_IMPR",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"3.243",	"0.817",	"GPI_IND",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"6.51",		"0.944",	"GPI_INFL",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"6.331",	"0.928",	"GPI_INIT",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"6.603",	"1.163",	"GPI_INOV",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"5.447",	"0.976",	"GPI_INTD",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"2.192",	"0.9",		"GPI_INTI",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"3.386",	"1.176",	"GPI_MAN",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"1.859",	"0.799",	"GPI_MIC",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"1.35",		"0.69",		"GPI_NA",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"4.722",	"0.867",	"GPI_OPEN",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"6.653",	"1.081",	"GPI_OPT",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"1.97",		"0.799",	"GPI_PA",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"5.825",	"0.764",	"GPI_RESP",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"6.16",		"1.128",	"GPI_RISK",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"7.154",	"0.866",	"GPI_SASI",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"5.042",	"0.727",	"GPI_SC",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"6.56",		"1.302",	"GPI_SO",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"5.373",	"1.15",		"GPI_ST",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"6.907",	"0.997",	"GPI_TA",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"7.63",		"1.097",	"GPI_TC",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"5.264",	"0.739",	"GPI_TF",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"5.193",	"0.716",	"GPI_TR",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"6.164",	"1.057",	"GPI_VIS",	"1"}),
		(new String [] {"gpil|European Exec., Sr. Exec, and CEO",	"6.473",	"1.1",		"GPI_WF",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"5.18",		"1.276",	"GPI_AD",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"5.324",	"0.846",	"GPI_ADPT",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"4.965",	"0.899",	"GPI_AST",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"3.91",		"1.092",	"GPI_COMP",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"7.03",		"1.152",	"GPI_CONS",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"5.368",	"1.093",	"GPI_DACH",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"3.464",	"1.199",	"GPI_DADV",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"5.047",	"1.067",	"GPI_DUT",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"4.273",	"1.039",	"GPI_EC",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"2.521",	"0.9",		"GPI_EGO",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"5.889",	"1.005",	"GPI_EL",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"4.683",	"0.803",	"GPI_EMP",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"3.4",		"0.954",	"GPI_IMPR",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"3.271",	"0.816",	"GPI_IND",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"5.92",		"1.025",	"GPI_INFL",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"5.99",		"1.038",	"GPI_INIT",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"6.29",		"1.145",	"GPI_INOV",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"5.302",	"0.98",		"GPI_INTD",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"1.819",	"0.861",	"GPI_INTI",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"3.116",	"1.147",	"GPI_MAN",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"1.796",	"0.743",	"GPI_MIC",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"1.445",	"0.706",	"GPI_NA",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"4.618",	"0.873",	"GPI_OPEN",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"6.455",	"0.953",	"GPI_OPT",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"1.995",	"0.894",	"GPI_PA",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"5.597",	"0.741",	"GPI_RESP",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"5.498",	"1.207",	"GPI_RISK",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"6.841",	"0.975",	"GPI_SASI",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"4.8",		"0.804",	"GPI_SC",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"6.492",	"1.316",	"GPI_SO",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"5.074",	"1.123",	"GPI_ST",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"6.845",	"0.925",	"GPI_TA",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"6.922",	"1.162",	"GPI_TC",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"4.804",	"0.831",	"GPI_TF",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"5.269",	"0.836",	"GPI_TR",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"5.612",	"1.148",	"GPI_VIS",	"1"}),
		(new String [] {"gpil|European First-Level Leader",	"6.204",	"1.035",	"GPI_WF",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"6.145",	"1.475",	"GPI_AD",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"4.817",	"1.161",	"GPI_ADPT",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"5.091",	"1.009",	"GPI_AST",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"3.067",	"1.188",	"GPI_COMP",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"7.755",	"1.426",	"GPI_CONS",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"4.53",		"1.171",	"GPI_DACH",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"3.653",	"1.067",	"GPI_DADV",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"5.425",	"1.232",	"GPI_DUT",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"4.223",	"1.077",	"GPI_EC",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"2.761",	"1.016",	"GPI_EGO",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"5.417",	"1.32",		"GPI_EL",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"4.844",	"0.968",	"GPI_EMP",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"4.522",	"1.207",	"GPI_IMPR",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"3.866",	"1.075",	"GPI_IND",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"4.978",	"1.22",		"GPI_INFL",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"5.632",	"1.249",	"GPI_INIT",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"5.882",	"1.315",	"GPI_INOV",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"4.761",	"0.952",	"GPI_INTD",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"2.573",	"0.959",	"GPI_INTI",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"3.132",	"1.415",	"GPI_MAN",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"2.554",	"0.946",	"GPI_MIC",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"2.336",	"0.994",	"GPI_NA",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"4.712",	"0.974",	"GPI_OPEN",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"6.086",	"1.379",	"GPI_OPT",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"2.968",	"0.996",	"GPI_PA",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"5.535",	"0.894",	"GPI_RESP",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"5.156",	"1.392",	"GPI_RISK",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"6.64",		"1.173",	"GPI_SASI",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"4.465",	"0.938",	"GPI_SC",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"5.645",	"1.402",	"GPI_SO",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"4.57",		"1.239",	"GPI_ST",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"6.726",	"1.36",		"GPI_TA",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"5.535",	"1.412",	"GPI_TC",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"4.457",	"0.96",		"GPI_TF",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"4.242",	"1.2",		"GPI_TR",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"5.14",		"1.196",	"GPI_VIS",	"1"}),
		(new String [] {"gpil|European Individual Contributor",	"5.933",	"1.27",		"GPI_WF",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"5.11",		"1.342",	"GPI_AD",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"5.461",	"0.857",	"GPI_ADPT",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"5.206",	"0.817",	"GPI_AST",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"4.323",	"1.107",	"GPI_COMP",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"7.066",	"1.152",	"GPI_CONS",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"5.843",	"1.067",	"GPI_DACH",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"4.021",	"1.013",	"GPI_DADV",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"5.262",	"1.043",	"GPI_DUT",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"4.432",	"1.008",	"GPI_EC",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"2.858",	"0.803",	"GPI_EGO",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"6.238",	"1.002",	"GPI_EL",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"4.726",	"0.829",	"GPI_EMP",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"3.648",	"0.847",	"GPI_IMPR",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"3.323",	"0.843",	"GPI_IND",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"6.215",	"0.939",	"GPI_INFL",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"6.304",	"0.957",	"GPI_INIT",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"6.496",	"1.077",	"GPI_INOV",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"5.395",	"1.035",	"GPI_INTD",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"2.054",	"0.836",	"GPI_INTI",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"3.453",	"1.239",	"GPI_MAN",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"1.845",	"0.799",	"GPI_MIC",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"1.549",	"0.76",		"GPI_NA",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"4.668",	"0.858",	"GPI_OPEN",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"6.485",	"1.008",	"GPI_OPT",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"2.055",	"0.855",	"GPI_PA",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"5.684",	"0.807",	"GPI_RESP",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"6.019",	"1.216",	"GPI_RISK",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"7.008",	"0.946",	"GPI_SASI",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"4.965",	"0.772",	"GPI_SC",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"6.41",		"1.353",	"GPI_SO",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"5.061",	"1.161",	"GPI_ST",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"6.897",	"0.96",		"GPI_TA",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"7.35",		"1.056",	"GPI_TC",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"5.108",	"0.775",	"GPI_TF",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"5.18",		"0.831",	"GPI_TR",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"6.042",	"0.996",	"GPI_VIS",	"1"}),
		(new String [] {"gpil|European Mid-Level Leader",	"6.282",	"1.206",	"GPI_WF",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"5.01",		"1.25",		"GPI_AD",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"5.53",		"0.85",		"GPI_ADPT",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"5.3",		"0.82",		"GPI_AST",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"4.59",		"1.04",		"GPI_COMP",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"7.55",		"1.09",		"GPI_CONS",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"6.39",		"0.88",		"GPI_DACH",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"4.07",		"0.95",		"GPI_DADV",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"6.08",		"0.89",		"GPI_DUT",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"4.83",		"0.9",		"GPI_EC",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"2.69",		"0.86",		"GPI_EGO",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"6.7",		"1.07",		"GPI_EL",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"4.9",		"0.79",		"GPI_EMP",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"3.7",		"0.67",		"GPI_IMPR",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"3.21",		"0.83",		"GPI_IND",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"6.57",		"0.94",		"GPI_INFL",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"6.41",		"0.96",		"GPI_INIT",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"6.67",		"1.06",		"GPI_INOV",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"5.5",		"0.96",		"GPI_INTD",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"1.85",		"0.81",		"GPI_INTI",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"2.97",		"1.11",		"GPI_MAN",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"1.6",		"0.73",		"GPI_MIC",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"1.231",	"0.743",	"GPI_NA",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"4.61",		"0.88",		"GPI_OPEN",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"6.86",		"1.06",		"GPI_OPT",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"1.72",		"0.79",		"GPI_PA",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"5.9",		"0.78",		"GPI_RESP",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"6.35",		"1.08",		"GPI_RISK",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"7.32",		"0.91",		"GPI_SASI",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"5.35",		"0.73",		"GPI_SC",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"6.38",		"1.42",		"GPI_SO",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"5.2",		"1.12",		"GPI_ST",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"7.04",		"0.93",		"GPI_TA",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"7.68",		"1.05",		"GPI_TC",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"5.34",		"0.74",		"GPI_TF",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"5.57",		"0.72",		"GPI_TR",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"6.57",		"0.95",		"GPI_VIS",	"1"}),
		(new String [] {"gpil|Freddie Mac Sr Executive",	"6.42",		"1.06",		"GPI_WF",	"1"}),
		(new String [] {"gpil|Global CEO",		"5.35",	"1.42",	"GPI_AD",	"1"}),
		(new String [] {"gpil|Global CEO",		"5.45",	"0.82",	"GPI_ADPT",	"1"}),
		(new String [] {"gpil|Global CEO",		"5.35",	"0.86",	"GPI_AST",	"1"}),
		(new String [] {"gpil|Global CEO",		"4.5",	"1.04",	"GPI_COMP",	"1"}),
		(new String [] {"gpil|Global CEO",		"7.69",	"1.18",	"GPI_CONS",	"1"}),
		(new String [] {"gpil|Global CEO",		"6.35",	"0.87",	"GPI_DACH",	"1"}),
		(new String [] {"gpil|Global CEO",		"4.08",	"0.96",	"GPI_DADV",	"1"}),
		(new String [] {"gpil|Global CEO",		"6.18",	"0.9",	"GPI_DUT",	"1"}),
		(new String [] {"gpil|Global CEO",		"4.84",	"0.96",	"GPI_EC",	"1"}),
		(new String [] {"gpil|Global CEO",		"2.68",	"0.92",	"GPI_EGO",	"1"}),
		(new String [] {"gpil|Global CEO",		"6.71",	"1.05",	"GPI_EL",	"1"}),
		(new String [] {"gpil|Global CEO",		"4.96",	"0.86",	"GPI_EMP",	"1"}),
		(new String [] {"gpil|Global CEO",		"3.77",	"0.8",	"GPI_IMPR",	"1"}),
		(new String [] {"gpil|Global CEO",		"3.34",	"0.93",	"GPI_IND",	"1"}),
		(new String [] {"gpil|Global CEO",		"6.69",	"0.93",	"GPI_INFL",	"1"}),
		(new String [] {"gpil|Global CEO",		"6.47",	"0.93",	"GPI_INIT",	"1"}),
		(new String [] {"gpil|Global CEO",		"6.79",	"1.09",	"GPI_INOV",	"1"}),
		(new String [] {"gpil|Global CEO",		"5.56",	"1.06",	"GPI_INTD",	"1"}),
		(new String [] {"gpil|Global CEO",		"1.88",	"0.85",	"GPI_INTI",	"1"}),
		(new String [] {"gpil|Global CEO",		"2.87",	"1.17",	"GPI_MAN",	"1"}),
		(new String [] {"gpil|Global CEO",		"1.7",	"0.77",	"GPI_MIC",	"1"}),
		(new String [] {"gpil|Global CEO",		"1.18",	"0.7",	"GPI_NA",	"1"}),
		(new String [] {"gpil|Global CEO",		"4.73",	"0.96",	"GPI_OPEN",	"1"}),
		(new String [] {"gpil|Global CEO",		"6.91",	"1",	"GPI_OPT",	"1"}),
		(new String [] {"gpil|Global CEO",		"1.8",	"0.83",	"GPI_PA",	"1"}),
		(new String [] {"gpil|Global CEO",		"5.99",	"0.78",	"GPI_RESP",	"1"}),
		(new String [] {"gpil|Global CEO",		"6.2",	"1.26",	"GPI_RISK",	"1"}),
		(new String [] {"gpil|Global CEO",		"7.46",	"0.88",	"GPI_SASI",	"1"}),
		(new String [] {"gpil|Global CEO",		"5.39",	"0.74",	"GPI_SC",	"1"}),
		(new String [] {"gpil|Global CEO",		"6.59",	"1.4",	"GPI_SO",	"1"}),
		(new String [] {"gpil|Global CEO",		"5.29",	"1.11",	"GPI_ST",	"1"}),
		(new String [] {"gpil|Global CEO",		"7.22",	"0.93",	"GPI_TA",	"1"}),
		(new String [] {"gpil|Global CEO",		"7.89",	"1.07",	"GPI_TC",	"1"}),
		(new String [] {"gpil|Global CEO",		"5.42",	"0.77",	"GPI_TF",	"1"}),
		(new String [] {"gpil|Global CEO",		"5.5",	"0.77",	"GPI_TR",	"1"}),
		(new String [] {"gpil|Global CEO",		"6.68",	"1.05",	"GPI_VIS",	"1"}),
		(new String [] {"gpil|Global CEO",		"6.59",	"1.12",	"GPI_WF",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"5.31",	"1.33",	"GPI_AD",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"5.46",	"0.84",	"GPI_ADPT",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"5.37",	"0.84",	"GPI_AST",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"4.37",	"1.07",	"GPI_COMP",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"7.52",	"1.15",	"GPI_CONS",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"6.29",	"0.86",	"GPI_DACH",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"3.96",	"0.94",	"GPI_DADV",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"6.09",	"0.9",	"GPI_DUT",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"4.87",	"0.97",	"GPI_EC",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"2.66",	"0.85",	"GPI_EGO",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"6.55",	"1.04",	"GPI_EL",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"4.89",	"0.83",	"GPI_EMP",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"3.77",	"0.75",	"GPI_IMPR",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"3.19",	"0.89",	"GPI_IND",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"6.52",	"0.89",	"GPI_INFL",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"6.42",	"0.9",	"GPI_INIT",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"6.66",	"1.08",	"GPI_INOV",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"5.56",	"0.97",	"GPI_INTD",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"1.89",	"0.84",	"GPI_INTI",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"2.88",	"1.14",	"GPI_MAN",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"1.72",	"0.8",	"GPI_MIC",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"1.23",	"0.71",	"GPI_NA",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"4.75",	"0.92",	"GPI_OPEN",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"6.83",	"0.99",	"GPI_OPT",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"1.79",	"0.83",	"GPI_PA",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"5.95",	"0.74",	"GPI_RESP",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"6.08",	"1.12",	"GPI_RISK",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"7.35",	"0.87",	"GPI_SASI",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"5.33",	"0.73",	"GPI_SC",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"6.38",	"1.44",	"GPI_SO",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"5.24",	"1.11",	"GPI_ST",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"7.16",	"0.9",	"GPI_TA",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"7.63",	"1.01",	"GPI_TC",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"5.36",	"0.75",	"GPI_TF",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"5.47",	"0.78",	"GPI_TR",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"6.56",	"0.99",	"GPI_VIS",	"1"}),
		(new String [] {"gpil|Global Senior Executive",	"6.47",	"1.1",	"GPI_WF",	"1"}),
		(new String [] {"gpil|Japan",	"5.442",	"1.267",	"GPI_AD",	"1"}),
		(new String [] {"gpil|Japan",	"4.778",	"1.053",	"GPI_ADPT",	"1"}),
		(new String [] {"gpil|Japan",	"4.907",	"1.031",	"GPI_AST",	"1"}),
		(new String [] {"gpil|Japan",	"4.073",	"1.184",	"GPI_COMP",	"1"}),
		(new String [] {"gpil|Japan",	"7.442",	"1.117",	"GPI_CONS",	"1"}),
		(new String [] {"gpil|Japan",	"5.805",	"1.091",	"GPI_DACH",	"1"}),
		(new String [] {"gpil|Japan",	"3.216",	"1.104",	"GPI_DADV",	"1"}),
		(new String [] {"gpil|Japan",	"5.066",	"1.116",	"GPI_DUT",	"1"}),
		(new String [] {"gpil|Japan",	"3.95",		"1.12",		"GPI_EC",	"1"}),
		(new String [] {"gpil|Japan",	"2.926",	"0.896",	"GPI_EGO",	"1"}),
		(new String [] {"gpil|Japan",	"5.154",	"1.323",	"GPI_EL",	"1"}),
		(new String [] {"gpil|Japan",	"4.417",	"0.881",	"GPI_EMP",	"1"}),
		(new String [] {"gpil|Japan",	"3.4",		"0.83",		"GPI_IMPR",	"1"}),
		(new String [] {"gpil|Japan",	"3.812",	"0.944",	"GPI_IND",	"1"}),
		(new String [] {"gpil|Japan",	"5.36",		"1.302",	"GPI_INFL",	"1"}),
		(new String [] {"gpil|Japan",	"5.472",	"1.216",	"GPI_INIT",	"1"}),
		(new String [] {"gpil|Japan",	"5.719",	"1.469",	"GPI_INOV",	"1"}),
		(new String [] {"gpil|Japan",	"4.838",	"1.035",	"GPI_INTD",	"1"}),
		(new String [] {"gpil|Japan",	"2.148",	"0.854",	"GPI_INTI",	"1"}),
		(new String [] {"gpil|Japan",	"4.473",	"1.238",	"GPI_MAN",	"1"}),
		(new String [] {"gpil|Japan",	"2.339",	"0.79",		"GPI_MIC",	"1"}),
		(new String [] {"gpil|Japan",	"2.246",	"0.814",	"GPI_NA",	"1"}),
		(new String [] {"gpil|Japan",	"4.076",	"1.031",	"GPI_OPEN",	"1"}),
		(new String [] {"gpil|Japan",	"5.361",	"1.227",	"GPI_OPT",	"1"}),
		(new String [] {"gpil|Japan",	"3.005",	"0.818",	"GPI_PA",	"1"}),
		(new String [] {"gpil|Japan",	"4.903",	"0.875",	"GPI_RESP",	"1"}),
		(new String [] {"gpil|Japan",	"5.532",	"1.206",	"GPI_RISK",	"1"}),
		(new String [] {"gpil|Japan",	"6.4",		"1.068",	"GPI_SASI",	"1"}),
		(new String [] {"gpil|Japan",	"4.198",	"0.958",	"GPI_SC",	"1"}),
		(new String [] {"gpil|Japan",	"5.293",	"1.531",	"GPI_SO",	"1"}),
		(new String [] {"gpil|Japan",	"4.273",	"1.329",	"GPI_ST",	"1"}),
		(new String [] {"gpil|Japan",	"6.76",		"1.061",	"GPI_TA",	"1"}),
		(new String [] {"gpil|Japan",	"6.139",	"1.52",		"GPI_TC",	"1"}),
		(new String [] {"gpil|Japan",	"4.221",	"0.979",	"GPI_TF",	"1"}),
		(new String [] {"gpil|Japan",	"4.915",	"0.852",	"GPI_TR",	"1"}),
		(new String [] {"gpil|Japan",	"5.563",	"1.275",	"GPI_VIS",	"1"}),
		(new String [] {"gpil|Japan",	"5.58",		"1.248",	"GPI_WF",	"1"}),
		(new String [] {"gpil|Japan All Management",	"5.322",	"1.442",	"GPI_AD",	"1"}),
		(new String [] {"gpil|Japan All Management",	"4.868",	"1.098",	"GPI_ADPT",	"1"}),
		(new String [] {"gpil|Japan All Management",	"4.954",	"1.133",	"GPI_AST",	"1"}),
		(new String [] {"gpil|Japan All Management",	"4.136",	"1.165",	"GPI_COMP",	"1"}),
		(new String [] {"gpil|Japan All Management",	"7.458",	"1.28",		"GPI_CONS",	"1"}),
		(new String [] {"gpil|Japan All Management",	"5.94",		"1.096",	"GPI_DACH",	"1"}),
		(new String [] {"gpil|Japan All Management",	"3.306",	"1.091",	"GPI_DADV",	"1"}),
		(new String [] {"gpil|Japan All Management",	"5.312",	"1.156",	"GPI_DUT",	"1"}),
		(new String [] {"gpil|Japan All Management",	"4.121",	"1.19",		"GPI_EC",	"1"}),
		(new String [] {"gpil|Japan All Management",	"2.99",		"0.901",	"GPI_EGO",	"1"}),
		(new String [] {"gpil|Japan All Management",	"5.468",	"1.299",	"GPI_EL",	"1"}),
		(new String [] {"gpil|Japan All Management",	"4.463",	"0.959",	"GPI_EMP",	"1"}),
		(new String [] {"gpil|Japan All Management",	"3.521",	"0.809",	"GPI_IMPR",	"1"}),
		(new String [] {"gpil|Japan All Management",	"3.804",	"0.924",	"GPI_IND",	"1"}),
		(new String [] {"gpil|Japan All Management",	"5.763",	"1.3",		"GPI_INFL",	"1"}),
		(new String [] {"gpil|Japan All Management",	"5.663",	"1.184",	"GPI_INIT",	"1"}),
		(new String [] {"gpil|Japan All Management",	"5.951",	"1.432",	"GPI_INOV",	"1"}),
		(new String [] {"gpil|Japan All Management",	"4.973",	"1.1",		"GPI_INTD",	"1"}),
		(new String [] {"gpil|Japan All Management",	"2.245",	"0.867",	"GPI_INTI",	"1"}),
		(new String [] {"gpil|Japan All Management",	"4.475",	"1.304",	"GPI_MAN",	"1"}),
		(new String [] {"gpil|Japan All Management",	"2.213",	"0.783",	"GPI_MIC",	"1"}),
		(new String [] {"gpil|Japan All Management",	"2.059",	"0.778",	"GPI_NA",	"1"}),
		(new String [] {"gpil|Japan All Management",	"4.14",		"1.056",	"GPI_OPEN",	"1"}),
		(new String [] {"gpil|Japan All Management",	"5.517",	"1.247",	"GPI_OPT",	"1"}),
		(new String [] {"gpil|Japan All Management",	"2.927",	"0.844",	"GPI_PA",	"1"}),
		(new String [] {"gpil|Japan All Management",	"5.015",	"0.902",	"GPI_RESP",	"1"}),
		(new String [] {"gpil|Japan All Management",	"5.699",	"1.272",	"GPI_RISK",	"1"}),
		(new String [] {"gpil|Japan All Management",	"6.567",	"1.085",	"GPI_SASI",	"1"}),
		(new String [] {"gpil|Japan All Management",	"4.236",	"0.996",	"GPI_SC",	"1"}),
		(new String [] {"gpil|Japan All Management",	"5.393",	"1.547",	"GPI_SO",	"1"}),
		(new String [] {"gpil|Japan All Management",	"4.464",	"1.377",	"GPI_ST",	"1"}),
		(new String [] {"gpil|Japan All Management",	"6.84",		"1.163",	"GPI_TA",	"1"}),
		(new String [] {"gpil|Japan All Management",	"6.694",	"1.423",	"GPI_TC",	"1"}),
		(new String [] {"gpil|Japan All Management",	"4.443",	"0.963",	"GPI_TF",	"1"}),
		(new String [] {"gpil|Japan All Management",	"4.939",	"0.929",	"GPI_TR",	"1"}),
		(new String [] {"gpil|Japan All Management",	"5.869",	"1.266",	"GPI_VIS",	"1"}),
		(new String [] {"gpil|Japan All Management",	"5.745",	"1.306",	"GPI_WF",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"5.447",	"1.212",	"GPI_AD",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"4.763",	"1.028",	"GPI_ADPT",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"4.973",	"0.965",	"GPI_AST",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"4.117",	"1.138",	"GPI_COMP",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"7.447",	"1.038",	"GPI_CONS",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"5.798",	"1.038",	"GPI_DACH",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"3.208",	"1.079",	"GPI_DADV",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"5.037",	"1.072",	"GPI_DUT",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"3.889",	"1.094",	"GPI_EC",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"2.906",	"0.882",	"GPI_EGO",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"5.083",	"1.271",	"GPI_EL",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"4.409",	"0.848",	"GPI_EMP",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"3.375",	"0.812",	"GPI_IMPR",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"3.825",	"0.97",		"GPI_IND",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"5.375",	"1.221",	"GPI_INFL",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"5.465",	"1.189",	"GPI_INIT",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"5.624",	"1.448",	"GPI_INOV",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"4.849",	"1.01",		"GPI_INTD",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"2.151",	"0.872",	"GPI_INTI",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"4.503",	"1.198",	"GPI_MAN",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"2.375",	"0.791",	"GPI_MIC",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"2.308",	"0.806",	"GPI_NA",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"4.046",	"1.009",	"GPI_OPEN",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"5.298",	"1.181",	"GPI_OPT",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"3.003",	"0.794",	"GPI_PA",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"4.885",	"0.836",	"GPI_RESP",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"5.522",	"1.099",	"GPI_RISK",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"6.386",	"1.041",	"GPI_SASI",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"4.181",	"0.896",	"GPI_SC",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"5.232",	"1.519",	"GPI_SO",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"4.244",	"1.311",	"GPI_ST",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"6.708",	"1.007",	"GPI_TA",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"6.162",	"1.375",	"GPI_TC",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"4.172",	"0.936",	"GPI_TF",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"4.882",	"0.802",	"GPI_TR",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"5.529",	"1.182",	"GPI_VIS",	"1"}),
		(new String [] {"gpil|Japan First-Level Leader",	"5.491",	"1.208",	"GPI_WF",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"5.53",		"1.173",	"GPI_AD",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"4.674",	"1.077",	"GPI_ADPT",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"4.818",	"1.031",	"GPI_AST",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"3.969",	"1.265",	"GPI_COMP",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"7.419",	"1.067",	"GPI_CONS",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"5.684",	"1.181",	"GPI_DACH",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"3.157",	"1.181",	"GPI_DADV",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"4.953",	"1.105",	"GPI_DUT",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"3.878",	"1.109",	"GPI_EC",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"2.9",		"0.917",	"GPI_EGO",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"4.974",	"1.41",		"GPI_EL",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"4.398",	"0.869",	"GPI_EMP",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"3.357",	"0.839",	"GPI_IMPR",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"3.79",		"0.942",	"GPI_IND",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"5.052",	"1.373",	"GPI_INFL",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"5.351",	"1.304",	"GPI_INIT",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"5.637",	"1.517",	"GPI_INOV",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"4.722",	"1.016",	"GPI_INTD",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"2.097",	"0.817",	"GPI_INTI",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"4.466",	"1.251",	"GPI_MAN",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"2.368",	"0.777",	"GPI_MIC",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"2.337",	"0.834",	"GPI_NA",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"4.024",	"1.054",	"GPI_OPEN",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"5.289",	"1.284",	"GPI_OPT",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"3.067",	"0.825",	"GPI_PA",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"4.87",		"0.907",	"GPI_RESP",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"5.401",	"1.307",	"GPI_RISK",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"6.324",	"1.071",	"GPI_SASI",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"4.18",		"1.038",	"GPI_SC",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"5.307",	"1.537",	"GPI_SO",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"4.174",	"1.332",	"GPI_ST",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"6.757",	"1.047",	"GPI_TA",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"5.702",	"1.659",	"GPI_TC",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"4.136",	"1.029",	"GPI_TF",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"4.924",	"0.854",	"GPI_TR",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"5.396",	"1.37",		"GPI_VIS",	"1"}),
		(new String [] {"gpil|Japan Individual Contributor",	"5.545",	"1.27",		"GPI_WF",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"6.2228",	"1.2354",	"GPI_AD",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"5.0512",	"0.8522",	"GPI_ADPT",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"5.2588",	"0.9456",	"GPI_AST",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"4.3054",	"1.2171",	"GPI_COMP",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"7.3994",	"1.2663",	"GPI_CONS",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"6.1688",	"0.9466",	"GPI_DACH",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"4.3487",	"1.0134",	"GPI_DADV",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"5.952",	"1.0336",	"GPI_DUT",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"4.2011",	"1.0828",	"GPI_EC",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"2.7929",	"0.9524",	"GPI_EGO",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"6.3233",	"1.1707",	"GPI_EL",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"4.798",	"0.8849",	"GPI_EMP",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"3.8653",	"0.8038",	"GPI_IMPR",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"3.0498",	"0.9281",	"GPI_IND",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"6.1956",	"1.0031",	"GPI_INFL",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"6.5374",	"0.9895",	"GPI_INIT",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"6.6038",	"1.1679",	"GPI_INOV",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"5.7731",	"1.0591",	"GPI_INTD",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"1.8658",	"0.8956",	"GPI_INTI",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"2.5867",	"1.2914",	"GPI_MAN",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"1.9216",	"0.8494",	"GPI_MIC",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"1.3939",	"0.7828",	"GPI_NA",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"4.7246",	"1.0476",	"GPI_OPEN",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"6.8307",	"0.97",		"GPI_OPT",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"1.8653",	"0.8882",	"GPI_PA",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"5.9262",	"0.8289",	"GPI_RESP",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"6.0673",	"1.2515",	"GPI_RISK",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"7.2113",	"1.0017",	"GPI_SASI",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"5.2016",	"0.7667",	"GPI_SC",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"6.3865",	"1.4146",	"GPI_SO",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"4.5088",	"1.1857",	"GPI_ST",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"7.03",		"0.9889",	"GPI_TA",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"7.3676",	"1.1563",	"GPI_TC",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"5.1365",	"0.8287",	"GPI_TF",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"5.0932",	"0.834",	"GPI_TR",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"6.226",	"1.1148",	"GPI_VIS",	"1"}),
		(new String [] {"gpil|Latin America All Management",	"6.2057",	"1.2928",	"GPI_WF",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"6.1225",	"1.256669",	"GPI_AD",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"5.094167",	"0.898768",	"GPI_ADPT",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"5.39",		"0.869388",	"GPI_AST",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"4.4875",	"1.069907",	"GPI_COMP",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"7.471667",	"1.137325",	"GPI_CONS",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"6.114167",	"1.037592",	"GPI_DACH",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"4.73",		"0.891721",	"GPI_DADV",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"5.568333",	"0.890491",	"GPI_DUT",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"4.299167",	"1.016317",	"GPI_EC",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"3.291667",	"0.896349",	"GPI_EGO",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"6.004167",	"1.125015",	"GPI_EL",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"5.040833",	"0.834684",	"GPI_EMP",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"4.4083",	"0.924362",	"GPI_IMPR",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"3.525833",	"0.977142",	"GPI_IND",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"6.116667",	"1.035194",	"GPI_INFL",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"6.338333",	"1.023199",	"GPI_INIT",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"6.495833",	"1.0051",	"GPI_INOV",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"5.695833",	"1.04886",	"GPI_INTD",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"2.231667",	"0.989745",	"GPI_INTI",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"3.88",		"1.337246",	"GPI_MAN",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"2.2125",	"0.942787",	"GPI_MIC",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"1.845",	"0.784822",	"GPI_NA",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"4.853333",	"0.933709",	"GPI_OPEN",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"6.524167",	"1.018043",	"GPI_OPT",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"2.556667",	"0.93838",	"GPI_PA",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"5.65",		"0.848962",	"GPI_RESP",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"5.780833",	"1.175314",	"GPI_RISK",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"7.24",		"1.033057",	"GPI_SASI",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"5.085",	"0.7731",	"GPI_SC",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"6.253333",	"1.299597",	"GPI_SO",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"4.538333",	"1.146186",	"GPI_ST",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"7.190833",	"1.051575",	"GPI_TA",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"6.955833",	"1.289075",	"GPI_TC",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"4.925",	"0.880507",	"GPI_TF",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"5.148333",	"0.86776",	"GPI_TR",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"6.294167",	"1.090936",	"GPI_VIS",	"1"}),
		(new String [] {"gpil|Middle East All Management",	"6.298333",	"1.128899",	"GPI_WF",	"1"}),
		(new String [] {"gpil|NA Executive",	"5.034",	"1.252",	"GPI_AD",	"1"}),
		(new String [] {"gpil|NA Executive",	"5.492",	"0.839",	"GPI_ADPT",	"1"}),
		(new String [] {"gpil|NA Executive",	"5.266",	"0.843",	"GPI_AST",	"1"}),
		(new String [] {"gpil|NA Executive",	"4.608",	"1.07",		"GPI_COMP",	"1"}),
		(new String [] {"gpil|NA Executive",	"7.494",	"1.072",	"GPI_CONS",	"1"}),
		(new String [] {"gpil|NA Executive",	"6.363",	"0.886",	"GPI_DACH",	"1"}),
		(new String [] {"gpil|NA Executive",	"3.997",	"0.941",	"GPI_DADV",	"1"}),
		(new String [] {"gpil|NA Executive",	"5.874",	"0.905",	"GPI_DUT",	"1"}),
		(new String [] {"gpil|NA Executive",	"4.829",	"0.89",		"GPI_EC",	"1"}),
		(new String [] {"gpil|NA Executive",	"2.741",	"0.837",	"GPI_EGO",	"1"}),
		(new String [] {"gpil|NA Executive",	"6.543",	"1.073",	"GPI_EL",	"1"}),
		(new String [] {"gpil|NA Executive",	"4.904",	"0.788",	"GPI_EMP",	"1"}),
		(new String [] {"gpil|NA Executive",	"3.672",	"0.704",	"GPI_IMPR",	"1"}),
		(new String [] {"gpil|NA Executive",	"3.308",	"0.836",	"GPI_IND",	"1"}),
		(new String [] {"gpil|NA Executive",	"6.524",	"0.947",	"GPI_INFL",	"1"}),
		(new String [] {"gpil|NA Executive",	"6.358",	"0.934",	"GPI_INIT",	"1"}),
		(new String [] {"gpil|NA Executive",	"6.624",	"1.096",	"GPI_INOV",	"1"}),
		(new String [] {"gpil|NA Executive",	"5.49",		"1.01",		"GPI_INTD",	"1"}),
		(new String [] {"gpil|NA Executive",	"1.9",		"0.826",	"GPI_INTI",	"1"}),
		(new String [] {"gpil|NA Executive",	"3.098",	"1.101",	"GPI_MAN",	"1"}),
		(new String [] {"gpil|NA Executive",	"1.564",	"0.717",	"GPI_MIC",	"1"}),
		(new String [] {"gpil|NA Executive",	"1.291",	"0.728",	"GPI_NA",	"1"}),
		(new String [] {"gpil|NA Executive",	"4.564",	"0.88",		"GPI_OPEN",	"1"}),
		(new String [] {"gpil|NA Executive",	"6.814",	"1.041",	"GPI_OPT",	"1"}),
		(new String [] {"gpil|NA Executive",	"1.859",	"0.801",	"GPI_PA",	"1"}),
		(new String [] {"gpil|NA Executive",	"5.882",	"0.803",	"GPI_RESP",	"1"}),
		(new String [] {"gpil|NA Executive",	"6.299",	"1.088",	"GPI_RISK",	"1"}),
		(new String [] {"gpil|NA Executive",	"7.254",	"0.914",	"GPI_SASI",	"1"}),
		(new String [] {"gpil|NA Executive",	"5.354",	"0.739",	"GPI_SC",	"1"}),
		(new String [] {"gpil|NA Executive",	"6.298",	"1.434",	"GPI_SO",	"1"}),
		(new String [] {"gpil|NA Executive",	"5.207",	"1.119",	"GPI_ST",	"1"}),
		(new String [] {"gpil|NA Executive",	"7.015",	"0.929",	"GPI_TA",	"1"}),
		(new String [] {"gpil|NA Executive",	"7.646",	"1.062",	"GPI_TC",	"1"}),
		(new String [] {"gpil|NA Executive",	"5.279",	"0.754",	"GPI_TF",	"1"}),
		(new String [] {"gpil|NA Executive",	"5.535",	"0.723",	"GPI_TR",	"1"}),
		(new String [] {"gpil|NA Executive",	"6.499",	"0.959",	"GPI_VIS",	"1"}),
		(new String [] {"gpil|NA Executive",	"6.37",		"1.058",	"GPI_WF",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"5.566",	"1.21",		"GPI_AD",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"5.351",	"0.892",	"GPI_ADPT",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"5.183",	"0.829",	"GPI_AST",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"4.364",	"1.132",	"GPI_COMP",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"7.636",	"1.051",	"GPI_CONS",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"6.081",	"0.931",	"GPI_DACH",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"3.762",	"1.059",	"GPI_DADV",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"5.758",	"0.872",	"GPI_DUT",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"4.825",	"0.965",	"GPI_EC",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"2.887",	"0.818",	"GPI_EGO",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"6.118",	"1.073",	"GPI_EL",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"4.957",	"0.794",	"GPI_EMP",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"3.836",	"0.692",	"GPI_IMPR",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"3.49",		"0.821",	"GPI_IND",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"6.093",	"1.012",	"GPI_INFL",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"6.24",		"0.954",	"GPI_INIT",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"6.439",	"1.103",	"GPI_INOV",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"5.164",	"0.989",	"GPI_INTD",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"1.839",	"0.772",	"GPI_INTI",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"3.262",	"1.058",	"GPI_MAN",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"1.886",	"0.726",	"GPI_MIC",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"1.415",	"0.735",	"GPI_NA",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"4.796",	"0.841",	"GPI_OPEN",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"6.637",	"1.044",	"GPI_OPT",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"2.137",	"0.763",	"GPI_PA",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"5.841",	"0.72",		"GPI_RESP",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"5.935",	"1.174",	"GPI_RISK",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"7.137",	"0.92",		"GPI_SASI",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"5.299",	"0.71",		"GPI_SC",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"6.374",	"1.383",	"GPI_SO",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"5.094",	"1.133",	"GPI_ST",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"6.979",	"0.864",	"GPI_TA",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"7.237",	"1.071",	"GPI_TC",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"5.024",	"0.77",		"GPI_TF",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"5.407",	"0.68",		"GPI_TR",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"6.247",	"1.008",	"GPI_VIS",	"1"}),
		(new String [] {"gpil|NA First-Level Leader",	"6.316",	"1.089",	"GPI_WF",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"5.744",	"1.204",	"GPI_AD",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"5.362",	"0.847",	"GPI_ADPT",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"5.118",	"0.869",	"GPI_AST",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"4.253",	"1.151",	"GPI_COMP",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"7.802",	"1.006",	"GPI_CONS",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"6.109",	"0.918",	"GPI_DACH",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"3.801",	"1.01",		"GPI_DADV",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"5.762",	"0.886",	"GPI_DUT",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"4.859",	"0.869",	"GPI_EC",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"2.915",	"0.858",	"GPI_EGO",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"6.055",	"1.101",	"GPI_EL",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"5.045",	"0.781",	"GPI_EMP",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"3.849",	"0.728",	"GPI_IMPR",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"3.541",	"0.788",	"GPI_IND",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"5.959",	"1.023",	"GPI_INFL",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"6.263",	"0.955",	"GPI_INIT",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"6.425",	"1.064",	"GPI_INOV",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"4.993",	"0.948",	"GPI_INTD",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"1.8",		"0.753",	"GPI_INTI",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"3.207",	"1.081",	"GPI_MAN",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"1.91",		"0.718",	"GPI_MIC",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"1.374",	"0.718",	"GPI_NA",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"4.859",	"0.859",	"GPI_OPEN",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"6.713",	"1.009",	"GPI_OPT",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"2.155",	"0.758",	"GPI_PA",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"5.932",	"0.685",	"GPI_RESP",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"5.883",	"1.172",	"GPI_RISK",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"7.171",	"0.871",	"GPI_SASI",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"5.33",		"0.681",	"GPI_SC",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"6.545",	"1.359",	"GPI_SO",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"5.141",	"1.042",	"GPI_ST",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"7.032",	"0.874",	"GPI_TA",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"6.93",		"1.192",	"GPI_TC",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"4.962",	"0.784",	"GPI_TF",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"5.408",	"0.653",	"GPI_TR",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"6.121",	"1.027",	"GPI_VIS",	"1"}),
		(new String [] {"gpil|NA Individual Contributor",	"6.395",	"1.017",	"GPI_WF",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"5.398",	"1.253",	"GPI_AD",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"5.368",	"0.881",	"GPI_ADPT",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"5.177",	"0.841",	"GPI_AST",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"4.573",	"1.132",	"GPI_COMP",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"7.504",	"1.081",	"GPI_CONS",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"6.211",	"0.895",	"GPI_DACH",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"3.899",	"0.993",	"GPI_DADV",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"5.762",	"0.89",		"GPI_DUT",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"4.773",	"0.974",	"GPI_EC",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"2.865",	"0.827",	"GPI_EGO",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"6.243",	"1.074",	"GPI_EL",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"4.892",	"0.777",	"GPI_EMP",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"3.775",	"0.731",	"GPI_IMPR",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"3.436",	"0.829",	"GPI_IND",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"6.251",	"0.951",	"GPI_INFL",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"6.283",	"0.956",	"GPI_INIT",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"6.463",	"1.08",		"GPI_INOV",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"5.313",	"1.012",	"GPI_INTD",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"1.93",		"0.821",	"GPI_INTI",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"3.247",	"1.102",	"GPI_MAN",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"1.791",	"0.757",	"GPI_MIC",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"1.404",	"0.732",	"GPI_NA",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"4.688",	"0.882",	"GPI_OPEN",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"6.696",	"1.058",	"GPI_OPT",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"2.044",	"0.824",	"GPI_PA",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"5.856",	"0.738",	"GPI_RESP",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"6.095",	"1.145",	"GPI_RISK",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"7.136",	"0.899",	"GPI_SASI",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"5.316",	"0.726",	"GPI_SC",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"6.236",	"1.418",	"GPI_SO",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"5.068",	"1.168",	"GPI_ST",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"6.925",	"0.899",	"GPI_TA",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"7.444",	"1.082",	"GPI_TC",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"5.114",	"0.753",	"GPI_TF",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"5.445",	"0.722",	"GPI_TR",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"6.339",	"0.97",		"GPI_VIS",	"1"}),
		(new String [] {"gpil|NA Mid-Level Leader",	"6.341",	"1.072",	"GPI_WF",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"5.824",	"1.155",	"GPI_AD",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"5.214",	"0.873",	"GPI_ADPT",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"4.984",	"0.825",	"GPI_AST",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"4.284",	"1.118",	"GPI_COMP",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"7.543",	"1.013",	"GPI_CONS",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"5.916",	"0.917",	"GPI_DACH",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"3.62",		"0.908",	"GPI_DADV",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"5.714",	"0.851",	"GPI_DUT",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"4.714",	"0.954",	"GPI_EC",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"2.912",	"0.836",	"GPI_EGO",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"5.959",	"1.06",		"GPI_EL",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"4.951",	"0.699",	"GPI_EMP",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"3.878",	"0.714",	"GPI_IMPR",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"3.511",	"0.796",	"GPI_IND",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"5.89",		"0.944",	"GPI_INFL",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"6.21",		"0.897",	"GPI_INIT",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"6.212",	"1.003",	"GPI_INOV",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"5.082",	"0.954",	"GPI_INTD",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"1.964",	"0.788",	"GPI_INTI",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"3.258",	"1.154",	"GPI_MAN",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"2.049",	"0.721",	"GPI_MIC",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"1.511",	"0.684",	"GPI_NA",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"4.844",	"0.815",	"GPI_OPEN",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"6.578",	"0.934",	"GPI_OPT",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"2.238",	"0.805",	"GPI_PA",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"5.818",	"0.671",	"GPI_RESP",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"5.645",	"1.237",	"GPI_RISK",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"7.023",	"0.846",	"GPI_SASI",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"5.29",		"0.655",	"GPI_SC",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"6.165",	"1.338",	"GPI_SO",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"5.074",	"1.044",	"GPI_ST",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"6.85",		"0.9",		"GPI_TA",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"7.018",	"1.048",	"GPI_TC",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"4.788",	"0.715",	"GPI_TF",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"5.296",	"0.702",	"GPI_TR",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"6.045",	"0.951",	"GPI_VIS",	"1"}),
		(new String [] {"gpil|NA Supervisor",	"6.368",	"0.906",	"GPI_WF",	"1"}),
		(new String [] {"gpil|North America",	"6.027",	"1.198",	"GPI_AD",	"1"}),
		(new String [] {"gpil|North America",	"4.464",	"1.056",	"GPI_ADPT",	"1"}),
		(new String [] {"gpil|North America",	"4.679",	"0.904",	"GPI_AST",	"1"}),
		(new String [] {"gpil|North America",	"3.51",		"1.203",	"GPI_COMP",	"1"}),
		(new String [] {"gpil|North America",	"7.516",	"1.328",	"GPI_CONS",	"1"}),
		(new String [] {"gpil|North America",	"4.94",		"1.187",	"GPI_DACH",	"1"}),
		(new String [] {"gpil|North America",	"3.185",	"1.081",	"GPI_DADV",	"1"}),
		(new String [] {"gpil|North America",	"5.5",		"1.036",	"GPI_DUT",	"1"}),
		(new String [] {"gpil|North America",	"3.936",	"1.157",	"GPI_EC",	"1"}),
		(new String [] {"gpil|North America",	"3.081",	"1.045",	"GPI_EGO",	"1"}),
		(new String [] {"gpil|North America",	"4.702",	"1.354",	"GPI_EL",	"1"}),
		(new String [] {"gpil|North America",	"4.697",	"0.922",	"GPI_EMP",	"1"}),
		(new String [] {"gpil|North America",	"3.946",	"0.824",	"GPI_IMPR",	"1"}),
		(new String [] {"gpil|North America",	"4.298",	"1.108",	"GPI_IND",	"1"}),
		(new String [] {"gpil|North America",	"5.052",	"1.2",		"GPI_INFL",	"1"}),
		(new String [] {"gpil|North America",	"5.287",	"1.188",	"GPI_INIT",	"1"}),
		(new String [] {"gpil|North America",	"5.773",	"1.257",	"GPI_INOV",	"1"}),
		(new String [] {"gpil|North America",	"4.095",	"1.133",	"GPI_INTD",	"1"}),
		(new String [] {"gpil|North America",	"2.318",	"0.942",	"GPI_INTI",	"1"}),
		(new String [] {"gpil|North America",	"3.631",	"1.324",	"GPI_MAN",	"1"}),
		(new String [] {"gpil|North America",	"2.711",	"0.868",	"GPI_MIC",	"1"}),
		(new String [] {"gpil|North America",	"2.367",	"0.946",	"GPI_NA",	"1"}),
		(new String [] {"gpil|North America",	"4.12",		"0.981",	"GPI_OPEN",	"1"}),
		(new String [] {"gpil|North America",	"5.708",	"1.307",	"GPI_OPT",	"1"}),
		(new String [] {"gpil|North America",	"2.979",	"0.958",	"GPI_PA",	"1"}),
		(new String [] {"gpil|North America",	"5.362",	"0.919",	"GPI_RESP",	"1"}),
		(new String [] {"gpil|North America",	"4.793",	"1.349",	"GPI_RISK",	"1"}),
		(new String [] {"gpil|North America",	"6.468",	"1.144",	"GPI_SASI",	"1"}),
		(new String [] {"gpil|North America",	"4.639",	"0.91",		"GPI_SC",	"1"}),
		(new String [] {"gpil|North America",	"5.307",	"1.603",	"GPI_SO",	"1"}),
		(new String [] {"gpil|North America",	"4.079",	"1.251",	"GPI_ST",	"1"}),
		(new String [] {"gpil|North America",	"6.341",	"1.114",	"GPI_TA",	"1"}),
		(new String [] {"gpil|North America",	"5.797",	"1.712",	"GPI_TC",	"1"}),
		(new String [] {"gpil|North America",	"4.357",	"0.897",	"GPI_TF",	"1"}),
		(new String [] {"gpil|North America",	"4.668",	"0.98",		"GPI_TR",	"1"}),
		(new String [] {"gpil|North America",	"5.533",	"1.203",	"GPI_VIS",	"1"}),
		(new String [] {"gpil|North America",	"5.831",	"1.156",	"GPI_WF",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"5.94",		"1.18",		"GPI_AD",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"5.427",	"0.989",	"GPI_ADPT",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"5.441",	"0.91",		"GPI_AST",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"4.218",	"1.068",	"GPI_COMP",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"7.927",	"1.025",	"GPI_CONS",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"6.382",	"0.951",	"GPI_DACH",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"4.751",	"0.947",	"GPI_DADV",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"5.766",	"0.876",	"GPI_DUT",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"4.615",	"1.085",	"GPI_EC",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"3.108",	"0.839",	"GPI_EGO",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"6.193",	"1.146",	"GPI_EL",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"5.312",	"0.752",	"GPI_EMP",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"4.39",		"0.937",	"GPI_IMPR",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"3.298",	"0.954",	"GPI_IND",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"6.27",		"1.045",	"GPI_INFL",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"6.402",	"1.015",	"GPI_INIT",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"6.762",	"1.006",	"GPI_INOV",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"6.028",	"1.039",	"GPI_INTD",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"1.761",	"0.908",	"GPI_INTI",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"3.459",	"1.346",	"GPI_MAN",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"1.704",	"0.908",	"GPI_MIC",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"1.654",	"0.806",	"GPI_NA",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"5.102",	"0.936",	"GPI_OPEN",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"6.828",	"1.019",	"GPI_OPT",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"2.374",	"0.894",	"GPI_PA",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"5.984",	"0.704",	"GPI_RESP",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"6.048",	"1.314",	"GPI_RISK",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"7.445",	"0.919",	"GPI_SASI",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"5.172",	"0.779",	"GPI_SC",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"6.601",	"1.232",	"GPI_SO",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"5.054",	"1.166",	"GPI_ST",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"7.502",	"0.922",	"GPI_TA",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"7.039",	"1.225",	"GPI_TC",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"5.201",	"0.798",	"GPI_TF",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"5.567",	"0.77",		"GPI_TR",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"6.427",	"1.047",	"GPI_VIS",	"1"}),
		(new String [] {"gpil|Saudi Aramco All Management",	"6.639",	"1.02",	"	GPI_WF",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"6.014",	"1.177",	"GPI_AD",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"5.492",	"0.938",	"GPI_ADPT",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"5.448",	"0.919",	"GPI_AST",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"4.286",	"1.111",	"GPI_COMP",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"7.965",	"0.971",	"GPI_CONS",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"6.458",	"0.902",	"GPI_DACH",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"4.789",	"0.97",		"GPI_DADV",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"5.779",	"0.86",		"GPI_DUT",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"4.65",		"1.109",	"GPI_EC",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"3.105",	"0.853",	"GPI_EGO",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"6.301",	"1.129",	"GPI_EL",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"5.31",		"0.745",	"GPI_EMP",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"4.422",	"0.927",	"GPI_IMPR",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"3.309",	"0.947",	"GPI_IND",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"6.339",	"1.016",	"GPI_INFL",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"6.452",	"1.014",	"GPI_INIT",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"6.772",	"1.021",	"GPI_INOV",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"6.031",	"1.09",		"GPI_INTD",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"1.781",	"0.892",	"GPI_INTI",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"3.421",	"1.266",	"GPI_MAN",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"1.667",	"0.841",	"GPI_MIC",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"1.617",	"0.77",		"GPI_NA",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"5.21",		"0.935",	"GPI_OPEN",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"6.915",	"1.001",	"GPI_OPT",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"2.342",	"0.895",	"GPI_PA",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"6.024",	"0.703",	"GPI_RESP",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"6.18",		"1.242",	"GPI_RISK",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"7.451",	"0.936",	"GPI_SASI",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"5.24",		"0.78",		"GPI_SC",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"6.594",	"1.234",	"GPI_SO",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"5.119",	"1.18",		"GPI_ST",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"7.574",	"0.899",	"GPI_TA",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"7.129",	"1.22",		"GPI_TC",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"5.242",	"0.802",	"GPI_TF",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"5.588",	"0.765",	"GPI_TR",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"6.478",	"1.054",	"GPI_VIS",	"1"}),
		(new String [] {"gpil|Saudi Aramco Mid-Level Manager",	"6.672",	"1.029",	"GPI_WF",	"1"}),
		(new String [] {"lei|All Leaders",	"40.15",	"14.13",	"LEI3_BDV",	"1"}),
		(new String [] {"lei|All Leaders",	"42.35",	"13.89",	"LEI3_BGR",	"1"}),
		(new String [] {"lei|All Leaders",	"396.59",	"88.9",		"LEI3_CHA",	"1"}),
		(new String [] {"lei|All Leaders",	"40.55",	"10.99",	"LEI3_CMT",	"1"}),
		(new String [] {"lei|All Leaders",	"58.49",	"10.95",	"LEI3_DEV",	"1"}),
		(new String [] {"lei|All Leaders",	"37.31",	"8.59",		"LEI3_EXT",	"1"}),
		(new String [] {"lei|All Leaders",	"31.93",	"9.96",		"LEI3_FAL",	"1"}),
		(new String [] {"lei|All Leaders",	"36.06",	"8.79",		"LEI3_FEX",	"1"}),
		(new String [] {"lei|All Leaders",	"66.17",	"19.06",	"LEI3_FIN",	"1"}),
		(new String [] {"lei|All Leaders",	"85.05",	"22.58",	"LEI3_FM",	"1"}),
		(new String [] {"lei|All Leaders",	"572.47",	"145.47",	"LEI3_GMT",	"1"}),
		(new String [] {"lei|All Leaders",	"65.71",	"17.77",	"LEI3_HR",	"1"}),
		(new String [] {"lei|All Leaders",	"44.85",	"15.92",	"LEI3_INN",	"1"}),
		(new String [] {"lei|All Leaders",	"101.83",	"29.2",		"LEI3_NEG",	"1"}),
		(new String [] {"lei|All Leaders",	"1518.1",	"342.85",	"LEI3_OAL",	"1"}),
		(new String [] {"lei|All Leaders",	"66.42",	"16.68",	"LEI3_OPX",	"1"}),
		(new String [] {"lei|All Leaders",	"203.77",	"33.88",	"LEI3_PCR",	"1"}),
		(new String [] {"lei|All Leaders",	"18.64",	"6.8",		"LEI3_PDT",	"1"}),
		(new String [] {"lei|All Leaders",	"97.63",	"25.81",	"LEI3_PGT",	"1"}),
		(new String [] {"lei|All Leaders",	"180.73",	"36.28",	"LEI3_PPL",	"1"}),
		(new String [] {"lei|All Leaders",	"43.34",	"10.53",	"LEI3_PRB",	"1"}),
		(new String [] {"lei|All Leaders",	"345.27",	"91.63",	"LEI3_RC",	"1"}),
		(new String [] {"lei|All Leaders",	"92.23",	"24.42",	"LEI3_RPR",	"1"}),
		(new String [] {"lei|All Leaders",	"63.12",	"10.44",	"LEI3_SDV",	"1"}),
		(new String [] {"lei|All Leaders",	"77.2",		"22.67",	"LEI3_SGY",	"1"}),
		(new String [] {"lei|All Leaders",	"16.76",	"5.91",		"LEI3_STA",	"1"}),
		(new String [] {"lei|All Leaders",	"74.43",	"17.64",	"LEI3_STF",	"1"}),
		(new String [] {"lei|All Leaders",	"137.18",	"35.94",	"LEI3_VAS",	"1"}),
		(new String [] {"lei|Executive",	"45.31",	"14.32",	"LEI3_BDV",	"1"}),
		(new String [] {"lei|Executive",	"48.23",	"13.46",	"LEI3_BGR",	"1"}),
		(new String [] {"lei|Executive",	"441.17",	"82.57",	"LEI3_CHA",	"1"}),
		(new String [] {"lei|Executive",	"45.48",	"10.72",	"LEI3_CMT",	"1"}),
		(new String [] {"lei|Executive",	"62.74",	"8.7",		"LEI3_DEV",	"1"}),
		(new String [] {"lei|Executive",	"39.21",	"8.67",		"LEI3_EXT",	"1"}),
		(new String [] {"lei|Executive",	"36.55",	"9.8",		"LEI3_FAL",	"1"}),
		(new String [] {"lei|Executive",	"40.05",	"7.69",		"LEI3_FEX",	"1"}),
		(new String [] {"lei|Executive",	"75.5",		"18.04",	"LEI3_FIN",	"1"}),
		(new String [] {"lei|Executive",	"96.59",	"19.99",	"LEI3_FM",	"1"}),
		(new String [] {"lei|Executive",	"644.88",	"134.06",	"LEI3_GMT",	"1"}),
		(new String [] {"lei|Executive",	"74.6",		"16.76",	"LEI3_HR",	"1"}),
		(new String [] {"lei|Executive",	"47.49",	"16.69",	"LEI3_INN",	"1"}),
		(new String [] {"lei|Executive",	"115.43",	"28.24",	"LEI3_NEG",	"1"}),
		(new String [] {"lei|Executive",	"1691.99",	"316.28",	"LEI3_OAL",	"1"}),
		(new String [] {"lei|Executive",	"73.64",	"15.01",	"LEI3_OPX",	"1"}),
		(new String [] {"lei|Executive",	"214.68",	"32.21",	"LEI3_PCR",	"1"}),
		(new String [] {"lei|Executive",	"20.83",	"6.71",		"LEI3_PDT",	"1"}),
		(new String [] {"lei|Executive",	"109.16",	"23.86",	"LEI3_PGT",	"1"}),
		(new String [] {"lei|Executive",	"197.82",	"33.34",	"LEI3_PPL",	"1"}),
		(new String [] {"lei|Executive",	"48.13",	"10.01",	"LEI3_PRB",	"1"}),
		(new String [] {"lei|Executive",	"391.26",	"86.37",	"LEI3_RC",	"1"}),
		(new String [] {"lei|Executive",	"102.61",	"23.69",	"LEI3_RPR",	"1"}),
		(new String [] {"lei|Executive",	"65.23",	"10.01",	"LEI3_SDV",	"1"}),
		(new String [] {"lei|Executive",	"89.32",	"20.42",	"LEI3_SGY",	"1"}),
		(new String [] {"lei|Executive",	"19.16",	"6.26",		"LEI3_STA",	"1"}),
		(new String [] {"lei|Executive",	"83.18",	"16.43",	"LEI3_STF",	"1"}),
		(new String [] {"lei|Executive",	"155.77",	"33.09",	"LEI3_VAS",	"1"}),
		(new String [] {"lei|First-Level Leader",	"34.32",	"11.75",	"LEI3_BDV",	"1"}),
		(new String [] {"lei|First-Level Leader",	"35.4",		"11.44",	"LEI3_BGR",	"1"}),
		(new String [] {"lei|First-Level Leader",	"336.99",	"65.9",		"LEI3_CHA",	"1"}),
		(new String [] {"lei|First-Level Leader",	"34.3",		"8.54",		"LEI3_CMT",	"1"}),
		(new String [] {"lei|First-Level Leader",	"53.09",	"10.6",		"LEI3_DEV",	"1"}),
		(new String [] {"lei|First-Level Leader",	"35.74",	"8.43",		"LEI3_EXT",	"1"}),
		(new String [] {"lei|First-Level Leader",	"26",		"7.36",		"LEI3_FAL",	"1"}),
		(new String [] {"lei|First-Level Leader",	"30.74",	"7.56",		"LEI3_FEX",	"1"}),
		(new String [] {"lei|First-Level Leader",	"54",		"14.14",	"LEI3_FIN",	"1"}),
		(new String [] {"lei|First-Level Leader",	"69.75",	"17.74",	"LEI3_FM",	"1"}),
		(new String [] {"lei|First-Level Leader",	"484.18",	"114.22",	"LEI3_GMT",	"1"}),
		(new String [] {"lei|First-Level Leader",	"54.9",		"13.69",	"LEI3_HR",	"1"}),
		(new String [] {"lei|First-Level Leader",	"38.95",	"12.66",	"LEI3_INN",	"1"}),
		(new String [] {"lei|First-Level Leader",	"84.98",	"22.19",	"LEI3_NEG",	"1"}),
		(new String [] {"lei|First-Level Leader",	"1298.08",	"259.7",	"LEI3_OAL",	"1"}),
		(new String [] {"lei|First-Level Leader",	"56.77",	"14.44",	"LEI3_OPX",	"1"}),
		(new String [] {"lei|First-Level Leader",	"188.72",	"29.99",	"LEI3_PCR",	"1"}),
		(new String [] {"lei|First-Level Leader",	"16.55",	"6.29",		"LEI3_PDT",	"1"}),
		(new String [] {"lei|First-Level Leader",	"83.89",	"21.96",	"LEI3_PGT",	"1"}),
		(new String [] {"lei|First-Level Leader",	"157.06",	"28.5",		"LEI3_PPL",	"1"}),
		(new String [] {"lei|First-Level Leader",	"37.1",		"8.14",		"LEI3_PRB",	"1"}),
		(new String [] {"lei|First-Level Leader",	"288.19",	"69.07",	"LEI3_RC",	"1"}),
		(new String [] {"lei|First-Level Leader",	"80.42",	"20.34",	"LEI3_RPR",	"1"}),
		(new String [] {"lei|First-Level Leader",	"60.94",	"10.22",	"LEI3_SDV",	"1"}),
		(new String [] {"lei|First-Level Leader",	"62.44",	"17.27",	"LEI3_SGY",	"1"}),
		(new String [] {"lei|First-Level Leader",	"13.92",	"4.03",		"LEI3_STA",	"1"}),
		(new String [] {"lei|First-Level Leader",	"62.83",	"13.14",	"LEI3_STF",	"1"}),
		(new String [] {"lei|First-Level Leader",	"114.02",	"27.32",	"LEI3_VAS",	"1"}),
		(new String [] {"lei|Freddie Mac Sr Executive",	"50.44",	"14.78",	"LEI3_BDV",	"1"}),
		(new String [] {"lei|Freddie Mac Sr Executive",	"53.47",	"13.71",	"LEI3_BGR",	"1"}),
		(new String [] {"lei|Freddie Mac Sr Executive",	"488.55",	"78.71",	"LEI3_CHA",	"1"}),
		(new String [] {"lei|Freddie Mac Sr Executive",	"51.18",	"10.36",	"LEI3_CMT",	"1"}),
		(new String [] {"lei|Freddie Mac Sr Executive",	"66.69",	"6.64",		"LEI3_DEV",	"1"}),
		(new String [] {"lei|Freddie Mac Sr Executive",	"41.56",	"9.11",		"LEI3_EXT",	"1"}),
		(new String [] {"lei|Freddie Mac Sr Executive",	"41.67",	"9.55",		"LEI3_FAL",	"1"}),
		(new String [] {"lei|Freddie Mac Sr Executive",	"42.99",	"7.33",		"LEI3_FEX",	"1"}),
		(new String [] {"lei|Freddie Mac Sr Executive",	"85.15",	"17.17",	"LEI3_FIN",	"1"}),
		(new String [] {"lei|Freddie Mac Sr Executive",	"107.43",	"18.91",	"LEI3_FM",	"1"}),
		(new String [] {"lei|Freddie Mac Sr Executive",	"712.1",	"131.07",	"LEI3_GMT",	"1"}),
		(new String [] {"lei|Freddie Mac Sr Executive",	"83.65",	"16.12",	"LEI3_HR",	"1"}),
		(new String [] {"lei|Freddie Mac Sr Executive",	"49.65",	"16.97",	"LEI3_INN",	"1"}),
		(new String [] {"lei|Freddie Mac Sr Executive",	"129.63",	"27.78",	"LEI3_NEG",	"1"}),
		(new String [] {"lei|Freddie Mac Sr Executive",	"1864.5",	"305.66",	"LEI3_OAL",	"1"}),
		(new String [] {"lei|Freddie Mac Sr Executive",	"79.09",	"14.78",	"LEI3_OPX",	"1"}),
		(new String [] {"lei|Freddie Mac Sr Executive",	"224.57",	"31.64",	"LEI3_PCR",	"1"}),
		(new String [] {"lei|Freddie Mac Sr Executive",	"22.77",	"6.53",		"LEI3_PDT",	"1"}),
		(new String [] {"lei|Freddie Mac Sr Executive",	"119.27",	"22.78",	"LEI3_PGT",	"1"}),
		(new String [] {"lei|Freddie Mac Sr Executive",	"216.09",	"31.53",	"LEI3_PPL",	"1"}),
		(new String [] {"lei|Freddie Mac Sr Executive",	"53.37",	"9.84",		"LEI3_PRB",	"1"}),
		(new String [] {"lei|Freddie Mac Sr Executive",	"439.3",	"83.06",	"LEI3_RC",	"1"}),
		(new String [] {"lei|Freddie Mac Sr Executive",	"114.01",	"23.07",	"LEI3_RPR",	"1"}),
		(new String [] {"lei|Freddie Mac Sr Executive",	"66.67",	"9.7",		"LEI3_SDV",	"1"}),
		(new String [] {"lei|Freddie Mac Sr Executive",	"100.78",	"19.05",	"LEI3_SGY",	"1"}),
		(new String [] {"lei|Freddie Mac Sr Executive",	"21.85",	"6.61",		"LEI3_STA",	"1"}),
		(new String [] {"lei|Freddie Mac Sr Executive",	"92.28",	"15.31",	"LEI3_STF",	"1"}),
		(new String [] {"lei|Freddie Mac Sr Executive",	"174.84",	"31.11",	"LEI3_VAS",	"1"}),
		(new String [] {"lei|Global CEO",	"56.35",	"12.26",	"LEI3_BDV",	"1"}),
		(new String [] {"lei|Global CEO",	"58.57",	"11.37",	"LEI3_BGR",	"1"}),
		(new String [] {"lei|Global CEO",	"498.42",	"83.53",	"LEI3_CHA",	"1"}),
		(new String [] {"lei|Global CEO",	"52.56",	"10.89",	"LEI3_CMT",	"1"}),
		(new String [] {"lei|Global CEO",	"65.49",	"6.86",		"LEI3_DEV",	"1"}),
		(new String [] {"lei|Global CEO",	"44.15",	"8.25",		"LEI3_EXT",	"1"}),
		(new String [] {"lei|Global CEO",	"42.33",	"10.37",	"LEI3_FAL",	"1"}),
		(new String [] {"lei|Global CEO",	"44.84",	"6.75",		"LEI3_FEX",	"1"}),
		(new String [] {"lei|Global CEO",	"89.94",	"18.17",	"LEI3_FIN",	"1"}),
		(new String [] {"lei|Global CEO",	"114.27",	"18.63",	"LEI3_FM",	"1"}),
		(new String [] {"lei|Global CEO",	"759.38",	"120.68",	"LEI3_GMT",	"1"}),
		(new String [] {"lei|Global CEO",	"88.42",	"15.82",	"LEI3_HR",	"1"}),
		(new String [] {"lei|Global CEO",	"48.46",	"19.07",	"LEI3_INN",	"1"}),
		(new String [] {"lei|Global CEO",	"137.67",	"26.39",	"LEI3_NEG",	"1"}),
		(new String [] {"lei|Global CEO",	"1945.3",	"301.28",	"LEI3_OAL",	"1"}),
		(new String [] {"lei|Global CEO",	"82.8",		"12.82",	"LEI3_OPX",	"1"}),
		(new String [] {"lei|Global CEO",	"225.32",	"31.55",	"LEI3_PCR",	"1"}),
		(new String [] {"lei|Global CEO",	"24.32",	"5.87",		"LEI3_PDT",	"1"}),
		(new String [] {"lei|Global CEO",	"125.05",	"21.02",	"LEI3_PGT",	"1"}),
		(new String [] {"lei|Global CEO",	"217.94",	"32.96",	"LEI3_PPL",	"1"}),
		(new String [] {"lei|Global CEO",	"55.05",	"10.54",	"LEI3_PRB",	"1"}),
		(new String [] {"lei|Global CEO",	"462.18",	"81.87",	"LEI3_RC",	"1"}),
		(new String [] {"lei|Global CEO",	"123.05",	"20.53",	"LEI3_RPR",	"1"}),
		(new String [] {"lei|Global CEO",	"67.21",	"9.24",		"LEI3_SDV",	"1"}),
		(new String [] {"lei|Global CEO",	"107.26",	"17.65",	"LEI3_SGY",	"1"}),
		(new String [] {"lei|Global CEO",	"22.91",	"7.02",		"LEI3_STA",	"1"}),
		(new String [] {"lei|Global CEO",	"93.17",	"16.42",	"LEI3_STF",	"1"}),
		(new String [] {"lei|Global CEO",	"183.54",	"30.73",	"LEI3_VAS",	"1"}),
		(new String [] {"lei|Global Senior Executive",	"47.17",	"14.48",	"LEI3_BDV",	"1"}),
		(new String [] {"lei|Global Senior Executive",	"50.78",	"13.51",	"LEI3_BGR",	"1"}),
		(new String [] {"lei|Global Senior Executive",	"462.19",	"83.28",	"LEI3_CHA",	"1"}),
		(new String [] {"lei|Global Senior Executive",	"47.58",	"10.54",	"LEI3_CMT",	"1"}),
		(new String [] {"lei|Global Senior Executive",	"63.98",	"8.63",		"LEI3_DEV",	"1"}),
		(new String [] {"lei|Global Senior Executive",	"40.67",	"8.31",		"LEI3_EXT",	"1"}),
		(new String [] {"lei|Global Senior Executive",	"38.51",	"9.85",		"LEI3_FAL",	"1"}),
		(new String [] {"lei|Global Senior Executive",	"41.87",	"7.31",		"LEI3_FEX",	"1"}),
		(new String [] {"lei|Global Senior Executive",	"80.41",	"17.84",	"LEI3_FIN",	"1"}),
		(new String [] {"lei|Global Senior Executive",	"102.25",	"18.94",	"LEI3_FM",	"1"}),
		(new String [] {"lei|Global Senior Executive",	"680.18",	"129.21",	"LEI3_GMT",	"1"}),
		(new String [] {"lei|Global Senior Executive",	"79.27",	"16.38",	"LEI3_HR",	"1"}),
		(new String [] {"lei|Global Senior Executive",	"50.27",	"17.78",	"LEI3_INN",	"1"}),
		(new String [] {"lei|Global Senior Executive",	"122.46",	"27.64",	"LEI3_NEG",	"1"}),
		(new String [] {"lei|Global Senior Executive",	"1779.21",	"310.26",	"LEI3_OAL",	"1"}),
		(new String [] {"lei|Global Senior Executive",	"46.71",	"14.58",	"LEI3_OPX",	"1"}),
		(new String [] {"lei|Global Senior Executive",	"222.09",	"33.34",	"LEI3_PCR",	"1"}),
		(new String [] {"lei|Global Senior Executive",	"21.79",	"6.62",		"LEI3_PDT",	"1"}),
		(new String [] {"lei|Global Senior Executive",	"114.86",	"22.65",	"LEI3_PGT",	"1"}),
		(new String [] {"lei|Global Senior Executive",	"205.77",	"33.87",	"LEI3_PPL",	"1"}),
		(new String [] {"lei|Global Senior Executive",	"50.47",	"10.36",	"LEI3_PRB",	"1"}),
		(new String [] {"lei|Global Senior Executive",	"414.75",	"83.34",	"LEI3_RC",	"1"}),
		(new String [] {"lei|Global Senior Executive",	"108.07",	"22.79",	"LEI3_RPR",	"1"}),
		(new String [] {"lei|Global Senior Executive",	"67.17",	"10.18",	"LEI3_SDV",	"1"}),
		(new String [] {"lei|Global Senior Executive",	"95.93",	"19.17",	"LEI3_SGY",	"1"}),
		(new String [] {"lei|Global Senior Executive",	"20.77",	"6.57",		"LEI3_STA",	"1"}),
		(new String [] {"lei|Global Senior Executive",	"87.03",	"16.69",	"LEI3_STF",	"1"}),
		(new String [] {"lei|Global Senior Executive",	"165.44",	"31.26",	"LEI3_VAS",	"1"}),
		(new String [] {"lei|Mid-Level Leader",		"37.5",		"12.6",		"LEI3_BDV",	"1"}),
		(new String [] {"lei|Mid-Level Leader",		"39.94",	"12.24",	"LEI3_BGR",	"1"}),
		(new String [] {"lei|Mid-Level Leader",		"386.44",	"71.09",	"LEI3_CHA",	"1"}),
		(new String [] {"lei|Mid-Level Leader",		"39.43",	"9.39",		"LEI3_CMT",	"1"}),
		(new String [] {"lei|Mid-Level Leader",		"58.75",	"9.41",		"LEI3_DEV",	"1"}),
		(new String [] {"lei|Mid-Level Leader",		"35.97",	"8.12",		"LEI3_EXT",	"1"}),
		(new String [] {"lei|Mid-Level Leader",		"30.58",	"8.44",		"LEI3_FAL",	"1"}),
		(new String [] {"lei|Mid-Level Leader",		"35.71",	"7.65",		"LEI3_FEX",	"1"}),
		(new String [] {"lei|Mid-Level Leader",		"63.51",	"15.77",	"LEI3_FIN",	"1"}),
		(new String [] {"lei|Mid-Level Leader",		"82.32",	"18.55",	"LEI3_FM",	"1"}),
		(new String [] {"lei|Mid-Level Leader",		"547.77",	"119.45",	"LEI3_GMT",	"1"}),
		(new String [] {"lei|Mid-Level Leader",		"62.57",	"14.5",		"LEI3_HR",	"1"}),
		(new String [] {"lei|Mid-Level Leader",		"41.01",	"14.08",	"LEI3_INN",	"1"}),
		(new String [] {"lei|Mid-Level Leader",		"95.71",	"24.23",	"LEI3_NEG",	"1"}),
		(new String [] {"lei|Mid-Level Leader",		"1460.11",	"275",		"LEI3_OAL",	"1"}),
		(new String [] {"lei|Mid-Level Leader",		"65.52",	"14.51",	"LEI3_OPX",	"1"}),
		(new String [] {"lei|Mid-Level Leader",		"197.83",	"29.81",	"LEI3_PCR",	"1"}),
		(new String [] {"lei|Mid-Level Leader",		"17.84",	"6.4",		"LEI3_PDT",	"1"}),
		(new String [] {"lei|Mid-Level Leader",		"94.76",	"22.47",	"LEI3_PGT",	"1"}),
		(new String [] {"lei|Mid-Level Leader",		"176.27",	"29.07",	"LEI3_PPL",	"1"}),
		(new String [] {"lei|Mid-Level Leader",		"42.67",	"8.9",		"LEI3_PRB",	"1"}),
		(new String [] {"lei|Mid-Level Leader",		"328.08",	"74.5",		"LEI3_RC",	"1"}),
		(new String [] {"lei|Mid-Level Leader",		"86.37",	"20.7",		"LEI3_RPR",	"1"}),
		(new String [] {"lei|Mid-Level Leader",		"62.1",		"10.15",	"LEI3_SDV",	"1"}),
		(new String [] {"lei|Mid-Level Leader",		"72.23",	"18.48",	"LEI3_SGY",	"1"}),
		(new String [] {"lei|Mid-Level Leader",		"15.58",	"4.78",		"LEI3_STA",	"1"}),
		(new String [] {"lei|Mid-Level Leader",		"73.42",	"14.54",	"LEI3_STF",	"1"}),
		(new String [] {"lei|Mid-Level Leader",		"130.37",	"29.18",	"LEI3_VAS",	"1"}),
		(new String [] {"rv|Australia - All Mgmt Levels, 2008",			"14.3",		"4.1",	"RAVENSB",	"1"}),
		(new String [] {"rv|Europe - All Mgmt Levels",					"14.3",		"4.1",	"RAVENSB",	"1"}),
		(new String [] {"rv|Global - All Mgmt Levels",					"10.74",	"5.01",	"RAVENSB",	"1"}), // updated per Stu/Jurgen
		(new String [] {"rv|Japan - All Mgmt Levels, 2008",				"14.3",		"4.1",	"RAVENSB",	"1"}),
		(new String [] {"rv|Latin America - All Mgmt Levels",			"14.3",		"4.1",	"RAVENSB",	"1"}),
		(new String [] {"rv|Middle East - All Mgmt Levels, 2008",		"14.3",		"4.1",	"RAVENSB",	"1"}),
		(new String [] {"rv|Non-Japan Asia - All Mgmt Levels, 2008",	"14.3",		"4.1",	"RAVENSB",	"1"}),
		(new String [] {"rv|North America - All Mgmt Levels",			"14.3",		"4.1",	"RAVENSB",	"1"}),
		(new String [] {"rv|Saudi Aramco - All Mgmt Levels, 2004",		"11.3",		"4.1",	"RAVENSB",	"1"}),
		(new String [] {"rv|Saudi Aramco - Mid-Level Manager, 2006",	"11.3",		"4.1",	"RAVENSB",	"1"}),
		(new String [] {"wg|Europe - All Management Levels",			"29.7",		"4.9",	"WGE_CT",	"1"}),
		(new String [] {"wg|First Level Leader",						"30.5",		"4.6",	"WGE_CT",	"1"}),
		(new String [] {"wg|General Population",						"21.2",		"4.9",	"WGE_CT",	"1"}),
		(new String [] {"wg|Global CEO/Senior Executive",				"31.23",	"4.19",	"WGE_CT",	"1"}),
		(new String [] {"wg|Mid-Level Leader",							"30.2",		"4.7",	"WGE_CT",	"1"}),
		(new String [] {"wg|NA Executive",								"31",		"4.3",	"WGE_CT",	"1"}),
		(new String [] {"wg|Non-Management",							"30.1",		"4.7",	"WGE_CT",	"1"}),
		(new String [] {"wg|Saudi Aramco - All Mgmt Levels, 2004",		"24",		"2.7",	"WGE_CT",	"1"}),
		(new String [] {"wg|Saudi Aramco - Mid-Level Manager, 2006",	"25.5",		"4.4",	"WGE_CT",	"1"}),
		(new String [] {"wg|Saudi Aramco - Supervisor, 2006",			"22.5",		"4.6",	"WGE_CT",	"1"}),
		(new String [] {"wg|Supervisor",								"28.4",		"5",	"WGE_CT",	"1"}),
		(new String [] {"wga|General Population",						"48.5",		"9.9",	"WG_A",		"1"}),
		(new String [] {"wga|Global CEO/Senior Executive",				"66.21",	"7.07",	"WG_A",		"1"}),
		(new String [] {"wga|NA Executive",								"66.64",	"6.71",	"WG_A",		"1"}),
		(new String [] {"wga|NA First-Level Leader",					"65.04",	"7.77",	"WG_A",		"1"}),
		(new String [] {"wga|NA Individual Contributor",				"64.4",		"8.12",	"WG_A",		"1"}),
		(new String [] {"wga|NA Mid-Level Leader",						"65.24",	"7.64",	"WG_A",		"1"}),
		(new String [] {"wga|NA Supervisor",							"61.74",	"8.77",	"WG_A",		"1"}),
		(new String [] {"wga|Saudi Aramco All Management",				"52.99",	"9.42",	"WG_A",		"1"}),
		(new String [] {"wga|Saudi Aramco Mid-Level Manager",			"55.9",		"8.9",	"WG_A",		"1"}),
		(new String [] {"wga|Saudi Aramco Supervisor",					"51",		"9.2",	"WG_A",		"1"})
	};	



    /*
	 * Runs as a headless app
	 */
	public static void main(String args[])
	{
		if (args.length == 0)
		{
			System.out.println(" No parameters were detected.");
			System.out.println(parmDescString());

			return;
		}

		if (args.length != 3)
		{
			System.out.println("Incorrect number of parameters (" + args.length + ")");
			System.out.println(parmDescString());
			return;
		}
		
		String url = args[0];
		String uid = args[1];
		String pw = args[2];
		
		String phase = null;
		Connection con = null;
		PreparedStatement ngIns = null;
		PreparedStatement ngId = null;
		PreparedStatement nIns = null;

		try
		{
			// Open the DB
			phase = "DB Open";
			con = openDb(url, uid, pw);
		
			System.out.println("Database opened.  url: " + url);

			if (USE_TEST_FILES)
				System.out.println("Writing to test files!  Flip the flag before you check it in!!!");
		
			// Set up the three prepared statements
			// Three calls 'cause you can't return multiple objects (without a List or a Map)
			phase = "Norm Group Insert PreparedStatement setup";
			ngIns = prepareNgInsStmt(con);
			phase = "Norm Group ID Fetch PreparedStatement setup";
			ngId = prepareNgIdStmt(con);
			phase = "Norm Insert PreparedStatement setup";
			nIns = prepareNormInsStmt(con);

			//Load the norm groups
			phase= "Loading Norm Groups";
			HashMap<String, KeyAndStuff> keys = loadNgData(con, ngIns, ngId);
		
			// load the norm data
			phase = "Loading Norms";
			loadNormData(con, keys, nIns);
			
			System.out.println("Done:  Wrote " + NORM_DATA.length + " norms in " + NORM_GROUP_DATA.length + " norm groups.");

		}
		catch (Exception e)
		{
			System.out.println("ERROR Detected in " + phase + " phase...");
			e.printStackTrace();
			return;
		}
		finally
		{
			if (nIns != null)
			{
				try
				{
					nIns.close();
				}
				catch (Exception e) {  }
			}
			if (ngId != null)
			{
				try
				{
					ngId.close();
				}
				catch (Exception e) {  }
			}
			if (ngIns != null)
			{
				try
				{
					ngIns.close();
				}
				catch (Exception e) {  }
			}
			if (con != null)
			{
				try
				{
					con.close();
				}
				catch (Exception e)  {  }
			}
		}
	}
	
	
	/*
	 * Method to display appropriate parameters.
	 * 
	 * Note that there is no closing nwe-line character; assumes that this method is called
	 * in a println() or that the user will handle the closing new-line himself.
	 */
	private static String parmDescString()
	{
		String str = "  This program requires three arguments as follows:\n";
		str += "    URL      - The URL where the database lies.  Examples:\n";
		str += "                 'jdbc:mysql://mspdbdev2:3306/pdi_certify' or\n";
		str += "                 'jdbc:mysql://sanfranqasql3:3306/scoring_reporting'\n";
		str += "    User id  - A user id for the specified database with the appropriate permissions.\n";
		str += "    Password - The password for the above user ID.";
		
		return str;
	}

	
	/*
	 * 
	 */
	private static Connection openDb(String url, String uid, String pw)
		throws Exception
	{
		Connection con = null;

		Class.forName(DRIVER);

		con = DriverManager.getConnection(url, uid, pw);
		//////con.setAutoCommit(false);
			
		return con;
	}


	/*
	 * Prepare the insert into norm group table
	 */
	private static PreparedStatement prepareNgInsStmt(Connection con)
		throws Exception
	{
		StringBuffer q = new StringBuffer();
		if (USE_TEST_FILES)
		{
			q.append("insert into test_norm_group ");
		}
		else
		{
			q.append("insert into pdi_norm_group ");
		}
		q.append("(instrumentCode, name, normType, active, lastDate, lastUser) ");
		q.append("VALUES(?, ?, ?, ?, ?, ?)");	
		return con.prepareStatement(q.toString());
	}


	/*
	 * Prepare the insert into norm group table
	 */
	private static PreparedStatement prepareNgIdStmt(Connection con)
		throws Exception
	{
		StringBuffer q = new StringBuffer();
		if (USE_TEST_FILES)
		{
			q.append("SELECT LAST_INSERT_ID() FROM test_norm_group");
		}
		else
		{
			q.append("SELECT LAST_INSERT_ID() FROM pdi_norm_group");
		}
		return con.prepareStatement(q.toString());
	}


	/*
	 * Prepare the insert into norm group table
	 */
	private static PreparedStatement prepareNormInsStmt(Connection con)
		throws Exception
	{
		StringBuffer q = new StringBuffer();
		if (USE_TEST_FILES)
		{
			q.append("INSERT INTO test_norm ");
		}
		else
		{
			q.append("INSERT INTO pdi_norm ");
		}
		q.append("        (normGroupId, mean, stdev, spssValue, active, lastDate, lastUser) ");
		q.append("  VALUES(?, ?, ?, ?, ?, ?, ?)");
		return con.prepareStatement(q.toString());
	}


	/*
	 * Load the norm group data
	 */
	private static HashMap<String, KeyAndStuff> loadNgData(Connection con,
														   PreparedStatement ngIns,
														   PreparedStatement ngId)
		throws Exception
	{
		String subPhase = null;
		HashMap<String, KeyAndStuff> ret = new HashMap<String, KeyAndStuff>();
		String[] ent = null;
		long id = 0;


		try
		{
			// Loop through the list
			//System.out.println("  " + NORM_GROUP_DATA.length + " Group entries...");
			for (int i=0; i < NORM_GROUP_DATA.length; i++)
			{
				id = 0;
				subPhase = "Group info Write";
				ent = NORM_GROUP_DATA[i];
				// insert the row...
				ngIns.setString(1, ent[0]);
				ngIns.setString(2, ent[1]);
				ngIns.setString(3, ent[2]);
				ngIns.setString(4, ent[3]);
				ngIns.setString(5, ent[4]);
				ngIns.setString(6, ent[5]);
				ngIns.executeUpdate();
	
				// ...get the id...
				subPhase = "ID fetch";
				ResultSet rs = ngId.executeQuery();
				if (rs.next())
				{
					id = rs.getLong(1);
				}
				//System.out.println("  Id for " + ent[0] + "|" + ent[1] + "=" + id);
	
				//Save the data in a map
				KeyAndStuff stuff = new LoadPdiNormStuff().new KeyAndStuff(id, ent[0], ent[1], ent[4], ent[5]);
				ret.put(stuff.getKey(), stuff);
			}
		}
		catch (Exception e)
		{
			String msg = "Error populating Norm Groups.  Subphase=" + subPhase +
						 ", inst=" + ent[0] +
						 ", name=" + ent[1] +
						 ", id=" + id +
						 ":  msg=" + e.getMessage();
			throw new Exception(msg, e.getCause());
		}
		
			return ret;
	}


	/*
	 * 	Load the norm data into the norm table
	 */
	private static void loadNormData(Connection con,
									 HashMap<String, KeyAndStuff> keys,
									 PreparedStatement nIns)
		throws Exception
	{
		String [] ent = null;
		
		try
		{
			// Loop through the norms
			//System.out.println("  " + NORM_DATA.length + " Norm entries...");
			for (int i=0; i < NORM_DATA.length; i++)
			{
				ent = NORM_DATA[i];
				KeyAndStuff ks = keys.get(ent[0]);
				if (ks == null)
				{
					throw new Exception("Unable to fetch normGroup for norm with key " + ent[0]);
				}

				nIns.setLong(1, ks.getId());			// Norm group id
				nIns.setString(2, ent[1]);				// mean
				nIns.setString(3, ent[2]);				// stdev
				nIns.setString(4, ent[3]);				// spss
				nIns.setString(5, ent[4]);				// active flag
				nIns.setString(6, ks.getCreateDate());	// Creation date
				nIns.setString(7, ks.getCreator());		// Creator

				nIns.executeUpdate();
			}
			
			return;
		}
		catch (SQLException e)
		{
			String msg = "Error populating Norm Groups.  Key=" + ent[0] +
			 			 ":  msg=" + e.getMessage();
			throw new Exception(msg, e.getCause());
		}
	}

	
	
	
	/*
	 * Private class to hold norm group information used when inserting norms
	 */
	private class KeyAndStuff
	{
		private String _key;
		private long _id;
		private String _cDate;
		private String _creator;
		
		public KeyAndStuff(long id, String inst, String name, String createDate, String creator)
		{
			_id = id;
			_key = inst + "|" + name;
			_cDate = createDate;
			_creator = creator;
		}
		
		// No setters, only getters
		public String getKey()
		{
			return _key;
		}
		public long getId()
		{
			return _id;
		}
		public String getCreateDate()
		{
			return _cDate;
		}
		public String getCreator()
		{
			return _creator;
		}
	}
}
