/**
 * Copyright (c) 2008 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v2.dto;


import com.pdi.scoring.Norm;

/**
 * ConstructDTO is a wrapper around a number of data items that are needed when 
 * the ManualEntry Vendor App is started
 * 
 * @author      MB Panichi
 * @author		Ken Beukelman
 */
public class ConstructDTO
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String constructId;
	private String constructName;
	private String constructScore;
	private String constructScore2;
	private String constructScore3;
	private Boolean constructValidated;
	private Number showCheckMark;
	private Norm constructNorm;
	
	//
	// Constructors.
	//
	public ConstructDTO()
	{
		// Does nothing presently

	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
	

	//*****************************************************************************************/
	public String getConstructId()
	{
		return constructId;
	}
	
	public void setConstructId(String value)
	{
		constructId = value;
	}	

	//*****************************************************************************************/
	public String getConstructName()
	{
		return constructName;
	}
	
	public void setConstructName(String value)
	{
		constructName = value;
	}
	
	//*****************************************************************************************/
	public String getConstructScore()
	{
		return constructScore;
	}
	
	public void setConstructScore(String value)
	{
		constructScore = value;
	}
	
	//*****************************************************************************************/
	public String getConstructScore2()
	{
		return constructScore2;
	}
	
	public void setConstructScore2(String value)
	{
		constructScore2 = value;
	}

	//*****************************************************************************************/
	public Boolean getConstructValidated()
	{
		return constructValidated;
	}
	
	public void setConstructValidated(Boolean value)
	{
		constructValidated = value;
	}

	//*****************************************************************************************/
	public Number getShowCheckMark()
	{
		return showCheckMark;
	}
	
	public void setShowCheckMark(Number value)
	{
		showCheckMark = value;
	}

	//*****************************************************************************************/
	public Norm getConstructNorm()
	{
		return constructNorm;
	}
	
	public void setConstructNorm(Norm value)
	{
		constructNorm = value;
	}

	public void setConstructScore3(String constructScore3) {
		this.constructScore3 = constructScore3;
	}

	public String getConstructScore3() {
		return constructScore3;
	}
	
}