package com.pdi.data.helpers.interfaces;

import com.pdi.data.dto.ProjectProduct;
import com.pdi.data.dto.SessionUser;

public interface IProjectProductHelper {
	/**
	 * Get an engagement/product link
	 * @param session
	 * @param engagementId
	 * @return
	 */
	public ProjectProduct fromProjectId(SessionUser session, String engagementId);
}
