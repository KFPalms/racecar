package com.pdi.data.dto;

public class Response  extends CoreDataObject{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4579822910128190501L;
	private String id;
	private String code;
	private String value;
	private String questionText;
	
	public void setId(String id) {
		this.id = id;
	}
	public String getId() {
		return id;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getCode() {
		return code;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getValue() {
		return value;
	}
	public void setQuestionText(String value) {
		this.questionText = value;
	}
	public String getQuestionText() {
		return questionText;
	}
}
