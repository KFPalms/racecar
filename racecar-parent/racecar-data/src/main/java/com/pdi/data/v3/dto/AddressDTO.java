/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.dto;

import org.w3c.dom.Node;

import com.pdi.data.util.BaseDTO;
import com.pdi.data.util.RequestStatus;
import com.pdi.xml.XMLUtils;

/**
 * DTO for address information
 * 
 *
 * @author		Gavin Myers
 */
public class AddressDTO extends BaseDTO
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _address;
	private String _city;
	private String _state;
	private String _country;
	private String _zip;

	//
	// Constructors.
	//
	
	/**
	 * "Standard" (no-parameter) constructor
	 */
	public AddressDTO()
	{
		// Does nothing presently
	}
	
	/**
	 * Builds the object based off an xml node
	 * @param node
	 * @throws Exception
	 */
	public AddressDTO(Node node)
	{
		/*
		 * The XML looks something like this:
		 * <address>123 Main Street</address>
		 * <city>Montreal</city>
		 * <stateOrProvince>QC</stateOrProvince>
		 * <country>Canada</country>
		 * <zip>H2S-2P2</zip>
		 */
		this.setAddress(XMLUtils.getElementValue(node, "address/."));
		this.setCity(XMLUtils.getElementValue(node, "city/."));
		this.setState(XMLUtils.getElementValue(node, "state/."));
		this.setCountry(XMLUtils.getElementValue(node, "country/."));
		this.setZip(XMLUtils.getElementValue(node, "zip/."));
		
		if (this.getAddress() == null &&
			this.getCity() == null &&
			this.getState() == null &&
			this.getCountry() == null &&
			this.getZip() == null)
		{
			this.getStatus().setStatusCode(RequestStatus.WB_NO_DATA);
		}
	}

	//
	// Instance methods.
	//
	
	/**
	 * Generic toString method
	 */
	public String toString()
	{
		String str = "\n --- AddressDTO --- \n";
		str += "\nAddress:             " + this.getAddress();
		str += "\nCity:                " + this.getCity();
		str += "\nState:               " + this.getState();
		str += "\nCountry:             " + this.getCountry();
		str += "\nZip:                 " + this.getZip();
		
		return str;
	}
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
	
	/*****************************************************************************************/
	public void setAddress(String value)
	{
		_address = value;
	}
	
	public String getAddress()
	{
		return _address;
	}
	
	/*****************************************************************************************/
	public void setCity(String value)
	{
		_city = value;
	}
	
	public String getCity()
	{
		return _city;
	}
	
	/*****************************************************************************************/
	public void setState(String value)
	{
		_state = value;
	}
	
	public String getState()
	{
		return _state;
	}
	
	/*****************************************************************************************/
	public void setCountry(String value)
	{
		_country = value;
	}
	
	public String getCountry()
	{
		return _country;
	}
	
	/*****************************************************************************************/
	public void setZip(String value)
	{
		_zip = value;
	}
	
	public String getZip()
	{
		return _zip;
	}
}
