
/**
 * Copyright (c) 2008 Personnel Decisions International, Inc.
 * All rights reserved.
 *
 * $Header: com/pdicorp/domain/dataObjects/KeyValuePairStringArrayList.java 6     8/25/05 3:23p mpanichi $
 */
package com.pdi.data.abyd.dto.common;



/**
 * KeyValuePairStringArrayList is a container class for key/value pairs (e.g., String id and array list)
 * 
 * Note that while there is a corresponding class on the AS side, the data is
 * indistinguishable from a native AS3 Object and is, therefore mapped to that
 * when it is transported to Flex.
 *
 * @author      mpanichi
 * @author		Ken Beukelman
 */

import java.util.ArrayList;

public class KeyValuePairStringArrayList
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
		private String _key; // moduleId
		private ArrayList<String> _value; // constructId's


		//
		// Constructors.
		//
	public KeyValuePairStringArrayList()
	{
		// Does nothing presently
	}
	
	//
	// Instance methods.
	//

	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
		
	/*****************************************************************************************/
	public String getTheKey()
	{
		return _key;
	}

	public void setTheKey(String value)
	{
		_key = value;
	}

	/*****************************************************************************************/
	public ArrayList<String> getTheValue()
	{
		return _value;
	}

	public void setTheValue(ArrayList<String> value)
	{
		_value = value;
	}

}