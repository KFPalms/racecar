package com.pdi.data.adapt.helpers.delegated;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import com.pdi.data.dto.Participant;
import com.pdi.data.dto.Project;
import com.pdi.data.dto.ProjectParticipant;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.interfaces.IProjectHelper;

public class ProjectHelper implements IProjectHelper{

//	//@Override
//	public ArrayList<Project> fromClientId(SessionUser session, String clientId){
//		// TODO Auto-generated method stub
//		return null;
//	}
	//@Override
	public ArrayList<Project> fromClientIdByDate(SessionUser session, String clientId){
		// TODO Auto-generated method stub
		return null;
	}
	//@Override
	public ArrayList<Project> fromClientIdByName(SessionUser session, String clientId){
		// TODO Auto-generated method stub
		return null;
	}
	
	//@Override
	public ArrayList<Project> fromParticipantId(SessionUser session, String participantId){
		// TODO Auto-generated method stub
		return null;	
	}
	
	//@Override
	public Project fromId(SessionUser session, String engagementId){
		// TODO Auto-generated method stub
		return null;
	}

//	//@Override
//	public ArrayList<Project> fromOwnerId(SessionUser session,
//			String clientId) {
//		// TODO Auto-generated method stub
//		return null;
//	}
	
	//@Override
	public ArrayList<Project> fromOwnerIdByDate(SessionUser session,
			String adminId) {
		// TODO Auto-generated method stub
		return null;
	}
	
	//@Override
	public ArrayList<Project> fromOwnerIdByName(SessionUser session,
			String adminId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProjectParticipant addParticipant(SessionUser session, Project project, String participantId) {
		// TODO Auto-generated method stub
		
		return null;
	}

	@Override
	public boolean addParticipants(SessionUser session, Project project, ArrayList<String> participants) {
		// TODO Auto-generated method stub
		
		return false;
	}

	@Override
	public boolean removeParticipant(SessionUser session, Project project, String participantId) {
		// TODO Auto-generated method stub
		
		return false;
	}

	@Override
	public boolean removeParticipants(SessionUser session, Project project, ArrayList<Participant> participants) {
		// TODO Auto-generated method stub
		
		return false;
	}
	

//	@Override
//	public ArrayList<Project> find(SessionUser session, String clientId, String target)
//	{
//		// TODO Auto-generated method stub
//		return new ArrayList<Project>();
//	}
	@Override
	public ArrayList<Project> findForClientId(SessionUser session, String clientId, String target)
	{
		// TODO Auto-generated method stub
		return new ArrayList<Project>();
	}
	@Override
	public ArrayList<Project> findForAdminId(SessionUser session, String adminId, String target)
	{
		// TODO Auto-generated method stub
		return new ArrayList<Project>();
	}

	
	@Override
	public Project createProject(SessionUser session, Project project){
		// TODO Auto-generated method stub
		
		
		return new Project();
	}


	@Override
	public boolean updateProject(SessionUser session, Project project)
	{
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean updateCoreProject(SessionUser session, Project project)
	{
		// TODO Auto-generated method stub
		return false;
	}
//
//
//	@Override
//	public boolean updateMetadataItems(SessionUser session, String projectId, HashMap<String, String> meta)
//	{
//		// TODO Auto-generated method stub
//		return false;
//	}


	@Override
	public boolean updateContentItems(SessionUser session, String projectId, HashSet<String> content)
	{
		// TODO Auto-generated method stub
		return false;
	}


//	@Override
//	public boolean removeMetadataItems(SessionUser session, String projectId, HashSet<String> meta)
//	{
//		// TODO Auto-generated method stub
//		return false;
//	}
//
//
//	@Override
//	public boolean removeContentItems(SessionUser session, String projectId, HashSet<String> content)
//	{
//		// TODO Auto-generated method stub
//		return false;
//	}
//
//
//	@Override
//	public boolean removeMetadataItems(SessionUser session, String projectId, HashSet<String> meta)
//	{
//	// TODO Auto-generated method stub
//	return false;
//	}
//
//
//	@Override
//	public boolean removeMetadataItems(SessionUser session, String projectId, HashMap<String, String> meta)
//	{
//		// TODO Auto-generated method stub
//		return false;
//		}
//
//
//	@Override
//	public boolean removeAllMetadataItems(SessionUser session, String projectId)
//	{
//		// TODO Auto-generated method stub
//		return false;
//		}


	@Override
	public boolean removeContentItems(SessionUser session, String projectId, HashSet<String> content)
	{
		// TODO Auto-generated method stub
		return false;
		}


	@Override
	public boolean removeAllContentItems(SessionUser session, String projectId)
	{
		// TODO Auto-generated method stub
		return false;
		}

	/**
	 * This is implemented in the V2 Project Helper
	 */
	@Override
	public ArrayList<String> getInstrumentCodesForProjectId(String projectId, String participantId) throws Exception
	{
		return null;
	}
	
	
}
