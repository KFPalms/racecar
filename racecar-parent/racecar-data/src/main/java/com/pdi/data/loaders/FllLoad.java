/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.loaders;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Iterator;
import java.util.Map;

/*
 * This module loads a number of data items related to the FLL data model.  It does
 * not include the Eval Guide BAR data.
 */
public class FllLoad
{
	//
	// Static data
	//
	protected static final String DRIVER = "com.mysql.jdbc.Driver";
	private static final String LAST_USER = "kbeukelm";

    // Text prepared statements
	private static PreparedStatement textInsertStmt;
	private static PreparedStatement textIdFetchStmt;
	private static PreparedStatement textUpdateStmt;

	//Stuff that gets filled as the load proceeds (used by loads that follow)
	private static long modelId = -1;
	private static long dnaId = -1;
	private static HashMap<String, Integer> compIds = new HashMap<String, Integer>();
	private static HashMap<String, Integer> groupIds = new HashMap<String, Integer>();
	private static HashMap<String, Integer> moduleIds = new HashMap<String, Integer>();

	// Model name - This works cause we are loading only one Model
	private static String MODEL_NAME = "FLL";

	// Module name - This works cause we are loading only one new module
	private static String MODULE_NAME = "Standard";
	
	// List of competencies and their descriptions
	private static final LinkedHashMap<String, String> FLL_COMPETENCY_DATA = new LinkedHashMap<String, String>();
	static
	{
		FLL_COMPETENCY_DATA.put("Analyze Issues and Solve Problems",	"Analyzes issues and their impacts on the business; integrates data from different sources to evaluate alternatives and make effective decisions; draws useful conclusions from financial, business, and quantitative information.");
		FLL_COMPETENCY_DATA.put("Understand Strategies",				"Understands, aligns to, or establishes the vision and direction of the organization;  aligns to the strategies to enhance business value; also aligns personal and workgroup activities.");
		FLL_COMPETENCY_DATA.put("Identify Improvements",				"Generates and champions new ideas and creates an environment that nurtures and supports innovation; leverages new paradigms to create value in the market; encourages new ways of looking at issues.");
		FLL_COMPETENCY_DATA.put("Seek Customer Satisfaction",			"Builds and delivers customer-centered solutions that go beyond price adjustment and address local market needs, ensure appropriate product quality, availability and display; considers the purchasing and service experience.");
		FLL_COMPETENCY_DATA.put("Establish Plans",						"Plans work; sets standards and improves work performance; identifies critical action steps to accomplish objectives;  aligns and optimizes the use of resources.");
		FLL_COMPETENCY_DATA.put("Execute Efficiently",					"Manages work and work performance, holding associates accountable to effectively and efficiently complete work responsibilities; demonstrates initiative, works to achieve results, meets or exceeds goals, acts on opportunities to create value.");
		FLL_COMPETENCY_DATA.put("Show Initiative",						"Demonstrates and fosters a sense of urgency, a \"can-do\" spirit, a sense of optimism, ownership, and strong commitment to achieving goals and organizational success; Demonstrates ownership and a commitment to achieving results.");
		FLL_COMPETENCY_DATA.put("Solicit Support",						"Influences and motivates others, clearly communicates appropriate information; articulates positions and explains ideas compellingly.");
		FLL_COMPETENCY_DATA.put("Encourage Commitment",					"Fosters personal achievement and excellence; articulates and inspires commitment to a vision aligned with the organizational mission and goals; instills and sustains energy and optimism in others.");
		FLL_COMPETENCY_DATA.put("Select and Develop",					"Attracts, develops, manages, and retains critical and top talent; plans and develops talent and bench strength; ensures that talent (top, new, existing, diverse, etc.) receives adequate support; shapes roles and assignments in ways that leverage and develop people's capabilities.");
		FLL_COMPETENCY_DATA.put("Communicate Effectively",				"Prepares and delivers clear, concise, effective, and persuasive written and spoken messages; listens to others attentively; promotes a flow of relevant information across the organization; encourages the open expression of opinions.");
		FLL_COMPETENCY_DATA.put("Relate Well to Others",				"Establishes relationships and enhances cooperation and trust between people; establishes relationships inside and outside of the organization; fosters a culture that makes people feel valued and respected.");
		FLL_COMPETENCY_DATA.put("Demonstrate Credibility",				"Gains the confidence and trust of others through principled leadership, sound business ethics, authenticity, and follow-through on commitments; treats all individuals fairly and with respect; maintains high standards of integrity.");
		FLL_COMPETENCY_DATA.put("Readily Adapt",						"Responds resourcefully, flexibly, and positively when faced with new challenges and demands; handles stress effectively; manages conditions of change or uncertainty productively.");
		FLL_COMPETENCY_DATA.put("Learn and Grow",						"Approaches feedback openly , learns from mistakes and develops constructive suggestions even when set-backs occur; updates knowledge, skills, and abilities regularly; builds own capabilities through stretch assignments.");
		FLL_COMPETENCY_DATA.put("Act with Integrity",					"Demonstrates consistency between words and actions; provides truthful facts even if the message conflicted with own interests; does not distort facts with own biases and agendas; effectively balances quality, time, and cost to ensure that deliverables meet the brand promise.");
	}

	private static final String[][] LINKS = new String[][] 
	{
		// 			grpSeq		groupName,				cmpSeq	competency Name							modSeq	mod Name
		(new String [] {"1",	"Thought Leadership",	"1",	"Analyze Issues and Solve Problems",	"2",	"GPI"}),
		(new String [] {"1",	"Thought Leadership",	"1",	"Analyze Issues and Solve Problems",	"3",	"Cogs"}),
		(new String [] {"1",	"Thought Leadership",	"1",	"Analyze Issues and Solve Problems",	"4",	"In-Box"}),
		(new String [] {"1",	"Thought Leadership",	"1",	"Analyze Issues and Solve Problems",	"9",	"Customer Meeting"}),
		(new String [] {"1",	"Thought Leadership",	"1",	"Analyze Issues and Solve Problems",	"10",	"Standard"}),
		(new String [] {"1",	"Thought Leadership",	"2",	"Understand Strategies",				"1",	"Interview"}),
		(new String [] {"1",	"Thought Leadership",	"2",	"Understand Strategies",				"2",	"GPI"}),
		(new String [] {"1",	"Thought Leadership",	"2",	"Understand Strategies",				"3",	"Cogs"}),
		(new String [] {"1",	"Thought Leadership",	"2",	"Understand Strategies",				"4",	"In-Box"}),
		(new String [] {"1",	"Thought Leadership",	"2",	"Understand Strategies",				"6",	"Boss Meeting"}),
		(new String [] {"1",	"Thought Leadership",	"2",	"Understand Strategies",				"7",	"Peer Meeting"}),
		(new String [] {"1",	"Thought Leadership",	"2",	"Understand Strategies",				"8",	"Team Meeting"}),
		(new String [] {"1",	"Thought Leadership",	"2",	"Understand Strategies",				"9",	"Customer Meeting"}),
		(new String [] {"1",	"Thought Leadership",	"2",	"Understand Strategies",				"10",	"Standard"}),
		(new String [] {"1",	"Thought Leadership",	"3",	"Identify Improvements",				"1",	"Interview"}),
		(new String [] {"1",	"Thought Leadership",	"3",	"Identify Improvements",				"2",	"GPI"}),
		(new String [] {"1",	"Thought Leadership",	"3",	"Identify Improvements",				"4",	"In-Box"}),
		(new String [] {"1",	"Thought Leadership",	"3",	"Identify Improvements",				"5",	"Direct Report Meeting"}),
		(new String [] {"1",	"Thought Leadership",	"3",	"Identify Improvements",				"6",	"Boss Meeting"}),
		(new String [] {"1",	"Thought Leadership",	"3",	"Identify Improvements",				"7",	"Peer Meeting"}),
		(new String [] {"1",	"Thought Leadership",	"3",	"Identify Improvements",				"8",	"Team Meeting"}),
		(new String [] {"1",	"Thought Leadership",	"3",	"Identify Improvements",				"9",	"Customer Meeting"}),
		(new String [] {"1",	"Thought Leadership",	"3",	"Identify Improvements",				"10",	"Standard"}),
		(new String [] {"2",	"Results Leadership",	"1",	"Seek Customer Satisfaction",			"1",	"Interview"}),
		(new String [] {"2",	"Results Leadership",	"1",	"Seek Customer Satisfaction",			"2",	"GPI"}),
		(new String [] {"2",	"Results Leadership",	"1",	"Seek Customer Satisfaction",			"4",	"In-Box"}),
		(new String [] {"2",	"Results Leadership",	"1",	"Seek Customer Satisfaction",			"5",	"Direct Report Meeting"}),
		(new String [] {"2",	"Results Leadership",	"1",	"Seek Customer Satisfaction",			"6",	"Boss Meeting"}),
		(new String [] {"2",	"Results Leadership",	"1",	"Seek Customer Satisfaction",			"7",	"Peer Meeting"}),
		(new String [] {"2",	"Results Leadership",	"1",	"Seek Customer Satisfaction",			"8",	"Team Meeting"}),
		(new String [] {"2",	"Results Leadership",	"1",	"Seek Customer Satisfaction",			"9",	"Customer Meeting"}),
		(new String [] {"2",	"Results Leadership",	"1",	"Seek Customer Satisfaction",			"10",	"Standard"}),
		(new String [] {"2",	"Results Leadership",	"2",	"Establish Plans",						"1",	"Interview"}),
		(new String [] {"2",	"Results Leadership",	"2",	"Establish Plans",						"4",	"In-Box"}),
		(new String [] {"2",	"Results Leadership",	"2",	"Establish Plans",						"5",	"Direct Report Meeting"}),
		(new String [] {"2",	"Results Leadership",	"2",	"Establish Plans",						"10",	"Standard"}),
		(new String [] {"2",	"Results Leadership",	"3",	"Execute Efficiently",					"1",	"Interview"}),
		(new String [] {"2",	"Results Leadership",	"3",	"Execute Efficiently",					"2",	"GPI"}),
		(new String [] {"2",	"Results Leadership",	"3",	"Execute Efficiently",					"4",	"In-Box"}),
		(new String [] {"2",	"Results Leadership",	"3",	"Execute Efficiently",					"5",	"Direct Report Meeting"}),
		(new String [] {"2",	"Results Leadership",	"3",	"Execute Efficiently",					"10",	"Standard"}),
		(new String [] {"2",	"Results Leadership",	"4",	"Show Initiative",						"1",	"Interview"}),
		(new String [] {"2",	"Results Leadership",	"4",	"Show Initiative",						"4",	"In-Box"}),
		(new String [] {"2",	"Results Leadership",	"4",	"Show Initiative",						"5",	"Direct Report Meeting"}),
		(new String [] {"2",	"Results Leadership",	"4",	"Show Initiative",						"6",	"Boss Meeting"}),
		(new String [] {"2",	"Results Leadership",	"4",	"Show Initiative",						"7",	"Peer Meeting"}),
		(new String [] {"2",	"Results Leadership",	"4",	"Show Initiative",						"8",	"Team Meeting"}),
		(new String [] {"2",	"Results Leadership",	"4",	"Show Initiative",						"9",	"Customer Meeting"}),
		(new String [] {"2",	"Results Leadership",	"4",	"Show Initiative",						"10",	"Standard"}),
		(new String [] {"3",	"People Leadership",	"1",	"Solicit Support",						"1",	"Interview"}),
		(new String [] {"3",	"People Leadership",	"1",	"Solicit Support",						"2",	"GPI"}),
		(new String [] {"3",	"People Leadership",	"1",	"Solicit Support",						"4",	"In-Box"}),
		(new String [] {"3",	"People Leadership",	"1",	"Solicit Support",						"5",	"Direct Report Meeting"}),
		(new String [] {"3",	"People Leadership",	"1",	"Solicit Support",						"6",	"Boss Meeting"}),
		(new String [] {"3",	"People Leadership",	"1",	"Solicit Support",						"7",	"Peer Meeting"}),
		(new String [] {"3",	"People Leadership",	"1",	"Solicit Support",						"8",	"Team Meeting"}),
		(new String [] {"3",	"People Leadership",	"1",	"Solicit Support",						"9",	"Customer Meeting"}),
		(new String [] {"3",	"People Leadership",	"1",	"Solicit Support",						"10",	"Standard"}),
		(new String [] {"3",	"People Leadership",	"2",	"Encourage Commitment",					"1",	"Interview"}),
		(new String [] {"3",	"People Leadership",	"2",	"Encourage Commitment",					"2",	"GPI"}),
		(new String [] {"3",	"People Leadership",	"2",	"Encourage Commitment",					"5",	"Direct Report Meeting"}),
		(new String [] {"3",	"People Leadership",	"2",	"Encourage Commitment",					"7",	"Peer Meeting"}),
		(new String [] {"3",	"People Leadership",	"2",	"Encourage Commitment",					"8",	"Team Meeting"}),
		(new String [] {"3",	"People Leadership",	"2",	"Encourage Commitment",					"10",	"Standard"}),
		(new String [] {"3",	"People Leadership",	"3",	"Select and Develop",					"1",	"Interview"}),
		(new String [] {"3",	"People Leadership",	"3",	"Select and Develop",					"4",	"In-Box"}),
		(new String [] {"3",	"People Leadership",	"3",	"Select and Develop",					"5",	"Direct Report Meeting"}),
		(new String [] {"3",	"People Leadership",	"3",	"Select and Develop",					"10",	"Standard"}),
		(new String [] {"3",	"People Leadership",	"4",	"Communicate Effectively",				"1",	"Interview"}),
		(new String [] {"3",	"People Leadership",	"4",	"Communicate Effectively",				"4",	"In-Box"}),
		(new String [] {"3",	"People Leadership",	"4",	"Communicate Effectively",				"5",	"Direct Report Meeting"}),
		(new String [] {"3",	"People Leadership",	"4",	"Communicate Effectively",				"6",	"Boss Meeting"}),
		(new String [] {"3",	"People Leadership",	"4",	"Communicate Effectively",				"10",	"Standard"}),
		(new String [] {"3",	"People Leadership",	"5",	"Relate Well to Others",				"2",	"GPI"}),
		(new String [] {"3",	"People Leadership",	"5",	"Relate Well to Others",				"5",	"Direct Report Meeting"}),
		(new String [] {"3",	"People Leadership",	"5",	"Relate Well to Others",				"7",	"Peer Meeting"}),
		(new String [] {"3",	"People Leadership",	"5",	"Relate Well to Others",				"8",	"Team Meeting"}),
		(new String [] {"3",	"People Leadership",	"5",	"Relate Well to Others",				"9",	"Customer Meeting"}),
		(new String [] {"3",	"People Leadership",	"5",	"Relate Well to Others",				"10",	"Standard"}),
		(new String [] {"4",	"Personal Leadership",	"1",	"Demonstrate Credibility",				"1",	"Interview"}),
		(new String [] {"4",	"Personal Leadership",	"1",	"Demonstrate Credibility",				"4",	"In-Box"}),
		(new String [] {"4",	"Personal Leadership",	"1",	"Demonstrate Credibility",				"5",	"Direct Report Meeting"}),
		(new String [] {"4",	"Personal Leadership",	"1",	"Demonstrate Credibility",				"9",	"Customer Meeting"}),
		(new String [] {"4",	"Personal Leadership",	"1",	"Demonstrate Credibility",				"10",	"Standard"}),
		(new String [] {"4",	"Personal Leadership",	"2",	"Readily Adapt",						"1",	"Interview"}),
		(new String [] {"4",	"Personal Leadership",	"2",	"Readily Adapt",						"2",	"GPI"}),
		(new String [] {"4",	"Personal Leadership",	"2",	"Readily Adapt",						"5",	"Direct Report Meeting"}),
		(new String [] {"4",	"Personal Leadership",	"2",	"Readily Adapt",						"6",	"Boss Meeting"}),
		(new String [] {"4",	"Personal Leadership",	"2",	"Readily Adapt",						"7",	"Peer Meeting"}),
		(new String [] {"4",	"Personal Leadership",	"2",	"Readily Adapt",						"8",	"Team Meeting"}),
		(new String [] {"4",	"Personal Leadership",	"2",	"Readily Adapt",						"9",	"Customer Meeting"}),
		(new String [] {"4",	"Personal Leadership",	"2",	"Readily Adapt",						"10",	"Standard"}),
		(new String [] {"4",	"Personal Leadership",	"3",	"Learn and Grow",						"1",	"Interview"}),
		(new String [] {"4",	"Personal Leadership",	"3",	"Learn and Grow",						"2",	"GPI"}),
		(new String [] {"4",	"Personal Leadership",	"3",	"Learn and Grow",						"3",	"Cogs"}),
		(new String [] {"4",	"Personal Leadership",	"3",	"Learn and Grow",						"10",	"Standard"}),
    	(new String [] {"4",	"Personal Leadership",	"4",	"Act with Integrity",					"1",	"Interview"}),
    	(new String [] {"4",	"Personal Leadership",	"4",	"Act with Integrity",					"2",	"GPI"}),
    	(new String [] {"4",	"Personal Leadership",	"4",	"Act with Integrity",					"5",	"Direct Report Meeting"}),
    	(new String [] {"4",	"Personal Leadership",	"4",	"Act with Integrity",					"10",	"Standard"})
	};    

	private static final String[][] IR_DATA = new String[][] 
	{
		// 			Module		Seq		Text
		(new String [] {"Interview",					"1", "Current Role and Business Challenges are aligned with target of this assessment"}),
		(new String [] {"Interview",					"2", "Career Goals are aligned with target of this assessment"}),
		(new String [] {"Interview",					"3", "Key Experiences in Career History are aligned with target of this assessment"}),
		(new String [] {"In-Box",					"1", "Prioritized items appropriately and made sound, timely decisions on the important and urgent issues impacting the business"}),
		(new String [] {"In-Box",					"2", "Provided accurate communications to senior management about the region’s financial performance and ability to hit plan"}),
		(new String [] {"In-Box",					"3", "Took steps to address risks and capitalize on opportunities in his/her area"}),
		(new String [] {"In-Box",					"4", "Took steps to ensure key talent needs and gaps were addressed"}),
		(new String [] {"In-Box",					"5", "Communicated and reinforced key messages to engage and mobilize the team for high performance"}),
		(new String [] {"In-Box",					"6", "Provided appropriate resources and support to ensure that initiatives continue to move forward"}),
    	(new String [] {"Direct Report Meeting",	"1", "Got the initiative back on track"}),
    	(new String [] {"Direct Report Meeting",	"2", "Equipped and built the direct report’s capabilities and got his/her commitment"}),
    	(new String [] {"Direct Report Meeting",	"3", "Took steps to address risks and capitalize on opportunities to ensure better results"}),
    	(new String [] {"Direct Report Meeting",	"4", "Ensured a smooth change effort and implementation"}),
    	(new String [] {"Boss Meeting",				"1", "Developed clear focus for own area"}),
    	(new String [] {"Boss Meeting",				"2", "Showed how the focus would align with strategies and competitive position"}),
    	(new String [] {"Boss Meeting",				"3", "Identified the resources needed to execute focus"}),
    	(new String [] {"Boss Meeting",				"4", "Handled challenges, questions, and concerns with confidence and enthusiasm"}),
    	(new String [] {"Boss Meeting",				"5", "Came across as a credible, knowledgeable first line leader"}),
    	(new String [] {"Peer Meeting",				"1", "Ensured that the major issues were understood and addressed"}),
    	(new String [] {"Peer Meeting",				"2", "Gained commitment from the team on how to move forward"}),
    	(new String [] {"Peer Meeting",				"3", "Came up with solutions and actions that would improve results"}),
    	(new String [] {"Peer Meeting",				"4", "Built a foundation for an effective relationship with both partners"}),
    	(new String [] {"Peer Meeting",				"5", "Determined how to structure the relationship and gain commitment from the partners"}),
    	(new String [] {"Team Meeting",				"1", "Ensured that the major issues were understood and addressed"}),
    	(new String [] {"Team Meeting",				"2", "Gained commitment from the team on how to move forward"}),
    	(new String [] {"Team Meeting",				"3", "Came up with solutions and actions that would improve results"}),
    	(new String [] {"Team Meeting",				"4", "Built a foundation for an effective relationship with both partners"}),
    	(new String [] {"Team Meeting",				"5", "Determined how to structure the relationship and gain commitment from the partners"}),
    	(new String [] {"Customer Meeting",			"1", "Established personal credibility with customer"}),
    	(new String [] {"Customer Meeting",			"2", "Resolved the product support and account management problems"}),
    	(new String [] {"Customer Meeting",			"3", "Provided a compelling value proposition for the customer"}),
    	(new String [] {"Customer Meeting",			"4", "Identified and pursued opportunities to expand account"}),
    	(new String [] {"Customer Meeting",			"5", "Aligned resources, capabilities, and strategies with customer needs"}),
    	(new String [] {"Standard",					"1", "Impact Statement 1"}),
    	(new String [] {"Standard",					"2", "Impact Statement 2"}),
    	(new String [] {"Standard",					"3", "Impact Statement 3"})
	};    

	//
	// Static methods
	//
	public static void main(String args[])
	{
		// Get the parameters
		if (args.length == 0)
		{
			System.out.println(" No parameters were detected.");
			System.out.println(parmDescString());

			return;
		}

		if (args.length != 3)
		{
			System.out.println("Incorrect number of parameters (" + args.length + ")");
			System.out.println(parmDescString());
			return;
		}
		
		String url = args[0];
		String uid = args[1];
		String pw = args[2];
	
		// Open up the DB
	
		Connection con = null;

		try
		{
			try
			{
				// Get the class for the MySQL jdbc driver
				Class.forName(DRIVER);

				con = DriverManager.getConnection(url, uid, pw);
				//con.setAutoCommit(false);
				System.out.println("Database URL: " + url);
			}
			catch (Exception e)
			{
				System.out.println("Unable to open connection: " + e);
				return;
			}

			// Set up prepared statements for text insertion
			boolean hasError = setUpTextPreparedStatements(con);
			if (hasError) return;

			// Insert the model.
			hasError = insertModel(con);
			if (hasError) return;
			
			// Insert the DNA.
			hasError = insertDNA(con);
			if (hasError) return;
			
			// Update the model with the DNA ID.
			hasError = updateModelDnaId(con);
			if (hasError)  return;

			// Load the competencies
			hasError = insertCompetencies(con);
			if (hasError)  return;

			// Load the competency descriptions
			hasError = insertCompetencyDescriptions(con);
			if (hasError)  return;

			// Load the new module descriptions
			hasError = insertNewModule(con);
			if (hasError)  return;
			
			// Fetch the groups
			hasError = fetchGroups(con);
			if (hasError)  return;
			//System.out.println("DEBUG - Groups:");
			//for (Iterator<Map.Entry<String, Integer>> itr=groupIds.entrySet().iterator(); itr.hasNext(); )
			//{
			//	Map.Entry<String, Integer> ent = itr.next();
			//	System.out.println("  " + ent.getKey() + "=" + ent.getValue());
			//}
			
			// Fetch the modules
			hasError = fetchModules(con);
			if (hasError)  return;
			//System.out.println("DEBUG - Modules:");
			//for (Iterator<Map.Entry<String, Integer>> itr=moduleIds.entrySet().iterator(); itr.hasNext(); )
			//{
			//	Map.Entry<String, Integer> ent = itr.next();
			//	System.out.println("  " + ent.getKey() + "=" + ent.getValue());
			//}

			// Load the DNA links
			hasError = insertLinks(con);
			if (hasError)  return;

			// Load the importance ratings
			hasError = insertIr(con);
			if (hasError)  return;

			// Load the bars
			System.out.println("");
			System.out.println("New IDs:  model = " + modelId + ", DNA=" + dnaId);
			System.out.println("");
			System.out.println("Dont forget to load the BAR data!!!");

			//// Done... commit the changes (Probably not operative in our version of MySql)
			//try
			//{
			//	con.commit();
			//}
			//catch(Exception e)
			//{
			//	System.out.println("Error on commit():  " + e);
			//}

			return;
		}
		finally
		{
			// close the statements and the connection
			if (textInsertStmt != null)
			{
				try
				{
					textInsertStmt.close();
					textInsertStmt = null;
				}
				catch (SQLException se)
				{
					System.out.println("SQLException on textInsertStmt close(): " + se);
				}
			}
			if (con != null)
			{
				try
				{
					con.close();
					con = null;
				}
				catch (SQLException se)
				{
					System.out.println("SQLException on connection close(): " + se);
				}
			}
		}
	}
	
	
	/*
	 * Method to display appropriate parameters.
	 * 
	 * Note that there is no closing nwe-line character; assumes that this method is called
	 * in a println() or that the user will handle the closing new-line himself.
	 */
	private static String parmDescString()
	{
		String str = "  This program requires three arguments as follows:\n";
		str += "    URL      - The URL where the database lies.  Examples:\n";
		str += "                 'jdbc:mysql://mspdbdev2:3306/pdi_certify' or\n";
		str += "                 'jdbc:mysql://sanfranqasql3:3306/scoring_reporting'\n";
		str += "    User id  - A user id for the specified database with the appropriate permissions.\n";
		str += "    Password - The password for the above user ID.";
		
		return str;
	}


	// Only one statement (text insertion) set up as global prepared statement here
	private static boolean setUpTextPreparedStatements(Connection con)
	{
		boolean err = false;
		
		// Set up a prepared statement for text data
		try
		{
		    // Text insert statement
			// Assumes that the languageId is 1 (US English) and it is the default text item
			 String textInsert =
				"INSERT INTO pdi_abd_text " +
				" (languageId, text, isDefault, active, lastUserId, lastDate) " +
				" VALUES(1, ?, 1, 1, '" + LAST_USER + "', NOW()) ";
			textInsertStmt = con.prepareStatement(textInsert);
		}
		catch (Exception e)
		{
			err = true;
			System.out.println("Error preparing textInsert statement:  " + e);
		}
		
		try
		{
			String textIdFetch = "SELECT LAST_INSERT_ID() FROM pdi_abd_text";
			textIdFetchStmt = con.prepareStatement(textIdFetch);
		}
		catch (Exception e)
		{
			err = true;
			System.out.println("Error preparing textInsert statement:  " + e);
		}
		
		try
		{
			String textUpdate =
				"UPDATE pdi_abd_text " +
				" SET textId = ? " +
				" WHERE uniqueId = ?";
			textUpdateStmt = con.prepareStatement(textUpdate);
		}
		catch (Exception e)
		{
			err = true;
			System.out.println("Error preparing textInsert statement:  " + e);
		}

		return err;
	}


	// General purpose text inserter... returns the new text ID.
	// Assumes that the data is always in the default language so the textId = the uniqueId
	private static int insertText(Connection con, String txt)
	{
		int id = -1;
		
		String phase = "";
		try
		{
			// Insert the row...
			phase = "Text row insert";
			textInsertStmt.setString(1, txt );	// Since there is only one, hard code the text here
			int rowCount = textInsertStmt.executeUpdate();
			//System.out.println("Insert rowCount: " + rowCount);
			if (rowCount != 1)
			{
				System.out.println("Incorrect row count (" + rowCount + ") inserting: " + txt);
				return -1;
			}

			// ...and get the id...
			phase = "Text ID fetch";
			ResultSet rs = textIdFetchStmt.executeQuery();
			rs.next();
			id = rs.getInt(1);
			
			phase = "ResultSet close";
			rs.close();
			rs = null;
			
			phase = "Updating textId";
			textUpdateStmt.clearParameters();
			textUpdateStmt.setInt(1, id);
			textUpdateStmt.setInt(2, id);
			int cnt = textUpdateStmt.executeUpdate();
			if (cnt !=1)
			{
				throw new Exception("Invalid update count - " + cnt);
			}
		}
		catch(Exception e)
		{
			System.out.println("insertText: Error " + phase + ":  " + e);
			return -1;
		}

		return id;
	}


	private static boolean insertModel(Connection con)
	{
		System.out.print(".");

		// Since there is only one model, we do not do any fancy loopy stuff.
		
		// First adjust the sequence # of the existing models
		// First adjust the sequence # of the existing models
		StringBuffer updtSql = new StringBuffer();
		updtSql.append("UPDATE pdi_abd_model ");
		updtSql.append("  SET modelSeq = modelSeq + 1");

		// Update the model sequence
		try
		{
			Statement stmt = con.createStatement();
			stmt.executeUpdate(updtSql.toString());
		}
		catch (Exception e)
		{
			System.out.println ("Error in updating model sequence:  " + e);
			return true;
		}
		
		// Insert the model text
		int textId = insertText(con, MODEL_NAME);
		if (textId < 1)
		{
			System.out.println("Invalid text ID inserting model name");
			return true;
		}
		
		// Note that the DNA ID is not known at this time so that value is
		// set to zero and updated once the DNA is inserted.
		//Note also that the sequence is hard-coded to 1
		StringBuffer insSql = new StringBuffer();
		insSql.append("INSERT INTO pdi_abd_model ");
		insSql.append(" (internalName, textId, dnaId, modelSeq, active, lastUserId, lastDate) ");
		insSql.append(" VALUES('" + MODEL_NAME + "', " + textId + ", 0, 1, 1, '" + LAST_USER + "', NOW()) ");

		// Insert the model
		try
		{
			Statement stmt = con.createStatement();
			stmt.executeUpdate(insSql.toString());
		}
		catch (Exception e)
		{
			System.out.println ("Error in inserting model:  " + e);
			return true;
		}

		// ...and get the id
		try
		{
			String querySql = "SELECT LAST_INSERT_ID() FROM pdi_abd_model ";
			Statement stmt = con.createStatement();

			ResultSet rs = stmt.executeQuery(querySql);
			rs.next();
			modelId = rs.getInt(1);

			stmt.close();
			stmt = null;
			//System.out.println("Model Id: " + modelId);
		}
		catch(Exception e)
		{
			System.out.println("Error executing model id fetch:  " + e);
			return true;
		}

		return false;
	}


	private static boolean insertDNA(Connection con)
	{
		System.out.print(".");
		
		// Since there is only one DNA, we hard code the values here.
		StringBuffer insSql = new StringBuffer();
		insSql.append("INSERT INTO pdi_abd_dna ");
		insSql.append(" (dnaName, projectId, modelId, ");
		insSql.append("  cMgrName, cMgrEmail, dnaSubmitted, ");
		insSql.append("  synthMean, synthStdDev, wbVersion, dnaNotes, active, ");
		insSql.append("  lastUserId, lastDate) ");
		insSql.append(" VALUES('DNA Template for FLL', 0, " + modelId + ", ");
		insSql.append("        'No name on template', 'noName@pdinh.com', 0, ");
		insSql.append("        0.0, 0.0, '000', 'DNA template for FLL model', 1, ");
		insSql.append("        '" + LAST_USER + "', NOW() )");

		// Insert the DNA
		try
		{
			Statement stmt = con.createStatement();
			stmt.executeUpdate(insSql.toString());
		}
		catch (Exception e)
		{
			System.out.println ("Error in inserting DNA:  " + e);
			return true;
		}

		// ...and get the id
		try
		{
			String querySql = "SELECT LAST_INSERT_ID() FROM pdi_abd_dna";
			Statement stmt = con.createStatement();

			ResultSet rs = stmt.executeQuery(querySql);
			rs.next();
			dnaId = rs.getInt(1);
			
			rs.close();
			rs = null;
			stmt.close();
			stmt = null;
			//System.out.println("DNA Id: " + dnaId);
		}
		catch(Exception e)
		{
			System.out.println("Error executing model id fetch:  " + e);
			return true;
		}

		return false;
	}


	private static boolean updateModelDnaId(Connection con)
	{
		System.out.print(".");

		StringBuffer insSql = new StringBuffer();
		insSql.append("UPDATE pdi_abd_model ");
		insSql.append("  SET  dnaId = " + dnaId + " ");
		insSql.append("  WHERE  modelId = " + modelId);

		try
		{
			Statement stmt = con.createStatement();
			stmt.executeUpdate(insSql.toString());
		}
		catch (Exception e)
		{
			System.out.println ("Error updating model " + modelId + ":  " + e);
			return true;
		}

		return false;
	}


	private static boolean insertCompetencies(Connection con)
	{
		System.out.print(".");

		// Set up PreparedStatement for the text id fetch, the competency insert and the comp id fetch
		PreparedStatement cStmt = null;
		PreparedStatement cIdStmt = null;
		try
		{
			StringBuffer insSql = new StringBuffer();
			insSql.append("INSERT INTO pdi_abd_competency ");
			insSql.append(" (internalName, textId, active, lastUserId, lastDate) ");
			insSql.append(" VALUES(?, ?, 1, '" + LAST_USER + "', NOW()) ");
			cStmt = con.prepareStatement(insSql.toString());
			
			cIdStmt = con.prepareStatement("SELECT LAST_INSERT_ID() FROM pdi_abd_competency");
		}
		catch (Exception e)
		{
			System.out.println("Error preparing competency statements:  " + e);
			return true;
		}

		// Assumes that internal text and external text are the same
		for (Iterator<Map.Entry<String, String>> itr= FLL_COMPETENCY_DATA.entrySet().iterator(); itr.hasNext();  )
		{
			int cmpId = -1;
			int txtId = -1;
			
			Map.Entry<String, String> ent = itr.next();
			String txt = ent.getKey();

			// Insert the competency name text
			txtId = insertText(con, txt);
			if (txtId < 1)
			{
				System.out.println("Error inserting comp text " + txt);
				return true;
			}
			
			// Insert the new competency
			try
			{
				cStmt.clearParameters();
				cStmt.setString(1, txt);
				cStmt.setInt(2, txtId);
				//int rowCount = cStmt.executeUpdate();
				//System.out.println("Insert rowCount: " + rowCount);
				cStmt.executeUpdate();
			}
			catch (Exception e)
			{
				System.out.println("Error inserting competency (" + txt + "):  " + e);
				return true;
			}

			// get the competency id
			try
			{
				ResultSet rs = cIdStmt.executeQuery();
				rs.next();
				cmpId = rs.getInt(1);
				if (cmpId < 1)
				{
					throw new Exception("Invalid ID");
				}
			}
			catch (Exception e)
			{
				System.out.println("Error fetching comptency text id for '" + txt + "':  " + e);
				return true;
			}

			// Save the competemcy id xref
			compIds.put(txt, cmpId);
			
		}	// End of for loop

		return false;
	}


	// This probably could (should) have been done with the competency
	// load, but the separation allows us to test independently
	private static boolean insertCompetencyDescriptions(Connection con)
	{
		System.out.print(".");

		// Set up PreparedStatement for the competency description text id fetch
		// and the competency description insert
		PreparedStatement cdStmt = null;
		try
		{
			StringBuffer insSql = new StringBuffer();
			insSql.append("INSERT INTO pdi_abd_mod_comp_desc ");
			insSql.append(" (modelId, competencyId,textId,lastUserId,lastDate) ");
			insSql.append(" VALUES(?, ?, ?, '" + LAST_USER + "', NOW()) ");
			cdStmt = con.prepareStatement(insSql.toString());
		}
		catch (Exception e)
		{
			System.out.println("Error preparing competency description statements:  " + e);
			return true;
		}

		// Run through the list of competencies and insert their descriptions
		for (Iterator<Map.Entry<String, String>> itr= FLL_COMPETENCY_DATA.entrySet().iterator(); itr.hasNext();  )
		{
			Map.Entry<String,String> ent = itr.next();
			String key = ent.getKey();
			String desc = ent.getValue();
			int compId = compIds.get(key);
			
			if (compId < 1)
			{
				System.out.println("No competency id for " + key);
				return true;
			}

			// Add the competency description text
			int txtId = insertText(con, desc);
			if (txtId < 1)
			{
				System.out.println("Error inserting comp desc text " + desc);
				return true;
			}

			// Add the map entry
			try
			{
				cdStmt.setLong(1, modelId);
				cdStmt.setInt(2, compId);
				cdStmt.setInt(3, txtId);
				cdStmt.executeUpdate();
			}
			catch (Exception e)
			{
				System.out.println("Error inserting mod comp desc entry for key "+ key +":  " + e);
				return true;
			}
		}	// End of for loop

		return false;
	}


	// Load the new module (used in this Model only)
	private static boolean insertNewModule(Connection con)
	{
		System.out.print(".");

		// Since there is only one module, we do not do any fancy loopy stuff.
		// Insert the module text
		int textId = insertText(con, MODULE_NAME);
		if (textId < 1)
		{
			System.out.println("Invalid text ID inserting model name");
			return true;
		}
		
		StringBuffer insSql = new StringBuffer();
		insSql.append("INSERT INTO pdi_abd_module ");
		insSql.append(" (internalName, textId, hasEG, specialType, active, lastUserId, lastDate) ");
		insSql.append(" VALUES('" + MODULE_NAME + "', " + textId + ", 1, 'No', 1, '" + LAST_USER + "', NOW()) ");

		// Insert the module
		try
		{
			Statement stmt = con.createStatement();
			stmt.executeUpdate(insSql.toString());
		}
		catch (Exception e)
		{
			System.out.println ("Error in inserting module:  " + e);
			return true;
		}

		return false;
	}

	
	private static boolean fetchGroups(Connection con)
	{
		System.out.print(".");

		String querySql = "SELECT g.groupId, t.text ";
		querySql +=       "  FROM pdi_abd_dna_grouping_value g, pdi_abd_text t ";
		querySql +=       "  WHERE g.textId = t.textId";

		Statement stmt = null;
		ResultSet rs = null;

		try
		{
			stmt = con.createStatement();
			rs = stmt.executeQuery(querySql);

			while (rs.next())
			{
				int id = rs.getInt(1);
				String name = rs.getString(2);		    
				groupIds.put(name, id);
			}
			if(groupIds.size() < 1)
			{
				System.out.println ("No groups fetched");
				return true;
			}

			stmt.close();
			stmt = null;
		}
		catch (Exception e)
		{
			System.out.println ("Error fetching groups:  " + e);
			return true;
		}

		return false;
	}


	private static boolean fetchModules(Connection con)
	{
		System.out.print(".");

		String querySql = "SELECT mm.moduleId, mm.internalName ";
		querySql +=       "  FROM pdi_abd_module mm ";
		querySql +=       "  WHERE mm.active = 1";

		Statement stmt = null;
		ResultSet rs = null;

		try
		{
			stmt = con.createStatement();
			rs = stmt.executeQuery(querySql);

			while (rs.next())
			{
				int id = rs.getInt(1);
				String name = rs.getString(2);		    
				moduleIds.put(name, id);
			}
			if(moduleIds.size() < 1)
			{
				System.out.println ("No modules fetched");
				return true;
			}

			stmt.close();
			stmt = null;
		}
		catch (Exception e)
		{
			System.out.println ("Error fetching modules:  " + e);
			return true;
		}

		return false;
	}


	private static boolean insertLinks(Connection con)
	{
		System.out.print(".");

		// Set up PreparedStatement for the link insert
		PreparedStatement insStmt = null;
		
		StringBuffer insSql = new StringBuffer();
		insSql.append("INSERT INTO pdi_abd_dna_link ");
		insSql.append(" (dnaId, moduleId, modSeq, groupId, groupSeq, ");
		insSql.append("  competencyId, compSeq, isUsed, active, lastUserId, lastDate) ");
		insSql.append(" VALUES(" + dnaId + ", ?, ?, ?, ?, ");
		insSql.append("        ?, ?, 1, 1, '" + LAST_USER + "', NOW()) ");
		try
		{
			insStmt = con.prepareStatement(insSql.toString());
		}
		catch (Exception e)
		{
			System.out.println("Error preparing link insert statement:  " + e);
			return true;
		}

		// Loop through the list
		for (int i = 0; i < LINKS.length; i++)
		{
			// 			grpSeq		groupName,				cmpSeq	competency Name							modSeq	mod Name
			int grpSeq = Integer.parseInt(LINKS[i][0]);
			int grpId = groupIds.get(LINKS[i][1]);
			int cmpSeq = Integer.parseInt(LINKS[i][2]);
			int cmpId = compIds.get(LINKS[i][3]);
			int modSeq = Integer.parseInt(LINKS[i][4]);
			int modId = moduleIds.get(LINKS[i][5]);
			//System.out.println("" + grpSeq + " " + LINKS[i][1] + "(" + grpId + ")   " + cmpSeq + " " + LINKS[i][3] + "(" + cmpId + ")   " + modSeq + " " + LINKS[i][5] + "(" + modId + ")");

			// Insert the new link
			try
			{
				insStmt.setInt(1, modId);
				insStmt.setInt(2, modSeq);
				insStmt.setInt(3, grpId);
				insStmt.setInt(4, grpSeq);
				insStmt.setInt(5, cmpId);
				insStmt.setInt(6, cmpSeq);

				//int rowCount = insStmt.executeUpdate();
				//System.out.println("Insert rowCount: " + rowCount);
				insStmt.executeUpdate();
			}
			catch (Exception e)
			{
				System.out.println("Error inserting link:  " + e);
				return true;
			}
		}	// End of for loop

		return false;
	}


	private static boolean insertIr(Connection con)
	{
		System.out.print(".");

		// Set up PreparedStatement for the IR data insert
		PreparedStatement irStmt = null;
		try
		{
			StringBuffer insSql = new StringBuffer();
			insSql.append("INSERT INTO pdi_abd_eg_ir ");
			insSql.append("  (modelId, moduleId, irSequence, textId, ");
			insSql.append("   active, lastUserId, lastDate) ");
			insSql.append("  VALUES(" + modelId + ", ?, ?, ?, ");
			insSql.append("         1,'" + LAST_USER + "', NOW())");
			irStmt = con.prepareStatement(insSql.toString());
		}
		catch (Exception e)
		{
			System.out.println("Error preparing IR statements:  " + e);
			return true;
		}

		// loop through the IR text array
		for (int i = 0; i < IR_DATA.length; i++)
		{
			int textId = 0;
			String modName = IR_DATA[i][0];
			String irSeq = IR_DATA[i][1];
			String irTxt = IR_DATA[i][2];

			// Get the module id
			int modId = moduleIds.get(modName);
			if (modId == 0)
			{
				System.out.println("Error fetching module id for " + modName + " (" + i + "}");
				return true;
			}

			// Insert the text
			textId = insertText(con, irTxt);
			if (textId < 1)
			{
				System.out.println("Invalid text ID inserting IR text");
				return true;
			}

			// Insert the map row
			try
			{
				irStmt.setInt(1, modId);
				irStmt.setString(2, irSeq);
				irStmt.setInt(3, textId);

				irStmt.executeUpdate();
			}
			catch (Exception e)
			{
				System.out.println("Error inserting IR map row:  " + e);
				return true;
			}
			//System.out.println("Inserted row - model=" + modelId + ", module=" + modId + ", seq=" + irSeq + ", tId=" + textId);
		}	// End of for loop

		return false;
	}
}
