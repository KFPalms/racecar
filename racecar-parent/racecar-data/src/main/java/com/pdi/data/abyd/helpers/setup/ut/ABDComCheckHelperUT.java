package com.pdi.data.abyd.helpers.setup.ut;

import com.pdi.ut.UnitTestUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;

import junit.framework.TestCase;

public class ABDComCheckHelperUT  extends TestCase
{
	//
	// Constructors
	//
	public ABDComCheckHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
	throws Exception
	{
		junit.textui.TestRunner.run(ABDComCheckHelperUT.class);
	}
	
	/*
	 * 
	 */
	public void testConnect()
		throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);

		UnitTestUtils.stop(this);
	}

}
