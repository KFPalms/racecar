/**
 * Copyright (c) 2008 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.dto.setup;

import com.pdi.data.abyd.dto.common.StatusDTO;
import com.pdi.scoring.Norm;
	
/**
 * DNADescription contains information about the DNA and provides a place for data
 * that is stored only once for a particular DNA.  Along with the DNSStructure class,
 * it provides all needful information about a DNA.
 * 
 * @author		Ken Beukelman
 */
public class DNADescription extends StatusDTO
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private long _dnaId;			// The internal id of the DNA 
	private String _dnaName;		// The name of the DNA - default = name of the V2 job (project)
	private String _jobId;			// V2 job (project) id - the "consultant experience" job
	private long _modelId;			// The model ID selected for this DNA
	private String _modelName;		// The model name associated with the above ID
	private String _cMgrName;		// The name of the client manager
	private String _cMgrEmail;		// The email address of the client manager
	private Boolean _dnaComplete;	// The "DNA Complete" flag
	private Norm _synthNorm;		// The entered synthetic norm.
	private String _wbVersion;		// The workbook version used in the design of the DNA
	private String _dnaNotes;		// User entered notes for this dna
	private String _testInstrument;	// The primary test instrument
	private Boolean _modelActive;

	//
	// Constructors.
	//
	public DNADescription()
	{
		// Initialize the Norm object (empty object)
		_synthNorm = new Norm();
	}
	
	//
	// Instance methods.
	//

	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
		
	/*****************************************************************************************/
	public long getDnaId()
	{
		return _dnaId;
	}
	
	public void setDnaId(long value)
	{
		_dnaId = value;
	}

	/*****************************************************************************************/
	public String getDnaName()
	{
		return _dnaName;
	}
	
	public void setDnaName(String value)
	{
		_dnaName = value;
	}	
		
	/*****************************************************************************************/
	public String getJobId()
	{
		return _jobId;
	}
	
	public void setJobId(String value)
	{
		_jobId = value;
	}
	
	/*****************************************************************************************/
	public long getModelId()
	{
		return _modelId;
	}
	
	public void setModelId(long value)
	{
		_modelId = value;
	}	
	
	/*****************************************************************************************/
	public String getModelName()
	{
		return _modelName;
	}
	
	public void setModelName(String value)
	{
		_modelName = value;
	}	
	
	/*****************************************************************************************/
	public String getcMgrName()
	{
		return _cMgrName;
	}

	public void setcMgrName(String value)
	{
		_cMgrName = value;
	}	

	/*****************************************************************************************/
	public String getcMgrEmail()
	{
		return _cMgrEmail;
	}

	public void setcMgrEmail(String value)
	{
		_cMgrEmail = value;
	}	
	
	/*****************************************************************************************/
	public Boolean getDnaComplete()
	{
		return _dnaComplete;
	}
	
	public void setDnaComplete(Boolean value)
	{
		_dnaComplete = value;
	}

	/*****************************************************************************************/
	public Norm getSynthNorm()
	{
		if (_synthNorm == null)
		{
			_synthNorm = new Norm();
		}
		return _synthNorm;
	}

	public void setSynthNorm(Norm value)
	{
		_synthNorm = value;
	}
		
	/*****************************************************************************************/
	public String getWbVersion()
	{
		return _wbVersion;
	}
	
	public void setWbVersion(String value)
	{
		_wbVersion = value;
	}
	
	/*****************************************************************************************/
	public String getDnaNotes()
	{
		return _dnaNotes;
	}
	
	public void setDnaNotes(String value)
	{
		_dnaNotes = value;
	}		
	
	/*****************************************************************************************/
	public String getTestInstrument()
	{
		return _testInstrument;
	}
	
	public void setTestInstrument(String value)
	{
		_testInstrument = value;
	}

	public Boolean get_modelActive() {
		return _modelActive;
	}

	public void set_modelActive(Boolean _modelActive) {
		this._modelActive = _modelActive;
	}		
}