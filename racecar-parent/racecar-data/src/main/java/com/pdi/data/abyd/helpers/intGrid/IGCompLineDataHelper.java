/**
 * Copyright (c) 2009, 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.intGrid;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdi.data.abyd.dto.common.EGBarDisplayData;
import com.pdi.data.abyd.dto.common.EGCompDisplayData;
import com.pdi.data.abyd.dto.common.IGInstCompData;
import com.pdi.data.abyd.dto.intGrid.IGCompDataDTO;
import com.pdi.data.abyd.dto.intGrid.IGInstDisplayData;
import com.pdi.data.abyd.dto.intGrid.IGModuleData;
import com.pdi.data.abyd.dto.intGrid.IGScaleScoreData;
import com.pdi.data.abyd.dto.intGrid.IGTestAndNormNameData;
import com.pdi.data.abyd.dto.intGrid.LineScoreData;
import com.pdi.data.abyd.helpers.intGrid.IGScoring.ScoreAndNormData;
import com.pdi.data.util.language.DefaultLanguageHelper;

/**
 * IGCompLineDataHelper extracts and returns an IGCompDataDTO object. It gets
 * the data required for a line of drill-down data in the Integration Grid.
 *
 * @author Ken Beukelman
 */
public class IGCompLineDataHelper {
	private static final Logger log = LoggerFactory.getLogger(IGCompLineDataHelper.class);
	private final Connection con;
	private final long compId;
	private final String partId;
	private final long dnaId;

	private final long dfltLangId;

	private final IGCompDataDTO ret = new IGCompDataDTO();

	private int testInsertIdx = -100;

	private boolean hasAlp = false; // Assume no ALP
	private boolean hasKf4d = false; // Assume no KF4D
	private boolean hasGpi = false; // Assume no Gpi
	
	private IGKf4dDataHelper kf4dHelper;

	//
	// Constructors.
	//
	/*
	public IGCompLineDataHelper(Connection con, long compId, String partId, long dnaId) throws Exception {
		// ensure there are believable parameters
		if (compId == 0) {
			throw new Exception("A valid competency id is required to fetch Integration Grid data.");
		}
		if (partId == null || partId.length() < 1) {
			throw new Exception("A participant id is required to fetch Integration Grid line data.");
		}
		if (dnaId == 0) {
			throw new Exception("A valid DNA id is required to fetch Integration Grid data.");
		}

		this.con = con;
		this.compId = compId;
		this.partId = partId;
		this.dnaId = dnaId;

		this.dfltLangId = DefaultLanguageHelper.fetchDefaultLanguageId(con);
	}
	*/
	public IGCompLineDataHelper(Connection con, long compId, String partId, long dnaId, IGKf4dDataHelper helper) throws Exception {
		// ensure there are believable parameters
		if (compId == 0) {
			throw new Exception("A valid competency id is required to fetch Integration Grid data.");
		}
		if (partId == null || partId.length() < 1) {
			throw new Exception("A participant id is required to fetch Integration Grid line data.");
		}
		if (dnaId == 0) {
			throw new Exception("A valid DNA id is required to fetch Integration Grid data.");
		}

		this.con = con;
		this.compId = compId;
		this.partId = partId;
		this.dnaId = dnaId;
		this.kf4dHelper = helper;

		this.dfltLangId = DefaultLanguageHelper.fetchDefaultLanguageId();
	}

	//
	// Instance methods.
	//

	/**
	 * Driver for the logic to fetch the data required for a single competency.
	 *
	 * @throws Exception
	 */
	public IGCompDataDTO getCompLine() throws Exception {
		log.debug("Calling getCompLine().  Participant ID: {}, DNA ID: {}, Competency ID: {}", this.partId, this.dnaId, this.compId);
		// Fill in the data from the request
		this.ret.setPartId(this.partId);
		this.ret.setDnaId(this.dnaId);
		this.ret.setCompId(this.compId);

		// Get the data needed for a competency header
		getHeaderData();

		// Get "modules used" data
		this.testInsertIdx = -1;
		ArrayList<IGModuleData> mods = getModData();

		// if the test insert index is still -1, then there are no testing
		// data... skip it
		if (this.testInsertIdx != -1) {
			// Get the testing competency data...
			IGModuleData tMod = getInstrumentMod();

			// ...and save the tMod into the mods array at the correct spot
			mods.add(testInsertIdx, tMod);
		}

		// Save in ret
		ret.setModArray(mods);

		ret.calcAvgCompScore();

		return ret;
	}

	/**
	 * Driver for the logic to fetch the data required for a single competency.
	 *
	 * @throws Exception
	 */
	private void getHeaderData() throws Exception {
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT 1 as dataType, ");
		sqlQuery.append("       txt.text as compDesc, ");
		sqlQuery.append("       txt2.text as compName, ");
		sqlQuery.append("       -1 as scoreIdx, ");
		sqlQuery.append("       NULL as scoreNote ");
		sqlQuery.append("  FROM pdi_abd_dna dna, ");
		sqlQuery.append("       pdi_abd_mod_comp_desc mcd, ");
		sqlQuery.append("       pdi_abd_text txt, ");
		sqlQuery.append("       pdi_abd_text txt2, ");
		sqlQuery.append("       pdi_abd_competency comp ");
		sqlQuery.append("  WHERE dna.dnaId = " + this.dnaId + " ");
		sqlQuery.append("    AND mcd.modelId = dna.modelId ");
		sqlQuery.append("    AND mcd.competencyId = " + this.compId + " ");
		sqlQuery.append("    AND mcd.competencyId = comp.competencyId ");
		sqlQuery.append("    AND (txt2.textId = comp.textId AND txt2.languageId = " + this.dfltLangId + ") ");
		sqlQuery.append("    AND (txt.textId = mcd.textId AND txt.languageId = " + this.dfltLangId + ") ");
		sqlQuery.append("UNION ");
		sqlQuery.append("SELECT 2 as datatype, ");
		sqlQuery.append("       NULL as compDesc, ");
		sqlQuery.append("       NULL as compName, ");
		sqlQuery.append("       icr.lcScore as scoreIdx, ");
		sqlQuery.append("       icr.lcNote as scoreNote ");
		sqlQuery.append("  FROM pdi_abd_igrid_comp_resp icr ");
		sqlQuery.append("  WHERE icr.dnaId = " + this.dnaId + " ");
		sqlQuery.append("    AND icr.participantId = '" + this.partId + "' ");
		sqlQuery.append("    AND icr.competencyId = " + this.compId);

		try {
			stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			if (!rs.isBeforeFirst()) {
				throw new Exception(
						"No IG header data.  comp=" + this.compId + ", dna=" + this.dnaId + ". part=" + this.partId);
			}

			while (rs.next()) {
				int type = rs.getInt("dataType");
				if (type == 1) {
					// System.out.println("Got a comp name of " +
					// rs.getString("compName"));
					this.ret.setCompDisplayName(rs.getString("compName"));
					this.ret.setCompDescription(rs.getString("compDesc"));
				} else {
					// It must be 2
					this.ret.setIntScoreIdx(rs.getInt("scoreIdx"));
					this.ret.setIntNotes(rs.getString("scoreNote"));
				}
			}

			return;
		} catch (SQLException ex) {
			// handle any errors
			throw new Exception("SQL in IG getHeaderData.  " + "SQLException: " + ex.getMessage() + ", " + "SQLState: "
					+ ex.getSQLState() + ", " + "VendorError: " + ex.getErrorCode());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					System.out.println("ERROR: rs close in IG getHeaderData - " + sqlEx.getMessage());
				}
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					System.out.println("ERROR: stmt close in IG getHeaderData - " + sqlEx.getMessage());
				}
				stmt = null;
			}
		}
	}

	/**
	 * Get the module data and put them into the return IGCompDataDTO object.
	 * Note that the modules should be in display order in the returned object
	 * and that only the ones used in this line should be present
	 *
	 * @return An ArrayList of IGModuleData objects
	 * @throws Exception
	 */
	private ArrayList<IGModuleData> getModData() throws Exception {
		ArrayList<IGModuleData> ret = new ArrayList<IGModuleData>();

		Statement stmt = null;
		ResultSet rs = null;
		String context = "module info";

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT DISTINCT dl.moduleId as modId, ");
		sqlQuery.append("                dl.modSeq as modSeq, ");
		sqlQuery.append("                mtxt.text as modName, ");
		sqlQuery.append("                mdul.internalName as mIntName, ");
		sqlQuery.append("                mdul.specialType, ");
		sqlQuery.append("                ISNULL(egr.egSubmitted,0) as modSubmitted ");
		sqlQuery.append("  FROM pdi_abd_dna_link dl ");
		sqlQuery.append("    INNER JOIN pdi_abd_module mdul ON mdul.moduleId = dl.moduleId ");
		sqlQuery.append("    LEFT JOIN pdi_abd_text mtxt ON (mtxt.textId = mdul.textId AND mtxt.languageID = "
				+ this.dfltLangId + ") ");
		sqlQuery.append("    LEFT JOIN pdi_abd_eg_response egr ON (egr.participantId = '" + this.partId + "' ");
		sqlQuery.append("                                      AND egr.dnaId = dl.dnaId ");
		sqlQuery.append("                                      AND egr.moduleId = dl.moduleId) ");
		sqlQuery.append("  WHERE dl.dnaId = " + this.dnaId + " ");
		sqlQuery.append("    AND dl.competencyId = " + this.compId + " ");
		sqlQuery.append("    AND dl.isUsed = 1 ");
		sqlQuery.append("  ORDER BY dl.modSeq");

		try {
			stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());
			// System.out.println(sqlQuery);
			while (rs.next()) {
				String mInt = rs.getString("mIntName");
				// System.out.println(mInt);
				// System.out.println(this.testInsertIdx);
				if (mInt.equals("GPI") || mInt.equals("Cogs") || mInt.equals("ALP") || mInt.equals("KF4D")) {
					if (mInt.equals("ALP")) {
						this.hasAlp = true;
					} else if (mInt.equals("KF4D")) {
						this.hasKf4d = true;
					} else if (mInt.equals("GPI")) {
						this.hasGpi = true;
					}
					if (this.testInsertIdx < ret.size()) {
						// save the index where the Testing module will be
						// inserted
						this.testInsertIdx = ret.size();
					}
					continue;
				}
				IGModuleData mod = new IGModuleData();
				mod.setModuleId(rs.getLong("modId"));
				mod.setModuleName(rs.getString("modName"));
				mod.setSpecialType(rs.getString("specialType"));
				mod.setModSeq(rs.getLong("modSeq"));

				ret.add(mod);
			}

			// clean up
			try {
				rs.close();
			} catch (SQLException sqlEx) {
				System.out.println("ERROR: rs close(1) in IG getModData - " + sqlEx.getMessage());
			}
			try {
				stmt.close();
			} catch (SQLException sqlEx) {
				System.out.println("ERROR: stmt close(1) in IG getModData - " + sqlEx.getMessage());
			} finally {
			}
			rs = null;
			stmt = null;

			// We now have the module names & ids... get the bar data
			// NOTE: This query is much like the one for bar data in
			// EGDataHelper. We
			// replicate it here (with minor changes) because the data required
			// is somewhat different and, therefore, the subsequent processing
			// is different. Changes to the data model that require changes in
			// this query may require changes in that one as well.
			context = "bar info";
			sqlQuery.setLength(0);
			sqlQuery.append("SELECT bar.moduleId as moduleId, ");
			sqlQuery.append("       bar.barSequence, ");
			sqlQuery.append("       ISNULL(bresp.barScore,-1) as barScore, ");
			sqlQuery.append("       t1.text as hiText, ");
			sqlQuery.append("       t2.text as midText, ");
			sqlQuery.append("       t3.text as loText, ");
			sqlQuery.append("       t4.text as hiExText, ");
			sqlQuery.append("       t5.text as midExText, ");
			sqlQuery.append("       t6.text as loExText, ");
			sqlQuery.append("       ISNULL(cresp.compNotes,'') as compNotes, ");
			sqlQuery.append("       cresp.orScoreIdx ");
			sqlQuery.append("  FROM pdi_abd_eg_bar bar ");
			sqlQuery.append("    INNER JOIN pdi_abd_dna dna ON dna.dnaId = " + this.dnaId + " ");
			sqlQuery.append(
					"    LEFT JOIN pdi_abd_eg_bar_response bresp ON (bresp.participantId = '" + this.partId + "' ");
			sqlQuery.append("                                            AND bresp.dnaId = dna.dnaId ");
			sqlQuery.append("                                            AND bresp.moduleId = bar.moduleId ");
			sqlQuery.append("                                            AND bresp.competencyId = bar.competencyId ");
			sqlQuery.append("                                            AND bresp.barSequence = bar.barSequence) ");
			sqlQuery.append("    LEFT JOIN pdi_abd_text t1 ON (t1.textId = bar.hiTextId AND t1.languageId = "
					+ this.dfltLangId + ") ");
			sqlQuery.append("    LEFT JOIN pdi_abd_text t2 ON (t2.textId = bar.medTextId AND t2.languageId = "
					+ this.dfltLangId + ") ");
			sqlQuery.append("    LEFT JOIN pdi_abd_text t3 ON (t3.textId = bar.loTextId AND t3.languageId = "
					+ this.dfltLangId + ") ");
			sqlQuery.append("    LEFT JOIN pdi_abd_text t4 ON (t4.textId = bar.hiExTextId AND t4.languageId = "
					+ this.dfltLangId + ") ");
			sqlQuery.append("    LEFT JOIN pdi_abd_text t5 ON (t5.textId = bar.medExTextId AND t5.languageId = "
					+ this.dfltLangId + ") ");
			sqlQuery.append("    LEFT JOIN pdi_abd_text t6 ON (t6.textId = bar.loExTextId AND t6.languageId = "
					+ this.dfltLangId + ") ");
			sqlQuery.append(
					"    LEFT JOIN pdi_abd_eg_comp_response cresp  ON (cresp.participantId = '" + this.partId + "' ");
			sqlQuery.append("                                            AND cresp.dnaId = dna.dnaId ");
			sqlQuery.append("                                            AND cresp.moduleId = bar.moduleId ");
			sqlQuery.append("                                            AND cresp.competencyId = bar.competencyId) ");
			sqlQuery.append("  WHERE bar.modelId = dna.modelId ");
			sqlQuery.append("    AND bar.competencyId = " + this.compId + " ");
			sqlQuery.append("    AND bar.active = 1 ");
			sqlQuery.append("  ORDER BY bar.moduleId, bar.barSequence ");
			// System.out.println("getModData SQL 2 = " + sqlQuery.toString());
			stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			long curMod = -1;
			HashMap<Long, EGCompDisplayData> modMap = new HashMap<Long, EGCompDisplayData>();
			EGCompDisplayData curObj = null;
			while (rs.next()) {
				long mod = rs.getLong("moduleId");
				if (mod != curMod) {
					// Set up for the new Mod id
					curMod = mod;
					curObj = new EGCompDisplayData();
					curObj.setCompId(curMod);
					// in here, add the compNotes text to the curObj
					curObj.setCompNotes(rs.getString("compNotes"));
					curObj.setOverideScoreIndex(rs.getInt("orScoreIdx"));
					modMap.put(new Long(curMod), curObj);
				}

				// create a BAR data object and put it in the current comp
				// object
				EGBarDisplayData bar = new EGBarDisplayData();
				bar.setBarSeq(rs.getInt("barSequence"));
				bar.setBarScore(rs.getInt("barScore"));
				bar.setBarHiText(rs.getString("hiText"));
				bar.setBarMidText(rs.getString("midText"));
				bar.setBarLoText(rs.getString("loText"));
				bar.setBarHiExText(rs.getString("hiExText"));
				bar.setBarMidExText(rs.getString("midExText"));
				bar.setBarLoExText(rs.getString("loExText"));

				curObj.getCompBars().add(bar);
			}

			// Calculate the Comp Scores
			for (Map.Entry<Long, EGCompDisplayData> ent : modMap.entrySet()) {
				double sum = 0;
				int cnt = 0;
				double avg = 0;
				EGCompDisplayData comp = ent.getValue();
				if (comp.getOverideScoreIndex() > 0) {
					// We have an override. process it and scram
					int idx = comp.getOverideScoreIndex();
					avg = (idx + 1.0) / 2.0;
				} else {
					// "Normal" scoring
					for (EGBarDisplayData bar : comp.getCompBars()) {
						int scor = bar.getBarScore();
						if (scor > 0) {
							sum += scor;
							cnt++;
						}
					}
					avg = (cnt < 2) ? 0.0 : sum / cnt;
				}
				avg = Math.floor((avg * 100.0) + 0.505) / 100.0;
				comp.setCalcedCompScore(avg == 0.0 ? "N/A" : String.format("%.2f", avg));
			}

			// and now we have the Comp Display data... put with the proper
			// module
			for (Iterator<IGModuleData> itr = ret.iterator(); itr.hasNext();) {
				IGModuleData mod = itr.next();
				if (mod.getSpecialType().equals("ES")) {
					// move the external score
					// create a comdatadisplay object and move it into the
					// module
					EGCompDisplayData cdd = getExtCompDispData(mod.getModuleId());
					if (cdd != null) {
						mod.setCompDispData(cdd);
					}
				} else {
					// "Normal" processing
					mod.setCompDispData(modMap.get(new Long(mod.getModuleId())));
				}
			}
			return ret;
		} catch (SQLException ex) {
			// handle any errors
			throw new Exception("SQL in IG getModData (" + context + ").  " + "SQLException: " + ex.getMessage() + ", "
					+ "SQLState: " + ex.getSQLState() + ", " + "VendorError: " + ex.getErrorCode());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					System.out.println("ERROR: rs close(2) in IG getModData - " + sqlEx.getMessage());
				}
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					System.out.println("ERROR: stmt close(2) in IG getModData - " + sqlEx.getMessage());
				}
				stmt = null;
			}
		}
	}

	/**
	 *
	 * @param modId
	 * @return
	 * @throws Exception
	 */
	private EGCompDisplayData getExtCompDispData(long modId) throws Exception {
		EGCompDisplayData ret = new EGCompDisplayData();

		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT tt1.text AS compName, ");
		sqlQuery.append("       tt2.text AS compDesc, ");
		sqlQuery.append("       ecr.compNotes, ");
		sqlQuery.append("       ecr.extCompScore ");
		sqlQuery.append("		FROM pdi_abd_competency cc");
		sqlQuery.append("		JOIN pdi_abd_text tt1 ON tt1.textId = cc.textId AND tt1.languageId = 1 ");
		sqlQuery.append("		JOIN pdi_abd_mod_comp_desc mcd ON mcd.competencyId = cc.competencyId ");
		sqlQuery.append("		JOIN pdi_abd_text tt2 ON tt2.textId = mcd.textId AND tt1.languageId = 1 ");
		sqlQuery.append("       JOIN dbo.pdi_abd_dna pad ON pad.modelId = mcd.modelId ");
		sqlQuery.append("		LEFT JOIN dbo.pdi_abd_eg_comp_response ecr ON ecr.competencyId = cc.competencyId ");
		sqlQuery.append("		               AND ecr.dnaId = pad.dnaId ");
		sqlQuery.append("		               AND ecr.participantId = " + this.partId + " ");
		sqlQuery.append("		               AND ecr.moduleId = " + modId + " ");
		sqlQuery.append("       WHERE cc.competencyId = " + this.compId + " ");
		sqlQuery.append("       AND pad.dnaId = " + this.dnaId);
		// System.out.println("getExtCompDispData query-" + sqlQuery);

		try {
			stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());
			if (!rs.isBeforeFirst()) {
				return null;
			}

			// There should be only one here... fetch it
			rs.next();

			ret.setCompId(this.compId);
			ret.setCompName(rs.getString("compName"));
			ret.setCompDesc(rs.getString("compDesc"));
			ret.setCompNotes(rs.getString("compNotes") == null ? "" : rs.getString("compNotes"));
			ret.setCompBars(new ArrayList<EGBarDisplayData>()); // Empty array
																// of
																// EGBarDisplayData
																// objects
			// Not setting overide score index
			// Not setting calced comp score
			double es = rs.getDouble("extCompScore");
			String str = "0.00";
			if (rs.getObject("extCompScore") == null) {
				str = "-1.00";
			} else if (es != 0.0) {
				es = Math.floor((es * 100.0) + 0.505) / 100.0;
				str = String.format("%.2f", es);
			}
			ret.setExtCompScore(str);

			return ret;
		} catch (SQLException ex) {
			// handle any errors
			throw new Exception("SQL in IG getExtCompDispData.  " + "SQLException: " + ex.getMessage() + ", "
					+ "SQLState: " + ex.getSQLState() + ", " + "VendorError: " + ex.getErrorCode());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					System.out.println("ERROR: rs close(2) in IG getModData - " + sqlEx.getMessage());
				}
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					System.out.println("ERROR: stmt close(2) in IG getModData - " + sqlEx.getMessage());
				}
				stmt = null;
			}
		}
	}

	/**
	 * Get the module data and put them into the return IGCompDataDTO object.
	 * This is for the single line data functionality. It gets the one comp.
	 * Note that the modules should be in display order in the returned object
	 *
	 * @throws Exception
	 */
	private IGModuleData getInstrumentMod() throws Exception {
		log.debug("Calling getInstrumentMod()");
		// create the return mod
		IGModuleData ret = new IGModuleData();
		ret.setModuleId(IGConstants.TESTING_MOD_ID);
		ret.setModuleName(this.hasAlp ? IGConstants.TESTING_MOD_NAME_ALP
				: (this.hasKf4d) ? IGConstants.TESTING_MOD_NAME_KF4D : IGConstants.TESTING_MOD_NAME);
		ret.setInstDispData(new IGInstDisplayData());
		ret.getInstDispData().setHasKfp(this.hasAlp);
		ret.getInstDispData().setHasKfar(this.hasKf4d);
		ret.getInstDispData().setGpiIncluded(this.hasGpi);

		if (this.hasAlp) {
			ret.getInstDispData().setRdi(Integer.parseInt(IGConstants.NO_RDI));
			ret.getInstDispData().setNormData(new ArrayList<IGTestAndNormNameData>()); // empty
																						// array

			IGAlrDataHelper alrHelper = new IGAlrDataHelper(this.con, this.partId, this.dnaId);
			IGInstCompData icData = alrHelper.getAlrCompScore(this.compId);
			ret.getInstDispData().setCogsDone(alrHelper.getAlrCogsDone());

			// Create the ArrayList and add the single entry
			ret.getInstDispData().setCompArray(new ArrayList<IGInstCompData>());
			ret.getInstDispData().getCompArray().add(icData);
		} else if (this.hasKf4d) {
			log.debug("This compentency has KF4D data");
			ret.getInstDispData().setRdi(Integer.parseInt(IGConstants.NO_RDI));
			ret.getInstDispData().setNormData(new ArrayList<IGTestAndNormNameData>()); // empty
																						// array
			if (this.kf4dHelper == null){
				this.kf4dHelper = new IGKf4dDataHelper(this.con, this.partId, this.dnaId);
			}
			IGInstCompData icData = kf4dHelper.getKf4dCompScore(this.compId);
			ret.getInstDispData().setCogsDone(kf4dHelper.getKf4dCogsDone());

			// Create the ArrayList and add the single entry
			ret.getInstDispData().setCompArray(new ArrayList<IGInstCompData>());
			ret.getInstDispData().getCompArray().add(icData);
		} else {
			// This is the code for the legacy stuff
			ScoreAndNormData dat = null;
			// get the scores
			dat = IGScoring.fetchIGTestScoresAndNorms(this.con, this.partId, this.dnaId);

			// Get the RDI - note that no check is done here for the case where
			// the GPI Scores are
			// not to be included as this is supposed to give all of the raw
			// scores
			String rdiValue = dat.getRdi();
			if (rdiValue == null || rdiValue.length() < 1) {
				rdiValue = IGConstants.NO_RDI;
			}
			ret.getInstDispData().setRdi(Integer.parseInt(rdiValue));

			// get the norm names
			ArrayList<IGTestAndNormNameData> nn = IGScoring.getTestAndNormNames(con, dat.getNormMap(),
					IGConstants.NO_LEI);
			ret.getInstDispData().setNormData(nn);

			// Create the comp array and the single IGInstCompData object in it
			IGInstCompData icData = new IGInstCompData();
			// set the comp id only... All the data should be available already
			// including the id
			icData.setCompId(this.compId);

			// Create the ArrayList and add the single entry
			ret.getInstDispData().setCompArray(new ArrayList<IGInstCompData>());
			ret.getInstDispData().getCompArray().add(icData);

			// Set the cogsDone flag
			ret.getInstDispData().setCogsDone(dat.getCogsDone());

			// if the flag is not set, check to see if there are no cogs at all
			if (!ret.getInstDispData().getCogsDone()) {
				// Not done... check to see if there are no cogs to do
				if (IGScoring.noCogsInDna(this.con, this.dnaId)) {
					ret.getInstDispData().setCogsDone(true);
				}

			}

			// Get the appropriate scores in and put them in the output
			LineScoreData lsd = getInstScores(dat.getScoreMap());
			icData.setCogColumnScores(lsd.getCogColumnScores());
			icData.setGpiColumnScores(lsd.getGpiColumnScores());
		}

		// Do the comp calcs
		ret.getInstDispData().calcCompScores();

		// scram
		return ret;
	}

	/**
	 * Get instrument scores used for the competency.
	 *
	 * ScoreMap is the input, cogScores and gpiScores are the outputs
	 *
	 * @param scoreMap
	 *            - The map of scale scores (pctl is the key, rating is the
	 *            value)
	 * @return A LineScoreData object
	 * @throws Exception
	 */
	private LineScoreData getInstScores(Map<String, IGScoring.ScorePair> scoreMap) throws Exception {
		LineScoreData ret = new LineScoreData();
		ArrayList<IGScaleScoreData> gpiDat = new ArrayList<IGScaleScoreData>();
		ArrayList<IGScaleScoreData> cogDat = new ArrayList<IGScaleScoreData>();

		// Get the score list for the competency
		ArrayList<IGScaleScoreData> keyList = IGScoring.getAScoreKeyList(con, this.compId);

		boolean gotGpi = hasType("GPI");
		boolean gotCogs = hasType("Cogs");

		// Fetch the scores from the score list and put them into the correct
		// bucket
		// key list is spss key/ literal name of scale
		for (Iterator<IGScaleScoreData> itr = keyList.iterator(); itr.hasNext();) {
			IGScaleScoreData ssd = itr.next();
			String spssVal = ssd.getSpssVal();
			String rating = "Not Available"; // Default
			if (scoreMap.containsKey(spssVal) && scoreMap.get(spssVal).getRating() != null) {
				rating = scoreMap.get(spssVal).getRating();
			}

			// put into object
			// key=name of the scale, val=rating
			IGScaleScoreData elt = new IGScaleScoreData();
			elt.setScaleName(ssd.getScaleName());
			elt.setRating(rating);
			elt.setIsNegative(ssd.getIsNegative());

			// Put object into correct output bucket, if it belongs
			if (spssVal.substring(0, 3).equals("GPI")) {
				if (gotGpi)
					gpiDat.add(elt);
			} else {
				if (gotCogs)
					cogDat.add(elt);
			}
		}

		// Scram
		ret.setGpiColumnScores(gpiDat);
		ret.setCogColumnScores(cogDat);
		return ret;
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	private boolean hasType(String type) throws Exception {
		if (!(type.equals("GPI") || type.equals("Cogs"))) {
			throw new IllegalArgumentException("Illegal type parameter in getType().  parm=" + type);
		}

		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT COUNT(*) AS cnt");
		sqlQuery.append("  FROM pdi_abd_dna_link dl ");
		sqlQuery.append("    INNER JOIN pdi_abd_module sims ON (sims.moduleId = dl.moduleId ");
		sqlQuery.append("                                   AND sims.internalName = '" + type + "') ");
		sqlQuery.append("  WHERE dl.dnaId = " + this.dnaId + " ");
		sqlQuery.append("    AND dl.competencyId = " + this.compId + " ");
		sqlQuery.append("    AND dl.isUsed = 1");
		// System.out.println("hasType() - query=" + sqlQuery);

		boolean ret = false;
		try {
			stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());
			// count() always returns a row
			rs.next();
			int cnt = rs.getInt("cnt");

			ret = (cnt > 0) ? true : false;
		} catch (SQLException e) {

		} finally {
			// clean up
			try {
				rs.close();
			} catch (SQLException sqlEx) {
				System.out.println("ERROR: rs close() in hasType() - " + sqlEx.getMessage());
			}
			try {
				stmt.close();
			} catch (SQLException sqlEx) {
				System.out.println("ERROR: stmt close() in hasType() - " + sqlEx.getMessage());
			}
			rs = null;
			stmt = null;

		}

		return ret;
	}
}