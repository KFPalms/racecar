/*
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.setup.ut;

import java.sql.Connection;
import java.util.Iterator;

import com.pdi.data.abyd.dto.setup.Setup2ReportOptionsDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportOptionValueDTO;
import com.pdi.data.abyd.helpers.setup.ReportOptionsDataHelper;
import com.pdi.ut.UnitTestUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;

import junit.framework.TestCase;

public class ReportOptionsDataHelperUT extends TestCase
{
	//
	// Constructors
	//

	public ReportOptionsDataHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
		throws Exception
    {
		junit.textui.TestRunner.run(EntryStateHelperUT.class);
    }


	/*
	 * testGetSetup2ReportOptionsData
	 */
	public void testGetSetup2ReportOptionsData()
		throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);

		Connection con = AbyDDatabaseUtils.getDBConnection();
		//String ret = "database URL=" + con.getMetaData().getURL() + "\n";
	
		// set a dna id
		long dnaId = 175;
		ReportOptionsDataHelper reportOptionsDataHelper = new ReportOptionsDataHelper();
		Setup2ReportOptionsDTO dto = reportOptionsDataHelper.getReportOptionData(con, dnaId);
		
		Iterator<Setup2ReportOptionValueDTO> it = dto.getOptionValuesArray().iterator();
		
		while(it.hasNext()){
			
			Setup2ReportOptionValueDTO valDto = it.next();
			if(valDto != null){
				System.out.println("valDto.getSequence()         " + valDto.getSequence());
				//System.out.println("valDto.getReportOptionsId()  " + valDto.getReportOptionsId());
				System.out.println("valDto.getLabel()            " + valDto.getLabel());
				//System.out.println("valDto.getOptionCode()       " + valDto.getOptionCode());
				System.out.println("valDto.getOptionValue()      " + valDto.getOptionValue());
				System.out.println("---------------------------------------------------------------");
			}
		}
		
		
		UnitTestUtils.stop(this);
	}
	
	/*
	 * testUpdateSetup2ReportOptionsData
	 */
	public void testUpdateSetup2ReportOptionsData()
		throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(false);

		Connection con = AbyDDatabaseUtils.getDBConnection();
		//String ret = "database URL=" + con.getMetaData().getURL() + "\n";
		
		/*
		 * to test this, i'll start with 2 identical 
		 * Setup2ReportOptionsDTO's  
		 * 
		 * then i'll change one, and then compare them 
		 */
		
		// set a dna id
		long dnaId = 175;

		// first, need to have 2 dto's to start with:	
		// get the first one from the database
		ReportOptionsDataHelper reportOptionsDataHelper = new ReportOptionsDataHelper();
		Setup2ReportOptionsDTO dto1 = reportOptionsDataHelper.getReportOptionData(con, dnaId);
		
		// now need to set up the second one and change it from the first.... 
		Setup2ReportOptionsDTO dto2 = reportOptionsDataHelper.getReportOptionData(con, dnaId);
	
		//create changed value dto's :
		Setup2ReportOptionValueDTO change5 = new Setup2ReportOptionValueDTO();
		if(dto1.getOptionValuesArray().get(4).getOptionValue()){
			change5.setOptionValue(false);
		}else{
			change5.setOptionValue(true);
		}		
		change5.setReportOptionsId(5);
		dto2.getOptionValuesArray().set(4, change5);
		System.out.println("TESTING: dto1 # 5 = " + dto1.getOptionValuesArray().get(4).getOptionValue());
		System.out.println("TESTING: dto2 # 5 = " + dto2.getOptionValuesArray().get(4).getOptionValue());
		
		Setup2ReportOptionValueDTO change10 =  new Setup2ReportOptionValueDTO();
		if(dto1.getOptionValuesArray().get(9).getOptionValue()){
			change10.setOptionValue(false);
		}else{
			change10.setOptionValue(true);
		}		
		change10.setReportOptionsId(10);
		dto2.getOptionValuesArray().set(9, change10);
		System.out.println("TESTING: dto1 # 10 = " + dto1.getOptionValuesArray().get(9).getOptionValue());
		System.out.println("TESTING: dto2 # 10 = " + dto2.getOptionValuesArray().get(9).getOptionValue());		
		
		// do the update
		reportOptionsDataHelper.updateReportOptionsData(con, dto2);
		
		//get the dto from the database, and make sure that the changes happened....
		Setup2ReportOptionsDTO dto3 = reportOptionsDataHelper.getReportOptionData(con, dnaId);
		

		System.out.println("-------------------");
		System.out.println("dto1: 0 "+ dto1.getOptionValuesArray().get(0).getReportOptionsId() + " : " + dto1.getOptionValuesArray().get(0).getOptionValue() );
		System.out.println("dto3: 0 "+ dto3.getOptionValuesArray().get(0).getReportOptionsId() + " : " + dto3.getOptionValuesArray().get(0).getOptionValue() );

		System.out.println("-------------------");
		System.out.println("dto1: 5 "+ dto1.getOptionValuesArray().get(4).getReportOptionsId() + " : " + dto1.getOptionValuesArray().get(4).getOptionValue() );
		System.out.println("dto3: 5 "+ dto3.getOptionValuesArray().get(4).getReportOptionsId() + " : " + dto3.getOptionValuesArray().get(4).getOptionValue() );

		System.out.println("-------------------");
		System.out.println("dto1: 10 "+ dto1.getOptionValuesArray().get(9).getReportOptionsId() + " : " + dto1.getOptionValuesArray().get(9).getOptionValue() );
		System.out.println("dto3: 10 "+ dto3.getOptionValuesArray().get(9).getReportOptionsId() + " : " + dto3.getOptionValuesArray().get(9).getOptionValue() );

		
		UnitTestUtils.stop(this);
	}
	
	
}
