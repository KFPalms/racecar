/**
 * Copyright (c) 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.dto.common;
/**
 * @author		Keith Yamry
  */
public class DeliveryMethodDTO
{
	//
	// Instance data.
	//
	private int _deliveryMethodId;
	private int _seq;
	private String _displayName;
	

	//
	// Constructors.
	//
	
	// No parameter constructor
	public DeliveryMethodDTO()
	{
		// Does nothing presently
	}
	
	
	//
	// Instance methods.
	//

	/*
	 * toString - return the content of this object as a string
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		String ret = "";
		ret += _deliveryMethodId + "/" + _seq + " - " + _displayName;
		
		return ret;
	}
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//
			
	/*****************************************************************************************/
	public int getDeliveryMethodId()
	{
		return _deliveryMethodId;
	}

	public void setDeliveryMethodId(int value)
	{
		_deliveryMethodId = value;
	}
	
	/*****************************************************************************************/
	public int getSeq()
	{
		return _seq;
	}

	public void setSeq(int value)
	{
		_seq = value;
	}
	
	/*****************************************************************************************/
	public String getDisplayName()
	{
		return _displayName;
	}

	public void setDisplayName(String value)
	{
		_displayName = value;
	}
}
