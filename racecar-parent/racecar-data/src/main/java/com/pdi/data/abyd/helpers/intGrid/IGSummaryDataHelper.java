/**
 * Copyright (c) 2009,2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.intGrid;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.w3c.dom.Document;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdi.data.abyd.dto.common.CompElementDTO;
import com.pdi.data.abyd.dto.common.CompGroupData;
import com.pdi.data.abyd.dto.common.DNACellDTO;
import com.pdi.data.abyd.dto.common.ModDataDTO;
import com.pdi.data.abyd.dto.intGrid.IGFinalScoreDTO;
import com.pdi.data.abyd.dto.intGrid.IGIntersectionAndOtherDataDTO;
import com.pdi.data.abyd.dto.intGrid.IGSummaryDataDTO;
import com.pdi.data.abyd.helpers.common.HelperUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.data.abyd.util.Utils;
import com.pdi.data.dto.Participant;
import com.pdi.data.dto.Project;
import com.pdi.data.dto.ProjectParticipantInstrument;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.IProjectHelper;
import com.pdi.data.helpers.interfaces.IProjectParticipantInstrumentHelper;
//import com.pdi.data.nhn.util.NhnDatabaseUtils;
//import com.pdi.data.util.DataLayerPreparedStatement;
//import com.pdi.data.util.DataResult;
//import com.pdi.data.util.RequestStatus;
import com.pdi.data.util.language.DefaultLanguageHelper;
//import com.pdi.logging.LogWriter;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.scoring.Norm;
import com.pdi.webservice.NhnWebserviceUtils;

/**
 * IGSummaryDataHelper contains the code needed to extract and return an
 * IGSummaryDataDTO. It provides data base access and data reduction for the
 * Assessment-by-Design Integration Grid generation app
 *
 * @author Ken Beukelman
 */
public class IGSummaryDataHelper {
	
	private static final Logger log = LoggerFactory.getLogger(IGSummaryDataHelper.class);
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private Connection con;
	private String lpId = "";
	private boolean isStartup = false;

	// stuff from linkparms
	private String partId;
	private long dnaId;
	private String partFname;
	private String partLname;

	// derived from linkparams
	private long projId = 0l;

	// Map to hold DNACellDTO objects
	HashMap<String, DNACellDTO> cellMap = new HashMap<String, DNACellDTO>();

	// instantiate a return object
	private final IGSummaryDataDTO ret = new IGSummaryDataDTO();

	// Internally used buckets
	private long gpiModId;
	private long cogModId;
	private long alpModId;
	private long k4dModId;

	private long finExModId = -1;
	private final ArrayList<Long> finExComps = new ArrayList<Long>();

	private long dfltLangId;

	private Utils utl = null;
	
	IGKf4dDataHelper kf4dHelper;
	
	Document projectInfo = null;

	//
	// Constructors.
	//

	/*
	 *  Limited usage constructor (cannot be used to invoke fetchSummaryData)
	 */
	public IGSummaryDataHelper(Connection con) {
		this.con = con;
	}

	/*
	 *  Another lLimited usage constructor.  It does not use a linkparams
	 *  to fill in the required data and the name is not passed.  It was
	 *  created to support the extract where that data already existed.
	 */
	public IGSummaryDataHelper(Connection con, String partId, long dnaId) throws Exception {
		this.con = con;
		this.partId = partId;
		this.dnaId = dnaId;
		this.partFname = "Not supplied";
		this.partLname = "Not supplied";

		// Get the default language
		this.dfltLangId = DefaultLanguageHelper.fetchDefaultLanguageId();
	}

	/*
	 * "Standard" constructor
	 */
	public IGSummaryDataHelper(Connection con, String lpId, boolean isStartup) throws Exception {
		// ensure there is a linkparams id
		if (lpId == null || lpId.length() < 1) {
			throw new Exception("A linkparms id is required to fetch Integration Grid data.");
		}

		this.con = con;
		this.lpId = lpId;
		this.isStartup = isStartup;

		// Get the linkparams data into the system
		fetchLinkparams();

		// Get the default language
		this.dfltLangId = DefaultLanguageHelper.fetchDefaultLanguageId();

		return;
	}

	//
	// Instance methods.
	//

	/**
	 * Fetch the data from the linkparams table - Used in method construction
	 *
	 * @throws Exception
	 */
	private void fetchLinkparams() throws Exception {
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT lp.participantId, ");
		sqlQuery.append("       dna.dnaId, ");
		sqlQuery.append("       dna.projectId ");
		sqlQuery.append("  FROM linkparams lp ");
		sqlQuery.append("    LEFT JOIN pdi_abd_dna dna ON dna.projectId = lp.projectId ");
		sqlQuery.append("  WHERE lp.linkparamId = " + this.lpId + " ");
		sqlQuery.append("    AND lp.active = 1");

		try {
			stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			if (!rs.isBeforeFirst()) {
				throw new Exception("No linkparams entry for lpId " + this.lpId);
			}

			// Get the first result row (only row)
			rs.next();

			this.partId = rs.getString("participantId");
			this.dnaId = rs.getLong("dnaId");
			this.projId = rs.getLong("projectId");

			if (this.dnaId == 0) {
				throw new Exception("No DNA id found for LPID=" + this.lpId);
			}

			// Get participant info
			SessionUser su = HelperDelegate.getSessionUserHelperHelper().createSessionUser();
			Participant participant = HelperDelegate.getParticipantHelper("com.pdi.data.nhn.helpers.delegated")
					.fromId(su, this.partId);

			this.partLname = participant.getLastName();
			this.partFname = participant.getFirstName();

			return;
		} catch (SQLException ex) {
			// handle any errors
			throw new Exception("SQL error in EG fetchLinkparams.  " + "SQLException: " + ex.getMessage() + ", "
					+ "SQLState: " + ex.getSQLState() + ", " + "VendorError: " + ex.getErrorCode());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					/*  Swallow the error */ }
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					/*  Swallow the error */ }
				stmt = null;
			}
		}
	}

	/**
	 * Convenience method - Gets the submitted date for AbyD reporting in MMMM
	 * yyyy format
	 *
	 * @throws Exception
	 */
	public String getIGSubmittedDate(Long dnaId, String participantId) throws Exception {
		return fetchAndFormatIGSubmittedDate(dnaId, participantId, false, "");
	}

	/**
	 * Convenience method - Gets the submitted date for AbyD reporting in MMMM
	 * dd, yyyy format
	 *
	 * @throws Exception
	 */
	public String getFullIGSubmittedDate(Long dnaId, String participantId) throws Exception {
		return fetchAndFormatIGSubmittedDate(dnaId, participantId, true, "");
	}

	/**
	 * Convenience method - Gets the submitted date for AbyD reporting in
	 * requested format
	 *
	 * @throws Exception
	 */
	public String getIGSubmittedDateWithFormat(Long dnaId, String participantId, String myFormat) throws Exception {
		return fetchAndFormatIGSubmittedDate(dnaId, participantId, true, myFormat);
	}

	/**
	 * Private method that does the work of getting the A by D admin date
	 *
	 * @throws Exception
	 */
	private String fetchAndFormatIGSubmittedDate(Long dnaId, String participantId, boolean doDay, String myFormat)
			throws Exception {
		Statement stmt = null;
		ResultSet rs = null;

		String dateStr = "";

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT igr.igLockDate, ");
		sqlQuery.append("       igr.lastDate ");
		sqlQuery.append("  FROM pdi_abd_igrid_response igr ");
		sqlQuery.append("  WHERE igr.dnaId = " + dnaId);
		sqlQuery.append("  and igr.participantid = '" + participantId + "'");

		try {
			stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			if (!rs.isBeforeFirst()) {
				throw new Exception("No IG data for DNA ID " + dnaId + ", participantId = " + participantId);
			}

			// Get the first result row
			rs.next();
			java.sql.Date lock = rs.getDate("igLockDate");
			java.sql.Date last = rs.getDate("lastDate");
			java.sql.Date date = lock != null ? lock : last;

			String fmt = "";
			if (myFormat.equals("") || myFormat.length() < 1) {
				fmt = doDay ? "MMMM d, yyyy" : "MMMM yyyy";
			} else if (myFormat.length() > 0) {
				fmt = myFormat;
			}

			SimpleDateFormat sdf = new SimpleDateFormat(fmt);
			dateStr = sdf.format(date);
			// System.out.println("DATE STRING... " + dateStr);

			return dateStr;
		} catch (SQLException ex) {
			// handle any errors
			throw new Exception("SQL error in fetchAndFormatIGSubmittedDate.  " + "SQLException: " + ex.getMessage()
					+ ", " + "SQLState: " + ex.getSQLState() + ", " + "VendorError: " + ex.getErrorCode());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					/*  Swallow the error */ }
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					/*  Swallow the error */ }
				stmt = null;
			}
		}
	}

	/**
	 * Controller method that fills the DTO.
	 *
	 * @throws Exception
	 */
	public IGSummaryDataDTO getSummaryData() throws Exception {
		// See if we need to score the ALP
		log.debug("Performing ALR Score check");
		alrScoreCheck(this.isStartup);
		log.debug("Finished ALR Score check");
		this.isStartup = false; // No need to force scoring after the check has
								// been done

		// Stick in stuff from the linkparams
		this.ret.setDnaId(this.dnaId);
		this.ret.setPartId(this.partId);
		this.ret.setPartFname(this.partFname);
		this.ret.setPartLname(this.partLname);
		
		this.projectInfo = new NhnWebserviceUtils().fetchProjectInfo(String.valueOf(projId));

		// Get some more IG-unique stuff
		log.debug("Get IG Unique Data");
		getIGUniqueData();
		log.debug("Finished Get IG Unique Data");

		// Now fill the DNA Structure DTO with appropriate data
		// First, get the module and competency lists...
		log.debug("Get Module and Competency Lists");
		getModAndCompLists();
		log.debug("Finished get Module and Competency Lists");

		// ...then get the score data for the intersections (but not yet in ret)
		// Set up the intersections
		log.debug("Setup Intersection Data");
		setUpIntersectionData();
		log.debug("Finished Setup Intersection data");

		// Special processing for FinEx
		log.debug("Finex Result conversion");
		finExResultConversion();
		log.debug("Finished Finex Result conversion");

		// Add the Eval Guide scores to the intersections (but not yet in ret)
		log.debug("Get Eval Guide Scores");
		getEGScores();
		log.debug("Finished get Eval Guide Scores");

		// Add the GPI & cog scores to the intersections (but not yet in ret)
		log.debug("Setup Testing Scores");
		setUpTestingScores();
		log.debug("Finished Setup Testing Scores");

		// Move the intersections to ret.
		log.debug("Move the Intersections to the Return object");
		this.ret.getGridData().getModCompIntersection().addAll(this.cellMap.values());
		log.debug("Finished Move the Intersections to the Return object");

		// Build the report list
		log.debug("Build the Report List");
		setUpReportList();
		log.debug("Finished Build the Report List");
		
		// Get the entered final scores
		log.debug("Get the entered final scores");
		this.ret.setCompFinData(getFinalScores());
		log.debug("Finished Get the entered final scores");

		// See if there should be a dLCI & final score
		log.debug("Calculate dLCI and final Score");
		calcDlciAndFinal();
		log.debug("Finished Calculate dLCI and final Score");

		// Get gpiIncluded boolean for UI display purposes...
		this.ret.setGpiIncluded(checkForGPIinDna(this.dnaId));

		return this.ret;
	}

	/**
	 * Fetch some data that is needed for the summary display. Adds the info to
	 * the instance DTO object.
	 *
	 * @throws Exception
	 */
	private void getIGUniqueData() throws Exception {
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT dna.dnaName as projectName, ");
		sqlQuery.append("       dna.projectId, ");
		sqlQuery.append("       dna.synthMean, ");
		sqlQuery.append("       dna.synthStdDev, ");
		sqlQuery.append("       igr.leadFirstName, ");
		sqlQuery.append("       igr.leadLastName, ");
		sqlQuery.append("       igr.igSubmitted, ");
		// sqlQuery.append(" igr.lastDate, ");
		sqlQuery.append("       igr.igLockDate, ");
		sqlQuery.append("       igr.workingNotes ");
		sqlQuery.append("  FROM pdi_abd_dna dna ");
		sqlQuery.append("    LEFT JOIN pdi_abd_igrid_response igr ON ");
		sqlQuery.append("                       (igr.dnaId = dna.dnaId ");
		sqlQuery.append("                    AND igr.participantId = " + this.partId + ") ");
		sqlQuery.append("  WHERE dna.dnaId = " + this.dnaId);

		try {
			stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			if (!rs.isBeforeFirst()) {
				throw new Exception(
						"No IG data for partId=" + this.partId + ", dnaId=" + this.dnaId + ". lpId=" + this.lpId);
			}

			// Get the first result row
			rs.next();
			this.ret.setProjectName(rs.getString("projectName"));
			this.ret.setProjectId(rs.getString("projectId"));
			this.ret.setLcFname(rs.getString("leadFirstName"));
			this.ret.setLcLname(rs.getString("leadLastName"));
			this.ret.setMean(rs.getDouble("synthMean"));
			this.ret.setStdDev(rs.getDouble("synthStdDev"));
			this.ret.setSubmitted(rs.getBoolean("igSubmitted"));

			if (rs.getDate("igLockDate") != null) {
				java.sql.Date date = rs.getDate("igLockDate");
				SimpleDateFormat sdf = new SimpleDateFormat("MMMM yyyy");
				this.ret.setSubmittedDate(sdf.format(date));
			} else {
				this.ret.setSubmittedDate("IG not submitted");
			}

			this.ret.setWorkingNotes(rs.getString("workingNotes"));

			// Get the client name
			SessionUser session = new SessionUser();
			IProjectHelper iproject = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated");
			Project project = iproject.fromId(session, "" + this.ret.getProjectId());

			this.ret.setClientName(project.getClient().getName());

			return;
		} catch (SQLException ex) {
			// handle any errors
			throw new Exception("SQL error in getIGUniqueData.  " + "SQLException: " + ex.getMessage() + ", "
					+ "SQLState: " + ex.getSQLState() + ", " + "VendorError: " + ex.getErrorCode());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					/*  Swallow the error */ }
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					/*  Swallow the error */ }
				stmt = null;
			}
		}
	}

	/**
	 * Get the Modules and Competencies for this Integration Grid This uses the
	 * current DNA id to get only those items allowed in this DNA.
	 *
	 * Note that this list is similar to that in the Setup but the competency
	 * list used has the superfactor level data present. Setup should eventually
	 * be refactored to use the full competency list.
	 *
	 * @throws Exception
	 */
	private void getModAndCompLists() throws Exception {
		Statement stmt = null;
		ResultSet rs = null;

		// The following query brings back both module and competency data at
		// the same time.
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("  SELECT DISTINCT 1 as typer, ");
		sqlQuery.append("                  dl.moduleId as modId, ");
		sqlQuery.append("                  dl.modSeq as modSeq, ");
		sqlQuery.append("                  mtxt.text as modName, ");
		sqlQuery.append("                  mdul.internalName as mIntName, ");
		sqlQuery.append("                  dl.modEdit as modEdit, ");
		sqlQuery.append("                  ISNULL(egr.egSubmitted,0) as modSubmitted, ");
		sqlQuery.append("                  NULL as groupId, ");
		sqlQuery.append("                  0 as groupSeq, ");
		sqlQuery.append("                  NULL as groupName, ");
		sqlQuery.append("                  NULL as cmpId, ");
		sqlQuery.append("                  0 as cmpSeq, ");
		sqlQuery.append("                  NULL as cmpName, ");
		sqlQuery.append("                  NULL as essentialFlag ");
		sqlQuery.append("    FROM pdi_abd_dna_link dl ");
		sqlQuery.append("      INNER JOIN pdi_abd_module mdul ON mdul.moduleId = dl.moduleId ");
		sqlQuery.append("      LEFT JOIN pdi_abd_text mtxt ON (mtxt.textId = mdul.textId AND mtxt.languageId = "
				+ this.dfltLangId + ") ");
		sqlQuery.append("      LEFT JOIN pdi_abd_eg_response egr ON (egr.participantId = '" + this.partId + "' ");
		sqlQuery.append("                                        AND egr.dnaId = dl.dnaId ");
		sqlQuery.append("                                        AND egr.moduleId = dl.moduleId) ");
		sqlQuery.append("    WHERE dl.dnaId = " + this.dnaId + " ");
		sqlQuery.append("      AND dl.isUsed = 1 ");
		sqlQuery.append("UNION ");
		sqlQuery.append("  SELECT distinct 2 as typer, ");
		sqlQuery.append("                  NULL as modId, ");
		sqlQuery.append("                  0 as modSeq, ");
		sqlQuery.append("                  NULL as modName, ");
		sqlQuery.append("                  NULL as mIntName, ");
		sqlQuery.append("                  NULL as modEdit, ");
		sqlQuery.append("                  NULL as modSubmitted, ");
		sqlQuery.append("                  dl.groupId, ");
		sqlQuery.append("                  dl.groupSeq as groupSeq, ");
		sqlQuery.append("                  gtxt.text as groupName, ");
		sqlQuery.append("                  dl.competencyId as cmpId, ");
		sqlQuery.append("                  dl.compSeq as cmpSeq, ");
		sqlQuery.append("                  ctxt.text as cmpName, ");
		sqlQuery.append("                  NULL as essentialFlag ");
		sqlQuery.append("    FROM pdi_abd_dna_link dl ");
		sqlQuery.append("      INNER JOIN pdi_abd_dna_grouping_value gv ON gv.groupId = dl.groupId ");
		sqlQuery.append("      LEFT JOIN pdi_abd_text gtxt ON (gtxt.textId = gv.textId AND gtxt.languageId = "
				+ this.dfltLangId + ") ");
		sqlQuery.append("      INNER JOIN pdi_abd_competency cmp ON cmp.competencyId = dl.competencyId ");
		sqlQuery.append("      LEFT JOIN pdi_abd_text ctxt ON (ctxt.textId = cmp.textId AND ctxt.languageId = "
				+ this.dfltLangId + ") ");
		sqlQuery.append("    WHERE dl.dnaId = " + this.dnaId + " ");
		sqlQuery.append("      AND dl.isUsed = 1 ");
		sqlQuery.append("    ORDER BY typer, modseq, groupseq, cmpseq ");
		// System.out.println("sqlQuery: " + sqlQuery.toString());
		try {
			stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			if (!rs.isBeforeFirst()) {
				throw new Exception("No module/competency data exists for DNA ID " + this.dnaId);
			}

			// process into the mod list and the comp list
			long curSfId = 0;
			CompGroupData curSfGroup = null;
			while (rs.next()) {
				int type = rs.getInt("typer");
				if (type == 1)// Module
				{
					ModDataDTO entry = new ModDataDTO();
					entry.setModId(rs.getLong("modId"));
					entry.setModName(rs.getString("modName"));
					entry.setModEditable(rs.getBoolean("modEdit"));
					entry.setModSubmitted(rs.getBoolean("modSubmitted"));
					this.ret.getGridData().getModArray().add(entry);
					if (rs.getString("mIntName").equals("GPI")) {
						this.gpiModId = rs.getLong("modId");
					}
					if (rs.getString("mIntName").equals("Cogs")) {
						this.cogModId = rs.getLong("modId");
					}
					if (rs.getString("mIntName").equals("ALP")) {
						this.alpModId = rs.getLong("modId");
					}
					if (rs.getString("mIntName").equals("KF4D")) {
						this.k4dModId = rs.getLong("modId");
					}
					if (rs.getString("mIntName").equals("Financial Exercise")) {
						this.finExModId = rs.getLong("modId");
					}
				} else if (type == 2)// Superfactor and Competency
				{
					// see if same superfactor
					long grpId = rs.getLong("groupId");
					if (grpId != curSfId) {
						// no, make new one, add it to the list and make it
						// current
						curSfGroup = new CompGroupData();
						curSfGroup.setCgId(rs.getLong("groupId"));
						curSfGroup.setCgName(rs.getString("groupName"));
						this.ret.getGridData().getFullCompArray().add(curSfGroup);
						curSfId = grpId;
					}
					// make new comp entry
					CompElementDTO cmp = new CompElementDTO();
					cmp.setCompId(rs.getLong("cmpId"));
					cmp.setCompName(rs.getString("cmpName"));
					cmp.setIsEssential(rs.getBoolean("essentialFlag"));
					// add it to the current superfactor
					curSfGroup.getCgCompList().add(cmp);
				}
			} // End of while loop

			return;
		} catch (SQLException ex) {
			// handle any errors
			throw new Exception("SQL error for getModAndCompLists.  " + "SQLException: " + ex.getMessage() + ", "
					+ "SQLState: " + ex.getSQLState() + ", " + "VendorError: " + ex.getErrorCode());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					/* swallow the error */ }
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					/* swallow the error */ }
				stmt = null;
			}
		}
	}

	/**
	 * Get all of the valid intersections for this DNA and store them internally
	 * for later use
	 *
	 * @throws Exception
	 */
	private void setUpIntersectionData() throws Exception {
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT dl.moduleId as module, ");
		sqlQuery.append("       dl.competencyId as comp ");
		sqlQuery.append("  FROM pdi_abd_dna_link dl ");
		sqlQuery.append("  WHERE dl.dnaId = " + this.dnaId + " ");
		sqlQuery.append("    AND dl.isUsed = 1 ");
		sqlQuery.append("    AND dl.active = 1 ");
		sqlQuery.append("  ORDER BY dl.moduleId, dl.competencyId");

		try {
			stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			if (!rs.isBeforeFirst()) {
				throw new Exception("Active intersection data does not exist for DNA ID " + this.dnaId);
			}

			// process into the temp map
			while (rs.next()) {
				long mod = rs.getLong("module");
				long cmp = rs.getLong("comp");
				DNACellDTO value = new DNACellDTO();
				value.setCompetencyId(cmp);
				value.setModuleId(mod);
				value.setScore("-1.00"); // Default value - indicates score not
											// present
				String key = "" + mod + "|" + cmp;
				this.cellMap.put(key, value);

				if (mod == finExModId) {
					finExComps.add(cmp);
				}
			}

			return;
		} catch (SQLException ex) {
			// handle any errors
			throw new Exception("SQL for setUpIntersectionData.  " + "SQLException: " + ex.getMessage() + ", "
					+ "SQLState: " + ex.getSQLState() + ", " + "VendorError: " + ex.getErrorCode());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					/* swallow the error */ }
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					/* swallow the error */ }
				stmt = null;
			}
		}
	}

	/**
	 * Do the special processing for FinEx data, if needed
	 *
	 * @throws Exception
	 */
	private void finExResultConversion() throws Exception {
		// See if the FinEx is an active module
		if (finExModId < 1) {
			// Not an active module
			return;
		}

		// See if there are any intersections with the fin ex in them
		if (finExComps.isEmpty()) {
			// Not used in any active intersections
			return;
		}

		HashMap<String, Integer> feScores = getFinExResults();
		if (feScores == null) {
			// No data to migrate... done
			return;
		}

		// Got data. See which comps need bar response data and fill as needed
		for (int i = 0; i < finExComps.size(); i++) {
			checkAndFillComps(finExComps.get(i), feScores);
		}

		return;
	}

	/*
	 * Get the FinEx scores from the results table
	 * A return of null means there is no data
	 */
	private HashMap<String, Integer> getFinExResults() throws Exception {
		HashMap<String, Integer> feScores = new HashMap<String, Integer>();

		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT rr.spssValue, ");
		sqlQuery.append("       rr.rawScore ");
		sqlQuery.append("  FROM results rr ");
		sqlQuery.append("  WHERE rr.ParticipantId = '" + partId + "' ");
		sqlQuery.append("   AND rr.instrumentCode = '" + IGConstants.I_CODE_FINEX + "' ");
		sqlQuery.append("  ORDER BY rr.spssValue, rr.lastDate");

		try {
			stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());
			if (!rs.isBeforeFirst()) {
				// no scores... Can do nothing here
				return null;
			}

			// process the scores into the list
			while (rs.next()) {
				feScores.put(rs.getString("spssValue"), rs.getInt("rawScore"));
			}

			return feScores;
		} catch (SQLException ex) {
			// handle any errors
			throw new Exception("SQL error in getFinExResults.  " + "SQLException: " + ex.getMessage() + ", "
					+ "SQLState: " + ex.getSQLState() + ", " + "VendorError: " + ex.getErrorCode());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					/* swallow the error */ }
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					/* swallow the error */ }
				stmt = null;
			}
		}
	}

	/**
	 * checkAndFillComps - See which comps need bar response data and fill as
	 * needed This code loops through a set of competency ids and: 1) checks to
	 * see if there is data in them 2) if so, skip the rest of the processing
	 * for the comp 3) if not, insert the FinEx scores into the bar response
	 * table
	 *
	 * @param long
	 *            compId - competency id
	 * @param HashMap<String,
	 *            Integer>feScores - map of fin ex scores
	 * @throws Exception
	 */
	private void checkAndFillComps(long compId, HashMap<String, Integer> feScores) throws Exception {
		Statement stmt = null;
		ResultSet rs = null;
		PreparedStatement pStmt = null;
		StringBuffer sqlQuery = new StringBuffer();

		try {
			sqlQuery.append("SELECT COUNT(*) AS cnt ");
			sqlQuery.append("  FROM pdi_abd_eg_response egr ");
			sqlQuery.append("  WHERE egr.participantId = '" + this.partId + "' ");
			sqlQuery.append("    AND egr.dnaId = " + this.dnaId + " ");
			sqlQuery.append("    AND egr.moduleId = " + this.finExModId);
			try {
				// See if the data exists
				stmt = this.con.createStatement();
				rs = stmt.executeQuery(sqlQuery.toString());

				/*
				 * NOTE:  THIS MAY BE A TO-DO
				 * Had a prod issue where the database was filled in with wrong data
				 * because the database pointer in the import from adapt was incorrect.
				 * The initial time that the IG is pulled up, the code goes through and does a
				 * calculation of the Financial Excercise (IG SummaryDataHelper.java  --
				 * checkAndFillComps() ) and at that point the bar scores are inserted into
				 * pdi_abd_eg_bar_response, and the completed is set in pdi_abd_eg_response.

					Subsequent times that the IG is opened, it merely checks to see that
					there is a row for that module in the eg_response, and does nothing with
					the bar_response table.

					What appears to have happened here is that the first time this IG was
					opened, it was pointed at the wrong database so it filled in the bar_response
					with 1's instead of the actual data.  Since it would appear, after that, to
					the code that all was well, it never updated those scores, despite the fact
					that they were updated in response data.

					An easy way around this:
					    1) make the code in the checkAndFillComps() method always either
					    insert or update, rather than just the initial
					        insert.
					Another possible option??
					     2) not sure if this is feasible, but on data import,
					     update the bar scores for the FE.
					Yet another option.... Do an update and if no rows are updated do an insert OR
						do an insert and if code 1062 comes back do an update (assumes unique key)

					NOTE - 2-6-2012 - Yup.  As always, Ken is right, and am fixing using the 1062...
							NHN -     NHN-1765 - updating FinEx bar scores in IG if scores are changed in TestData

				 */

				// Only one result
				rs.next();
				if (rs.getInt("cnt") > 0) {
					// 2-6-2012
					// there is a row, so let's make sure that it has the latest
					// data,
					// because if changes were made in test data, it didn't
					// update the bar scores
					// call the update function....
					updateFinExBarResponseData(compId, feScores);

				}

				rs.close();
				stmt.close();
			} catch (SQLException ex) {
				// handle any errors
				throw new Exception("SQL error in checkAndFillComps (1).  " + "SQLException: " + ex.getMessage() + ", "
						+ "SQLState: " + ex.getSQLState() + ", " + "VendorError: " + ex.getErrorCode());
			}

			// OK, so now we think that we need to move the scores.
			String phase = null;
			sqlQuery.setLength(0);
			sqlQuery.append("INSERT INTO pdi_abd_eg_bar_response ");
			sqlQuery.append("  (participantId, ");
			sqlQuery.append("   dnaId, ");
			sqlQuery.append("   moduleId, ");
			sqlQuery.append("   competencyId, ");
			sqlQuery.append("   barScore, ");
			sqlQuery.append("   barSequence, ");
			sqlQuery.append("   lastUserId, ");
			sqlQuery.append("   lastDate) ");
			sqlQuery.append("  VALUES(" + this.partId + ", ");
			sqlQuery.append("         " + this.dnaId + ", ");
			sqlQuery.append("         " + this.finExModId + ", ");
			sqlQuery.append("         " + compId + ", ");
			sqlQuery.append("         ?, ");
			sqlQuery.append("         ?, ");
			sqlQuery.append("         SYSTEM_USER,");
			sqlQuery.append("         GETDATE())");

			try {
				// Set up the prepared statement.
				phase = "Inserting bar scores";
				pStmt = con.prepareStatement(sqlQuery.toString());

				// Loop through the bars in order an put in the data
				// Note that we start at 1; i = bar sequence number
				for (int i = 1; i < IGConstants.FINEX_BAR_POSN.size(); i++) {
					// fill in the data
					pStmt.setInt(1, feScores.get(IGConstants.FINEX_BAR_POSN.get(i)));
					pStmt.setInt(2, i);
					pStmt.executeUpdate();
				}

				// Now write out a response row
				sqlQuery.setLength(0);
				phase = "Inserting response row";
				sqlQuery.append("INSERT INTO pdi_abd_eg_response ");
				sqlQuery.append(
						"  (participantId, dnaId, moduleId, egSubmitted, raterFirstName, raterLastName, simLangId, lastUserId, lastDate) ");
				sqlQuery.append("  VALUES(" + this.partId + ", " + this.dnaId + ", " + this.finExModId + ", " + "1, "
						+ "N'', " + "N'', " + this.dfltLangId + ", " + "SYSTEM_USER, " + "GETDATE())");
				stmt = this.con.createStatement();
				stmt.executeUpdate(sqlQuery.toString());
			} catch (SQLException ex) {
				// this shouldn't actually happen, but just in case...
				// TODO Refactor so that the update does not depend upon the
				// error code (do a query)
				// Duplicate key error triggers update; error code = 1062 on
				// MySQL, 2627 on SQL Server
				if (ex.getErrorCode() == 2627) {
					// 2-6-2012
					// insert fails because
					// there is already a row, so let's make sure that it has
					// the latest data,
					// because if changes were made in test data, it didn't
					// update the bar scores
					// call the update function....
					updateFinExBarResponseData(compId, feScores);

				} else {
					// handle any errors
					throw new Exception("SQL for insert FinEx scores as BARs(" + phase + ").  " + "SQLException: "
							+ ex.getMessage() + ", " + "SQLState: " + ex.getSQLState() + ", " + "VendorError: "
							+ ex.getErrorCode());
				}

			}
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					/* swallow the error */ }
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					/* swallow the error */ }
				stmt = null;
			}
			if (pStmt != null) {
				try {
					pStmt.close();
				} catch (SQLException sqlEx) {
					/* swallow the error */ }
				pStmt = null;
			}
		}
	}

	/**
	 * updateFinExBarResponseData - this updates the FinEx bar response data. It
	 * updates regardless of if any data has changed. This was implemented so
	 * that if scores for the finEx were changed in Test Data, that those would
	 * be updated into the IG bar/response data tables.
	 *
	 * @param long
	 *            compId - competency id
	 * @param HashMap<String,
	 *            Integer>feScores - map of fin ex scores
	 * @throws Exception
	 */
	private void updateFinExBarResponseData(long compId, HashMap<String, Integer> feScores) throws Exception {
		Statement stmt = null;
		ResultSet rs = null;
		PreparedStatement pStmt = null;
		StringBuffer sqlQuery = new StringBuffer();

		// update the bar scores
		String phase = null;
		sqlQuery.setLength(0);
		sqlQuery.append("UPDATE pdi_abd_eg_bar_response  ");
		sqlQuery.append("  SET barScore = ?, "); // 1
		sqlQuery.append("  	   lastUserId = SYSTEM_USER, ");
		sqlQuery.append("      lastDate = GETDATE() ");
		sqlQuery.append("   WHERE participantId = " + this.partId + " ");
		sqlQuery.append("   AND dnaId = " + this.dnaId + " ");
		sqlQuery.append("   AND moduleId = " + this.finExModId + " ");
		sqlQuery.append("   AND competencyId =" + compId + " ");
		sqlQuery.append("   AND barSequence = ?  "); // 2

		try {
			// Set up the prepared statement.
			phase = "Update bar scores";
			pStmt = con.prepareStatement(sqlQuery.toString());

			// Loop through the bars in order an put in the data
			// Note that we start at 1; i = bar sequence number
			for (int i = 1; i < IGConstants.FINEX_BAR_POSN.size(); i++) {
				// fill in the data
				pStmt.setInt(1, feScores.get(IGConstants.FINEX_BAR_POSN.get(i)));
				// System.out.println("feScores.get(IGConstants.FINEX_BAR_POSN.get(i))
				// " + feScores.get(IGConstants.FINEX_BAR_POSN.get(i)));
				pStmt.setInt(2, i);
				// System.out.println("i: " + i);
				pStmt.executeUpdate();
			}

			// Now update the a response row
			sqlQuery.setLength(0);
			phase = "Update response row";
			sqlQuery.append("UPDATE pdi_abd_eg_response  ");
			sqlQuery.append("  SET lastuserId = SYSTEM_USER, ");
			sqlQuery.append("  lastDate = GETDATE() ");
			sqlQuery.append("  where participantId = " + this.partId + " ");
			sqlQuery.append("  and dnaId = " + this.dnaId + " ");
			sqlQuery.append("  and moduleId = " + this.finExModId + " ");

			stmt = this.con.createStatement();
			stmt.executeUpdate(sqlQuery.toString());
		} catch (SQLException ex) {
			// handle any errors
			throw new Exception("SQL for update FinEx scores into pdi_abd_eg_response(" + phase + ").  "
					+ "SQLException: " + ex.getMessage() + ", " + "SQLState: " + ex.getSQLState() + ", "
					+ "VendorError: " + ex.getErrorCode());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					/* swallow the error */ }
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					/* swallow the error */ }
				stmt = null;
			}
			if (pStmt != null) {
				try {
					pStmt.close();
				} catch (SQLException sqlEx) {
					/* swallow the error */ }
				pStmt = null;
			}
		}
	}

	/**
	 * Get the Eval Guide scores that are present and available and put them
	 * into their appropriate intersection objects
	 *
	 * @throws Exception
	 */
	private void getEGScores() throws Exception {
		// get the scores
		ArrayList<DNACellDTO> egScores = IGEvalGuideScoring.calcEGScores(con, partId, dnaId);

		// Stick them in the appropriate spot in the cellMap
		for (DNACellDTO cell : egScores)
		// for (int idx=0; idx < egScores.size(); idx++)
		{
			// DNACellDTO cell = egScores.get(idx);
			String key = "" + cell.getModuleId() + "|" + cell.getCompetencyId();
			if (this.cellMap.get(key) == null) {
				throw new Exception("Score data for an invalid intersection; modID=" + cell.getModuleId() + ", compID="
						+ cell.getCompetencyId());
			}
			this.cellMap.get(key).setScore(cell.getScore());
		}
	}

	/**
	 * Get the GPI and Cog test scores (as PDI ratings) that are present and put
	 * them into their appropriate intersection objects
	 *
	 * @throws Exception
	 */
	private void setUpTestingScores() throws Exception {
		// Do I need to set up the testing scores?
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		boolean hasAlp = false;
		boolean hasKf4d = false;

		sqlQuery.append("SELECT DISTINCT(mm.internalName) as name, COUNT(ll.moduleId) as cnt ");
		sqlQuery.append("  FROM pdi_abd_dna_link ll ");
		sqlQuery.append("    LEFT JOIN pdi_abd_module mm ON mm.moduleId = ll.moduleId ");
		sqlQuery.append("  WHERE dnaId = " + this.dnaId + " ");
		sqlQuery.append("    AND ll.moduleId IN (SELECT moduleId ");
		sqlQuery.append("                          FROM pdi_abd_module ");
		sqlQuery.append("                          WHERE internalName IN ('GPI','Cogs','ALP','KF4D')) ");
		sqlQuery.append("  GROUP BY ll.moduleId, mm.internalName"); // Had to
																	// add
																	// internal
																	// name for
																	// SQL
																	// Server

		try {
			stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			if (!rs.isBeforeFirst()) {
				// No testing data to get.. set ret and scram
				this.ret.setGpiAndCogDataAvailable(false);
				this.ret.setAlpDataAvailable(false);
				this.ret.setKf4dDataAvailable(false);
				this.ret.setRdi(0);
				return;
			}
			int cnt = 0;
			while (rs.next()) {
				String name = rs.getString("name");
				if (name.equals("ALP")) {
					hasAlp = true;
				} else if (name.equals("KF4D")) {
					hasKf4d = true;
				}
				cnt += rs.getInt("cnt");
			}
			if (cnt < 1) {
				// No testing data to get.. set ret and scram
				this.ret.setGpiAndCogDataAvailable(false);
				this.ret.setAlpDataAvailable(hasAlp); // Should always be false
														// (no GPI, Cogs, or
														// ALP)
				this.ret.setKf4dDataAvailable(hasKf4d); // Should always be
														// false (no GPI, Cogs,
														// ALP, or KF4D)
				this.ret.setRdi(0);
				return;
			}
		} catch (SQLException ex) {
			// handle any errors
			throw new Exception("SQL error in setUpTestingScores.  " + "SQLException: " + ex.getMessage() + ", "
					+ "SQLState: " + ex.getSQLState() + ", " + "VendorError: " + ex.getErrorCode());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					/*  Swallow the error */ }
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					/*  Swallow the error */ }
				stmt = null;
			}
		}

		// Supposed to have testing scores... go on
		// Get the score related data - initial state has the cognitive tests
		// turned on
		// Defaults to "cogs on"
		log.debug("Fetching Intersection and other data DTO");
		if (hasKf4d && (kf4dHelper == null)){
			kf4dHelper = new IGKf4dDataHelper(con, partId, dnaId, projectInfo);
		}
		IGIntersectionAndOtherDataDTO data = IGScoring.fetchTestingIntersectionData(this.con, this.partId, this.dnaId,
				true, hasAlp, hasKf4d, kf4dHelper);
		log.debug("Finished fetching intersection and other data DTO");
		

		if (hasAlp || hasKf4d) {
			// Assume that there is no GPI and set that flag off
			this.ret.setGpiAndCogDataAvailable(false);
		} else {
			// Have GPI... do the old stuff
			if (data.getTestingCells() == null || data.getTestingCells().size() < 1) {
				this.ret.setGpiAndCogDataAvailable(false);
			} else {
				this.ret.setGpiAndCogDataAvailable(true);
			}
		}
		this.ret.setAlpDataAvailable(hasAlp);
		this.ret.setKf4dDataAvailable(hasKf4d);

		// Set the RDI
		this.ret.setRdi(data.getRdi());

		// Get the intersection data and make the proper output ArrayList
		ArrayList<DNACellDTO> tCells = data.getTestingCells();

		// find the gpi and cog indices into the mod name array and save them
		ArrayList<ModDataDTO> mods = this.ret.getGridData().getModArray();
		int gpiIdx = -1;
		int cogIdx = -1;
		int extIdx = -1;
		for (int i = 0; i < mods.size(); i++) {
			ModDataDTO modDto = mods.get(i);
			long id = modDto.getModId();
			if (gpiIdx == -1 && id == this.gpiModId) {
				gpiIdx = i;
			}
			if (cogIdx == -1 && id == this.cogModId) {
				cogIdx = i;
			}
			if (extIdx == -1 && (id == this.alpModId || id == this.k4dModId)) {
				extIdx = i;
			}
		}

		// remove the name data from the mods array
		// needs to be in specific order so the remove doesn't gack
		if (hasAlp || hasKf4d) {
			// Assume that there will be no alp/kf4d/GPI co-mingling in a model
			if (cogIdx > extIdx) {
				// cogs is highest and must be greater than -1
				mods.remove(cogIdx);
				if (extIdx != -1) {
					// Remove only if there is one (idx > -1)
					mods.remove(extIdx);
				}
			}
			if (extIdx > cogIdx) {
				// ext (alp/kf4d) is highest and must be greater than -1
				mods.remove(extIdx);
				if (cogIdx != -1) {
					// Remove only if there is one (idx > -1)
					mods.remove(cogIdx);
				}
			}
		} else {
			// This is the old way; still valid for legacy ALP
			if (cogIdx > gpiIdx) {
				// cogs is highest and must be greater than -1
				mods.remove(cogIdx);
				if (gpiIdx != -1) {
					// Remove only if there is one (idx > -1)
					mods.remove(gpiIdx);
				}
			}
			if (gpiIdx > cogIdx) {
				// gpi is highest and must be greater than -1
				mods.remove(gpiIdx);
				if (cogIdx != -1) {
					// Remove only if there is one (idx > -1)
					mods.remove(cogIdx);
				}
			}
		}

		// Put in the new "Testing" module at the lowest valid old index
		// (default to 0)
		// First, make the "Testing" ModDataDTO...
		ModDataDTO tMod = new ModDataDTO();
		tMod.setModId(IGConstants.TESTING_MOD_ID);
		tMod.setModName(hasAlp ? IGConstants.TESTING_MOD_NAME_ALP
				: (hasKf4d ? IGConstants.TESTING_MOD_NAME_KF4D : IGConstants.TESTING_MOD_NAME));

		tMod.setModEditable(true);
		tMod.setModSubmitted(false);

		// ...then determine the index...
		int insertIdx = 0; // Default to first position
		// Note that the single '&' (logical AND) is deliberate in this sequence
		// of 'if' statements.
		if (hasAlp || hasKf4d) {
			insertIdx = extIdx;
		} else {
			if (gpiIdx < cogIdx & gpiIdx > -1) {
				insertIdx = gpiIdx;
			} else if (cogIdx < gpiIdx & cogIdx > -1) {
				insertIdx = cogIdx;
			} else if (gpiIdx == cogIdx & gpiIdx > -1) {
				insertIdx = gpiIdx;
			}
		}

		// ...and shove it into the Mod array and put it back to the returned
		// DTO
		mods.add(insertIdx, tMod);
		this.ret.getGridData().setModArray(mods);

		// Now remove the intersections for the old columns and create
		// new intersections for the new columns. Data is still in cellMap at
		// this time
		// We have to get a list of the keys first, then delete them because
		// otherwise
		// the code will throw is a ConcurrentModificationException
		ArrayList<String> keyList = new ArrayList<String>();
		for (Iterator<Map.Entry<String, DNACellDTO>> itr = this.cellMap.entrySet().iterator(); itr.hasNext();) {
			Map.Entry<String, DNACellDTO> entry = itr.next();
			if (entry.getValue().getModuleId() == this.gpiModId || entry.getValue().getModuleId() == this.cogModId
					|| entry.getValue().getModuleId() == this.alpModId
					|| entry.getValue().getModuleId() == this.k4dModId) {
				// save the key in the list
				keyList.add(entry.getKey());
			}
		}

		// So now put the testing column data into the cellMap. I would expect
		// that
		// the module id in the cells will always be IGConstants.TESTING_MOD_ID.
		for (Iterator<DNACellDTO> itr = tCells.iterator(); itr.hasNext();) {
			DNACellDTO cell = itr.next();
			String key = "" + cell.getModuleId() + "|" + cell.getCompetencyId();

			for (Iterator<String> itr2 = keyList.iterator(); itr2.hasNext();) {
				String code = itr2.next();
				DNACellDTO cellItem = this.cellMap.get(code);
				// If this comp / mod intersection does not exist, then do not
				// include
				if (cellItem.getCompetencyId() == cell.getCompetencyId()) {
					this.cellMap.put(key, cell);
					break;
				}
			}
		}

		// Now delete the items whose keys we saved
		for (Iterator<String> itr = keyList.iterator(); itr.hasNext();) {
			String code = itr.next();
			this.cellMap.remove(code);
		}

		return;
	}

	/**
	 * Build the possible report list The original version used
	 * ParticipantDataHelper().getIndividualReportScoreData() which got all
	 * valid results data for the person (not just this project) and built an
	 * IndividualReport object for them. This logic then looked at the
	 * ReportData objects and built the list. This just queries the new results
	 * table for all instruments and builds the list off of that. CHQ has always
	 * been an outlier, however. More and mor outliers crop up. See logic at the
	 * end for logic for them
	 *
	 * @throws Exception
	 */
	private void setUpReportList() throws Exception {
		// This code does not work properly for those participant who are
		// members of multiple projects; false
		// positives appear in the list and can lead to problems down the line
		// (no norms are available).
		// We still want to filter with completion....

		// Look at the instruments for which we have scores and use that to see
		// if we will be generating reports.
		// That method works for all traditional instruments except CHQ, which
		// was an out-lier anyway
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT DISTINCT rr.instrumentCode ");
		sqlQuery.append("  FROM results rr ");
		sqlQuery.append("  WHERE rr.participantId = " + this.partId + " ");
		// Get them all like
		// ParticipantDataHelper.getIndividualReportScoreData() does
		// sqlQuery.append(" AND rr.projectId = " + this.ret.getProjectId());

		try {
			Boolean csFound = false;
			Set<String> instSet = new HashSet<String>();
			stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());
			if (rs.isBeforeFirst()) {
				// There are completed instruments... check 'em
				while (rs.next()) {
					instSet.add(rs.getString("instrumentCode"));
				} // End of the rs while loop
			} // End of rs content if (rs.isBeforFirst)
				// Now check if a) the instrument is valid, and b) if there is
				// data
				// available
			if ((instSet.contains(IGConstants.I_CODE_GPI) && isInstValid(IGConstants.I_CODE_GPI))
					|| (instSet.contains(IGConstants.I_CODE_GPI_SHORT) && isInstValid(IGConstants.I_CODE_GPI_SHORT))) {
				this.ret.getReportCodes().add(ReportingConstants.REPORT_CODE_GPI_GRAPHICAL);
			}
			if ((instSet.contains(IGConstants.I_CODE_CS) && isInstValid(IGConstants.I_CODE_CS))
					|| (instSet.contains(IGConstants.I_CODE_CS2) && isInstValid(IGConstants.I_CODE_CS2))) {
				// There is no cs report, but the flag is needed for later
				csFound = true;
			}
			if (instSet.contains(IGConstants.I_CODE_LEI) && isInstValid(IGConstants.I_CODE_LEI)) {
				this.ret.getReportCodes().add(ReportingConstants.REPORT_CODE_LEI_GRAPHICAL);
			}
			if ((instSet.contains(IGConstants.I_CODE_RAVB) && isInstValid(IGConstants.I_CODE_RAVB))
					|| (instSet.contains(IGConstants.I_CODE_RAVB2) && isInstValid(IGConstants.I_CODE_RAVB2))) {
				this.ret.getReportCodes().add(ReportingConstants.REPORT_CODE_RAV_B_GRAPHICAL);
			}
			if (instSet.contains(IGConstants.I_CODE_WGE) && isInstValid(IGConstants.I_CODE_WGE)) {
				this.ret.getReportCodes().add(ReportingConstants.REPORT_CODE_WGE_GRAPHICAL);
			}
			if (instSet.contains(IGConstants.I_CODE_FINEX) && isInstValid(IGConstants.I_CODE_FINEX)) {
				this.ret.getReportCodes().add(ReportingConstants.REPORT_CODE_FINEX_DETAIL);
			}
			if (this.ret.getReportCodes().contains(ReportingConstants.REPORT_CODE_LEI_GRAPHICAL)
					|| this.ret.getReportCodes().contains(ReportingConstants.REPORT_CODE_GPI_GRAPHICAL)
					|| this.ret.getReportCodes().contains(ReportingConstants.REPORT_CODE_RAV_B_GRAPHICAL) || csFound) {
				// if GPI, CS, LEI, RAV found, lets enable the trans scales
				// report!
				this.ret.getReportCodes().add(ReportingConstants.REPORT_CODE_ABYD_TRANSITION_SCALES);
			}

			// Now get CHQ existence
			SessionUser session = new SessionUser();
			IProjectParticipantInstrumentHelper ppiHelper = HelperDelegate
					.getProjectParticipantInstrumentHelper("com.pdi.data.nhn.helpers.delegated");
			ProjectParticipantInstrument ppi = ppiHelper.fromLastParticipantIdInstrumentId(session, this.partId,
					IGConstants.I_CODE_CHQ);
			if (ppi != null) {
				this.ret.getReportCodes().add(ReportingConstants.REPORT_CODE_CHQ_DETAIL);
			}

			// OK, so now we have other out-liers (Bizsim content reports)
			if (isInstValid(IGConstants.I_IBLVA)) {
				this.ret.getReportCodes().add(ReportingConstants.REPORT_CODE_IBLVA_CONSULTANT_REVIEW);
			}
			if (isInstValid(IGConstants.I_NIS)) {
				this.ret.getReportCodes().add(ReportingConstants.REPORT_CODE_NIS_CONSULTANT_REVIEW);
			}
			if (isInstValid(IGConstants.I_ISMLL)) {
				this.ret.getReportCodes().add(ReportingConstants.REPORT_CODE_ISMLL_CONSULTANT_REVIEW);
			}
			if (isInstValid(IGConstants.I_ISBUL)) {
				this.ret.getReportCodes().add(ReportingConstants.REPORT_CODE_ISBUL_CONSULTANT_REVIEW);
			}
			if (isInstValid(IGConstants.I_ISZ)) {
				this.ret.getReportCodes().add(ReportingConstants.REPORT_CODE_ISZ_CONSULTANT_REVIEW);
			}
			if (isInstValid(IGConstants.I_EIS)) {
				this.ret.getReportCodes().add(ReportingConstants.REPORT_CODE_EIS_CONSULTANT_REVIEW);
			}
			if (isInstValid(IGConstants.I_BRE2)) {
				this.ret.getReportCodes().add(ReportingConstants.REPORT_CODE_BRE2_CONSULTANT_REVIEW);
			}
			if (isInstValid(IGConstants.I_BRESEA)) {
				this.ret.getReportCodes().add(ReportingConstants.REPORT_CODE_BRESEA_CONSULTANT_REVIEW);
			}
			if (isInstValid(IGConstants.I_SFECEO)) {
				this.ret.getReportCodes().add(ReportingConstants.REPORT_CODE_SFECEO_CONSULTANT_REVIEW);
			}
			if (isInstValid(IGConstants.I_BREMED)) {
				this.ret.getReportCodes().add(ReportingConstants.REPORT_CODE_BREMED_CONSULTANT_REVIEW);
			}

			// Adding the Traits and Behavior reports for ALR
			if (ret.getAlpDataAvailable()) {
				this.ret.getReportCodes().add(ReportingConstants.REPORT_CODE_ALR_TRAITS);
				this.ret.getReportCodes().add(ReportingConstants.REPORT_CODE_ALR_DRIVERS);
			}

			// Adding the Traits, Drivers and Risk reports for KF4D
			// Traits and Drivers spreadsheets to be replaced by Traits,
			// Drivers, and Learning Agility
			// Note that the logic invoked by these reports has neither been
			// deleted nor commented out
			if (ret.getKf4dDataAvailable()) {
				String manifestVersion = null;
				if (projectInfo != null){
					manifestVersion = new NhnWebserviceUtils().getExternalManifestVersion(projectInfo);
				}else {
					manifestVersion = new NhnWebserviceUtils().getExternalManifestVersion("" + projId);
				}
				if (kf4dHelper.courseList.contains("dla")){
					this.ret.getReportCodes().add(ReportingConstants.REPORT_CODE_KF4D_TD);
					this.ret.getReportCodes().add(ReportingConstants.REPORT_CODE_KF4D_LA);
					this.ret.getReportCodes().add(ReportingConstants.REPORT_CODE_KF4D_ENTERPRISE_RISK);
					
				}	else if (kf4dHelper.courseList.contains("dpa")){
					this.ret.getReportCodes().add(ReportingConstants.REPORT_CODE_KF4D_ENTERPRISE_RISK);
					this.ret.getReportCodes().add(ReportingConstants.REPORT_CODE_POTENTIAL);
				} else if (kf4dHelper.courseList.contains("kf4d")){
					if (StringUtils.isEmpty(manifestVersion) || manifestVersion.equalsIgnoreCase("1.7")) {
						
						this.ret.getReportCodes().add(ReportingConstants.REPORT_CODE_KF4D_TDLA);
						this.ret.getReportCodes().add(ReportingConstants.REPORT_CODE_KF4D_RISK);
						
					} else {
						this.ret.getReportCodes().add(ReportingConstants.REPORT_CODE_KF4D_TD);
						this.ret.getReportCodes().add(ReportingConstants.REPORT_CODE_KF4D_LA);
						this.ret.getReportCodes().add(ReportingConstants.REPORT_CODE_KF4D_ENTERPRISE_RISK);
					}
				}
			}

			return;
		} catch (SQLException ex) {
			// handle any errors
			throw new Exception("SQL error in setUpReportList.  " + "SQLException: " + ex.getMessage() + ", "
					+ "SQLState: " + ex.getSQLState() + ", " + "VendorError: " + ex.getErrorCode());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					/* swallow the error */ }
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					/* swallow the error */ }
				stmt = null;
			}
		}
	}

	/**
	 *
	 * @return flag that says the passed intrument abbreviation is valid (and
	 *         completed) for this ppt/proj
	 */
	// Used as source for shameless copy in EGDataHelper
	private boolean isInstValid(String abbv) {
		if (utl == null) {
			utl = new Utils();
		}

		return utl.isInstValid(abbv, this.partId, "" + this.projId);
	}

	/**
	 * Get the competency final scores (and notes) that are present and put them
	 * into the final score ArrayList
	 *
	 * @throws Exception
	 */
	// private void getFinalScores()
	private ArrayList<IGFinalScoreDTO> getFinalScores() throws Exception {
		ArrayList<IGFinalScoreDTO> ary = new ArrayList<IGFinalScoreDTO>();
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT igcr.competencyId, ");
		sqlQuery.append("       igcr.lcScore, ");
		sqlQuery.append("       igcr.lcNote ");
		sqlQuery.append("  FROM pdi_abd_igrid_comp_resp igcr ");
		sqlQuery.append("  WHERE igcr.participantId = '" + this.partId + "' ");
		sqlQuery.append("    AND igcr.dnaId = " + this.dnaId);

		try {
			stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			if (!rs.isBeforeFirst()) {
				// no scores... nothing to do here
				return ary;
			}

			// process the scores into the list
			while (rs.next()) {
				IGFinalScoreDTO elt = new IGFinalScoreDTO();
				elt.setPartId(this.partId);
				elt.setDnaId(this.dnaId);
				elt.setCompId(rs.getLong("competencyId"));
				elt.setCompScoreIdx(rs.getInt("lcScore"));
				elt.setCompNote(rs.getString("lcNote"));
				ary.add(elt);
			}

			return ary;
		} catch (SQLException ex) {
			// handle any errors
			throw new Exception("SQL error in getFinalScores.  " + "SQLException: " + ex.getMessage() + ", "
					+ "SQLState: " + ex.getSQLState() + ", " + "VendorError: " + ex.getErrorCode());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					System.out.println("ERROR: rs close in getFinalScores - " + sqlEx.getMessage());
				}
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					System.out.println("ERROR: stmt close in getFinalScores - " + sqlEx.getMessage());
				}
				stmt = null;
			}
		}
	}

	/**
	 * calcDlciAndFinal - Use data in the IGSummaryDataDTO and calculate the
	 * dLCI and the average of the final scores, but only if needed
	 */
	private void calcDlciAndFinal() {
		boolean doCalc = true; // Assume that all of the final scores are
								// present
		// Get the comps for each superfactor (competency group)
		// First get the superfactor
		for (CompGroupData cgd : this.ret.getGridData().getFullCompArray()) {
			if (doCalc) {
				// Now get the comps for the superfactor
				for (CompElementDTO cmp : cgd.getCgCompList()) {
					if (doCalc) {
						boolean hasFinalScore = false;
						// Now loop the the final score list to see if there is
						// a score for the competency
						for (IGFinalScoreDTO igfs : this.ret.getCompFinData()) {
							if (cmp.getCompId() == igfs.getCompId()) {
								hasFinalScore = true;
								break;
							}
						}
						doCalc = hasFinalScore;
					} else {
						break;
					}
				} // End of competency loop (comps in a superfactor)
			} else {
				// No score found for a comp... done here
				break;
			}
		} // End of compGroup loop (superfactor loop)

		// do the calculations
		if (doCalc) {
			// First calc the average...
			int cnt = 0;
			double sum = 0;
			for (IGFinalScoreDTO fs : this.ret.getCompFinData()) {
				if (fs.getCompScoreIdx() > 0) {
					sum += (fs.getCompScoreIdx() + 1) / 2.0;
					cnt++;
				}
			}
			double avg = sum / cnt;
			this.ret.setFinalAverage(String.format("%.2f", avg));

			// ...then the dLCI
			Norm norm = new Norm();
			norm.setMean(this.ret.getMean());
			norm.setStdDev(this.ret.getStdDev());
			double zScor = norm.calcZScore(avg);
			this.ret.setDLCI("" + Norm.calcIntPctlFromZ(zScor));

			// Also set the filled flag. We wouldn't get into this logic if it
			// wasn't filled
			this.ret.setFilled(true);
		}
	}

	/**
	 * Quick convenience method to check to see if the GPI has been included in
	 * the modules for this dna model
	 *
	 * @throws Exception
	 */
	public boolean checkForGPIinDna(Long dnaId) throws Exception {
		boolean includesGPI = true;

		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT distinct dl.moduleId  ");
		sqlQuery.append("  FROM pdi_abd_dna_link as dl  ");
		sqlQuery.append("  RIGHT JOIN pdi_abd_module as m  ");
		sqlQuery.append("  		ON ( m.moduleId = dl.moduleId  ");
		sqlQuery.append("  			 AND m.internalName like '%GPI%' ) ");
		sqlQuery.append("  WHERE dl.dnaId = " + dnaId);

		try {
			stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			if (rs == null || !rs.isBeforeFirst()) {
				includesGPI = false;

			} else {

				includesGPI = true;
			}

			return includesGPI;
		} catch (SQLException ex) {
			// handle any errors
			throw new Exception("SQL error in checkForGPIinDna.  " + "SQLException: " + ex.getMessage() + ", "
					+ "SQLState: " + ex.getSQLState() + ", " + "VendorError: " + ex.getErrorCode());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					/*  Swallow the error */ }
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					/*  Swallow the error */ }
				stmt = null;
			}

		}
	}

	/**
	 * alrScoreCheck - method that sees if there is a kfp in the project and
	 * forces a score if this is the startup
	 */
	private void alrScoreCheck(boolean isStartup) throws Exception {
		// See if we have an ALP
		boolean hasKfp = HelperUtils.alpCheck(this.dnaId);
		if (hasKfp) {
			// We have a kfp... force score it (ignore any return data)
			if (isStartup) {
				//Connection management is done upstream.  Do not close the connection.
				IGAlrDataHelper helper = new IGAlrDataHelper(con, this.partId, this.dnaId);
				helper.getAlrRgrData(this.partId, this.projId, true);
			}
		}
	}

	public Document getProjectInfo() {
		return projectInfo;
	}

	public void setProjectInfo(Document projectInfo) {
		this.projectInfo = projectInfo;
	}

	public IGKf4dDataHelper getKf4dHelper() {
		return kf4dHelper;
	}

	public void setKf4dHelper(IGKf4dDataHelper kf4dHelper) {
		this.kf4dHelper = kf4dHelper;
	}
}
