/*
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.setup.ut;

//import com.pdi.data.abyd.helpers.setup.SetupSendEmailHelper;
//import com.pdi.ut.UnitTestUtils;
//import com.pdi.data.abyd.util.AbyDDatabaseUtils;

import junit.framework.TestCase;

public class SetupSendEmailDataHelperUT extends TestCase
{
	//
	// Constructors
	//

	public SetupSendEmailDataHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
		throws Exception
    {
		junit.textui.TestRunner.run(SetupSendEmailDataHelperUT.class);
    }

	
	/* THIS (SENDING EMAILS) PROBABLY DOES NOT WORK ANY LONGER */
	/*
	 * testSendEmail
	 */
//	public void testSendEmail()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//
//		String address = "kbeukelm@pdi-corp.com";
//
//		String textFlg = "modelChange";
//		String clientName = "Bogus 1 Corp.";
//		SetupSendEmailHelper helper = new SetupSendEmailHelper(address, textFlg, clientName);        
//		helper.sendEmail();
//		helper = null;
//
//		textFlg = "dnaChange";		//, "spChange", "dnaSubmitted"
//		clientName = "Bogus 2 Corp.";
//		helper = new SetupSendEmailHelper(address, textFlg, clientName);        
//		helper.sendEmail();
//		helper = null;
//
//		textFlg = "spChange";		//, "dnaSubmitted"
//		clientName = "Bogus 3 Corp.";
//		helper = new SetupSendEmailHelper(address, textFlg, clientName);        
//		helper.sendEmail();
//		helper = null;
//
//		textFlg = "dnaSubmitted";
//		clientName = "Bogus 4 Corp.";
//		helper = new SetupSendEmailHelper(address, textFlg, clientName);        
//		helper.sendEmail();
//		helper = null;
//
//		UnitTestUtils.stop(this);
//	}

}
