/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.setup;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.pdi.data.abyd.dto.setup.ReportNoteDTO;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;


/**
 * ReportNoteDataHelper contains all the code needed to set up an A by D report
 *
 * @author		Ken Beukelman
 */
public class ReportNoteDataHelper
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	
	//
	// Constructors.
	//
	
	//
	//  Instance methods.
	//

	/**
	 * Fetch the Setup2 Report Notes from the a DNA table based upon the DNA id.
	 * Assumes that the DNA exists (it should... it's set up in Setup 1).
	 *
	 * @param con A database Connection object
	 * @param dnaId A DNA ID
	 * @return A ReportNoteDTO object 
	 * @throws Exception
	 */
	 public ReportNoteDTO getReportNote(Connection con, long dnaId)
		throws Exception
	{
		Statement stmt = null;
		ResultSet rs = null;
		ReportNoteDTO ret = new ReportNoteDTO();
		ret.setDnaId(dnaId);

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT ISNULL(reportNote,'') as note ");
		sqlQuery.append("  FROM pdi_abd_dna ");
		sqlQuery.append("  WHERE dnaId = " + dnaId);

		try
		{
			stmt = con.createStatement();
        	rs = stmt.executeQuery(sqlQuery.toString());
        	if(!rs.isBeforeFirst()) {
        		return ret;
        	}
        	// Get the text (should be one and only one row)
        	rs.next();

        	ret.setText(rs.getString("note").trim());

			return ret;
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("getReportNotes SQL.  " +
					"SQLException: " + ex.getMessage() + ", " +
					"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (rs != null)
			{
				try
				{
					rs.close();
				}
				catch (SQLException ex)
				{
					System.out.println("Closing rs in getReportNotes.  " +
							"SQLException: " + ex.getMessage() + ", " +
							"SQLState: " + ex.getSQLState() + ", " +
							"VendorError: " + ex.getErrorCode());
				}
				rs = null;
			}
			if (stmt != null)
			{
				try
				{
					stmt.close();
				}
				catch (SQLException ex)
				{
					System.out.println("Closing stmt in getReportNotes.  " +
							"SQLException: " + ex.getMessage() + ", " +
							"SQLState: " + ex.getSQLState() + ", " +
							"VendorError: " + ex.getErrorCode());
				}
				stmt = null;
			}
		}
	}
	
	
	/**
	 * Update the report note field.  Assumes that the row exists for the specified
	 * DNA.  That is true because this is a Setup 2 function; Setup 1 establishes
	 * the DNA and thus ensures that the row is present.  Yes, this is a Setup 2
	 * function, but it modifies a column in a Setup 1 table.
	 *
	 * @param con A database Connection object
	 * @param dto A ReportNoteDTO
	 * @throws Exception
	 */
	 public void saveReportNote(Connection con, ReportNoteDTO dto)
		throws Exception
	{
		DataResult dr = null;

		// update the field
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("UPDATE pdi_abd_dna ");
		sqlQuery.append("  SET reportNote = ? ");
		sqlQuery.append("  WHERE dnaId = ? ");
		

		DataLayerPreparedStatement dlps = AbyDDatabaseUtils.prepareStatement(sqlQuery);

		if (dlps.isInError())
		{
			return;
		}
		dto.setText(dto.getText().replace("'","''"));
		dlps.getPreparedStatement().setString(1, dto.getText());
		dlps.getPreparedStatement().setLong(2, dto.getDnaId());
		try
		{
			dr = AbyDDatabaseUtils.update(dlps);
		}
		finally
		{
	        if (dr != null) { dr.close(); dr = null; }
		}
	}
}

