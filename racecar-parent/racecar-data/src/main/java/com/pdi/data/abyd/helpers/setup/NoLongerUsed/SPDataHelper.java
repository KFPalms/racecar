/**
 * Copyright (c) 2008, 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */

package com.pdi.data.abyd.helpers.setup.NoLongerUsed;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.pdi.data.abyd.dto.common.KeyValuePair;
import com.pdi.data.abyd.dto.common.CompElementDTO;
import com.pdi.data.abyd.dto.setup.NoLongerUsed.SPFullDataDTO;

///**
// * DNAStructureHelper contains all the code needed to extract and return a DNAStructureDTO.  It
// * provides data base access and data reduction for the Assessment-by-Design Project Setup app
// *
// * @author		Ken Beukelman
// * @version	$Revision: 6 $  $Date: 8/25/05 3:23p $
// */
public class SPDataHelper
{
	//
	// Static data.
	//
	/** The source revision. */
	public static final String REVISION = "$Revision: 5 $";

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private Connection connection;
	private long dnaId;
	
		// instantiate a return object
	private SPFullDataDTO ret = new SPFullDataDTO();

	
	//
	// Constructors.
	//
	public SPDataHelper(Connection con, long dnaId)
		throws Exception
	{
		// ensure there is a dna id
		if (dnaId == 0)
		{
			throw new Exception("A valid DNA id is required to fetch SP data.");
		}

		this.connection = con;
		this.dnaId = dnaId;
	}

	//
	// Instance methods.
	//

	/**
	 * Fetch all of the data needed for SP manipulation and setup.
	 *
	 * @return the SPFullDataDTO object
	 * @throws Exception
	 */
	public SPFullDataDTO getAllSPData()
		throws Exception
	{
		this.ret = new SPFullDataDTO();
		
		// Populate the module name list
		getCompList();
		
		// Get the elements (essential/not essential flags)
		getCompElts();
		
		// Get the elements (essential/not essential flags)
		getTextElts();
		
		return this.ret;
	}       

	
	/**
	 * Get the Competencies for the DNA selected.  All valid competencies
	 * that are used are retrieved;  competencies not checked in any module
	 * (isActive = 0) are not retrieved.
	 *
	 * @throws Exception
	 */
	private void getCompList()
		throws Exception
	{
		ArrayList<KeyValuePair> modList = new ArrayList<KeyValuePair>();
		
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT DISTINCT dl.groupSeq, ");
		sqlQuery.append("                dl.competencyId, ");
		sqlQuery.append("                dl.compSeq, ");
		sqlQuery.append("                ct.text as modName ");
		sqlQuery.append("  FROM pdi_abd_dna_link dl ");
		sqlQuery.append("    INNER JOIN pdi_abd_competency cmp ON cmp.competencyId = dl.competencyId ");
		sqlQuery.append("    LEFT JOIN pdi_abd_text ct ON ct.textId = cmp.textId ");
		sqlQuery.append("  WHERE dl.dnaId = " + this.dnaId + " ");
		sqlQuery.append("    AND dl.isUsed = 1 ");
		sqlQuery.append("    AND dl.active = 1 ");
		sqlQuery.append("  ORDER BY groupseq, compSeq");

		try
		{
		    stmt = this.connection.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			if (! rs.isBeforeFirst())
			{
			    throw new Exception("No modules exist for DNA ID " + this.dnaId);
			}

			// process into the mod list
			while (rs.next())
			{
				KeyValuePair entry = new KeyValuePair();
					entry.setTheKey(rs.getLong("competencyId"));
					entry.setTheValue(rs.getString("modName"));
					modList.add(entry);	  	       	        
			}
			
			this.ret.setCompList(modList);
			return;
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL getModList (SP).  " +
							"SQLException: " + ex.getMessage() + ", " +
							"SQLState: " + ex.getSQLState() + ", " +
							"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (rs != null)
			{
			    try  {  rs.close();  }
			    catch (SQLException sqlEx)  {  /* Swallow the error */  }
			    rs = null;
			}
			if (stmt != null)
			{
			    try  {  stmt.close();  }
			    catch (SQLException sqlEx)  {  /* Swallow the error */  }
			    stmt = null;
			}
		}		    
	}

	
	/**
	 * Get the flags for the competencies.  If none exist yet,
	 * make some based upon the comp list
	 *
	 * @throws Exception
	 */
	private void getCompElts()
		throws Exception
	{
		ArrayList<CompElementDTO> compElts = new ArrayList<CompElementDTO>();
		
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT elts.competencyId, ");
		sqlQuery.append("       elts.spValue ");
		sqlQuery.append("  FROM pdi_abd_dna_sp_comp_elements elts ");
		sqlQuery.append("  WHERE elts.dnaId = " + this.dnaId + " ");
		sqlQuery.append("    AND elts.active = 1");
		
		try
		{
		    stmt = this.connection.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			if (rs.isBeforeFirst())
			{
				// process into the comp elt list
				while (rs.next())
				{
					CompElementDTO entry = new CompElementDTO();
					entry.setCompId(rs.getLong("competencyId"));
					entry.setIsEssential(rs.getBoolean("spValue"));
					compElts.add(entry);	  	       	        
				}
			}  else {
				// Make a default list of all FALSE based on the comp list
				for(int i=0; i < this.ret.getCompList().size(); i++)
				{
					CompElementDTO entry = new CompElementDTO();
					entry.setCompId(((KeyValuePair) this.ret.getCompList().get(i)).getTheKey());
					entry.setIsEssential(false);
					compElts.add(entry);	  	       	        
				}
			}
			
			this.ret.setCompElts(compElts);
			return;
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL getCompElts (SP).  " +
							"SQLException: " + ex.getMessage() + ", " +
							"SQLState: " + ex.getSQLState() + ", " +
							"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (rs != null)
			{
			    try  {  rs.close();  }
			    catch (SQLException sqlEx)  {  /* Swallow the error */  }
			    rs = null;
			}
			if (stmt != null)
			{
			    try  {  stmt.close();  }
			    catch (SQLException sqlEx)  {  /* Swallow the error */  }
			    stmt = null;
			}
		}		    
	}

	
	/**
	 * Get the Success Profile text elements.  This could put an empty Map
	 * into the output object.
	 *
	 * @throws Exception
	 */
	private void getTextElts()
		throws Exception
	{
		// Presently, there is no "secret decoder ring" embodied in code that correlates the spTextType
		// to the content associated with that type.  here is the definition as of 4/2/09:
		//	Type	Text Type
		//	 1		Business Challenges
		//	 2		Leadership Challenges
		//	 3		Experience Requirements
		//	 4		Motivators / Career Drivers
		//	 5		Leadership Style and Culture
		//	 6		Deraillers
		//	 7		Job Requirements
		// This data is duplicated in SaveSPTextHelper.  Changes here should also be applied there.
		// Secret decoder ring is now in SetupConstants

		ArrayList<KeyValuePair> textElts = new ArrayList<KeyValuePair>();
		
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT txt.spTextType, ");
		sqlQuery.append("       txt.spText ");
		sqlQuery.append("  FROM pdi_abd_dna_sp_comp_text txt ");
		sqlQuery.append("  WHERE txt.dnaId = " + this.dnaId + " ");
		sqlQuery.append("    AND txt.active = 1");
		try
		{
		    stmt = this.connection.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			// process into the comp text list
			while (rs.next())
			{
				KeyValuePair txtElt = new KeyValuePair();
				txtElt.setTheKey(rs.getInt("spTextType"));
				txtElt.setTheValue(rs.getString("spText"));
				textElts.add(txtElt);
			}
			
			this.ret.setTextElts(textElts);
			return;
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL getTextElts (SP).  " +
							"SQLException: " + ex.getMessage() + ", " +
							"SQLState: " + ex.getSQLState() + ", " +
							"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (rs != null)
			{
			    try  {  rs.close();  }
			    catch (SQLException sqlEx)  {  /* Swallow the error */  }
			    rs = null;
			}
			if (stmt != null)
			{
			    try  {  stmt.close();  }
			    catch (SQLException sqlEx)  {  /* Swallow the error */  }
			    stmt = null;
			}
		}		    
	}
}
