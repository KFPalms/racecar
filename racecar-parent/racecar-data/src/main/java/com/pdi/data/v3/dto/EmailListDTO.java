/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.dto;

import java.util.ArrayList;

import com.pdi.data.util.BaseDTO;
import com.pdi.data.util.RequestStatus;

/**
 * EmailListDTO is a class designed to convey the information for a 
 * list of emails used in the PDI V3 extensions.  It contains an ArrayList
 * of EmailDTO objects; the status in each of those may be redundant.
 * 
 * @author		Ken Beukelman
 * @author		MB Panichi
  */

public class EmailListDTO  extends BaseDTO
{
	//
	// Static data.
	//


	//
	// Static methods.
	//


	//
	// Instance data.
	//
	private ArrayList<EmailDTO> _emailList = new ArrayList<EmailDTO>();


	//
	// Constructors.
	//
	public EmailListDTO()
	{
		super();
		// does nothing right now
	}
	
	// Status setup constructor - email list data remains unset
	public EmailListDTO(RequestStatus stat)
	{
		this.setStatus(stat);
	}
	
	//
	// Instance methods.
	//

	/**
	 * Generic toString method
	 */
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("EmaiListDTO:  ");
		sb.append("Status code=" + this.getStatus().getStatusCode() + ", count=" + _emailList.size());
		for(int i=0; i < _emailList.size(); i++)
		{
			sb.append("\n      id=" + _emailList.get(i).getEmailId());
			sb.append(", name=>" + _emailList.get(i).getDisplayName() + "<");
		}
		return sb.toString();
	}
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public ArrayList<EmailDTO> getEmailList()
	{
		return _emailList;
	}
	
	public void setEmailList(ArrayList<EmailDTO> value)
	{
		_emailList = value;
	}
}
