/*
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.setup.ut;

import com.pdi.ut.UnitTestUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;

import junit.framework.TestCase;

public class ReportNotesDataHelperUT extends TestCase
{
	//
	// Constructors
	//

	public ReportNotesDataHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
		throws Exception
    {
		junit.textui.TestRunner.run(EntryStateHelperUT.class);
    }


	/*
	 * testGetEntryStateData
	 */
	public void testGetSetup2InitialData()
		throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);

//		Connection con = AbyDDatabaseUtils.getDBConnection();
//		String jobId = "LKLVXNGM";
//		
//		System.out.println("EntryStateHelperUT:");
//		System.out.println("    Input job ID=" + jobId);
//		
//		EntryStateHelper helper = new EntryStateHelper(con, jobId);
//		EntryStateDTO dto = helper.getEntryStateData();
//		
//// TODO  What can I do to validate this?
//		System.out.println("    Output:");
//		System.out.println("         JobName=" + dto.getJobName());
//		System.out.println("      ClientName=" + dto.getClientName());
//		System.out.println("        DNA Desc=" + dto.getState());
//		System.out.println("           State=" + dto.getState());
//		System.out.println("        Models:=");
//		for (KeyValuePair kvp : dto.getModelArray())
//		{
//			System.out.println("          " + kvp.getTheKey() + " - " + kvp.getTheValue());
//		}

		UnitTestUtils.stop(this);
	}
}
