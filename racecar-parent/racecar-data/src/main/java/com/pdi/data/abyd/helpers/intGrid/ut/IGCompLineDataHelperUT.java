package com.pdi.data.abyd.helpers.intGrid.ut;

import java.sql.Connection;

import com.pdi.data.abyd.dto.intGrid.IGCompDataDTO;
import com.pdi.data.abyd.helpers.intGrid.IGCompLineDataHelper;
import com.pdi.data.abyd.helpers.intGrid.IGKf4dDataHelper;
import com.pdi.ut.UnitTestUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;

import junit.framework.TestCase;

public class IGCompLineDataHelperUT  extends TestCase
{
	//
	// Constructors
	//
	public IGCompLineDataHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
	throws Exception
	{
		junit.textui.TestRunner.run(IGCompLineDataHelperUT.class);
	}


	/*
	 * testGetCompLine
	 */
	public void testGetCompLine()
		throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);
		
		Connection con = AbyDDatabaseUtils.getDBConnection();
		//String partId = "60254";
		//long dnaId = 6;
		//long compId = 119;
		String partId = "367671";
		long dnaId = 28;
		long compId = 119;

		// get the data
		IGKf4dDataHelper kf4dHelper = new IGKf4dDataHelper(con, partId, dnaId);
		IGCompLineDataHelper helper = new IGCompLineDataHelper(con, compId, partId, dnaId, kf4dHelper);
		IGCompDataDTO ret = helper.getCompLine();
		System.out.print(ret.toString());

		UnitTestUtils.stop(this);
	}
}
