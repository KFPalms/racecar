/**
 * Copyright (c) 2008 Personnel Decisions International, Inc.
 * All rights reserved.
 *
 * $Header:
 */

package com.pdi.data.abyd.util.email;

//NOTE:  Eventually the email functionality in A By D may be migrated
//to the standard Oxcart functionality.  At that time, this module and
//its brethren in this folder can be dumped.

/**
 * Prepares email requests to be handed off to an email server.
 *
 * @author	Mike Solomonson
 * @version	$Revision: 1 $  $Date: 12/04/08 12:23p $
 */
public class EmailRequestService
{
	//
	// Static data.
	//
    
	//
	// Static methods.
	//

	//
	// Instance data.
	//    
    protected String emailServerName = "";
    
    
	//
	// Constructors.
	//
	public EmailRequestService(String emailServer)
		throws Exception
	{
		if (emailServer == null || emailServer.length() == 0)
		{
			throw new Exception("Missing or zero-length server name.");
		}
		
	    this.emailServerName = emailServer;
	}    
    
    public void sendEmail(EmailMessage email)
    	throws Exception
    {
        //System.out.println("\nEmailRequestService.sendEmail() " + (new Date().toString()) +
        //        ", this.emailServerName = " + this.emailServerName);
        
        EmailRequestHelper helper = new EmailRequestHelper();
        
        // This is where we could check the incoming EmailMessage object to see if it is complete.
        // If not complete and/or it contains a value for _emailTemplateId then
        // we could check the database for an email template. 
        //
        // As of Dec. 8, 2008 the only known request through here is to notify the PDI client mgr from DesignDNA
        // so that email data will all be sent by the requester.
        
        helper.processEmailRequest(this.emailServerName, email);
    }
}
