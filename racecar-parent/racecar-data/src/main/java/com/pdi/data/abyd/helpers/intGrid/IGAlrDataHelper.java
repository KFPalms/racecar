package com.pdi.data.abyd.helpers.intGrid;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.pdi.data.abyd.dto.common.IGInstCompData;
import com.pdi.data.abyd.dto.common.PrnData;
import com.pdi.data.abyd.dto.common.SpssInfo;
import com.pdi.data.abyd.dto.intGrid.IGScaleScoreData;
import com.pdi.data.abyd.dto.intGrid.LineScoreData;
import com.pdi.data.abyd.helpers.common.HelperUtils;
import com.pdi.data.abyd.helpers.common.Rv2Helper;
import com.pdi.data.abyd.helpers.intGrid.IGScoring.ScoreAndNormData;
import com.pdi.data.abyd.util.Utils;
import com.pdi.data.nhn.util.NhnDatabaseUtils;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.util.RequestStatus;
import com.pdi.data.util.language.DefaultLanguageHelper;
import com.pdi.properties.PropertyLoader;
import com.pdi.webservice.NhnWebserviceUtils;

/**
 * This class handles stuff that is associated with the KFALR project type
 */
public class IGAlrDataHelper {
	
	private static final Logger log = LoggerFactory.getLogger(IGAlrDataHelper.class);

	private static final String SCORE_PARM_STR = "/KfalpPptScoresServlet?pptId={pptId}&projId={projId}&targIdx={tgtLvl}&nVersion={normVer}";
	private static final String WG_SCALE = "WGE_CT";
	private static final String RAV_SCALE = "RV2"; // All caps is deliberate
	private static final String FLL_MLL_HARDCODE_VERSION = "2.02";

	// Instance variables
	private Connection con;
	private String partId;
	private long dnaId;

	// derived values
	private long dfltLangId;
	private long projId;

	private AlrIgData holder = null;

	private Map<Long, List<SpssInfo>> compSpssInfo = null;

	//
	// Constructors
	//
	/* 
	@SuppressWarnings("unused")
	private IGAlrDataHelper() {
		// Does nothing... invalid for use (needs the 2 or 3 param constructor)
	}
	*/

	/**
	 * Constructor.
	 * 
	 * @param eCon
	 * @param partId
	 * @param dnaId
	 * @throws Exception
	 */
	public IGAlrDataHelper(Connection eCon, String partId, long dnaId) throws Exception {
		if (eCon == null) {
			throw new Exception("Invalid connection parameter for getAlrScoring");
		}
		if (partId == null || partId.length() < 1) {
			throw new Exception("Invalid participant ID parameter for getAlrScoring");
		}
		if (dnaId == 0) {
			throw new Exception("Invalid dna ID parameter for getAlrScoring");
		}

		this.con = eCon;
		this.partId = partId;
		this.dnaId = dnaId;

		this.dfltLangId = DefaultLanguageHelper.fetchDefaultLanguageId();
		this.projId = HelperUtils.getProjIdFromDna(this.dnaId);
	}

	// Methods that expose ALR data

	/**
	 * hasAlpInDna - Return a boolean indicating if there are ALP items in
	 * selected DNA. NOTE: This method does not use the underlying private data
	 * class.
	 * 
	 * @return
	 * @throws Exception
	 */
	// TODO is there a way to check both ALP and K4D at the same time?
	public boolean hasAlpInDna() throws Exception {
		boolean ret = false; // default value says that there are no ALP items

		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT COUNT(*) AS alpCnt ");
		sqlQuery.append("  FROM pdi_abd_dna_link dl ");
		sqlQuery.append("  WHERE dl.dnaId = " + this.dnaId + " ");
		sqlQuery.append("    AND dl.moduleId IN (SELECT mm.moduleId ");
		sqlQuery.append("                          FROM pdi_abd_module mm ");
		sqlQuery.append("                          WHERE mm.internalName = 'ALP')");
		// System.out.println("noCogsInDna SQL " + sqlQuery.toString());

		try {
			stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			// "SELECT COUNT()..." here always returns exactly 1 row... get it
			rs.next();

			int cnt = rs.getInt("alpCnt");

			ret = cnt == 0 ? false : true;

			return ret;
		} catch (SQLException ex) {
			// handle any errors
			throw new Exception("SQL error for hasAlpInDna.  " + "SQLException: " + ex.getMessage() + ", "
					+ "SQLState: " + ex.getSQLState() + ", " + "VendorError: " + ex.getErrorCode());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					System.out.println("ERROR: rs close in noCogsInDna - " + sqlEx.getMessage());
				}
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					System.out.println("ERROR: stmt close in noCogsInDna - " + sqlEx.getMessage());
				}
				stmt = null;
			}
		}
	}

	/**
	 * getAlrCompScore - Gets the scale scores for the testing column for the
	 * detail data when "Edit" is pressed. Sticks the ALR scores for a single
	 * competency into the GPI collection.
	 * 
	 * @param compId
	 * @return
	 * @throws Exception
	 */
	public IGInstCompData getAlrCompScore(long compId) throws Exception {
		// Create the comp array and the single IGInstCompData object in it
		IGInstCompData icData = new IGInstCompData();

		// set the comp id only... All the data should be available already
		// including the id
		icData.setCompId(compId);

		// Get the scale scores from the ALP/ALR scoring.
		LineScoreData lsd = getAlrDetailScores(compId);
		icData.setCogColumnScores(lsd.getCogColumnScores());
		icData.setGpiColumnScores(lsd.getGpiColumnScores());

		return icData;
	}

	/**
	 * getAlrTestCompScores - Simple converter to take the generated ALR comp
	 * scores from the internal data object and convert them to the string
	 * objects required by the IG display
	 * 
	 * @return
	 * @throws Exception
	 */
	public Map<Long, String> getAlrTestCompScores() throws Exception {
		HashMap<Long, String> ret = new HashMap<Long, String>();

		AlrIgData csd = getAlrIgScoreData();

		for (Iterator<Map.Entry<Long, Float>> itr = csd.getCompScores().entrySet().iterator(); itr.hasNext();) {
			Map.Entry<Long, Float> ent = itr.next();

			// round the comp scores to 2 decimals
			// BigDecimal bd = new BigDecimal(ent.getValue());
			// bd = bd.setScale(2, RoundingMode.HALF_UP);
			// //ret.put(ent.getKey(), "" + bd.floatValue());
			// ret.put(ent.getKey(), String.format("%.2f", bd.floatValue())); //
			// Need 2 decimals in result string

			double score = Math.floor((ent.getValue() * 100.0) + 0.505) / 100.0;
			ret.put(ent.getKey(), String.format("%.2f", score)); // Need 2
																	// decimals
																	// in result
																	// string
		}

		return ret;
	}

	/**
	 * getAlrCompData - gets the data about constituent norm scores for the
	 * testing column
	 * 
	 * @return
	 * @throws Exception
	 */
	public ArrayList<IGInstCompData> getAlrCompData() throws Exception {
		ArrayList<IGInstCompData> ret = new ArrayList<IGInstCompData>();

		// Get the competency info
		Map<Long, IGInstCompData> holder = new LinkedHashMap<Long, IGInstCompData>();
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT DISTINCT dl.competencyId, ");
		sqlQuery.append("                txt1.text as compName, ");
		sqlQuery.append("                txt2.text as compDesc, ");
		sqlQuery.append("                cmp.internalName as intName, ");
		sqlQuery.append("                dl.groupSeq, ");
		sqlQuery.append("                dl.compSeq ");
		sqlQuery.append("  FROM pdi_abd_dna_link dl ");
		sqlQuery.append("  INNER JOIN pdi_abd_module sims ON (sims.moduleId = dl.moduleId ");
		sqlQuery.append("                                 AND sims.internalName in ('ALP')) ");
		sqlQuery.append("  LEFT JOIN pdi_abd_competency cmp ON cmp.competencyId = dl.competencyId ");
		sqlQuery.append("  LEFT JOIN pdi_abd_text txt1 ON (txt1.textId = cmp.textId AND txt1.languageId = "
				+ this.dfltLangId + ") ");
		sqlQuery.append("  INNER JOIN pdi_abd_dna dna on dna.dnaId = dl.dnaId ");
		sqlQuery.append("  LEFT JOIN pdi_abd_mod_comp_desc mcd on (mcd.modelId = dna.modelId ");
		sqlQuery.append("                                      AND mcd.competencyId = dl.competencyId) ");
		sqlQuery.append("  LEFT JOIN pdi_abd_text txt2 ON (txt2.textId = mcd.textId AND txt2.languageId = "
				+ this.dfltLangId + ") ");
		sqlQuery.append("  WHERE dl.dnaId = " + this.dnaId + " ");
		sqlQuery.append("  and dl.isUsed = 1 ");
		sqlQuery.append("  ORDER BY dl.groupSeq, dl.compSeq");

		try {
			stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			// Get the competencies - generate IGInstCompData objects
			// (no score data) and a map of compId/intName
			Map<String, Long> nameMap = new HashMap<String, Long>();
			while (rs.next()) {
				IGInstCompData elt = new IGInstCompData();
				elt.setCompId(rs.getLong("competencyId"));
				elt.setCompName(rs.getString("compName"));
				elt.setCompDesc(rs.getString("compDesc"));
				Long cmpId = new Long(rs.getLong("competencyId"));

				holder.put(cmpId, elt);
				nameMap.put(rs.getString("intName"), cmpId);
			}

			Map<Long, LineScoreData> sl = getScoreLists();

			// Stick the score lists into the IGInstCompData objects
			// and put the result in the output array
			for (Iterator<Map.Entry<Long, IGInstCompData>> itr = holder.entrySet().iterator(); itr.hasNext();) {
				Map.Entry<Long, IGInstCompData> ent = itr.next();
				LineScoreData scoreLists = sl.get(ent.getKey());
				IGInstCompData compData = ent.getValue();
				compData.setCogColumnScores(scoreLists.getCogColumnScores());
				compData.setGpiColumnScores(scoreLists.getGpiColumnScores());

				ret.add(compData);
			}

			return ret;
		} catch (SQLException ex) {
			// handle any errors
			throw new Exception("SQL in getAlrCompData.  " + "SQLException: " + ex.getMessage() + ", " + "SQLState: "
					+ ex.getSQLState() + ", " + "VendorError: " + ex.getErrorCode());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					System.out.println("ERROR: rs close in IG getCompData - " + sqlEx.getMessage());
				}
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					System.out.println("ERROR: stmt close in IG getCompData - " + sqlEx.getMessage());
				}
				stmt = null;
			}
		}
	}

	/**
	 * getAlrCogsDone - public method to expose the cogsDone flag
	 *
	 * @return
	 * @throws Exception
	 */
	public boolean getAlrCogsDone() throws Exception {
		AlrIgData csd = getAlrIgScoreData();

		return csd.getCogsDone();
	}

	/**
	 * getExperienceRecommendation - publicly expose the Leadership Experience
	 * recommendation
	 * 
	 * @return
	 * @throws Exception
	 */
	public int getExperienceRecommendation() throws Exception {
		AlrIgData csd = getAlrIgScoreData();

		return csd.getExperienceRec();
	}

	/**
	 * getStyleRecommendation - publicly expose the Leadership Style
	 * recommendation
	 * 
	 * @return
	 * @throws Exception
	 */
	public int getStyleRecommendation() throws Exception {
		AlrIgData csd = getAlrIgScoreData();

		return csd.getStyleRec();
	}

	/**
	 * getInterestRecommendation - publicly expose the Leadership Interest
	 * recommendation
	 * 
	 * @return
	 * @throws Exception
	 */
	public int getInterestRecommendation() throws Exception {
		AlrIgData csd = getAlrIgScoreData();

		return csd.getInterestRec();
	}

	/**
	 * getDerailmentRecommendation - publicly expose the Derailment Risk
	 * recommendation
	 * 
	 * @return
	 * @throws Exception
	 */
	public int getDerailmentRecommendation() throws Exception {
		AlrIgData csd = getAlrIgScoreData();

		return csd.getDerailRec();
	}

	/**
	 * getAdvancementRecommendation - publicly expose the Long-term Advancement
	 * Potential recommendation
	 * 
	 * @return
	 * @throws Exception
	 */
	public int getAdvancementRecommendation() throws Exception {
		AlrIgData csd = getAlrIgScoreData();

		return csd.getAdvPotRec();
	}

	/**
	 * getTraits - used by the reporting code... Gets the trait (scale) data for
	 * reporting
	 * 
	 * @return
	 * @throws Exception
	 */
	public Map<String, PrnData> getTraits() throws Exception {
		Map<String, PrnData> ret = new HashMap<String, PrnData>();
		AlrIgData csd = getAlrIgScoreData();

		ret.putAll(csd.getScaleData());

		return ret;
	}

	/**
	 * getDrivers - used by the reporting code... Gets the driver data for
	 * reporting
	 * 
	 * @return
	 * @throws Exception
	 */
	public Map<String, PrnData> getDrivers() throws Exception {
		Map<String, PrnData> ret = new HashMap<String, PrnData>();
		AlrIgData csd = getAlrIgScoreData();

		ret.putAll(csd.getDrivers());

		return ret;
	}

	// Methods that support the above public methods

	/**
	 * getAlrInstScores - Gets the ALR scale scores (ALP scores normed for ALR)
	 * for a single competency. Called from getAlrCompScore().
	 * 
	 * @param compId
	 * @return
	 * @throws Exception
	 */
	private LineScoreData getAlrDetailScores(long compId) throws Exception {
		LineScoreData ret = new LineScoreData();

		// Get the scale scores
		Map<String, String> scores = getAlrScaleScores();

		// get the key list for this comp
		List<SpssInfo> comps = getSpssForComp(compId);

		// loop through the list and get the one for this comp (cogs and ALR)
		ArrayList<IGScaleScoreData> cogs = new ArrayList<IGScaleScoreData>();
		ArrayList<IGScaleScoreData> alp = new ArrayList<IGScaleScoreData>();
		for (SpssInfo ci : comps) {
			String key = ci.getSpssId();
			boolean inverted = ci.isInverted();
			String val = IGConstants.SCALE_NAMES.get(key);
			if (val == null) {
				val = "N/A - " + key;
			}
			// Enclose the name in parenthesis if inverted
			if (inverted) {
				val = "(" + val + ")";
			}

			IGScaleScoreData ssd = new IGScaleScoreData();

			// truncate it and make it a string
			String rat = scores.get(key);
			if (rat == null) {
				rat = "N/A - " + key;
			}

			ssd.setRating(rat); // PDINH rating (empty)
			ssd.setScaleName(val); // Name of the scale
			ssd.setIsNegative(inverted);
			ssd.setSpssVal(key); // spss key

			if (key.substring(0, 3).equals("ALR")) {
				alp.add(ssd);
			} else {
				cogs.add(ssd);
			}
		}

		// Scram
		ret.setGpiColumnScores(alp);
		ret.setCogColumnScores(cogs);
		return ret;
	}

	/**
	 * getAlrScaleScores - Simple converter object to take the ALR scale ratings
	 * (ALP scales normed for ALR and converted to ratings) from the internal
	 * data object and convert them to the string objects required by the IG (a
	 * String object)
	 *
	 * @return - Map. key = Scale key, value = value (as a string)
	 */
	private Map<String, String> getAlrScaleScores() throws Exception {
		HashMap<String, String> ret = new HashMap<String, String>();

		AlrIgData csd = getAlrIgScoreData();

		// Traits
		for (Iterator<Map.Entry<String, PrnData>> itr = csd.getScaleData().entrySet().iterator(); itr.hasNext();) {
			Map.Entry<String, PrnData> ent = itr.next();

			// round the scale ratings to 1 decimal
			BigDecimal bd = new BigDecimal(ent.getValue().getRate());
			bd = bd.setScale(1, RoundingMode.HALF_UP);
			ret.put(ent.getKey(), "" + bd.floatValue());
		}

		// Drivers
		// Adding "ALR_" Prefix to drivers for processing reasons
		for (Iterator<Map.Entry<String, PrnData>> itr = csd.getDrivers().entrySet().iterator(); itr.hasNext();) {
			Map.Entry<String, PrnData> ent = itr.next();

			// round the scale ratings to 1 decimal
			BigDecimal bd = new BigDecimal(ent.getValue().getRate());
			bd = bd.setScale(1, RoundingMode.HALF_UP);
			ret.put("ALR_" + ent.getKey(), "" + bd.floatValue());
		}

		return ret;
	}

	/**
	 * getSpssForComp - Fetch the list of spss keys that are valid for the
	 * specified competency
	 *
	 * @param compId
	 * @return
	 * @throws Exception
	 */
	private List<SpssInfo> getSpssForComp(long compId) throws Exception {
		if (compSpssInfo == null) {
			this.compSpssInfo = (new Utils()).getCompSpssInfo(this.dnaId);
		}

		List<SpssInfo> ret = this.compSpssInfo.get(compId);
		if (ret == null) {
			// Make an empty one
			ret = new ArrayList<SpssInfo>();
		}

		return ret;
	}

	/**
	 * getSpssForComps - Fetch a map (key = comp ID) that contains lists of spss
	 * keys that are valid for the key competency.
	 *
	 * @return
	 * @throws Exception
	 */
	private Map<Long, List<SpssInfo>> getSpssForComps() throws Exception {
		if (compSpssInfo == null) {
			compSpssInfo = (new Utils()).getCompSpssInfo(this.dnaId);
		}
		return compSpssInfo;
	}

	/**
	 * getScoreLists - gets a map (key = competency Id) of score containers
	 * (scores separated by "cog" vs. "alp" with alp carried in the "gpi"
	 * container). Used to get the normed scale scores for display in the "Edit"
	 * and "View" data in the IG
	 * 
	 * @return
	 * @throws Exception
	 */
	private Map<Long, LineScoreData> getScoreLists() throws Exception {
		Map<Long, LineScoreData> ret = new HashMap<Long, LineScoreData>();

		// Get the scale scores
		Map<String, String> scaleScoreMap = getAlrScaleScores();

		// Get the comp/scale cross reference
		Map<Long, List<SpssInfo>> spssLists = getSpssForComps();

		// For each item in the SPSS list...
		for (Iterator<Map.Entry<Long, List<SpssInfo>>> itr = spssLists.entrySet().iterator(); itr.hasNext();) {
			Map.Entry<Long, List<SpssInfo>> ent = itr.next();
			long compId = ent.getKey();
			if (!ret.containsKey(compId)) {
				// NO - create an new entry in the ret map (compID, new
				// arraylist
				ret.put(compId, new LineScoreData());
			}

			// For each item in the list, get the score associated with it
			List<SpssInfo> val = ent.getValue();
			for (SpssInfo ci : val) {
				// get the score
				String scaleId = ci.getSpssId();
				String score = scaleScoreMap.get(scaleId);
				if (score == null) {
					continue;
				}

				// build the comp score info container data object
				IGScaleScoreData ssd = new IGScaleScoreData();
				ssd.setSpssVal(scaleId);
				ssd.setRating(score);
				ssd.setIsNegative(ci.isInverted());
				ssd.setScaleName(IGConstants.SCALE_NAMES.get(scaleId));

				// Stick the data object into the output object
				if (scaleId.substring(0, 3).equals("ALR") || scaleId.substring(0, 3).equals("RAV")) {
					ret.get(compId).getGpiColumnScores().add(ssd);
				} else {
					ret.get(compId).getCogColumnScores().add(ssd);
				}
			}
		}

		return ret;

	}

	/**
	 * getAlrIgScoreData - Gets a private object filled with data needed for the
	 * ALR IG. Gets data from the database or the pipeline as needed, then puts
	 * it into the private class
	 * 
	 * @return
	 * @throws Exception
	 */
	private AlrIgData getAlrIgScoreData() throws Exception {
		if (this.holder != null) {
			// Been here before... return the generated holder
			return this.holder;
		}

		// Never been here before... build the new container object
		AlrIgData ret = new AlrIgData();

		// get the testing mods
		// ArrayList<String> courseList = new NhnWebserviceUtils()
		// .fetchCourseListForProject(PropertyLoader.getProperty("com.pdi.listener.portal.application",
		// "courseList.url"), ""+this.projId);
		ArrayList<String> courseList = new NhnWebserviceUtils().getUnfilteredProjectCourseList(this.projId);

		// WG is the only cog associated with ALR
		if (courseList == null) {
			log.debug("getAlrIgScoreData:  Empty courselist");
		}
		if (courseList.contains(IGConstants.I_CODE_WGE)) {
			// Get the WG scale score code
			ScoreAndNormData sn = IGScoring.getScoreAndNormData(this.con, this.partId, this.dnaId, false, true, false);

			ret.setCogsDone(sn.getCogsDone());
			if (sn.getCogsDone()) {
				String wgRating = sn.getScoreMap().get(WG_SCALE).getRating();
				if (wgRating != null) {
					PrnData pd = new PrnData();
					pd.setRate(Float.parseFloat(wgRating));
					ret.getScaleData().put(WG_SCALE, pd);
				}
			}
		} else {
		
			// No cogs means they are "all done"
			ret.setCogsDone(true);
		}
		

		// Get the rest of the scores
		String xml = getAlrRgrData(this.partId, this.projId);
		
		boolean hasScores = true;
		if (xml == null || xml.length() < 1) {
			hasScores = false;
		}
		if (hasScores) {
			// Pull the scores out of the document
			DocumentBuilderFactory dbfac = null;
			DocumentBuilder docBuilder = null;
			Document doc = null;
			try {
				dbfac = DocumentBuilderFactory.newInstance();
				docBuilder = dbfac.newDocumentBuilder();
				doc = docBuilder.parse(new InputSource(new StringReader(xml)));
			} catch (ParserConfigurationException pce) {
				String err = "IGAlrDataHelper.getAlrIgScoreData() - XML parser config ERROR: ppt=" + this.partId
						+ ", proj=" + this.projId + ", xml=" + xml + ", msg=" + pce.getMessage();
				System.out.println(err);
				throw new Exception(err);
			} catch (SAXException se) {
				String err = "IGAlrDataHelper.getAlrIgScoreData() - XML SAX ERROR: ppt=" + this.partId + ", proj="
						+ this.projId + ", xml=" + xml + ", msg=" + se.getMessage();
				System.out.println(err);
				throw new Exception(err);
			}

			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();

			// Get the scale scores from kfar data
			NodeList nodeList = (NodeList) xpath.compile("/kfarScoreData/scales/scale").evaluate(doc,
					XPathConstants.NODESET);
			// int size = nodeList.getLength();
			if (nodeList == null) {
				System.out.println("getAlrIgScoreData(): empty scale list");
			}
			for (int index = 0; index < nodeList.getLength(); index++) {

				Element e = (Element) nodeList.item(index);
				String name = e.getAttribute("id").toUpperCase();
				NodeList nl = e.getChildNodes();
				PrnData prn = new PrnData();
				for (int i2 = 0; i2 < nl.getLength(); i2++) {
					
					Element e2 = (Element) nl.item(i2);
					String n = e2.getNodeName();
					String val = e2.getTextContent();
					
					
					if (name.equals(RAV_SCALE) && !HelperUtils.courseComplete(this.partId, IGConstants.I_CODE_RAVB2)){
						log.debug("Found RV2 in Scale scores but RV2 has not yet been completed.  Check for RV1");
						if (!courseList.contains(IGConstants.I_CODE_RAVB) && HelperUtils.courseComplete(this.partId, IGConstants.I_CODE_RAVB)){
							
							if (val == null || val.isEmpty() || val.equals("NaN")) {
								//RV1 score manually entered in "Test Data"
								log.debug("User has completed RV1.  Getting the score");
								prn = Rv2Helper.getRvRating(this.partId, con);
								log.debug("Got RV1 rating: {} ", prn.getRate());
								break;
							} else {
								//in this case RV1 scoring was already done based on scorm data over in report world
							}
							
						}
					}
					
					if (val == null || val.isEmpty() || val.equals("NaN")) {
						continue;
					}
					
					BigDecimal bd = new BigDecimal(val);
					bd = bd.setScale(2, RoundingMode.HALF_UP);
					if (n.equals("pctl")) {
						prn.setPctl(bd.floatValue());
					} else if (n.equals("rate")) {
						prn.setRate(bd.floatValue());
					} else if (n.equals("narr")) {
						prn.setNarr(bd.intValue());
					}
					
				} 
				// end of "score" (prn) loop
					// data put into output here.. names are the suffix of the
					// scale score
				String key;
				if (name.equals(RAV_SCALE)) {
					key = "RAVENSB";
					if (!courseList.contains(IGConstants.I_CODE_RAVB2) && !courseList.contains(IGConstants.I_CODE_RAVB)){
						prn.setRate(0);
						//POR-370 if the project doesn't contain RV2, then don't use Ravens score, 
						//even if it exists from a different project.
					}
				} else {
					key = "ALR_" + name;
				}
				if (prn != null){
					ret.getScaleData().put(key, prn);
				}
			} // end of "scale" loop

			// Get the rcommended ratings
			nodeList = (NodeList) xpath.compile("/kfarScoreData/recommendedRatings/rating").evaluate(doc,
					XPathConstants.NODESET);
			if (nodeList == null) {
				System.out.println("getAlrIgScoreData(): empty recommended rating list");
			}
			// int size = nodeList.getLength();
			for (int index = 0; index < nodeList.getLength(); index++) {
				Element e = (Element) nodeList.item(index);
				String name = e.getAttribute("id");
				int val = (int) Float.parseFloat(e.getTextContent());
				if (name.equals("experience")) {
					ret.setExperienceRec(val);
				} else if (name.equals("style")) {
					ret.setStyleRec(val);
				} else if (name.equals("interest")) {
					ret.setInterestRec(val);
				} else if (name.equals("derail")) {
					ret.setDerailRec(val);
				} else if (name.equals("potential")) {
					ret.setAdvPotRec(val);
				} else {
					System.out.println("Invalid recommendation name (" + name + ")... ignored.");
				}
			}

			nodeList = (NodeList) xpath.compile("/kfarScoreData/drivers/driver").evaluate(doc, XPathConstants.NODESET);
			if (nodeList == null) {
				System.out.println("getAlrIgScoreData(): empty driver list");
			}
			for (int index = 0; index < nodeList.getLength(); index++) {
				Element e = (Element) nodeList.item(index);
				String name = e.getAttribute("id").toUpperCase();
				NodeList nl = e.getChildNodes();

				PrnData prn = new PrnData();
				for (int i2 = 0; i2 < nl.getLength(); i2++) {
					Element e2 = (Element) nl.item(i2);
					String n = e2.getNodeName();
					String val = e2.getTextContent();
					if (val == null || val.isEmpty() || val.equals("NaN")) {
						continue;
					}
					BigDecimal bd = new BigDecimal(val);
					bd = bd.setScale(2, RoundingMode.HALF_UP);
					if (n.equals("pctl")) {
						prn.setPctl(bd.floatValue());
					} else if (n.equals("rate")) {
						prn.setRate(bd.floatValue());
					} else if (n.equals("narr")) {
						prn.setNarr(bd.intValue());
					}
				} // end of "score" (prn) loop
					// data put into output here.. names are the suffix of the
					// scale score
				ret.getDrivers().put(name, prn);

				// Turns out that Drivers are also "scales", so put them in
				// there with a scale type name
				name = "ALR_" + name;
				ret.getScaleData().put(name, prn);
			} // end of "drivers" loop
		}

		// now make the competency scores
		// Get the list of competencies with constituent scales
		Map<Long, List<SpssInfo>> compScales = getSpssForComps();

		// get the normed scale score ratings
		Map<String, PrnData> scores = ret.getScaleData();

		// generate the competency scores
		// TODO Add the ravens empty check for SBA-216
		@SuppressWarnings("unused")
		boolean noRav = false;
		for (Iterator<Map.Entry<Long, List<SpssInfo>>> itr = compScales.entrySet().iterator(); itr.hasNext();) {
			// get the next competency
			Map.Entry<Long, List<SpssInfo>> ent = itr.next();
			long compId = ent.getKey();
			float compScore = 0.0f; // Default value = 0

			if (hasScores) {
				// get the constituent scores and average them
				int cnt = 0;
				float cumScore = 0.0f;
				for (SpssInfo ci : ent.getValue()) {
					// Get the rating for the named spss key. If it isn't
					// present, skip it
					String spssKey = ci.getSpssId();
					PrnData pd = scores.get(spssKey);
					if (pd == null) {
						if (spssKey.equals(WG_SCALE)) {
							continue;
						} else {
							throw new Exception("Score data for key " + spssKey + " does not exist; ppt=" + this.partId
									+ ", project=" + this.projId);
						}
					}
					Float score = scores.get(spssKey).getRate();
					if (score == null || score == 0.0f) {
						if (spssKey.equals("RAVENSB")) {
							noRav = true;
						}
						continue;
					}

					// got a live one
					cnt++;
					// No rounding being done here
					if (ci.isInverted()) {
						cumScore += (6.0 - score);
					} else {
						cumScore += score;
					}
				}

				// Finished the list of scales for the competency. Calculate the
				// score and save it for output.
				if (cnt > 0) {
					compScore = cumScore / cnt;
				}
			} else {
				compScore = -1f;
			}

			ret.getCompScores().put(compId, compScore);
		}

		// Save it for use during this instance
		this.holder = ret;
		return ret;
	}
	
	private PrnData getRv2Rating(String partId, long projId) throws Exception {
		int ravScore = 0;
		Rv2Helper rv2Helper = new Rv2Helper();
		try {
			//ravScore = getRv2RawScore(partId, projId);
			ravScore = rv2Helper.getRv2RawScore(partId, projId);
			log.debug("Got RV2 Raw Score: {}", ravScore);
			//this.setRv2Rawscore(ravScore);
		} catch (Exception e) {
			// Pass it on along
			throw new Exception(e);
		}
		if (ravScore == -1) {
			return null;
		}

		// Norm it

		return Rv2Helper.doRavens2Norming(ravScore);
	}

	/**
	 * getAlrRgrData - Public method supporting forced scoring
	 * 
	 * @param partId
	 * @param projId
	 * @return
	 * @throws Exception
	 */
	public String getAlrRgrData(String partId, long projId, boolean forceScore) throws Exception {
		return fetchAlrPayload(partId, projId, forceScore);
	}

	/**
	 * getAlrRgrData - Public convenience method supporting pre-forced
	 * invocations (or invocations where forced scoring of ALR data is not
	 * needed)
	 * 
	 * @param partId
	 * @param projId
	 * @return
	 * @throws Exception
	 */
	public String getAlrRgrData(String partId, long projId) throws Exception {
		return fetchAlrPayload(partId, projId, false);
	}

	/**
	 *
	 * @param partId
	 * @param projId
	 * @return
	 * @throws Exception
	 */
	private String fetchAlrPayload(String partId, long projId, boolean forceScore) throws Exception {
		// TODO The query to get PALMS data should be a service
		// ----------------------------------------------------------------------------------------------//
		// TODO If the report system is updated to use a newer Admin version, do
		// this over there by getting
		// a Project object and fetching the data (no need to send it across)
		// Get the project related data needed for queries
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT  nn.version, ");
		sb.append(" 	   tlt.code, ");
		sb.append(" 	   tlt.target_level_index ");
		sb.append("  FROM platform.dbo.project pp WITH(NOLOCK) ");
		sb.append(
				"  LEFT JOIN platform.dbo.target_level_type tlt ON tlt.target_level_type_id = pp.target_level_type_id ");
		sb.append(
				"  LEFT JOIN platform.dbo.norm nn ON nn.project_type_id = pp.project_type_id and nn.default_norm = 1 ");
		sb.append("  WHERE pp.project_id = " + projId);

		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(sb);
		log.debug("Querying Platform with SQL: {}", dlps.toString());
		if (dlps.isInError()) {
			String err = "IGAlrDataHelper.getAlrIgScoreData() - Error preparing project query.  " + dlps.toString();
			System.out.println(err);
			throw new Exception(err);
		}

		// Go and get the project related info
		DataResult dr = null;
		ResultSet rs = null;
		String nVer = null;
		String tlCode = null;
		int tlIdx;

		try {
			dr = NhnDatabaseUtils.select(dlps);
			int stat = dr.getStatus().getStatusCode();
			if (stat == RequestStatus.RS_ERROR) {
				String err = "IGAlrDataHelper.getAlrRgrData() - Error executing select.  Ppt=" + partId + ", proj="
						+ projId + ". " + dlps.toString();
				log.error(err);
				throw new Exception(err);
			} else {
				if (stat == RequestStatus.DBS_NO_DATA) {
					// No data to find...
					String err = "IGAlrDataHelper.getAlrRgrData() - No data found for project " + projId + ". ";
					log.error(err);
					throw new Exception(err);
				} else {
					// Get the data
					rs = dr.getResultSet();
					if (rs == null || !rs.isBeforeFirst()) {
						String err = "IGAlrDataHelper.getAlrRgrData() - No ResultSet for project " + projId + ". ";
						log.error(err);
						throw new Exception(err);
					} else {
						// Get the data -- assume that there is a single row
						rs.next();
						nVer = rs.getString("version");
						tlCode = rs.getString("code");
						tlIdx = rs.getInt("target_level_index");
						log.debug("Got Norm version: {}, Target Level Code: {}, Target Level Index: {}", nVer, tlCode, tlIdx);
					}
				}
			}
		} finally {
			if (dr != null) {
				dr.close();
				dr = null;
			}
		}

		// Do our magic hardcode of the MLL version number
		if (tlCode.equals("KFP_FLL") || tlCode.equals("KFP_MLL")) {
			// Try to remove the hard-code when the versions all get in sync
			nVer = FLL_MLL_HARDCODE_VERSION;
		}

		// Check version
		if (nVer == null || nVer.length() < 1) {
			String err = "IGAlrDataHelper.getAlrRgrData - No norm version data for project " + projId;
			log.error(err);
			throw new Exception(err);
		}

		if (tlCode == null || tlCode.length() < 1) {
			String err = "IGAlrDataHelper.getAlrRgrData - No target level code found for project " + projId;
			log.error(err);
			throw new Exception(err);
		}

		// Check code - assumes that the index will be correct if there is a
		// valid code
		if (tlCode.equals("KFP_FLL") || tlCode.equals("KFP_MLL") || tlCode.equals("KFP_BUL")
				|| tlCode.equals("KFP_SEA")) {
			// We are ok... use the tlIdx value
		} else {
			String err = "IGAlrDataHelper.getAlrRgrData - Invalid target level (" + tlCode + ") for project " + projId
					+ ".  Only FLL, MLL, BUL, or SEA are allowed.";
			log.error(err);
			throw new Exception(err);
		}
		// ----------------------------------------------------------------------------------------------//

		URL url = null;
		URLConnection uc = null;
		InputStream content = null;

		// Call the servlet
		// servlet should get data from the RGR if possible, otherwise get a
		// payload from scoring.
		String surl = PropertyLoader.getProperty("com.pdi.data.abyd.application", "palms.reportServer.url");
		
		if (surl.charAt(surl.length() - 1) == '/') {
			surl = surl.substring(0, surl.length() - 2);
		}
		surl += SCORE_PARM_STR;

		surl = surl.replace("{pptId}", partId);
		surl = surl.replace("{projId}", "" + projId);
		surl = surl.replace("{tgtLvl}", "" + tlIdx);
		surl = surl.replace("{normVer}", nVer);

		if (forceScore) {
			surl += "&forceScore=true";
		}
		
		log.debug("Calling URL: {}", surl);

		try {
			url = new URL(surl);
		} catch (MalformedURLException e) {
			String err = "IGAlrDataHelper.getAlrRgrData - Malformed URL - " + surl;
			log.error(err);
			throw new Exception(err);
		}

		// open the connection.
		try {
			uc = url.openConnection();
		} catch (IOException e) {
			String err = "IGAlrDataHelper.getAlrRgrData - Unable to open connection.  URL=" + surl + ".  Msg="
					+ e.getMessage();
			log.error(err);
			throw new Exception(err);
		}

		// Get the data
		// boolean hasErr = false;
		HttpURLConnection huc = (HttpURLConnection) uc;
		boolean hasScores = true;
		try {
			int statusCode = huc.getResponseCode();
			if (statusCode == HttpServletResponse.SC_NO_CONTENT
					|| statusCode == HttpServletResponse.SC_INTERNAL_SERVER_ERROR) {
				// String str = "The KFALP instrument has not been completed for
				// participant " + this.partId + ". The IG will not be available
				// until it is complete.";
				// throw new Exception(str);
				hasScores = false;
			} else if (statusCode >= 200 && statusCode < 300) {
				content = huc.getInputStream();
			} else {
				String str = "Error returned from score fetch: Code=" + statusCode + " for URL=" + surl + ".  Message="
						+ huc.getResponseMessage();
				log.error(str);
				throw new Exception(str);
			}
		} catch (IOException e) {
			String err = "fetchAlrPayload: Error fetching score RGR data.  Msg=" + e.getMessage();
			log.error(err);
			throw new Exception(err);
		}

		StringBuffer result = new StringBuffer();
		if (hasScores) {
			BufferedReader br = new BufferedReader(new InputStreamReader(content));
			String line;
			try {
				while ((line = br.readLine()) != null) {
					result.append(line);
				}
			} catch (IOException e) {
				String err = "IGAlrDataHelper.getAlrRgrData - Unable to read content.  URL=" + surl + ".  Msg="
						+ e.getMessage();
				log.error(err);
				throw new Exception(err);
			}
		}

		// OK
		log.debug("Success");
		return result.toString();
	}

	/**
	 * AlrIgData - A thin class to carry data needed by the IG and related
	 * screens when using ALR data.
	 */
	private class AlrIgData {

		//
		// Instance data.
		//

		private boolean cogsDone = false;
		private final Map<String, PrnData> scaleData = new HashMap<String, PrnData>(); // Traits
		private final Map<Long, Float> compScores = new HashMap<Long, Float>();
		private final Map<String, PrnData> drivers = new HashMap<String, PrnData>();

		// Recomendations
		private int experienceRec = 0;
		private int styleRec = 0;
		private int interestRec = 0;
		private int derailRec = 0;
		private int advPotRec = 0;

		//
		// Constructors.
		//
		public AlrIgData() {
			// default constructor
		}

		//
		// Instance methods.
		//

		/*****************************************************************************************/
		public boolean getCogsDone() {
			return this.cogsDone;
		}

		public void setCogsDone(boolean value) {
			this.cogsDone = value;
		}

		/*****************************************************************************************/
		public Map<String, PrnData> getDrivers() {
			return this.drivers;
		}

		// public void setDrivers(Map<String, PrnData> value)
		// {
		// this.drivers = value;
		// }

		/*****************************************************************************************/
		public Map<String, PrnData> getScaleData() {
			return this.scaleData;
		}

		// public void setScaleData(Map<String, PrnData> value)
		// {
		// this.scaleData = value;
		// }

		/*****************************************************************************************/
		public Map<Long, Float> getCompScores() {
			return this.compScores;
		}

		// public void setCompScores(Map<Long, Float> value)
		// {
		// this.compScores = value;
		// }

		/*****************************************************************************************/
		public int getExperienceRec() {
			return this.experienceRec;
		}

		public void setExperienceRec(int value) {
			this.experienceRec = value;
		}

		/*****************************************************************************************/
		public int getStyleRec() {
			return this.styleRec;
		}

		public void setStyleRec(int value) {
			this.styleRec = value;
		}

		/*****************************************************************************************/
		public int getInterestRec() {
			return this.interestRec;
		}

		public void setInterestRec(int value) {
			this.interestRec = value;
		}

		/*****************************************************************************************/
		public int getDerailRec() {
			return this.derailRec;
		}

		public void setDerailRec(int value) {
			this.derailRec = value;
		}

		/*****************************************************************************************/
		public int getAdvPotRec() {
			return this.advPotRec;
		}

		public void setAdvPotRec(int value) {
			this.advPotRec = value;
		}
	}
}
