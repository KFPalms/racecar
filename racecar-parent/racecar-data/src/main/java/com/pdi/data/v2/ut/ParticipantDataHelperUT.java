package com.pdi.data.v2.ut;

//import java.io.FileOutputStream;
//import java.io.OutputStream;
import java.util.HashMap;
import java.util.Iterator;
//import java.util.Map;

//// Stuff used in debug dumps
//import java.util.Iterator;
//import java.util.Map;

//import com.pdi.data.v2.dto.SessionUserDTO;
import com.pdi.data.v2.helpers.ParticipantDataHelper;
import com.pdi.data.v2.util.V2DatabaseUtils;
//import com.pdi.data.v2.util.V2WebserviceUtils;
//import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;
//import com.pdi.reporting.request.ReportingRequest;
//import com.pdi.reporting.response.ReportingResponse;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

public class ParticipantDataHelperUT  extends TestCase
{
	public ParticipantDataHelperUT(String name)
	{
		super(name);
	}

	

//	// getting report data for particular person.... 
//	public void testGetIndividualReportData()
//		throws Exception
//	{
////		if(true)
////		{
////			return;
////		}
//		
//		UnitTestUtils.start(this);
//		V2DatabaseUtils.setUnitTest(true);
//		
//		boolean tcon = V2DatabaseUtils.testConnection();
//		System.out.println("Connection=" + tcon);
//		
//		String username = "testuser";
//		String password = "testuser";
//		V2WebserviceUtils v2web = new V2WebserviceUtils();
//		SessionUserDTO sessionUser = v2web.registerUser(username, password);
//		System.out.println("The user name is " + sessionUser.getName());
//		
//		// Dev data - part ID then project ID
//		//IndividualReport ir = ParticipantDataHelper.getReportData(sessionUser, "LMHJFVKU", "IXRYXBYO");
//		//IndividualReport ir = ParticipantDataHelper.getReportData(sessionUser, "LLGXCNAL", "HOCAQLAH");
//		//IndividualReport ir = ParticipantDataHelper.getIndividualReportData(sessionUser, "KBMWHRNX", "HOCAQLAH");
//		//IndividualReport ir = ParticipantDataHelper.getReportData(sessionUser, "DXDJKSAT", "CTNJPLFK");
//		//IndividualReport ir = ParticipantDataHelper.getReportData(sessionUser, "CPNNUCZC", "BHFLYUZC");	// No transition level error
//		// Cert data
//		//IndividualReport ir = ParticipantDataHelper.getIndividualReportData(sessionUser, "IUGYNYEH", "HOCAQLAH");	// Jaroslava Testuser !
//		//IndividualReport ir = ParticipantDataHelper.getIndividualReportData(sessionUser, "DVASJMRS", "HOCAQLAH");	// Johna DTestuer !
//		//IndividualReport ir = ParticipantDataHelper.getIndividualReportData(sessionUser, "IUGIWFBS", "HOCAQLAH");	// Radua ATestuser !
//		//IndividualReport ir = ParticipantDataHelper.getIndividualReportData(sessionUser, "CRNLTHAI", "HOCAQLAH");	// Christinea Testuser !
//		//IndividualReport ir = ParticipantDataHelper.getIndividualReportData(sessionUser, "LLONVITN", "HOCAQLAH");	// Chrisa Testuser !
//		//IndividualReport ir = ParticipantDataHelper.getIndividualReportData(sessionUser, "FDWOWSBR", "HOCAQLAH");	// Rolson CareerSurveyComplete TPOT
//		//IndividualReport ir = ParticipantDataHelper.getIndividualReportData(sessionUser, "ITQXJULS", "HOCAQLAH");	// Jiria Testuser !
//		//IndividualReport ir = ParticipantDataHelper.getIndividualReportData(sessionUser, "IYHCWBRK", "HOCAQLAH");	// Russella Testuser !
//		//IndividualReport ir = ParticipantDataHelper.getIndividualReportData(sessionUser, "HRZEUIRY", "HOCAQLAH");	// Marka Testuser !
//		//IndividualReport ir = ParticipantDataHelper.getIndividualReportData(sessionUser, "FGHOCBNA", "HOCAQLAH");	// Cognitive Test2
//		//IndividualReport ir = ParticipantDataHelper.getIndividualReportData(sessionUser, "HMHPWJJM", "HOCAQLAH");	// CareerSOnly Testuser
//		//IndividualReport ir = ParticipantDataHelper.getIndividualReportData(sessionUser, "BKZWOVUX", "HOCAQLAH");	// Jodi test 070308
//		//IndividualReport ir = ParticipantDataHelper.getIndividualReportData(sessionUser, "COBXYAXR", "HOCAQLAH");	// Blanka Testuser !
//		//IndividualReport ir = ParticipantDataHelper.getIndividualReportData(sessionUser, "BLBZRDZI", "HOCAQLAH");	//Rick 081808 2 Testuser
//		ParticipantDataHelper pdh = new ParticipantDataHelper();	// Not static anymore... need an instance
//		IndividualReport ir = pdh.getIndividualReportData(sessionUser, "IYIVROGG", "DWANNLCU");	// Directly to where this participant data lies
//		
//		
//		// Here's one to test the manual entry module processing
//		//IndividualReport ir = ParticipantDataHelper.getIndividualReportData(sessionUser, "CSYLSUON", "HPLMNNWG");	// MB Test 03-19-2010
//		
//		assertNotNull("No IndividualReport object returned.  ");
//			
//		//TODO These asserts check only the score data... add more testing
//		HashMap<String, ReportData> hm = ir.getReportData();
//		assertNotNull("No ReportData objects returned.  ", hm);
//			
//		// Minimal testing - just verify that the expected score data exists
//// Commented out asserts to allow incomplete data to flow to the report generation process
////		assertNull("No Ravens ReportData object should have been returned.  ", hm.get(ReportingConstants.RC_RAVENS_SF));
////		assertNotNull("A GPI ReportData object should have been returned.  ", hm.get(ReportingConstants.RC_GPI));
////		assertNotNull("An LEI ReportData object should have been returned.  ", hm.get(ReportingConstants.RC_LEI));
////		assertNotNull("A Career Survey ReportData object should have been returned.  ", hm.get(ReportingConstants.RC_CAREER_SURVEY));
//
//		//// Just for grins, dump the data (debug)
//		//// Display data
//		//HashMap<String, String> dd = ir.getDisplayData();
//		//for (Iterator<Map.Entry<String, String>> itr= ir.getDisplayData().entrySet().iterator(); itr.hasNext();  )
//		//{
//		//	Map.Entry<String, String> ent = itr.next();
//		//	System.out.println(ent.getKey() + "=" + ent.getValue());
//		//}
//		//// Score data
//		//for (Iterator<String> itr= hm.keySet().iterator(); itr.hasNext();  )
//		//{
//		//	String key = itr.next();
//		//	System.out.println("Score data for " + key);
//		//	ReportData rd = hm.get(key);
//		//	System.out.println(rd.scoresToString());
//		//}
//
////		//Build the individual report
////		ir.setReportCode(ReportingConstants.REPORT_CODE_TLT_INDIVIDUAL_SUMMARY);
////		ir.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);
////		ReportingRequest request = new ReportingRequest();
////		request.addReport(ir);
////		ReportingResponse response = request.generateReports();
////			
////		OutputStream outputStream = new FileOutputStream ("TestTLTIndivSummaryReport.pdf");
////		if(response.toFile() != null)
////		{
////			response.toFile().writeTo(outputStream);
////		}
//
//		UnitTestUtils.stop(this);
//	}


	// getting IdividualReport data for particular person and job 
	// note that the Session ID is not needed for this access
	public void testGetIndividualReportScoreDataWithJob()
		throws Exception
	{
//		if(true)
//		{
//			return;
//		}

		UnitTestUtils.start(this);
		V2DatabaseUtils.setUnitTest(true);
		
		// Cert data
		//IndividualReport ir = ParticipantDataHelper.getIndividualReportScoreData("FCXHWLDS", "DTXAAMBJ");	// Directly to where this participant data lies
		//IndividualReport ir = ParticipantDataHelper.getIndividualReportScoreData("HQRMTCQP", "DTXAAMBJ");	// Directly to where this participant data lies
		ParticipantDataHelper participantDataHelper = new ParticipantDataHelper();	// No longer a static - create an instance
		//IndividualReport ir = participantDataHelper.getIndividualReportScoreData("HQRMTCQP", "BJGGLVDE");	// Directly to where this participant data lies
		IndividualReport ir = participantDataHelper.getIndividualReportScoreData("IYIVROGG", "DWANNLCU");	// Directly to where this participant data lies
		
		assertNotNull("No IndividualReport object returned.  ");

		// Just for grins, dump the data (debug)
		// Display data
		//System.out.println("IR Display data:");
		//HashMap<String, String> dd = ir.getDisplayData();
		//for (Iterator<Map.Entry<String, String>> itr= ir.getDisplayData().entrySet().iterator(); itr.hasNext();  )
		//{
		//	Map.Entry<String, String> ent = itr.next();
		//	System.out.println(ent.getKey() + "=" + ent.getValue());
		//}
		HashMap<String, String> dispDat = ir.getDisplayData();
		System.out.println("IR displayData count=" + dispDat.size());
		
		HashMap<String, String> scrDat = ir.getScriptedData();
		System.out.println("IR scriptedData count=" + scrDat.size());
		
		HashMap<String, Double> sgDat = ir.getScriptedGroupData();
		System.out.println("IR scriptedGroupData count=" + sgDat.size());
		
			// And now the ReportData
		HashMap<String, ReportData> rDat = ir.getReportData();
		System.out.println("IR scriptedGroupData count=" + rDat.size());
		for (Iterator<String> itr= rDat.keySet().iterator(); itr.hasNext();  )
		{
			String key = itr.next();
			ReportData rd = rDat.get(key);
			System.out.println("Score data for " + key + " (" + rd.getScoreData().size() + " items)");
			System.out.println(rd.scoreToString());
			
			System.out.println("Raw data for " + key + " (" + rd.getRawData().size() + " items");
			
			System.out.println("Display data for " + key + " (" + rd.getDisplayData().size() + " items)");
			System.out.println(rd.displayToString());
			
			System.out.println("Scripted data for " + key + " (" + rd.getScriptedData().size() + " items)");
		}

		UnitTestUtils.stop(this);
	}


	// getting IdividualReport data for particular person
//	public void testGetIndividualReportScoreData()
//		throws Exception
//	{
////		if(true)
////		{
////			return;
////		}
//
//		UnitTestUtils.start(this);
//		V2DatabaseUtils.setUnitTest(true);
//		
//		// Cert data
//		//IndividualReport ir = ParticipantDataHelper.getIndividualReportScoreData("FCXHWLDS");
//		ParticipantDataHelper participantDataHelper = new ParticipantDataHelper();
//		IndividualReport ir = participantDataHelper.getIndividualReportScoreData("HQRMTCQP");
//	
//		assertNotNull("No IndividualReport object returned.  ");
//
//		// Just for grins, dump the data (debug)
//		HashMap<String, String> dispDat = ir.getDisplayData();
//		System.out.println("IR displayData count=" + dispDat.size());
//		
//		HashMap<String, String> scrDat = ir.getScriptedData();
//		System.out.println("IR scriptedData count=" + scrDat.size());
//		
//		HashMap<String, Double> sgDat = ir.getScriptedGroupData();
//		System.out.println("IR scriptedGroupData count=" + sgDat.size());
//		
//			// And now the ReportData
//		HashMap<String, ReportData> rDat = ir.getReportData();
//		System.out.println("IR scriptedGroupData count=" + rDat.size());
//		for (Iterator<String> itr= rDat.keySet().iterator(); itr.hasNext();  )
//		{
//			String key = itr.next();
//			ReportData rd = rDat.get(key);
//			System.out.println("Score data for " + key + " (" + rd.getScoreData().size() + " items)");
//			System.out.println(rd.scoresToString());
//			
//			System.out.println("Raw data for " + key + " (" + rd.getRawData().size() + " items");
//			
//			System.out.println("Display data for " + key + " (" + rd.getDisplayData().size() + " items)");
//			System.out.println(rd.displayToString());
//			
//			System.out.println("Scripted data for " + key + " (" + rd.getScriptedData().size() + " items)");
//		}
//
//		UnitTestUtils.stop(this);
//	}

}
