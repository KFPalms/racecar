/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */

package com.pdi.data.abyd.helpers.intGrid;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import com.pdi.data.abyd.dto.intGrid.IGFinalScoreDTO;


/**
 * SaveIGCompScoreDataHelper contains the code needed to save the integration score
 * and a possible note as entered by the lead consultant for the Integration Grid.
 *
 * @author		Ken Beukelman
 */
public class SaveIGCompScoreDataHelper
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private Connection con;
	private String participantId;
	private long dnaId;
	private long competencyId;
	private int scoreIdx;
	private String note;

	//
	//
	// Constructors.
	//
	public SaveIGCompScoreDataHelper(Connection con, IGFinalScoreDTO compData)
		throws Exception
	{
		// Check key data
		if (compData.getPartId() == null || compData.getPartId().length() == 0  ||
			compData.getDnaId()	== 0 ||
			compData.getCompId() == 0)
		{
			throw new Exception("Invalid Final Score Data key:  part=" +  (compData.getPartId() == null ? "null" : (compData.getPartId().length() == 0 ? "<empty string>" : compData.getPartId())) +
					            ", dna=" + compData.getDnaId() +
					            ", module=" + compData.getCompId());
		}

		this.con = con;
		this.participantId = compData.getPartId();
		this.dnaId = compData.getDnaId();
		this.competencyId = compData.getCompId();
		this.scoreIdx = compData.getCompScoreIdx();
		this.note = compData.getCompNote();
	}

	//
	// Instance methods.
	//

	/**
	 * Insert or update the comp integration score and associated note.  Returns nothing.
	 *
	 * @throws Exception
	 */
	public void saveScore()
		throws Exception
	{
		// Change any double quotes to single quotes
		// This prevents "Bad SQL" messages for strings that include double quotes
		// also, if it's an empty string, just null it
		if (this.note != null)
		{
			this.note = this.note.trim().replace("'","''");
			if (this.note.length() == 0)
			{
				this.note = null;
			}
		}

		Statement stmt = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("INSERT INTO pdi_abd_igrid_comp_resp ");
		sqlQuery.append("  (participantId, dnaId, competencyId, lcScore, lcNote, lastUserId, lastDate) ");
		sqlQuery.append("  VALUES(" + this.participantId + ", ");
		sqlQuery.append("         " + this.dnaId + ", ");
		sqlQuery.append("         " + this.competencyId + ", ");
		sqlQuery.append("         " + this.scoreIdx + ", ");
		sqlQuery.append("         " + (this.note == null ? "null" : "N'" + this.note + "'") + ", ");
		sqlQuery.append("         SYSTEM_USER, ");
		sqlQuery.append("         GETDATE() )");

		try
		{
			// Do the insert
			stmt = this.con.createStatement();
			stmt.executeUpdate(sqlQuery.toString());
		}
		catch (SQLException ex)
		{
			// TODO Refactor so that the update does not depend upon the error code (do a query)
			// Duplicate key error triggers update; error code = 1062 on MySQL, 2627 on SQL Server
			if (ex.getErrorCode() == 2627)
			{
				// It's already there... update it
				try  {  stmt.close();  }
				catch (SQLException sqlEx)
				{
			    	System.out.println("ERROR: stmt close in saveScore-update - " + sqlEx.getMessage());	
				}
				stmt = null;
				sqlQuery.setLength(0);
				sqlQuery.append("UPDATE pdi_abd_igrid_comp_resp ");
				sqlQuery.append("  SET lcScore = " + this.scoreIdx + ", ");
				sqlQuery.append("      lcNote = " + (this.note == null ? "null" : "N'" + this.note + "'") + ", ");
				sqlQuery.append("      lastUserId = SYSTEM_USER, ");
				sqlQuery.append("      lastDate = GETDATE() ");
				sqlQuery.append("  WHERE participantId = " + this.participantId + " ");
				sqlQuery.append("    AND dnaId = " + this.dnaId + " ");
				sqlQuery.append("    AND competencyId = " + this.competencyId);

				try
				{
					stmt = this.con.createStatement();
					stmt.executeUpdate(sqlQuery.toString());
				}
				catch (SQLException ex2)
				{
					throw new Exception("SQL saveScore (update).  " +
							"SQLException: " + ex2.getMessage() + ", " +
							"SQLState: " + ex2.getSQLState() + ", " +
							"VendorError: " + ex2.getErrorCode());
				}
			}
			else
			{
				throw new Exception("SQL saveScore (insert).  " +
									"SQLException: " + ex.getMessage() + ", " +
									"SQLState: " + ex.getSQLState() + ", " +
									"VendorError: " + ex.getErrorCode());
			}
		}
		finally
		{
			if (stmt != null)
			{
				try  {  stmt.close();  }
				catch (SQLException sqlEx)
				{
			    	System.out.println("ERROR: stmt close in saveScore - " + sqlEx.getMessage());	
				}
				stmt = null;
			}
		}
	}
}
