/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.dto.intGrid;

import java.util.ArrayList;

import com.pdi.data.abyd.dto.intGrid.IGExtCogData;
import com.pdi.data.abyd.dto.intGrid.IGExtEGCompletionData;
import com.pdi.data.abyd.dto.intGrid.IGExtRatingData;
import com.pdi.data.abyd.dto.intGrid.IGExtSPData;

/**
 * IGExtractDataDTO is a wrapper around a number of data items
 * required for the Integration Grid report data extract.
 * 
 * @author		Ken Beukelman
 */
public class IGExtractDataDTO
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _partId;
	//	cand Last Name
	// cand first name
	// client name
	// Job name
	private String _modelName;
	private ArrayList<IGExtEGCompletionData> _egCompletionDates;
	private String _igCompletionDate;
	private ArrayList<IGExtCompData> _compData;
	//overall competency score
	//dLCI
	private ArrayList<IGExtCogData> _cogData;

	private ArrayList<IGExtRatingData> _leiRatings;
	private String _leiNormName;
	private String _rdi;
	private ArrayList<IGExtRatingData> _gpiRatings;
	private String _gpiNormName;
	private ArrayList<IGExtSPData> _successProfile;
	private String _workingNotes;

	//
	// Constructors.
	//
	public IGExtractDataDTO()
	{
		// does nothing right now
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public String getPartId()
	{
		return _partId;
	}
	
	public void setPartId(String value)
	{
		_partId = value;
	}	

	/*****************************************************************************************/
	public String getModelName()
	{
		return _modelName;
	}
	
	public void setModelName(String value)
	{
		_modelName = value;
	}	

	/*****************************************************************************************/
	public ArrayList<IGExtEGCompletionData> getEgCompletionDates()
	{
		if (_egCompletionDates == null)
		{
			_egCompletionDates = new ArrayList<IGExtEGCompletionData>();
		}
		return _egCompletionDates;
	}
	
	public void setEgCompletionDates(ArrayList<IGExtEGCompletionData> value)
	{
		_egCompletionDates = value;
	}	

	/*****************************************************************************************/
	public String getIgCompletionDate()
	{
		return _igCompletionDate;
	}
	
	public void setIgCompletionDate(String value)
	{
		_igCompletionDate = value;
	}	

	/*****************************************************************************************/
	public ArrayList<IGExtCompData> getCompData()
	{
		if (_compData == null)
		{
			_compData = new ArrayList<IGExtCompData>();
		}
		return _compData;
	}
	
	public void setCompData(ArrayList<IGExtCompData> value)
	{
		_compData = value;
	}

	/*****************************************************************************************/
	public ArrayList<IGExtCogData> getCogData()
	{
		if (_cogData == null)
		{
			_cogData = new ArrayList<IGExtCogData>();
		}
		return _cogData;
	}
	
	public void setCogData(ArrayList<IGExtCogData> value)
	{
		_cogData = value;
	}

	/*****************************************************************************************/
	public ArrayList<IGExtRatingData> getLeiRatings()
	{
		if (_leiRatings == null)
		{
			_leiRatings = new ArrayList<IGExtRatingData>();
		}
		return _leiRatings;
	}
	
	public void setLeiRatings(ArrayList<IGExtRatingData> value)
	{
		_leiRatings = value;
	}

	/*****************************************************************************************/
	public String getLeiNormName()
	{
		return _leiNormName;
	}
	
	public void setLeiNormName(String value)
	{
		_leiNormName = value;
	}	

	/*****************************************************************************************/
	public String getRdi()
	{
		return _rdi;
	}
	
	public void setRdi(String value)
	{
		_rdi = value;
	}	

	/*****************************************************************************************/
	public ArrayList<IGExtRatingData> getGpiRatings()
	{
		if (_gpiRatings == null)
		{
			_gpiRatings = new ArrayList<IGExtRatingData>();
		}
		return _gpiRatings;
	}
	
	public void setGpiRatings(ArrayList<IGExtRatingData> value)
	{
		_gpiRatings = value;
	}

	/*****************************************************************************************/
	public String getGpiNormName()
	{
		return _gpiNormName;
	}
	
	public void setGpiNormName(String value)
	{
		_gpiNormName = value;
	}	

	/*****************************************************************************************/
	public ArrayList<IGExtSPData> getSuccessProfile()
	{
		if (_successProfile == null)
		{
			_successProfile = new ArrayList<IGExtSPData>();
		}
		return _successProfile;
	}
	
	public void setSuccessProfile(ArrayList<IGExtSPData> value)
	{
		_successProfile = value;
	}

	/*****************************************************************************************/
	public String getWorkingNotes()
	{
		return _workingNotes;
	}
	
	public void setWorkingNotes(String value)
	{
		_workingNotes = value;
	}



	/*
	 * Show me what you've got
	 */
	public String toString()
	{
		String ret = "IGExtractDataDTO content (Java)\n";
		ret += "            Part.  ID=" + _partId + "\n";
		ret += "           Model Name=" + _modelName + "\n";
		ret += "  EG Completion Dates:\n";
		if (_egCompletionDates == null || _egCompletionDates.size() < 1)
		{
			ret += "    None";
		}
		else
		{
			for (IGExtEGCompletionData ecd : _egCompletionDates)
			{
				ret += ecd.toString();
			}
		}
		ret += "  EG Completion Date=" + _igCompletionDate + "\n";
		ret += "  EG Extract Competencies:\n";
		if (_compData == null || _compData.size() < 1)
		{
			ret += "    None";
		}
		else
		{
			for (IGExtCompData ied : _compData)
			{
				ret += ied.toString();
			}
		}
		ret += "  EG Extract Cog Info:\n";
		if (_cogData == null || _cogData.size() < 1)
		{
			ret += "    None";
		}
		else
		{
			for (IGExtCogData cog : _cogData)
			{
				ret += cog.toString();
			}
		}
		ret += "  LEI Ratings:\n";
		if (_leiRatings == null || _leiRatings.size() < 1)
		{
			ret += "    None";
		}
		else
		{
			for (IGExtRatingData lei : _leiRatings)
			{
				ret += lei.toString();
			}
		}
		ret += "       LEI Norm Name=" + _leiNormName + "\n";
		ret += "                 RDI=" + _rdi + "\n";
		ret += "  GPI Ratings:\n";
		if (_gpiRatings == null || _gpiRatings.size() < 1)
		{
			ret += "    None";
		}
		else
		{
			for (IGExtRatingData gpi : _gpiRatings)
			{
				ret += gpi.toString();
			}
		}
		ret += "       GPI Norm Name=" + _gpiNormName + "\n";
		ret += "  Success Profile:\n";
		if (_successProfile == null || _successProfile.size() < 1)
		{
			ret += "    None";
		}
		else
		{
			for (IGExtSPData gpi : _successProfile)
			{
				ret += gpi.toString();
			}
		}
		ret += "       Working Notes=" + _workingNotes + "\n";
		
		return ret;
	}
}
