package com.pdi.data.helpers.interfaces;


import java.util.HashMap;

import com.pdi.data.dto.Norm;
import com.pdi.data.dto.SessionUser;

public interface INormHelper {
	
	public Norm fromId(SessionUser session, int normGroupId);
	public Norm fromSpssValueGroupNormId(SessionUser session, String spssValue, int groupNormId);
	public HashMap<String, Norm> fromNormGroupId(SessionUser session, int instrumentId);
	

}
