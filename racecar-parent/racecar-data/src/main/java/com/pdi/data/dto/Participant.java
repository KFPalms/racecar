/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.data.dto;

import java.util.HashMap;

/**
 * Participant is a basic data class that contains participant data.
 * 
 * @author		Gavin Myers
 */
public class Participant extends CoreDataObject
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	/**
	 * 
	 */
	private static final long serialVersionUID = -4160106005552869737L;
	//
	// Instance data.
	//
	private String id = null;
	private String firstName = null;
	private String lastName = null;
	private String email = null;
	private NoteHolder adminNotes = null;	// Must be explicitly populated... not part of the normal fetch

	private HashMap<String, String> metadata = new HashMap<String, String>();

	//
	// Constructors.
	//
	public Participant()
	{
		// does nothing right now
	}
	
	public Participant(String idIn)
	{
		id = idIn;
	}

	//
	// Instance methods.
	//
	
	public String toString()
	{
		String ret = "  Participant - id=" + id +
			", Name=" + (firstName == null ? "-" : firstName) + " " + (lastName == null ? "-" : lastName) +
			", Email=" + (email == null ? "-" : email);
//		ret += "\n    Meta:";
//		for (Iterator<Entry<String, String>> itr=metadata.entrySet().iterator(); itr.hasNext(); )
//		{
//			Map.Entry<String, String> ent = itr.next();
//			ret += "\n     " + ent.getKey() + "=" + ent.getValue();
//		}
		
		return ret;
	}
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public void setId(String value)
	{
		id = value;
	}
	
	public String getId()
	{
		return id;
	}

	/*****************************************************************************************/
	public void setFirstName(String value)
	{
		firstName = value;
	}
	
	public String getFirstName()
	{
		return firstName;
	}

	/*****************************************************************************************/
	public void setLastName(String value)
	{
		lastName = value;
	}
	
	public String getLastName()
	{
		return lastName;
	}

	/*****************************************************************************************/
	public void setEmail(String value)
	{
		email = value;
	}
	
	public String getEmail()
	{
		return email;
	}

	/*****************************************************************************************/
	public void setAdminNotes(NoteHolder value)
	{
		adminNotes = value;
	}
	
	public NoteHolder getAdminNotes()
	{
		return adminNotes;
	}

	/*****************************************************************************************/
	public void setMetadata(HashMap<String, String> value)
	{
		metadata = value;
	}
	
	public HashMap<String, String> getMetadata()
	{
		return metadata;
	}
}
