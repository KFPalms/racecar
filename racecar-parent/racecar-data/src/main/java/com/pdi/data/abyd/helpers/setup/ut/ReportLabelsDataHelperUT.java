/*
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.setup.ut;

import com.pdi.data.abyd.dto.setup.Setup2ReportLabelsDTO;
import com.pdi.data.abyd.helpers.setup.ReportLabelsDataHelper;
import com.pdi.data.abyd.helpers.setup.SetupConstants;
import com.pdi.ut.UnitTestUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;

import junit.framework.TestCase;

public class ReportLabelsDataHelperUT extends TestCase
{
	//
	// Constructors
	//

	public ReportLabelsDataHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
		throws Exception
    {
		junit.textui.TestRunner.run(EntryStateHelperUT.class);
    }


	/*
	 * testGetReportLabelData
	 */
//	public void testGetSetupReportLabelData()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//
//		// set a dna id
//		long dnaId = 6;
//		Setup2ReportLabelsDTO dto = null;
//		
//		ReportLabelsDataHelper reportLabelsDataHelper = new ReportLabelsDataHelper();
//		
////		System.out.println("-------  EDITABLE  -------");
////		dto = reportLabelsDataHelper.getSetupReportLabelData(dnaId, SetupConstants.EDITABLE);
////		System.out.println(dto.toString());
//		
////		System.out.println("-------  NOT_EDITABLE  -------");
////		dto = reportLabelsDataHelper.getSetupReportLabelData(dnaId, SetupConstants.NOT_EDITABLE);
////		System.out.println(dto.toString());
//		
//		System.out.println("-------  ALL_LABELS  -------");
//		dto = reportLabelsDataHelper.getSetupReportLabelData(dnaId, SetupConstants.ALL_LABELS);
//		System.out.println(dto.toString());
//	
//		UnitTestUtils.stop(this);
//	}


	/*
	 * testUpdateSetup2ReportOptionsData
	 */
//	public void testUpdateSetup2ReportLabelData()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//
//		/*
//		 * to test this, I'll start with 2 identical 
//		 * Setup2ReportLabelDTO's  
//		 * 
//		 * then I'll change one, and then compare them 
//		 */
//		
//		// set a dna id
//		long dnaId = 6;
//		ReportLabelsDataHelper reportLabelsDataHelper = new ReportLabelsDataHelper();
//
//		// Get an existing state to modify
//		Setup2ReportLabelsDTO dto2 = reportLabelsDataHelper.getReportLabelData(dnaId, SetupConstants.EDITABLE);
//	
//		//create changed value dto's :
//		String str = dto2.getLabelValuesArray().get(4).getCustomLabel();
//		if(str == null || str.equals("fake test Label #5 - try 2"))
//		{
//			str = "fake test Label #5";	
//		}else{
//			str = "fake test Label #5 - try 2";
//		}
//		dto2.getLabelValuesArray().get(4).setCustomLabel(str);
//	
//		str = dto2.getLabelValuesArray().get(8).getCustomLabel();
//		if(str == null || str.equals("fake test Label #9 - try 2")){
//			str = "fake test Label #9";
//		}else{
//			str = "fake test Label #9 - try 2";
//		}
//		dto2.getLabelValuesArray().get(8).setCustomLabel(str);
//	
//		//also update one of the Legends... 
//		str = dto2.getLeadershipLegend().getCustomLabel();
//		if(str == null || str.equals("Change the Leadership Competencies Legend....  -- try 2"))
//		{
//			str = "Change the Leadership Competencies Legend.... ";
//		}else{
//			str = "Change the Leadership Competencies Legend....  -- try 2";
//		}		
//		dto2.getLeadershipLegend().setCustomLabel(str);
//
//		// do the update
//		reportLabelsDataHelper.updateReportLabelData(dto2);
//		
//		//get the dto from the database, and make sure that the changes happened....
//		Setup2ReportLabelsDTO dto3 =   reportLabelsDataHelper.getReportLabelData( dnaId, SetupConstants.EDITABLE);
//
//		System.out.println("-------------------");
//		System.out.println("dto2: 0 "+ dto2.getLabelValuesArray().get(0).getReportLabelId() + " : " + dto2.getLabelValuesArray().get(0).getLabel() );
//		System.out.println("dto3: 0 "+ dto3.getLabelValuesArray().get(0).getReportLabelId() + " : " + dto3.getLabelValuesArray().get(0).getCustomLabel() );
//
//		System.out.println("-------------------");
//		System.out.println("dto2: 5 "+ dto2.getLabelValuesArray().get(4).getReportLabelId() + " : " + dto2.getLabelValuesArray().get(4).getLabel() );
//		System.out.println("dto3: 5 "+ dto3.getLabelValuesArray().get(4).getReportLabelId() + " : " + dto3.getLabelValuesArray().get(4).getCustomLabel() );
//
//		System.out.println("-------------------");
//		System.out.println("dto2: 9 "+ dto2.getLabelValuesArray().get(8).getReportLabelId() + " : " + dto2.getLabelValuesArray().get(8).getLabel() );
//		System.out.println("dto3: 9 "+ dto3.getLabelValuesArray().get(8).getReportLabelId() + " : " + dto3.getLabelValuesArray().get(8).getCustomLabel() );
//
//		System.out.println("-------------------");
//		System.out.println("dto2: 19 "+ dto2.getLeadershipLegend().getReportLabelId() + " : " + dto2.getLeadershipLegend().getLabel() );
//		System.out.println("dto3: 19 "+ dto3.getLeadershipLegend().getReportLabelId() + " : " + dto3.getLeadershipLegend().getCustomLabel() );
//		
//		UnitTestUtils.stop(this);
//	}


	/*
	 * testGetTranslatedReportLabelData (lang id)
	 */
//	public void testGetTranslatedReportLabelData_Id()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//	
//		// set a dna id
//		long dnaId = 6;
//		//long langId = 6;	// 6 = de
//		long langId = 1;	// 1 = en (default)
//		Setup2ReportLabelsDTO dto = null;
//		
//		ReportLabelsDataHelper reportLabelsDataHelper = new ReportLabelsDataHelper();
//		
//		System.out.println("-------  EDITABLE  -------");
//		dto = reportLabelsDataHelper.getTranslatedReportLabelData(dnaId, langId, SetupConstants.EDITABLE);
//		System.out.println(dto.toString());
//		
////		System.out.println("-------  NOT_EDITABLE  -------");
////		dto = reportLabelsDataHelper.getTranslatedReportLabelData(dnaId, langId, SetupConstants.NOT_EDITABLE);
////		System.out.println(dto.toString());
//		
////		System.out.println("-------  ALL_LABELS  -------");
////		dto = reportLabelsDataHelper.getTranslatedReportLabelData(dnaId, langId, SetupConstants.ALL_LABELS);
////		System.out.println(dto.toString());
//	
//		UnitTestUtils.stop(this);
//	}

	
	/*
	 * testGetTranslatedReportLabelData (lang code)
	 */
	public void testGetTranslatedReportLabelData_code()
		throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);
	
		// set a dna id
		long dnaId = 6;
		String langCode = "de";	// 6 = de
		//String langCode = "en";	// 6 = en (default)
		Setup2ReportLabelsDTO dto = null;
		
		ReportLabelsDataHelper reportLabelsDataHelper = new ReportLabelsDataHelper();
		
		System.out.println("-------  EDITABLE  -------");
		dto = reportLabelsDataHelper.getTranslatedReportLabelData(dnaId, langCode, SetupConstants.EDITABLE);
		System.out.println(dto.toString());
		
		System.out.println("-------  NOT_EDITABLE  -------");
		dto = reportLabelsDataHelper.getTranslatedReportLabelData(dnaId, langCode, SetupConstants.NOT_EDITABLE);
		System.out.println(dto.toString());
		
		System.out.println("-------  ALL_LABELS  -------");
		dto = reportLabelsDataHelper.getTranslatedReportLabelData(dnaId, langCode, SetupConstants.ALL_LABELS);
		System.out.println(dto.toString());
	
		UnitTestUtils.stop(this);
	}

}

