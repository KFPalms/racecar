/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.dto.intGrid;


/**
 * IGScaleScoreData is a thin class to hold score information for
 * later use.  It includes the scale name, the rating and a flag
 * indicating if this is a positive or negative score.
 * 
 * @author		Ken Beukelman
 */
public class IGScaleScoreData
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _scaleName;
	private String _rating;
	private boolean _isNegative = false;		// Default value is false
	private String _spssVal;

	//
	// Constructors.
	//
	public IGScaleScoreData()
	{
		// does nothing right now
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public String getScaleName()
	{
		return _scaleName;
	}
	
	public void setScaleName(String value)
	{
		_scaleName = value;
	}	

	/*****************************************************************************************/
	public String getRating()
	{
		return _rating;
	}
	
	public void setRating(String value)
	{
		_rating = value;
	}	

	/*****************************************************************************************/
	public boolean getIsNegative()
	{
		return _isNegative;
	}
	
	public void setIsNegative(boolean value)
	{
		_isNegative = value;
	}	
	
	/*****************************************************************************************/
	public String getSpssVal()
	{
		return _spssVal;
	}
	
	public void setSpssVal(String value)
	{
		_spssVal = value;
	}	



	/*
	 * Show me what you've got
	 */
	public String toString()
	{
		String ret = "IGScaleScoreData content (Java)\n";
		ret += "   Scale Name=" + _scaleName + "\n";
		ret += "       Rating=" + _rating + "\n";
		ret += "  is Negative=" + _isNegative + "\n";
		ret += "   SPSS Value=" + _spssVal + "\n";

		return ret;
	}
}
