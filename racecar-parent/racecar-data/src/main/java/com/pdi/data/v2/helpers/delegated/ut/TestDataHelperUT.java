package com.pdi.data.v2.helpers.delegated.ut;

import com.pdi.data.dto.SessionUser;
import com.pdi.data.dto.TestData;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.ITestDataHelper;
import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

public class TestDataHelperUT extends TestCase {
	
	public TestDataHelperUT(String name)
	{
		super(name);
	}
	
//	public void testGetCogsDoneFlag()
//	throws Exception
//	{
//		
//		SessionUser session = new SessionUser();
//		session.getMetadata().put("USERNAME", "mbpanichi");
//		session.getMetadata().put("PASSWORD", "griffin12");
//		UnitTestUtils.start(this);
//		V2DatabaseUtils.setUnitTest(true);
//	
//		ITestDataHelper helper = HelperDelegate.getTestDataHelper("com.pdi.data.v2.helpers.delegated");
//		System.out.println("testGetCogsDoneFlag....");
//		
//		// set up a fake TestData object
//		TestData td = new TestData();
//		td.setDnaId(6);
//		td.setParticipantId(60254);
//		td.setProjectId(1);
//		
//		boolean done = helper.getCogsDoneFlag(session, td);
//		
//		System.out.println("done == " + done);
//
//		UnitTestUtils.stop(this);
//
//	}
	
	public void testSaveCogsDoneFlag()
	throws Exception
	{
		
		SessionUser session = new SessionUser();
		session.getMetadata().put("USERNAME", "mbpanichi");
		session.getMetadata().put("PASSWORD", "griffin12");
		UnitTestUtils.start(this);
		V2DatabaseUtils.setUnitTest(true);
	
		ITestDataHelper helper = HelperDelegate.getTestDataHelper("com.pdi.data.v2.helpers.delegated");
		System.out.println("saveCogsDoneFlag....");
		
		// set up a fake TestData object
		TestData td = new TestData();
		td.setDnaId(6);
		td.setParticipantId(60254);
		td.setProjectId(0);
		
		helper.saveCogsDoneFlag(session, td);		
		boolean done = helper.getCogsDoneFlag(session, td);
		System.out.println("testGetCogsDoneFlag....");
		System.out.println("done == " + done);

		UnitTestUtils.stop(this);

	}

}
 