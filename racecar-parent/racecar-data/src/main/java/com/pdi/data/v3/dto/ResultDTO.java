/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.dto;

import java.util.ArrayList;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.pdi.data.util.BaseDTO;
import com.pdi.data.util.RequestStatus;
import com.pdi.xml.XMLUtils;

/**
 * DTO for result information
 * 
 *
 * @author		Gavin Myers
 */
public class ResultDTO extends BaseDTO
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _id;
	private ModuleDTO _module;
	private ProductDTO _product;
	private ArrayList<ScaleScoreDTO> _scaleScores;	// = new ArrayList<ScaleScoreDTO>();
	private ArrayList<RawScoreDTO> _rawScores;		// = new ArrayList<RawScoreDTO>();
	
	//
	// Constructors.
	//

	/**
	 * "Standard" (no-parameter) constructor
	 */
	public ResultDTO()
	{
		// Does nothing presently
	}
	
	/**
	 * Builds the object based off an xml node
	 * @param node
	 * @throws Exception
	 */
	public ResultDTO(Node node, String moduleId)
	{
		/*
		 * The XML looks something like this:
		 * <?xml version="1.0" encoding="utf-8" standalone="yes"?>
		 * <ns2:participantResultModuleList xmlns:ns2="urn:iscopia.com:participant-1.0">
		 *   <ns2:participantResultModuleListLine>
		 *     <moduleIdentifier>10fe2cac-b951-4b2a-bb76-2f524629ceb5</moduleIdentifier>
		 *     <moduleName>Career Survey</moduleName>
		 *     <resultList>
		 *       <ns2:resultListLine>
		 *         <normName>Raw Score</normName>
		 *         <participantResultList>
		 *           <ns2:participantResultListLine>
		 *             <candidateIdentifier>CWNL-QWAA</candidateIdentifier>
		 *             <firstName>q</firstName>
		 *             <lastName>q</lastName>
		 *             <constructList>
		 *               <ns2:constructListLine>
		 *                 <constructName>F_Career Goals Norm</constructName>
		 *                 <score>94.294656700000000</score>
		 *               </ns2:constructListLine>
		 *               <ns2:constructListLine>
		 *                 <constructName>F_Learning Orientation Norm</constructName>
		 *                 <score>99.949906300000000</score>
		 *               </ns2:constructListLine>
		 *               <ns2:constructListLine>
		 *                 <constructName>F_Learning Orientation</constructName>
		 *                 <score>5.000000000000000</score>
		 *               </ns2:constructListLine>
		 *               <ns2:constructListLine>
		 *                 <constructName>F_Career Drivers Norm</constructName>
		 *                 <score>19.766254300000000</score>
		 *               </ns2:constructListLine>
		 *               <ns2:constructListLine>
		 *                 <constructName>F_Career Drivers</constructName>
		 *                 <score>2.000000000000000</score>
		 *               </ns2:constructListLine>
		 *               <ns2:constructListLine>
		 *                 <constructName>F_Career Goals</constructName>
		 *                 <score>4.500000000000000</score>
		 *               </ns2:constructListLine>
		 *               <ns2:constructListLine>
		 *                 <constructName>F_Experience orientation</constructName>
		 *                 <score>3.000000000000000</score>
		 *               </ns2:constructListLine>
		 *               <ns2:constructListLine>
		 *                 <constructName>F_Experience orientation Norm</constructName>
		 *                 <score>7.564354900000000</score>
		 *               </ns2:constructListLine>
		 *             </constructList>
		 *           </ns2:participantResultListLine>
		 *         </participantResultList>
		 *       </ns2:resultListLine>
		 *     </resultList>
		 *   </ns2:participantResultModuleListLine>
		 * </ns2:participantResultModuleList>
		 */
		
		this.setModule(new ModuleDTO(node));
		this.getModule().setModuleGUID(moduleId);
		if (this.getModule().getStatus().getStatusCode() == RequestStatus.WB_NO_DATA)
			this.getModule().getStatus().setStatusCode(RequestStatus.RS_OK);
		
		// get the scale scores, but only put them in if there is something there
		NodeList nodes = XMLUtils.getElements(node, "//participantResultModuleListLine[moduleIdentifier='"+moduleId+"']/resultList/resultListLine/participantResultList/participantResultListLine/constructList/constructListLine");
		ArrayList<ScaleScoreDTO> scores = new ArrayList<ScaleScoreDTO>();
		for(int i = 0; i < nodes.getLength(); i++) {
			Node node2 = nodes.item(i);
			ScaleScoreDTO dto = new ScaleScoreDTO(node2);
			if (dto.getStatus().getStatusCode() != RequestStatus.WB_NO_DATA)
				scores.add(dto);
		}
		this.setScaleScores(scores);
			
		if (this.getModule().getStatus().getStatusCode() == RequestStatus.WB_NO_DATA &&
			this.getScaleScores().size() == 0)
		{
			this.getStatus().setStatusCode(RequestStatus.WB_NO_DATA);
		}
	}

	//
	// Instance methods.
	//
	
	/**
	 * Generic toString method
	 */
	public String toString()
	{
		String str = "\n --- ResultDTO --- \n";
		str += "\nID:               " + _id;
		str += _module.toString();
		for(ScaleScoreDTO scale : _scaleScores)
		{
			str += scale.toString();
		}
		for(RawScoreDTO rawScale : _rawScores)
		{
			str += rawScale.toString();
		}
		
		return str;
	}	

	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
	
	/*****************************************************************************************/
	public void setId(String value)
	{
		_id = value;
	}
	
	public String getId()
	{
		return _id;
	}
	
	/*****************************************************************************************/
	public void setModule(ModuleDTO value)
	{
		_module = value;
	}
	
	public ModuleDTO getModule()
	{
		return _module;
	}
	
	/*****************************************************************************************/
	public void setScaleScores(ArrayList<ScaleScoreDTO> value)
	{
		_scaleScores = value;
	}
	
	public ArrayList<ScaleScoreDTO> getScaleScores()
	{
		return _scaleScores;
	}
	
	/*****************************************************************************************/
	public void setRawScores(ArrayList<RawScoreDTO> value)
	{
		_rawScores = value;
	}
	
	public ArrayList<RawScoreDTO> getRawScores()
	{
		return _rawScores;
	}

	public void setProduct(ProductDTO _product) {
		this._product = _product;
	}

	public ProductDTO getProduct() {
		return _product;
	}

}
