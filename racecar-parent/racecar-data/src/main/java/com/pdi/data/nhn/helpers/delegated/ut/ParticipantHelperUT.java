/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.data.nhn.helpers.delegated.ut;

import com.pdi.data.helpers.interfaces.IParticipantHelper;

import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.nhn.util.NhnDatabaseUtils;
import com.pdi.data.dto.NoteHolder;
import com.pdi.data.dto.SessionUser;
import com.pdi.ut.UnitTestUtils;
import junit.framework.TestCase;

public class ParticipantHelperUT extends TestCase {

	public ParticipantHelperUT(String name) 	{
		super(name);
	}
	
	
	/*
	 * 
	 */
//	public void testGetParticipant() throws Exception {
//		UnitTestUtils.start(this);
//		NhnDatabaseUtils.setUnitTest(true);
//		
//		SessionUser session = new SessionUser();
//		
//		IParticipantHelper iparticipant = HelperDelegate.getParticipantHelper("com.pdi.data.nhn.helpers.delegated");
//
//		//String participantId = "51081";
//		//String participantId = "104156";         
//		String participantId = "370369";	// Cert - w/note
//		//String participantId = "365122";	// Cert - no note
//		Participant singleUser = iparticipant.fromId(session, participantId);
//		
//		try{
//			System.out.println("singleUser.getId = " + singleUser.getId());
//			System.out.println("singleUser.getFirstName = " + singleUser.getFirstName());
//			System.out.println("singleUser.getLastName = " + singleUser.getLastName());
//			System.out.println("singleUser.getEmail = " + singleUser.getEmail());
////			System.out.println("singleUser.getBusinessUnit = " + singleUser.getMetadata().get("OPTIONAL_1"));	
////			System.out.println("singleUser.getJobTitle = " + singleUser.getMetadata().get("OPTIONAL_2"));
//			System.out.println("singleUser.getMetadata = " + singleUser.getMetadata().toString());
//		}catch (Exception e) {
//			System.out.println("participantId " + participantId + " did not return a valid participant.");
//		}
//
//		String engagementId = "461";	// Cert
//		ArrayList<Participant> participants = iparticipant.fromProjectId(null, engagementId);
//		try {
//			StringBuffer ps = new StringBuffer();
//			int numParticipants = participants.size();
//			ps.append("Project " + engagementId + " selection was successful! ");
//			ps.append("Its participants are: ");
//			for (Participant p : participants){
//				ps.append(p.getId() + ",");
//			}
//			System.out.println(ps + " " + numParticipants + " users total.");
//		}catch (Exception e) {
//			System.out.println("no engagement/project found for id " + engagementId + ".");
//		}
//
//		String clientId = "1407";	// Cert
//		ArrayList<Participant> byClient = iparticipant.fromClientId(null, clientId);
//		try {
//			StringBuffer ps = new StringBuffer();
//			int numParticipants = byClient.size();
//			ps.append("Client " + clientId + " selection was successful! ");
//			ps.append("Its participants are: ");
//			for (Participant p : byClient){
//				ps.append(p.getId() + ",");
//			}
//			System.out.println(ps + " " + numParticipants + " users total.");
//		}catch (Exception e) {
//			System.out.println("no engagement/project found for id " + clientId + ".");
//		}
//	}
	
	
	/*
	 * fromAdminId
	 */
//	public void testFromAdminId()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		NhnDatabaseUtils.setUnitTest(true);
//		
//		SessionUser session = new SessionUser();
//		
//		IParticipantHelper helper = HelperDelegate.getParticipantHelper("com.pdi.data.nhn.helpers.delegated");
//
//		//String adminId = "087255";	// Cert - type 4 - 101 users
//		//String adminId = "114626";	// Cert - type 4 - 1339 users
//		//String adminId = "317396";	// Cert - type 4 - 188 users
//		String adminId = "357690";	// Cert - type 4 - 218 users
//		ArrayList<Participant> participants = helper.fromAdminId(null, adminId);
//		try {
//			StringBuffer ps = new StringBuffer();
//			int numParticipants = participants.size();
//			ps.append("Admin (" + adminId + ") selection was successful! ");
//			ps.append("Its participants are: ");
//			for (Participant p : participants){
//				ps.append(p.getId() + ",");
//			}
//			System.out.println(ps + " " + numParticipants + " users total.");
//		}catch (Exception e) {
//			System.out.println("no user found for id " + adminId + ".");
//		}
//
//		UnitTestUtils.stop(this);
//	}


	/*
	 * find
	 */
//	public void testFind()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		NhnDatabaseUtils.setUnitTest(true);
//		
//		SessionUser session = new SessionUser();
//		String adminId;
//		String target;
//		ArrayList<Participant> ret;
//		
//		IParticipantHelper helper = HelperDelegate.getParticipantHelper("com.pdi.data.nhn.helpers.delegated");
//		
//		//adminId = "48901";	// Type 4
//		adminId = "357690";	// Type 4 - in cert
//		target = "jeff";	// something in the first name
//		ret = helper.find(session, adminId, target);
//		assertEquals("T1b:  Failed to find 2 'jeff' entries...", 2, ret.size());
//		printNames(adminId, target, ret);
//		
//		target = "dinka";		// something in the last name
//		ret = helper.find(session, adminId, target);
//		assertEquals("T1b:  Failed to find 2 'dinka' entries...", 2, ret.size());
//		printNames(adminId, target, ret);
//		
//		target = "andrea";		// something in the email address
//		ret = helper.find(session, adminId, target);
//		assertEquals("T1c:  Failed to find correct number of 'andrea' entries...", 2, ret.size());
//		printNames(adminId, target, ret);
//		
////		// Try a different admin
////		adminId = "174581";	// Type 1
////		target = "ky";	// something in the first name - 1 row
////		ret = helper.find(session, adminId, target);
////		assertEquals("T2a:  Failed to find 1 'ky' entries...", 1, ret.size());
////		printNames(adminId, target, ret);
////		
////		target = "mu";	// something in the last5 name - 2 rows
////		ret = helper.find(session, adminId, target);
////		assertEquals("T2b:  Failed to find 2 'mu' entry...", 2, ret.size());
////		printNames(adminId, target, ret);
////		
////		target = "slkp";	// something in the email address - 3 rows
////		ret = helper.find(session, adminId, target);
////		assertEquals("T2c:  Failed to find 3 'slkp' entries...", 3, ret.size());
////		printNames(adminId, target, ret);
////		
////		// Yet another adminId
////		adminId = "45983";
////		target = "Deb";	// Person with no cosubid - 0 rows
////		ret = helper.find(session, adminId, target);
////		assertEquals("T3:  Failed to find 0 entries...", 0, ret.size());
////		printNames(adminId, target, ret);
//
//		UnitTestUtils.stop(this);
//	}
//	
//	/*
//	 * Used in the find() testing
//	 */
//	private void printNames(String pId, String targ, ArrayList<Participant> ary)
//	{
//		System.out.println("For AdminID=" + pId + ", searchString='" + targ + "' (" + ary.size() + "):");
//		if (ary.size() == 0)
//		{
//			System.out.println("   No data returned");
//
//		}
//		else
//		{
//			for (Participant pp : ary)
//			{
//				System.out.println("   " + pp.getLastName() + ", " + pp.getFirstName());
//			}
//		}
//	}


	/*
	 * 
	 */
	public void testGetNotes()
		throws Exception
	{
	UnitTestUtils.start(this);
	NhnDatabaseUtils.setUnitTest(true);
	
	SessionUser session = new SessionUser();
	
	IParticipantHelper iparticipant = HelperDelegate.getParticipantHelper("com.pdi.data.nhn.helpers.delegated");

	String participantId = "370369";	// Cert - w/note
	//String participantId = "365122";	// Cert - no note
	NoteHolder nh = iparticipant.getNotes(session, participantId);
	System.out.println(nh);

		UnitTestUtils.stop(this);
	}

}
