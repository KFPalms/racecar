package com.pdi.data.abyd.helpers.setup.NoLongerUsed.ut;

import java.sql.Connection;
import java.util.ArrayList;

import com.pdi.data.abyd.dto.common.KeyValuePair;
import com.pdi.data.abyd.helpers.setup.NoLongerUsed.SaveSPTextHelper;
import com.pdi.ut.UnitTestUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;

import junit.framework.TestCase;

public class SaveSPTextHelperUT extends TestCase
{
	//
	// Constructors
	//

	public SaveSPTextHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
		throws Exception
    {
		junit.textui.TestRunner.run(SaveSPTextHelperUT.class);
    }


	/*
	 * testSaveSPText
	 */
	public void testSaveSPText()
		throws Exception
	{
		System.out.println("SaveSPTextHelperUT:");
		
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);

		Connection con = AbyDDatabaseUtils.getDBConnection();
		long dnaId = 41;
		ArrayList<KeyValuePair> ary = new ArrayList<KeyValuePair>();
    	for (int i = 0; i < 7; i++)
    	{
    		KeyValuePair kv = new KeyValuePair();
    		kv.setTheKey(i + 1);
    		kv.setTheValue("Helper UT test text for item type " + (i + 1));
    		ary.add(kv);
    	}
		
		System.out.println("    DNA ID=" + dnaId);

    	SaveSPTextHelper helper = new SaveSPTextHelper(con, dnaId, ary);
		helper.saveSPTexts();

		System.out.println("    No data returned.");

		UnitTestUtils.stop(this);
	}
}
