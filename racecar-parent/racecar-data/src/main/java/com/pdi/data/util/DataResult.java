/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.util;

//import java.sql.Connection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
//import java.sql.SQLException;
import java.sql.Statement;


/**
 * DataResult is a thin class that transports status data, live data, and housekeeping
 * information from the common database queries to the callers in the Data Layer.  It is
 * meant to transport data only within the Data Layer.  Communication to the Business
 * Layer will be done using a different base class.
 *
 * @author		Ken Beukelman
 */

// CAUTION:	If you close this object, you close the Statement and Connection that are associated
//			with the DataLayerPreparedStatement that was used to create this object.
// NOTE:	Gavin designed (and I implemented) this class to hold all of the information about the
//			transaction,  That means that when we close this class, we close the ResultSet, the
//			Statement, and the connection.  In retrospect, we probably should have divided the
//			resposibility as the DataLayerPreparedStatement also holds the statement and at times 
//			feels to be the more logical place to do the close of the statement and the connection.
//
public class DataResult
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//

	// Status info
	private RequestStatus _status = new RequestStatus();
	
	// Note that not all of the following fields are used in all instances
	
	// Results information...
	private int _rowCount = 0; 		// used in update and delete queries
	private long _newId = 0; 	// used in insert queries with auto_increment keys

	// Database information... used in select queries
	private ResultSet _resultSet = null;

	// housekeeping info... 
	private Statement _statement = null;
	private PreparedStatement _preparedStatement = null;

	//
	// Constructors.
	//
	public DataResult()
	{
		// Nothing done on a no-parameter constructor
	}
	
	
	/*
	 * Constructors
	 */
	public DataResult(RequestStatus stat)
	{
		this.setStatus(stat);
	}
	
	
	// I do not believe this constructor is used
	public DataResult(Statement s, ResultSet r, PreparedStatement p)
	{
		this.setStatement(s);
		this.setPreparedStatement(p);
		this.setResultSet(r);
	}

	//
	// Instance methods.
	//

	/*
	 * Convenience class to check if there was data fetched
	 */
	public boolean hasNoData()
	{
		// hasNoData is true when there is no data - in that sense it is the opposite of checking for data
		return (this.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA ? true : false);
	}

	/*
	 * Convenience class to check the status of the DataResult
	 */
	public boolean isInError()
	{
		return (this.getStatus().getStatusCode() == RequestStatus.RS_OK ? false : true);
	}
	
	//////////////////////////////////////
	//        Setters and Getters       //
	//////////////////////////////////////
	
	/********************************************************/
	public void setStatus(RequestStatus value)
	{
		_status = value;
	}

	public RequestStatus getStatus()
	{
		return _status;
	}
	
	/********************************************************/
	public void setRowCount(int value)
	{
		this._rowCount = value;
	}

	public int getRowCount()
	{
		return this._rowCount;
	}
	
	/********************************************************/
	public void setNewId(long value)
	{
		this._newId = value;
	}

	public long getNewId()
	{
		return this._newId;
	}
	
	/********************************************************/
	public void setResultSet(ResultSet value)
	{
		this._resultSet = value;
	}

	public ResultSet getResultSet() {
		return this._resultSet;
	}
	
	/********************************************************/
	public void setStatement(Statement value)
	{
		this._statement = value;
	}

	public Statement getStatement()
	{
		return this._statement;
	}

	/********************************************************/
	public void setPreparedStatement(PreparedStatement value)
	{
		this._preparedStatement = value;
	}

	public PreparedStatement getPreparedStatement()
	{
		return this._preparedStatement;
	}

	
	//////////////////////////////////////
	//   Additional instance functions  //
	//////////////////////////////////////
	
	/**
	 * Close the closable elements in this data result object
	 * @return void
	 */
	public void close()
	{
		if (this._resultSet != null)
		{
			try  { 
				this._resultSet.close();  
				}
			catch (Exception e)
			{
				System.out.println("Error closing DataResult ResultSet:  " +e.getMessage());
			}
			this._resultSet = null;
		}
		
		Connection myCon = null;
		if (this._statement != null)
		{
			try  { 
				if (this._statement.getConnection() != null)
					myCon = this._statement.getConnection();
				this._statement.close();  
				if (myCon != null)
					myCon.close();
			}
			catch (Exception e)
			{
				System.out.println("Error closing DataResult Statement:  " +e.getMessage());
			}
			this._statement = null;
		}
		
		myCon = null;
		if (this._preparedStatement != null)
		{
			try  { 
				if (this._preparedStatement.getConnection() != null)
					myCon = this._preparedStatement.getConnection();
				this._preparedStatement.close();
				if (myCon != null)
					myCon.close();
				}
			catch (Exception e)
			{
				System.out.println("Error closing DataResult PreparedStatement and Connection:  " + e.getMessage());
			}
			this._preparedStatement = null;
		}
		
	}

	
	/**
	 * 	DataResult finalize - Try to close the closeable items if the programmer did not
	 */
	public void finalize()
	{
	    try
	    {
	        this.close();        // close open files
	    }
	    finally
	    {
	    	try
	    	{
		        super.finalize();
	    	}
	    	catch (Throwable ex)
			{
				System.out.println("Error finalizing DataResult:  " +ex.getMessage());
			}
	    }
	}


	public String toString()
	{
		return "DataResult class:  ";
	}


}
