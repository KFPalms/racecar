/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.dto;

import java.util.ArrayList;

import com.pdi.data.util.BaseDTO;
import com.pdi.data.util.RequestStatus;

/**
 * DTO for a list of client information objects
 *
 * @author		Ken Beukelman
 */
public class ClientListDTO extends BaseDTO
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private ArrayList<ClientDTO> _clientList = new ArrayList<ClientDTO>();


	//
	// Constructors.
	//

 	public ClientListDTO()
	{
 		super();
		// does nothing right now
	}
	
	// Set up a new DTO with status but no data
	public ClientListDTO(RequestStatus stat)
	{
		super(stat);
	}
	
	// Set up a new DTO with data (assumes no data problems)
	// It does check for data
	public ClientListDTO(ArrayList<ClientDTO> ary)
	{
		super();
		this.setClientList(ary);
		if (ary.size() < 1)
		{
			this.getStatus().setStatusCode(RequestStatus.WB_NO_DATA);
		}
	}
	
	
	//
	// Instance methods.
	//

	/**
	 * Generic toString method
	 */
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append("ClientListDTO:  ");
		sb.append("Status code=" + this.getStatus().getStatusCode() + ", count=" + _clientList.size());
		for(int i=0; i < _clientList.size(); i++)
		{
			_clientList.get(i).toString();
		}
		return sb.toString();
	}
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public ArrayList<ClientDTO> getClientList()
	{
		return _clientList;
	}
	
	public void setClientList(ArrayList<ClientDTO> value)
	{
		_clientList = value;
	}	
}
