/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.evalGuide;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;

import com.pdi.data.abyd.dto.evalGuide.EGIrDataDTO;
import com.pdi.data.abyd.dto.evalGuide.EGIrScoreData;


/**
 * SaveEGIrScoreDataHelper contains the code needed to save the Impact
 * Rating score data as found on the last data page of the Eval Guide.
 *
 * @author		Ken Beukelman
 */
public class SaveEGIrDataHelper
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private Connection con;
	private String participantId;
	private long dnaId;
	private long moduleId;
	private String irNotes;			// IR notes text
	private ArrayList<EGIrScoreData> irArray;

	//
	//
	// Constructors.
	//
	public SaveEGIrDataHelper(Connection con, EGIrDataDTO irScoreData)
		throws Exception
	{
		// Check key data
		if (irScoreData.getParticipantId() == null || irScoreData.getParticipantId().length() == 0  ||
				irScoreData.getDnaId()	== 0 ||
				irScoreData.getModuleId() == 0)
		{
			throw new Exception("Invalid IR Data key:  part=" + (irScoreData.getParticipantId() == null ? "null" : (irScoreData.getParticipantId().length() == 0 ? "<empty string>" : irScoreData.getParticipantId())) +
					            ", dna=" + irScoreData.getDnaId() +
					            ", module=" + irScoreData.getModuleId());
		}

		this.con = con;
		this.participantId = irScoreData.getParticipantId();
		this.dnaId = irScoreData.getDnaId();
		this.moduleId = irScoreData.getModuleId();
		this.irNotes = irScoreData.getIrNotes();
		this.irArray = irScoreData.getIrArray();
	}

	//
	// Instance methods.
	//

	/**
	 * Update the IR notes field in the EG Response table and the scores
	 * in the eg_ir_response table.  Returns nothing.
	 *
	 * @throws Exception
	 */
	public void saveIrData()
		throws Exception
	{
		// First do the notes...
		saveIrNote();
		
		// ...then do the scores
		saveIrScores();
		
		return;
	}

	
	/**
	 * Update the IR notes field.
	 *
	 * @throws Exception
	 */
	private void saveIrNote()
		throws Exception
	{
		// Change any double quotes to single quotes
		// This prevents "Bad SQL" messages for strings that include double quotes
		if (this.irNotes != null)
			this.irNotes = this.irNotes.trim().replace("'","''");

		Statement stmt = null;
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("UPDATE pdi_abd_eg_response ");
		if (this.irNotes == null || this.irNotes.length() < 1)
		{
			sqlQuery.append("  SET irNotes = NULL, ");
		}  else  {
			sqlQuery.append("  SET irNotes = N'" + this.irNotes + "', ");
		}
		sqlQuery.append("  	   lastUserId = SYSTEM_USER, ");
		sqlQuery.append("  	   lastDate = GETDATE() ");
		sqlQuery.append("  WHERE participantId = " + this.participantId + " ");
		sqlQuery.append("    AND dnaId = " + this.dnaId + " ");
		sqlQuery.append("    AND moduleId = " + this.moduleId);

		try
		{
			stmt = this.con.createStatement();
			stmt.executeUpdate(sqlQuery.toString());

			return;
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL saveIrNote.  " +
        						"SQLException: " + ex.getMessage() + ", " +
        						"SQLState: " + ex.getSQLState() + ", " +
        						"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (stmt != null)
			{
				try  {  stmt.close();  }
				catch (SQLException sqlEx)  {  /* swallow the error */  }
				stmt = null;
			}
		}
	}

	
	/**
	 * Update/Insert the IR scores.
	 *
	 * @throws Exception
	 */
	private void saveIrScores()
		throws Exception
	{
		// Get the existing sequence numbers
		ArrayList<Integer> seqList = getSeqList();
		
		// loop through the scores passed in
		for(Iterator<EGIrScoreData> itr = this.irArray.iterator(); itr.hasNext(); )
		{
			EGIrScoreData cur = itr.next();
			Integer chkIt = new Integer(cur.getIrSeq());
			if(seqList.contains(chkIt))
			{
				// The sequence exists, update the score row.
				updateIrScore(cur);
			}
			else
			{
				// The sequence does not exist, insert a new score row.
				insertIrScore(cur);
			}
		}
	}
	
	
	/**
	 * getSeqList returns an ArrayList of Integers containing the IR
	 * sequence ids of IR scores that are already in the database.
	 *
	 * @return the ArrayList
	 * @throws Exception
	 */
	private ArrayList<Integer> getSeqList()
		throws Exception
	{
		ArrayList<Integer> results = new ArrayList<Integer>();
		
		Statement stmt = null;
		ResultSet rs = null;
		
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT irr.irSequence ");
		sqlQuery.append("  FROM pdi_abd_eg_ir_response irr ");
		sqlQuery.append("  WHERE irr.participantId = '" + this.participantId + "' ");
		sqlQuery.append("    AND irr.dnaId = " + this.dnaId + " ");
		sqlQuery.append("    AND irr.moduleId = " + this.moduleId);

		try
		{
	       	stmt = this.con.createStatement();
	       	rs = stmt.executeQuery(sqlQuery.toString());
	       	
	       	while (rs.next())
	       	{
	       		results.add(new Integer(rs.getInt("irSequence")));
	       	}

	       	return results;
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL getSeqList.  " +
        			"SQLException: " + ex.getMessage() + ", " +
        			"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
		finally
		{
        	if (rs != null)
        	{
        		try {  rs.close();  }
        		catch (SQLException sqlEx)	{  /* swallow the error */  }
        		rs = null;
        	}
        	if (stmt != null)
        	{
        		try	{  stmt.close();  }
        		catch (SQLException sqlEx)	{  /* swallow the error */  }
        		stmt = null;
        	}
		}
	}

	
	/**
	 * Insert an IR score.
	 *
	 * @param irScoreData a EGIrScoreData object
	 * @throws Exception
	 */
	private void insertIrScore(EGIrScoreData irScoreData)
			throws Exception
	{
		Statement stmt = null;

		// Set up the insert
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("INSERT INTO pdi_abd_eg_ir_response ");
		sqlQuery.append("  (participantId, ");
		sqlQuery.append("   dnaId, ");
		sqlQuery.append("   moduleId, ");
		sqlQuery.append("   irSequence, ");
		sqlQuery.append("   irScore, ");
		sqlQuery.append("   lastUserId, ");
		sqlQuery.append("   lastDate) ");
		sqlQuery.append("  VALUES(" + this.participantId + ", "); 
		sqlQuery.append("         " + this.dnaId + ", "); 
		sqlQuery.append("         " + this.moduleId + ", ");
		sqlQuery.append("         " + irScoreData.getIrSeq() + ", ");
		sqlQuery.append("         " + irScoreData.getIrScore() + ", ");
		sqlQuery.append("         SYSTEM_USER," );
		sqlQuery.append("         GETDATE())");

		try
		{
	       	stmt = this.con.createStatement();
	       	stmt.executeUpdate(sqlQuery.toString());
		}
        catch (SQLException ex)
		{
            // handle any errors
        	throw new Exception("SQL insertIrScore.  " +
        						"SQLException: " + ex.getMessage() + ", " +
        						"SQLState: " + ex.getSQLState() + ", " +
        						"VendorError: " + ex.getErrorCode());
		}
        finally
		{
        	if (stmt != null)
        	{
        		try  {  stmt.close();  }
        		catch (SQLException sqlEx)  { /* Swallow the error */}
        		stmt = null;
        	}
		}
	}
	


	/**
	 * Update an IR score.
	 *
	 * @param irScoreData an EGIrScoreData object
	 * @throws Exception
	 */
	private void updateIrScore(EGIrScoreData irScoreData)
		throws Exception
	{
		Statement stmt = null;
		
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("UPDATE pdi_abd_eg_ir_response ");
		sqlQuery.append("  SET irScore = " + irScoreData.getIrScore() + ", ");
		sqlQuery.append("      lastUserId = SYSTEM_USER, ");
		sqlQuery.append("      lastDate = GETDATE() ");
		sqlQuery.append("  WHERE participantId = '" + this.participantId + "' ");
		sqlQuery.append("    AND dnaId = " + this.dnaId + " ");
		sqlQuery.append("    AND moduleId = " + this.moduleId + " ");
		sqlQuery.append("    AND irSequence = " + irScoreData.getIrSeq());		
		
		try
		{
			stmt = this.con.createStatement();
			stmt.executeUpdate(sqlQuery.toString());
		}
		catch (SQLException ex)
		{
			// handle any errors
        	throw new Exception("SQL updateIrScore.  " +
        			"SQLException: " + ex.getMessage() + ", " +
        			"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (stmt != null)
			{
				try  {  stmt.close();  }
				catch (SQLException sqlEx)  {  /* Swallow the error */  }
				stmt = null;
			}
		}
	}
}
