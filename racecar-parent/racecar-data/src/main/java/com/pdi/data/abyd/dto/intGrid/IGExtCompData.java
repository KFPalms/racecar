/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.dto.intGrid;

/**
 * IGExtCogData is a thin class to hold certain information about
 * a single competency for the Integration Grid Extract.
 * 
 * @author		Ken Beukelman
 */
public class IGExtCompData
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _compName;
	private int _compFinalScoreIdx;
	private boolean _compEssential;

	//
	// Constructors.
	//
	public IGExtCompData()
	{
		// does nothing right now
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public String getCompName()
	{
		return _compName;
	}
	
	public void setCompName(String value)
	{
		_compName = value;
	}	

	/*****************************************************************************************/
	public int getCompFinalScoreIdx()
	{
		return _compFinalScoreIdx;
	}
	
	public void setCompFinalScoreIdx(int value)
	{
		_compFinalScoreIdx = value;
	}	

	/*****************************************************************************************/
	public boolean getCompEssential()
	{
		return _compEssential;
	}
	
	public void setCompEssential(boolean value)
	{
		_compEssential = value;
	}	



	/*
	 * Show me what you've got
	 */
	public String toString()
	{
		String ret = "IGExtCompData content (Java)\n";
		ret += "    Competency Name=" + _compName + "\n";
		ret += "  Final Score Index=" + _compFinalScoreIdx + "\n";
		//ret += "       is Essential=" + _compEssential + "\n";
		
		return ret;
	}
}
