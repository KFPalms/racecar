	/*
	 * Copyright (c) 2009 Personnel Decisions International, Inc.
	 * All rights reserved.
	 */
package com.pdi.data.abyd.helpers.setup.ut;

import java.sql.Connection;

import com.pdi.data.abyd.dto.setup.DNADescription;
import com.pdi.data.abyd.helpers.setup.SaveDNADescHelper;
import com.pdi.scoring.Norm;
import com.pdi.ut.UnitTestUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.data.nhn.util.NhnDatabaseUtils;

import junit.framework.TestCase;

public class SaveDNADescHelperUT extends TestCase
{
	//
	// Constructors
	//

	public SaveDNADescHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
		throws Exception
    {
		junit.textui.TestRunner.run(SaveDNADescHelperUT.class);
    }


	/*
	 * testSaveNameModelCmgrInsert
	 * NOTE:  This tests the insert code.  To test the insert we need to 
	 * 		  put in a DTO with an id of 0.
	 */
//	public void testSaveNameModelCmgrInsert()
//		throws Exception
//	{
//		System.out.println("DNADescHelperUT:");
//		
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//
//		Connection con = AbyDDatabaseUtils.getDBConnection();
//		DNADescription inp = new DNADescription();
//		inp.setDnaId(0);
//		inp.setDnaName("DNA for project 60");
//		inp.setJobId("60");
//		inp.setModelId(1);
//		inp.setcMgrName("Joseph P. Blough - HUT1");
//		inp.setcMgrEmail("blough-hard-HUT1@bogus.com");
//			
//		System.out.println("    DNA ID=" + inp.getDnaId());
//
//		SaveDNADescHelper helper = new SaveDNADescHelper(con, inp);
//		DNADescription dto = helper.saveNameModelCmgr();
//			
//		System.out.println("    Output:");
//		System.out.println("      Returned Status=" + dto.getSuccessStatus());
//		if (dto.getStatusMsg() != null)
//		{
//			System.out.println("        Msg=" + dto.getStatusMsg());
//		}
//		System.out.println("      Returned DNA id =" + dto.getDnaId() + ", Name=" + dto.getDnaName());
//
//		UnitTestUtils.stop(this);
//	}


	/*
	 * testSaveNameModelCmgrUpdate
	 * NOTE:  This tests the update code only.  To test the insert we need to 
	 * 		  put in a DTO with an id of 0 and a unique name.
	 */
//	public void testSaveNameModelCmgrUpdate()
//		throws Exception
//	{
//		System.out.println("DNADescHelperUT:");
//		
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//
//		Connection con = AbyDDatabaseUtils.getDBConnection();
//		DNADescription inp = new DNADescription();
//		inp.setDnaId(48);
//		inp.setDnaName("Test Dup Name");
//		inp.setJobId("TSTJOBXX");
//		inp.setModelId(46);
//		inp.setcMgrName("Joseph P. Blough - HUT1");
//		inp.setcMgrEmail("blough-hard-HUT1@bogus.com");
//			
//		System.out.println("    DNA ID=" + inp.getDnaId());
//
//		SaveDNADescHelper helper = new SaveDNADescHelper(con, inp);
//		DNADescription dto = helper.saveNameModelCmgr();
//			
//		System.out.println("    Output:");
//		System.out.println("      Returned Status=" + dto.getSuccessStatus());
//		if (dto.getStatusMsg() != null)
//		{
//			System.out.println("        Msg=" + dto.getStatusMsg());
//		}
//		System.out.println("      Returned DNA id =" + dto.getDnaId() + ", Name=" + dto.getDnaName());
//
//		UnitTestUtils.stop(this);
//	}


	/*
	 * testSaveMoreDNAData
	 */
	public void testSaveMoreDNAData()
		throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);
		NhnDatabaseUtils.setUnitTest(true);

		Connection con = AbyDDatabaseUtils.getDBConnection();
		DNADescription inp = new DNADescription();
		inp.setDnaId(8);
		inp.setJobId("60");
		inp.setSynthNorm(new Norm(1.5,.5));
		inp.setWbVersion("2.2.1.1");
		inp.setDnaNotes("Notes-HUT2");
			
		//System.out.println("    2nd update...");

		SaveDNADescHelper helper = new SaveDNADescHelper(con, inp);
		helper.saveMoreDNAData();
			
		System.out.println("    Output:  Nothing returned");

		UnitTestUtils.stop(this);
	}
}
