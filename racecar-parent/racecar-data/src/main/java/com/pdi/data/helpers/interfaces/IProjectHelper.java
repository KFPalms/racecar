/**
 * Copyright (c) 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.helpers.interfaces;

import java.util.ArrayList;
//import java.util.HashMap;
import java.util.HashSet;

import com.pdi.data.dto.Participant;
import com.pdi.data.dto.Project;
import com.pdi.data.dto.ProjectParticipant;
import com.pdi.data.dto.SessionUser;

public interface IProjectHelper
{
	
	public ArrayList<Project> fromClientIdByDate(SessionUser session, String clientId) throws Exception;
	public ArrayList<Project> fromClientIdByName(SessionUser session, String clientId) throws Exception;
	public ArrayList<Project> fromOwnerIdByDate(SessionUser session, String adminId) throws Exception;
	public ArrayList<Project> fromOwnerIdByName(SessionUser session, String adminId) throws Exception;
	public ArrayList<Project> fromParticipantId(SessionUser session, String participantId)  throws Exception;
	public Project fromId(SessionUser session, String engagementId) throws Exception;
	public ProjectParticipant addParticipant(SessionUser session, Project project, String participantId);
	public boolean addParticipants(SessionUser session, Project project, ArrayList<String> participants);
	public boolean removeParticipant(SessionUser session, Project project, String participantId);
	public boolean removeParticipants(SessionUser session, Project project, ArrayList<Participant> participants);
	//public void enrollParticipant(Project project, Integer participantId);
	public ArrayList<Project> findForClientId(SessionUser session, String clientId, String target)  throws Exception;
	public ArrayList<Project> findForAdminId(SessionUser session, String adminId, String target) throws Exception;

	public ArrayList<String> getInstrumentCodesForProjectId(String projectId, String participantId) throws Exception;
	public Project createProject(SessionUser session, Project project);
	public boolean updateProject(SessionUser session, Project project);
	public boolean updateCoreProject(SessionUser session, Project project);
	public boolean updateContentItems(SessionUser session, String projectId, HashSet<String> content);
////	public boolean updateMetadataItems(SessionUser session, String projectId, HashMap<String, String> meta);
////	public boolean removeMetadataItems(SessionUser session, String projectId, HashSet<String> meta);
////	public boolean removeMetadataItems(SessionUser session, String projectId, HashMap<String, String> meta);
////	public boolean removeAllMetadataItems(SessionUser session, String projectId);
	public boolean removeContentItems(SessionUser session, String projectId, HashSet<String> content);
	public boolean removeAllContentItems(SessionUser session, String projectId);
}
