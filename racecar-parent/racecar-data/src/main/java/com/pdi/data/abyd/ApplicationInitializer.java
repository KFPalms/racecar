package com.pdi.data.abyd;

import com.pdi.data.abyd.util.AbyDDatabaseUtils;

public class ApplicationInitializer {
	private static ApplicationData application;
	
	public ApplicationInitializer() {

		System.out.println("************ ABYD DATABASE ************");
		try {
			//Hibernate
			AbyDDatabaseUtils.registerPersistence();
		} catch(Exception e) {
			e.printStackTrace();
		}

		try {
			//JDBC
			AbyDDatabaseUtils.registerConnection();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public static void setApplication(ApplicationData application) {
		ApplicationInitializer.application = application;
	}

	public static ApplicationData getApplication() {
		return application;
	}
}
