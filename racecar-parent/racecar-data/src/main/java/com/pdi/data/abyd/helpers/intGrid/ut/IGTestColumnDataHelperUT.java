package com.pdi.data.abyd.helpers.intGrid.ut;

import java.sql.Connection;

import com.pdi.data.abyd.dto.intGrid.IGInstDisplayData;
import com.pdi.data.abyd.helpers.intGrid.IGTestColumnDataHelper;
import com.pdi.ut.UnitTestUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;

import junit.framework.TestCase;

public class IGTestColumnDataHelperUT  extends TestCase
{
	//
	// Constructors
	//
	public IGTestColumnDataHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
	throws Exception
	{
		junit.textui.TestRunner.run(IGTestColumnDataHelperUT.class);
	}


	/*
	 * testGetTestingColumnData
	 */
	public void testGetTestingColumnData()
		throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);
		
		// Connect to the database
		Connection con = AbyDDatabaseUtils.getDBConnection();
		String partId = "60254";
		long dnaId = 6;

		// get the data
		IGTestColumnDataHelper helper = new IGTestColumnDataHelper(con, partId, dnaId);
		IGInstDisplayData ret = helper.getTestingColumnData();
		System.out.print(ret.toString());

		UnitTestUtils.stop(this);
	}

}
