/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.dto.intGrid;

import java.util.ArrayList;

import com.pdi.data.abyd.dto.intGrid.IGModuleData;

/**
 * IGCompDataDTO is a wrapper around a number of data items needed to display
 * the drill-down for a line of competency data when the "Edit" button
 * associated with a competency line in the Integration Grid app is pressed.
 * 
 * @author		Ken Beukelman
  */
public class IGCompDataDTO
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
		// These are "Key" data.  The UI should have them, but
		// they are are being returned as a safety net.
	private long _compId;
	private String _partId;
	private long _dnaId;
	
		// The rest of the data...
	private String _compDescription;
	private int _intScoreIdx;
	private String _intNotes;
	private ArrayList<IGModuleData> _modArray;
	private String _compDisplayName;
	private String _averageCompetencyScore;


	//
	// Constructors.
	//
	public IGCompDataDTO()
	{
		// does nothing right now
	}

	//
	// Instance methods.
	//
	
	// Calc comp for the line
	public void calcAvgCompScore()
	{
		double tot = 0.0;
		int cnt = 0;
		for(IGModuleData md : _modArray)
		{
			String str = md.getModCompScore();	// Note that this includes a look for the ext comp score (see code)
			if (str.isEmpty() || str.equals("-") || str.equals("0.00") || str.equals("-1.00"))
			{
				// Skip
				continue;
			}
			else
			{
				double score = 0.0;
				try
				{
					score = Double.parseDouble(str);
				}
				catch (NumberFormatException e)
				{
					//Skip it
					continue;
				}
				if (score > 0.0)
				{
					tot += score;
					cnt ++;
				}
			}
		}
		double avg = 0.0;
		if(cnt > 0)
		{
			avg = tot / cnt;
			avg = Math.floor((avg * 100.0) + 0.505) / 100.0;
		}
		_averageCompetencyScore = (avg == 0.0 ? "Not Available" : String.format("%.2f", avg));
		
		return;
	}
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public long getCompId()
	{
		return _compId;
	}
	
	public void setCompId(long value)
	{
		_compId = value;
	}	

	/*****************************************************************************************/
	public String getPartId()
	{
		return _partId;
	}
	
	public void setPartId(String value)
	{
		_partId = value;
	}	

	/*****************************************************************************************/
	public long getDnaId()
	{
		return _dnaId;
	}
	
	public void setDnaId(long value)
	{
		_dnaId = value;
	}	

	/*****************************************************************************************/
	public String getCompDescription()
	{
		return _compDescription;
	}
	
	public void setCompDescription(String value)
	{
		_compDescription = value;
	}	

	/*****************************************************************************************/
	public int getIntScoreIdx()
	{
		return _intScoreIdx;
	}
	
	public void setIntScoreIdx(int value)
	{
		_intScoreIdx = value;
	}	

	/*****************************************************************************************/
	public String getIntNotes()
	{
		return _intNotes;
	}
	
	public void setIntNotes(String value)
	{
		_intNotes = value;
	}	

	/*****************************************************************************************/
	public ArrayList<IGModuleData> getModArray()
	{
		return _modArray;
	}
	
	public void setModArray(ArrayList<IGModuleData> value)
	{
		_modArray = value;
	}	

	/*****************************************************************************************/
	public String getCompDisplayName()
	{
		return _compDisplayName;
	}
	
	public void setCompDisplayName(String value)
	{
		_compDisplayName = value;
	}

	/*****************************************************************************************/
	public String getAverageCompetencyScore()
	{
		return _averageCompetencyScore;
	}
	
	public void setAverageCompetencyScore(String value)
	{
		_averageCompetencyScore = value;
	}

	/*
	 * Show me what you've got
	 */
	public String toString()
	{
		String ret = "IGCompDataDTO content (Java)\n";
		ret += "           Competency ID=" + _compId + "\n";
		ret += "          Participant ID=" + _partId + "\n";
		ret += "                  DNA ID=" + _dnaId + "\n";
		ret += "  Competency Description=" + _compDescription + "\n";
		ret += "        Int. Score Index=" + _intScoreIdx + "\n";
		ret += "              Int. Notes=" + _intNotes + "\n";
		ret += "            Module Array:\n";
		if (_modArray != null)
		{
			for(IGModuleData mod : _modArray) 
			{
				ret += mod.toString();
			}
		}
		ret += "   Avg. Competency Score=" + _averageCompetencyScore + "\n";

		return ret;
	}
}
