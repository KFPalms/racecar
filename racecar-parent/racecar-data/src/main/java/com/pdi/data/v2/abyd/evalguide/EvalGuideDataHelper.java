/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v2.abyd.evalguide;

import java.sql.SQLException;

import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.v2.abyd.evalguide.dto.EvalGuideDTO;
import com.pdi.data.v2.util.V2DatabaseUtils;

/*
 * Helper class to retrieve Eval Guides
 *
 * 	NOTE:  This class is only partially implemented.  Additional
 *         work must be done to make this a fully functional class
 */
public class EvalGuideDataHelper {
	//
	// Static data.
	//

	//
	//
	// Static methods.
	//

	/**
	 * Return an eval guide from the database
	 * 
	 * @param lpId
	 *            the Link Param ID
	 * @return an EvalGuideDTO object
	 */
	public static EvalGuideDTO fetchEvalGuide(String lpid) {

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT egr.egSubmitted, ");
		sqlQuery.append("       egr.raterFirstName, ");
		sqlQuery.append("       egr.raterLastName, ");
		sqlQuery.append("       egr.irNotes, ");
		sqlQuery.append("       egr.opr, ");
		sqlQuery.append("       egr.briefDesc,");
		sqlQuery.append("       egr.keyStrengths, ");
		sqlQuery.append("       egr.keyNeeds, ");
		sqlQuery.append("       egr.coachNotes, ");
		sqlQuery.append("       egr.themesFromPeers, ");
		sqlQuery.append("       egr.themesFromDirectReports, ");
		sqlQuery.append("       egr.themesFromBoss ");
		sqlQuery.append("  FROM pdi_abd_eg_response egr ");
		sqlQuery.append("  WHERE egr.participantId = ? ");
		sqlQuery.append("    AND egr.dnaId = ? ");
		sqlQuery.append(" AND egr.moduleId = ? ");

		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError()) {
			return null;
		}

		DataResult dr = null;

		try {
			// Fill sql with values
			dlps.getPreparedStatement().setString(1, "ABC");
			dlps.getPreparedStatement().setString(2, "2");
			dlps.getPreparedStatement().setString(3, "2");

			dr = V2DatabaseUtils.select(dlps);

			// dr.getResultSet();
			// do stuff with the query here
			// dr.close();

			return new EvalGuideDTO();
		} catch (SQLException ex) {
			// Do something real here. Be sure you pass the status back
			return null;
		} finally {
			if (dr != null) {
				dr.close();
				dr = null;
			}
		}
	}
	////
	//// public static void fetchException() throws Exception {
	//// throw new Exception("You asked for it!");
	//// }

	//
	// Instance data.
	//

	//
	// Constructors.
	//

	//
	// Instance methods.
	//

}
