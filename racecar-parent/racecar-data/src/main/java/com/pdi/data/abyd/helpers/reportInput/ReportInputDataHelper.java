/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.reportInput;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.pdi.data.abyd.dto.reportInput.ReportInputDTO;
import com.pdi.data.abyd.helpers.intGrid.IGAlrDataHelper;
import com.pdi.data.abyd.helpers.intGrid.IGKf4dDataHelper;
import com.pdi.data.abyd.util.data.DataUtils;
import com.pdi.data.abyd.util.email.EmailMessage;
import com.pdi.data.abyd.util.email.EmailRequestService;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.util.RequestStatus;
import com.pdi.data.util.language.DefaultLanguageHelper;
import com.pdi.data.v2.helpers.ParticipantDataHelper;
import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.properties.PropertyLoader;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.GroupReport;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;
import com.pdi.reporting.request.ReportingRequest;
import com.pdi.reporting.response.ReportingResponse;
import com.pdi.scoring.Norm;
import com.pdi.scoring.NormSet;
import com.pdi.webservice.NhnWebserviceUtils;
import com.pdi.xml.XMLUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.data.dto.Note;
import com.pdi.data.dto.NoteHolder;
import com.pdi.data.dto.Participant;
import com.pdi.data.dto.Project;
import com.pdi.data.dto.ProjectParticipantInstrument;
import com.pdi.data.dto.Score;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.dto.TextGroup;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.IParticipantHelper;
import com.pdi.data.helpers.interfaces.IProjectHelper;
import com.pdi.data.helpers.interfaces.IProjectParticipantInstrumentHelper;
import com.pdi.data.helpers.interfaces.ITextHelper;


/**
 * ReportInputDataHelper contains the code needed to support the report input
 * functionality invoked off of the Integration Grid.
 *
 * @author		Keith Yamry
 * @author		Gavin Myers
 */
public class ReportInputDataHelper
{
	//
	//  Static data.
	//
	
	// Integration grid path suffix
	//private static String LP_SUFFIX = "/pdi-web/Flex/AbyDIntegrationGrid.jsp?lpUIS=";
	
	// Johnson & Johnson Product code (used to trigger non-alr Fit & Readiness calcs
	private static String JnJ_CODE = "JNJ";
	
	// Stolen dirctly out of portalServletLockHelper
	private static String RATINGS = "RR";
	private static String ORG_TEXT = "OT";
	private static String PART_TEXT = "PT";


	//  constants for Leadership
	private static int WEAK = 1;
	private static int MIXED = 2;
	private static int STRONG = 3;
	private static int VERY_STRONG = 4;

	//  constants for Derailment Risk
	private static int MINIMAL = 4;
	private static int LOW = 3;
	private static int MODERATE = 2;
	private static int HIGH = 1;

	// Used multiple places (fit & readiness)
	private static int INSUF_DATA = 0;

	//  constants for Fit Recommendation
	private static int NOT_RECOM = 1;
	private static int RECOM_RESERV = 2;
	private static int RECOM = 3;
	private static int STRONG_RECOM = 4;
	
	// constants for ALR Readiness
	private static int ORANGE = 1;
	private static int YELLOW = 2;
	private static int GREEN = 3;
	private static int BLUE = 4;
	
	// Constants for e-mail text substitutions
	private static String ORG_FACING_TEXT = "Organization-facing";
	private static String PPT_FACING_TEXT = "Participant-facing";

	// Text group to use
	private static final String textGroupCode = "ABYD_MISC";

//	// List of ALR (Readiness Assessment, actually) model names (INTERNAL/not display name)
//	// Tried using ALR as a prefix and that worked until Novartis GPH came along
//	// This list includes both Eval Guide style and Auto style scoring
//	// If adding a name here also add it to the project list logic in EntryStateHelper but note that THAT uses external name
//	// TODO Refactor to use database flag (to be added)
//	private static Set<String> ALR_MODELS_INT = new HashSet<String>();
//	static {
//		ALR_MODELS_INT.add("ALR BUL");
//		ALR_MODELS_INT.add("ALR BUL 26 STD");
//		ALR_MODELS_INT.add("ALR BUL 26 STD Auto");
//		ALR_MODELS_INT.add("ALR BUL 26 STD Auto v2");
//		ALR_MODELS_INT.add("ALR SEA 24 STD");
//		ALR_MODELS_INT.add("ALR SEA 24 STD Auto");
//		ALR_MODELS_INT.add("CEO");
//		ALR_MODELS_INT.add("Novartis SMDP Center");
//		ALR_MODELS_INT.add("Novartis SMDP Center v2");
//		ALR_MODELS_INT.add("Novartis SMDP V3");
//		ALR_MODELS_INT.add("Novartis GPH");
//		ALR_MODELS_INT.add("Novartis AzaamTek");
//		ALR_MODELS_INT.add("KFLA MLL Zenio");
//		ALR_MODELS_INT.add("KFLA MLL Zenio 1.0");
//		ALR_MODELS_INT.add("Ericsson EEA/EAC ALR 2017");
//		ALR_MODELS_INT.add("J&J SEA (KF4D)");
//		ALR_MODELS_INT.add("J&J BUL (KF4D)");
//		//ALR_MODELS_INT.add("J&J BUL (KF4D)");
//		ALR_MODELS_INT.add("MLL-KF4D-Manual");
//		ALR_MODELS_INT.add("BUL-KF4D-Manual");
//		ALR_MODELS_INT.add("SEA-KF4D-Manual");
//		ALR_MODELS_INT.add("CEO-KF4D-Manual");
//		ALR_MODELS_INT.add("SEA-KF4D-Auto");
//		ALR_MODELS_INT.add("BUL-KF4D-Auto");
//		ALR_MODELS_INT.add("CEO-KF4D-Auto");
//		ALR_MODELS_INT.add("CEO-KF4D-Auto-V2");
//		//ALR_MODELS_INT.add("CapGem VP");
//		//ALR_MODELS_INT.add("CapGem EVP");
//		ALR_MODELS_INT.add("TolMed");
//		ALR_MODELS_INT.add("MLL-KF4D-Auto");
//		ALR_MODELS_INT.add("SEA-KF4D-Auto-360");
//	}
	
	// For the Dormant generator logic
	private static boolean debug = false;

	
	//
	// Instance data.
	//
	private ReportInputDTO _ri;	// The ReportInputDTO object being created
	private Norm _synthNorm;	// The synthetic norm set up in Setup1 (used for dLCI)
	private String intModelName;	// Internal Model name
	private int _mdlTransLevel;	// The transition level associated with the DNA model

	private int _dlci = 0;		// Holder for calculated dLCI
	private double _dlciZ = 0.0;	// Holder for calculated dLCI
	private int _curDerailStanine = 0;
	private boolean _curLeInterest;
	private boolean _nxtLeInterest;
	private boolean _curLeExperience;
	private boolean _nxtLeExperience;
	private boolean _curLeFoundation;
	private boolean _nxtLeFoundation;

	private boolean _errorDetectedNxt = false;	// used for internal processing
	private boolean _doNxtLvl = true;	// not at SEA... get next level stuff

	// Assume instruments are missing until proven otherwise
	private boolean _gpiMissing = true;
	private boolean _leiMissing = true;
	private boolean _csMissing = true;
	private boolean _rvMissing = true;
	
	private TextGroup tg = null;
	
	// Dormant DRI stuff
	private int eCnt = 0;
	private int doneCnt = 0;


	//
	// Instance methods.
	//
	//

	/**
	 * Fetch and calculate all of the externally visible report input data
	 *
	 * @param participantId - A V2 participant ID
	 * @param danId - A DNA ID.  If the participant is not part of the
	 *                project in the DNA, no data will be returned.
	 * @throws Exception
	 */
	public ReportInputDTO getReportInput(String participantId, long dnaId)
		throws Exception
	{
		return getReportInput(participantId, dnaId, DefaultLanguageHelper.fetchDefaultLanguageCode());
	}


	/**
	 * Convenience method. Adds a preview value of false
	 * @param participantId
	 * @param dnaId
	 * @param langCode
	 * @return
	 * @throws Exception
	 */
	public ReportInputDTO getReportInput(String participantId, long dnaId, String langCode)
		throws Exception
	{
		return getReportInput(participantId, dnaId, langCode, false);
	}


	/**
	 * Fetch and calculate all of the externally visible report input data
	 *
	 * @param participantId - A V2 participant ID
	 * @param danId - A DNA ID.  If the participant is not part of the
	 *                project in the DNA, no data will be returned.
	 * @param langCode - The language code to get text in.  Note that this is the
	 *                   language code string and not the numeric language ID.
	 * @param isPreview
	 * @throws Exception
	 */
	public ReportInputDTO getReportInput(String participantId, long dnaId, String langCode, boolean isPreview)
		throws Exception
	{
		_ri = new ReportInputDTO();
		_ri.setDnaId(dnaId);
		_ri.setParticipantId(participantId);

		// Get the labels that could be used later
		getTextLabels(langCode);

		// Get the ReportInput strings and such
		readRiData();
		
		// Get the dna info
		fetchDNAData();
		//System.out.println("DEBUG:  trans level = " + _mdlTransLevel);

		// Get other info used for display (names and dates)
		fetchMiscData();

		// Get the custom report flag
		fetchCdrFlag();

		// Get the dLCI
		calcDlci();

		// Get the TLT group info
		calcTLT();

		// Derive the recommended values
		deriveRecommendedValues(langCode, isPreview);

		// Put the project name into the DNA name if there is nothing there
		// (that shouldn't happen unless there is an invalid DNA ID)
		if(_ri.getDnaName() == null || _ri.getDnaName().length() < 1)
		{
			_ri.setDnaName(_ri.getProjectName());
		}
		return _ri;
	}



	/**
	 * Fetch text label data that resides in the database
	 *
	 * @throws Exception
	 */


	private void getTextLabels(String langCode)
		throws Exception
	{
		// Get the text
		SessionUser sessionUser = new SessionUser();
		ITextHelper helper = HelperDelegate.getTextHelper(PropertyLoader.getProperty("com.pdi.data.v2.application","datasource.v2"));
		this.tg = helper.fromGroupCodeLanguageCodeWithDefault(sessionUser, textGroupCode, langCode);
		
		return;
	}
 

	/**
	 * Fetch report input data that resides in the database
	 *
	 * @throws Exception
	 */
	private void readRiData()
		throws Exception
	{
		// Get the "pure" RI data
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT * ");
		sqlQuery.append("  FROM pdi_abd_rpt_input ri ");
		sqlQuery.append("  WHERE ri.dnaId = ? ");
		sqlQuery.append("    AND ri.participantId = ?");

		DataLayerPreparedStatement dlps =  AbyDDatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			throw new Exception("Prepare Report Input data.  code=" +
								dlps.getStatus().getExceptionCode() +
								", msg=" + dlps.getStatus().getExceptionMessage());
		}

		dlps.getPreparedStatement().setLong(1, _ri.getDnaId());
		dlps.getPreparedStatement().setString(2, _ri.getParticipantId());

		DataResult dr =  null;
		try {
			dr =   AbyDDatabaseUtils.select(dlps);
			if (dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
			{
				// No data... return
				//System.out.println("NO DATA FOR REPORT!");
				return;
			}
			ResultSet rs = dr.getResultSet();
	
			// Should be a single row... fetch it.
			rs.next();
	
			// And move the data across
			// "Header" (general purpose) info
			_ri.setInputId(rs.getLong("inputId"));	//RI id
////			_ri.setStatusId(rs.getLong("statusId"));
			_ri.setRateStatusId(rs.getLong("rateStatusId"));
			_ri.setOtxtStatusId(rs.getLong("oTxtStatusId"));
			_ri.setPtxtStatusId(rs.getLong("pTxtStatusId"));
	
			// Skills to leverage
			_ri.setSkillsToLeverageOrgText(rs.getString("skillsToLeverageOrgText"));
			_ri.setSkillsToLeveragePartText(rs.getString("skillsToLeveragePartText"));
	
			// Skills to develop
			_ri.setSkillsToDevelopOrgText(rs.getString("skillsToDevelopOrgText"));
			_ri.setSkillsToDevelopPartText(rs.getString("skillsToDevelopPartText"));
	
			// Leadership Competencies summary
			_ri.setLeadershipSkillOrgText(rs.getString("leadershipSkillOrgText"));
			_ri.setLeadershipSkillPartText(rs.getString("leadershipSkillPartText"));
			_ri.setLeadershipSkillRating(rs.getInt("leadershipSkillRating"));
	
			// Leadership Experience Summary
			_ri.setLeadershipExperienceOrgText(rs.getString("leadershipExperienceOrgText"));
			_ri.setLeadershipExperiencePartText(rs.getString("leadershipExperiencePartText"));
			_ri.setLeadershipExperienceRating(rs.getInt("leadershipExperienceRating"));
	
			// Leadership Style and Aptitude
			_ri.setLeadershipStyleOrgText(rs.getString("leadershipStyleOrgText"));
			_ri.setLeadershipStylePartText(rs.getString("leadershipStylePartText"));
			_ri.setLeadershipStyleRating(rs.getInt("leadershipStyleRating"));
	
			// Leadership interest
			_ri.setLeadershipInterestOrgText(rs.getString("leadershipInterestOrgText"));
			_ri.setLeadershipInterestPartText(rs.getString("leadershipInterestPartText"));
			_ri.setLeadershipInterestRating(rs.getInt("leadershipInterestRating"));
	
			// Derailment Risk
			_ri.setDerailmentOrgText(rs.getString("derailmentOrgText"));
			_ri.setDerailmentPartText(rs.getString("derailmentPartText"));
			_ri.setDerailmentRating(rs.getInt("derailmentRating"));
	
			// Long-term Advancement Potential
			_ri.setLongtermOrgText(rs.getString("longtermOrgText"));
			_ri.setLongtermRating(rs.getInt("longtermRating"));
	
			// Fit Summary
			_ri.setFitOrgText(rs.getString("fitOrgText"));
////			_ri.setCandidateFitIndex(rs.getInt("candidateFitIndex"));
	
			// Readiness Summary
			_ri.setReadinessOrgText(rs.getString("readinessOrgText"));
	
			// Development Summary
			_ri.setDevelopmentPartText(rs.getString("developmentPartText"));
	
			// Pivotal Opportunities
			_ri.setPivotalPartText(rs.getString("pivotalPartText"));
	
			// IDP
			_ri.setIdpImagePath(rs.getString("idpImagePath"));
			_ri.setIdpDocumentPath(rs.getString("idpDocumentPath"));
			
			//additional notes
			_ri.setAdditionalNotes(rs.getString("additionalNotes"));
//
////			_ri.setProjectId(rs.getString("jobId"));
			
			// ALR stuff
			_ri.setAlrReadinessOrgText(rs.getString("alrReadinessOrgText"));
			_ri.setAlrReadinessRating(rs.getInt("alrReadinessRating"));
			_ri.setAlrSugReadinessRating(rs.getInt("alrSugReadinessRating"));
			_ri.setAlrReadinessORAuth(rs.getString("alrReadinessORAuth"));
			_ri.setAlrReadinessORJust(rs.getString("alrReadinessORJust"));
			
			_ri.setAlrCultureFitOrgText(rs.getString("alrCultureFitOrgText"));
			_ri.setAlrCultureFitPartText(rs.getString("alrCultureFitPartText"));
			_ri.setAlrCultureFitRating(rs.getInt("alrCultureFitRating"));
			_ri.setAlrSugCultureFitRating(rs.getInt("alrSugCultureFitRating"));
			_ri.setAlrCultureFitORAuth(rs.getString("alrCultureFitORAuth"));
			_ri.setAlrCultureFitORJust(rs.getString("alrCultureFitORJust"));
		
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (dr != null) {  dr.close();  dr = null; }
		}
	}


	/**
	 * Fetch dna related data for later use.  Stores some data in
	 *  variables and other data in the ReportInputDTO object.
	 *
	 * @param dnaId - The dnaID
	 * @throws Exception
	 */
	private void fetchDNAData()
		throws Exception
	{
		int transLvl = 0;
		
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT dna.dnaName, ");
		sqlQuery.append("       dna.projectId, ");
		sqlQuery.append("       dna.modelId, ");
		sqlQuery.append("       dna.synthMean as mean, ");
		sqlQuery.append("       dna.synthStdDev as stDev, ");
		sqlQuery.append("       mdl.internalName, ");
		sqlQuery.append("       mdl.targetLevelTypeId, ");
		sqlQuery.append("       mdl.isRa ");
		sqlQuery.append("  FROM pdi_abd_dna dna ");
		sqlQuery.append("    LEFT JOIN pdi_abd_model mdl ON mdl.modelId = dna.modelId ");
		sqlQuery.append("  WHERE dna.dnaId = " + _ri.getDnaId());
		//System.out.println("dna id sql = " +sqlQuery.toString());
		DataLayerPreparedStatement dlps =  AbyDDatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			throw new Exception("Error preparing DNA data fetch.  Code=" +
						dlps.getStatus().getExceptionCode() +
						", Msg=" + dlps.getStatus().getExceptionMessage());
		}

		DataResult dr =  null;
		try {
			dr =  AbyDDatabaseUtils.select(dlps);
			if (dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
			{
				dlps.getStatus().setExceptionMessage("No DNA data for dnaID " + _ri.getDnaId());
			}
			if (dr.isInError())
			{
				throw new Exception("Error fetching DNA data.  Code=" +
						dlps.getStatus().getExceptionCode() +
						", Msg=" + dlps.getStatus().getExceptionMessage());
			}
	
			ResultSet rs = dr.getResultSet();
	
			// should be a single row
			rs.next();

			_ri.setDnaName(rs.getString("dnaName"));
			_ri.setProjectId(rs.getString("projectId"));

			_synthNorm = new Norm();
			_synthNorm.setMean(rs.getDouble("mean"));
			_synthNorm.setStdDev(rs.getDouble("stDev"));
			intModelName = rs.getString("internalName");
			transLvl = rs.getInt("targetLevelTypeId");
			// Note that "isAlr"  probably should have been called "isReadinessAssessment"
			_ri.setIsAlr(rs.getBoolean("isRa"));
			_ri.setModelName(intModelName);

			if (transLvl == ReportingConstants.TGT_LVL_FLL)
			{
				_mdlTransLevel = 6;
			}
			else if (transLvl == ReportingConstants.TGT_LVL_MLL)
			{
				_mdlTransLevel = 7;
			}
			else if (transLvl == ReportingConstants.TGT_LVL_SLL ||
					 transLvl == ReportingConstants.TGT_LVL_BUL)
			{
				_mdlTransLevel = 8;
			}
			else if (transLvl == ReportingConstants.TGT_LVL_SEA)
			{
				_mdlTransLevel = 9;
			}
			else
			{
				throw new Exception("Error determining transition level for model named " + intModelName + " (transLvl=" + transLvl + ")");
			}
		}
		catch (Exception e)  {  e.printStackTrace();  }
		finally
		{
			if (dr != null) {  dr.close();  dr = null; }
		}
		return;
	}


	/**
	 * Fetch miscellaneous data for later use.  Data stored in the ReportInputDTO object.
	 *
	 * @param dnaId - The dnaID
	 * @throws Exception
	 */
	private void fetchMiscData()
		throws Exception
	{
		// Create a dummy session and get the project data
		SessionUser session = new SessionUser();
		IProjectHelper ipHelper = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated"); 
		Project project = ipHelper.fromId(session, this._ri.getProjectId());

		_ri.setProjectName(project.getName());
		_ri.setClientName(project.getClient().getName());

		// Now get the int grid stuff
		ResultSet rs = null;
		DataLayerPreparedStatement dlps = null;
		DataResult dr = null;

		boolean hasIg = true;
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT igr.leadFirstName, ");
		sqlQuery.append("       igr.leadLastName, ");
		sqlQuery.append("       igr.igLockDate, ");
		sqlQuery.append("       igr.lastDate ");
		sqlQuery.append("  FROM pdi_abd_igrid_response igr ");
		sqlQuery.append("  WHERE igr.dnaId = " + _ri.getDnaId() + " ");
		sqlQuery.append("    AND igr.participantId = '" + _ri.getParticipantId() + "'");
		dlps =  AbyDDatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			throw new Exception("Error preparing misc igrid data fetch.  Code=" +
						dlps.getStatus().getExceptionCode() +
						", Msg=" + dlps.getStatus().getExceptionMessage());
		}
		try
		{
			dr =  AbyDDatabaseUtils.select(dlps);
			if (dr.isInError())
			{
				if(dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
				{
					//throw new Exception("No IG response data for participant " + _ri.getParticipantId() +
					//		" in DNA " + _ri.getDnaId());
					hasIg = false;
				}
				else
				{
					throw new Exception("Error fetching misc igrid data.  Code=" +
							dlps.getStatus().getExceptionCode() +
							", Msg=" + dlps.getStatus().getExceptionMessage());
				}
			}

			String str = null;
			if (hasIg)
			{
				// should be a single row
				//ResultSet rs = dr.getResultSet();
				rs = dr.getResultSet();
				rs.next();
				str = rs.getString("leadFirstName") + " " + rs.getString("leadLastName");
				_ri.setConsultantName(str);
				Date last = rs.getDate("lastDate");
				Date lock = rs.getDate("igLockDate");
				Date date = lock != null ? lock : last;
			    SimpleDateFormat formatter = new SimpleDateFormat("MMMM yyyy");
			    str = formatter.format(date);
				_ri.setAdminDate(str);
			}
			else
			{
				_ri.setConsultantName("Not Available");
				_ri.setAdminDate("Not Available");
			}

			// Get the participant name
			Participant part = getPartInfo(_ri.getParticipantId());
			_ri.setParticipantName(getPartName(part));
			_ri.setParticipantNameInverse(getPartNameInv(part));
			_ri.setPartNotes(part.getAdminNotes());
		}
		finally
		{
			if (dr != null) {  dr.close();  dr = null; }
		}

		// Clean up
		dr = null;		// Clean up the old dr
		dlps = null;	// ...and the old dlps
		try
		{
			// Get the interview date from it's Eval Guide
			sqlQuery.setLength(0);
			sqlQuery.append("SELECT lastDate ");
			sqlQuery.append("  FROM pdi_abd_eg_response cr ");
			sqlQuery.append("  WHERE cr.dnaId = " + _ri.getDnaId() + " ");
			sqlQuery.append("    AND cr.participantId = '" + _ri.getParticipantId() + "'");
			sqlQuery.append("    AND moduleId IN (SELECT moduleId FROM pdi_abd_module WHERE internalName = 'Interview')");
			dlps =  AbyDDatabaseUtils.prepareStatement(sqlQuery);
			if (dlps.isInError())
			{
				throw new Exception("Error preparing misc EG data fetch.  Code=" +
							dlps.getStatus().getExceptionCode() +
							", Msg=" + dlps.getStatus().getExceptionMessage());
			}

			
			dr =  AbyDDatabaseUtils.select(dlps);
			if (dr.isInError())
			{
				if(dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
				{
					_ri.setInterviewDateStr(null);
				}
			} else
			{
				rs = dr.getResultSet();
				rs.next();
				Date intDate = rs.getDate("lastDate");
			    _ri.setInterviewDateStr(new SimpleDateFormat("MMMM d, yyyy").format(intDate));
			}
		}
		finally
		{
			if (dr != null) {  dr.close();  dr = null; }
		}
		
		// Get the linkparam id of the IG with which this DRI is associated
		// Clean up
		dr = null;		// Clean up the old dr
		dlps = null;	// ...and the old dlps
		try
		{
			sqlQuery.setLength(0);
			sqlQuery.append("SELECT lp.linkparamId ");
			sqlQuery.append("  FROM linkparams lp ");
			sqlQuery.append("    LEFT JOIN pdi_abd_dna dna on dna.projectId = lp.projectId ");
			sqlQuery.append("  WHERE lp.participantId = " + _ri.getParticipantId() + " ");
			sqlQuery.append("    AND lp.moduleId = 0 ");	// 0 = module ID of IG in linkparams
			sqlQuery.append("    AND dna.dnaId = " + _ri.getDnaId());
			dlps =  AbyDDatabaseUtils.prepareStatement(sqlQuery);
			if (dlps.isInError())
			{
				throw new Exception("Error preparing misc linkparam data fetch.  Code=" +
							dlps.getStatus().getExceptionCode() +
							", Msg=" + dlps.getStatus().getExceptionMessage());
			}
			
			dr =  AbyDDatabaseUtils.select(dlps);
			if (dr.isInError())
			{
				if(dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
				{
					_ri.setLpId(0);
				}
			} else {
				rs = dr.getResultSet();
				rs.next();
				_ri.setLpId(rs.getLong("linkparamId"));
			}
			
			return;
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}


	/**
	 * Fetch participant information
	 *
	 * @throws Exception
	 */
	private Participant getPartInfo(String participantId)
		throws Exception
	{
		SessionUser su = new SessionUser();	// Create a dummy session user
		IParticipantHelper ipHlpr = HelperDelegate.getParticipantHelper("com.pdi.data.nhn.helpers.delegated");
		Participant part = ipHlpr.fromId(su, participantId);
		NoteHolder nh = ipHlpr.getNotes(su, participantId);
		if(nh != null){
			for(Note n : nh.getNotesArray()){
				n.setNoteText(XMLUtils.xmlEscapeString(n.getNoteText()));
			}
		}
			
		part.setAdminNotes(nh);
		return part;
	}


	/**
	 * Generate the participant name
	 */
	private String getPartName(Participant dto)
	{
		String str;

		if (dto.getFirstName() == null && dto.getLastName() == null)
		{
			str = "(Not Available)";
		} else {
			str = (dto.getFirstName() == null ? "-" : dto.getFirstName()) + " ";
			str += (dto.getLastName() == null ? "-" : dto.getLastName());
		}
		return str;
	}


	/**
	 * Get the participant name, last name first, no delimiter
	 */
	private String getPartNameInv(Participant dto)
	{
		String str;

		if (dto.getFirstName() == null && dto.getLastName() == null)
		{
			str = "(Not Available)";
		} else {
			str = (dto.getLastName() == null ? "-" : dto.getLastName()) + " ";
			str += (dto.getFirstName() == null ? "-" : dto.getFirstName());
		}
		return str;
	}


	/**
	 * Get the "Show Custom Dashboard Report" flag
	 * Try to dna option first, and if it isn't there, use the option default
	 *
	 * @throws Exception
	 */
	private void fetchCdrFlag()
		throws Exception
	{
		// Get the SHO_CDR option both from the dna data and the default
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT rod.option_value AS dnaValue, ");
		sqlQuery.append("       olu.is_default AS dfltValue ");
		sqlQuery.append("  FROM pdi_abd_rpt_options_lookup olu ");
		sqlQuery.append("    LEFT JOIN pdi_abd_rpt_options_dna rod ON (rod.report_options_id = olu.report_options_id AND rod.dna_id = " + _ri.getDnaId() + ") ");
		sqlQuery.append("  WHERE olu.option_code = 'SHO_CDR'");

		DataLayerPreparedStatement dlps =  AbyDDatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			throw new Exception("Error preparing CDR flag fetch.  Code=" +
						dlps.getStatus().getExceptionCode() +
						", Msg=" + dlps.getStatus().getExceptionMessage());
		}

		DataResult dr =  null;
		try
		{
			dr =  AbyDDatabaseUtils.select(dlps);
			if (dr.isInError())
			{
				throw new Exception("Error fetching CDR flag.  Code=" +
							dlps.getStatus().getExceptionCode() +
							", Msg=" + dlps.getStatus().getExceptionMessage());
			}
			
			ResultSet rs = dr.getResultSet();
			
			// Always assume the row exists (because of the default form th options table)
			rs.next();
			String colName = "dfltValue";	// Default to dflt
			if (rs.getString("dnaValue") != null)
			{
				// Use the data for the dna
				colName = "dnaValue";
			}
			_ri.setShowCustomReport(rs.getBoolean(colName));

			return;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if (dr != null) {  dr.close();  dr = null; }
		}
	}


	/**
	 * Calculate the final competency score from the integration
	 * grid and use them to generate the dLCI
	 *
	 * @throws Exception
	 */
	private void calcDlci()
		throws Exception
	{
		// Get the final scores for each competency from the Integration Grid
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT igcr.competencyId, ");
		sqlQuery.append("       igcr.lcScore as scoreIdx ");
		sqlQuery.append("  FROM pdi_abd_igrid_comp_resp igcr ");
		sqlQuery.append("  WHERE igcr.participantId = '" + _ri.getParticipantId() + "' ");
		sqlQuery.append("    AND igcr.dnaId = " + _ri.getDnaId());

		DataLayerPreparedStatement dlps =  AbyDDatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			throw new Exception("Error preparing score fetch for dLCI.  Code=" +
					dlps.getStatus().getExceptionCode() +
					", Msg=" + dlps.getStatus().getExceptionMessage());
		}

		DataResult dr =  null;
		try {
			dr =  AbyDDatabaseUtils.select(dlps);
			if (dr.isInError())
			{
				if (dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
				{
					// No data... return
					return;
				}
				else
				{
					throw new Exception("Error fetching final scores for dLCI.  Code=" +
							dlps.getStatus().getExceptionCode() +
							", Msg=" + dlps.getStatus().getExceptionMessage());
				}
			}
			ResultSet rs = dr.getResultSet();
	
			// dLCI calc code stolen from AS3 script in Integration Grid (view/controls/home.mxml)
			double tot = 0.0;
			int cnt = 0;
			try
			{
				while (rs.next())
				{
					int idx = rs.getInt("scoreIdx");
					if (idx == -1 || idx == 0)
					{
						// If a score hasn't been entered (-1), or the "N/A" has been selected (0), don't try to score this
						continue;
					}
					else
					{
						// Could have used a 10 value switch based upon the index.  To save logic,
						// use an algorithm based upon the fact that each index (1 - 9) represents
						// a discrete score value that runs from 1.0 to 5.0 in 1/2 point increments.
						tot += (idx + 1.0) / 2.0;
						cnt++;
					}
				}
			}
			catch (SQLException e)
			{
				throw new Exception("calcDlci(): Error processing score data.  Msg=" + e.getMessage());
			}
			finally {}
	
			// Now calc the average final score and apply the norm
			_dlciZ = _synthNorm.calcZScore(tot / cnt);
			_dlci = Norm.calcIntPctlFromZ(_dlciZ);
			//System.out.println("DEBUG: :  mean=" + _synthNorm.getMean() + ", stDev=" + _synthNorm.getStdDev());
			//System.out.println("DEBUG: :  dLCI=" + _dlci);
	
			// Save it in the DTO
			_ri.setLeadershipSkillIndex(_dlci);
		} finally {
			if (dr != null) {  dr.close();  dr = null; }
		}
		return;
	}


	/**
	 * Calculate the requisite TLT scores.  Pass a ReportInputDTO object with the DNA and
	 * participant IDs set.  This method fetches TLT score data for the individual and
	 * stores the data in-place (back in the same DTO).  Nothing is explicitly passed back.
	 *
	 * @throws Exception
	 */
	private void calcTLT()
		throws Exception
	{
		// Get PPI from v2 (bogus Part & proj returned)
		SessionUser su = new SessionUser();
		IProjectParticipantInstrumentHelper ppih = HelperDelegate.getProjectParticipantInstrumentHelper("com.pdi.data.v2.helpers.delegated");
		ArrayList<ProjectParticipantInstrument> ppis = ppih.fromLastParticipantId(su, IProjectParticipantInstrumentHelper.MODE_FULL_COMPLETED, _ri.getParticipantId(), null);
		
		// Supress subordinate instruments
		ppis = new DataUtils().suppressSubInstUtil(ppis);
		
		// Get NHN proj and participant info
		Participant participant = HelperDelegate.getParticipantHelper("com.pdi.data.nhn.helpers.delegated").fromId(su, _ri.getParticipantId());
		Project project = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated").fromId(su, _ri.getProjectId());

		// and merge them
		for(ProjectParticipantInstrument ppi : ppis)
		{
			ppi.setParticipant(participant);
			ppi.setProject(project);
		}

		// Get TLT data... start with the current target level
		IndividualReport irCur = getTLTData(_mdlTransLevel, ppis);
		_ri.setCurrentTLT(irCur);
		
		// Set up additional stuff off the current level
		if(irCur.getScriptedData().get(ReportingConstants.DR_DERAIL).equals("0.0"))
		{
			_curDerailStanine = 0;
		} else {
			_curDerailStanine = Integer.parseInt(irCur.getScriptedData().get(ReportingConstants.DR_DERAIL));
		}

		_curLeInterest = (irCur.getScriptedGroupData().get("LE_INTER_FINALSCORE") != 0.0);
		_curLeExperience = (irCur.getScriptedGroupData().get("LE_EXPER_FINALSCORE") != 0.0);
		_curLeFoundation = (irCur.getScriptedGroupData().get("LE_FOUND_FINALSCORE") != 0.0);
//			//System.out.println("got cur level DR and LE data.");
//			System.out.println("DR=" + _curDerailStanine +
//					   			   ", LE_INTER=" + (_curLeInterest ? "true" : "false") +
//					   			   ", LE_EXPER=" + (_curLeExperience ? "true" : "false") +
//								   ", LE_INTER=" + (_curLeFoundation ? "true" : "false") );
//			System.out.println("CalcTLT: Missing instruments?  GPI=" + (irCur.getDisplayData("GPI_MISSING") == null ? "Present" : "Missing") +
//			"  LEI=" + (irCur.getDisplayData("LEI_MISSING") == null ? "Present" : "Missing") +
//			"  CS=" + (irCur.getDisplayData("CS_MISSING") == null ? "Present" : "Missing"));

		/*
		 * OK; having done that for the level we desire we need to do it for one level
		 * higher for Long-Term Advancement Potential.  Set the level up by 1, then run
		 * the data gathering for IR and GR again but with the destination transition
		 * level set up by 1.
		 * NOTE: skip this step if the destination is SEA (there is no level above SEA)
		 */
		if (_mdlTransLevel == 9)
		{
			_doNxtLvl = false;
			return;
		}

		// Get the value for the target level - This works for now because the
		// levels are sequential ascending.  If that changes, then we will have
		// to change this logic.
		IndividualReport irNxt = getTLTData(_mdlTransLevel + 1, ppis);
		_ri.setNextTLT(irNxt);

		// Extract the data we want
		String errStr = irCur.getScriptedData().get("ERROR");
		if (errStr != null)
		{
			_errorDetectedNxt = true;
			//System.out.println("NextTLT - Unable to fetch next level DR and LE data.");
			//System.out.println("  Msg=" + errStr);
			//return;  // removing the return because we want it just to run...
		}
		_nxtLeInterest = (irNxt.getScriptedGroupData().get("LE_INTER_FINALSCORE") != 0.0);
		_nxtLeExperience = (irNxt.getScriptedGroupData().get("LE_EXPER_FINALSCORE") != 0.0);
		_nxtLeFoundation = (irNxt.getScriptedGroupData().get("LE_FOUND_FINALSCORE") != 0.0);
//			System.out.println("got next level DR and LE data.");
//			System.out.println("LE_INTER=" + (_nxtLeInterest ? "true" : "false") +
//					   			   ", LE_EXPER=" + (_nxtLeExperience ? "true" : "false") +
//								   ", LE_INTER=" + (_nxtLeFoundation ? "true" : "false") );
		
		return;
	}


	/**
	 * Get data from TLT.  This is special-purpose code set up to run the TLT
	 * scoring.  It is called to calculate the current level and then again one
	 * level up calling this same method.  It does the heavy lifting for the
	 * calcTLT() method and should be called only from there.
	 *
	 * @throws Exception
	 */
	private IndividualReport getTLTData(int targetLevel, ArrayList<ProjectParticipantInstrument> ppis)
		throws Exception
	{
		IndividualReport ir = new IndividualReport();

		// Set the stuff for "reporting"
		ir.setReportCode(ReportingConstants.REPORT_CODE_TLT_GROUP_DETAIL);
		ir.setReportType(ReportingConstants.REPORT_TYPE_GROUP_DATA);
		//ret.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);
		
		// Set the transition level
		// The transition level as passed in is the value in the value from the
		// transitionLevel column in normset (7, 8, or 9).  We need to store a value in the
		// IR object that represents a transition as defined in the project (the "from-to"
		// type, like IC->FLL = 1) because the code that gets the Individual norm in the V2
		// ParticipantDataHelper uses that to get the appropriate Individual norms from
		// the normset table.  We probably need to re-factor this (see notes there), but
		// note again that the transition code comes from the project and my be difficult
		// to change.  We use one of the transition codes on the left below to represent
		// the value we have from the column on the right.
		// Secret decoder ring:
		// 	Transition		transitionLevel code
		// 	----------		-----------
		// 1 -  IC-->FLL		6
		// 2 -  IC-->MLL		7
		// 3 - FLL-->MLL		7
		// 4 - FLL-->SE/BUL		8
		// 5 - MLL-->SE/BUL		8
		// 10- MLL-->SEA		9	<-- New... No group norms exist
		// 11- BUL-->SEA		9	<-- New... No group norms exist
		String trans;
		//String curLev;
		switch (targetLevel)
		{
			case 6:		// FLL
				trans = "1";	// IC->FLL
				break;
			case 7:		// MLL
				trans = "2";	// IC->MLL
				break;
			case 8:		// EXEC/Sr. Exec/BUL
				trans = "4";	// FLL->SE/BUL
				break;
			case 9:		// SEA
				trans = "10";		// MLL->SEA
				break;
			default:
				throw new Exception("Invalid transition level (" + _mdlTransLevel + ")");
		}
		ir.getDisplayData().put("TRANSITION_LEVEL", trans); //need these for Transition Report
		ir.getDisplayData().put("TARGET_LEVEL", targetLevel + "");
		
		// Get the TLT norms and populate the IR with the data
		int indivNormsetId = new ParticipantDataHelper().getIndivNormsetId(trans + "");	// Destination level adjusted transition level
		NormSet normset;
		normset = new ParticipantDataHelper().getPdiTransitionNormset(indivNormsetId);
		normset = new ParticipantDataHelper().populateNormset(normset);

		for(Norm norm : normset.getNorms())
		{
			//System.out.println("NORMNAMES:  " + norm.getName()+ "_MEAN  " + norm.getMean());
			ir.addDisplayData(norm.getName()+ "_MEAN", (new Double(norm.getMean())).toString());
			ir.addDisplayData(norm.getName()+ "_STDEV", (new Double(norm.getStdDev())).toString());
		}

		ir.getDisplayData().put(ReportingConstants.RC_COGNITIVES_INCLUDED, "1");
		
		// Now generate the RD object for each instrument
		boolean firstPpi = true;
		for(ProjectParticipantInstrument ppi : ppis)
		{
			if (firstPpi)
			{
				// Do a bunch of stuff that needs be done only once
				ir.addDisplayData("FIRST_NAME", ppi.getParticipant().getFirstName());
				ir.addDisplayData("LAST_NAME", ppi.getParticipant().getLastName());
				ir.addDisplayData("ORGANIZATION", ppi.getProject().getClient().getName());
				firstPpi = false;
			}

			// we need a full GPI for A by D... skip if this is a short GPI
			if (ppi.getInstrument().getCode().equals("gpi"))
			{
				continue;
			}

			ReportData rd = new ReportData();
			if(ppi.getScoreGroup() != null)
			{
				for(Score s : ppi.getScoreGroup().getScores().values()) {					
					rd.addScoreData(s.getCode(), s.getRawScore());
				}
			}
			// Get the reporting instrument info
			String rptId = ppi.getInstrument().getMetadata().get("reportingInstId");
			ir.getReportData().put(rptId, rd);
			if (rptId.equals(ReportingConstants.RC_GPI))
			{
				_gpiMissing = false;
			}
			else if (rptId.equals(ReportingConstants.RC_LEI))
			{
				_leiMissing = false;
			}
			else if (rptId.equals(ReportingConstants.RC_CAREER_SURVEY))
			{
				_csMissing = false;
			}
			else if (rptId.equals(ReportingConstants.RC_RAVENS_B) ||
					 rptId.equals(ReportingConstants.RC_RAVENS_SF))
			{
				_rvMissing = false;
			}
		}	// End of for loop on ppis

		// set up auxilliary data based on missing flag.
		// We want as much data as possible to come across so we are going to fake
		// the Groovy script way down below by adding in empty (or nearly so)
		// ReportData objects for the missing instruments

		if(_gpiMissing)
		{
			ir.addScriptedData("STATUS", "NOT_FINISHED");
			ir.addDisplayData("GPI_MISSING", "MISSING"); 
			ir.getScriptedData().put("ERROR", "GPI has not been completed, unable to generate report");
			ReportData rd = new ReportData();
			ir.getReportData().put(ReportingConstants.RC_GPI, rd);
		}
		if(_leiMissing)
		{
			ir.addScriptedData("STATUS", "NOT_FINISHED");
			ir.addDisplayData("LEI_MISSING", "MISSING"); 
			ir.getScriptedData().put("ERROR", "LEI has not been completed, unable to generate report");
			ReportData rd = new ReportData();
			ir.getReportData().put(ReportingConstants.RC_LEI, rd);
		}
		if(_csMissing)
		{
			ir.addScriptedData("STATUS", "NOT_FINISHED");
			ir.addDisplayData("CS_MISSING", "MISSING"); 
			ir.getScriptedData().put("ERROR", "CAREER SURVEY has not been completed, unable to generate report");
			ReportData rd = new ReportData();
			ir.getReportData().put(ReportingConstants.RC_CAREER_SURVEY, rd);
		}
		if (_rvMissing)
		{
			// Do nothing at this time
		}

		// We want to get as much data as we can for A by D so fake
		// out subsequent scoring by hard-coding to "Partial" 
		ir.getScriptedData().put(ReportingConstants.PART_COMPL_RPT, ReportingConstants.PARTIAL_REPORT);

		//Create a group report
		GroupReport gr = new GroupReport();
		gr.setReportCode(ReportingConstants.REPORT_CODE_TLT_GROUP_DETAIL);
		gr.setReportType(ReportingConstants.REPORT_TYPE_GROUP_DATA);
		gr.setTargetTransitionLevel(targetLevel);
		gr.getIndividualReports().add(ir);

		// Generate the tlt score data
		ReportingRequest request = new ReportingRequest();
		request.addReport(gr);
		ReportingResponse response = request.generateReports();

		GroupReport resp = (GroupReport)response.getReports().get(0);
		return resp.getIndividualReports().get(0);
	}

// Saved old doco that is still relevant
//		// Set the transition level
//		// The transition level as passed in is the value in the value from the
//		// transitionLevel column in normset (7, 8, or 9).  We need to store a value in the
//		// IR object that represents a transition as defined in the project (the "from-to"
//		// type, like IC->FLL = 1) because the code that gets the Individual norm in the V2
//		// ParticipantDataHelper uses that to get the appropriate Individual norms from
//		// the normset table.  We probably need to re-factor this (see notes there), but
//		// note again that the transition code comes from the project and my be difficult
//		// to change.  We use one of the transition codes on the left below to represent
//		// the value we have from the column on the right.
//		// Secret decoder ring:
//		// 	Transition		transitionLevel code
//		// 	----------		-----------
//		// 1 -  IC-->FLL		6
//		// 2 -  IC-->MLL		7
//		// 3 - FLL-->MLL		7
//		// 4 - FLL-->SE/BUL		8
//		// 5 - MLL-->SE/BUL		8
//		// 10- MLL-->SEA		9	<-- New... No group norms exist
//		// 11- BUL-->SEA		9	<-- New... No group norms exist


	/**
	 * Calculate the recommended values for those buckets that need them
	 *
	 * @param langCode - The language code (not the numeric ID) of the language needed
	 * @throws Exception
	 */
	private void deriveRecommendedValues(String langCode, boolean isPreview)
		throws Exception
	{
		// Skills to leverage - No derived values

		// Skills to develop - No derived values

		// Leadership Competencies summary
		// Add the "suggested" value
		
		// Temporary fix for NHN-2751 so that focus text will be found if Canadian is selected (use French)
		String langCodeTemp = langCode;
		if(langCode.equals("fr_CA"))
		{
			langCodeTemp = "fr";
		}
		
		int lss = 0;
		if (_dlci == 1)
			lss = -1;
		else if (_dlci >1 && _dlci < 25)
			lss = WEAK;
		else if (_dlci >= 25  && _dlci < 50)
			lss = MIXED;
		else if (_dlci >= 50 && _dlci < 75)
			lss = STRONG;
		else if (_dlci >= 75)
			lss = VERY_STRONG;

		// Only put the recommended in if the dLCI > 0 (0 implies no data)
		if (lss > 0)
		{
			_ri.getLeadershipSkillRecommendedRatings().add(lss);
		}

		if (_ri.getIsAlr())
		{
			// Save the rating to prime the pump for ALR
			if(_ri.getLeadershipSkillRating() == 0 && lss > 0)
			{
				_ri.setLeadershipSkillRating(lss);
			}
		}

		if (isPreview)
		{
			// Put in some dummy ratings so we don't try to get data
			_ri.getLeadershipExperienceRecommendedRatings().add(2);
			_ri.getLeadershipStyleRecommendedRatings().add(2);
			_ri.getLeadershipInterestRecommendedRatings().add(2);
			_ri.setDerailmentRecommendedRating(2);
			_ri.getLongtermRecommendedRatings().add(2);
		}
		else
		{
			Connection con = null;
			try
			{
				con = AbyDDatabaseUtils.getDBConnection();
				IGAlrDataHelper iadHelper = new IGAlrDataHelper(con, _ri.getParticipantId(), _ri.getDnaId());
				IGKf4dDataHelper k4dHelper = new IGKf4dDataHelper(con, _ri.getParticipantId(), _ri.getDnaId());
				if (_ri.getIsAlr() &&  iadHelper.hasAlpInDna())
				{
						//get ALP recommendations
					try
					{
						getAlpRecommendations(iadHelper);
					}
					catch (Exception e)
					{
						// Report it but go on
						System.out.println("Unable to fetch ALR recommendations (KFALP); continuing...  pptId=" + _ri.getParticipantId() + ", dnaId=" + _ri.getDnaId() );
					}
				}
				else if (_ri.getIsAlr() &&  k4dHelper.hasKf4dInDna())
				{
						getKf4dRecommendations(k4dHelper);
				}
				else
				{
					// It is not an ALR type (or it IS but doesn't have ALP or KF4D in the DNA)
					getNonAlrRecommendations();
				}
			}
			finally
			{
				if (con != null)
				{
					try
					{
						con.close();
					}
					catch (Exception e) { }
					con = null;
				}
			}
		}
		
		// Fit and readiness suggested values
		getSugFitAndReadiness(_ri, langCodeTemp);

		// Development Summary - No derived values

		// Pivotal Opportunities - No derived values

		// IDP - No derived values
	}

	
	private void getAlpRecommendations(IGAlrDataHelper helper)
		throws Exception
	{
		// Leadership Experience Summary
		_ri.getLeadershipExperienceRecommendedRatings().add(helper.getExperienceRecommendation());

		// Leadership Style and Aptitude (Foundations)
		_ri.getLeadershipStyleRecommendedRatings().add(helper.getStyleRecommendation());

		// Leadership interest
		_ri.getLeadershipInterestRecommendedRatings().add(helper.getInterestRecommendation());

		// Derailment Risk
		_ri.setDerailmentRecommendedRating(helper.getDerailmentRecommendation());

		// Long-term Advancement Potential
		_ri.getLongtermRecommendedRatings().add(helper.getAdvancementRecommendation());
	}

	
	private void getKf4dRecommendations(IGKf4dDataHelper helper)
		throws Exception
	{
		// Leadership Experience Summary
		// No Experience Rating available from KF4D

		// Leadership Style and Aptitude (Foundations)
		_ri.getLeadershipStyleRecommendedRatings().add(helper.getStyleRecommendation());

		// Leadership interest
		_ri.getLeadershipInterestRecommendedRatings().add(helper.getInterestRecommendation());

		// Derailment Risk
		_ri.setDerailmentRecommendedRating(helper.getDerailmentRecommendation());

		// Long-term Advancement Potential
		_ri.getLongtermRecommendedRatings().add(helper.getAdvancementRecommendation());
	}


	/**
	 * getNonAlrRecommendations - The "old school" calculations for recommendations, derived from GPI, LEI, CS
	 */
	private void getNonAlrRecommendations()
	{
		// Leadership Experience Summary
		if (_leiMissing)
		{
			// do nothing
		}
		else
		{
			//Add the suggested value
			if (_curLeExperience)
			{
				_ri.getLeadershipExperienceRecommendedRatings().add(STRONG);
				_ri.getLeadershipExperienceRecommendedRatings().add(VERY_STRONG);
			}
			else
			{
				_ri.getLeadershipExperienceRecommendedRatings().add(MIXED);
				_ri.getLeadershipExperienceRecommendedRatings().add(WEAK);
			}
		}

		// Leadership Style and Aptitude (Foundations)
		if (_gpiMissing)
		{
			// Do nothing
		}
		else
		{
			//Add the suggested value
			if (_curLeFoundation)
			{
				_ri.getLeadershipStyleRecommendedRatings().add(STRONG);
				_ri.getLeadershipStyleRecommendedRatings().add(VERY_STRONG);
			}
			else
			{
				_ri.getLeadershipStyleRecommendedRatings().add(MIXED);
				_ri.getLeadershipStyleRecommendedRatings().add(WEAK);
			}
		}

		// Leadership interest
		if (_csMissing)
		{
			// Do nothing
		}
		else
		{
			//Add the suggested value
			if (_curLeInterest)
			{
				_ri.getLeadershipInterestRecommendedRatings().add(STRONG);
				_ri.getLeadershipInterestRecommendedRatings().add(VERY_STRONG);
			}
			else
			{
				_ri.getLeadershipInterestRecommendedRatings().add(MIXED);
				_ri.getLeadershipInterestRecommendedRatings().add(WEAK);
			}
		}

		// Derailment Risk
		// Code stolen from TLT_INDIVIDUAL_SUMMARY_REPORT because
		// the Group report only puts out the stanine
		//TODO Possible re-factor of this code into the Group Report where the
		//     stanine is calculated and put out an extra data item there that
		//     we pick up here in calcTlt.
		int derail = 0;
		if (_gpiMissing)
		{
			// derail should be 0... that is the default value
		}
		else
		{
			if (_curDerailStanine == 9)
			{
				derail = HIGH;
			}  else if (_curDerailStanine == 8)
			{
				derail = MODERATE;
			}  else if (_curDerailStanine > 4 &&
					_curDerailStanine < 8 )
			{
				derail = LOW;
			}  else if (_curDerailStanine >= 1 &&
					_curDerailStanine < 5)
			{
				derail = MINIMAL;
			}
		}
		_ri.setDerailmentRecommendedRating(derail);

		// Long-term Advancement Potential
		// Don't fill the recommended if we aren't supposed to do the next level
		// or if there was an error getting the next level data
		if (_doNxtLvl && ! _errorDetectedNxt)
		{
			if (_gpiMissing || _leiMissing || _csMissing)
			{
				// Do nothing
			}
			else
			{
				//Add up the number of leaderhip items on
				int itmCnt = 0;
				if (_nxtLeFoundation)
					itmCnt++;
				if (_nxtLeInterest)
					itmCnt++;
				if (_nxtLeExperience)
					itmCnt++;

				// ...and use that total to determine the suggested rating
				int sug;
				switch (itmCnt)
				{
					case 3:
						sug = VERY_STRONG;
						break;
					case 2:
						sug = STRONG;
						break;
					case 1:
						sug = MIXED;
						break;
					default:
						sug = WEAK;
						break;
				}
				_ri.getLongtermRecommendedRatings().add(sug);
			}
		}
//
//		// Fit Summary
//		// Assumes that the consultant has rated Leadership Competencies, Leadership
//		// Experience, Style & Aptitude, Leadership Interest and Derailment.
//		double newZ = _dlciZ;
//		int[] ary = new int[5];
//		ary[0] = _ri.getLeadershipSkillRating();
//		ary[1] = _ri.getLeadershipExperienceRating();
//		ary[2] = _ri.getLeadershipStyleRating();
//		ary[3] = _ri.getLeadershipInterestRating();
//		ary[4] = _ri.getDerailmentRating();
//
//		// "Bad data" includes invalid values but the most
//		// likely is a 0 (no data)
//		boolean badData = false;
//		for (int i=0; i < ary.length; i++)
//		{
//
//			switch (ary[i])
//			{
//				case 4:
//					newZ += 0.125;
//					break;
//				case 3:
//					newZ += 0.0625;
//					break;
//				case 2:
//					newZ -= 0.0625;
//					break;
//				case 1:
//					newZ -= 0.125;
//					break;
//				default:
//					badData = true;
//					break;
//			}
//			if (badData)
//				break;
//		}
//
//		if(badData)
//		{
//			_ri.setFitRecommendedRating(INSUF_DATA);
//		}
//		else
//		{
//			int fitIdx = Norm.calcIntPctlFromZ(newZ);
//			_ri.setCandidateFitIndex(fitIdx);
//			if (fitIdx < 25)
//				_ri.setFitRecommendedRating(NOT_RECOM);
//			else if (fitIdx < 50) // implied >= 25 and < 50
//				_ri.setFitRecommendedRating(RECOM_RESERV);
//			else if (fitIdx < 75) // implied >= 50 and < 75
//				_ri.setFitRecommendedRating(RECOM);
//			else				 // implied >= 75
//				_ri.setFitRecommendedRating(STRONG_RECOM);
//
//		}
//		//System.out.println(Norm.calcIntPctlFromZ(newZ) + " vs " + badData);
//
//		getReadinessFocus(langCodeTemp);
//
//		// Development Summary - No derived values
//
//		// Pivotal Opportunities - No derived values
//
//		// IDP - No derived values
	}


	/**
	 * Convenience method to default language
	 * @throws Exception
	 */
	private void getSugFitAndReadiness(ReportInputDTO ri)
		throws Exception
	{
		getSugFitAndReadiness(ri, DefaultLanguageHelper.fetchDefaultLanguageCode());
		return;
	}


	/**
	 * getSugFitAndReadiness - Calculates default fit and readiness values
	 * @param langCodeTemp
	 * @throws Exception
	 */
	private void getSugFitAndReadiness(ReportInputDTO ri, String langCode)
		throws Exception
	{
		boolean isJnJ = false;
		NhnWebserviceUtils nws = new NhnWebserviceUtils();
		ArrayList<String> productCodes = nws.getProjectProductCodes(ri.getProjectId());
		if (productCodes.contains(JnJ_CODE))
		{
			isJnJ = true;
		}
		
		// Fit
		getSugFit(ri, isJnJ);
		// Readiness
		getSugReadinessAndFocus(ri, langCode, isJnJ);

		return;
	}
	

	/**
	 * getReadinessFocus - get the readiness rating (if necessary) and
	 * 					   focus statement (in appropriate spot)
	 * @param langCode
	 * @throws Exception
	 */
	private void getSugFit(ReportInputDTO ri, boolean isJnJ)
	{
		if (ri.getIsAlr() && ! isJnJ)
		{
			calcAlrCultureFit(ri);
		}
		
		// This is the legacy fit calc code - Also called candidate fit
		// Assumes that the consultant has rated Leadership Competencies, Leadership
		// Experience, Style & Aptitude, Leadership Interest and Derailment.
		// Used for candidate fit on RA projects
		double newZ = _dlciZ;
		int[] ary = new int[5];
		ary[0] = ri.getLeadershipSkillRating();
		ary[1] = ri.getLeadershipExperienceRating();
		ary[2] = ri.getLeadershipStyleRating();
		ary[3] = ri.getLeadershipInterestRating();
		ary[4] = ri.getDerailmentRating();

		// "Bad data" includes invalid values but the most
		// likely is a 0 (no data)
		boolean badData = false;
		for (int i=0; i < ary.length; i++)
		{

			switch (ary[i])
			{
				case 4:
					newZ += 0.125;
					break;
				case 3:
					newZ += 0.0625;
					break;
				case 2:
					newZ -= 0.0625;
					break;
				case 1:
					newZ -= 0.125;
					break;
				default:
					badData = true;
					break;
			}
			if (badData)
				break;
		}

		int fitRate = 0;
		int fitIdx = 0;
		if(badData)
		{
			fitRate = INSUF_DATA;
		}
		else
		{
			fitIdx = Norm.calcIntPctlFromZ(newZ);
			if (fitIdx < 25)
				fitRate = NOT_RECOM;
			else if (fitIdx < 50) // implied >= 25 and < 50
				fitRate = RECOM_RESERV;
			else if (fitIdx < 75) // implied >= 50 and < 75
				fitRate = RECOM;
			else				 // implied >= 75
				fitRate = STRONG_RECOM;
		}
		ri.setCandidateFitIndex(fitIdx);
		ri.setFitRecommendedRating(fitRate);
		
		if (ri.getIsAlr() && isJnJ)
		{
			// Used for culture fit at J&J
			ri.setAlrSugCultureFitRating(fitRate);
		}
	}
	

	/**
	 * getSugReadinessAndFocus - get the readiness rating (if necessary) and
	 * 					         focus statement (in appropriate spot)
	 * @param langCode
	 * @param isJnJ
	 * @throws Exception
	 */
	private void getSugReadinessAndFocus(ReportInputDTO ri, String langCode, boolean isJnJ)
			throws Exception
		{
		// Always calc the ALR readiness (if ALR)
		if (ri.getIsAlr() && ! isJnJ)
		{
			calcAlrReadiness(ri);
		}

		// Readiness (for both) or ALR Summary (for focus)
		if (ri.getLeadershipSkillRating() == 0  ||
			ri.getLeadershipExperienceRating() == 0 ||
			ri.getLeadershipStyleRating() == 0 ||
			ri.getLeadershipInterestRating() == 0 ||
			ri.getDerailmentRating() == 0)
		{
			if (this.tg == null)
			{
				this.getTextLabels(langCode);
			}
			ri.setReadinessRating(INSUF_DATA);
			ri.setReadinessFocus(this.tg.getLabelMap().get("INSUF_DATA").getText());
			// ALR focus needs all of these values so this is correct
			ri.setAlrReadinessFocus(this.tg.getLabelMap().get("INSUF_DATA").getText());
			return;
		}
		
		// Have the data... do it
		// Calculate the  readiness "magic number"
		int wrk;
		int key = 0;
		int perfAtTarget = 3;	// Hard-coded to 3 for now

		// Skills = value * 10000
		key += ri.getLeadershipSkillRating() * 10000;

		// Performance at Target = mod. value * 1000
		if (perfAtTarget == VERY_STRONG ||
			perfAtTarget == STRONG)	// working value: VS & S = 2, M & W = 1
			wrk = 2;
		else
			wrk = 1;
		key += wrk * 1000;

		// Experience = mod. value * 100
		if (ri.getLeadershipExperienceRating() == VERY_STRONG ||
			ri.getLeadershipExperienceRating() == STRONG)	// working value:  VS & S = 2, M & W = 1
			wrk = 2;
		else
			wrk = 1;
		key += wrk * 100;

		// Interest (Career Drivers) = mod. value * 10
		if (ri.getLeadershipInterestRating() == VERY_STRONG ||
			ri.getLeadershipInterestRating() == STRONG)	// working value:  VS & S = 2, M & W = 1
			wrk = 2;
		else
			wrk = 1;
		key += wrk * 10;

		// Style & Ability (Foundations) = mod. value
		if (ri.getLeadershipStyleRating() == VERY_STRONG ||
			ri.getLeadershipStyleRating() == STRONG)	// working value:  VS & S = 2, M & W = 1
			wrk = 2;
		else
			wrk = 1;
		key += wrk;
		//System.out.println("Magic Number=" + key);

		// Get the Readiness Rating and Focus Statement from the database
		// Decoder Ring for Readiness Recommendation:
		//		1 = Develop In Place
		//		2 - Broaden
		//		3 = Prepare
		//		4 -  Ready
		ResultSet rs = null;
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT rf.readinessRating, ");
		sqlQuery.append("		txt.text as focusText ");
		sqlQuery.append("  FROM pdi_abd_readiness_focus rf ");
		sqlQuery.append("    LEFT JOIN pdi_abd_text txt ON (txt.textId = rf.focusTextId AND ");
		sqlQuery.append("  		                            txt.languageId = (SELECT ala.languageId ");
		sqlQuery.append("  		                                                FROM pdi_abd_language ala ");
		sqlQuery.append("  		  											    WHERE ala.languageCode = '" + langCode + "')) ");
		sqlQuery.append("  WHERE rf.synthKey = " + key);
		DataLayerPreparedStatement dlps =  AbyDDatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			throw new Exception("Error preparing readiness/focus data fetch.  Code=" +
						dlps.getStatus().getExceptionCode() +
						", Msg=" + dlps.getStatus().getExceptionMessage());
		}
		DataResult dr =  null;
		try
		{
			dr =  AbyDDatabaseUtils.select(dlps);
			if (dr.isInError())
			{
				if(dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
				{
					throw new Exception("Unable to find focus key " + key);
				}
				else
				{
					throw new Exception("Error fetching readiness/focus data for key " + key +
							".  Code=" + dlps.getStatus().getExceptionCode() +
							", Msg=" + dlps.getStatus().getExceptionMessage());
				}
			}
			// should be a single row
			rs = dr.getResultSet();
			rs.next();
			String str = rs.getString("focusText");
			// Now add focus text clauses as needed.  First check the derailment
			if (ri.getDerailmentRating() == HIGH ||
				ri.getDerailmentRating() == MODERATE)
			{
				if (this.tg == null)
				{
					this.getTextLabels(langCode);
				}
				//System.out.println("MAN_DR = " + this.tg.getLabelMap().get("MAN_DR").getText());
				str += "  " + this.tg.getLabelMap().get("MAN_DR").getText();
			}			
			
			int rating = rs.getInt("readinessRating");
			if (ri.getIsAlr())
			{
				if (isJnJ)
				{
					ri.setAlrSugReadinessRating(rating);
				}
				// Focus only... most ALR doesn't do the regular rating
				ri.setAlrReadinessFocus(str);
			}
			else
			{
				// Non-ALR does the rating and the focus stmt
				ri.setReadinessRating(rating);
				ri.setReadinessFocus(str);
			}
		}
		finally
		{
			if (dr != null) {  dr.close();  dr = null; }
		}
		
		return;
	}


	/**
	 * Insert the report input data
	 *
	 *
	 * @param ReportInputDTO reportInput
	 * @return long - the DataResult id
	 * @throws Exception
	 */
	// QUESTION _ Do we just return the id (as now implemented)
	// or do we return the whole input DTO with the inputId now set?
	//
	// NOTE:  Currently only called the first time the DRI is fetched and there is no data.  No lock dates are supported.
	public long addReportInput(ReportInputDTO reportInput)
		throws Exception
	{
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("INSERT INTO pdi_abd_rpt_input");
		sqlQuery.append("(");
		sqlQuery.append("dnaId,");							// 1
		sqlQuery.append("rateStatusId,");					// 2 - old statusId replaced with rateStatusId;  rest are down below

		sqlQuery.append("skillsToLeverageOrgText,");		// 3
		sqlQuery.append("skillsToLeveragePartText,");		// 4

		sqlQuery.append("skillsToDevelopOrgText,");			// 5
		sqlQuery.append("skillsToDevelopPartText,");		// 6

		sqlQuery.append("leadershipSkillOrgText,");			// 7
		sqlQuery.append("leadershipSkillPartText,");		// 8
		sqlQuery.append("leadershipSkillRating,");			// 9

		sqlQuery.append("leadershipExperienceOrgText,");	// 10
		sqlQuery.append("leadershipExperiencePartText,");	// 11
		sqlQuery.append("leadershipExperienceRating,");		// 12

		sqlQuery.append("leadershipStyleOrgText,");			// 13
		sqlQuery.append("leadershipStylePartText,");		// 14
		sqlQuery.append("leadershipStyleRating,");			// 15

		sqlQuery.append("leadershipInterestOrgText,");		// 16
		sqlQuery.append("leadershipInterestPartText,");		// 17
		sqlQuery.append("leadershipInterestRating,");		// 18

		sqlQuery.append("derailmentOrgText,");				// 19
		sqlQuery.append("derailmentPartText,");				// 20
		sqlQuery.append("derailmentRating,");				// 21

		sqlQuery.append("longtermOrgText,");				// 22
		sqlQuery.append("longtermRating,");					// 23

		sqlQuery.append("fitOrgText,");						// 24
//		sqlQuery.append("candidateFitIndex,");

		sqlQuery.append("readinessOrgText,");				// 25

		sqlQuery.append("developmentPartText,");			// 26

		sqlQuery.append("pivotalPartText,");				// 27

		sqlQuery.append("idpImagePath,");					// 28
		sqlQuery.append("idpDocumentPath,");				// 29
		
		sqlQuery.append("additionalNotes,");				// 30

		sqlQuery.append("lastUserId,");						// 31
		sqlQuery.append("lastDate,");						// Date inserted by GETDATE() function
		sqlQuery.append("participantId,");					// 32
		
		sqlQuery.append("alrReadinessOrgText,");			// 33
		sqlQuery.append("alrReadinessRating,");				// 34
		sqlQuery.append("alrSugReadinessRating,");			// 35
		sqlQuery.append("alrReadinessORAuth,");				// 36
		sqlQuery.append("alrReadinessORJust,");				// 37

		sqlQuery.append("alrCultureFitOrgText,");			// 38
		sqlQuery.append("alrCultureFitPartText,");			// 39
		sqlQuery.append("alrCultureFitRating,");			// 40
		sqlQuery.append("alrSugCultureFitRating,");			// 41
		sqlQuery.append("alrCultureFitORAuth,");			// 42
		sqlQuery.append("alrCultureFitORJust,");			// 43
		
		sqlQuery.append("oTxtStatusId,");					// 44
		sqlQuery.append("pTxtStatusId");					// 45


		sqlQuery.append(")");
		sqlQuery.append("VALUES");
		sqlQuery.append("(");
		sqlQuery.append("?,");	// 1 - DNA ID
		sqlQuery.append("?,");	// 2 - Status ID - Nope, now it is rateStatusId  The other two are down below

		sqlQuery.append("?,");	// 3 - Skills To Leverage Org Text
		sqlQuery.append("?,");	// 4 - Skills To Leverage Part Text

		sqlQuery.append("?,");	// 5 - Skills To Develop Org Text
		sqlQuery.append("?,");	// 6 - Skills To Develop Part Text

		sqlQuery.append("?,");	// 7 - Leadership Skill Org Text
		sqlQuery.append("?,");	// 8 - Leadership Skill Part Text
		sqlQuery.append("?,");	// 9 - Leadership Skill Rating

		sqlQuery.append("?,");	// 10 - Leadership Experience Org Text
		sqlQuery.append("?,");	// 11 - Leadership Experience Part Text
		sqlQuery.append("?,");	// 12 - Leadership Experience Rating

		sqlQuery.append("?,");	// 13 - Leadership Style Org Text
		sqlQuery.append("?,");	// 14 - Leadership Style Part Text
		sqlQuery.append("?,");	// 15 - Leadership Style Rating

		sqlQuery.append("?,");	// 16 - Leadership Interest Org Text
		sqlQuery.append("?,");	// 17 - Leadership Interest Part Text
		sqlQuery.append("?,");	// 18 - Leadership Interest Rating

		sqlQuery.append("?,");	// 19 - Derailment Org Text
		sqlQuery.append("?,");	// 20 - Derailment Part Text
		sqlQuery.append("?,");	// 21 - Derailment Rating

		sqlQuery.append("?,");	// 22 - Longterm Org Text
		sqlQuery.append("?,");	// 23 - Longterm Rating

		sqlQuery.append("?,");	// 24 - Fit Org Text

		sqlQuery.append("?,");	// 25 - Readiness Org Text

		sqlQuery.append("?,");	// 26 - Development Part Text

		sqlQuery.append("?,");	// 27 - Pivotal Part Text

		sqlQuery.append("?,");	// 28 - IDP Image Path
		sqlQuery.append("?,");	// 29 - IDP Document Path

		sqlQuery.append("?,");	// 30 - Additional Notes
		sqlQuery.append("?,");	// 31 - Last User Id
		sqlQuery.append("GETDATE(),");	// Date inserted by GETDATE() function
		sqlQuery.append("?,");	// 32 - Participant ID

		sqlQuery.append("?,");	// 33 - alrReadinessOrgText
		sqlQuery.append("?,");	// 34 - alrReadinessRating
		sqlQuery.append("?,");	// 35 - alrSugReadinessRating
		sqlQuery.append("?,");	// 36 - alrReadinessORAuth
		sqlQuery.append("?,");	// 37 -alrReadinessORJust

		sqlQuery.append("?,");	// 38 - alrCultureFitOrgText
		sqlQuery.append("?,");	// 39 - alrCultureFitPartText
		sqlQuery.append("?,");	// 40 - alrCultureFitRating
		sqlQuery.append("?,");	// 41 - alrSugCultureFitRating
		sqlQuery.append("?,");	// 42 - alrCultureFitORAuth
		sqlQuery.append("?,");	// 43 - alrCultureFitORJust
		
		sqlQuery.append("?,");	// 44 - oTxtStatusId
		sqlQuery.append("?");	// 45 - pTxtStatusId

		sqlQuery.append(")");

		DataLayerPreparedStatement dlps =  AbyDDatabaseUtils.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
		if (dlps.isInError())
		{
			throw new Exception("Error preparing report input INSERT.  Code=" +
					dlps.getStatus().getExceptionCode() +
					", Msg=" + dlps.getStatus().getExceptionMessage());
		}

		dlps.getPreparedStatement().setLong(1, reportInput.getDnaId());

		dlps.getPreparedStatement().setLong(2, reportInput.getRateStatusId());

		dlps.getPreparedStatement().setString(3, reportInput.getSkillsToLeverageOrgText());
		dlps.getPreparedStatement().setString(4, reportInput.getSkillsToLeveragePartText());

		dlps.getPreparedStatement().setString(5, reportInput.getSkillsToDevelopOrgText());
		dlps.getPreparedStatement().setString(6, reportInput.getSkillsToDevelopPartText());

		dlps.getPreparedStatement().setString(7, reportInput.getLeadershipSkillOrgText());
		dlps.getPreparedStatement().setString(8, reportInput.getLeadershipSkillPartText());
		dlps.getPreparedStatement().setInt(9, reportInput.getLeadershipSkillRating());

		dlps.getPreparedStatement().setString(10, reportInput.getLeadershipExperienceOrgText());
		dlps.getPreparedStatement().setString(11, reportInput.getLeadershipExperiencePartText());
		dlps.getPreparedStatement().setInt(12, reportInput.getLeadershipExperienceRating());

		dlps.getPreparedStatement().setString(13, reportInput.getLeadershipStyleOrgText());
		dlps.getPreparedStatement().setString(14, reportInput.getLeadershipStylePartText());
		dlps.getPreparedStatement().setInt(15, reportInput.getLeadershipStyleRating());

		dlps.getPreparedStatement().setString(16, reportInput.getLeadershipInterestOrgText());
		dlps.getPreparedStatement().setString(17, reportInput.getLeadershipInterestPartText());
		dlps.getPreparedStatement().setInt(18, reportInput.getLeadershipInterestRating());

		dlps.getPreparedStatement().setString(19, reportInput.getDerailmentOrgText());
		dlps.getPreparedStatement().setString(20, reportInput.getDerailmentPartText());
		dlps.getPreparedStatement().setInt(21, reportInput.getDerailmentRating());

		dlps.getPreparedStatement().setString(22, reportInput.getLongtermOrgText());
		dlps.getPreparedStatement().setInt(23, reportInput.getLongtermRating());

		dlps.getPreparedStatement().setString(24, reportInput.getFitOrgText());
////		dlps.getPreparedStatement().setInt(25, reportInput.getCandidateFitIndex());

		dlps.getPreparedStatement().setString(25, reportInput.getReadinessOrgText());

		dlps.getPreparedStatement().setString(26, reportInput.getDevelopmentPartText());

		dlps.getPreparedStatement().setString(27, reportInput.getPivotalPartText());
//Need more logic for the IDP image and doc
		dlps.getPreparedStatement().setString(28, "");
		dlps.getPreparedStatement().setString(29, "");

//new additional information on DRI
		dlps.getPreparedStatement().setString(30, reportInput.getAdditionalNotes());
		dlps.getPreparedStatement().setString(31, "100");
		//date inserted via sql function GETDATE() above
		dlps.getPreparedStatement().setString(32, reportInput.getParticipantId());
		
		// ALR stuff
		dlps.getPreparedStatement().setString(33, reportInput.getAlrReadinessOrgText());
		dlps.getPreparedStatement().setInt(34, reportInput.getAlrReadinessRating());
		dlps.getPreparedStatement().setInt(35, reportInput.getAlrSugReadinessRating());
		dlps.getPreparedStatement().setString(36, reportInput.getAlrReadinessORAuth());
		dlps.getPreparedStatement().setString(37, reportInput.getAlrReadinessORJust());

		dlps.getPreparedStatement().setString(38, reportInput.getAlrCultureFitOrgText());
		dlps.getPreparedStatement().setString(39, reportInput.getAlrCultureFitPartText());
		dlps.getPreparedStatement().setInt(40, reportInput.getAlrCultureFitRating());
		dlps.getPreparedStatement().setInt(41, reportInput.getAlrSugCultureFitRating());
		dlps.getPreparedStatement().setString(42, reportInput.getAlrCultureFitORAuth());
		dlps.getPreparedStatement().setString(43, reportInput.getAlrCultureFitORJust());

		dlps.getPreparedStatement().setLong(44, reportInput.getOtxtStatusId());
		dlps.getPreparedStatement().setLong(45, reportInput.getPtxtStatusId());

		DataResult dr =  null;
		try {
			dr =  AbyDDatabaseUtils.insert(dlps);
			if (dr.isInError())
			{
				throw new Exception("Error inserting ReportInput data.  Code=" +
						dr.getStatus().getExceptionCode() +
						", Msg=" + dr.getStatus().getExceptionMessage());
			}
	
			long id = dr.getNewId();
			return id;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (dr != null) {  dr.close();  dr = null; }
		}
		return 0;


	}


	/**
	 * Update a report input data row; note that all data columns are updated;  data
	 * not changing must be retained and sent back so that it is properly updated.
	 *
	 * @param lpId - The lpId used in the integration grid invocation.  Used in the
	 * 				 e-mail send to SAS when the status goes from "In Progress (1)
	 * 				 to "Ready for Editing" (2).  Note that this value can be null,
	 * 				 in which case the sendEmail flag will remain false.
	 * @param reportInput - A ReportInputDTO object.  All desired data must be present
	 *                      because all column data is replaced by this update.
	 * @throws Exception
	 */
	public void updateReportInput(String lpId, ReportInputDTO reportInput)
		throws Exception
	{
		boolean sendOrgEmail = false;
		boolean sendPptEmail = false;
		boolean orgLockCand = false;
		boolean pptLockCand = false;
		boolean unlockRR = false;

		// TODO Refactor to fetch all of the existing (DB) status so each piece/method call of the following logic doesn't have to
		// Check status at the top (before it gets changed)
		if (lpId != null)	// If it's null, we can't send the e-mail anyway
		{
			// if the status is "Ready For Editing" see what it is currently
			if (reportInput.getOtxtStatusId() == ReportInputDTO.READY_FOR_EDITING)
			{
				// If additional conditions are required, this may be modified...
				sendOrgEmail = checkRiInProgOrComp(reportInput.getDnaId(), reportInput.getParticipantId(), ORG_TEXT);
			}
			if (reportInput.getPtxtStatusId() == ReportInputDTO.READY_FOR_EDITING)
			{
				// If additional conditions are required, this may be modified...
				sendPptEmail = checkRiInProgOrComp(reportInput.getDnaId(), reportInput.getParticipantId(), PART_TEXT);
			}
		}
		//System.out.println(reportInput.getDnaId() + " = " + reportInput.getLeadershipSkillRating());
		
		// if the status is "complete" see what it is now (in the database)
		if (reportInput.getOtxtStatusId() == ReportInputDTO.COMPLETED)
		{
			orgLockCand = checkRiDataUnLocked(reportInput.getParticipantId(), reportInput.getDnaId(), ORG_TEXT);
		}
		if (reportInput.getPtxtStatusId() == ReportInputDTO.COMPLETED)
		{
			pptLockCand = checkRiDataUnLocked(reportInput.getParticipantId(), reportInput.getDnaId(), PART_TEXT);
		}
		
		// "Soft unlock" of RR required?
		// Both have to come in as "In progress" to consider unlocking RR
		if (reportInput.getOtxtStatusId() == ReportInputDTO.IN_PROGRESS  &&  reportInput.getPtxtStatusId() == ReportInputDTO.IN_PROGRESS)
		{
			// See if either of the texts were "Ready to Edit"  if so, unlock rr
			unlockRR = checkTxtInEdit(reportInput.getParticipantId(), reportInput.getDnaId());
		}
		
		// Recalc the ALR Readiness and Fit rating if needed
		checkAlrRatingRecalc(reportInput);
		
		// Clean up justification and authorization and other stuff
		checkIntegrity(reportInput);

		// do the update
		StringBuffer sqlQuery = new StringBuffer();
		//update pdi_abd_rpt_input set statusId = ?, candidateFitIndex = ?, skillsToLeverageOrgText=? ... where dnaId = ?
		sqlQuery.append("UPDATE pdi_abd_rpt_input ");
		sqlQuery.append("  SET ");

		sqlQuery.append("    rateStatusId = ?,");					// 1

		sqlQuery.append("    skillsToLeverageOrgText = ?,");		// 2
		sqlQuery.append("    skillsToLeveragePartText = ?,");		// 3

		sqlQuery.append("    skillsToDevelopOrgText = ?,");			// 4
		sqlQuery.append("    skillsToDevelopPartText = ?,");		// 5

		sqlQuery.append("    leadershipSkillOrgText = ?,");			// 6
		sqlQuery.append("    leadershipSkillPartText = ?,");		// 7
		sqlQuery.append("    leadershipSkillRating = ?,");			// 8

		sqlQuery.append("    leadershipExperienceOrgText = ?,");	// 9
		sqlQuery.append("    leadershipExperiencePartText = ?,");	// 10
		sqlQuery.append("    leadershipExperienceRating = ?,");		// 11

		sqlQuery.append("    leadershipStyleOrgText = ?,");			// 12
		sqlQuery.append("    leadershipStylePartText = ?,");		// 13
		sqlQuery.append("    leadershipStyleRating = ?,");			// 14

		sqlQuery.append("    leadershipInterestOrgText = ?,");		// 15
		sqlQuery.append("    leadershipInterestPartText = ?,");		// 16
		sqlQuery.append("    leadershipInterestRating = ?,");		// 17

		sqlQuery.append("    derailmentOrgText = ?,");				// 18
		sqlQuery.append("    derailmentPartText = ?,");				// 19
		sqlQuery.append("    derailmentRating = ?,");				// 20

		sqlQuery.append("    longtermOrgText = ?,");				// 21
		sqlQuery.append("    longtermRating = ?,");					// 22

		sqlQuery.append("    fitOrgText = ?,");						// 23
//		sqlQuery.append("    candidateFitIndex = ?,");

		sqlQuery.append("    readinessOrgText = ?,");				// 24
		sqlQuery.append("    developmentPartText = ?,");			// 25
		sqlQuery.append("    pivotalPartText = ?,");				// 26

		sqlQuery.append("    idpImagePath = ?,");					// 27
		sqlQuery.append("    idpDocumentPath = ?,");				// 28
		
		sqlQuery.append("    additionalNotes = ?,");				// 29

		sqlQuery.append("    lastUserId = ?,");						// 30
		sqlQuery.append("    lastDate = GETDATE(),");				// No variable associated/ Not a user-inserted parameter
		
		sqlQuery.append("    alrReadinessOrgText = ?,");			// 31
		sqlQuery.append("    alrReadinessRating = ?,");				// 32
		sqlQuery.append("    alrSugReadinessRating = ?,");			// 33
		sqlQuery.append("    alrReadinessORAuth = ?,");				// 34
		sqlQuery.append("    alrReadinessORJust = ?,");				// 35
		
		sqlQuery.append("    alrCultureFitOrgText = ?,");			// 36
		sqlQuery.append("    alrCultureFitPartText = ?,");			// 37
		sqlQuery.append("    alrCultureFitRating = ?,");			// 38
		sqlQuery.append("    alrSugCultureFitRating = ?,");			// 39
		sqlQuery.append("    alrCultureFitORAuth = ?,");			// 40
		sqlQuery.append("    alrCultureFitORJust = ?,");			// 41

		
		
		
		sqlQuery.append("     oTxtStatusId = ?,");					// 42
		sqlQuery.append("     pTxtStatusId = ?");					// 43

		sqlQuery.append("  WHERE dnaId = ?");						// 44
		sqlQuery.append("    AND participantId = ?");				// 45

		DataResult dr = null;
		try
		{
			DataLayerPreparedStatement dlps =  AbyDDatabaseUtils.prepareStatement(sqlQuery);
			if (dlps.isInError())
			{
				return;
			}
	
			dlps.getPreparedStatement().setLong(1, reportInput.getRateStatusId());
	
			dlps.getPreparedStatement().setString(2, reportInput.getSkillsToLeverageOrgText());
			dlps.getPreparedStatement().setString(3, reportInput.getSkillsToLeveragePartText());
	
			dlps.getPreparedStatement().setString(4, reportInput.getSkillsToDevelopOrgText());
			dlps.getPreparedStatement().setString(5, reportInput.getSkillsToDevelopPartText());
	
			dlps.getPreparedStatement().setString(6, reportInput.getLeadershipSkillOrgText());
			dlps.getPreparedStatement().setString(7, reportInput.getLeadershipSkillPartText());
			dlps.getPreparedStatement().setInt(8, reportInput.getLeadershipSkillRating());
	
			//System.out.println("The leadership competency rating is ... " + reportInput.getLeadershipSkillRating());
	
			dlps.getPreparedStatement().setString(9, reportInput.getLeadershipExperienceOrgText());
			dlps.getPreparedStatement().setString(10, reportInput.getLeadershipExperiencePartText());
			dlps.getPreparedStatement().setInt(11, reportInput.getLeadershipExperienceRating());
	
			dlps.getPreparedStatement().setString(12, reportInput.getLeadershipStyleOrgText());
			dlps.getPreparedStatement().setString(13, reportInput.getLeadershipStylePartText());
			dlps.getPreparedStatement().setInt(14, reportInput.getLeadershipStyleRating());
	
			dlps.getPreparedStatement().setString(15, reportInput.getLeadershipInterestOrgText());
			dlps.getPreparedStatement().setString(16, reportInput.getLeadershipInterestPartText());
			dlps.getPreparedStatement().setInt(17, reportInput.getLeadershipInterestRating());
	
			dlps.getPreparedStatement().setString(18, reportInput.getDerailmentOrgText());
			dlps.getPreparedStatement().setString(19, reportInput.getDerailmentPartText());
			dlps.getPreparedStatement().setInt(20, reportInput.getDerailmentRating());
	
			dlps.getPreparedStatement().setString(21, reportInput.getLongtermOrgText());
			dlps.getPreparedStatement().setInt(22, reportInput.getLongtermRating());
	
			dlps.getPreparedStatement().setString(23, reportInput.getFitOrgText());
	////		dlps.getPreparedStatement().setInt(24, reportInput.getCandidateFitIndex());
	
			dlps.getPreparedStatement().setString(24, reportInput.getReadinessOrgText());
			dlps.getPreparedStatement().setString(25, reportInput.getDevelopmentPartText());
			dlps.getPreparedStatement().setString(26, reportInput.getPivotalPartText());
	
			dlps.getPreparedStatement().setString(27, reportInput.getIdpImagePath());
			dlps.getPreparedStatement().setString(28, reportInput.getIdpDocumentPath());
			
			dlps.getPreparedStatement().setString(29, reportInput.getAdditionalNotes());
	//lastUserId is being hard coded here...
			dlps.getPreparedStatement().setString(30, "300");

			dlps.getPreparedStatement().setString(31, reportInput.getAlrReadinessOrgText());
			dlps.getPreparedStatement().setInt(32, reportInput.getAlrReadinessRating());
			dlps.getPreparedStatement().setInt(33, reportInput.getAlrSugReadinessRating());
			dlps.getPreparedStatement().setString(34, reportInput.getAlrReadinessORAuth());
			dlps.getPreparedStatement().setString(35, reportInput.getAlrReadinessORJust());

			dlps.getPreparedStatement().setString(36, reportInput.getAlrCultureFitOrgText());
			dlps.getPreparedStatement().setString(37, reportInput.getAlrCultureFitPartText());
			dlps.getPreparedStatement().setInt(38, reportInput.getAlrCultureFitRating());
			dlps.getPreparedStatement().setInt(39, reportInput.getAlrSugCultureFitRating());
			dlps.getPreparedStatement().setString(40, reportInput.getAlrCultureFitORAuth());
			dlps.getPreparedStatement().setString(41, reportInput.getAlrCultureFitORJust());

			dlps.getPreparedStatement().setLong(42, reportInput.getOtxtStatusId());
			dlps.getPreparedStatement().setLong(43, reportInput.getPtxtStatusId());

			dlps.getPreparedStatement().setLong(44, reportInput.getDnaId());
			dlps.getPreparedStatement().setString(45, reportInput.getParticipantId());

			dr = AbyDDatabaseUtils.update(dlps);
			if (dr.isInError())
			{
				throw new Exception("Error updating ReportInput data.  Code=" +
						dr.getStatus().getExceptionCode() +
						", Msg=" + dr.getStatus().getExceptionMessage());
			}

			// The status is OK; do all of the rest of the stuff that is needed.
			// Need to unlock Rates and recommends?
			if (unlockRR)
			{
				unlockRR(reportInput.getParticipantId(),reportInput.getDnaId());
			}

			// Need to send e-mails?
			if (sendOrgEmail)
			{
				// Send the e-mail
				sendRiEmail(reportInput.getParticipantId(),reportInput.getDnaId(), lpId, ORG_TEXT);
				// and lock the ratings
				setRiLockDate(reportInput.getDnaId(), reportInput.getParticipantId(), RATINGS);
			}
			if (sendPptEmail)
			{
				// Send the e-mail
				sendRiEmail(reportInput.getParticipantId(),reportInput.getDnaId(), lpId, PART_TEXT);
				// and lock the ratings
				setRiLockDate(reportInput.getDnaId(), reportInput.getParticipantId(), RATINGS);
			}
	
			// if the status "complete" see if we need to set the lock dates
			if (reportInput.getOtxtStatusId() == ReportInputDTO.COMPLETED  &&  orgLockCand)
			{
					setRiLockDate(reportInput.getDnaId(), reportInput.getParticipantId(), ORG_TEXT);
					setRiLockDate(reportInput.getDnaId(), reportInput.getParticipantId(), RATINGS);
					ResearchExtractHelper.setupResearchExtractCandidate(reportInput);
			}
			if (reportInput.getPtxtStatusId() == ReportInputDTO.COMPLETED  &&  pptLockCand)
			{
					setRiLockDate(reportInput.getDnaId(), reportInput.getParticipantId(), PART_TEXT);
					setRiLockDate(reportInput.getDnaId(), reportInput.getParticipantId(), RATINGS);
					ResearchExtractHelper.setupResearchExtractCandidate(reportInput);
			}

// Per SBA-141 we need to make it an expot candidate when either of the text statuses go complete
//			// If status is going to COMPLETED, make it an export candidate
//			if (reportInput.getCompositeStatusId() == (long)ReportInputDTO.COMPLETED)
//			{
//				// Go do the extract data capture
//				ResearchExtractHelper.setupResearchExtractCandidate(reportInput);
//			}
		}
		catch (Exception e)
		{
			System.out.println("Exception caught in DRI Update.  msg = " + e.getMessage());
			e.printStackTrace();
			throw new Exception(e);	// Pass it along
		}
		finally
		{
			if (dr != null) {  dr.close();  dr = null; }
		}
	}


	/**
	 * unlockRR - Set the rate/recommend status to "In Progress" and set the unlock date
	 * @param participantId
	 * @param dnaId
	 * @throws Exception
	 */
	private void unlockRR(String participantId, long dnaId)
		throws Exception
	{
		DataResult dr = null;
		try
		{
			// do the update
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("UPDATE pdi_abd_rpt_input ");
			sqlQuery.append("  SET ");
			sqlQuery.append("    rateStatusId = " +  ReportInputDTO.IN_PROGRESS + ",");
			sqlQuery.append("    rateUnlockDate = GETDATE() ");
			sqlQuery.append("  WHERE dnaId = " + dnaId + " ");
			sqlQuery.append("    AND participantId = " + participantId);
	
			DataLayerPreparedStatement dlps =  AbyDDatabaseUtils.prepareStatement(sqlQuery);
			if (dlps.isInError())
			{
				throw new Exception("Error preparing in data update (unlockRR).  Code=" +
							dlps.getStatus().getExceptionCode() +
							", Msg=" + dlps.getStatus().getExceptionMessage());
			}
	
			dr =  AbyDDatabaseUtils.update(dlps);
			if (dr.isInError())
			{
				throw new Exception("Error updating data (unlockRR).  Code=" +
						dlps.getStatus().getExceptionCode() +
						", Msg=" + dlps.getStatus().getExceptionMessage());
			}
	
			return;
		}
		finally
		{
			if (dr != null) {  dr.close();  dr = null; }
		}
	}
	/**
	 * Update last date.  This is needed for the Regen Process so that the Data Warehouse feed will resend everything instead of just the extract xml
	 * @param participantId
	 * @param dnaId
	 * @throws Exception
	 */
	public void updateReportInputLastDate(String participantId, long dnaId)
			throws Exception
		{
			DataResult dr = null;
			try
			{
				// do the update
				StringBuffer sqlQuery = new StringBuffer();
				sqlQuery.append("UPDATE pdi_abd_rpt_input ");
				sqlQuery.append("  SET ");
				sqlQuery.append("    lastDate = GETDATE() ");
				sqlQuery.append("  WHERE dnaId = " + dnaId + " ");
				sqlQuery.append("    AND participantId = " + participantId);
		
				DataLayerPreparedStatement dlps =  AbyDDatabaseUtils.prepareStatement(sqlQuery);
				if (dlps.isInError())
				{
					throw new Exception("Error preparing in data update (unlockRR).  Code=" +
								dlps.getStatus().getExceptionCode() +
								", Msg=" + dlps.getStatus().getExceptionMessage());
				}
		
				dr =  AbyDDatabaseUtils.update(dlps);
				if (dr.isInError())
				{
					throw new Exception("Error updating data (unlockRR).  Code=" +
							dlps.getStatus().getExceptionCode() +
							", Msg=" + dlps.getStatus().getExceptionMessage());
				}
		
				return;
			}
			finally
			{
				if (dr != null) {  dr.close();  dr = null; }
			}
		}


	/**
	 * Check to see if the designated report input is in progress or completed.  If so return true. modified for NHN-1781-kyamry
	 *
	 * @param dnaId - The dnaID
	 * @param dnaId - The participant ID
	 * @throws Exception
	 */

	private boolean checkRiInProgOrComp(long dnaId, String participantId, String type)
		throws Exception
	{
		String selStr = null;
		if (type.equals(ORG_TEXT))
		{
			selStr = "oTxtStatusId";
		}
		else if (type.equals(PART_TEXT))
		{
			selStr = "pTxtStatusId";
		}
		else
		{
			throw new Exception("Invalid status type (" + type + ") in checkRiInProgOrComp()");
		}

		DataResult dr = null;
		try
		{
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT  ari." + selStr + " ");
			sqlQuery.append("  FROM pdi_abd_rpt_input ari ");
			sqlQuery.append("  WHERE ari.dnaId = " + dnaId + " ");
			sqlQuery.append("    AND ari.participantId = '" + participantId + "'");

			DataLayerPreparedStatement dlps =  AbyDDatabaseUtils.prepareStatement(sqlQuery);
			if (dlps.isInError())
			{
				throw new Exception("Error preparing in data fetch (checkRiInProgOrComp).  Code=" +
							dlps.getStatus().getExceptionCode() +
							", Msg=" + dlps.getStatus().getExceptionMessage());
			}

			dr =  AbyDDatabaseUtils.select(dlps);
			if (dr.isInError())
			{
				throw new Exception("Error fetching RI status (checkRiInProgOrComp).  Code=" +
						dlps.getStatus().getExceptionCode() +
						", Msg=" + dlps.getStatus().getExceptionMessage());
			}

			// should be a single row
			ResultSet rs = dr.getResultSet();
			rs.next();

			long stat = rs.getLong(selStr);
			return (stat == ReportInputDTO.IN_PROGRESS || stat == ReportInputDTO.COMPLETED) ? true : false;
		}
		finally
		{
			if (dr != null) {  dr.close();  dr = null; }
		}
	}


	/**
	 * Check to see if the designated report input is not currently completed/locked.  If so return true.
	 *
	 * @param dnaId - The dnaID
	 * @param participantId - The participant ID
	 * @param type - =What data are we looking at (OT, PT or RR)
	 * @throws Exception
	 */
	private boolean checkRiDataUnLocked(String participantId, long dnaId, String type)
		throws Exception
	{
		String selStr = null;
		if (type.equals(ORG_TEXT))
		{
			selStr = "oTxtStatusId";
		}
		else if (type.equals(PART_TEXT))
		{
			selStr = "pTxtStatusId";
		}
		else
		{
			throw new Exception("Invalid staus type (" + type + ") in checkRiDataUnLocked()");
		}

		DataResult dr = null;
		try
		{
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT  ari." + selStr + " ");
			sqlQuery.append("  FROM pdi_abd_rpt_input ari ");
			sqlQuery.append("  WHERE ari.dnaId = " + dnaId + " ");
			sqlQuery.append("    AND ari.participantId = '" + participantId + "'");

			DataLayerPreparedStatement dlps =  AbyDDatabaseUtils.prepareStatement(sqlQuery);
			if (dlps.isInError())
			{
				throw new Exception("Error preparing in data fetch (checkRiDataUnLocked).  Type=" + type + ", Code=" +
							dlps.getStatus().getExceptionCode() +
							", Msg=" + dlps.getStatus().getExceptionMessage());
			}

			dr =  AbyDDatabaseUtils.select(dlps);
			if (dr.isInError())
			{
				throw new Exception("Error fetching data (checkRiDataUnLocked).  Type=" + type + ", Code=" +
						dlps.getStatus().getExceptionCode() +
						", Msg=" + dlps.getStatus().getExceptionMessage());
			}

			// should be a single row
			ResultSet rs = dr.getResultSet();
			rs.next();

			long stat = rs.getLong(selStr);
			// if the current status is COMPLETED then it is NOT unlocked
			return (stat == ReportInputDTO.COMPLETED) ? false : true;
		}
		finally
		{
			if (dr != null) {  dr.close();  dr = null; }
		}
	}


	/**
	 * checkTxtInEdit - Checks to see if one or the other text is in edit status (current db state)
	 * 
	 * @param participantId
	 * @param dnaId
	 * @return - true if either (or both) texts are in editing in the database row
	 * @throws Exception
	 */
	private boolean checkTxtInEdit(String participantId, long dnaId)
		throws Exception
	{
		DataResult dr = null;
		try
		{
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT ri.oTxtStatusId, ");
			sqlQuery.append("       ri.pTxtStatusId ");
			sqlQuery.append("  FROM pdi_abd_rpt_input ri ");
			sqlQuery.append("  WHERE ri.participantId = '" + participantId + "' "); 
			sqlQuery.append("    AND ri.dnaId = " + dnaId);

			DataLayerPreparedStatement dlps =  AbyDDatabaseUtils.prepareStatement(sqlQuery);
			if (dlps.isInError())
			{
				throw new Exception("Error preparing in data fetch (checkTxtInEdit).  Error code=" +
									dlps.getStatus().getExceptionCode() +
									", Msg=" + dlps.getStatus().getExceptionMessage());
			}

			dr =  AbyDDatabaseUtils.select(dlps);
			if (dr.isInError())
			{
				throw new Exception("Error fetching data (checkTxtInEdit).  Error code=" +
									dlps.getStatus().getExceptionCode() +
									", Msg=" + dlps.getStatus().getExceptionMessage());
			}

			// should be a single row
			ResultSet rs = dr.getResultSet();
			rs.next();

			int otxtStat = rs.getInt("oTxtStatusId");
			int ptxtStat = rs.getInt("pTxtStatusId");
			return (otxtStat == ReportInputDTO.READY_FOR_EDITING  ||  ptxtStat == ReportInputDTO.READY_FOR_EDITING);
		}
		finally
		{
			if (dr != null) {  dr.close();  dr = null; }
		}
	}	


	/**
	 * setRiLockDate Set the appropriate lock date
	 * @param dnaId
	 * @param participantId
	 * @param type
	 * @throws Exception
	 */
	private void setRiLockDate(long dnaId, String participantId, String type)
		throws Exception
	{
		// Note that we NOT the ORed conditions;  If any of there are there then there is a valid type
		if (! (type.equals(ORG_TEXT) || type.equals(PART_TEXT) || type.equals(RATINGS)))
		{
			throw new Exception("Invalid staus type (" + type + ") in lockRiData()");
		}

		DataResult dr = null;
		try
		{
			// do the update
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("UPDATE pdi_abd_rpt_input ");
			sqlQuery.append("  SET ");
			if (type.equals(ORG_TEXT))
			{
				sqlQuery.append("    oTxtStatusId = " +  ReportInputDTO.COMPLETED + ",");
				sqlQuery.append("    oTxtLockDate = GETDATE()");
			}
			else if (type.equals(PART_TEXT))
			{
				sqlQuery.append("    pTxtStatusId = " +  ReportInputDTO.COMPLETED + ",");
				sqlQuery.append("    pTxtLockDate = GETDATE()");
			}
			else if (type.equals(RATINGS))
			{
				sqlQuery.append("    rateStatusId = " +  ReportInputDTO.COMPLETED + ",");
				sqlQuery.append("    rateLockDate = GETDATE() ");
			}
			sqlQuery.append("  WHERE dnaId = " + dnaId + " ");
			sqlQuery.append("    AND participantId = " + participantId);

			DataLayerPreparedStatement dlps =  AbyDDatabaseUtils.prepareStatement(sqlQuery);
			if (dlps.isInError())
			{
				throw new Exception("Error preparing in data update (setRiLockDate).  Type=" + type + ", Code=" +
							dlps.getStatus().getExceptionCode() +
							", Msg=" + dlps.getStatus().getExceptionMessage());
			}

			dr =  AbyDDatabaseUtils.update(dlps);
			if (dr.isInError())
			{
				throw new Exception("Error updating data (setRiLockDate).  Type=" + type + ", Code=" +
						dlps.getStatus().getExceptionCode() +
						", Msg=" + dlps.getStatus().getExceptionMessage());
			}

			return;
		}
		finally
		{
			if (dr != null) {  dr.close();  dr = null; }
		}
	}


	/**
	 * checkAlrRatingRecalc - Check the data and if this is an ALR model, check the data for changes in
	 *                        Skill or Experience ratings.  If changed, recalc the Readiness rating.
	 * @param ri - The ReportInputDTO in question
	 * @throws Exception
	 */
	private void checkAlrRatingRecalc(ReportInputDTO ri)
		throws Exception
	{
		DataResult dr = null;
		
		if (! ri.getIsAlr())
		{
			// only do this if it is an ALR model
			return;
		}
		
		try
		{
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT ari.leadershipSkillRating, ");
			sqlQuery.append("       ari.leadershipExperienceRating ");
			sqlQuery.append("  FROM pdi_abd_rpt_input ari ");
			sqlQuery.append("  WHERE ari.dnaId = " + ri.getDnaId() + " ");
			sqlQuery.append("    AND ari.participantId = '" + ri.getParticipantId() + "'");

			DataLayerPreparedStatement dlps =  AbyDDatabaseUtils.prepareStatement(sqlQuery);
			if (dlps.isInError())
			{
				throw new Exception("Error preparing in data fetch (IP status Check).  Code=" +
							dlps.getStatus().getExceptionCode() +
							", Msg=" + dlps.getStatus().getExceptionMessage());
			}

			dr =  AbyDDatabaseUtils.select(dlps);
			if (dr.isInError())
			{
				if(dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
				{
					// No row yet... nothing to do here
					return;
				}
				else
				{
					throw new Exception("Error fetching Report Input data.  Code=" +
					dlps.getStatus().getExceptionCode() +
					", Msg=" + dlps.getStatus().getExceptionMessage());
				}
			}

			// should be a single row
			ResultSet rs = dr.getResultSet();
			rs.next();

			long comp = rs.getLong("leadershipSkillRating");
			long exp = rs.getLong("leadershipExperienceRating");
			if (ri.getLeadershipSkillRating() == comp &&
				ri.getLeadershipExperienceRating() == exp)
			{
				// Nothing to do
				return;
			}
			
			// Do the recalc and overwrite the suggested Readiness and Fit values in the RI DTO
			//calcAlrReadiness(ri);
			//calcAlrCultureFit(ri);
			getSugFitAndReadiness(ri);

			return;
		}
		finally
		{
			if (dr != null) {  dr.close();  dr = null; }
		}
	}


	/**
	 * checkIntegrity - validates the override data - Readiness assessment only
	 * @param ri
	 */
	private void checkIntegrity(ReportInputDTO ri)
	{
		String auth = null;
		String just = null;
		// First check Fit
		auth = ri.getAlrCultureFitORAuth();
		if (auth == null)
			auth = "";
		just = ri.getAlrCultureFitORJust();
		if (just == null)
			just = "";
		if (auth.isEmpty() || just.isEmpty())
		{
			ri.setAlrCultureFitORAuth(null);
			ri.setAlrCultureFitORJust(null);
			ri.setAlrCultureFitRating(ri.getAlrSugCultureFitRating());
		}

		// Then check readiness
		auth = ri.getAlrReadinessORAuth();
		if (auth == null)
			auth = "";
		just = ri.getAlrReadinessORJust();
		if (just == null)
			just = "";
		if (auth.isEmpty() || just.isEmpty())
		{
			ri.setAlrReadinessORAuth(null);
			ri.setAlrReadinessORJust(null);
			ri.setAlrReadinessRating(ri.getAlrSugReadinessRating());
		}
		
		// Additional stuff
		// Fill the reported with the suggested, if reported is empty
		// This may be covered by the above but just to be safe...
		if(ri.getAlrCultureFitRating() == 0  && ri.getAlrSugCultureFitRating() != 0)
		{
			ri.setAlrCultureFitRating(ri.getAlrSugCultureFitRating());
		}
		if(ri.getAlrReadinessRating() == 0  && ri.getAlrSugReadinessRating() != 0)
		{
			ri.setAlrReadinessRating(ri.getAlrSugReadinessRating());
		}
	}


	/**
	 * calcAlrReadiness - Recalculates the ALR Readiness rating in place
	 */
	private void calcAlrReadiness(ReportInputDTO ri)
	{
		int rating = 0;	// Insuficient Data
		
		long comp = ri.getLeadershipSkillRating();
		long exp = ri.getLeadershipExperienceRating();
		
		if (comp == 0 || exp == 0)
		{
			// One value or the other is 0... set rating to 0
			ri.setAlrSugReadinessRating(rating);
			return;
		}
		if (comp > 4  || exp > 4)
		{
			// invalid values... do nothing here
			return;
		}
		
		// OK lets calculate
		if (comp == 4)	// VERY STRONG
		{
			if (exp == 1)	// WEAK
			{
				rating = YELLOW;	// value = 2
			}
			else if (exp == 2)	// MIXED
			{
				// rating = GREEN;	// value = 3	Changed per SBA-490
				rating = YELLOW;	// value = 2
			}
			else if (exp == 3 || exp == 4)	// STRONG or VERY STRONG
			{
				rating = BLUE;	// value = 4
			}
		}
		else if (comp == 3)	// STRONG
		{
			if (exp == 1 || exp == 2)	// WEAK or MIXED
			{
				rating = YELLOW;	// value = 2
			}
			else if (exp == 3)	// STRONG
			{
				rating = GREEN;		// value = 3
			}
			else if (exp == 4)	// VERY STRONG
			{
				//rating = BLUE;	// value = 4	Changed per SBA-490
				rating = GREEN;	// value = 3
			}
		}
		else if (comp == 2)	// value = 2
		{
			if (exp == 1)	// WEAK
			{
				rating = ORANGE;	// value = 1
			}
			else if (exp == 2 || exp == 3 || exp == 4)
			{
				rating = YELLOW;	// value = 2
			}
		}
		else if (comp == 1)	// value = 1
		{
			// comp must be WEAK (value = 1)
			if (exp == 1 || exp == 2 || exp == 3 || exp == 4)
			{
				rating = ORANGE;	// value = 1
			}
		}

		ri.setAlrSugReadinessRating(rating);
		
		return;
	}


		/**
		 * calcAlrCultureFit - Recalculates the ALR Fit rating in place
		 * 
		 * NOTE:  This is the same as Readiness, but separated out because
		 *		  they may diverge in the future
		 */
		private void calcAlrCultureFit(ReportInputDTO ri)
		{
			int rating = 0;	// Insuficient Data
			
			long comp = ri.getLeadershipSkillRating();
			long exp = ri.getLeadershipExperienceRating();
			
			if (comp == 0 || exp == 0)
			{
				// One value or the other is 0... set rating to 0
				ri.setAlrSugCultureFitRating(rating);
				return;
			}
			if (comp > 4  || exp > 4)
			{
				// invalid values... do nothing here
				return;
			}
			
			// OK lets calculate
			if (comp == 4)	// VERY STRONG
			{
				if (exp == 1 || exp == 2)	// WEAK or MIXED
				{
					rating = YELLOW;	// value = 2
				}
				else if (exp == 3 || exp == 4)	// STRONG or VERY STRONG
				{
					rating = BLUE;	// value = 4
				}
			}
			else if (comp == 3)	// STRONG
			{
				if (exp == 1 || exp == 2)	// WEAK or MIXED
				{
					rating = YELLOW;	// value = 2
				}
				else if (exp == 3  || exp == 4)	// STRONG or VERY STRONG
				{
					rating = GREEN;		// value = 3
				}
			}
			else if (comp == 2)	// value = 2
			{
				if (exp == 1)	// WEAK
				{
					rating = ORANGE;	// value = 1
				}
				else if (exp == 2 || exp == 3 || exp == 4)
				{
					rating = YELLOW;	// value = 2
				}
			}
			else if (comp == 1)	// value = 1
			{
				// comp must be WEAK (value = 1)
				if (exp == 1 || exp == 2 || exp == 3 || exp == 4)
				{
					rating = ORANGE;	// value = 1
				}
			}

			ri.setAlrSugCultureFitRating(rating);
			
			return;
		}


	/**
	 * Email notification
	 *
	 * @param participantId - The participant ID
	 * @param dnaId - The dnaID
	 * @param lpId - the lpId for the URL string
	 * @throws Exception
	 */
	private void sendRiEmail(String participantId, long dnaId, String lpId, String type)
		throws Exception
	{
		DataResult dr = null;
		String projectId;
		String clientName = null;
		String leadName = null;
		StringBuffer sqlQuery = new StringBuffer();
		String typeText = "";
		
		if (type.equals(ORG_TEXT))
		{
			typeText = ORG_FACING_TEXT;
		}
		else if (type.equals(PART_TEXT))
		{
			typeText = PPT_FACING_TEXT;
		}
		else
		{
			throw new Exception("Invalid text type (" + type + ") in sendRiEmail().");
		}

		// Get the participant name
		String partName = 	getPartName(getPartInfo(participantId));
		sqlQuery.append("SELECT dna.projectId, ");
		sqlQuery.append("       air.leadFirstName, ");
		sqlQuery.append("       air.leadLastName ");
		sqlQuery.append("  FROM pdi_abd_dna dna ");
		sqlQuery.append("    LEFT JOIN pdi_abd_igrid_response air ON (air.dnaId = dna.dnaId ");
		sqlQuery.append("                                         AND air.participantId = " + participantId + ") ");
		sqlQuery.append("  WHERE dna.dnaId = " + dnaId);
		try
		{
			DataLayerPreparedStatement dlps =  AbyDDatabaseUtils.prepareStatement(sqlQuery);
			if (dlps.isInError())
			{
				throw new Exception("Error preparing in email misc data fetch.  Code=" +
							dlps.getStatus().getExceptionCode() +
							", Msg=" + dlps.getStatus().getExceptionMessage());
			}

			dr =  AbyDDatabaseUtils.select(dlps);
			if (dr.isInError())
			{
				throw new Exception("Error fetching email misc data .  Code=" +
						dlps.getStatus().getExceptionCode() +
						", Msg=" + dlps.getStatus().getExceptionMessage());
			}

			// should be a single row
			ResultSet rs = dr.getResultSet();
			rs.next();

			projectId = rs.getString("projectId");
			String str = rs.getString("leadFirstName");
			leadName = (str == null ? "-" : str) + " ";
			str = rs.getString("leadLastName");
			leadName += (str == null ? "-" : str);
		}
		finally
		{
			if (dr != null) {  dr.close();  dr = null; }
		}
		
		// Now get the client name
		SessionUser session = new SessionUser();
		IProjectHelper ipHelper = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated"); 
		Project project = ipHelper.fromId(session, projectId);
		clientName = project.getClient().getName();

		// Check for subject and text
		String emailSubject = PropertyLoader.getProperty("com.pdi.data.abyd.application", "abd.dna.emailSubject.statusToEdit");

		if (emailSubject == null)
		{
			throw new Exception("No subject text for text type 'statusToEdit'");
		}
		String emailBody = PropertyLoader.getProperty("com.pdi.data.abyd.application", "abd.dna.emailText.statusToEdit");
		if (emailBody == null)
		{
			throw new Exception("No body text for text type 'statusToEdit'");
		}

		// Get the igURL fragment and build the full URL
		// Old method got from properties file... use new method that gets from global
		//String url = PropertyLoader.getProperty("com.pdi.data.abyd.application", "abd.ig.uiServer");
		//url += lpId;
				
		//String url = PropertyLoader.getProperty("application", "global.url");	// from pdi-web application.properties
		String url = PropertyLoader.getProperty("com.pdi.data.abyd.application", "abd.ig.uiServer");
		// Add the IG suffix and the linkparam ID
		//url += LP_SUFFIX + lpId;//http://apptest.pdininthhouse.com/pdi-web/Flex/AbyDIntegrationGrid.jsp?lpUIS=
		url += lpId;

		// Get the to and from address and the e-mail server name
		String toAddr = PropertyLoader.getProperty("com.pdi.data.abyd.application", "abd.dna.toAddress.statusToEdit");
		String fromAddr = PropertyLoader.getProperty("com.pdi.data.abyd.application", "abd.dna.fromAddress.statusToEdit");
		String ccAddr = PropertyLoader.getProperty("com.pdi.data.abyd.application", "abd.dna.ccAddress.statusToEdit");
		String host = PropertyLoader.getProperty("com.pdi.data.abyd.application", "abd.emailServer");
		
		// Set up the message object
		EmailMessage email = new EmailMessage();
		email.setToAddress(toAddr);
		email.setFromAddress(fromAddr);
		email.setCcAddress(ccAddr);
		email.setSubject(emailSubject);
		email.setText(emailBody);
		email.getTextSubstitutions().add(clientName);
		email.getTextSubstitutions().add(partName);
		email.getTextSubstitutions().add(url);
		email.getTextSubstitutions().add(leadName);
		// Yes the same text two times (unless the business changes the texts in either the subject or body
		email.getTextSubstitutions().add(typeText);
		email.getTextSubstitutions().add(typeText);

		EmailRequestService emailService = new EmailRequestService(host);
		emailService.sendEmail(email);

		return;
	}


	/**
	 * Though this should probably reside in its own helper, it is here to leverage access to the rptInput methods.
	 * @return
	 */
	public String dormantCandExt()
	{
		// Time and timing
		DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		// Limit parameter.  0 = no limit.
		// Logic assumes positive number.  If externalized, then add to code: "max = Math.abs(max);" (to ensure a positive number)
		int max = 25;
		this.eCnt = 0;
		this.doneCnt = 0;
		int candCnt = 0;
		String retStr = "Dormant DRI Execution Report\n";
		if (max > 0)
		{
			retStr += "  Max candidates=" + max + "\n";
		}
		retStr += "  Start time: " + df.format(cal.getTime()) + "\n";
		
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT ri.participantId, ri.dnaId ");
		sqlQuery.append("  FROM pdi_abd_rpt_input ri ");
		sqlQuery.append("  WHERE ri.lastDate < DATEADD(day, -30, GETDATE()) ");
		sqlQuery.append("    AND ri.oTxtStatusId < 3 ");
		sqlQuery.append("    AND ri.pTxtStatusId < 3 ");
		sqlQuery.append("    AND NOT EXISTS (SELECT 'X' FROM pdi_abd_extract_cand ec ");
		sqlQuery.append("                      WHERE ec.participantId = ri.participantId ");
		sqlQuery.append("                      AND ec.dnaId = ri.dnaId) ");
		sqlQuery.append("  ORDER BY ri.lastDate DESC ");

		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			return retStr + "Dormant Extract query setup failed - msg=" + dlps.getStatus().getExceptionMessage();
		}

		DataResult dr = null;
		ResultSet rs  = null;

		try {
			dr =  V2DatabaseUtils.select(dlps);
			rs = dr.getResultSet();
		
			if (rs != null && rs.isBeforeFirst())
			{
				while (rs.next())
				{
					String ppt =  rs.getString("participantId");
					long dna = rs.getLong("dnaId");
					candCnt++;
					retStr += doCand(ppt, dna);

					if (max > 0)
					{
						if ((this.doneCnt + this.eCnt) >= max)
							{
								retStr += "Reached max candidates (" + max + ")... stopping\n";
								break;
							}
					}
				}
			}
		}
		catch (SQLException e)
		{
			return retStr + "Dormant Extract query execution failed - msg=" + e.getMessage();
		}
		finally
		{
			if (dr != null) {  dr.close();  dr = null; }
		}

		retStr += "Candidate count = " + candCnt + "\n";
		retStr += "Error count = " + this.eCnt + "\n";
		retStr += "Done count = " + this.doneCnt + "\n";
		cal = Calendar.getInstance();
		return retStr + "End time: " + df.format(cal.getTime()) + "\n";
	}


	/**
	 * 
	 * @param ppt
	 * @param dna
	 * @return
	 */
	private String doCand(String ppt, long dna)
	{
		String ret = "";
		long startTime = 0;
		long endTime = 0;

		ReportInputDTO rptInp;
		if (debug)
		{
			startTime = System.currentTimeMillis();
			ret += "  Processing ppt.dna " + ppt + "/" + dna + "\n";
		}
		try
		{
			rptInp = getReportInput(ppt, dna, DefaultLanguageHelper.fetchDefaultLanguageCode(), false);
		}
		catch (Exception e)
		{
			ret += "  ERROR:  Unable to fetch Report Input data for ppt/dna " + ppt + "/" + dna + ". Excepion msg=" + e.getMessage() + "\n";
//			this.eCnt++;
//			return ret;
			return handleError(ppt, dna, ret);
		}

		// create the XML
		String xml = "";
		try
		{
			xml = ResearchExtractHelper.createExtractXML(rptInp);
		}
		catch (Exception e)
		{
			ret += "  ERROR:  Unable to create extract XML for ppt/dna " + ppt + "/" + dna + ". Excepion msg=" + e.getMessage() + "\n";
//			this.eCnt++;
//			return ret;
			return handleError(ppt, dna, ret);
		}
		if (xml == null || xml.isEmpty())
		{
			ret += "  ERROR:  No XML generated for ppt/dna " + ppt + "/" + dna + ".\n";
			return ret;
		}

		// write out the row
		try
		{
			ResearchExtractHelper.writeExtractRow(ResearchExtractHelper.EXT_DMNT, rptInp.getParticipantId(), rptInp.getDnaId(), xml);
		} catch (Exception e)
		{
			ret += "  ERROR:  Unable to write out the extract candidate row for ppt/dna " + ppt + "/" + dna + ". Excepion msg=" + e.getMessage() + "\n";
			System.out.println(ret);
			this.eCnt++;
			return ret;
		}

		this.doneCnt ++;
		if (debug)
		{
			endTime = System.currentTimeMillis();
			ret += "    Done with " + ppt + "/" + dna + "... " + ((double)(endTime - startTime) / 1000.0) + " seconds\n";
		}

		return ret;
	}
	
	private String handleError(String ppt, long dna, String ret)
	{
		this.eCnt++;
		
		try
		{
			ResearchExtractHelper.writeExtractRow(ResearchExtractHelper.EXT_DMNT_ERR, ppt, dna, ret);
		}
		catch (Exception e)
		{
			ret += "  ERROR:  Unable to write out the ERROR extract candidate row for ppt/dna " + ppt + "/" + dna + ". Excepion msg=" + e.getMessage() + "\n";
			System.out.println(ret);
			return ret;
		}
		
		return ret;
	}
}
