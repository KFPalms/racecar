/*
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.helpers.ut;

import java.util.ArrayList;

import com.pdi.data.util.RequestStatus;
import com.pdi.data.v3.dto.EmailRecipientTypeDTO;
import com.pdi.data.v3.dto.EmailRecipientTypeListDTO;
import com.pdi.data.v3.dto.SessionUserDTO;
import com.pdi.data.v3.helpers.EmailRecipientTypeDataHelper;
import com.pdi.data.v3.util.V3DatabaseUtils;
import com.pdi.properties.PropertyLoader;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

public class EmailRecipientTypeDataHelperUT extends TestCase
{
	
	//
	// Constructors
	//

	public EmailRecipientTypeDataHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args) throws Exception
    {
		junit.textui.TestRunner.run(EmailRecipientTypeDataHelperUT.class);
    }

	/*
	 * testProperties
	 */
	public void testProperties()
		throws Exception
	{
    	UnitTestUtils.start(this);
    	
    	//Does the local property work?
    	String applicationId = PropertyLoader.getProperty("com.pdi.data.v3.application", "application.code");
    	assertEquals(applicationId != null, true);
    	assertEquals(applicationId.equals("PDI-DATA-V3"), true);
    	 
    	UnitTestUtils.stop(this);
    }
 
    
    /*
     * testGet
     */
	public void testGet() throws Exception
	{
		UnitTestUtils.start(this);
		V3DatabaseUtils.setUnitTest(true);
     	
    	SessionUserDTO sessionUser = new SessionUserDTO();
    	sessionUser.setUsername(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.username")); 
    	sessionUser.setPassword(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.password"));

		// Actual test code starts here
		
		// Check the insert
		EmailRecipientTypeDTO ins = new EmailRecipientTypeDTO();
		ins.setName("Unit Test type");
		EmailRecipientTypeDTO insOut = EmailRecipientTypeDataHelper.addRecipientType(sessionUser, ins);
		//assertEquals("addRecipientType status != OK",
		//			 RequestStatus.RS_OK, insOut.getStatus().getStatusCode());
		assertFalse("addRecipientType id not returned", (0 == insOut.getId()));
		System.out.println("Insert succeeded; New ID = " + insOut.getId());

		// Now check the select for that item
		EmailRecipientTypeDTO singleType = EmailRecipientTypeDataHelper.getRecipientType(insOut.getId());
		assertEquals("getRecipientType status != OK",
					 RequestStatus.RS_OK, singleType.getStatus().getStatusCode());
		assertEquals("getRecipientType returned incorrect name",
				ins.getName(), singleType.getName());
		System.out.println("Get succeeded");

		// Test the "Get All Types" cross-referenced to the get single type
		EmailRecipientTypeListDTO tlDto = EmailRecipientTypeDataHelper.getAllRecipientTypes(sessionUser);
		assertEquals("getAllRecipientTypes status != OK - returns " + tlDto.getStatus().getStatusCode(),
					 RequestStatus.RS_OK, tlDto.getStatus().getStatusCode());

		ArrayList<EmailRecipientTypeDTO> typeList = tlDto.getTypeList();
		for (EmailRecipientTypeDTO type : typeList)
		{
			//	//Print out the recipient type
			//	System.out.println(type.toString());

			//For fun... let's ask the db to get the same thing AGAIN
			singleType = null;
			singleType = EmailRecipientTypeDataHelper.getRecipientType(type.getId());

			//Are they the same object... well they should be!
			assertEquals(singleType.getId(), type.getId());
		}
		System.out.println("Get All succeeded");

		// Check the update on that item
		EmailRecipientTypeDTO updt = new EmailRecipientTypeDTO();
		updt.setId(insOut.getId());
		updt.setName("Unit Test type (updated-" + insOut.getId() + ")");
		RequestStatus updtOut = EmailRecipientTypeDataHelper.updateRecipientType(sessionUser, updt);
		assertEquals("updateRecipientType status != OK.  msg=" + updtOut.getExceptionMessage() + ".  ",
					 RequestStatus.RS_OK, updtOut.getStatusCode());
		EmailRecipientTypeDTO updtChk = EmailRecipientTypeDataHelper.getRecipientType(insOut.getId());
		assertEquals("updateRecipientType check (get) status != OK.  msg=" + updtChk.getStatus().getExceptionMessage(),
					 RequestStatus.RS_OK, updtChk.getStatus().getStatusCode());
		assertEquals("getRecipientType returned incorrect name",
					 updt.getName(), updtChk.getName());
		System.out.println("update succeeded");

		//Check that the item deletes
		RequestStatus delOut = EmailRecipientTypeDataHelper.deleteRecipientType(sessionUser, insOut.getId());
		assertEquals("deleteRecipientType status != OK.  msg=" + delOut.getExceptionMessage() + ".  ",
					 RequestStatus.RS_OK, delOut.getStatusCode());
		EmailRecipientTypeDTO delChk = EmailRecipientTypeDataHelper.getRecipientType(insOut.getId());
		assertEquals("deleteRecipientType check status != OK.  msg=" + delChk.getStatus().getExceptionMessage(),
					 RequestStatus.DBS_NO_DATA, delChk.getStatus().getStatusCode());
		System.out.println("delete succeeded");

		// Actual test code ends here
		
		UnitTestUtils.stop(this);
	}
}