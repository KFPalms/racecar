/*
 *  Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.util;

import java.sql.Connection;

/**
 * DataLayerConnection is a thin class that transports status data and a Connection object
 * within the Data Layer only.  It is to be used instead of throwing an exception so that
 * we can avoid throwing exceptions in the data layer and can, instead, pass error
 * information up to the Business Layer classes.
 *
 * @author		Ken Beukelman
 */
public class DataLayerConnection extends BaseDTO
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private Connection _connection = null;
	
	//
	// Constructors.
	//
	public DataLayerConnection()
	{
		// Nothing done on a no-parameter constructor
	}

	//
	// Instance methods.
	//

	public boolean isInError()
	{
		return (this.getStatus().getStatusCode() == RequestStatus.RS_OK ? false : true);
	}
	//////////////////////////////////////
	//        Setters and Getters       //
	//////////////////////////////////////
	
	/********************************************************/
	public void setConnection(Connection value)
	{
		_connection = value;
	}

	public Connection getConnection()
	{
		return _connection;
	}
}
