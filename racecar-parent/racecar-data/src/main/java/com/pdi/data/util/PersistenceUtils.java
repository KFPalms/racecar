package com.pdi.data.util;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.HashMap;


public class PersistenceUtils {
	
	
	private static final PersistenceUtils singleton = new PersistenceUtils();
		
	public static PersistenceUtils getInstance() {
		return singleton;
	}
	
	protected HashMap<String,EntityManagerFactory> entityManagerFactories = new HashMap<String,EntityManagerFactory>();
	 
	  public EntityManagerFactory getEntityManagerFactory(String code) {
		    
		    if(entityManagerFactories.get(code) == null)
		      createEntityManagerFactory(code);
		    return entityManagerFactories.get(code);
		  }
	
	  protected void createEntityManagerFactory(String code) {
		    
		    this.entityManagerFactories.put(code,Persistence.createEntityManagerFactory(code));

		  }
	  
	public static EntityManager createEntityManager(String code) {
		EntityManager em = PersistenceUtils.getInstance().getEntityManagerFactory(code).createEntityManager();
		return em;
	}
	
	public static Query createNamedQuery(String environment, String name) {
		return createEntityManager(environment).createNamedQuery(name);
	}

	@SuppressWarnings("unchecked")
	public static ArrayList getResultList(String environment, String name) {
		EntityManager em = createEntityManager(environment);
		ArrayList l = (ArrayList)em.createNamedQuery(name).getResultList();
		em.close();
		return l;
	}
	

}