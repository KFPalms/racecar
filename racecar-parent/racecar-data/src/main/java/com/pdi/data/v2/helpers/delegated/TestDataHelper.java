package com.pdi.data.v2.helpers.delegated;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.pdi.data.dto.SessionUser;
import com.pdi.data.dto.TestData;
import com.pdi.data.helpers.interfaces.ITestDataHelper;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.util.RequestStatus;
import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.logging.LogWriter;


public class TestDataHelper implements ITestDataHelper{
	
	private String dbUser = "TestDataHlpr";	
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	/**
	 * This is used by the TestData application.  
	 * When the CogsCompleted button is clicked in Test Data, 
	 * this method is called to update the database.
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.ITestDataHelper#saveCogsDoneFlag(com.pdi.data.dto.SessionUser, TestData testData)
	 */
	public void saveCogsDoneFlag(SessionUser session, TestData testData){
		
		////////////////////////////////////////////
		// Set up the initial checking on it query.
		///////////////////////////////////////////
		StringBuffer sqlQuery1 = new StringBuffer();
		sqlQuery1.append("SELECT cogsDone ");
		sqlQuery1.append("  FROM pdi_abd_igrid_response ");
		sqlQuery1.append("  WHERE participantId = ? ");			// 1
		sqlQuery1.append("  AND dnaId = ? ");					// 2
	
		//System.out.println("sqlQuery1 " + sqlQuery1);	
		
		DataResult dr1 = null;
		DataResult drInsert = null;
		DataResult drUpdate = null;
		
		////////////////////////////////////////////
		// Set up the update query.
		///////////////////////////////////////////		
		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE pdi_abd_igrid_response ");  					
		sb.append(" SET  cogsDone = ?, ");  			// 1
		sb.append(" lastUser = ?, ");  					// 2
		sb.append(" lastDate = ? ");  					// 3
		sb.append(" WHERE dnaId = ? ");  		    // 4
		sb.append(" AND participantId = ? ");  		// 5
	
//		DataResult dr = null;
			
		try
		{
			////////////////////////////////////////////////////////////////////////
			// spin through the scores, see if there's a row out there first... 
			////////////////////////////////////////////////////////////////////////	
//			int testIndex = 0;
//			for(String k : sg.getScores().keySet())
//			{
				try
				{
					DataLayerPreparedStatement dlps1 = V2DatabaseUtils.prepareStatement(sqlQuery1);
					if (dlps1.isInError())
					{
						//System.out.println("Error preparing saveCogsDoneFlag query." + dlps1.toString());
						LogWriter.logSQL(LogWriter.ERROR, this,
								"Error preparing saveCogsDoneFlag query." + dlps1.toString(),
								sqlQuery1.toString());
						return;
					}
					
					// fill in the ?'s for the initial query... 
					dlps1.getPreparedStatement().setLong(1, testData.getParticipantId());
					dlps1.getPreparedStatement().setLong(2, testData.getDnaId());

	
					// now do the initial select, to see if the row is there.... 
					dr1 = V2DatabaseUtils.select(dlps1);	
					
					// if there isn't any data, do insert....
					if (dr1.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
					{	
						//System.out.println("INSERT INTO RESULTS.....");
						StringBuffer sbInsert = new StringBuffer();
						sbInsert.append("INSERT INTO pdi_abd_igrid_response ");
						sbInsert.append("(participantId, dnaId, igSubmitted, cogsDone, lastUser, lastDate) ");
						sbInsert.append(" VALUES (?, ?, ?, ?, ?, ?)  ");
	
						DataLayerPreparedStatement dlpsInsert = V2DatabaseUtils.prepareStatement(sbInsert);
						if (dlpsInsert.isInError())
						{
							//System.out.println("Error preparing doInsert for saveCogsDoneFlag query." + dlpsInsert.toString());
							LogWriter.logBasic(LogWriter.ERROR, this,
									"Error preparing doInsert for saveCogsDoneFlag query." + dlpsInsert.toString());
							return;
						}
	
						try
						{
							String date = sdf.format(new Date());
								
							// Fill sql parameters with values and get the data
							dlpsInsert.getPreparedStatement().setLong(1, testData.getParticipantId());
							dlpsInsert.getPreparedStatement().setLong(2, testData.getDnaId());
							dlpsInsert.getPreparedStatement().setInt(3, 0);  // default set to not submitted
							dlpsInsert.getPreparedStatement().setInt(4, testData.getCogsCompleted()? 1 : 0 );
							dlpsInsert.getPreparedStatement().setString(5, dbUser);
							dlpsInsert.getPreparedStatement().setString(6, date);

							try {
								drInsert = V2DatabaseUtils.insert(dlpsInsert);
							} catch (Exception e) {
								LogWriter.logSQL(LogWriter.ERROR, this,
										"Insert error in result:" +
										"  PartId = " + testData.getParticipantId() + 
										", projId = " + testData.getProjectId() + 
										", dnaId = " + testData.getDnaId() +
										", cogsDone = " + testData.getCogsCompleted(),
										sbInsert.toString());
								}
						}
						catch(java.sql.SQLException se)
						{
							LogWriter.logSQL(LogWriter.ERROR, this,
									"Insert error in result:" +
									"  PartId = " + testData.getParticipantId() + 
									", projId = " + testData.getProjectId() + 
									", dnaId = " + testData.getDnaId() +
									", cogsDone = " + testData.getCogsCompleted(),
									sbInsert.toString());
						}
					}
					else if (dr1.getStatus().getStatusCode() == 0 )  // there is data, so do an update....
					{   
						//System.out.println("UPDATE RESULTS DATA.........");
						// THERE ALREADY IS a row, so do the update..	

						String date = sdf.format(new Date());
						
						DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
						if (dlps.isInError())
						{
							//System.out.println("Error preparing writeResultsDataToDatabase query." + dlps.toString());
							LogWriter.logBasic(LogWriter.ERROR, this, 
								"Insert error in result:" +
								"  PartId = " + testData.getParticipantId() + 
								", projId = " + testData.getProjectId() + 
								", dnaId = " + testData.getDnaId() +
								", cogsDone = " + testData.getCogsCompleted());
							return;
						}	
							
						//THIS IS THE UPDATE ...						
						// Fill sql parmeters with values and get the data
						dlps.getPreparedStatement().setInt(1, testData.getCogsCompleted()? 1 : 0 );
						dlps.getPreparedStatement().setString(2, dbUser);
						dlps.getPreparedStatement().setString(3, date);
						dlps.getPreparedStatement().setLong(4, testData.getDnaId());
						dlps.getPreparedStatement().setLong(5, testData.getParticipantId());

						try
						{
							drUpdate = V2DatabaseUtils.update(dlps);
						} catch(Exception e)
						{
							LogWriter.logSQL(LogWriter.ERROR, this,
									"Insert error in result:" +
									"  PartId = " + testData.getParticipantId() + 
									", projId = " + testData.getProjectId() + 
									", dnaId = " + testData.getDnaId() +
									", cogsDone = " + testData.getCogsCompleted(),
									sb.toString());
						}
					}
					else
					{							
						LogWriter.logSQL(LogWriter.ERROR, this,
								"Insert error in result:" +
								"  PartId = " + testData.getParticipantId() + 
								", projId = " + testData.getProjectId() + 
								", dnaId = " + testData.getDnaId() +
								", cogsDone = " + testData.getCogsCompleted(),
								sb.toString());
					}
				} catch (Exception e) {
					// Does nothing
				}
//			}	// End of for loop
		}
		finally
		{
			if(dr1 != null) { dr1.close(); dr1 = null; }
			if(drInsert != null) { drInsert.close(); drInsert = null; }
			if(drUpdate != null) { drUpdate.close(); drUpdate = null; }
		}
		
	}
	
	
	/**
	 * This is used by the TestData application. 
	 * It is called on startup of the application, to determine
	 * if the CogsComplete button has been checked.
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.ITestDataHelper#getCogsDoneFlag(com.pdi.data.dto.SessionUser, TestData testData)
	 */	
	public boolean getCogsDoneFlag(SessionUser session, TestData testData){
		

		try {
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT  cogsDone ");
			sb.append("  FROM pdi_abd_igrid_response ");
			sb.append("  WHERE participantId = ?  ");
			sb.append("  AND dnaId = ?  ");
				
			DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
			if (dlps.isInError())
			{
				//System.out.println("Error preparing getCogsDoneFlag query." + dlps.toString());
				LogWriter.logSQL(LogWriter.WARNING, this,
						"Error preparing getCogsDoneFlag query",
						sb.toString());
			}

			DataResult dr = null;
			try
			{

				dlps.getPreparedStatement().setLong(1, testData.getParticipantId());
				dlps.getPreparedStatement().setLong(2, testData.getDnaId());

				dr = V2DatabaseUtils.select(dlps);
				ResultSet rs = dr.getResultSet();

				if (rs != null && rs.isBeforeFirst())
				{
					while(rs.next()) {
						int cogsDone = rs.getInt("cogsDone");
						
						if(cogsDone == 1)
							return true;
						if(cogsDone == 0)
							return false;
					}
				}
				
			}
			catch(java.sql.SQLException se)
			{
				LogWriter.logSQL(LogWriter.ERROR, this,
						"Fetch error in getCogsDoneFlag", sb.toString());
				se.printStackTrace();
			}
			finally
			{
		        if (dr != null) { dr.close(); dr = null; }
			}
		} catch(Exception e) {
			LogWriter.logBasic(LogWriter.ERROR, this,
					"General error in getCogsDoneFlag");
			//se.printStackTrace();
		}

		return false;
	}
	
	
	/** 
	 * This is an AbyD specific method, so I'm not putting it
	 * into the ProjectHelper, which should be project-type agnostic.
	 * It's going to get used for the TestData app. 
	 * The other place it could go would be AbyD Utils, I suppose,
	 * but this is as good a place as any.
	 * 
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.ITestDataHelper#getDnaIdFromProjectId(com.pdi.data.dto.SessionUser, com.pdi.data.dto.TestData)
	 */
	public long getDnaIdFromProjectId(SessionUser session, TestData testData){
		
/*		select dnaId
		from pdi_abd_dna
		where projectId = 35*/
		
		try {
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT  dna.dnaId ");
			sb.append("  FROM pdi_abd_dna dna ");
			sb.append("  WHERE dna.projectId = ?  ");
				
			DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
			if (dlps.isInError())
			{
				//System.out.println("Error preparing getDnaIdFromProjectId query." + dlps.toString());
				LogWriter.logSQL(LogWriter.WARNING, this,
						"Error preparing getDnaIdFromProjectId query",
						sb.toString());
			}

			DataResult dr = null;
			try
			{
				dlps.getPreparedStatement().setLong(1, testData.getProjectId());
				
				dr = V2DatabaseUtils.select(dlps);
				ResultSet rs = dr.getResultSet();

				if (rs != null && rs.isBeforeFirst())
				{
					if(rs.next()) {
						long dnaId = rs.getLong("dnaId");
						return dnaId;

					}else{
						
						throw new SQLException("No dnaId found for project: " + testData.getDnaId());
						
					}
					
				}
			}
			catch(java.sql.SQLException se)
			{
				LogWriter.logSQL(LogWriter.ERROR, this,
						"Fetch error in getDnaIdFromProjectId", sb.toString());
				se.printStackTrace();
			}
			finally
			{
		        if (dr != null) { dr.close(); dr = null; }
			}
		} catch(Exception e) {
			LogWriter.logBasic(LogWriter.ERROR, this,
					"General error in getDnaIdFromProjectId");
			//se.printStackTrace();
		}
		
		return new Long(0).longValue();
	}

	/**
	 * Defined in pdi.data.nhn
	 * 
	 * The method getUserCourseListFromStoredProc calls the stored procedure 
	 * user_course_list_status_assessment.
	 * This method is used by the Test Data application in determining which
	 * instruments should be shown.  The stored proc is also used by the Launch Page
	 * for Participants.
	 * 
	 * @param String clientId
	 * @param String participantId
	 * @return ArrayList<String> - the list of course abbreviations 
	 * 								that can be shown
	 */
	public ArrayList<String> getUserCourseListFromStoredProc(String clientId, String participantId){
		return new ArrayList<String>();
	}
}
