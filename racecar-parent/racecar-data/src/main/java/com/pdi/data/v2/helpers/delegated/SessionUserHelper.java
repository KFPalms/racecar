/**
 * Copyright (c) 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v2.helpers.delegated;

import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.interfaces.ISessionUserHelper;

public class SessionUserHelper implements ISessionUserHelper {

	/*
	 * createSessionUser
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.ISessionUserHelper#createSessionUser(com.pdi.data.dto.SessionUser)
	 */
	//@Override
	public SessionUser createSessionUser(SessionUser session) {
		// TODO Auto-generated method stub
		return new SessionUser();
	}

	/*
	 * createSessionUser
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.ISessionUserHelper#createSessionUser()
	 */
	//@Override
	public SessionUser createSessionUser() {
		// TODO Auto-generated method stub
		return new SessionUser();	
	}

}
