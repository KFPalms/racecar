/**
 * Copyright (c) 2008 Personnel Decisions International, Inc.
 * All rights reserved.
 *
 * $Header: com/pdi/data/abyd/dto/setup/Setup2ReportOptionsDTO.java 6     8/25/05 3:23p mpanichi $
 */
package com.pdi.data.abyd.dto.setup;


/**
 * Setup2ReportOptionsDTO is a wrapper around a number of data items that are needed when 
 * part 2 of the Design Setup app is initially started.
 * 
 * @author		Ken Beukelman
 * @author      MB Panichi
 */
public class Setup2ReportOptionValueDTO
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//

	private String _optionCode = "";	
	private String _label = "";	
	private int _sequence = 0;
    private long _reportOptionsId = 0;
    private boolean _optionValue = false; 
    private String _optionString = "";

	//
	// Constructors.
	//
	public Setup2ReportOptionValueDTO()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//
	public String toString(){
		return "option value dto : "+_label + " code:"+ this._optionCode + " string="+ this._optionString + " value="+ this._optionValue;
		
	}
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public String getOptionCode()
	{
		return _optionCode.trim();
	}
		
	public void setOptionCode(String value)
	{
		_optionCode = value.trim();
	}	
	
	/*****************************************************************************************/
	public String getLabel()
	{
		return _label;
	}
		
	public void setLabel(String value)
	{
		_label = value;
	}	
	
	/*****************************************************************************************/
	public int getSequence()
	{
		return _sequence;
	}
		
	public void setSequence(int value)
	{
		_sequence = value;
	}

	/*****************************************************************************************/
	public long getReportOptionsId()
	{
		return _reportOptionsId;
	}
		
	public void setReportOptionsId(long value)
	{
		_reportOptionsId = value;
	}

	/*****************************************************************************************/
	public boolean getOptionValue()
	{
		return _optionValue;
	}
		
	public void setOptionValue(boolean value)
	{
		_optionValue = value;
	}

	/*****************************************************************************************/
	public String getOptionString() {
		return _optionString;
	}

	
	public void setOptionString(String str) {
		_optionString = str;
	}
	/*****************************************************************************************/
	
	
	
	
}
