/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.data.helpers.interfaces;

import com.pdi.data.dto.Instrument;
import com.pdi.data.dto.Participant;
import com.pdi.data.dto.ResponseGroup;
import com.pdi.data.dto.ScoreGroup;

public interface IScoringHelper {

	/**
	 * Score an instrument with a set of response data
	 * @param i
	 * @param rg
	 * @return
	 */
	public ScoreGroup score(Instrument i, ResponseGroup rg);
	
	//"TLT_SCORING"
	//"ABYD_SPECIAL_SCORING"
	public ScoreGroup score(String scoreCode, Participant p);
	
}
