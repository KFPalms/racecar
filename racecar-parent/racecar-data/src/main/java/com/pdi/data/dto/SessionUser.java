/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.dto;

import java.io.Serializable;
import java.util.HashMap;

/**
 * SessionUser is a basic data class that contains session user data.
 * At this point, it is a server side object only
 * 
 * @author		Gavin Myers
 */
public class SessionUser implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4975249582102058132L;
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private HashMap<String, String> metadata = new HashMap<String, String>();
	private Individual individual;
	//
	// Constructors.
	//
	public SessionUser()
	{
		// does nothing right now
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public void setMetadata(HashMap<String, String> value)
	{
		metadata = value;
	}

	public HashMap<String, String> getMetadata()
	{
		return metadata;
	}

	public void setIndividual(Individual individual) {
		this.individual = individual;
	}

	public Individual getIndividual() {
		return individual;
	}
}
