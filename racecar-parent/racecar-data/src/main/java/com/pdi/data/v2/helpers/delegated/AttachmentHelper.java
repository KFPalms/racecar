package com.pdi.data.v2.helpers.delegated;

//import java.sql.Connection;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.sql.Statement;
import java.util.ArrayList;
//import java.util.Iterator;

//import com.pdi.logging.LogWriter;

import com.pdi.data.dto.Attachment;
import com.pdi.data.dto.AttachmentHolder;
//import com.pdi.data.dto.AttachmentType;
import com.pdi.data.dto.SessionUser;
//import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.IAttachmentHelper;
//import com.pdi.data.util.DataLayerPreparedStatement;
//import com.pdi.data.util.DataResult;
//import com.pdi.data.v2.util.V2DatabaseUtils;


/*
 * AttachmentHelper
 */

/*
 * NOTE:  The Attachment functionality has been moved to NHN.  All
 *        methods here have been replaced with a thrown Exception.
 *        Please see:
 *        com.pdi.data.nhn.helpers.delegated.AttachmentHelper
 *        for the requisite (and appropriate) code.
 */

public class AttachmentHelper implements IAttachmentHelper
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//

	//
	// Constructors.
	//

	//
	// Instance methods.
	//

	/**
	 * fetch all Attachments for this project and participant, 
	 * and return them in the AttachmentHolder.
	 * 
	 * Required method in the Interface, but V2 does not support
	 * the project portion...
	 *
	 * @param  String participantId
	 * @return AttachmentHolder
	 * @throws Exception
	 */
	public AttachmentHolder fromProjectIdParticipantId(SessionUser session,
			                                           String projectId,
			                                           String participantId)
		throws Exception
	{
		throw new Exception("AttachmentHelper fromProjectIdParticipantId() is not operative (V2)");
	}

	
	/**
	 * fetch all Attachments for this participant, 
	 * and return them in the AttachmentHolder
	 *
	 * @param  String participantId
	 * @return AttachmentHolder
	 * @throws Exception
	 */
	public AttachmentHolder fromParticipantId(SessionUser session, String participantId) 
		throws Exception
	{
		throw new Exception("AttachmentHelper fromProjectIdParticipantId() is not operative (V2)");
//		AttachmentHolder attachmentHolder = new AttachmentHolder();
//
//		try{
//			attachmentHolder.setParticipant(HelperDelegate.getParticipantHelper().fromId(session, participantId));
//			ArrayList<AttachmentType> types = this.getAttachmentTypes();
//			attachmentHolder.setAttachmentTypes(types);
//		}catch (Exception e){
//			// put in empty array
//			LogWriter.logBasic(LogWriter.ERROR, this, "Unable to retrieve AttachmentTypes Array. ");
//			attachmentHolder.setAttachmentTypes(new ArrayList<AttachmentType>());
//		}
//		
//		ArrayList<Attachment> attachmentArray = new ArrayList<Attachment>();
//		attachmentHolder.setAttachments(attachmentArray);
//
//		StringBuffer sqlQuery = new StringBuffer();
//		sqlQuery.append(" SELECT attachmentId,   value,  attachmentType, description, active  ");
//		sqlQuery.append(" FROM pdi_participant_attachment  ");
//		sqlQuery.append(" WHERE participantId =  ? ");
//		sqlQuery.append(" and active = 1  ");
//	
//		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
//	
//		DataResult dr = null;
//		ResultSet rs  = null;
//	
//		try {
//			dlps.getPreparedStatement().setString(1,participantId);
//	
//			if (dlps.isInError())
//			{
//				LogWriter.logBasic(LogWriter.WARNING, this, "dlps.isInError()" + dlps.toString());
//				return attachmentHolder;
//			}
//	
//			dr =  V2DatabaseUtils.select(dlps);
//			rs = dr.getResultSet();
//		
//			if (rs != null && rs.isBeforeFirst())
//			{
//				while (rs.next())
//				{
//					Attachment attachment = new Attachment();
//					attachment.setAttachmentId(rs.getLong("attachmentId"));
//					attachment.setAttachmentUrl(rs.getString("value"));
//					attachment.setAttachmentTypeId(rs.getInt("attachmentType"));
//					attachment.setAttachmentDescription(rs.getString("description"));	
//					attachment.setAttachmentActive(rs.getBoolean("active"));
//					attachmentArray.add(attachment);
//					//System.out.println("fetching:  "  + attachment.getAttachmentDescription() + " " + attachment.getAttachmentUrl());
//				}
//			}
//		} catch (SQLException sqlEx){
//			// swallow the error
//			LogWriter.logSQLWithException(LogWriter.ERROR, this, 
//					"Error in com.pdi.data.v2.helpers.delegated.AttachmentHelper fromParticipantId(SessionUser session, String participantId) ", 
//					sqlQuery.toString(), sqlEx);
//
//		}finally {
//			if (rs != null)
//			{
//				try
//				{
//					rs.close();
//				}catch (SQLException sqlEx)	{
//					// swallow the error
//					LogWriter.logSQLWithException(LogWriter.ERROR, this, 
//							"Error in com.pdi.data.v2.helpers.delegated.AttachmentHelper fromParticipantId(SessionUser session, String participantId) ", 
//							sqlQuery.toString(), sqlEx);
//
//				}
//
//				rs = null;
//			}
//			if (dr != null)
//			{
//				dr.close();
//				dr = null;
//			}
//		}
//		return attachmentHolder;
	}
	public AttachmentHolder fromProjectId(SessionUser session,
            String projectId)
		throws Exception
	{
		throw new Exception("AttachmentHelper fromProjectIdId() is not operative (V2)");
	}

	/**
	 * getAttachmentTypes()
	 *
	 * 
	 * @return ArrayList<AttachmentType>
	 * @throws Exception
	 */
//	private ArrayList<AttachmentType> getAttachmentTypes()
//		throws Exception
//	{
//		ArrayList<AttachmentType> attachmentArray = new ArrayList<AttachmentType>();
//		
//		StringBuffer sqlQuery = new StringBuffer();
//		sqlQuery.append(" SELECT attachmentTypeId,  name  ");
//		sqlQuery.append(" FROM pdi_attachment_type  ");
//		sqlQuery.append(" ORDER BY attachmentTypeId ");
//
//		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
//
//		if (dlps.isInError())
//		{
//			LogWriter.logBasic(LogWriter.WARNING, this, "dlps.isInError()" + dlps.toString());
//			return attachmentArray;
//		}
//		DataResult dr = null;
//		ResultSet rs  = null;
//
//		dr =  V2DatabaseUtils.select(dlps);
//		rs = dr.getResultSet();
//
//		try {
//			if (rs != null && rs.isBeforeFirst())
//			{
//				while (rs.next())
//				{
//					AttachmentType at = new AttachmentType();
//					at.setId(rs.getInt("attachmentTypeId"));
//					at.setName(rs.getString("name"));
//
//					attachmentArray.add(at);
//				}
//			}
//		} finally {
//			if (rs != null)
//			{
//				try {
//					rs.close();
//				}
//				catch (SQLException sqlEx) {
//					// swallow the error
//					LogWriter.logSQLWithException(LogWriter.ERROR, this, 
//							"Error in com.pdi.data.v2.helpers.delegated.AttachmentHelper getAttachmentTypes() ", 
//							sqlQuery.toString(), sqlEx);
//				}
//				rs = null;
//			}
//			if (dr != null)
//			{
//				dr.close();
//				dr = null;
//			}
//		}
//		return attachmentArray;
//	}


	/**
	 * addAttachments
	 * Insert or update a note into pdi_abd_participant_attachment
	 *
	 * @param  Connection - the database connection
	 * @param  ReportInputNoteHolderDTO holderDto - holds the report notes
	 * @throws Exception
	 */
	public void modify(AttachmentHolder holder)
		throws Exception
	{
		// This code is no longer used
		throw new Exception("Modify attachment is no longer operative (V2)");
//		ArrayList<Attachment> attachmentArray = holder.getAttachments();
//
//		String userId = "attchHlpr";
//
//		Statement stmt = null;
//		ResultSet rs = null;
//		Connection con = V2DatabaseUtils.getDBConnection();
//		
//		try
//		{
//			// iterate through the dto's -- we need to look at each one...
//			Iterator<Attachment> it = attachmentArray.iterator();
//			while(it.hasNext())
//			{
//				Attachment attachment = new Attachment();
//				attachment = it.next();
//
//				// look at the attachment... if it's a new one, it's not
//				// going to have an attachmentId, because we need to
//				// get that from the database...
//				boolean insert = false;
//				if(attachment.getAttachmentId() == 0)
//				{
//					insert = true;
//				}
//				else
//				{
//					// see if there exists a row in the
//					// pdi_participant_attachment table...
//					// i'm leaving this check here, because I'm
//					// betting that they're going to allow updates
//					// to previous notes, even if they said they're
//					// not right now...
//					// select sql:
//					StringBuffer sqlQuery = new StringBuffer();
//					sqlQuery.append("  SELECT attachmentId   ");
//					sqlQuery.append("  FROM pdi_participant_attachment  ");
//					sqlQuery.append("  WHERE attachmentId = " + attachment.getAttachmentId() + "  ");
//					//System.out.println("sqlQuery = " + sqlQuery.toString());
//
//					// run the query
//					stmt = con.createStatement();
//					rs = stmt.executeQuery(sqlQuery.toString());
//				}
//
//				// if the row DOES NOT EXIST,
//				if ((insert || !rs.isBeforeFirst()) && (attachment.getAttachmentId() == 0))
//				{
//					//...we will need to INSERT IT...
//					StringBuffer sqlInsert = new StringBuffer();
//					sqlInsert.append("INSERT into pdi_participant_attachment ");
//					sqlInsert.append("  (   participantId, value, attachmentType, lastUserId, lastDate, description, active   ) VALUES( ");
//					sqlInsert.append("  ?,  ");  //1
//					sqlInsert.append("  ?,  ");  //2
//					sqlInsert.append("  ?,  ");  //3
//					sqlInsert.append("  ?,  ");  //4
//					sqlInsert.append("  now(),  ");
//					sqlInsert.append("  ?,  ");  // 5
//					sqlInsert.append("  1   ");  // adding, so automatically active
//					sqlInsert.append("   )  ");
//					
//					//System.out.println(" add attachment string " + sqlInsert.toString());
//					
//					DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlInsert);
//
//					dlps.getPreparedStatement().setString(1, holder.getParticipant().getId());
//					dlps.getPreparedStatement().setString(2, attachment.getAttachmentUrl());
//					dlps.getPreparedStatement().setLong(3, attachment.getAttachmentTypeId());
//					dlps.getPreparedStatement().setString(4, userId);
//					dlps.getPreparedStatement().setString(5, attachment.getAttachmentDescription());
//					V2DatabaseUtils.insert(dlps).close();
//				} else {
//					//... we just need to UPDATE IT....
//			       		
//					// get the rs. from the previous query... 
//					rs.next();
//
//					// get the database value
//					//String attachmentId = rs.getString(1); // 
//
//					// UPDATE the value in the database
//					//System.out.println();
//					StringBuffer sqlUpdate = new StringBuffer();
//					sqlUpdate.append("UPDATE pdi_participant_attachment ");
//					sqlUpdate.append("  SET value = ?, ");  //1
//					sqlUpdate.append("  attachmentType = ?,  "); //2
//					sqlUpdate.append("  lastDate = now(),  ");
//					sqlUpdate.append("  lastUserId = ?,  ");  //3
//					sqlUpdate.append("  description = ? ");//4
//					sqlUpdate.append("  active = ?  ");// 5
//					sqlUpdate.append("  WHERE attachmentId = ?  "); //6 
//
//					DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlUpdate);
//
//					dlps.getPreparedStatement().setString(1, attachment.getAttachmentUrl());
//					dlps.getPreparedStatement().setLong(2, attachment.getAttachmentTypeId());
//					dlps.getPreparedStatement().setString(3, userId);
//					dlps.getPreparedStatement().setString(4, attachment.getAttachmentDescription());
//					dlps.getPreparedStatement().setInt(5, (attachment.getAttachmentActive() ? 1 : 0));
//					dlps.getPreparedStatement().setLong(6, attachment.getAttachmentId());
//					V2DatabaseUtils.update(dlps);
//				}
//			}// end main while iterator
//		}
//		catch (SQLException ex)
//		{
//			LogWriter.logSQLWithException(LogWriter.ERROR, this, 
//					"Error in com.pdi.data.v2.helpers.delegated.AttachmentHelper modify(AttachmentHolder holder)"
//					+ "SQLState: " + ex.getSQLState() + ", " +
//					"VendorError: " + ex.getErrorCode(), 
//					null, ex);
//			
//			// handle any errors
//			throw new Exception("Error in com.pdi.data.v2.helpers.delegated.AttachmentHelper modify(AttachmentHolder holder)  " +
//				"SQLException: " + ex.getMessage() + ", " +
//				"SQLState: " + ex.getSQLState() + ", " +
//				"VendorError: " + ex.getErrorCode());
//		}
//		finally
//		{
//			if (rs != null)
//			{
//				try
//				{
//					rs.close();
//				}
//				catch (SQLException sqlEx)
//				{
//					// swallow the error
//					LogWriter.logSQLWithException(LogWriter.ERROR, this, 
//							"Error in com.pdi.data.v2.helpers.delegated.AttachmentHelper modify(AttachmentHolder holder)"
//							+ "SQLState: " + sqlEx.getSQLState() + ", " +
//							"VendorError: " + sqlEx.getErrorCode(), 
//							null, sqlEx);
//				}
//				rs = null;
//			}
//			if (stmt != null)
//			{
//				try
//				{
//					stmt.close();
//				}
//				catch (SQLException sqlEx)
//				{
//					// swallow the error
//					LogWriter.logSQLWithException(LogWriter.ERROR, this, 
//							"Error in com.pdi.data.v2.helpers.delegated.AttachmentHelper modify(AttachmentHolder holder)"
//							+ "SQLState: " + sqlEx.getSQLState() + ", " +
//							"VendorError: " + sqlEx.getErrorCode(), 
//							null, sqlEx);
//				}
//				stmt = null;
//			}
//		}
	}

	/**
	 *  deleteAttachments
	 *
	 * 
	 * @return ArrayList<AttachmentType>
	 * @throws Exception
	 */
	public void delete(ArrayList<Attachment> attachmentArray)
		throws Exception
	{
		throw new Exception("Delete attachment list is no longer operative (V2)");
//
//			Iterator<Attachment> it = attachmentArray.iterator();
//			while(it.hasNext())
//			{
//				Attachment attachment = new Attachment();
//				attachment = it.next();
//				delete(attachment);
//			}// end main while iterator

	}

	//@Override
	public void delete(Attachment attachment) throws Exception {
		throw new Exception("Delete attachment is no longer operative (V2)");

//		String userId = "attchHlpr";
//
//		Statement stmt = null;
//		ResultSet rs = null;
//		//Connection con = V2DatabaseUtils.getDBConnection();
//
//		try
//		{
//
//
//				// look at the attachment... 
//				if(attachment.getAttachmentActive())
//				{
//					//... we just need to UPDATE the ACTIVE flag....
//					StringBuffer sqlUpdate = new StringBuffer();
//					sqlUpdate.append("UPDATE pdi_participant_attachment ");
//					sqlUpdate.append("  SET active = 0, ");  
//					sqlUpdate.append("  lastDate = now(),  ");
//					sqlUpdate.append("  lastUserId = ?  ");  //1
//					sqlUpdate.append("  WHERE attachmentId = ?  "); //2 
//
//					DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlUpdate);
//
//					dlps.getPreparedStatement().setString(1, userId);
//					dlps.getPreparedStatement().setLong(2, attachment.getAttachmentId());
//					V2DatabaseUtils.update(dlps);
//				} 
//		}
//		catch (SQLException ex)
//		{
//			LogWriter.logSQLWithException(LogWriter.ERROR, this, 
//					"Error in com.pdi.data.v2.helpers.delegated.AttachmentHelper delete(Attachment attachment)"
//					+ "AttachmentId: " + attachment.getAttachmentId() + ",  "
//					+ "SQLState: " + ex.getSQLState() + ", " +
//					"VendorError: " + ex.getErrorCode(), 
//					null, ex);
//			
//			// handle any errors
//			throw new Exception("SQL updating V2 report options data.  " +
//				"SQLException: " + ex.getMessage() + ", " +
//				"SQLState: " + ex.getSQLState() + ", " +
//				"VendorError: " + ex.getErrorCode());
//		}
//		finally
//		{
//			if (rs != null)
//			{
//				try
//				{
//					rs.close();
//				}
//				catch (SQLException sqlEx)
//				{
//					// swallow the error
//					LogWriter.logSQLWithException(LogWriter.ERROR, this, 
//							"Error in com.pdi.data.v2.helpers.delegated.AttachmentHelper delete(Attachment attachment)"
//							+ "rs.close(); "
//							+ "AttachmentId: " + attachment.getAttachmentId() + ",  "
//							+ "SQLState: " + sqlEx.getSQLState() + ", " +
//							"VendorError: " + sqlEx.getErrorCode(), 
//							null, sqlEx);
//				}
//				rs = null;
//			}
//			if (stmt != null)
//			{
//				try
//				{
//					stmt.close();
//				}
//				catch (SQLException sqlEx)
//				{
//					// swallow the error
//					LogWriter.logSQLWithException(LogWriter.ERROR, this, 
//							"Error in com.pdi.data.v2.helpers.delegated.AttachmentHelper delete(Attachment attachment)"
//							+ "stmt.close() "
//							+ "AttachmentId: " + attachment.getAttachmentId() + ",  "
//							+ "SQLState: " + sqlEx.getSQLState() + ", " +
//							"VendorError: " + sqlEx.getErrorCode(), 
//							null, sqlEx);
//				}
//				stmt = null;
//			}
//		}
//		
	}
}
