package com.pdi.data.v2.ut;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.StringReader;
import java.sql.ResultSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

//import com.pdi.data.dto.NormGroup;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.ut.UnitTestUtils;
import com.pdi.xml.XMLUtils;

import junit.framework.TestCase;

public class NormGroupLoaderUT extends TestCase {
	public NormGroupLoaderUT(String name)
	{
		super(name);
	}
	
	public void testLoadNorms() throws Exception {
		UnitTestUtils.start(this);
		V2DatabaseUtils.setUnitTest(true);
		StringBuffer output = new StringBuffer();
		
		StringBuffer sb = new StringBuffer();
		sb.append(" select * from norms n inner join pdi_norm_group ng on ng.v2normgroupid = n.uniqueidstamp ");
		
		StringBuffer sb2 = new StringBuffer();
		sb2.append("insert into pdi_norm (normGroupId, v2ConstructId, mean, stdev, v2normgroupid) values (${1},'${2}',${3},${4},'${5}'); \n");
		
		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
		
		
		DataResult dr = null;
		ResultSet rs = null;
		try
		{
			//dlps.getPreparedStatement().setInt(1, normGroupId);
			dr = V2DatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			
			while (rs.next())
			{
				System.out.println();
				XPath xpath = XPathFactory.newInstance().newXPath(); 
				
				DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
				DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
				Document doc = docBuilder.parse(new InputSource(new StringReader(rs.getString("xml"))));
				int ng = rs.getInt("normGroupId");
				String v2NormGroupID = rs.getString("v2NormGroupID");
				//System.out.println(rs.getString("xml"));
				NodeList records = (NodeList) xpath.evaluate("//C", doc, XPathConstants.NODESET);
				
				for(int i = 0; i < records.getLength(); i++) {
					Node node = records.item(i);
					//DataLayerPreparedStatement dlps2 = V2DatabaseUtils.prepareStatement(sb2);
					String id = XMLUtils.getAttributeValue(node, "Id");
					String mean = XMLUtils.getAttributeValue(node, "Avg");
					String stDev = XMLUtils.getAttributeValue(node, "StdDev");
					if(mean.length() == 0 || stDev.length() == 0 || id.length() == 0) {
						continue;
					}
					//dlps2.getPreparedStatement().setInt(1, ng);
					//dlps2.getPreparedStatement().setString(2, id);
					//dlps2.getPreparedStatement().setDouble(3, Double.parseDouble(mean));
					//dlps2.getPreparedStatement().setDouble(4, Double.parseDouble(stDev));
					//dlps2.getPreparedStatement().setString(5, v2NormGroupID);
					String s = sb2.toString();
					s = s.replace("${1}", ng +"");
					s = s.replace("${2}", id);
					s = s.replace("${3}", mean);
					s = s.replace("${4}", stDev);
					s = s.replace("${5}", v2NormGroupID);
					output.append(s);
					//V2DatabaseUtils.update(dlps2);
					
				}
			}
			
			BufferedWriter out = new BufferedWriter(new FileWriter("c:\\norms.sql"));
			out.write(output.toString());
			out.close();
		}
		finally
		{
	        if (dr != null) { dr.close(); dr = null; }
		}
		
		UnitTestUtils.stop(this);
	}
}
