package com.pdi.data.helpers.interfaces;

import java.util.ArrayList;

import com.pdi.data.dto.EmailTemplate;
import com.pdi.data.dto.SessionUser;

public interface IEmailDataHelper
{
	public void send(SessionUser session,
					 String emailTemplateId,
					 String projectId,
					 ArrayList<String> recipients);
	
	public ArrayList<EmailTemplate> fromClientId(SessionUser session,String clientId);
	
	public EmailTemplate fromId(SessionUser session, String emailTemplateId);
	
	
	// This method is used for testing only
	public boolean testEmailContent(String emailTemplateId, String emailAddr);

}
