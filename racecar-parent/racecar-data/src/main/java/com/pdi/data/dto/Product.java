/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.dto;

import java.util.HashMap;

/**
 * Product is a basic data class that contains product data.
 * 
 * @author		Gavin Myers
 * @author      MB Panichi
 */
public class Product extends CoreDataObject
{
	//
	// Static data.
	//
//	// Must be defined the same as on the Flex side
//	public static int ABYD = 1;       // in v2, is 25, 26
//	public static int TLT = 2;          // in v2 is 23
//	public static int ASSESSMENT = 3;   // in v2, anything other than the above
	
	//
	// Static methods.
	//

	/**
	 * 
	 */
	private static final long serialVersionUID = 7719026187624279935L;
	//
	// Instance data.
	//
	private String id;
	private String name;
	private int code;
	
	private HashMap<String, String> metadata;

	//
	// Constructors.
	//
	public Product()
	{
		// does nothing right now
	}

	//
	// Instance methods.
	//
	
	public String toString()
	{
		String ret = "Product " + (id == null ? "-" : id) + " (" + code + ") - " + (name==null ? "-": name);
//		ret += "\n  Meta:";
//		for (Iterator<Entry<String, String>> itr=metadata.entrySet().iterator(); itr.hasNext(); )
//		{
//			Map.Entry<String, String> ent = itr.next();
//			ret += "\n     " + ent.getKey() + "=" + ent.getValue();
//		}

		return ret;
	}
	
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public void setId(String value)
	{
		id = value;
	}
	
	public String getId()
	{
		return id;
	}

	/*****************************************************************************************/
	public void setName(String value)
	{
		name = value;
	}
	
	public String getName()
	{
		return name;
	}

	/*****************************************************************************************/
	public void setCode(int value)
	{
		code = value;
	}
	
	public int getCode()
	{
		return code;
	}

	/*****************************************************************************************/
	public void setMetadata(HashMap<String, String> value)
	{
		metadata = value;
	}
	
	public HashMap<String, String> getMetadata()
	{
		return metadata;
	}
}
