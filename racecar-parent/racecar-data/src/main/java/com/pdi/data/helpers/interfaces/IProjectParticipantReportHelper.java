package com.pdi.data.helpers.interfaces;

import java.util.ArrayList;

import com.pdi.data.dto.ProjectParticipantReport;
import com.pdi.data.dto.SessionUser;

public interface IProjectParticipantReportHelper {
	/**
	 * Get all report by a participant/engagement link
	 * usecase: I've selected a engagement/participant and I want to see reports from that link
	 * @param session
	 * @param engagementId
	 * @param participantId
	 * @return
	 */
	public ArrayList<ProjectParticipantReport> fromProjectIdParticipantId(SessionUser session, String engagementId, String participantId);
	
	/**
	 * Get a list of reports from a list of ProjectParticipant objects
	 * usecase: I selected a number of engagement/participant objects  and I want to see all reports from that link
	 * @param session
	 * @param epList
	 * @return
	 */
	//public ArrayList<ProjectParticipantReport> fromProjectParticipantList(SessionUser session, ArrayList<ProjectParticipant> epList);
	
//	/**
//	 * Get all reports that this participant has ever done
//	 * usecase: I need to know all instruments this participant has compelted
//	 * @param session
//	 * @param participantId
//	 * @return
//	 */
//	public ArrayList<ProjectParticipantReport> fromParticipantId(SessionUser session, String participantId);
//	
//	/**
//	 * Get all reports that this group of participant has ever done
//	 * usecase: I need to know all instruments this participant has compelted
//	 * @param session
//	 * @param participantId
//	 * @return
//	 */
//	public ArrayList<ProjectParticipantReport> fromParticipantList(SessionUser session, ArrayList<String> participantIds);
//	
//	
//	/**
//	 * Get all reports availabe from this engagement has ever done
//	 * usecase: I need to know all instruments this participant has compelted
//	 * @param session
//	 * @param participantId
//	 * @return
//	 */
//	public ArrayList<ProjectParticipantReport> fromProjectId(SessionUser session, String engagementId);
//
//	/**
//	 * Get all reports availabe from this engagement has ever done
//	 * usecase: I need to know all instruments this participant has compelted
//	 * @param session
//	 * @param participantId
//	 * @return
//	 */
//	public ArrayList<ProjectParticipantReport> fromProjectList(SessionUser session, ArrayList<String> engagementIds);

	
}
