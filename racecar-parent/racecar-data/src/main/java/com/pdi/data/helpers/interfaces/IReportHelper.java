/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.data.helpers.interfaces;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.poi.ss.usermodel.Workbook;

import com.pdi.data.dto.ProjectParticipantInstrument;
import com.pdi.data.dto.SessionUser;
import com.pdi.reporting.response.ReportingResponse;
import com.pdi.reporting.report.ExtractItem;
import com.pdi.reporting.report.ExtractParams;
import com.pdi.reporting.report.GroupReport;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportDataException;
import com.pdi.reporting.request.ReportingRequest;

public interface IReportHelper {
	
	/*
	 * Generate a multi-instrument report
	 */
	public void addReport(SessionUser session,
			              String reportCode,
			              String langCode,
			              ArrayList<ProjectParticipantInstrument> epis,
			              HashMap<String, String> overrides,
			              String combinationReport,
			              String projectName)
		throws Exception;
	
	/*
	 * Generate a group report (array of participants) 
	 */
	//public void addGroupReport(SessionUser session, String reportCode, ArrayList<ProjectParticipantInstrument> epis)  throws Exception;
	
	/*
	 * Generate a single-instrument report
	 */
	public void addReport(SessionUser session, String reportCode, ProjectParticipantInstrument epi, HashMap<String, String> overrides, String combinationReport)  throws Exception;
	
	public ReportingResponse generate(SessionUser session)  throws Exception;

	public ReportingRequest getReportingRequest() throws Exception;

	public Workbook buildPoiXlsForAssessmentExtract(IndividualReport ir) throws ReportDataException;

	public Workbook buildPoiXlsForAbyDExtract(ArrayList<ExtractItem> extractList, GroupReport group) throws ReportDataException;

	public Workbook buildPoiXlsForAbyDAnalyticsExtract(ArrayList<ExtractItem> extractList, GroupReport group, ExtractParams parms) throws ReportDataException;
	
	public Workbook buildPoiXlsForAbyDRawExtract(ArrayList<ExtractItem> extractList, GroupReport group) throws ReportDataException;
	
	public Workbook buildPoiXlsForTLTRawExtract(GroupReport gr) throws ReportDataException;
	
	public Workbook buildPoiXlsForTLTExtract(GroupReport gr) throws ReportDataException;
	
	public String buildXMLForTLTExtract(GroupReport gr) throws ReportDataException;
	
	public void generateRank(IndividualReport ir);
	
	public ArrayList<IndividualReport> useGeneratedRanksToCreateReportOrder( ArrayList<IndividualReport> irs );

	public Workbook buildPoiXlsForALRExtract(ArrayList<ExtractItem> extractList, GroupReport group) throws ReportDataException;
	
	public Workbook buildPoiXlsForAlrDrivers(IndividualReport ir) throws ReportDataException;
	
	public Workbook buildPoiXlsForAlrTraits(IndividualReport ir) throws ReportDataException;
	
	public Workbook buildPoiXlsForRiskFactors(IndividualReport ir) throws ReportDataException;
	
	public Workbook buildPoiXlsForAlpRawExtract(ArrayList<ExtractItem> extractList, GroupReport group);
	
}
