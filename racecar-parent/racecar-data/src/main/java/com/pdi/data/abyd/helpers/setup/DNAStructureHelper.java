/**
 * Copyright (c) 2008, 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.setup;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

import com.pdi.data.abyd.dto.common.DNACellDTO;
import com.pdi.data.abyd.dto.common.DNAStructureDTO;
import com.pdi.data.abyd.dto.common.KeyValuePair;
import com.pdi.data.abyd.dto.common.ModDataDTO;
import com.pdi.data.util.language.DefaultLanguageHelper;

/**
 * DNAStructureHelper contains all the code needed to extract and return a DNAStructureDTO.  It
 * provides data base access and data reduction for the Assessment-by-Design Project Setup app
 *
 * @author		Ken Beukelman
 */
public class DNAStructureHelper
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private Connection connection;
	private long dnaId;
	
	// instantiate a return object
	private DNAStructureDTO ret = new DNAStructureDTO();
	
	private long dfltLangId = 0;

	
	//
	// Constructors.
	//
	public DNAStructureHelper(Connection con, long dnaId)
		throws Exception
	{
		// ensure there is a DNA id
		if (dnaId == 0)
		{
			throw new Exception("A valid DNA id is required to fetch DNA structure data.");
		}

		this.connection = con;
		this.dnaId = dnaId;
		
		// Get the default language
		this.dfltLangId = DefaultLanguageHelper.fetchDefaultLanguageId();
	}

	//
	// Instance methods.
	//

	/**
	 * Fetch the initial data needed for the By Design Setup app.
	 *
	 * @return the EntryStateDTO object
	 * @throws Exception
	 */
	public DNAStructureDTO getSetupDNAStructData()
		throws Exception
	{
		this.ret = new DNAStructureDTO();// Not sure if we truly need to do this but seemed like a good idea at the time...
	    
		long templateDnaId = getDnaTemplateId();
		
		// Populate _modArray and _competencyArray
		getDnaModulesAndCompetencies(templateDnaId);
		
		//Get the DNA structure data from the template
		ArrayList<Long> templateModuleList = new ArrayList<Long>();
		Map<String, DNACellDTO> templateDnaCellsMap = getDnaIntersectionCells(templateDnaId, templateModuleList);
		
		// Get the DNA structure data and overlay... now we can generate the selected module array
		ArrayList<Long> dnaModuleList = new ArrayList<Long>();
		Map<String, DNACellDTO> dnaCellsMap = getDnaIntersectionCells(this.dnaId, dnaModuleList);
		
		this.ret.setSelectedModsArray(dnaModuleList);
		
		// Populate _modCompIntersection
		applyDnaToTemplate(templateDnaCellsMap, dnaCellsMap);
		
		return this.ret;
	}


	/**
	 * Return the raw link structure (used in cloning)
	 * @return
	 * @throws Exception
	 */
	public ArrayList<DNACellDTO> getDnaLinks()
		throws Exception
	{
		ArrayList<Long> dnaModuleList = new ArrayList<Long>();	// Data not used here
		Map<String, DNACellDTO> dnaCellsMap = getDnaIntersectionCells(this.dnaId, dnaModuleList);
		
		// Make the arrayList (note that there will be no "order" to the elements
		ArrayList<DNACellDTO> ret = new ArrayList<DNACellDTO>(dnaCellsMap.values());

		return ret;
	}

	/**
	 * Get the ID of the DNA Template for the given DNA
	 *
	 * @return the ID of a DNA Template
	 * @throws Exception
	 */
	private long getDnaTemplateId()
		throws Exception
	{
	    long ret = 0;
	    Statement stmt = null;
	    ResultSet rs = null;
	    
	    StringBuffer sqlQuery = new StringBuffer();		
		sqlQuery.append("SELECT md.dnaId ");
		sqlQuery.append("  FROM pdi_abd_dna dn ");
		sqlQuery.append("  INNER JOIN pdi_abd_model md ON md.modelId = dn.modelId ");
		sqlQuery.append("  WHERE dn.dnaId = " + this.dnaId);
		
        try
		{
	       	stmt = this.connection.createStatement();
	       	rs = stmt.executeQuery(sqlQuery.toString());
           	
	       	if (! rs.isBeforeFirst())
	       	{
	       		throw new Exception("No Template DNA available for DnaId: " + this.dnaId);
	       	}
	       	if (rs.next())
	       	{
	       	    ret = rs.getLong("dnaId");
	       	}
		}
        catch (SQLException ex)
		{
        	// handle any errors
        	throw new Exception("SQL fetching Template DnaId.  " +
        			"SQLException: " + ex.getMessage() + ", " +
					"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
        finally
		{
            if (rs != null)
            {
                try
				{
                    rs.close();
                }
                catch (SQLException sqlEx)
				{
                	// swallow the error
                }
                rs = null;
            }
            if (stmt != null)
            {
                try
				{
                    stmt.close();
                }
                catch (SQLException sqlEx)
				{
                	// swallow the error
				}
                stmt = null;
            }
        }			
        
       	if (ret == 0)
       	{
       		throw new Exception("No Template DNA available for DnaId: " + this.dnaId);
       	}
		
		return ret;
	}
		
	/**
	 * Get the Modules and Competencies in the DNA Template
	 *
	 * @param templateDnaId a DNA ID for a template
	 * @throws Exception
	 */
	private void getDnaModulesAndCompetencies(long templateDnaId)
		throws Exception
	{
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT DISTINCT 1 as typer, ");
		sqlQuery.append("        1 as groupId, ");
		sqlQuery.append("        1 as groupSeq, ");
		sqlQuery.append("        dl.moduleId as theId, ");
		sqlQuery.append("        dl.modSeq as theSeq, ");
		sqlQuery.append("        dl.modEdit as editFlag, ");
		sqlQuery.append("        tx1.text ");
		sqlQuery.append("  FROM pdi_abd_dna_link dl ");
		sqlQuery.append("    INNER JOIN pdi_abd_module mdul ON mdul.moduleId = dl.moduleId ");
		sqlQuery.append("    LEFT JOIN pdi_abd_text tx1 ON (tx1.textId = mdul.textId ");
		sqlQuery.append("                               AND tx1.languageId = " + this.dfltLangId + ") ");
		sqlQuery.append("    WHERE dl.dnaId = " + templateDnaId);
		sqlQuery.append(" UNION ");
		sqlQuery.append("SELECT DISTINCT 2 as typer, ");
		sqlQuery.append("        dl.groupId, ");
		sqlQuery.append("        dl.groupSeq, ");
		sqlQuery.append("        dl.competencyId as theId, ");
		sqlQuery.append("        dl.compSeq as theSeq, ");
		sqlQuery.append("        0, ");
		sqlQuery.append("        tx2.text ");
		sqlQuery.append("  FROM pdi_abd_dna_link dl ");
		sqlQuery.append("    INNER JOIN pdi_abd_competency cmp ON cmp.competencyId = dl.competencyId ");
		sqlQuery.append("    LEFT JOIN pdi_abd_text tx2 ON (tx2.textId = cmp.textId ");
		sqlQuery.append("                               AND tx2.languageId = " + this.dfltLangId + ") ");
		sqlQuery.append("    WHERE dl.dnaId = " + templateDnaId);
		sqlQuery.append(" ORDER BY typer, groupSeq, theSeq");

		try
		{
		    stmt = this.connection.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			if (! rs.isBeforeFirst())
			{
			    throw new Exception("Template DNA does not exist for ID: " + templateDnaId);
			}

			// process into the mod list and the comp list
			while (rs.next())
			{
			    int type = rs.getInt("typer");

			    if (type == 1)// Module
			    {
					ModDataDTO entry = new ModDataDTO();
					entry.setModId(rs.getLong("theId"));
					entry.setModName(rs.getString("text"));
					// note that the sense of the flag here is the opposite of that in the database
					entry.setModEditable(rs.getBoolean("editFlag"));
					this.ret.getModArray().add(entry);	       	        
			    }
			    else if (type == 2)// Competency
			    {
					KeyValuePair entry = new KeyValuePair();
					entry.setTheKey(rs.getLong("theId"));
					entry.setTheValue(rs.getString("text"));
					this.ret.getCompetencyArray().add(entry);	  	       	        
			    }
			}
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL fetching Template DNA.  " +
							"SQLException: " + ex.getMessage() + ", " +
							"SQLState: " + ex.getSQLState() + ", " +
							"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (rs != null)
			{
			    try
				{
			        rs.close();
			    }
			    catch (SQLException sqlEx)
				{
			        // swallow the error
			    }
			    rs = null;
			}
			if (stmt != null)
			{
			    try
				{
			        stmt.close();
			    }
			    catch (SQLException sqlEx)
				{
			        // swallow the error
				}
			    stmt = null;
			}
		}		    
	}


	/**
	 * Get the Module ID and Competency ID and the isUsed boolean for each available intersection cell 
	 * in the DNA. This method can be used for both a Template and a "customized" DNA.
	 *
	 * @param aDnaId a DNA ID 
	 * @param moduleList a List to populate with all selected moduleIDs
	 * @return a Map of intersection objects (DNACellDTO) with a key of moduleId|competencyId
	 * @throws Exception
	 */
	private Map<String, DNACellDTO> getDnaIntersectionCells(long aDnaId, List<Long> moduleList)
		throws Exception
	{
		Map<String, DNACellDTO> intersectionCellMap = new HashMap<String, DNACellDTO>();
			
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT dl.moduleId, dl.competencyId, dl.isUsed ");
		sqlQuery.append("  FROM pdi_abd_dna_link dl ");
		sqlQuery.append("  WHERE dl.dnaId = " + aDnaId);
		sqlQuery.append("  ORDER BY dl.moduleId, dl.competencyId");

		try
		{
		    stmt = this.connection.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			// Build a map of intersection objects (DNACellDTO) with a key of moduleId|competencyId.
			long previousModuleId = 0;
			while (rs.next())
			{
			    long moduleId = rs.getLong("moduleId");
			    long compId = rs.getLong("competencyId");
			    String modCompKey = moduleId + "|" + compId;
			    
			    boolean isUsed = rs.getBoolean("isUsed");

			    DNACellDTO cell = new DNACellDTO();
			    cell.setModuleId(moduleId);
			    cell.setCompetencyId(compId);
			    cell.setIsUsed(isUsed);			    
			    
			    intersectionCellMap.put(modCompKey, cell);
			    
			    if (previousModuleId != moduleId)
			    {
			        // We've got a new Module from the ResultSet
			        previousModuleId = moduleId;

			        // When we're getting the actual DNA cells, we need to add the moduleId 
			        // to _selectedModsArray in the DNAStructureDTO.
			        //moduleList.add(moduleId);// can't add primitives to an ArrayList so make it a Long.
		            moduleList.add( new Long(moduleId) );
		            // QUESTION-->  Do we need to also check if any of the cells' isUsed is true before we
		            //              add the module to the moduleList here?
		            
			    }
			}
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL fetching DNA Intersection Cells.  " +
							"SQLException: " + ex.getMessage() + ", " +
							"SQLState: " + ex.getSQLState() + ", " +
							"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (rs != null)
			{
			    try
				{
			        rs.close();
			    }
			    catch (SQLException sqlEx)
				{
			        // swallow the error
			    }
			    rs = null;
			}
			if (stmt != null)
			{
			    try
				{
			        stmt.close();
			    }
			    catch (SQLException sqlEx)
				{
			        // swallow the error
				}
			    stmt = null;
			}
		}
		
		return intersectionCellMap;
	}	
	

	/**
	 * Apply the actual DNA Structure (cells) to the Template DNA Structure
	 * overlaying Template isUsed values. The end result of this method is that 
	 * the _modCompIntersection ArrayList in a DNAStructureDTO object is populated.
	 *
	 * @param templateDnaCellsMap 
	 * @param dnaCellsMap
	 */
	private void applyDnaToTemplate(Map<String, DNACellDTO> templateDnaCellsMap, Map<String, DNACellDTO> dnaCellsMap)
	{	
	    if (dnaCellsMap.isEmpty())
	    {
	        // No DNA structure defined yet so have to use the template

	        // Populate the _modCompIntersection ArrayList in the 
		    // DNAStructureDTO object (known as ret in this instance).
		    for (Iterator<DNACellDTO> iter = templateDnaCellsMap.values().iterator(); iter.hasNext(); )
		    {
		        DNACellDTO cell = iter.next();
		        this.ret.getModCompIntersection().add(cell);
		    }	        
	    }
	    else
	    {
	        // DNA structure exists
            // Template is a (possible) superset of the actual structure for this DNA.
            // This code puts the previously selected ids in context with the template,
            // overriding the template where isUsed is different and not doing anything
            // where isUsed is the same or where the template hase the intersection and
            // the persisted DNA does not
	        
	        // Compare each template cell with those in the dna
		    for (Iterator<String> iter = templateDnaCellsMap.keySet().iterator(); iter.hasNext(); )
		    {
		        String templateKey = iter.next();
		        
		        if (dnaCellsMap.containsKey(templateKey))
		        {
		            // The dna has a cell that matches one in the template
		            DNACellDTO dnaCell = (DNACellDTO) dnaCellsMap.get(templateKey);

		            // Update the template DNA to reflect the persisted DNA
		            DNACellDTO tpltCell = (DNACellDTO) templateDnaCellsMap.get(templateKey);
		            if (tpltCell.getIsUsed() != dnaCell.getIsUsed())
		            {
		            	tpltCell.setIsUsed(dnaCell.getIsUsed());
		            }
		        }
		    }
		    
		    // Now populate the _modCompIntersection ArrayList in the 
		    // DNAStructureDTO object (known as ret in this instance).
		    for (Iterator<DNACellDTO> iter = templateDnaCellsMap.values().iterator(); iter.hasNext(); )
		    {
		        DNACellDTO cell = iter.next();
		        this.ret.getModCompIntersection().add(cell);
		    }
	    }
	}
}
