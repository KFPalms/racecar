package com.pdi.data.abyd.helpers.intGrid.ut;

import java.sql.Connection;

import com.pdi.data.abyd.helpers.intGrid.IGComCheckHelper;
import com.pdi.ut.UnitTestUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;

import junit.framework.TestCase;

public class IGComCheckHelperUT  extends TestCase
{
	//
	// Constructors
	//
	public IGComCheckHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
	throws Exception
	{
		junit.textui.TestRunner.run(IGComCheckHelperUT.class);
	}
	
	/*
	 * 
	 */
	public void testConnect()
		throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);
	
		// Connect to the database
		Connection con = AbyDDatabaseUtils.getDBConnection();

		// get the data
		IGComCheckHelper helper = new IGComCheckHelper(con);        
		String ret = helper.comCheck();
		System.out.println("made connection.  Return = " + ret);
		
		UnitTestUtils.stop(this);
	}

}
