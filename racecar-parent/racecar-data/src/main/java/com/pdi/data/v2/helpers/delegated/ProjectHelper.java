/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v2.helpers.delegated;

import java.sql.ResultSet;
//import java.sql.SQLException;
import java.util.ArrayList;
//import java.util.HashMap;
import java.util.HashSet;

import com.pdi.data.dto.Participant;
import com.pdi.data.dto.Project;
import com.pdi.data.dto.ProjectParticipant;
//import com.pdi.data.dto.ProjectParticipantInstrument;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.interfaces.IProjectHelper;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.logging.LogWriter;


public class ProjectHelper implements IProjectHelper {

	
	/**
	 * This is used specifically for the Assessment Testing
	 * 
	 This bit is added per (NHN-2241).
	 	Business needed for the Assessment Report to only report on 
	 	instruments that are part of the Assessment project.
	 	The getInstrumentCodesForProjectId method was added specifically
	 	for this purpose.
	 */
	@Override
	public ArrayList<String> getInstrumentCodesForProjectId(String projectId, String participantId) throws Exception
	{
		ArrayList<String> courseArray = new ArrayList<String>();
		
		StringBuffer sb = new StringBuffer();
		try
		{
			//sb.append(" SELECT instrumentCode ");
			//sb.append("  FROM pdi_participant_instrument_norm_map");
			//sb.append("   where participantId =  '" + participantId.toString() + "'  ");
			//sb.append("   and projectid = '" + projectId.toString() + "' " );

			sb.append("SELECT instrumentCode ");
			sb.append("  FROM pdi_participant_instrument_norm_map ");
			sb.append("  WHERE participantId IN ('" + participantId + "', '0') ");
			sb.append("    AND projectId = '" + projectId + "' ");
			sb.append("    ORDER BY instrumentCode, participantId DESC");		
			//System.out.println("getInstrumentCodesForProjectId.. " + sb.toString());			
			DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
			if (dlps.isInError())
			{
				//System.out.println("Error preparing writeAnswerDataToDatabase query." + dlps.toString());
				LogWriter.logBasic(LogWriter.WARNING, this, "Error preparing getInstrumentCodesForProjectId query." + dlps.toString());
			}

			DataResult dr = null;

			try
			{
				dr = V2DatabaseUtils.select(dlps);
				ResultSet rs = dr.getResultSet();
				
				String code = "";
				
				if (rs != null && rs.isBeforeFirst())
				{
					while (rs.next())
					{
						code = rs.getString("instrumentCode");
						//System.out.println(" code = " + code);
						if (! courseArray.contains(code))
						{
							courseArray.add(code);
						}
					}	// End of while loop
				}	// End of "do we have something" if
			}
			catch(java.sql.SQLException se)
			{
				//se.printStackTrace();
				LogWriter.logSQL(LogWriter.ERROR, this,
						"Error in data fetch in getInstrumentCodesForProjectId."  +
						", partID=" + participantId + ", projID=" + projectId,
						sb.toString());
			}
			finally
			{
		        if (dr != null) { dr.close(); dr = null; }
			}
		} catch(Exception e) {
			//e.printStackTrace();
			LogWriter.logBasic(LogWriter.ERROR, this,
					"General error in getInstrumentCodesForProjectId."  +
					" , partID=" + participantId + ", projID=" + projectId);
		}

		//System.out.println(".......courseArray.size() " + courseArray.size());		
		return courseArray;
	}
		
	
	
	
	/**
	 * fromId
	 * use case:  get engagement from the engagementId
	 * @param  SessionUser session
	 * @param String engagementId
	 * @return Project
	 */
	//@Override
	public Project fromId(SessionUser session, String engagementId)
	{
		
		// Either remove from here and interface or rework using JDBC calls.

//		//TODO Need better spec on what needs to be present (like Client data and full Product data)
//		Project project = new Project();
//		try
//		{
//			String username = session.getMetadata().get("USERNAME");
//			String password = session.getMetadata().get("PASSWORD");
//			V2WebserviceUtils v2web = new V2WebserviceUtils();
//			SessionUserDTO sessionUser = v2web.registerUser(username, password);
//				
//			String xml = "";
//			xml += "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
//			xml += "		<ETR SessionId=\"${sessionId}\">";
//			xml += "			<Rqt Cmd=\"OpenSQL\" AN=\"JobListSQL\" FA=\"Y\" FR=\"1\" RC=\"N\" SQ=\"select j.ClientID, j.UniqueIdStamp, j.Title, c.Name, j.xml, j.DateCreatedStamp, j.JobType from Clients c, Jobs j where c.UniqueIdStamp = j.ClientID and j.uniqueIdStamp = '${projId}' and j.JobType in ('23','25','26')  order by j.Title\" />";
//			xml += "			<Rqt Cmd=\"Read\" AN=\"JobListSQL\" Sub=\"JobList\" Rpt=\"*\" RQ=\"N\">";
//			xml += "				<Fld FN=\"UniqueIdStamp\" Fmt=\"A\" />";
//			xml += "				<Fld FN=\"ClientID\" Fmt=\"A\" />";
//			xml += "				<Fld FN=\"Title\" Fmt=\"A\" />";
//			xml += "				<Fld FN=\"Name\" Fmt=\"A\" />";
//			xml += "				<Fld FN=\"JobType\" Fmt=\"A\" />";
//			xml += "				<Fld FN=\"xml_TPOTTransLevel\" Fmt=\"A\" />";
//			xml += "				<Fld FN=\"xml_TPOTTransLevelCurrent\" Fmt=\"A\" />";
//			xml += "				<Fld FN=\"xml_TPOTTransLevelTarget\" Fmt=\"A\" />";
//			xml += "				<Fld FN=\"xml_TPOTCogsInclded\" Fmt=\"A\" />";
//			//// abyd
//			//xml += "				<Fld FN=\"xml_chooseCogs\" Fmt=\"A\" />";
//			//xml += "				<Fld FN=\"xml_CareerSurvey\" Fmt=\"A\" />";
//			xml += "			</Rqt>";
//			xml += "			<Rqt Cmd=\"Close\" AN=\"JobListSQL\" />";
//			xml += "		</ETR>";
//			
//			xml = xml.replace("${sessionId}", sessionUser.getSessionId());
//			xml = xml.replace("${projId}", engagementId);
//			
//			XPath xpath = XPathFactory.newInstance().newXPath(); 
//			
//			Document doc = v2web.sendData(xml);
//			
//			NodeList records = (NodeList) xpath.evaluate("//Record", doc, XPathConstants.NODESET); 
//			
//			for(int i = 0; i < records.getLength(); i++)
//			{
//				Node node = records.item(i);
//				project.setId(XMLUtils.getAttributeValue(node, "UniqueIdStamp"));
//				//engagement.setName(XMLUtils.xmlFormatString(XMLUtils.getAttributeValue(node, "Title")));
//				project.setName(XMLUtils.getAttributeValue(node, "Title"));
//				
////				Product product = new Product();
////				Integer jobType = new Integer(XMLUtils.getAttributeValue(node, "JobType"));
////				if(jobType.intValue() == 25 || jobType.intValue() == 26){
////					product.setCode(Product.A_BY_D);
////				}else if(jobType.intValue() == 23){
////					product.setCode(Product.TLT);
////				}else{
////					product.setCode(Product.ASSESSMENT);
////				}
//				/*
//				String jobType = V2ProjectConstants.getProjectTypeCode(XMLUtils.getAttributeValue(node, "JobType"));
//				if(jobType.equals(ProjectConstants.EC_TYPE_ABYD_CONSULTANT) ||
//				   jobType.equals(ProjectConstants.EC_TYPE_ABYD_TESTING)){
//					product.setCode(ProjectConstants.EC_PROD_A_BY_D);
//				}else if(jobType.equals(ProjectConstants.EC_TYPE_TLT)){
//					product.setCode(ProjectConstants.EC_PROD_TLT);
//				}else{
//					product.setCode(ProjectConstants.EC_PROD_ASSESSMENT);
//				}
//				engagement.setProduct(product);
//				*/
//			}
//		
//		} catch(Exception e) {
//			e.printStackTrace();
//		}
//		
//		return project;
	
		return null;
	}

	/**
	 * fromClientId
	 * use case:  get list of Projects by clientId
	 * @param SessionUser session
	 * @param String clientId
	 * @return  ArrayList<Project> 
	 * 
	 */
//	//@Override
//	public ArrayList<Project> fromClientId(SessionUser session, String clientId)
//	{
//		
//		// Either remove from here and interface or rework using JDBC calls.
//		// If the later, change jobId to projectId
//
////		ArrayList<Project> projectList = new ArrayList<Project>();
////		System.out.println("Pull a client!");
////		try{
////			
////			String username = session.getMetadata().get("USERNAME");
////			String password = session.getMetadata().get("PASSWORD");
////			V2WebserviceUtils v2web = new V2WebserviceUtils();
////			SessionUserDTO sessionUser = v2web.registerUser(username, password);		
////			
////			String xml = "";
////			xml += "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
////			xml += "		<ETR SessionId=\"${sessionId}\">";
////			xml += "			<Rqt Cmd=\"OpenSQL\" AN=\"JobListSQL\" FA=\"Y\" FR=\"1\" RC=\"N\" SQ=\"select j.ClientID, j.UniqueIdStamp, (select count(*) from candidates c inner join cand_jobs cj on c.uniqueidstamp = cj.candidateid where cj.jobid = j.UniqueIdStamp and cj.archived=0 and c.archived=0) as ParticipantCount, j.Title, c.Name, j.DateCreatedStamp, j.JobType, j.xml from Clients c, Jobs j where c.UniqueIdStamp = j.ClientID and j.ClientID = '${clientId}' and j.JobType in ('23','26','25')  order by j.Title\" />";		
////			xml += "			<Rqt Cmd=\"Read\" AN=\"JobListSQL\" Sub=\"JobList\" Rpt=\"*\" RQ=\"N\">";
////			xml += "				<Fld FN=\"UniqueIdStamp\" Fmt=\"A\" />";
////			xml += "				<Fld FN=\"ClientID\" Fmt=\"A\" />";
////			xml += "				<Fld FN=\"ParticipantCount\" Fmt=\"A\" />";
////			xml += "				<Fld FN=\"Title\" Fmt=\"A\" />";
////			xml += "				<Fld FN=\"Name\" Fmt=\"A\" />";
////			xml += "				<Fld FN=\"JobType\" Fmt=\"A\" />";
////			xml += "				<Fld FN=\"xml_TPOTTransLevel\" Fmt=\"A\" />";
////			xml += "				<Fld FN=\"xml_TPOTTransLevelCurrent\" Fmt=\"A\" />";
////			xml += "				<Fld FN=\"xml_TPOTTransLevelTarget\" Fmt=\"A\" />";
////			//// abyd
////			//xml += "				<Fld FN=\"xml_chooseCogs\" Fmt=\"A\" />";
////			//xml += "				<Fld FN=\"xml_CareerSurvey\" Fmt=\"A\" />";
////			xml += "			</Rqt>";
////			xml += "			<Rqt Cmd=\"Close\" AN=\"JobListSQL\" />";
////			xml += "		</ETR>";
////			
////			xml = xml.replace("${sessionId}", sessionUser.getSessionId());
////			xml = xml.replace("${clientId}", clientId);
////			
////			XPath xpath = XPathFactory.newInstance().newXPath(); 
////			Document doc = v2web.sendData(xml);			
////			NodeList records = (NodeList) xpath.evaluate("//Record", doc, XPathConstants.NODESET); 		
////			for(int i = 0; i < records.getLength(); i++)
////			{
////				Node node = records.item(i);
////				Project project = new Project();
////				project.setId(XMLUtils.getAttributeValue(node, "UniqueIdStamp"));
////				//engagement.setName(XMLUtils.xmlFormatString(XMLUtils.getAttributeValue(node, "Title")));
////				project.setName(XMLUtils.getAttributeValue(node, "Title"));
////				
//////				Product product = new Product();
//////				Integer jobType = new Integer(XMLUtils.getAttributeValue(node, "JobType"));
//////				if(jobType.intValue() == 25 || jobType.intValue() == 26){
//////					product.setCode(Product.A_BY_D);
//////				}else if(jobType.intValue() == 23){
//////					product.setCode(Product.TLT);
//////				}else{
//////					product.setCode(Product.ASSESSMENT);
//////				}
////				/*
////				String jobType = V2ProjectConstants.getProjectTypeCode(XMLUtils.getAttributeValue(node, "JobType"));
////				if(jobType.equals(ProjectConstants.EC_TYPE_ABYD_CONSULTANT) ||
////				   jobType.equals(ProjectConstants.EC_TYPE_ABYD_TESTING)){
////					product.setCode(ProjectConstants.EC_PROD_A_BY_D);
////				}else if(jobType.equals(ProjectConstants.EC_TYPE_TLT)){
////					product.setCode(ProjectConstants.EC_PROD_TLT);
////				}else{
////					product.setCode(ProjectConstants.EC_PROD_ASSESSMENT);
////				}
////				engagement.setProduct(product);
////				*/
////				projectList.add(project);
////			}
////		
////		} catch(Exception e) {
////			e.printStackTrace();
////		}
////		
////		return projectList;
//		
//		return null;
//	}
//	//@Override
//	public ArrayList<Project> fromClientId(SessionUser session, String clientId)
//	{
//		
//		// Either remove from here and interface or rework using JDBC calls.
//		// If the later, change jobId to projectId
//
////		ArrayList<Project> projectList = new ArrayList<Project>();
////		System.out.println("Pull a client!");
////		try{
////			
////			String username = session.getMetadata().get("USERNAME");
////			String password = session.getMetadata().get("PASSWORD");
////			V2WebserviceUtils v2web = new V2WebserviceUtils();
////			SessionUserDTO sessionUser = v2web.registerUser(username, password);		
////			
////			String xml = "";
////			xml += "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
////			xml += "		<ETR SessionId=\"${sessionId}\">";
////			xml += "			<Rqt Cmd=\"OpenSQL\" AN=\"JobListSQL\" FA=\"Y\" FR=\"1\" RC=\"N\" SQ=\"select j.ClientID, j.UniqueIdStamp, (select count(*) from candidates c inner join cand_jobs cj on c.uniqueidstamp = cj.candidateid where cj.jobid = j.UniqueIdStamp and cj.archived=0 and c.archived=0) as ParticipantCount, j.Title, c.Name, j.DateCreatedStamp, j.JobType, j.xml from Clients c, Jobs j where c.UniqueIdStamp = j.ClientID and j.ClientID = '${clientId}' and j.JobType in ('23','26','25')  order by j.Title\" />";		
////			xml += "			<Rqt Cmd=\"Read\" AN=\"JobListSQL\" Sub=\"JobList\" Rpt=\"*\" RQ=\"N\">";
////			xml += "				<Fld FN=\"UniqueIdStamp\" Fmt=\"A\" />";
////			xml += "				<Fld FN=\"ClientID\" Fmt=\"A\" />";
////			xml += "				<Fld FN=\"ParticipantCount\" Fmt=\"A\" />";
////			xml += "				<Fld FN=\"Title\" Fmt=\"A\" />";
////			xml += "				<Fld FN=\"Name\" Fmt=\"A\" />";
////			xml += "				<Fld FN=\"JobType\" Fmt=\"A\" />";
////			xml += "				<Fld FN=\"xml_TPOTTransLevel\" Fmt=\"A\" />";
////			xml += "				<Fld FN=\"xml_TPOTTransLevelCurrent\" Fmt=\"A\" />";
////			xml += "				<Fld FN=\"xml_TPOTTransLevelTarget\" Fmt=\"A\" />";
////			//// abyd
////			//xml += "				<Fld FN=\"xml_chooseCogs\" Fmt=\"A\" />";
////			//xml += "				<Fld FN=\"xml_CareerSurvey\" Fmt=\"A\" />";
////			xml += "			</Rqt>";
////			xml += "			<Rqt Cmd=\"Close\" AN=\"JobListSQL\" />";
////			xml += "		</ETR>";
////			
////			xml = xml.replace("${sessionId}", sessionUser.getSessionId());
////			xml = xml.replace("${clientId}", clientId);
////			
////			XPath xpath = XPathFactory.newInstance().newXPath(); 
////			Document doc = v2web.sendData(xml);			
////			NodeList records = (NodeList) xpath.evaluate("//Record", doc, XPathConstants.NODESET); 		
////			for(int i = 0; i < records.getLength(); i++)
////			{
////				Node node = records.item(i);
////				Project project = new Project();
////				project.setId(XMLUtils.getAttributeValue(node, "UniqueIdStamp"));
////				//engagement.setName(XMLUtils.xmlFormatString(XMLUtils.getAttributeValue(node, "Title")));
////				project.setName(XMLUtils.getAttributeValue(node, "Title"));
////				
//////				Product product = new Product();
//////				Integer jobType = new Integer(XMLUtils.getAttributeValue(node, "JobType"));
//////				if(jobType.intValue() == 25 || jobType.intValue() == 26){
//////					product.setCode(Product.A_BY_D);
//////				}else if(jobType.intValue() == 23){
//////					product.setCode(Product.TLT);
//////				}else{
//////					product.setCode(Product.ASSESSMENT);
//////				}
////				/*
////				String jobType = V2ProjectConstants.getProjectTypeCode(XMLUtils.getAttributeValue(node, "JobType"));
////				if(jobType.equals(ProjectConstants.EC_TYPE_ABYD_CONSULTANT) ||
////				   jobType.equals(ProjectConstants.EC_TYPE_ABYD_TESTING)){
////					product.setCode(ProjectConstants.EC_PROD_A_BY_D);
////				}else if(jobType.equals(ProjectConstants.EC_TYPE_TLT)){
////					product.setCode(ProjectConstants.EC_PROD_TLT);
////				}else{
////					product.setCode(ProjectConstants.EC_PROD_ASSESSMENT);
////				}
////				engagement.setProduct(product);
////				*/
////				projectList.add(project);
////			}
////		
////		} catch(Exception e) {
////			e.printStackTrace();
////		}
////		
////		return projectList;
//		
//		return null;
//	}

	//@Override
	public ArrayList<Project> fromClientIdByName(SessionUser session, String clientId)
	{
		// TODO Auto-generated method stub
		return null;
	}

	//@Override
	public ArrayList<Project> fromClientIdByDate(SessionUser session, String clientId)
	{
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * fromParticipantId
	 * use case: get list of Projects for this participant
	 * @param SessionUser session
	 * @param String participantId
	 * @return ArrayList<Project>
	 */
	//@Override
	public ArrayList<Project> fromParticipantId(SessionUser session, String participantId){
		
		// Either remove from here and interface or rework using JDBC calls.
		// If the later, change jobId to projectId
		// If the later, change candidateId to participantId

//		ArrayList<Project> projectList = new ArrayList<Project>();
//		
//		try{
//			
//			String username = session.getMetadata().get("USERNAME");
//			String password = session.getMetadata().get("PASSWORD");
//			V2WebserviceUtils v2web = new V2WebserviceUtils();
//			SessionUserDTO sessionUser = v2web.registerUser(username, password);	
//		
//			String xml = "";
//			xml += "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
//			xml += "		<ETR SessionId=\"${sessionId}\">";
//			xml += "			<Rqt Cmd=\"OpenSQL\" AN=\"JobListSQL\" FA=\"Y\" FR=\"1\" RC=\"N\" SQ=\"select j.clientId, j.uniqueIdStamp, j.Title, j.JobType, j.dateCreatedStamp, j.xml from candidates c, cand_jobs cj, jobs j  where c.uniqueIdstamp = '${candidateId}' and c.uniqueIdStamp = cj.candidateId and cj.jobId = j.uniqueIdstamp and cj.archived='0' and c.archived='0' and j.JobType in ('23','26','25')  order by j.Title\" />";
//			xml += "			<Rqt Cmd=\"Read\" AN=\"JobListSQL\" Sub=\"JobList\" Rpt=\"*\" RQ=\"N\">";
//			xml += "				<Fld FN=\"ClientID\" Fmt=\"A\" />";
//			xml += "				<Fld FN=\"UniqueIdStamp\" Fmt=\"A\" />";
//			xml += "				<Fld FN=\"ParticipantCount\" Fmt=\"A\" />";
//			xml += "				<Fld FN=\"Title\" Fmt=\"A\" />";
//			xml += "				<Fld FN=\"JobType\" Fmt=\"A\" />";
//			xml += "				<Fld FN=\"dateCreatedStamp\" Fmt=\"A\" />";
//			xml += "				<Fld FN=\"xml_TPOTTransLevel\" Fmt=\"A\" />";
//			xml += "				<Fld FN=\"xml_TPOTTransLevelCurrent\" Fmt=\"A\" />";
//			xml += "				<Fld FN=\"xml_TPOTTransLevelTarget\" Fmt=\"A\" />";
//			//// abyd
//			//xml += "				<Fld FN=\"xml_chooseCogs\" Fmt=\"A\" />";
//			//xml += "				<Fld FN=\"xml_CareerSurvey\" Fmt=\"A\" />";
//			xml += "			</Rqt>";
//			xml += "			<Rqt Cmd=\"Close\" AN=\"JobListSQL\" />";
//			xml += "		</ETR>";
//			
//			xml = xml.replace("${sessionId}", sessionUser.getSessionId());
//			xml = xml.replace("${candidateId}", participantId);
//			
//			XPath xpath = XPathFactory.newInstance().newXPath(); 
//			Document doc = v2web.sendData(xml);
//						
//			NodeList records = (NodeList) xpath.evaluate("//Record", doc, XPathConstants.NODESET);
//			for(int i = 0; i < records.getLength(); i++)
//			{
//				Node node = records.item(i);
//				Project engagement = new Project();
//				engagement.setId(XMLUtils.getAttributeValue(node, "UniqueIdStamp"));
//				//engagement.setName(XMLUtils.xmlFormatString(XMLUtils.getAttributeValue(node, "Title")));
//				engagement.setName(XMLUtils.getAttributeValue(node, "Title"));
//				
////				Product product = new Product();
////				Integer jobType = new Integer(XMLUtils.getAttributeValue(node, "JobType"));
////				if(jobType.intValue() == 25 || jobType.intValue() == 26){
////					product.setCode(Product.A_BY_D);
////				}else if(jobType.intValue() == 23){
////					product.setCode(Product.TLT);
////				}else{
////					product.setCode(Product.ASSESSMENT);
////				}
//				/*
//				String jobType = V2ProjectConstants.getProjectTypeCode(XMLUtils.getAttributeValue(node, "JobType"));
//				if(jobType.equals(ProjectConstants.EC_TYPE_ABYD_CONSULTANT) ||
//				   jobType.equals(ProjectConstants.EC_TYPE_ABYD_TESTING)){
//					product.setCode(ProjectConstants.EC_PROD_A_BY_D);
//				}else if(jobType.equals(ProjectConstants.EC_TYPE_TLT)){
//					product.setCode(ProjectConstants.EC_PROD_TLT);
//				}else{
//					product.setCode(ProjectConstants.EC_PROD_ASSESSMENT);
//				}
//				engagement.setProduct(product);
//*/
//				projectList.add(engagement);
//			}
//		
//		} catch(Exception e) {
//			e.printStackTrace();
//		}
//
//		return projectList;

		return null;
	}

//	//@Override
//	public ArrayList<Project> fromOwnerId(SessionUser session,
//			String clientId) {
//		// TODO Auto-generated method stub
//		return null;
//	}
	
	//@Override
	public ArrayList<Project> fromOwnerIdByDate(SessionUser session, String adminId) {
		// TODO Auto-generated method stub
		return null;
	}
	
	//@Override
	public ArrayList<Project> fromOwnerIdByName(SessionUser session, String adminId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProjectParticipant addParticipant(SessionUser session, Project project, String participantId) {
		// TODO Auto-generated method stub
		
		return null;
	}

	@Override
	public boolean addParticipants(SessionUser session, Project project, ArrayList<String> participants) {
		// TODO Auto-generated method stub
		
		return false;
	}

	@Override
	public boolean removeParticipant(SessionUser session, Project project, String participantId) {
		// TODO Auto-generated method stub
		
		return false;
	}

	@Override
	public boolean removeParticipants(SessionUser session, Project project, ArrayList<Participant> participants) {
		// TODO Auto-generated method stub
		
		return false;
	}


	@Override
	public Project createProject(SessionUser session, Project project){
		// TODO Auto-generated method stub
		
		return new Project();
	}
	

//	@Override
//	public ArrayList<Project> find(SessionUser session, String clientId, String target)
//	{
//		// TODO Auto-generated method stub
//		return new ArrayList<Project>();
//	}
	@Override
	public ArrayList<Project> findForClientId(SessionUser session, String clientId, String target)
	{
		// TODO Auto-generated method stub
		return new ArrayList<Project>();
	}
	@Override
	public ArrayList<Project> findForAdminId(SessionUser session, String adminId, String target)
	{
		// TODO Auto-generated method stub
		return new ArrayList<Project>();
	}


	@Override
	public boolean updateProject(SessionUser session, Project project)
	{
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean updateCoreProject(SessionUser session, Project project)
	{
		// TODO Auto-generated method stub
		return false;
	}
//
//	
//	@Override
//	public boolean updateMetadataItems(SessionUser session, String projectId, HashMap<String, String> meta)
//	{
//		// TODO Auto-generated method stub
//		return false;
//	}
	
	
	@Override
	public boolean updateContentItems(SessionUser session, String projectId, HashSet<String> content)
	{
		// TODO Auto-generated method stub
		return false;
	}
//	
//	
//	@Override
//	public boolean removeMetadataItems(SessionUser session, String projectId, HashSet<String> meta)
//	{
//		// TODO Auto-generated method stub
//		return false;
//	}
//	
//	
//	@Override
//	public boolean removeMetadataItems(SessionUser session, String projectId, HashMap<String, String> meta)
//	{
//		// TODO Auto-generated method stub
//		return false;
//	}
//	
//	
//	@Override
//	public boolean removeAllMetadataItems(SessionUser session, String projectId)
//	{
//		// TODO Auto-generated method stub
//		return false;
//	}
	
	
	@Override
	public boolean removeContentItems(SessionUser session, String projectId, HashSet<String> content)
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	
	@Override
	public boolean removeAllContentItems(SessionUser session, String projectId)
	{
		// TODO Auto-generated method stub
		return false;
	}

}
