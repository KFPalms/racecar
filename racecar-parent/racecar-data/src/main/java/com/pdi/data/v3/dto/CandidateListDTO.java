/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.dto;

import java.util.ArrayList;

import com.pdi.data.util.BaseDTO;
import com.pdi.data.util.RequestStatus;

/**
 * DTO for a list of candidate information objects
 *
 * @author		Ken Beukelman
 */
public class CandidateListDTO extends BaseDTO
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private ArrayList<CandidateDTO> _candidateList = new ArrayList<CandidateDTO>();


	//
	// Constructors.
	//

 	public CandidateListDTO()
	{
 		super();
		// does nothing right now
	}
	
	// Set up a new DTO with status but no data
	public CandidateListDTO(RequestStatus stat)
	{
		this.setStatus(stat);
	}
	
	// Set up a new DTO with data (assumes no data problems)
	// It does check for data
	public CandidateListDTO(ArrayList<CandidateDTO> ary)
	{
		this.setCandidateList(ary);
		if (ary.size() < 1)
		{
			this.getStatus().setStatusCode(RequestStatus.WB_NO_DATA);
		}
	}
	
	
	//
	// Instance methods.
	//

	/**
	 * Generic toString method
	 */
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append("CandidateListDTO:  ");
		sb.append("Status code=" + this.getStatus().getStatusCode() + ", count=" + _candidateList.size());
		for(int i=0; i < _candidateList.size(); i++)
		{
			_candidateList.get(i).toString();
		}
		return sb.toString();
	}
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public ArrayList<CandidateDTO> getCandidateList()
	{
		return _candidateList;
	}
	
	public void setCandidateList(ArrayList<CandidateDTO> value)
	{
		_candidateList = value;
	}	
}
