package com.pdi.data.abyd.helpers.intGrid.ut;

import java.sql.Connection;

import com.pdi.data.abyd.dto.intGrid.IGFinalScoreDTO;
import com.pdi.data.abyd.helpers.intGrid.SaveIGCompScoreDataHelper;
import com.pdi.ut.UnitTestUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;

import junit.framework.TestCase;

public class SaveIGCompScoreDataHelperUT  extends TestCase
{
	//
	// Constructors
	//
	public SaveIGCompScoreDataHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
	throws Exception
	{
		junit.textui.TestRunner.run(SaveIGCompScoreDataHelperUT.class);
	}


	/*
	 * testSaveScore
	 */
	public void testSaveScore()
		throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);
		
		Connection con = AbyDDatabaseUtils.getDBConnection();
		IGFinalScoreDTO inp = new IGFinalScoreDTO();
		inp.setPartId("9999");
    	inp.setDnaId(99);
    	inp.setCompId(100);
       	inp.setCompScoreIdx(9);
    	inp.setCompNote("Comp Note - HUT");

		SaveIGCompScoreDataHelper helper = new SaveIGCompScoreDataHelper(con, inp);        
		helper.saveScore();
		System.out.println("Complete... check database");

		UnitTestUtils.stop(this);
	}

}
