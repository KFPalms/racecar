package com.pdi.data.helpers;
import java.util.MissingResourceException;

//import org.hibernate.cfg.Environment;

import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.interfaces.IAttachmentHelper;
import com.pdi.data.helpers.interfaces.IClientHelper;
import com.pdi.data.helpers.interfaces.IEmailDataHelper;
import com.pdi.data.helpers.interfaces.IEquivalentScoreHelper;
import com.pdi.data.helpers.interfaces.ILanguageHelper;
import com.pdi.data.helpers.interfaces.IProjectHelper;
import com.pdi.data.helpers.interfaces.IProjectParticipantInstrumentHelper;
import com.pdi.data.helpers.interfaces.IProjectParticipantReportHelper;
import com.pdi.data.helpers.interfaces.IMigrationHelper;
import com.pdi.data.helpers.interfaces.INormGroupHelper;
import com.pdi.data.helpers.interfaces.IReportHelper;
import com.pdi.data.helpers.interfaces.IScoringHelper;
import com.pdi.data.helpers.interfaces.INormHelper;
import com.pdi.data.helpers.interfaces.ITestDataHelper;
import com.pdi.data.helpers.interfaces.ITextHelper;
//import com.pdi.data.helpers.interfaces.IProjectProductHelper;
import com.pdi.data.helpers.interfaces.IInstrumentHelper;
import com.pdi.data.helpers.interfaces.INoteHelper;
import com.pdi.data.helpers.interfaces.IParticipantHelper;
import com.pdi.data.helpers.interfaces.ISessionUserHelper;
import com.pdi.properties.PropertyLoader;

public class HelperDelegate {

	private static String envrionment;

	//TODO: Make this work
	private SessionUser sessionUser;

	@SuppressWarnings("unchecked")
	private static Object getObject(String path) throws Exception {
		Class c = Class.forName(path);
    	return c.newInstance();
	}

	public static IReportHelper getReportHelper()  throws Exception{
		return (IReportHelper)getObject("com.pdi.listener.portal.delegated.helpers.ReportHelper");
	}


	public static IScoringHelper getScoringHelper()  throws Exception{
		return (IScoringHelper)getObject("com.pdi.listener.portal.delegated.helpers.ScoringHelper");
	}


	public static IClientHelper getClientHelper(String env)  throws Exception{
		return (IClientHelper)getObject(env+".ClientHelper");
	}

	public static IClientHelper getClientHelper()  throws Exception{
		return getClientHelper(getEnvironment());
	}

	public static IProjectHelper getProjectHelper(String env)  throws Exception{
		return (IProjectHelper)getObject(env+".ProjectHelper");
	}

	public static IProjectHelper getProjectHelper()  throws Exception{
		return getProjectHelper(getEnvironment());
	}

	public static IInstrumentHelper getInstrumentHelper(String env)  throws Exception{
		return (IInstrumentHelper)getObject(env+".InstrumentHelper");
	}
	public static IInstrumentHelper getInstrumentHelper()  throws Exception{
		return getInstrumentHelper(getEnvironment());
	}

	public static IMigrationHelper getMigrationHelper(String env)  throws Exception{
		return (IMigrationHelper)getObject(env+".MigrationHelper");
	}
	public static IMigrationHelper getMigrationHelper()  throws Exception{
		return getMigrationHelper(getEnvironment());
	}
	public static ILanguageHelper getLanguageHelper(String env)  throws Exception{
		return (ILanguageHelper)getObject(env+".LanguageHelper");
	}
	
	public static IParticipantHelper getParticipantHelper(String env)  throws Exception{
		return (IParticipantHelper)getObject(env+".ParticipantHelper");
	}
	public static IParticipantHelper getParticipantHelper()  throws Exception{
		return getParticipantHelper(getEnvironment());
	}

	public static ISessionUserHelper getSessionUserHelperHelper(String env)  throws Exception{
		return (ISessionUserHelper)getObject(env+".SessionUserHelper");
	}
	public static ISessionUserHelper getSessionUserHelperHelper()  throws Exception{
		return getSessionUserHelperHelper(getEnvironment());
	}

	public static IProjectParticipantInstrumentHelper getProjectParticipantInstrumentHelper(String env)  throws Exception{
		return (IProjectParticipantInstrumentHelper)getObject(env+".ProjectParticipantInstrumentHelper");
	}

	public static IProjectParticipantInstrumentHelper getProjectParticipantInstrumentHelper()  throws Exception{
		return getProjectParticipantInstrumentHelper(getEnvironment());
	}

	public static IProjectParticipantReportHelper getProjectParticipantReportHelper(String env)  throws Exception{
		return (IProjectParticipantReportHelper)getObject(env+".ProjectParticipantReportHelper");
	}

	public static IProjectParticipantReportHelper getProjectParticipantReportHelper()  throws Exception{
		return getProjectParticipantReportHelper(getEnvironment());
	}

//	public static IProjectProductHelper getProjectProductHelper(String env)  throws Exception{
//		return (IProjectProductHelper)getObject(env+".ProjectParticipantReportHelper");
//	}
//
//	public static IProjectProductHelper getProjectProductHelper()  throws Exception{
//		return getProjectProductHelper(getEnvironment());
//	}

	public static INoteHelper getNoteHelper(String env)  throws Exception{
		return (INoteHelper)getObject(env+".NoteHelper");
	}

	public static INoteHelper getNoteHelper()  throws Exception{
		return getNoteHelper(getEnvironment());
	}

	public static IAttachmentHelper getAttachmentHelper(String env)  throws Exception{
		return (IAttachmentHelper)getObject(env+".AttachmentHelper");
	}

	public static IAttachmentHelper getAttachmentHelper()  throws Exception{
		return getAttachmentHelper(getEnvironment());
	}

	public static INormHelper getNormHelper(String env)  throws Exception{
		return (INormHelper)getObject(env+".NormHelper");
	}

	public static INormHelper getNormHelper()  throws Exception{
		return getNormHelper(getEnvironment());
	}

	public static INormGroupHelper getNormGroupHelper(String env)  throws Exception{
		return (INormGroupHelper)getObject(env+".NormGroupHelper");
	}
	public static INormGroupHelper getNormGroupHelper()  throws Exception{
		return getNormGroupHelper(getEnvironment());
	}

	public static IEmailDataHelper getEmailDataHelper(String env)  throws Exception{
		return (IEmailDataHelper)getObject(env+".EmailDataHelper");
	}

	public static IEmailDataHelper getEmailDataHelper()  throws Exception{
		return getEmailDataHelper(getEnvironment());
	}

	public static ITextHelper getTextHelper(String env)
		throws Exception
	{
		return (ITextHelper)getObject(env+".TextHelper");
	}

	public static ITextHelper getTextHelper()
		throws Exception
	{
		return getTextHelper(getEnvironment());
	}

	public static ITestDataHelper getTestDataHelper(String env)
	throws Exception
	{
		return (ITestDataHelper)getObject(env+".TestDataHelper");
	}
	
	public static ITestDataHelper getTestDataHelper()
		throws Exception
	{
		return getTestDataHelper(getEnvironment());
	}
	
	public static IEquivalentScoreHelper getEquivalentScoreHelper(String env)
		throws Exception
	{
		return (IEquivalentScoreHelper)getObject(env+".EquivalentScoreHelper");
	}
	
	public static IEquivalentScoreHelper getEquivalentScoreHelper()
		throws Exception
	{
		return getEquivalentScoreHelper(getEnvironment());
	}

	private static String getEnvironment() throws Exception {
		try {
			if(getEnvrionmentBundle() != null && getEnvrionmentBundle().length() > 0) {
				return getEnvrionmentBundle();
			} else {
				return PropertyLoader.getProperty("com.pdi.data.application", PropertyLoader.getProperty("application", "global.application"));
			}

		} catch(MissingResourceException e) {
			//this is a unit test and has no line of sight to the application context, this should only happen during a unit test
			return PropertyLoader.getProperty("com.pdi.data.application", "global.application");
		}
	}

	public static void setEnvrionment(String e) {
		envrionment = e;
	}

	private static String getEnvrionmentBundle() {
		return envrionment;
	}

	public void setSessionUser(SessionUser su) {
		sessionUser = su;
	}

	public SessionUser getSessionUser() {
		return sessionUser;
	}
}
