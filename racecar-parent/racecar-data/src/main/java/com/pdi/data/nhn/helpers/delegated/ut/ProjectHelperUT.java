/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.data.nhn.helpers.delegated.ut;

import com.pdi.data.helpers.interfaces.IProjectHelper;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.nhn.util.NhnDatabaseUtils;
import com.pdi.data.dto.Project;
import com.pdi.data.dto.SessionUser;
import com.pdi.ut.UnitTestUtils;
import junit.framework.TestCase;

public class ProjectHelperUT extends TestCase {

	public ProjectHelperUT(String name) 	{
		super(name);
	}
	
	/*
	 * testing the logger.... 
	 * 
	 */
//	public void testGetLogging() throws Exception {
//		UnitTestUtils.start(this);
//		NhnDatabaseUtils.setUnitTest(true);
//		
//		LogWriter.logBasic(LogWriter.ERROR, this, 
//				"TEST LOG FILE WRITER.... ");
//		
//		
//		UnitTestUtils.stop(this);
//	}


	/*
	 * getProject
	 */
	public void testGetProject() throws Exception {
		UnitTestUtils.start(this);
		NhnDatabaseUtils.setUnitTest(true);
		
		SessionUser session = new SessionUser();
		
		IProjectHelper iproject = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated"); 

		//String projectId = "35";         
		//String projectId = "330";         
		//String projectId = "329";         
		//String projectId = "360";         
		String projectId = "427";         
		Project project = iproject.fromId(session, projectId);
		
		try{
			System.out.println("project.getId = " + project.getId());
			System.out.println("project.getName = " + project.getName());
			//System.out.println("project.get_projectCode = " + project.get_projectCode());
			System.out.println("project.getProjectTypeId = " + project.getProjectTypeId());
			System.out.println("project.getProjectTypeName = " + project.getProjectTypeName());
			System.out.println("project.getTargetLevelTypeId = " + project.getTargetLevelTypeId());
			System.out.println("project.getClient.getName = " + project.getClient().getName());
			System.out.println("project.getMetadata = " + project.getMetadata().toString());
			System.out.println("project.getContentCollection = " + project.getContentCollection().toString());
			System.out.println("project.getParticipantCollection = " + project.getParticipantCollection().toString());
		}catch (Exception e) {
			System.out.println("projectId " + projectId + " did not return a valid project.");
		}
		
//		String participantId = "104156";
//		ArrayList<Project> pProjects = iproject.fromParticipantId(null, participantId);
//		try {
//			StringBuffer ps = new StringBuffer();
//			int numProjects = pProjects.size();
//			ps.append("Projects by user " + participantId + " selection was successful! ");
//			ps.append("The projects are: ");
//			for (Project p : pProjects){
//				ps.append(p.getName() + ",");
//			}
//			System.out.println(ps + " " + numProjects + " project(s) total.");
//		}catch (Exception e) {
//			System.out.println("no project/project found for id " + participantId + ".");
//		}
//
//		// For more testing of "by name" and "by date" fetches, see relevant tests below
//		String clientId = "504";
//		ArrayList<Project> cProjects = iproject.fromClientIdByName(null, clientId);
//		try {
//			StringBuffer ps = new StringBuffer();
//			int numProjects = cProjects.size();
//			ps.append("Projects by client " + clientId + " selection was successful! ");
//			ps.append("The projects are: ");
//			for (Project p : cProjects){
//				ps.append(p.getName() + ",");
//			}
//			System.out.println(ps + " " + numProjects + " project(s) total.");
//		}catch (Exception e) {
//			System.out.println("no project/project found for id " + clientId + ".");
//		}
	}


	/*
	 * getProjectByName
	 */
//	public void testGetProjectByName()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		NhnDatabaseUtils.setUnitTest(true);
//		
//		SessionUser session = new SessionUser();
//		
//		IProjectHelper iproject = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated"); 
//
//		String clientId = "751";
//		ArrayList<Project> cProjects = iproject.fromClientIdByName(session, clientId);
//		System.out.println("Selected " + cProjects.size() + " projects in name order for client " + clientId);
//		if (cProjects.size() > 0)
//		{
//			System.out.println("Projects:");
//			boolean isFirst = true;
//			for (Project p : cProjects)
//			{
//				String str = "  " + p.getName();
//				if (isFirst)
//					isFirst = false;
//				else
//					str += ",";
//				System.out.println(str);
//			}
//		}
//		
//		cProjects = null;
//		String adminId = "55422";
//		cProjects = iproject.fromOwnerIdByName(session, adminId);
//		System.out.println("Selected " + cProjects.size() + " projects in name order for admin ID " + adminId);
//		if (cProjects.size() > 0)
//		{
//			System.out.println("Projects:");
//			boolean isFirst = true;
//			for (Project p : cProjects)
//			{
//				String str = "  " + p.getName();
//				if (isFirst)
//					isFirst = false;
//				else
//					str += ",";
//				System.out.println(str);
//			}
//		}
//	}


	/*
	 * testGetProjectByDate
	 */
//	public void testGetProjectByDate()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		NhnDatabaseUtils.setUnitTest(true);
//		
//		SessionUser session = new SessionUser();
//		
//		IProjectHelper iproject = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated"); 
//
//		String clientId = "751";
//		ArrayList<Project> cProjects = iproject.fromClientIdByDate(session, clientId);
//		System.out.println("Selected " + cProjects.size() + " projects in date order for client " + clientId);
//		if (cProjects.size() > 0)
//		{
//			System.out.println("Projects:");
//			boolean isFirst = true;
//			for (Project p : cProjects)
//			{
//				String str = "  " + p.getId() + ": " + p.getDateCreated() + " - " + p.getName();
//				if (isFirst)
//					isFirst = false;
//				else
//					str += ",";
//				System.out.println(str);
//			}
//		}
//
//		cProjects = null;
//		String adminId = "55422";
//		cProjects = iproject.fromOwnerIdByDate(session, adminId);
//		System.out.println("Selected " + cProjects.size() + " projects in date order for admin ID " + adminId);
//		if (cProjects.size() > 0)
//		{
//			System.out.println("Projects:");
//			boolean isFirst = true;
//			for (Project p : cProjects)
//			{
//				String str = "  " + p.getId() + ": " + p.getDateCreated() + " - " + p.getName();
//				if (isFirst)
//					isFirst = false;
//				else
//					str += ",";
//				System.out.println(str);
//			}
//		}
//	}


	/*
	 * addParticipant
	 */
//	public void testAddParticipant()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		NhnDatabaseUtils.setUnitTest(true);
//		
//		SessionUser session = new SessionUser();
//
//		Project proj = new Project();
//		proj.setId("9");
//		String partId = "52349";
//		
//		IProjectHelper helper = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated"); 
//		ProjectParticipant pp = helper.addParticipant(session, proj, partId);
//
//		assertNotNull("Data not inserted", pp);
//		
//		// Dump the data
//		System.out.println("ret:  " + pp.toString());
//
//		UnitTestUtils.stop(this);
//	}


	/*
	 * removeParticipant
	 */
//	public void testRemoveParticipant()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		NhnDatabaseUtils.setUnitTest(true);
//		
//		SessionUser session = new SessionUser();
//
//		Project proj = new Project();
//		proj.setId("9");
//		String partId = "52349";
//		
//		IProjectHelper helper = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated");
//		
//		// Ensure that the participant exists
//		helper.addParticipant(session, proj, partId);
//		
//		// Now do the delete...
//		boolean stat = helper.removeParticipant(session, proj, partId);
//		
//		// Check the status...
//		assertTrue("Remove failed...", stat);
//		
//		// ...and get the data back
//		ArrayList<Project> fb = helper.fromParticipantId(session, "" + partId);
//		assertEquals("Incorrect number of results", 0, fb.size());
//
//		UnitTestUtils.stop(this);
//	}


	/*
	 * addParticipants
	 */
//	public void testAddParticipants()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		NhnDatabaseUtils.setUnitTest(true);
//		
//		SessionUser session = new SessionUser();
//
//		Project proj = new Project();
//		String projId = "6";
//		proj.setId(projId);
//		ArrayList<String> partIds = new ArrayList<String>(); 
//		partIds.add("545562");
//		partIds.add("451034");
//
//		IProjectHelper helper = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated"); 
//		boolean status = helper.addParticipants(session, proj, partIds);
//		
//		assertTrue("Multi-insert failed...", status);
//
//		// Now get the data back
//		Project ret = helper.fromId(session, projId);
//		assertEquals("Incorrect number of results", 2, ret.getParticipantCollection().size());
//
//		UnitTestUtils.stop(this);
//	}


	/*
	 * removeParticipants
	 */
//	public void testRemoveParticipants()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		NhnDatabaseUtils.setUnitTest(true);
//		
//		SessionUser session = new SessionUser();
//
//		Project proj = new Project();
//		String projId = "6";
//		proj.setId(projId);
//		ArrayList<String> partIds = new ArrayList<String>(); 
//		partIds.add("545562");
//		partIds.add("451034");
//		ArrayList<Participant> participants = new ArrayList<Participant>();
//		for (String pId : partIds)
//		{
//			Participant p = new Participant();
//			p.setId(pId.toString());
//			participants.add(p);
//		}
//		
//		IProjectHelper helper = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated");
//		
//		// Ensure that the participants exist
//		boolean status = helper.addParticipants(session, proj, partIds);
//		assertTrue("Failure to add test intersections", status);
//
//		// Now do the delete...
//		status = helper.removeParticipants(session, proj, participants);
//		
//		assertTrue("Multi-insert failed...", status);
//
//		// Now get the data back
//		Project ret = helper.fromId(session, projId);
//		int cnt = -1;
//		cnt = (ret.getParticipantCollection() == null ? 0 : ret.getParticipantCollection().size());
//		assertEquals("Incorrect number of results", 0, cnt);
//
//		UnitTestUtils.stop(this);
//	}


	/*
	 * find - for Client
	 */
//	public void testFindForClient()
//	throws Exception
//	{
//		UnitTestUtils.start(this);
//		NhnDatabaseUtils.setUnitTest(true);
//		
//		SessionUser session = new SessionUser();
//		String target;
//		String client;
//		ArrayList<Project> ret;
//	
//		IProjectHelper helper = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated");
//	
//		// Buncha tests for client 751
//		client = "751";
//		
//		target = "NHN";	// Should find 4 rows
//		ret = helper.findForClientId(session, client, target);
//		assertEquals("T1:  Failed to find 4 'NHN' entries...", 4, ret.size());
//		printNames(client, target, ret);
//	
//		target = "Ken";	// Should find no rows
//		ret = helper.findForClientId(session, client, target);
//		assertEquals("T2:  Found data where it shouldn't...", 0, ret.size());
//		printNames(client, target, ret);
//	
//		target = "with";	// Should find 4 rows
//		ret = helper.findForClientId(session, client, target);
//		assertEquals("T3:  Failed to find 1 'with' entry...", 4, ret.size());
//		printNames(client, target, ret);
//		
//		// Test data common on two clients
//		target = "project";
//		ret = helper.findForClientId(session, client, target);
//		assertEquals("T4:  Failed to find multiple 'project' entries...", 17, ret.size());
//		printNames(client, target, ret);
//		
//		// Now do the test on a different client
//		client = "504";
//		ret = helper.findForClientId(session, client, target);
//		assertEquals("T5:  Failed to find 1 'project' entry...", 1, ret.size());
//		printNames(client, target, ret);
//
//		UnitTestUtils.stop(this);
//	}
//	
//	/*  This goes with testFindForClient and testFindForAdmin */
//	private void printNames(String cId, String targ, ArrayList<Project> ary)
//	{
//		System.out.println("For ID=" + cId + ", searchString='" + targ + "' (" + ary.size() + "):");
//		if (ary.size() == 0)
//		{
//			System.out.println("   No data returned");
//	
//		}
//		else
//		{
//			for (Project pp : ary)
//			{
//				System.out.println("   " + pp.getName() + " -- " + pp.getAdminId());
//			}
//		}
//	}
//
//	/*
//	 * find - for Admin
//	 */
//	public void testFindForAdmin()
//	throws Exception
//	{
//		UnitTestUtils.start(this);
//		NhnDatabaseUtils.setUnitTest(true);
//		
//		SessionUser session = new SessionUser();
//		String target;
//		String admin;
//		ArrayList<Project> ret;
//	
//		IProjectHelper helper = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated");
//	
//		// Same tests, as for client, but looking for adminId
//		admin = "55422";
//		
//		target = "NHN";	// Should find 0 rows (all admin fields for these rows are null)
//		ret = helper.findForAdminId(session, admin, target);
//		assertEquals("T1:  Found 'NHN' entries (shouldn't)...", 0, ret.size());
//		printNames(admin, target, ret);
//	
//		target = "Ken";	// Should find no rows
//		ret = helper.findForAdminId(session, admin, target);
//		assertEquals("T2:  Found data where it shouldn't...", 0, ret.size());
//		printNames(admin, target, ret);
//	
//		target = "with";	// Should find 3 rows
//		ret = helper.findForAdminId(session, admin, target);
//		assertEquals("T3:  Failed to find proper 'with' entries...", 3, ret.size());
//		printNames(admin, target, ret);
//		
//		// Test data common on two clients
//		target = "project";
//		ret = helper.findForAdminId(session, admin, target);
//		assertEquals("T4:  Failed to find proper 'project' entries...", 5, ret.size());
//		printNames(admin, target, ret);
//		
//		// Now do the test on a different client
//		admin = "60080";
//		ret = helper.findForAdminId(session, admin, target);
//		assertEquals("T5:  Failed to find 1 'project' entry...", 1, ret.size());
//		printNames(admin, target, ret);
//
//		UnitTestUtils.stop(this);
//	}


	/*
	 * createProject
	 */
//	public void testCreateProject()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		NhnDatabaseUtils.setUnitTest(true);
//		
//		SessionUser session = new SessionUser();
//		
//		IProjectHelper helper = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated");
//		
//		Project proj = new Project();
//		proj.setName("Testing project");
//		proj.setClient(new Client("751"));
//		proj.setProjectCode("Project Code for UT test");
//		proj.setProjectTypeId(2);
//		proj.setAdminId("572441");
//		// Add content (ids only)
//		proj.setContentCollection(new HashSet<String>());
//		proj.getContentCollection().add("39");
//		proj.getContentCollection().add("40");
//		proj.getContentCollection().add("50");
//		// Add metadata
//		proj.setMetadata(new HashMap<String, String>());
//		proj.getMetadata().put("metaT1", "Metadata Test 1");
//		proj.getMetadata().put("metaT2", "Metadata Test 2");
//		// Do it
//		Project ret = helper.createProject(session, proj);
//
//		assertNotNull("T1: Create project failed (no Project)...", ret);
//		assertNotNull("T1: Create project failed (no ID)...", ret.getId());
//		System.out.println("Created project ID " + ret.getId());
//
//		// Now try to get it back
//		Project newProj = helper.fromId(session, ret.getId());
//		assertNotNull(newProj);
//		assertEquals(ret.getId(), newProj.getId());
//		assertEquals(ret.getMetadata().get("metaT1"), newProj.getMetadata().get("metaT1"));
//		
//		System.out.println(newProj.toString());
//	
//		UnitTestUtils.stop(this);
//	}


	/*
	 * testUpdateProject
	 */
//	public void testUpdateProject()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		NhnDatabaseUtils.setUnitTest(true);
//		
//		SessionUser session = new SessionUser();
//		Project retProj = null;
//		boolean stat = false;
//
//		IProjectHelper helper = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated");
//
//		// Get an existing project
//		retProj = helper.fromId(session, "363");
//		// and "update" it
//		stat = helper.updateProject(session, retProj);
//		assertTrue("T0...", stat);
//
//		// Do an update (no change to project_content or project_metadata)
//		retProj = helper.fromId(session, "363");
//		// change the data
//		retProj.setName(retProj.getName().equals("Testing project") ? "Testing project!" : "Testing project");
//		retProj.setAdminId(retProj.getAdminId().equals("572441") ? "95724419" : "572441");
//		retProj.setProjectTypeId(retProj.getProjectTypeId() == 1 ? 2 : 1);
//		retProj.setProjectCode(retProj.getProjectCode().equals("Project Code for UT test") ? "Project Code for UT test - 1" : "Project Code for UT test");
//		retProj.setActive(retProj.getActive() == 1 ? 0 : 1);	// flip the active flag
//		// and update it
//		//System.out.println("T1...");
//		stat = helper.updateProject(session, retProj);
//		assertTrue("T2...", stat);
//		
//		System.out.println(retProj.toString());
////
////		// Do a metadata update (no change to core data or project_content)
////		retProj = helper.fromId(session, "48");
////		// 'rowguid' is not true metadata.  If there is only one piece of metadata, break here
////		assertTrue("T3-0...Cannot continue test with empty metadata... ", retProj.getMetadata().size() != 1);	// only has the rowguid
////		// change the data
////		String str = retProj.getMetadata().get("metaT1");
////		retProj.getMetadata().put("metaT1", (str.equals("Metadata Test 1") ? "Metadata Test 1-1":"Metadata Test 1"));
////		retProj.getMetadata().put("metaT3", "Metadata Test 3");
////		// and update it
////		//System.out.println("T1...");
////		stat = helper.updateProject(session, retProj);
////		assertTrue("T3...", stat);
////
////		// Test the content changes (no change to core data or project_content)
////		retProj = helper.fromId(session, "48");
////		int cnt = retProj.getContentCollection().size();
////		retProj.getContentCollection().remove("111");
////		retProj.getContentCollection().add("9999");
////		stat = helper.updateProject(session, retProj);
////		assertTrue("T4-1...", stat);
////		retProj = helper.fromId(session, retProj.getId());
////		assertEquals("T4-2...", cnt + 1, retProj.getContentCollection().size());
//	
//		UnitTestUtils.stop(this);
//	}


	/*
	 * testUpdateCoreProject
	 */
//	public void testUpdateCoreProject()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		NhnDatabaseUtils.setUnitTest(true);
//		
//		SessionUser session = new SessionUser();
//		Project retProj = null;
//		boolean stat = false;
//
//		IProjectHelper helper = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated");
//
//		// Get a non-existing project
//		retProj = helper.fromId(session, "99999");
//		// and "update" it
//		stat = helper.updateCoreProject(session, retProj);
//		assertFalse("T0...", stat);
//
//		// Do an update (should be no change to project_content or project_metadata)
//		retProj = helper.fromId(session, "48");
//		// change the data
//		retProj.setName(retProj.getName().equals("Testing project") ? "Testing project!" : "Testing project");
//		retProj.setAdminId(retProj.getAdminId().equals("572441") ? "95724419" : "572441");
//		retProj.setProjectCode(retProj.getProjectType().equals("TLT") ? "TLT-1" : "TLT");
//		retProj.setProjectCode(retProj.getProjectCode().equals("Project Code for UT test") ? "Project Code for UT test - 1" : "Project Code for UT test");
//		retProj.setActive(retProj.getActive() == 1 ? 0 : 1);	// flip the active flag
//		int metaCount = retProj.getMetadata().size();
//		int conCount = retProj.getContentCollection().size();
//		retProj.getMetadata().put("bogus", "Should not appear!");
//		retProj.getContentCollection().add("1234567");
//		// and update it
//		stat = helper.updateCoreProject(session, retProj);
//		assertTrue("T1...", stat);
//		// Check the meta and content
//		retProj = helper.fromId(session, "48");
//		assertEquals("T1-2...", metaCount, retProj.getMetadata().size());
//		assertEquals("T1-2...", conCount, retProj.getContentCollection().size());
//		
//		UnitTestUtils.stop(this);
//	}


// NO LONGER OPERATIVE... Project metadata functionality removed from the tables
//	/*
//	 * testUpdateMetadataItems
//	 */
//	public void testUpdateMetadataItems()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		NhnDatabaseUtils.setUnitTest(true);
//		
//		SessionUser session = new SessionUser();
//		Project retProj = null;
//		boolean stat = false;
//		String projId = "48";
//
//		IProjectHelper helper = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated");
//
//		// Do a metadata update (no change to core data or project_content)
//		retProj = helper.fromId(session, projId);
//		// 'rowguid' is not true metadata.  If there is only one piece of metadata, break here
//		assertTrue("T1-0...Cannot continue test with empty metadata... ", retProj.getMetadata().size() != 1);	// only has the rowguid
//		// change the data
//		String str = retProj.getMetadata().get("metaT1");
//		retProj.getMetadata().put("metaT1", (str.equals("Metadata Test 1") ? "Metadata Test 1-1":"Metadata Test 1"));
//		retProj.getMetadata().put("metaT3", "Metadata Test 3");
//		retProj.getMetadata().put("bogus", "Delete Me!");
//		// and update it
//		stat = helper.updateMetadataItems(session, projId, retProj.getMetadata());
//		assertTrue("T3...", stat);
//		System.out.println("Check the metadata for project " + projId + " in the DB");
//		
//		UnitTestUtils.stop(this);
//	}


	/*
	 * testUpdateContentItems
	 */
//	public void testUpdateContentItems()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		NhnDatabaseUtils.setUnitTest(true);
//		
//		SessionUser session = new SessionUser();
//		Project retProj = null;
//		boolean stat = false;
//		String projId = "48";
//
//		IProjectHelper helper = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated");
//
//		// Do a content update (no change to core data or project_metadata)
//		retProj = helper.fromId(session, projId);
//		int cnt = retProj.getContentCollection().size();
//		retProj.getContentCollection().remove("111");
//		retProj.getContentCollection().add("9999");
//		stat = helper.updateContentItems(session, projId, retProj.getContentCollection());
//		assertTrue("T1...", stat);
//		retProj = helper.fromId(session, retProj.getId());
//		assertEquals("T2...", cnt + 1, retProj.getContentCollection().size());	// This will fail if you don't delete the bogus row (9999)
//		System.out.println("Check the metadata for project " + projId + " in the DB");
//		
//		UnitTestUtils.stop(this);
//	}


		/*
		 * testRemoveContentItems
		 */
//		public void testRemoveContentItems()
//			throws Exception
//		{
//			UnitTestUtils.start(this);
//			NhnDatabaseUtils.setUnitTest(true);
//			
//			SessionUser session = new SessionUser();
//			Project tstProj = null;
//			boolean stat = false;
//			String projId = "48";
//			int b4Cnt = 0;
//			
//			HashSet<String> testData = new HashSet<String>();
//			testData.add("9876");
//			testData.add("9875");
//
//			IProjectHelper helper = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated");
//
//			// Set up data
//			tstProj = helper.fromId(session, projId);
//			b4Cnt = tstProj.getContentCollection().size();
//			tstProj.getContentCollection().addAll(testData);
//			stat = helper.updateContentItems(session, projId, tstProj.getContentCollection());
//			assertTrue("T0-1: Test data add failed... cannot continue.", stat);
//			tstProj = helper.fromId(session, projId);
//			assertEquals("T0-2: Possible add failure; cannot continue...", b4Cnt+2, tstProj.getContentCollection().size());
//			
//			// do the delete -- Delete the ones we just added
//			stat = 	helper.removeContentItems(session, projId, testData);
//			assertTrue("T1-1: Test data delete failed...", stat);
//			tstProj = helper.fromId(session, projId);
//			assertEquals("T1-2: Item count incorrect after delete...", b4Cnt, tstProj.getContentCollection().size());
//			
//			UnitTestUtils.stop(this);
//		}


	/*
	 * testRemoveAllContentItems
	 */
//	public void testRemoveAllContentItems()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		NhnDatabaseUtils.setUnitTest(true);
//		
//		SessionUser session = new SessionUser();
//		Project tstProj = null;
//		boolean stat = false;
//		String projId = "48";
//		HashSet<String> oldData = new HashSet<String>();
//
//		IProjectHelper helper = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated");
//
//		// Set up data
//		tstProj = helper.fromId(session, projId);
//		assertTrue("T0: We need content data in order to delete it.  Test cannot continue.", tstProj.getContentCollection().size() > 0);
//		oldData = tstProj.getContentCollection();
//		
//		// do the delete
//		stat = 	helper.removeAllContentItems(session, projId);
//		assertTrue("T1-1: Test data delete failed...", stat);
//		tstProj = helper.fromId(session, projId);
//		assertEquals("T1-2: Item count incorrect after delete...", 0, tstProj.getContentCollection().size());
//
//		// Restore the original data
//		stat = helper.updateContentItems(session, projId, oldData);
//		assertTrue("T2-1: Test data insert failed...", stat);
//		tstProj = helper.fromId(session, projId);
//		assertEquals("T2-2: Item count incorrect after insert...", oldData.size(), tstProj.getContentCollection().size());
//		
//		UnitTestUtils.stop(this);
//	}


// NO LONGER OPERATIVE... Project metadata functionality removed from the tables
//	/*
//	 * testRemoveMetadataItems_set
//	 */
//	public void testRemoveMetadataItems_set()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		NhnDatabaseUtils.setUnitTest(true);
//		
//		SessionUser session = new SessionUser();
//		Project tstProj = null;
//		boolean stat = false;
//		String projId = "48";
//		HashMap<String, String> oldData = new HashMap<String, String>();
//		HashMap<String, String> newData = new HashMap<String, String>();
//		HashSet<String> metaSet = new HashSet<String>();
//
//		IProjectHelper helper = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated");
//
//		// Get existing data
//		tstProj = helper.fromId(session, projId);
//		assertTrue("T0: We need metadata in order to delete it.  Test cannot continue.", tstProj.getContentCollection().size() > 1);
//		oldData = tstProj.getMetadata();
//		
//		// Add something new
//		newData.put("Bogus 1", "Bogus Data 1");
//		newData.put("Bogus 2", "Bogus Data 2");
//		stat = helper.updateMetadataItems(session, projId, newData);
//		assertTrue("T0-1...", stat);
//		tstProj = helper.fromId(session, projId);
//		assertEquals("T0-2...", oldData.size() + newData.size(), tstProj.getMetadata().size());
//		
//		// Delete the something new
//		metaSet.addAll(newData.keySet());
//		stat = helper.removeMetadataItems(session, projId, metaSet);
//		assertTrue("T1-1...", stat);
//		tstProj = helper.fromId(session, projId);
//		assertEquals("T1-2...", oldData.size(), tstProj.getMetadata().size());
//		
//		UnitTestUtils.stop(this);
//	}


// NO LONGER OPERATIVE... Project metadata functionality removed from the tables
//	/*
//	 * testRemoveMetadataItems_map
//	 */
//	public void testRemoveMetadataItems_map()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		NhnDatabaseUtils.setUnitTest(true);
//		
//		SessionUser session = new SessionUser();
//		Project tstProj = null;
//		boolean stat = false;
//		String projId = "48";
//		HashMap<String, String> oldData = new HashMap<String, String>();
//		HashMap<String, String> newData = new HashMap<String, String>();
//
//		IProjectHelper helper = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated");
//
//		// Get existing data
//		tstProj = helper.fromId(session, projId);
//		assertTrue("T0: We need metadata in order to delete it.  Test cannot continue.", tstProj.getContentCollection().size() > 1);
//		oldData = tstProj.getMetadata();
//		
//		// Add something new
//		newData.put("Bogus 3", "Bogus Data 3");
//		newData.put("Bogus 4", "Bogus Data 4");
//		stat = helper.updateMetadataItems(session, projId, newData);
//		assertTrue("T0-1...", stat);
//		tstProj = helper.fromId(session, projId);
//		assertEquals("T0-2...", oldData.size() + newData.size(), tstProj.getMetadata().size());
//		
//		// Delete the something new
//		stat = helper.removeMetadataItems(session, projId, newData);
//		assertTrue("T1-1...", stat);
//		tstProj = helper.fromId(session, projId);
//		assertEquals("T1-2...", oldData.size(), tstProj.getMetadata().size());
//		
//		UnitTestUtils.stop(this);
//	}


// NO LONGER OPERATIVE... Project metadata functionality removed from the tables
//	/*
//	 * testRemoveAllMetadataItems
//	 */
//	public void testRemoveAllMetadataItems()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		NhnDatabaseUtils.setUnitTest(true);
//		
//		SessionUser session = new SessionUser();
//		Project tstProj = null;
//		boolean stat = false;
//		String projId = "48";
//		HashMap<String, String> oldData = new HashMap<String, String>();
//
//		IProjectHelper helper = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated");
//
//		// Get existing data
//		tstProj = helper.fromId(session, projId);
//		assertTrue("T0: We need metadata in order to delete it.  Test cannot continue.", tstProj.getContentCollection().size() > 1);
//		oldData = tstProj.getMetadata();
//		
//		// Delete all of the metadata
//		stat = helper.removeAllMetadataItems(session, projId);
//		assertTrue("T1-1...", stat);
//		tstProj = helper.fromId(session, projId);
//		assertEquals("T1-2...", 1, tstProj.getMetadata().size());	// rowguid is saved in project but carried in metadata
//		
//		// Resotre the old metadata
//		stat = helper.updateMetadataItems(session, projId, oldData);
//		assertTrue("T2-1...", stat);
//		tstProj = helper.fromId(session, projId);
//		assertEquals("T2-2...", oldData.size(), tstProj.getMetadata().size());
//		
//		UnitTestUtils.stop(this);
//	}

}
