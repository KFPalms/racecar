package com.pdi.data.helpers.interfaces;

import java.util.ArrayList;

import com.pdi.data.dto.Client;
import com.pdi.data.dto.SessionUser;

public interface IClientHelper {
	public Client fromId(SessionUser session, String clientId);
	public ArrayList<Client> all(SessionUser session);
	public Client getClientByParticipantId(String participantId); 
}
