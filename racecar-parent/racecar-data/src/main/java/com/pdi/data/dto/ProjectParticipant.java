/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.data.dto;

public class ProjectParticipant extends CoreDataObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8727260558800939956L;
	private String id;
	private Participant participant;
	private Project project;
	
	// Constructors
	public ProjectParticipant()
	{
		// Does nothing at this time
	}
	
	public ProjectParticipant(String idIn, String projIdIn, String partIdIn)
	{
		id = idIn;
		participant = new Participant(partIdIn);
		project = new Project(projIdIn);
	}
	
	public String toString()
	{
		String ret = "ProjectParticipant:";
		ret += "  id=" + id;
		ret += ", projId=" + project.getId();
		ret += ", partId=" + participant.getId();
		
		return ret;
	}
	
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}
	public Participant getParticipant() {
		return participant;
	}
	public void setProject(Project project) {
		this.project = project;
	}
	public Project getProject() {
		return project;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getId() {
		return id;
	}
}
