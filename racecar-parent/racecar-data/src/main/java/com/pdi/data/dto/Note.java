/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.dto;

/**
 * Note is a wrapper around the note data for
 * a given participant. 
 * 12-08-2010
 * Notes are created in the Test Data application,
 * associated with a Participant.  
 * Notes are accessible - read only - in the 
 * DRI.
 * 
 * @author      MB Panichi
 */
public class Note extends CoreDataObject
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	/**
	 * 
	 */
	private static final long serialVersionUID = 6739158089802572552L;
	//
	// Instance data.
	//
	private long noteId = 0;
    private String noteText = "";
    private String noteDate = "";

	//
	// Constructors.
	//
	public Note()
	{
		// Does nothing presently
	}
	public Note(long id, String text, String date)
	{
		noteId = id;
	    noteText = text;
	    noteDate = date;
	}
	
	public String toString()
	{
		return "" + noteId + "|" + noteText +  "|" + noteDate;
	}

	//
	// Instance methods.
	//

	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public long getNoteId()
	{
		return noteId;
	}

	public void setNoteId(long value)
	{
		noteId = value;
	}

	/*****************************************************************************************/
	public String getNoteText()
	{
		return noteText;
	}

	public void setNoteText(String value)
	{
		noteText = value;
	}

	/*****************************************************************************************/
	public String getNoteDate()
	{
		return noteDate;
	}

	public void setNoteDate(String value)
	{
		noteDate = value;
	}

}

