/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v2.helpers;

import com.pdi.data.v2.dto.SessionUserDTO;
import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.data.v2.util.V2WebserviceUtils;

public class UserDataHelper
{
	/**
	 * Log the user into the system, return the session information
	 * @param username
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public  SessionUserDTO getUser(String username, String password)
		throws Exception
	{
		V2WebserviceUtils v2web = new V2WebserviceUtils();
		V2DatabaseUtils.testConnection();
		return v2web.registerUser(username, password);
	}
}
