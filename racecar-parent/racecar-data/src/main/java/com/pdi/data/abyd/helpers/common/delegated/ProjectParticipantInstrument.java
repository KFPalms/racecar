package com.pdi.data.abyd.helpers.common.delegated;

import java.util.ArrayList;

import com.pdi.data.dto.Participant;
import com.pdi.data.dto.Project;
import com.pdi.data.dto.ResponseGroup;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.interfaces.IProjectParticipantInstrumentHelper;

public class ProjectParticipantInstrument   implements IProjectParticipantInstrumentHelper {

	@Override
	public void addUpdate(SessionUser session, int mode,
			com.pdi.data.dto.ProjectParticipantInstrument epi,
			boolean updateFromTestDataApp) {

		
	}

	@Override
	public void addUpdate(SessionUser session, int mode,
			ArrayList<com.pdi.data.dto.ProjectParticipantInstrument> epis,
			boolean updateFromTestDataApp) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ArrayList<com.pdi.data.dto.ProjectParticipantInstrument> fromProjectIdParticipantId(
			SessionUser session, int mode, String engagementId,
			String participantId) {
		ArrayList<com.pdi.data.dto.ProjectParticipantInstrument> ppis = new ArrayList<com.pdi.data.dto.ProjectParticipantInstrument>();
		try {
//			IProjectParticipantInstrumentHelper ippnhn = HelperDelegate.getProjectParticipantInstrumentHelper("com.pdi.data.nhn.helpers.delegated");
//			IProjectParticipantInstrumentHelper ippv2 = HelperDelegate.getProjectParticipantInstrumentHelper("com.pdi.data.v2.helpers.delegated");
			//magic goes here
			
		} catch (Exception e) {
			
		}
		return ppis;
	}

	@Override
	public ArrayList<com.pdi.data.dto.ProjectParticipantInstrument> fromProjectId(
			SessionUser session, int mode, String engagementId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public com.pdi.data.dto.ProjectParticipantInstrument fromProjectIdParticipantIdInstrumentId(
			SessionUser session, int mode, String engagementId,
			String participantId, String instrumentId, String callingProjectId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<com.pdi.data.dto.ProjectParticipantInstrument> fromParticipantId(
			SessionUser session, int mode, String participantId, String callingProjectId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<com.pdi.data.dto.ProjectParticipantInstrument> fromParticipantIdInstrumentId(
			SessionUser session, String participantId, String instrumentId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<com.pdi.data.dto.ProjectParticipantInstrument> fromParticipantListInstrumentId(
			SessionUser session, ArrayList<Participant> participants,
			String instrumentId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<com.pdi.data.dto.ProjectParticipantInstrument> fromLastParticipantId(
			SessionUser session, int mode, String participantId, String callingProjectId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public com.pdi.data.dto.ProjectParticipantInstrument fromLastParticipantIdInstrumentId(
			SessionUser session, String participantId, String instrumentId) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * Used in Test Data, for AbyD type projects.
	 * For AbyD, need to get all instruments available, even if there
	 * is no participant data for that instrument.
	 * This method gets the Instrument, and also gets the ScoreGroup
	 * data for the unfinished PPI passed in, and returns the "completed"
	 * PPI with no data, but all scales/norms for that instrument.
	 * 
	 * @param  ProjectParticipantInstrument ppi
	 * @return ProjectParticipantInstrument	  
	 */
	public com.pdi.data.dto.ProjectParticipantInstrument getPPIWithoutScoredData(SessionUser session, Project proj, Participant part, String instId) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * Return all instruments.
	 * Return the most recent instrument
	 * if there is a duplicate instrument in the list.
	 * 
	 * Note that the "last" instrument may be from a
	 * different project then the other instruments.  In
	 * fact, if you have various instruments from multiple
	 * projects, the latest may all be from different 
	 * projects.  You really just don't know until
	 * you get them.
	 * 
	 * NHN 1738: new use case, with participant in two
	 * different AbyD projects, so different sets of norms
	 * 
	 * * usecase: I the most recent GPI for this participant, but he
	 * is in two different projects, with different norms.  I want the
	 * norms for the current project.
	 * @param SessionUser session
	 * @param int mode
	 * @param String particpantId
	 * @param String callingProjectId
	 * @return ArrayList<ProjectParticipantInstrument> ppiList
	 */
	public ArrayList<com.pdi.data.dto.ProjectParticipantInstrument> fromLastParticipantIdIncludeProjectIdForNorms
							(SessionUser session,
							 int mode, 
							 String participantId,
							 String callingProjectId) {
		
		return null;
	}
	
	/**
	 * Used in Test Data, for the FinEx only.
	 * NHN-1624 adds the ability to input FinEx response
	 * data to the TestData App.
	 * 
	 * This method already existed in the nhn helper, 
	 * but wasn't accessible as it was private.
	 *  
	 * @param  String participantId
	 * @return String instrumentId  
	 */
	public ResponseGroup getResponseGroupByParticipantId(String participantId, String instrumentId){
		return null;
	}

	
}
