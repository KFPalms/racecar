/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.dto;

import org.w3c.dom.Node;

import com.pdi.data.util.RequestStatus;

/**
 * DTO for session user information
 * 
 * NOTE:  This information is used only on the server side.
 *        There is no corresponding object on the client side.
 * 
 *
 * @author		Gavin Myers
 */
public class SessionUserDTO extends UserDTO
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _password; 
	private String _sessionId;
	
	//
	// Constructors.
	//
	
	/**
	 * "Standard" no param constructor - no additional functionality
	 */
	public SessionUserDTO()
	{
		// Does nothing presently
		super();
	}
	
	/**
	 * Status only constructor
	 */
	public SessionUserDTO(RequestStatus stat)
	{
		// Does nothing presently
		super(stat);
	}
	
	/**
	 * Constructs the object from an XML Node object
	 * 
	 * @param node
	 */
	public SessionUserDTO(Node node)
	{
		super(node);
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
	
	/*****************************************************************************************/
	public void setPassword(String value)
	{
		_password = value;
	}

	public String getPassword()
	{
		return _password;
	}

	public void setSessionId(String _sessionId) {
		this._sessionId = _sessionId;
	}

	public String getSessionId() {
		return _sessionId;
	}
}
