/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.dto;

/**
 * ProjectParticipantInstrument is a basic data class that contains
 * related engagement, participant, and instrument data.
 * 
 * @author      Gavin Myers
 * @author      MB Panichi
  */
public class ProjectParticipantReport extends CoreDataObject
{
	
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 597758830872948558L;
	private Project engagement;
	private Participant participant;
//	private Report report;
	private String reportCode;
	private String reportType;
	
	/**
	 * No-parameter constructor
	 */
	public ProjectParticipantReport()
	{
		//  Does nothing presently
	}
	
	
	public String toString()
	{
		String ret = "ProjectParticipantReport:";
		ret += "\n  " + engagement.toString();
		ret += "\n  " + participant.toString();
//		ret += "\n  " + report.toString();
		
		return ret;
	}
	
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public void setProject(Project value) {
		this.engagement = value;
	}
	public Project getProject() {
		return engagement;
	}
	
	/*****************************************************************************************/
	public void setParticipant(Participant value) {
		this.participant = value;
	}
	public Participant getParticipant() {
		return participant;
	}


	public void setReportCode(String reportCode) {
		this.reportCode = reportCode;
	}


	public String getReportCode() {
		return reportCode;
	}


	public void setReportType(String reportType) {
		this.reportType = reportType;
	}


	public String getReportType() {
		return reportType;
	}
	
//	/*****************************************************************************************/
//	public void setReport(Report value) {
//		this.report = value;
//	}
//	public Report getReport() {
//		return report;
//	}
	
}
