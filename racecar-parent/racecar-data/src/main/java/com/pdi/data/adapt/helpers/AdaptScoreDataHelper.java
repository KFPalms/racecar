/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.adapt.helpers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;
//import java.util.Iterator;
//import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.pdi.data.adapt.util.AdaptWebserviceUtils;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;
import com.pdi.reporting.request.ReportingRequest;
import com.pdi.reporting.response.ReportingResponse;
import com.pdi.xml.XMLUtils;

/**
 * Helper class for getting data from ADAPT
 */
public class AdaptScoreDataHelper
{
	//
	// Static data.
	//
	
	// Converts a module/task id to a ReportingConstants value
	private static HashMap<String, String> modIdToRptConst = new HashMap<String, String>();
	static
	{
		modIdToRptConst.put("GPI", ReportingConstants.RC_GPI);
		modIdToRptConst.put("LEI", ReportingConstants.RC_LEI);
		modIdToRptConst.put("FINEX", ReportingConstants.RC_FIN_EX);
		modIdToRptConst.put("CHQ", ReportingConstants.RC_CHQ);
	};
	
	// The ranking text list.  Theoretically we should pick this up from ADAPT, but since
	// this is an "interum solution" we put them in here.
	private static ArrayList<String> Q71_RATE_TEXT = new ArrayList<String>();
	static
	{
		Q71_RATE_TEXT.add("Advancement opportunities (promotions)");
		Q71_RATE_TEXT.add("Fast-paced environment with lots of change");
		Q71_RATE_TEXT.add("Friendly workplace with good relations with coworkers, vendors, and/or clients");
		Q71_RATE_TEXT.add("Sense of personal accomplishment");
		Q71_RATE_TEXT.add("Hard-driving, performance-based culture");
		Q71_RATE_TEXT.add("Autonomy in carrying out my responsibilities");
		Q71_RATE_TEXT.add("Belief in the mission of the organization");
		Q71_RATE_TEXT.add("Expert or authority status");
		Q71_RATE_TEXT.add("Fair and consistent managers");
		Q71_RATE_TEXT.add("Influence on the direction of the organization");
		Q71_RATE_TEXT.add("Monetary compensation");
		Q71_RATE_TEXT.add("Opportunity to be creative");
		Q71_RATE_TEXT.add("Responsibility for the performance of others and the results of the unit");
		Q71_RATE_TEXT.add("Stimulating, challenging work");
		Q71_RATE_TEXT.add("Training or development opportunities");
		Q71_RATE_TEXT.add("Variety in job duties");
		Q71_RATE_TEXT.add("Visibility and recognition");
		Q71_RATE_TEXT.add("Working in a stable job with little change");
		Q71_RATE_TEXT.add("Work-life balance");
	};

	
	// 
	//
	// Static methods.
	//
	
	/*
	 * Gets data from ADAPT, rolls up the results (generates raw scale scores), and
	 * return the data so it can eventually be placed into the V2 results table.
	 *
	 * The overall process will be as follows:
	 * 1. Set up the request (passing ADAPT engagement and participant id)
	 * 2. Make the request (returns a Document object)
	 * 3. Set up the IndividualReport object with base data and raw (response) data from doc
	 * 4. Add picklist data (if present)
	 * 5. Score the data
	 * 6. Return the data
	 */
	/**
	 *   pullAdaptData gets ADAPT data via Cast Iron and puts it into an
	 *   Individual Report object.  The boolean is present because there
	 *   is a case in Manual Entry where we are only entering and saving
	 *   the participant's adapt userid and engagementId, and want to
	 *   forego the task data.
	 *   
	 *   @param long engagement - the adapt engagementId
	 *   @param long person - the adapt user id
	 *   @param boolean idOnly - if we are only dealing with user credentials
	 *   @return IndividualReport - the IndividualReport Object that contains
	 *   the adapt data for this participant.
	 *   @throws Exception
	 */
	//public static IndividualReport fetchAdaptData(long engagement, long person, boolean idOnly)
	public static IndividualReport pullAdaptData(long engagement, long person, boolean idOnly)
		throws Exception
	{
		IndividualReport ir = new IndividualReport();
		
		// Get the raw response data from ADAPT (via Cast Iron)
		AdaptWebserviceUtils util = new AdaptWebserviceUtils();
		Document doc = util.requestAdaptData(engagement, person);
		System.out.println("RESPONSE!!!!");
		System.out.println(XMLUtils.nodeToString(doc.getFirstChild()));
		// Process the name data
		// In adapt, first and last name are required, so
		// we should only ever hit null once, but on the off
		// chance that something else is going on, catching it
		// in both places..
		
		
		String fName = "N/A";
		String lName = "N/A";
		Node node;
		
		// Check to see if the person is in the engagement
		NodeList pNode=  XMLUtils.getElements(doc, "//PersonNotInProject");
		if (pNode != null  && pNode.getLength() > 0)
		{
			// No data
			return null;
		}
		
		pNode=  XMLUtils.getElements(doc, "//PersonFirst");
		if (pNode != null  && pNode.getLength() > 0)
		{
			node = pNode.item(0);
			fName = node.getTextContent();
		}else{
			return null;
		}
		pNode =  XMLUtils.getElements(doc, "//PersonLast");
		if (pNode != null  && pNode.getLength() > 0)
		{
			node = pNode.item(0);
			lName = node.getTextContent();
		}else{
			return null;
		}
		//System.out.println("  fName=" + fName + ", lName=" + lName);
		ir.getDisplayData().put("ADAPT_FIRST_NAME", fName);
		ir.getDisplayData().put("ADAPT_LAST_NAME", lName);
		
		if(idOnly){
			// we're just validating the ADAPT user, 
			// so don't bother with the task data.
			return ir;
			
		}else{
			// deal with the Task stuff... 
						
			// Process the picklist choice codes if they exist
			HashMap<String, String> pl = null;
			NodeList plNodes =  XMLUtils.getElements(doc, "//PickList");
			//System.out.println("PickList node count=" + plNodes.getLength());
			if (plNodes.getLength() > 0)
			{
				// Process the picklist info (Should only be one PickList node at this point (for CHQ)
				pl = processPickListData(ir, plNodes);
			}
	
			// Now put the response data into an IndividualReport object
			// Get the tasks
			NodeList taskNodes =  XMLUtils.getElements(doc, "//Task");
			//System.out.println("Task node count=" + taskNodes.getLength());
			if (taskNodes.getLength() < 1)
			{
				// nothing to do... just leave
				return null;
			}
			
			ArrayList<String> modCodes = processAnswerData(ir, taskNodes, pl);
	
			// Score the data for each report harvested, if needed
			//System.out.println("modCodes=" + modCodes.toString());
			ReportingResponse response = null;
			for (int i=0; i < modCodes.size(); i++)
			{

				ir.setReportCode(modCodes.get(i));
				ir.setReportType(ReportingConstants.REPORT_TYPE_SCORE);
			
				ReportingRequest request = new ReportingRequest();
				request.addReport(ir);
				response = request.generateReports();
			}
			IndividualReport resp = (IndividualReport)response.getReports().get(0);
			
			return resp;
		}
	}


	/*
	 * Processes the PickList nodes, saving the data in a HashMap
	 * to be returned when done.
	 * 
	 * The logic assumes that choice values are unique through the entire ADAPT system
	 */
	private static HashMap<String, String> processPickListData(IndividualReport ir, NodeList plNodes)
	{
		HashMap<String, String> ret = null;
		
		for (int i=0; i < plNodes.getLength(); i++)
		{
			Node plItem = plNodes.item(i);
			NodeList choiceNodes = XMLUtils.getElements(plItem, "Choice");
			if (choiceNodes.getLength() < 1)
			{
				// No choice data... Go to next Picklist (should only be 1 at this point)
				//System.out.println("    No data");
				continue;
			}
			
			// If here we have PickList choices... make the return Map
			if (ret == null)
			{
				ret = new HashMap<String, String>();
			}
			
			// Process the choices
			//System.out.println("Choice node count=" + choiceNodes.getLength());
			for (int j=0; j < choiceNodes.getLength(); j++)
			{
				// Put the data into the IndividualReport DisplayData object
				Node ch = choiceNodes.item(j);
				ret.put(XMLUtils.getAttributeValue(ch, "ID"), ch.getTextContent());
			}	// End of Choice loop (index = j)
		}	// End of PickList loop (index = i)
		
		//// Debug dump
		//System.out.println("Picklist data:");
		//for (Iterator<Map.Entry<String, String>> itr = ret.entrySet().iterator(); itr.hasNext(); )
		//{
		//	Map.Entry<String, String> me = itr.next();
		//	System.out.println("    " + me.getKey() + "=" + me.getValue());
		//}

		return ret;
	}


	/*
	 * Processes the Task nodes looking for answers.  If it finds them for a task,
	 * it processes them, sticking the raw response data into the RawData bucket
	 * of the appropriate ReportDetail object.
	 * 
	 * @param ir - IndividualReport object in which to store the report data
	 * @param taskNodes - A NodeList containing all nodes with the a name of 'Task'
	 * @param pl - A HashMap of PickList codes and associated text values
	 * @return A list of module identifiers of those modules with score data
	 */
	private static ArrayList<String> processAnswerData(IndividualReport ir,
													   NodeList taskNodes,
													   HashMap<String, String> pl)
	{
		
		ArrayList<String> modCodes = new ArrayList<String>();
		for (int i=0; i < taskNodes.getLength(); i++)
		{
			Node task = taskNodes.item(i);
			//System.out.println("Task - " + XMLUtils.getAttributeValue(task, "TASK_LABEL"));
			NodeList answerNodes = XMLUtils.getElements(task, "Answer");
			if (answerNodes.getLength() < 1)
			{
				// No answer data... Go to next task
				//System.out.println("answerNodes    No data");
				continue;
			}
			
			// Process the answers
			for (int j=0; j < answerNodes.getLength(); j++)
			{
				Node ans = answerNodes.item(j);
				System.out.println("    " + XMLUtils.getAttributeValue(ans, "QUESTION_ID") +
							", int=" + XMLUtils.getAttributeValue(ans, "INTERNAL_VALUE") +
							", resp=" + XMLUtils.getAttributeValue(ans, "RESPONSE") +
							", ext mod=" + XMLUtils.getAttributeValue(ans, "EXT_MODULE_ID") +
							", ext q=" + XMLUtils.getAttributeValue(ans, "EXT_QUESTION_CODE"));

				// Put the data into the appropriate ReportData object
				String rpKey = modIdToRptConst.get(XMLUtils.getAttributeValue(ans, "EXT_MODULE_ID"));
				if (rpKey == null)
				{
					// can't put anything into a module that we haven't mapped
					//System.out.println("Un-mapped module code - " + XMLUtils.getAttributeValue(ans, "EXT_MODULE_ID"));
					continue;
				}
				ReportData rd  = ir.getReportData().get(rpKey);
				if (rd == null)
				{
					rd = new ReportData();
					ir.getReportData().put(rpKey, rd);
					modCodes.add(rpKey);
				}
				
				String key = XMLUtils.getAttributeValue(ans, "EXT_QUESTION_CODE");
				
				String value;
				if (rpKey.equals(ReportingConstants.RC_CHQ))
				{
					value = XMLUtils.getAttributeValue(ans, "INTERNAL_VALUE");
				}
				else
				{
					value = XMLUtils.getAttributeValue(ans, "RESPONSE");
				}
				
				// do some additional processing if this is a CHQ
				if (rpKey.equals(ReportingConstants.RC_CHQ))
				{
					value = replaceChqChoiceWithText(value, XMLUtils.getAttributeValue(ans, "CONTROL_TYPE"), pl);
				}
					
				rd.getRawData().put(key, value);
			}	// End of answers loop (index = j)
		}
		
		return modCodes;
	}


	/*
	 * For those items that are choice controls, replace the choice code
	 * with the text value associated with that value.
	 * 
	 * @param val - the incoming value
	 * @param ctl - The ADAPT control type
	 * @param pl - A HashMap of PickList codes and associated text values
	 * @return The (potentially) updated value
	 */
	private static String replaceChqChoiceWithText(String val, String ctl, HashMap<String, String> pl)
	{
		// If the control is not there, there is nothing to do
		if (ctl == null || ctl.length() < 1)
			return val;
		
		// Check "normal" pick list replacements
		if (ctl.equals("drop down list")     ||
		    ctl.equals("if yes linkd hash")  ||
		    ctl.equals("radio button group") ||
		    ctl.equals("seq drop down list") ||
		    ctl.equals("leading question")   ||
		    ctl.equals("linked hash drpdwn")   ||
		    ctl.equals("if no seq list"))
		{
			String repl = pl.get(val);
			if (repl != null)
				val = repl;
		}
		else if (ctl.equals("check box"))
		{
			// From ADAPT, if there is nothing present then the box is not checked
			String str = "Box ";
			str += (val == null  || val.length() < 1) ? "is NOT" : "IS";
			str += " checked";

			val = str;
		}
		else if (ctl.equals("drop down list multiple") ||
				 ctl.equals("drpdwn mult alpha"))
		{
			// Multiple selection dropdowns need to be parsed and need to check for 999
			StringTokenizer tokenizer = new StringTokenizer(val, "|");
			int i = 0;
			String outStr = "";
			while (tokenizer.hasMoreTokens())
			{
				String curVal = tokenizer.nextToken();
				if (i > 0)
					outStr += "|";
				if (curVal.trim().equals("999"))
				{
					outStr += "Other";
				}
				else
				{
					String tmp = pl.get(curVal);
					outStr += tmp == null ? curVal : tmp;
				}
				i++;
			}
			val = outStr;
		}
		else if (ctl.equals("small box text area"))
		{
			// In the CHQ, this is the "rate from a list" question (Q71).  Parse
			// out the tokens and use the numerical position and the existence
			// of a value there to determine which of the strings to display.
			// Note that the list is hard-coded here and if the order changes
			// then the list has to change. 
			StringTokenizer tokenizer = new StringTokenizer(val, "|");
			int i = -1;
			String outStr = "";
			while (tokenizer.hasMoreTokens())
			{
				i++;
				String curVal = tokenizer.nextToken().trim();
				if (curVal == null  || curVal.length() < 1)
				{
					// Nothing to do here
					continue;
				}
				if (outStr.length() > 0)
					outStr += "|";
				outStr += curVal + " - " + Q71_RATE_TEXT.get(i);
			}
			val = outStr;
		}

		return val;
	}



	//
	// Instance data.
	//

	//
	// Constructors.
	//

	//
	// Instance methods.
	//

}
