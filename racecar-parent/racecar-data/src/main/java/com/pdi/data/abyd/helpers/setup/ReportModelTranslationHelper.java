/**
 * Copyright (c) 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.setup;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.pdi.data.abyd.dto.setup.ReportModelTranslationDTO;
import com.pdi.data.abyd.dto.setup.ReportModelTranslationItemDTO;
import com.pdi.data.util.language.DefaultLanguageHelper;

/**
 * ReportModelTranslationHelper contains the methods associated with
 * the Report Model text translation functionality in Setup2.
 *
 * @author		Ken Beukelman
 */
public class ReportModelTranslationHelper
{
	//
	//  Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private Connection con = null;
	
	//
	// Constructors.
	//
	/*
	 * Single parameter constructor.
	 * 
	 * @param connection - A database Connection object
	 * @throws Exception
	 */
	public ReportModelTranslationHelper(Connection connection)
		throws Exception
	{
		if (connection == null)
		{
			throw new Exception("Database Connection required for Report Model translation.");
		}
		this.con = connection;
	}
	
	//
	//  Instance methods.
	//

	/**
	 * getTranslationData
	 * Get the translation data for a particular DNA in a particular language.
	 *
	 * @param xlateDto - A minimally filled ReportModelTranslationDTO object.
	 * @throws Exception
	 */
	 public ReportModelTranslationDTO getTranslationData(ReportModelTranslationDTO xlateDto)
		throws Exception
	{
		 ReportModelTranslationDTO ret = new ReportModelTranslationDTO();
		 long dnaId = xlateDto.getDnaId();
		 long langId = xlateDto.getLanguageItem().getId();
		 long dfltLangId = DefaultLanguageHelper.fetchDefaultLanguageId();
		 
		 // Put the know data into the output object
		 ret.setDnaId(dnaId);
		 ret.setLanguageItem(xlateDto.getLanguageItem());
		 
		 // Get the DNA structure
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sql = new StringBuffer();
		sql.append("SELECT sf.sfId, ");
		sql.append("       sf.sequence, ");
		sql.append("       sf.defaultTextId AS sfDefaultTextId, ");
		sql.append("       txt1.text AS sfStaticEnglishText, ");
		sql.append("       txt11.text AS sfStaticLangText, ");
		sql.append("       sf.userTextId AS sfUserTextId, ");
		sql.append("       txt2.userText AS sfUserEnglishText, ");
		sql.append("       txt12.userText AS sfUserLangText, ");
		sql.append("       cmp.rmCompId, ");
		sql.append("       cmp.sequence, ");
		sql.append("       cmp.defaultTextId AS cmpDefaultTextId, ");
		sql.append("       txt3.text AS cmpStaticEnglishText, ");
		sql.append("       txt13.text AS cmpStaticLangText, ");
		sql.append("       cmp.userTextId AS cmpUserTextId, ");
		sql.append("       txt4.userText AS cmpUserEnglishText, ");
		sql.append("       txt14.userText AS cmpUserLangText ");
		sql.append("  FROM pdi_abd_rm_superfactor sf ");
		sql.append("    LEFT JOIN pdi_abd_rm_competency cmp on (cmp.sfId = sf.sfId) ");
		sql.append("    LEFT JOIN pdi_abd_text txt1 ON (txt1.textId = sf.defaultTextId AND txt1.languageId = " + dfltLangId + ") ");
		sql.append("    LEFT JOIN user_text txt2 ON (txt2.userTextId = sf.userTextId AND txt2.languageId = " + dfltLangId + ") ");
		sql.append("    LEFT JOIN pdi_abd_text txt3 ON (txt3.textId = cmp.defaultTextId AND txt3.languageId = " + dfltLangId + ") ");
		sql.append("    LEFT JOIN user_text txt4 ON (txt4.userTextId = cmp.userTextId AND txt4.languageId = " + dfltLangId + ") ");
		sql.append("    LEFT JOIN pdi_abd_text txt11 ON (txt11.textId = sf.defaultTextId AND txt11.languageId = " + langId + ") ");
		sql.append("    LEFT JOIN user_text txt12 ON (txt12.userTextId = sf.userTextId AND txt12.languageId = " + langId + ") ");
		sql.append("    LEFT JOIN pdi_abd_text txt13 ON (txt13.textId = cmp.defaultTextId AND txt13.languageId = " + langId + ") ");
		sql.append("    LEFT JOIN user_text txt14 ON (txt14.userTextId = cmp.userTextId AND txt14.languageId = " + langId + ") ");
		sql.append("  WHERE sf.dnaId = " + dnaId + " ");
		sql.append("  ORDER BY sf.sequence, cmp.sequence");
		
		try
		{
			stmt = this.con.createStatement();
			rs = stmt.executeQuery(sql.toString());

			if (! rs.isBeforeFirst())
			{
				// No data... nothing to do
				return ret;
			}
			
			// Read the data and build the item list array
			long curSf = 0L;
			while(rs.next())
			{
				// Check the super-factor
				long sfId = rs.getLong("sfId");
				if (sfId != curSf)
				{
					ReportModelTranslationItemDTO itm = new ReportModelTranslationItemDTO();

					// Super-factor changed... put out the SF text
					itm.setItemId(rs.getLong("sfId"));
					itm.setItemType(ReportModelTranslationItemDTO.IT_SF);
					itm.setStaticTextId(rs.getLong("sfDefaultTextId"));
					itm.setUserTextId(rs.getLong("sfUserTextId"));
					itm.setChanged(false);
					
					// OK.  This is the data for the "English" column.  The rule
					// is, "Use the user text if it is there, else use the
					// default/static text"
					if (itm.getUserTextId() == 0L)
					{
						// Use the static/default English text 
						itm.setReferenceText(rs.getString("sfStaticEnglishText"));
					}
					else
					{
						// use the user English text
						itm.setReferenceText(rs.getString("sfUserEnglishText"));
					}
					
					// And here is the data for the translated column.  The rule here is,
					// "If there is no user text, show the translated static/default text.
					// If there is a user text, show the translated user text.  If there 
					// is no translation for the user text, show a blank entry".
					if (itm.getUserTextId() == 0L)
					{
						// Show the translated static/default text
						itm.setTranslatedText(rs.getString("sfStaticLangText"));
					}
					else
					{
						itm.setTranslatedText(rs.getString("sfUserLangText"));
					}
					
					// Put this item into the output array
					ret.getXlateItemList().add(itm);
					
					// Reset the curSf
					curSf = sfId;
				}	// End of if(sfId != curSf)
				
				// Now do the competency that is part of this row
				ReportModelTranslationItemDTO itm = new ReportModelTranslationItemDTO();

				itm.setItemId(rs.getLong("rmCompId"));
				itm.setItemType(ReportModelTranslationItemDTO.IT_CMP);
				itm.setStaticTextId(rs.getLong("cmpDefaultTextId"));
				itm.setUserTextId(rs.getLong("cmpUserTextId"));
				itm.setChanged(false);
									
				// Data for the "English" column.  Same rules as above.
				if (itm.getUserTextId() == 0L)
				{
					// Use the static/default English text 
					itm.setReferenceText(rs.getString("cmpStaticEnglishText"));
				}
				else
				{
					// use the user English text
					itm.setReferenceText(rs.getString("cmpUserEnglishText"));
				}

				// Data for the translated column.  Same rules as above.
				if (itm.getUserTextId() == 0L)
				{
					// Show the translated static/default text
					itm.setTranslatedText(rs.getString("cmpStaticLangText"));
				}
				else
				{
					itm.setTranslatedText(rs.getString("cmpUserLangText"));
				}
								
				// Put this item into the output array
				ret.getXlateItemList().add(itm);
			}	// End of while loop on the Result Set

			return ret;
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL getTranslationData.  " +
	    			"SQLException: " + ex.getMessage() + ", " +
	    			"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (rs != null)
			{
				try	{  rs.close();  }
				catch (SQLException sqlEx) {  sqlEx.printStackTrace();  }
				rs = null;
			}
			if (stmt != null)
			{
				try	{  stmt.close();  }
				catch (SQLException sqlEx) {  sqlEx.printStackTrace();  }
				stmt = null;
			}
		}
	}


	/**
	 * updateTranslationData
	 * Update user texts as needed using the incoming data.
	 * 
	 * NOTE:  This code assumes that the data contained (IDs, etc.) are
	 * correct.  The data passed in here is usually the data that was
	 * passed out in the getTranslationData method (above).
	 *
	 * @param xlateDto - A minimally filled ReportModelTranslationDTO object
	 * @throws Exception
	 */
	 public void updateTranslationData(ReportModelTranslationDTO xlateDto)
		throws Exception
	{
		// get the dna Id and the language ID
		long dnaId = xlateDto.getDnaId();
		long langId = xlateDto.getLanguageItem().getId();
		long dfltLangId = DefaultLanguageHelper.fetchDefaultLanguageId();

		String phase = "";
		PreparedStatement existPs = null;
		PreparedStatement userInsDfltPs = null;
		PreparedStatement userUpdtTidPs = null;
		PreparedStatement userInsLangPs = null;
		PreparedStatement userUpdtTextPs = null;
		PreparedStatement sfUpdtPs = null;
		PreparedStatement cmpUpdtPs = null;
		PreparedStatement userDelPs = null;
		ResultSet rs = null;
		
		try
		{
			StringBuffer sql = new StringBuffer();

			// Set up prepared statements... They might not all
			// get used, but we are going to set them up anyway.
			
			// user text insert for default text
			sql.setLength(0);
			sql.append("INSERT INTO user_text ");
			sql.append("  (languageId, typeCode, userText, isDefault, lastUserId, lastDate) ");
			sql.append("  VALUES ( ?, 'C', ?, 1, SYSTEM_USER, GETDATE())");
			phase = "Prepare userInsDfltPs";
			userInsDfltPs =  con.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
				
			// user text id update on default row
			sql.setLength(0);
			sql.append("UPDATE user_text ");
			sql.append("  SET userTextId = ? ");
			sql.append("  WHERE uniqueId = ?");
			phase = "Prepare userUpdtTidPs";
			userUpdtTidPs =  con.prepareStatement(sql.toString());
			
			// user text insert - not default
			sql.setLength(0);
			sql.append("INSERT INTO user_text ");
			sql.append("  (userTextId, languageId, typeCode, userText, isDefault, lastUserId, lastDate) ");
			sql.append("  VALUES ( ?, ?, 'C', ?, 0, SYSTEM_USER, GETDATE())");
			phase = "Prepare userInsLangPs";
			userInsLangPs =  con.prepareStatement(sql.toString());

		 	// item update - SF
			sql.setLength(0);
			sql.append("UPDATE pdi_abd_rm_superfactor ");
			sql.append("  SET userTextId = ?, ");
			sql.append("      lastDate = GETDATE() ");
			sql.append("  WHERE dnaId = ? ");
			sql.append("    AND sfId = ?");
			phase = "Prepare sfUpdtPs";
			sfUpdtPs =  con.prepareStatement(sql.toString());
			
			// item update - Competency
			sql.setLength(0);
			sql.append("UPDATE pdi_abd_rm_competency ");
			sql.append("  SET userTextId = ?, ");
			sql.append("      lastDate = GETDATE() ");
			sql.append("  WHERE dnaId = ? ");
			sql.append("    AND rmCompId = ?");
			phase = "Prepare cmpUpdtPs";
			cmpUpdtPs =  con.prepareStatement(sql.toString());
			
		 	// existence check
			sql.setLength(0);
			sql.append("SELECT COUNT(*) ");
			sql.append("  FROM user_text ");
			sql.append("  WHERE userTextId = ? ");
			sql.append("    AND languageId = ?");
			phase = "Prepare existPs";
			existPs =  con.prepareStatement(sql.toString());

		 	// user text update
			sql.setLength(0);
			sql.append("UPDATE user_text ");
			sql.append("  SET userText = ?, ");
			sql.append("      lastDate = GETDATE() ");
			sql.append("  WHERE userTextId = ? ");
			sql.append("    AND languageId = ?");
			phase = "Prepare userInsLangPs";
			userUpdtTextPs =  con.prepareStatement(sql.toString());
			
		 	// delete a language text
			sql.setLength(0);
			sql.append("DELETE FROM user_text ");
			sql.append("  WHERE userTextId = ? ");
			sql.append("    AND languageId = ?");
			phase = "Prepare userDelPs";
			userDelPs =  con.prepareStatement(sql.toString());

			// Loop through the translation items.  If changed:
			for(ReportModelTranslationItemDTO itm : xlateDto.getXlateItemList())
			{
				if (! itm.getChanged())
				{
					// Item is not changed... don't do anything with it
					continue;
				}

				long userTextId = itm.getUserTextId();
			
				if (itm.getTranslatedText() == null  || itm.getTranslatedText().length() < 1)
				{
					// is it the default language?
					if (langId == dfltLangId)
					{
						// Skip the delete... for now don't delete the default
						continue;
					}
					
					// delete the language text in that language
					phase = "Setting values - userDelPs";
					userDelPs.setLong(1,userTextId);
					userDelPs.setLong(2,langId);
					phase = "Execute - userDelPs";
					userDelPs.executeUpdate();

					
					// done... loop back to the top
					continue;
				}

				// Got changes.  we always manipulate user text (never static text)
				if (userTextId == 0L)
				{
					// We don't yet have a user text item for this... make one
			 		// Insert the reference/English text as default
					phase = "Setting values - userInsDfltPs";
					userInsDfltPs.setLong(1, dfltLangId);
					userInsDfltPs.setString(2, itm.getReferenceText().replace("'", "''"));
					phase = "Execute - userInsDfltPs";
					userInsDfltPs.executeUpdate();
					
					// Get the unique id...
					if ( rs != null)
					{
						phase = "Clearing rs (1)";
						rs.close();
						rs = null;
					}
					phase = "Get key - userInsDfltPs";
					rs = userInsDfltPs.getGeneratedKeys();
					rs.next();
					userTextId = rs.getLong(1);	// Replace the 0 with a real value
	
					// .. and stick it into the text ID field
					phase = "Setting values - userUpdtTidPs";
					userUpdtTidPs.setLong(1, userTextId);
					userUpdtTidPs.setLong(2, userTextId);
					phase = "Execute - userUpdtTidPs";
					userUpdtTidPs.executeUpdate();
					
					// Default text inserted... insert the one we want to now (if needed)
					if (langId != dfltLangId)
					{
						phase = "Setting values - userInsLangPs (1)";
						userInsLangPs.setLong(1, userTextId);
						userInsLangPs.setLong(2, langId);
						userInsLangPs.setString(3, itm.getTranslatedText());
						phase = "Execute - userInsLangPs (1)";
						userInsLangPs.executeUpdate();
					}
					
				 	// Update the item user text id
					// What sort of item is it?
					if (itm.getItemType().equals(ReportModelTranslationItemDTO.IT_SF))
					{
						// Super-factor
						// Update the item row with the new text id
						phase = "Setting values - sfUpdtPs";
						sfUpdtPs.setLong(1,userTextId);
						sfUpdtPs.setLong(2, dnaId);
						sfUpdtPs.setLong(3, itm.getItemId());
						phase = "Execute - sfUpdtPs";
						sfUpdtPs.executeUpdate();
					}
					if (itm.getItemType().equals(ReportModelTranslationItemDTO.IT_CMP))
					{
						// Competency
						// Update the competency row with the new text id
						phase = "Setting values - cmpUpdtPs";
						cmpUpdtPs.setLong(1,userTextId);
						cmpUpdtPs.setLong(2, dnaId);
						cmpUpdtPs.setLong(3, itm.getItemId());
						phase = "Execute - cmpUpdtPs";
						cmpUpdtPs.executeUpdate();
					}
				}
				else
				{
					// else, there is already a text in some language
			 		// See if the text exists in THIS language
					if ( rs != null)
					{
						phase = "Clearing rs (2)";
						rs.close();
						rs = null;
					}
					phase = "Setting values - existPs";
					existPs.setLong(1,userTextId);
					existPs.setLong(2,langId);
					phase = "Execute - existPs";
					rs = existPs.executeQuery();
					phase = "Fetching count after existPs";
					rs.next();
					int cnt = rs.getInt(1);
					
					// if there is none yet, insert it
					if (cnt < 1)
					{
						// Row does not exist (no user text in this language)... insert one
						phase = "Setting values - userInsLangPs (2)";
						userInsLangPs.setLong(1, userTextId);
						userInsLangPs.setLong(2, langId);
						userInsLangPs.setString(3, itm.getTranslatedText());
						phase = "Execute - userInsLangPs (2)";
						userInsLangPs.executeUpdate();
					}
					else
					{
						// Update the existing one
						phase = "Setting values - userUpdtTextPs";
						userUpdtTextPs.setString(1, itm.getTranslatedText());
						userUpdtTextPs.setLong(2, userTextId);
						userUpdtTextPs.setLong(3, langId);
						phase = "Execute - userUpdtTextPs";
						userUpdtTextPs.executeUpdate();
					}	// end of else (row count check was > 0)
				}	// End of the else (userTextId was not = 0)
			}	// End of the for loop

			return;
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new SQLException("SQL updateTranslationData, phase=" + phase + ".", ex);
		}
		finally
		{
			if (rs != null)
			{
				try	{  rs.close();  }
				catch (SQLException sqlEx) {  sqlEx.printStackTrace();  }
				rs = null;
			}
			if (existPs != null)
			{
				try	{  existPs.close();  }
				catch (SQLException sqlEx) {  sqlEx.printStackTrace();  }
				existPs = null;
			}
			if (userInsDfltPs != null)
			{
				try	{  userInsDfltPs.close();  }
				catch (SQLException sqlEx) {  sqlEx.printStackTrace();  }
				userInsDfltPs = null;
			}
			if (userUpdtTidPs != null)
			{
				try	{  userUpdtTidPs.close();  }
				catch (SQLException sqlEx) {  sqlEx.printStackTrace();  }
				userUpdtTidPs = null;
			}
			if (userInsLangPs != null)
			{
				try	{  userInsLangPs.close();  }
				catch (SQLException sqlEx) {  sqlEx.printStackTrace();  }
				userInsLangPs = null;
			}
			if (userUpdtTextPs != null)
			{
				try	{  userUpdtTextPs.close();  }
				catch (SQLException sqlEx) {  sqlEx.printStackTrace();  }
				userUpdtTextPs = null;
			}
			if (sfUpdtPs != null)
			{
				try	{  sfUpdtPs.close();  }
				catch (SQLException sqlEx) {  sqlEx.printStackTrace();  }
				sfUpdtPs = null;
			}
			if (cmpUpdtPs != null)
			{
				try	{  cmpUpdtPs.close();  }
				catch (SQLException sqlEx) {  sqlEx.printStackTrace();  }
				cmpUpdtPs = null;
			}
			if (userDelPs != null)
			{
				try	{  userDelPs.close();  }
				catch (SQLException sqlEx) {  sqlEx.printStackTrace();  }
				userDelPs = null;
			}
		}
	}
}
