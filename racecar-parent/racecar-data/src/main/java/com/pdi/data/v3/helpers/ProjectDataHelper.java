/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.helpers;

import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.pdi.data.util.RequestStatus;
import com.pdi.data.v3.dto.ClientDTO;
import com.pdi.data.v3.dto.DocumentDTO;
import com.pdi.data.v3.dto.ParticipantListDTO;
import com.pdi.data.v3.dto.ProjectDTO;
import com.pdi.data.v3.dto.ProjectListDTO;
import com.pdi.data.v3.dto.SessionUserDTO;
import com.pdi.data.v3.util.V3WebserviceUtils;
import com.pdi.xml.XMLUtils;

/**
 * DataHelper for Project Information
 * 
 * @author		Gavin Myers
 * @author		Ken Beukelman
 */
public class ProjectDataHelper
{

	//
	// Static data.
	//

	//
	// Static methods.
	//


	////////////////////////////////////////////
	//  Locally defined private static class  //
	////////////////////////////////////////////
	
	/**
	 * Internal Thread Class used to get data (participant counts) for
	 * multiple projects back from the single project list
	 */
	private static class ProjectPartCountThread extends Thread
	{
		// Instance Data
		private SessionUserDTO _sessionUser;
		private ProjectDTO _project;

		// Constructor
		public ProjectPartCountThread()
		{
		}

		// Instance methods  
		public void run()
		{
			//hit the webservice
			try
			{
				_project.setParticipantCount(getPartCount(_sessionUser, _project));
			}
			catch(Exception e)
			{
				//
			}
		}


		// Getters and setters
		
		//****************************************************************
		public void setProject(ProjectDTO value)
		{
			_project = value;
		}
//
//		public ProjectDTO getProject()
//		{
//			return _project;
//		}
		
		//****************************************************************
		public void setSessionUser(SessionUserDTO value)
		{
			_sessionUser = value;
		}
//
//		public SessionUserDTO getSessionUser()
//		{
//			return _sessionUser;
//		}
	}	// End locally used thread class


	//////////////////////////////
	//  Private static methods  //
	//////////////////////////////


	/**
	 * Thread out the participant count get
	 * relies on the fact that we are passing by reference
	 * @param sessionUser
	 * @return
	 */
	private static void threadPartCount(SessionUserDTO sessionUser, ArrayList<ProjectDTO> results)
	{
		//Thread out to get the participant counts
		int count = results.size();
		ProjectPartCountThread	threads[] = new ProjectPartCountThread[count];

		for(int i = 0; i < count; i++)
		{
			threads[i] = new ProjectPartCountThread();
			threads[i].setSessionUser(sessionUser);
			threads[i].setProject(results.get(i));
			threads[i].start();
		}

		//Join the threads
		for(int i = 0; i < count; i++)
		{
			try
			{
				threads[i].join();
			}
			catch (InterruptedException e)
			{
				System.out.print("Join interrupted\n");
			}
		}
	}

	/**
	 * Working code for the participant counts
	 * relies on the fact that we are passing by reference
	 * @param sessionUser
	 * @return
	 */
	private static int getPartCount(SessionUserDTO sessionUser, ProjectDTO project)
	{
		DocumentDTO dto = V3WebserviceUtils.request(sessionUser,"project/"+project.getId()+"/participant/list","");
		if (! dto.getStatus().isOk())
		{
			return 0;
		}

		Document response = dto.getDocument();
		NodeList nodes = XMLUtils.getElements(response, "//participantListLine");
		
		return nodes.getLength();
	}



	/////////////////////////////
	//  Public static methods  //
	/////////////////////////////
	/**
	 * Get all projects a user has access to
	 * @param sessionUser
	 * @return
	 */
	public static ProjectListDTO getProjects(SessionUserDTO sessionUser)
	{
		DocumentDTO dto = V3WebserviceUtils.request(sessionUser,"project/list","");
		if (! dto.getStatus().isOk())
		{
			return new ProjectListDTO(dto.getStatus());
		}

		Document response = dto.getDocument();		
		NodeList nodes = XMLUtils.getElements(response, "//projectListLine");
		
		ArrayList<ProjectDTO> results = new ArrayList<ProjectDTO>();
		for(int i = 0; i < nodes.getLength(); i++)
		{
			Node node = nodes.item(i);
			ProjectDTO project = new ProjectDTO(node);
			results.add(project);
		}
		
		// For each project in the list, get the participant count	
		threadPartCount(sessionUser, results);

		return new ProjectListDTO(results);
	}


	/**
	 * Get all projects a user has access to BY CLIENT
	 * @param sessionUser
	 * @return
	 */
//TODO Code not fully tested as it is not exposed by the service
	public static ProjectListDTO getProjects(SessionUserDTO sessionUser, ClientDTO client)
	{
		DocumentDTO dto = V3WebserviceUtils.request(sessionUser,"project/list/"+client.getId(),"");
		if (! dto.getStatus().isOk())
		{
			return new ProjectListDTO(dto.getStatus());
		}

		Document response = dto.getDocument();
		NodeList nodes = XMLUtils.getElements(response, "//projectListLine");
		
		ArrayList<ProjectDTO> results = new ArrayList<ProjectDTO>();
		for(int i = 0; i < nodes.getLength(); i++)
		{
			Node node = nodes.item(i);
			ProjectDTO project = new ProjectDTO(node);
			//ArrayList<ParticipantDTO> participants = ParticipantDataHelper.getParticipants(sessionUser, project);
			//project.setParticipants(participants);
			results.add(project);
		}

		// For each project in the list, get the participant count	
		threadPartCount(sessionUser, results);

		return new ProjectListDTO(results);
	}


	/**
	 * Fill a project - if there is a URI, it will be used, otherwise the id will be used.
	 * @param sessionUser
	 * @param project dto
	 * @return
	 */
	public static ProjectDTO getProject(SessionUserDTO sessionUser, ProjectDTO project)
	{
		if (project.getUri() == null  &&
			project.getId() == null)
		{
			project.setStatus(new RequestStatus(RequestStatus.RS_ERROR, "A candidate ID or a URI is required to fetch a client."));
			return project;
		}

		DocumentDTO docDto;
		if (project.getUri() == null)
		{
			// get the data directly
			docDto = V3WebserviceUtils.request(sessionUser,"project/" + project.getId(),"");
		}
		else
		{
			// get the data using the uri
			docDto = V3WebserviceUtils.request(sessionUser,project.getUri(),"");
		}

		if (! docDto.getStatus().isOk())
		{
			project.setStatus(docDto.getStatus());
			return project;
		}

		Document response = docDto.getDocument();
		NodeList nodes = XMLUtils.getElements(response, "//resultProject");
		
		ProjectDTO newProject = new ProjectDTO(nodes.item(0));
		// Preserve the incoming URI (if any)
		if (project.getUri() != null)
		{
			newProject.setUri(project.getUri());		
		}
		
		// and get the participant list
		ParticipantListDTO partListDto = ParticipantDataHelper.getParticipants(sessionUser, newProject);
		if (partListDto.getStatus().isOk())
		{
			newProject.setParticipantList(partListDto.getParticipantList());
		}
		
		// Get the participant count
		newProject.setParticipantCount(getPartCount(sessionUser, newProject));

		return newProject;
	}
	
	/**
	 * Registers/activates all participants on a project
	 * @param sessionUser
	 * @param project dto
	 * @return
	 */
	public static void registerProjectParticipants(SessionUserDTO sessionUser, ProjectDTO project)
	{
		String url = "project/"+project.getId()+"/activity/"+project.getActivityId()+"/participations";
		System.out.println(url);
		DocumentDTO document = V3WebserviceUtils.request(sessionUser,url,"");
		System.out.println(document.getStatus().getExceptionMessage());
		System.out.println(XMLUtils.nodeToString(document.getDocument().getFirstChild()));
	}
	
//	/**
//	 * Get the Project from the uri:
//	 * https://platform.iscopia.com/webservices1/rest/v1/project/PWDQ-KPQN
//	 * @param uri string
//	 * @return ProjectDTO
//	 */
//	public static ProjectDTO getProjectFromURI(String uri)
//	{
//	   	SessionUserDTO sessionUser = new SessionUserDTO();
//    	sessionUser.setUsername(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.username")); 
//    	sessionUser.setPassword(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.password"));
//
//    	DocumentDTO dto = V3WebserviceUtils.request(sessionUser,uri,"");
//		if (! dto.getStatus().isOk())
//		{
//			return new ProjectDTO();
//		}
//
//		Document response = dto.getDocument();		
//		System.out.println("dto   " + dto.toString());
//		System.out.println(" response.getNodeName():  "  + response.getNodeName());
//		ProjectDTO pdto = new ProjectDTO(response);
//		
//
//		return pdto;
//	}
//	
//	
//	/**
//	 * Get project-specific special field data:
//	 * 		transition level, 
//	 * 		current transition level label
//	 * 		target transition level label
//	 * 		(not currently, but eventually:  individual report norm)
//	 * @param uri string
//	 * @return ProductDTO
//	 */
//	public static HashMap<String, String> getProjectCustomFieldData(String uri)
//	{
//	   	SessionUserDTO sessionUser = new SessionUserDTO();
//    	sessionUser.setUsername(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.username")); 
//    	sessionUser.setPassword(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.password"));
//
//		DocumentDTO dto = V3WebserviceUtils.request(sessionUser,uri,"");
//		if (! dto.getStatus().isOk())
//		{
//			//return new ProductDTO();
//		}
//
//		Document response = dto.getDocument();
//		HashMap<String, String> fieldData = new HashMap<String, String>();
//		
//		//get customField nodes.... 
//		NodeList nodes = XMLUtils.getElements(response, "//resultProject/customFields/customField/.");
//		
//		for(int i = 0; i < nodes.getLength(); i++) {
//			Node node = nodes.item(i);
//			
//			// not sure if this is good or not, BUT... 
//			String nodeValue = node.getFirstChild().getTextContent();			
//			String nodeValue2 = node.getFirstChild().getNextSibling().getTextContent();	
//			
//			System.out.println(nodeValue + ",  " + nodeValue2);
//			fieldData.put(nodeValue, nodeValue2);
//			
//		}
//			
//
//		return fieldData;
//	}	

	//
	// Instance data.
	//

	//
	// Constructors.
	//

	//
	// Instance methods.
	//
}
