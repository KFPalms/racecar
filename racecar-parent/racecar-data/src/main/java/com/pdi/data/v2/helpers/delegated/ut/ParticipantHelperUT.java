/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.data.v2.helpers.delegated.ut;

//import java.util.ArrayList;

import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.IParticipantHelper;
import com.pdi.properties.PropertyLoader;
//import com.pdi.data.dto.Project;
import com.pdi.data.dto.Participant;
import com.pdi.data.dto.SessionUser;
import junit.framework.TestCase;


public class ParticipantHelperUT extends TestCase{

	public ParticipantHelperUT(String name)
	{
		super(name);
	}
/*
	public void testGetParticipantsByProjectId()
	throws Exception{
		
		System.out.println("*********** testGetParticipantsByProjectId ***********");
		SessionUser session = new SessionUser();
		session.getMetadata().put("USERNAME", "cdunn");
		session.getMetadata().put("PASSWORD", "cdunn");
		
		IProjectParticipantHelper iP = HelperDelegate.getProjectParticipantHelper(PropertyLoader.getProperty("com.pdi.data.v2.application","datasource.v2"));
		ArrayList<ProjectParticipant> participants = iP.fromProjectId(session, "HSPMUGMB");

		for(ProjectParticipant ePart: participants){
			
			Project e = ePart.getProject();
			System.out.println("     " + e.getId());
			Participant p = ePart.getParticipant();
			System.out.println("     " + p.getId() + "  " + p.getFirstName() + " " + p.getLastName());
			//System.out.println("     " + p.getEmail() + "  " + p.getBusinessUnit() + " " + p.getJobTitle());
			System.out.println("      ------ ");
			
		}
		
				
	}

	public void testGetParticipantByProjectIdParticipantId()
	throws Exception{
		
		System.out.println("*********** testGetParticipantByProjectIdParticipantId ***********");
		SessionUser session = new SessionUser();
		session.getMetadata().put("USERNAME", "cdunn");
		session.getMetadata().put("PASSWORD", "cdunn");
		
		IProjectParticipantHelper iP = HelperDelegate.getProjectParticipantHelper(PropertyLoader.getProperty("com.pdi.data.application","datasource.v2"));
		ProjectParticipant ePart = iP.fromProjectIdParticipantId(session, "HSPMUGMB", "KEXUKZVI");
		
		Project e = ePart.getProject();
		System.out.println("     " + e.getId());
		Participant p = ePart.getParticipant();
		System.out.println("     " + p.getId() + "  " + p.getFirstName() + " " + p.getLastName());
		//System.out.println("     " + p.getEmail() + "  " + p.getBusinessUnit() + " " + p.getJobTitle());
			
			
	}
		*/
	public void testGetParticipantById()
	throws Exception{
		
		System.out.println("*********** testGetParticipantById ***********");
		SessionUser session = new SessionUser();
		session.getMetadata().put("USERNAME", "cdunn");
		session.getMetadata().put("PASSWORD", "cdunn");
		
		IParticipantHelper iP = HelperDelegate.getParticipantHelper(PropertyLoader.getProperty("com.pdi.data.application","datasource.v2"));
		Participant p = iP.fromId(session, "FDBACQOM");
		if (p != null)
		{
		System.out.println("     " + p.getId() + "  " + p.getFirstName() + " " + p.getLastName());
		//System.out.println("     " + p.getEmail() + "  " + p.getBusinessUnit() + " " + p.getJobTitle());
		}
		else
		{
			System.out.println("fromId() commented out.... returns null");
		}
			
			
	}
}
