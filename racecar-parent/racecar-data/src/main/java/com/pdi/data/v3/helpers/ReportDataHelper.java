package com.pdi.data.v3.helpers;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.util.RequestStatus;
import com.pdi.data.v3.dto.ParticipantDTO;
import com.pdi.data.v3.dto.ParticipantListDTO;
import com.pdi.data.v3.dto.ParticipantReportListDTO;
import com.pdi.data.v3.dto.ParticipationDTO;
import com.pdi.data.v3.dto.ProjectDTO;
import com.pdi.data.v3.dto.ReportDTO;
import com.pdi.data.v3.dto.ResultDTO;
import com.pdi.data.v3.dto.ScaleScoreDTO;
import com.pdi.data.v3.dto.SessionUserDTO;
import com.pdi.data.v3.util.V3DatabaseUtils;
import com.pdi.properties.PropertyLoader;
import com.pdi.scoring.Norm;
import com.pdi.scoring.NormSet;

/**
 * DataHelper for Client Information
 * 
 *
 * @author		Gavin Myers
 * @author		MB Panichi
 */

public class ReportDataHelper {
	
	//
	// Static data.
	//

	// 
	// Static methods.
	//

	//
	// Instance data.
	//

	private static HashMap<String, String> rptDisplayDataMap = new HashMap<String, String>();
	private static HashMap<String, String> resultsData = new HashMap<String, String>();
	//
	// Constructors.
	//

	//
	// Instance methods.
	//
	/**
	 * Return a list of participant report matches
	 * 
	 * @param sessionUser
	 * @param participantList
	 * @return List of ParticipantReportListDTO objects
	 */
	public static ArrayList<ParticipantReportListDTO> getParticipantReportList(SessionUserDTO sessionUser, ParticipantListDTO participantList)
	{
		
		/*
		 * This is built on how I think the data will be coming back from the database
		 * Step 1: Loop through the participants
		 * Step 2: Get the reports that are available for one participant
		 * Step 3: Build a list of reports/participant matches
		 * In order to do step 3 we almost have to flip the data, we have a list of  participanats with reports
		 * while what we really want is a list of reports with participants
		 */
		
		ArrayList<ParticipantReportListDTO> participantReportList = new ArrayList<ParticipantReportListDTO>();
		
		//Step 1:
		for(ParticipantDTO participant : participantList.getParticipantList())
		{
			ArrayList<ReportDTO> reports = ReportDataHelper.getParticipantReportList(sessionUser, participant);
			
			//Step 2:
			for(ReportDTO report : reports)
			{
				boolean foundReport = false;
				
				//Step 3:
				for(ParticipantReportListDTO participantReport : participantReportList) {
					if(participantReport.getReport().getReportId() == report.getReportId()) {
						foundReport = true;
						participantReport.getParticipantList().getParticipantList().add(participant);
					}
				}

				if(!foundReport) {
					ParticipantReportListDTO participantReport = new ParticipantReportListDTO();
					participantReport.setReport(report);
					participantReport.getParticipantList().getParticipantList().add(participant);
					participantReportList.add(participantReport);
				}
			}
		}
		
		return participantReportList;
	}

	/**
	 * Return a list of reports that a particular participant has access too
	 * @param sessionUser
	 * @param participant
	 * @return
	 */
	public static ArrayList<ReportDTO> getParticipantReportList(SessionUserDTO sessionUser, ParticipantDTO participant) {
		
		ArrayList<ReportDTO> reports = new ArrayList<ReportDTO>();
		
		ReportDTO report = new ReportDTO();
		/*
		report.setReportId(1);
		report.setReportName("TLT Group Summary");
		report.setReportCode("TLT_GROUP_SUMMARY_REPORT");
		reports.add(report);
		*/
		report = new ReportDTO();
		report.setReportId(2);
		report.setReportName("TLT Individual Report");
		report.setReportCode("TLT_INDIVIDUAL_SUMMARY_REPORT");
		reports.add(report);
		
		report = new ReportDTO();
		report.setReportId(3);
		report.setReportName("TLT Group Detail Report");
		report.setReportCode("TLT_GROUP_DETAIL_REPORT");
		reports.add(report);
		
		report = new ReportDTO();
		report.setReportId(4);
		report.setReportName("Development Dashboard Report ");
		report.setReportCode("ABYD_DEVELOPMENT_REPORT");
		reports.add(report);
		
		report = new ReportDTO();
		report.setReportId(5);
		report.setReportName("Fit Dashboard Report ");
		report.setReportCode("ABYD_FIT_REPORT");
		reports.add(report);
		
		report = new ReportDTO();
		report.setReportId(6);
		report.setReportName("Readiness Dashboard Report");
		report.setReportCode("ABYD_READINESS_REPORT");
		reports.add(report);
		
		report = new ReportDTO();
		report.setReportId(7);
		report.setReportName("GPI Graphical Report");
		report.setReportCode("GPI_GRAPHICAL_REPORT");
		reports.add(report);
		
		report = new ReportDTO();
		report.setReportId(8);
		report.setReportName("LEI Graphical Report");
		report.setReportCode("LEI_GRAPHICAL_REPORT");
		reports.add(report);
		
		/*
		report = new ReportDTO();
		report.setReportId(9);
		report.setReportName("RAV_SF_GRAPHICAL_REPORT");
		report.setReportCode("RAV_SF_GRAPHICAL_REPORT");
		reports.add(report);
		*/
		
		report = new ReportDTO();
		report.setReportId(10);
		report.setReportName("Ravens Form Graphical Report ");
		report.setReportCode("RAV_B_GRAPHICAL_REPORT");
		reports.add(report);
		
		
		report = new ReportDTO();
		report.setReportId(11);
		report.setReportName("CHQ Detail Report");
		report.setReportCode("CHQ_DETAIL_REPORT");
		reports.add(report);
		
		report = new ReportDTO();
		report.setReportId(12);
		report.setReportName("Watson-Glaser E Graphical Report");
		report.setReportCode("WGE_DETAIL_REPORT");
		reports.add(report);
		
		
		return reports;
	}	
	/**
	 * Convenience method - do sql to get available reports list:
	 * @param String productId, String clientId
	 * 
	 */
	public static ArrayList<ReportDTO> getReportsForClientProduct(String v3productId, String clientId)
	{
		DataResult dr = null;
		ReportDTO rdto = new ReportDTO();
		ArrayList<ReportDTO> ret = new ArrayList<ReportDTO>();

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT rgm.reportId, ");
		sqlQuery.append("       r.reportName  ");
		sqlQuery.append("  FROM rp_product_map pm, rp_report_group_lookup rgl, ");
		sqlQuery.append("  				 rp_report_group_map rgm, rp_report r  ");
		sqlQuery.append("  WHERE pm.V3ProdId = '" + v3productId + "'  ");
		sqlQuery.append("    AND clientId = '" + clientId  + "'  ");
		sqlQuery.append("    AND r.active = 1  ");
		sqlQuery.append("    AND rgm.reportGroupId = rgm.reportGroupId  ");
		sqlQuery.append("    AND rgm.reportId = r.reportId   ");
		//System.out.println("sql:  " + sqlQuery.toString());		
		
		DataLayerPreparedStatement dlps = V3DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			rdto.setStatus(dlps.getStatus());
			ret.add(rdto);
			return ret;
		}

		try
		{
			dr = V3DatabaseUtils.select(dlps);
			
			rdto.setStatus(dr.getStatus());
			switch (rdto.getStatus().getStatusCode())
			{
				case RequestStatus.RS_ERROR:
					rdto.setStatus(dr.getStatus());
					ret.add(rdto);
					break;
				case RequestStatus.DBS_NO_DATA:
					rdto.getStatus().setExceptionMessage("No reports exist for this product/client.");
					ret.add(rdto);
					break;
				case RequestStatus.RS_OK:
					ResultSet rs = dr.getResultSet();
					while(rs.next()){
						//moveData(rs, rdto);
						rdto = new ReportDTO();
						rdto.setReportId( (new Long(rs.getString(1))).longValue());
						rdto.setReportName(rs.getString(2));
						System.out.println(rdto.getReportId() + "  " + rdto.getReportName());
						ret.add(rdto);
					}					
					break;
				default:
					rdto.setStatus(unexpectedStatus(rdto.getStatus()));
					ret.add(rdto);
					break;
			}
	
			return ret;
		}
		catch (SQLException ex)
		{
			rdto.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
											ex.getErrorCode(), "getLanguage:  " + ex.getMessage()));
			ret.add(rdto);
			return ret;
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}	

	/**
	 * This gets  reportData
	 * It needs a full ProjectDTO  and a ParticipantDTO with at least it's
	 * Participations fully loaded, so that you can get results data.
	 * 
	 * @param ProjectDTO projDTO
	 * @param ParticipantDTO partDTO
	 * 
	 */
	public void getReportData(ProjectDTO projDTO, ParticipantDTO partDTO)
	{
		
       	SessionUserDTO sessionUser = new SessionUserDTO();
    	sessionUser.setUsername(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.username")); 
    	sessionUser.setPassword(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.password"));

		// 1 get project transition levels, labels.... 
		//https://platform.iscopia.com/webservices1/rest/v1/project/PYKJ-BVWS
////		setRptDisplayDataMap(ProjectDataHelper.getProjectCustomFieldData(projDTO.getUri()));
		setRptDisplayDataMap(projDTO.getCustomFields());
		
		try {
			//2 get ParticipantResults.... 
			partDTO = ParticipantDataHelper.getParticipantResults(sessionUser, partDTO);
			
			//3  get the right participation for that participant.... 
			// to figure out which participation you need... 
			// get the participations.... then spin through them... 
			// compare the participation <productIdentifier> to the 
			// productDTO.productID in the ProjectDTO	
			ParticipationDTO participationDto = new ParticipationDTO();
			for(ParticipationDTO particDTO : partDTO.getParticipations())
			{
				if(particDTO.getProductId().equals(projDTO.getProductId())  ){
					// then this is the participation that you want!
					participationDto = particDTO;										
				}
			}
			
			ArrayList<ResultDTO> results = participationDto.getResults();

			//spin through the ResultDTO's, and build a hash map -
				// scale name, scale score
			for(int i = 0; i < results.size(); i++){
				 
				ResultDTO rDto = results.get(i);
				ArrayList<ScaleScoreDTO> scores = rDto.getScaleScores();
				for(int j = 0; j < scores.size(); j++){
					ScaleScoreDTO score = scores.get(j);  //id, value
					
					resultsData.put(score.getId(), score.getValue());
					System.out.println("ScaleScoreDTO id, value:  " + score.getId() + ", " + score.getValue());
				}
				
			}
			
		}catch(Exception ex){
			
			// this is an error.... 
			// no idea what I'm doing here... 
			
		}
		
	}	

	/*
	 * Creates a Pdi Normset 
	 *
	 */	
	static public NormSet getPdiNormset(Integer normSetId)
	{
		
		NormSet ret = new NormSet();
		ret.setNormsetId(normSetId);
		DataResult dr = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT  ns.displayName, ns.transitionLevel, ns.active  ");
		sqlQuery.append("  FROM  pdi_normset ns ");
		sqlQuery.append("  WHERE  ns.normsetId = " + normSetId.toString()  + "  ");
		sqlQuery.append("  AND ns.active = 1  ");
		//System.out.println("sql:  " + sqlQuery.toString());		
		
		DataLayerPreparedStatement dlps = V3DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			return ret;
		}

		try
		{
			dr = V3DatabaseUtils.select(dlps);
			ResultSet rs = dr.getResultSet();
			while(rs.next()){
				//setting the basics....
				ret.setDisplayName(rs.getString(0));
				ret.setTransitionLevel(rs.getInt(1));
				ret.setActive(rs.getInt(2));
				
			}
	
			return ret;
		}
		catch (SQLException ex)
		{
			//rdto.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
			//								ex.getErrorCode(), "getLanguage:  " + ex.getMessage()));
			//ret.add(rdto);
			return ret;
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}	
	}
	
	
	/*
	 * Creates a Pdi Normset 
	 *
	 *  Using this in the group report.
	 */	
	static public HashMap<String, NormSet> getPdiNormsets(Integer transitionLevel, Double targetTransitionLevel)
	{
		HashMap<String, NormSet> ret = new HashMap<String, NormSet>();
		DataResult dr = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT ns.normsetid, ns.displayName, ns.scoringStepName,  ");
		sqlQuery.append("		ns.transitionLevel, ns.transitionType,  "); 
		sqlQuery.append("       n.normId, n.labelKey,  ");
		sqlQuery.append("		n.mean, n.standardDeviation, n.weight ");
		sqlQuery.append("FROM pdi_normset ns, pdi_norm n ");
		sqlQuery.append("WHERE ns.normsetid = n.normsetId ");
		sqlQuery.append("AND (  ");
		sqlQuery.append(" ns.scoringStepName = 'TPOT2_Group_component_z_score'  ");
		sqlQuery.append("	OR  ");
		sqlQuery.append("   ns.scoringStepName = 'TPOT2_Group_Logodds'   ");	
		sqlQuery.append("AND ns.transitionLevel = '" + transitionLevel.intValue() + "' "); //projectObject.transitionLevel
		sqlQuery.append("	OR  ");	
		sqlQuery.append("   ns.scoringStepName = 'TPOT2_Group_Performance' 	");
		sqlQuery.append("AND ns.transitionLevel = '" + targetTransitionLevel.intValue() + "' "); //projectObject.getTargetLevelIDForTransition()
		sqlQuery.append("	OR  ");
		sqlQuery.append("   ns.scoringStepName = 'TPOT2_Group_Logodds_From_Zscor' 	");
		sqlQuery.append("AND ns.transitionLevel = '" + transitionLevel.intValue() + "' "); // projectObject.transitionLevel
		sqlQuery.append("	OR  ");
		sqlQuery.append("   ns.scoringStepName = 'TPOT2_Group_Performance_From_Z' ");
		sqlQuery.append("AND ns.transitionLevel = '" + targetTransitionLevel.intValue() + "' "); //projectObject.getTargetLevelIDForTransition()
		sqlQuery.append(" )  ");
		sqlQuery.append("AND ns.active = '1' ");
		sqlQuery.append("AND n.active = '1'  ");
		sqlQuery.append("ORDER BY  ns.normsetid, ns.displayName, ns.scoringStepName,  ");
		sqlQuery.append("ns.transitionLevel, ns.transitionType, n.normId  ");
		//System.out.println("getPdiNormset sql:  " + sqlQuery.toString());	
																		
		DataLayerPreparedStatement dlps = V3DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			return ret;
		}

		try
		{
			dr = V3DatabaseUtils.select(dlps);
			ResultSet rs = dr.getResultSet();
			String nsId = "0";
			NormSet normset= new NormSet();
			boolean isFirst = true;
			while(rs.next()){
				
				// check for a new NormSet Id
				if(! rs.getString(1).equals(nsId)){
					
					// create a new NormSet 
					if(isFirst){
						//reset the variable
						isFirst = false;
						
					}else {
						// changing normsets, so we want to add 
						// the now populated one to
						// the arraylist of normsets before we refresh it
						ret.put(normset.getDisplayName(), normset);												
						// NOW, create a new one... 
						normset = new NormSet();						
					}
					
					//setting the basics....
					normset.setNormsetId(new Integer(rs.getInt(1)));
					normset.setDisplayName(rs.getString(2));
					normset.setScoringStepName(rs.getString(3));					
					normset.setTransitionLevel(rs.getInt(4));
					normset.setTransitionType(rs.getInt(5));
					normset.setActive(1);
					
					// reset nsId...
					nsId = normset.getNormsetId().toString();					
//					System.out.println(" NORMSET:  ");
//					System.out.println("        :  " + normset.getNormsetId().toString());
//					System.out.println("        :  " +  normset.getDisplayName());
//					System.out.println("        :  " +  normset.getScoringStepName());
//					System.out.println("        :  " +  normset.getTransitionLevel());
//					System.out.println("        :  " +  normset.getTransitionType());
				
				}
				
				// now go ahead and get the norms.... 
					Norm norm = new Norm();
					//  n.normId, n.mean, n.standardDeviation, n.weight
					norm.setId(rs.getString(6));
					norm.setName(rs.getString(7));
					norm.setMean  ( ( rs.getString(8).equals("")) ?  new Double(0.0) : new Double(rs.getString(8)));  
					norm.setStdDev( ( rs.getString(9).equals("")) ? new Double(0.0)  : new Double(rs.getString(9)));  			
					norm.setWeight( ( rs.getString(10).equals(""))? new Double(0.0) : new Double(rs.getString(10))); 

					// add the norms to the normset
					normset.getNorms().add(norm);
//					System.out.println("        : NORM  : " + norm.getId());
//					System.out.println("        : NORM  : " + norm.getName());
//					System.out.println("        : NORM  : " + norm.getMean());	
//					System.out.println("        : NORM  : " + norm.getStdDev());	
//					System.out.println("        : NORM  : " + norm.getWeight());					
				}
	
			// add b4 returning!! 
			ret.put(normset.getDisplayName(), normset);				
	
			return ret;
		}
		catch (SQLException ex)
		{
			//rdto.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
			//								ex.getErrorCode(), "getLanguage:  " + ex.getMessage()));
			//ret.add(rdto);
			return ret;
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}	
	}
	
	/*
	 * Populates the NormSet with it's norms from the database.
	 *
	 */	
	static public NormSet populateNormset(NormSet normset)
	{
		
		DataResult dr = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT n.normId, ");
		sqlQuery.append("  n.labelKey,  ");
		sqlQuery.append("  n.mean,   ");
		sqlQuery.append("  n.standardDeviation  ");
		sqlQuery.append("  FROM  pdi_normset ns, pdi_norm n ");
		sqlQuery.append("  WHERE  ns.normsetId = " + normset.getNormsetId().toString() + "  ");
		sqlQuery.append("  AND ns.normsetId = n.normsetId    ");
		sqlQuery.append("  AND ns.active = 1  ");
		sqlQuery.append("  AND n.active = 1 ");
		//System.out.println("sql:  " + sqlQuery.toString());		
		
		DataLayerPreparedStatement dlps = V3DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			System.out.println("dlps.isInError()  ");	
			//rdto.setStatus(dlps.getStatus());
			//ret.add(rdto);
			return normset;
		}

		try
		{
			dr = V3DatabaseUtils.select(dlps);
			ResultSet rs = dr.getResultSet();
			while(rs.next()){
				
				Norm norm = new Norm ();
				norm.setId((new Integer(rs.getInt(1))).toString());
				norm.setName(rs.getString(2));
				norm.setMean((new Double(rs.getString(3)).doubleValue()));
				norm.setStdDev((new Double(rs.getString(4)).doubleValue()));
				normset.getNorms().add(norm);
				//System.out.println("norm: " + norm.getName() + " " + norm.getMean());
				
			}
	
			return normset;
		}
		catch (SQLException ex)
		{
			//rdto.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
			//								ex.getErrorCode(), "getLanguage:  " + ex.getMessage()));
			//ret.add(rdto);
			return normset;
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}	
	
	}

	/*
	 * Convenience method to determine the "target" level of a transition.
	 * For example, the "target" level for #1,IC-FLL is "FLL". The TPOT II
	 * norms in the database use these "target" levels:
	 *
	 *	6 = FLL
	 *          IC-->FLL
	 *
	 *	7 = MLL
	 *          FLL-->MLL
	 *           IC-->MLL	 
	 *
	 *	8 = EXEC
	 *          FLL-->EXEC
	 *          MLL-->EXEC
	 */	
	public static int getTargetLevelIDForTransition(String transitionLevel)
	{
		int returnValue = 0;
	
		if(transitionLevel.equals("IC - FLL")){
			//System.out.println("6 - IC - FLL");
			returnValue = 6;			
		}else if(transitionLevel.equals("IC - MLL")){
				//System.out.println("7 - IC - MLL");
			returnValue = 7;			
		}else if(transitionLevel.equals("FLL - MLL")){
				//System.out.println("7 - FLL - MLL");
			returnValue = 7;			
		}else if(transitionLevel.equals("FLL - Sr Exec")){
				//System.out.println("8 - FLL - Sr Exec");
			returnValue = 8;			
		}else if(transitionLevel.equals("MLL - Sr Exec")){
				//System.out.println("8 - MLL - Sr Exec");
			returnValue = 8;			
		}else{
			// error condition.... 
		}
	
		return returnValue;
	}	

	
	/*
	 * Convenience method to determine the "target" level of a transition.
	 * For example, the "target" level for #1,IC-FLL is "FLL". The TPOT II
	 * norms in the database use these "target" levels:
	 *
	 *	6 = FLL
	 *          IC-->FLL
	 *
	 *	7 = MLL
	 *          FLL-->MLL
	 *           IC-->MLL	 
	 *
	 *	8 = EXEC
	 *          FLL-->EXEC
	 *          MLL-->EXEC
	 */	
	public static Integer getTLTIndividualNormSetId(String transitionLevel)
	{
		int normSetId = 0;
	
		if(transitionLevel.equals("IC - MLL") || transitionLevel.equals("FLL - MLL"))
		{
			normSetId = 1;
		}else if (transitionLevel.equals("FLL - Sr Exec") || transitionLevel.equals("MLL - Sr Exec"))
		{			
			normSetId = 2;
		}
		else if(transitionLevel.equals("IC - FLL"))
		{			
			normSetId = 3;	
		} else {
			
			// error condition
			// can't go on w/out a valid normset
		}
	
		return normSetId;
	}	
	
	public  HashMap<String, String> getRptDisplayDataMap(){
		
		return rptDisplayDataMap;
	}
	
	public  void setRptDisplayDataMap(HashMap<String, String> value){
		
		rptDisplayDataMap = value;
	}
	
	/**
	 * Process the input status object to add the "Unexpected..." message
	 * 
	 * @param The RequestStatus object 
	 * @return A RequestStatus object
	 */
	private static RequestStatus unexpectedStatus(RequestStatus inp)
	{
		RequestStatus out = new RequestStatus(inp);
		String str = "Unexpected Status code encountered";
		str += (out.getExceptionMessage() == null ? "." : "; msg=" + out.getExceptionMessage());
		inp.setExceptionMessage(str);
		return out;
	}
}
