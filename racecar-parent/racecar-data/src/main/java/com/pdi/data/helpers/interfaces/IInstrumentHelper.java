package com.pdi.data.helpers.interfaces;

import java.util.ArrayList;
import com.pdi.data.dto.Instrument;
//import com.pdi.data.dto.ProjectParticipantInstrument;
import com.pdi.data.dto.SessionUser;

public interface IInstrumentHelper {
	
	// Types passed in call
	public static int TYPE_SCORABLE = 58010;
	public static int TYPE_ALL = 35741;

	// DB values
	public static int DB_NON_SCORE_TYPE = 0;	// e.g., CHQ
	public static int DB_SCORE_TYPE = 1;		// e.g., GPI, LEI, etc.
	public Instrument fromId(SessionUser session, String instrumentId);

	/**
	 * All instruments by a type
	 * @param session
	 * @return
	 */
	ArrayList<Instrument> all(SessionUser session, int type) throws Exception;
	
	//public ArrayList<Instrument> fromProjectId(SessionUser session, String engagementId);
	//public HashMap<String, String> getScoresForInstrument(SessionUser session, String instrumentId, String engagementid, String participantId);
	//public HashMap<String, String> getResponsesForInstrument(SessionUser session, String instrumentId, String engagementid, String participantId);
	//public boolean saveScoresForInstrument(SessionUser session, String instrumentId, String engagementid, String participantId,  HashMap<String, String> scoreMap);
	//public boolean saveResponsesForInstrument(SessionUser session, String instrumentId, String engagementid, String participantId,  HashMap<String, String> responseMap);

	//ArrayList<Instrument> fromProjectId(SessionUser session, String engagementId);

//	public ArrayList<Instrument> all(	SessionUser session);
	
}
