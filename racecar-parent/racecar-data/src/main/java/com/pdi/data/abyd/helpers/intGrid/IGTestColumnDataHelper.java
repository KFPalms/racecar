/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.intGrid;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import com.pdi.data.abyd.dto.common.IGInstCompData;
import com.pdi.data.abyd.dto.intGrid.IGInstDisplayData;
import com.pdi.data.abyd.dto.intGrid.IGScaleScoreData;
import com.pdi.data.abyd.dto.intGrid.IGTestAndNormNameData;
import com.pdi.data.abyd.helpers.intGrid.IGConstants;
import com.pdi.data.abyd.helpers.intGrid.IGScoring.ScoreAndNormData;
import com.pdi.data.util.language.DefaultLanguageHelper;

/**
 * IGTestColumnDataHelper returns the drill-down data for the "Testing" column on the
 * Integration Grid.
 *
 * @author		Ken Beukelman
 */
public class IGTestColumnDataHelper
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private Connection con;
	private String partId;
	private long dnaId;
	
	private long dfltLangId;

	//
	// Constructors.
	//
	public IGTestColumnDataHelper(Connection con, String partId, long dnaId)
		throws Exception
	{
		// ensure there is a candidate/participant id
		if (partId == null  || partId.length() < 1)
		{
			throw new Exception("A participant id is required to fetch Integration Grid column data.");
		}
		if (dnaId == 0)
		{
			throw new Exception("A DNA id is required to fetch Integration Grid data.");
		}

		this.con = con;
		this.partId = partId;
		this.dnaId = dnaId;
		
		this.dfltLangId = DefaultLanguageHelper.fetchDefaultLanguageId();
	}
	
	
	/**
	 * Controller method that gets the score data.
	 *
	 * @throws Exception
	 */
	public IGInstDisplayData getTestingColumnData()
		throws Exception
	{
		IGAlrDataHelper igah = new IGAlrDataHelper(this.con, this.partId, this.dnaId);
		boolean hasAlp = igah.hasAlpInDna();
		IGKf4dDataHelper igkh = new IGKf4dDataHelper(this.con, this.partId, this.dnaId);
		boolean hasKf4d = igkh.hasKf4dInDna();
		IGSummaryDataHelper igh = new IGSummaryDataHelper(this.con);
		boolean hasGpi = igh.checkForGPIinDna(this.dnaId);

		IGInstDisplayData ret = new IGInstDisplayData();
		
		// see if this has an ALP reference
		if (hasAlp)
		{
			ret.setHasKfp(true);
			ret.setRdi(Integer.parseInt(IGConstants.NO_RDI));
			ret.setCogsDone(igah.getAlrCogsDone());

			ret.setNormData(new ArrayList<IGTestAndNormNameData>());	// Empty norm list

			// Build an ArrayList of IGInstCompData objects and save them
			ret.setCompArray(igah.getAlrCompData());
		}
		else if (hasKf4d)
		{
			ret.setHasKfar(true);
			
			ret.setRdi(Integer.parseInt(IGConstants.NO_RDI));
			//this is where the scores and project info get pulled from remote services
			ret.setCogsDone(igkh.getKf4dCogsDone());

			ret.setNormData(new ArrayList<IGTestAndNormNameData>());	// Empty norm list

			// Build an ArrayList of IGInstCompData objects and save them
			ret.setCompArray(igkh.getKf4dCompData());
			ret.setTestInstrumentTitle(igkh.getTestInstrument());
		}
		else
		{
			ret.setGpiIncluded(hasGpi);
			// No ALP... do the old schools stuff... get the scores
			ScoreAndNormData dat = IGScoring.fetchIGTestScoresAndNorms(this.con,
																	   this.partId, this.dnaId);
	
			// Save overall/"header" data
			String rdiValue = dat.getRdi();
			if(rdiValue == null  || rdiValue.length() < 1)
			{
				rdiValue = IGConstants.NO_RDI;
			}
			ret.setRdi(Integer.parseInt(rdiValue));
	
			// Set the cogsDone flag
			ret.setCogsDone(dat.getCogsDone());
			
			// if the flag is not set, check to see if there are no cogs at all
			if(! ret.getCogsDone())
			{
				// Not done... check to see if there are no cogs to do
				if (IGScoring.noCogsInDna(this.con, this.dnaId))
					ret.setCogsDone(true);
			}
	
			// Save the norm names
			ArrayList<IGTestAndNormNameData> nn = IGScoring.getTestAndNormNames(con,dat.getNormMap(), IGConstants.NO_LEI);
			ret.setNormData(nn);
	
			// Build an ArrayList of IGInstCompData objects and save them
			ret.setCompArray(getCompData(dat.getScoreMap()));
		}

		ret.calcCompScores();

		return ret;
	}


	/**
	 * Controller method that gets the score data.
	 *
	 * @throws Exception
	 */
	private ArrayList<IGInstCompData> getCompData(Map<String, IGScoring.ScorePair> scoreMap)
		throws Exception
	{
		Map<Long, IGInstCompData> holder = new LinkedHashMap<Long, IGInstCompData>();
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT DISTINCT dl.competencyId, ");
		sqlQuery.append("                sims.internalName as type, ");
		sqlQuery.append("                txt1.text as compName, ");
		sqlQuery.append("                txt2.text as compDesc, ");
		sqlQuery.append("                cmp.internalName as intName, ");
		sqlQuery.append("                dl.groupSeq, ");
		sqlQuery.append("                dl.compSeq ");
		sqlQuery.append("  FROM pdi_abd_dna_link dl ");
		sqlQuery.append("  INNER JOIN pdi_abd_module sims ON (sims.moduleId = dl.moduleId ");
		sqlQuery.append("                                 AND sims.internalName in ('GPI','Cogs')) ");
		sqlQuery.append("  LEFT JOIN pdi_abd_competency cmp ON cmp.competencyId = dl.competencyId ");
		sqlQuery.append("  LEFT JOIN pdi_abd_text txt1 ON (txt1.textId = cmp.textId AND txt1.languageId = " + this.dfltLangId + ") ");
		sqlQuery.append("  INNER JOIN pdi_abd_dna dna on dna.dnaId = dl.dnaId ");
		sqlQuery.append("  LEFT JOIN pdi_abd_mod_comp_desc mcd on (mcd.modelId = dna.modelId ");
		sqlQuery.append("                                      AND mcd.competencyId = dl.competencyId) ");
		sqlQuery.append("  LEFT JOIN pdi_abd_text txt2 ON (txt2.textId = mcd.textId AND txt2.languageId = " + this.dfltLangId + ") ");
		sqlQuery.append("  WHERE dl.dnaId = " + this.dnaId + " ");
		sqlQuery.append("  and dl.isUsed = 1 ");
		sqlQuery.append("  ORDER BY dl.groupSeq, dl.compSeq");

		try
		{
			stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			// Get the competencies - generate IGInstCompData objects
			// (no score data) and a map of compId/intName
			Map<String, Long> nameMap = new HashMap<String, Long>();
			Set<Long> hasCogs = new HashSet<Long>();
			Set<Long> hasGpi = new HashSet<Long>();
			while (rs.next())
			{
				Long cmpId = rs.getLong("competencyId");
				if (! holder.containsKey(cmpId))
				{
					IGInstCompData elt = new IGInstCompData();
					elt.setCompId(cmpId);
					elt.setCompName(rs.getString("compName"));
					elt.setCompDesc(rs.getString("compDesc"));
					
					holder.put(cmpId, elt);
					nameMap.put(rs.getString("intName"), cmpId);
				}
				String typ = rs.getString("type");
				if (typ.equals("GPI"))
				{
					hasGpi.add(cmpId);
				}
				else if (typ.equals("Cogs"))
				{
					hasCogs.add(cmpId);
				}
			}

			// use the name map and the score map to get lists of scores for each compid
			Map<Long, IGScoring.ScoreLists> scores = IGScoring.getScoreLists(nameMap, scoreMap);

			// Stick the score lists into the IGInstCompData objects
			// and put the result in the output array
			ArrayList<IGInstCompData> ret = new ArrayList<IGInstCompData>();
			for (Iterator<Map.Entry<Long, IGInstCompData>> itr=holder.entrySet().iterator(); itr.hasNext(); )
			{
				Map.Entry<Long, IGInstCompData> ent = itr.next();
				IGScoring.ScoreLists scoreLists = scores.get(ent.getKey());
				IGInstCompData compData = ent.getValue();
					// Move in scores that are relevant for this competency
				if (hasCogs.contains(ent.getKey()))
				{
					// move in the cog scores
					compData.setCogColumnScores(scoreLists.getCogScores());
				}  else  {
					// put in an empty array
					compData.setCogColumnScores(new ArrayList<IGScaleScoreData>());
				}
				if (hasGpi.contains(ent.getKey()))
				{
					// move in the gpi scores
					compData.setGpiColumnScores(scoreLists.getGpiScores());
				}  else  {
					// put in an empty array
					compData.setGpiColumnScores(new ArrayList<IGScaleScoreData>());
				}
				
				ret.add(compData);
			}

			return ret;
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL in IG getCompData.  " +
							"SQLException: " + ex.getMessage() + ", " +
							"SQLState: " + ex.getSQLState() + ", " +
							"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (rs != null)
			{
			    try {  rs.close();  }
			    catch (SQLException sqlEx)
			    {
			    	System.out.println("ERROR: rs close in IG getCompData - " + sqlEx.getMessage());
			    }
				rs = null;
			}
			if (stmt != null)
			{
			    try {  stmt.close();  }
			    catch (SQLException sqlEx)
			    {
			    	System.out.println("ERROR: stmt close in IG getCompData - " + sqlEx.getMessage());
			    }
			    stmt = null;
			}
		}
	}
}
