/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.checkbox.util;

//import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.w3c.dom.Document;
//import org.xml.sax.InputSource;

import com.pdi.properties.PropertyLoader;

/*
 * Class providing webservice utility methods for retrieving data from Checkbox
 */
public class CheckboxWebserviceUtils
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//

	//
	// Constructors.
	//

	//
	// Instance methods.
	//
	
	/*
	 * Sends request to the Cast Iron server for Checkbox data
	 * @param - Checkbox person id
	 * @return A Document object containing the response
	 */
	public Document requestCheckboxData(long rid)
		throws Exception
	{
		String req = PropertyLoader.getProperty("com.pdi.data.checkbox.application", "castIron.url");
		req += "?pid=" + rid;
		//System.out.println("Request string = " + req);
		PostMethod post = new PostMethod(req);

		HttpClient client = new HttpClient();
		int result = client.executeMethod(post);
		if (result != 200)
		{
			throw new Exception("Data not retrieved.  Status = " + result);
		}
	
		DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
		Document doc = docBuilder.parse(post.getResponseBodyAsStream());

		return doc;
	}
}
