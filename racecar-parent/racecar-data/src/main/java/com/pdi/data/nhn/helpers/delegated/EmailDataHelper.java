/**
 * Copyright (c) 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.nhn.helpers.delegated;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.pdi.data.dto.EmailTemplate;
import com.pdi.data.dto.EmailTemplateText;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.IEmailDataHelper;
import com.pdi.data.nhn.util.NhnDatabaseUtils;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.util.RequestStatus;
import com.pdi.email.Email;
import com.pdi.email.EmailServer;
import com.pdi.email.EmailUtils;
import com.pdi.logging.LogWriter;
import com.pdi.properties.PropertyLoader;

/*
 * This class provides e-mail support in the NHN environment
 */
public class EmailDataHelper implements IEmailDataHelper
{
	//
	// Static data
	//
	private static String DEFAULT_LANGUAGE = "en";
	
	// Note the following assumptions:
	//	1) The trailing backslash is not present on the URL in the properties file
	//	2) The lang parameter is the first parm in the parm list
	private static String LANG_PARM = "/?lang=";


	//
	// Static methods.
	//

	//
	// Instance data.
	//
	
	// Property data
	private String server = null;
	private String fromAddr = null;
	private String partExpUrl = null;
	// Reset URL Removed per TLT Standup meeting, 3/9/11
	//private String resetUrl = null;
	
	//
	//
	// Constructors.
	//

	//
	// Instance methods.
	//



	/*
	 * send - Bulk send of e-mails.  Many participants, one project, one email template
	 * 
	 * @param session A SessionUser object
	 * @param emailTemplateId A String containing an e-mail template ID
	 * @param projectId A String containing the relevant project ID
	 * @param recipients An ArrayList of Strings containing recipient IDs
	 *
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IEmailDataHelper#send(com.pdi.data.dto.SessionUser, java.lang.String, java.lang.String, java.util.ArrayList)
	 */
	@Override
	public void send(SessionUser session,
					 String emailTemplateId,
					 String projectId,
					 ArrayList<String> recipients)
	{
		// Email text data - key = language id, value = EmailTemplateText object
		HashMap<String, EmailTemplateText> texts = new HashMap<String, EmailTemplateText>();

		// Get the desired texts and put them into a HashMap
		ArrayList<EmailTemplateText> txtList = getEmailTemplateText(emailTemplateId);
		if (txtList.size() < 1)
		{
			// Nothing to do...
			LogWriter.logBasic(LogWriter.INFO, this, "EmailDataHelper txtList size = 0");
			return;
		}
		for(EmailTemplateText ett: txtList)
		{
			texts.put(ett.getLanguage().getCode(), ett);
		}

		StringBuffer query = new StringBuffer();
		DataLayerPreparedStatement dlps = null;
		DataResult dr = null;
		ResultSet rs = null;
		String phase = null;
		
		try
		{
			// get the static substitution variables
			phase = "Getting data from properties";
			this.server = PropertyLoader.getProperty("com.pdi.data.nhn.application", "tlt.emailServer");
			this.fromAddr = PropertyLoader.getProperty("com.pdi.data.nhn.application", "tlt.email.fromAddress");
			this.partExpUrl =PropertyLoader.getProperty("com.pdi.data.nhn.application", "tlt.email.participantExperienceURL");
			////this.resetUrl = PropertyLoader.getProperty("com.pdi.data.nhn.application", "tlt.email.resetURL");
		
			// Set up the prepared statement for the recipients -- Gets the substitution
			// variables as well as the required data (language and email address).
			phase = "Email recipient fetch (prepare)";
			query.setLength(0);
			query.append("SELECT uu.lang_pref, ");
			query.append("       uu.email, ");
			query.append("       uu.firstname, ");
			query.append("       uu.lastname, ");
			query.append("       uu.username, ");	
			query.append("       uu.password, ");
			query.append("       uu.supervisor_name, ");
			query.append("       uu.supervisor_email, ");
			query.append("       uu.optional_1, ");
			query.append("       uu.optional_2, ");
			query.append("       uu.optional_3, ");
			query.append("       proj.name as projName, ");
			query.append("       cc.company_id, ");
			query.append("       cc.CoName ");
			query.append("  FROM platform.dbo.users uu ");
			query.append("    LEFT JOIN platform.dbo.project proj ON proj.project_id = ? ");
			query.append("    LEFT JOIN platform.dbo.company cc ON cc.company_id = uu.company_id ");
			query.append("  WHERE uu.users_id = ?");

			//LogWriter.logBasic(LogWriter.INFO, this, "EmailDataHelper calling query " + query.toString());
			dlps = NhnDatabaseUtils.prepareStatement(query);
			if (dlps.isInError())
			{
				LogWriter.logSQL(LogWriter.ERROR, this, 
						"Recipient fetch prepare error: " +dlps.toString(), 
						query.toString());
				return;
			}

			// loop through the participant list
			String coId = null;

			LogWriter.logBasic(LogWriter.INFO, this, "EmailDataHelper looping through recipients");
			for (String id : recipients)
			{
				dlps = NhnDatabaseUtils.prepareStatement(query);
				if (dlps.isInError())
				{
					LogWriter.logSQL(LogWriter.ERROR, this, 
							"Recipient fetch prepare error: " +dlps.toString(), 
							query.toString());
					return;
				}
				phase = "Setting ids for " + id;
				dlps.getPreparedStatement().setString(1, projectId);	// project
				dlps.getPreparedStatement().setString(2, id);			// user
				
				phase = "Getting data for users_id " + id;
				Email em = new Email();
				String lang = null;
				EmailTemplateText ett = null;
				try
				{
					dr = NhnDatabaseUtils.select(dlps);
					// Look for a bad result (empty
					if (dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
					{
						LogWriter.logBasic(LogWriter.WARNING, this, "Email loop:  partId " + id + " does not exist.  E-mail send skipped");
						continue;
					}
					if (dr.isInError())
					{
						LogWriter.logSQL(LogWriter.ERROR, this, "Email Recipient fetch error:  " +
								"Status="+ dr.getStatus().getStatusCode() +
								", exceptionCode=" + dr.getStatus().getExceptionCode() +
								", exceptionMsg=" + dr.getStatus().getExceptionMessage(), 
								query.toString());
						return;
					}
	
					// get the single row
					phase = "Users data retrieval (id=" + id + ")";
					rs = dr.getResultSet();
					rs.next();
					lang = rs.getString("lang_pref");
					if (lang == null)
						lang = DEFAULT_LANGUAGE;
					coId = rs.getString("company_id");
	
					// get the email texts
					ett = texts.get(lang);
					if (ett == null)
					{
						LogWriter.logBasic(LogWriter.WARNING, this, 
								"No email texts available in template " + emailTemplateId + " for " + lang + ".  Atemping default");
						// No text gotten.  use the default
						ett = texts.get(DEFAULT_LANGUAGE);
						if (ett == null)
						{
							// No default language either... report it and scram
							LogWriter.logBasic(LogWriter.ERROR, this, 
									"Default language (" + DEFAULT_LANGUAGE + ") not available in email template ID " + emailTemplateId);
							return;
						}
					}
	
					// Set up the email
					em.setFrom(this.fromAddr);
					em.setTo(rs.getString("email"));
					em.setSubject(ett.getSubject());
					em.setMessage(ett.getBody());
					em.setVariables(new HashMap<String, String>());
	
					// Fill in the variables from the db...
					em.getVariables().put(Email.SV_P_FIRST, rs.getString("firstname"));
					em.getVariables().put(Email.SV_P_LAST, rs.getString("lastname"));
					em.getVariables().put(Email.SV_P_EMAIL, rs.getString("email"));
					// Note the username column is lagacy and should be ignored.
					// Rather, use the email address for the User ID
					// NOTE: The above is no longer true.
					em.getVariables().put(Email.SV_P_UID, rs.getString("username"));
					//em.getVariables().put(Email.SV_P_UID, rs.getString("email"));
					em.getVariables().put(Email.SV_P_PW, rs.getString("password"));
					em.getVariables().put(Email.SV_P_OPT1, rs.getString("optional_1"));
					em.getVariables().put(Email.SV_P_OPT2, rs.getString("optional_2"));
					em.getVariables().put(Email.SV_P_OPT3, rs.getString("optional_3"));
					em.getVariables().put(Email.SV_MGR_NAME, rs.getString("supervisor_name"));
					em.getVariables().put(Email.SV_MGR_EMAIL, rs.getString("supervisor_email"));
					em.getVariables().put(Email.SV_PROJECT, rs.getString("projName"));
					em.getVariables().put(Email.SV_CLIENT, rs.getString("CoName"));
	
					// ...and the static ones
					String pUrl = this.partExpUrl + LANG_PARM + lang;
					//em.getVariables().put(Email.SV_PART_URL, this.partExpUrl);
					em.getVariables().put(Email.SV_PART_URL, pUrl);
					// Per Damon on 4/7/11, the IIS side is the only place where this should
					// be used.We will, therefore, leave it as unsubstituted for now.
					////em.getVariables().put(Email.SV_PW_RESET, this.resetUrl);
				}
				finally
				{
					if (dr != null) { dr.close(); dr = null; }
				}
				
				phase = "Send the e-mail";
				boolean emailSent = true;
				EmailServer ems = new EmailServer();
				ems.setAddress(this.server);

				LogWriter.logBasic(LogWriter.INFO, this, "EmailDataHelper calling EmailUtils.send");
				try
				{
					EmailUtils.send(em, ems);
				}
				catch (Exception e)
				{
					emailSent = false;
					LogWriter.logBasic(LogWriter.ERROR, this,
							"Email send failure for partId " + id + "in project " + projectId + 
							 " for template " + emailTemplateId +
							 ":  msg=" + e.getMessage());
				}
				
				if(emailSent)
				{
					setEmailStatus(id, projectId, ett.getEmailConstant(), lang,
							emailTemplateId, coId);
					setEmailLog(id, projectId, ett.getEmailConstant(), lang,
							emailTemplateId, coId);
				}
			}	// End of participant loop
			
			return;
		}
		catch (Exception e)
		{
			LogWriter.logBasicWithException(LogWriter.ERROR, this, 
					"Error in email send() -- step=" + phase + "partid= " +
							"  \n" + e.getMessage(),
					e);
			return;
		}
//		finally
//		{
//			if (dr != null) { dr.close(); dr = null; }
//		}
	}


	/*
	 * setEmailStatus - Write out the latest e-mail send info.
	 * 
	 * @param userId A String containing the relevant user ID
	 * @param projectId A String containing the relevant project ID
	 * @param emailConstant A String containing the relevant email constant
	 * @param langCode A String containing the language code used to send the email
	 * @param emailTemplateId A String containing an e-mail template ID
	 * @param companyId A String containing the relevant company ID
	 */
	private void setEmailStatus(String userId,
								String projectId,
								String emailConstant,
								String langCode,
								String emailTemplateId,
								String companyId)
	{
		StringBuffer q = new StringBuffer();
		ResultSet rs = null;
		int statId = 0;
		
		// See if the row exists.
		q.append("SELECT ses.email_status_id ");
		q.append("  FROM platform.dbo.ScheduledEmailStatus ses ");
		q.append("  WHERE ses.users_id = " + userId + " ");
		q.append("    AND ses.project_id = " + projectId + " ");
		q.append("    AND ses.email_constant = '" + emailConstant + "'");
		
		DataLayerPreparedStatement chkStat = NhnDatabaseUtils.prepareStatement(q);
		if (chkStat.isInError())
		{
			LogWriter.logSQL(LogWriter.ERROR, this, 
					"setEmailStatus - Fetch prep error: " + chkStat.toString(), 
					q.toString());
			return;
		}

		boolean doInsert = false;
		DataResult chkDr = null;
		try
		{
			chkDr = NhnDatabaseUtils.select(chkStat);

			if (chkDr.hasNoData())
			{
				// do an insert
				doInsert = true;
			}
			else if (chkDr.isInError())
			{
				LogWriter.logSQL(LogWriter.ERROR, this, 
						"setEmailStatus - Fetch error: " + chkStat.toString(), 
						q.toString());
				return;
			}
			else
			{
				//get the ID data
				rs = chkDr.getResultSet();
				try
				{
				rs.next();
				statId = rs.getInt("email_status_id");
				}
				catch (SQLException e)
				{
					LogWriter.logBasicWithException(LogWriter.ERROR, this,
							"setEmailStatus - ResultSet fetch error: " + 
											userId + "/" + projectId + "/" + emailConstant, 
							e);
					return;
				}
			}
		}
		finally
		{
			if (chkDr != null) { chkDr.close(); chkDr = null; }
		}

		if (doInsert)
		{
			q.setLength(0);
			q.append("INSERT INTO platform.dbo.ScheduledEmailStatus ");
			q.append("  (users_id, project_id, email_constant, " +
					"language_code, email_id, company_id, date_completed) ");
			q.append("  VALUES(" + userId + ", " + projectId + ", '" + emailConstant + "', " + 
					"'" + langCode + "', " + emailTemplateId + ", " + companyId + ", GETDATE())");

			DataLayerPreparedStatement insPs = NhnDatabaseUtils.prepareStatement(q);
			if (insPs.isInError())
			{
				LogWriter.logSQL(LogWriter.ERROR, this, 
						"setEmailStatus - Insert prep error: " + insPs.toString(), 
						q.toString());
				return;
			}

			DataResult insDr = null;
			try
			{
				insDr = NhnDatabaseUtils.insert(insPs);
				if (insDr.isInError())
				{
					LogWriter.logSQL(LogWriter.ERROR, this, "setEmailStatus - Insert error:  " +
							"Status="+ insDr.getStatus().getStatusCode() +
							", exceptionCode=" + insDr.getStatus().getExceptionCode() +
							", exceptionMsg=" + insDr.getStatus().getExceptionMessage(), 
							q.toString());
					return;
				}
			}
			finally
			{
				if (insDr != null) { insDr.close(); insDr = null; }
			}

			// "Normal" exit
			return;
		}	// End of if(doInsert)
		
		// If here, we are doing an update
		q.setLength(0);
		q.append("UPDATE platform.dbo.ScheduledEmailStatus ");
		q.append("  SET date_completed =  GETDATE() ");
		q.append("  WHERE email_status_id = '" + statId + "'");

		DataLayerPreparedStatement updtPs = NhnDatabaseUtils.prepareStatement(q);
		if (updtPs.isInError())
		{
			LogWriter.logSQL(LogWriter.ERROR, this, 
					"setEmailStatus - Update prep error: " + updtPs.toString(), 
					q.toString());
			return;
		}

		DataResult updtDr = null;
		try
		{
			updtDr = NhnDatabaseUtils.update(updtPs);
			if (updtDr.isInError())
			{
				LogWriter.logSQL(LogWriter.ERROR, this, "setEmailStatus - Update error:  " +
						"Status="+ updtDr.getStatus().getStatusCode() +
						", exceptionCode=" + updtDr.getStatus().getExceptionCode() +
						", exceptionMsg=" + updtDr.getStatus().getExceptionMessage(), 
						q.toString());
				return;
			}
		}
		finally
		{
			if (updtDr != null) { updtDr.close(); updtDr = null; }
		}

		// "Normal" exit
		return;
	}


	/*
	 * setEmailLog - Write out a record of an email send.
	 * 
	 * @param userId A String containing the relevant user ID
	 * @param projectId A String containing the relevant project ID
	 * @param emailConstant A String containing the relevant email constant
	 * @param langCode A String containing the language code used to send the email
	 * @param emailTemplateId A String containing an e-mail template ID
	 * @param companyId A String containing the relevant company ID
	 */
	private void setEmailLog(String userId,
							 String projectId,
							 String emailConstant,
							 String langCode,
							 String emailTemplateId,
							 String companyId)
	{
		StringBuffer q = new StringBuffer();
		
		q.append("INSERT INTO platform.dbo.ScheduledEmailLog ");
		q.append("  (users_id, project_id, email_constant, " +
					"language_code, email_id, company_id, date_completed) ");
		q.append("  VALUES(" + userId + ", " + projectId + ", '" + emailConstant + "', " + 
					"'" + langCode + "', " + emailTemplateId + ", " + companyId + ", GETDATE())");

		DataLayerPreparedStatement insPs = NhnDatabaseUtils.prepareStatement(q);
		if (insPs.isInError())
		{
			LogWriter.logSQL(LogWriter.ERROR, this, 
					"setEmailLog - Insert prep error: " + insPs.toString(), 
					q.toString());
			return;
		}

		DataResult insDr = null;
		try
		{
			insDr = NhnDatabaseUtils.insert(insPs);
			if (insDr.isInError())
			{
				LogWriter.logSQL(LogWriter.ERROR, this, "setEmailLog - Insert error:  " +
						"Status="+ insDr.getStatus().getStatusCode() +
						", exceptionCode=" + insDr.getStatus().getExceptionCode() +
						", exceptionMsg=" + insDr.getStatus().getExceptionMessage(), 
						q.toString());
				return;
			}
		}
		finally
		{
			if (insDr != null) { insDr.close(); insDr = null; }
		}

		// "Normal" exit
		return;
	}


	/*
	 * NOTE:  THIS METHOD IS FOR TESTING ONLY!
	 * 
	 * testEmailContent - Sends an email for each language available in
	 * the template.  All substitution variables (except the email
	 * address) are fake data.
	 * 
	 * @param emailTemplateId A String containing an e-mail template ID
	 * @param emailAddr A String containing an email address
	 * @return A boolean success/failure
	 */
    @Override
	public boolean testEmailContent(String emailTemplateId, String emailAddr)
	{
		// Email text data - key = language id, value = EmailTemplateText object
		HashMap<String, EmailTemplateText> texts = new HashMap<String, EmailTemplateText>();

		// Get the desired texts and put them into a HashMap
		ArrayList<EmailTemplateText> txtList = getEmailTemplateText(emailTemplateId);
		if (txtList.size() < 1)
		{
			// No texts available - nothing to do...
			return false;
		}
		for(EmailTemplateText ett: txtList)
		{
			texts.put(ett.getLanguage().getCode(), ett);
		}

		// Get real info from properties
		this.server = PropertyLoader.getProperty("com.pdi.data.nhn.application", "tlt.emailServer");

		// Set up bogus substitution variables (except for email)
		HashMap<String, String> subs = new HashMap<String, String>();
		subs.put(Email.SV_P_FIRST, "TestPartFirstName");
		subs.put(Email.SV_P_LAST, "TestPartLastName");
		subs.put(Email.SV_P_EMAIL, emailAddr);
		subs.put(Email.SV_P_UID, "TestPartId");
		subs.put(Email.SV_P_PW, "TestPartPassword");
		subs.put(Email.SV_P_OPT1, "TestPartOpt1");
		subs.put(Email.SV_P_OPT2, "TestPartOpt2");
		subs.put(Email.SV_P_OPT3, "TestPartOpt3");
		subs.put(Email.SV_MGR_NAME, "TestMgrName");
		subs.put(Email.SV_MGR_EMAIL, "TestMgrEmail@bogus.com");
		subs.put(Email.SV_PROJECT, "TestProjectName");
		subs.put(Email.SV_CLIENT, "TestClientName");
		subs.put(Email.SV_PART_URL, "www.TestPartUrl.com/testing");

		// loop through the texts in the template
		for (Iterator<Map.Entry<String, EmailTemplateText>> itr=texts.entrySet().iterator(); itr.hasNext();  )
		{
			Map.Entry<String, EmailTemplateText> ent = itr.next();
			String langId = ent.getKey();
			EmailTemplateText ett = ent.getValue();
			String subTxt = "Test " + langId + "-->" + ett.getSubject();

			// Set up the email
			Email em = new Email();
			em.setFrom(emailAddr);
			em.setTo(emailAddr);
			em.setSubject(subTxt);
			em.setMessage(ett.getBody());
			em.setVariables(subs);

			EmailServer ems = new EmailServer();
			ems.setAddress(this.server);
			try
			{
				EmailUtils.send(em, ems);
			}
			catch (Exception e)
			{
				LogWriter.logBasicWithException(LogWriter.ERROR, this,
						"Email send failure for langId " + langId + 
						 " for template " + emailTemplateId,
						 e);
				return false;
			}
		}	// End of text loop
		
		// Note that no logging is done here
		
		// Made it to here... assume OK
		return true;
	}

	
	

	@Override
	public ArrayList<EmailTemplate> fromClientId(SessionUser su, String clientId) {
		ArrayList<EmailTemplate> templates = new ArrayList<EmailTemplate>();

		//select * from ScheduledEmailDef sd where sd.id in (select email_id from ScheduledEmailCompany where company_id = 751) or isTemplate = 1
		
		StringBuffer query = new StringBuffer();
		DataLayerPreparedStatement dlps = null;
		DataResult dr = null;
		ResultSet rs = null;
		
		try
		{
			query.setLength(0);
			query.append("select * from platform.dbo.ScheduledEmailDef sd where active = 1 and sd.emailType != 'SYS' and (sd.id in (select email_id from platform.dbo.ScheduledEmailCompany where company_id = ?) or isTemplate = 1) order by isTemplate, constant, Title");
			
			dlps = NhnDatabaseUtils.prepareStatement(query);
			dlps.getPreparedStatement().setLong(1, Long.parseLong(clientId));	// project
			
			if (dlps.isInError())
			{
				throw new Exception(dlps.toString());
			}
			dr = NhnDatabaseUtils.select(dlps);;
			if (dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
			{
				
				return templates;
			}
			if (dr.isInError())
			{
				throw new Exception("Status="+ dr.getStatus().getStatusCode() +
					", exceptionCode=" + dr.getStatus().getExceptionCode() +
					", exceptionMsg=" + dr.getStatus().getExceptionMessage());
			}

			// Save all of the texts
			rs = dr.getResultSet();
			while(rs.next())
			{
				EmailTemplate et = new EmailTemplate();
				et.setId(rs.getLong("id"));
				et.setCode(rs.getString("constant"));
				et.setName(rs.getString("title"));
				templates.add(et);
			}
		}
		catch (Exception e)
		{
			LogWriter.logSQLWithException(LogWriter.ERROR, this,
					"fromClientId - Fetching email text defs.  Client id " + clientId,
					query.toString(), e);
		}
		finally
		{
//			if (rs != null)
//			{
//				try { rs.close(); }
//				catch (Exception e) { System.out.println("Error closing email ResultSet.  " +
//										e.getMessage()); }
//			}
			if (dr != null) { dr.close(); dr = null; }
		}
		
		return templates;
	}

	
	private ArrayList<EmailTemplateText> getEmailTemplateText(String emailTemplateId)
	{
		ArrayList<EmailTemplateText> texts = new ArrayList<EmailTemplateText>();
		

		//select * from ScheduledEmailDef sd where sd.id in (select email_id from ScheduledEmailCompany where company_id = 751) or isTemplate = 1
		
		StringBuffer query = new StringBuffer();
		DataLayerPreparedStatement dlps = null;
		DataResult dr = null;
		ResultSet rs = null;
		
		try
		{
			query.setLength(0);
			//query.append("select * from platform.dbo.ScheduledEmailText sd where sd.email_id = ?");
			query.append("SELECT et.lang, ");
			query.append("       ed.constant, ");
			query.append("       et.subject, ");
			query.append("       et.text ");
			query.append("  FROM platform.dbo.ScheduledEmailText et ");
			query.append("    INNER JOIN platform.dbo.ScheduledEmailDef ed ON ed.id = et.email_id ");
			query.append("  WHERE et.email_id = ?");
			
			dlps = NhnDatabaseUtils.prepareStatement(query);
			dlps.getPreparedStatement().setLong(1, Long.parseLong(emailTemplateId));	// project
			
			if (dlps.isInError())
			{
				throw new Exception(dlps.toString());
			}
			dr = NhnDatabaseUtils.select(dlps);;
			if (dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
			{
				return texts;
			}
			if (dr.isInError())
			{
				throw new Exception("Status="+ dr.getStatus().getStatusCode() +
					", exceptionCode=" + dr.getStatus().getExceptionCode() +
					", exceptionMsg=" + dr.getStatus().getExceptionMessage());
			}

			// Save all of the texts
			rs = dr.getResultSet();
			while(rs.next())
			{
				EmailTemplateText et = new EmailTemplateText(HelperDelegate.getLanguageHelper("com.pdi.data.nhn.helpers.delegated").fromCode(null, rs.getString("lang")),
															 rs.getString("constant"),
															 rs.getString("subject"),
															 rs.getString("text"));
				texts.add(et);
			}
			
			return texts;
		}
		catch (Exception e)
		{
			LogWriter.logSQLWithException(LogWriter.ERROR, this,
					"getEmailTemplateText - Fetching email texts for id " + emailTemplateId, query.toString(), e);
			return null;
		}
		finally
		{
//			if (rs != null)
//			{
//				try { rs.close(); }
//				catch (Exception e) { LogWriter.logBasicWithException(LogWriter.WARNING, this,
//						"getEmailTemplateText - Closing ResultSet", e); }
//			}
			if (dr != null) { dr.close(); dr = null; }
		}
	}

	@Override
	public EmailTemplate fromId(SessionUser su, String emailTemplateId) {
		EmailTemplate et = new EmailTemplate();

		//select * from ScheduledEmailDef sd where sd.id in (select email_id from ScheduledEmailCompany where company_id = 751) or isTemplate = 1
		
		StringBuffer query = new StringBuffer();
		DataLayerPreparedStatement dlps = null;
		DataResult dr = null;
		ResultSet rs = null;
		
		try
		{
			query.setLength(0);
			query.append("select * from platform.dbo.ScheduledEmailDef sd where sd.id = ?");
			
			dlps = NhnDatabaseUtils.prepareStatement(query);
			dlps.getPreparedStatement().setLong(1, Long.parseLong(emailTemplateId));	// project
			
			if (dlps.isInError())
			{
				throw new Exception(dlps.toString());
			}
			dr = NhnDatabaseUtils.select(dlps);;
			if (dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
			{
				
				return et;
			}
			if (dr.isInError())
			{
				throw new Exception("Status="+ dr.getStatus().getStatusCode() +
					", exceptionCode=" + dr.getStatus().getExceptionCode() +
					", exceptionMsg=" + dr.getStatus().getExceptionMessage());
			}

			// Save all of the texts
			rs = dr.getResultSet();
			while(rs.next())
			{
				et.setId(rs.getLong("id"));
				et.setCode(rs.getString("constant"));
				et.setName(rs.getString("title"));
				et.setEmailTemplateTexts(getEmailTemplateText(emailTemplateId));
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
//			if (rs != null)
//			{
//				try { rs.close(); }
//				catch (Exception e) { System.out.println("Error closing email ResultSet.  " +
//										e.getMessage()); }
//			}
			if (dr != null) { dr.close(); dr = null; }
		}
		
		return et;
	}
}
