/**
 * Copyright (c) 2008 Personnel Decisions International, Inc.
 * All rights reserved.
 *
 * $Header: com/pdi/data/abyd/dto/setup/Setup2ReportLabelDTO.java 6     8/25/05 3:23p mpanichi $
 */
package com.pdi.data.abyd.dto.setup;


/**
 * Setup2ReportLabelDTO is a wrapper around a number of data items that are needed when 
 * part 2 of the Design Setup app is initially started.
 * 
 * @author		Ken Beukelman
 * @author      MB Panichi
 */
public class Setup2ReportLabelValueDTO
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//

	private long _reportLabelId = 0;
	private String _reportCode = "";
	private String _label = "";
	private String _customLabel = "";
	private int _sequence = 0;

	
	
	//
	// Constructors.
	//
	public Setup2ReportLabelValueDTO()
	{
		// Does nothing presently

		
	}

	//
	// Instance methods.
	//
	
	public String toString()
	{
		String ret = "  Setup2ReportLabelValueDTO:  label ID=" + _reportLabelId +
		                                             ", code=" + _reportCode +
		                                             ",  seq=" + _sequence + "\n";
		ret += "    label=" + _label + "\n";
		ret += "    cust label=" + _customLabel;

		
		return ret;
	}
	
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public long getReportLabelId()
	{
		return _reportLabelId;
	}
		
	public void setReportLabelId(long value)
	{
		_reportLabelId = value;
	}
	
	/*****************************************************************************************/
	public String getReportCode()
	{
		return _reportCode;
	}
		
	public void setReportCode(String value)
	{
		_reportCode = value;
	}	

	/*****************************************************************************************/
	public String getLabel()
	{
		return _label;
	}
		
	public void setLabel(String value)
	{
		_label = value;
	}	

	/*****************************************************************************************/
	public String getCustomLabel()
	{
		return _customLabel;
	}
		
	public void setCustomLabel(String value)
	{
		_customLabel = value;
	}
	
	/*****************************************************************************************/
	public int getSequence()
	{
		return _sequence;
	}
		
	public void setSequence(int value)
	{
		_sequence = value;
	}





}