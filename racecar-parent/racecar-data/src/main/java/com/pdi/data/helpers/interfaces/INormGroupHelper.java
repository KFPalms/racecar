package com.pdi.data.helpers.interfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import com.pdi.data.dto.NormGroup;
import com.pdi.data.dto.SessionUser;

public interface INormGroupHelper {
	public NormGroup fromId(SessionUser session, int normGroupId);

	public NormGroup fromName(SessionUser session, String normGroupName);

	public ArrayList<NormGroup> fromInstrumentId(SessionUser session, String instrumentId);

	public ArrayList<NormGroup> fromInstrumentIdOrdered(SessionUser session, String instrumentId);

	// public ArrayList<NormGroup> fromParticipantInstrumentId(SessionUser
	// session, String participantId, String instrumentId);
	public void update(SessionUser session, NormGroup ng, String engagementId, String participantId,
			String instrumentCode);

	public ArrayList<NormGroup> fromParticipantInstrumentIdProjectId(SessionUser session, String participantId,
			String instrumentCode, String projectId);

	public HashMap<String, ArrayList<NormGroup>> getSelectedNormGroups(String pptId, String projId);

	public HashSet<Integer> getDefaultNormGroupSet(String projId);

	public HashMap<String, ArrayList<NormGroup>> getDefaultNormGroupMap(String projId);

	public HashSet<String> getNormableCourseSet();

	public boolean updatePptNormByProjIdNormGroup(String projId, NormGroup ng);

	public int getDefaultNormGroupForInstrument(String instrumentCode, int normType);
}
