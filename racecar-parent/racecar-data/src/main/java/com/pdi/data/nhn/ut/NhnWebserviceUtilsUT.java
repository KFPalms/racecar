package com.pdi.data.nhn.ut;


import java.util.ArrayList;

import com.pdi.ut.UnitTestUtils;
import com.pdi.webservice.NhnWebserviceUtils;

import junit.framework.TestCase;


public class NhnWebserviceUtilsUT  extends TestCase {
	public NhnWebserviceUtilsUT(String name)
	{
		super(name);
	}

	
	public void testFetchCourseListforProject()
	{
		UnitTestUtils.start(this);

		/*
			## webservice URL for course list  
			## http://localhost:8083/PDINHServer-rest/jaxrs/projects/{projectId}/courses
			UNT.webservice.url.nhn=http://localhost:8083/PDINHServer-rest/jaxrs/projects/{projectId}/courses
			LOC.webservice.url.nhn=http://localhost:8083/PDINHServer-rest/jaxrs/projects/{projectId}/courses
			PRE.webservice.url.nhn=http://localhost:8083/PDINHServer-rest/jaxrs/projects/{projectId}/courses
			##PRD.webservice.url.nhn=http://localhost:8083/PDINHServer-rest/jaxrs/projects/{projectId}/courses		
		*/
		
		String path = "PDINHServer-rest/jaxrs/projects/{projectId}/courses";
		String projectId = "1024";
		path = path.replace("{projectId}", projectId);
		ArrayList<String> courseList = new ArrayList<String>();
	
//		courseList = new NhnWebserviceUtils().fetchCourseListForProject(path, projectId);
//		for(String s : courseList){
//			System.out.println("course abbv(1):  " + s);
//		}
		
		courseList = new NhnWebserviceUtils().getFilteredProjectCourseList(projectId);
		for(String s : courseList){
			System.out.println("Filtered course abbv(1):  " + s);
			
		}
		
		courseList = new NhnWebserviceUtils().getUnfilteredProjectCourseList(projectId);
		for(String s : courseList){
			System.out.println("Unfiltered course abbv(1):  " + s);
			
		}
		
		
		UnitTestUtils.stop(this);
	}
}

