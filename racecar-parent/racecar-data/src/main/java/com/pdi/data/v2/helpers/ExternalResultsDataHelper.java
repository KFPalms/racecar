/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v2.helpers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;
import com.pdi.xml.XMLUtils;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.util.RequestStatus;
import com.pdi.data.v2.util.Utils;
import com.pdi.data.v2.util.V2DatabaseUtils;

/*
 * Stores external data to the V2 database
 */
public class ExternalResultsDataHelper
{
	
	//
	// Static data.
	//
	
	// Results table XML wrappers
	 private static String XML_HDR = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	 private static String R_XML_LEADR = "<XML_Fields><CScore.S0>";
	 private static String R_XML_TRLR = "</CScore.S0></XML_Fields>";
	 private static String A_XML_LEADR = "<XML_Fields><Answers>";
	 private static String A_XML_TRLR = "</Answers></XML_Fields>";
	 private static String C_XML = "<XML_Fields><Answers></Answers></XML_Fields>";
	
	// ReportingConstants code to moduleId cross reference
	private static HashMap<String, String> RC_MOD_MAP = new HashMap<String, String>();
	{
		//RC_MOD_MAP.put(ReportingConstants.RC_CAREER_SURVEY, "IVUVHYKM");	// The V2 CS (US English) module id
		RC_MOD_MAP.put(ReportingConstants.RC_CAREER_SURVEY, "FFGLQDEG");	// The V2 CS (the Checkpoint place-holder) module id
		RC_MOD_MAP.put(ReportingConstants.RC_CHQ, "LIBPMLBJ");	// The V2 CHQ (A by D) module id
		RC_MOD_MAP.put(ReportingConstants.RC_GPI, "IXLGVIOQ");	// The V2 GPI (US English) module id
		RC_MOD_MAP.put(ReportingConstants.RC_LEI, "BMZLGGZL");	// The V2 LEI (US English) module id
		RC_MOD_MAP.put(ReportingConstants.RC_FIN_EX, "FAGNDFAC");	// The V2 FE (A by D) module id
	}

	//
	// Static methods.
	//

	//
	// Constructors.
	//

	//
	//  Instance data
	//
	
	// Holder for construct/SPSS mapping data
	private  HashMap<String, String> constructMap = null;
	// Holder for job that the construct map is good for
	private  String constructJobId = "";

	
	//
	//  Instance methods.
	//
	
	/**
	 * Method to store ADAPT score information in the V2 database.  Note that no
	 * validation of the data is performed in this method.
	 * @param job - V2 job id
	 * @param cand - V2 candidate id
	 * @param ir - An IndividualReport object with the data to store
	 */
	 public ArrayList<String> saveAdaptData(String jobId,
			 							String candId,
			 							ArrayList<String> selModList,
			 							IndividualReport ir)
	{
		//System.out.println("saveAdaptData.....");
		 ArrayList<String> ret = new ArrayList<String>();
		
		if (candId == null || candId.length() < 1)
		{
			System.out.println("saveAdaptData - A V2 candidate id is required to insert external data into V2");
			return ret;
		}
		if (jobId == null || jobId.length() < 1)
		{
			System.out.println("saveAdaptData - A V2 job id is required to insert external data into V2");
			return ret;
		}
		if (selModList == null)
		{
			System.out.println("saveAdaptData - A list of \"selected\" module ids is required to insert external data into V2");
			return ret;
		}
		if (ir == null)
		{
			System.out.println("saveAdaptData - An IndividualReport object is required to insert external data into V2");
			return ret;
		}
		
		if (ir.getReportData().isEmpty())
		{
			// Nothing to do.  Return with OK status.
			return ret;
		}

		for (Iterator<String> itr = ir.getReportData().keySet().iterator(); itr.hasNext(); )
		{
			RequestStatus rs;
			String key = itr.next();
			ReportData rd = ir.getReportData().get(key);
			//System.out.println("Data type - " + key);
			
			if (! selModList.contains(key))
			{
				// Don't wanna save the data
				//System.out.println("Don't save data for " + key);
				continue;
			}

			
			if (key.equals(ReportingConstants.RC_GPI) ||
				key.equals(ReportingConstants.RC_LEI) ||
				key.equals(ReportingConstants.RC_FIN_EX))
			{
				if (rd.getScoreData().isEmpty())
				{
					// No data... nothing to do
					continue;
				}

				// Save the data to the results table
				//System.out.println("Save results for " + key);
				rs = storeResults(key, jobId, candId, rd.getScoreData());
				//System.out.println("Results saved");
			}
			else if (key.equals(ReportingConstants.RC_CHQ))
			{
				if (rd.getRawData().isEmpty())
				{
					// No data... nothing to do
					continue;
				}
				// Save the data to the answers table
				//System.out.println("Save answers for " + key);
				rs = storeAnswers(key, jobId, candId, rd.getRawData());
				//System.out.println("Answers saved");
			}
			else
			{
				// "Invalid" key - not recognized here at this point
				System.out.println("Unrecognized instrument key - " + key);
				continue;
			}
			if (rs.isOk())
			{
				ret.add(key);
			}
			else
			{
				System.out.println("Error moving ADAPT data for candidate " + candId +
								   " in job " + jobId +
								   ".  msg=" + rs.getExceptionMessage());
			}
		}
		
		return ret;
	}


	/**
	 * Method to store score values into the V2 results table.
	 * 
	 * Note that this method does no data checking.  If the row for
	 * the job/canidiate/construct already exists, the score data
	 * is not preserved, but is overwritten by the external score
	 * information - no norms or norm scores are preserved.
	 * 
	 * @param key A HashMap containing the score data.  Passed through
	 *            to the routine that sets the "Completed" flag.
	 * @param jobId The V2 job ID
	 * @param candId The V2 candidate ID
	 * @param data a HashMap containing the score data
	 */
	 private RequestStatus storeResults(String key, String jobId, String candId, HashMap<String, String> data)
	{
		//Get the valid construct ids for this project, if necessary
		if (constructMap == null || !jobId.equals(constructJobId))
		{
			RequestStatus rs = fillConstructMap(jobId);
			if (! rs.isOk())
			{
				return rs;
			}
		}
		////Debug
		//for(Iterator<Map.Entry<String, String>> itr=constructMap.entrySet().iterator(); itr.hasNext(); )
		//{
		//	Map.Entry<String, String> ent = itr.next();
		//	System.out.println(ent.getKey() + "=" + ent.getValue());
		//}
		
		//loop through the available constructs
		for (Iterator<Map.Entry<String, String>> itr=data.entrySet().iterator(); itr.hasNext(); )
		{
			String spss;
			String construct;
			String score;
			
			// Get one entry and the construct associated with it
			Map.Entry<String, String> ent = itr.next();
			spss = ent.getKey();
			construct = constructMap.get(spss);
			if (construct == null)
			{
				// Don't put it in
				continue;
			}
			score = ent.getValue();
			
			// Set up the query.
			// It's inside the loop because the statement gets closed on execution.
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT rr.UniqueIdStamp ");
			sqlQuery.append("  FROM results rr ");
			sqlQuery.append("  WHERE rr.JobId = ? ");
			sqlQuery.append("    AND rr.CandidateId = ? ");
			sqlQuery.append("    AND rr.ConstructId = ?");

			DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
			if (dlps.isInError())
			{
				return dlps.getStatus();
			}

			DataResult dr = null;
			ResultSet rs = null;
			RequestStatus stat;

			try
			{
				// Fill the sql parameter and get the data
				dlps.getPreparedStatement().setString(1, jobId);
				dlps.getPreparedStatement().setString(2, candId);
				dlps.getPreparedStatement().setString(3, construct);

				dr = V2DatabaseUtils.select(dlps);
				if (dr.isInError())
				{
					if (dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
					{
						//insert
						stat = insertScore(construct, jobId, candId, score);
						if (! stat.isOk())
							return stat;
					}
					else
					{
						return dr.getStatus();
					}
				}
				else
				{
					// Get the unique id then update that row
					rs = dr.getResultSet();
					rs.next();
					String uid = rs.getString("UniqueIdStamp");
					stat = updateScore(uid, construct, score);
					if (! stat.isOk())
						return stat;
				}
				
				// OK, now insert/update an answers table row to set the "Completed" flag
				stat = setCompletedFlag(key, jobId, candId);
				if (! stat.isOk())
					return stat;
			}
			catch (SQLException ex)
			{
				return new RequestStatus(RequestStatus.RS_ERROR, ex.getErrorCode() + " - " + ex.getMessage());
			}
			finally
			{
				if (dr != null) { dr.close(); dr = null; }
			}
		}
		
		// If here, we finished the loop with no errors
		return new RequestStatus();	// Default status is OK	
	}


	/**
	 * Create the construct/SPSS map for this project.
	 * The query joins the jobs, job_mod, and pdi_construct_spss_value
	 * tables to come up with a list of valid SPSS keys with their
	 * associated construct ids valid for this project.  Only those
	 * keys in the construct map that are valid for the modules
	 * selected for this project are present.
	 * 
	 * @param jobId The job id 
	 * @return A RequestStatus object
	 */
	 private RequestStatus fillConstructMap(String jobId)
	{
		constructJobId = new String(jobId);
		constructMap = new HashMap<String, String>();
		
		// Set up the query.
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT cc.UniqueIdStamp as constructId, ");
		sqlQuery.append("       csv.spssValue ");
		sqlQuery.append("FROM job_mod jm, ");
		sqlQuery.append("     constructs cc, ");
		sqlQuery.append("     pdi_construct_spss_value csv ");
		sqlQuery.append("  WHERE jm.JobID = ? ");
		sqlQuery.append("    AND cc.ModuleId = jm.ModuleID ");
		sqlQuery.append("    AND csv.constructId = cc.UniqueIdStamp");

		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			return dlps.getStatus();
		}

		DataResult dr = null;
		ResultSet rs = null;
		String curError = null;

		try
		{
			// Fill the sql parmeter and get the data
			curError = "Unable to set job id in construct map setup";
			dlps.getPreparedStatement().setString(1, jobId);

			
			dr = V2DatabaseUtils.select(dlps);
			if (dr.isInError())
			{
				if (dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
				{
					dr.getStatus().setExceptionMessage("No construct/SPSS data available for job " + jobId);
				}
				return dr.getStatus();
			}
			rs = dr.getResultSet();

			// process the returned data
			curError = "isBeforeFirst failed";
			if (! rs.isBeforeFirst())
			{
				// No data... return
				return new RequestStatus(RequestStatus.DBS_NO_DATA,"No valid constructs were found for job " + jobId);
			}

			curError = "Error fetching data from ResultSet";
			while (rs.next())
			{
				constructMap.put(rs.getString("spssValue"), rs.getString("constructId"));
			}

			return new RequestStatus();	// all is OK
		}
		catch (SQLException ex)
		{
			return new RequestStatus(RequestStatus.RS_ERROR, curError);
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}	// End fillConstructMap method


	/**
	 * Insert a score line into the results table
	 * 
	 * @param conId - Construct id
	 * @param jobId A V2 job id
	 * @param candId A V2 candidate id
	 * @param score - The score to save
	 * @return A RequestStatus object
	 */
	 private RequestStatus insertScore(String conId, String jobId, String candId, String score)
	{
		// Set up the query.
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("INSERT INTO results ");
		sqlQuery.append("  (UniqueIdStamp, UniqueVsStamp, DateCreatedStamp, DateModifiedStamp, ");
		sqlQuery.append("   CreatedByStamp, ModifiedByStamp, ");
		sqlQuery.append("   CandidateId, JobId, ConstructId, ");
		sqlQuery.append("   CScore, XML) ");
		sqlQuery.append("  VALUES(?, ?, ?, ?, ");
		sqlQuery.append("         'ExtResDH', 'ExtResDH', ");
		sqlQuery.append("         ?, ?, ?, ");
		sqlQuery.append("         ?,?)");

		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			return dlps.getStatus();
		}

		DataResult dr = null;
		String curError = null;
		
		// get ancillary data
		String uid = Utils.getNumericLPKey();
		String dt = Utils.getHRADateString();

		try
		{
			// Fill the SQL parameters
			curError = "Filling insert parameters";
			dlps.getPreparedStatement().setString(1, uid);
			dlps.getPreparedStatement().setString(2, uid);
			dlps.getPreparedStatement().setString(3, dt);
			dlps.getPreparedStatement().setString(4, dt);
			dlps.getPreparedStatement().setString(5, candId);
			dlps.getPreparedStatement().setString(6, jobId);
			dlps.getPreparedStatement().setString(7, conId);
			dlps.getPreparedStatement().setString(8, score);
			dlps.getPreparedStatement().setString(9, XML_HDR + R_XML_LEADR + score.trim() + R_XML_TRLR);

			curError = "Inserting";
			dr = V2DatabaseUtils.insert(dlps);

			return dr.getStatus();
		}
		catch (SQLException ex)
		{
			return new RequestStatus(RequestStatus.RS_ERROR, curError);
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}	// End insertScore method


	/**
	 * Update a score row in the results table
	 * Note that the score XML is completely replaced.
	 * 
	 * @param uid - The unique id of the existing row
	 * @param conId - Construct id
	 * @param score - The score to save
	 * @return A RequestStatus object
	 */
	 private RequestStatus updateScore(String uid, String conId, String score)
	{
		// Set up the query.
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("UPDATE results ");
		sqlQuery.append("  SET DateModifiedStamp = ?, ");
		sqlQuery.append("      ModifiedByStamp = 'ExtResDH', ");
		sqlQuery.append("      CScore = ?, ");
		sqlQuery.append("      XML = ? ");
		sqlQuery.append("  WHERE UniqueIdStamp = ?");

		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			return dlps.getStatus();
		}

		DataResult dr = null;
		String curError = null;
		
		// get ancillary data
		String dt = Utils.getHRADateString();

		try
		{
			// Fill the SQL parameters
			curError = "Filling update parameters";
			dlps.getPreparedStatement().setString(1, dt);
			dlps.getPreparedStatement().setString(2, score);
			dlps.getPreparedStatement().setString(3, XML_HDR + R_XML_LEADR + score.trim() + R_XML_TRLR);
			dlps.getPreparedStatement().setString(4, uid);

			curError = "Updating";
			dr = V2DatabaseUtils.update(dlps);

			return dr.getStatus();
		}
		catch (SQLException ex)
		{
			return new RequestStatus(RequestStatus.RS_ERROR, curError);
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}	// End updateScore method


	/**
	 * Method to set a completed status in the V2 answers table.  Used in
	 * conjunction with the store/update results to store a "completed"
	 * status in an answers table row.  Note that no data checking is
	 * performed; information existent in the row updated is overwritten.
	 * 
	 * @param mod_code - The module type for the score data set being completed
	 * @param jobId A V2 job id
	 * @param candId A V2 candidate id
	 * @return A RequestStatus object
	 */
	public  RequestStatus setCompletedFlag(String mod_code, String jobId, String candId)
	{
		if (mod_code.isEmpty())
		{
			return new RequestStatus(RequestStatus.RS_ERROR, "Module code, candidate id and job id are all required to set the \"completed\" flag.");
		}
//
//		// get the appropriate module code
//		String module = RC_MOD_MAP.get(mod_code);
//		if(module == null)
//		{
//			return new RequestStatus(RequestStatus.RS_ERROR, "No V2 module mapping available for " + mod_code);
//		}
			
		// Generate a bogus XML
		String xml = XML_HDR + C_XML;

		return answerWrite(mod_code, jobId, candId, xml);
	}
	
	
	/*
	 * Method to store response values in the V2 answers table.
	 * Note that this method does no data checking.
	 * 
	 * @param mod_code - The ReportingConstants module id for this module
	 * @param jobId A V2 job id
	 * @param candId A V2 candidate id
	 * @param data a HashMap containing the raw data
	 * @return a RequestStatus object
	 */
	 private RequestStatus storeAnswers(String mod_code, String jobId, String candId, HashMap<String, String> data)
	{
		if (data.isEmpty())
		{
			// Nothing to do
			return new RequestStatus();
		}
		
		// Generate a sorted Map
		// We may not NEED to do this but it makes it easier to view the results.
		// If this is deemed totally useless, remove these few lines and the
		// comparator at the bottom of this module.
		TreeMap<String, String> tm = new TreeMap<String, String>(new TreeComp());
		for(Iterator<Map.Entry<String, String>> itr=data.entrySet().iterator(); itr.hasNext(); )
		{
			Map.Entry<String, String> ent = itr.next();
			tm.put(ent.getKey(), ent.getValue());
		}
		
		// Generate the XML
		String xml = XML_HDR + A_XML_LEADR;
		
		for(Iterator<Map.Entry<String, String>> itr=tm.entrySet().iterator(); itr.hasNext(); )
		{
			Map.Entry<String, String> ent = itr.next();
			if (ent.getValue().trim().isEmpty())
			{
				continue;
			}
			
			xml += "<A N=\"" + ent.getKey() + "\">" + XMLUtils.xmlFormatString(ent.getValue()) + "</A>";
		}
		xml += A_XML_TRLR;
		//System.out.println("XML=" + xml);

		return answerWrite(mod_code, jobId, candId, xml);
	}
	
	
	/*
	 * Method to store data into the V2 answers table.  It is used
	 * both to store real answers (e.g., the CHQ) or to store the
	 * completed flag when writing scores to the results table
	 * (the answer XML is bogus in that case).  Note that this
	 * method does no data checking.
	 * 
	 * @param rcMod - The ReportingConstants module id for this module
	 * @param jobId A V2 job id
	 * @param candId A V2 candidate id
	 * @param xml The answer XML to write
	 * @return A RequestStatus object
	 */
	private  RequestStatus answerWrite(String rcMod, String jobId, String candId, String xml)
	{
		// get the appropriate module code
		String module = RC_MOD_MAP.get(rcMod);
		if(module == null)
		{
			return new RequestStatus(RequestStatus.RS_ERROR, "No V2 module mapping available for " + rcMod);
		}

		// Do we update or insert?  Set up the query to check.
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT aa.UniqueIdStamp ");
		sqlQuery.append("  FROM answers aa ");
		sqlQuery.append("  WHERE aa.JobID = ? ");
		sqlQuery.append("    AND aa.CandidateID = ? ");
		sqlQuery.append("    AND aa.ModuleID = ?");

		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			return dlps.getStatus();
		}

		DataResult dr = null;
		RequestStatus stat;

		try
		{
			// Fill the sql parameter and get the data
			dlps.getPreparedStatement().setString(1, jobId);
			dlps.getPreparedStatement().setString(2, candId);
			dlps.getPreparedStatement().setString(3, module);

			dr = V2DatabaseUtils.select(dlps);
			if (dr.isInError())
			{
				if (dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
				{
					//insert
					stat = insertAnswers(module, jobId, candId, xml);
					if (! stat.isOk())
						return stat;
				}
				else
				{
					return dr.getStatus();
				}
			}
			else
			{
				// Get the unique id then update that row
				ResultSet rs = dr.getResultSet();
				rs.next();
				String uid = rs.getString("UniqueIdStamp");
				stat = updateAnswer(uid, module, xml);
				if (! stat.isOk())
					return stat;
			}
			
			// If we made it here, must be good
			return new RequestStatus();
		}
		catch (SQLException ex)
		{
			return new RequestStatus(RequestStatus.RS_ERROR, ex.getErrorCode() + " - " + ex.getMessage());
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}


	/**
	 * Insert an answer line into the answers table
	 * 
	 * @param modId - V2 module id
	 * @param jobId - V2 job id
	 * @param candId - V2 candidate id
	 * @param xml - Generated answers xml (special)
	 * @return A RequestStatus object
	 */
	 private RequestStatus insertAnswers(String mod, String jobId, String candId, String xml)
	{
		// Set up the query.
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("INSERT INTO answers ");
		sqlQuery.append("  (UniqueIdStamp, UniqueVsStamp, DateCreatedStamp, DateModifiedStamp, ");
		sqlQuery.append("   CreatedByStamp, ModifiedByStamp, ");
		sqlQuery.append("   JobID, CandidateID, ModuleID, ");
		sqlQuery.append("   Completed, XML) ");
		sqlQuery.append("  VALUES(?, ?, ?, ?, ");
		sqlQuery.append("         'ExtResDH', 'ExtResDH', ");
		sqlQuery.append("         ?, ?, ?, ");
		sqlQuery.append("         1, ?)");

		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			return dlps.getStatus();
		}

		DataResult dr = null;
		String curError = null;

		// get ancillary data
		String uid = Utils.getNumericLPKey();
		String dt = Utils.getHRADateString();

		try
		{
			// Fill the SQL parameters
			curError = "Filling insert parameters (answers table)";
			dlps.getPreparedStatement().setString(1, uid);
			dlps.getPreparedStatement().setString(2, uid);
			dlps.getPreparedStatement().setString(3, dt);
			dlps.getPreparedStatement().setString(4, dt);
			dlps.getPreparedStatement().setString(5, jobId);
			dlps.getPreparedStatement().setString(6, candId);
			dlps.getPreparedStatement().setString(7, mod);

			dlps.getPreparedStatement().setString(8, xml);

			curError = "Inserting (answers table)";
			dr = V2DatabaseUtils.insert(dlps);

			return dr.getStatus(); 
		}
		catch (SQLException ex)
		{
			return new RequestStatus(RequestStatus.RS_ERROR, curError);
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}	// End insertScore method


	/**
	 * Update a row in the answers table
	 * The only items changed are:
	 * -- The answer XML is completely replaced
	 * -- Complete is set to 1
	 * -- Modified date and modified by are changed
	 * 
	 * @param uid - The unique id of the existing row
	 * @param modId - V2 module id
	 * @param xml - The xml to save
	 * @return A RequestStatus object
	 */
	 private RequestStatus updateAnswer(String uid, String modId, String xml)
	{
		// Set up the query.
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("UPDATE answers ");
		sqlQuery.append("  SET DateModifiedStamp = ?, ");
		sqlQuery.append("      ModifiedByStamp = 'ExtResDH', ");
		sqlQuery.append("      Completed = 1, ");
		sqlQuery.append("      XML = ? ");
		sqlQuery.append("  WHERE UniqueIdStamp = ?");

		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			return dlps.getStatus();
		}

		DataResult dr = null;
		String curError = null;
		
		// get ancillary data
		String dt = Utils.getHRADateString();

		try
		{
			// Fill the SQL parameters
			curError = "Filling update parameters (answer table)";
			dlps.getPreparedStatement().setString(1, dt);
			dlps.getPreparedStatement().setString(2, xml);
			dlps.getPreparedStatement().setString(3, uid);

			curError = "Updating";
			dr = V2DatabaseUtils.update(dlps);

			return dr.getStatus();
		}
		catch (SQLException ex)
		{
			return new RequestStatus(RequestStatus.RS_ERROR, curError);
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}	// End updateAnswer method


	/**
	 * Method to store Checkbox score information in the V2 database.  Note
	 * that no validation of the data is performed in this method.
	 * @param job - V2 job id
	 * @param cand - V2 candidate id
	 * @param ir - An IndividualReport object with the data to store
	 */
	 public RequestStatus saveCheckboxData(String jobId, String candId, IndividualReport ir)
	{
		//System.out.println("saveCheckboxData -- jobId : " + jobId + "   candId: " + candId);
		if (candId == null || candId.length() < 1)
		{
			return new RequestStatus(RequestStatus.RS_ERROR,
				"A V2 candidate id is required to insert external data into V2");
		}
		if (jobId == null || jobId.length() < 1)
		{
			return new RequestStatus(RequestStatus.RS_ERROR,
				"A V2 job id is required to insert external data into V2");
		}
		if (ir == null)
		{
			return new RequestStatus(RequestStatus.RS_ERROR,
				"An IndividualReport object is required to insert external data into V2");
		}

		if (ir.getReportData().isEmpty())
		{
			// Nothing to do.  Return with OK status.
			return new RequestStatus();
		}

		ReportData rd = ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY);

		if (rd.getScoreData().isEmpty())
		{
			// No data... nothing to do
			return new RequestStatus();
		}

		// Save the data to the results table
		//System.out.println("Save Checkbox results...");
		return storeResults(ReportingConstants.RC_CAREER_SURVEY, jobId, candId, rd.getScoreData());
	}

	
	//
	// Private  classes
	//
	
	/*
	 * Comparator for answer data.
	 * Sorts data of the form ANN_N numeric ascending.
	 * Note that the trailing construct (_N) need not
	 * be present.
	 * 
	 * This comparator makes the following assumptions:
	 * -- First character is a non-numeric.
	 * -- all other characters are numeric or '_'
	 * -- Max of 2 '_' per string
	 * -- If there are two '_' in a string, the string is constructed so that
	 *    the removal of the second '_' creates a valid number (example: If
	 *    there is a Q36_10_1, then the Q36_09_1 has to have the leading zero
	 *    in the second zone)
	 *    
	 *  If at some point in the future we need to put out answers that have
	 *  a different format for their numbering (e.g., embedded letters, a
	 *  different number format - Q_1_1 - etc.) This comparator will have
	 *  to be rewritten so that it will sort the data in aascending numeric
	 *  fashion (2 follows 1 rather than 10 following 1).
	 */
	private  class TreeComp implements Comparator<String>
	{
		public int compare(String e1, String e2)
		{
			// Strip the leading Q and make the rest a number
			String a1 = e1.substring(1).replace('_', '.').trim();
			if (a1.indexOf('.') != a1.lastIndexOf('.'))
			{
				// parse out the second '.'
				String s1 = a1.substring(0,a1.lastIndexOf('.')) + a1.substring(a1.lastIndexOf('.')+1);
				a1 = s1;
			}
			
			String a2 = e2.substring(1).replace('_', '.').trim();
			if (a2.indexOf('.') != a2.lastIndexOf('.'))
			{
				// parse out the second '.'
				String s2 = a2.substring(0,a2.lastIndexOf('.')) + a2.substring(a2.lastIndexOf('.')+1);
				a2 = s2;
			}
			
			float f1 = Float.parseFloat(a1);
			float f2 = Float.parseFloat(a2);
			
			if (f2 > f1)
				return -1;
			
			if (f1 > f2)
				return 1;
			
			return 0;
		}
	}

}
