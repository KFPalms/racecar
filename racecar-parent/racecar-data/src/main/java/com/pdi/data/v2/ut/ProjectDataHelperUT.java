package com.pdi.data.v2.ut;

//import java.util.ArrayList;

//import javax.xml.xpath.XPath;
//import javax.xml.xpath.XPathConstants;
//import javax.xml.xpath.XPathFactory;

//import org.w3c.dom.Document;
//import org.w3c.dom.Node;
//import org.w3c.dom.NodeList;

//import com.pdi.data.v2.dto.ClientDTO;
//import com.pdi.data.v2.dto.ProjectDTO;
import com.pdi.data.v2.dto.SessionUserDTO;
//import com.pdi.data.v2.helpers.ClientDataHelper;
//import com.pdi.data.v2.helpers.ProjectDataHelper;
import com.pdi.data.v2.util.V2WebserviceUtils;
import com.pdi.properties.PropertyLoader;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

public class ProjectDataHelperUT extends TestCase {
	
	public ProjectDataHelperUT(String name)
	{
		super(name);
	}
	

	public void testProperties()
		throws Exception
	{
		UnitTestUtils.start(this);
		
		//Does the local property work?
		String applicationId = PropertyLoader.getProperty("com.pdi.data.v2.application", "application.code");
		assertEquals(applicationId != null, true);
		assertEquals(applicationId.equals("PDI-DATA-V2"), true);
		
		UnitTestUtils.stop(this);
	}
	

//	public void testGetProjectInfo()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		
////		//Does the local property work?
////		String applicationId = PropertyLoader.getProperty("com.pdi.data.v2.application", "application.code");
////		assertEquals(applicationId != null, true);
////		assertEquals(applicationId.equals("PDI-DATA-V2"), true);
//		
//		UnitTestUtils.stop(this);
//	}
	

	public void testGetProjects() 
		throws Exception
	{
		UnitTestUtils.start(this);
		
		//String username = "cdunn";
		//String password = "cdunn";
		String username = "cdunn";
		String password = "cdunn";
		V2WebserviceUtils v2web = new V2WebserviceUtils();
		SessionUserDTO sessionUser = v2web.registerUser(username, password);
		System.out.println("The user name is " + sessionUser);
		
		
		
		String xml = "";
		xml += "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
		xml += "<ETR SessionId=\"${sessionId}\">";
		xml += "<Rqt Cmd=\"OpenSQL\" AN=\"CandModSQL\" FA=\"Y\" FR=\"1\" RC=\"N\" SQ=\"select cj.dateapplied, c.name, j.title, j.xml, can.firstname, can.lastname from clients c inner join jobs j on c.uniqueidstamp = j.clientid inner join cand_jobs cj on j.uniqueidstamp = cj.jobid inner join candidates can on can.uniqueidstamp = cj.candidateid  where j.jobtype = '23'\" />";
		xml += "<Rqt Cmd=\"Read\" AN=\"CandModSQL\" Sub=\"CandidateModules\" Rpt=\"*\" RQ=\"N\">";
		xml += "	<Fld FN=\"FirstName\" Fmt=\"A\" />";
		xml += "	<Fld FN=\"DateApplied\" Fmt=\"A\" />";
		xml += "	<Fld FN=\"LastName\" Fmt=\"A\" />"; 
		xml += "    <Fld FN=\"xml_BU\" Fmt=\"A\" />";
		xml += "    <Fld FN=\"xml_JobTitle\" Fmt=\"A\" />";
		xml += "				<Fld FN=\"Title\" Fmt=\"A\" />";
		xml += "				<Fld FN=\"Name\" Fmt=\"A\" />";
		xml += "				<Fld FN=\"xml_TPOTTransLevel\" Fmt=\"A\" />";
		xml += "				<Fld FN=\"xml_TPOTTransLevelCurrent\" Fmt=\"A\" />";
		xml += "				<Fld FN=\"xml_TPOTTransLevelTarget\" Fmt=\"A\" />";
		xml += "</Rqt>";
		xml += "<Rqt Cmd=\"Close\" AN=\"CandModSQL\" />";
		xml += "</ETR>";
		xml = xml.replace("${sessionId}", sessionUser.getSessionId());
		
		//XPath xpath = XPathFactory.newInstance().newXPath(); 
		
		//Document doc = v2web.sendData(xml);

		UnitTestUtils.stop(this);
	}
}
