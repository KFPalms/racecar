/**
 * Copyright (c) 2008 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.dto.common;

import java.util.ArrayList;

import com.pdi.data.abyd.dto.common.CompElementDTO;


/**
 * CompGroupData is a container for information about a competency group (aka - superfactor).
 * it is intended to hold display information about the comp group and its children (competencies).
 * 
 * Used in Integration Grids.
 * 
 * @author		Ken Beukelman
 */
public class CompGroupData
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private long _cgId;
	private String _cgName;
	private ArrayList<CompElementDTO> _cgCompList = null;

	//
	// Constructors.
	//
	public CompGroupData()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//

	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public long getCgId()
	{
		return _cgId;
	}

	public void setCgId(long value)
	{
		_cgId = value;
	}

	/*****************************************************************************************/
	public String getCgName()
	{
		return _cgName;
	}
	
	public void setCgName(String value)
	{
		_cgName = value;
	}

	/*****************************************************************************************/
	public ArrayList<CompElementDTO> getCgCompList()
	{
		if (_cgCompList == null)
		{
			_cgCompList = new ArrayList<CompElementDTO>();
		}
		return _cgCompList;
	}
	
	public void setCgCompList(ArrayList<CompElementDTO> value)
	{
		_cgCompList = value;
	}



	/*
	 * Show me what you've got
	 */
	public String toString()
	{
		String ret = "CompGroupData content (Java)\n";
		ret += "    Competency Group ID=" + _cgId + "\n";
		ret += "  Competency Group Name=" + _cgName + "\n";
		ret += "        SP Comp Elements:\n";
		for (CompElementDTO sce : _cgCompList)
		{
			ret += sce.toString();
		}
		
		return ret;
	}
}
