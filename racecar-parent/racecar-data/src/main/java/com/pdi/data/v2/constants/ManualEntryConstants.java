/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v2.constants;

import java.util.HashMap;

/**
 * 	Constants used in fetching score data from the Manual Entry app
 * 
 * @author mbpanichi
 */
public class ManualEntryConstants
{
	//
	// Static data.
	//
	
	public static final transient String ME_A_BY_D_TYPE = "abyd";
	
	public static final transient String CHQ_MOD = "LIBPMLBJ";
	
	public static final transient HashMap <String, Number> NON_NORMED_CONSTRUCTS = new HashMap <String, Number> ();
	static {
		NON_NORMED_CONSTRUCTS.put("FDXKKUON", 1);	//Raven's APM Short Form-Incorrect
		NON_NORMED_CONSTRUCTS.put("HQAGQXCW", 2);	//Raven's APM Short Form-Unanswered
		NON_NORMED_CONSTRUCTS.put("BLVPPMWO", 3);	//Watson-Glaser A Incorrect
		NON_NORMED_CONSTRUCTS.put("HORUSCUE", 4);	//Watson-Glaser A Unanswered
		NON_NORMED_CONSTRUCTS.put("BHCCADRC", 5);	//Watson-Glaser A Incorrect - SHORT FORM
		NON_NORMED_CONSTRUCTS.put("HOGDVPTY", 6);	//Watson-Glaser A Unanswered - SHORT FORM
		NON_NORMED_CONSTRUCTS.put("ISWBZZCA", 7);	//Response Distortion Index Calc 1
		NON_NORMED_CONSTRUCTS.put("FDOIGXXV", 8);	//Response Distortion Index Calc 10
		NON_NORMED_CONSTRUCTS.put("DZONHYPB", 9);	//Response Distortion Index Calc 11
		NON_NORMED_CONSTRUCTS.put("DUBHDXTF", 10);	//Response Distortion Index Calc 12 - raw RDI score
		NON_NORMED_CONSTRUCTS.put("FBYHENOP", 11);	//Response Distortion Index Calc 2
		NON_NORMED_CONSTRUCTS.put("LLHTBMDQ", 12);	//Response Distortion Index Calc 3
		NON_NORMED_CONSTRUCTS.put("FDVUOQBB", 13);	//Response Distortion Index Calc 4
		NON_NORMED_CONSTRUCTS.put("LGLSZQXN", 14);	//Response Distortion Index Calc 5
		NON_NORMED_CONSTRUCTS.put("FFEXSMZS", 15);	//Response Distortion Index Calc 6
		NON_NORMED_CONSTRUCTS.put("BMOIKYJF", 16);	//Response Distortion Index Calc 7
		NON_NORMED_CONSTRUCTS.put("DZLDAPCG", 17);	//Response Distortion Index Calc 8
		NON_NORMED_CONSTRUCTS.put("DVFMVVYU", 18);	//Response Distortion Index Calc 9
		NON_NORMED_CONSTRUCTS.put("CSIELREN", 19);	//Items - Section 2	 - lei
		NON_NORMED_CONSTRUCTS.put("CNAMJDUS", 20);	//LEI DevSugg Scale Standardization
		NON_NORMED_CONSTRUCTS.put("FBEWMRJL", 21);	//Work Orientation - Avoid (Performance) Goal Orient
		NON_NORMED_CONSTRUCTS.put("CRDXOADK", 22);	//Work Orientation - Learning Goal Orientation
		NON_NORMED_CONSTRUCTS.put("CQWPKSBF", 23);	//Work Orientation - Prove (Performance) Goal Orient
		NON_NORMED_CONSTRUCTS.put("BKYGIQIH", 24);  // Wesman Incorrect
		NON_NORMED_CONSTRUCTS.put("DXITYQZI", 25);  // Wessman Unanswered
	};
}

