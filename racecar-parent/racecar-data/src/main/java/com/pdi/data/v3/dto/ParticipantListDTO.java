/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.dto;

import java.util.ArrayList;

import com.pdi.data.util.BaseDTO;
import com.pdi.data.util.RequestStatus;

/**
* ParticipantListDTO is designed to convey the information for a number of
* participants.  It contains an ArrayList of ParticipantDTO objects; the
* status in each of the individual elements describes the state of that element.
* 
* @author		Ken Beukelman
*/
public class ParticipantListDTO extends BaseDTO
{
	//
	// Static data.
	//


	//
	// Static methods.
	//


	//
	// Instance data.
	//
	private ArrayList<ParticipantDTO> _participantList = new ArrayList<ParticipantDTO>();


	//
	// Constructors.
	//
	public ParticipantListDTO()
	{
		super();
		// does nothing right now
	}
	
	// Set up a new DTO with status but no data
	public ParticipantListDTO(RequestStatus stat)
	{
		super();
		this.setStatus(stat);
	}
	
	// Set up a new DTO with data (status of DTO is OK... check the individual items)
	public ParticipantListDTO(ArrayList<ParticipantDTO> data)
	{
		super();
		this.setParticipantList(data);
	}
	
	
	//
	// Instance methods.
	//

	/**
	 * Generic toString method
	 */
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append("ParticipantListDTO:  ");
		sb.append("Status code=" + this.getStatus().getStatusCode() + ", count=" + _participantList.size());
		for(int i=0; i < _participantList.size(); i++)
		{
			_participantList.get(i).toString();
		}
		return sb.toString();
	}

	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public ArrayList<ParticipantDTO> getParticipantList()
	{
		return _participantList;
	}
	
	public void setParticipantList(ArrayList<ParticipantDTO> value)
	{
		_participantList = value;
	}	
}
