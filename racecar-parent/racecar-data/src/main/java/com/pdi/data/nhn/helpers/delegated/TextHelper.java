/**
 * Copyright (c) 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.nhn.helpers.delegated;

import java.util.HashMap;

import com.pdi.data.dto.SessionUser;
import com.pdi.data.dto.Text;
import com.pdi.data.dto.TextGroup;
import com.pdi.data.helpers.interfaces.ITextHelper;


/*
 * This class provides language differentiated text support in the NHN environment
 */
public class TextHelper implements ITextHelper
{
	//
	// Static data
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//

	//
	// Instance methods.
	//

		
	/*
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.ITextHelper#fromGroupCodeLanguageCode(java.lang.String, java.lang.String)
	 */
	@Override
	public TextGroup fromGroupCodeLanguageCode(SessionUser sessionUser, String groupCode, String languageCode)
	{
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.ITextHelper#fromGroupCodeLanguageCodeWithDefault(java.lang.String, java.lang.String)
	 */
	@Override
	public TextGroup fromGroupCodeLanguageCodeWithDefault(SessionUser sessionUser, String groupCode, String languageCode)
	{
		return null;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.ITextHelper#fromGroupCode(java.lang.String)
	 */
	@Override
	public HashMap<String, TextGroup> fromGroupCode(SessionUser sessionUser, String groupCode)
	{
		return null;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.ITextHelper#fromGroupCodeLanguageCodeLabelCode(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public Text fromGroupCodeLanguageCodeLabelCode(SessionUser sessionUser, String groupCode, String languageCode, String labelCode)
	{
		return null;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.ITextHelper#fromGroupCodeLanguageCodeLabelCodeWithDefault(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public Text fromGroupCodeLanguageCodeLabelCodeWithDefault(SessionUser sessionUser, String groupCode, String languageCode, String labelCode)
	{
		return null;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.ITextHelper#fromTextIdLanguageCode(java.lang.String, java.lang.String)
	 */
	@Override
	public String fromTextIdLanguageCode(SessionUser sessionUser, String textId, String languageCode)
	{
		return null;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.ITextHelper#fromTextIdLanguageCodeWithDefault(java.lang.String, java.lang.String)
	 */
	@Override
	public String fromTextIdLanguageCodeWithDefault(SessionUser sessionUser, String textId, String languageCode)
	{
		return null;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.ITextHelper#clearGroupCache()
	 */
	@Override
	public void clearGroupCache()
	{
		return;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.ITextHelper#clearGroupCache(java.lang.String)
	 */
	@Override
	public void clearGroupCache(String groupCode)
	{
		return;
	}
}
