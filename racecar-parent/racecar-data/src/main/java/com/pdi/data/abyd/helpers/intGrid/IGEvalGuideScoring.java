/**
 * Copyright (c) 2009, 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.intGrid;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.pdi.data.abyd.dto.common.DNACellDTO;

/**
 * IGEvalGuideScoring contains the scoring for the Eval Guides as used in the
 * Integration Grid. It contains some code that is used in more than one of the
 * data helpers in the Integration Grid app. Insofar as possible, this has been
 * implemented as static methods to avoid having to instantiate a separate
 * class.
 *
 * @author Ken Beukelman
 */
public class IGEvalGuideScoring {
	//
	// Static data.
	//

	//
	// Static methods.
	//

	/**
	 * Calculate the Eval Guide scores that are present and available. If the
	 * comp score is overridden, use the override score. Stuff them in a Cell
	 * object and return them in a list. Any additional processing should be
	 * done by the caller.
	 *
	 * @param con
	 *            - A database connection
	 * @param partId
	 *            - V2 participant ID
	 * @param dnaId
	 *            - A DNA ID
	 * @return An ArrayList of DNACellDTO objects
	 * @throws Exception
	 */
	static public ArrayList<DNACellDTO> calcEGScores(Connection con, String partId, long dnaId) throws Exception {
		ArrayList<DNACellDTO> egScores = new ArrayList<DNACellDTO>();

		Statement stmt = null;
		ResultSet rs = null;
		StringBuffer sqlQuery = new StringBuffer();

		// Get the overide and extComp scores and hold the data (separately)
		Map<String, String> overides = new HashMap<String, String>();
		ArrayList<DNACellDTO> extScores = new ArrayList<DNACellDTO>();
		sqlQuery.append("SELECT cr.moduleId, ");
		sqlQuery.append("       cr.competencyId, ");
		sqlQuery.append("       cr.orScoreIdx, ");
		sqlQuery.append("       cr.extCompScore, ");
		sqlQuery.append("       amod.specialType "); // pdi_abd_module
		sqlQuery.append("  FROM pdi_abd_eg_comp_response cr");
		sqlQuery.append("    LEFT JOIN pdi_abd_module amod ON amod.moduleId = cr.moduleId ");
		sqlQuery.append("  WHERE cr.participantId = '" + partId + "' ");
		sqlQuery.append("    AND cr.dnaId = " + dnaId + " ");
		sqlQuery.append("    AND (cr.orScoreIdx != 0 OR cr.extCompScore > 0.0)");
		sqlQuery.append("  ORDER BY moduleId, competencyId");

		try {
			stmt = con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			if (rs.isBeforeFirst()) {
				// Got stuff... put it in the map
				long mod = 0;
				long comp = 0;

				while (rs.next()) {
					mod = rs.getLong("moduleId");
					comp = rs.getLong("competencyId");
					int scoreIdx = rs.getInt("orScoreIdx");
					double ecScore = rs.getDouble("extCompScore");
					String st = rs.getString("specialType");

					if (st.equals("ES")) {
						// save external scoring data
						DNACellDTO cell = new DNACellDTO();
						cell.setModuleId(mod);
						cell.setCompetencyId(comp);
						cell.setScore(IGScoring.scoreFloat2String(ecScore));
						extScores.add(cell);
					} else {
						// Save the information for overiding scores
						// Make a key
						String key = mod + "|" + comp;

						// Make a value (1.0 to 5.0. in 0.5 increments)
						double dd = (scoreIdx + 1.0) / 2.0;
						String val = IGScoring.scoreFloat2String(dd);

						overides.put(key, val);
					}
				} // End of overide map / external score list generation
			}
		} catch (SQLException ex) {
			// handle any errors
			throw new Exception("SQL for calcEGScores (1).  " + "SQLException: " + ex.getMessage() + ", " + "SQLState: "
					+ ex.getSQLState() + ", " + "VendorError: " + ex.getErrorCode());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					System.out.println("ERROR: rs close in calcEGScores (1) - " + sqlEx.getMessage());
				}
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					System.out.println("ERROR: stmt close in calcEGScores (1) - " + sqlEx.getMessage());
				}
				stmt = null;
			}
		}

		// Now get the bar scores and score them
		sqlQuery.setLength(0);
		sqlQuery.append("SELECT br.moduleId, ");
		sqlQuery.append("       br.competencyId, ");
		sqlQuery.append("       br.barScore ");
		// sqlQuery.append(" br.barScore,");
		// sqlQuery.append(" amod.specialType "); //pdi_abd_module
		sqlQuery.append("  FROM pdi_abd_eg_bar_response br ");
		sqlQuery.append("    INNER JOIN pdi_abd_eg_response egr ON (egr.participantId = br.participantId ");
		sqlQuery.append("                                       AND egr.dnaId = br.dnaId ");
		sqlQuery.append("                                       AND egr.moduleId = br.moduleId) ");
		// sqlQuery.append(" AND egr.egSubmitted = 1) ");
		// sqlQuery.append(" LEFT JOIN pdi_abd_module amod ON amod.moduleId =
		// br.moduleId ");
		sqlQuery.append("  WHERE br.participantId = '" + partId + "' ");
		sqlQuery.append("    AND br.dnaId = " + dnaId + " ");
		sqlQuery.append("  ORDER BY br.moduleId, br.competencyId");

		try {
			stmt = con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			if (!rs.isBeforeFirst()) {
				// no bar scores... nothing else to do here
				if (extScores.size() > 0) {
					egScores.addAll(extScores);
				}
				return egScores;
			}

			// process the scores
			boolean firstTime = true;
			long curMod = 0;
			long curComp = 0;
			double cum = 0.0;
			int cnt = 0;
			// int zeroCnt = 0;
			double avgScore = 0;
			long mod = 0;
			long comp = 0;
			// String spcType = "";

			while (rs.next()) {
				mod = rs.getLong("moduleId");
				comp = rs.getLong("competencyId");
				int score = rs.getInt("barScore");
				// spcType = rs.getString("specialType");

				if (firstTime) {
					curMod = mod;
					curComp = comp;
					firstTime = false;
				}
				if (comp != curComp || mod != curMod) {
					String key = curMod + "|" + curComp;
					String dnaScore = overides.get(key);
					if (dnaScore == null) {
						// Score and save the data
						// "Competency score is the calculated average of the
						// behavior scores. 2.2.1.2"
						// "If 2 or more behaviors are rated N/A, the competency
						// score then is also N/A. 2.2.1.5"
						// if zeroCnt > 1 it means 2 or more are rated N/A
						// avgScore = (zeroCnt > 1) ? 0.0 : (cum / (double)cnt);
						// //if (zeroCnt > 1)
						// //{
						// // avgScore = 0.0;
						// //}
						// //else
						// //{
						// // avgScore = cum / (double)cnt;
						// //}
						avgScore = (cnt < 2) ? 0.0 : (cum / cnt);
						dnaScore = IGScoring.scoreFloat2String(avgScore);
					}
					// Build the cell
					DNACellDTO cell = new DNACellDTO();
					cell.setModuleId(curMod);
					cell.setCompetencyId(curComp);
					cell.setScore(dnaScore);
					egScores.add(cell);

					// initialize for next go around
					curMod = mod;
					curComp = comp;

					// clear the counters
					cum = 0.0;
					cnt = 0;
					// zeroCnt = 0;
				} // End of the scoring and saving if stmt (comp or mod changed)

				// Accumulate the data
				if (score > 0) {
					cum += score;
					cnt++;
				}
				// else if (score == 0)
				// {
				// zeroCnt++;
				// }
			} // End of while loop

			// calc and save the last score - same comments as above
			String key = curMod + "|" + curComp;
			String dnaScore = overides.get(key);
			if (dnaScore == null) {
				// avgScore = (zeroCnt > 1) ? 0.0f : (cum / (double)cnt);
				// //if (zeroCnt > 1)
				// //{
				// // avgScore = 0.0;
				// //}
				// //else
				// //{
				// // avgScore = cum / (double)cnt;
				// //}
				avgScore = (cnt < 2) ? 0.0f : (cum / cnt);
				dnaScore = IGScoring.scoreFloat2String(avgScore);
			}
			DNACellDTO cell = new DNACellDTO();
			cell.setModuleId(curMod);
			cell.setCompetencyId(curComp);
			cell.setScore(dnaScore);
			egScores.add(cell);
		} catch (SQLException ex) {
			// handle any errors
			throw new Exception("SQL for calcEGScores (2).  " + "SQLException: " + ex.getMessage() + ", " + "SQLState: "
					+ ex.getSQLState() + ", " + "VendorError: " + ex.getErrorCode());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					System.out.println("ERROR: rs close in calcEGScores (2) - " + sqlEx.getMessage());
				}
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					System.out.println("ERROR: stmt close in calcEGScores (2) - " + sqlEx.getMessage());
				}
				stmt = null;
			}
		}

		// Check if we need to pass along external scores
		// No longer needed with removal of KF360 (CapGem) stuff
		// sqlQuery.setLength(0);
		// sqlQuery.append("SELECT moduleId, ");
		// sqlQuery.append(" competencyId, ");
		// sqlQuery.append(" extCompScore ");
		// sqlQuery.append(" FROM pdi_abd_eg_comp_response ");
		// sqlQuery.append(" WHERE participantId = " + partId + " ");
		// sqlQuery.append(" AND dnaId = " + dnaId + " ");
		// sqlQuery.append(" AND moduleId = (SELECT moduleId FROM pdi_abd_module
		// WHERE internalName = 'KF360')");
		//
		// try
		// {
		// stmt = con.createStatement();
		// rs = stmt.executeQuery(sqlQuery.toString());
		//
		// if (rs.isBeforeFirst())
		// {
		// // Got stuff... put it in the map
		// long mod = 0;
		// long comp = 0;
		//
		// while (rs.next())
		// {
		// mod = rs.getLong("moduleId");
		// comp = rs.getLong("competencyId");
		// double extScore = rs.getDouble("extCompScore");
		// String val = IGScoring.scoreFloat2String(extScore);
		//
		// DNACellDTO cell = new DNACellDTO();
		// cell.setModuleId(mod);
		// cell.setCompetencyId(comp);
		// cell.setScore(val);
		// egScores.add(cell);
		// } // End of external score capture
		// }
		// }
		// catch (SQLException ex)
		// {
		// // handle any errors
		// throw new Exception("SQL for calcEGScores (3). " +
		// "SQLException: " + ex.getMessage() + ", " +
		// "SQLState: " + ex.getSQLState() + ", " +
		// "VendorError: " + ex.getErrorCode());
		// }
		// finally
		// {
		// if (rs != null)
		// {
		// try { rs.close(); }
		// catch (SQLException sqlEx)
		// {
		// System.out.println("ERROR: rs close in calcEGScores (3) - " +
		// sqlEx.getMessage());
		// }
		// rs = null;
		// }
		// if (stmt != null)
		// {
		// try { stmt.close(); }
		// catch (SQLException sqlEx)
		// {
		// System.out.println("ERROR: stmt close in calcEGScores (3) - " +
		// sqlEx.getMessage());
		// }
		// stmt = null;
		// }
		// }

		// Add any external scores
		if (extScores.size() > 0) {
			egScores.addAll(extScores);
		}
		return egScores;
	}

	//
	// Instance data.
	//

	//
	// Constructors.
	//

	//
	// Instance methods.
	//
}
