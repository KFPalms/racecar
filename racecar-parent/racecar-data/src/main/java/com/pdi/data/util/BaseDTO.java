/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.util;

/**
 * BaseDTO is the base class for most of the DTOs that access email
 * related data.  It contains a RequestStatus object and will be
 * augmented by additional data when sub-classed.
 *
 * @author		Gavin Meyers
 * @author		Ken Beukelman
 */
public class BaseDTO
{
	//
	// Static data.  
	//
	public static final int NOT_ALLOWED = -1;
	public static final int PROXY = 1;
	public static final int FILLED = 0;

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private RequestStatus _status = new RequestStatus(); 
	private int _proxy = NOT_ALLOWED;
	
	//
	// Constructors.
	//
	public BaseDTO()
	{
		// Nothing done on a no-parameter constructor
	}
	
	public BaseDTO(RequestStatus stat)
	{
		_status = stat;
	}

	//
	// Instance methods.
	//

	/*
	 * Convenience method to check if the DTO is a proxy (minimally filled with data)
	 */
	public boolean isProxy()
	{
		return (_proxy == PROXY ? true : false);
	}
//	
//	public boolean mustBeFilled()
//	{
//		return (! _proxy == NOT_ALLOWED);
//	}
	
	
	//////////////////////////////////////
	//        Setters and Getters       //
	//////////////////////////////////////
	
	/********************************************************/
	public void setStatus(RequestStatus value)
	{
		_status = value;
	}

	public RequestStatus getStatus()
	{
		return _status;
	}
	
	/********************************************************/
	public void setProxy(int value)
	{
		_proxy = value;
	}

	public int getProxy()
	{
		return _proxy;
	}
	
	
	/*
	 * Method to dump my contents
	 */
	public String toString()
	{
		String ret = null;
		
		ret += this.getStatus().toString();
		ret += "Proxy State=" + _proxy;

		return ret;
	}
}
