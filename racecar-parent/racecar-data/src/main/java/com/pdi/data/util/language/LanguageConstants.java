/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.data.util.language;

/**
 * Constants associated with languages
 * 
 * @author		Ken Beukelman
 */
public class LanguageConstants
{
	// default language id
	protected static final String DEFAULT_LANG_CODE = "en";
}
