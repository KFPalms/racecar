/**
 * Copyright (c) 2008 Personnel Decisions International, Inc.
 * All rights reserved.
 *
 * $Header: //com/pdi/data/v2/dataObjects/KeyValuePairStrings.java 6     8/25/05 3:23p Kbeukelm $
 */
package com.pdi.data.v2.dataObjects;


/**
 * KeyValuePairStrings is a container class for key/value pairs (e.g., HRA Varchar id and name)
 * 
 * Note that while there is a corresponding class on the AS side, the data is
 * indistinguishable from a native AS3 Object and is, therefore mapped to that
 * when it is transported to Flex.
 *
 * @author      mpanichi
 * @author		Ken Beukelman
 * @version	$Revision: 6 $  $Date: 8/25/05 3:23p $
 */
public class KeyValuePairStrings
{

	//
	// Static data.
	//

	/** The source revision. */
	public static final String REVISION = "$Revision: 5 $";

	//
	// Static methods.
	//

	//
	// Instance data.
	//
		private String _key;
		private String _value;


		//
		// Constructors.
		//
	public KeyValuePairStrings()
	{
		// Does nothing presently
	}
	
	//
	// Instance methods.
	//

	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
		
	/*****************************************************************************************/
	public String getTheKey()
	{
		return _key;
	}

	public void setTheKey(String value)
	{
		_key = value;
	}

	/*****************************************************************************************/
	public String getTheValue()
	{
		return _value;
	}

	public void setTheValue(String value)
	{
		_value = value;
	}

}

