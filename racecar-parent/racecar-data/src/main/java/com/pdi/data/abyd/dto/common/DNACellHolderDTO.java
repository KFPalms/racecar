/**
 * Copyright (c) 2018 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.dto.common;

import java.util.ArrayList;

import com.pdi.data.abyd.dto.common.DNACellDTO;

/**
 * DNACellHolderDTO is a wrapper around the
 * array list of DNA Cell data for
 * saving the component selections.
 *
 * @author      Steve Emrud
 */
public class DNACellHolderDTO
{
	//
	// Instance data.
	//
    private long _dnaId = 0;
    private ArrayList<DNACellDTO> _modCompIntersection = new ArrayList<DNACellDTO>();

	//
	// Constructors.
	//
	public DNACellHolderDTO() {
		// Does nothing presently
	}

	//
	// Instance methods.
	//
	public String toString() {
		String ret = "";

		ret += "DNACellHolderDTO:\n";
		ret += "Id = " + this._dnaId + "\n";
		ret += "    " + this._modCompIntersection.size() + " notes:\n";
		for (DNACellDTO dnaCell : _modCompIntersection) {
			ret += dnaCell.toString() + "\n";
		}

		return ret;
	}

	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public long getDnaId() {
		return _dnaId;
	}

	public void setDnaId(long value) {
		_dnaId = value;
	}

	/*****************************************************************************************/
	public ArrayList<DNACellDTO> getModCompIntersection() {
		return _modCompIntersection;
	}

	public void setModCompIntersection(ArrayList<DNACellDTO> value) {
		_modCompIntersection = value;
	}

}
