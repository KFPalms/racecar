/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.dto;

import com.pdi.data.util.BaseDTO;

/**
 * ParticipantReportListDTO is a class designed 
 * to bring a list of participant/report pairs
 * 
 * @author		Gavin Myers
 * @author      MB Panichi
  */

public class ParticipantReportListDTO  extends BaseDTO {
	public ParticipantReportListDTO() {
		this.participantList = new ParticipantListDTO();
	}
	
	private ParticipantListDTO participantList;
	private ReportDTO report;
	public void setParticipantList(ParticipantListDTO participantList) {
		this.participantList = participantList;
	}
	public ParticipantListDTO getParticipantList() {
		return participantList;
	}
	public void setReport(ReportDTO report) {
		this.report = report;
	}
	public ReportDTO getReport() {
		return report;
	}
	
}
