/*
 * Copyright (c) 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.setup.ut;

import java.sql.Connection;

import com.pdi.data.abyd.dto.setup.ReportModelTranslationDTO;
import com.pdi.data.abyd.dto.setup.ReportModelTranslationItemDTO;
import com.pdi.data.abyd.helpers.setup.ReportModelTranslationHelper;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.data.util.language.LanguageItemDTO;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

public class ReportModelTranslationHelperUT extends TestCase
{
	//
	// Constructors
	//

	public ReportModelTranslationHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
		throws Exception
    {
		junit.textui.TestRunner.run(ReportModelTranslationHelperUT.class);
    }


	/*
	 * testGetTranslationData
	 */
//	public void testGetTranslationData()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//
//		Connection con = AbyDDatabaseUtils.getDBConnection();
//
//		ReportModelTranslationDTO rmtd = new ReportModelTranslationDTO();
//		rmtd.setDnaId(6);
//		LanguageItemDTO lid = new LanguageItemDTO();
//		lid.setId(4);
//		lid.setCode("es");
//		lid.setDisplayName("Spanish");
//		rmtd.setLanguageItem(lid);
//	
//		ReportModelTranslationHelper helper = new ReportModelTranslationHelper(con);
//		ReportModelTranslationDTO out = helper.getTranslationData(rmtd);
//		System.out.println("Data returned:  DNA=" + out.getDnaId() + ", lang=" + out.getLanguageItem().getDisplayName() + " (" + out.getLanguageItem().getId() + ")");
//		for (ReportModelTranslationItemDTO itm : out.getXlateItemList() )
//		{
//			System.out.println(itm.toString());
//		}
//
//		UnitTestUtils.stop(this);
//	}


	/*
	 * testUpdateTranslationData
	 */
	public void testUpdateTranslationData()
		throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);

		Connection con = AbyDDatabaseUtils.getDBConnection();

		// Get a translation stack
		ReportModelTranslationDTO rmtd = new ReportModelTranslationDTO();
		rmtd.setDnaId(6);
//		LanguageItemDTO lid = new LanguageItemDTO();
//		lid.setId(4);
//		lid.setCode("es");
//		lid.setDisplayName("Spanish");
		LanguageItemDTO lid = new LanguageItemDTO();
		lid.setId(6);
		lid.setCode("de");
		lid.setDisplayName("German");
		rmtd.setLanguageItem(lid);
	
		ReportModelTranslationHelper helper = new ReportModelTranslationHelper(con);
		ReportModelTranslationDTO data = helper.getTranslationData(rmtd);
		
		System.out.println("Original Data returned:  DNA=" + data.getDnaId() + ", lang=" + data.getLanguageItem().getDisplayName() + " (" + data.getLanguageItem().getId() + ")");
		for (ReportModelTranslationItemDTO itm : data.getXlateItemList() )
		{
			System.out.println(itm.toString());
		}
		
		// Modify it
//		// 1st iteration
//		ReportModelTranslationItemDTO item = data.getXlateItemList().get(17);
//		//item.setTranslatedText("Personal Leadership - new text - es");
//		item.setTranslatedText("Personal Leadership - new text - de");
//		item.setChanged(true);
//		// 2nd iteration
//		ReportModelTranslationItemDTO item = data.getXlateItemList().get(2);
//		item.setTranslatedText("Think Strategically - new text - de");
//		item.setChanged(true);
//		// 3rd iteration
//		ReportModelTranslationItemDTO item = data.getXlateItemList().get(2);
//		//item.setTranslatedText("");
//		item.setTranslatedText(null);
//		item.setChanged(true);
		
		// Test with UTF-8 String
		ReportModelTranslationItemDTO item = data.getXlateItemList().get(1);
		item.setTranslatedText("de2 - АБПŃńŅņŇňŉŊŋŌōŎŏ日Öö本語漢字じり어/조말Ü中華民國bуلعرкиçaÅÄÃÂÁÀÅseÆĮį");
		item.setChanged(true);
		
		// Transmit it back for processing
		helper.updateTranslationData(data);
		
		// Get back the data now
		ReportModelTranslationDTO data2 = helper.getTranslationData(rmtd);
		System.out.println("Updated Data returned:  DNA=" + data2.getDnaId() + ", lang=" + data2.getLanguageItem().getDisplayName() + " (" + data2.getLanguageItem().getId() + ")");
		for (ReportModelTranslationItemDTO itm : data2.getXlateItemList() )
		{
			System.out.println(itm.toString());
		}

		UnitTestUtils.stop(this);
	}

}
