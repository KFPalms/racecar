package com.pdi.data.abyd.helpers.setup.ut;

import java.sql.Connection;
import java.util.ArrayList;

import com.pdi.data.abyd.dto.common.DNACellDTO;
import com.pdi.data.abyd.helpers.setup.SaveDNAStructureHelper;
import com.pdi.ut.UnitTestUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;

import junit.framework.TestCase;

public class SaveDNAStructureHelperUT extends TestCase
{
	//
	// Constructors
	//

	public SaveDNAStructureHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
		throws Exception
    {
		junit.textui.TestRunner.run(SaveDNAStructureHelperUT.class);
    }

	/*
	 * testSaveStructure
	 * NOTE:  Not all of the functionality is tested.  Longer and shorter arrays
	 *        will exercise the insert and delete functionality.
	 */
	public void testSaveStructure()
		throws Exception
	{
		System.out.println("SaveDNAStructureHelperUT:");
		
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);

		Connection con = AbyDDatabaseUtils.getDBConnection();
		
		ArrayList<DNACellDTO> chgAry= new ArrayList<DNACellDTO>();
    	long[] modAry = {75,77,78,76};
    	long[] cmpAry = {508,508,508,509};
    	for (int i = 0; i < modAry.length; i++)
    	{
    		DNACellDTO elt = new DNACellDTO();
    		elt.setModuleId(modAry[i]);
    		elt.setCompetencyId(cmpAry[i]);
    		elt.setIsUsed((i % 2 == 1) ? false : true);
    		chgAry.add(elt);
    	}
    	long dnaId = 9;
		
		System.out.println("    DNA ID=" + dnaId);

		SaveDNAStructureHelper helper = new SaveDNAStructureHelper(con, dnaId, chgAry);
		helper.saveStructure();
			
		System.out.println("    Output:  None");

		UnitTestUtils.stop(this);
	}
}
