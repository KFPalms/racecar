/**
 * Copyright (c) 2008 Personnel Decisions International, Inc.
 * All rights reserved.
 *
 * $Header:
 */

package com.pdi.data.abyd.util.email;

import java.util.Date;
import java.util.Properties;

import javax.annotation.Resource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//NOTE:  Eventually the email functionality in A By D may be migrated
//to the standard Oxcart functionality.  At that time, this module and
//its brethren in this folder can be dumped.

/**
 * Directs email requests to an email server.
 *
 * @author	Mike Solomonson
 * @version	$Revision: 1 $  $Date: 12/04/08 12:23p $
 */
public class EmailRequestHelper
{
	/**
	 * This injection doesn't work.  Fix me!!
	 */
	@Resource(name = "java:jboss/mail/pdinh")
	private Session mailSession;
	
	private static final Logger log = LoggerFactory.getLogger(EmailRequestHelper.class);
	//
	// Static data.
	//
    
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	
	//
	// Constructors.
	//
	public EmailRequestHelper()
	{}

	//
	// Instance methods.
	//	
    
    public void processEmailRequest(String emailServer, EmailMessage email)
    	throws Exception
    {
        // Create properties, get Session
        Properties props = new Properties();
        props.put("mail.smtp.host", emailServer);
        
        log.debug("Sending mail to server: {}", emailServer);
        // props.put("mail.debug", "true");		// Used for debugging mail related stuff
        
        // resolve the substitutions in both the subject and body
        if (email.getTextSubstitutions().size() > 0)
        {
        	// You got some substitutions to do
    		String subjectText = email.getSubject();
    		String bodyText = email.getText();
    		for (int idx=0; idx < email.getTextSubstitutions().size(); idx++)
        	{
        		String target = "{" + (idx + 1) + "}";
        		String replacement = email.getTextSubstitutions().get(idx);
        		subjectText = subjectText.replace(target, replacement);
        		bodyText = bodyText.replace(target, replacement);
        	}
    		email.setSubject(subjectText);
    		email.setText(bodyText);
        }

        // When no authentication is needed:
        Session session = Session.getInstance(props);
        //
        // or...
        //
        // To use userid/password authentication (also see SMTPAuthenticator below):
        //props.put("mail.smtp.auth", "true");
        //Authenticator auth = new SMTPAuthenticator();
        //Session session = Session.getDefaultInstance(props, auth);

        try 
        {
        	
            Message msg = new MimeMessage(session);

            msg.setFrom(new InternetAddress(email.getFromAddress()));
            InternetAddress[] address = {new InternetAddress(email.getToAddress())};
            msg.setRecipients(Message.RecipientType.TO, address);
            if (email.getCcAddress() != null && email.getCcAddress().length() > 1)
            {
            	InternetAddress[] ccAddress = {new InternetAddress(email.getCcAddress())};
            	msg.setRecipients(Message.RecipientType.CC, ccAddress);
            }
            msg.setSubject(email.getSubject());
            msg.setSentDate(new Date());
            msg.setText(email.getText());

            Transport.send(msg);
        }
        catch (MessagingException mex) 
        {
            // Fail silently, but dump the error to the stdout log
        	log.error("Email send failed.  msg={}", mex.getMessage());
            mex.printStackTrace();
        }
    }

	/**
	 *	readProperties reads the properties file into a Properties object
	 *
	 *	@return A Properties object for use by this application
	 */
    /*
	private Properties readProperties()
		throws Exception
	{
		Properties properties = new Properties();
		String propertiesFileLocation = System.getProperty("user.dir") +
											PROPERTY_FILE_PATH + PROPERTY_FILE_NAME;
		System.out.println("\nAttempting to load Properties file: " + propertiesFileLocation);
		
        properties.load(new FileInputStream(propertiesFileLocation));
	    
	    //DEBUG
	    //System.out.println("\n properties.load: ");
	    //for (Enumeration e = properties.propertyNames(); e.hasMoreElements(); ) 
	    //{
	    //    String propName = (String) e.nextElement();
	    //    System.out.println("properties.getProperty('" + propName + "') = " +
	    //            properties.getProperty(propName));
	    //}		
	    //DEBUG	    
	    
		return properties;
	}    */
    
    /**
    * SimpleAuthenticator is used to do simple authentication
    * when the SMTP server requires it.
    */
/*		This may be needed if the SMTP email server requires authentication...	
     private class SMTPAuthenticator extends javax.mail.Authenticator
    {

        public PasswordAuthentication getPasswordAuthentication()
        {
            String username = "";
            String password = "";
            return new PasswordAuthentication(username, password);
        }
    }*/
}
