package com.pdi.data.abyd.helpers.evalGuide.ut;

import java.sql.Connection;

import com.pdi.data.abyd.helpers.evalGuide.EGComCheckHelper;
import com.pdi.ut.UnitTestUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;

import junit.framework.TestCase;

public class EGComCheckHelperUT extends TestCase
{
	//
	// Constructors
	//
	public EGComCheckHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
	throws Exception
	{
		junit.textui.TestRunner.run(EGComCheckHelperUT.class);
	}
	
	/*
	 * 
	 */
	public void testConnect()
		throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);
	
		Connection con = AbyDDatabaseUtils.getDBConnection();

		EGComCheckHelper helper = new EGComCheckHelper(con);        
		String ret = helper.comCheck();

		System.out.println("made connection.  Return = " + ret);
		
		UnitTestUtils.stop(this);
	}
}
