/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.dto.common;

import java.util.ArrayList;

import com.pdi.data.abyd.dto.common.EGBarDisplayData;

/**
 * EGCompData is a thin class containing the information
 * required to build a single competency in an Eval Guide.
 * 
 * @author		Ken Beukelman
 */
public class EGCompDisplayData
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private long _compId;							// The internal competency id
	private String _compName;						// The name of this competency
	private String _compDesc;						// The description for this competency in this model
	private String _compNotes;						// Eval Guide (consultant entered) notes associated with this competency
	private ArrayList<EGBarDisplayData> _compBars;	// Array of EGBarDisplayData objects one for each bar in this competency
	private int _overideScoreIndex = 0;				// Competency overide score index.  A value of 0 indicates that there is no overide
	private String _calcedCompScore = "";			// Calculated competency score (possibly overridden) converted to a String
	private String _extCompScore;					// External competency score.

	private String specialType;
	//
	// Constructors.
	//
	public EGCompDisplayData()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public long getCompId()
	{
		return _compId;
	}
		
	public void setCompId(long value)
	{
		_compId = value;
	}

	/*****************************************************************************************/
	public String getCompName()
	{
		return _compName;
	}
		
	public void setCompName(String value)
	{
		_compName = value;
	}

	/*****************************************************************************************/
	public String getCompDesc()
	{
		return _compDesc;
	}
		
	public void setCompDesc(String value)
	{
		_compDesc = value;
	}

	/*****************************************************************************************/
	public String getCompNotes()
	{
		return _compNotes;
	}
		
	public void setCompNotes(String value)
	{
		_compNotes = value;
	}

	/*****************************************************************************************/
	public ArrayList<EGBarDisplayData> getCompBars()
	{
		if (_compBars == null)
		{
			_compBars = new ArrayList<EGBarDisplayData>();
		}
		return _compBars;
	}
		
	public void setCompBars(ArrayList<EGBarDisplayData> value)
	{
		_compBars = value;
	}

	/*****************************************************************************************/
	public int getOverideScoreIndex()
	{
		return _overideScoreIndex;
	}

	public void setOverideScoreIndex(int value)
	{
		this._overideScoreIndex = value;
	}

	/*****************************************************************************************/
	public String getCalcedCompScore()
	{
		return _calcedCompScore;
	}

	public void setCalcedCompScore(String value)
	{
		this._calcedCompScore = value;
	}

	/*****************************************************************************************/
	public String getExtCompScore()
	{
		return _extCompScore;
	}

	public void setExtCompScore(String value)
	{
		this._extCompScore = value;
	}
	
	/*****************************************************************************************/
	public String getSpecialType() {
		return specialType;
	}

	
	public void setSpecialType(String specialType) {
		this.specialType = specialType;
	}
	/*****************************************************************************************/

	/*
	 * Show me what you've got
	 */
	public String toString()
	{
		String ret = "EGCompDisplayData content (Java)\n";
		ret += "          Comp. ID=" + _compId + "\n";
		ret += "        Comp. Name=" + _compName + "\n";
		ret += "        Comp. Desc=" + _compDesc + "\n";
		ret += "       Comp. Notes=" + _compNotes + "\n";
		ret += "  Competency Bars:\n";
		for (EGBarDisplayData bar : _compBars)
		{
			ret += bar.toString();
		}
		ret += "     Overide Index=" + _overideScoreIndex + "\n";
		ret += "Calced Comp. Score=" + _compName + "\n";

		
		return ret;
	}

	
}
