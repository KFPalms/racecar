/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.helpers;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.util.RequestStatus;
import com.pdi.data.v3.dto.CandidateDTO;
import com.pdi.data.v3.dto.ClientDTO;
import com.pdi.data.v3.dto.EmailDTO;
import com.pdi.data.v3.dto.EmailGroupDTO;
import com.pdi.data.v3.dto.EmailListDTO;
import com.pdi.data.v3.dto.EmailTextDTO;
import com.pdi.data.v3.dto.ParticipantDTO;
import com.pdi.data.v3.dto.ParticipantEmailListDTO;
import com.pdi.data.v3.dto.ParticipantListDTO;
import com.pdi.data.v3.dto.ProductDTO;
import com.pdi.data.v3.dto.ProjectDTO;
import com.pdi.data.v3.dto.SessionUserDTO;
import com.pdi.data.v3.util.V3DatabaseUtils;
import com.pdi.email.Email;
import com.pdi.email.EmailServer;
import com.pdi.email.EmailUtils;
import com.pdi.properties.PropertyLoader;

/**
 * EmailDataHelper provides functionality to add, change,
 * delete, or fetch e-mails and their translated texts.
 *
 * @author		Ken Beukelman
 * @author		MB Panichi
 */
public class EmailDataHelper
{

	//
	// Static data.
	//
	public static final String ID = "[PARTICIPANT_ID]";
	public static final String FIRST_NAME = "[PARTICIPANT_FIRST_NAME]";
	public static final String LAST_NAME = "[PARTICIPANT_LAST_NAME]";
	public static final String EMAIL_ADDRESS = "[PARTICIPANT_EMAIL_ADDRESS]";
	public static final String ORGANIZATION = "[COMPANY_NAME]";
	public static final String PROJECT_ID = "[PROJECT_ID]";
	public static final String PROJECT_NAME = "[PROJECT_NAME]";
	public static final String PART_EXP_URL = "[PARTICIPANT_EXPERIENCE_URL]";

	//
	// Static methods.
	//
	
	//////////////////////////////
	//  Private static methods  //
	//////////////////////////////
	
	/**
	 * Process the input status object to add the "Unexpected..." message
	 * 
	 * @param The RequestStatus object 
	 * @return A RequestStatus object
	 */
	private static RequestStatus setUnexpectedStatus(RequestStatus inp)
	{
		RequestStatus out = new RequestStatus(inp);
		String str = "Unexpected Status code encountered";
		str += (inp.getExceptionMessage() == null ? "." : "; msg=" + inp.getExceptionMessage());
		out.setExceptionMessage(str);
		return out;
	}


	/**
	 * Move data from a ResultSet object to a EmailDTO object
	 * This function has been written to be "generic";  if the data is not
	 * in the result set, it won't be moved to the DTO.
	 * 
	 * @param rs A ResultSet object
	 * @param dto An EmailDTO object
	 */
	private static void moveItemData(ResultSet rs, EmailDTO dto)
	{
		try
		{
			ResultSetMetaData rsmd = rs.getMetaData();

			// Get all of the columns
			HashSet<String> set = new HashSet<String>();

			for(int i=1; i <= rsmd.getColumnCount(); i++)
			{
				//set.add(rsmd.getColumnName(i));
				set.add(rsmd.getColumnLabel(i));
			}
			/*
			private String _recipientTypeName;
			private HashMap<Long, EmailTextDTO> _emailTextDTOMap; // key = languageId
			*/
			
			//  Put them in the proper slot in the dto
			// Note that you must use the name in the select (in an
			// "as" clause if the column name is not the same)

			if (set.contains("emailId"))
			{
				dto.setEmailId(rs.getInt("emailId"));
			}
			if (set.contains("emailName"))
			{
				dto.setDisplayName(rs.getString("emailName"));
			}
			if (set.contains("emailDescription"))
			{
				dto.setDescription(rs.getString("emailDescription"));
			}
			if (set.contains("recipientTypeId"))
			{
				dto.setRecipientTypeId(rs.getLong("recipientTypeId"));
			}
			if (set.contains("recipientTypeName"))
			{
				dto.setRecipientTypeName(rs.getString("recipientTypeName"));
			}
			if (set.contains("groupId"))
			{
				dto.setGroupId(rs.getLong("groupId"));
			}
			if (set.contains("groupName"))
			{
				dto.setGroupName(rs.getString("groupName"));
			}

			return;
		}
		catch (SQLException ex)
		{
			dto.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
					ex.getErrorCode(), ex.getMessage()));
			
			// Log the error
			String exc = "\n************************************* " + 
				"\nError in EmailDataHelper.moveData for EmailDTO: " +
				"\nSQLException: " + ex.getMessage() + " " +
				"\nSQLState: " + ex.getSQLState() + " " +
				"\nVendorError: " + ex.getErrorCode();
			System.err.println(exc);
			System.err.println("************************************* ");

			return;
		}
	}	


	/**
	 * Move data from a ResultSet object to an EmailTextDTO object
	 * This function has been written to be "generic";  if the data is not
	 * in the result set, it won't be moved to the DTO.
	 * 
	 * @param rs A ResultSet object
	 * @param dto An EmailDTO object
	 */
	private static void moveTextData(ResultSet rs, EmailTextDTO dto)
	{
		try
		{
			ResultSetMetaData rsmd = rs.getMetaData();

			// Get all of the columns
			HashSet<String> set = new HashSet<String>();

			for(int i=1; i <= rsmd.getColumnCount(); i++)
			{
				//set.add(rsmd.getColumnName(i));
				set.add(rsmd.getColumnLabel(i));
			}
			
			//  Put them in the proper slot in the dto
			if (set.contains("emailTextId"))
			{
				dto.setEmailTextId(rs.getLong("emailTextId"));
			}
			if (set.contains("emailId"))
			{
				dto.setEmailId(rs.getLong("emailId"));
			}
			if (set.contains("languageId"))
			{
				dto.setLanguageId(rs.getLong("languageId"));
			}
			if (set.contains("isoCode"))
			{
				dto.setIsoCode(rs.getString("isoCode"));
			}
			if (set.contains("languageName"))
			{
				dto.setLanguageName(rs.getString("languageName"));
			}
			if (set.contains("subjectText"))
			{
				dto.setSubjectText(rs.getString("subjectText"));
			}
			if (set.contains("bodyText"))
			{
				dto.setBodyText(rs.getString("bodyText"));
			}
			if (set.contains("baseline"))
			{
				dto.setBaseline(rs.getBoolean("baseline"));
			}

			return;
		}
		catch (SQLException ex)
		{
			dto.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
					ex.getErrorCode(), ex.getMessage()));
			
			// Log the error
			String exc = "\n************************************* " + 
				"\nError in EmailDataHelper.moveData for emailTextDTO: " +
				"\nSQLException: " + ex.getMessage() + " " +
				"\nSQLState: " + ex.getSQLState() + " " +
				"\nVendorError: " + ex.getErrorCode();
			System.err.println(exc);
			System.err.println("************************************* ");

			return;
		}
	}		


	/**
	 * Insert a new group type map row - convenience method for addEmailItem
	 * @param An EmailDTO object
	 * @param An EmailGroupDTO object
	 * @return A RequestStatus object 
	 */
	private static RequestStatus insertGroupXref(SessionUserDTO sessionUser, EmailDTO email, EmailGroupDTO group)
	{
		DataResult dr = null;

		// Assumes that parameter checking was done prior to entry

		String tableName = null;
		String extraParms = null;
		String extraQmarks = null;
		if (group.getCode().equals(EmailGroupDTO.GLOBAL))
		{
			tableName = "em_group_gl_map";
			extraParms = "";
			extraQmarks = "";
		}
		else if (group.getCode().equals(EmailGroupDTO.CLIENT))
		{
			tableName = "em_group_cl_map";
			extraParms = "clientId, ";
			extraQmarks = "?, ";
		}
		else if (group.getCode().equals(EmailGroupDTO.PRODUCT))
		{
			tableName = "em_group_pd_map";
			extraParms = "productId, ";
			extraQmarks = "?, ";
		}
		else if (group.getCode().equals(EmailGroupDTO.PROD_CLIENT))
		{
			tableName = "em_group_pc_map";
			extraParms = "productId, clientId, ";
			extraQmarks = "?, ?, ";
		}
		else
		{
			return new RequestStatus(RequestStatus.RS_ERROR, "Invalid e-mail group code (" + group.getCode() + ")");
		}

		////TODO Modify query to eliminate hard-coded "xxxDate" fields once we have a DB with triggers installed

			// These are test queries until we get rights to add triggers.  At that time the
			// queries should be modified to remove the triggered fields.
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("INSERT INTO " + tableName + " ");
		sqlQuery.append("  (groupId, emailId, " + extraParms + "createdBy,createdDate,modifiedBy,modifiedDate) ");
		sqlQuery.append("  VALUES( ?, ?, " + extraQmarks + "?, GETDATE(), ?, GETDATE())");

		DataLayerPreparedStatement dlps = V3DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			return dlps.getStatus();
		}

		try
		{
			// Set the common fields
			dlps.getPreparedStatement().setLong(1,group.getId());
			dlps.getPreparedStatement().setLong(2,email.getEmailId());

			// now set the type specific ones
			int nxtIdx = 3;
			if (group.getCode().equals(EmailGroupDTO.CLIENT))
			{
				dlps.getPreparedStatement().setString(3,email.getClientId());
				nxtIdx = 4;
			}
			else if (group.getCode().equals(EmailGroupDTO.PRODUCT))
			{
				dlps.getPreparedStatement().setLong(3,email.getProductId());
				nxtIdx = 4;
			}
			else if (group.getCode().equals(EmailGroupDTO.PROD_CLIENT))
			{
				dlps.getPreparedStatement().setLong(3,email.getProductId());
				dlps.getPreparedStatement().setString(4,email.getClientId());
				nxtIdx = 5;
			}
			
			// "Modified by" fields
			dlps.getPreparedStatement().setString(nxtIdx, sessionUser.getUsername());
			nxtIdx++;
			dlps.getPreparedStatement().setString(nxtIdx, sessionUser.getUsername());

			dr = V3DatabaseUtils.insert(dlps);

			RequestStatus ret = dr.getStatus();
			switch (ret.getStatusCode())
			{
				case RequestStatus.RS_ERROR:
					break;
				case RequestStatus.DBS_DUP_KEY:
					ret.setExceptionMessage("Group ID/e-mail ID combination already exists.  " +
							"group ID=" + group.getId() + ", e-mail ID=" + email.getEmailId());
					break;
				case RequestStatus.RS_OK:
					break;
				default:
					ret = setUnexpectedStatus(ret);
					break;
			}

			return ret;
		}
		catch (SQLException ex)
		{
			return new RequestStatus(RequestStatus.RS_ERROR,
									 ex.getErrorCode(),
									 "insertGroupXref:  Group code=" + group.getCode() + ", msg=" + ex.getMessage());
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}


	////////  Lookup specific private methods  ////////

	/**
	 * Generalized private method to get e-mail proxies for a project and/or client
	 * The type is passed and, depending upon the signature different data is gathered
	 * 		GL - Gets all active emails from every type  NOTE - will expose all client specific data
	 * 		CL - Gets globals, client specific, and prod/client specific for a particular client
	 * 		PD - Gets globals, product specific, and prod/client specific for a particular product (and client for P/C)
	 * 		PC - Gets globals and client/product specific for a particular product/client
	 * 
	 * @param sessionUser
	 * @param clientId A client id.  Assumed to be vetted by calling code
	 * @return A EmailListDTO object
	 */
	private static EmailListDTO getEmailProxies(SessionUserDTO sessionUser, String type, String clientId, long productId)
	{
		DataResult dr = null;
		EmailListDTO ret = new EmailListDTO();
	
		// Set up the query
		StringBuffer sqlQuery = new StringBuffer();
		
		// First do the Global portion
		sqlQuery.append("  SELECT gl.groupId, ");
 		sqlQuery.append("         gl.displayName AS groupName, ");
		sqlQuery.append("         glm.emailId AS emailId, ");
		sqlQuery.append("         ee.displayName AS emailName, ");
		sqlQuery.append("         ee.description AS emailDescription, ");
		sqlQuery.append("         ee.recipientTypeId AS recipientTypeId, ");
 		sqlQuery.append("         rtp.displayName AS recipientTypeName ");
		sqlQuery.append("    FROM em_group_gl_map glm ");
		sqlQuery.append("      LEFT JOIN em_email ee ON (ee.emailId = glm.emailId AND ee.active = 1) ");
		sqlQuery.append("      LEFT JOIN em_recipient_type_lookup rtp ON (rtp.recipientTypeId = ee.recipientTypeId AND rtp.active = 1) ");
		sqlQuery.append("      LEFT JOIN em_group_lookup gl ON (gl.groupId = glm.groupId and gl.active = 1) ");
		sqlQuery.append("    WHERE glm.active = 1 ");

		// See if we need the client info
		if (type == EmailGroupDTO.GLOBAL || type == EmailGroupDTO.CLIENT)
		{
			sqlQuery.append("UNION ");
			sqlQuery.append("  SELECT  gl.groupId, ");
	 		sqlQuery.append("          gl.displayName AS groupName, ");
			sqlQuery.append("          clm.emailId AS emailId, ");
			sqlQuery.append("          ee.displayName AS emailName, ");
			sqlQuery.append("          ee.description AS emailDescription, ");
			sqlQuery.append("          ee.recipientTypeId AS recipientTypeId, ");
			sqlQuery.append("          rtp.displayName AS recipientTypeName ");
			sqlQuery.append("    FROM em_group_cl_map clm ");
			sqlQuery.append("      LEFT JOIN em_email ee ON (ee.emailId = clm.emailId AND ee.active = 1) ");
			sqlQuery.append("      LEFT JOIN em_recipient_type_lookup rtp ON (rtp.recipientTypeId = ee.recipientTypeId AND rtp.active = 1) ");
			sqlQuery.append("      LEFT JOIN em_group_lookup gl ON (gl.groupId = clm.groupId AND gl.active = 1) ");
			sqlQuery.append("    WHERE clm.active = 1 ");
			if (type == EmailGroupDTO.CLIENT)
			{
				sqlQuery.append("      AND clm.clientId = ? ");
			}
		}

		// See if we need the product info
		if (type == EmailGroupDTO.GLOBAL || type == EmailGroupDTO.PRODUCT)
		{
			sqlQuery.append("UNION ");
			sqlQuery.append("  SELECT  gl.groupId, ");
	 		sqlQuery.append("          gl.displayName AS groupName, ");
			sqlQuery.append("          pdm.emailId AS emailId, ");
			sqlQuery.append("          ee.displayName AS emailName, ");
			sqlQuery.append("          ee.description AS emailDescription, ");
			sqlQuery.append("          ee.recipientTypeId AS recipientTypeId, ");
			sqlQuery.append("          rtp.displayName AS recipientTypeName ");
			sqlQuery.append("    FROM em_group_pd_map pdm ");
			sqlQuery.append("      LEFT JOIN em_email ee ON (ee.emailId = pdm.emailId AND ee.active = 1) ");
			sqlQuery.append("      LEFT JOIN em_recipient_type_lookup rtp ON (rtp.recipientTypeId = ee.recipientTypeId AND rtp.active = 1) ");
			sqlQuery.append("      LEFT JOIN em_group_lookup gl ON (gl.groupId = pdm.groupId AND gl.active = 1) ");
			sqlQuery.append("    WHERE pdm.active = 1 ");
			if (type == EmailGroupDTO.PRODUCT)
			{
				sqlQuery.append("      AND pdm.productId = ? ");
			}
		}

		// Get the product/client info
		// Note that we will always be getting info from this
		// query (though different kinds depending upon the query)
		sqlQuery.append("UNION ");
		sqlQuery.append("  SELECT  gl.groupId, ");
 		sqlQuery.append("          gl.displayName AS groupName, ");
		sqlQuery.append("          pcm.emailId AS emailId, ");
		sqlQuery.append("          ee.displayName AS emailName, ");
		sqlQuery.append("          ee.description AS emailDescription, ");
		sqlQuery.append("          ee.recipientTypeId AS recipientTypeId, ");
		sqlQuery.append("          rtp.displayName AS recipientTypeName ");
		sqlQuery.append("    FROM em_group_pc_map pcm ");
		sqlQuery.append("      LEFT JOIN em_email ee ON (ee.emailId = pcm.emailId AND ee.active = 1) ");
		sqlQuery.append("      LEFT JOIN em_recipient_type_lookup rtp ON (rtp.recipientTypeId = ee.recipientTypeId AND rtp.active = 1) ");
		sqlQuery.append("      LEFT JOIN em_group_lookup gl ON (gl.groupId = pcm.groupId AND gl.active = 1) ");
		sqlQuery.append("    WHERE pcm.active = 1 ");
		if (type == EmailGroupDTO.CLIENT  || type == EmailGroupDTO.PRODUCT  ||  type == EmailGroupDTO.PROD_CLIENT)
		{
			// all types except GL require a client id (product needs it
			// to avoid showing client specific data to every client)
			sqlQuery.append("      AND pcm.clientId = ? ");
		}
		if (type == EmailGroupDTO.PRODUCT  ||  type == EmailGroupDTO.PROD_CLIENT)
		{
			// only product oriented types use productId
			sqlQuery.append("      AND pcm.productId = ? ");
		}
		sqlQuery.append("  ORDER BY groupName, emailName, emailId");

		DataLayerPreparedStatement dlps = V3DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			return new EmailListDTO(dlps.getStatus());
		}

		try
		{
			if (type == EmailGroupDTO.CLIENT)
			{
				dlps.getPreparedStatement().setString(1,clientId);				
				dlps.getPreparedStatement().setString(2,clientId);				
			}
			else if (type == EmailGroupDTO.PRODUCT)
			{
				dlps.getPreparedStatement().setLong(1,productId);				
				dlps.getPreparedStatement().setString(2,clientId);				
				dlps.getPreparedStatement().setLong(3,productId);				
			}
			else if (type == EmailGroupDTO.PROD_CLIENT)
			{
				dlps.getPreparedStatement().setString(1,clientId);				
				dlps.getPreparedStatement().setLong(2,productId);				
			}

			dr = V3DatabaseUtils.select(dlps);

			ret.setStatus(dr.getStatus());
			switch (ret.getStatus().getStatusCode())
			{
				case RequestStatus.RS_ERROR:
					break;
				case RequestStatus.DBS_NO_DATA:
					String str = "No Email Group Data exists.  TypeID=" + type;
					if (type == EmailGroupDTO.CLIENT  || type == EmailGroupDTO.PRODUCT  ||  type == EmailGroupDTO.PROD_CLIENT)
					{
						str += ", Client ID=" + clientId;
					}
					if (type == EmailGroupDTO.PRODUCT  ||  type == EmailGroupDTO.PROD_CLIENT)
					{
						str += ", Product ID=" + productId;
					}
					ret.getStatus().setExceptionMessage(str);
				break;
				case RequestStatus.RS_OK:
					ResultSet rs = dr.getResultSet();
					rs.next();
					while (! rs.isAfterLast())
					{
						EmailDTO dto = new EmailDTO();
						moveItemData(rs, dto);
						ret.getEmailList().add(dto);
						rs.next();
					}
					if (ret.getEmailList().size() == 0)
					{
						ret.getStatus().setStatusCode(RequestStatus.DBS_NO_DATA);
					}
					break;
				default:
					ret.setStatus(EmailDataHelper.setUnexpectedStatus(ret.getStatus()));
					break;
			}

			return ret;
		}
		catch (SQLException ex)
		{
			return new EmailListDTO(new RequestStatus(RequestStatus.RS_ERROR,
					"getEmailProxy:  (" + type + ") " + ex.getErrorCode() + " - " + ex.getMessage()));
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}



	/////////////////////////////////
	//        Public methods       //
	//  Email Item and Email Text  //
	/////////////////////////////////

	/**
	 * Return a list of participant email matches
	 * @param sessionUser
	 * @param participantList A list of ParticipantDTO objects
	 * @return A EmailListDTO object
	 */
	public static ArrayList<ParticipantEmailListDTO> getParticipantEmailList(SessionUserDTO sessionUser, ParticipantListDTO participantList)
	{
		/*
		 * This is built on how I think the data will be coming back from the database
		 * Step 1: Loop through the participants
		 * Step 2: Get the emails that are available for one participant
		 * Step 3: Build a list of emails/participant matches
		 * In order to do step 3 we almost have to flip the data, we have a list of  participanats with emails
		 * while what we really want is a list of emails with participants
		 */
		
		ArrayList<ParticipantEmailListDTO> participantEmailList = new ArrayList<ParticipantEmailListDTO>();

		if(participantList == null)
		{
			// Simplify this when we work on this code
			ParticipantEmailListDTO dto = new ParticipantEmailListDTO();
			dto.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
				"A ParticipantListDTO object is required to return an email list."));
			participantEmailList.add(dto);
			return participantEmailList;
		}
		
		//Step 1:
		for(ParticipantDTO participant : participantList.getParticipantList()) {
			ArrayList<EmailDTO> emails = EmailDataHelper.getParticipantEmailList(participant);
			
			//Step 2:
			for(EmailDTO email : emails)
			{
				boolean foundEmail = false;
				
				//Step 3:
				for(ParticipantEmailListDTO participantEmail : participantEmailList)
				{
					if(participantEmail.getEmail().getEmailId() == email.getEmailId())
					{
						foundEmail = true;
						participantEmail.getParticipantList().getParticipantList().add(participant);
					}
				}

				if(!foundEmail)
				{
					ParticipantEmailListDTO participantEmail = new ParticipantEmailListDTO();
					participantEmail.setEmail(email);
					participantEmail.getParticipantList().getParticipantList().add(participant);
					participantEmailList.add(participantEmail);
				}
			}
		}
		
		return participantEmailList;
	}
	
	/**
	 * Return a list of emails that a particular participant has access to
	 * 	NOTE:  THIS IS HARD-CODED AT PRESENT
	 * @param participant
	 * @return
	 */
	public static ArrayList<EmailDTO> getParticipantEmailList(ParticipantDTO participant)
	{
		ArrayList<EmailDTO> emails = new ArrayList<EmailDTO>();

		if(participant == null)
		{
			emails.add(new EmailDTO(new RequestStatus(RequestStatus.RS_ERROR,
					"A ParticipantDTO object is required to return an email list.")));
			return emails;
		}
		
		if(true)
		{
			EmailDTO email = new EmailDTO();
			email.setEmailId(1);
			email.setDisplayName("TLT Welcome Email");
			emails.add(email);
		}
		
		Random r = new Random();
		if(r.nextBoolean()) {
			EmailDTO email = new EmailDTO();
			email.setEmailId(2);
			email.setDisplayName("TLT Reminder Email");
			emails.add(email);
		}
		
		if(r.nextBoolean()) {
			EmailDTO email = new EmailDTO();
			email.setEmailId(3);
			email.setDisplayName("TLT Thank You Email");
			emails.add(email);
		}
		
		if(r.nextBoolean()) {
			EmailDTO email = new EmailDTO();
			email.setEmailId(4);
			email.setDisplayName("Generic Reminder Email");
			emails.add(email);
		}
		
		if(r.nextBoolean()) {
			EmailDTO email = new EmailDTO();
			email.setEmailId(5);
			email.setDisplayName("Generic Welcome Email");
			emails.add(email);
		}
		
		return emails;
	}


	/**
	 * Insert a new email "Header" (no text data).  Upon successful completion, this
	 * method returns the same EmailDTO passed in with the new ID and status present.
	 * 
	 * @param sessionUser
	 * @param An EmailDTO object
	 * @return An EmailDTO object (same data, plus ID and status)
	 */
	public static EmailDTO addEmailItem(SessionUserDTO sessionUser, EmailDTO newEmail)
	{
		DataResult dr = null;

		if(newEmail == null)
		{
			return new EmailDTO(new RequestStatus(RequestStatus.RS_ERROR,
					"A valid EmailDTO object is required to create an email."));
		}
		
		// In this iteration, assume that product id and/or client id are valid if present
		if (  newEmail.getRecipientTypeId() == 0
		   || (newEmail.getDisplayName() == null || newEmail.getDisplayName().trim().length() == 0)
		   || (newEmail.getDescription() == null || newEmail.getDescription().trim().length() == 0)
		   || newEmail.getGroupId() == 0)
		{
			return new EmailDTO(new RequestStatus(RequestStatus.RS_ERROR,
					"An EmailDTO object with a recipientTypeId, displayName, description and valid group id is required to create an email."));
		}
		
		// Get the group row
		EmailGroupDTO grp = EmailGroupDataHelper.getGroupInfo(newEmail.getGroupId());
		if (! grp.getStatus().isOk())
		{
			// Oops... return a bad status
			return new EmailDTO(grp.getStatus());
		}
		
		if (grp.getCode().equals(EmailGroupDTO.CLIENT)  &&
			(newEmail.getClientId() == null || newEmail.getClientId().trim().length() == 0))
		{
			return new EmailDTO(new RequestStatus(RequestStatus.RS_ERROR,
					"An e-mail with a group type of 'Client' must have a valid client ID."));
		}
		else if (grp.getCode().equals(EmailGroupDTO.PRODUCT) && newEmail.getProductId() == 0)
		{
			return new EmailDTO(new RequestStatus(RequestStatus.RS_ERROR,
					"An e-mail with a group type of 'Product' must have a valid product ID."));
		}
		if (grp.getCode().equals(EmailGroupDTO.PROD_CLIENT) &&
				((newEmail.getClientId() == null || newEmail.getClientId().trim().length() == 0) ||
						newEmail.getProductId() == 0))
		{
			return new EmailDTO(new RequestStatus(RequestStatus.RS_ERROR,
				"An e-mail with a group type of 'Product-Client' must have both a valid client id and a valid product ID."));
		}
		
		EmailDTO ret = newEmail;
		
//TODO Modify query to eliminate hard-coded "xxxDate" fields once we have a DB with triggers installed
	// This is a test query until we get rights to add triggers.  At that time, only the
	// first two fields will be needed; this query should then be modified to reflect that.
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("INSERT INTO em_email ");
		sqlQuery.append("  (recipientTypeId, displayName, description, ");
		sqlQuery.append("   createdBy,createdDate,modifiedBy,modifiedDate) ");
		sqlQuery.append("  VALUES( ?, ?, ?, ?, GETDATE(), ?, GETDATE())");
		
		DataLayerPreparedStatement dlps = 
			V3DatabaseUtils.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
		if (dlps.isInError())
		{
			ret.setStatus(dlps.getStatus());
			return ret;
		}

		try
		{
			dlps.getPreparedStatement().setLong(1, newEmail.getRecipientTypeId());
			dlps.getPreparedStatement().setString(2, newEmail.getDisplayName());
			dlps.getPreparedStatement().setString(3, newEmail.getDescription());
			dlps.getPreparedStatement().setString(4, sessionUser.getUsername());
			dlps.getPreparedStatement().setString(5, sessionUser.getUsername());

			////dr = V3DatabaseUtils.insert(dlps.getPreparedStatement());
			dr = V3DatabaseUtils.insert(dlps);
			
			ret.setStatus(dr.getStatus());
			switch (ret.getStatus().getStatusCode())
			{
				case RequestStatus.RS_ERROR:
					break;
				case RequestStatus.DBS_DUP_KEY:
					ret.getStatus().setExceptionMessage("Display name (" + newEmail.getDisplayName() + ") already exists.");
					break;
				case RequestStatus.RS_OK:
					ret.setEmailId(dr.getNewId());

					//  insert a group map row into the proper map table
					RequestStatus textRet = insertGroupXref(sessionUser, newEmail, grp);
					if (! textRet.isOk())
					{
						ret.setStatus(textRet);
					}
					break;
				default:
					ret.setStatus(setUnexpectedStatus(ret.getStatus()));
					break;
			}
	
			return ret;
		}
		catch (SQLException ex)
		{
			ret.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
					ex.getErrorCode(), "addEmail:  " + ex.getMessage()));
			return ret;
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}

	
	/**
	 * Get "header" data for a specific email (no text data)
	 * @param emailId
	 * @return An EmailDTO object without any of the
	 * 			related language translations
	 */
	public static EmailDTO getEmailProxy(long emailId)
//		throws Exception
	{

		if(emailId == 0)
		{
			return new EmailDTO((new RequestStatus(RequestStatus.RS_ERROR,
					"A valid emailId is required to fetch the e-mail proxy.")));
		}
		
		DataResult dr = null;
		EmailDTO ret = new EmailDTO();
		
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT em.emailId, ");
		sqlQuery.append("       em.displayName AS emailName, ");
		sqlQuery.append("       em.description AS emailDescription, ");
		sqlQuery.append("       em.recipientTypeId, ");
		sqlQuery.append("       rt.displayName AS recipientTypeName");
		sqlQuery.append("  FROM em_email em ");
		sqlQuery.append("    LEFT JOIN em_recipient_type_lookup rt ON rt.recipientTypeId = em.recipientTypeId ");
		sqlQuery.append("  WHERE em.active = 1 ");
		sqlQuery.append("    AND em.emailId = ?");
		
		DataLayerPreparedStatement dlps = V3DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{			
			ret.setStatus(dlps.getStatus());
			return ret;
		}

		try
		{
			dlps.getPreparedStatement().setLong(1,emailId);
			
			dr = V3DatabaseUtils.select(dlps);
			
			ret.setStatus(dr.getStatus());
			switch (ret.getStatus().getStatusCode())
			{
				case RequestStatus.RS_ERROR:
					break;
				case RequestStatus.DBS_NO_DATA:
					ret.getStatus().setExceptionMessage("Email ID " + emailId + " does not exist.");
					break;
				case RequestStatus.RS_OK:
					ResultSet rs = dr.getResultSet();
					rs.next();
					moveItemData(rs, ret);
					ret.setEmailId(emailId);
					break;
				default:
					ret.setStatus(setUnexpectedStatus(ret.getStatus()));
					break;
			}
	
			return ret;
		}
		catch (SQLException ex)
		{
			ret.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
					ex.getErrorCode(), "getEmailProxy:  " + ex.getMessage()));
			return ret;
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}


	/**
	 * Insert new email text
	 *  Note that this does an insert of the new text only.  It returns the input DTO
	 *  with the new ID and status.  It does nothing to update any buffers or DTOs.
	 *   Such adjustments  should take place in the business layer.
	 *  
	 * @param session User
	 * @param an EmailTextDTO object
	 * @return An EmailTextDTO object with the status and the new ID in it
	 */
	public static EmailTextDTO addEmailText(SessionUserDTO sessionUser, EmailTextDTO emailText)
	{
		DataResult dr = null;	// Used in both the baseline check and the insert

		// Validate input parameters...
		if(  emailText == null)
		{
			return new EmailTextDTO(new RequestStatus(RequestStatus.RS_ERROR,
					"An input EmailTextDTO is required to create an email text."));
		}
		if ( emailText.getEmailId() == 0         
		  || emailText.getLanguageId() == 0
		  || (emailText.getSubjectText() == null || emailText.getSubjectText().trim().length() == 0)
		  || (emailText.getBodyText() == null || emailText.getBodyText().trim().length() == 0))
		{
			emailText.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
					"An emailId, languageId, subjectText and bodyText are required to create an email text."));
			return emailText;
		}

		EmailTextDTO ret = emailText;

		// ...check to see if this is a baseline language...
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT * from em_email_text ");
		sqlQuery.append("  WHERE emailId = " + emailText.getEmailId());
		
		DataLayerPreparedStatement dlps = V3DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			ret.setStatus(dlps.getStatus());
			return ret;
		}

		try
		{
			dr = V3DatabaseUtils.select(dlps);
	
			ret.setStatus(dr.getStatus());
			switch (ret.getStatus().getStatusCode())
			{
				case RequestStatus.RS_ERROR:
					return ret;
				case RequestStatus.RS_OK:
					emailText.setBaseline(false);
					break;
				case RequestStatus.DBS_NO_DATA:
					emailText.setBaseline(true);
					break;
				default:
					ret.setStatus(setUnexpectedStatus(ret.getStatus()));
					return ret;
			}
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
		dlps = null;
		
		// Reset the ReqsestStatus portion of the DTO
		ret.setStatus(new RequestStatus());
		
		// ...now do the actual insert
//TODO Modify query to eliminate hard-coded "xxxDate" fields once we have a DB with triggers installed
		// This is a test query until we get rights to add triggers.  At that time, only the
		// first two fields will be needed; this query should then be modified to reflect that.
		sqlQuery = new StringBuffer();
		sqlQuery.append("INSERT INTO em_email_text ");
		sqlQuery.append("  (emailId, languageId, subjectText, bodyText, baseline, ");
		sqlQuery.append("  createdBy,createdDate,modifiedBy,modifiedDate) ");
		sqlQuery.append("  VALUES( ?, ?, ?, ?, ?, ?, GETDATE(), ?, GETDATE())");
		
		dlps = V3DatabaseUtils.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
		if (dlps.isInError())
		{
			ret.setStatus(dlps.getStatus());
			return ret;
		}

		try
		{
			dlps.getPreparedStatement().setLong(1,emailText.getEmailId());
			dlps.getPreparedStatement().setLong(2,emailText.getLanguageId());
			dlps.getPreparedStatement().setString(3,emailText.getSubjectText());
			dlps.getPreparedStatement().setString(4,emailText.getBodyText());
			dlps.getPreparedStatement().setBoolean(5,emailText.getBaseline());
			dlps.getPreparedStatement().setString(6,sessionUser.getUsername());
			dlps.getPreparedStatement().setString(7,sessionUser.getUsername());
			
			dr = V3DatabaseUtils.insert(dlps);
			
			ret.setStatus(dr.getStatus());
			switch (ret.getStatus().getStatusCode())
			{
				case RequestStatus.RS_ERROR:
					break;
				case RequestStatus.DBS_DUP_KEY:
					ret.getStatus().setExceptionMessage("Text already exists for this Email and Language.  " +
							"Email ID=" + emailText.getEmailId() + 
							"Language ID=" +  emailText.getLanguageId());
					break;
				case RequestStatus.RS_OK:
					ret.setEmailTextId(dr.getNewId());
					break;
				default:
					ret.setStatus(setUnexpectedStatus(ret.getStatus()));
				break;
			}
			
			return ret;
		}
		catch (SQLException ex)
		{
			ret.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
					ex.getErrorCode(), "addEmailText:  " + ex.getMessage()));
			return ret;
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}


	/**
	 * Get an email header and all email texts for a given email -
	 * populate the HashMap<String, EmailTextDTO> _textMap;
	 * 
	 * @param sessionUser
	 * @param  EmailDTOobjject with at least the emailId attribute populated
	 * @return A fully formed EmailDTO object
	 *          with all related translations 
	 */
	public static EmailDTO getEmail(SessionUserDTO sessionUser, EmailDTO inp)
	{
		DataResult dr = null;

		if(    inp == null)
		{
			return new EmailDTO(new RequestStatus(RequestStatus.RS_ERROR,
				"A valid EmailDTO object is required to fetch a complete email object."));
		}
		if (inp.getEmailId() == 0)
		{
			inp.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
				"A valid email Id is required to fetch a complete email object."));
			return inp;
		}

		EmailDTO ret = new EmailDTO();

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT em.emailId, ");
		sqlQuery.append("       em.displayName AS emailName, ");
		sqlQuery.append("       em.description AS emailDescription, ");
		sqlQuery.append("       em.recipientTypeId, ");
		sqlQuery.append("       rtl.displayName AS recipientTypeName, ");
		sqlQuery.append("       emt.emailTextId, ");
		sqlQuery.append("       emt.languageId, ");
		sqlQuery.append("       ll.displayName AS languageName, ");
		sqlQuery.append("       ll.isoCode, ");
		sqlQuery.append("       emt.subjectText, ");
		sqlQuery.append("       emt.bodyText, ");
		sqlQuery.append("       emt.baseline ");
		sqlQuery.append("  FROM em_email em ");
		sqlQuery.append("    LEFT JOIN em_recipient_type_lookup rtl ON rtl.recipientTypeId = em.recipientTypeId ");
		sqlQuery.append("    LEFT JOIN em_email_text emt ON (emt.emailId = em.emailId and emt.active = 1) ");
		sqlQuery.append("    LEFT JOIN language_lookup ll ON ll.languageId = emt.languageId ");
		sqlQuery.append("  WHERE em.active = 1 ");
		sqlQuery.append("    AND em.emailId = ?");

		DataLayerPreparedStatement dlps = V3DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			ret.setStatus(dlps.getStatus());
			return ret;
		}
		
		try
		{
////			dlps.getPreparedStatement().setLong(1,emailId);
			dlps.getPreparedStatement().setLong(1,inp.getEmailId());
			
			dr = V3DatabaseUtils.select(dlps);
			
			ret.setStatus(dr.getStatus());
			switch (ret.getStatus().getStatusCode())
			{
				case RequestStatus.RS_ERROR:
					break;
				case RequestStatus.DBS_NO_DATA:
					ret.getStatus().setExceptionMessage("Email ID " + inp.getEmailId() + " does not exist.");
					break;
				case RequestStatus.RS_OK:
					ResultSet rs = dr.getResultSet();
					rs.next();	// Get first row in result set
					moveItemData(rs, ret);	// Put it into the email item
					// now put data into the text array, but only if there is any
					if (rs.getString("emailTextId") != null)
					{
						while (! rs.isAfterLast())
						{
							EmailTextDTO dto = new EmailTextDTO();
							moveTextData(rs, dto);
							////ret.getTextMap().put("" + dto.getLanguageId(), dto);
							ret.getTextMap().put(dto.getIsoCode(), dto);
							rs.next();
						}
					}
					break;
				default:
					ret.setStatus(setUnexpectedStatus(ret.getStatus()));
					break;
			}
			
			return ret;
		}
		catch (SQLException ex)
		{
			ret.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
					ex.getErrorCode(), "getEmail:  " + ex.getMessage()));
			return ret;
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}	
	

	/**
	 * Get the text for a specific e-mail in a specific language 
	 * 
	 * @param An email Id
	 * @param A language Id
	 * @return An EmailTextDTO object 
	 */
	public static EmailTextDTO getEmailText(long emailId, long langId)
	{
		DataResult dr = null;
		EmailTextDTO ret = new EmailTextDTO();

		if(emailId == 0 || langId == 0)
		{
			ret.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
					"An email ID and a language ID are required to get text for an email."));
			return ret;
		}

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT emt.emailTextId, ");
		sqlQuery.append("       emt.emailId, ");
		sqlQuery.append("       emt.languageId, ");
		sqlQuery.append("       ll.displayName as lanuageName, ");
		sqlQuery.append("       ll.isoCode, ");
		sqlQuery.append("       emt.subjectText, ");
		sqlQuery.append("       emt.bodyText, ");
		sqlQuery.append("       emt.baseline ");
		sqlQuery.append("  FROM em_email_text emt ");
		sqlQuery.append("    LEFT JOIN language_lookup ll ON ll.languageId = emt.languageId ");
		sqlQuery.append("  WHERE emt.active = 1 ");
		sqlQuery.append("    AND emt.emailId = ? ");
		sqlQuery.append("  AND emt.languageId = ?");
		
		DataLayerPreparedStatement dlps = V3DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			ret.setStatus(dlps.getStatus());
			return ret;
		}

		try
		{
			dlps.getPreparedStatement().setLong(1,emailId);
			dlps.getPreparedStatement().setLong(2,langId);
			
			dr = V3DatabaseUtils.select(dlps);

			ret.setStatus(dr.getStatus());
			switch (ret.getStatus().getStatusCode())
			{
				case RequestStatus.RS_ERROR:
					break;
				case RequestStatus.DBS_NO_DATA:
					ret.getStatus().setExceptionMessage("Text for email ID " + emailId +
										" and language ID " + langId + " does not exist.");
					break;
				case RequestStatus.RS_OK:
					ResultSet rs = dr.getResultSet();
					rs.next();
					moveTextData(rs, ret); // loads the emailText data into the dto
					break;
				default:
					ret.setStatus(setUnexpectedStatus(ret.getStatus()));
					break;
			}
	
			return ret;
		}
		catch (SQLException ex)
		{
			ret.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
					ex.getErrorCode(), "getEmailText:  " + ex.getMessage()));
			return ret;
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}	


	/**
	 * updateEmailItem - Updates the "header" portion of an email only
	 *                   To update the text, use the function updatEmailText
	 *
	 *@param sessionUser
	 * @param An EmailDTO object
	 * @return A RequestStatus object
	 */
	public static RequestStatus updateEmailItem(SessionUserDTO sessionUser, EmailDTO email)
	{
		DataResult dr = null;

		if(email == null ||  email.getEmailId() == 0 ||
				( email.getRecipientTypeId() == 0 &&
					(email.getDisplayName() == null || email.getDisplayName().trim().length() == 0) &&
					(email.getDescription() == null || email.getDescription().trim().length() == 0)
				)
		   )
		{
			return new RequestStatus(RequestStatus.RS_ERROR,
					"Update requires a valid EmailDTO object with email ID, and at least one of the following fields:  recipient type ID, name, description.");
		}

		String dn = null;
		String desc = null;
		String setStr = "";
		int index = 0;
		if (email.getRecipientTypeId() != 0)
		{
			index += 1;
			setStr += "recipientTypeId = ?";
		}
		if (email.getDisplayName() != null && email.getDisplayName().trim().length() != 0)
		{
			dn = email.getDisplayName().trim().replace("'","''");
			index += 2;
			if (setStr != null)
			{
				setStr += ", ";
			}
			setStr += "displayName = ?";
		}
		if (email.getDescription() != null && email.getDescription().trim().length() != 0)
		{
			desc = email.getDescription().trim().replace("'","''");
			index += 4;
			if (setStr != null)
			{
				setStr += ", ";
			}
			setStr += "description = ?";
		}

//TODO Modify query to eliminate hard-coded "ModifiedDate" field
//	once we have a DB with triggers installed
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("UPDATE em_email ");
		sqlQuery.append("  SET " + setStr + " ");
		sqlQuery.append(", modifiedBy = ?, modifiedDate = GETDATE()");
		sqlQuery.append("  WHERE emailId = ?");

		DataLayerPreparedStatement dlps = V3DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			return dlps.getStatus();
		}

		try
		{
			switch (index)
			{
				case 1:		// Recipient type only
					dlps.getPreparedStatement().setLong(1, email.getRecipientTypeId());
					dlps.getPreparedStatement().setString(2, sessionUser.getUsername());
					dlps.getPreparedStatement().setLong(3, email.getEmailId());
				case 2:		// Display name only
					dlps.getPreparedStatement().setString(1, dn);
					dlps.getPreparedStatement().setString(2, sessionUser.getUsername());
					dlps.getPreparedStatement().setLong(3, email.getEmailId());
				case 3:		// Recipient type and Display name
					dlps.getPreparedStatement().setLong(1, email.getRecipientTypeId());
					dlps.getPreparedStatement().setString(2, dn);
					dlps.getPreparedStatement().setString(3, sessionUser.getUsername());
					dlps.getPreparedStatement().setLong(4, email.getEmailId());
				case 4:		// Description only
					dlps.getPreparedStatement().setString(1, desc);
					dlps.getPreparedStatement().setString(2, sessionUser.getUsername());
					dlps.getPreparedStatement().setLong(3, email.getEmailId());
				case 5:		// Recipient type and Description
					dlps.getPreparedStatement().setString(1, dn);
					dlps.getPreparedStatement().setString(2, desc);
					dlps.getPreparedStatement().setString(3, sessionUser.getUsername());
					dlps.getPreparedStatement().setLong(4, email.getEmailId());
				case 6:		// Display name and Description
					dlps.getPreparedStatement().setString(1, dn);
					dlps.getPreparedStatement().setString(2, desc);
					dlps.getPreparedStatement().setString(3, sessionUser.getUsername());
					dlps.getPreparedStatement().setLong(4, email.getEmailId());
				case 7:		// Recipient type, Display name and Description
					dlps.getPreparedStatement().setLong(1, email.getRecipientTypeId());
					dlps.getPreparedStatement().setString(2, dn);
					dlps.getPreparedStatement().setString(3, desc);
					dlps.getPreparedStatement().setString(4, sessionUser.getUsername());
					dlps.getPreparedStatement().setLong(5, email.getEmailId());
			}
			dr = V3DatabaseUtils.update(dlps);

			switch (dr.getStatus().getStatusCode())
			{
				case RequestStatus.RS_ERROR:
				case RequestStatus.RS_OK:
					return dr.getStatus();
				default:
					return setUnexpectedStatus(dr.getStatus());
			}
		}
		catch (SQLException ex)
		{
			return new RequestStatus(RequestStatus.RS_ERROR,
					ex.getErrorCode(), "updateEmailItem:  " + ex.getMessage());
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}	


	/**
	 * updateEmailText - Updates a email text.  Note that it is for a single
	 * 					 language in the email.  Multiple language updates
	 * 					 will be handled by the caller with multiple calls
	 * 					 to this function.
	 * NOTE:  The primary key for the email text table is the email text ID,
	 *        and that is what will be used to update the row.  Note also
	 *        that this updates the text for an email and language.  Any
	 *        attempt to change the email or language IDs will be ignored.
	 *        
	 * DO WE NEED AN 'ACTIVE' CHECK?
	 *
	 * @param session user
	 * @param An EmailTextDTO object
	 * @return A RequestStatus object
	 */
	public static RequestStatus updateEmailText(SessionUserDTO sessionUser, EmailTextDTO eText)
	{
		DataResult dr = null;

		if( eText == null ||
			eText.getEmailTextId() == 0 ||
		  ((eText.getSubjectText() == null || eText.getSubjectText().trim().length() == 0) &&
		   (eText.getBodyText() == null    || eText.getBodyText().trim().length() == 0) ) )
		{
			return new RequestStatus(RequestStatus.RS_ERROR,
					"Update requires a validEmailDTO object with an email text ID, and at least one of the following fields: Subject text, Body text.");
		}
		
		String subject = null;
		String body = null;
		String setStr = "";
		int index = 0;
		if (eText.getSubjectText() != null && eText.getSubjectText().trim().length() != 0)
		{
			subject = eText.getSubjectText().trim().replace("'","''");
			index += 1;
			setStr += "subjectText = ?";
		}
		if (eText.getBodyText() != null && eText.getBodyText().trim().length() != 0)
		{
			body = eText.getBodyText().trim().replace("'","''");
			index += 2;
			if (setStr != null)
			{
				setStr += ", ";
			}
			setStr += "bodyText = ?";
		}

//TODO Modify query to eliminate hard-coded "ModifiedDate" field once we have a DB with triggers installed
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("UPDATE em_email_text ");
		sqlQuery.append(" SET " + setStr + " ");
		sqlQuery.append(", modifiedBy = ?, modifiedDate = GETDATE()  ");
		sqlQuery.append("  WHERE emailTextId = ?");

		DataLayerPreparedStatement dlps = V3DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			return dlps.getStatus();
		}

		try
		{
			switch (index)
			{
				case 1:		// Subject only
					dlps.getPreparedStatement().setString(1, subject);
					dlps.getPreparedStatement().setString(2, sessionUser.getUsername());
					dlps.getPreparedStatement().setLong(3, eText.getEmailTextId());
				case 2:		// Body only
					dlps.getPreparedStatement().setString(1, body);
					dlps.getPreparedStatement().setString(2, sessionUser.getUsername());
					dlps.getPreparedStatement().setLong(3, eText.getEmailTextId());
				case 3:		// Both subject and body
					dlps.getPreparedStatement().setString(1, subject);
					dlps.getPreparedStatement().setString(2, body);
					dlps.getPreparedStatement().setString(3, sessionUser.getUsername());
					dlps.getPreparedStatement().setLong(4, eText.getEmailTextId());
			}
			dr = V3DatabaseUtils.update(dlps);

			switch (dr.getStatus().getStatusCode())
			{
				case RequestStatus.RS_ERROR:
					return dr.getStatus();
				case RequestStatus.RS_OK:
					if (dr.getRowCount() > 0)
					{
						return dr.getStatus();
					}
					else
					{
						return new RequestStatus(RequestStatus.DBS_NO_DATA, "No data to update for emailTextId " + eText.getEmailTextId());
					}
				default:
					return setUnexpectedStatus(dr.getStatus());
			}
		}
		catch (SQLException ex)
		{
			return new RequestStatus(RequestStatus.RS_ERROR,
					ex.getErrorCode(), "updateEmailItem:  " + ex.getMessage());
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}	


	/**
	 * deleteEmail - "Delete" an email item and all of its children texts.
	 * NOTE:  This is a logical delete, not a physical one.  We simply
	 *        update the active flag to 0.
	 * 
	 * @param An id
	 * @return A RequestStatus object
	 */
	public static RequestStatus deleteEmail(SessionUserDTO sessionUser, long emailId)
	{
		DataResult dr = null;

		if (emailId == 0)
		{
			return new RequestStatus(RequestStatus.RS_ERROR,
					"Delete email requires a valid Email ID.");
		}

		// first "delete" the children texts
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("UPDATE em_email_text ");
		sqlQuery.append("  SET active = 0, modifiedBy = ? ");
		sqlQuery.append("  WHERE emailId = ?");
		
		DataLayerPreparedStatement dlps = V3DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			return dlps.getStatus();
		}

		try
		{
			dlps.getPreparedStatement().setString(1, sessionUser.getUsername());
			dlps.getPreparedStatement().setLong(2, emailId);

			dr = V3DatabaseUtils.update(dlps);
	
			switch (dr.getStatus().getStatusCode())
			{
				case RequestStatus.RS_ERROR:
					return dr.getStatus();
				case RequestStatus.RS_OK:
					break;
				default:
					return setUnexpectedStatus(dr.getStatus());
			}
		}
		catch (SQLException ex)
		{
			dlps = null;
			return new RequestStatus(RequestStatus.RS_ERROR,
					ex.getErrorCode(), "deleteEmail (text):  " + ex.getMessage());
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
		
		// clean up for the next query
		dlps = null;
		//// Should be unnecessary
		//if (dr != null) { dr.close(); dr = null; }

		// then "delete" the header part of the email
		sqlQuery.setLength(0);
		sqlQuery.append("UPDATE em_email ");
		sqlQuery.append("  SET active = 0, modifiedBy = ?");
		sqlQuery.append("  WHERE emailId = ?");
		
		dlps = V3DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			return dlps.getStatus();
		}

		try
		{
			dlps.getPreparedStatement().setString(1, sessionUser.getUsername());
			dlps.getPreparedStatement().setLong(2, emailId);

			dr = V3DatabaseUtils.update(dlps);

			switch (dr.getStatus().getStatusCode())
			{
				case RequestStatus.RS_ERROR:
				case RequestStatus.RS_OK:
					return dr.getStatus();
				default:
					return setUnexpectedStatus(dr.getStatus());
			}
		}
		catch (SQLException ex)
		{
			return new RequestStatus(RequestStatus.RS_ERROR,
					ex.getErrorCode(), "deleteEmail (item):  " + ex.getMessage());
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}	


	/**
	 * deleteEmailText - "Deletes" (sets active flag to 0) an email text.  Note
	 * that no checks for things like whether the "baseline" language should be
	 * deleted are done here.  At this point we are assuming that the business
	 * layer does that type of logic.  If we need to move some of that type of 
	 * logic into here we can, but let's keep it as low key as possible.
	 * 
	 * @param  An email text id
	 * @return A RequestStatus object
	 */
	public static RequestStatus deleteEmailText(SessionUserDTO sessionUser, long textId)
	{
		DataResult dr = null;

		if (textId == 0)
		{
			return new RequestStatus(RequestStatus.RS_ERROR,
					"Delete email text requires a valid text ID");
		}

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("UPDATE em_email_text ");
		sqlQuery.append("  SET active = 0, modifiedBy = ?");
		sqlQuery.append("  WHERE emailTextId = ?");
		
		DataLayerPreparedStatement dlps = V3DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			return dlps.getStatus();
		}

		try
		{
			dlps.getPreparedStatement().setString(1, sessionUser.getUsername());
			dlps.getPreparedStatement().setLong(2, textId);

			dr = V3DatabaseUtils.update(dlps);
			
			switch (dr.getStatus().getStatusCode())
			{
				case RequestStatus.RS_ERROR:
				case RequestStatus.RS_OK:
					return dr.getStatus();
				default:
					return setUnexpectedStatus(dr.getStatus());
			}
		}
		catch (SQLException ex)
		{
			return new RequestStatus(RequestStatus.RS_ERROR,
					ex.getErrorCode(), "deleteEmailText:  " + ex.getMessage());
		} 
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}	


	/**
	 * UnDelete an email - this is currently only for TESTING!!!!!!
	 * 	Set the active flag on an email item and all of its text children
	 *   Note that this could be dangerous.  ALL texts are restored, including
	 *   texts that may have been legitimately deleted.  That's why it's for testing...
	 * 
	 * @param An emailId
	 * @return A RequestStatus object
	 */
	public static RequestStatus unDeleteEmail(SessionUserDTO sessionUser, long emailId)
	{
		DataResult dr = null;

		if (emailId == 0)
		{
			return new RequestStatus(RequestStatus.RS_ERROR,
					"unDeleteEmail requires a valid Email ID.");
		}

		// first restore the parent
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("UPDATE em_email ");
		sqlQuery.append("  SET active = 1, modifiedBy = ?");
		sqlQuery.append("  WHERE emailId = ?  ");
		
		DataLayerPreparedStatement dlps = V3DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			return dlps.getStatus();
		}

		try
		{
			dlps.getPreparedStatement().setString(1, sessionUser.getUsername());
			dlps.getPreparedStatement().setLong(2, emailId);

			dr = V3DatabaseUtils.update(dlps);
			
			switch (dr.getStatus().getStatusCode())
			{
				case RequestStatus.RS_ERROR:
					return dr.getStatus();
				case RequestStatus.RS_OK:
					break;
				default:
					return setUnexpectedStatus(dr.getStatus());
			}
		}
		catch (SQLException ex)
		{
			return new RequestStatus(RequestStatus.RS_ERROR,
						ex.getErrorCode(), "unDeleteEmail (item):  " + ex.getMessage());
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
		
		// clean up for the next query
		dlps = null;
		//// Should be unnecessary
		//if (dr != null) { dr.close(); dr = null; }
		
		// then un delete the child texts
		sqlQuery.setLength(0);
		sqlQuery.append("UPDATE em_email_text ");
		sqlQuery.append("  SET active = 1, modifiedBy = ?");
		sqlQuery.append("  WHERE emailId = ? ");
		
		dlps = V3DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			return dlps.getStatus();
		}

		try
		{
			dlps.getPreparedStatement().setString(1, sessionUser.getUsername());
			dlps.getPreparedStatement().setLong(2, emailId);

			dr = V3DatabaseUtils.update(dlps);
			
//			ret.setStatus(dr.getStatus());
//			
			switch (dr.getStatus().getStatusCode())
			{
				case RequestStatus.RS_ERROR:
				case RequestStatus.RS_OK:
					return dr.getStatus();
				default:
					return setUnexpectedStatus(dr.getStatus());
			}
		}
		catch (SQLException ex)
		{
			return (new RequestStatus(RequestStatus.RS_ERROR,
					ex.getErrorCode(), "Un-deleteEmail (texts):  " + ex.getMessage()));
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}

	
	/**
	 * Send an email to a single participant.
	 * 
	 * @param sessionUser The user DTO for the current session
	 * @param email The email to send.
	 * @param participants An array of one or more ParticipantDTO objects
	 * @return A status
	 */
	public static RequestStatus sendEmail(SessionUserDTO sessionUser, EmailDTO emailData, ParticipantDTO participant)
	{
		String langKey;
		EmailTextDTO txt = null;
		HashMap<String, String> vars = new HashMap<String, String>();;

		// Do gross parameter checks
		if (emailData == null || emailData.getTextMap().size() < 1)
		{
			return new RequestStatus(RequestStatus.RS_ERROR, "An EmailDTO is required to send an email and it must have at least one EmailTextDTO object");
		}
		
		if (participant == null)
		{
			return new RequestStatus(RequestStatus.RS_ERROR, "A ParticipantDTO is required to send an email");
		}

		// Hold on to the candidate
		CandidateDTO currentCandidate = participant.getCandidate();
		
		// Get the language.  The language is not available in the participant data unless the
		// candidate has been filled out.  This logic allows us to fill out the CandidateDTO
		// if needed, but reuse it if it already filled out.
		langKey = participant.getCandidate().getLanguage();
		if (langKey == null)
		{
			// get a new candidate dto (the one in participant is insufficient) and get the language
			currentCandidate = CandidateDataHelper.getCandidate(sessionUser, participant.getCandidate().getId());
			if (! currentCandidate.getStatus().isOk())
			{
				return currentCandidate.getStatus();
			}
			
			langKey = currentCandidate.getLanguage();
		}
		
		// Extract relevant text
		txt = emailData.getTextMap().get(langKey);
		if (txt == null)
		{
			// look for the baseline text
			for (Iterator<Map.Entry<String, EmailTextDTO>> itr = emailData.getTextMap().entrySet().iterator(); itr.hasNext(); )
			{
				Map.Entry<String, EmailTextDTO> me = itr.next();
				if (me.getValue().getBaseline())
				{
					txt = me.getValue();
					break;
				}
			}
		}
		if (txt == null)
		{
			return new RequestStatus(RequestStatus.DBS_NO_DATA,"Language " + langKey + " not present and no baseline available.");
		}

		// Get a project if needed
		ProjectDTO proj = null;
		//if (txt.getSubjectText().contains(EmailUtils.PROJECT_NAME)  ||
		//		txt.getBodyText().contains(EmailUtils.PROJECT_NAME) ||
		//		txt.getSubjectText().contains(EmailUtils.PART_EXP_URL) ||
		//		txt.getBodyText().contains(EmailUtils.PART_EXP_URL))
		if (txt.getSubjectText().contains(PROJECT_NAME)  ||
				txt.getBodyText().contains(PROJECT_NAME) ||
				txt.getSubjectText().contains(PART_EXP_URL) ||
				txt.getBodyText().contains(PART_EXP_URL))
		{
			// Get the project
			proj = new ProjectDTO();
			proj.setId(participant.getProjectId());
			proj = ProjectDataHelper.getProject(sessionUser, proj);
		}
		
		// Fill out the participations, if needed
		//if (txt.getSubjectText().contains(EmailUtils.PART_EXP_URL) ||
		//		txt.getBodyText().contains(EmailUtils.PART_EXP_URL))
		if (txt.getSubjectText().contains(PART_EXP_URL) ||
				txt.getBodyText().contains(PART_EXP_URL))
		{
			// Get the participations
			if (participant.getParticipations() == null || participant.getParticipations().size() == 0)
			{
				if (participant.getUri() == null)
				{
					// Create one from the project id and the participant id
					participant.setUri("project/"+proj.getId()+"/participant/" + participant.getId());
				}
				participant = ParticipantDataHelper.getParticipant(sessionUser, participant);
			}
		}

		// Recover the Candidate
		participant.setCandidate(currentCandidate);
		
		// Look for each of the keys and save the
		// substitution data in the array if necessary
		//if (txt.getSubjectText().contains(EmailUtils.ID)  ||
		//		txt.getBodyText().contains(EmailUtils.ID))
		if (txt.getSubjectText().contains(ID)  ||
				txt.getBodyText().contains(ID))
		{
			if (participant.getId() != null)
			{
				//vars.put(EmailUtils.ID, participant.getId());
				vars.put(ID, participant.getId());
			}
		}

		//if (txt.getSubjectText().contains(EmailUtils.FIRST_NAME)  ||
		//		txt.getBodyText().contains(EmailUtils.FIRST_NAME))
		if (txt.getSubjectText().contains(FIRST_NAME)  ||
				txt.getBodyText().contains(FIRST_NAME))
		{
			if (participant.getCandidate().getIndividual().getGivenName() != null)
			{
				//vars.put(EmailUtils.FIRST_NAME, participant.getCandidate().getIndividual().getGivenName());
				vars.put(FIRST_NAME, participant.getCandidate().getIndividual().getGivenName());
			}
		}

		//if (txt.getSubjectText().contains(EmailUtils.LAST_NAME)  ||
		//		txt.getBodyText().contains(EmailUtils.LAST_NAME))
		if (txt.getSubjectText().contains(LAST_NAME)  ||
				txt.getBodyText().contains(LAST_NAME))
		{
			if (participant.getCandidate().getIndividual().getFamilyName() != null)
			{
				//vars.put(EmailUtils.LAST_NAME, participant.getCandidate().getIndividual().getFamilyName());
				vars.put(LAST_NAME, participant.getCandidate().getIndividual().getFamilyName());
			}
		}

//TODO This uses the external reference id for the participation id.  If this is wrong, change it!
		//if (txt.getSubjectText().contains(EmailUtils.PART_EXP_URL) ||
		//		txt.getBodyText().contains(EmailUtils.PART_EXP_URL))
		if (txt.getSubjectText().contains(PART_EXP_URL) ||
				txt.getBodyText().contains(PART_EXP_URL))
		{
			// Get the product id
			String prodId = proj.getProductId();
			
			// loop through the participations looking for that product id
			String participationId = null;
			if (participant.getParticipations() != null)
			{
				// capture the associated participation ID
				for (int i=0; i < participant.getParticipations().size(); i++)
				{
					if (prodId.equals(participant.getParticipations().get(i).getProductId()))
					{
						participationId = participant.getParticipations().get(i).getExtId();
						break;
					}
				}
				if (participationId != null)
				{
					// Construct the URL and put it in the array
					String peUrl = PropertyLoader.getProperty("com.pdi.data.v3.application", "participant.experience.url");
					peUrl = peUrl.replace("<pId>", participationId);
					peUrl = peUrl.replace("<lang>", langKey);
					//vars.put(EmailUtils.PART_EXP_URL, peUrl);
					vars.put(PART_EXP_URL, peUrl);
				}
			}
		}

		//if (txt.getSubjectText().contains(EmailUtils.EMAIL_ADDRESS)  ||
		//		txt.getBodyText().contains(EmailUtils.EMAIL_ADDRESS))
		if (txt.getSubjectText().contains(EMAIL_ADDRESS)  ||
					txt.getBodyText().contains(EMAIL_ADDRESS))
		{
			if (participant.getCandidate().getIndividual().getEmail() != null)
			{
				//vars.put(EmailUtils.EMAIL_ADDRESS, participant.getCandidate().getIndividual().getEmail());
				vars.put(EMAIL_ADDRESS, participant.getCandidate().getIndividual().getEmail());
			}
		}

		//if (txt.getSubjectText().contains(EmailUtils.ORGANIZATION)  ||
		//		txt.getBodyText().contains(EmailUtils.ORGANIZATION))
		if (txt.getSubjectText().contains(ORGANIZATION)  ||
				txt.getBodyText().contains(ORGANIZATION))
		{
			if (participant.getCandidate().getOrganization().getName() != null)
			{
				//vars.put(EmailUtils.ORGANIZATION, participant.getCandidate().getOrganization().getName());
				vars.put(ORGANIZATION, participant.getCandidate().getOrganization().getName());
			}
		}

		//if (txt.getSubjectText().contains(EmailUtils.PROJECT_ID)  ||
		//		txt.getBodyText().contains(EmailUtils.PROJECT_ID))
		if (txt.getSubjectText().contains(PROJECT_ID)  ||
				txt.getBodyText().contains(PROJECT_ID))
		{
			if (participant.getProjectId() != null)
			{
				//vars.put(EmailUtils.PROJECT_ID, participant.getProjectId());
				vars.put(PROJECT_ID, participant.getProjectId());
			}
		}

		//if (txt.getSubjectText().contains(EmailUtils.PROJECT_NAME)  ||
		//		txt.getBodyText().contains(EmailUtils.PROJECT_NAME))
		if (txt.getSubjectText().contains(PROJECT_NAME)  ||
				txt.getBodyText().contains(PROJECT_NAME))
		{
			if (proj != null && proj.getName() != null)
			{
				//vars.put(EmailUtils.PROJECT_NAME, proj.getName());
				vars.put(PROJECT_NAME, proj.getName());
			}
		}
		
		// Send the e-mail
		Email email = new Email();
		email.setTo(participant.getCandidate().getIndividual().getEmail());
		email.setFrom(PropertyLoader.getProperty("com.pdi.listener.portal.application", "email.fromAddress"));
		email.setSubject(txt.getSubjectText());
		email.setMessage(txt.getBodyText());
		email.setVariables(vars);

		// Set up the server
		EmailServer server = new EmailServer();
		server.setAddress(PropertyLoader.getProperty("com.pdi.listener.portal.application", "email.server"));

		try
		{
			EmailUtils.send(email, server);
		}
		catch (Exception ex)
		{
			return new RequestStatus(RequestStatus.RS_ERROR,
					"E-mail request failed:  To=" + participant.getCandidate().getIndividual().getEmail() + ", Msg=" + ex.getMessage());
		}
		
		// Success
		return new RequestStatus(RequestStatus.RS_OK, "E-mail request submitted - " + participant.getCandidate().getIndividual().getEmail()); 
	}




	///////////////////////////
	//     Public methods    //
	//  Email Group Lookups  //
	///////////////////////////
	
	
	/**
	 * Get a list of the client related email proxies (global, client, client related P/C)
	 * 
	 * @param sessionUser
	 * @param client A ClientDTO object with at least the ID set
	 * @return A EmailListDTO object
	 */
	public static EmailListDTO getAllProxyList(SessionUserDTO sessionUser)
	{
		return getEmailProxies(sessionUser, EmailGroupDTO.GLOBAL, null, 0);
	}
	
	
	/**
	 * Get a list of the client related email proxies (global, client, client related P/C)
	 * 
	 * @param sessionUser
	 * @param client A ClientDTO object with at least the ID set
	 * @return A EmailListDTO object
	 */
	public static EmailListDTO getClientProxyList(SessionUserDTO sessionUser, ClientDTO client)
	{
		if (client == null || client.getId() == null)
		{
			return new EmailListDTO(new RequestStatus(RequestStatus.RS_ERROR, "Missing Client ID"));
		}
		
		return getEmailProxies(sessionUser, EmailGroupDTO.CLIENT, client.getId(), 0);
	}
	
	
	/**
	 * Get a list of the product related email proxies
	 *  (global, product, product related P/C for the client)
	 * 
	 * @param sessionUser
	 * @param client A ClientDTO object with at least the ID set
	 * @return A EmailListDTO object
	 */
	public static EmailListDTO getProductProxyList(SessionUserDTO sessionUser, ClientDTO client, ProductDTO product)
	{
		if (client == null || client.getId() == null)
		{
			return new EmailListDTO(new RequestStatus(RequestStatus.RS_ERROR, "Missing Client ID"));
		}
		if (product == null || product.getProductId() == 0)
		{
			return new EmailListDTO(new RequestStatus(RequestStatus.RS_ERROR, "Missing Product ID"));
		}
		
		return getEmailProxies(sessionUser, EmailGroupDTO.PRODUCT, client.getId(), product.getProductId());
	}
	
	
	/**
	 * Get a list of the product/client related email proxies
	 *  (global, P/C for the product and client)
	 * 
	 * @param sessionUser
	 * @param client A ClientDTO object with at least the ID set
	 * @param product A ProductDTO object with at least the ID set
	 * @return A EmailListDTO object
	 */
	public static EmailListDTO getProdClientProxyList(SessionUserDTO sessionUser, ClientDTO client, ProductDTO product)
	{
		if (client == null || client.getId() == null)
		{
			return new EmailListDTO(new RequestStatus(RequestStatus.RS_ERROR, "Missing Client ID"));
		}
		if (product == null || product.getProductId() == 0)
		{
			return new EmailListDTO(new RequestStatus(RequestStatus.RS_ERROR, "Missing Product ID"));
		}
		
		return getEmailProxies(sessionUser, EmailGroupDTO.PROD_CLIENT, client.getId(), product.getProductId());
	}

	//
	// Instance data.
	//

	//
	// Constructors.
	//

	//
	// Instance methods.
	//
}
