/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.data.v2.helpers;

import java.io.ByteArrayInputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Random;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.pdi.data.dto.Participant;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.util.RequestStatus;
import com.pdi.data.v2.dto.ModConstsDTO;
import com.pdi.data.v2.dto.ProjectDTO;
import com.pdi.data.v2.dto.SessionUserDTO;
import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.dateTime.DateTimeUtils;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.GroupReport;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;
import com.pdi.scoring.Norm;
import com.pdi.scoring.NormSet;

public class ParticipantDataHelper
{
	private static final Logger log = LoggerFactory.getLogger(ParticipantDataHelper.class);
	//
	//  Static data
	//
	 private static HashMap<String, String> INST_MAP = new HashMap<String, String>();
	{
		INST_MAP.put("IXLGVIOQ", ReportingConstants.RC_GPI);		// US English GPI
		INST_MAP.put("LKXOOMME", ReportingConstants.RC_GPI);		// Chinese GPI
		INST_MAP.put("FFWDUFWY", ReportingConstants.RC_GPI);		// French GPI
		INST_MAP.put("HOGEZINK", ReportingConstants.RC_GPI);		// German GPI
		INST_MAP.put("BMMLZKOK", ReportingConstants.RC_GPI);		// Spanish GPI
		INST_MAP.put("BMZLGGZL", ReportingConstants.RC_LEI);		// US English LEI
		INST_MAP.put("KCIYQGYX", ReportingConstants.RC_LEI);		// Chinese LEI
		INST_MAP.put("DUJKAXPI", ReportingConstants.RC_LEI);		// French LEI
		INST_MAP.put("BHJAYRLB", ReportingConstants.RC_LEI);		// German LEI
		INST_MAP.put("CNBPPCGQ", ReportingConstants.RC_LEI);		// Spanish LEI
		INST_MAP.put("IYJOHVLX", ReportingConstants.RC_RAVENS_SF);	// US English Ravens
		INST_MAP.put("DYMYDGHI", ReportingConstants.RC_RAVENS_SF);	// Chinese Ravens
		INST_MAP.put("ITVTIIPB", ReportingConstants.RC_RAVENS_SF);	// French Ravens
		INST_MAP.put("FDZAJOAY", ReportingConstants.RC_RAVENS_SF);	// German Ravens
		INST_MAP.put("DWWJAAHE", ReportingConstants.RC_RAVENS_SF);	// Spanish Ravens
		INST_MAP.put("HNZYMHDV", ReportingConstants.RC_RAVENS_B);	// Ravens Form B
		INST_MAP.put("IVUVHYKM", ReportingConstants.RC_CAREER_SURVEY);	// US English Career Survey
		INST_MAP.put("ISWEGTQC", ReportingConstants.RC_CAREER_SURVEY);	// Chinese Career Survey
		INST_MAP.put("BKUEDDNI", ReportingConstants.RC_CAREER_SURVEY);	// French Career Survey
		INST_MAP.put("CPXPHWRY", ReportingConstants.RC_CAREER_SURVEY);	// German Career Survey
		INST_MAP.put("FGGQYSFG", ReportingConstants.RC_CAREER_SURVEY);	// Spanish Career Survey
		INST_MAP.put("FFGLQDEG", ReportingConstants.RC_CAREER_SURVEY);	// Vendor Link - Checkbox - AbyD
		INST_MAP.put("FFJUUSBA", ReportingConstants.RC_WG_E);		// Vendor Link - WG-E - NH/Relativity - AbyD
		INST_MAP.put("LLQIPSHN", ReportingConstants.RC_WG_E);		// WG II/WG A - AbyD
	}
	
	// Hard-coded normset IDs
	private static int FLL_NORMSET_ID = 3;
	private static int MLL_NORMSET_ID = 1;
	private static int BUL_NORMSET_ID = 2; // Also called EXEC and Sr. Exec
	private static int SEA_NORMSET_ID = 29;

	// TLT/TPOT2 type code - Used as flag to see if we need to get transition level based norms
	private static String TLT_TYPE = "23";

	//
	// Static methods.
	//

	//
	// Instance data.
	//

	private LinkedHashMap<String, Module> _modMap = null;
	private HashMap<String, Construct> _conMap = null;
	private HashMap<String, NormData> _normDataMap = null;
	private HashMap<String, String> _constructSpssXref = null;

	//
	// Constructors.
	//


	//
	// Instance methods.
	//


	/**
	 * Get some of the ancillary data used in an IndividualReport object
	 * 
	 * @param sessionUser
	 * @param candidate
	 * @param job
	 * @param ret The IndividualReport to be fleshed out
	 * @return an IndividualReport object with more data
	 * @throws Exception
	 */
	public IndividualReport populateIndividualReportData(SessionUserDTO sessionUser,
														  String candidate,
														  String job,
														  IndividualReport ret)
		throws Exception
	{

		//Get Participant object
		Participant participant = HelperDelegate.getParticipantHelper().fromId(HelperDelegate.getSessionUserHelperHelper().createSessionUser(), candidate);

		ret.addDisplayData("FIRST_NAME", participant.getFirstName());
		ret.addDisplayData("LAST_NAME", participant.getLastName());
		ret.getDisplayData().put("PARTICIPANT_NAME", participant.getLastName() + ", " + participant.getFirstName());
		ret.addDisplayData("EMAIL", participant.getEmail());
		// BUS_UNIT AND JOB_TITLE are v2 specific inputs, which have been shifted to 
		// the optional fields in the new admin tools, leaving them, for legacy, for now...
		ret.addDisplayData("BUS_UNIT", participant.getMetadata().get("optional_1"));
		ret.addDisplayData("JOB_TITLE", participant.getMetadata().get("optional_2"));
		// adding optional 1 & 2, because of how they are being saved and requested in the html 
		//  and because job title and business unit are v2 things
		ret.addDisplayData("OPTIONAL_1", participant.getMetadata().get("optional_1"));
		ret.addDisplayData("OPTIONAL_2", participant.getMetadata().get("optional_2"));
		ret.addDisplayData("OPTIONAL_3", participant.getMetadata().get("optional_3"));

		Random generator = new Random();
		int randomIndex = generator.nextInt( 10000 ) * 10000;
		//System.out.println("Participant:  name=" + participant.getFirstName() + " " + participant.getLastName()+ ".  Biz Unit=" + participant.getBusinessUnit());
		String firstName = participant.getFirstName();
		if(firstName == null) {
			firstName = "";
		}
		String lastName = participant.getLastName();
		if(lastName == null) {
			lastName = "";
		}
		String businessUnit = participant.getMetadata().get("OPTIONAL_1");
		if(businessUnit == null) {
			businessUnit = "";
		}
		ret.setName(firstName + "_" + lastName + "_" + businessUnit.replace("/", "_").replace("\\", "_") + "_" + randomIndex + ".pdf");
		//Get Project object
		ProjectDTO project = new ProjectDataHelper().getProject(sessionUser, job);
		if(project != null && project.getClient() != null) {
			ret.addDisplayData("ORGANIZATION", project.getClient().getName());
		}
  		////System.out.println("project... ORGANIZATION  " + project.getClient().getName());
  		ret.addDisplayData("TRANSITION_LEVEL", project.getTransitionLevel());  // "Raw" transition level
  		ret.addDisplayData("PROJECT_NAME", project.getName());
		return ret;
	}


	/**
	 * Get the instrument used by a participant
	 * Currently returns an empty IndividualReport object
	 * 
	 * @param candidateId
	 * @param job
	 * @return an IndividualReport object
	 */
	public IndividualReport getParticipantInstruments(String candidateId, String job)
	{
		IndividualReport ret = new IndividualReport();
		
		return ret;
	}
	
	/**
	 * Get report information from V2 for a person and project
	 * 
	 * @param SessionUserDTO sessionUser - the session user 
	 * @param String candidate - the candidate id
	 * @param String job - the project/job id
	 * @return IndividualReport - the IndividualReport object containing the user data
	 * @throws Exception
	 */
	public IndividualReport getIndividualReportData(SessionUserDTO sessionUser,
													 String candidate,
													 String job)
		throws Exception
	{
		//System.out.println("Report Request For cond " + candidate + " in job " + job);		IndividualReport ret = getIndividualReportScoreData(candidate, job);
		IndividualReport ret = getIndividualReportScoreData(candidate, job);		
		ret = populateIndividualReportData(sessionUser, candidate, job, ret);

		// need to see here if ret knows if there is missing gpi/lei/cs data... 
		if(ret.getReportData().get(ReportingConstants.RC_GPI) == null) {
			//System.out.println("getIndividualreportData-V2:PartDataHlper - no gpi... ");
			ret.addScriptedData("STATUS", "NOT_FINISHED");
			ret.addDisplayData("GPI_MISSING", "MISSING"); 
			ret.getScriptedData().put("ERROR", "GPI has not been completed, unable to generate report");
		}
		if(ret.getReportData().get(ReportingConstants.RC_LEI) == null) {
			//System.out.println("getIndividualreportData-V2:PartDataHlper - no lei... ");
			ret.addScriptedData("STATUS", "NOT_FINISHED");
			ret.addDisplayData("LEI_MISSING", "MISSING"); 
			ret.getScriptedData().put("ERROR", "LEI has not been completed, unable to generate report");
		}
		if(ret.getReportData().get(ReportingConstants.RC_CAREER_SURVEY) == null) {
			//System.out.println("getIndividualreportData-V2:PartDataHlper - no cs... ");
			ret.addScriptedData("STATUS", "NOT_FINISHED");
			ret.addDisplayData("CS_MISSING", "MISSING"); 
			ret.getScriptedData().put("ERROR", "CAREER SURVEY has not been completed, unable to generate report");
		}
		
		

		
		ProjectDTO project = new ProjectDataHelper().getProject(sessionUser, job);

		
  		String str = project.getTPOTTransLevelCurrent();
  		str = (str == null || str.length() < 1) ? calculateCurrentLevel(project.getClient().getId(), Integer.parseInt(project.getTransitionLevel())) : str;
		//System.out.println("CURRENT LEVEL IS : " + str);
		ret.getDisplayData().put("CURRENT_LEVEL", str);
		
		str = project.getTPOTTransLevelTarget();
		str = (str == null || str.length() < 1) ? calculateTargetLevel(project.getClient().getId(), Integer.parseInt(project.getTransitionLevel())) : str;
		//System.out.println("TARGET LEVEL IS : " + str);
		ret.getDisplayData().put("TARGET_LEVEL", str);
		
  		ret.addDisplayData("PROJECT_NAME", project.getName());
   		
  		//System.out.println("JobType=" + project.getType());
  		if (project.getType().equals(TLT_TYPE))
  		{
  			// then get the mean & std dev...
			getIndivTransNormData(ret);
  		}
  		if(project.getCognitivesIncluded().equalsIgnoreCase("2") || project.getCognitivesIncluded().equalsIgnoreCase("4")) {
  			ret.addDisplayData(ReportingConstants.RC_COGNITIVES_INCLUDED, "1");
  			
  		} else {
  			ret.addDisplayData(ReportingConstants.RC_COGNITIVES_INCLUDED, "0");
  		}
		return ret;
	}	// End 'getIndividualReportData' method


	/**
	 * Get all of the score and norm info for a particular person in a particular job
	 * Note that the combination of candidate/job must have valid data in order for
	 * data to be returned
	 * 
	 * Note also that there is another method with the same name but a different
	 * signature that functions differently from this one
	 * 
	 * @param String candidate - candidate Id
	 * @param String job - job/project Id
	 * @return IndividualReport - the IndividualReport object
	 * 
	 */
	public IndividualReport getIndividualReportScoreData(String candidate, String job)  throws Exception
	{	

		IndividualReport ret = null;
		try {
			// Set up the score data objects used
			_modMap = new LinkedHashMap<String, Module>();
			_conMap = new HashMap<String, Construct>();
	
			// Get all norm info and save it for use later in this process
			_normDataMap = getAllNormData();
			_constructSpssXref = getNormXrefData();
			// Get the score data (data resides in instance variables modMap and conMap)
			getScoreData(candidate, job);
			// Move score data to an IndividualReport object
			ret = moveScoreAndNormData();
			
		} catch (Exception e) {
			
		}
		return ret;
	}


	/**
	 * Get all of the score and norm info for a particular person.  If there is score data
	 * present for this person it will be returned.  If multiple scores are available for
	 * a particular construct only the most recent score is returned.
	 * 
	 * Note also that there is another method with the same name but a different
	 * signature that functions differently from this one
	 * 
	 *  @param String candidate - the candidate id
	 *  @return IndividualReport - the IndividualReport object
	 */
	public IndividualReport getIndividualReportScoreData(String candidate)  throws Exception
	{	 
		// Set up the score data objects used
		_modMap = new LinkedHashMap<String, Module>();
		_conMap = new HashMap<String, Construct>();

		// Get all norm info and save it for use later in this process
		_normDataMap = getAllNormData();
		_constructSpssXref = getNormXrefData();
		
		// Get the score data (data resides in instance variables modMap and conMap)
		getScoreData(candidate);
		
		// Move score data to an IndividualReport object
		IndividualReport ret = moveScoreAndNormData();
		
		return ret;
	}


	/**
	 * Get score information from V2 for a person and project and
	 * save it in instance variables.  A subsequent method will
	 * consume the data as appropriate.
	 * 
	 * Note that there is another method with the same name but a
	 * different signature; that one gets the last scores rather
	 * than the ones associated with this project.
	 * 
	 * @param candidate - A V2 candidate (participant) ID
	 * @param job - A V2 job (project) ID
	 */
	private void getScoreData(String candidate, String job)  throws Exception
	{
		// Set up the query.  This query gets both module and construct information, and
		// score data in a union.  The module and construct info is used to construct a
		// series of module and c holder objects, then the score data is inserted into the
		// appropriate construct object.
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT 1 AS typer, ");
		sqlQuery.append("       csv.ModuleID AS modId, ");
		sqlQuery.append("       mm.Name AS modName, ");
		sqlQuery.append("       cc.UniqueIdStamp AS conId, ");
		sqlQuery.append("       cc.Name AS conName, ");
		sqlQuery.append("       csv.spssValue AS conSpss, ");
		sqlQuery.append("       NULL AS createDate, ");
		sqlQuery.append("       NULL AS cScore, ");
		sqlQuery.append("       NULL AS sXml ");
		sqlQuery.append("  FROM job_mod jm ");
		sqlQuery.append("    LEFT JOIN modules mm ON mm.UniqueIdStamp = jm.ModuleID ");
		sqlQuery.append("    LEFT JOIN constructs cc ON (cc.ModuleId = jm.ModuleID AND cc.ModConst = 1) ");
		sqlQuery.append("    LEFT JOIN pdi_construct_spss_value csv ON csv.constructId = cc.UniqueIdStamp ");
		sqlQuery.append("  WHERE jm.JobID = ? ");
		sqlQuery.append("UNION ");
		sqlQuery.append("SELECT 2 AS typer, ");
		sqlQuery.append("       NULL AS modId, ");
		sqlQuery.append("       NULL AS modName, ");
		sqlQuery.append("       rr.ConstructId AS conId, ");
		sqlQuery.append("       NULL AS conName, ");
		sqlQuery.append("       NULL AS conSpss, ");
		sqlQuery.append("       rr.DateCreatedStamp AS createDate, ");
		sqlQuery.append("       rr.CScore AS cScore, ");
		sqlQuery.append("       rr.XML AS sXml ");
		sqlQuery.append("  FROM results rr ");
		sqlQuery.append("  WHERE rr.CandidateId = ? ");
		sqlQuery.append("    AND rr.JobID = ? ");
		sqlQuery.append("ORDER BY typer");
		//System.out.println("396 - PDHlpr.getScoreData sql: " + sqlQuery.toString() + " cand, job " + candidate + ", " + job);
		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			//TODO When this method returns something, this should be modified
			System.out.println("Error preparing getRawScores query." + dlps.toString());
			return;
		}

		DataResult dr = null;
		ResultSet rs = null;

		try
		{
			// Fill sql parmeters with values and get the data
			dlps.getPreparedStatement().setString(1, job);
			dlps.getPreparedStatement().setString(2, candidate);
			dlps.getPreparedStatement().setString(3, job);

			dr = V2DatabaseUtils.select(dlps);
			if (dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
			{
				// No data... return
				System.out.println("ParticipantDataHelper - 419 - NO JOB/SCORE DATA - partID=" + candidate + ", projID=" + job);
				return;
			}
			
			// process the returned data
			rs = dr.getResultSet();
			
			Module curMod;
			Construct curCon;
			
			while (rs.next())
			{
				int type = rs.getInt("typer");
				if (type == 1)	// Module and construct info
				{
					// build module if needed
					String modId = rs.getString("modId");
					if (modId == null)
					{
						// Ignore the items with a modId of null - the construct is not in csv
						continue;
					}
					curMod = _modMap.get(modId);
					if (curMod == null)
					{
						// make one and stick it in the map
						curMod = new ParticipantDataHelper.Module();
						curMod.setId(modId);
						curMod.setName(rs.getString("modName"));
						_modMap.put(modId,curMod);
						//System.out.println(" module name: " + curMod.getName());
					}

					// Build the construct, if needed
					String conId = rs.getString("conId");
					curCon = _conMap.get(conId);
					if (curCon == null)
					{
						// Make a new one
						curCon = new ParticipantDataHelper.Construct();
						curCon.setId(conId);
						curCon.setName(rs.getString("conName"));
						curCon.setSpss(rs.getString("conSpss"));
						_conMap.put(conId, curCon);
						

					}

					// Now put the construct id in the list in the module
					curMod.addConId(conId);
				}  else  {
					// must be type 2 (score data)
					// Find the construct
					String conId = rs.getString("conId");
					curCon = _conMap.get(conId);
					if (curCon == null)
					{
						//System.out.println("Construct ID " + conId + " does not exist.  Score entry skipped.");
						continue;
					}

					curCon.setTempDate(rs.getDouble("createDate"));
					//System.out.println(curCon.getName() + " - " + rs.getString("cScore"));
					curCon.setCScore(rs.getString("cScore"));
					curCon.setXml(rs.getString("sXml"));
					getNormIdsAndPctls(curCon);
				}	// End else (data IS type 2)
			}	// End while loop

			//dr.close();

			// print stuff out here (used for testing)
			//for (Iterator<Module> itr=modMap.values().iterator(); itr.hasNext();  )
			//{
			//	Module mod = itr.next();
			//	//System.out.println(mod.genStr(conMap));	// All construct data
			//	System.out.println(mod.toString());	// Just the modules and constructs
			//}

			return;	// done
		}
		catch (SQLException ex)
		{
			//TODO When this method returns something, this should be modified
			// to do something real here.  Be sure you pass the status back
			return;
		}
		finally
		{
			if(dr != null) { dr.close(); dr = null; }
		}
	}	// End 'getScoreData(candidateId, jobId)' method


	/**
	 * Get score information from V2 for a person and save it in instance
	 * variables.  Data may come from any project in which the person has
	 * participated but the latest score will be the one used.  A
	 * subsequent method will consume the data as appropriate.
	 * 
	 * Note that there is another method with the same name but a
	 * different signature; that one gets the scores for the project
	 * (not necessarily the latest).
	 * 
	 * @param candidate - A V2 candidate (participant) ID
	 *
	 */
	private void getScoreData(String candidate) throws Exception
	{
	
		// Set up the query.  
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT csv.moduleId AS modId, ");
		sqlQuery.append("       mm.Name AS modName, ");
		sqlQuery.append("       rr.ConstructId AS conId, ");
		sqlQuery.append("       cc.Name conName, ");
		sqlQuery.append("       csv.spssValue AS conSpss, ");
		sqlQuery.append("       rr.DateCreatedStamp AS createDate, ");
		sqlQuery.append("       rr.CScore AS cScore, ");
		sqlQuery.append("       rr.XML AS sXml ");
		sqlQuery.append("  FROM results rr ");
		sqlQuery.append("    LEFT JOIN constructs cc ON cc.UniqueIdStamp = rr.ConstructId ");
		sqlQuery.append("    LEFT JOIN pdi_construct_spss_value csv ON csv.constructId = rr.ConstructId ");
		sqlQuery.append("    LEFT JOIN modules mm on mm.UniqueIdStamp = csv.moduleId ");
		sqlQuery.append("  WHERE rr.CandidateId = ? ");
		sqlQuery.append("  ORDER BY conSpss, createDate ");
		//System.out.println(" part.data.helper.. 543  sql: " + sqlQuery.toString() + " candidate: " + candidate);
		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			return;
		}

		DataResult dr = null;
		ResultSet rs = null;

		try
		{
			// Fill SQL parameter with a value and get the data
			dlps.getPreparedStatement().setString(1, candidate);

			dr = V2DatabaseUtils.select(dlps);
			if (dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
			{
				// No data... return
				//System.out.println("NO JOB/SCORE DATA - partID=" + candidate);
				return;
			}
			
			// process the returned data
			rs = dr.getResultSet();
			
			Module curMod;
			Construct curCon;
			
			while (rs.next())
			{
				String modId = rs.getString("modId");
				if (modId == null)
				{
					// Skip it
					continue;
				}
				
				// build module if needed
				curMod = _modMap.get(modId);
				if (curMod == null)
				{
					// make one and stick it in the map
					curMod = new ParticipantDataHelper.Module();
					curMod.setId(modId);
					curMod.setName(rs.getString("modName"));
					_modMap.put(modId,curMod);
				}
				//System.out.println("Mod=" + curMod.getName());

				// Build the construct, if needed
				String conId = rs.getString("conId");
				curCon = _conMap.get("conId");
				if (curCon == null)
				{
					// Make a new one
					curCon = new ParticipantDataHelper.Construct();
					curCon.setId(conId);
					curCon.setName(rs.getString("conName"));
					curCon.setSpss(rs.getString("conSpss"));
					//System.out.println("spss val = " + curCon.getName() + ",  " + curCon.getSpss());
					_conMap.put(conId, curCon);
				}

				// Now put the construct id in the list in the module
				curMod.addConId(conId);
		
				// and add the score data
				curCon.setTempDate(rs.getDouble("createDate"));
				curCon.setCScore(rs.getString("cScore"));
				curCon.setXml(rs.getString("sXml"));
			}	// End while loop
			
			// OK, now parse out the appropriate norms for each construct
			for (Iterator<Construct> itr=_conMap.values().iterator(); itr.hasNext();  )
			{
				Construct cst = itr.next();
				getNormIdsAndPctls(cst);
			}

//			//// print stuff out here (used for testing)
//			for (Iterator<Module> itr = _modMap.values().iterator(); itr.hasNext();  )
//			{
//				Module mod = itr.next();
//				//System.out.println(mod.genStr(conMap));	// All construct data
//				System.out.println(mod.toString());	// Just the modules and constructs
//			}

			return;	// done
		}
		catch (SQLException ex)
		{
			//TODO When this method returns something, this should be modified
			// to do something real here.  Be sure you pass the status back
			System.out.println("Error processing getScoreData(partId) query or ResultSet." +
					"  code="+ ex.getErrorCode() +
					", msg=" + ex.getMessage());
			return;
		}
		finally
		{
			if(dr != null) { dr.close(); dr = null; }
		}
	}	// End 'getScoreData(candidateId)' method
	

	/**
	 * Determines the target level by client id
	 * currently this data is not in the database, this was
	 * previously hard coded on the flash application
	 * TODO: Fix, place in a database, xml, provide a UI or somehow remove it from the java code
	 * 
	 * @param clientId
	 * @param transitionLevel
	 * @return
	 */
	private String calculateTargetLevel(String clientId, int transitionLevel)
	{
		//System.out.println("Calc Target Level " + clientId + " vs " + transitionLevel);
		/*		
		    <TargetLevel>
		        <clientId name = "default">
		            <IC-FLL>First-Level Leader</IC-FLL>
		            <IC-MLL>Mid-Level Leader</IC-MLL>
		            <FLL-MLL>Mid-Level Leader</FLL-MLL>
		            <FLL-EXEC>Senior Executive</FLL-EXEC>
		            <MLL-EXEC>Senior Executive</MLL-EXEC>
		        </clientId>
		        <!--Microsoft Labels  Microsoft certify clientID = "CNUMECEZ" -->
		        <!--Microsoft Labels  Microsoft PROD clientID = "DVYADYPX" -->
		        <!--for testing: Generic TPOT  clientID = "IYBWJXCM" -->
		        <clientId name = "DVYADYPX">
		            <IC-FLL>First-Level Leader</IC-FLL>
		            <IC-MLL>Mid-Level Leader</IC-MLL>
		            <FLL-MLL>Mid-Level Leader</FLL-MLL>
		            <FLL-EXEC>Senior Leader</FLL-EXEC>
		            <MLL-EXEC>Senior Leader</MLL-EXEC>
		        </clientId>
		        <!--Land o Lakes  certify clientId = CPGAQGEN -->
		        <!--Land o Lakes  production clientId = COBMKEQV -->
		        <clientId name = "COBMKEQV">
		            <IC-FLL>First-Level Leader</IC-FLL>
		            <IC-MLL>Manager or Senior Leader</IC-MLL>
		            <FLL-MLL>Manager or Senior Leader</FLL-MLL>
		            <FLL-EXEC>Senior Executive</FLL-EXEC>
		            <MLL-EXEC>Senior Executive</MLL-EXEC>
		        </clientId>
		        <!--Children's Hospital   certify clientId = KAKBWFBH -->
		        <!--Children's Hospital   production clientId = DVALWHTK -->
		        <clientId name = "DVALWHTK">
		            <IC-FLL>First-Level Leader</IC-FLL>
		            <IC-MLL>Director</IC-MLL>
		            <FLL-MLL>Director</FLL-MLL>
		            <FLL-EXEC>Senior Leader</FLL-EXEC>
		            <MLL-EXEC>Senior Leader</MLL-EXEC>
		        </clientId>
		        <!--Electrolux   certify clientId = FBBJQOIW -->
		        <!--Electrolux   production clientId = KEAKFPQB -->
		        <clientId name = "KEAKFPQB">
		            <IC-FLL>First-Level Leader</IC-FLL>
		            <IC-MLL>Mid-Level Leader</IC-MLL>
		            <FLL-MLL>Mid-Level Leader</FLL-MLL>
		            <FLL-EXEC>Senior Executive</FLL-EXEC>
		            <MLL-EXEC>Business Unit Leader</MLL-EXEC>
		        </clientId>
		    </TargetLevel>
		 */
		
		if(clientId.equalsIgnoreCase("DVYADYPX") || clientId.equalsIgnoreCase("CNUMECEZ")) {
			if(transitionLevel == 1) {
				return "First-Level Leader";
			} else if(transitionLevel == 2) {
				return "Mid-Level Leader";
			} else if(transitionLevel == 3) {
				return "Mid-Level Leader";
			} else if(transitionLevel == 4) {
				return "Senior Leader";
			} else if(transitionLevel == 5) {
				return "Senior Leader";
			}
			
		} else if(clientId.equalsIgnoreCase("COBMKEQV") || clientId.equalsIgnoreCase("CPGAQGEN")) {		
			if(transitionLevel == 1) {
				return "First-Level Leader";
			} else if(transitionLevel == 2) {
				return "Manager or Senior Leader";
			} else if(transitionLevel == 3) {
				return "Manager or Senior Leader";
			} else if(transitionLevel == 4) {
				return "Senior Executive";
			} else if(transitionLevel == 5) {
				return "Senior Executive";
			}
			
		} else if(clientId.equalsIgnoreCase("DVALWHTK") || clientId.equalsIgnoreCase("KAKBWFBH")) {
			if(transitionLevel == 1) {
				return "First-Level Leader";
			} else if(transitionLevel == 2) {
				return "Director";
			} else if(transitionLevel == 3) {
				return "Director";
			} else if(transitionLevel == 4) {
				return "Senior Leader";
			} else if(transitionLevel == 5) {
				return "Senior Leader";
			}
			
		} else if(clientId.equalsIgnoreCase("KEAKFPQB") || clientId.equalsIgnoreCase("FBBJQOIW")) {
			if(transitionLevel == 1) {
				return "First-Level Leader";
			} else if(transitionLevel == 2) {
				return "Mid-Level Leader";
			} else if(transitionLevel == 3) {
				return "Mid-Level Leader";
			} else if(transitionLevel == 4) {
				return "Senior Executive";
			} else if(transitionLevel == 5) {
				return "Business Unit Leader";
			}
			
		} else {
			if(transitionLevel == 1) {
				return "First-Level Leader";
			} else if(transitionLevel == 2) {
				return "Mid-Level Leader";
			} else if(transitionLevel == 3) {
				return "Mid-Level Leader";
			} else if(transitionLevel == 4) {
				return "Senior Executive";
			} else if(transitionLevel == 5) {
				return "Senior Executive";
			}
		}
	
		return "N/A";
	}
	
	
	/**
	 * Determines the target level by client id
	 * currently this data is not in the database, this was
	 * previously hard coded on the flash application
	 * TODO: Fix, place in a database, xml, provide a UI or somehow remove it from the java code
	 * 
	 * @param clientId
	 * @param transitionLevel
	 * @return
	 */
	private String calculateCurrentLevel(String clientId, int transitionLevel) {
		
		/*
		<?xml version="1.0" encode="UTF-8"?>
		<!DOCTYPE data[
		    <!ELEMENT clientId(IC-FLL, IC-MLL, FLL-MLL, FLL-EXEC, MLL-EXEC)>
		    <!ATTLIST clientId name CDATA #REQUIRED>
		    <!ELEMENT IC-FLL (#PCDATA)>
		    <!ELEMENT IC-MLL (#PCDATA)>
		    <!ELEMENT FLL-MLL (#PCDATA)>
		    <!ELEMENT FLL-EXEC (#PCDATA)>
		    <!ELEMENT MLL-EXEC (#PCDATA)>
		]>
		<data>
		    <CurrentLevel>
		        <clientId name = "default">
		            <IC-FLL>Individual Contributor</IC-FLL>
		            <IC-MLL>Individual Contributor</IC-MLL>
		            <FLL-MLL>First-Level Leader</FLL-MLL>
		            <FLL-EXEC>First-Level Leader</FLL-EXEC>
		            <MLL-EXEC>Mid-Level Leader</MLL-EXEC>
		        </clientId>
		        <!--Land o Lakes  certify clientId = CPGAQGEN -->
		        <!--Land o Lakes  production clientId = COBMKEQV -->
		        <clientId name = "CPGAQGEN">
		            <IC-FLL>Individual Contributor</IC-FLL>
		            <IC-MLL>Individual Contributor</IC-MLL>
		            <FLL-MLL>First-Level Leader</FLL-MLL>
		            <FLL-EXEC>First-Level Leader</FLL-EXEC>
		            <MLL-EXEC>Manager or Senior Leader</MLL-EXEC>
		        </clientId>
		        <!--Children's Hospital  certify clientId = KAKBWFBH -->
		        <!--Children's Hospital production clientId = DVALWHTK -->
		        <clientId name = "DVALWHTK">
		            <IC-FLL>Individual Contributor</IC-FLL>
		            <IC-MLL>Individual Contributor</IC-MLL>
		            <FLL-MLL>First-Level Leader</FLL-MLL>
		            <FLL-EXEC>First-Level Leader</FLL-EXEC>
		            <MLL-EXEC>Director</MLL-EXEC>
		        </clientId>
		    </CurrentLevel>
		</data> 	
		 */
		
		if(clientId.equalsIgnoreCase("CPGAQGEN") || clientId.equalsIgnoreCase("COBMKEQV")) {
			if(transitionLevel == 1) {
				return "Individual Contributor";
			} else if(transitionLevel == 2) {
				return "Individual Contributor";
			} else if(transitionLevel == 3) {
				return "First-Level Leader";
			} else if(transitionLevel == 4) {
				return "First-Level Leader";
			} else if(transitionLevel == 5) {
				return "Manager or Senior Leader";
			}
			
		} else if(clientId.equalsIgnoreCase("DVALWHTK") || clientId.equalsIgnoreCase("KAKBWFBH")) {		
			if(transitionLevel == 1) {
				return "Individual Contributor";
			} else if(transitionLevel == 2) {
				return "Individual Contributor";
			} else if(transitionLevel == 3) {
				return "First-Level Leader";
			} else if(transitionLevel == 4) {
				return "First-Level Leader";
			} else if(transitionLevel == 5) {
				return "Director";
			}
			
		} else {
			if(transitionLevel == 1) {
				return "Individual Contributor";
			} else if(transitionLevel == 2) {
				return "Individual Contributor";
			} else if(transitionLevel == 3) {
				return "First-Level Leader";
			} else if(transitionLevel == 4) {
				return "First-Level Leader";
			} else if(transitionLevel == 5) {
				return "Mid-Level Leader";
			}
		}
		
		return "N/A";
	}

	
	


	/**
	 * getNormedScores the normed scores and the norms from 
	 * the results xml and puts them into the passed object. 
	 * 
	 * The XML data format (From cand=KEYVOFZF, const=KAYHKNZU, job=HROUXIU) is:
	 *     <?xml version=""1.0"" encoding=""UTF-8""?>
	 *     <XML_Fields>
	 *       <CScore.S0>4
	 *         <Norms>
	 *           <DZYBBWPO></DZYBBWPO>...
	 *         </Norms>
	 *       </CScore.S0>
	 *       <DZYBBWPO>0.0199999999999977973</DZYBBWPO>....
	 *     </XML_Fields>
	 * 
	 */
	private void getNormIdsAndPctls(Construct cst) throws Exception
	{
		// Parse the norm ids out of the xml.  The evaluate methods
		// in the XPath and XPathExpression interfaces are used to
		// parse an XML document with XPath expressions.  The
		// XPathFactory class is used to create an XPath object. 
		// Create an XPathFactory object with the  newInstance 
		// method of the XPathFactory class. 
		XPathFactory  xpFactory = XPathFactory.newInstance();

		//Create an XPath object from the XPathFactory object with the newXPath method.
		XPath myXPath = (XPath) xpFactory.newXPath();

		InputSource is;
		
		//System.out.println("Construct (B4) -" + cst.toString());
		String xml = cst.getXml();
		try
		{
			boolean gotGP = false;
			boolean gotSP = false;
			// Go to the 'Norms' node
			String expression = "/XML_Fields/CScore.S0/Norms";
			is = new InputSource(new ByteArrayInputStream(xml.getBytes()));
			NodeList nodes = null;
			nodes = (NodeList) myXPath.evaluate(expression, is, XPathConstants.NODESET);
			if (nodes == null || nodes.getLength() < 1)
			{
				// return with no additional data
				//System.out.println("  No nodes... done");
				return;
			}

			// The node called "Norms"
			Node node = nodes.item(0);
			NodeList nlist = node.getChildNodes();
				
			// Get all of the norms in the 'Norms' node and their scores...
			for (int i=0; i < nlist.getLength(); i++)
			{

				Node normNode = nlist.item(i);
				String normId = normNode.getNodeName();
				
				// use the normId to get the normed score value.... 
				XPathExpression  xPathExpression =   myXPath.compile("/XML_Fields/" + normId);
				is = new InputSource(new ByteArrayInputStream(xml.getBytes()));
				String scoreStr =  xPathExpression.evaluate(is);
				
				// get the name and type data
				NormData nd = _normDataMap.get(normId);
				if (nd == null)
				{
					//System.out.println("  No norm for " + normId + "... Done");
					return;
				}
				if ( ! gotGP && nd.isGenPop())
				{
					// put the first GP into the object
					cst.setGpNormName(nd.getName());
					cst.setGpNormId(nd.getId());
					cst.setGpPctl(scoreStr);
					gotGP = true;
				}
				else if ( ! gotSP && nd.isSpecPop())
				{
					// put the first SP into the object
					cst.setSpNormName(nd.getName());
					cst.setSpNormId(nd.getId());
					cst.setSpPctl(scoreStr);
					gotSP = true;
				}
				if (gotGP && gotSP)
				{
					
					return;
				}
			}
			
			//System.out.println("Dropped thru... Something missing:  " + cst.toString());
			return;
		}
		catch (XPathExpressionException ex)
		{
			System.out.println("ParticipantDataHelper: Unable to parse norm data from " + xml);
			ex.printStackTrace();
			throw new Exception("Unable to parse norm data from " + xml);
		}
	}


	/*
	 * Put the score and norm info into an IndividualReport object
	 * Get any instruments/modules that exist in the INST_MAP and
	 * move them to an individualReport object (which gets returned) 
	 */
	private IndividualReport moveScoreAndNormData()
	{	
		IndividualReport ret = new IndividualReport();

		for (Iterator<String> itr=INST_MAP.keySet().iterator(); itr.hasNext();  )
		{
			Module curMod = _modMap.get(itr.next());
			if (curMod == null)
			{
				// Skip it if it's not on our list
				continue;
			}
			
			// This is a live one... create the ReportData object
			//System.out.println("Got one... curMod=" + curMod.toString());
			ReportData rd = new ReportData();
			
			// Move the construct score data across as needed.
			// Note that this is a straight move and only works for constructs
			// that are present in the pdi_construct_spss_value table.  Some
			// of the derived values may not be present there and may have to
			// be added.  Note also that this assumes that the appropriate
			// value is present in cScore.  If that is not the case, this won't
			// work.  If there are values present in the XML, we can extract
			// them and put them in here but it won't be automatic like this.
			
			String gpNormId = null;
			String spNormId = null;
			double tDate = 0.0;
			for (String conId : curMod.getConIds())
			{
				Construct curCon = _conMap.get(conId);
				//System.out.println("Got one... curCon=" + curCon.toString());
				String spss = curCon.getSpss();
				if (spss != null)
				{
					// save the scale name
					if (curCon.getName() != null) {
						rd.addDisplayData(spss + "_SCALE_NAME", curCon.getName());
						//System.out.println("SPSS = " + spss +  " constructname: " + curCon.getName()  + "  score:  " + curCon.getCScore());
					}
					// save the Admin date
					if (curCon.getTempDate() > tDate)
						tDate = curCon.getTempDate();
					// Raw scale score
					if (curCon.getCScore() != null)
						rd.addScoreData(spss, curCon.getCScore());
					// GP scale pctl & rating
					if (curCon.getGpPctl() != null && !curCon.getGpPctl().equalsIgnoreCase(""))
					{
						rd.addScoreData(spss + "_GP_PCTL", curCon.getGpPctl());
						rd.addScoreData(spss + "_GP_RATING", Norm.calcPdiRating(Double.parseDouble(curCon.getGpPctl())));
					}
					// SP scale pctl & rating
					if (curCon.getSpPctl() != null && !curCon.getSpPctl().equalsIgnoreCase(""))
					{
						rd.addScoreData(spss + "_SP_PCTL", curCon.getSpPctl());
						rd.addScoreData(spss + "_SP_RATING", Norm.calcPdiRating(Double.parseDouble(curCon.getSpPctl())));
					}
					// Mod level info - Use the first 3 char of the SPSS key
					// to make a unique identifier.  This assumes that the
					// first three characters of the SPSS value/key are unique
					// and uniquely identify the module.  Moves the name & id multiple times
					String modStr = spss.substring(0,3);
					if (modStr != null)
					{
						ret.addDisplayData("MODULE_ID", modStr);
					}
					
					if (curCon.getGpNormName() != null)
					{
						if (gpNormId != null && ! gpNormId.equals(curCon.getGpNormId()))
						{
							// Warning message
							System.out.println("Multiple GP Norms: modStr=" + modStr + ", prev NormId=" + gpNormId + ", new NormId=" + curCon.getGpNormId());
						}
						gpNormId = curCon.getGpNormId();
						rd.addScoreData(modStr + "_GP_NAME", curCon.getGpNormName());
					}
					if (curCon.getSpNormName() != null)
					{
						if (spNormId != null && ! spNormId.equals(curCon.getSpNormId()))
						{
							// Warning message
							System.out.println("Multiple SP Norms: modStr=" + modStr + ", prev NormId=" + spNormId + ", new NormId=" + curCon.getSpNormId());
						}
						spNormId = curCon.getSpNormId();
						rd.addScoreData(modStr + "_SP_NAME", curCon.getSpNormName());
					}
				}
			}
			
			// Admin date
			Date createDate = null;
			String dateStr = null;
			try
			{
				createDate = DateTimeUtils.getNormalDateStringFromHRADate("" + tDate);
				////SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss zzz");
				SimpleDateFormat format = new SimpleDateFormat("MMMMMMMM d, yyyy");
				dateStr = format.format(createDate);
			}
			catch (Exception ex)
			{
				//System.out.println("Unable to convert date... msg=" + ex.getMessage());
				dateStr = "N/A";
			}
			rd.addDisplayData("ADMIN_DATE", dateStr);
			
			// We have the score and percentiles for each scale in this instrument
			// now we need to put the GP and SP normsets out as well
			addNormsetToData(gpNormId, "GP", rd);
			addNormsetToData(spNormId, "SP", rd);

			// Put the data for this module out to the IndividualReport object
			if (rd.getScoreData().size() > 0)
			{
				// Only put it out if there is data to use downstream
				String modKey = INST_MAP.get(curMod.getId());
				ret.getReportData().put(modKey, rd);
				//System.out.println("reportData modKey = " + modKey);
			}
		}
		
		return ret;
	}	// End 'moveScoreAndNormData' method


	/**
	 * Move a normset (mean/stddev for each norm) to the given ReportData object
	 * @param normId - Normset Id to move
	 * @param type - Normset type (GP or SP)
	 * @param rd - ReportData object to fill
	 */
	private void addNormsetToData(String normId, String type, ReportData rd)
	{
		//System.out.println("addNormsetToData: ID=" + normId + ", type=" + type);
		if (type == null)
		{
			//System.out.println("addNormsetToData:  type is null.  No data moved.");
			return;
		}
		if (normId == null)
		{
			// We call this for Both SP and GP for all mods.
			// Some modules legitimately have only one or the other.
			// Don't put out a message (just return)
			////System.out.println("addNormsetToData:  norm ID is null.  No data moved for type " + type);
			return;
		}
		if (rd == null)
		{
			//System.out.println("addNormsetToData: RD is null.  No data moved for type " + type);
			return;
		}
		
		// Get the normset XML and parse out the Constructs (includes mean & std deviation)
		
		// Parse the construct data out of the xml.  The evaluate methods
		// in the XPath and XPathExpression interfaces are used to parse
		// an XML document with XPath expressions.  The XPathFactory class
		// is used to create an XPath object.  Create an XPathFactory object
		// with the  newInstance method of the XPathFactory class. 
		XPathFactory  xpFactory = XPathFactory.newInstance();

		//Create an XPath object from the XPathFactory object with the newXPath method.
		XPath myXPath = (XPath) xpFactory.newXPath();

		InputSource is;
		
		String xml = _normDataMap.get(normId).getXml();
		try
		{
			// Go to the 'Constructs' node
			String expression = "/XML_Fields/Constructs";
			is = new InputSource(new ByteArrayInputStream(xml.getBytes()));
			NodeList nodes = null;
			nodes = (NodeList) myXPath.evaluate(expression, is, XPathConstants.NODESET);
			if (nodes == null || nodes.getLength() < 1)
			{
				// return with no additional data
				return;
			}

			// The node called "Constructs"
			Node node = nodes.item(0);
			NodeList nlist = node.getChildNodes();
				
			// Get all of the "C" nodes
			for (int i=0; i < nlist.getLength(); i++)
			{
				Node cNode = nlist.item(i);
				
				NamedNodeMap nnm = cNode.getAttributes();
				String mean =  nnm.getNamedItem("Avg").getNodeValue();
				if(mean == null || mean.length() < 1)
				{
					continue;
				}
				String stDev = nnm.getNamedItem("StdDev").getNodeValue();
				if(stDev == null || stDev.length() < 1)
				{
					continue;
				}
				String cid = nnm.getNamedItem("Id").getNodeValue();
				if(cid == null || cid.length() < 1)
				{
					continue;
				}
				String spss = _constructSpssXref.get(cid);
				if(spss == null || spss.length() < 1)
				{
					continue;
				}
				//System.out.println("Mean=" + mean + ", stdev=" + stDev + ", id=" + cid + ", spss=" + spss);
				
				// Put the data into the rd
				rd.getScoreData().put(spss + "_" + type + "_SCALE_MEAN", mean);
				rd.getScoreData().put(spss + "_" + type + "_SCALE_STDEV", stDev);
			}
			
			return;
		}
		catch (XPathExpressionException ex)
		{
			System.out.println("Unable to parse construct data from " + xml);
			return;
		}
	}	// End 'addNormsetToData' method


	/*
	 * Get norm information for an individual
	 * @param ir - An IndividualReport object to be updated with norm data
	 */
	public void getIndivTransNormData(IndividualReport ir)
	{
		// get the transition level
  		String tLvl = ir.getDisplayData("TRANSITION_LEVEL");	// ""Raw" transition level
  		if (tLvl == null || tLvl.length() < 1)
  		{
  			System.out.println("Unable to extract transition level");
  			return;
  		}

  		int indivNormsetId = getIndivNormsetId(tLvl);	// Destination level adjusted transition level

		NormSet normset;
		normset = getPdiTransitionNormset(indivNormsetId);
		normset = populateNormset(normset);

		for(Norm norm : normset.getNorms())
		{
			//System.out.println("NORMNAMES:  " + norm.getName()+ "_MEAN  " + norm.getMean());
			ir.addDisplayData(norm.getName()+ "_MEAN", (new Double(norm.getMean())).toString());
			ir.addDisplayData(norm.getName()+ "_STDEV", (new Double(norm.getStdDev())).toString());
		}
	}


	/*
	 * Fetches a normset ID for an individual report
	 * No... looks like its a transition id (transition level in the normset table)
	 * 
	 * The normset IDs used depend upon the level targeted in the
	 * transition level.  Here's the "secret decoder ring":
	 * 	Transition		Ind Normset ID
	 * 	----------		-----------
	 * 1 -  IC-->FLL		3
	 * 2 -  IC-->MLL		1
	 * 3 - FLL-->MLL		1
	 * 4 - FLL-->SE			2
	 * 5 - MLL-->SE			2
	 * 
	 * NOTE there are no group norms for levels 10 and 11 at this time (4/27/10)
	 * The levels are bogus, but must be present to allow us to get individual norms
	 * 10- MLL-->SEA		29
	 * 11-  SE-->SEA		29
	 * 
	 * Code adapted from TLT_GROUP_DETAIL_REPORT_V3 in pdi-listener-portal
	 * and ReportDataHelper in pdi-data-v3
	 */	
	//TODO THIS SHOULD BE ABSTARCTED!!!!
	// There are hard-coded normset ids in here (even if they are defined).  We
	// should change the query that this is used in getPdiTransitionNormset to use
	// the same type of logic used in the group norm fetch (getPdiNormsets) where the
	// displayName (here, "PDI Global Norm")and the transitionLevel (here, 6, 7, 8)
	// are used to pull the correct data.  But note that the original transition level comes from the project
	public int getIndivNormsetId(String transitionLvl)
	{
		int ret;

		switch (Integer.parseInt(transitionLvl))
		{
			case 1:	// Target = FLL
				//ret = 3;
				ret = FLL_NORMSET_ID;
				break;
			case 2:	// Target = MLL
			case 3:
				//ret = 1;
				ret = MLL_NORMSET_ID;
				break;
			case 4:	// Target = Sr. Exec.
			case 5:
				//ret = 2;
				ret = BUL_NORMSET_ID;
				break;
			case 9:	// Target = SEA
			case 10:
			case 11:
				//ret = 2;
				ret = SEA_NORMSET_ID;
				break;
			default:
				log.debug("Invalid transition level (individual) - {}", transitionLvl);
				ret = 0;
		}
		log.debug("transition normset id={}", ret);
		return ret;
	}
	
	 
	/*
	 * Fetches a normset ID for a transition level (used in group reports)
	 * 
	 * The normset IDs used depend upon the level targeted in the
	 * transition level.  Here's the "secret decoder ring":
	 * 	Transition		Transition Level ID
	 * 	----------		------------------
	 * 1 -  IC-->FLL		6
	 * 2 -  IC-->MLL		7
	 * 3 - FLL-->MLL		7
	 * 4 - FLL-->SE			8
	 * 5 - MLL-->SE			8
	 * 
	 * Code adapted from TLT_GROUP_DETAIL_REPORT_V3 in pdi-listener-portal
	 * and ReportDataHelper in pdi-data-v3
	 */
	private int getGroupTranNormsetId(String transitionLvl)
	{
		int ret;

		switch (Integer.parseInt(transitionLvl))
		{
			case 1:	// Target = FLL
				ret = 6;
				break;
			case 2:	// Target = MLL
			case 3:
				ret = 7;
				break;
			case 4:	// Target = Sr. Exec.
			case 5:
				ret = 8;
				break;
			default:
				//System.out.println("Invalid transition level (group) - " + transitionLvl);
				ret = 0;
		}

		return ret;
	}
	
	
	/*
	 * Fetches a Pdi Transition Normset for an individual
	 * Code stolen from ReportDataHelper in pdi-data-v3
	 */	
	public NormSet getPdiTransitionNormset(int normSetId)
	{
		NormSet ret = new NormSet();
		ret.setNormsetId(normSetId);
		DataResult dr = null;
		log.debug("The NORMSET ID IS..... {}", normSetId);
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT  ns.displayName, ns.transitionLevel, ns.active  ");
		sqlQuery.append("  FROM  pdi_tlt_normset ns ");
		sqlQuery.append("  WHERE  ns.normsetId = " + normSetId  + "  ");
		sqlQuery.append("  AND ns.active = 1  ");
		log.debug("sql:  {}", sqlQuery.toString());		
		
		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			log.debug("Statement prep error in getPdiTransitionNormset():  normsetId={}, StatusCode={}, errCode={}, errMsg={}", normSetId, dlps.getStatus().getStatusCode(), 
					dlps.getStatus().getExceptionCode(), dlps.getStatus().getExceptionMessage());
			return ret;
		}

		try
		{
			dr = V2DatabaseUtils.select(dlps);
			ResultSet rs = dr.getResultSet();
			while(rs.next())
			{
				//setting the basics....
				ret.setDisplayName(rs.getString("displayName"));
				ret.setTransitionLevel(rs.getInt("transitionLevel"));
				ret.setActive(rs.getInt("active"));
			}

			return ret;
		}
		catch (Exception ex)
		{
			//rdto.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
			//								ex.getErrorCode(), "getLanguage:  " + ex.getMessage()));
			//ret.add(rdto);
			log.debug("Exception in getPdiTransitionNormset():  normsetId={}, Msg={}", normSetId, ex.getMessage());
			return ret;
		}
		finally
		{
			if(dr != null) { dr.close(); dr = null; }
		}	
	}


	/*
	 * Gets relevant group normsets for later use
	 *
	 *  Using this in the group report.
	 *  Code stolen from pdi-data-v3\blahblahblah\ReportDataHelper
	 */	
	public HashMap<String, NormSet> getPdiNormsets(int transitionLevel, int targetTransitionLevel)
	{
		HashMap<String, NormSet> ret = new HashMap<String, NormSet>();
		DataResult dr = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT ns.normsetid, ");
		sqlQuery.append("       ns.displayName, ");
		sqlQuery.append("       ns.scoringStepName, ");
		sqlQuery.append("       ns.transitionLevel, ");
		sqlQuery.append("       ns.transitionType, ");
		sqlQuery.append("       n.normId, ");
		sqlQuery.append("       n.labelKey, ");
		sqlQuery.append("       n.mean, ");
		sqlQuery.append("       n.standardDeviation, ");
		sqlQuery.append("       n.weight ");
		sqlQuery.append("  FROM pdi_tlt_normset ns, pdi_tlt_norm n ");
		sqlQuery.append("  WHERE ns.normsetid = n.normsetId ");
		sqlQuery.append("    AND ( ns.scoringStepName = 'TPOT2_Group_component_z_score' OR ");
		sqlQuery.append("         (ns.scoringStepName = 'TPOT2_Group_Logodds'            AND ns.transitionLevel = '" + transitionLevel + "') OR ");
		sqlQuery.append("         (ns.scoringStepName = 'TPOT2_Group_Logodds_From_Zscor' AND ns.transitionLevel = '" + transitionLevel + "') OR ");
		sqlQuery.append("         (ns.scoringStepName = 'TPOT2_Group_Performance'        AND ns.transitionLevel = '" + targetTransitionLevel + "') OR ");
		sqlQuery.append("         (ns.scoringStepName = 'TPOT2_Group_Performance_From_Z' AND ns.transitionLevel = '" + targetTransitionLevel + "')  ) ");
		sqlQuery.append("    AND ns.active = '1' ");
		sqlQuery.append("    AND n.active = '1' ");
		sqlQuery.append("  ORDER BY  ns.normsetid, ns.displayName, ns.scoringStepName,  ns.transitionLevel, ns.transitionType, n.normId ");
	  	//System.out.println("getPdiNormset sql:  " + sqlQuery.toString());	

		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			System.out.println("getPdiNormsets()-Lvl=" + transitionLevel + ", targ=" + targetTransitionLevel + ": Prep Stmt." +
								"  Stat=" + dlps.getStatus().getStatusCode() +
								", ExCode=" + dlps.getStatus().getExceptionCode() +
								", ExMsg=" + dlps.getStatus().getExceptionMessage());
			return ret;
		}

		try
		{
			dr = V2DatabaseUtils.select(dlps);
			ResultSet rs = dr.getResultSet();
			String nsId = "0";
			NormSet normset= new NormSet();
			boolean isFirst = true;
			while(rs.next())
			{
				// check for a new NormSet Id
				if(! rs.getString("normsetid").equals(nsId))
				{
					// create a new NormSet 
					if(isFirst)
					{
						//reset the variable
						isFirst = false;
					} else {
						// changing normsets, so we want to add 
						// the now populated one to
						// the arraylist of normsets before we refresh it
						ret.put(normset.getDisplayName(), normset);												
						// NOW, create a new one... 
						normset = new NormSet();						
					}

					//setting the basics....
					normset.setNormsetId(new Integer(rs.getInt("normsetid")));
					normset.setDisplayName(rs.getString("displayName"));
					normset.setScoringStepName(rs.getString("scoringStepName"));					
					normset.setTransitionLevel(rs.getInt("transitionLevel"));
					normset.setTransitionType(rs.getInt("transitionType"));
					normset.setActive(1);

					// reset nsId...
					nsId = normset.getNormsetId().toString();					
				}

				// now go ahead and get the norms.... 
				Norm norm = new Norm();
				//  n.normId, n.mean, n.standardDeviation, n.weight
				norm.setId(rs.getString("normId"));
				norm.setName(rs.getString("labelKey"));
				norm.setMean  ( ( rs.getString("mean").equals("")) ?  new Double(0.0) : new Double(rs.getString("mean")));  
				norm.setStdDev( ( rs.getString("standardDeviation").equals("")) ? new Double(0.0)  : new Double(rs.getString("standardDeviation")));  			
				norm.setWeight( ( rs.getString("weight").equals(""))? new Double(0.0) : new Double(rs.getString("weight"))); 

				// add the norms to the normset
				normset.getNorms().add(norm);
			}

			// add b4 returning!! 
			ret.put(normset.getDisplayName(), normset);				

			return ret;
		}
		catch (SQLException ex)
		{
			//rdto.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
			//								ex.getErrorCode(), "getLanguage:  " + ex.getMessage()));
			//ret.add(rdto);
			System.out.println("getPdiNormsets()-Lvl=" + transitionLevel + ", targ=" + targetTransitionLevel + ": Exception detected;" +
					" Msg=" + ex.getMessage());
			return ret;
		}
		finally
		{
			if(dr != null) { dr.close(); dr = null; }
		}	
	}


	/*
	 * Populates the NormSet with it's norms from the database and returns it.
	 * Code stolen from ReportDataHelper in pdi-data-v3
	 */	
	public NormSet populateNormset(NormSet normset)
	{
		DataResult dr = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT n.normId, ");
		sqlQuery.append("  n.labelKey,  ");
		sqlQuery.append("  n.mean,   ");
		sqlQuery.append("  n.standardDeviation  ");
		sqlQuery.append("  FROM  pdi_tlt_normset ns, pdi_tlt_norm n ");
		sqlQuery.append("  WHERE  ns.normsetId = " + normset.getNormsetId().toString() + "  ");
		sqlQuery.append("  AND ns.normsetId = n.normsetId    ");
		sqlQuery.append("  AND ns.active = 1  ");
		sqlQuery.append("  AND n.active = 1 ");
		//System.out.println("sql:  " + sqlQuery.toString());		
		
		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			String nStr = (normset == null) ? "null" : " " + normset.getNormsetId();
			System.out.println("Norm data fetch error: NormsetId=" + nStr + ".  Error info - status code=" + dlps.getStatus().getStatusCode() +
							   ", exception code=" + dlps.getStatus().getExceptionCode() +
							   ", msg=" + dlps.getStatus().getExceptionMessage());
			return normset;
		}

		try
		{
			dr = V2DatabaseUtils.select(dlps);
			ResultSet rs = dr.getResultSet();
			while(rs.next())
			{
				Norm norm = new Norm ();
				norm.setId(rs.getString("normId"));
				norm.setName(rs.getString("labelKey"));
				norm.setMean(rs.getDouble("mean"));
				norm.setStdDev(rs.getDouble("standardDeviation"));
				normset.getNorms().add(norm);
				//System.out.println("norm: " + norm.getName() + " " + norm.getMean());
			}

			return normset;
		}
		catch (SQLException ex)
		{
			return normset;
		}
		finally
		{
			if(dr != null) { dr.close(); dr = null; }
		}	
	}


	/*
	 * Get group report information from V2 for a project
	 */
	public GroupReport getGroupReportData(SessionUserDTO sessionUser,
										  String reportCode,
										  String reportType,
										  String job)
		throws Exception
	{
		GroupReport ret = new GroupReport();
		
		// Set code and type
		ret.setReportCode(reportCode);
		ret.setReportType(reportType);

		// Get the relevant project data and set it up in the GroupReport object
		ProjectDTO project = new ProjectDataHelper().getProject(sessionUser, job);
		////System.out.println("Got project for job " + job + ".  cogsInc=" + project.getCognitivesIncluded());
		ret.setName("_" + project.getName() + "_TLTGroupDetail.pdf");
		int transitionLevel = Integer.parseInt(project.getTransitionLevel());	// "Raw"/Individual transition level
		int targetTransitionLevel = getGroupTranNormsetId(project.getTransitionLevel());	// Group transition level
		ret.setTransitionLevel(transitionLevel);
		ret.setTargetTransitionLevel(targetTransitionLevel);
		
		//  Get the group normset
		HashMap<String, NormSet> groupNormSets = new HashMap<String, NormSet>();
		groupNormSets = getPdiNormsets(transitionLevel, targetTransitionLevel);
		Iterator<String> it = groupNormSets.keySet().iterator();
		while (it.hasNext()) 
		{		
			String s = it.next();
			 NormSet ns = groupNormSets.get(s);
			 for(Norm n : ns.getNorms())
			 {			 
				ret.addDisplayData(ns.getDisplayName() + "_" + n.getName()+ "_MEAN", new Double(n.getMean()).toString());
				ret.addDisplayData(ns.getDisplayName() + "_" + n.getName()+ "_STDEV", new Double(n.getStdDev()).toString());
				ret.addDisplayData(ns.getDisplayName() + "_" + n.getName()+ "_WEIGHT", new Double(n.getWeight()).toString());
			 }
		}

		// Project level data
		ret.getDisplayData().put("ORGANIZATION", project.getClient().getName());
  		String str = project.getTPOTTransLevelCurrent();
		str = (str == null || str.length() < 1) ? calculateCurrentLevel(project.getClient().getId(), Integer.parseInt(project.getTransitionLevel())) : str;
		ret.getDisplayData().put("CURRENT_LEVEL", str);
		str = project.getTPOTTransLevelTarget();
		str = (str == null || str.length() < 1) ? calculateTargetLevel(project.getClient().getId(), Integer.parseInt(project.getTransitionLevel())) : str;
		ret.getDisplayData().put("TARGET_LEVEL", str);
  		 
		//TODO: Put real data here (from Project)
		ret.getDisplayData().put("DATE_STRING", "What date do we want here?");
		
  		if(project.getCognitivesIncluded().equalsIgnoreCase("2") || project.getCognitivesIncluded().equalsIgnoreCase("4")) {
  			ret.addDisplayData(ReportingConstants.RC_COGNITIVES_INCLUDED, "1");
  		} else {
  			ret.addDisplayData(ReportingConstants.RC_COGNITIVES_INCLUDED, "0");
  		}

		return ret;
	}


	/*
	 * getAllNormData - collects a few relevant pieces of information
	 *                  about all of the norms set up in V2
	 */
	//This can be static since it is a direct map to the database with no participant intersection
	private static HashMap<String, NormData> norms = null;
	 public HashMap<String, NormData> getAllNormData()
	{
		if(ParticipantDataHelper.norms != null) {
			return ParticipantDataHelper.norms;
		}
		DataResult dr = null;

		HashMap<String, NormData> ret = new HashMap<String, NormData>();
		
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT nn.UniqueIdStamp, ");
		sqlQuery.append("       nn.NormName, ");
		sqlQuery.append("       nn.XML ");
		sqlQuery.append("  FROM norms nn ");
		//System.out.println("sql:  " + sqlQuery.toString());		
		
		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			System.out.println("Norm data fetch error - status code=" + dlps.getStatus().getStatusCode() +
							   ", exception code=" + dlps.getStatus().getExceptionCode() +
							   ", msg=" + dlps.getStatus().getExceptionMessage());
			return ret;
		}

		try
		{
			dr = V2DatabaseUtils.select(dlps);
			ResultSet rs = dr.getResultSet();
			while(rs.next())
			{
				NormData nd = new NormData();
				nd.setId(rs.getString("UniqueIdStamp"));
				nd.setName(rs.getString("NormName"));
				String xml = rs.getString("XML");
				nd.setXml(xml);
				
				// Grab the group
				int idx = xml.indexOf("</Group>");
				if (idx < 1)
				{
					//Skip it if not found or if its index is 0 (first thing in the XML)
					continue;
				}
				idx--;
				nd.setType(xml.charAt(idx));
				
				ret.put(nd.getId(), nd);
			}

		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return null;
		}
		finally
		{
			if(dr != null) { dr.close(); dr = null; }
		}	
		ParticipantDataHelper.norms = ret;
		return ParticipantDataHelper.norms;
	}


	/*
	 * getNormXrefData - collects the pdi xross reference data
	 *                   between V2 constructs and SPSS keys
	 */
	 private static HashMap<String, String> normXRefData = null;
	 public HashMap<String, String> getNormXrefData()
	{
		 if(normXRefData != null) {
			 return normXRefData;
		 }
		DataResult dr = null;

		HashMap<String, String> ret = new HashMap<String, String>();
		
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT constructId, ");
		sqlQuery.append("       spssValue ");
		sqlQuery.append("  FROM pdi_construct_spss_value ");
		//System.out.println("sql:  " + sqlQuery.toString());		
		
		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			System.out.println("construct/spss fetch error - status code=" + dlps.getStatus().getStatusCode() +
							   ", exception code=" + dlps.getStatus().getExceptionCode() +
							   ", msg=" + dlps.getStatus().getExceptionMessage());
		}

		try
		{
			dr = V2DatabaseUtils.select(dlps);
			ResultSet rs = dr.getResultSet();
			while(rs.next())
			{
				ret.put(rs.getString("constructId"), rs.getString("spssValue"));
			}

		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			if(dr != null) { dr.close(); dr = null; }
		}	
		normXRefData = ret;
		return normXRefData;
	}
	
	 
	/*
	 * Get a list of module information associated with a particular project
	 * 
	 * I think this should probably be private, but I'm not sure but some
	 * groovy script might call it
	 */
	public ArrayList<ModConstsDTO>getModules(String participantId, String projectId)
	{
		try
		{
			if (participantId.length() > 16 || projectId.length() > 16)
			{
				return null;
			}
		}
		catch (Exception e) {}		// Should at least dump to the log

		ArrayList<ModConstsDTO>modules = new ArrayList<ModConstsDTO>();
		DataLayerPreparedStatement dlps = null;
	    ResultSet rs = null;
	    DataResult dr = null;
	    StringBuffer sql = new StringBuffer();
	  	sql.append("Select m.uniqueIdStamp, m.name, a.completed " +
					"from modules m, job_mod jm " +
					"LEFT JOIN answers a ON (a.jobId = jm.jobId and a.moduleId = jm.moduleId and a.candidateId = ?) " +
					"where jm.jobId = ? and jm.moduleId = m.uniqueIdStamp order by jm.pos");
	  	
	  	try
	  	{
			dlps = V2DatabaseUtils.prepareStatement(sql);
			dlps.getPreparedStatement().setString(1, participantId);
			dlps.getPreparedStatement().setString(2, projectId);
			dr = V2DatabaseUtils.select(dlps);
			rs = dr.getResultSet();

			
			while (rs.next())
			{			
				ModConstsDTO module = new ModConstsDTO();
                //System.out.println(rs.getString("name"));
				module.setModuleId(rs.getString("uniqueIdStamp"));
				module.setModuleName(rs.getString("name"));
				String modCompletion = rs.getString("completed");
				try
				{
					if (null == modCompletion || "0".equals(modCompletion))
					{
							module.setStatus(ModConstsDTO.NOT_STARTED);
					} else if("1".equals(modCompletion)) {
						module.setStatus(ModConstsDTO.COMPLETED);
					} else {
						module.setStatus(ModConstsDTO.IN_PROGRESS);
					}
				}
				catch (Exception e)
				{
					module.setStatus(ModConstsDTO.NOT_STARTED);
				}
				
				modules.add(module);
				//System.out.println("-----------id=" + rs.getString("uniqueIdStamp") + ", name=" + rs.getString("name") + ", completed=" + rs.getString("completed"));
			}
		}
	  	catch (Exception e)
	  	{
		}
		finally {
			if(dr != null) { dr.close(); dr = null; }
		}
		return modules;
	}


	/*
	 * Gets a particular module
	 */
	public ModConstsDTO getModule(String moduleId)
	{
		ModConstsDTO module = new ModConstsDTO();
		DataLayerPreparedStatement dlps = null;
	    ResultSet rs = null;
	    DataResult dr = null;
	    
		try
		{
			if (moduleId.length() > 16)
			{
				return module;
			}
		}
		catch (Exception e)
		{
			return module;
		}
		
		StringBuffer sql = new StringBuffer();
		sql.append("Select name " +
				   "from modules " +
				   "where uniqueIdStamp = ?");
		try
		{
			dlps = V2DatabaseUtils.prepareStatement(sql);
			dlps.getPreparedStatement().setString(1, moduleId);
			dr = V2DatabaseUtils.select(dlps);
			rs = dr.getResultSet();
					
			while (rs.next())
			{						
				module.setModuleId(moduleId);
				module.setModuleName(rs.getString("name"));
				break;
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally {
			if(dr != null) { dr.close(); dr = null; }
		}
		return module;
	}
		

	/*
	 * Sees if a consent has bee answerred in the afirmative
	 */
	public boolean validateConsent(String participantId, String projectId)
	{
		boolean consent = false;
		DataLayerPreparedStatement dlps = null;
	    ResultSet rs = null;
	    DataResult dr = null;
	    
		try
		{
			if (participantId.length() > 16 || projectId.length() > 16)
			{
				return false;
			}
		}
		catch (Exception e)
		{
		}
		
		StringBuffer sql = new StringBuffer();
		sql.append("select completed from answers where " +
					"moduleId = 'HMWIJBYN' and " +
					"jobId = ? and " +
					"candidateId = ?");
		try
		{
			dlps = V2DatabaseUtils.prepareStatement(sql);
			dlps.getPreparedStatement().setString(1, projectId);
			dlps.getPreparedStatement().setString(2, participantId);
			dr = V2DatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			if(rs == null) {
				return false;
			}
			while (rs.next())
			{			
				String completed = rs.getString("completed") + "";
				consent = "1".equals(completed);
				if (consent)
				{
					break;
				}
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			consent = false;
		}
		
		finally {
			if(dr != null) { dr.close(); dr = null; }
		}
		
		return consent;
	}



/****************************************************/
/**           Private classes                      **/
/****************************************************/
	private class Module
	{
		private String _id;
		private String _name;
		private ArrayList<String> _conIds;

		public Module()
		{
			_conIds = new ArrayList<String>();
		}

		/***********************************/
		protected void setId(String value)
		{
			_id = value;
		}
		
		protected String getId()
		{
			return _id;
		}

		/***********************************/
		protected void setName(String value)
		{
			_name = value;
		}
		
		protected String getName()
		{
			return _name;
		}

		/***********************************/
		protected ArrayList<String> getConIds()
		{
			return _conIds;
		}
		
		protected void addConId(String value)
		{
			_conIds.add(value);
			return;
		}


//		/*
//		 * genStr is like toString, but it allows one to pass in the construct map
//		 * needed to print construct and score info 
//		 */
//		private String genStr(HashMap<String, Construct> conMap)
//		{
//			String ret = "Module:  id=" + this.getId() + ", name=" + this.getName();
//			
//			if (this.getConIds().size() == 0)
//			{
//				ret += "\nNo constructs found...";
//			}
//			else
//			{
//				for (String conId : getConIds())
//				{
//					Construct con = conMap.get(conId);
//					ret += "\n   Construct:  id=" + con.getId() + ", name=" + con.getName();
//					ret += "\n                SPSS=" + con.getSpss() + ", cScore=" + con.getCScore() +
//						                     ", lastDate=" + con.getDate();
//				}
//			}
//			
//			return ret;
//		}


		/*
		 * toString - Puts out info about the Module class 
		 */
		public String toString()
		{
			String ret = "Module:  id=" + this.getId() + ", name=" + this.getName();
			
			if (this.getConIds().size() == 0)
			{
				ret += "\nNo constructs found...";
			}
			else
			{
				for (String conId : getConIds())
				{
					ret += "\n   Construct id=" + conId;
				}
			}
			
			return ret;
		}
	}


	public class NormData
	{
		private String _id;
		private String _name;
		private char _type;
		private String _xml;

		public NormData()
		{

		}


		/***********************************/
		protected void setId(String value)
		{
			_id = value;
		}

		protected String getId()
		{
			return _id;
		}

		/***********************************/
		protected void setName(String value)
		{
			_name = value;
		}

		protected String getName()
		{
			return _name;
		}

		/***********************************/
		protected void setType(char value)
		{
			_type = value;
		}

		protected boolean isGenPop()
		{
			return (_type == '1');
		}

		protected boolean isSpecPop()
		{
			return (_type == '2');
		}

		/***********************************/
		protected void setXml(String value)
		{
			_xml = value;
		}

		protected String getXml()
		{
			return _xml;
		}
	}

	private  class Construct
	{
		private String _id;
		private String _name;
		private String _spss;
		private double _tempDate;
		private String _cScore;
		private String _xml;
		private String _gpNormName;
		private String _gpNormId;
		private String _gpPctl;
		private String _spNormName;
		private String _spNormId;
		private String _spPctl;

		public Construct()
		{
			
		}

		/***********************************/
		protected void setId(String value)
		{
			_id = value;
		}
		
		protected String getId()
		{
			return _id;
		}

		/***********************************/
		protected void setName(String value)
		{
			_name = value;
		}
		
		protected String getName()
		{
			return _name;
		}

		/***********************************/
		protected void setSpss(String value)
		{
			_spss = value;
		}
		
		protected String getSpss()
		{
			return _spss;
		}

		/***********************************/
		protected void setTempDate(double value)
		{
			_tempDate = value;
		}
		
		protected double getTempDate()
		{
			return _tempDate;
		}

		/***********************************/
		protected void setCScore(String value)
		{
			_cScore = value;
		}
		
		protected String getCScore()
		{
			return _cScore;
		}

		/***********************************/
		protected void setXml(String value)
		{
			_xml = value;
		}
		
		protected String getXml()
		{
			return _xml;
		}

		/***********************************/
		protected void setGpNormName(String value)
		{
			_gpNormName = value;
		}
		
		protected String getGpNormName()
		{
			return _gpNormName;
		}

		/***********************************/
		protected void setGpNormId(String value)
		{
			_gpNormId = value;
		}
		
		protected String getGpNormId()
		{
			return _gpNormId;
		}

		/***********************************/
		protected void setGpPctl(String value)
		{
			_gpPctl = value;
		}
		
		protected String getGpPctl()
		{
			return _gpPctl;
		}

		/***********************************/
		protected void setSpNormName(String value)
		{
			_spNormName = value;
		}
		
		protected String getSpNormName()
		{
			return _spNormName;
		}

		/***********************************/
		protected void setSpNormId(String value)
		{
			_spNormId = value;
		}
		
		protected String getSpNormId()
		{
			return _spNormId;
		}

		/***********************************/
		protected void setSpPctl(String value)
		{
			_spPctl = value;
		}
		
		protected String getSpPctl()
		{
			return _spPctl;
		}



		public String toString()
		{
			String ret = "\n  Construct:  id=" + this.getId() +
						 ", name=" + this.getName() +
						 ", spss=" + this.getSpss() +
						 ", cScore=" + this.getCScore() +
						 ", date=" + this.getTempDate();
			ret += "\n       Norms - GP=" + this.getGpNormName() + ", SP=" + this.getSpNormName();
			ret += "\n       Pctls - GP=" + this.getGpPctl() + ", SP=" + this.getSpPctl();
			ret += "\n       XML=" + this.getXml();

			return ret;
		}
	}
}
