/**
 * Copyright (c) 2008 Personnel Decisions International, Inc.
 * All rights reserved.
 */

package com.pdi.data.abyd.helpers.evalGuide;


/**
 * EGContants provides a spot for defined constants that can be used in Eval
 * Guide processing.  Currently defined with static data only.
 *
 * NOTE: This class has a companion set of definitions on the UI side
 *       (abd\evalGuides.EGConstants.as).  Every effort should be made
 *       to keep these files in sync.
 *
 * @author		Ken Beukelman
 */
public class EGConstants
{
	//
	// Static data.
	//
		// module "Special" flag values
	// Default implies  there is nothing special about the module
	public static final transient String DEFAULT = "No";
	// The module is a financial Exercise and needs special treatment on the UI side.
	public static final transient String FIN_EX = "FE";
	// The module is the Participant Interview and needs special treatment on the UI side.
	public static final transient String PART_INT = "PI";

	//
	// Static methods.
	//

}
