package com.pdi.data.abyd.helpers.evalGuide.ut;

import java.sql.Connection;

import com.pdi.data.abyd.helpers.evalGuide.SubmitEGDataHelper;
import com.pdi.ut.UnitTestUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;

import junit.framework.TestCase;

public class SubmitEGDataHelperUT extends TestCase
{
	//
	// Constructors
	//
	public SubmitEGDataHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
	throws Exception
	{
		junit.textui.TestRunner.run(SubmitEGDataHelperUT.class);
	}


	/*
	 * testSubmitEG
	 */
	public void testSubmitEG()
		throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);
		
		Connection con = AbyDDatabaseUtils.getDBConnection();
    	String partId = "KENSPART";
    	long dnaId = 999;
    	long modId = 888;

    	SubmitEGDataHelper helper = new SubmitEGDataHelper(con, partId, dnaId, modId);    
		helper.submitEG();
		System.out.println("Check the db...");

		UnitTestUtils.stop(this);
	}
}
