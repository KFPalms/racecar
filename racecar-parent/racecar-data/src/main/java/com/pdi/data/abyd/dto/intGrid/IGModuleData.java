/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.dto.intGrid;

import com.pdi.data.abyd.dto.common.EGCompDisplayData;
import com.pdi.data.abyd.dto.intGrid.IGInstDisplayData;

/**
 * IGMoululeData is a class designed to convey the information for one
 * module for the competency information in the Integration Grid.
 * 
 * @author		Ken Beukelman
  */
public class IGModuleData
{
	//
	// Static data.
	//


	//
	// Static methods.
	//


	//
	// Instance data.
	//
	private long _moduleId;
	private String _moduleName;
	private EGCompDisplayData _compDispData = null;
	private IGInstDisplayData _instDispData = null;
	private String _specialType = "";
	private long _modSeq;

	//
	// Constructors.
	//
	public IGModuleData()
	{
		// does nothing right now
	}

	//
	// Instance methods.
	//
	
	// Sort of a getter
	public String getModCompScore()
	{
		if(this._specialType.equals("ES"))
		{
			// Only comp type (EG scored) have external comp scores at this point.
			return _compDispData.getExtCompScore();
		}
		else if(_compDispData != null)
		{
			return _compDispData.getCalcedCompScore();
		}
		else if(_instDispData != null)
		{
			return _instDispData.getCalcedCompScore();
		}
		// If neither
		return "0.0";
	}
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public long getModuleId()
	{
		return _moduleId;
	}
	
	public void setModuleId(long value)
	{
		_moduleId = value;
	}	

	/*****************************************************************************************/
	public String getModuleName()
	{
		return _moduleName;
	}
	
	public void setModuleName(String value)
	{
		_moduleName = value;
	}	

	/*****************************************************************************************/
	public EGCompDisplayData getCompDispData()
	{
		// We explicitly do NOT lazy initialize because _compDispData and _instDispData are
		// mutually exclusive and we check one for NULL to see if we do the other.  Lazy
		// initialize would create the list we are checking for non-existence, destroying
		// the logic that has been implemented.
		return _compDispData;
	}
	
	public void setCompDispData(EGCompDisplayData value)
	{
		_compDispData = value;
	}	

	/*****************************************************************************************/
	public IGInstDisplayData getInstDispData()
	{
		// We explicitly do NOT lazy initialize because _instDispData and _compDispData are
		// mutually exclusive and we check one for NULL to see if we do the other.  Lazy
		// initialize would create the list we are checking for non-existence, destroying
		// the logic that has been implemented.
		return _instDispData;
	}
	
	public void setInstDispData(IGInstDisplayData value)
	{
		_instDispData = value;
	}	

	/*****************************************************************************************/
	public String getSpecialType() {
		return _specialType;
	}

	
	public void setSpecialType(String _specialType) {
		this._specialType = _specialType;
	}

	/*****************************************************************************************/
	public long getModSeq()
	{
		return _modSeq;
	}
	
	public void setModSeq(long value)
	{
		_modSeq = value;
	}	

	
	/*
	 * Show me what you've got
	 */
	public String toString()
	{
		String ret = "IGModuleData content (Java)\n";
		ret += "    Module ID=" + _moduleId + "\n";
		ret += "  Module Name=" + _moduleName + "\n";
		if (_compDispData == null)
		{
			ret += "  No Eval Guide data present\n";
		}
		else
		{
			ret += _compDispData.toString();
		}
		if (_instDispData == null)
		{
			ret += "  No Integration Grid data present\n";
		}
		else
		{
			ret += _instDispData.toString();
		}
		
		return ret;
	}


}
