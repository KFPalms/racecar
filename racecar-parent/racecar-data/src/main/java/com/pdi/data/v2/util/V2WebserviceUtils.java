/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v2.util;

import com.pdi.data.v2.dto.SessionUserDTO;
import com.pdi.properties.PropertyLoader;

//import java.io.StringReader;
import java.net.SocketException;

import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.*;
  
import org.w3c.dom.*;

import javax.xml.parsers.*;

//import org.xml.sax.InputSource;


/*
 * Provides access to V2 web services (ETR calls to V2)
 */
public class V2WebserviceUtils
{
	/*
	 * Logs you into the v2 system
	 * 
	 * 	ETRMgr.UserName = "KWIDJSKL";
	 *	ETRMgr.Password = "WKEIDSLA";
	 *
	 * @return a session id used for all other calls
	 */
	public SessionUserDTO registerUser(String username, String password)
		throws Exception
	{
		//get properties
		String server = PropertyLoader.getProperty("com.pdi.data.v2.application", "webservice.path");
		String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><ETR><Rqt Cmd=\"000\" UN=\"${username}\" Pw=\"${password}\" URL=\""+server+"\" /><Rqt Cmd=\"OpenSQL\" AN=\"UserSQL\" FA=\"N\" FR=\"1\" RC=\"N\" SQ=\"SELECT U.UniqueIdStamp as UserID, L.Name, L.Branch, L.Active AS ClientActive, U.Active, L.ClientType, U.AccessLevel, U.ClientId, U.UserType FROM Users as U INNER JOIN Clients as L on (L.UniqueIdStamp=U.ClientID) WHERE U.UserName=&apos;${username}&apos; and Password=&apos;${password}&apos;\" /><Rqt Cmd=\"Read\" AN=\"UserSQL\" Sub=\"UserInfo\" Rpt=\"1\" RQ=\"N\"><Fld FN=\"UserID\" Fmt=\"V\" /><Fld FN=\"UserID\" Fmt=\"A\" /><Fld FN=\"Branch\" Fmt=\"A\" /><Fld FN=\"ClientType\" Fmt=\"A\" /><Fld FN=\"AccessLevel\" Fmt=\"A\" /><Fld FN=\"ClientId\" Fmt=\"A\" /><Fld FN=\"UserType\" Fmt=\"A\" /><Fld FN=\"Active\" Fmt=\"A\" /><Fld FN=\"Name\" Fmt=\"A\" /><Fld FN=\"ClientActive\" Fmt=\"A\" /></Rqt><Rqt Cmd=\"GetIP\" /><Rqt Cmd=\"Close\" AN=\"UserSQL\" /></ETR>";
		xml = xml.replace("${username}", username);
		xml = xml.replace("${password}", password);
		
		Document doc = sendData(xml);

		SessionUserDTO dto = new SessionUserDTO(doc);
		
		return dto;
	}
	
	
	/*
	 * Sends data to the v2 server
	 * @param xml - the xml to send
	 * @return a string of the response
	 */
	public Document sendData(String xml)
		throws Exception
	{
		//get properties
		
//		System.out.println("-- GOING OUT --");
//		System.out.println(xml);
//		System.out.println("-- DONE GOING OUT --");
		try {
		PostMethod post = new PostMethod(PropertyLoader.getProperty("com.pdi.data.v2.application", "webservice.url")+"/etr");
		
//		System.out.println(PropertyLoader.getProperty("com.pdi.data.v2.application", "webservice.url"));
		
		post.setRequestHeader("Host", PropertyLoader.getProperty("com.pdi.data.v2.application","webservice.url"));
		post.setRequestHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.8) Gecko/2009032609 Firefox/3.0.8 (.NET CLR 3.5.30729)");
		post.setRequestHeader("Accept-Language", "en-us,en;q=0.5");
		post.setRequestHeader("Accept-Encoding", "gzip,deflate");
		post.setRequestHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
		post.setRequestHeader("Content-type", "text/xml; charset=\"utf8\"");
		post.setRequestHeader("Referer", PropertyLoader.getProperty("com.pdi.data.v2.application","webservice.url")+"/interface/");

		post.setRequestHeader("Content-Length", xml.length()+"");
		
		//post.setRequestBody(xml);	// Deprecated... replaced by following line
		post.setRequestEntity(new StringRequestEntity(xml));

 
		HttpClient client = new HttpClient();
		//int result = client.executeMethod(post);
		client.executeMethod(post);
		//post.getResponseBodyAsStream()
		
		// for debug - strXMLResponse never used
		String strXMLResponse = new String(post.getResponseBodyAsString().getBytes(), "UTF-8");
//		//in error situations v2 returns an invalid version
		strXMLResponse = strXMLResponse.replace("version=\"\"", "version=\"1.0\"");
//		System.out.println("-- COMING BACK --");
//		System.out.println(strXMLResponse);
//		System.out.println("-- DONE COMING BACK --");
		
		DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
		Document doc = docBuilder.parse(post.getResponseBodyAsStream());
		
		return doc;
		} catch (SocketException se) {
			//Socket exception, connection reset, seems to happen alot with v2, may as well try again
			return sendData(xml);
		}
	}

	
	/*
	 * 
	 */
    public void insertAnswerRow(String sessionId, String userName, String password, 
    								String candId, 	  String modId,    String jobId)
    throws Exception
    {
	    String server = PropertyLoader.getProperty("com.pdi.data.v2.application", "webservice.path");
	    String answerXML = "<Answers></Answers>";
	    String modLoadVs = "3.00.38";
	    
	    String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
	    xml += "<ETR SessionId=\"${sessionId}\">";
	    xml += "<Rqt Cmd=\"000\" UN=\"${username}\" Pw=\"${password}\" URL=\""+server+"\" />";
	    xml += "<Rqt Cmd=\"Write\" TN=\"Answers\" Mode=\"Append\" Sub=\"Answers\">";
	    xml += "<FldToRtn FN=\"UniqueIdStamp\" Fmt=\"V\" />";
	    xml += "<FldToRtn FN=\"UniqueIdStamp\" Fmt=\"A\" />";
	    xml += "<FldToRtn FN=\"DateCreatedStamp\" Fmt=\"A\" />";
	    xml += "<FldToRtn FN=\"CreatedByStamp\" Fmt=\"A\" />";
	    xml += "<FldToRtn FN=\"DateModifiedStamp\" Fmt=\"A\" />";
	    xml += "<FldToRtn FN=\"ModifiedByStamp\" Fmt=\"A\" />";
	    xml += "<FldToRtn FN=\"ModifiedByStamp\" Fmt=\"A\" />";
	    xml += "<FldToRtn FN=\"ModifiedByStamp\" Fmt=\"A\" />";
	    xml += "<FldToRtn FN=\"UniqueVsStamp\" Fmt=\"A\" />";
	    xml += "<Fld FN=\"modLoadVs\">${modLoadVs}</Fld>";
	    xml += "<Fld FN=\"Completed\">0</Fld>";
	    xml += "<Fld FN=\"ModuleId\">${modId}</Fld>";
	    xml += "<Fld FN=\"CandidateId\">${candId}</Fld>";
	    xml += "<Fld FN=\"JobId\">${jobId}</Fld>";
	    xml += "<Fld FN=\"XML_Answers\">${answerXML}</Fld>";
	    xml += "</Rqt>";
	    xml += "</ETR>";
	    
	    xml = xml.replace("${username}", userName);
	    xml = xml.replace("${password}", password);
	    xml = xml.replace("${modLoadVs}", modLoadVs);
	    xml = xml.replace("${answerXML}", answerXML);
	    xml = xml.replace("${modId}", modId);
	    xml = xml.replace("${candId}", candId);
	    xml = xml.replace("${jobId}", jobId);
	    xml = xml.replace("${sessionId}", sessionId);
	    //Document doc = sendData(xml);
	    sendData(xml);
    }
}
