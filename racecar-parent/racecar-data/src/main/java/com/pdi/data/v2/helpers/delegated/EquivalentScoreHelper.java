/**
 * Copyright (c) 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v2.helpers.delegated;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import com.pdi.data.dto.EqInfo;
import com.pdi.data.dto.TextPair;
import com.pdi.data.helpers.interfaces.IEquivalentScoreHelper;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.util.RequestStatus;
import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.logging.LogWriter;

public class EquivalentScoreHelper implements IEquivalentScoreHelper
{
	/*
	 * fetchInstEquivLists
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IEquivalentScoreHelper#fetchInstEquivLists()
	 */
	@Override
	public HashMap<String, ArrayList<TextPair>> fetchInstEquivLists()
	{
		HashMap<String, ArrayList<TextPair>> ret = new HashMap<String, ArrayList<TextPair>>();
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT instCode, ");
			sb.append("       oldInstCode, ");
			sb.append("	      oldInstName ");
			sb.append("  FROM pdinh_equiv_instruments ");
			sb.append("  WHERE active = 1 ");
			sb.append("  ORDER BY instCode, sequence");

			DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
			if (dlps.isInError())
			{
				LogWriter.logSQL(LogWriter.WARNING, this,
						"Error preparing fetchInstEquivLists query",
						sb.toString());
				return null;
			}

			DataResult dr = null;
			try
			{
				dr = V2DatabaseUtils.select(dlps);
				ResultSet rs = dr.getResultSet();
				if (rs == null)
				{
					LogWriter.logSQL(LogWriter.WARNING, this,
							"Fetch error - No equivalency data - fetchInstEquivLists",
							sb.toString());
					return null;
				}
				
				try
				{
					if (! rs.isBeforeFirst())
					{
						LogWriter.logSQL(LogWriter.WARNING, this,
								"No equivalency data found - fetchInstEquivLists",
								sb.toString());
						return null;
					}
					
					String curInst = null;
					ArrayList<TextPair> curAry = null;
					while(rs.next())
					{
						String inst = rs.getString("instCode");
						if (! inst.equals(curInst))
						{
							// create a new arrayList
							curAry = new ArrayList<TextPair>();
							// stick it in the output
							ret.put(inst, curAry);
							// update curInst
							curInst = inst;
						}
						
						// Add the TextPair to the list
						TextPair tp = new TextPair(rs.getString("oldInstCode"), rs.getString("oldInstName"));
						curAry.add(tp);
					}
					
					return ret;
				}
				finally
				{
			        if (rs != null)
			        {
			            try
						{
			                rs.close();
			            }
			            catch (SQLException sqlEx)
						{
			            	sqlEx.printStackTrace();
			            }
			            rs = null;
			        }
			        if (dr != null)
			        {
			        	dr.close();
			            dr = null;
			        }
				}
			}
			catch(java.sql.SQLException se)
			{
				LogWriter.logSQL(LogWriter.ERROR, this,
						"Fetch error in fetchInstEquivLists", sb.toString());
				se.printStackTrace();
				return null;
			}
			finally
			{
				if (dr != null) { dr.close(); dr = null; }
			}
		}
		catch(Exception e)
		{
			LogWriter.logBasic(LogWriter.ERROR, this,
					"General error in fetchInstEquivLists");
			return null;
		}
	}


	/*
	 * fetchEquivalentScore
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IEquivalentScoreHelper#fetchEquivalentScore(java.lang.String, java.lang.String)
	 */
	@Override
	public String fetchEquivalentScore(String oldInst, String oldScore)
	{
		String ret = null;
		
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT instRawScore ");
			sb.append("  FROM pdinh_equiv_scores ");
			sb.append("  WHERE oldInstCode = ? ");
			sb.append("    AND oldInstRawScore = ?");

			DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
			if (dlps.isInError())
			{
				LogWriter.logSQL(LogWriter.WARNING, this,
						"Error preparing fetchEquivalentScore query",
						sb.toString());
			}

			DataResult dr = null;
			try
			{
				// Fill sql parmeters with values and get the data
				dlps.getPreparedStatement().setString(1, oldInst);
				dlps.getPreparedStatement().setString(2, oldScore);

				dr = V2DatabaseUtils.select(dlps);
				ResultSet rs = dr.getResultSet();

				try
				{
					if (rs == null)
					{
						LogWriter.logSQL(LogWriter.ERROR, this,
								"No ResultSet in fetchEquivalentScore", sb.toString());
						return ret;
					}
					if (! rs.isBeforeFirst())
					{
						LogWriter.logSQL(LogWriter.ERROR, this,
								"No data in fetchEquivalentScore for oldInst=" + oldInst + ", oldScore=" + oldScore, sb.toString());
						return ret;
					}

					// Should be a single row
					rs.next();
					ret = rs.getString("instRawScore");
					
					return ret;
				}
				finally
				{
			        if (rs != null)
			        {
			            try
						{
			                rs.close();
			            }
			            catch (SQLException sqlEx)
						{
			            	sqlEx.printStackTrace();
			            }
			            rs = null;
			        }
			        if (dr != null)
			        {
			        	dr.close();
			            dr = null;
			        }
				}
			}
			catch(java.sql.SQLException se)
			{
				LogWriter.logSQL(LogWriter.ERROR, this,
						"Fetch error in fetchEquivalentScore", sb.toString());
				se.printStackTrace();
				return ret;
			}
			finally
			{
				if (dr != null) { dr.close(); dr = null; }
			}
		}
		catch(Exception e)
		{
			LogWriter.logBasic(LogWriter.ERROR, this,
					"General error in fetchEquivalentScore");
			return ret;
		}
	}


	/*
	 * writeEquivalentLog
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IEquivalentScoreHelper#writeEquivalentLog(com.pdi.data.dto.EqInfo)
	 */
	@Override
	public void writeEquivalentLog(EqInfo logInfo)
	{
		StringBuffer sb = new StringBuffer();
		sb.append("INSERT INTO pdinh_equiv_log ");
		sb.append("  (participantId, projectId, instCode, equivRawScore, oldInstCode, oldRawScore, saveDate) ");
		sb.append("  VALUES (?, ?, ?, ?, ?, ?, GETDATE());");

		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
		if (dlps.isInError())
		{
			LogWriter.logSQL(LogWriter.WARNING, this,
					"Error preparing writeEquivalentLog query",
					sb.toString());
		}

		DataResult dr = null;
		try
		{
			// Fill sql parmeters with values and write the data
			dlps.getPreparedStatement().setString(1, logInfo.getParticipantId());
			dlps.getPreparedStatement().setString(2, logInfo.getProjectId());
			dlps.getPreparedStatement().setString(3, logInfo.getInstCode());
			dlps.getPreparedStatement().setString(4, logInfo.getEqScore());
			dlps.getPreparedStatement().setString(5, logInfo.getOldInstCode());
			dlps.getPreparedStatement().setString(6, logInfo.getOldScore());

			dr = V2DatabaseUtils.insert(dlps);

			if (dr.getStatus().getStatusCode() != RequestStatus.RS_OK)
			{
				LogWriter.logBasic(LogWriter.ERROR, this,
						"Error in writeEquivalentLog -- code=" + dr.getStatus().getExceptionCode() + 
						", msg=" + dr.getStatus().getExceptionMessage());
				return;
			}
		}
		catch(java.sql.SQLException se)
		{
			LogWriter.logSQL(LogWriter.ERROR, this,
						"parm set error in writeEquivalentLog", sb.toString());
			se.printStackTrace();
			return;
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}

}
