package com.pdi.data.abyd.dto.common;

public class PrnData {



	//
	// Instance data.
	//
	
	private float pctl = 0.0f;
	private float rate = 0.0f;
	private int narr = 0;
	private String narrText = "";

	//
	// Constructors.
	//
	public PrnData()
	{
		// default constructor
	}

	//
	// Instance methods.
	//

	/*****************************************************************************************/
	public float getPctl()
	{
		return this.pctl;
	}

	public void setPctl(float value)
	{
		this.pctl = value;
	}

	/*****************************************************************************************/
	public float getRate()
	{
		return this.rate;
	}

	public void setRate(float value)
	{
		this.rate = value;
	}

	/*****************************************************************************************/
	public int getNarr()
	{
		return this.narr;
	}

	public void setNarr(int value)
	{
		this.narr = value;
	}

	/*****************************************************************************************/
	public String getNarrText()
	{
		return this.narrText;
	}

	public void setNarrText(String value)
	{
		this.narrText = value;
	}
}
