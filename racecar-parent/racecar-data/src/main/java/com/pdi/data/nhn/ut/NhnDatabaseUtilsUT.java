package com.pdi.data.nhn.ut;

import java.sql.ResultSet;
import com.pdi.data.nhn.util.NhnDatabaseUtils;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.properties.PropertyLoader;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;


public class NhnDatabaseUtilsUT  extends TestCase {
	public NhnDatabaseUtilsUT(String name)
	{
		super(name);
	}

	public void testProperties()
	{
		UnitTestUtils.start(this);
		
		//Does the local property work?
		String applicationId = PropertyLoader.getProperty("com.pdi.data.nhn.application", "application.code");
		assertEquals(applicationId != null, true);
		assertEquals(applicationId.equals("PDI-DATA-CHECKBOX"), true);
		
		UnitTestUtils.stop(this);
	}
	
	public void testJDBC()
	{
		UnitTestUtils.start(this);
		NhnDatabaseUtils.setUnitTest(true);
		
		

		StringBuffer sb = new StringBuffer();		
		sb.append(" select * from platform.dbo.course ");

		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(sb);
		if (dlps.isInError())
		{
			//TODO When this method returns something, this should be modified
			System.out.println("Error preparing writeAnswerDataToDatabase query." + dlps.toString());
		}
		DataResult dr = null;
		ResultSet rs = null;
		try
		{
			// Fill sql parmeters with values and get the data
			//dlps.getPreparedStatement().setString(1, participantId);

			dr = NhnDatabaseUtils.select(dlps);
			rs = dr.getResultSet();

			if (rs != null && rs.isBeforeFirst())
			{
				while (rs.next())
				{
					String test = rs.getString("course");
					System.out.println(test);
				}
				//rs.close();
			}
		}
		catch(java.sql.SQLException se)
		{
			se.printStackTrace();
		}
		finally
		{
			if (dr != null) {dr.close(); dr = null;}
		}
		
		UnitTestUtils.stop(this);
	}
}
