/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.util;

import java.sql.Connection;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdi.data.util.DataLayerConnection;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.util.DatabaseUtils;
import com.pdi.data.util.PersistenceUtils;
import com.pdi.data.util.RequestStatus;
import com.pdi.data.v2.util.V2DatabaseUtils;
////import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.properties.PropertyLoader;


/**
 * The AbyDDatabaseUtils class provides a place to put database
 * methods valid for use in more than one application
 * in the A by D suite of apps.
 *
 * @author		Ken Beukelman
 */
public class AbyDDatabaseUtils
{
	private static final Logger log = LoggerFactory.getLogger(AbyDDatabaseUtils.class);
	//
	// Static data.
	//
	private static boolean _isUnitTest = false;
	
	//
	// Static methods.
	//
	/**
	 * This pair of methods sets the unit test
	 * flag and also checks the value later.
	 */
	public static void setUnitTest(boolean value)
	{
		_isUnitTest = value;
	}

	public static boolean isUnitTest()
	{
		return _isUnitTest;
	}


	/**
	 * 	Sets up the underlying environment if its a unit test
	 */
	public static void determineUnitTest()
	{
		if(_isUnitTest)
		{
			DatabaseUtils.prepareUnitTest(
					PropertyLoader.getProperty("com.pdi.data.abyd.application", "database.url"), 
					PropertyLoader.getProperty("com.pdi.data.abyd.application", "database.username"), 
					PropertyLoader.getProperty("com.pdi.data.abyd.application", "database.password"),
					PropertyLoader.getProperty("com.pdi.data.abyd.application", "database.class"));
		}
	}


	/**
	 * Private function, used to determine what database
	 * we are connecting to based on the environment passed 
	 * (development,preproduction,production)
	 * NOTE:  Actually, this is the jndi data source.
	 * 
	 * @return The environment string
	 */
	private static String determineEnvironment()
	{	
		return PropertyLoader.getProperty("com.pdi.data.abyd.application", "datasource");
	}

	
	/**
	 * Hibernate specific calls to create an entity manager
	 */
	public static void registerPersistence() {
		try {
			PersistenceUtils.createEntityManager(determineEnvironment());
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * JDBC Only, registers with the pool (for legacy)
	 */
	public static void registerConnection() {
		testConnection();
	}
	
	
	/**
	 * Get a raw DB connection for use by a call
	 *
	 * @return a Connection object
	 * @throws Exception
	 */
	public static Connection getDBConnection()
		throws Exception
	{
		determineUnitTest();
		
		DataLayerConnection dlCon = DatabaseUtils.getDBConnection(determineEnvironment(), true);
		if (dlCon.isInError())
		{
			RequestStatus rs = dlCon.getStatus();
			String errStr = new String("Error connecting to database:  Stat=");
			errStr += rs.getStatusCode();
			if (rs.getExceptionCode() != 0)
			{
				errStr += ", Exception code=" + rs.getExceptionCode();
			}
			if (rs.getExceptionMessage() != null)
			{
				errStr += ", Exception message=" + rs.getExceptionMessage();
			}
			throw new Exception(errStr);
		}

		return dlCon.getConnection();
	}	


	
	/*
	 * Re-factor the following code ... it is a clone (for the most part) of the
	 * V2DatabaseUtils code.
	*/
	
	
	
	
	/**
	 * Get an encapsulated DB connection for use by a call
	 * @param the environment (development, preproduction, production)
	 * @param the SQL to execute
	 * @return a DataLayerPreparedStatement object
	 */
	public static DataLayerPreparedStatement prepareStatement(StringBuffer sql)
	{
		determineUnitTest();
		//if(isUnitTest()) {
		//	System.out.println("UNIT TEST: " + sql);
		//}
		return DatabaseUtils.prepareStatement(determineEnvironment(), sql);
	}
	
	/**
	 * Get an encapsulated DB connection for use by a call
	 * @param the environment (development, preproduction, production)
	 * @param the SQL to execute
	 * @return a DataLayerPreparedStatement object
	 */
	public static DataLayerPreparedStatement prepareStatement(StringBuffer sql, int autogen)
	{
		determineUnitTest();
		//if(isUnitTest()) {
		//	System.out.println("UNIT TEST: " + sql);
		//}
		return DatabaseUtils.prepareStatement(determineEnvironment(), sql, autogen);
	}

	// NOTE that the following functions are intermediate functions used within the data layer.
	//  The actual functionality performed in the helpers may be different from the function
	//	called here; e.g., a delete in a helper may actually call an update function to set
	//	the active flag off.  
	
	/**
	 * The insert interface function.  Note that there is no checking that the
	 * prepared statement is an insert statement.  If the user calls this with
	 * some other functionality, data will be lost at a minimum and the
	 * (properly formated) statement could error out.
	 *
	 * @param A DataLayerPreparedStatement with the PreparedStatement to execute
	 * @return a DataResult object
	 */
	public static DataResult insert(DataLayerPreparedStatement dlps)
	{
		determineUnitTest();
		return statusTransform(DatabaseUtils.execute(determineEnvironment(),
							   dlps,
							   DatabaseUtils.DBA_INSERT));
	}


	/**
	 * The select interface function.  Note that there is no checking that the
	 * prepared statement is a select statement.  If the user calls this with
	 * some other functionality, data will be lost at a minimum and the
	 * (properly formated) statement could error out.
	 *
	 * @param A DataLayerPreparedStatement with the PreparedStatement to execute
	 * @return a DataResult object
	 */
	public static DataResult select(DataLayerPreparedStatement dlps)
	{
		determineUnitTest();
		return statusTransform(DatabaseUtils.execute(determineEnvironment(),
							   dlps,
							   DatabaseUtils.DBA_SELECT));
	}


	/**
	 * The update interface function.  Note that there is no checking that the
	 * prepared statement is an update statement.  If the user calls this with
	 * some other functionality, data will be lost at a minimum and the
	 * (properly formated) statement could error out.
	 *
	 * @param A DataLayerPreparedStatement with the PreparedStatement to execute
	 * @return a DataResult object
	 */
	public static DataResult update(DataLayerPreparedStatement dlps)
	{
		determineUnitTest();
		return statusTransform(DatabaseUtils.execute(determineEnvironment(),
							   dlps,
							   DatabaseUtils.DBA_UPDATE));
	}


	/**
	 * The delete interface function.  Note that there is no checking that the
	 * prepared statement is a delete statement.  If the user calls this with
	 * some other functionality, data will be lost at a minimum and the
	 * (properly formated) statement could error out.
	 *
	 * @param A DataLayerPreparedStatement with the PreparedStatement to execute
	 * @return a DataResult object
	 */
	public static DataResult delete(DataLayerPreparedStatement dlps)
	{
		determineUnitTest();
		return statusTransform(DatabaseUtils.execute(determineEnvironment(),
							   dlps,
							   DatabaseUtils.DBA_DELETE));
	}

	/**
	 * A private method to transform certain db error conditions
	 * to possible "soft fail" status codes.
	 *
	 * @param the DataResult object to transform
	 * @return the transformed DataResult object
	 */
	private static DataResult statusTransform(DataResult inp)
	{
		// Transform errors that may be "soft" errors to status codes here.
		// Currently there is only one status that is transformed, but
		// others could be added as needed.
		DataResult ret = inp;
		if (inp.isInError())
		{
			// Transform the error code as required
			// TODO Refactor so that the update does not depend upon the error code (do a query)
			// Duplicate key error triggers update; error code = 1062 on MySQL, 2627 on SQL Server
			if (inp.getStatus().getExceptionCode() == 2627)		// MySQL error code for dup keys
			{
				ret.getStatus().setStatusCode(RequestStatus.DBS_DUP_KEY);
			}
		}

		return ret;
	}


	/**
	 * Tests a connection to the V2 database
	 * @param the environment (development, preproduction, production)
	 * @return a boolean
	 */
	public static boolean testConnection() 
	{
		V2DatabaseUtils.determineUnitTest();
		
		//System.out.println(PropertyLoader.getProperty("com.pdi.data.v2.application", "database.url"));
		//build sql
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT GETDATE() AS dbTime");
		log.debug("Preparing SQL: {}", sql);
		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sql);
		if (dlps.isInError())
		{
			return false;
		}

		DataResult dr = null;
		
		try
		{
			//get result set
			dr = select(dlps);
			if (dr.isInError())
			{
				return false;
			}
			//do normal resultset stuff
			dr.getResultSet().next();
			
			//get value from result set and do stuff with value
			//String time = dr.getResultSet().getString("dbTime");
			//System.out.println("*** Testing Connection for V2 database: DB time is " + time);
			dr.getResultSet().getString("dbTime");
			
			//close connection

		}
		catch (SQLException ex)
		{
			return false;
		} finally {
			if (dr != null) { dr.close(); dr = null; }
		}
		
		//return value
		return true;
	}
	//
	// Instance data.
	//
	
	//
	// Constructors.
	//

	//
	// Instance methods.
	//

}
