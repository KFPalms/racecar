/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 *
 */
package com.pdi.data.abyd.dto.common;

/**
 * EGBarDisplay Data is a convenience class containing all of the information
 * required to display a single bar in an Eval Guide.
 * 
 * @author		Ken Beukelman
 */
public class EGBarDisplayData
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private int _barSeq;			// The sequence of this bar within the competency that it is displayed
	private int _barScore;			// The current score for this bar
	private String _barHiText;		// The text for the high potential score description
	private String _barMidText;		// The text for the mid potential score description
	private String _barLoText;		// The text for the low potential score description
	private String _barHiExText;	// The text for the high potential example/explanation
	private String _barMidExText;	// The text for the mid potential example/explanation
	private String _barLoExText;	// The text for the low potential example/explanation
	private boolean _visible = true;	// Visible flag for roxcart UI - defaults to true - used in UI only
	
	//
	// Constructors.
	//
	public EGBarDisplayData()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public int getBarSeq()
	{
		return _barSeq;
	}
		
	public void setBarSeq(int value)
	{
		_barSeq = value;
	}

	/*****************************************************************************************/
	public int getBarScore()
	{
		return _barScore;
	}
		
	public void setBarScore(int value)
	{
		_barScore = value;
	}

	/*****************************************************************************************/
	public String getBarHiText()
	{
		return _barHiText;
	}
		
	public void setBarHiText(String value)
	{
		_barHiText = value;
	}

	/*****************************************************************************************/
	public String getBarMidText()
	{
		return _barMidText;
	}
		
	public void setBarMidText(String value)
	{
		_barMidText = value;
	}

	/*****************************************************************************************/
	public String getBarLoText()
	{
		return _barLoText;
	}
		
	public void setBarLoText(String value)
	{
		_barLoText = value;
	}

	/*****************************************************************************************/
	public String getBarHiExText()
	{
		return _barHiExText;
	}
		
	public void setBarHiExText(String value)
	{
		_barHiExText = value;
	}

	/*****************************************************************************************/
	public String getBarMidExText()
	{
		return _barMidExText;
	}
		
	public void setBarMidExText(String value)
	{
		_barMidExText = value;
	}

	/*****************************************************************************************/
	public String getBarLoExText()
	{
		return _barLoExText;
	}
		
	public void setBarLoExText(String value)
	{
		_barLoExText = value;
	}

	/*****************************************************************************************/
	public boolean getVisible()
	{
		return _visible;
	}
		
	public void setVisible(boolean value)
	{
		_visible = value;
	}


	/*
	 * Show me what you've got
	 */
	public String toString()
	{
		String ret = "EGBarDisplayData content (Java)\n";
		ret += "          Sequence=" + _barSeq + "\n";
		ret += "             Score=" + _barScore + "\n";
		ret += "           Hi Text=" + _barHiText + "\n";
		ret += "          Mid Text=" + _barMidText + "\n";
		ret += "           Lo Text=" + _barLoText + "\n";
		ret += "   Example Hi Text=" + _barHiExText + "\n";
		ret += "  Example Mid Text=" + _barMidExText + "\n";
		ret += "   Example Lo Text=" + _barLoExText + "\n";
		
		return ret;
	}
}