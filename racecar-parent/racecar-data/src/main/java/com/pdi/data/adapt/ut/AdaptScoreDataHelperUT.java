/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.adapt.ut;



import java.util.Iterator;
import java.util.Map;

import com.pdi.data.adapt.helpers.AdaptScoreDataHelper;
import com.pdi.properties.PropertyLoader;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

public class AdaptScoreDataHelperUT  extends TestCase
{
	public AdaptScoreDataHelperUT(String name)
	{
		super(name);
	}

	public void testProperties()
	throws Exception
	{
		UnitTestUtils.start(this);
		
		//Does the local property work?
		String applicationId = PropertyLoader.getProperty("com.pdi.data.adapt.application", "application.code");
		assertEquals(applicationId != null, true);
		assertEquals(applicationId.equals("PDI-DATA-ADAPT"), true);
		
		UnitTestUtils.stop(this);
	}
	
	public void testAdapt()
		throws Exception
	{
		UnitTestUtils.start(this);
		
//		// Project - MB test Project 11/2/05, Person - Ben Skywalker
//		long engmnt = 289;
//		long pers= 1029;	
//		// Project - Test Freddie Mac Project, Person - Freddie Mac Testuser
//		long engmnt = 410;
//		long pers= 966;	
//		// Project - Test Bob's Project, Person - Bob1 Testuser  NOTE-Very little answer data present
//		long engmnt = 424;
//		long pers= 1010;	
//		// Project - Copyright Project, Person - Copyright Testuser
//		long engmnt = 451;
//		long pers= 1090;
		// Project - ??, Person - ??
		long engmnt = 432;
		long pers= 1109;
		// testing - Invalid person/engagement
		//long engmnt = 4573;
		//long pers= 1102;	

		
		IndividualReport ir = AdaptScoreDataHelper.pullAdaptData(engmnt, pers, false); 
		
		// Dump the results
		System.out.println("-----------------------------------------------------------------------");
		System.out.println("                      Results Dump from UT");
		System.out.println("-----------------------------------------------------------------------");
		if (ir == null)	// Should only happen if the person isn't in the engagement
		{
			System.out.println("ERROR:  No IndividualReport object returned; pid " + pers + " is not in eid " + engmnt + ".");
		}
		else if (ir.getDisplayData().isEmpty())
		{
			System.out.println("No display data found");
		} else {
			System.out.println("Display data:");
			for (Iterator<Map.Entry<String, String>> itr = ir.getDisplayData().entrySet().iterator(); itr.hasNext(); )
			{
				Map.Entry<String, String> ent = itr.next();
				System.out.println("  " + ent.getKey() + "=" + ent.getValue());
			}
			if (ir.getReportData().isEmpty())
			{
				System.out.println("No report data found");
			}
			System.out.println("Report data:");
			for (Iterator<String> itr = ir.getReportData().keySet().iterator(); itr.hasNext(); )
			{
				String key = itr.next();
				ReportData rd = ir.getReportData().get(key);
				System.out.println("Report type - " + key + ":");
				
				//if (! key.equals("CHQ"))
				//{
				//	// Skip all but above named module(s) (DEBUG)
				//	continue;
				//}
				
				// raw data - input scores
				System.out.println("  Raw data:");
				for (Iterator<Map.Entry<String, String>> it2 = rd.getRawData().entrySet().iterator(); it2.hasNext(); )
				{
					Map.Entry<String, String> me = it2.next();
					System.out.println("    " + me.getKey() + "=" + me.getValue());
				}
				
				// score data - raw scale scores
				System.out.println("  Score data:");
				for (Iterator<Map.Entry<String, String>> it3 = rd.getScoreData().entrySet().iterator(); it3.hasNext(); )
				{
					Map.Entry<String, String> me2 = it3.next();
					System.out.println("    " + me2.getKey() + "=" + me2.getValue());
				}
			}
		}

		UnitTestUtils.stop(this);
	}
}
