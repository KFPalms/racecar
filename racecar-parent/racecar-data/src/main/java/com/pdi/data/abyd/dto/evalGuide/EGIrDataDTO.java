/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.dto.evalGuide;

import java.util.ArrayList;


/**
 * EGIrScoreDTO is a thin class containing all of the data
 * needed to add or update the Impact Rating data for an
 * Eval Guide.
 * 
 * @author		Ken Beukelman
 */
public class EGIrDataDTO
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _participantId;
	private long _dnaId;
	private long _moduleId;
	private String _irNote;					// Consultant entered notes about Importance Ratings
	private ArrayList<EGIrScoreData> _irArray;	// Importance Rating score items


	//
	// Constructors.
	//
	public EGIrDataDTO()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public String getParticipantId()
	{
		return _participantId;
	}
		
	public void setParticipantId(String value)
	{
		_participantId = value;
	}

	/*****************************************************************************************/
	public long getDnaId()
	{
		return _dnaId;
	}
		
	public void setDnaId(long value)
	{
		_dnaId = value;
	}

	/*****************************************************************************************/
	public long getModuleId()
	{
		return _moduleId;
	}
		
	public void setModuleId(long value)
	{
		_moduleId = value;
	}

	/*****************************************************************************************/
	public String getIrNotes()
	{
		return _irNote;
	}
		
	public void setIrNotes(String value)
	{
		_irNote = value;
	}
	
	/*****************************************************************************************/
	public ArrayList<EGIrScoreData> getIrArray()
	{
		return _irArray;
	}
		
	public void setIrArray(ArrayList<EGIrScoreData> value)
	{
		_irArray = value;
	}
}
