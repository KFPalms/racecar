/**
 * Copyright (c) 2008 Personnel Decisions International, Inc.
 * All rights reserved.
 *
 * $Header: /Platform/Java/sourceCode/com/pdicorp/app/tcm/export/vo/ExportDataVO.java 6     8/25/05 3:23p Kbeukelm $
 */
package com.pdi.data.abyd.dto.setup.NoLongerUsed;

import java.util.ArrayList;

import com.pdi.data.abyd.dto.common.KeyValuePair;
import com.pdi.data.abyd.dto.common.CompElementDTO;


/**
 * SPFullDataDTO is a thin class containing list of competencies, competency elements and text
 * elements.  It contains everything needed to fetch all of the data for SP processing.
 * 
 * @author		Ken Beukelman
 */
public class SPFullDataDTO
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private ArrayList<KeyValuePair> _compList;		// An array of competencies in display order
	private ArrayList<CompElementDTO> _compElts;	// An array of CompElementDTO objects
	private ArrayList<KeyValuePair> _textElts;		// An collection of KeyValuePair objects

	//
	// Constructors.
	//
	public SPFullDataDTO()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public ArrayList<KeyValuePair> getCompList()
	{
		return _compList;
	}
	
	public void setCompList(ArrayList<KeyValuePair> value)
	{
		_compList = value;
	}	

	/*****************************************************************************************/
	public ArrayList<CompElementDTO> getCompElts()
	{
		return _compElts;
	}

	public void setCompElts(ArrayList<CompElementDTO> value)
	{
		_compElts = value;
	}	

	/*****************************************************************************************/
	public ArrayList<KeyValuePair> getTextElts()
	{
		return _textElts;
	}

	public void setTextElts(ArrayList<KeyValuePair> value)
	{
		_textElts = value;
	}	
}