/**
 * Copyright (c) 2012, 2014 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.loaders;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

//import org.apache.poi.hssf.usermodel.HSSFSheet;
//import org.apache.poi.hssf.usermodel.HSSFWorkbook;
//import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;


/**
 * BLAccessTest is cut down from BARLoader and performs all of the preliminary existing data loads
 * (for checking) found there.  Instead of inserting the data, however, it reads to see if the data
 * already exists and throws a message if it does so.  After a pre-determined number of matches
 * (MAX_ERRORS - currently set at 10) are found it stops further checking.
 * 
 * @author		Ken Beukelman
 */
public class BLAccessTest
{
	//
	// Static variables
	//
//	private static final String DRIVER = "com.mysql.jdbc.Driver";
	private static final int MAX_ERRORS = 10;

	//
	// Static classes
	//
	public static void main(String args[])
	{
		// The arguments are specified in the Run program Arguments in eclipse.
		if (args.length == 0)
		{
			System.out.println(" No parameters were detected.");
			System.out.println(parmDescString());

			return;
		}

		if (args.length != 4)
		{
			System.out.println("Incorrect number of parameters (" + args.length + ")");
			System.out.println(parmDescString());
			return;
		}
		
		String url = args[0];
		String uid = args[1];
		String pw = args[2];
		String xlFile = args[3];
		Connection con;
		
		// connect to the database
		try
		{
			//Class.forName(DRIVER);
			con = DriverManager.getConnection(url, uid, pw);
			//con.setAutoCommit(false);	// This is not operative in My SQL as currently configured
		}
		catch (Exception e)
		{
			System.out.println("Unable to open database connection");
			e.printStackTrace();
			return;
		}
		System.out.println("Connected to " + url);

		BLAccessTest loader = new BLAccessTest();
		loader.run(con, uid, xlFile);
	}
	
	
	/*
	 * Method to display appropriate parameters.
	 * 
	 * Note that there is no closing new-line character; assumes that this method is called
	 * in a println() or that the user will handle the closing new-line himself.
	 */
	private static String parmDescString()
	{
		String str = "  This program requires four arguments as follows:\n";
		str += "    URL      - The URL where the database lies.  Examples:\n";
		str += "                 'jdbc:mysql://mspdbdev2:3306/pdi_certify' (for MySQL) or\n";
		str += "                 'jdbc:sqlserver://INTDB01:1433;DatabaseName=scoring_reporting' (for SQL Server)\n";
//		str += "       DON'T FORGET THE CHARACTER SET PARAMS AFTER THE URL! Use the following:\n";
//		str += "         \"?useUnicode=true&characterEncoding=UTF-8&characterSetResults=UTF-8\"\n";
		str += "    User id  - A user id for the specified database with the appropriate permissions.\n";
		str += "    Password - The password for the above user ID.\n";
		str += "    Worksheet - The Path and name of the worksheet that the BAR data is in (enclose in double quotes if it conatins embedded spaces).\n";
		
		return str;
	}

	//
	// Instance variables
	//
//	private String lastUser = "";
	private Map<String, Long> modelMap = null;
	private Map<String, Long> simMap = null;
	private Map<String, Long> compMap = null;
	private ArrayList<BarData> barArray = null;
//	private PreparedStatement txtInsPs = null;
//	private PreparedStatement getSeqPs = null;
//	private PreparedStatement txtUpdtPs = null;
	
	
		//
		// Instance methods
		//
		/**
		 * Controller method for the load
		 */
		protected void run(Connection con, String uid, String fileName)
		{
//			this.lastUser = uid;
			
			// Get models
			this.modelMap = getModelData(con);
			if (this.modelMap == null)
				return;

			// Get modules/sims/instruments
			this.simMap = getSimData(con);
			if (this.simMap == null)
				return;

			// Get competencies
			this.compMap = getCompData(con);
			if (this.compMap == null)
				return;

			// Fetch the BARs
			this.barArray = readBarData(fileName);
			if (barArray == null)
			{
				System.out.print("Error detected fetching bar data.  Exiting.");
				return;
			}
			////Debug
			//for(BarData bar : barArray)
			//{
			//	System.out.println(bar.toString());
			//}

			System.out.println("Access testing only!  Load is not performed.");
//			// Load the BARs
//			if (loadBars(con))
//				System.out.println("FLL BAR load completed successfully.");
			// Check the BAR keys
			checkKeys(con);
			System.out.println("Testing ends.");
		}
			
	
		/**
		 * Fetches the model data to a map for later lookup
		 */
		private Map<String, Long> getModelData(Connection con)
		{
			System.out.println("  Fetching model info");
			
			Map<String, Long> ret = new HashMap<String, Long>();

			Statement stmt = null;
		    ResultSet rs = null;

		    StringBuffer query = new StringBuffer();
		    query.append("SELECT mdl.internalName, ");
		    query.append("       mdl.modelId");
		    query.append("  FROM pdi_abd_model mdl ");
		    query.append("  WHERE mdl.active = 1");

		    try
		    {
		    	stmt = con.createStatement();
		        rs = stmt.executeQuery(query.toString());
	
				while (rs.next())
				{
					ret.put(rs.getString("internalName").trim(), new Long(rs.getLong("modelId")));
				}

				return ret;
		     }
		    catch (SQLException se)
		    {
				System.out.println("Error detected in getModelData.");
				se.printStackTrace();
				return null;
		    }
		    finally
		    {
		    	if (stmt != null)
		    	{
		        	try { stmt.close(); }
		        	catch (SQLException se) { /* swallow the exception */ }
		    		stmt = null;
		    	}
		    	if (rs != null)
		    	{
		        	try { rs.close(); }
		        	catch (SQLException se) { /* swallow the exception */ }
		    		rs = null;
		    	}
		    }
		}


		/**
		 * Fetches the module/sim/instrument data to a map for later lookup
		 */
		private Map<String, Long> getSimData(Connection con)
		{
			System.out.println("  Fetching sim info");

			Map<String, Long> ret = new HashMap<String, Long>();

			Statement stmt = null;
			ResultSet rs = null;
	
			StringBuffer query = new StringBuffer();
			query.append("SELECT sim.internalName, ");
			query.append("       sim.moduleId ");
			query.append("  FROM pdi_abd_module sim");
			try
			{
				stmt = con.createStatement();
				rs = stmt.executeQuery(query.toString());
	
				while (rs.next())
				{
					ret.put(rs.getString("internalName").trim(), new Long(rs.getLong("moduleId")));
				}
				
				return ret;
			}
			catch (SQLException se)
			{
				System.out.println("Error detected in getSimData.");
				se.printStackTrace();
				return null; 
			}
			finally
			{
				try { stmt.close(); }
				catch (SQLException se) { /* swallow the exception */ }
				stmt = null;
				try { rs.close(); }
				catch (SQLException se) { /* swallow the exception */ }
				rs = null;
			}
		}
	

		/**
		 * Fetches the competency data to a map for later lookup
		 */
		private Map<String, Long> getCompData(Connection con)
		{
			System.out.println("  Fetching comp info");
	
			Map<String, Long> ret = new HashMap<String, Long>();
	
			Statement stmt = null;
			ResultSet rs = null;
		
			StringBuffer query = new StringBuffer();
			query.append("SELECT cmp.internalName, ");
			query.append("       cmp.competencyId ");
			query.append("  FROM pdi_abd_competency cmp");
			try
			{
				stmt = con.createStatement();
				rs = stmt.executeQuery(query.toString());
		
				while (rs.next())
				{
					ret.put(rs.getString("internalName").trim(), new Long(rs.getLong("competencyId")));
				}
				
				return ret;
			}
			catch (SQLException se)
			{
				System.out.println("Error detected in getCompData.");
				se.printStackTrace();
				return null;
			}
			finally
			{
				try { stmt.close(); }
				catch (SQLException se) { /* swallow the exception */ }
				stmt = null;
				try { rs.close(); }
				catch (SQLException se) { /* swallow the exception */ }
				rs = null;
			}
		}
	
	
		/*
		 * Read the bars.
		 * 
		 * Assumes an old-format (.xls) spreadsheet <-- Not any more... Now uses IWorkbook to determine the type
		 */
		private ArrayList<BarData> readBarData(String fileName)
		{
			ArrayList<BarData> ret = new ArrayList<BarData>();
	
			try
			{
				// Open the spreadsheet
				//POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(fileName));
				//HSSFWorkbook wb = new HSSFWorkbook(fs);
				Workbook wb = WorkbookFactory.create(new FileInputStream(fileName));
				
				System.out.println("  " + fileName + " has "+ wb.getNumberOfSheets() + " sheets");
	
				//for (int i=1; i < wb.getNumberOfSheets(); i++)
				for (int i=0; i < wb.getNumberOfSheets(); i++)
				{
					Sheet sheet = wb.getSheetAt(i);
					System.out.println("  Sheet " + i + ":  " + sheet.getSheetName() + " (" + sheet.getLastRowNum()+ " rows)");
	
					for (Row row : sheet)
					{
						// Always skip row 0 in the sheet (it is the header)
						if (row.getRowNum() == 0)
						{
							continue;
						}
						
						BarData bd = new BLAccessTest().new BarData();
						for (Cell cell : row)
						{
							int idx = cell.getColumnIndex();
							switch (idx)
							{
								case 0:		// Model name - String
									if (cell.getCellType() != Cell.CELL_TYPE_STRING)
									{
										throw new Exception("Sheet: " + sheet.getSheetName() + ", row " + row.getRowNum() + ", dataType=" + cell.getCellType() + ": Model name expected");
									}
									bd.setModel(cell.getStringCellValue().trim());
									break;
								case 1:		// Module name
									if (cell.getCellType() != Cell.CELL_TYPE_STRING)
									{
										throw new Exception("Sheet: " + sheet.getSheetName() + ", row " + row.getRowNum() + ", dataType=" + cell.getCellType() + ": Module name expected");
									}
									bd.setModule(cell.getStringCellValue().trim());
									break;
								case 2:		// Competency name
									if (cell.getCellType() != Cell.CELL_TYPE_STRING)
									{
										throw new Exception("Sheet: " + sheet.getSheetName() + ", row " + row.getRowNum() + ", dataType=" + cell.getCellType() + ": Competency name expected");
									}
									bd.setComp(cell.getStringCellValue().trim());
									break;
								case 3:		// BAR sequence
									if (cell.getCellType() != Cell.CELL_TYPE_NUMERIC)
									{
										throw new Exception("Sheet: " + sheet.getSheetName() + ", row " + row.getRowNum() + ", dataType=" + cell.getCellType() + ": Sequence number expected");
									}
					    			bd.setSeq((int)cell.getNumericCellValue());
									break;
								case 4:		// Behavior id
									if (cell.getCellType() != Cell.CELL_TYPE_NUMERIC)
									{
										throw new Exception("Sheet: " + sheet.getSheetName() + ", row " + row.getRowNum() + ", dataType=" + cell.getCellType() + ": Behavior ID expected");
									}
					    			bd.setBehId((long)cell.getNumericCellValue());
									break;
								case 5:		// High value (5) text
									if (cell.getCellType() != Cell.CELL_TYPE_STRING)
									{
										throw new Exception("Sheet: " + sheet.getSheetName() + ", row " + row.getRowNum() + ", dataType=" + cell.getCellType() + ": High (5) text expected");
									}
					    			bd.setHi(cell.getStringCellValue().trim());
									break;
								case 6:		// Middle value (3) text
									if (cell.getCellType() != Cell.CELL_TYPE_STRING)
									{
										throw new Exception("Sheet: " + sheet.getSheetName() + ", row " + row.getRowNum() + ", dataType=" + cell.getCellType() + ": Mid (3) text expected");
									}
					    			bd.setMid(cell.getStringCellValue().trim());
									break;
								case 7:		// Low value (1) text
									if (cell.getCellType() != Cell.CELL_TYPE_STRING)
									{
										throw new Exception("Sheet: " + sheet.getSheetName() + ", row " + row.getRowNum() + ", dataType=" + cell.getCellType() + ": Low (1) text expected");
									}
					    			bd.setLo(cell.getStringCellValue().trim());
									break;
								case 8:		// High value (5) example text
									if (cell == null || cell.getCellType() == Cell.CELL_TYPE_BLANK)
									{
										// Do nothing... leave the entry as null in the BarData object
									}
									else if (cell.getCellType() == Cell.CELL_TYPE_STRING)
									{
										bd.setHiE(cell.getStringCellValue().trim());
									}
									else
									{
										// Bad data
										throw new Exception("Sheet: " + sheet.getSheetName() + ", row " + row.getRowNum() + ", dataType=" + cell.getCellType() + ": High (5) extended text expected");
									}
									break;
								case 9:		// Middle value (3) example text
									if (cell == null || cell.getCellType() == Cell.CELL_TYPE_BLANK)
									{
										// Do nothing... leave the entry as null in the BarData object
									}
									else if (cell.getCellType() == Cell.CELL_TYPE_STRING)
									{
										bd.setMidE(cell.getStringCellValue().trim());
									}
									else
									{
										// Bad data
										throw new Exception("Sheet: " + sheet.getSheetName() + ", row " + row.getRowNum() + ", dataType=" + cell.getCellType() + ": Mid (3) extended text expected");
									}
									break;
								case 10:		// Low value (1) example text
									if (cell == null || cell.getCellType() == Cell.CELL_TYPE_BLANK)
									{
										// Do nothing... leave the entry as null in the BarData object
									}
									else if (cell.getCellType() == Cell.CELL_TYPE_STRING)
									{
										bd.setLoE(cell.getStringCellValue().trim());
									}
									else
									{
										// Bad data
										throw new Exception("Sheet: " + sheet.getSheetName() + ", row " + row.getRowNum() + ", dataType=" + cell.getCellType() + ": Low (1) extended text expected");
									}
									break;
								default:
									System.out.println("Sheet: " + sheet.getSheetName() + ", row " + row.getRowNum() + ", column=" + cell.getColumnIndex() + ", dataType=" + cell.getCellType() + ": No data expected... ignored");
							}	// End of the Cell switch
						}	// End of the Cell loop
						
						//Save the BarData object
						ret.add(bd);
					}	// End of Row loop
				}	// End of sheet FOR loop
				
				return ret;
			}
			catch (Exception e)
			{
				System.out.println("Error in BAR data fetch.");
				e.printStackTrace();  
				return null;
			}
		}


		/**
		 * Checks the keys against the database.
		 * Assumes that the data in the barArray is sorted by model, module, and competency
		 */
		private void checkKeys(Connection con)
		{
			StringBuffer query = new StringBuffer();
			PreparedStatement keyCheckPs = null;
			ResultSet rs = null;
	
	 		String curModel = null;
	 		String curModule= null;
	 		int curBehCnt = 0;
	 		int errCnt = 0;

			try
			{
				// Existence check query
				query.append("SELECT 'x' ");
				query.append("  FROM pdi_abd_eg_bar ");
				query.append("  WHERE modelId = ? ");
				query.append("    AND moduleId = ? ");
				query.append("    AND competencyId = ? ");
				query.append("    AND barSequence = ?");
				keyCheckPs = con.prepareStatement(query.toString());

				// loop through the bar array
				for (BarData theBar : this.barArray)
				{ 
					if (curModel == null  ||  (! curModel.equals(theBar.getModel())) )
					{
						// Got a new model
						if (curModel != null)
						{
							// Not the first time... put out the count
							System.out.println("");
							System.out.println("        Total BARs in module:  " + curBehCnt);
							curBehCnt = 0;
							curModule = null;
						}
						curModel = theBar.getModel();
						System.out.println("  Checking keys for model:  " + curModel);
					}
					if (curModule == null  ||  (! curModule.equals(theBar.getModule())) )
					{
						if (curModule != null)
						{
							System.out.println("");
							System.out.println("        Total BARs in module:  " + curBehCnt);
							curBehCnt = 0;
						}
						curModule = (String) theBar.getModule();
						System.out.print("    Checking Module:  " + curModule);
					}

					keyCheckPs.setLong(1, ((Long)this.modelMap.get(theBar.getModel())).longValue() );
					keyCheckPs.setLong(2, ((Long)this.simMap.get(theBar.getModule())).longValue() );
					keyCheckPs.setLong(3, ((Long)this.compMap.get(theBar.getComp())).longValue() );
					keyCheckPs.setInt(4, theBar.getSeq());

					rs = keyCheckPs.executeQuery();
					if (rs.isBeforeFirst())
					{
						//Got an error report it
						System.out.println("Key Exists: Model=" + theBar.getModel() +
													   ", Sim=" + theBar.getModule() +
													   ", Comp=" + theBar.getComp() +
													   ". Seq=" + theBar.getSeq());
						errCnt++;
					}
					else
					{
						// Not there... good
						System.out.print(".");
						curBehCnt++;
					}

					if (errCnt >= MAX_ERRORS)
					{
						System.out.println("Max. Errors (" + MAX_ERRORS + ") reached... exiting");
						break;
					}
		    	}	// end of for loop

				// put out the last count
				System.out.println("");
				System.out.println("        Total BARs in module:  " + curBehCnt);
				
				return;
			}
			catch (SQLException se)
			{
				System.out.println("\nSQL error in keyCheck.");
				se.printStackTrace();
				return; 
			}
			finally
			{
				if(rs != null)
				{
					try  {  rs.close();  }
					catch (SQLException e) { // swallow it
					}
				}
				if (keyCheckPs != null)
				{
					try {  keyCheckPs.close();  }
					catch (SQLException e) { // swallow it
					}
				}
			}
		}
	
		
	
		/**
		 * Copyright (c) 2009, 2011, 2012 Personnel Decisions International, Inc.
		 * All rights reserved.
		 * 
		 * @author		Ken Beukelman
		 */
		private class BarData
		{
		    //
		    // Static variables
		    //
			   
		    //
		    // Instance variables
		    //
			private String _model = null;
			private String _module = null;
			private String _comp = null;
			private int _seq = 0;
			private long _bId = 0;
			private String _hi = null;
			private String _mid = null;
			private String _lo = null;
			private String _hiE = null;
			private String _midE = null;
			private String _loE = null;
	
		    //
		    // Class methods
		    //
	
		    //
		    // Instance methods
		    //
			public String toString()
			{
				String ret = "";
				ret += _model;
				ret += "|" + _module;
				ret += "|" + _comp;
				ret += "|" + _seq;
				ret += "|" + _bId;
				ret += "\n   Hi-" + _hi;
				ret += "\n  Mid-" + _mid;
				ret += "\n  Low-" + _lo;
				ret += "\n   HiE-" + _hiE;
				ret += "\n  MidE-" + _midE;
				ret += "\n  LowE-" + _loE;
				
				return ret;
			}
	
			/******************************************/
			public String getModel()
			{
				return _model;
			}
	
			public void setModel(String value)
			{
				_model = value;
			}
	
			/******************************************/
			public String getModule()
			{
				return _module;
			}
	
			public void setModule(String value)
			{
				_module = value;
			}
	
			/******************************************/
			public String getComp()
			{
				return _comp;
			}
	
			public void setComp(String value)
			{
				_comp = value;
			}
	
			/******************************************/
			public int getSeq()
			{
				return _seq;
			}
	
			public void setSeq(int value)
			{
				_seq = value;
			}
	
			/******************************************/
//			public long getBehId()
//			{
//				return _bId;
//			}
	
			public void setBehId(long value)
			{
				_bId = value;
			}
	
			/******************************************/
//			public String getHi()
//			{
//				return _hi;
//			}
	
			public void setHi(String value)
			{
				_hi = value;
			}
	
			/******************************************/
//			public String getMid()
//			{
//				return _mid;
//			}
	
			public void setMid(String value)
			{
				_mid = value;
			}
	
			/******************************************/
//			public String getLo()
//			{
//				return _lo;
//			}
	
			public void setLo(String value)
			{
				_lo = value;
			}
	
			/******************************************/
//			public String getHiE()
//			{
//				return _hiE;
//			}
	
			public void setHiE(String value)
			{
				_hiE = value;
			}
	
			/******************************************/
//			public String getMidE()
//			{
//				return _midE;
//			}
	
			public void setMidE(String value)
			{
				_midE = value;
			}
	
			/******************************************/
//			public String getLoE()
//			{
//				return _loE;
//			}
	
			public void setLoE(String value)
			{
				_loE = value;
			}
		}	// end of BarData private class
}
