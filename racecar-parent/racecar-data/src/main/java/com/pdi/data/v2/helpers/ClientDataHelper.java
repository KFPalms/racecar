/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v2.helpers;

import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import javax.xml.xpath.*;

import com.pdi.data.v2.dto.ClientDTO;
import com.pdi.data.v2.dto.SessionUserDTO;
import com.pdi.data.v2.util.V2WebserviceUtils;
//import com.pdi.xml.XMLUtils;

public class ClientDataHelper
{
//	/**
//	 * Get all clients that a particular user has access to
//	 * Test status on client by client basis
//	 * @param sessionUser
//	 * @return
//	 */
//	public  ArrayList<ClientDTO> getClients(SessionUserDTO sessionUser) throws Exception
//	{
//		ArrayList<ClientDTO> clients = new ArrayList<ClientDTO>();
//		
//		String xml = "";
//		xml += "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
//		xml += "<ETR SessionId=\""+sessionUser.getSessionId()+"\">";
//		xml += "<Rqt Cmd=\"PickList\" Id=\"CLM001\" /></ETR>";
//		XPath xpath = XPathFactory.newInstance().newXPath(); 
//		
//		V2WebserviceUtils v2web = new V2WebserviceUtils();
//		Document doc = v2web.sendData(xml);
//		
//		NodeList records = (NodeList) xpath.evaluate("//I", doc, XPathConstants.NODESET); 
//		
//		for(int i = 0; i < records.getLength(); i++) {
//			Node record = records.item(i);
//			ClientDTO client = new ClientDTO(record);
//			clients.add(client);
//		}
//		
//		return clients;
//	}


	/**
	 * Get all clients that a particular user has access to
	 * Test status on client by client basis
	 * @param sessionUser
	 * @return
	 */
	public  ArrayList<ClientDTO> getClients(SessionUserDTO sessionUser) throws Exception
	{
		
		ArrayList<ClientDTO> clients = new ArrayList<ClientDTO>();

		//TODO: Need to create sql here to get Clients listed depending on user access level... ARRRGHHH!
		String xml = "";
		xml += "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
		xml += "		<ETR SessionId=\"${sessionId}\">";
		xml += "			<Rqt Cmd=\"OpenSQL\" AN=\"JobListSQL\" FA=\"Y\" FR=\"1\" RC=\"N\" SQ=\"SELECT UniqueIdStamp, Name, Parent, ClientType FROM Clients where ( UniqueIdStamp = (select ClientID from users where UniqueIDStamp = '${userId}') or 'EXT_ROOT' = (select ClientID from users where UniqueIDStamp = '${userId}') or 'INT_ROOT' = (select ClientID from users where UniqueIDStamp = '${userId}') ) ORDER BY Name\" />";		
		xml += "			<Rqt Cmd=\"Read\" AN=\"JobListSQL\" Sub=\"JobList\" Rpt=\"*\" RQ=\"N\">";
		xml += "				<Fld FN=\"UniqueIdStamp\" Fmt=\"A\" />";
		xml += "				<Fld FN=\"Name\" Fmt=\"A\" />";
		xml += "				<Fld FN=\"Parent\" Fmt=\"A\" />";
		xml += "				<Fld FN=\"ClientType\" Fmt=\"A\" />";

		xml += "			</Rqt>";
		xml += "			<Rqt Cmd=\"Close\" AN=\"JobListSQL\" />";
		xml += "		</ETR>";
		xml = xml.replace("${sessionId}", sessionUser.getSessionId());
		xml = xml.replace("${userId}", sessionUser.getId());
		
		XPath xpath = XPathFactory.newInstance().newXPath(); 
			
		V2WebserviceUtils v2web = new V2WebserviceUtils();
		Document doc = v2web.sendData(xml);
		NodeList records = (NodeList) xpath.evaluate("//Record", doc, XPathConstants.NODESET); 
		
		for(int i = 0; i < records.getLength(); i++) {
			Node record = records.item(i);
			ClientDTO client = new ClientDTO(record);
			clients.add(client);
		}
		
		return clients;
	}
}
