/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.dto.intGrid;


/**
 * IGTestNormNameData is a container for test name/norm name data used
 * when displaying data in the IG.
 * 
 * @author		Ken Beukelman
 */
public class IGTestAndNormNameData
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _testName;
	private String _normName;

	//
	// Constructors.
	//
	public IGTestAndNormNameData()
	{
		// does nothing currently
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public String getTestName()
	{
		return _testName;
	}
	
	public void setTestName(String value)
	{
		_testName = value;
	}	

	/*****************************************************************************************/
	public String getNormName()
	{
		return _normName;
	}
	
	public void setNormName(String value)
	{
		_normName = value;
	}	



	/*
	 * Show me what you've got
	 */
	public String toString()
	{
		String ret = "IGTestAndNormNameData content (Java)\n";
		ret += "  Test Name=" + _testName + "\n";
		ret += "  Norm Name=" + _normName + "\n";

		return ret;
	}
}
