/**
 * Copyright (c) 2008, 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v2.helpers;

//import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.OutputStreamWriter;
import java.io.StringReader;
//import java.io.UnsupportedEncodingException;
//import java.io.Writer;
//import java.lang.reflect.Array;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.*;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.pdi.data.dto.Participant;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.v2.constants.ManualEntryConstants;
import com.pdi.data.v2.dto.*;
import com.pdi.data.v2.dataObjects.*;
import com.pdi.data.v2.util.NormUtils;
import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.data.v2.constants.EntryAndScoringConstants;
import com.pdi.xml.XMLUtils;

/**
 * ManualEntryDataHelper contains all the code needed to extract and return a ManualEntryDTO for
 * the Manual Entry Vendor Application.  It provides data base access and data reduction for the
 *  Assessment-by-Design Project Setup app
 *
 * @author		MB Panichi 
 * @author		Ken Beukelman (original code)
 * @version	$Revision: 6 $  $Date: 8/25/05 3:23p $
 */
public class ManualEntryDataHelper
{
	//
	//  Static data.
	//
	public static final transient HashMap <String, Number> CPI_CONSTRUCTS = new HashMap <String, Number> ();
	{
		CPI_CONSTRUCTS.put("LLZZFUIE", 1);	//CPI Reliability
		CPI_CONSTRUCTS.put("HOZJAOQZ", 2);	//CPI Gender
		CPI_CONSTRUCTS.put("DUCNXIIU", 3);	//CPI Type
		CPI_CONSTRUCTS.put("DVAREUYD", 4);	//CPI DO
		CPI_CONSTRUCTS.put("HTAXOLWI", 6);	//CPI SY
		CPI_CONSTRUCTS.put("IVFNVVYQ", 7);	//CPI SP
		CPI_CONSTRUCTS.put("DURXDROO", 8);	//CPI SA
		CPI_CONSTRUCTS.put("CNMBHCQL", 9);	//CPI IN		
		CPI_CONSTRUCTS.put("FCJNTWXI", 10);	//CPI EM
		CPI_CONSTRUCTS.put("FFWYCPSQ", 11); //CPI RE
		CPI_CONSTRUCTS.put("LKTBHTXO", 12);	//CPI SO
		CPI_CONSTRUCTS.put("BMVSWTAL", 13);	//CPI SC
		CPI_CONSTRUCTS.put("DUNFVGUA", 14);	//CPI GI
		CPI_CONSTRUCTS.put("KCKHSHUD", 15);	//CPI CM
		CPI_CONSTRUCTS.put("FFIEQYVH", 16);	//CPI WB
		CPI_CONSTRUCTS.put("CNACMYBF", 17);	//CPI TO
		CPI_CONSTRUCTS.put("HMGQUHSX", 18);	//CPI AC
		CPI_CONSTRUCTS.put("FDHTEEEQ", 19);	//CPI AI
		CPI_CONSTRUCTS.put("BKCDZIRI", 20);	//PI IE
		CPI_CONSTRUCTS.put("DXTTTKVO", 21);	//CPI PY
		CPI_CONSTRUCTS.put("HOZBMIBO", 22);	//CPI FX
		CPI_CONSTRUCTS.put("HSHOBDDG", 23);	//CPI FM
		CPI_CONSTRUCTS.put("ITFUGMOX", 24);	//CPI MP
		CPI_CONSTRUCTS.put("IXONGOZU", 25);	//CPI WO
		CPI_CONSTRUCTS.put("KFBNCYWX", 26);	//CPI CT
		CPI_CONSTRUCTS.put("KESEXPCS", 27);	//CPI LP
		CPI_CONSTRUCTS.put("FFWORDYJ", 28);	//CPI AMI
		CPI_CONSTRUCTS.put("DVOAZEZI", 29);	//CPI LEO
		CPI_CONSTRUCTS.put("FBMHLHPK", 30);	//CPI TM
	};

	public static final transient HashMap <String, Number> EAS_CONSTRUCTS = new HashMap <String, Number> ();
	{
		EAS_CONSTRUCTS.put("ISSTJWID", 1);	//EAS1
		EAS_CONSTRUCTS.put("CQRGVGDB", 2);	//EAS6
		EAS_CONSTRUCTS.put("EZZXXQIF", 3);	//EAS7
	};
	
	public static final transient String VERIFY_CONSTRUCT = "KBPMPMRE";  // name = "Verify"
	public static final transient HashMap <String, Number> VERIFY_CONSTRUCTS = new HashMap <String, Number> ();
	{
		VERIFY_CONSTRUCTS.put("KBPMPMRE", 1);	//"Verify"

	};
	
	// Construct Maps for the A by D Manual entry
	public static final transient HashMap <String, Number> GPI_CONSTRUCTS = new HashMap <String, Number> ();
	{
		GPI_CONSTRUCTS.put("KEAPJLBK", 1);	//	Thought Agility
		GPI_CONSTRUCTS.put("KEIHXMEN", 2);	//	Innovation/Creativity
		GPI_CONSTRUCTS.put("CQYBFDTY", 3);	//	Thought Focus
		GPI_CONSTRUCTS.put("BKCPWARH", 4);	//	Vision
		GPI_CONSTRUCTS.put("HRTQMZQN", 5);	//	Attention to Detail
		GPI_CONSTRUCTS.put("BINLNTVG", 6);	//	Work Focus
		GPI_CONSTRUCTS.put("LKJQVTAO", 7);	//	Taking Charge
		GPI_CONSTRUCTS.put("JZFGMAAR", 8);	//	Influence
		GPI_CONSTRUCTS.put("CRGUODBU", 9);	//	Ego-centered
		GPI_CONSTRUCTS.put("EZWJRFWY", 10);	//	Manipulation
		GPI_CONSTRUCTS.put("LJNUOYOP", 11);	//	Micro-managing
		GPI_CONSTRUCTS.put("HNLORCQB", 12);	//	Intimidating
		GPI_CONSTRUCTS.put("BLIHZLDM", 13);	//	Passive-aggressive
		GPI_CONSTRUCTS.put("COTAPFDP", 14);	//	Sociability
		GPI_CONSTRUCTS.put("BLFQHAFX", 15);	//	Consideration
		GPI_CONSTRUCTS.put("JZRBLNQC", 16);	//	Empathy
		GPI_CONSTRUCTS.put("DTUDFZBI", 17);	//	Trust
		GPI_CONSTRUCTS.put("FETTIQMV", 18);	//	Social Astuteness
		GPI_CONSTRUCTS.put("HPOZYCGM", 19);	//	Energy Level
		GPI_CONSTRUCTS.put("KFBBJPBH", 20);	//	Initiative
		GPI_CONSTRUCTS.put("IZADDTKZ", 21);	//	Desire for Achievement
		GPI_CONSTRUCTS.put("KDDVAAYJ", 22);	//	Adaptability
		GPI_CONSTRUCTS.put("CREBXPZF", 23);	//	Openness
		GPI_CONSTRUCTS.put("KACZXSJA", 24);	//	Negative Affectivity
		GPI_CONSTRUCTS.put("CPJIWVWK", 25);	//	Optimism
		GPI_CONSTRUCTS.put("CNUHTURC", 26);	//	Emotional Control
		GPI_CONSTRUCTS.put("HMSVQYQN", 27);	//	Stress Tolerance
		GPI_CONSTRUCTS.put("KFJPVXOW", 28);	//	Self-confidence
		GPI_CONSTRUCTS.put("IXHCBYUD", 29);	//	Impressing
		GPI_CONSTRUCTS.put("HNDRFSMS", 30);	//	Self-awareness/Insight
		GPI_CONSTRUCTS.put("LJXUAGGW", 31);	//	Independence
		GPI_CONSTRUCTS.put("IXYPBOLO", 32);	//	Competitive
		GPI_CONSTRUCTS.put("DZGOUCXX", 33);	//	Risk-taking
		GPI_CONSTRUCTS.put("HPNBOSEJ", 34);	//	Desire for Advancement
		GPI_CONSTRUCTS.put("LJTZYBBX", 35);	//	Interdependence
		GPI_CONSTRUCTS.put("DYZASGTO", 36);	//	Dutifulness
		GPI_CONSTRUCTS.put("KBWTQJQX", 37);	//	Responsibility
		GPI_CONSTRUCTS.put("FAHLWLQA", 38);	//	Response Distortion Index Calc 13 - RDI percentile
	};
	
	public static final transient HashMap <String, Number> LEI_CONSTRUCTS = new HashMap <String, Number> ();
	{
		LEI_CONSTRUCTS.put("CQLWENZB", 1);	//	Overall
		LEI_CONSTRUCTS.put("CSTIMAIP", 2);	//	Super-Scale - General Management Experiences - GEN
		LEI_CONSTRUCTS.put("CNUCTNVY", 3);	//	Super-Scale - Adversity - ADVERSE
		LEI_CONSTRUCTS.put("IUCWGWPK", 4);	//	Super-Scale - Risky/Critical Experiences - RISKCRI
		LEI_CONSTRUCTS.put("CNENVVRN", 5);	//	Super-Scale - Personal and Career Related Experien
		LEI_CONSTRUCTS.put("LHTLVCLF", 6);	//	Scale 01 - Strategy Development - STRATEGY
		LEI_CONSTRUCTS.put("FEPIWKMQ", 7);	//	Scale 02 - Project Management and Implementation -
		LEI_CONSTRUCTS.put("LIUIKFXY", 8);	//	Scale 03 - Business Development and Marketing Scal
		LEI_CONSTRUCTS.put("LMCNFSQJ", 9);	//	Scale 04 - Business Growth Scale - BGROW
		LEI_CONSTRUCTS.put("FGKBNXMQ", 10);	//	Scale 05 - Product Development Scale - PRODUCT
		LEI_CONSTRUCTS.put("LFNCWGBM", 11);	//	Scale 06 - Start-up Business Scale - START
		LEI_CONSTRUCTS.put("FFHBQIQJ", 12);	//	Scale 07 - Financial Management Scale - FM
		LEI_CONSTRUCTS.put("LFVVMPPK", 13);	//	Scale 08 - Operations Scale - OPEXP
		LEI_CONSTRUCTS.put("LJKTELWM", 14);	//	Scale 09 - Support Functions Scale - FCNEXP
		LEI_CONSTRUCTS.put("LITBJJGV", 15);	//	Scale 10 - External Relations Scale - REPRE
		LEI_CONSTRUCTS.put("LITXLYHK", 16);	//	Scale 11 - Inherited Problems and Challenges Scale
		LEI_CONSTRUCTS.put("CTMQIDVW", 17);	//	Scale 12 - Interpersonally Challenging Situations
		LEI_CONSTRUCTS.put("IWXAWKKR", 18);	//	Scale 13 - Downturn and/or Failures Scale - FAILUR
		LEI_CONSTRUCTS.put("CPQNOZPF", 19);	//	Scale 14 - Difficult Financial Situations Scale -
		LEI_CONSTRUCTS.put("CTRJIKKM", 20);	//	Scale 15 - Difficult Staffing Situations Scale - S
		LEI_CONSTRUCTS.put("LHYXBYZM", 21);	//	Scale 16 - High Risk Situations Scale - HIRISK
		LEI_CONSTRUCTS.put("IXASMJTZ", 22);	//	Scale 17 - Critical Negotiations Scale - NEGOT
		LEI_CONSTRUCTS.put("ISUUPTXS", 23);	//	Scale 18 - Crisis Management Scale - CMGMT
		LEI_CONSTRUCTS.put("ITOGBFQK", 24);	//	Scale 19 - Highly Critical/Visible Assignments or
		LEI_CONSTRUCTS.put("CPILJBKZ", 25);	//	Scale 20 - Self-Development Scale - SELFDEV
		LEI_CONSTRUCTS.put("CRHOINUO", 26);	//	Scale 21 - Development of Others Scale - DEV
		LEI_CONSTRUCTS.put("IXXBWRFV", 27);	//	Scale 22 - International/Cross-Cultural Scale - IN
		LEI_CONSTRUCTS.put("IUKSTSWD", 28);	//	Scale 23 - Extracurricular Activities Scale - EXTR
	};

	public static final transient HashMap <String, Number> CAREER_SURVEY_CONSTRUCTS = new HashMap <String, Number> ();
	{
		CAREER_SURVEY_CONSTRUCTS.put("KAIODRDP", 1);	//	Career Goals constructs
		CAREER_SURVEY_CONSTRUCTS.put("CQKIKTHU", 1);	//	Career Drivers constructs
		CAREER_SURVEY_CONSTRUCTS.put("IYFKZJYE", 1);	//	Learning Orientation constructs
		CAREER_SURVEY_CONSTRUCTS.put("BMSHHDHS", 1);	//	Experience Orientation constructs
	};

	public static final transient HashMap <String, Number> CHECKBOX_CAREER_SURVEY_CONSTRUCTS = new HashMap <String, Number> ();
	{
		CHECKBOX_CAREER_SURVEY_CONSTRUCTS.put("HQETRZZN", 1);	//	Career Goals constructs
		CHECKBOX_CAREER_SURVEY_CONSTRUCTS.put("CTDHAPYM", 2);	//	Career Drivers constructs
		CHECKBOX_CAREER_SURVEY_CONSTRUCTS.put("CQXBXZWB", 3);	//	Learning Orientation constructs
		CHECKBOX_CAREER_SURVEY_CONSTRUCTS.put("IYHKFGAS", 4);	//	Experience Orientation constructs
	};
	
	public static final transient HashMap <String, Number> FIN_EX_CONSTRUCTS = new HashMap <String, Number> ();
	{
		FIN_EX_CONSTRUCTS.put("FBSXVRBA", 1);	//	Total answered correctly
		FIN_EX_CONSTRUCTS.put("DZUEXWBI", 2);	//	Make prudent decisions
		FIN_EX_CONSTRUCTS.put("BJJIGKGA", 3);	//	Draw appropriate conclusions
		FIN_EX_CONSTRUCTS.put("KEZSGNJY", 4);	//	Accurately forecast costs
		FIN_EX_CONSTRUCTS.put("KDYFCHSO", 5);	//	Deliver appropriate messages
	};
	
	public static final transient HashMap <String, Number> RAVENS_SHORT_CONSTRUCTS = new HashMap <String, Number> ();
	{
		RAVENS_SHORT_CONSTRUCTS.put("FGLGLBUM", 1);	//	Harcourt Raven APM Short Form	
	};


	public static final transient HashMap <String, Number> RAVENS_LONG_CONSTRUCTS = new HashMap <String, Number> ();
	{
		RAVENS_LONG_CONSTRUCTS.put("KAJBMATX", 1);	//	Raven's Timed Long form - Manual Entry module cons	
	};

	public static final transient HashMap <String, Number> RAVENS_2010_CONSTRUCTS = new HashMap <String, Number> ();
	{
		RAVENS_2010_CONSTRUCTS.put("FCNEVHUL", 1);	//	Raven's form B - Manual Entry module construct	
	};

	public static final transient HashMap <String, Number> WG_E_CONSTRUCTS = new HashMap <String, Number> ();
	{					
		WG_E_CONSTRUCTS.put("FBGFXLHU", 1);	//	Watson-Glaser E 

	};
	
	public static final transient HashMap <String, Number> WG_A_LONG_CONSTRUCTS = new HashMap <String, Number> ();
	{					
		WG_A_LONG_CONSTRUCTS.put("KBYWNSXD", 1);	//	Watson-Glaser A Long Score	
	};
	

	public static final transient HashMap <String, Number> WG_A_SHORT_CONSTRUCTS = new HashMap <String, Number> ();
	{		
		WG_A_SHORT_CONSTRUCTS.put("BNBZUXKQ", 1);	//	Watson-Glaser A Short Score	
	};

	public static final transient HashMap <String, Number> WG_C_CONSTRUCTS = new HashMap <String, Number> ();
	{	
		WG_C_CONSTRUCTS.put("LKIQQVWW", 1);	//	Watson Glaser C - Manual Entry module construct	
	};

	public static final transient HashMap <String, Number> WESMAN_CONSTRUCTS = new HashMap <String, Number> ();
	{	
		WESMAN_CONSTRUCTS.put("KAYHKNZU", 1);	//	Wesman module construct	
	};
	
	public static final transient HashMap <String, Number> COGS_COMPLETED_CONSTRUCT = new HashMap <String, Number> ();
	{	
		COGS_COMPLETED_CONSTRUCT.put("COGSDONE", 1);	//	the CogsCompleted module construct	
	};	
	
	
	// Vendor integration Mapping:
	// We need a way to map the tag names to the actual
	// construct Id's so that we can properly build the 
	// linkParams DTO object
	public static final transient HashMap <String, String>SHL_TAG_CONSTRUCT_MAP = new HashMap <String, String> ();
	{
		SHL_TAG_CONSTRUCT_MAP.put("T-SCORE", "KBPMPMRE");

	};

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	
	private String lpXML = "";
	// instantiate a return object
	private ManualEntryDTO lpDTO = new ManualEntryDTO();
	
	//
	// Constructors.
	//
	public ManualEntryDataHelper()
		throws Exception
	{
		////System.out.println("constructor... creating ManualEntryDataHelper...");
	}
	
	//
	// Instance methods.
	//

	/**
	 * Fetch the initial data needed for the Manual Entry app.
	 *
	 * @return the ManualEntryDTO object
	 * @throws Exception
	 */
	public  ManualEntryDTO getLinkParamsData(String lpId)
		throws Exception
	{        
      
		////System.out.println(" enterring getLinkParamsData:  linkParamId = " + lpId);
		
        // get the linkParam XML string 
		// from the V2 database
        getLinkParamsXML(lpId);

        // parse the linkParam XML and  
        // update the DTO.
        parseLinkParamsXML(lpId);  
        
        getAdaptV2UserData();
        
 		return this.lpDTO;
	}       


	/**
	 * Fetch the LinkParams row.
	 *
	 * @return an ArrayList of KeyValuePair objects
	 * @throws Exception
	 */
	private void getLinkParamsXML(String lpId)
		throws Exception
	{
		
      
        StringBuffer sqlQuery = new StringBuffer();
        sqlQuery.append("SELECT  XML ");
        sqlQuery.append("  FROM  linkparams ");
        sqlQuery.append("  WHERE uniqueIdStamp = ?  ");
        //System.out.println(" get linkparams sql : " + sqlQuery.toString());

    		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
    		if (dlps.isInError())
    		{
    			//TODO When this method returns something, this should be modified
    			System.out.println("Error preparing getLinkParamsXML query." + dlps.toString());
    			return;
    		}
    		
    		DataResult dr = null;
    		ResultSet rs = null;
 	    try
		{
			dlps.getPreparedStatement().setString(1, lpId);

			dr = V2DatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			
	       	if (! rs.isBeforeFirst())
	       	{
	       		throw new Exception("The link param Id is not valid.");
	       	}
	       	
	       	while (rs.next())
	       	{
	       		//System.out.println("rs.getString(XML)" + rs.getString("XML"));
	       		lpXML = (String) rs.getString("XML");
	       	}
		}
        catch (SQLException ex)
		{
        	// handle any errors
        	ex.printStackTrace();
        	throw new Exception("SQL fetching linkParams data.  " +
        			"SQLException: " + ex.getMessage() + ", " +
					"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
        finally
		{
			if (dr != null) { dr.close(); dr = null; }
        }
	}
	
	/*
	 * This method will parse the XML from the
	 * link params table to get the candidateId and
	 * the projectId, and fill in the ManualEntryDTO.
	 * 
	 * 
	 * <?xml version="1.0" encoding="UTF-8"?>
	 * <XML_Fields>
	 * 		<JobId>HMGGTIRD</JobId>
	 * 		<CandList>IYGRJBEM</CandList>
	 * </XML_Fields>
	 * 
	 * 
	 */
	private void parseLinkParamsXML( String lpId )
		throws Exception
	{	
		try
		{

			//The evaluate methods in the XPath and XPathExpression 
			//interfaces are used to parse an XML document with XPath expressions. 
			//The XPathFactory class is used to create an XPath object. 
			//Create an XPathFactory object with the  newInstance 
			//method of the XPathFactory class. 
			XPathFactory  xpFactory = XPathFactory.newInstance();

			//Create an XPath object from the XPathFactory object with the newXPath method. );
			XPath myXPath = (XPath) xpFactory.newXPath();
			
						
			//Create and compile an XPath expression with the compile method 
			//of the XPath object. As an example, select the title of the 
			//article with its date attribute set to January-2004. 
			//An attribute in an XPath expression is specified with an @ symbol. 
			//For further reference on XPath expressions, see the XPath specification 
			//for examples on creating an XPath expression. 
			XPathExpression  xPathExpression =  null;			
			InputSource is = null;

			
			 //Evaluate the XPath expression with the InputSource of 
			 //the example XML document to evaluate over. 

			 // Check to see if there's a Manual Entry type. 
			 // If there is, then the xml is a little different.....
			 is = new InputSource(new ByteArrayInputStream(lpXML.getBytes()));
			 xPathExpression =   myXPath.compile("/XML_Fields/METype");
			 String meType =  xPathExpression.evaluate(is);
			 if(meType != null){
				 ////System.out.println("meType =   " + meType); 
				 this.lpDTO.setManualEntryType(meType);
			 }	else {
				 this.lpDTO.setManualEntryType("");
			 }
			 String candidateId = "";
//			 String fName = "";
//			 String lName = "";
			 String jobId = "";
			 String moduleId = "";
			 
			 if(meType.equals(ManualEntryConstants.ME_A_BY_D_TYPE)){
				 
				 xPathExpression =   myXPath.compile("/XML_Fields/CandId");			
				 is = new InputSource(new ByteArrayInputStream(lpXML.getBytes()));

				 candidateId =  xPathExpression.evaluate(is);
				 //System.out.println("candidateId =  " + candidateId);	
				 
				 is = new InputSource(new ByteArrayInputStream(lpXML.getBytes()));
				 xPathExpression =   myXPath.compile("/XML_Fields/JobId");
				 jobId =  xPathExpression.evaluate(is);
				 //System.out.println("jobId2 =   " + jobId);	
				 
//				 is = new InputSource(new ByteArrayInputStream(lpXML.getBytes()));
//				 xPathExpression =   myXPath.compile("/XML_Fields/CandFname");
//				 fName =  xPathExpression.evaluate(is);
//				 //System.out.println("fName =   " + fName);	
//				 
//				 is = new InputSource(new ByteArrayInputStream(lpXML.getBytes()));
//				 xPathExpression =   myXPath.compile("/XML_Fields/CandLname");
//				 lName =  xPathExpression.evaluate(is);
//				 //System.out.println("lName =   " + lName);	
			 
						 
			 }else {
			 
				 is = new InputSource(new ByteArrayInputStream(lpXML.getBytes()));
				 xPathExpression =   myXPath.compile("/XML_Fields/CandList");
				 candidateId =  xPathExpression.evaluate(is);
				 //System.out.println("candidateId =  " + candidateId);	
				 
				 is = new InputSource(new ByteArrayInputStream(lpXML.getBytes()));
				 xPathExpression =   myXPath.compile("/XML_Fields/JobId");
				 jobId =  xPathExpression.evaluate(is);
				 //System.out.println("jobId2 =   " + jobId);	
				 
//				 is = new InputSource(new ByteArrayInputStream(lpXML.getBytes()));
//				 xPathExpression =   myXPath.compile("/XML_Fields/FirstName");
//				 fName =  xPathExpression.evaluate(is);
//				 //System.out.println("fName =   " + fName);	
//				 
//				 is = new InputSource(new ByteArrayInputStream(lpXML.getBytes()));
//				 xPathExpression =   myXPath.compile("/XML_Fields/LastName");
//				 lName =  xPathExpression.evaluate(is);
//				 //System.out.println("lName =   " + lName);	
				 
				 // for the vendor integration, check for the moduleId
				 // it won't be there for Manual Entry, but we'll need it
				 // for vendor integration
				 is = new InputSource(new ByteArrayInputStream(lpXML.getBytes()));
				 xPathExpression =   myXPath.compile("/XML_Fields/ModuleId");
				 moduleId =  xPathExpression.evaluate(is);
				 if(moduleId != null){
					 //System.out.println("moduleId =   " + moduleId); 
					 this.lpDTO.setModuleId(moduleId);
				 }		
			 
			 }
			 
			 this.lpDTO.setCandidateId(candidateId);
			 this.lpDTO.setProjectId(jobId);
			 this.lpDTO.setLinkParamId(lpId);

			Participant participant = HelperDelegate.getParticipantHelper().fromId(HelperDelegate.getSessionUserHelperHelper().createSessionUser(), candidateId);

						 
			 this.lpDTO.setFirstName(participant.getFirstName());
			 this.lpDTO.setLastName(participant.getLastName());
			 
//			 System.out.println("**********************************************************");
//			 System.out.println("*candidateId  )" + candidateId);
//			 System.out.println("*participant.getFirstName()  " + participant.getFirstName());
//			 System.out.println("*participant.getLastName()  " + participant.getLastName());
//			 System.out.println("**********************************************************");
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
	       	throw new Exception("Unable to parse the LinkParams XML:  " + ex.getMessage());
		}
	}
 
	/**
	 * Get ADAPT data stored in V2.
	 *
	 * @return an ArrayList of KeyValuePair objects
	 * @throws Exception
	 */
	private void getAdaptV2UserData()
		throws Exception
	{
        StringBuffer sqlQuery = new StringBuffer();
        sqlQuery.append("SELECT  AdaptUserId as uid, ");
        sqlQuery.append("        AdaptProjectId as eid, ");
        sqlQuery.append("        AdaptFirstName as fname, ");
        sqlQuery.append("        AdaptLastName as lname ");
        sqlQuery.append("  FROM  pdi_adapt_user_map ");
        sqlQuery.append("  WHERE v2CandidateId = ? ");
        sqlQuery.append("    AND v2JobId = ?");
        //System.out.println(" get getAdaptV2UserData sql : " + sqlQuery.toString());

   		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
   		if (dlps.isInError())
   		{
   			//TODO When this method returns something, this should be modified
   			System.out.println("Error preparing getLinkParamsXML query." + dlps.toString());
   			return;
   		}
    		
   		DataResult dr = null;
   		ResultSet rs = null;
   		try
		{
			dlps.getPreparedStatement().setString(1, this.lpDTO.getCandidateId());
			dlps.getPreparedStatement().setString(2, this.lpDTO.getProjectId());
			
			dr = V2DatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			
			/*  It's actually valid to NOT have any V2 data yet, 
				for instance, when the admin is initially going into
				Manual Entry to INPUT that data.					
			 */
	       	if (rs == null || !rs.isBeforeFirst())
	       	{
	       		//this.lpDTO.setAdaptUserId(0);
	       		//this.lpDTO.setAdaptProjectId(0);
	       		return;
	       		
	       	} else {
		       	while (rs.next())
		       	{
		       		this.lpDTO.setAdaptUserId(rs.getInt("uid"));
		       		this.lpDTO.setAdaptProjectId(rs.getInt("eid"));
		       		this.lpDTO.setAdaptFirstName(rs.getString("fname"));
		       		this.lpDTO.setAdaptLastName(rs.getString("lname"));
		       	}
	       	}
		}
        catch (SQLException ex)
		{
        	ex.printStackTrace();
        	// handle any errors
        	throw new Exception("SQL fetching linkParams data.  " +
        			"SQLException: " + ex.getMessage() + ", " +
					"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
        finally
		{
			if (dr != null) { dr.close(); dr = null; }
        }
	}
	
	/**
	 * Delete the LinkParams row, as it's no longer needed
	 *
	 * @param link params id
	 * @throws Exception
	 */

	public void deleteLinkParamsRow(String lpId)
		throws Exception
	{
		DataResult dr = null;
        StringBuffer sqlQuery = new StringBuffer();
        sqlQuery.append("DELETE ");
        sqlQuery.append("  FROM  linkparams ");
        sqlQuery.append("  WHERE uniqueIdStamp = ?  ");
 
		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			//TODO When this method returns something, this should be modified
			System.out.println("Error preparing deleteLinkParamsRow query." + dlps.toString());
			return;
		}
		
        try
		{
        	// Fill sql parmeters with values and get the data
			dlps.getPreparedStatement().setString(1, lpId);
			
			dr = V2DatabaseUtils.delete(dlps);
		}
        catch (SQLException ex)
		{
        	ex.printStackTrace();
        	// handle any errors
        	throw new Exception("Unable to delete linkParams row for LinkParamId =  " + lpId + "  " +
        			"SQLException: " + ex.getMessage() + ", " +
					"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
        finally
        {
			if (dr != null) { dr.close(); dr = null; }
        }
	}

	
	/**
	 * Fetch the modules for the given project id.
	 *
	 * @return an ArrayList of KeyValuePair objects
	 * @throws Exception
	 */
	public ArrayList<KeyValuePairStrings> getModulesForProjectId(String jobId)
		throws Exception
	{
		
		ArrayList<KeyValuePairStrings> ary = new ArrayList<KeyValuePairStrings>();
		    
        StringBuffer sqlQuery = new StringBuffer();
        sqlQuery.append("SELECT  jm.moduleId, m.name  ");
        sqlQuery.append("  FROM  job_mod as jm, modules as m  ");
        sqlQuery.append("  WHERE jobId = ?  ");
        sqlQuery.append("  AND  jm.moduleId = m.uniqueIdStamp  ");
        sqlQuery.append("  ORDER BY m.name  ");
        //System.out.println("getModulesForProjectId SQL " +sqlQuery.toString());
        
		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			//TODO When this method returns something, this should be modified
			System.out.println("Error preparing getModulesForProjectId query." + dlps.toString());
			return ary;
		}

		DataResult dr = null;
		ResultSet rs = null;
        try
		{
			// Fill sql parmeters with values and get the data
			dlps.getPreparedStatement().setString(1, jobId);
           	
			dr = V2DatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			
	       	if (! rs.isBeforeFirst())
	       	{
	       		throw new Exception("SQL error obtaining module Ids.  ");
	       	}

	       	while (rs.next())
	       	{
	       		
	       		String modId = (String) rs.getString("moduleId");
	       		String modName = (String) rs.getString("name");	  
	       		//System.out.println("modId, modName  " + modId + "  " + modName);
	       		KeyValuePairStrings kvps = new KeyValuePairStrings();
	       		if(
	       				(lpDTO.getManualEntryType() != null && lpDTO.getManualEntryType().equals(ManualEntryConstants.ME_A_BY_D_TYPE)) 
	       				|| EntryAndScoringConstants.VENDOR_TESTING_MODULES.contains(modId)
	       				){
	       			kvps.setTheKey(modId);
	       			kvps.setTheValue(modName);
	       			ary.add(kvps);
	       		}
/*
 * GAVIN COMMENTED OUT
	       		if(lpDTO.getManualEntryType() == null && EntryAndScoringConstants.VENDOR_TESTING_MODULES.contains(modId)){
	       			kvps.setTheKey(modId);
	       			kvps.setTheValue(modName);
	       			ary.add(kvps);
	       			
	       		} else if (lpDTO.getManualEntryType() != null && lpDTO.getManualEntryType().equals(ManualEntryConstants.ME_A_BY_D_TYPE)) {
	       			kvps.setTheKey(modId);
	       			kvps.setTheValue(modName);
	       			ary.add(kvps);
	       		}
*/	       		
	       	}
	       	
	       return ary;	
	       
		}
        catch (SQLException ex)
		{
        	ex.printStackTrace();
        	// handle any errors
        	throw new Exception("SQL fetching linkParams data.  " +
        			"SQLException: " + ex.getMessage() + ", " +
					"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
        finally
		{
			if (dr != null) { dr.close(); dr = null; }
        }
	}
	
	/**
	 * Fetch the modules for the given project id.
	 *
	 * @return an ArrayList of KeyValuePair objects
	 * @throws Exception
	 */
	public ArrayList<ModConstsDTO> 
			getConstructsforModules(ArrayList<KeyValuePairStrings> modList)
						throws Exception
	{
		//System.out.println("modlist.length..." + modList.size());
		// get module list string for SQL
		String moduleString = "";
		int index = 0;
		Iterator<KeyValuePairStrings> iter = modList.iterator();
		
		while(iter.hasNext()){
			KeyValuePairStrings kvps = new KeyValuePairStrings();
			kvps = iter.next();
			
			if(index == 0){
				moduleString += "('" + kvps.getTheKey() + "'";
			}else {
				moduleString += ",'" + kvps.getTheKey() + "'";
			}
			index++;
		}
		if(moduleString != ""){
			moduleString += ")";
		}
		//System.out.println("moduleString = " + moduleString);
		
		// array list to hold moduleId + list of constructId's for that module
		ArrayList<ModConstsDTO> ary = new ArrayList<ModConstsDTO>();
		

        StringBuffer sqlQuery = new StringBuffer();
        sqlQuery.append("SELECT  c.moduleId, m.name as modName, c.uniqueIdStamp, c.name as constName ");
        sqlQuery.append("  FROM  constructs as c, modules as m  ");
        sqlQuery.append("  WHERE moduleId in " + moduleString + "  ");
        sqlQuery.append("  AND m.UniqueIdStamp = c.moduleId  ");
        sqlQuery.append("  ORDER BY moduleId  ");
        //System.out.println("getConstructsforModules SQL = " + sqlQuery.toString());
        
		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			//TODO When this method returns something, this should be modified
			System.out.println("Error preparing getConstructsforModules query." + dlps.toString());
			return ary;
		}

		DataResult dr = null;
		ResultSet rs = null;
		
        try
		{
        	// Fill sql parmeters with values and get the data
			//dlps.getPreparedStatement().setString(1, moduleString);
			
			dr = V2DatabaseUtils.select(dlps);
			rs = dr.getResultSet();
           	
	       	if (! rs.isBeforeFirst())
	       	{
	       		System.out.println("722    SQL error obtaining module Ids.  ");
	       		throw new Exception("SQL error obtaining module Ids.  ");
	       		
	       	}
	       	String modId = "";
	       	ModConstsDTO modConstDTO = null;
	       	ConstructDTO[] tempConstructArray = null;
	       	int i = 0;
	       	while (rs.next())
	       	{
	       		String newModId = rs.getString("moduleId");
//System.out.println("745.. newModId = " + newModId);	       		
	       		// deal with the model info
	       		if( (i == 0) || (! newModId.equals(modId)) ){
	       			// first time through or new model id
	       			
					// add the complete dto to the array
					if( i != 0){
		       			// not the first time through, so there's 
		       			// data in the tempConstructArray
			       		// set the dto constructs array so that the 
			       		// constructs are in the right order
		       			for(int j=0; j < tempConstructArray.length;j++)
		       			{		       				
		       				modConstDTO.getConstructsAry().add(j, tempConstructArray[j]);
		       			}
		       			
		       			// add the dto to the array
						ary.add(modConstDTO);
					}
	       			modConstDTO = new ModConstsDTO();
	       			modConstDTO.setModuleId(newModId);
	       			modConstDTO.setModuleName(rs.getString("modName"));
	       			modConstDTO.setHasScores(false);  // false is default value - we really don't expect there to be answers very often
		       		//System.out.println("modConstDTO.setModuleName " + modConstDTO.getModuleName());	
		       		
					// also get the normList array, and add that
	       			// to the modConstDto!
	       			
	       			if(this.lpDTO.getManualEntryType().equals(ManualEntryConstants.ME_A_BY_D_TYPE) ){
	       				//System.out.println(" ******  762......");
	       				modConstDTO.setNormList(fetchNormsForModuleSpecPopOnly(newModId));
	       				modConstDTO.setNormListGenPop(fetchNormsForModuleGenPopOnly(newModId));
	       			} else {
	       				modConstDTO.setNormList(fetchNormsForModule(newModId));
	       				//System.out.println(" ******  766......");
	       			}

	       			// set the modConstDTO constArray to the right length	       			
		       		if(newModId.equals("DTYAQNYJ") ){
		       			// EAS
		       			modConstDTO.setConstructsAry(new ArrayList<ConstructDTO>(EAS_CONSTRUCTS.size()));
		       		} else if (newModId.equals("HSJRIKLO")){
		       			// CPI
		       			modConstDTO.setConstructsAry(new ArrayList<ConstructDTO>(CPI_CONSTRUCTS.size()));
		       		} else if (modId.equals("IZFTQXVI")) {
		       			// Verify -- there is only one construct
		       			modConstDTO.setConstructsAry(new ArrayList<ConstructDTO>(VERIFY_CONSTRUCTS.size()));		       		
		       		} else if (modId.equals("IXLGVIOQ")) {
		       			// Global Personality Inventory (GPI)
		       			modConstDTO.setConstructsAry(new ArrayList<ConstructDTO>(GPI_CONSTRUCTS.size()));
		       		} else if (modId.equals("BMZLGGZL")) {
		       			// Leadership Experience Inventory (LEI) 
		       			modConstDTO.setConstructsAry(new ArrayList<ConstructDTO>(LEI_CONSTRUCTS.size()));
		       		} else if (modId.equals("IYJOHVLX")) {
		       			// Raven's APM Short Form -- there is only one construct
		       			modConstDTO.setConstructsAry(new ArrayList<ConstructDTO>(RAVENS_SHORT_CONSTRUCTS.size()));
		       		} else if (modId.equals("ITAXVGGU")) {
		       			// Raven's Timed Long Form - Manual Entry -- there is only one construct
		       			modConstDTO.setConstructsAry(new ArrayList<ConstructDTO>(RAVENS_LONG_CONSTRUCTS.size()));
		       		} else if (modId.equals("FFZFFOKV")) {
		       			// Watson-Glaser A	 -- there is only one construct
		       			modConstDTO.setConstructsAry(new ArrayList<ConstructDTO>(WG_A_LONG_CONSTRUCTS.size()));
		       		} else if (modId.equals("ISTVNYDL")) {
		       			// Watson-Glaser A - SHORT FORM -- there is only one construct
		       			modConstDTO.setConstructsAry(new ArrayList<ConstructDTO>(WG_A_SHORT_CONSTRUCTS.size()));
		       		} else if (modId.equals("DWGODHMX")) {
		       			// Watson-Glaser C - Manual Entry -- there is only one construct
		       			modConstDTO.setConstructsAry(new ArrayList<ConstructDTO>(WG_C_CONSTRUCTS.size()));
		       		} else if (modId.equals("FFJUUSBA")) {
		       			// Watson-Glaser E - Manual Entry -- there is only one construct
		       			modConstDTO.setConstructsAry(new ArrayList<ConstructDTO>(WG_E_CONSTRUCTS.size()));
		       		} else if (modId.equals("LGHSTSHU")) {
		       			// Wesman - -- there is only one construct
		       			//System.out.println("793  modId.equals  LGHSTSHU");
		       			modConstDTO.setConstructsAry(new ArrayList<ConstructDTO>(WESMAN_CONSTRUCTS.size()));
		       		} else if (modId.equals("KFVDJXHR")) {
		       			// CogsDone Module -- there is only one construct
		       			modConstDTO.setConstructsAry(new ArrayList<ConstructDTO>(COGS_COMPLETED_CONSTRUCT.size()));
		       		} else if (modId.equals("HNZYMHDV")){
		       			// Raven's APM form B
		       			modConstDTO.setConstructsAry(new ArrayList<ConstructDTO>(RAVENS_2010_CONSTRUCTS.size()));
		       		} else if (modId.equals("FFGLQDEG")){
		       			// Vendor Link - Checkbox - AbyD - career survey
		       			modConstDTO.setConstructsAry(new ArrayList<ConstructDTO>(CHECKBOX_CAREER_SURVEY_CONSTRUCTS.size()));
		       		} else if (modId.equals("FAGNDFAC")){
		       			// Financial Exercise - AbyD
		       			modConstDTO.setConstructsAry(new ArrayList<ConstructDTO>(FIN_EX_CONSTRUCTS.size()));
		       		} else {
		       			modConstDTO.setConstructsAry(new ArrayList<ConstructDTO>());
		       		}
	       					       		
	       			// the old modId becomes the new/next modId
	       			modId = newModId; 
	       			
	       			// get a new tempConstructArray
		       		if(modId.equals("DTYAQNYJ") ){
		       			// EAS
		       			tempConstructArray = new ConstructDTO[EAS_CONSTRUCTS.size()];
		       		} else if (modId.equals("HSJRIKLO")){
		       			// CPI
		       			tempConstructArray = new ConstructDTO[CPI_CONSTRUCTS.size()];
		       		} else if (modId.equals("IZFTQXVI")) {
		       			// Verify -- there is only one construct
		       			tempConstructArray = new ConstructDTO[VERIFY_CONSTRUCTS.size()];
		       		} else if (modId.equals("IXLGVIOQ")) {
		       			// Global Personality Inventory (GPI) 
		       			tempConstructArray = new ConstructDTO[GPI_CONSTRUCTS.size()];
		       		} else if (modId.equals("BMZLGGZL")) {
		       			// Leadership Experience Inventory (LEI)
		       			tempConstructArray = new ConstructDTO[LEI_CONSTRUCTS.size()];
		       		} else if (modId.equals("IYJOHVLX")) {
		       			// Raven's APM Short Form -- there is only one construct
		       			tempConstructArray = new ConstructDTO[RAVENS_SHORT_CONSTRUCTS.size()];
		       		} else if (modId.equals("ITAXVGGU")) {
		       			// Raven's Timed Long Form - Manual Entry -- there is only one construct
		       			tempConstructArray = new ConstructDTO[RAVENS_LONG_CONSTRUCTS.size()];
		       		} else if (modId.equals("FFZFFOKV")) {
		       			// Watson-Glaser A	 -- there is only one construct
		       			tempConstructArray = new ConstructDTO[WG_A_LONG_CONSTRUCTS.size()];
		       		} else if (modId.equals("ISTVNYDL")) {
		       			// Watson-Glaser A - SHORT FORM -- there is only one construct
		       			tempConstructArray = new ConstructDTO[WG_A_SHORT_CONSTRUCTS.size()];
		       		} else if (modId.equals("DWGODHMX")) {
		       			// Watson-Glaser C - Manual Entry -- there is only one construct
		       			tempConstructArray = new ConstructDTO[WG_C_CONSTRUCTS.size()];
		       		} else if (modId.equals("FFJUUSBA")) {
		       			// Watson-Glaser E - Manual Entry -- there is only one construct
		       			tempConstructArray = new ConstructDTO[WG_E_CONSTRUCTS.size()];
		       		} else if (modId.equals("LGHSTSHU")) {
		       			// WESMAN -- there is only one construct
		       			//System.out.println("843  modId.equals  LGHSTSHU");
		       			tempConstructArray = new ConstructDTO[WESMAN_CONSTRUCTS.size()];
		       		} else if (modId.equals("KFVDJXHR")) {
		       			// Cogs completed module -- there is only one construct
		       			tempConstructArray = new ConstructDTO[COGS_COMPLETED_CONSTRUCT.size()];
		       		} else if (modId.equals("HNZYMHDV")){
		       			// Raven's APM form B
		       			tempConstructArray = new ConstructDTO[RAVENS_2010_CONSTRUCTS.size()];
		       		} else if (modId.equals("FFGLQDEG")){
		       			// Vendor Link - Check box - AbyD - career survey
		       			tempConstructArray = new ConstructDTO[CHECKBOX_CAREER_SURVEY_CONSTRUCTS.size()];
		       		} else if (modId.equals("FAGNDFAC")){
		       			// Financial Exercise - AbyD
		       			tempConstructArray = new ConstructDTO[FIN_EX_CONSTRUCTS.size()];
		       		} else {
		       			tempConstructArray = new ConstructDTO[0];
		       		}
	       			//System.out.println("done getting the empty construct array.... ");
	       		}
	       		
	       		// now deal with the constants info....
	       		// build a new ConstructDTO each time around
	       		// add it to the array of constructDTO's in the ModConstsDto
	       		ConstructDTO constDTO = new ConstructDTO();
	       		constDTO.setConstructId(rs.getString("uniqueIdStamp"));
	       		constDTO.setConstructName(rs.getString("constName"));
	       		constDTO.setShowCheckMark(0); // 0 is the default
	       		//System.out.println("build construct DTO for:  " + constDTO.getConstructId());
	       		if(modId.equals("DTYAQNYJ") ){
	       			// EAS
	       			tempConstructArray[(EAS_CONSTRUCTS.get(constDTO.getConstructId())).intValue() - 1] = constDTO;
	       		} else if (modId.equals("HSJRIKLO")){
	       			// CPI
	       			tempConstructArray[(CPI_CONSTRUCTS.get(constDTO.getConstructId())).intValue() - 1] = constDTO;
	       		} else if (modId.equals("IZFTQXVI")) {
	       			// Verify -- there is only one construct
	       			tempConstructArray[(VERIFY_CONSTRUCTS.get(constDTO.getConstructId())).intValue() - 1] = constDTO;
	       		} else if (modId.equals("IXLGVIOQ")) {
	       			// Global Personality Inventory (GPI) 
	       			if(constDTO.getConstructId().equals("ISWBZZCA") || 
	       					constDTO.getConstructId().equals("FDOIGXXV") || 
	       					constDTO.getConstructId().equals("DZONHYPB") || 
	       					constDTO.getConstructId().equals("DUBHDXTF") ||
	       					constDTO.getConstructId().equals("FBYHENOP") ||
	       					constDTO.getConstructId().equals("LLHTBMDQ") ||
	       					constDTO.getConstructId().equals("FDVUOQBB") ||
	       					constDTO.getConstructId().equals("LGLSZQXN") ||
	       					constDTO.getConstructId().equals("FFEXSMZS") ||
	       					constDTO.getConstructId().equals("BMOIKYJF") ||
	       					constDTO.getConstructId().equals("DZLDAPCG") ||
	       					constDTO.getConstructId().equals("DVFMVVYU")){ 
	       				// do nothing
	       			} else {
	       				tempConstructArray[(GPI_CONSTRUCTS.get(constDTO.getConstructId())).intValue() - 1] = constDTO;	       				
	       			}
	       		} else if (modId.equals("BMZLGGZL")) {
	       			// Leadership Experience Inventory (LEI)	       				
	       			// don't add work orientation constructs to the list.... 
	       			if(constDTO.getConstructId().equals("FBEWMRJL") || 
	       					constDTO.getConstructId().equals("CRDXOADK") || 
	       					constDTO.getConstructId().equals("CQWPKSBF") || 
	       					constDTO.getConstructId().equals("CSIELREN") ||
	       					constDTO.getConstructId().equals("CNAMJDUS")){ 
	       				// do nothing
	       			} else {
	       				tempConstructArray[(LEI_CONSTRUCTS.get(constDTO.getConstructId())).intValue() - 1] = constDTO;
	       			}
	       		} else if (modId.equals("IYJOHVLX")) {
	       			// Raven's APM Short Form -- there is only one construct
	       			if(constDTO.getConstructId().equals("FDXKKUON") || 
	       					constDTO.getConstructId().equals("HQAGQXCW") ){ 
	       				// do nothing
	       			} else {
	       				tempConstructArray[(RAVENS_SHORT_CONSTRUCTS.get(constDTO.getConstructId())).intValue() - 1] = constDTO;
	       			}
	       		} else if (modId.equals("ITAXVGGU")) {
	       			// Raven's Timed Long Form - Manual Entry -- there is only one construct
	       			tempConstructArray[(RAVENS_LONG_CONSTRUCTS.get(constDTO.getConstructId())).intValue() - 1] = constDTO;
	       		} else if (modId.equals("FFZFFOKV")) {
	       			// Watson-Glaser A	 -- there is only one construct
	       			if(constDTO.getConstructId().equals("BLVPPMWO") || 
	       					constDTO.getConstructId().equals("HORUSCUE") ){ 
	       				// do nothing
	       			} else {
	       				tempConstructArray[(WG_A_LONG_CONSTRUCTS.get(constDTO.getConstructId())).intValue() - 1] = constDTO;
	       			}
	       		} else if (modId.equals("ISTVNYDL")) {
	       			// Watson-Glaser A - SHORT FORM -- there is only one construct
	       			if(constDTO.getConstructId().equals("BHCCADRC") || 
	       					constDTO.getConstructId().equals("HOGDVPTY") ){ 
	       				// do nothing
	       			} else {
	       				tempConstructArray[(WG_A_SHORT_CONSTRUCTS.get(constDTO.getConstructId())).intValue() - 1] = constDTO;	       				
	       			}
	       		} else if (modId.equals("FFJUUSBA")) {
	       			// Watson-Glaser E
	       			if(constDTO.getConstructId().equals("EZXWGDLB") || 
	       					constDTO.getConstructId().equals("IUJCEXXY") || 
	       					constDTO.getConstructId().equals("HNRAUOFE")){ 
	       				// do nothing
	       			} else {
	       				tempConstructArray[(WG_E_CONSTRUCTS.get(constDTO.getConstructId())).intValue() - 1] = constDTO;	       				
	       			}
	       		} else if (modId.equals("DWGODHMX")) {
	       			// Watson-Glaser C - Manual Entry -- there is only one construct
	       			tempConstructArray[(WG_C_CONSTRUCTS.get(constDTO.getConstructId())).intValue() - 1] = constDTO;
	       		} else if (modId.equals("LGHSTSHU")) {
	       			// Wesman -- there is only one construct
	       			if(constDTO.getConstructId().equals("BKYGIQIH") || 
	       					constDTO.getConstructId().equals("DXITYQZI") ){ 
	       				// do nothing
	       				//System.out.println("constDTO.getConstructId() = " + constDTO.getConstructId());
	       			} else {
	       				tempConstructArray[(WESMAN_CONSTRUCTS.get(constDTO.getConstructId())).intValue() - 1] = constDTO;
	       				///System.out.println("tempconstructArray.length " + tempConstructArray.length);
	       			}
	       		} else if (modId.equals("KFVDJXHR")) {
	       			// cogs completed module -- there is only one construct
	       			tempConstructArray[(COGS_COMPLETED_CONSTRUCT.get(constDTO.getConstructId())).intValue() - 1] = constDTO;
	       		} else if (modId.equals("HNZYMHDV")){
	       			// Raven's APM form B  - there is only one construct
	       			if(constDTO.getConstructId().equals("FFSALJFE") || 
	       				constDTO.getConstructId().equals("FDVTZYRA") ){ 
	       				// do nothing
	       			} else {
	       				tempConstructArray[(RAVENS_2010_CONSTRUCTS.get(constDTO.getConstructId())).intValue() - 1] = constDTO;
	       			}
	       		} else if (modId.equals("FFGLQDEG")){
	       			// Vendor Link - Check box - AbyD - career survey
	       			tempConstructArray[(CHECKBOX_CAREER_SURVEY_CONSTRUCTS.get(constDTO.getConstructId())).intValue() - 1] = constDTO;
	       		} else if (modId.equals("FAGNDFAC")) {
	       			//Financial Exercise - AbyD
	       			tempConstructArray[(FIN_EX_CONSTRUCTS.get(constDTO.getConstructId())).intValue() - 1] = constDTO;
	       		}
	       		i++;
	       	}
	       	//System.out.println("******  969  ");
       		// set the dto constructs array so that the 
       		// constructs are in the right order
	       	// the last time through, we won't get a chance to 
	       	// do this, so I have to do it here for the last
	       	// modConstDTO

			for(int j=0; j < tempConstructArray.length;j++)
			{
				modConstDTO.getConstructsAry().add(j, tempConstructArray[j]);
			}
			
			if(modId != "" || i == 1){
				// there is only 1 module, so you 
				// have to add it to the ary.
				// or, this is the last one that
				// was created, so add it before
				// you send the ary
				ary.add(modConstDTO);
			}
			//System.out.println("******  989  ");
	       return ary;	
	       
		}
        catch (SQLException ex)
		{
        	ex.printStackTrace();
        	// handle any errors
        	throw new Exception("SQL fetching linkParams data.  " +
        			"SQLException: " + ex.getMessage() + ", " +
					"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
        finally
		{
			if (dr != null) { dr.close(); dr = null; }
        }
	}	

	
	/**
	 * Fetch the results for any constructs  that have
	 * results for a given candidate, construct, project
	 *
	 * @param ArrayList<ModConstsDTO> modConstList - list of ModConstsDTO's
	 * @param String candId - the v2 candidate ID
	 * @param String projId - the v2 project ID
	 * @return an ArrayList of KeyValuePair objects
	 * @throws Exception
	 */
	public ArrayList<ModConstsDTO> getResultsForConstructs(ArrayList<ModConstsDTO> modConstList,
			                                               String candId,
			                                               String projId)
		throws Exception
	{
		// get module list string for SQL
		String moduleString = "";
		int index = 0;
		Iterator<ModConstsDTO> it = modConstList.iterator();
		while(it.hasNext())
		{
			ModConstsDTO mcDTO = new ModConstsDTO();
			mcDTO = (ModConstsDTO) it.next();
			if(index == 0){
				moduleString += "('" + mcDTO.getModuleId() + "'";
			} else {
				moduleString += ",'" + mcDTO.getModuleId() + "'";
			}
			index++;
		}
		if(moduleString != "")
		{
			moduleString += ")";
		}
		//System.out.println("ArrayList<ModConstsDTO> modConstList  = " + moduleString);

        
        StringBuffer sqlQuery = new StringBuffer();
        sqlQuery.append("SELECT  distinct c.moduleId, c.uniqueIdStamp as constructId, c.name, r.CScore, r.XML ");
        sqlQuery.append("  FROM  constructs as c, modules as m, results as r  ");
        sqlQuery.append("  WHERE  m.UniqueIdStamp = c.moduleId  ");
        sqlQuery.append("  AND c.uniqueIdStamp =  r.constructId  ");
        sqlQuery.append("  AND r.candidateId = ?    ");
        sqlQuery.append("  AND r.jobId = ?    ");
        sqlQuery.append("  AND m.UniqueIdStamp in   " + moduleString + "  ");
        sqlQuery.append("  ORDER BY c.moduleId, constructId   ");
        //System.out.println("getResultsForConstructs SQL = " + sqlQuery.toString());
        
		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			//TODO When this method returns something, this should be modified
			System.out.println("Error preparing getRawScores query." + dlps.toString());
			return modConstList;
		}

		DataResult dr = null;
		ResultSet rs = null;
        try
		{
			// Fill sql parmeters with values and get the data
			dlps.getPreparedStatement().setString(1, candId);
			dlps.getPreparedStatement().setString(2, projId);
			//System.out.println("candId = " + candId + "   projId = " + projId);
			dr = V2DatabaseUtils.select(dlps);
			rs = dr.getResultSet();

	       	if (rs == null || ! rs.isBeforeFirst())
	       	{
	       		// there are no results for this candidate
	       		// so just return the modConstList that we
	       		// got, unchanged.
	       		//System.out.println("result set is null or empty.... ");
	       		return  modConstList;
	       	}

	       	String currentModId = "";
	       	HashMap<String, HashMap<String, String>> modMap = new HashMap<String, HashMap<String, String>>();;
	       	HashMap<String, String> constMap = new HashMap<String, String>();
	       	HashMap<String, HashMap<String, String>> normsMap = new HashMap<String, HashMap<String, String>>();
	       	String normId = ""; // for spec pop norm
	       	String genPopNormId = "";
	       	boolean hasNorms = true;
	       	int i = 0;
	       	while (rs.next())
	       	{
	       		//reset the boolean
	       		hasNorms = true;
	       		//reset the norms
		       	normId = ""; // for spec pop norm
		       	genPopNormId = "";	       		
	       		
	       		String newModId = rs.getString("moduleId");
	       		String constId = rs.getString("constructId");
	       		String score = rs.getString("CScore");  // this is the un-normed score

	       		// we're only going to look at norms if this is an a by d project
	       		if(this.lpDTO.getManualEntryType().equals(ManualEntryConstants.ME_A_BY_D_TYPE))
	       		{
	       			//System.out.println(" type = manualEntry ");
	       			Iterator<String> iter = ManualEntryConstants.NON_NORMED_CONSTRUCTS.keySet().iterator();
	       			//System.out.println("getting norms for construct ID: " + constId);
	       			//System.out.println("un-normed score = " + score);
	       			
	       			while(iter.hasNext())
	       			{
	       				String id = iter.next();
	       				if(id.equals(constId))
	       				{
	       					//System.out.println("CONTAINED IN: ManualEntryConstants.NON_NORMED_CONSTRUCTS : " + constId);
	       					hasNorms = false;
	       					break;
	       				}	       					
	       			}
	       			
	       			if(hasNorms)
	       			{       				
		       			String xml = rs.getString("XML");
			       		// then you gotta parse the xml to get the normdata
			       		// first, get the Norm ID
		       			if(lpDTO.getManualEntryType().equals(ManualEntryConstants.ME_A_BY_D_TYPE))
		       			{
		       				normId = parseResultsXML(xml, newModId);
		       				genPopNormId = parseResultsXMLForGenPopNorm(xml, newModId);
		       			} else {
		       				normId = parseResultsXML(xml);
		       			}
		       			
		       			// add the normId to the right ModConstsDTO
		       			if(normId != null || genPopNormId != null)
		       			{
		       				int l = 0;
		       				ModConstsDTO dto = new ModConstsDTO();
		       				Iterator<ModConstsDTO> it2 = modConstList.iterator();
		       				while(it2.hasNext())
		       				{
		       					dto = it2.next();							
		       					if(dto.getModuleId().equals(newModId))
		       					{
		       						dto.setNormId(normId);
		       						dto.setGenPopNormId(genPopNormId);
		       					}
		       					l++;
		       					dto = null;
		       				}						
		       			}
					
		       			if(normId == null && genPopNormId == null)
		       			{
		       				hasNorms = false;
		       			} else if(normId != null)
		       			{
		       				// we go out and get the norm xml based on the normId
		       				String normsXML = NormUtils.fetchNormsXML(normId);
		       				//then we parse the norms xml...
		       				// ...and get the mean/stddev from the norm for this construct
		       				// and put that data into the normsMap:  
		       				// key = constructId  value = key/value pair = mean/stddev
		       				hasNorms = NormUtils.parseNormsXML(normsXML, normsMap, constId);
			       		
		       				// getting the cscore -- for ManualEntry, we're displaying
		       				// the c-score, and not the normed scores, though we ARE
		       				// collecting the norm info as above.... 
		       				score = parseResultsXMLForNormedScore(xml, normId);					       		
		       			} else if(genPopNormId != null)
		       			{	
				       		// we go out and get the norm xml based on the normId
				       		String normsXML = NormUtils.fetchNormsXML(genPopNormId);
				       		//then we parse the norms xml...
				       		// ...and get the mean/stddev from the norm for this construct
				       		// and put that data into the normsMap:  
				       		// key = constructId  value = key/value pair = mean/stddev
				       		hasNorms = NormUtils.parseNormsXML(normsXML, normsMap, constId);
			       		
				       		// getting the cscore -- for ManualEntry, we're displaying
				       		// the c-score, and not the normed scores, though we ARE
				       		// collecting the norm info as above.... 
				       		score = parseResultsXMLForNormedScore(xml, genPopNormId);					       		
		       			}
	       			}	// End if(hasNorms)
	       		}	// End if ME_A_BY_D_TYPE
	       		
	       		// add construct data to the construct map
	       		// we'll add this map to the modMap when we get a new module Id	       		
	   			constMap.put(constId, score);
	   			//System.out.println("constId, score :   " + constId + ", " + score);
	       		
	       		// first time through or new model id
	       		if( (i == 0) || (! newModId.equals(currentModId)) )
	       		{
	       			// add the existing data to the modMap hash
	       			modMap.put(newModId, constMap);
	       			// set the current modId
	       			currentModId = rs.getString("moduleId");
	       			// create a new constMap to hold construct Data for the new module
	       			//constMap = new HashMap<String, String>();
	       		}
	       		
	       		// add construct data to the construct map
	       		// we'll add this map to the modMap when we get a new module Id	       		
	   			//constMap.put(constId, score);
	   			//System.out.println("constId, score :   " + constId + ", " + score);
	
	       		i++;
	       	}	// End while(rs.next())
	       	//System.out.println("normsMap.length " + normsMap.size());
	       	
		    // okay... we have all the construct scores now....
		    // we need to update the ArrayList<ModConstsDTO> modConstList
		    // so we can send it back to update the ManualEntryDTO....
		    // so... spin through the modConstList and plug in our data....
	       	Iterator<ModConstsDTO> it3 = modConstList.iterator();
			while(it3.hasNext())
			{			
				ModConstsDTO mcDTO = new ModConstsDTO();
				mcDTO = (ModConstsDTO) it3.next();

				// get the current moduleId
				String modKey = mcDTO.getModuleId();			

				if (modKey.equals(ManualEntryConstants.CHQ_MOD))
				{
					// The CHQ isn't scored and no data is placed into
					// the results table.  Set the hasScores flag to
					// indicate that the data has been imported.
					//System.out.println("Gotta do something about the CHQ!");
					mcDTO.setHasScores(isChqImported(candId, projId)); 
				}
				
				// see if it exists in the results data
				if(modMap.containsKey(modKey))
				{
					// we have a module, so we can get the construct data
					//System.out.println("getModuleName name " + mcDTO.getModuleName());
					// get the constants array<ConstructDTO> from the dto, which we'll spin through
					// to add the scores to the dto
					//System.out.println("2 mcDTO.getConstructsAry().size  " + mcDTO.getConstructsAry().size());
					
					ArrayList<ConstructDTO> constAr = mcDTO.getConstructsAry();	
					// check to see if constructs 1, 2, and 3 are null.
					// if they are, put them in before we actually do the iterator.....
					// this is because we HAD the 3 constructs, then we TOOK AWAY the 3
					// constructs, and now we're ADDING THEM BACK.
					// CPI_CONSTRUCTS.put("ITOVAHTN", 1);	//CPI Reliability
					// CPI_CONSTRUCTS.put("FAWCQLDH", 2);	//CPI Gender
					// CPI_CONSTRUCTS.put("IUWLBGPA", 3);	//CPI Type
					if(modKey.contentEquals("HSJRIKLO"))
					{
						//System.out.println("modKey == HSJRIKLO"); 
						if( constAr.get(0) == null)
						{
							//System.out.println("constAr[0] is null");
				       		ConstructDTO constDTO = new ConstructDTO();
				       		constDTO.setConstructId("ITOVAHTN");
				       		constDTO.setConstructName("CPI Reliability");
				       		constDTO.setShowCheckMark(0); // 0 is the default	
				       		constAr.remove(0);
				       		constAr.add(0, constDTO);
						}
						if( constAr.get(1) == null)
						{
							//System.out.println("constAr[1] is null");
				       		ConstructDTO constDTO = new ConstructDTO();
				       		constDTO.setConstructId("FAWCQLDH");
				       		constDTO.setConstructName("CPI Gender");
				       		constDTO.setShowCheckMark(0); // 0 is the default
				       		constAr.remove(1);
				       		constAr.add(1, constDTO);
						}
						if( constAr.get(2) == null)
						{
							//System.out.println("constAr[2] is null");
				       		ConstructDTO constDTO = new ConstructDTO();
				       		constDTO.setConstructId("IUWLBGPA");
				       		constDTO.setConstructName("CPI Type");
				       		constDTO.setShowCheckMark(0); // 0 is the default
				       		constAr.remove(2);
				       		constAr.add(2, constDTO);
						}						
					}	// End if (HSJRIKLO)
					
					Iterator<ConstructDTO> it4 = constAr.iterator();
					while(it4.hasNext())
					{
						ConstructDTO cdto = (ConstructDTO) it4.next();

						String cKey = cdto.getConstructId();
						//System.out.println("cdto.getConstructName() " + cdto.getConstructName());
						//System.out.println("  cdto.getConstructId() " + cdto.getConstructId());
						//System.out.println("  constMap.get(cKey)  " + constMap.get(cKey));
						
						// check for a score in the constMap.put(constId, score)
						if(constMap.containsKey(cKey))
						{
							// add the new score to the construct array in the dto
							cdto.setConstructScore(constMap.get(cKey));
							//System.out.println("cdto.getConstructScore() " + cdto.getConstructScore());
							cdto.setConstructScore2(constMap.get(cKey));
							cdto.setShowCheckMark(2); // set to valid
							cdto.setConstructValidated(true);
							// there's a construct score, so set the "hasScores" to true;
							//System.out.println(mcDTO.getModuleName() + "  setting setHasScores to true");
							mcDTO.setHasScores(true); 
						}
					}	// End while(it4.hasNext())
				}	// End if (map contains key)
			}	// End while(it3.hasNext())

	       return modConstList;	
		}
        catch (SQLException ex)
		{
        	ex.printStackTrace();
        	// handle any errors
        	throw new Exception("SQL fetching linkParams data.  " +
        			"SQLException: " + ex.getMessage() + ", " +
					"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
        finally
		{
			if (dr != null) { dr.close(); dr = null; }
        }
	}	

	
	/**
	 * checkChq checks the answers table to see if the CHQ
	 * has been imported (the completed flag = 1)
	 *
	 * @param candId - the candidate Id
	 * @param projId - the project Id
	 * @return a boolean; true if completed = 1, false in all other circumstances
	 * @throws Exception
	 */
	private boolean isChqImported(String candId, String projId)
		throws Exception
	{
		boolean ret = false;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT aa.Completed ");
		sqlQuery.append("  FROM answers aa ");
		sqlQuery.append("  WHERE aa.CandidateID = ? ");
		sqlQuery.append("    AND aa.JobID = ? ");
		sqlQuery.append("    AND aa.ModuleID = '" + ManualEntryConstants.CHQ_MOD + "'");
		//System.out.println("checkChq SQL = " + sqlQuery.toString());

		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			//TODO When this method returns something, this should be modified
			System.out.println("Error preparing checkChq query." + dlps.toString());
			return ret;
		}

		DataResult dr = null;
		ResultSet rs = null;

		try
		{
			// Fill sql parmeters with values and get the data
			dlps.getPreparedStatement().setString(1, candId);
			dlps.getPreparedStatement().setString(2, projId);
			//System.out.println("candId = " + candId + "   projId = " + projId);
			dr = V2DatabaseUtils.select(dlps);
			rs = dr.getResultSet();

			if (rs == null || ! rs.isBeforeFirst())
			{
				// there are no results for this candidate
				// so return the default (false)
				return  ret;
	       	}

			// Get the first (and only row
			rs.next();
			float comp = rs.getFloat("Completed");
			if (comp == 1.0)
				ret = true;
			
			return ret;
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			// handle any errors
			throw new Exception("SQL fetching linkParams data.  " +
					"SQLException: " + ex.getMessage() + ", " +
					"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}
	
	
	/**
	 * This version of getResultsForConstructsForVendorIntegration
	 * is intended for the Vendor Integration of the   
	 * 
	 * It was adapted, initially, by
	 * MB for the SHL vendor integration.
	 * 
	 * Fetch the results for any constructs  that have
	 * results for a given candidate, construct, project
	 *
	 * @param modConstList
	 * @param candId - the candidate Id
	 * @param projId - the project Id
	 * @param scoreData 
	 * @return an ArrayList of KeyValuePair objects
	 * @throws Exception
	 */
	public ArrayList<ModConstsDTO> 
			getResultsForConstructsForVendorIntegration
			(ArrayList<ModConstsDTO> modConstList, String candId, String projId,
					HashMap<String, String> scoreData)
		throws Exception
	{
	
		// get module list string for SQL
		String moduleString = "";
		int index = 0;
		Iterator<ModConstsDTO> it = modConstList.iterator();
		while(it.hasNext())
		{
			ModConstsDTO mcDTO = new ModConstsDTO();
			mcDTO = (ModConstsDTO) it.next();
			if(index == 0)
			{
				moduleString += "('" + mcDTO.getModuleId() + "'";
			} else {
				moduleString += ",'" + mcDTO.getModuleId() + "'";
			}
			index++;
		}
		if(moduleString != "")
		{
			moduleString += ")";
		}
		 //System.out.println("moduleString = " + moduleString);
	    
	    StringBuffer sqlQuery = new StringBuffer();
	    sqlQuery.append("SELECT  distinct c.moduleId, c.uniqueIdStamp as constructId, c.name, r.CScore ");
	    sqlQuery.append("  FROM  constructs as c, modules as m, results as r  ");
	    sqlQuery.append("  WHERE  m.UniqueIdStamp = c.moduleId  ");
	    sqlQuery.append("  AND c.uniqueIdStamp =  r.constructId  ");
	    sqlQuery.append("  AND r.candidateId = ?   ");
	    sqlQuery.append("  AND r.jobId = ?   ");
	    sqlQuery.append("  AND m.UniqueIdStamp in ?  ");
	    sqlQuery.append("  ORDER BY c.moduleId, constructId   ");
	    //System.out.println("SQL = " + sqlQuery.toString());
	    
		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			//TODO When this method returns something, this should be modified
			System.out.println("Error preparing getRawScores query." + dlps.toString());
			return modConstList;
		}
	
		DataResult dr = null;
		ResultSet rs = null;
	    try
		{
			// Fill sql parmeters with values and get the data
			dlps.getPreparedStatement().setString(1, candId);
			dlps.getPreparedStatement().setString(2, projId);
			dlps.getPreparedStatement().setString(3, moduleString);
	
			dr = V2DatabaseUtils.select(dlps);
			rs = dr.getResultSet();

	       	if (! rs.isBeforeFirst())
	       	{
	       		// System.out.println("there are no results for this candidate.... ");       		
	       		// there are no existing results for this candidate
				// so just update w/ our passed in score data.... 
				
				it = null; // iterator -- reusing from above....
			   	it = modConstList.iterator();
				while(it.hasNext())
				{
					ModConstsDTO mcDTO = new ModConstsDTO();
					mcDTO = (ModConstsDTO) it.next();
					
					// get the current moduleId
					String modKey = mcDTO.getModuleId();
					//System.out.println("getModule id " + mcDTO.getModuleId());
	
					// get the constants array<ConstructDTO> from the dto, which we'll spin through
					// to add the scores to the dto
					ArrayList<ConstructDTO> constAr = mcDTO.getConstructsAry();	
					// Look for the 3 unrequired  cpi constructs....
					// check to see if constructs 1, 2, and 3 are null.
					// if they are, put them in before we actually do the iterator.....
					// this is because we HAD the 3 constructs, then we TOOK AWAY the 3
					// constructs, and now we're ADDING THEM BACK, and they're not
					// required.
					// CPI_CONSTRUCTS.put("ITOVAHTN", 1);	//CPI Reliability
					// CPI_CONSTRUCTS.put("FAWCQLDH", 2);	//CPI Gender
					// CPI_CONSTRUCTS.put("IUWLBGPA", 3);	//CPI Type
					if(modKey.contentEquals("HSJRIKLO"))
					{
						//System.out.println("modKey == HSJRIKLO"); 
						if( constAr.get(0) == null)
						{
							//System.out.println("constAr[0] is null");
				       		ConstructDTO constDTO = new ConstructDTO();
				       		constDTO.setConstructId("ITOVAHTN");
				       		constDTO.setConstructName("CPI Reliability");
				       		constDTO.setShowCheckMark(0); // 0 is the default	
				       		constAr.remove(0);
				       		constAr.add(0, constDTO);
						}
						if( constAr.get(1) == null)
						{
							//System.out.println("constAr[1] is null");
				       		ConstructDTO constDTO = new ConstructDTO();
				       		constDTO.setConstructId("FAWCQLDH");
				       		constDTO.setConstructName("CPI Gender");
				       		constDTO.setShowCheckMark(0); // 0 is the default
				       		constAr.remove(1);
				       		constAr.add(1, constDTO);
						}
						if( constAr.get(2) == null){
							//System.out.println("constAr[2] is null");
				       		ConstructDTO constDTO = new ConstructDTO();
				       		constDTO.setConstructId("IUWLBGPA");
				       		constDTO.setConstructName("CPI Type");
				       		constDTO.setShowCheckMark(0); // 0 is the default
				       		constAr.remove(2);
				       		constAr.add(2, constDTO);
						}						
					}
			
					Iterator<ConstructDTO> it2 = constAr.iterator();
					while(it2.hasNext())
					{	
						ConstructDTO cdto = (ConstructDTO) it2.next();
						String cKey = cdto.getConstructId();					
						//System.out.println("cdto.getConstructId() " + cdto.getConstructId());
						
						// check for a score in the scoreData map
						// that we passed in -- these are the scores 
						// from the XML
						if(scoreData.containsKey(cKey))
						{
							// add the new score to the construct array in the dto	
							// Note that I'm only setting scores here, not the other
							// construct data, which we don't care about 
							cdto.setConstructScore(scoreData.get(cKey));
							//System.out.println("new Integer(scoreData.get(cKey)) " + scoreData.get(cKey));
							cdto.setConstructScore2(scoreData.get(cKey));
	
							// there's a construct score, so set the "hasScores" to true;
							mcDTO.setHasScores(true); 
						}
					} // End of itr2 loop
				}	// End of while loop on it
	
	       		return  modConstList;
	       		
	       	} else {  // there ARE scores for this participant already existing in the database
	       		
	           	String currentModId = "";
	           	HashMap<String, HashMap<String, String>> modMap = new HashMap<String, HashMap<String, String>>();;
	           	HashMap<String, String> constMap = new HashMap<String, String>();
	           	int i = 0;
	           	while (rs.next())
	           	{
	           		String newModId = rs.getString("moduleId");
	           		String constId = rs.getString("constructId");
	           		String score = rs.getString("CScore");
	           		//System.out.println("string score = " + score);
	           		
	           		// first time through or new model id
	           		if( (i == 0) || (! newModId.equals(currentModId)) )
	           		{
	           			// add the existing data to the modMap hash
	           			modMap.put(newModId, constMap);
	    			
	           			// set the current modId
	           			currentModId = rs.getString("moduleId");
	           			// create a new constMap to hold construct Data for the new module
	           			//constMap = new HashMap<String, String>();
	           		}
	           		
	           		// add construct data to the construct map
	           		// we'll add this map to the modMap when we get a new module Id
	           		constMap.put(constId, score);
	           		//System.out.println("constId, score " + constId + " " + score);
	           		i++;
	           	}	// End of while loop on rs
	           	
	           	
	    	    // okay... we have all the construct scores now....
	    	    // we need to update the ArrayList<ModConstsDTO> modConstList
	    	    // so we can send it back to update the ManualEntryDTO....
	    	    // so... spin through the modConstList and plug it in our data.....        	
	           	
	           	it = null; // iterator -- reusing from above....
	           	it = modConstList.iterator();
	    		while(it.hasNext())
	    		{			
	    			ModConstsDTO mcDTO = new ModConstsDTO();
	    			mcDTO = (ModConstsDTO) it.next();
	    			
	    			// get the current moduleId
	    			String modKey = mcDTO.getModuleId();
	    	
	    			// see if it exists in the results data
	    			if(modMap.containsKey(modKey))
	    			{
	    				// we have a module, so we can get the construct data
	    				//System.out.println("getModule id " + mcDTO.getModuleId());
	    				// get the constants array<ConstructDTO> from the dto, which we'll spin through
	    				// to add the scores to the dto
	    				//System.out.println("2 mcDTO.getConstructsAry().size  " + mcDTO.getConstructsAry().size());
	    				
	    				ArrayList<ConstructDTO> constAr = mcDTO.getConstructsAry();	
	    				// check to see if constructs 1, 2, and 3 are null.
	    				// if they are, put them in before we actually do the iterator.....
	    				// this is because we HAD the 3 constructs, then we TOOK AWAY the 3
	    				// constructs, and now we're ADDING THEM BACK.
	    				// CPI_CONSTRUCTS.put("ITOVAHTN", 1);	//CPI Reliability
	    				// CPI_CONSTRUCTS.put("FAWCQLDH", 2);	//CPI Gender
	    				// CPI_CONSTRUCTS.put("IUWLBGPA", 3);	//CPI Type
	    				if(modKey.contentEquals("HSJRIKLO"))
	    				{
	    					//System.out.println("modKey == HSJRIKLO"); 
	    					if( constAr.get(0) == null)
	    					{
	    						//System.out.println("constAr[0] is null");
	    			       		ConstructDTO constDTO = new ConstructDTO();
	    			       		constDTO.setConstructId("ITOVAHTN");
	    			       		constDTO.setConstructName("CPI Reliability");
	    			       		constDTO.setShowCheckMark(0); // 0 is the default	
	    			       		constAr.remove(0);
	    			       		constAr.add(0, constDTO);
	    					}
	    					if( constAr.get(1) == null)
	    					{
	    						//System.out.println("constAr[1] is null");
	    			       		ConstructDTO constDTO = new ConstructDTO();
	    			       		constDTO.setConstructId("FAWCQLDH");
	    			       		constDTO.setConstructName("CPI Gender");
	    			       		constDTO.setShowCheckMark(0); // 0 is the default
	    			       		constAr.remove(1);
	    			       		constAr.add(1, constDTO);
	    					}
	    					if( constAr.get(2) == null)
	    					{
	    						//System.out.println("constAr[2] is null");
	    			       		ConstructDTO constDTO = new ConstructDTO();
	    			       		constDTO.setConstructId("IUWLBGPA");
	    			       		constDTO.setConstructName("CPI Type");
	    			       		constDTO.setShowCheckMark(0); // 0 is the default
	    			       		constAr.remove(2);
	    			       		constAr.add(2, constDTO);
	    					}						
	    				}
	    				
	    				Iterator<ConstructDTO> it2 = constAr.iterator();
	    				while(it2.hasNext())
	    				{
	    					ConstructDTO cdto = (ConstructDTO) it2.next();
	    					String cKey = cdto.getConstructId();
	     					//System.out.println("cdto.getConstructId() " + cdto.getConstructId());
	     					
	    					// check for a score in the constMap.put(constId, score)
	    					if(scoreData.containsKey(cKey))
	    					{
	    						// add the new score to the construct array in the dto
	    						
	    						cdto.setConstructScore(scoreData.get(cKey));
	    						//System.out.println("new Integer(scoreData.get(cKey)) " + scoreData.get(cKey));
	     						cdto.setConstructScore2(scoreData.get(cKey));
	
	     						// there's a construct score, so set the "hasScores" to true;
	    						mcDTO.setHasScores(true); 
	    					}
	    				}	// End of while loop on itr2
	    			}	// End if( map contains key)
	    		}	// End of while loop on it
	       	}
	
	       return modConstList;	
		}
	    catch (SQLException ex)
		{
	    	ex.printStackTrace();
	    	// handle any errors
	    	throw new Exception("SQL fetching linkParams data.  " +
	    			"SQLException: " + ex.getMessage() + ", " +
					"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
	    finally
		{
			if (dr != null) { dr.close(); dr = null; }
	    }
	}	


	/**
	 * This method will parse the XML from the
	 * results table to get the norm ID used 
	 * to norm this instrument
	 * 
	 * @param String resultsXML - the xml to be parsed
	 * @return String normId - the norm Id of the norm
	 * 							used to score this instrument
	 * 
	 */
	
	private String parseResultsXML( String resultsXML)
		throws Exception
	{
		String normId = null;
		try
		{
			//XPathExpression  xPathExpression =  null;			
			//InputSource is = null;
			
			DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
			dbfac.setNamespaceAware(true);
			DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
			Document doc = docBuilder.parse(new InputSource(new StringReader(resultsXML)));
			
			XPath xpath = XPathFactory.newInstance().newXPath(); 
			NodeList nodes = (NodeList) xpath.evaluate("//Norms", doc, XPathConstants.NODESET); 
			Node node = nodes.item(0);
			
			if(node != null){
				//System.out.println("node.getNodeName() = " + node.getNodeName());
				Node node2 = node.getFirstChild();
				if(node2 == null){
					//System.out.println("node2 == null ");		
				}else{								
					if(node2.getNodeName() == null || node2.getNodeName().equals("") || node2.getNodeName().equals("null")){
						//System.out.println("node2.get node name == null... no normId");
						//System.out.println("resultsXML = " + resultsXML);
					}else{
						//System.out.println("node2.getFirstChild().getNodeName() = " + node2.getNodeName());
						normId = node2.getNodeName();	
					}
						
				}
			}
			
			 return normId;
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
	       	throw new Exception("Unable to parse the LinkParams XML:  " + ex.getMessage());
		}
	}

	/**
	 * This method will parse the XML from the
	 * results table to get the norm ID used 
	 * to norm this instrument
	 * 
	 * @param String resultsXML - the xml to be parsed
	 * @return String normId - the norm Id of the norm
	 * 							used to score this instrument
	 * 
	 */
	
	private String parseResultsXML( String resultsXML, String modId)
		throws Exception
	{
		//System.out.println("1818 parseResultsXML");
		String normId = null;
	
		// whip through the modconstdtolist in the lpdto and get the
		// modconstdto for this module id... 
		// use the normlist in that modconstdto list to determine
		// if this is a group 2 norm...
		ModConstsDTO mcDTO = null;
		ArrayList <KeyValuePairStrings> normList = null;
		List <ModConstsDTO> temp = this.lpDTO.getModConstDTOList();
		Iterator <ModConstsDTO> it = temp.iterator();
		while(it.hasNext())
		{
			ModConstsDTO dto = it.next();
			if(modId.equals(dto.getModuleId()))
			{
				mcDTO = dto;
				break;
			}
		}	
		
		if(mcDTO.getNormList() == null)
		{
			//System.out.println("No norms associated with this ModConstsDTO. ");
			return null;
		}
		
		normList =  mcDTO.getNormList();
		ArrayList<String> normNames = new ArrayList<String>();
		// build me a string list that i can use.... 
		Iterator <KeyValuePairStrings>it2 = normList.iterator();
		while(it2.hasNext())
		{
			//System.out.println("NormNames List = " + it2.next().getTheKey());
			normNames.add(it2.next().getTheKey());		
		}
		
		try
		{
			
			DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
			dbfac.setNamespaceAware(true);
			DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
			Document doc = docBuilder.parse(new InputSource(new StringReader(resultsXML)));
			
			XPath xpath = XPathFactory.newInstance().newXPath(); 
			NodeList nodes = (NodeList) xpath.evaluate("//Norms", doc, XPathConstants.NODESET); 
	
			boolean done = false;
	
			Node node = nodes.item(0);
			if(node != null)
			{
				//System.out.println("node.getNodeName() = " + node.getNodeName());
				Node node2 = node.getFirstChild();
				if(node2 == null)
				{
					//System.out.println("node2 == null ");		
				} else {								
					if(node2.getNodeName() == null ||
					   node2.getNodeName().equals("") ||
					   node2.getNodeName().equals("null"))
					{
						//System.out.println("node2.get node name == null... no normId");
						//System.out.println("resultsXML = " + resultsXML);
					} else {
						// FINDING A MATCH AGAINST THE NORM NAMES MAP....
						String nodeName = node2.getNodeName();
						for(int j=0; j<normNames.size();j++)
						{
							//System.out.println("1881 NODE NAME: " + nodeName + "  normNames.get(" + j +") " + normNames.get(j));
							if(nodeName.equals(normNames.get(j)))
							{
								normId = nodeName;
								//System.out.println("1883   SETTING NORM ID = " + nodeName);
								break;
							}
						}
							
						if(normId == null)
						{
							NodeList nodes2 = node.getChildNodes();
								
							for(int i=0; i<nodes2.getLength(); i++)
							{
								if(done)
								{
									break;
								}
								Node node3 = nodes2.item(i);
								nodeName = node3.getNodeName();
								//System.out.println("1897 nodeName " + nodeName);
								for(int j=0; j<normNames.size();j++)
								{
									//System.out.println("1904 NODE NAME: " + nodeName + "  normNames.get(" + j +") " + normNames.get(j));
									if(nodeName.equals(normNames.get(j)))
									{
										normId = nodeName;
										//System.out.println("1904  SETTING NORM ID = " + nodeName);
										done = true;
										break;
									}
								}	// End normNames loop
							}	// End loop on nodes2
						}	// End if (normId == null)
					}	// End of else on node2.normName
				}	// End of else on node2 null check
			}	// End of if(node != null)
	
			return normId;
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
	       	throw new Exception("Unable to parse the LinkParams XML:  " + ex.getMessage());
		}
	}


	/**
	 * This method will parse the XML from the
	 * results table to get the norm ID used 
	 * to norm this instrument
	 * 
	 * @param String resultsXML - the xml to be parsed
	 * @return String normId - the norm Id of the norm
	 * 							used to score this instrument
	 * 
	 */
	private String parseResultsXMLForGenPopNorm( String resultsXML, String modId)
		throws Exception
	{
		//System.out.println("1945 parseResultsXMLForGenPopNorm");
		String normId = null;  // this is the return string
	
		// whip through the modconstdtolist in the lpdto and get the
		// modconstdto for this module id... 
		// use the normlist in that modconstdto list to determine
		// if this is a group 2 norm...
		ModConstsDTO mcDTO = null;
		ArrayList <KeyValuePairStrings> normList = null;
		List <ModConstsDTO> temp = this.lpDTO.getModConstDTOList();
		Iterator <ModConstsDTO> it = temp.iterator();
		while(it.hasNext())
		{
			ModConstsDTO dto = it.next();
			if(modId.equals(dto.getModuleId()))
			{
				mcDTO = dto;
				break;
			}
		}	
		
		// USE THE GEN POP NORM LIST
		if(mcDTO.getNormListGenPop() == null)
		{
			//System.out.println("No norms associated with this ModConstsDTO. ");
			return null;
		}
		
		normList =  mcDTO.getNormListGenPop();
		ArrayList<String> normNames = new ArrayList<String>();
		// build me a string list that i can use.... 
		Iterator <KeyValuePairStrings>it2 = normList.iterator();
		while(it2.hasNext())
		{
			//System.out.println("GenPopNormNames List = " + it2.next().getTheKey());
			normNames.add(it2.next().getTheKey());
		}
		
		//System.out.println("normnames.size = " + normNames.size());
		//System.out.println("normNames.get(0) = " + normNames.get(0));
		
		try
		{
			DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
			dbfac.setNamespaceAware(true);
			DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
			Document doc = docBuilder.parse(new InputSource(new StringReader(resultsXML)));
			
			XPath xpath = XPathFactory.newInstance().newXPath(); 
			NodeList nodes = (NodeList) xpath.evaluate("//Norms", doc, XPathConstants.NODESET); 
	
			boolean done = false;
	
			Node node = nodes.item(0);
			if(node != null)
			{
				//System.out.println("node.getNodeName() = " + node.getNodeName()); // <Norms>
				Node node2 = node.getFirstChild();
				if(node2 == null)
				{
					//System.out.println("node2 == null ");		
				} else {							
					if(node2.getNodeName() == null ||
					   node2.getNodeName().equals("") ||
					   node2.getNodeName().equals("null"))
					{
						//System.out.println("node2.get node name == null... no normId");
						//System.out.println("resultsXML = " + resultsXML);
					} else {
						// FINDING A MATCH AGAINST THE NORM NAMES MAP....
						//String nodeName = node2.getNodeName();
						//for(int j=0; j<normNames.size();j++)
						//{
						//	System.out.println("1881  normNames.get(j) " + normNames.get(j));
						//	if(nodeName.equals(normNames.get(j)))
						//{
						//		normId = nodeName;
						//		System.out.println("1883   SETTING NORM ID = " + nodeName);
						//
						//		break;
						//	}
						//}
						
						if(normId == null)
						{
							NodeList nodes2 = node.getChildNodes();
							
							for(int i=0; i<nodes2.getLength(); i++)
							{
								if(done){
									break;
								}
								Node node3 = nodes2.item(i);
								String nodeName = node3.getNodeName();
								for(int j=0; j<normNames.size();j++)
								{
									//System.out.println("2033 NODE NAME: " + nodeName + "  normNames.get(" + j +") " + normNames.get(j));
									if(nodeName.equals(normNames.get(j)))
									{
										normId = nodeName;
										//System.out.println("2036  SETTING NORM ID = " + nodeName);
										done = true;
										break;
									}
								}	// End of normNames loop
							}	// End of node2 for loop
						}	// End of if(normId == null)
					}	// End of else on node2 nodeName chaecks
				}	// End of else on if(node2 == null)
			}	// End of if(node != null)
				 
			return normId;
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
	       	throw new Exception("Unable to parse the LinkParams XML:  " + ex.getMessage());
		}
	}

	/**
	 * This method will parse the XML from the
	 * results table to get the norm ID used 
	 * to norm this instrument
	 * 
	 * @param String resultsXML - the xml to be parsed
	 * @return String score - the instrument raw or scale score
	 * 
	 */
	
	private String parseResultsXMLForNormedScore( String resultsXML, String normId)
		throws Exception
	{
		//System.out.println("parseResultsXMLForNormedScore... normId " + normId);
		//String score = "";
		try
		{
			DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
			dbfac.setNamespaceAware(true);
			DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
			Document doc = docBuilder.parse(new InputSource(new StringReader(resultsXML)));
			
			XPath xpath = XPathFactory.newInstance().newXPath();
			String evalPath = "//XML_Fields";
			NodeList nodes = (NodeList) xpath.evaluate(evalPath, doc, XPathConstants.NODESET);
			Node xmlFields = nodes.item(0);
			
			// this is the CScore - the raw or scale score
			Node cscore = xmlFields.getFirstChild();
			//System.out.println("cscore.getNodeName() = " + cscore.getNodeName());
			//System.out.println("cscore.getTextContent() = " + cscore.getTextContent());
			
			if(cscore.getTextContent() != null)
			{
				return cscore.getTextContent();
			}
			
			//// this is the normed score, if present
			//Node nodeName = xmlFields.getFirstChild().getNextSibling();		
			////System.out.println("nodeName.getNodeName() = " + nodeName.getNodeName());
			////System.out.println("nodeName.getTextContent() = " + nodeName.getTextContent());
			//
			//if(nodeName != null){
			//	//System.out.println("nodeName.getNodeName() = " + nodeName.getNodeName());
			//	//System.out.println("nodeName.getParentNode().getNodeName() = " + nodeName.getParentNode().getNodeName());
			//	if(nodeName.getTextContent() != null){
			//		//System.out.println("nodeName.getTextContent() = " + nodeName.getTextContent());
			//		// set the normed score
			//		score = nodeName.getTextContent();
			//		return score;
			//	}
			//}
			
			return null;
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
	       	throw new Exception("Unable to parse the LinkParams XML:  " + ex.getMessage());
		}
	}

	/**
	 * Fetch all norms for the given module
	 *
	 * @return an ArrayList of KeyValuePair objects
	 * @throws Exception
	 */
	protected ArrayList<KeyValuePairStrings> fetchNormsForModule(String moduleId)
		throws Exception
	{
		ArrayList<KeyValuePairStrings> normList = new ArrayList<KeyValuePairStrings>();
	    
	    StringBuffer sqlQuery = new StringBuffer();
	    sqlQuery.append("SELECT  uniqueIdStamp, NormName ");
	    sqlQuery.append("  FROM  norms ");
	    sqlQuery.append("  WHERE testId = ?  ");
	    sqlQuery.append("  AND archived = '0'    ");
	    sqlQuery.append("  ORDER BY NormName   ");
	    //System.out.println("fetchNormsForModule SQL = " + sqlQuery.toString());
	    
		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			//TODO When this method returns something, this should be modified
			System.out.println("Error preparing fetchNormsForModule query." + dlps.toString());
			return normList;
		}
	
		DataResult dr = null;
		ResultSet rs = null;
		
	    try
		{
			// Fill sql parmeters with values and get the data
			dlps.getPreparedStatement().setString(1, moduleId);
	
			dr = V2DatabaseUtils.select(dlps);
			rs = dr.getResultSet();
	       	
	       	if (rs == null || ! rs.isBeforeFirst())
	       	{
	       		// return empty list.
	       		return  normList;
	       	}
	
	       	while (rs.next())
	       	{
	      	
		       	KeyValuePairStrings kvps = new KeyValuePairStrings();
		       	kvps.setTheKey(rs.getString("uniqueIdStamp"));
		       	kvps.setTheValue(rs.getString("NormName"));
		        	//System.out.println("     " + kvps.getTheKey() + "  " + kvps.getTheValue());
		       	normList.add(kvps);
	 
	       	}
	        
	       // add an empty one to the list... 
	       	KeyValuePairStrings kvps = new KeyValuePairStrings();
	       	kvps.setTheKey("0");
	       	kvps.setTheValue("Select One");
	       	normList.add(0, kvps);
	       return normList;	
	       
		}
	    catch (SQLException ex)
		{
	    	ex.printStackTrace();
	    	// handle any errors
	    	throw new Exception("SQL fetching linkParams data.  " +
	    			"SQLException: " + ex.getMessage() + ", " +
					"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
	    finally
		{
			if (dr != null) { dr.close(); dr = null; }
	    }
	}	


	/**
	 * Fetch spec-pop norms for the given module
	 *
	 * @return an ArrayList of KeyValuePair objects
	 * @throws Exception
	 */
	protected ArrayList<KeyValuePairStrings> fetchNormsForModuleSpecPopOnly(String moduleId)
		throws Exception
	{
		ArrayList<KeyValuePairStrings> normList = new ArrayList<KeyValuePairStrings>();
	 
	    StringBuffer sqlQuery = new StringBuffer();
	    sqlQuery.append("SELECT  uniqueIdStamp, NormName ");
	    sqlQuery.append("  FROM  norms ");
	    sqlQuery.append("  WHERE testId = ?  ");
	    sqlQuery.append("  AND archived = '0'    ");
	    sqlQuery.append("  AND xml like '%<Group>2<%'    ");
	    sqlQuery.append("  ORDER BY NormName   ");
	    //System.out.println("fetchNormsForModuleSpecPopOnly SQL = testID = " + moduleId + "  " + sqlQuery.toString());
	    
		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
		
		if (dlps.isInError())
		{
			//TODO When this method returns something, this should be modified
			System.out.println("Error preparing fetchNormsForModuleSpecPopOnly query." + dlps.toString());
			return normList;
		}
	
		DataResult dr = null;
		ResultSet rs = null;
	    try
		{
			// Fill sql parmeters with values and get the data
			dlps.getPreparedStatement().setString(1, moduleId);
	
			dr = V2DatabaseUtils.select(dlps);
			rs = dr.getResultSet();
	       	if (rs == null || ! rs.isBeforeFirst())
	       	{
	       		//System.out.println("  return empty normsList list.   ");
	       		// return empty list.
	       		return  normList;
	       	}
	       	while (rs.next())
	       	{
		       	KeyValuePairStrings kvps = new KeyValuePairStrings();
		       	kvps.setTheKey(rs.getString("uniqueIdStamp"));
		       	kvps.setTheValue(rs.getString("NormName"));
		        	//System.out.println("     " + kvps.getTheKey() + "  " + kvps.getTheValue());
		       	normList.add(kvps);
	       	}
	        
	       // add an empty one to the list... 
	       	KeyValuePairStrings kvps = new KeyValuePairStrings();
	       	kvps.setTheKey("0");
	       	kvps.setTheValue("Select Special Population Norm");
	       	normList.add(0, kvps);
	       	return normList;	
		}
	    catch (SQLException ex)
		{
	    	ex.printStackTrace();
	    	// handle any errors
	    	throw new Exception("SQL fetching linkParams data.  " +
	    			"SQLException: " + ex.getMessage() + ", " +
					"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
	    finally
		{
			if (dr != null) { dr.close(); dr = null; }
	    }
	}

	
	/**
	 * Fetch gen-pop norms for the given module
	 *
	 * @param moduleId the module ID for which to get the norms
	 * @return an ArrayList of KeyValuePair objects
	 * @throws Exception
	 */
	protected ArrayList<KeyValuePairStrings> fetchNormsForModuleGenPopOnly(String moduleId)
		throws Exception
	{
		ArrayList<KeyValuePairStrings> normListGenPop = new ArrayList<KeyValuePairStrings>();
	 
	    StringBuffer sqlQuery = new StringBuffer();
	    sqlQuery.append("SELECT  uniqueIdStamp, NormName ");
	    sqlQuery.append("  FROM  norms ");
	    sqlQuery.append("  WHERE testId = ?  ");
	    sqlQuery.append("  AND archived = '0'    ");
	    sqlQuery.append("  AND xml like '%<Group>1<%'    ");
	    sqlQuery.append("  ORDER BY NormName   ");
	    //System.out.println("fetchNormsForModuleGenPopOnly SQL = testID = " + moduleId + "  "  +sqlQuery.toString());
	    
		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
		
		if (dlps.isInError())
		{
			//TODO When this method returns something, this should be modified
			System.out.println("Error preparing fetchNormsForModuleGenPopOnly query." + dlps.toString());
			return normListGenPop;
		}
	
		DataResult dr = null;
		ResultSet rs = null;
		
	    try
		{
			// Fill sql parmeters with values and get the data
			dlps.getPreparedStatement().setString(1, moduleId);
	
			dr = V2DatabaseUtils.select(dlps);
			rs = dr.getResultSet();
	
	       	if (rs == null || ! rs.isBeforeFirst())
	       	{
	       		//System.out.println("  return empty normsList list.   ");
	       		// return empty list.
	       		return  normListGenPop;
	       	}
	
	       	while (rs.next())
	       	{
		       	KeyValuePairStrings kvps = new KeyValuePairStrings();
		       	kvps.setTheKey(rs.getString("uniqueIdStamp"));
		       	kvps.setTheValue(rs.getString("NormName"));
		        	//System.out.println("     " + kvps.getTheKey() + "  " + kvps.getTheValue());
		       	normListGenPop.add(kvps);
	       	}
	        
	       // add an empty one to the list... 
	       	KeyValuePairStrings kvps = new KeyValuePairStrings();
	       	kvps.setTheKey("0");
	       	kvps.setTheValue("Select General Population Norm");
	       	normListGenPop.add(0, kvps);

	       	return normListGenPop;	
		}
	    catch (SQLException ex)
		{
	    	ex.printStackTrace();
	    	// handle any errors
	    	throw new Exception("SQL fetching linkParams data.  " +
	    			"SQLException: " + ex.getMessage() + ", " +
					"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
	    finally
		{
			if (dr != null) { dr.close(); dr = null; }
	    }
	}

	
	/**
	 * Fetch answers from V2 for the specified participant and module
	 *
	 * @param partId participantId for whom the answers are being fetched
	 * @param moduleId the V2 module id for which answers are being retrieved
	 * @return a HashMap of question IDs (key) and answers (value)
	 * @throws Exception
	 */
	public  HashMap<String, String> getAdaptV2Answers(String partId, String moduleId)
		throws Exception
	{
		String answerXML = "";
		HashMap<String, String> ret = new HashMap<String, String> ();
		
	    StringBuffer sqlQuery = new StringBuffer();
	    sqlQuery.append("SELECT  aa.XML ");
	    sqlQuery.append("  FROM  answers aa ");
	    sqlQuery.append("  WHERE aa.candidateId = '" + partId +"'  ");
	    sqlQuery.append("  AND   aa.moduleId = '" + moduleId + "' " );
	    sqlQuery.append("  order by dateCreatedStamp DESC  " );
	    //System.out.println(" get linkparams sql : " + sqlQuery.toString());
	
		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			//TODO When this method returns something, this should be modified
			System.out.println("Error preparing getChqAnswers query." + dlps.toString());
			return null;
		}
		
		DataResult dr = null;
		ResultSet rs = null;
	
	    try
		{
			dr = V2DatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			
	       	if (rs == null || ! rs.isBeforeFirst())
	       	{
	       		//throw new Exception("No CHQ data for " + partId);
	       		return null;
	       	}
	       	
	       	rs.next();
	
	   		//System.out.println("rs.getString(XML)" + rs.getString("XML"));
	       	answerXML = new String(rs.getBytes("XML"), "UTF-8");
		}
	    catch (SQLException ex)
		{
	    	ex.printStackTrace();
	    	// handle any errors
	    	throw new Exception("SQL fetching CHQ Answer Data  " +
	    			"SQLException: " + ex.getMessage() + ", " +
					"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
	    finally
		{
			if (dr != null) { dr.close(); dr = null; }
	    }
	
	    // now parse the xml....
	    ret = parseAnswerXml(answerXML);
	
	    return ret;
	}


	/**
	* parseAnswerXml puts the chq answers that had been
	* retrieved from ADAPT into a hashmap that can be
	* used by groovy to create the velocity report.
	* 
	* @param xml - the answer xml
	* @return HashMap<String, String> -the hashmap of questionids and answers
	* @throws Exception 
	*/
	
	private HashMap<String, String> parseAnswerXml(String xml)
		throws Exception
	{
		HashMap<String, String> ret = new HashMap<String, String>();
		
		//The evaluate methods in the XPath and XPathExpression 
		//interfaces are used to parse an XML document with XPath expressions. 
		//The XPathFactory class is used to create an XPath object. 
		//Create an XPathFactory object with the  newInstance 
		//method of the XPathFactory class. 
		XPathFactory  xpFactory = XPathFactory.newInstance();
		
		//Create an XPath object from the XPathFactory object with the newXPath method.
		XPath myXPath = (XPath) xpFactory.newXPath();
		
		InputSource is;
		//LinkedHashMap<String, String> myNorms = new LinkedHashMap<String, String>();
			
		try
		{
			// Go to the 'Norms' node
			String expression = "/XML_Fields/Answers";
			is = new InputSource(new ByteArrayInputStream(xml.getBytes("UTF-8")));
			NodeList nodes = null;
			nodes = (NodeList) myXPath.evaluate(expression, is, XPathConstants.NODESET);
			if (nodes == null || nodes.getLength() < 1)
			{
				// return empty data
				return ret;
			}
		
			// The node called "Answers"
			Node node = nodes.item(0);
			NodeList nlist = node.getChildNodes();
			String all = "";
			// Get all of the answers in the Answer node and their responses...
			for (int i=0; i < nlist.getLength(); i++)
			{
				Node normNode = nlist.item(i);
				//String normId = normNode.getNodeName();
				Node n = normNode.getAttributes().getNamedItem("N");
				String questionId = n.getNodeValue();
				String response = XMLUtils.xmlFormatString(normNode.getFirstChild().getTextContent());
				all+=response;
				ret.put(questionId, response);
			}
			 
		}
		catch (XPathExpressionException ex)
		{
			ex.printStackTrace();
			System.out.println("ManualEntryDataHelper: Unable to parse norm data from " + xml);
			ex.printStackTrace();
			throw new Exception("Unable to parse norm data from " + xml);
		}
			
		return ret;
	}
}
