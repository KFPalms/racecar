package com.pdi.data.v3.helpers.ut;

//import java.util.ArrayList;

//import com.pdi.data.v3.dto.SessionUserDTO;
//import com.pdi.data.v3.dto.UserDTO;
//import com.pdi.data.v3.helpers.UserDataHelper;
//import java.util.Date;
import java.util.Random;

import com.pdi.properties.PropertyLoader;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

public class UserDataHelperUT extends TestCase {
    public UserDataHelperUT(
            String name) {
            super(name);
        } 
    
    
    public void testProperties() throws Exception {
    	UnitTestUtils.start(this);
    	
    	//Does the local property work?
    	String applicationId = PropertyLoader.getProperty("com.pdi.data.v3.application", "application.code");
    	assertEquals(applicationId != null, true);
    	assertEquals(applicationId.equals("PDI-DATA-V3"), true);
    	
    	UnitTestUtils.stop(this);
    }
    

    
    /*
     * Tests the default list
     * 
     */
    public void testGet() throws Exception {
    	UnitTestUtils.start(this);
    	/*
    	SessionUserDTO sessionUser = UserDataHelper.getUser(
    					PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.username"), 
    					PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.password"));
    	
    	System.out.println(sessionUser.toString());
    	
    	ArrayList<UserDTO> results = UserDataHelper.getUsers(sessionUser);
    	for(UserDTO result : results) {
    		result.toString();
    		//System.out.println(result.toString());
    	}
    	*/
    	UnitTestUtils.stop(this);
    }
    
    public void testCreatePassword()
    {

	    	//Valid formatting codes
	    	char[] codes = {'X','U','L','N'};
	    	
	    	//numbers array
	        char[] numbers = {'2','3','4','5','6','7','8','9'};
	        //lower case letters array
	        char[] lettersLC = {'a','b','c','d','e','f','g','h','j','k','m','n','p','q','r','s','t','u','v','w','x','y','z'};
	        //upper case letters array
	        char[] lettersUC = {'A','B','C','D','E','F','G','H','J','K','L','M','N','P','Q','R','S','T','U','V','W','X','Y','Z'};
	
	        //The format of the password
	        //U = upper case letter
	        //L = lower case letter
	        //N = number
	        //X = random
	        String format = "UXXLXNXX";
	        
	        Random random = new java.util.Random();
	        StringBuffer password = new StringBuffer();
	        
	        for(int i = 0; i < format.length(); i++) {
	        	char val = format.charAt(i);
	        	
	        	if(val == 'X') {
	        		val = codes[random.nextInt(3) + 1];
	        	}
	        	
	            switch (val)
	            {
	                case 'U':   // number
	                	password.append(lettersUC[random.nextInt(lettersUC.length)]);
	                    break;
	                case 'L':   // lower case
	                	 password.append(lettersLC[random.nextInt(lettersLC.length)]);
	                    break;
	                case'N':   // upper case
	                	password.append(numbers[random.nextInt(numbers.length)]);
	                    break;
	            }

	        System.out.println("PASSWORD: " + password); 
    	}
        //return str;
    }
    
    public static void main(String[] args) throws Exception {

        junit.textui.TestRunner.run(
        		UserDataHelperUT.class);
    }
}