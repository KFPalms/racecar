/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.dto;

import java.util.ArrayList;

/**
 * NoteHolder is a wrapper around the Note data for
 * a given participant. 
 * 12-08-2010
 * Notes are created in the Test Data application,
 * associated with a Participant.  
 * Notes are accessible - read only - in the 
 * DRI.
 * 
 * @author      MB Panichi
 */
public class NoteHolder extends CoreDataObject
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	/**
	 * 
	 */
	private static final long serialVersionUID = -4770957517776128528L;
	//
	// Instance data.
	//
	private String participantId = "";
    private ArrayList<Note> notesArray = new ArrayList<Note>();

	//
	// Constructors.
	//
	public NoteHolder()
	{
		// Does nothing presently
	}
	public NoteHolder(String partId)
	{
		participantId = partId;
	}

	//
	// Instance methods.
	//

	public String toString()
	{
		String ret = "NoteHolder:  ";
		ret += "part=" + participantId + ".  ";
		ret += " notes-[";
		String str = null;
		for(Note n:notesArray)
		{
			if (str != null)
				str+= ", ";
			str += n.toString();
		}
		ret += str + "]";
		
		return ret;
	}
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public String getParticipantId()
	{
		return participantId;
	}

	public void setParticipantId(String value)
	{
		participantId = value;
	}


	/*****************************************************************************************/
	public ArrayList<Note> getNotesArray()
	{
		return notesArray;
	}

	public void setNotesArray(ArrayList<Note> value)
	{
		notesArray = value;
	}

}

