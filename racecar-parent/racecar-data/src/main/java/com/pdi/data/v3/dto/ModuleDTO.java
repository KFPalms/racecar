/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.dto;

import org.w3c.dom.Node;

import com.pdi.data.util.BaseDTO;
import com.pdi.data.util.RequestStatus;
import com.pdi.xml.XMLUtils;

/**
 * DTO for module information
 * 
 *
 * @author		Gavin Myers
 */
public class ModuleDTO extends BaseDTO
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	
	//pulled from the database
	private String _moduleCode;
	private long _moduleId;
	
	//Pulled from webservices
	private String _moduleName;
	private String _moduleGUID;

	// Inferred
	private String _moduleStatus;
	//
	// Constructors.
	//

	/**
	 * "Standard" (no-parameter) constructor
	 */
	public ModuleDTO()
	{
		super();
		// Does nothing presently
	}
	
	/**
	 * Builds the object based off an xml node
	 * @param node
	 */
	public ModuleDTO(Node node)
	{
		/*
		 * The XML looks something like this:
		 * <?xml version="1.0" encoding="utf-8" standalone="yes"?>
		 * <ns2:participantResultModuleList xmlns:ns2="urn:iscopia.com:participant-1.0">
		 *   <ns2:participantResultModuleListLine>
		 *     <moduleIdentifier>10fe2cac-b951-4b2a-bb76-2f524629ceb5</moduleIdentifier>
		 *     <moduleName>Career Survey</moduleName>
		 *   </ns2:participantResultModuleListLine>
		 * </ns2:participantResultModuleList>
		 */
		
		this.setModuleGUID(XMLUtils.getElementValue(node, "moduleIdentifier/."));
		this.setModuleName(XMLUtils.getElementValue(node, "moduleName/."));
		
		if (this.getModuleGUID() == null &&
			this.getModuleName() == null)
		{
			this.getStatus().setStatusCode(RequestStatus.WB_NO_DATA);
		}
	}

	//
	// Instance methods.
	//
	
	/**
	 * Generic toString method
	 */
	public String toString()
	{
		String str = "\n --- ModuleDTO --- \n";
		str += "\nGUID (Web):            " + _moduleGUID;
		str += "\nName (Web):            " + _moduleName;
		str += "\nCode (DB):             " + _moduleCode;
		str += "\nID (DB):               " + _moduleId;
		str += "\nStatus (Web-Inferred): " + _moduleStatus;
		
		return str;
	}


	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
	
	/*****************************************************************************************/
	public void setModuleCode(String value)
	{
		_moduleCode = value;
	}

	public String getModuleCode()
	{
		return _moduleCode;
	}

	/*****************************************************************************************/
	public void setModuleName(String value)
	{
		_moduleName = value;
	}

	public String getModuleName()
	{
		return _moduleName;
	}

	/*****************************************************************************************/
	public void setModuleId(long value)
	{
		_moduleId = value;
	}

	public long getModuleId()
	{
		return _moduleId;
	}

	/*****************************************************************************************/
	public void setModuleGUID(String value)
	{
		_moduleGUID = value;
	}

	public String getModuleGUID()
	{
		return _moduleGUID;
	}	

	/*****************************************************************************************/
	public void setModuleStatus(String value)
	{
		_moduleStatus = value;
	}

	public String getModuleStatus()
	{
		return _moduleStatus;
	}	
}
