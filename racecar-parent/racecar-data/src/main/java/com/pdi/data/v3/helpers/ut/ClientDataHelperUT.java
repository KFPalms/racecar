package com.pdi.data.v3.helpers.ut;

//import java.util.ArrayList;

import com.pdi.data.v3.dto.ClientDTO;
import com.pdi.data.v3.dto.ParticipantDTO;
import com.pdi.data.v3.dto.ProjectDTO;
import com.pdi.data.v3.dto.SessionUserDTO;
import com.pdi.data.v3.helpers.ClientDataHelper;
import com.pdi.data.v3.helpers.ProjectDataHelper;
import com.pdi.properties.PropertyLoader;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

public class ClientDataHelperUT extends TestCase
{
	public ClientDataHelperUT(String name)
	{
		super(name);
	}



	public void testProperties()
		throws Exception
	{
    	UnitTestUtils.start(this);
    	
    	//Does the local property work?
    	String applicationId = PropertyLoader.getProperty("com.pdi.data.v3.application", "application.code");
    	assertEquals(applicationId != null, true);
    	assertEquals(applicationId.equals("PDI-DATA-V3"), true);
    	
    	UnitTestUtils.stop(this);
    }
    

    
    /*
     * Tests the default List Candidates
     * 
     */
    public void testGetClients()
    	throws Exception
    {
    	UnitTestUtils.start(this);
 
    	System.out.println("-----------------  getClients()  -----------------");
    	SessionUserDTO sessionUser = new SessionUserDTO();
    	sessionUser.setUsername(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.username")); 
    	sessionUser.setPassword(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.password"));

    	for(ClientDTO client : ClientDataHelper.getClients(sessionUser).getClientList())
    	{
    		//System.out.println(ClientDataHelper.getClient(sessionUser, result)); 
    		System.out.println("--> CLIENT " + client.getName());
    		for(ProjectDTO project : ProjectDataHelper.getProjects(sessionUser, client).getProjectList())
    		{
    			
    			System.out.println("----> PROJECT " + project.getName());
    			if (project.getParticipantList() == null)
    			{
    				System.out.println("--------> NO PARTICIPANTS ");
    			}
    			else
    			{
	    			for(ParticipantDTO participant : project.getParticipantList())
	    			{ 
	    				System.out.println("--------> PARTICIPANT " + participant.getCandidate().getIndividual().getGivenName() + " " + participant.getCandidate().getIndividual().getFamilyName());
	    			}
    			}
    		}
    	}
    	
    	UnitTestUtils.stop(this);
    }


    
	/*
	 * Tests the get of the single candidtate List Candidates
	 * 
	 */
	public void testGetClient()
		throws Exception
	{
		UnitTestUtils.start(this);

		System.out.println("-----------------  Testing getClient()  -----------------");
		SessionUserDTO sessionUser = new SessionUserDTO();
		sessionUser.setUsername(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.username")); 
		sessionUser.setPassword(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.password"));

		System.out.println("    -------------  Testing getClient() with ID  -------------");
		ClientDTO inp = new ClientDTO();
		inp.setId("OVNX-JPFM");
		ClientDTO out = ClientDataHelper.getClient(sessionUser, inp);
		System.out.println(out.toString());
		out = null;
		
		System.out.println("    -------------  Testing getClient() with URI  -------------");
		inp = new ClientDTO();
		inp.setUri("https://platform.iscopia.com/webservices1/rest/v1/client/OKLU-MJGG");
		out = ClientDataHelper.getClient(sessionUser, inp);
		System.out.println(out.toString());

		UnitTestUtils.stop(this);
	}
 
    
    public static void main(String[] args)
    throws Exception
    {
        junit.textui.TestRunner.run(ClientDataHelperUT.class);
    }
}