/**
 * Copyright (c) 2008 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v2.dto;


import java.util.ArrayList;

import com.pdi.data.v2.dataObjects.KeyValuePairStrings;

/**
 * ModConstsDTO is a wrapper around a number of data items that are needed when 
 * the ManualEntry Vendor App is started
 * 
 * @author      MB Panichi
 * @author		Ken Beukelman
 */
public class ModConstsDTO
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String moduleId;
	private String moduleName;
	private ArrayList<ConstructDTO> constructsAry;
	private Boolean hasScores;
	private String normId;
	private String genPopNormId;
	private ArrayList<KeyValuePairStrings> normList;
	private ArrayList<KeyValuePairStrings> normListGenPop;
	
	public static final String NOT_STARTED = "not_started";
	public static final String IN_PROGRESS = "in_progress";
	public static final String COMPLETED = "completed";

	private String status;

	//
	// Constructors.
	//
	public ModConstsDTO()
	{
		// Does nothing presently

	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
	

	//*****************************************************************************************/
	public String getModuleId()
	{
		return moduleId;
	}
	
	public void setModuleId(String value)
	{
		moduleId = value;
	}	

	//*****************************************************************************************/
	public String getModuleName()
	{
		return moduleName;
	}
	
	public void setModuleName(String value)
	{
		moduleName = value;
	}
	
	//*****************************************************************************************/
	public ArrayList<ConstructDTO> getConstructsAry()
	{
		if(constructsAry == null){
			constructsAry = new ArrayList<ConstructDTO>();
		}
		return constructsAry;
	}
	
	public void setConstructsAry(ArrayList<ConstructDTO> value)
	{
		constructsAry = value;
	}
	
	//*****************************************************************************************/
	public Boolean getHasScores()
	{
		return hasScores;
	}
	
	public void setHasScores(Boolean value)
	{
		hasScores = value;
	}	

	//*****************************************************************************************/
	public String getNormId()
	{
		return normId;
	}
	
	public void setNormId(String value)
	{
		normId = value;
	}

	//*****************************************************************************************/
	public String getGenPopNormId()
	{
		return genPopNormId;
	}
	
	public void setGenPopNormId(String value)
	{
		genPopNormId = value;
	}
	
	//*****************************************************************************************/
	public ArrayList<KeyValuePairStrings> getNormList()
	{
		if(normList == null){
			normList = new ArrayList<KeyValuePairStrings>();
		}
		return normList;
	}
	
	public void setNormList(ArrayList<KeyValuePairStrings> value)
	{
		normList = value;
	}
	
	//*****************************************************************************************/
	public ArrayList<KeyValuePairStrings> getNormListGenPop()
	{
		if(normListGenPop == null){
			normListGenPop = new ArrayList<KeyValuePairStrings>();
		}
		return normListGenPop;
	}
	
	public void setNormListGenPop(ArrayList<KeyValuePairStrings> value)
	{
		normListGenPop = value;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}	
}
