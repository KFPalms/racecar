/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.dto.setup;


/**
 * ReportNoteDTO contains the data required for a Report note.
 * 
 * @author		Ken Beukelman
 */
public class ReportNoteDTO
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private long _dnaId;
	private String _text = "";;

	//
	// Constructors.
	//
	public ReportNoteDTO()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public long getDnaId()
	{
		return _dnaId;
	}

	public void setDnaId(long value)
	{
		_dnaId = value;
	}

	/*****************************************************************************************/
	public String getText()
	{
		return _text;
	}

	public void setText(String value)
	{
		_text = value;
	}

	
	public String toString()
	{
		String str = "ReportNoteDTO:  DNA Id=" + _dnaId + "\n";
		str += "text=" +  _text + "\n";
		
		return str;
	}
}
