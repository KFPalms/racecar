/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.dto;

import org.w3c.dom.Node;

import com.pdi.data.util.BaseDTO;
import com.pdi.data.util.RequestStatus;
import com.pdi.xml.XMLUtils;

/**
 * DTO for scale scores
 * 
 *
 * @author		Gavin Myers
 */
public class ScaleScoreDTO extends BaseDTO
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _id;
	private String _value;
	
	//
	// Constructors.
	//

	/**
	 * "Standard" (no-parameter) constructor
	 */
	public ScaleScoreDTO()
	{
		// Does nothing presently
	}
	
	/**
	 * Builds the object based off an xml node
	 * @param node
	 */
	public ScaleScoreDTO(Node node)
	{
		/*
		 * The XML looks something like this:
		 * <?xml version="1.0" encoding="utf-8" standalone="yes"?>
		 * <ns2:constructListLine>
		 *   <constructName>F_Sociability</constructName>
		 *   <score>0.000000000000000</score>
		 * </ns2:constructListLine>
		 */
		//System.out.println(XMLUtils.getElementValue(node, "//constructName"));
		//System.out.println(XMLUtils.nodeToString(node));
		this.setId(XMLUtils.getElementValue(node, "constructName/."));
		this.setValue(XMLUtils.getElementValue(node, "score/."));
		
		if (this.getId() == null &&
			this.getValue() == null)
		{
			this.getStatus().setStatusCode(RequestStatus.WB_NO_DATA);
		}
	}

	//
	// Instance methods.
	//
	
	/**
	 * Generic toString method
	 */
	public String toString() {
		String str = "\n --- ScaleScoreDTO --- \n";
		str += "\nID:               " + _id;
		str += "\nValue:               " + _value;
		
		return str;
	}	

	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
	
	/*****************************************************************************************/
	public void setId(String value)
	{
		_id = value;
	}
	
	public String getId()
	{
		return _id;
	}
	
	/*****************************************************************************************/
	public void setValue(String value)
	{
		_value = value;
	}
	
	public String getValue()
	{
		return _value;
	}
}
