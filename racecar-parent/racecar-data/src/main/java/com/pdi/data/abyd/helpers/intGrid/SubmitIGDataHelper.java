/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.intGrid;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;


/**
 * SubmitIGDataHelper contains the code needed to submit the Integration Grid.
 *
 * @author		Ken Beukelman
 */
public class SubmitIGDataHelper
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private Connection con;
	private String participantId;
	private long dnaId;

	//
	//
	// Constructors.
	//
	public SubmitIGDataHelper(Connection con, String partId, long dnaId)
		throws Exception
	{
		if (con == null)
			throw new Exception("A vaild connection is required for the IG submit function.");
		
		// check for key data
		if (partId == null || partId.length() == 0  ||  dnaId == 0)
		{
			throw new Exception("Invalid IG Data key:  part=" +  (partId == null ? "null" : (partId.length() == 0 ? "<empty string>" : partId)) +
					            ", dna=" + dnaId);
		}

		this.con = con;
		this.participantId = partId;
		this.dnaId = dnaId;
	}


	//
	// Instance methods.
	//

	/**
	 * Set the submit flag
	 * This will be an update because data will have been written by the time we get here.
	 *
	 * @throws Exception
	 */
	public void submitIG()
		throws Exception
	{
		Statement stmt = null;
		StringBuffer sqlQuery = new StringBuffer();

		sqlQuery.append("UPDATE pdi_abd_igrid_response ");
		sqlQuery.append("  SET igSubmitted = 1, ");
		sqlQuery.append("      igLockDate  = GETDATE()," );
		sqlQuery.append("      lastUser    = SYSTEM_USER," );
		sqlQuery.append("      lastDate    = GETDATE()");
		sqlQuery.append("  WHERE participantId = '" + this.participantId + "' ");
		sqlQuery.append("    AND dnaId = " + this.dnaId);

		try
		{
	       	stmt = this.con.createStatement();
	       	int cnt = stmt.executeUpdate(sqlQuery.toString());
	       	
	       	if (cnt != 1)
	       	{
	       		throw new Exception("submitIG did not properly update (part=" + this.participantId +
	       							", dna=" + this.dnaId +
	       							", cnt=" + cnt);
	       	}
	
			return;
		}
		catch (SQLException ex)
		{
			// handle any errors
        	throw new Exception("SQL submitIG.  " +
        			"SQLException: " + ex.getMessage() + ", " +
        			"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (stmt != null)
			{
				try  {  stmt.close();  }
		    catch (SQLException sqlEx)
		    {
		    	System.out.println("ERROR: stmt close in submitIG - " + sqlEx.getMessage());
		    }
				stmt = null;
			}
		}
	}
}
