/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 *
 * $Header: /Platform/Java/sourceCode/com/pdicorp/app/tcm/export/vo/ExportDataVO.java 6     8/25/05 3:23p Kbeukelm $
 */

package com.pdi.data.abyd.dto.evalGuide;

/**
* EGBarScoreData is a thin class containing the information
* required to build a single competency in an Eval Guide.
* 
* @author		Ken Beukelman
*/
public class EGBarScoreData
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private int _barSeq;
	private int _barScore;
	
	//
	// Constructors.
	//
	public EGBarScoreData()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public int getBarSeq()
	{
		return _barSeq;
	}
		
	public void setBarSeq(int value)
	{
		_barSeq = value;
	}

	/*****************************************************************************************/
	public int getBarScore()
	{
		return _barScore;
	}
		
	public void setBarScore(int value)
	{
		_barScore = value;
	}
}
