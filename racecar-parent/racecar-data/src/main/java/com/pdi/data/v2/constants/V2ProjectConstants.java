/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v2.constants;

import java.util.HashMap;

import com.pdi.engagement.constants.ProjectConstants;

/**
 * Class that provides cross reference between V2 project
 * (engagement) IDs and the defined Project codes.
 * 
 * @author kbeukelm
 */
public class V2ProjectConstants
{
	// Cross reference table
	private static HashMap<String, String> ENG_TYPE = new HashMap<String, String>();
	static
	{
		// Note that this is the complete list of V2 job types.  Many are irrelevant
		// in current usage. (from picklists in Job Types - original order preserved)
		ENG_TYPE.put("26", ProjectConstants.EC_TYPE_ABYD_TESTING);				// AbyD Testing Components
		ENG_TYPE.put("25", ProjectConstants.EC_TYPE_ABYD_CONSULTANT);			// AbyD Consultant Components
		ENG_TYPE.put("2",  ProjectConstants.EC_TYPE_ASSESSMENT);					// Assessment
		ENG_TYPE.put("3",  ProjectConstants.EC_TYPE_LEADERSHIP_PROFILE);			// Leadership Profile
		ENG_TYPE.put("32", ProjectConstants.EC_TYPE_LEADERSHIP_PROFILE_SHORT);	// Leadership Profile - Short
		ENG_TYPE.put("24", ProjectConstants.EC_TYPE_ASSESSMENT_REPORTING);		// Assessments Reporting
		ENG_TYPE.put("23", ProjectConstants.EC_TYPE_TLT);						// TalentView of Transitions (TPOT2)
		ENG_TYPE.put("21", ProjectConstants.EC_TYPE_TLR);						// TalentView of Readiness (TVOR)
		ENG_TYPE.put("22", ProjectConstants.EC_TYPE_TLR_MS);						// TalentView of Readiness - Microsoft BUL (TVOR-BUL-MS)
		ENG_TYPE.put("20", ProjectConstants.EC_TYPE_TPOT);						// TPOT-OLD
		ENG_TYPE.put("30", ProjectConstants.EC_TYPE_TCM);						// TCM
		ENG_TYPE.put("31", ProjectConstants.EC_TYPE_JAV);						// TCM JAV
		ENG_TYPE.put("40", ProjectConstants.EC_TYPE_BUL_ASMT);					// BUL Assessment
		ENG_TYPE.put("50", ProjectConstants.EC_TYPE_SAC_PRE);					// SAC Pre-work
		ENG_TYPE.put("51", ProjectConstants.EC_TYPE_SAC_PROC);					// SAC Proctored 
	}
	
	
	/*
	 * Returns the instrument code associated with the instrument id passed in
	 */
	public static String getProjectTypeCode(String typeId)
	{
		String str = ENG_TYPE.get(typeId);
		if (str == null)
		{
			str = "";
			//System.out.println("No engagement type code found for V2 type " + typeId);
		}
		
		return str;
	}
}
