/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v2.constants;

import java.util.HashMap;

import com.pdi.instrument.constants.InstrumentConstants;

/**
 * Class that provides cross reference between V2 module
 * (instrument) IDs and the defined Instrument codes.
 * 
 * @author kbeukelm
 */
public class V2InstrumentConstants
{
	// Cross reference table
	private static HashMap<String, String> INST = new HashMap<String, String>();
	static
	{
		// GPI
		INST.put("BMMLZKOK", InstrumentConstants.IC_GPI);	// Global Personality Inventory (GPI) - Spanish
		INST.put("FDHIRHYW", InstrumentConstants.IC_GPI);	// Global Personality Inventory (GPI) UK English
		INST.put("FFWDUFWY", InstrumentConstants.IC_GPI);	// Global Personality Inventory (GPI) - French
		INST.put("HOGEZINK", InstrumentConstants.IC_GPI);	// Global Personality Inventory (GPI) - German
		INST.put("IXLGVIOQ", InstrumentConstants.IC_GPI);	// Global Personality Inventory (GPI)
		INST.put("LKXOOMME", InstrumentConstants.IC_GPI);	// Global Personality Inventory (GPI) - Chinese

		// LEI
		INST.put("BHJAYRLB", InstrumentConstants.IC_LEI);	// Leadership Experience Inventory (LEI) - German
		INST.put("BMZLGGZL", InstrumentConstants.IC_LEI);	// Leadership Experience Inventory (LEI)
		INST.put("CNBPPCGQ", InstrumentConstants.IC_LEI);	// Leadership Experience Inventory (LEI) - Spanish
		INST.put("DUJKAXPI", InstrumentConstants.IC_LEI);	// Leadership Experience Inventory (LEI) - French
		INST.put("KCIYQGYX", InstrumentConstants.IC_LEI);	// Leadership Experience Inventory (LEI) - Chinese

		// Ravens (Short Form)
		INST.put("DWWJAAHE", InstrumentConstants.IC_RAVENS_SF);	// Raven's APM Short Form - Spanish
		INST.put("DYMYDGHI", InstrumentConstants.IC_RAVENS_SF);	// Raven's APM Short Form - Chinese
		INST.put("FDZAJOAY", InstrumentConstants.IC_RAVENS_SF);	// Raven's APM Short Form - German
		INST.put("ITVTIIPB", InstrumentConstants.IC_RAVENS_SF);	// Raven's APM Short Form - French
		INST.put("IYJOHVLX", InstrumentConstants.IC_RAVENS_SF);	// Raven's APM Short Form

		// Ravens Timed Long form no longer used
		
		// Ravens (Form B)
		INST.put("HNZYMHDV", InstrumentConstants.IC_RAVENS_B);	// Raven's APM Form B

		// Career Survey
		INST.put("BKUEDDNI", InstrumentConstants.IC_CAREER_SURVEY);	// Career Survey - French
		INST.put("CPXPHWRY", InstrumentConstants.IC_CAREER_SURVEY);	// Career Survey - German
		INST.put("FFGLQDEG", InstrumentConstants.IC_CAREER_SURVEY);	// Career Survey
		INST.put("FGGQYSFG", InstrumentConstants.IC_CAREER_SURVEY);	// Career Survey - Spanish
		INST.put("ISWEGTQC", InstrumentConstants.IC_CAREER_SURVEY);	// Career Survey - Chinese
		INST.put("IVUVHYKM", InstrumentConstants.IC_CAREER_SURVEY);	// Career Survey
		
		//Wesman
		INST.put("LGHSTSHU", InstrumentConstants.IC_WESMAN);	// Wesman - Personnel Classification Test - Alexander G. Wesman
		
		// Watson-Glaser A is no longer used
		// Watson-Glaser C was never fully implemented
		
		// Watson-Glaser (Form E).  Also called the Watson-Glaser II
		INST.put("FFJUUSBA", InstrumentConstants.IC_WG_E);	// Watson-Glaser II - Relativity
		INST.put("LLQIPSHN", InstrumentConstants.IC_WG_E);	// WG II/WG A - AbyD
		
		// Financial Exercise
		INST.put("FAGNDFAC", InstrumentConstants.IC_FIN_EX);	// Financial Exercise
		
		// CHQ
		INST.put("FEXLEUFE", InstrumentConstants.IC_CHQ);	// Career History and Interest Questionnaire (CHQ)
		INST.put("DYEDZSOS", InstrumentConstants.IC_CHQ);	// Career History and Interest Questionnaire (CHQ) - German
		INST.put("CNBLECCN", InstrumentConstants.IC_CHQ);	// CHQ - TLT - English
		INST.put("DWXITAYZ", InstrumentConstants.IC_CHQ);	// CHQ - TLT - Spanish
		INST.put("HNYZARXC", InstrumentConstants.IC_CHQ);	// CHQ - TLT - Chinese
		INST.put("IYBRXLCI", InstrumentConstants.IC_CHQ);	// CHQ - TLT - German
		INST.put("COEZVIOS", InstrumentConstants.IC_CHQ);	// CHQ - TLT - French
		INST.put("LIBPMLBJ", InstrumentConstants.IC_CHQ);	// CHQ - ADAPT - AbyD
	}
	
	
	/*
	 * Returns the instrument code associated with the instrument id passed in
	 */
	public static String getInstrumentCode(String instrumentId)
	{
		String str = INST.get(instrumentId);
		if (str == null)
		{
			str = "";
			//System.out.println("No instrument code found for V2 ID " + instrumentId);
		}
		
		return str;
	}
}
