/**
 * Copyright (c) 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v2.helpers.delegated;

import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Comparator;

import com.pdi.data.dto.ProjectParticipantInstrument;
import com.pdi.data.dto.ProjectParticipantReport;
//import com.pdi.data.dto.Report;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.IProjectParticipantInstrumentHelper;
import com.pdi.data.helpers.interfaces.IProjectParticipantReportHelper;
//import com.pdi.project.constants.ProjectConstants;
//import com.pdi.data.v2.dto.SessionUserDTO;
//import com.pdi.data.v2.util.V2WebserviceUtils;

import com.pdi.logging.LogWriter;
import com.pdi.properties.PropertyLoader;
//import com.pdi.reporting.constants.ReportingConstants;

public class ProjectParticipantReportHelper implements IProjectParticipantReportHelper
{

	/**
	 * Get reports by a participant/project link
	 * use case: I've selected a project/participant and I want to see instruments from that link
	 * @param SessionUser session
	 * @param String projectId
	 * @param String participantId
	 * @return ArrayList<ProjectParticipantReport>
	 */
	//@Override
	public ArrayList<ProjectParticipantReport> fromProjectIdParticipantId(SessionUser session,
			                                                              String projectId,
			                                                              String participantId)
	{
		ArrayList<ProjectParticipantReport> projectParticipantReportList = new ArrayList<ProjectParticipantReport>();
		
		try
		{	
			//String username = session.getMetadata().get("USERNAME");
			//String password = session.getMetadata().get("PASSWORD");
			//V2WebserviceUtils v2web = new V2WebserviceUtils();
			//SessionUserDTO sessionUser = v2web.registerUser(username, password);
					
			// get the  project/participant objects for this project.... 
			IProjectParticipantInstrumentHelper iEPI = HelperDelegate.getProjectParticipantInstrumentHelper(PropertyLoader.getProperty("com.pdi.data.v2.application", "datasource.v2"));
			ArrayList<ProjectParticipantInstrument> engagmentParticipantInstrumentList = iEPI.fromProjectIdParticipantId(session, IProjectParticipantInstrumentHelper.MODE_DEFAULT, projectId, participantId);
			
			boolean gotTypeReports = false;
			for(ProjectParticipantInstrument epi : engagmentParticipantInstrumentList)
			{
				// put out the reports needed for this type, but only once
				if (! gotTypeReports)
				{
					//ProjectParticipantReport epr;
					//Report rpt;
					
	 				// look at PROJECT TYPE -- ACTUALLY PRODUCT TYPE IN THE NEWSPEAK.  
					//  product code is the project type
					
					/*
					if(epi.getProject().getProduct().getCode() == ProjectConstants.EC_PROD_TLT)
					{
						// Build the two mandatory TLT reports
						// First the individual summary report...
						epr = new ProjectParticipantReport();
						epr.setProject(epi.getProject());
						epr.setParticipant(epi.getParticipant());
						rpt = new Report(ReportingConstants.REPORT_CODE_TLT_INDIVIDUAL_SUMMARY);
						epr.setReport(rpt);
						projectParticipantReportList.add(epr);
						// ...then the group report
						epr = new ProjectParticipantReport();
						epr.setProject(epi.getProject());
						epr.setParticipant(epi.getParticipant());
						rpt = new Report(ReportingConstants.REPORT_CODE_TLT_GROUP_DETAIL);
						epr.setReport(rpt);
						projectParticipantReportList.add(epr);
					}
					else if(epi.getProject().getProduct().getCode() == ProjectConstants.EC_PROD_A_BY_D )
					{
						// Build the three mandatory A by D reports
						// Development Report...
						epr = new ProjectParticipantReport();
						epr.setProject(epi.getProject());
						epr.setParticipant(epi.getParticipant());
						rpt = new Report(ReportingConstants.REPORT_CODE_ABYD_DEVELOPMENT_REPORT);
						epr.setReport(rpt);
						projectParticipantReportList.add(epr);
						// ...Fit Report...
						epr = new ProjectParticipantReport();
						epr.setProject(epi.getProject());
						epr.setParticipant(epi.getParticipant());
						rpt = new Report(ReportingConstants.REPORT_CODE_ABYD_FIT_REPORT);
						epr.setReport(rpt);
						projectParticipantReportList.add(epr);
						// ...and Readiness report
						epr = new ProjectParticipantReport();
						epr.setProject(epi.getProject());
						epr.setParticipant(epi.getParticipant());
						rpt = new Report(ReportingConstants.REPORT_CODE_ABYD_READINESS_REPORT);
						epr.setReport(rpt);
						projectParticipantReportList.add(epr);
					}
					*/

					gotTypeReports = true;
				}
				
				// Now build the instrument report (if it is needed)
				// Build the EPR record on spec (discarded if the report isn't needed)
				ProjectParticipantReport ePReport = new ProjectParticipantReport();
				ePReport.setProject(epi.getProject());
				ePReport.setParticipant(epi.getParticipant());

	// Temporarily removed because the Report object no longer exists
//				// See if this instrument needs a report built
//				if(epi.getInstrument().getCode().equals(ReportingConstants.RC_GPI) )
//				{
//					Report report = new Report(ReportingConstants.REPORT_CODE_GPI_GRAPHICAL);
//					ePReport.setReport(report);
//					projectParticipantReportList.add(ePReport);
//				}
//				else if(epi.getInstrument().getCode().equals(ReportingConstants.RC_LEI))
//				{
//					Report report = new Report(ReportingConstants.REPORT_CODE_LEI_GRAPHICAL);
//					ePReport.setReport(report);
//					projectParticipantReportList.add(ePReport);
//				}
//				else if(epi.getInstrument().getCode().equals(ReportingConstants.RC_RAVENS_B))
//				{
//					Report report = new Report(ReportingConstants.REPORT_CODE_RAV_B_GRAPHICAL);
//					ePReport.setReport(report);
//					projectParticipantReportList.add(ePReport);
//				}
//				else if(epi.getInstrument().getCode().equals(ReportingConstants.RC_CHQ))
//				{
//					Report report = new Report(ReportingConstants.REPORT_CODE_CHQ_DETAIL);
//					ePReport.setReport(report);
//					projectParticipantReportList.add(ePReport);
//				}
//				else if(epi.getInstrument().getCode().equals(ReportingConstants.RC_WG_E))
//				{
//					Report report = new Report(ReportingConstants.REPORT_CODE_WGE_GRAPHICAL);
//					ePReport.setReport(report);
//					projectParticipantReportList.add(ePReport);
//				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			LogWriter.logBasic(LogWriter.ERROR, this,
					"Error in fromProjectIdParticipantId." +
					"  ProjId=" + projectId +
					", partId=" + participantId);
		}
		
		return projectParticipantReportList;
	}

	/**
	 * Get a list of the reports to be generated from an input list of ProjectParticipant
	 * objects.  The output will be a List of ProjectParticipantReport objects sorted by
	 * report name and participant name.
	 * 
	 * @param session
	 * @param epList
	 * @return ArrayList<ProjectParticipantReport>
	 */
//	//@Override
//	public ArrayList<ProjectParticipantReport> fromProjectParticipantList(SessionUser session, ArrayList<ProjectParticipant> epList)
//	{
//		ArrayList<ProjectParticipantReport> fullList = new ArrayList<ProjectParticipantReport>();
//		
//		// Loop through the incoming ArrayList and generate the list of reports (addAll)
//		for(ProjectParticipant ep : epList)
//		{
//			ArrayList<ProjectParticipantReport> res = fromProjectIdParticipantId(session,
//					                                                                   ep.getProject().getId(),
//					                                                                   ep.getParticipant().getId());
//			
//			fullList.addAll(res);
//		}
//
//		
//		// sort the resultant list by report,participant, project
//		Collections.sort(fullList, new EprComparator());
//		
//		return fullList;
//	}


//	// Comparator to sort ProjectParticipantReport Objects
//	// by ReportName/Participant name (last,first)/project name
//	private class EprComparator implements Comparator<ProjectParticipantReport>
//	{
//		public int compare(ProjectParticipantReport epr1, ProjectParticipantReport epr2)
//		{
//			int ret;
//			
//			// compare the Report.  Use code for now as it is all that is in Report.
//			ret = epr1.getReport().getCode().compareToIgnoreCase(epr2.getReport().getCode());
//			if (ret != 0)
//				return ret;
//			
//			// Report name is tied...  Try last name.
//			ret = epr1.getParticipant().getLastName().compareToIgnoreCase(epr2.getParticipant().getLastName());
//			if (ret != 0)
//				return ret;
//			
//			// Last name is tied... Try first name
//			ret = epr1.getParticipant().getFirstName().compareToIgnoreCase(epr2.getParticipant().getFirstName());
//			if (ret != 0)
//				return ret;
//			
//			// First name is tied... Try project name
//			return epr1.getProject().getName().compareToIgnoreCase(epr2.getProject().getName());
//		
//		}
//	}
	
	
//	/**
//	 * Get all reports that this participant has ever done
//	 * usecase: I need to know all instruments this participant has compelted
//	 * @param session
//	 * @param participantId
//	 * @return
//	 */
//	//@Override
//	public ArrayList<ProjectParticipantReport> fromParticipantId(SessionUser session, String participantId){
//		try{
//			
//			String username = session.getMetadata().get("USERNAME");
//			String password = session.getMetadata().get("PASSWORD");
//			V2WebserviceUtils v2web = new V2WebserviceUtils();
//			SessionUserDTO sessionUser = v2web.registerUser(username, password);
//		} catch(Exception e) {
//			e.printStackTrace();
//		}
//		
//		return null;
//	}
//	
//	/**
//	 * Get all reports that this group of participants has ever done
//	 * use case: I need to know all instruments these participants have completed
//	 * 
//	 * @param SessionUser session 
//	 * @param ArrayList<String> participantIds
//	 * 
//	 * @return ArrayList<ProjectParticipantReport>
//	 */
//	//@Override
//	public ArrayList<ProjectParticipantReport> fromParticipantList(SessionUser session, ArrayList<String> participantIds){
//		try{
//			
//			String username = session.getMetadata().get("USERNAME");
//			String password = session.getMetadata().get("PASSWORD");
//			V2WebserviceUtils v2web = new V2WebserviceUtils();
//			SessionUserDTO sessionUser = v2web.registerUser(username, password);
//			
//			
//		} catch(Exception e) {
//			e.printStackTrace();
//		}
//		
//		return null;
//	}
//	
//	
//	/**
//	 * Get all reports availabe from this project has ever done
//	 * usecase: I need to know all instruments this participant has compelted
//	 * @param session
//	 * @param participantId
//	 * @return
//	 */
//	//@Override
//	public ArrayList<ProjectParticipantReport> fromProjectId(SessionUser session, String projectId){
//		try{
//			
//			String username = session.getMetadata().get("USERNAME");
//			String password = session.getMetadata().get("PASSWORD");
//			V2WebserviceUtils v2web = new V2WebserviceUtils();
//			SessionUserDTO sessionUser = v2web.registerUser(username, password);
//		} catch(Exception e) {
//			e.printStackTrace();
//		}
//		
//		return null;
//	}
//
//	/**
//	 * Get all reports available from this project List
//	 * use case: I need to see all the reports avaiable for each participant in each project
//	 * @param SessionUser session
//	 * @param ArrayList<String> projectIds
//	 * @return ArrayList<ProjectParticipantReport>
//	 */
//	//@Override
//	public ArrayList<ProjectParticipantReport> fromProjectList(SessionUser session, ArrayList<String> projectIds){
//		
//		try{
//			
//			String username = session.getMetadata().get("USERNAME");
//			String password = session.getMetadata().get("PASSWORD");
//			V2WebserviceUtils v2web = new V2WebserviceUtils();
//			SessionUserDTO sessionUser = v2web.registerUser(username, password);
//			
//
//		} catch(Exception e) {
//			e.printStackTrace();
//		}
//		
//		return null;
//	}

}
