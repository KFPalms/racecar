package com.pdi.data.abyd.helpers.intGrid.ut;

import java.sql.Connection;
import java.util.ArrayList;

import com.pdi.data.abyd.dto.common.DNACellDTO;
import com.pdi.data.abyd.helpers.intGrid.IGTestingIntersectionDataHelper;
import com.pdi.ut.UnitTestUtils;
//import com.pdi.data.abyd.util.AbyDDatabaseUtils;

import junit.framework.TestCase;

public class IGTestingIntersectionDataHelperUT  extends TestCase
{
	//
	// Constructors
	//
	public IGTestingIntersectionDataHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
	throws Exception
	{
		junit.textui.TestRunner.run(IGTestingIntersectionDataHelperUT.class);
	}


	/*
	 * testGetTestingData
	 */
	public void testGetTestingDataTrue()
		throws Exception
	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//		
//		
//		Connection con = AbyDDatabaseUtils.getDBConnection();
//		
//		// Dev
//		//String partId = "KEYVOFZF";
//		//long dnaId = 41;
//		//boolean cogsOn = true;
//		
//		// Cert
//		String partId = "FFTAEVVN";
//		long dnaId = 201;
//		boolean cogsOn = true;
//
//		// get the data - NOTE:  Assumes regualr test data... NOT ALP!
//		IGTestingIntersectionDataHelper helper = new IGTestingIntersectionDataHelper(con, partId, dnaId, cogsOn, false);        
//		ArrayList<DNACellDTO> ret = helper.getTestingData();
//		
//		System.out.println("Intersection Array:");
//		for (DNACellDTO cell : ret)
//		{
//			System.out.print(cell.toString());
//		}
//
//		UnitTestUtils.stop(this);
	}


	/*
	 * testGetTestingData
	 */
	public void testGetTestingDataFalse()
		throws Exception
	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//		
//		
//		Connection con = AbyDDatabaseUtils.getDBConnection();
//		
//		// Dev
//		//String partId = "KEYVOFZF";
//		//long dnaId = 41;
//		//boolean cogsOn = true;
//		
//		// Cert
//		String partId = "FFTAEVVN";
//		long dnaId = 201;
//		boolean cogsOn = true;
//
//		// get the data - NOTE:  Assumes regular test data... NOT ALP!
//		IGTestingIntersectionDataHelper helper = new IGTestingIntersectionDataHelper(con, partId, dnaId, cogsOn, false);        
//		ArrayList<DNACellDTO> ret = helper.getTestingData();
//		
//		System.out.println("Intersection Array:");
//		for (DNACellDTO cell : ret)
//		{
//			System.out.print(cell.toString());
//		}
//
//		UnitTestUtils.stop(this);
	}

}
