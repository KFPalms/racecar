/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.dto.intGrid;


/**
 * IGExtEGCompData is a container for Model/Module/Completion Date data used in the IG Extract.
 * 
 * @author		Ken Beukelman
 */
public class IGExtEGCompletionData 
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _modelName;
	private String _moduleName;
	private String _dateString;

	//
	// Constructors.
	//
	public IGExtEGCompletionData()
	{
		// default the date string to an empty string (rather than a null)
		_dateString = "";
		
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public String getModelName()
	{
		return _modelName;
	}
	
	public void setModelName(String value)
	{
		_modelName = value;
	}	

	/*****************************************************************************************/
	public String getModuleName()
	{
		return _moduleName;
	}
	
	public void setModuleName(String value)
	{
		_moduleName = value;
	}	

	/*****************************************************************************************/
	public String getDateString()
	{
		return _dateString;
	}
	
	public void setDateString(String value)
	{
		_dateString = value;
	}	



	/*
	 * Show me what you've got
	 */
	public String toString()
	{
		String ret = "IGExtEGCompletionData content (Java)\n";
		ret += "       Model Name=" + _modelName + "\n";
		ret += "      Module Name=" + _moduleName + "\n";
		ret += "  Completion Date=" + _dateString + "\n";
		
		return ret;
	}
}
