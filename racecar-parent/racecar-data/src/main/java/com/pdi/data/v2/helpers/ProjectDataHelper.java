/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v2.helpers;

import java.util.ArrayList;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.pdi.data.v2.dto.ClientDTO;
import com.pdi.data.v2.dto.ProjectDTO;
import com.pdi.data.v2.dto.SessionUserDTO;
import com.pdi.data.v2.util.V2WebserviceUtils;
/**
 * Get all clients that a particular user has access to
 * Test status on client by client basis
 * @param sessionUser
 * @return
 */
public class ProjectDataHelper
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	
	//
	// Constructors.
	//

	//
	// Instance methods.
	//

	/**
	 * Get project/user information - Gets a combination of state info
	 * for this participant in this project.  Information returned 
	 * includes the following:
	 * 		- Business Unit --> NO, do this when you get the participant object
	 * 		- Organization
	 * 		- Transition level
	 * 		- TPOT current level
	 * 		- TPOT target level
	 * @param cand id
	 * @param job id
	 * @return
	 * @throws Exception
	 */
	public  void getProjectInfo(String jobId)
		//throws Exception
	{
			// get the data (most in XML
			// parse it out
			// dump it (Replaced by a data object later)
	}

	/**
	 * Get projects by user/client
	 * @param sessionUser
	 * @param client
	 * @return
	 * @throws Exception
	 */
	public  ArrayList<ProjectDTO> getProjects(SessionUserDTO sessionUser, ClientDTO client)
		throws Exception
	{
		//and j.JobType in ('23','26','25')  
		ArrayList<ProjectDTO> projects = new ArrayList<ProjectDTO>();
		String xml = "";
		xml += "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
		xml += "		<ETR SessionId=\"${sessionId}\">";
		xml += "			<Rqt Cmd=\"OpenSQL\" AN=\"JobListSQL\" FA=\"Y\" FR=\"1\" RC=\"N\" SQ=\"select j.ClientID, j.UniqueIdStamp, (select count(*) from candidates c inner join cand_jobs cj on c.uniqueidstamp = cj.candidateid where cj.jobid = j.UniqueIdStamp and cj.archived=0 and c.archived=0) as ParticipantCount, j.Title, c.Name, j.DateCreatedStamp, j.xml from Clients c, Jobs j where c.UniqueIdStamp = j.ClientID and j.ClientID = '${clientId}' order by j.Title\" />";		
		xml += "			<Rqt Cmd=\"Read\" AN=\"JobListSQL\" Sub=\"JobList\" Rpt=\"*\" RQ=\"N\">";
		xml += "				<Fld FN=\"UniqueIdStamp\" Fmt=\"A\" />";
		xml += "				<Fld FN=\"ClientID\" Fmt=\"A\" />";
		xml += "				<Fld FN=\"ParticipantCount\" Fmt=\"A\" />";
		xml += "				<Fld FN=\"Title\" Fmt=\"A\" />";
		xml += "				<Fld FN=\"Name\" Fmt=\"A\" />";
		xml += "				<Fld FN=\"xml_TPOTTransLevel\" Fmt=\"A\" />";
		xml += "				<Fld FN=\"xml_TPOTTransLevelCurrent\" Fmt=\"A\" />";
		xml += "				<Fld FN=\"xml_TPOTTransLevelTarget\" Fmt=\"A\" />";
		xml += "			</Rqt>";
		xml += "			<Rqt Cmd=\"Close\" AN=\"JobListSQL\" />";
		xml += "		</ETR>";
		
		xml = xml.replace("${sessionId}", sessionUser.getSessionId());
		xml = xml.replace("${clientId}", client.getId());
		
		XPath xpath = XPathFactory.newInstance().newXPath(); 
		
		V2WebserviceUtils v2web = new V2WebserviceUtils();
		Document doc = v2web.sendData(xml);
		
		NodeList records = (NodeList) xpath.evaluate("//Record", doc, XPathConstants.NODESET); 
		
		for(int i = 0; i < records.getLength(); i++)
		{
			Node record = records.item(i);
			projects.add(new ProjectDTO(record));
		}
		
		return projects;
	}
	
	
	/**
	 * Get project by user/projectId
	 * @param sessionUser
	 * @param projId
	 * @return
	 * @throws Exception
	 */
	public  ProjectDTO getProject(SessionUserDTO sessionUser, String projId)
		throws Exception
	{
		ProjectDTO project = new ProjectDTO();
		String xml = "";
		xml += "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
		xml += "		<ETR SessionId=\"${sessionId}\">";
		xml += "			<Rqt Cmd=\"OpenSQL\" AN=\"JobListSQL\" FA=\"Y\" FR=\"1\" RC=\"N\" SQ=\"select j.ClientID, j.UniqueIdStamp, j.Title, c.Name, j.xml, j.DateCreatedStamp, j.JobType from Clients c, Jobs j where c.UniqueIdStamp = j.ClientID and j.uniqueIdStamp = '${projId}' order by j.Title\" />";
		xml += "			<Rqt Cmd=\"Read\" AN=\"JobListSQL\" Sub=\"JobList\" Rpt=\"*\" RQ=\"N\">";
		xml += "				<Fld FN=\"UniqueIdStamp\" Fmt=\"A\" />";
		xml += "				<Fld FN=\"ClientID\" Fmt=\"A\" />";
		xml += "				<Fld FN=\"Title\" Fmt=\"A\" />";
		xml += "				<Fld FN=\"Name\" Fmt=\"A\" />";
		xml += "				<Fld FN=\"JobType\" Fmt=\"A\" />";
		xml += "				<Fld FN=\"xml_TPOTTransLevel\" Fmt=\"A\" />";
		xml += "				<Fld FN=\"xml_TPOTTransLevelCurrent\" Fmt=\"A\" />";
		xml += "				<Fld FN=\"xml_TPOTTransLevelTarget\" Fmt=\"A\" />";
		xml += "				<Fld FN=\"xml_TPOTCogsInclded\" Fmt=\"A\" />";
		// abyd
		xml += "				<Fld FN=\"xml_chooseCogs\" Fmt=\"A\" />";
		xml += "				<Fld FN=\"xml_CareerSurvey\" Fmt=\"A\" />";
		
		xml += "			</Rqt>";
		xml += "			<Rqt Cmd=\"Close\" AN=\"JobListSQL\" />";
		xml += "		</ETR>";
		
		xml = xml.replace("${sessionId}", sessionUser.getSessionId());
		xml = xml.replace("${projId}", projId);
		
		XPath xpath = XPathFactory.newInstance().newXPath(); 
		
		V2WebserviceUtils v2web = new V2WebserviceUtils();
		Document doc = v2web.sendData(xml);
		
		NodeList records = (NodeList) xpath.evaluate("//Record", doc, XPathConstants.NODESET); 
		
		for(int i = 0; i < records.getLength(); i++)
		{
			Node record = records.item(i);
			project = new ProjectDTO(record);
		}
		
		return project;
	}
}
