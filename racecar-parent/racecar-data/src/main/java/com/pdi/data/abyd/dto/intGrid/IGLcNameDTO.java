/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */

package com.pdi.data.abyd.dto.intGrid;


/**
* IGLcNameDTO is a helper class to allow us to pass only the information required
* to save the lead consultant name for an Integration grid.
* 
* @author		Ken Beukelman
*/
public class IGLcNameDTO
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _partId;
	private long _dnaId;
	private String _lcFirstName;
	private String _lcLastName;

	//
	// Constructors.
	//
	public IGLcNameDTO()
	{
		// does nothing at present
	}

	//
	// Instance methods.
	//

	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public String getPartId()
	{
		return _partId;
	}

	public void setPartId(String value)
	{
		_partId = value;
	}	

	/*****************************************************************************************/
	public long getDnaId()
	{
		return _dnaId;
	}

	public void setDnaId(long value)
	{
		_dnaId = value;
	}	

	/*****************************************************************************************/
	public String getLcFirstName()
	{
		return _lcFirstName;
	}

	public void setLcFirstName(String value)
	{
		_lcFirstName = value;
	}	

	/*****************************************************************************************/
	public String getLcLastName()
	{
		return _lcLastName;
	}

	public void setLcLastName(String value)
	{
		_lcLastName = value;
	}	
}
