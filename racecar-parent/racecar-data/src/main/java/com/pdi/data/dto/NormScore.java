package com.pdi.data.dto;

import com.pdi.scoring.NormUtils;
import com.pdi.scoring.ScoreUtils;

public class NormScore  extends CoreDataObject{

	public NormScore() {
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 6670911659682117806L;
	private Norm norm;
	private Score score;
	
	//TODO: preprocess this value
	//Before we do this we should make sure we aren't going to encounter threading issues
	//for example, new norms are applied, what should happen? I would imagine
	//this object would be destroyed and rebuilt, but that needs to be validated
	//private int percentile;
	//private Double z;
	
	public String toString() {
		return "Z = " + this.toZ() + " | Rating = " + this.toRating() + " | Percentile = " + this.toPercentile() + " | Stanine = " + this.toStanine();
	}
	
	public Double toZ() {
		//
		//return (score - mean) / standardDeviation;

		return ((score.scoreAsNumber() - norm.getMean()) / norm.getStandardDeviation());
	}
	
	public Double toRating() {
		return ScoreUtils.getPDIRating(NormUtils.getPercentileInt(this.toZ()));
	}

	public int toPercentile() {
		//do the calc

		return NormUtils.getPercentileInt(this.toZ());
	}

	public int toStanine() {
		return ScoreUtils.getStanine(NormUtils.getPercentileInt(this.toZ()));
	}

	public void setNorm(Norm norm) {
		this.norm = norm;
	}

	public Norm getNorm() {
		return norm;
	}

	public void setScore(Score score) {
		this.score = score;
	}

	public Score getScore() {
		return score;
	}

}
