/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v2.dto;

import org.w3c.dom.Node;

import com.pdi.xml.XMLUtils;

/**
 * ParticipantDTO is a wrapper around a number of data items
 * that convey information about a participant. 
 */
public class ParticipantDTO
{
	
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//	
	private String id;
	private String firstName;
	private String lastName;
	private String email;
	private String jobTitle;
	private String businessUnit;
	
	//
	// Constructors.
	//
	
	/**
	 * "Standard" (no-parameter) constructor
	 */
	public ParticipantDTO()
	{
		super();
	}
	
	/**
	 * Constructor to build a ParticipantDTO object from an XML document
	 */
	public ParticipantDTO(Node node) {
		//<Record CandidateID="DXRAOEXL" FirstName="Theresa" LastName="Test Nov 21" Email="tmartins@personneldecisions.com"></Record>

		this.setId(XMLUtils.getAttributeValue(node, "UniqueIdStamp"));
		//this.setFirstName(XMLUtils.xmlFormatString(XMLUtils.getAttributeValue(node, "FirstName")));
		//this.setLastName(XMLUtils.xmlFormatString(XMLUtils.getAttributeValue(node, "LastName")));
		//this.setEmail(XMLUtils.xmlFormatString(XMLUtils.getAttributeValue(node, "Email")));
		//// looks like these are backwards... fixed 5/9/10
		////this.setBusinessUnit(XMLUtils.xmlFormatString(XMLUtils.getAttributeValue(node, "xml_JobTitle")));
		////this.setJobTitle(XMLUtils.xmlFormatString(XMLUtils.getAttributeValue(node, "xml_BU")));
		//this.setBusinessUnit(XMLUtils.xmlFormatString(XMLUtils.getAttributeValue(node, "xml_BU")));
		//this.setJobTitle(XMLUtils.xmlFormatString(XMLUtils.getAttributeValue(node, "xml_JobTitle")));
		this.setFirstName(XMLUtils.getAttributeValue(node, "FirstName"));
		this.setLastName(XMLUtils.getAttributeValue(node, "LastName"));
		this.setEmail(XMLUtils.getAttributeValue(node, "Email"));
		this.setBusinessUnit(XMLUtils.getAttributeValue(node, "xml_BU"));
		this.setJobTitle(XMLUtils.getAttributeValue(node, "xml_JobTitle"));
		
		
		//System.out.println("XMLUtils.xmlFormatString(XMLUtils.getAttributeValue(node, FirstName) " + XMLUtils.xmlFormatString(XMLUtils.getAttributeValue(node, "FirstName") ));
		
		
	}

	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}
	/*****************************************************************************************/
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFirstName() {
		return firstName;
	}
	/*****************************************************************************************/
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLastName() {
		return lastName;
	}
	/*****************************************************************************************/
	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail() {
		return email;
	}
	/*****************************************************************************************/	
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getJobTitle() {
		return jobTitle;
	}
	/*****************************************************************************************/	
	public void setBusinessUnit(String businessUnit) {
		this.businessUnit = businessUnit;
	}

	public String getBusinessUnit() {
		return businessUnit;
	}
}
