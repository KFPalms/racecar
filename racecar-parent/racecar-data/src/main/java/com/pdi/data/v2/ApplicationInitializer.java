package com.pdi.data.v2;

import com.pdi.data.v2.helpers.ParticipantDataHelper;
//import com.pdi.data.v2.helpers.delegated.TextHelper;
import com.pdi.data.v2.util.V2DatabaseUtils;

public class ApplicationInitializer {
	private static ApplicationData application;
	
	public ApplicationInitializer() {

		System.out.println("************ V2 DATABASE ************");
		try {
			//Hibernate
			V2DatabaseUtils.registerPersistence();
		} catch(Exception e) {
			e.printStackTrace();
		}

		try {
			//JDBC
			V2DatabaseUtils.registerConnection();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		new ParticipantDataHelper().getAllNormData();
		new ParticipantDataHelper().getNormXrefData();
		////new TextHelper().initializeCache();
	}

	public static void setApplication(ApplicationData application) {
		ApplicationInitializer.application = application;
	}

	public static ApplicationData getApplication() {
		return application;
	}
}
