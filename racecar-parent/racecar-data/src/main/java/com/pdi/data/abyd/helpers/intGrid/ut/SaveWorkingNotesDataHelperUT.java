package com.pdi.data.abyd.helpers.intGrid.ut;

import java.sql.Connection;

import com.pdi.data.abyd.helpers.intGrid.SaveWorkingNotesDataHelper;
import com.pdi.ut.UnitTestUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;

import junit.framework.TestCase;

public class SaveWorkingNotesDataHelperUT  extends TestCase
{
	//
	// Constructors
	//
	public SaveWorkingNotesDataHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
	throws Exception
	{
		junit.textui.TestRunner.run(SaveWorkingNotesDataHelperUT.class);
	}


	/*
	 * testSaveWorkingNotes
	 */
	public void testSaveWorkingNotes()
		throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);
		
		Connection con = AbyDDatabaseUtils.getDBConnection();
		String partId = "9999";
    	long dnaId = 99;
    	String note = "Working note from HUT";

		SaveWorkingNotesDataHelper helper = new SaveWorkingNotesDataHelper(con, partId, dnaId, note);        
		helper.saveWorkingNotes();
		System.out.println("Complete... check database");

		UnitTestUtils.stop(this);
	}

}
