/**
 * Copyright (c) 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.constants;

import java.util.HashMap;

/**
 * DataConstants contains various constants that may be used in multiple
 * and unrelated spots in the code.
 */
public class DataConstants
{
	// User text data types
	public static final String UTT_LABEL = "L";
	public static final String UTT_CUSTCOMP = "C";
	
	// Legacy instrument pseudocodes
	// These codes are used only in conjunction with the equivalency tables
	// and should NEVER be used elsewhere in the system.
	
	// Legacy Watson-Glaser
	public static final String WG_ABC = "wg_abc";	// Watson-Glaser forms A, B, & C
	public static final String WG_SHORT = "wg_sh";	// Watson-Glaser short form
	
	// Legacy Raven's
	public static final String RV_A = "rv_a";	// Raven's Form A
	public static final String RV_SHORT = "rv_sh";	// Raven's Short Form
	
	// FinEx question text, used in TestData (NHN-1624)
	public static HashMap<String, String> FINEX_QUES_TEXT = 
			new HashMap<String, String>();
		static
		{	
			// full questions, then the cut off ones... 
//			FINEX_QUES_TEXT.put("Q1", "If Fleet/Warehouse/Other expense (a Service Expense) had been higher in the current year to date than the 463,000 that was actually spent, what other change would have occurred in the Operating Results statement?");
//			FINEX_QUES_TEXT.put("Q2", "Of the various factors that affect Product Gross Margin, which do you have the most control over as the District or Area Manager?");
//			FINEX_QUES_TEXT.put("Q3", "If your Sales Representative Productivity had met plan (see Key Operating Statistics), how much higher would revenue have been?");
//			FINEX_QUES_TEXT.put("Q4", "What would be an accurate message to deliver to your boss regarding profitability in the year to date as compared to the same period last year?");
//			FINEX_QUES_TEXT.put("Q5", "Suppose an internal study shows that if your district or area had spent a little more money on Travel and Entertainment expenses, your sales representatives would have sold much more product. If this is true, which of the following most accurately reflects how various line items would have been affected on the operating results statement?");
//			FINEX_QUES_TEXT.put("Q6", "What is the most likely impact of reducing your average Time to Repair (see Key Operating Statistics)?");
//			FINEX_QUES_TEXT.put("Q7", "If a service contract that covers an Aria copier for the next five years is sold this quarter for 40,000, what value is closest to the amount that will be counted toward Service Revenue during the next quarter?");
//			FINEX_QUES_TEXT.put("Q8", "What would be a reasonable message to give your boss about why your district or area is under plan for District or Area Profit year to date?");
//			FINEX_QUES_TEXT.put("Q9", "Aria is considering spending 30,000 in the third quarter on a new targeted ad campaign that is expected to bring in additional product revenue of 100,000 in the third quarter. If this happens as predicted, and if the Product Cost of Goods Sold percentage remains the same as forecast for the third quarter, what dollar amount will this add to the gross margin? If none of the other dollar values differ from the forecast (e.g., Travel and Entertainment is 178,000) except the increase in Advertising, what is the most likely impact on Product Operating Margins?");
//			FINEX_QUES_TEXT.put("Q10", "Of the following, what would you say is the main reason that Service Gross Margins are behind plan in your district or area?");
//			FINEX_QUES_TEXT.put("Q11", "In the actual year to date, your district sold 1,626 Premier copiers at an average sales price of 4,001. If your district or area had increased prices by 5% and sold 4% fewer Premier copiers as a result, would this have given your district or area more or less Product Revenue than it actually achieved?");
//			FINEX_QUES_TEXT.put("Q12", "Your district or area has two fewer Service Representatives than planned, and one more Service Representative than the prior year. Our Service Labor and Benefits are 9,000 less than planned and 61,000 higher than the prior year. Based on this information and what you know about your district or area, what is the most likely explanation for these numbers?");
//			FINEX_QUES_TEXT.put("Q13", "How many more Premier copiers would your district or area have needed to sell during the year to date to meet their plan for year to date Product Operating Margin? In answering this question, assume the Products Operating Margin remained constant at 24.6% of Product Revenue, and the average sale price was 4,001 per copier.");
			FINEX_QUES_TEXT.put("Q1", "1. If Fleet/Warehouse/Other expense ...");
			FINEX_QUES_TEXT.put("Q2", "2. Of the various factors that affect Product Gross Margin...");
			FINEX_QUES_TEXT.put("Q3", "3. If your Sales Representative Productivity...");
			FINEX_QUES_TEXT.put("Q4", "4. What would be an accurate message...");
			FINEX_QUES_TEXT.put("Q5", "5. Suppose an internal study shows that...");
			FINEX_QUES_TEXT.put("Q6", "6. What is the most likely impact...");
			FINEX_QUES_TEXT.put("Q7", "7. If a service contract that covers an Aria...");
			FINEX_QUES_TEXT.put("Q8", "8. What would be a reasonable message...");
			FINEX_QUES_TEXT.put("Q9", "9. Aria is considering spending 30,000...");
			FINEX_QUES_TEXT.put("Q10", "10. Of the following, what would you say is...");
			FINEX_QUES_TEXT.put("Q11", "11. In the actual year to date, your district...");
			FINEX_QUES_TEXT.put("Q12", "12. Your district or area has two fewer Service Representatives...");
			FINEX_QUES_TEXT.put("Q13", "13. How many more Premier copiers...");

		};

}
