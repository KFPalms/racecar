/**
 * Copyright (c) 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.nhn.helpers.delegated;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import com.pdi.data.dto.Client;
import com.pdi.data.dto.Participant;
import com.pdi.data.dto.Project;
import com.pdi.data.dto.ProjectParticipant;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.IClientHelper;
import com.pdi.data.helpers.interfaces.IProjectHelper;
import com.pdi.data.nhn.util.NhnDatabaseUtils;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.util.RequestStatus;

import com.pdi.logging.*;

public class ProjectHelper implements IProjectHelper
{
	//
	// Static data
	//
	
	// Sort-order parameters
	private static final int SORT_BY_NAME = 1;
	private static final int SORT_BY_DATE = 2;
	
	// Find type parameters
	private static final int FIND_BY_CLIENT = 101;
	private static final int FIND_BY_ADMIN = 102;
	
	
	//
	// Instance methods
	//
	
	/*
	 * fromId
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IProjectHelper#fromId(com.pdi.data.dto.SessionUser, java.lang.String)
	 */
	@Override
	public Project fromId(SessionUser session, String projectId)
		throws Exception
	{
		Project project = new Project();	
		StringBuffer sb = new StringBuffer();		
		//sb.append("select * from platform.dbo.project p with(NOLOCK) " +
		//		"where project_id = " + Integer.parseInt(projectId));
//		sb.append("SELECT p.project_id, ");
//		sb.append("       p.name, ");
//		sb.append("       p.admin_id, ");
//		sb.append("       p.project_code, ");
//		sb.append("       p.active, ");
//		sb.append("       p.project_type_id as project_type_id, ");
//		sb.append("       pt.name as project_type_name, ");
//		sb.append("       p.target_level_type_id, ");
//		sb.append("       p.date_created, ");
//		sb.append("       p.company_id, ");
//		sb.append("       p.rowguid, ");
//		sb.append("       p.participants_added, ");
//		sb.append("       pt.project_type_code ");
//		sb.append("  FROM platform.dbo.project p WITH(NOLOCK) ");
//		sb.append("    INNER JOIN platform.dbo.project_type pt ON pt.project_type_id = p.project_type_id ");
//		sb.append("  WHERE project_id = " + Integer.parseInt(projectId));
		sb.append("SELECT pv.project_id, ");
		sb.append("       pv.name, ");
		sb.append("       pv.admin_id, ");
		sb.append("       pv.project_code, ");
		sb.append("       pv.active, ");
		sb.append("       pv.project_type_id as project_type_id, ");
		sb.append("       pt.name as project_type_name, ");
		sb.append("       pv.target_level_type_id, ");
		sb.append("       pv.date_created, ");
		sb.append("       pv.company_id, ");
		sb.append("       pv.rowguid, ");
		sb.append("       pv.users_count, ");
		sb.append("       pt.project_type_code, ");
		sb.append("       tlt.name AS target_level_name, ");
		sb.append("       tlt.code AS target_level_code, ");
		sb.append("       tlt.target_level_index ");
		sb.append("  FROM platform.dbo.project_view pv WITH(NOLOCK) ");
		sb.append("    INNER JOIN platform.dbo.project_type pt WITH (NOLOCK) ON pt.project_type_id = pv.project_type_id ");
		sb.append("    LEFT OUTER JOIN platform.dbo.target_level_type AS tlt WITH (NOLOCK) ON pv.target_level_type_id = tlt.target_level_type_id ");
		sb.append("  WHERE project_id = " + Integer.parseInt(projectId));
		// There is a message that comes out of here indicating the statement is closed.
		// it comes from deep in the bowels of ResourceBundle and I cannot isolate it.
		// Ignore for now until we can look at it in Our Copious Amounts Of Free Time
		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(sb);
		if (dlps.isInError()) {
			System.out.println("Error preparing Project fromId query." + dlps.toString());
			LogWriter.logSQL(LogWriter.ERROR, this,
					"Error preparing Project fromId query." + dlps.toString(),
					sb.toString());
			return project;
		}
		DataResult dr = null;
		ResultSet rs = null;
		String company_id = "";
		try		
		{
			dr = NhnDatabaseUtils.select(dlps);
			if(dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
			{
				// No data to find... we are done here
				//return project;
				throw new Exception("Project " + projectId + " not found in database");
			}
			rs = dr.getResultSet();
			if (rs != null && rs.isBeforeFirst())			
			{
				while (rs.next())				
				{
					project.setId(rs.getString("project_id"));
					project.setName(rs.getString("name"));
					project.setAdminId(rs.getString("admin_id"));
					project.setProjectCode(rs.getString("project_code"));
					project.setActive(rs.getInt("active"));
					project.setProjectTypeId(rs.getInt("project_type_id"));
					project.setProjectTypeName(rs.getString("project_type_name"));
					project.setTargetLevelTypeId(rs.getInt("target_level_type_id"));
					project.setDateCreated(rs.getDate("date_created"));
					//project.setParticipantsAdded(rs.getBoolean("participants_added"));
					project.setParticipantsAdded(rs.getInt("users_count") > 0 ? true : false);
					project.setProjectTypeCode(rs.getString("project_type_code"));
					project.setTargetLevelName(rs.getString("target_level_name"));
					project.setTargetLevelCode(rs.getString("target_level_code"));
					project.setTargetLevelIndex(rs.getInt("target_level_index"));
					company_id = rs.getString("company_id");								
					HashMap<String, String> hm = new HashMap<String, String>(); 
					hm.put(Project.GUID, rs.getString("rowguid"));
					project.setMetadata(hm);
				}
			}
		}
		catch(java.sql.SQLException se)
		{	
			LogWriter.logBasic(LogWriter.INFO, this, 
					"ProjectAdministrator: error : HelperDelegate.getProjectHelper() for " + projectId
					+ "\n" + se.getMessage());
			return project;
		}
		finally
		{
//			if (rs != null){
//				try {  rs.close();  }
//				catch(Exception e)
//				{
//					LogWriter.logBasic(LogWriter.WARNING, this,
//							"Problem closing ResultSet.  Msg=" + e.getMessage());
//					return project;
//				}
//			}
			if (dr != null) { dr.close(); dr = null; }
		}
		
		try
		{
			IClientHelper iclient = HelperDelegate.getClientHelper("com.pdi.data.nhn.helpers.delegated");
			Client client = iclient.fromId(null, company_id);
			project.setClient(client);
		} catch (Exception e) {
			LogWriter.logBasic(LogWriter.ERROR, this,
					"Error fetching client:  projId=" + projectId +
					", clientId=" + company_id +
					".  Exception message=" + e.getMessage());
			return project;
		}

		// get the content collection
		//System.out.println("Project setContentCollection");
		project.setContentCollection(this.populateContent(project));		
		// get the user collection
		//System.out.println("Project setParticipantCollection");
		project.setParticipantCollection(this.populateUsers(project));		
//		// update the meta data collection
//		//System.out.println("Project setMetadata");
//		project.setMetadata(this.populateMetaData(project));
		
		return project;
	}

	
	/**
	 * get the content HashSet for the project
	 * 
	 * @param Project project
	 * @return  HashSet<String> content
	 */
	private HashSet<String> populateContent(Project project){
		
		// get the content collection
		HashSet<String> content = new HashSet<String>();
		StringBuffer sb = new StringBuffer();		
		sb.append("select content_id from platform.dbo.project_content where project_id = " + project.getId());
		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(sb);
		if (dlps.isInError()) {
			LogWriter.logSQL(LogWriter.ERROR, this,
					"Error preparing populateContent (projID=" + project.getId() +
					").  " + dlps.toString(), 
					sb.toString());
		}
		DataResult dr = null;
		ResultSet rs = null;
		
		try		
		{
			dr = NhnDatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			if (rs != null && rs.isBeforeFirst())			
			{
				//HashSet<String> content = new HashSet<String>();
				while (rs.next()){
					int cId = rs.getInt("content_id");
					content.add(Integer.toString(cId));	
				}
			}
		}
		catch(java.sql.SQLException se)
		{
			LogWriter.logBasic(LogWriter.ERROR, this,
					"Error fetching data in populateContent (projID=" + project.getId() +
					",  Message=" + se.getMessage());
		}
//		catch (Exception e)
//		{
//			LogWriter.logBasic(LogWriter.ERROR, this,
//					"General error in populateContent (projID=" + project.getId() +
//					",  Message=" + e.getMessage());
//		}
		finally
		{			
			if (dr != null) { dr.close(); dr = null; }
		}
		
		return content;
	}
	
	
	/**
	 * get the users collection for the project
	 * @param Project project
	 * @return HashSet<String> users
	 */
	private HashSet<String> populateUsers(Project project)
	{
		HashSet<String> users = new HashSet<String>();		
		StringBuffer sb = new StringBuffer();		
		sb.append("select * from platform.dbo.project_user where project_id = " + project.getId());
		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(sb);
		if (dlps.isInError()) {
			LogWriter.logSQL(LogWriter.ERROR, this,
					"Error preparing populateUsers(projID=" + project.getId() +
					").  " + dlps.toString(),
					sb.toString());
		}

		DataResult dr = null;
		ResultSet rs = null;
		try
		{
			dr = NhnDatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			if (rs != null && rs.isBeforeFirst())			
			{
				while (rs.next())
				{
					users.add(Integer.toString(rs.getInt("users_id")));	
				}
			}
		}
		catch(java.sql.SQLException se)
		{
			LogWriter.logBasic(LogWriter.ERROR, this,
					"Error fetching data in populateUsers (projID=" + project.getId() +
					".  Msg=" + se.getMessage());
		}
		//catch (Exception e)
		//{
		//	LogWriter.logBasic(LogWriter.ERROR, this,
		//			"General error in populateUsers (projID=" + project.getId() +
		//			").  Msg=" + e.getMessage());
		//}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
		
		return users;
	}


	/*
	 * fromParticipantId
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IProjectHelper#fromParticipantId(com.pdi.data.dto.SessionUser, java.lang.String)
	 */
	@Override
	public ArrayList<Project> fromParticipantId(SessionUser session, String participantId)
		throws Exception
	{				
		ArrayList<Project> projects = new ArrayList<Project>();
		StringBuffer sb = new StringBuffer();		
		sb.append("select project_id from platform.dbo.project_user where users_id = " + participantId);
		//System.out.println(sb);
		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(sb);
		if (dlps.isInError()) {
			LogWriter.logSQL(LogWriter.ERROR, this,
					"Error preparing fromParticipantId (partID=" + participantId +
					").  Data:  " + dlps.toString(),
					sb.toString());
		}
		DataResult dr = null;
		ResultSet rs = null;
		try	
		{
			dr = NhnDatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			if (rs != null && rs.isBeforeFirst())
			{
				while (rs.next())
				{
					//System.out.println("found one");
					projects.add(fromId(session, Integer.toString(rs.getInt("project_id")) ));
				}	
			}
		}
		catch(java.sql.SQLException se)
		{
			LogWriter.logBasic(LogWriter.ERROR, this,
					"Error fetching data in fromParticipantId (partID=" + participantId +
					").  Msg=" + se.getMessage());
		}
		//catch (Exception e)
		//{
		//	LogWriter.logBasic(LogWriter.ERROR, this,
		//			"General error in fromParticipantId (partID=" + participantId +
		//			").  Msg=" + e.getMessage());
		//}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
		return projects;
	}


	/*
	 * fromClientIdByName - Gets a list of all active projects for a client sorted by name
	 * 
	 * @param session A SessionUser object
	 * @param clientId A String containing a client ID
	 * @returns An ArrayList of Project objects
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IProjectHelper#fromClientIdByName(com.pdi.data.dto.SessionUser, java.lang.String)

	 */
	public ArrayList<Project> fromClientIdByName(SessionUser session, String clientId)
		throws Exception
	{
		return fromClientId(session, clientId, SORT_BY_NAME);
	}

	
	/*
	 * fromClientIdByDate - Gets a list of all active projects for a client in reverse order
	 *  of creation (most recent first).
	 * 
	 * NOTE:  The name says by create date but the sort is actually by project_id.  The
	 * sort was done this way because we were told that the id sort is faster.  Order
	 * is preserved because the ID is sequential ascending.  This assumption breaks down
	 * if the created date is changed for some reason, but that should not happen in
	 * current usage.
	 * 
	 * @param session A SessionUser object
	 * @param clientId A String containing a client ID
	 * @returns An ArrayList of Project objects
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IProjectHelper#fromClientIdByDate(com.pdi.data.dto.SessionUser, java.lang.String)

	 */
	public ArrayList<Project> fromClientIdByDate(SessionUser session, String clientId)
		throws Exception
	{
		return fromClientId(session, clientId, SORT_BY_DATE);
	}


	/*
	 * fromClientId - private method that does the actual work for both 
	 * fromClientIdByName and fromClientIdByName
	 * 
	 * @param session A SessionUser object
	 * @param clientId A String containing a client ID
	 * @returns An ArrayList of Project objects
	 */
	private ArrayList<Project> fromClientId(SessionUser session, String clientId, int sortOrder)
		throws Exception
	{
		ArrayList<Project> projects = new ArrayList<Project>();
		StringBuffer sb = new StringBuffer();		
		//sb.append("select project_id from platform.dbo.project where company_id = " + clientId);
		sb.append("SELECT project_id ");
		sb.append("  FROM platform.dbo.project ");
		sb.append("  WHERE company_id = " + clientId + " ");
		switch (sortOrder)
		{
		case SORT_BY_NAME:
			sb.append("  ORDER BY name");
			break;
		case SORT_BY_DATE:
			//sb.append("  ORDER BY date_created DESC, name");
			sb.append("  ORDER BY project_id DESC");
			break;
		default:
			LogWriter.logSQL(LogWriter.ERROR, this,
					"fromClientId - Invalid sort order parameter (" + sortOrder + ").",
					sb.toString());
			return projects;
		}
		//System.out.println("Time to get the data connection");
		ArrayList<Integer> projectIds = new ArrayList<Integer>();
		
		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(sb);
		if (dlps.isInError()) {
			LogWriter.logSQL(LogWriter.ERROR, this,
					"Error preparing query in fromClientId (clientId=" + clientId +
					").  Data:  " + dlps.toString(),
					sb.toString());
			return projects;
		}
		//System.out.println("time to build data");
		DataResult dr = null;
		ResultSet rs = null;
		try
		{
			dr = NhnDatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			if (rs != null && rs.isBeforeFirst())
			{
				while (rs.next())
				{
					projectIds.add(rs.getInt("project_id"));
				}
				//rs.close();			
			}
		}
		catch(java.sql.SQLException se)
		{
			LogWriter.logBasic(LogWriter.ERROR, this,
					"Error fetching in fromClientId (clientId=" + clientId +
					").  Msg:  " + se.getMessage());
			return projects;
		}
		finally
		{
			if (dr != null) {dr.close(); dr = null;}			
		}
		for(int id : projectIds)
		{
			projects.add(fromId(session, Integer.toString(id) ));
		}
		
		return projects;
	}


	/*
	 * fromOwnerIdByName - returns a list of Project objects representing all
	 * projects associated with this owner.  List is in alphabetical order by name.
	 * 
	 * @param session A SessionUser object
	 * @param asminId A String containing an adminId
	 * @returns An ArrayList of Project objects 
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IProjectHelper#fromOwnerIdByName(com.pdi.data.dto.SessionUser, java.lang.String)
	 */
	@Override
	public ArrayList<Project> fromOwnerIdByName(SessionUser session, String adminId)
		throws Exception
	{
		return fromOwnerId(session, adminId, SORT_BY_NAME);
	}


	/*
	 * fromOwnerIdByDate - returns a list of Project objects representing all
	 * projects associated with this owner.  List is in reverse order of creation.
	 * 
	 * NOTE:  The name says by create date but the sort is actually by project_id.  The
	 * sort was done this way because we were told that the id sort is faster.  Order
	 * is preserved because the ID is sequential ascending.  This assumption breaks down
	 * if the created date is changed for some reason, but that should not happen in
	 * current usage.
	 * 
	 * @param session A SessionUser object
	 * @param asminId A String containing an adminId
	 * @returns An ArrayList of Project objects 
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IProjectHelper#fromOwnerIdByDate(com.pdi.data.dto.SessionUser, java.lang.String)
	 */
	@Override
	public ArrayList<Project> fromOwnerIdByDate(SessionUser session, String adminId)
		throws Exception
	{
		return fromOwnerId(session, adminId, SORT_BY_DATE);
	}


	/*
	 * fromOwnerId - The core method that produces the results for the "fromOwnerIdByXxx"
	 * methods
	 * 
	 * @param session A SessionUser object
	 * @param asminId A String containing an adminId
	 * @returns An ArrayList of Project objects 
	 */
	private ArrayList<Project> fromOwnerId(SessionUser session, String adminId, int sortOrder)
		throws Exception
	{
		ArrayList<Project> projects = new ArrayList<Project>();
		StringBuffer sb = new StringBuffer();		
		//sb.append("select project_id from platform.dbo.project where admin_id = " + adminId + " order by project_id DESC");
		sb.append("SELECT project_id ");
		sb.append("  FROM platform.dbo.project ");
		sb.append("  WHERE admin_id = " + adminId + " ");
		switch (sortOrder)
		{
		case SORT_BY_NAME:
			sb.append("  ORDER BY name");
			break;
		case SORT_BY_DATE:
			//sb.append("  ORDER BY date_created DESC, name");
			sb.append("  ORDER BY project_id DESC");
			break;
		default:
			LogWriter.logSQL(LogWriter.ERROR, this,
					"fromOwnerId - Invalid sort order parameter (" + sortOrder + ").",
					sb.toString());
			return projects;
		}
		//System.out.println("Time to get the data connection");
		ArrayList<Integer> projectIds = new ArrayList<Integer>();
		
		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(sb);
		if (dlps.isInError()) {
			LogWriter.logSQL(LogWriter.ERROR, this,
					"Error preparing query in fromClientId (adminId=" + adminId +
					").  Data:  " + dlps.toString(),
					sb.toString());
			return projects;
		}
		//System.out.println("time to build data");
		DataResult dr = null;
		ResultSet rs = null;
		try
		{
			dr = NhnDatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			if (rs != null && rs.isBeforeFirst())
			{
				while (rs.next())
				{
					projectIds.add(rs.getInt("project_id"));
				}
				//rs.close();			
			}
		}
		catch(java.sql.SQLException se)
		{
			LogWriter.logBasic(LogWriter.ERROR, this,
					"Error fetching in fromClientId (adminId=" + adminId +
					").  Msg:  " + se.getMessage());
			return projects;
		}
		finally
		{
			if (dr != null) {dr.close(); dr = null;}			
		}
		for(int id : projectIds)
		{
			projects.add(fromId(session, Integer.toString(id) ));
		}
		
		return projects;
	}

	
	/*
	 * Add a single participant to the project_users table
	 * 
	 * @param session - A SessionUser object
	 * @param project - A Project object with at least a project ID set
	 * @param participantId - A participant ID
	 * @return Minimally populated (id, projId, partId) ProjectParticipant object
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IProjectHelper#addParticipant(com.pdi.data.dto.Project, java.lang.Long)
	 */
	@Override
	public ProjectParticipant addParticipant(SessionUser session, Project project, String partId)
	{
		ProjectParticipant ret = null;
		
		// Check to see if the participant is already in the project_user table
		ProjectParticipant pp = getProjPart(project.getId(), partId);
		if (pp != null)
		{
			// Yup.  Return the ProjectParticipant object
			return pp;
		}
		
		// Doesn't exist... add it
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		String id = null;
		StringBuffer query = new StringBuffer();
		try
		{
			con = NhnDatabaseUtils.getDBConnection();
			
			query.append("INSERT INTO platform.dbo.project_user ");
			query.append("  (project_id, users_id) ");
			query.append("      VALUES( " + project.getId()+ ", " + partId + ")");

			stmt = con.createStatement();
			stmt.execute(query.toString(), Statement.RETURN_GENERATED_KEYS);
			
			// Assumes single row insert
			rs = stmt.getGeneratedKeys();
			rs.next();
			id = rs.getString(1);
			
			// build the new ProjectParticipant object
			ret = new ProjectParticipant(id, project.getId(), ""+partId);
			
			return ret;
		}
		catch (Exception e)
		{
			LogWriter.logSQL(LogWriter.ERROR, this,
					"Error inserting in addParticipant " +
					"(partID=" + partId + ", projID=" + project.getId() +
					").  Msg=" + e.getMessage(),
					query.toString());
			return ret;
		}
		finally
		{
			if (rs != null) {
				try { rs.close(); }
				catch(Exception e) {
					LogWriter.logBasic(LogWriter.WARNING, this,
							"Error closing ResultSet in finally.  Msg=" + e.getMessage());
				};
			}
			if (stmt != null) {
				try { stmt.close(); }
				catch(Exception e) { 
					LogWriter.logBasic(LogWriter.WARNING, this,
							"Error closing Statement in finally.  Msg=" + e.getMessage());
				};
			}
			if (con != null) {
				try { con.close(); }
				catch(Exception e) {
					LogWriter.logBasic(LogWriter.WARNING, this,
							"Error closing Connection in finally.  Msg=" + e.getMessage());
				};
			}
		}
	}


	/*
	 * Add a number of participants to the project_users table
	 * 
	 * @param session - A SessionUser object
	 * @param project - A Project object with at least a project ID set
	 * @param participantId - An ArrayList of participant IDs
	 * @return A boolean indicating success (true) or failure (false)
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IProjectHelper#addParticipants(com.pdi.data.dto.Project, java.util.ArrayList)
	 */
	@Override
	public boolean addParticipants(SessionUser session, Project project, ArrayList<String> inpIds)
	{
		// OK.  Found that if we write out a row to the table in a transaction, then try
		// to do a read on that table the select locks up waiting for the transaction to
		// complete.  Create a list of the IDs that actually need insertion and run that
		// instead of trying to do it all at once...
		ArrayList<String> partIds = new ArrayList<String>();
		for (String pId : inpIds)
		{
			// See if we need to insert it....
			// Check to see if the paricipant/project exists
			if (getProjPart(project.getId(), pId) == null)
			{
				partIds.add(pId);
			}
			else
			{
				// Exists... skip it
				//System.out.println("Already there... scram");
				continue;
			}
		}
		if (partIds.size() < 1)
		{
			// Nothing to insert -- feign success
			return true;
		}
		
		// Set up the prepared statement
		StringBuffer query = new StringBuffer();
		query.append("INSERT INTO platform.dbo.project_user ");
		query.append("  (project_id, users_id) ");
		query.append("      VALUES( ?, ?)");
		
		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(query);
		if (dlps.isInError())
		{
			LogWriter.logSQL(LogWriter.ERROR, this,
					"Error preparing addParticipants query. Data:  " + dlps.toString(),
					query.toString());
			return false;
		}
		
		// Set up for a transaction
		try
		{
			dlps.getPreparedStatement().getConnection().setAutoCommit(false);
		}
		catch (Exception e)
		{
			LogWriter.logBasic(LogWriter.ERROR, this,
					"addParticipants:  Unable to set AutoCommit off.  msg=" + e.getMessage());
			return false;
		}
		
		// Loop through the list of participants to be inserted
		DataResult dr = null;
		try
		{
			for (String pId : partIds)
			{
				try
				{
					dlps.getPreparedStatement().setString(1, project.getId());
					dlps.getPreparedStatement().setString(2, pId);
				}
				catch (Exception e)
				{
					LogWriter.logBasic(LogWriter.ERROR, this,
							"Error setting insert parameters in addParticipants (projId=" +
							project.getId() + ", partId=" + pId + ").  Msg=" + e.getMessage());
					try
					{
						dlps.getPreparedStatement().getConnection().rollback();
					}
					catch (Exception e2)
					{
						LogWriter.logBasic(LogWriter.ERROR, this,
								"Error rolling back in addParticipants (1:  projId=" + project.getId() +
								", partId=" + pId + ").  Msg=" + e.getMessage());
					}
					return false;
				}
				dr = NhnDatabaseUtils.insert(dlps);
				if (dr.isInError())
				{
					LogWriter.logBasic(LogWriter.ERROR, this,
							"Error inserting in addParticipants (projId=" + project.getId() +
							", partId=" + pId + ").  Status="+ dr.getStatus().getStatusCode() +
							", exceptionCode=" + dr.getStatus().getExceptionCode() +
							", exceptionMsg=" + dr.getStatus().getExceptionMessage());
					try
					{
						dlps.getPreparedStatement().getConnection().rollback();
					}
					catch (Exception e2)
					{
						LogWriter.logBasic(LogWriter.ERROR, this,
								"Error rolling back in addParticipants (2:  projId=" + project.getId() +
								", partId=" + pId + ").  Msg=" + e2.getMessage());
					}
					return false;
				}
			}	// End of the loop
			
			// Commit the changes
			try
			{
				dlps.getPreparedStatement().getConnection().commit();
				//System.out.println("DEBUG:  Rolling back...");
				//dlps.getPreparedStatement().getConnection().rollback();
			}
			catch (Exception e3)
			{
				LogWriter.logBasic(LogWriter.ERROR, this,
						"Error committing in addParticipants (projId=" + project.getId() +
						").  Msg=" + e3.getMessage());
				return false;
			}
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}

		// Made it.
		return true;
	}


	/*
	 * Remove a single participant from the project_users table
	 * 
	 * I could probably have used the dlps to do this
	 * 
	 * @param session - A SessionUser object
	 * @param project - A Project object with at least the project ID set
	 * @param partId - A participant ID
	 * @return A boolean indicating success (true) or failure (false)
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IProjectHelper#removeParticipant(com.pdi.data.dto.Project, java.lang.Long)
	 */
	@Override
	public boolean removeParticipant(SessionUser session, Project project, String partId)
	{
		boolean ret = false;
		
 		Connection con = null;
		Statement stmt = null;
		
		StringBuffer query = new StringBuffer();
		query.setLength(0);
		query.append("DELETE FROM platform.dbo.project_user ");
		query.append("  WHERE project_id = " + project.getId() + " ");
		query.append("    AND users_id = " + partId);

		try
		{
			con = NhnDatabaseUtils.getDBConnection();
			
			stmt = con.createStatement();
			stmt.executeUpdate(query.toString());
			ret = true;
			
			return ret;
		}
		catch (Exception e)
		{
			LogWriter.logSQL(LogWriter.ERROR, this,
					"Error in removeParticipant (projID=" + project.getId() +
					", partID=" + partId + ".  Msg=" + e.getMessage(),
					query.toString());
			return ret;
		}
		finally
		{
			if (stmt != null) {
				try { stmt.close(); }
				catch(Exception e) {
					LogWriter.logBasic(LogWriter.WARNING, this,
							"Statement close failed.  Msg=" + e.getMessage());
				};
			}
			if (con != null) {
				try { con.close(); }
				catch(Exception e) {
					LogWriter.logBasic(LogWriter.WARNING, this,
							"Connection close failed.  Msg=" + e.getMessage());
				};
			}
		}
	}


	/*
	 * Remove a number of participants from the project_users table
	 * 
	 * @param session - A SessionUser object
	 * @param project - A Project object with at least the project ID set
	 * @param partList - An ArrayList of Participant objects
	 * @return A boolean indicating success (true) or failure (false)
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IProjectHelper#removeParticipant(com.pdi.data.dto.Project, java.lang.Long)
	 */
	@Override
	public boolean removeParticipants(SessionUser session, Project project, ArrayList<Participant> partList)
	{
		StringBuffer query = new StringBuffer();
		query.append("DELETE FROM platform.dbo.project_user ");
		query.append("  WHERE project_id = ? ");
		query.append("    AND users_id = ?");
		
		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(query);
		if (dlps.isInError())
		{
			LogWriter.logSQL(LogWriter.ERROR, this,
					"Error preparing statement in removeParticipants (projID=" + project.getId() +
					").  Data:  " + dlps.toString(),
					query.toString());
		}
		
		// Set up transaction
		try
		{
			dlps.getPreparedStatement().getConnection().setAutoCommit(false);
		}
		catch (Exception e)
		{
			LogWriter.logBasic(LogWriter.ERROR, this,
					"setAutoCommitOff failed.  Msg=" + e.getMessage());
			return false;
		}

		DataResult dr = null;
		try
		{
			for (Participant part : partList)
			{
				dr = null;
				try
				{
					dlps.getPreparedStatement().setString(1, project.getId());
					dlps.getPreparedStatement().setString(2, part.getId());
				}
				catch (Exception e)
				{
					LogWriter.logBasic(LogWriter.ERROR, this,
							"Error setting insert parameters (projID=" + project.getId() +
							", partID=" + part.getId() + 
							").  Msg=" + e.getMessage());
					try
					{
						dlps.getPreparedStatement().getConnection().rollback();
					}
					catch (Exception e2)
					{
						LogWriter.logBasic(LogWriter.ERROR, this,
								"Error rolling back in removeParticipants (1:  projId=" + project.getId() +
								", partId=" + part.getId() + ").  Msg=" + e2.getMessage());
					}
					return false;
				}
				dr = NhnDatabaseUtils.delete(dlps);
				if (dr.isInError())
				{
					LogWriter.logBasic(LogWriter.ERROR, this,
							"Error deleteing (projID=" + project.getId() +
							", partId=" + part.getId() + ")." +
							"  Status="+ dr.getStatus().getStatusCode() +
							", exceptionCode=" + dr.getStatus().getExceptionCode() +
							", exceptionMsg=" + dr.getStatus().getExceptionMessage());
					try
					{
						dlps.getPreparedStatement().getConnection().rollback();
					}
					catch (Exception e2)
					{
						LogWriter.logBasic(LogWriter.ERROR, this,
								"Error rolling back in removeParticipants (2:  projId=" + project.getId() +
								", partId=" + part.getId() + ").  Msg=" + e2.getMessage());
					}
					return false;
				}
			}	// End of the loop
			
			// Commit the changes
			try
			{
				dlps.getPreparedStatement().getConnection().commit();
				//System.out.println("DEBUG:  Rolling back...");
				//dlps.getPreparedStatement().getConnection().rollback();
			}
			catch (Exception e3)
			{
				LogWriter.logBasic(LogWriter.ERROR, this,
						"Error commiting (projId=" + project.getId() +
						").  Msg=" + e3.getMessage());
				return false;
			}
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
		
		return true;
	}

	
	/*
	 * --> INTERNAL USE ONLY <--
	 * Get a participant/project intersection.  Return null if it does not exist
	 * 
	 * @param projId A project ID
	 * @param partId A participant ID
	 * @return A ProjectParticipant object if it exists, null if it does not exist
	 */
	private ProjectParticipant getProjPart(String projId, String partId)
	{
		ProjectParticipant ret = null;

		StringBuffer query = new StringBuffer();
		query.append("SELECT row_id ");
		query.append("  FROM platform.dbo.project_user pu ");
		query.append("  WHERE pu.project_id = " + projId + " ");
		query.append("  AND pu.users_id = " + partId);
		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(query);
		if (dlps.isInError())
		{
			LogWriter.logSQL(LogWriter.ERROR, this,
					"Error preparing Statement (projId=" + projId +
					", partId=" + partId + ").  Data:  " + dlps.toString(),
					query.toString());
		}
		
		DataResult dr = null;
		try
		{
			dr = NhnDatabaseUtils.select(dlps);
			if (dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
			{
				// This is a non-failure... just no data.
				return ret;
			}
			if (dr.isInError())
			{
				LogWriter.logBasic(LogWriter.ERROR, this,
						"Error selecting participant (projId=" + projId +
						", partId=" + partId + ").  Data:  " + dlps.toString());
				return ret;
			}
			
			ResultSet rs = null;
			try
			{
				rs = dr.getResultSet();
				if (rs != null && rs.isBeforeFirst())
				{
					// Create the return object
					rs.next();
					String id = rs.getString(1);
					ret = new ProjectParticipant(id, projId, ""+partId);
				}
			}
			catch (Exception e)
			{
				LogWriter.logBasic(LogWriter.ERROR, this,
						"Error fetching participant data (projId=" + projId +
						", partId=" + partId + ").  Msg=" + e.getMessage());
			}
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}

		return ret;
	}


	/*
		@Override
		public void enrollParticipant(Project project, Integer participantId) {

			//enroll participant in all content (project.contentCollection) 
			
		}
	*/


	/*
	 * findForClientId - select a list of projects associated with a particular
	 * client and having a specified string in their name.
	 * 
	 * @param session A SessionUser object
	 * @param clientId A String containing a client ID
	 * @param target A String containing the desired name text
	 * @returns An ArrayList of Project objects
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IProjectHelper#findForClientId(com.pdi.data.dto.SessionUser, java.lang.String, java.lang.String)
	 */
	public ArrayList<Project> findForClientId(SessionUser session, String clientId, String target)
		throws Exception
	{
		return find(session, FIND_BY_CLIENT, clientId, target);
	}


	/*
	 * findForAdminId - select a list of projects associated with a particular
	 * administrator and having a specified string in their name.
	 * 
	 * @param session A SessionUser object
	 * @param clientId A String containing a client ID
	 * @param target A String containing the desired name text
	 * @returns An ArrayList of Project objects
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IProjectHelper#findForAdminId(com.pdi.data.dto.SessionUser, java.lang.String, java.lang.String)
	 */
	public ArrayList<Project> findForAdminId(SessionUser session, String adminId, String target)
		throws Exception
	{
		return find(session, FIND_BY_ADMIN, adminId, target);
	}

	
	/*
	 * find - Gets a list of projects that contain the input strings in their name.
	 * Always returns an ArrayList, even if there are no results.
	 * 
	 * @param session A SessionUser object
	 * @param A type parameter; indicates whether the search is by client or by admin
	 * @param target A string to be search upon
	 * @return An ArrayList of Project objects (0 to n objects
	 */
	private ArrayList<Project> find(SessionUser session, int findType, String id, String target)
		throws Exception
	{
		ArrayList<Project> ret = new ArrayList<Project>();
		
		String where = null;
		switch (findType)
		{
		case FIND_BY_CLIENT:
			where = "company_id = " + id;
			break;
		case FIND_BY_ADMIN:
			where = "admin_id = " + id;
			break;
		default:
			LogWriter.logBasic(LogWriter.ERROR, this,
					"find - Invalid find type (" + findType + ".");
			return ret;
		}
	
		// Note that the query appears to be case insensitive.  If that proves not
		// to be the case, then add LOWER() around name and the comparison string. 
		StringBuffer query = new StringBuffer();
		query.append("SELECT project_id ");
		query.append("  FROM platform.dbo.project ");
		query.append("  WHERE " + where + " ");
		query.append("    AND name LIKE '%" + target + "%' ");
		query.append("    AND active = 1 ");
		query.append("  ORDER BY name");
		//System.out.println("find query... " + query.toString());

		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(query);
		if (dlps.isInError())
		{
			LogWriter.logSQL(LogWriter.ERROR, this,
					"find - Preparing query.  Data:  " + dlps.toString(),
					query.toString());
			return ret;
		}
		
		DataResult dr = null;
		ResultSet rs = null;
		ArrayList<String> ids = new ArrayList<String>();
		try
		{
			dr = NhnDatabaseUtils.select(dlps);
			if (dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
			{
				// No problem... we are done here
				return ret;
			}
			if (dr.isInError())
			{
				LogWriter.logBasic(LogWriter.ERROR, this,
						"find - Query error.  Type=" + findType +
						", ID=" + id +
						", target=" + target +
						", status="+ dr.getStatus().getStatusCode() +
						", exceptionCode=" + dr.getStatus().getExceptionCode() +
						", exceptionMsg=" + dr.getStatus().getExceptionMessage());
				return ret;
			}
			rs = dr.getResultSet();
	
			try
			{
				if (rs == null || ! rs.isBeforeFirst())	
				{
					// No data.  Done here.
					return ret;
				}
	
				while (rs.next())
				{
					ids.add(rs.getString("project_id"));
				}
			}
			catch (Exception e)
			{
				LogWriter.logBasicWithException(LogWriter.ERROR, this,
						"find - Results error.  Type=" + findType +
						", ID=" + id +
						", target=" + target,
						e);
				return ret;
			}
			finally
			{
				if (rs != null)
				{
					try { rs.close(); }
					catch (Exception e)  {
						LogWriter.logBasicWithException(LogWriter.WARNING, this,
								"find - Closing ResultSet.", e);
						return ret;
					}
				}
			}
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
		
		// Now get the Project objects
		for(String projId : ids)
		{
			Project proj = fromId(session, projId);
			if (proj == null || proj.getId() == null  || proj.getId().length() < 1)
			{
				// Nothing returned... don't save it
				continue;
			}
			ret.add(proj);
		}
		//System.out.println(" ret. size = " + ret.size());
		
		return ret;
	}


	/*
	 * Create a new set of Project rows in the db.  Return the input project with
	 * the id populated if the add is successful and return a null if it is not.
	 * 
	 * NO data validation is performed in this method.
	 * 
	 * @param session - A SessionUser object
	 * @param project A Project object
	 * @return The input Project object with the id set if successful, null if not
	 *
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IProjectHelper#createProject(com.pdi.data.dto.SessionUser, com.pdi.data.dto.Project)
	 */
	@Override
	public Project createProject(SessionUser session, Project project)
	{
		// Note we are not using the dlps structure here because it is
		// associated with a single prepared statement and we need at
		// least three to do the job.  Also, we need to get the gen-
		// erated ID back and that is not possible with the DLPS as
		// currently coded in NHN
		
		Connection con = null;
		PreparedStatement projPs = null;
		PreparedStatement projContentPs = null;
		ResultSet rs = null;
		String newId = null;
		String phase = null;
		StringBuffer query = new StringBuffer();

		try
		{
			int cnt = 0;
			
			// set up for the project insert
			phase = "Aquire Connection";
			con = NhnDatabaseUtils.getDBConnection();
			
			phase = "Prepare project INSERT statement";
			query.append("INSERT INTO platform.dbo.project ");
			//query.append("        ( name, company_id, admin_id, project_code, active, project_type) ");
			query.append("        ( name, company_id, admin_id, project_code, active, project_type_id, target_level_type_id) ");
			query.append("  VALUES( ?, ?, ?, ?, 1, ?, ?)");
			projPs = con.prepareStatement(query.toString(),Statement.RETURN_GENERATED_KEYS);

			phase = "Prepare project_content INSERT statement";
			query.setLength(0);
			query.append("INSERT INTO platform.dbo.project_content ");
			query.append("        (project_id, content_id) ");
			query.append("  VALUES(?, ?)");
			projContentPs = con.prepareStatement(query.toString());

			// Set up the transaction
			con.setAutoCommit(false);
			//name, company_id, admin_id, project_code
			// Insert data into project
			phase = "Project data insertion";
			projPs.setString(1, project.getName());
			projPs.setInt(2, Integer.parseInt(project.getClient().getId()));
			projPs.setInt(3, Integer.parseInt(project.getAdminId()));
			projPs.setString(4, project.getProjectCode());
			projPs.setInt(5, project.getProjectTypeId());
			projPs.setInt(6, project.getTargetLevelTypeId());
			cnt = projPs.executeUpdate();
			if (cnt != 1)
			{
				//it didn't insert...
				throw new Exception("Project Insert count incorrect - " + cnt + "for projectId " + project.getId());
			}
			
			// get the id
			phase = "getting new key";
			// Assumes single row insert
			rs = projPs.getGeneratedKeys();
			if (rs == null  ||  ! rs.isBeforeFirst())
			{
				//it didn't return an ID...
				throw new Exception("No Project id returned..."  + "for projectId " + project.getId());
			}
			rs.next();
			newId = rs.getString(1);
			project.setId(newId);
 			
			// Insert data into project_content (if needed)
			if (project.getContentCollection().size() > 0)
			{
				phase = "Project content insertion";
				//for (HashSet<String> ent : project.getContentCollection())
				for (Iterator<String> itr=project.getContentCollection().iterator(); itr.hasNext();  )
				{
					projContentPs.setString(1, newId);
					projContentPs.setString(2, itr.next());
					cnt = projContentPs.executeUpdate();
					if (cnt != 1)
					{
						//it didn't insert...
						throw new Exception("Proj. content Insert count incorrect - " + cnt  + "for projectId " + project.getId());
					}
				}
			}	// End of content if

			
			// All done... commit the transaction
			phase = "Committing";
			con.commit();
			
			return project;
		}
		catch (Exception e)
		{
			LogWriter.logBasic(LogWriter.ERROR, this,
					"Error in createProject:  Step = " + phase + 
					"  for projectId " + project.getId() + ".  Msg=" + e.getMessage());
			e.printStackTrace();
			try {  con.rollback();  }
			catch (Exception e2)
			{
				LogWriter.logBasic(LogWriter.ERROR, this,
						"Error in createProject rollback.  Msg=" + e2.getMessage());
			}
			return null;
		}
		finally
		{
			if (rs != null)
			{
				try { rs.close(); }
				catch (Exception e) { 
					LogWriter.logBasic(LogWriter.WARNING, this,
							"Closing ResultSet in createProject.  Msg=" + e.getMessage());
					}
				rs = null;
			}
			if (projPs != null)
			{
				try { projPs.close(); }
				catch (Exception e) {
					LogWriter.logBasic(LogWriter.WARNING, this,
							"Closing project PreparedStatement in createProject.  Msg=" + e.getMessage());
					}
				projPs = null;
			}
			if (projContentPs != null)
			{
				try { projContentPs.close(); }
				catch (Exception e) { 
					LogWriter.logBasic(LogWriter.WARNING, this,
							"Closing content PreparedStatement in createProject.  Msg=" + e.getMessage());
					}
				projContentPs = null;
			}
			if (con != null)
			{
				try { con.close(); }
				catch (Exception e) { 
					LogWriter.logBasic(LogWriter.WARNING, this,
							"Closing Connection in createProject.  Msg=" + e.getMessage());
					}
				con = null;
			}
		}
	}	// End createProject


	/*
	 * Update a project from all of the data in the input Project object.
	 * 
	 * NO data validation is performed in this method.
	 * 
	 * @param session - A SessionUser object
	 * @param project A Project object
	 * @return Success (true) or failure (false) status
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IProjectHelper#updateProject(com.pdi.data.dto.SessionUser, com.pdi.data.dto.Project)
	 */
	@Override
	public boolean updateProject(SessionUser session, Project proj)
	{
		// Note we are not using the dlps structure here because it is
		// associated with a single prepared statement.
		
		boolean ret = false;	// defaults to failure status
		String stage = null;
		Connection con = null;
		
		try
		{
			stage = "Establishing connection";
			con = NhnDatabaseUtils.getDBConnection();
			
			// verify that the project exists
			Project oldProj = this.fromId(session, proj.getId());
			if (oldProj.getId() == null || oldProj.getId().length() < 1)
			{
				LogWriter.logBasic(LogWriter.ERROR, this,
						"updateProject:  Input ID (" + proj.getId() + ")does not exist");
				return false;
			}

			// Set up the transaction
			stage = "Start transaction";
			con.setAutoCommit(false);

			// Update the core
			stage = "Updating core project data";
			ret = doCoreUpdt(session, con, oldProj, proj);
//			
//			// Update the metadata
//			if (ret)
//			{
//				stage = "Updating project metadata";
//				ret = doMetadataUpdt(con, proj.getId(), proj.getMetadata());
//			}
			// Update the content
			if (ret)
			{
				stage = "Updating project content data";
				ret = doContentUpdt(con, proj.getId(), proj.getContentCollection());
			}
			
			// Commit/rollback the transaction
			if (ret)
			{
				stage = "Commiting";
				con.commit();
			}
			else
			{
				stage = "Rolling back";
				con.rollback();
			}
			
			return ret;
		}
		catch (Exception e)
		{
			LogWriter.logBasic(LogWriter.ERROR, this,
					"updateCompletProject:  Stage=" + stage + ", projId=" + proj.getId() +
					".  Msg=" + e.getMessage());
			if (con != null)
			{
				try {  con.rollback();  }
				catch (Exception rb)
				{
					LogWriter.logBasicWithException(LogWriter.WARNING, this,
							"updateProject:  Exception rollback", rb);
				}
			}
			return false;
		}
		finally
		{
			if( con != null)
			{
				try {  con.close(); }
				catch (Exception e)
				{
					LogWriter.logBasicWithException(LogWriter.WARNING, this,
							"updateProject: Closing Connection", e);
				}
			}
		}
	}	// End updateProject


	/*
	 * Update the core data of a project from the input Project object.
	 * See doCoreUpdt for details on what is considered Core data
	 * 
	 * NO data validation is performed in this method.
	 * 
	 * @param session - A SessionUser object
	 * @param project A Project object
	 * @return Success (true) or failure (false) status
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IProjectHelper#updateCoreProject(com.pdi.data.dto.SessionUser, com.pdi.data.dto.Project)
	 */
	@Override
	public boolean updateCoreProject(SessionUser session, Project proj)
	{
		// Note we are not using the dlps structure here.  We probably
		// could have, but we wanted to reuse the same code here as in
		// the full update.
		
		boolean ret = false;	// defaults to failure status
		String stage = null;
		Connection con = null;
		
		try
		{
			stage = "Establishing connection";
			con = NhnDatabaseUtils.getDBConnection();
			
			// verify that the project exists
			Project oldProj = this.fromId(session, proj.getId());
			if (oldProj.getId() == null || oldProj.getId().length() < 1)
			{
				LogWriter.logBasic(LogWriter.ERROR, this,
						"updateCoreProject:  Input ID (" + proj.getId() + ")does not exist");
				return false;
			}

			// Set up the transaction
			stage = "Start transaction";
			con.setAutoCommit(false);

			// Update the core
			stage = "Updating core project data";
			ret = doCoreUpdt(session, con, oldProj, proj);
			
			// Commit/rollback the transaction
			if (ret)
			{
				stage = "Commiting";
				con.commit();
			}
			else
			{
				stage = "Rolling back";
				con.rollback();
			}
			
			return ret;
		}
		catch (Exception e)
		{
			LogWriter.logBasic(LogWriter.ERROR, this,
					"updateCoreProject:  Stage=" + stage + ", projId=" + proj.getId() +
					".  Msg=" + e.getMessage());
			if (con != null)
			{
				try {  con.rollback();  }
				catch (Exception rb)
				{
					LogWriter.logBasicWithException(LogWriter.WARNING, this,
							"updateCoreProject: Exception rollback", rb);
				}
			}
			return false;
		}
		finally
		{
			if( con != null)
			{
				try {  con.close(); }
				catch (Exception e)
				{
					LogWriter.logBasicWithException(LogWriter.WARNING, this,
							"updateCoreProject: Closing Connection", e);
				}
			}
		}
	}	// End updateCoreProject
//
//
////	/*
////	 * Update project metadata.
////	 * 
////	 * Note that this routine can update existing metadata items
////	 * and add new metadata items, but it will not delete existing
////	 * metadata items from a project.  For that functionality, see
////	 * the removeMetadataItems method
////	 * 
////	 * NO data validation is performed in this method.
////	 * 
////	 * @param session - A SessionUser object
////	 * @param projectId A projId string
////	 * @param meta A HashMap of metadata items
////	 * @return Success (true) or failure (false) status
////	 * 
////	 * (non-Javadoc)
////	 * @see com.pdi.data.helpers.interfaces.IProjectHelper#updateMetadataItems(com.pdi.data.dto.SessionUser, java.lang.String, java.util.HashMap)
////	 */
////	@Override
//	public boolean updateMetadataItems(SessionUser session, String projId, HashMap<String, String> meta)
//	{
////		// Note we are not using the dlps structure here.  We probably
////		// could have, but we wanted to reuse the same code here as in
////		// the full update.
////		
////		boolean ret = false;	// defaults to failure status
////		String stage = null;
////		Connection con = null;
////		
////		try
////		{
////			stage = "Establishing connection";
////			con = NhnDatabaseUtils.getDBConnection();
////			
////			// verify that the project exists
////			Project oldProj = this.fromId(session, projId);
////			if (oldProj.getId() == null || oldProj.getId().length() < 1)
////			{
////				LogWriter.logBasic(LogWriter.ERROR, this,
////						"updateMetadataItems:  Input ID (" + projId + ")does not exist");
////				return false;
////			}
////
////			// Set up the transaction
////			stage = "Start transaction";
////			con.setAutoCommit(false);
////
////			// Update the metadata
////			stage = "Updating project metadata";
////			ret = doMetadataUpdt(con, projId, meta);
////
////			// Commit/rollback the transaction
////			if (ret)
////			{
////				stage = "Commiting";
////				con.commit();
////			}
////			else
////			{
////				stage = "Rolling back";
////				con.rollback();
////			}
////			
////			return ret;
////		}
////		catch (Exception e)
////		{
////			LogWriter.logBasicWithException(LogWriter.ERROR, this,
////					"updateMetadataItems:  Stage=" + stage + ", projId=" + projId +
////					".  Msg=" + e.getMessage(), e);
////			if (con != null)
////			{
////				try {  con.rollback();  }
////				catch (Exception rb)
////				{
////					LogWriter.logBasicWithException(LogWriter.WARNING, this,
////							"updateMetadataItems: Exception rollback", rb);
////				}
////			}
////			return false;
////		}
////		finally
////		{
////			if( con != null)
////			{
////				try {  con.close(); }
////				catch (Exception e)
////				{
////					LogWriter.logBasicWithException(LogWriter.WARNING, this,
////							"updateMetadataItems:  Closing Connection", e);
////				}
////			}
////		}
//		return false;	// until we get the metadata permantly removed
//	}	// End of updateMetadataItems


	/*
	 * Update project content.
	 * 
	 * Note that this routine can update will add new content items,
	 * but it will not delete existing ones from a project.  For that
	 * functionality, see the removeContentItems method
	 * 
	 * NO data validation is performed in this method.
	 * 
	 * @param session - A SessionUser object
	 * @param projectId A projId string
	 * @param content A HashSet of content IDs
	 * @return Success (true) or failure (false) status
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IProjectHelper#updateContentItems(com.pdi.data.dto.SessionUser, java.lang.String, java.util.HashSet)
	 */
	@Override
	public boolean updateContentItems(SessionUser session, String projId, HashSet<String> content)
	{
		// Note we are not using the dlps structure here.  We probably
		// could have, but we wanted to reuse the same code here as in
		// the full update.
		
		boolean ret = false;	// defaults to failure status
		String stage = null;
		Connection con = null;
		
		try
		{
			stage = "Establishing connection";
			con = NhnDatabaseUtils.getDBConnection();
			
			// verify that the project exists
			Project oldProj = this.fromId(session, projId);
			if (oldProj.getId() == null || oldProj.getId().length() < 1)
			{
				LogWriter.logBasic(LogWriter.ERROR, this,
						"updateContentItems:  Input ID (" + projId + ")does not exist");
				return false;
			}

			// Set up the transaction
			stage = "Start transaction";
			con.setAutoCommit(false);

			// Update the content
			stage = "Updating project content data";
			ret = doContentUpdt(con, projId, content);
			
			// Commit/rollback the transaction
			if (ret)
			{
				stage = "Commiting";
				con.commit();
			}
			else
			{
				stage = "Rolling back";
				con.rollback();
			}
			
			return ret;
		}
		catch (Exception e)
		{
			LogWriter.logBasicWithException(LogWriter.ERROR, this,
					"updateCompletProject:  Stage=" + stage + ", projId=" + projId + ".", e);
			if (con != null)
			{
				try {  con.rollback();  }
				catch (Exception rb)
				{
					LogWriter.logBasicWithException(LogWriter.WARNING, this,
							"updateContentItems:  Exception rollback", rb);
				}
			}
			return false;
		}
		finally
		{
			if( con != null)
			{
				try {  con.close(); }
				catch (Exception e)
				{
					LogWriter.logBasicWithException(LogWriter.WARNING, this,
							"updateContentItems: Closing Connection", e);
				}
			}
		}
	}	// End of updateContentItems


	/*
	 * removeContentItems - Delete the specified content items for a project from
	 * the project_content link table
	 * 
	 * @param session A SessionUser object
	 * @param projId A project ID String object
	 * @psram content A HashSet containing content ID String objects
	 * @return Success (true) or failure (false) status
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IProjectHelper#removeContentItems(com.pdi.data.dto.SessionUser, java.lang.String, java.util.HashSet)
	 */
	@Override
	public boolean removeContentItems(SessionUser session, String projId, HashSet<String> content)
	{
		// Check for content here.  Mandatory here because the underlying routine
		// uses a null content variable to trigger a deletion of all data. 
		if (content == null)
		{
			LogWriter.logBasic(LogWriter.ERROR, this,
					"removeContentItems:  Invalid parameter - Content Set is null (Project ID=" + projId + ")");
			return false;
		}

		return doContentDel(session, projId, content);
	}


	/*
	 * removeAllContentItems - A convenience class to delete all content items for
	 * a project from the project_content link table
	 * 
	 * @param session A SessionUser object
	 * @param projId A project ID String object
	 * @return Success (true) or failure (false) status
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IProjectHelper#removeAllContentItems(com.pdi.data.dto.SessionUser, java.lang.String)
	 */
	@Override
	public boolean removeAllContentItems(SessionUser session, String projId)
	{
		return doContentDel(session, projId, null);
	}

	
	//------------------------------------------------------------------------------
	// Private methods that support the various flavors of project update and delete
	//------------------------------------------------------------------------------
	
	/*
	 * doCoreUpdt - basic update of core Project data.  Core data is the basic data
	 * associated with a Project (but not metadata or content data).
	 * Currently, core data is defined as the following:
	 * 		name
	 * 		admin_id
	 * 		project_code
	 * 		project_type_id
	 * 		active
	 * 		target_level_type_id
	 * 
	 * Core attributes that are NOT updated are:
	 * 		company_id
	 * 		rowguid
	 * 		date_created
	 * 
	 * Project_id is the primary key and it has been our practice to never modify primary keys.
	 * 
	 * Metadata and content are modified in other private methods
	 * 
	 * @param session A SessionUser object
	 * @param con A Connection object
	 * @param proj A Project object with the update data in it
	 * @return status - true if successful, false if failure
	 */
	private boolean doCoreUpdt(SessionUser session, Connection con, Project oldProj, Project proj)
	{
		// Perform initial data checks
		if (proj.getId() == null || proj.getId().length() < 1)
		{
			LogWriter.logBasic(LogWriter.WARNING, this,
					"doCoreUpdt:  Invalid input; no project ID");
			return false;
		}

		// Passed prelim checks... start the updating
		PreparedStatement projPs = null;
		StringBuffer query = new StringBuffer();
		
		// See if anything changeable changed (can't change id, guid, or client_id
		if (oldProj.getName().equals(proj.getName()) &&
			oldProj.getAdminId().equals(proj.getAdminId()) &&
			oldProj.getProjectCode().equals(proj.getProjectCode()) &&
			//oldProj.getProjectType().equals(proj.getProjectType()) &&
			oldProj.getProjectTypeId() == proj.getProjectTypeId() &&
			oldProj.getActive() == proj.getActive() &&
			oldProj.getTargetLevelTypeId() == proj.getTargetLevelTypeId())
		{
			// Nothing to do... feign success
			return true;
		}

		try
		{
			int cnt = 0;
			
			// set up for the project update
			query.append("UPDATE platform.dbo.project ");
			query.append("  SET name = ?, admin_id = ?, project_code = ?, active = ?, project_type_id = ?, target_level_type_id = ?");
			query.append("  WHERE project_id = ?");
			try
			{
				projPs = con.prepareStatement(query.toString());
			}
			catch (Exception e)
			{
				LogWriter.logBasicWithException(LogWriter.ERROR, this,
						"doCoreUpdt: PrepareStatement", e);
				return false;
			}

			// Update data in project
			// name, admin_id, project_code, active
			try
			{
				projPs.setString(1, proj.getName());
				projPs.setString(2, proj.getAdminId());
				projPs.setString(3, proj.getProjectCode());
				projPs.setInt(4, proj.getActive());
				projPs.setInt(5, proj.getProjectTypeId());
				projPs.setInt(6, proj.getTargetLevelTypeId());
				projPs.setString(7, proj.getId());
				cnt = projPs.executeUpdate();
			}
			catch (SQLException e)
			{
				LogWriter.logBasicWithException(LogWriter.ERROR, this,
						"doCoreUpdt: Preparing or executing update.  ProjID=" + proj.getId(), e);
				return false;
			}
			if (cnt != 1)
			{
				//it didn't insert...
				LogWriter.logBasic(LogWriter.ERROR, this,
						"doCoreUpdt: Invalid update count.  Cnt=" + cnt + ", projID=" + proj.getId());
				return false;
			}

			// Made it... project core data written OK
			return true;
		}
		finally
		{
			if (projPs != null)
			{
				try  {  projPs.close();  }
				catch (Exception e)  {
					LogWriter.logBasic(LogWriter.WARNING, this,
							"doCoreUpdt: Closing project PreparedStatement.  Msg=" + e.getMessage());
				}
				projPs = null;
			}
		}
	}	// End of doCoreUpdt


	/*
	 * doContentUpdt - basic update of Project content data.
	 * The routine ensures that there is are no duplicate
	 * content entries in the database.  If the data exists,
	 * the incoming content item is ignored.  If it does
	 * not exist, a row is inserted.
	 * 
	 * @param con A Connection object
	 * @param projId The project ID to which this metadata belongs
	 * @param meta A HashMap of the metadata
	 * @return status - true if successful, false if failure
	 */
	private boolean doContentUpdt(Connection con, String projId, HashSet<String> content)
	{
		StringBuffer query = new StringBuffer();
		PreparedStatement fetchPS = null;
		PreparedStatement insertPS = null;
		ResultSet rs = null;
		String stage = null;
		HashSet<String> existing = new HashSet<String>();
		
		// Perform initial data checks
		if (projId == null || projId.length() < 1)
		{
			LogWriter.logBasic(LogWriter.ERROR, this,
					"doContentUpdt:  Invalid input; no project ID");
			return false;
		}
		
		// If the content is null or empty, feign success
		if (content == null || content.size() < 1)
		{
			return true;
		}

		try
		{
			// Set up the prepared statements
			stage = "Preparing fetch";
			query.append("SELECT content_id ");
			query.append("  FROM platform.dbo.project_content ");
			query.append("  WHERE project_id = " + projId);
			fetchPS = con.prepareStatement(query.toString());

			stage = "Preparing insert";
			query.setLength(0);
			query.append("INSERT INTO platform.dbo.project_content ");
			query.append("        (project_id, content_id) ");
			query.append("  VALUES(?, ?)");
			insertPS = con.prepareStatement(query.toString());
			
			// Get the current content to a HashSet
			stage = "FetchingContent";
			rs = fetchPS.executeQuery();
			
			if(rs.isBeforeFirst())
			{
				// Got existing data... retrieve it
				while (rs.next())
				{
					//existing.put(rs.getString("content_id"), "x");
					existing.add(rs.getString("content_id"));
				}
			}

			// loop through the input
			//		-- If id exists in the db, continue
			//		-- Otherwise, insert it
			stage = "Adding content";
			for (String cId : content)
			{
				if (existing.contains(cId))
				{
					continue;
				}
				
				// Gotta insert a row
				int cnt = 0;
				insertPS.setString(1, projId);
				insertPS.setString(2, cId);
				cnt = insertPS.executeUpdate();
				if (cnt != 1)
				{
					throw new Exception("Insert count (" + cnt + ") in error.  Content ID=" + cId);
				}
			}

			// If we made it here we are golden
			return true;
		}
		catch (SQLException se)
		{
			LogWriter.logBasicWithException(LogWriter.ERROR, this,
						"doContentUpdt:  " + stage + " -- projId=" + projId, se);
			return false;
		}
		catch (Exception e)
		{
			LogWriter.logBasicWithException(LogWriter.ERROR, this,
					"doContentUpdt:  " + stage + " -- projId=" + projId, e);
			return false;
		}
		finally
		{
			if (rs != null)
			{
				try  {  rs.close();  }
				catch (Exception e)  {
					LogWriter.logBasicWithException(LogWriter.WARNING, this,
							"doContentUpdt:  Closing ResultSet.", e);
				}
				rs = null;
			}
			if (fetchPS != null)
			{
				try { fetchPS.close(); }
				catch (Exception e)  {
				LogWriter.logBasicWithException(LogWriter.WARNING, this,
						"doContentUpdt:  Closing fetch PreparedStatement.", e);
				}
				fetchPS = null;
			}
			if (insertPS != null)
			{
				try { insertPS.close(); }
				catch (Exception e)  {
				LogWriter.logBasicWithException(LogWriter.WARNING, this,
						"doContentUpdt:  Closing insert PreparedStatement.", e);
				}
				insertPS = null;
			}
		}	// End of finally
	}	// End of doContentUpdt

	
	/*
	 * doContentDel - The private method that does the actual content link deletion
	 * for all externally exposed content delete functions.
	 * 
	 * Note:  If the content HashMap is null, all content items are deleted from the
	 * database
	 * 
	 * @param session A SessionUser object
	 * @param projId A project ID String object
	 * @psram content A HashSet containing content ID String objects
	 * @return Success (true) or failure (false) status
	 */
	private boolean doContentDel(SessionUser session, String projId, HashSet<String> content)
	{
		String stage = null;
		Connection con = null;
		PreparedStatement delContentPS = null;
		StringBuffer query = new StringBuffer();
		boolean deleteAll = (content == null);
		
		if(content!= null && content.size() < 1)
		{
			// Nothing to do... feign success
			return true;
		}
		
		try
		{
			// Setup
			stage = "Establishing connection";
			con = NhnDatabaseUtils.getDBConnection();
			
			// verify that the project exists
			stage = "Check project existence";
			Project oldProj = this.fromId(session, projId);
			if (oldProj.getId() == null || oldProj.getId().length() < 1)
			{
				LogWriter.logBasic(LogWriter.ERROR, this,
						"doContentDel:  Input ID (" + projId + ")does not exist");
				return false;
			}
			
			stage = "Set up PreparedStatement";
			query.append("DELETE FROM platform.dbo.project_content ");
			query.append("  WHERE project_id = " + projId + " ");
			if (! deleteAll)
			{
				query.append("  AND content_id = ?");
			}
			delContentPS = con.prepareStatement(query.toString());

			// Set up the transaction
			stage = "Start transaction";
			con.setAutoCommit(false);
			
			stage = "Do the delete";
			if (deleteAll)
			{
				//execute the statement
				delContentPS.executeUpdate();
			}
			else
			{
				// Loop through the content list
				for (String contId : content)
				{
					//Execute the statement
					delContentPS.setString(1, contId);
					delContentPS.executeUpdate();
				}
			}

			// if we get here, we are golden
			stage = "Commit transaction";
			con.commit();
			return true;
		}
		catch (Exception e)
		{
			LogWriter.logBasicWithException(LogWriter.ERROR, this,
					"doContentDel:  " + stage + " -- projId=" + projId, e);
			if (con != null)
			{
				try {  con.rollback();  }
				catch (Exception rb)
				{
					LogWriter.logBasicWithException(LogWriter.WARNING, this,
							"doContentDel:  Exception rollback", rb);
				}
			}
			return false;
		}
		finally
		{
			if( delContentPS != null)
			{
				try {  delContentPS.close(); }
				catch (SQLException se)
				{
					LogWriter.logSQLWithException(LogWriter.WARNING, this,
							"doContentDel:  Closing Delete PreparedStatement", query.toString(), se);
				}
			}
			if( con != null)
			{
				try {  con.close(); }
				catch (Exception e)
				{
					LogWriter.logBasicWithException(LogWriter.WARNING, this,
							"doContentDel:  Closing Connection", e);
				}
			}
		}
	}	// End of doContentDel
	
	
	/**
	 * This is implemented in the V2 Project Helper
	 */
	public ArrayList<String> getInstrumentCodesForProjectId(String projectId, String participantId) throws Exception
	{
		return null;
	}
	
	
	
}
