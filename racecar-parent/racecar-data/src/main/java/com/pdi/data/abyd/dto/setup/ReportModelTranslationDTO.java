/**
 * Copyright (c) 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.dto.setup;

import java.util.ArrayList;

import com.pdi.data.util.language.LanguageItemDTO;

/**
 * ReportModelTranslationDTO is a wrapper around the data used
 * when translating the texts in an RM/CC model.
 * 
 * @author		Ken Beukelman
 */
public class ReportModelTranslationDTO
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private long _dnaId;
	private LanguageItemDTO _languageItem;
	private ArrayList<ReportModelTranslationItemDTO> _xlateItemList = new ArrayList<ReportModelTranslationItemDTO>();

	//
	// Constructors.
	//
	public ReportModelTranslationDTO()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public long getDnaId()
	{
		return _dnaId;
	}

	public void setDnaId(long value)
	{
		_dnaId = value;
	}

	/*****************************************************************************************/
	public LanguageItemDTO getLanguageItem()
	{
		return _languageItem;
	}

	public void setLanguageItem(LanguageItemDTO value)
	{
		_languageItem = value;
	}

	/*****************************************************************************************/
	public ArrayList<ReportModelTranslationItemDTO> getXlateItemList()
	{
		return _xlateItemList;
	}

	public void setXlateItemList(ArrayList<ReportModelTranslationItemDTO> value)
	{
		_xlateItemList = value;
	}

	public void addXlateItemList(ReportModelTranslationItemDTO value)
	{
		_xlateItemList.add(value);
	}
}
