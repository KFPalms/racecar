/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.dto;

import java.util.ArrayList;

import com.pdi.data.util.BaseDTO;

/**
 * ProductDTO is a hybrid of the product information we get
 * from our own database and the web services
 * 
 * @author		Gavin Myers
 * 
 */
public class ProductDTO extends BaseDTO
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//

	//pulled from the database
	private String _productCode;
	private long _productId;

	//pulled from webservices
	private String _productGUID;
	private String _productName;
	private ArrayList<ModuleDTO> _modules;

	
	//
	// Constructors.
	//
	public ProductDTO()
	{
		super();
		// Nothing done on a no-parameter constructor
	}
	

	//
	// Instance methods.
	//

	//////////////////////////////////////
	//        Setters and Getters       //
	//////////////////////////////////////
	
	/********************************************************/
	public void setProductCode(String value)
	{
		_productCode = value;
	}
	
	public String getProductCode()
	{
		return _productCode;
	}
	
	/********************************************************/
	public void setProductGUID(String value)
	{
		_productGUID = value;
	}
	
	public String getProductGUID()
	{
		return _productGUID;
	}
	
	/********************************************************/
	public void setProductId(long value)
	{
		_productId = value;
	}
	
	public long getProductId()
	{
		return _productId;
	}
	
	/********************************************************/
	public void setModules(ArrayList<ModuleDTO> value)
	{
		_modules = value;
	}
	
	public ArrayList<ModuleDTO> getModules()
	{
		return _modules;
	}
	
	/********************************************************/
	public void setProductName(String value)
	{
		_productName = value;
	}
	
	public String getProductName()
	{
		return _productName;
	}
}
