/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */

package com.pdi.data.abyd.helpers.evalGuide;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import com.pdi.data.abyd.dto.evalGuide.EGUrlDTO;
import com.pdi.data.abyd.dto.evalGuide.EGUrlData;
import com.pdi.data.abyd.util.Utils;
import com.pdi.data.dto.Participant;
import com.pdi.data.dto.Project;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.IParticipantHelper;
import com.pdi.data.helpers.interfaces.IProjectHelper;
import com.pdi.data.util.language.DefaultLanguageHelper;
import com.pdi.properties.PropertyLoader;

/**
 * EGUrlDataHelper contains the code needed to extract and return an EGUrlDTO.  It
 * provides data base access and data reduction to generate a list of URLs that is
 * used for the Assessment-by-Design Evaluation Guide invocations.
 *
 * @author		Ken Beukelman
 */
public class EGUrlDataHelper
{
	//
	// Static data.
	//
	private static final int IG_MOD_ID = 0;		// Fake module ID for the Integration Grid
	private static final String IG_MOD_NAME = "Integration Grid";	// Name for the fake module

	//
	// Static methods.
	//

	//
	// Instance data.
	//

	// Holders for constructor verified or fetched data
	private Connection con;
	private String egUrl = null;
	private String igUrl = null;

	// Stuff we hold for later
	int dnaId = 0;
	String dnaName = null;
	LinkedHashMap<Integer, String> modStuff = new LinkedHashMap<Integer, String>();
	LinkedHashMap<Integer, String> modTemp = new LinkedHashMap<Integer, String>();
	private Boolean additionalLPsRequired = false;
	private long dfltLangId = 1;

	// Instantiate the return object
	EGUrlDTO ret = new EGUrlDTO();

	//
	// Constructors.
	//


	/*
	 * Constructor for EGUrlDataHelper - Saves the Connection object in the holder and
	 * verifies that the URL strings exist in the properties file
	 *
	 * @param con - A db Connection object
	 * @throws Exception
	 */
	public EGUrlDataHelper(Connection con)
	throws Exception
	{
		this.con = con;

		// Get the EG and IG URL properties
		this.egUrl = PropertyLoader.getProperty("com.pdi.data.abyd.application", "abd.eg.uiServer");

		if (this.egUrl == null || this.egUrl.length() < 1)
		{
			throw new Exception("An 'abd.eg.uiServer' property is required to generate Eval Guide URL data.");
		}

		this.igUrl = PropertyLoader.getProperty("com.pdi.data.abyd.application", "abd.ig.uiServer");
		if (this.igUrl == null || this.igUrl.length() < 1)
		{
			throw new Exception("An 'abd.ig.uiServer' property is required to generate Integration Grid URL data.");
		}

		// Get the default language
		this.dfltLangId = DefaultLanguageHelper.fetchDefaultLanguageId();
	}

	//
	// Instance methods.
	//
	/**
	 * Main controller to return the generated URLs needed to invoke the Eval Guide app.
	 *
	 * @return An EGUrlDTO object
	 * @throws Exception
	 */
	public EGUrlDTO getUrlData(String projId, String partId)
		throws Exception
	{

		// Need the mod names & ids in any case, so get them
		fetchDnaAndMods(projId);
		if (! this.ret.getSuccessStatus())
		{
			// Soft fail
			return this.ret;
		}

		// See if there is data out there for this project/participant combo
		if (! checkLpByProjPart(projId, partId))
		{
			// No data... make some
			makeNewLPs(projId, partId);
		}

		// Load up the data and return it
		getLpData(projId, partId);

		return this.ret;
	}


	/**
	 * Fetch the dna id associated with the project and also the module ids for
	 * the dna and their names. Stick the information into instance variables
	 *
	 * @throws Exception
	 */
	private void fetchDnaAndMods(String projId) throws Exception {
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT DISTINCT dl.dnaId, ");
		sqlQuery.append("                dl.moduleId, ");
		sqlQuery.append("                m.internalName, ");
		sqlQuery.append("                dna.dnaName, ");
		sqlQuery.append("                dna.dnaSubmitted, ");
		sqlQuery.append("                mtxt.text as modName ");
		sqlQuery.append("  FROM pdi_abd_dna_link dl ");
		sqlQuery.append("    LEFT JOIN pdi_abd_dna dna ON dna.dnaId = dl.dnaId ");
		sqlQuery.append("    INNER JOIN pdi_abd_module m ON (m.moduleId = dl.moduleId and m.hasEG = 1) ");
		sqlQuery.append("    LEFT JOIN pdi_abd_text mtxt ON (mtxt.textId = m.textId AND mtxt.languageId = "
				+ this.dfltLangId + ") ");
		sqlQuery.append("  WHERE dl.dnaId IN (SELECT dna.dnaId ");
		sqlQuery.append("    FROM pdi_abd_dna dna ");
		sqlQuery.append("    WHERE dna.projectId = " + projId + ") ");
		sqlQuery.append("  ORDER BY modName ");

		try {
			stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			if (!rs.isBeforeFirst()) {
				// Fail soft
				this.ret.setSuccessStatus(false);
				this.ret.setStatusMsg(
						"There are no non-test components selected for this DNA/project; no Eval Guides or Integration Grid are available (Project ID="
								+ projId + ").");
				return;
			}

			boolean firstLine = true;
			while (rs.next()) {
				if (firstLine) {
					if (!rs.getBoolean("dnaSubmitted")) {
						this.ret.setSuccessStatus(false);
						this.ret.setStatusMsg(
								"The A by D setup has not been submitted; no Eval Guides or Integration Grid are available (projectId = "
										+ projId + ", dnaId=" + rs.getString("dnaId") + ").");
						return;
					}

					// First time, save the DNA ID and name as well
					this.dnaId = rs.getInt("dnaId");
					this.dnaName = rs.getString("dnaName");

					firstLine = false;
				}

				// ABD-305 - remove the Financials Exercise from the URL
				// Generator,
				// as it is only used in adapt.
				if (!rs.getString("internalName").equals("Financial Exercise")) {
					modStuff.put(rs.getInt("moduleId"), rs.getString("modName"));
				}
			} // End of while loop

			// OK. Put in the fake integration grid item as well
			modStuff.put(IG_MOD_ID, IG_MOD_NAME);

			return;
		} catch (SQLException ex) {
			throw new SQLException("SQL in fetchDnaAndMods.  Msg=" + ex.getMessage(), ex);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) {
					sqlEx.printStackTrace();
				}
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) {
					sqlEx.printStackTrace();
				}
				stmt = null;
			}
		}
	}


	/*
	 * checkLpBProjPart - See if there is any data for this Proj/Part combo
	 *
	 * NOTE:  This code assumes that if there is any data there it is all there
	 *
	 * @param projId - A String with the project ID
	 * @param partId - A String with the participant ID
	 * @returns A boolean indicating that there is or is not data present
	 * @throws Exception
	 */
	private boolean checkLpByProjPart(String projId, String partId)
		throws Exception
	{
		Statement stmt = null;
		ResultSet rs = null;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT COUNT(*) AS cnt ");
		sqlQuery.append("  FROM linkparams lp ");
		sqlQuery.append("  WHERE lp.projectId = " + projId + " ");
		sqlQuery.append("    AND lp.participantId = " + partId);

		try
		{
			stmt = this.con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());

			rs.next();
			return rs.getBoolean("cnt");
		}
		catch (SQLException ex)
		{
			throw new SQLException("SQL in checkLpBProjPart.", ex);
		}
		finally
		{
			if (rs != null)
			{
			    try {  rs.close();  }
			    catch (SQLException sqlEx)  {  sqlEx.printStackTrace();  }
				rs = null;
			}
			if (stmt != null)
			{
			    try {  stmt.close();  }
			    catch (SQLException sqlEx)  {  sqlEx.printStackTrace();  }
			    stmt = null;
			}
		}
	}


	/*
	 * makeNewLPs - Controller to make new LP rows
	 */
	private void makeNewLPs(String projId, String partId)
		throws Exception
	{
		StringBuffer sql = new StringBuffer();
		PreparedStatement pstmt = null;

		// Make a PreparedStatement
		sql.append("INSERT INTO linkparams ");
		sql.append("  (linkparamId, participantId, projectId, moduleId, active, lastUserId, lastDate) ");
		sql.append("  VALUES (?, ?, ?, ?, 1, 'egUrlGen', GETDATE())");

		try
		{
			pstmt = this.con.prepareStatement(sql.toString());
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL in makeNewLPs (prepare).  " +
								"SQLException: " + ex.getMessage() + ", " +
								"SQLState: " + ex.getSQLState() + ", " +
								"VendorError: " + ex.getErrorCode());
		}

		// Loop through the module list and insert a linkparams row for each
		for (Iterator<Map.Entry<Integer, String>> itr=this.modStuff.entrySet().iterator(); itr.hasNext(); )
		{
			Map.Entry<Integer, String> ent = itr.next();

			// Move all of the data except the key into the prepared statement
			pstmt.clearParameters();
			pstmt.clearWarnings();
			pstmt.setString(2, partId);
			pstmt.setString(3, projId);
			pstmt.setInt(4, ent.getKey());

			// Now try to put it into linkparams
			insertLinkparam(pstmt);
		}	// End the for loop (running through the list of modules)

		//// Now do the one for the Integration grid
		// NO.  We added one to the initial module fetch;  we don't need to write one
		//      out here.  It is already added because it is in the module list.
		//pstmt.clearParameters();
		//pstmt.clearWarnings();
		//pstmt.setString(2, partId);
		//pstmt.setString(3, projId);
		//pstmt.setInt(4, IG_MOD_ID);
		//insertLinkparam(pstmt);

		return;
	}	// End makeNewLPs


	/**
	 * insertLinkparam - Writes out a single linkparam row.  It assumes a PreparedStatement
	 * that embodies an INSERT into the linkparams table.  Upon entry, all data has already
	 * been set in the statement WITH THE EXCEPTION OF the linkparamId (field 1).  This
	 * method fills in the linkparamId field and attempts to write out the row to the table.
	 * If the write is successful, the method returns.  If the key already exists in the
	 * table, a new key is generated and the write will be attempted again.  The logic will
	 * generate a key and attempt the write up to three times.  If unsuccessful all three
	 * times, an exception will be thrown.
	 *
	 * @param pstmt A partially set up PreparedStatement as described above.
	 * @return the key used in the successful execution of the insertion
	 * @throws Exception
	 */
	private String insertLinkparam(PreparedStatement pstmt)
		throws Exception
	{
		String key = "";
		int cnt = 0;
		boolean done = false;
		String keys = "";
		while (! done)
		{
			// See if we've tried 3 times
			if (++cnt > 3)
			{
				throw new Exception("Unable to insert row into linkparams (duplicate key error).  Keys=" + keys);
			}
			// Get the key and put it into the prepared statement
			key = Utils.getNumericLPKey();
			pstmt.setString(1, key);

			// Insert it
			try
			{
				pstmt.executeUpdate();
			}
			catch (SQLException ex)
			{
				// TODO Refactor so that the update does not depend upon the error code (do a query)
				// Duplicate key error triggers update; error code = 1062 on MySQL, 2627 on SQL Server
				if (ex.getErrorCode() == 2627)
				{
					// Duplicate key found... save key name and try again
					if (keys.length() > 0)
						keys += ", ";
					keys += key;
					continue;
				}
				else
				{
					// unexpected error - throw it and "exception" on out
					throw ex;
				}
			}

			// it only gets here upon a success
			done = true;
		}	// End the while insert unsuccessful loop

		return key;
	}






	/*
	 * getLpData - Get linkParams data from the database
	 *
	 * @param projId - A project ID string
	 * @param partId - A participantId string
	 * @throws An Exception
	 */
	private void getLpData(String projId, String partId)
		throws Exception
	{
		// clear LP array
		ArrayList<EGUrlData> arr = new ArrayList<EGUrlData>();
		this.ret.setUrlDataArray(arr);
		// Put stuff into the return DTO... start with the stuff we already have
		this.ret.setCandidateId(partId);
		this.ret.setDnaName(this.dnaName);

		// Get the Participant data object
		SessionUser session = new SessionUser();	// Dummy session
		IParticipantHelper iPartHelper = HelperDelegate.getParticipantHelper("com.pdi.data.nhn.helpers.delegated");
		Participant part = iPartHelper.fromId(session, partId);
		this.ret.setFormattedCandName(part.getLastName() + ", " + part.getFirstName());

		// Now get the Project object
		IProjectHelper iProjHelper = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated");
		Project proj = iProjHelper.fromId(session, projId);
		this.ret.setProjectName(proj.getName());
		this.ret.setClientName(proj.getClient().getName());

		// Now get the link param information from the DB and put it into a map
		Statement stmt = null;
		ResultSet rs = null;
		HashMap<Long, Long> lpMap = new HashMap<Long, Long>();
		HashMap<Long, Integer> statMap = new HashMap<Long, Integer>();
		String phase = "";

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT lp.moduleId, ");
		sqlQuery.append("       lp.linkparamId ");
		sqlQuery.append("  FROM linkparams lp ");
		sqlQuery.append("  WHERE lp.projectId = " + projId + " ");
		sqlQuery.append("    AND lp.participantId = " + partId);

		try
		{
			phase = "Create statement";
			stmt = this.con.createStatement();
			phase = "Fetch lp IDs";
			rs = stmt.executeQuery(sqlQuery.toString());

			phase = "Retrieving lp data";
			if (! rs.isBeforeFirst())
			{
				// Make sure there is an empty array there and return
				this.ret.getUrlDataArray();
				return;
			}

			// Save the data to a map
			while (rs.next())
			{
				lpMap.put(rs.getLong("moduleId"), rs.getLong("linkparamId"));
			}



			// clean up and get the status inf
			try {  rs.close();  }
			catch ( SQLException ex) {  ex.printStackTrace(); }
			rs = null;
			sqlQuery.setLength(0);

			// Get the status info.  Note that count is ignored.  It is present
			// so that the second query will always have a row even if there is
			// no IG data for the specified participant/DNA combination.
			sqlQuery.append("SELECT egr.moduleId AS modId, ");
			sqlQuery.append("		egr.egSubmitted AS submitted, ");
			sqlQuery.append("		NULL AS name	");
			sqlQuery.append("  FROM pdi_abd_eg_response egr ");
			sqlQuery.append("  WHERE egr.dnaId = " + this.dnaId + " ");
			sqlQuery.append("    AND egr.participantId = " + partId + " ");
			sqlQuery.append("UNION ");
			sqlQuery.append("SELECT 0 AS modId, ");
			sqlQuery.append("       igr.igSubmitted AS submitted, ");
			sqlQuery.append("		igr.leadFirstName AS name	");
			sqlQuery.append("  FROM pdi_abd_igrid_response igr ");
			sqlQuery.append("  WHERE igr.dnaId = " + this.dnaId + " ");
			sqlQuery.append("    AND igr.participantId = " + partId + " ");
			phase = "Fetch status";
			rs = stmt.executeQuery(sqlQuery.toString());

			phase = "Retrieving status data";

			while (rs.next())
			{
				//long id = rs.getLong("modId");
				//int st = rs.getInt("submitted");
				//String nm = rs.getString("name");
				//int nml = rs.getString("name").length();
				if (rs.getLong("modId") != 0){
					statMap.put(rs.getLong("modId"), rs.getInt("submitted"));
				}else if (rs.getString("name") != null && rs.getString("name").length() > 0){
					statMap.put(rs.getLong("modId"), rs.getInt("submitted"));
				}else{
					//where the IG is not visited (no rater name) we want to skip giving a status (null means not started below)
					statMap.put(rs.getLong("modId"), null);
				}

			}

			//Run through modStuff to generate output data (it was fetched in the proper order)
			for (Iterator<Map.Entry<Integer, String>> itr=this.modStuff.entrySet().iterator(); itr.hasNext(); )
			{
				Map.Entry<Integer, String> ent = itr.next();
				long key = ent.getKey();
				Long lpId = lpMap.get(key);
				if (lpId == null)
				{
					//NHN-1751  this mod is missing a LP. put it in the modTemp for after this loop..
					modTemp.put(ent.getKey(), ent.getValue());
					additionalLPsRequired = true; //triggers insert new mod LPs and restart the whole process.
					//currently we do not remove previously selected but now deselected modules from the LP table.. sure this leaves an 'active' EG link out there, but at least it gives the user the chance to revert back to their original setup without loosing status of EG.
					//throw new Exception("linkParam does not exist for module " + key + " for part/dna " + partId + "/" + this.dnaId);
				}

				EGUrlData out = new EGUrlData();
				out.setModName(ent.getValue());
				if (key != 0L)
				{
					// We have a "real" EG
					out.setEgUrl(this.egUrl + lpId);
					out.setEgFlag(true);
				}
				else
				{
					// We have the IG
					out.setEgUrl(this.igUrl + lpId);
					out.setEgFlag(false);
				}

				int val;
				Integer stat = statMap.get(key);
				if (stat == null)
					val = EGUrlData.MODSTAT_NOT_STARTED;
				else
				{
					if (stat == 0)
						val = EGUrlData.MODSTAT_STARTED;
					else
						val = EGUrlData.MODSTAT_COMPLETE;
				}

				out.setModStatus(val);

				// insert the entry into the output list
				this.ret.getUrlDataArray().add(out);
			}
			if(additionalLPsRequired){
				modStuff = modTemp;
				makeNewLPs(projId, partId);
				//new LP's inserted..
				//start over.
				additionalLPsRequired = false;
				modStuff = new LinkedHashMap<Integer, String>();
				getUrlData(projId, partId);
			}

		}
		catch (SQLException ex)
		{
			throw new SQLException("SQL in getLpData, phase=." + phase, ex);
		}
		finally
		{
			if (rs != null)
			{
			    try {  rs.close();  }
			    catch (SQLException sqlEx)  {  sqlEx.printStackTrace();  }
				rs = null;
			}
			if (stmt != null)
			{
			    try {  stmt.close();  }
			    catch (SQLException sqlEx)  {  sqlEx.printStackTrace();  }
			    stmt = null;
			}
		}
	}

}
