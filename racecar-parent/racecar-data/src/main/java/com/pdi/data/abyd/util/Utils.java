/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.util;

//import java.sql.Connection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
//import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.pdi.data.abyd.dto.common.SpssInfo;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.nhn.util.NhnDatabaseUtils;
//import com.pdi.data.dto.TestData;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.util.RequestStatus;
import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.dateTime.DateTimeUtils;
import com.pdi.logging.LogWriter;


/**
 * The Utils class provides a place to put miscellaneous
 * utility methods valid for use in more than one
 * application or application family
 *
 * @author		Ken Beukelman
 */
public class Utils
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

//	/**
//	 * Get a DB connection for use by a call
//	 *
//	 * @return a Connection object
//	 * @throws Exception
//	 */
//	public static Connection getDBConnection()
//		throws Exception
//	{
//		Connection con = null;
//
//		try
//		{
//			/*
//			 * Gavin:
//			 * this is used to know what environment we are on
//			 * with that info we can choose what database to use (local,dev,prod)
//			 * currently this is not fully implemented
//			 */
//			Properties globalProperties = PropertyLoader.loadProperties("application");
//			String environment = globalProperties.getProperty("global.environment");
//			
//    		Context initContext = new InitialContext();
//    		Context envContext  = (Context)initContext.lookup("java:/comp/env");
//    		
//    		DataSource ds = null;
//    		
//    		/*
//    		 * Gavin: This will need to be more efficient, however the catch should not occur in production environment
//    		 * this should only occur on local machines.
//    		 */
//    		try {
//    			ds = (DataSource) envContext.lookup(CONTEXT_DB_RESOURCE);
//    		} catch (Exception e) {
//    			ds = (DataSource) envContext.lookup(CONTEXT_DB_RESOURCE_GENERIC + environment);
//    		}
//    		
//    		if (ds == null)
//    		{
//    			throw new IllegalArgumentException("Unable to initialize Export data source");
//    		}
//
//    		// open a connection
//    		con = ds.getConnection();
//    		if (con == null)
//    		{
//    			throw new Exception("No export connection; make sure pools are configured correctly.");
//    		}
//		}
//		catch (SQLException ex)
//		{
//            // handle any errors
//        	throw new Exception("Export connection attempt failed.  SQLException: " + ex.getMessage() + ", " +
//        						"SQLState: " + ex.getSQLState() + ", " +
//								"VendorError: " + ex.getErrorCode());
//        }
//        catch (Exception ex)
//		{
//	       	throw new Exception("Export connection setup failed:  " + ex.getMessage());
//		}
//
//		return con;
//	}	
//
//	
//	/**
//	 * Fetch the LinkParams row.
//	 *
//	 * @return an ArrayList of KeyValuePair objects
//	 * @throws Exception
//	 */
// Method references non-existent column (XML)
//	public static String getLinkParamsXML(Connection con, String linkParamsId)
//		throws Exception
//	{
//		
//		String lpXML = "";
//		
//        Statement stmt = null;
//        ResultSet rs = null;
//        
//        StringBuffer sqlQuery = new StringBuffer();
//        sqlQuery.append("SELECT  XML ");
//        sqlQuery.append("  FROM  linkparams ");
//        sqlQuery.append("  WHERE uniqueIdStamp = '" + linkParamsId + "'  ");
//        //System.out.println(" get linkparams sql : " + sqlQuery.toString());
//        try
//		{
//	       	stmt = con.createStatement();
//	       	rs = stmt.executeQuery(sqlQuery.toString());
//           	
//	       	if (! rs.isBeforeFirst())
//	       	{
//	       		throw new Exception("The link param Id is not valid.");
//	       	}
//	       	
//	       	while (rs.next())
//	       	{
//	       		//System.out.println("rs.getString(XML)" + rs.getString("XML"));
//	       		lpXML = (String) rs.getString("XML");
//	       	}
//	       	
//	       	return lpXML;
//		}
//        catch (SQLException ex)
//		{
//        	// handle any errors
//        	throw new Exception("SQL fetching linkParams data.  " +
//        			"SQLException: " + ex.getMessage() + ", " +
//					"SQLState: " + ex.getSQLState() + ", " +
//					"VendorError: " + ex.getErrorCode());
//		}
//        finally
//		{
//            if (rs != null)
//            {
//                try
//				{
//                    rs.close();
//                }
//                catch (SQLException sqlEx)
//				{
//                	// swallow the error
//                }
//                rs = null;
//            }
//            if (stmt != null)
//            {
//                try
//				{
//                    stmt.close();
//                }
//                catch (SQLException sqlEx)
//				{
//                	// swallow the error
//				}
//                stmt = null;
//            }
//        }
//	}	
	
	//
	// Instance data.
	//
	
	//
	// Constructors.
	//

	//
	// Instance methods.
	//

// Method moved to DateTimeUtils
//	/*
//	 *  This method does the heavy lifting of calculating an HRA
//	 *  date.  You set up a GregorianCalendar with the right time
//	 *  zone and the right time there, then pass it into here for calculations
//	 *  
//	 *  @param gc a Gregorian Calendar object that has been initialized
//	 *            with the time zone and the date in that timezone
//	 *  @return a Sting formatted HRA date
//	 */
//	static private double getHraTimestampFromCalendar(GregorianCalendar gc)
//	{
//		TimeZone tz = gc.getTimeZone();
//		
//		long now = gc.getTimeInMillis();
//		
//		// Adjust the time stamp by the differential to UTC (neg number)
//		now += tz.getOffset(now);
//	
//		// Get the number of days since the Epoch
//		// (divide by the number of milliseconds in a day)
//		double edays = ((double) now) / 86400000.0d;
//	
//		// Add the days from 1/1/1900 to 1/1/1970 (the "Epoch")
//		//edays += 25569.00000d;
//		edays += (double)DAYS_OFFSET;
//
//		// Adjust the output to 5 significant digits
//		long bigInt = (long) ((edays * 100000.0d) + .5d);
//		edays = (double)bigInt / 100000.0d;
//
//		return edays;
//	}

	
//// This is the original createHraDate method, preserved just in case	
////	/*
////	 *  createHRADate is a private method that creates a double containing
////	 *  the current time in HRA date format.  Its primary use is for
////	 *  creating dates for use in V2 tables, but it is also used to generate
////	 *  anonymous (numeric digit) keys for linkparams.
////	 *  
////	 *  Note that this signature sends back a double, which is not the
////	 *  desired format for use in the database.
////	 *  
////	 *  @return a double containing an HRA date
////	 */
////	static private double createHRADate()
////	{
////		// Set the time zone.  We use that of our Montreal Partners to
////		// avoid confusion with other similar dates in the database. 
////		TimeZone tz = TimeZone.getTimeZone("America/Montreal");
////		
////		// Create a calendar so we gan get a current timestamp
////		// (the number od millisecs since midnight, 1/1/1970)
////		GregorianCalendar gc = new GregorianCalendar(tz);
////		long now = gc.getTimeInMillis();
////		
////		// Adjust the time stamp by the differential to UTC (neg number)
////		now += tz.getOffset(now);
////		
////		// Get the number of days since the Epoch
////		// (divide by the number of milliseconds in a day)
////		double edays = ((double) now) / 86400000.0d;
////		
////		// Add the days from 1/1/1900 to 1/1/1970 (the "Epoch")
////		edays += 25570.00000d; //<------ Note that this is too big by 1
////	
////		// Adjust the output to 5 significant digits
////		long bigInt = (long) ((edays * 100000.0d) + .5d);
////		edays = (double)bigInt / 100000.0d;
////		
////		return edays;
////	}

// Method moved to DateTimeUtils
//	/*
//	 *  createHRADate is a private method that creates a double containing
//	 *  the current time in HRA date format.  Its primary use is for
//	 *  creating dates for use in V2 tables, but it is also used to generate
//	 *  anonymous (numeric digit) keys for linkparams.
//	 *  
//	 *  Note that this signature sends back a double, which is not the
//	 *  desired format for use in the database.
//	 *  
//	 *  @return a double containing an HRA date
//	 */
//	static private double createHRADate()
//	{
//		// Set the time zone.  We use that of our Montreal Partners to
//		// avoid confusion with other similar dates in the database. 
//		TimeZone tz = TimeZone.getTimeZone("America/Montreal");
//		
//		// Create a calendar so we gan get a current timestamp
//		// (the number od millisecs since midnight, 1/1/1970)
//		GregorianCalendar gc = new GregorianCalendar(tz);
//		
//		
//		return getHraTimestampFromCalendar(gc);
//	}
	
// Method moved to DateTimeUtils
//	/*
//	 *  getHRADateString is a convenience method that returns an
//	 *  HRA V2 date as a string.  It calls the date method, then
//	 *  casts the result to a String.  The output from this
//	 *  method is what would actually be used to get the string
//	 *  used in the V2 database.  The date/time is from the
//	 *  system clock.
//	 *  
//	 *  @return a Sting formatted HRA date
//	 */
//	static public String getHRADateString()
//	{
//		return Double.toString(createHRADate());
//	}

// Method moved to DateTimeUtils
//	/*
//	 * Another convenience method allowing the user to determine the
//	 * time zone and the date and time that is to be used.
//	 */
//	static public String getHRADateString(TimeZone tz,Date date)
//	{
//		// Create a calendar with the desired time zone, then set it up with
//		// a Date object initialized to a time in that time zone.  Example:
//		// PSI dates are Pacific Time.  If we create a Date object using a
//		// PSI time string and set the time zone to Pacific time, the time
//		// stamp should be compatible with the times that we use in the database
//		
//		GregorianCalendar gc = new GregorianCalendar(tz);
//		gc.setTime(date);
//		
//		return Double.toString(getHraTimestampFromCalendar(gc));
//	}
	
// Method moved to DateTimeUtils
//	/*
//	 *  getNormalDateStringFromHRADate changes a HRA Date String into a Java Data object.
//	 *   
//	 *  @return a Date
//	 */
//	static public Date getNormalDateStringFromHRADate(String dateIn)
//		throws Exception
//	{
//		// This was the code supplied to me with 25570 as the number of days from 1/1/1900 to 1/1/1970.
//		// However it was producing a date that one earlier than the correct date...at least in my tests.
//		// So changed it to 25569.
//		//float since70 = Float.parseFloat(dateIn) - 25570f; // Subtract the number of days from 1/1/1900 to 1/1/1970
//		//float since70 = Float.parseFloat(dateIn) - 25569f; // Subtract the number of days from 1/1/1900 to 1/1/1970
//		float since70 = Float.parseFloat(dateIn) - DAYS_OFFSET; // Subtract the number of days from 1/1/1900 to 1/1/1970
//		if (since70 < 0)
//		{
//			throw new Exception("Date conversion cannot handle dates before 1/1/1970. Date = " + dateIn);
//		}
//
//		long ts = (long) ((since70 * 86400000f) + .5f);
//		TimeZone tz = TimeZone.getTimeZone("America/Montreal");
//		GregorianCalendar gc = new GregorianCalendar(tz);
//		gc.setTime(new Date(ts-tz.getOffset(ts)));  // Adjust by the differential to UTC
//		return gc.getTime();
//	}	

	
	/*
	 *  getNumericLPKey is a convenience method that returns a
	 *  string that can be used as a key in linkparams.  It calls
	 *  the date method, then further randomizes it by adding
	 *  a random number to the day fraction.  Tthe output is then
	 *  converted to a String and checked for length.  If the
	 *  output is shorter than 8 characters, it is left padded
	 *  with zeros to 8 characters.  The number cannot be longer
	 *  than 8 digits.
	 *  
	 * 	The use of two "random" numbers may be unnecessary, but
	 *  this is a port of existing functionality.
 
	 *  NOTE:  We use this in other places as well, e.g, answers and results
	 *  
	 *  NOTE2: We could probably rename this to createNumericUniqueId()
	 *  
	 *  @return a Sting of numeric characters of length 8
	 */
	static public String getNumericLPKey()
	{
		// Get the date
		double edays = DateTimeUtils.createHRADate();
		
		// Get the fractional part and convert it to an integer.  We know
		// the "magic number" is 100000 because we generate the number and
		// we know it is exactly 5 digits long.
		int sInt = (int) (((edays - Math.floor(edays)) * 100000.0d) + 0.5);
		
		// Now get an int between 1 and 99900000 and add it to the day fraction
		Random randomGenerator = new Random();
		int randomInt = randomGenerator.nextInt(99900000);
		sInt += randomInt;
		
		// ...and convert it to a right justified zero padded number
		return String.format("%1$08d", sInt);
	}

	/*
	 *  getStringForArrayList just builds a nice little
	 *  string for your array list of strings.
	 * 
	 * @param  the array list - ArrayList<String> al
	 * @return a string - String
	 */
	static public String getStringForArrayList(ArrayList<String> al){
		
		String arrayString = "'";
		
		for(int i = 0; i < al.size(); i++){
			
			String candId = al.get(i);
			
			if(i < al.size()-1){
				arrayString += candId + "','";
				
			}else {
				arrayString += candId + "'";					
			}
		
		}
		System.out.println("getStringForArrayList  " + arrayString);
		return arrayString;
	}
	
	
	/** 
	 * This is an AbyD specific method, so I'm not putting it
	 * into the ProjectHelper, which should be project-type agnostic.
	 * It's going to get used in IGScoring.
	 * 
	 */
	static public long getProjectIdFromDNAId(SessionUser session, long dnaId){
		
		
		try {
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT  dna.projectId ");
			sb.append("  FROM pdi_abd_dna dna ");
			sb.append("  WHERE dna.dnaId = ?  ");
				
			DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
			if (dlps.isInError())
			{
				//System.out.println("Error preparing getDnaIdFromProjectId query." + dlps.toString());
				LogWriter.logSQL(LogWriter.WARNING, null,
						"Error preparing getDnaIdFromProjectId query",
						sb.toString());
			}

			DataResult dr = null;
			try
			{
				dlps.getPreparedStatement().setLong(1, dnaId);
				
				dr = V2DatabaseUtils.select(dlps);
				ResultSet rs = dr.getResultSet();
					
				if (rs != null && rs.isBeforeFirst())
				{
					if(rs.next()) {
						long projectId = rs.getLong("projectId");
						return projectId;

					}else{
						
						throw new SQLException("No projectId found for dna: " + dnaId);
						
					}
					
				}
			}
			catch(java.sql.SQLException se)
			{
				LogWriter.logSQL(LogWriter.ERROR, null,
						"Fetch error in getDnaIdFromProjectId", sb.toString());
				se.printStackTrace();
			}
			finally
			{
		        if (dr != null) { dr.close(); dr = null; }
			}
		} catch(Exception e) {
			LogWriter.logBasic(LogWriter.ERROR, null,
					"General error in getDnaIdFromProjectId");
			//se.printStackTrace();
		}
		
		return new Long(0).longValue();
	}


	/**
	 * getDnaIdFromProjectId
	 * @param projId
	 * @return
	 */
	static public long getDnaIdFromProjectId(long projId)
	{
		try
		{
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT  dna.dnaId ");
			sb.append("  FROM pdi_abd_dna dna ");
			sb.append("  WHERE dna.projectId = ?  ");
				
			DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
			if (dlps.isInError())
			{
				//System.out.println("Error preparing getDnaIdFromProjectId query." + dlps.toString());
				LogWriter.logSQL(LogWriter.WARNING, null,
						"Error preparing getDnaIdFromProjId query",
						sb.toString());
			}

			DataResult dr = null;
			try
			{
				dlps.getPreparedStatement().setLong(1, projId);

				dr = V2DatabaseUtils.select(dlps);
				ResultSet rs = dr.getResultSet();

				if (rs != null && rs.isBeforeFirst())
				{
					if(rs.next())
					{
						long dnaId = rs.getLong("dnaId");
						return dnaId;
					}
					else
					{
						throw new SQLException("No dnaId found for project: " + projId);
					}
				}
			}
			catch(java.sql.SQLException se)
			{
				LogWriter.logSQL(LogWriter.ERROR, null,
						"Fetch error in getDnaIdFromProjectId", sb.toString());
				se.printStackTrace();
			}
			finally
			{
		        if (dr != null) { dr.close(); dr = null; }
			}
		}
		catch(Exception e)
		{
			LogWriter.logBasic(LogWriter.ERROR, null,
					"General error in getDnaIdFromProjectId");
			//se.printStackTrace();
		}

		return new Long(0).longValue();	// fall-through... value = 0
	}


	/**
	 * isInstValid - Check to see if this instrument is valid (and completed) for this ppt/proj
	 * @param abbv
	 * @param ppt
	 * @param proj
	 * @return
	 */
	public boolean isInstValid(String abbv, String ppt, String proj)
	{
		boolean ret = false;
		
		StringBuffer sb = new StringBuffer();		
		sb.append("SELECT  'x' AS ecks ");
		sb.append("  FROM platform.dbo.project_user pu WITH(NOLOCK) ");
		sb.append("    INNER JOIN platform.dbo.project_content pc WITH(NOLOCK) ON (pc.project_id = pu.project_id ");
		sb.append("                                  AND pc.content_id in (SELECT course FROM platform.dbo.course WITH(NOLOCK) WHERE abbv = '" + abbv + "') ");
		sb.append("                                  AND pc.selected_flag = 1) ");
		sb.append("    INNER JOIN platform.dbo.position pp WITH(NOLOCK) ON (pp.users_id = pu.users_id ");
		sb.append("                                        AND pp.course = pc.content_id ");
		sb.append("                                        AND pp.completion_status = 2) ");
		sb.append("  WHERE pu.project_id = " + proj + " AND pu.users_id = " + ppt);
	
		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(sb);
		if (dlps.isInError()) {
			String err = "Error preparing Utils.isInstValid(" + abbv + ")." + dlps.toString();
			System.out.println(err);
			LogWriter.logSQL(LogWriter.ERROR, this, err, sb.toString());
			return ret;
		}
		DataResult dr = null;
		try
		{
			dr = NhnDatabaseUtils.select(dlps);
			int stat = dr.getStatus().getStatusCode();
			if (stat == RequestStatus.RS_ERROR)
			{
				String err = "Error executing select in Utils.isInstValid(" + abbv + "). " + dlps.toString();
				System.out.println(err);
				LogWriter.logSQL(LogWriter.ERROR, this, err, sb.toString());
			}
			else
			{
				if(stat == RequestStatus.DBS_NO_DATA)
				{
					// No data to find... we are done here... default is false
				} else {
					ret = true;
				}
			}
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	
		return ret;
	}


	/**
	 * getCompSpssInfo - gets the SPSS information for the components of scoring (curtrently ALP and KF4D only) for a competency
	 * @param dnaId
	 * @return
	 * @throws Exception
	 */
	public Map<Long, List<SpssInfo>> getCompSpssInfo(long dnaId)
		throws Exception
	{
		Map<Long, List<SpssInfo>> ret = new HashMap<Long, List<SpssInfo>>();
		StringBuffer sqlQuery = new StringBuffer();
		Statement stmt = null;
		ResultSet rs = null;
		String phase = "";

		
		Connection con = null;
		try
		{
			try
			{
				con = AbyDDatabaseUtils.getDBConnection();
			}
			catch (Exception e)
			{
				throw new Exception("getCompSpssInfo() - getting connection:  " + e.getMessage());
			}

			phase = "Creating Statement";
		    stmt = con.createStatement();

		    phase = "comp/spss query execution";
			sqlQuery.append("SELECT csm.competencyId, ");
			sqlQuery.append("       csm.inverted, ");
			sqlQuery.append("       csm.spssId ");
			sqlQuery.append("  FROM pdi_abd_mod_comp_spss_map csm  ");
			sqlQuery.append("  WHERE csm.modelId = (SELECT modelId FROM pdi_abd_dna where dnaId = " + dnaId + ")  ");
			sqlQuery.append("   AND csm.active = 1 ");
			sqlQuery.append("  ORDER BY csm.competencyId, csm.spssId ");
			//System.out.println("sqlQuery.. " + sqlQuery.toString());
				
			phase = "Comp SPSS query execute";
			rs = stmt.executeQuery(sqlQuery.toString());
			if (! rs.isBeforeFirst())
			{
				// no dsta... return the empty output data object
				return ret;	
			}

			// Make the output list
			long curComp = 0;
			phase = "SPSS query data retrieval";
			List<SpssInfo> curList = null;
			while (rs.next())
			{
				long compId =  rs.getLong("competencyId");
				String spss =  rs.getString("spssId");
				boolean inv =  rs.getBoolean("inverted");
				if (compId != curComp)
				{
					curList = new ArrayList<SpssInfo>();
					ret.put(compId, curList);
					curComp = compId;
				}
				curList.add(new SpssInfo(spss, inv));
			}
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL error in Utils.getCompSpssInfo(), (" +
								"DNA ID=" + dnaId + ", " +
								"phase=" + phase + ".  " +
								"SQLException: " + ex.getMessage() + ", " +
								"SQLState: " + ex.getSQLState() + ", " +
								"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (rs != null)
			{
			    try {  rs.close();  }
			    catch (SQLException sqlEx)
			    {
			    	System.out.println("ERROR: rs close in getSpssForComp 'finally' - " + sqlEx.getMessage());
			    }
				rs = null;
			}
			if (stmt != null)
			{
			    try {  stmt.close();  }
			    catch (SQLException sqlEx)
			    {
			    	System.out.println("ERROR: stmt close in getSpssForComp' - " + sqlEx.getMessage());
			    }
			    stmt = null;
			}
			if (con != null)
			{
				try
				{
					con.close();
				}
				catch (SQLException se) {}
				con = null;
			}
		}

		return ret;
	}

}
