package com.pdi.data.nhn.helpers.delegated.ut;

//import com.pdi.data.helpers.HelperDelegate;
//import com.pdi.data.helpers.interfaces.IEmailDataHelper;
import java.util.ArrayList;

import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.IEmailDataHelper;
import com.pdi.data.nhn.util.NhnDatabaseUtils;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

public class EmailDataHelperUT extends TestCase
{

	public EmailDataHelperUT(String name) 	{
		super(name);
	}
	
	
	/*
	 * send
	 */
	public void testSend()
		throws Exception
	{
		UnitTestUtils.start(this);
		NhnDatabaseUtils.setUnitTest(true);
		
		SessionUser session = new SessionUser();
		String templateId = "155";	// TLT Initial 
		//String templateId = "156";	// TLT Reminder
		//String templateId = "157";	// Reset Password
		//String templateId = "3";	// Custom password guy
		String projId = "7";
		ArrayList<String> parts = new ArrayList<String>();
		parts.add("572526");	//US English
		////parts.add("572526");	//US English - modified to a different language for testing
		//parts.add("572595");	//US English - username differs from email address
		//parts.add("572527");	//French
	//	parts.add("572528");	//Nothing
	//	parts.add("987654321");	// bogus ID
		
		IEmailDataHelper helper = HelperDelegate.getEmailDataHelper("com.pdi.data.nhn.helpers.delegated");
		helper.send(session, templateId, projId, parts);

		// Check to see if the e-mails come through
		
		UnitTestUtils.stop(this);
	}
	

	/*
	 * testEmailContent - Used to test to see if the content
	 *         for a template can get sent (in each language)
	 *         
	 * Does NOT run through the send() method... no queue rows generated
	 * Intended for content testing (not functionality).
	 */
	public void testEmailContent()
	throws Exception
	{
		UnitTestUtils.start(this);
		NhnDatabaseUtils.setUnitTest(true);

//	IEmailDataHelper helper = HelperDelegate.getEmailDataHelper("com.pdi.data.nhn.helpers.delegated");
	
	// Set the first param to the template you want to test and the second to a
	// valid e-mail address that you can access to check the resultant e-mails.
	//helper.testEmailContent("155", "kbeukelm@pdinh.com");	// Initial contact
	//helper.testEmailContent("156", "kbeukelm@pdinh.com");	// Reminder
	//helper.testEmailContent("157", "kbeukelm@pdinh.com");	// Reset

	// Check to see if the e-mails come through

		UnitTestUtils.stop(this);
	}

	
//	public void testGetEmailTemplateTexts() throws Exception {
//		UnitTestUtils.start(this);
//		NhnDatabaseUtils.setUnitTest(true);
//		
//		IEmailDataHelper helper = HelperDelegate.getEmailDataHelper("com.pdi.data.nhn.helpers.delegated");
//		
//		for(EmailTemplate et : helper.fromClientId("751")) {
//			for(EmailTemplateText ets : helper.fromId(et.getId() + "").getEmailTemplateTexts()) {
//				System.out.println(ets.getLang() + " - " + et.getCode() + " - " + ets.getSubject());
//			}
//		}
//		
//		UnitTestUtils.stop(this);
//	}
	

	/*
	 * send - This is also a "test" of send.  What we are really doing here is
	 * 		  testing the email load.
	 */
//	public void testEmailLoad()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		NhnDatabaseUtils.setUnitTest(true);
//		
//		SessionUser session = new SessionUser();
//		// You'll have to verify that these ids are valid in SceduledEmailDef
//		//String templateId = "42";	// TLT First Contact
//		String templateId = "43";	// TLT Reminder
//		//////String templateId = "44";	// Reset Password - deleted per TLT standup, 3/9/11
//		String projId = "7";
//		ArrayList<String> parts = new ArrayList<String>();
//		parts.add("572526");	//US English
//		//parts.add("572527");	//French
//		
//		IEmailDataHelper helper = HelperDelegate.getEmailDataHelper("com.pdi.data.nhn.helpers.delegated");
//		helper.send(session, templateId, projId, parts);
//
//		// Check to see if the e-mails come through
//		
//		UnitTestUtils.stop(this);
//	}

}
