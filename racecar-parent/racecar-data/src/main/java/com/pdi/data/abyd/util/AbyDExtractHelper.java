/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.LinkedHashMap;

import com.pdi.data.abyd.dto.common.CompGroupData;
import com.pdi.data.abyd.dto.common.DNACellDTO;
import com.pdi.data.abyd.dto.common.EGBarDisplayData;
import com.pdi.data.abyd.dto.common.EGCompDisplayData;
import com.pdi.data.abyd.dto.common.EGFullDataDTO;
import com.pdi.data.abyd.dto.common.CompElementDTO;
import com.pdi.data.abyd.dto.intGrid.IGFinalScoreDTO;
import com.pdi.data.abyd.dto.intGrid.IGSummaryDataDTO;
import com.pdi.data.abyd.dto.reportInput.ReportInputDTO;
import com.pdi.data.abyd.dto.setup.ReportModelCompChildDTO;
import com.pdi.data.abyd.dto.setup.ReportModelCompetencyDTO;
import com.pdi.data.abyd.dto.setup.ReportModelStructureDTO;
import com.pdi.data.abyd.dto.setup.ReportModelSuperFactorDTO;
import com.pdi.data.abyd.helpers.common.EGDataHelper;
import com.pdi.data.abyd.helpers.intGrid.IGSummaryDataHelper;
import com.pdi.data.abyd.helpers.setup.ReportModelDataHelper;
import com.pdi.data.dto.Participant;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.v2.dto.SessionUserDTO;
import com.pdi.reporting.report.GroupReport;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.xml.XMLUtils;


/*
 * AbdExtractHelper provides a repository for methods used by the ABYD_EXTRACT.groovy
 * scripts in the listener-portal (data collection) environment
 */
public class AbyDExtractHelper
{
	//
	//  Static data
	//

	//
	// Statics used in extract data fetch
	//
	
	// Leadership rating (also Long Term Potential)
	private static String[] LEADERSHIP = new String[5];
	static
	{
		LEADERSHIP[0] = "Insufficient Data";
		LEADERSHIP[1] = "Weak";
		LEADERSHIP[2] = "Mixed";
		LEADERSHIP[3] = "Strong";
		LEADERSHIP[4] = "Very Strong";
	}
	
	//  Fit Recommendation
	private static String[] FIT = new String[5];
	static
	{
		FIT[0] = "Insufficient Data";
		FIT[1] = "Not Recommended";
		FIT[2] = "Recommended w/Reservations";
		FIT[3] = "Recommended";
		FIT[4] = "Strongly Recommended";
	}

	//  Readiness Rating
	private static String[] READINESS = new String[5];
	static
	{
		READINESS[0] = "Insufficient Data";
		READINESS[1] = "Develop in Place";
		READINESS[2] = "Broaden";
		READINESS[3] = "Prepare";
		READINESS[4] = "Ready";
	}

	//  Derailment Rating
	private static String[] DERAIL = new String[5];
	static
	{
		DERAIL[0] = "Insufficient Data";
		DERAIL[1] = "High";
		DERAIL[2] = "Moderate";
		DERAIL[3] = "Low";
		DERAIL[4] = "Minimal";
	}
	
	// Mapping to derailment strings
	private static int DR_NO_DAT = 0;
	private static int DR_HIGH = 1;
	private static int DR_MODERATE = 2;
	private static int DR_LOW = 3;
	private static int DR_MINIMAL = 4;
	

	//
	// Static methods.
	//
	
	//
	// These methods are called from the groovy script in portal.
	// They extract the data that will be needed for the extract.
	//
	/**
	 * Fetch DNA info and client name based upon the V2 job (project) id.
	 *
	 * @throws Exception
	 */
	//public static GroupReport getGroupData(Connection con, String jobId)
	public static void getGroupData(Connection con, String jobId, GroupReport gr)
		throws Exception
	{
		Statement stmt = null;
		ResultSet rs = null;
		//GroupReport gr = new GroupReport();

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT dna.dnaId, ");
		sqlQuery.append("       dna.dnaName, ");
		sqlQuery.append("       dna.modelId, ");
		sqlQuery.append("       modl.internalName as modelName, ");
		sqlQuery.append("       dna.synthMean, ");
		sqlQuery.append("       dna.synthStdDev, ");
		sqlQuery.append("       dna.dnaNotes, ");
		sqlQuery.append("       dna.reportNote ");
//		sqlQuery.append("       cc.Name as clientName ");
		sqlQuery.append("  FROM pdi_abd_dna dna ");
		sqlQuery.append("    LEFT JOIN pdi_abd_model modl ON modl.modelId = dna.modelId ");
//		sqlQuery.append("    LEFT JOIN jobs jj on jj.UniqueIdStamp = dna.jobId ");
//		sqlQuery.append("    LEFT JOIN clients cc on cc.UniqueIdStamp = jj.ClientID ");
		sqlQuery.append("  WHERE dna.projectId = '" + jobId + "'");
		//System.out.println("GRP Data SQL=" + sqlQuery.toString());
	
		try
		{
        	stmt = con.createStatement();
        	rs = stmt.executeQuery(sqlQuery.toString());

        	// If there is no row, return a null
        	if (! rs.isBeforeFirst())
        	{
        		//return null;
        	}

	       	// Get the first result row and put the data into the gr
	       	rs.next();
	       	
			gr.getDisplayData().put("DNA_ID", rs.getString("dnaId"));		
			gr.getDisplayData().put("ProjectName", rs.getString("dnaName"));
			gr.getDisplayData().put("Model", rs.getString("modelName") == null ? "" : rs.getString("modelName"));
			gr.getDisplayData().put("dlciMean", rs.getString("synthMean"));
			gr.getDisplayData().put("dlciStdDev", rs.getString("synthStdDev"));
			gr.getDisplayData().put("DnaNotes", rs.getString("dnaNotes") == null ? "" : XMLUtils.suppressHtmlTags(rs.getString("dnaNotes")));
			gr.getDisplayData().put("Rpt_Note", rs.getString("reportNote") == null ? "" : XMLUtils.suppressHtmlTags(rs.getString("reportNote")));
//			gr.getDisplayData().put("ClientName", rs.getString("clientName") == null ? "" : rs.getString("clientName"));
        	
	       //return gr;
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL fetching DNA.  " +
        					"SQLException: " + ex.getMessage() + ", " +
        					"SQLState: " + ex.getSQLState() + ", " +
        					"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (rs != null)
			{
				try
				{
					rs.close();
				}
        		catch (SQLException sqlEx)
        		{
        			// swallow the error
        		}
        		rs = null;
        	}
        	if (stmt != null)
        	{
        		try
        		{
        			stmt.close();
        		}
        		catch (SQLException sqlEx)
        		{
        			// swallow the error
        		}
        		stmt = null;
        	}
		}
	}
	
	
	/**
	 * Put the Client Competency (Report Model) structure into the
	 * GroupReport object
	 * 
	 * This code esentially duplicates the score code, except it
	 * saves structure (but not scores) and into the GroupReport
	 * object.
	 * 
	 * Perhaps we could find some way to abstract the functionality somehow...
	 *
	 * @throws Exception
	 */
	//public static GroupReport addClientCompetencyStructure(Connection con, GroupReport gr, long dnaId)
	public static void addClientCompetencyStructure(Connection con, GroupReport gr, long dnaId)
		throws Exception
	{
		// Get the data (including scores)
		ReportModelStructureDTO ccStruct =
			new ReportModelDataHelper().getReportModel(con, dnaId);

		//  Using indices so that holes in the sequence don't hurt
		int i = 0;
		int j = 0;
		int k = 0;
		gr.getDisplayData().put("CC_SF_Count", "" + ccStruct.getSuperFactorList().size());
		for (ReportModelSuperFactorDTO sf : ccStruct.getSuperFactorList())
		{
			j = 0;	// Init the competency counter
			i++;
			gr.getDisplayData().put("CC_SF" + i + "_Name", sf.getDisplayName());
			gr.getDisplayData().put("CC_SF" + i + "_Comp_Count", "" + sf.getCompList().size());
			for (ReportModelCompetencyDTO cmp : sf.getCompList())
			{
				k = 0;	// Initialize the child comp counter
				j++;
				gr.getDisplayData().put("CC_SF" + i + "_Comp" + j + "_Name", cmp.getDisplayName());
				gr.getDisplayData().put("CC_SF" + i + "_Comp" + j + "_Essential", cmp.getEssential() ? "True" : "False");

				gr.getDisplayData().put("CC_SF" + i + "_Comp" + j + "_Child_Count", "" + cmp.getChildren().size());
				for(ReportModelCompChildDTO cc : cmp.getChildren())
				{
					k++;
					gr.getDisplayData().put("CC_SF" + i + "_Comp" + j + "_Child" + k + "_Dna_Name", cc.getDnaCompName());
					gr.getDisplayData().put("CC_SF" + i + "_Comp" + j + "_Child" + k + "_Dna_Essential", cc.isDnaCompEssential() ? "True" : "False");
				}
			}
    	}
		
//		return gr;
	}

	
	/**
	 * Put the candidate info into the IR
	 * 
	 *
	 * @throws Exception
	 */
	public static IndividualReport addPartInfo(SessionUserDTO sessionUser,  IndividualReport ir, String partId)
		throws Exception
	{
		ir.getDisplayData().put("ParticipantId", partId);
		
		SessionUser su = HelperDelegate.getSessionUserHelperHelper().createSessionUser();	
		Participant participant = HelperDelegate.getParticipantHelper().fromId(su, partId);

		ir.getDisplayData().put("ParticipantFname", participant.getFirstName() == null ? "Not Available" : participant.getFirstName());
		ir.getDisplayData().put("ParticipantLname", participant.getLastName() == null ? "Not Available" : participant.getLastName());
		
		return ir;
	}
	
	
	/**
	 * Put the DRI stuff into the IR object
	 *
	 * @throws Exception
	 */
	public static IndividualReport addDRIandTLT(IndividualReport ir, long dnaId, String partId, ReportInputDTO ri)
		throws Exception
	{
		
		//ReportInputDTO ri = new ReportInputDataHelper().getReportInput(partId, dnaId);
		// Get the dLCi/LeadershipSkills Index.  Save it twice to satisfy either naming convention
		ir.getDisplayData().put("IG_dLCI", "" + ri.getLeadershipSkillIndex());
		ir.getDisplayData().put("DRI_LSkillIndex", "" + ri.getLeadershipSkillIndex());
		
		// Consultant selected ratings
		ir.getDisplayData().put("DRILeadSkillRating", LEADERSHIP[ri.getLeadershipSkillRating()]);
		ir.getDisplayData().put("DRILeadExperRating", LEADERSHIP[ri.getLeadershipExperienceRating()]);
		ir.getDisplayData().put("DRILeadStyleRating", LEADERSHIP[ri.getLeadershipStyleRating()]);
		ir.getDisplayData().put("DRILeadIntrstRating", LEADERSHIP[ri.getLeadershipInterestRating()]);
		ir.getDisplayData().put("DRIDerailmentRating", DERAIL[ri.getDerailmentRating()]);
		ir.getDisplayData().put("DRILongTermRating", LEADERSHIP[ri.getLongtermRating()]);
		ir.getDisplayData().put("DRILeadSkillRatingNumber", "" + ri.getLeadershipSkillRating());
		ir.getDisplayData().put("DRILeadExperRatingNumber", "" + ri.getLeadershipExperienceRating());
		ir.getDisplayData().put("DRILeadStyleRatingNumber", "" + ri.getLeadershipStyleRating());
		ir.getDisplayData().put("DRILeadIntrstRatingNumber", "" + ri.getLeadershipInterestRating());
		ir.getDisplayData().put("DRIDerailmentRatingNumber", "" + ri.getDerailmentRating());
		ir.getDisplayData().put("DRILongTermRatingNumber", "" + ri.getLongtermRating());
		
		// Fit index and recomendation
		ir.getDisplayData().put("DRIFitIndex", "" + ri.getCandidateFitIndex());	// %tile
		ir.getDisplayData().put("DRIFitRating", FIT[ri.getFitRecommendedRating()]);
		
		// Readiness Recomendation and focus statement
		ir.getDisplayData().put("DRIReadinessRating", READINESS[ri.getReadinessRating()]);
		ir.getDisplayData().put("DRIReadinessRatingNumber", "" + ri.getReadinessRating());
		ir.getDisplayData().put("DRIDevelopmentFocus", ri.getReadinessFocus());

		// Counsultant entered text
		ir.getDisplayData().put("DRISkillsToLeverageOrgText", XMLUtils.suppressHtmlTags(ri.getSkillsToLeverageOrgText()));
		ir.getDisplayData().put("DRISkillsToLeveragePartText", XMLUtils.suppressHtmlTags(ri.getSkillsToLeveragePartText()));
		ir.getDisplayData().put("DRISkillsToDevelopOrgText", XMLUtils.suppressHtmlTags(ri.getSkillsToDevelopOrgText()));
		ir.getDisplayData().put("DRISkillsToDevelopPartText", XMLUtils.suppressHtmlTags(ri.getSkillsToDevelopPartText()));
		ir.getDisplayData().put("DRILeadershipSkillOrgText", XMLUtils.suppressHtmlTags(ri.getLeadershipSkillOrgText()));
		ir.getDisplayData().put("DRILeadershipSkillPartText", XMLUtils.suppressHtmlTags(ri.getLeadershipSkillPartText()));
		ir.getDisplayData().put("DRILeadershipExpOrgText", XMLUtils.suppressHtmlTags(ri.getLeadershipExperienceOrgText()));
		ir.getDisplayData().put("DRILeadershipExpPartText", XMLUtils.suppressHtmlTags(ri.getLeadershipExperiencePartText()));
		ir.getDisplayData().put("DRILeadershipStyleOrgText", XMLUtils.suppressHtmlTags(ri.getLeadershipStyleOrgText()));
		ir.getDisplayData().put("DRILeadershipStylePartText", XMLUtils.suppressHtmlTags(ri.getLeadershipStylePartText()));
		ir.getDisplayData().put("DRILeadershipInterestOrgText", XMLUtils.suppressHtmlTags(ri.getLeadershipInterestOrgText()));
		ir.getDisplayData().put("DRILeadershipInterestPartText", XMLUtils.suppressHtmlTags(ri.getLeadershipInterestPartText()));
		ir.getDisplayData().put("DRIDerailmentOrgText", XMLUtils.suppressHtmlTags(ri.getDerailmentOrgText()));
		ir.getDisplayData().put("DRIDerailmentPartText", XMLUtils.suppressHtmlTags(ri.getDerailmentPartText()));
		ir.getDisplayData().put("DRILongTermPotentialOrgText", XMLUtils.suppressHtmlTags(ri.getLongtermOrgText()));
		ir.getDisplayData().put("DRIFitOrgText", XMLUtils.suppressHtmlTags(ri.getFitOrgText()));
		ir.getDisplayData().put("DRIReadinessOrgText", XMLUtils.suppressHtmlTags(ri.getReadinessOrgText()));
		ir.getDisplayData().put("DRIDevelopmentPartText", XMLUtils.suppressHtmlTags(ri.getDevelopmentPartText()));
		ir.getDisplayData().put("DRIPivotalPartText", XMLUtils.suppressHtmlTags(ri.getPivotalPartText()));
		
		// Put out the TLT stuff
		IndividualReport tlt = ri.getCurrentTLT();
		
		//System.out.println(tlt.toString());
		ir.getDisplayData().put("TLTLeadershipInterest", tlt.getScriptedGroupData("LE_INTER_FINALSCORE") == 1 ? "On" : "Off");
		// per NHN-1646, do not show leadership experience if no lei or no cs
		if(ir.getDisplayData().get("CS_MISSING") != null || ir.getDisplayData().get("LEI_MISSING") != null ){
			// skip these
			//System.out.println("do not show leadership experience if no lei or no cs.....");
		}else{
			// do these
			ir.getDisplayData().put("TLTLeadershipExperience", tlt.getScriptedGroupData("LE_EXPER_FINALSCORE") == 1 ? "On" : "Off");
		}		
		ir.getDisplayData().put("TLTLeadershipFoundations", tlt.getScriptedGroupData("LE_FOUND_FINALSCORE") == 1 ? "On" : "Off");
		
		// For derailment Risk, make the following assumption (based on the color of the box on the 
		// TLT Group Detail Report):
		// Stanine	Box Color	String
        //  1 - 3	  Blue		"Minimal"
		//  4 - 7	  Green		"Low"
		//	  8		  Yellow	"Moderate"
		//	  9		  Red		"High"
		String str = tlt.getScriptedData("DERAIL_FACTORS");
		int num = -1;
		boolean valid = true;
		if (str == null)
		{
			num = DR_NO_DAT;
		}
		else if ( str.equals("1") || str.equals("2") || str.equals("3"))
		{
			num = DR_MINIMAL;
		}
		else if ( str.equals("4") || str.equals("5") || str.equals("6") || str.equals("7"))
		{
			num = DR_LOW;
		}
		else if ( str.equals("8"))
		{
			num = DR_MODERATE;
		}
		else if ( str.equals("9"))
		{
			num = DR_HIGH;
		}
		else
		{
			num = DR_NO_DAT;
			valid = false;
		}
		if (valid)
		{
			str = DERAIL[num];
		}
		else
		{
			str = "Invalid Value (" + str + ")";
		}
		ir.getDisplayData().put("TLTDerailmentRisk", str);
		ir.getDisplayData().put("TLTDerailmentRiskNumber", "" + num);
		
		// Interest
		//per NHN 1646 - if cs is not present, do not show interest scores
		if(ir.getDisplayData().get("CS_MISSING") == null ){
			// do these
			ir.getDisplayData().put("TLTLeadershipAspiration", tlt.getScriptedData("CAREER_GOALS"));
			ir.getDisplayData().put("TLTCareerDrivers", tlt.getScriptedData("CAREER_DRIVERS"));
			ir.getDisplayData().put("TLTLearningOrientation", tlt.getScriptedData("LEARNING_ORIENT"));
			ir.getDisplayData().put("TLTExperienceOrientation", tlt.getScriptedData("EXPERIENCE_ORIENT"));
		}else {
			// skip these
			//System.out.println("if cs is not present, do not show interest scores.....");
		}

		
		// Experience
		// per NHN 1646 - if cs or lei are not present do not show Experience scores
		if(ir.getDisplayData().get("CS_MISSING") != null || ir.getDisplayData().get("LEI_MISSING") != null ){
			// skip these
			//System.out.println("if cs or lei are not present do not show Experience scores.....");
		}else{
			// do these
			ir.getDisplayData().put("TLTBusinessOperations", tlt.getScriptedData("LEI_OPERATIONS"));
			ir.getDisplayData().put("TLTHandlingToughChallenges", tlt.getScriptedData("LEI_SIT_TOUGH_CHAL"));
			ir.getDisplayData().put("TLTHighVisibility", tlt.getScriptedData("LEI_HIGH_VIS"));
			ir.getDisplayData().put("TLTGrowingTheBusiness", tlt.getScriptedData("LEI_BD_GROW_BUS"));
			ir.getDisplayData().put("TLTPersonalDevelopment", tlt.getScriptedData("LEI_PERSONAL"));
		}

		
		// Foundations
	    if (tlt.getDisplayData().get("COGNITIVES_INCLUDED").equals("1"))
	    {
	    	str = tlt.getScriptedData("ZGEST");
	    }
	    else
	    {
	    	str = "Not available";
	    }
	    // per NHN 1646 - if cs is not present do not show the following foundations scores
		if(ir.getDisplayData().get("CS_MISSING") == null ){
			// do these
	    	ir.getDisplayData().put("TLTProblemSolving",str);
			ir.getDisplayData().put("TLTIntellectualEngagement", tlt.getScriptedData("INTELLECTUAL_ENGMT"));
			ir.getDisplayData().put("TLTAttentionToDetail", tlt.getScriptedData("DETAIL_ORIENT"));
			ir.getDisplayData().put("TLTImpactInfluence", tlt.getScriptedData("FACIL_LEADERSHP"));
			ir.getDisplayData().put("TLTInterpersonalEngagement", tlt.getScriptedData("INTER_PERSONAL_ENGMT"));
		}else{                      
			// skip these
			//System.out.println(" if cs is not present do not show the following foundations scores.....");
		}
		ir.getDisplayData().put("TLTAchievementDrive", tlt.getScriptedData("DRIVE"));
		ir.getDisplayData().put("TLTAdvancementDrive", tlt.getScriptedData("INDIV_ORIENT"));
		ir.getDisplayData().put("TLTCollectiveOrientation", tlt.getScriptedData("COLLECTIVE_ORIENT"));
		ir.getDisplayData().put("TLTFlexibilityAdaptability", tlt.getScriptedData("POSITIVITY"));
		
		return ir;
	}
	
	
	/**
	 * Put the Client Competency (Report Model) scores into the IR object
	 * 
	 * This code esentially duplicates the structure code, except it
	 * saves scores only and into the IndividualReport object.
	 * 
	 * Perhaps we could find some way to abstract the functionality somehow...
	 *
	 * @throws Exception
	 */
	public static IndividualReport addClientCompetencyScores(Connection con, IndividualReport ir, String partId, 
																long dnaId, ReportModelStructureDTO ccStruct)
		throws Exception
	{
		// Get the data (including scores)
//		ReportModelStructureDTO ccStruct =
//			new ReportModelDataHelper().getReportModelWithScores(con, dnaId, partId);
		//  Using indices so that holes in the sequence don't hurt
		int i = 0;
		int j = 0;
		for (ReportModelSuperFactorDTO sf : ccStruct.getSuperFactorList())
		{
			j = 0;	// Init the competency counter
			i++;
			for (ReportModelCompetencyDTO cmp : sf.getCompList())
			{
				j++;
				ir.getDisplayData().put("CC_SF" + i + "_Comp" + j + "_Score", "" + cmp.getCompScore());
				//System.out.println("CC_SF" + i + "_Comp" + j + "_Score" + cmp.getCompScore());
			}
			
    	}
		
		
		return ir;
	}
	
	
	/**
	 * Put the IntegrationGrig (IG) info into the IR object
	 * Note that the dLCI is calculated in a different module and the data is inserted there
	 *
	 * @throws Exception
	 */
	public static IndividualReport addIGData(Connection con, IndividualReport ir, String partId, long dnaId)
		throws Exception
	{
		IGSummaryDataDTO ig = new IGSummaryDataHelper(con, partId, dnaId).getSummaryData();
		ir.getDisplayData().put("IG_WorkingNotes", ig.getWorkingNotes() == null ? "" : ig.getWorkingNotes());
		
		// adding the IG Submitted dates per NHN-2199	
		ir.getDisplayData().put("IGDate", new IGSummaryDataHelper(con).getIGSubmittedDateWithFormat(dnaId, partId, "dd/MM/yyyy"));
		ir.getDisplayData().put("ReportDate", new IGSummaryDataHelper(con).getIGSubmittedDate(dnaId, partId));		
		

		// get the list of comps in order (need name and save Id)
		int i = 0;
		LinkedHashMap<Long, CompInfo> cmps= new LinkedHashMap<Long, CompInfo>();
		// Loop through the superfactors
		for (CompGroupData cg : ig.getGridData().getFullCompArray())
		{
			// No sf info needed... Loop through the competencies
			for(CompElementDTO comp : cg.getCgCompList())
			{
				i++;
				// Put out the comp name and save the comp id
				cmps.put(new Long(comp.getCompId()), new AbyDExtractHelper().new CompInfo(i));
				ir.getDisplayData().put("IG_Comp" + i + "_Name", comp.getCompName());
			}
		}
		ir.getDisplayData().put("IG_Comp_Count", "" + i);

		// Final (consultant entered)scores (and notes)
		double finCum = 0.0;
		int finCnt = 0;
		for(IGFinalScoreDTO finScores : ig.getCompFinData())
		{
			// get the compId
			long id = finScores.getCompId();
			// Note that the value here is an index into a list, not the value to be used
			int idx = finScores.getCompScoreIdx();
			//get the actual score value as a string
			// use simplifying assumptions 1=1.0, 2=1.5, 3=2.0,...9=5.0
			String scor = null;
			if (idx < 1)
			{
				scor = "N/A";
			}
			else
			{
				double raw = ((double)idx + 1.0) / 2.0;
				raw = Math.floor((raw * 100.0) + 0.505) / 100.0;
				scor = "" + raw;
				finCum += raw;
				finCnt++;
			}
			// Get the position of the competency and put out the score
			int posn = cmps.get(new Long(id)).getPosition();
			ir.getDisplayData().put("IG_Comp" + posn + "_FinalScore", scor);
			// and don't forget the note
			ir.getDisplayData().put("IG_Comp" + posn + "_Note", finScores.getCompNote() == null ? "" : finScores.getCompNote());
		}
		// Total score
		double tot = 0.0;
		if (finCnt != 0.0)
		{
			tot = finCum / (double)finCnt;
			tot = Math.floor((tot * 100.0) + 0.505) / 100.0;
		}
		ir.getDisplayData().put("IG_TotalScore", "" + tot);

		// gen data for calculated comp scores
		for (DNACellDTO cell : ig.getGridData().getModCompIntersection())
		{
			// Loop through accumulating data
			long id = cell.getCompetencyId();
			String sc = cell.getScore();
			double scor = Double.parseDouble(sc);
			cmps.get(new Long(id)).addCum(scor);
		}
		// Calculate and display calculated comp scores
		for (Iterator<CompInfo> itr=cmps.values().iterator(); itr.hasNext(); )
		{
			String str;
			CompInfo cur = itr.next();
			if (cur.getCount() == 0.0)
			{
				str = "0.0";
			}
			else
			{
				double scor = cur.getCum() / (double)cur.getCount();
				scor = Math.floor((scor * 100.0) + 0.505) / 100.0;
				str = "" + scor;
			}
			ir.getDisplayData().put("IG_Comp" + cur.getPosition() + "_CalcScore", str);
		}
		
		return ir;
	}
	
	
	/**
	 * Put the EG info into the IR
	 *
	 * @throws Exception
	 */
	public static IndividualReport addEGData(Connection con, IndividualReport ir, String partId, long dnaId)
		throws Exception
	{
		Statement stmt = null;
		ResultSet rs = null;

		// Get the module IDs in order
		StringBuffer sqlQuery = new StringBuffer(); 
		sqlQuery.append("SELECT DISTINCT dl.moduleId, amod.internalName, dl.modSeq "); 
		sqlQuery.append("  FROM pdi_abd_dna_link dl, pdi_abd_module amod ");
		sqlQuery.append("  WHERE dl.dnaId = " + dnaId + " ");
		sqlQuery.append("    AND dl.isUsed = 1 ");
		sqlQuery.append("    AND amod.moduleId = dl.moduleId ");
//		sqlQuery.append("    AND (amod.hasEG = 1 OR amod.internalName = 'KF360') ");
		sqlQuery.append("    AND amod.hasEG = 1 ");
		sqlQuery.append("  ORDER BY dl.modSeq");
		
		try
		{
			stmt = con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());
		    
			if (! rs.isBeforeFirst())
			{
				System.out.println("No EGs for DNA " + dnaId);
				return null;
			}
			
			int i = 0;
			while(rs.next())
			{
				i++;
//				String intName = rs.getString("internalName");
				EGFullDataDTO eg = new EGDataHelper(con, partId, dnaId, rs.getLong("moduleId")).getEGDisplayData();
				// Put out sim level data
				ir.getDisplayData().put("EG" + i + "_Name", eg.getSimName());
				ir.getDisplayData().put("EG" + i + "_OPR", "" + eg.getOverallData().getOpr());
				ir.getDisplayData().put("EG" + i + "_BriefDesc", eg.getOverallData().getBriefDesc() == null ? "" : eg.getOverallData().getBriefDesc());
				ir.getDisplayData().put("EG" + i + "_KeyStrengths", eg.getOverallData().getKeyStrengths() == null ? "" : eg.getOverallData().getKeyStrengths());
				ir.getDisplayData().put("EG" + i + "_KeyNeeds", eg.getOverallData().getKeyDevNeeds() == null ? "" : eg.getOverallData().getKeyDevNeeds());
				ir.getDisplayData().put("EG" + i + "_CoachNotes", eg.getOverallData().getCoachNotes() == null ? "" : eg.getOverallData().getCoachNotes());
				// Put out the comps for this sim
				int j = 0;
				for(EGCompDisplayData comp : eg.getCompArray())
				{
					j++;
					ir.getDisplayData().put("EG" + i + "_Comp" + j + "_Name", comp.getCompName());
					ir.getDisplayData().put("EG" + i + "_Comp" + j + "_Notes", comp.getCompNotes() == null ? "" : comp.getCompNotes());
					
					int zeroCnt = 0;
					double avgScore = 0.0;
//					if ("kf360".equals(intName))
//					{
//						// This check is not strictly needed as we don't do this yet, but... doing it anyway.
//						avgScore = comp.getExtCompScore();
//						if (avgScore == 0.0)
//						{
//							zeroCnt = 2;
//						}
//					}
//					else
//					{	
						// Be careful here... this always puts the override score in for
						// the final score... is this applicable in Raw data extract?			
						if(comp.getOverideScoreIndex() == 0)
						{
							// do it the regular way
							// Get the average score (N/A if less than 2, but FE passes through)
							double cum = 0;
							int cnt = 0;
							for (EGBarDisplayData bar : comp.getCompBars())
							{
								int score = bar.getBarScore();
								if (score > 0)
								{
									cum += (double)score;
									cnt++;
								}
								else if (score == 0)
								{
									zeroCnt++;
								}
							}
							// calc and save the score
							if (cnt > 0)
							{
								avgScore = (zeroCnt > 1) ? 0.0f : (cum / (double)cnt);
							}
						}
						else
						{
							// There is a non-zero value for override... use it (zeroCnt has no meaning here; overide assumes that the number of behaviors scored is irrelevant)
							avgScore = ((double)comp.getOverideScoreIndex() + 1.0) / 2.0;
						}
//					}
					
					ir.getDisplayData().put("EG" + i + "_Comp" + j + "_Score", zeroCnt > 1 ? "N/A" : "" + avgScore);
				}
				ir.getDisplayData().put("EG" + i + "_Comp_Count", "" + j);
			}
			ir.getDisplayData().put("EG_Count", "" + i);
			
			return ir;
		}
		catch (SQLException ex)
		{
			throw new Exception("SQL in addEGData.  " +
								"SQLException: " + ex.getMessage() + ", " +
								"SQLState: " + ex.getSQLState() + ", " +
								"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (rs != null)
			{
				try {  rs.close();  }
				catch (SQLException sqlEx) {    /*  Swallow the error */ }
				rs = null;
			}
			if (stmt != null)
			{
				try {  stmt.close();  }
				catch (SQLException sqlEx) {  /*  Swallow the error */  }
				stmt = null;
			}
		}
	}

	
	//
	// Instance data.
	//

	//
	// Constructors.
	//


	//
	// Instance methods.
	//


	
	/*
	 * Private class that holds data about a competency.  Used for accumulating scores.
	 */
	private class CompInfo
	{
		// Instance variables
		private int _posn;
		private int _cnt = 0;
		private double _cum = 0.0;
		
		public CompInfo(int pos)
		{
			_posn = pos;
		}
		
		// methods
		public int getPosition()
		{
			return _posn;
		}
		
		// There is no setCount method.  The count is bumped when a
		// valid score is added 
		public int getCount()
		{
			return _cnt;
		}
		
		// Adds the value to the cum bucket and bumps the count bucket per the
		// rules implemented in the UI (view.controls.home ~ 700 & ~715)
		public void addCum(double value)
		{
			if (value == -1.0)
				return;
			else if (value == 0.0)
				return;
			else
			{
				_cum += value;
				_cnt++;
			}
		}

		public double getCum()
		{
			return _cum;
		}
	}

}
