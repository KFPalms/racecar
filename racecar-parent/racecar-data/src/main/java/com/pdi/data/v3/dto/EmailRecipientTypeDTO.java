/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.dto;

import com.pdi.data.util.BaseDTO;
import com.pdi.data.util.RequestStatus;


/**
 * EmailRecipientTypeDTO is a class designed to convey the information for one
 * recipient type and its request status used in the PDI V3 extensions.
 * 
 * @author		Ken Beukelman
  */
public class EmailRecipientTypeDTO extends BaseDTO
{
	//
	// Static data.
	//


	//
	// Static methods.
	//


	//
	// Instance data.
	//
	private long _id;
	private String _name = null;


	//
	// Constructors.
	//
	public EmailRecipientTypeDTO()
	{
		super();
		// does nothing right now
	}
	
	// Status setup constructor - recipient type data remains unset
	public EmailRecipientTypeDTO(RequestStatus stat)
	{
		this.setStatus(stat);
	}
	
	
	//
	// Instance methods.
	//

	/**
	 * Generic toString method
	 */
	public String toString() {
		String str = "EmailRecipientTypeDTO - " +
					 "Status code=" + this.getStatus().getStatusCode() +
					 ", id=" + this.getId() +
					 ", name=>" + this.getName() + "<";
		return str;
	}
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public long getId()
	{
		return _id;
	}
	
	public void setId(long value)
	{
		_id = value;
	}	

	/*****************************************************************************************/
	public String getName()
	{
		return _name;
	}
	
	public void setName(String value)
	{
		_name = value;
	}	
}
