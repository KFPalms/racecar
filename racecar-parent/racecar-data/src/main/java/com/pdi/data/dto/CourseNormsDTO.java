/**
 * Copyright (c) 2013 Personnel Decisions International, Inc., A Korn/Ferry Company
 * All rights reserved.
 */
package com.pdi.data.dto;

/**
 * CourseNormDTO hold norm information about a course.  That includes the course abbreviation
 * and a genPop and specPop NormGroup ID.  Its primary usage is holding default norm information.
 */
public class CourseNormsDTO
{
	
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//	
	private String course;
	private int spNgId = 0;
	private int gpNgId = 0;

	
	//
	// Constructors.
	//
	
	// Hide the 0-param constructor
	@SuppressWarnings("unused")
	private CourseNormsDTO()
	{
		// Does nothing... And can't be invoked.
	}
	
	// One parm constructor
	public CourseNormsDTO(String crs)
	{
		this.course = crs;
	}
	
	//
	// Instance methods
	//
	
	//***************************************
	//            Getters & Setters
	//***************************************

	// No setter for course... set in the only exposed constructor
	
	public String getCourse()
	{
		return this.course;
	}


	public void setSpNgId(int val)
	{
		this.spNgId = val;
	}
	
	public void setSpNgId(String val)
	{
		try
		{
			this.spNgId = Integer.parseInt(val);
		}
		catch(NumberFormatException e)
		{
	       	System.out.println("WARNING - CourseNormsDTO.setSpNgId(): Unable to convert spNgId.  Inp=" + val + ".  msg=" + e.getLocalizedMessage());
			this.spNgId = 0;
		}
	}
	
	public int getSpNgId()
	{
		return this.spNgId;
	}
	
	public void setGpNgId(int val)
	{
		this.gpNgId = val;
	}
		
	public void setGpNgId(String val)
	{
		try
		{
			this.gpNgId = Integer.parseInt(val);
		}
		catch(NumberFormatException e)
		{
	       	System.out.println("WARNING - CourseNormsDTO.setSpNgId(): Unable to convert gpNgId.  Inp=" + val + ".  msg=" + e.getLocalizedMessage());
			this.gpNgId = 0;
		}
	}
	
	public int getGpNgId()
	{
		return this.gpNgId;
	}
}
