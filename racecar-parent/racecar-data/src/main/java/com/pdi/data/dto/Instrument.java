/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.dto;

import java.util.HashMap;


/**
 * Instrument is a basic data class that contains instrument data.
 * 
 * @author		Gavin Myers
 */
public class Instrument extends CoreDataObject
{
	//
	// Static data.
	//

	
	//
	// Static methods.
	//

	/**
	 * 
	 */
	private static final long serialVersionUID = 5618767471649527552L;
	//
	// Instance data.
	//
	
	private String code;  // this is the reporting constants *universal* code (unique in the database pdiinstrumentcode)
	private String description;
	private String shortName; // this is a short name used for Test Data tabs 
	//private String id;    // v2 module id (or 9th house, or whatever)
	
	private HashMap<String, String> metadata;
	//
	// Constructors.
	//
	public Instrument()
	{
		this.setMetadata(new HashMap<String, String>());
		// does nothing right now
	}

	//
	// Instance methods.
	//
	
	public String toString()
	{
		String ret = "Instrument:";	
		ret += "  Code=" + code;
		ret += ", Description=" + description;
		ret += ", Short Name=" + shortName;
		//ret += "  ID=" + id;
		
		return ret;
	}
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
//	public void setId(String value)
//	{
//		id = value;
//	}
//	
//	public String getId()
//	{
//		return id;
//	}

	/*****************************************************************************************/
	public void setShortName(String value)
	{
		shortName = value;
	}
	
	public String getShortName()
	{
		return shortName;
	}
	
	/*****************************************************************************************/
	public void setDescription(String value)
	{
		description = value;
	}
	
	public String getDescription()
	{
		return description;
	}

	/*****************************************************************************************/
	public void setCode(String value)
	{
		code = value;
	}
	
	public String getCode()
	{
		return code;
	}
	

	/*****************************************************************************************/
	public void setMetadata(HashMap<String, String> value)
	{
		metadata = value;
	}
	
	public HashMap<String, String> getMetadata()
	{
		return metadata;
	}
	
}
