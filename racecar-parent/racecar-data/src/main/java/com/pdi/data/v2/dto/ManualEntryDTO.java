/**
 * Copyright (c) 2008, 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v2.dto;

import java.util.ArrayList;

import com.pdi.data.v2.dataObjects.KeyValuePairStrings;

/**
 * ManualEntryDTO is a wrapper around a number of data items that are needed when 
 * the ManualEntry Vendor App is started
 * 
 * @author      MB Panichi
 * @author		Ken Beukelman
  */
public class ManualEntryDTO
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _projectId;
	private String _candidateId;
	private String _linkParamId;
	private String _firstName = "";
	private String _lastName = "";
	private ArrayList<KeyValuePairStrings> _moduleList;
	private ArrayList<ModConstsDTO> _modConstDTOList;
	private ArrayList<String> _adaptDataModuleList;  // used for the getAdaptData() function
	private String _moduleId;  // this is for the SHL vendor integration
	private String _manualEntryType; // this is for the Manual Entry application

	private Integer _adaptUserId;
	private Integer _adaptProjectId;
	private String _adaptFirstName = "";
	private String _adaptLastName = "";
	
	//
	// Constructors.
	//
	public ManualEntryDTO()
	{
		// Does nothing presently

	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
	

	//*****************************************************************************************/
	public String getProjectId()
	{
		return _projectId;
	}
	
	public void setProjectId(String value)
	{
		_projectId = value;
	}	

	//*****************************************************************************************/
	public String getCandidateId()
	{
		return _candidateId;
	}
	
	public void setCandidateId(String value)
	{
		_candidateId = value;
	}
	
	//*****************************************************************************************/
	public String getLinkParamId()
	{
		return _linkParamId;
	}
	
	public void setLinkParamId(String value)
	{
		_linkParamId = value;
	}
	
	//*****************************************************************************************/
	public String getFirstName()
	{
		return _firstName;
	}
	
	public void setFirstName(String value)
	{
		_firstName = value;
	}
	
	//*****************************************************************************************/
	public String getLastName()
	{
		return _lastName;
	}
	
	public void setLastName(String value)
	{
		_lastName = value;
	}

	//*****************************************************************************************/
	public ArrayList<KeyValuePairStrings> getModuleList()
	{
		if(_moduleList == null){
			_moduleList = new ArrayList<KeyValuePairStrings>();
		}
		return _moduleList;
	}
	
	public void setModuleList(ArrayList<KeyValuePairStrings> value)
	{
		_moduleList = value;
	}

	//*****************************************************************************************/
	public ArrayList<ModConstsDTO> getModConstDTOList()
	{
		if(_modConstDTOList == null){
			_modConstDTOList = new ArrayList<ModConstsDTO>();
		}
		return _modConstDTOList;
	}
	
	public void setModConstDTOList(ArrayList<ModConstsDTO> value)
	{
		_modConstDTOList = value;
	}
	
	//*****************************************************************************************/
	public ArrayList<String> getAdaptDataModuleList()
	{
		if(_adaptDataModuleList == null){
			_adaptDataModuleList = new ArrayList<String>();
		}
		return _adaptDataModuleList;
	}
	
	public void setAdaptDataModuleList(ArrayList<String> value)
	{
		_adaptDataModuleList = value;
	}
	//*****************************************************************************************/
	public String getModuleId()
	{
		return _moduleId;
	}
	
	public void setModuleId(String value)
	{
		_moduleId = value;
	}
	
	//*****************************************************************************************/
	public String getManualEntryType()
	{
		return _manualEntryType;
	}
	
	public void setManualEntryType(String value)
	{
		_manualEntryType = value;
	}	

	//*****************************************************************************************/
	public Integer getAdaptUserId()
	{
		return _adaptUserId;
	}
	
	public void setAdaptUserId(Integer value)
	{
		_adaptUserId = value;
	}
	
	//*****************************************************************************************/
	public Integer getAdaptProjectId()
	{
		return _adaptProjectId;
	}
	
	public void setAdaptProjectId(Integer value)
	{
		_adaptProjectId = value;
	}
	
	//*****************************************************************************************/
	public String getAdaptFirstName()
	{
		return _adaptFirstName;
	}
	
	public void setAdaptFirstName(String value)
	{
		_adaptFirstName = value;
	}
	
	//*****************************************************************************************/
	public String getAdaptLastName()
	{
		return _adaptLastName;
	}
	
	public void setAdaptLastName(String value)
	{
		_adaptLastName = value;
	}
}
