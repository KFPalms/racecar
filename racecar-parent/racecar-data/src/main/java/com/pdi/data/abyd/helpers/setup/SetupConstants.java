/**
 * Copyright (c) 2008 Personnel Decisions International, Inc.
 * All rights reserved.
 */

package com.pdi.data.abyd.helpers.setup;

/**
 * SetupConstants provides a spot for defined constants that can be used in
 * DNA Setup processing.  Currently defined with static data only.
 *
 * NOTE: This class has a companion set of definitions on the UI side
 *       (abd\setup\SetupConstants.as).  Every effort should be made
 *       to keep these files in sync.
 *
 * @author		Ken Beukelman
 */
public class SetupConstants
{
	//
	// Static data.
	//
		// Editable label types
	public static final int EDITABLE = 1;
	public static final int	NOT_EDITABLE = 2;
	public static final int ALL_LABELS = 3;

		// Entry State flag values - all Start with SS (Setup State)
	public static final transient int SS_DNA_INVALID = -1;		// Initial value for state; no checks made or all checks failed
	public static final transient int SS_DNA_NEW = 0;			// New DNA; no information is present (not even a dnaId)
	public static final transient int SS_DNA_STARTED = 1;		// DNA input started but not completed
	public static final transient int SS_DNA_SUBMITTED = 2;		// DNA submitted; no participants assigned
	public static final transient int SS_DNA_LOCKED = 3;		// DNA submitted and participants assigned
	
//		// The secret decoder ring for Success Profile text types
//	public static final transient int BUSINESS_CHALLENGES = 1;
//	public static final transient int LEADERSHIP_CHALLENGES = 2;
//	public static final transient int EXPERIENCE_REQUIREMENTS = 3;
//	public static final transient int MOTIVATORS_CAREER_DRIVERS = 4;
//	public static final transient int LEADERSHIP_STYLE_CULTURE = 5;
//	public static final transient int DERAILLERS = 6;
//	public static final transient int JOB_REQUIREMENTS = 7;
}
