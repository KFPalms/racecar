package com.pdi.data.nhn.util;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;


public  class NhnContentTokenUtil {

		 static final String MAKE_NEW_TOKEN = "makeNewContentToken ?, ?, ?";
		 static final String UPDATE_TOKEN = "updateContentToken ?";
		 private Integer usersId = null;
//		 CREATE  Proc dbo.makeNewContentToken ( 
//			     @pv_users_id  int =NULL ,
//			     @pv_content_id  varchar(50) =NULL,
//				@pv_company_id  int =NULL
//			                   ) 
			                   
		public static String makeNewContentToken(String usersId, int companyId, String contentId){
			Connection con = null;
			CallableStatement proc_stmt = null;
			String returnString = null;
			ResultSet rs  = null;
			//System.out.println("Making new Content Token.  User: " + usersId + "   Company: " + companyId + "   Content: " + contentId);
			 try 
			 {
				 con = NhnDatabaseUtils.getDBConnection();
				 proc_stmt = con.prepareCall("{ call platform.dbo.makeNewContentToken(?,?,?) }");
				 proc_stmt.setInt(1, Integer.parseInt(usersId));
				 proc_stmt.setString(2, contentId);
				 proc_stmt.setInt(3, companyId);
			      rs = proc_stmt.executeQuery();
			      
			      if (rs != null && rs.isBeforeFirst()){ 
				      while (rs.next()) {
				    	  
				    	  returnString = rs.getString("guid");
				      } 
			      }else {
					System.out.println("Failed to create content Token for user: " + usersId);
				  }
			    } catch (SQLException e1) {
			    	System.out.println("SQLException: Failed to create content Token for user: " + usersId);
			    	//e1.printStackTrace();
			    }catch (Exception e) {
			    	System.out.println("Exception: Failed to create content Token for user: " + usersId);
				     // e.printStackTrace();
				 }  finally {
			      try {
			    	  proc_stmt.close();
			      } catch(Exception e) {}
			      try {
			        con.close();
			      } catch(Exception e) {}
			    }
			//System.out.println("Returning Content Token: " + returnString + " \nUser: " + usersId);
			return returnString;
		}
		
		
	}
