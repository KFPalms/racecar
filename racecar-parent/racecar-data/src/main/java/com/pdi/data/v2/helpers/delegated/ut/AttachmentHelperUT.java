package com.pdi.data.v2.helpers.delegated.ut;

import junit.framework.TestCase;

public class AttachmentHelperUT extends TestCase {

	public AttachmentHelperUT(String name)
	{
		super(name);
	}

	// This functionality has been moved to the Admin tools
	
//	public void testAddNewParticipantAttachments()
//	throws Exception
//	{
//		throw new Exception("AttachmentHelper functionality is not operative (V2)");
//
//		//SessionUser session = new SessionUser();
//		//session.getMetadata().put("USERNAME", "cdunn");
//		//session.getMetadata().put("PASSWORD", "cdunn");
//		//
//		//// add this so that JDBC works in UT's
//		//V2DatabaseUtils.setUnitTest(true);
//		//
//		//String participantId = "IUTMFZDL";  // mb test abyd 4       09-28-2010
//		//
//		////INoteHelper iNote = HelperDelegate.getNoteHelper(PropertyLoader.getProperty("com.pdi.data.v2.application", "datasource.v2"));
//		//IAttachmentHelper iAttch = HelperDelegate.getAttachmentHelper();
//		//AttachmentHolder holder = new AttachmentHolder();
//		//ArrayList<Attachment> list = new ArrayList<Attachment>();
//		//
//		//// set this up with NEW ATTACHMENTS	
//		//holder.setParticipant(HelperDelegate.getParticipantHelper().fromId(session, participantId));
//		////holder.setParticipantId(participantId);
//		//Attachment a1 = new Attachment();
//		//a1.setAttachmentId(0);
//		//a1.setAttachmentUrl("attachment1.doc");
//		//a1.setAttachmentTypeId(1);
//		//list.add(a1);
//		//Attachment a2 = new Attachment();
//		//a2.setAttachmentId(0);
//		//a2.setAttachmentUrl("attachment2.pdf");
//		//a2.setAttachmentTypeId(2);
//		//list.add(a2);
//		//Attachment a3 = new Attachment();
//		//a3.setAttachmentId(0);
//		//a3.setAttachmentUrl("attachment3.xls");
//		//a3.setAttachmentTypeId(3);
//		//list.add(a3);
//		//holder.setAttachments(list);
//		//
//		//// add new attachments to db
//		//iAttch.modify(holder);
//		//
//		//// grab the attachments, and the type list
//		//AttachmentHolder attachmentHolder = iAttch.fromParticipantId(session, participantId);
//		//
//		//System.out.println("ADDED ATTACHMENTS.... ");
//		//for(Attachment attachment : attachmentHolder.getAttachments()) {
//		//	System.out.println(attachment.getAttachmentId() + " - " + attachment.getAttachmentTypeId() + " - " + attachment.getAttachmentUrl());
//		//}
//		//
//		//// 	NOW SET UP FOR UPDATES
//		//for(Attachment attachment : attachmentHolder.getAttachments()) {		
//		//	attachment.setAttachmentTypeId(6);  // reset the attachment type to 6 for all
//		//}
//		//
//		//// do the updates by "adding" updated attachments to db
//		//iAttch.modify(holder);
//		//
//		//// grab the attachments to display:
//		//attachmentHolder = iAttch.fromParticipantId(session, participantId);	
//		//System.out.println("UPDATED ATTACHMENTS.... ");
//		//for(Attachment attachment : attachmentHolder.getAttachments()) {
//		//	System.out.println(attachment.getAttachmentId() + " - " + attachment.getAttachmentTypeId() + " - " + attachment.getAttachmentUrl());
//		//}
//		//
//		//// 	NOW SET UP FOR DELETES
//		//for(Attachment attachment : attachmentHolder.getAttachments()) {		
//		//	attachment.setAttachmentActive(false);  // reset to inactive/deleted
//		//}
//		//
//		//// do the delete:
//		//iAttch.delete(attachmentHolder.getAttachments());
//		//// grab the attachments to display:
//		//attachmentHolder = iAttch.fromParticipantId(session, participantId);
//		//System.out.println("DELETED ATTACHMENTS.... ");
//		//for(Attachment attachment : attachmentHolder.getAttachments()) {
//		//	System.out.println(attachment.getAttachmentId() + " - " + attachment.getAttachmentActive());
//		//	// if they've been set to deleted, the fetch will not get them.  so this should have no output.
//		//}		

//	}

	
}


