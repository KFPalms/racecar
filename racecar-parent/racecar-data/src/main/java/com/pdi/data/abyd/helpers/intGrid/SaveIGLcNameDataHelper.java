/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.intGrid;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import com.pdi.data.abyd.dto.intGrid.IGLcNameDTO;


/**
 * SaveIGLcNameHelper contains the code needed to save the lead consultant
 * name as entered by the lead consultant for the Integration Grid.
 *
 * @author		Ken Beukelman
 */
public class SaveIGLcNameDataHelper
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private Connection con;
	private String participantId;
	private long dnaId;
	private String fName;
	private String lName;

	//
	//
	// Constructors.
	//
	public SaveIGLcNameDataHelper(Connection con, IGLcNameDTO name)
		throws Exception
	{
		if (name == null)
		{
			throw new Exception("IGLcNameDTO required to save name");
		}
		if (name.getPartId() == null || name.getPartId().length() == 0 ||
			name.getDnaId() == 0)
		{
			throw new Exception("Invalid Name Data key:  part=" +  (name.getPartId() == null ? "null" : (name.getPartId().length() == 0 ? "<empty string>" : name.getPartId())) +
					            ", dna=" + name.getDnaId());
		}

			this.con = con;
			this.participantId = name.getPartId();
			this.dnaId = name.getDnaId();
			this.fName = name.getLcFirstName();
			this.lName = name.getLcLastName();
	}

	//
	// Instance methods.
	//

	/**
	 * Insert or update the lead consultant name.  Returns nothing.
	 *
	 * @throws Exception
	 */
	public void saveLcName()
		throws Exception
	{
		// Change any double quotes to single quotes
		// This prevents "Bad SQL" messages for strings that include double quotes
		// UI ensures that both exist
		this.fName = this.fName.trim().replace("'","''");
		this.lName = this.lName.trim().replace("'","''");


		Statement stmt = null;
		StringBuffer sqlQuery = new StringBuffer();
		
		sqlQuery.append("INSERT INTO pdi_abd_igrid_response ");
		sqlQuery.append("  (participantId,dnaId,leadFirstName,leadLastName,lastUser,lastDate) ");
		sqlQuery.append("  VALUES(" + this.participantId + ",");
		sqlQuery.append("         " + this.dnaId + ",");
		sqlQuery.append("         N'" + this.fName + "',");
		sqlQuery.append("         N'" + this.lName + "',");
		sqlQuery.append("         SYSTEM_USER,");
		sqlQuery.append("         GETDATE() )");

		try
		{
			// Do the insert
			stmt = this.con.createStatement();
			stmt.executeUpdate(sqlQuery.toString());
		}
		catch (SQLException ex)
		{
			// TODO Refactor so that the update does not depend upon the error code (do a query)
			// Duplicate key error triggers update; error code = 1062 on MySQL, 2627 on SQL Server
			if (ex.getErrorCode() == 2627)
			{
				// It's already there... update it
				try  {  stmt.close();  }
				catch (SQLException sqlEx)
				{
			    	System.out.println("ERROR: stmt close in saveLcName-update - " + sqlEx.getMessage());	
				}
				stmt = null;
				sqlQuery.setLength(0);
				sqlQuery.append("UPDATE pdi_abd_igrid_response ");
				sqlQuery.append("  SET leadFirstName = N'" + this.fName + "',");
				sqlQuery.append("      leadLastName = N'" + this.lName + "',");
				sqlQuery.append("      lastUser = SYSTEM_USER,");
				sqlQuery.append("      lastDate = GETDATE()");
				sqlQuery.append("  WHERE participantId = " + this.participantId + " ");
				sqlQuery.append("    AND dnaId = " + this.dnaId);

				try
				{
					stmt = this.con.createStatement();
					stmt.executeUpdate(sqlQuery.toString());
				}
				catch (SQLException ex2)
				{
					throw new Exception("SQL saveLcName (update).  " +
							"SQLException: " + ex2.getMessage() + ", " +
							"SQLState: " + ex2.getSQLState() + ", " +
							"VendorError: " + ex2.getErrorCode());
				}
			}
			else
			{
				throw new Exception("SQL saveScore (insert).  " +
									"SQLException: " + ex.getMessage() + ", " +
									"SQLState: " + ex.getSQLState() + ", " +
									"VendorError: " + ex.getErrorCode());
			}
		}
		finally
		{
			if (stmt != null)
			{
				try  {  stmt.close();  }
				catch (SQLException sqlEx)
				{
			    	System.out.println("ERROR: stmt close in saveLcName - " + sqlEx.getMessage());	
				}
				stmt = null;
			}
		}
	}
}
