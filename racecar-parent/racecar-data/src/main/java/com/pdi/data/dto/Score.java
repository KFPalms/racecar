package com.pdi.data.dto;

public class Score  extends CoreDataObject{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8736874614866429736L;
	
	// Scale type constants
	public static final int INTERNAL = 0;
	public static final int EDITABLE = 1;

	
	//Reference of the item in the row, scoreId - Auto-increment
	private String id;
	//Anytime you use a code it should come from a constants/dictionary table
	// Usually used for spss value
	private String code;
	
	private String scaleName;
	
	private String rawScore;

	// Constructor
	public Score() {
		
	}
	public String toString()
	{
		
		//ProjectParticipantInstrument epi = new ProjectParticipantInstrument();

		
			String ret = "Score:";
			ret += "\n  " + (id == null ? "ID null" : id.toString());
			ret += "\n  " + (code == null ? "Code null" : code.toString());
			ret += "\n  " + (rawScore == null ? "Raw null" : rawScore.toString());

			return ret;
	}
	
	
	
	public void setCode(String code) {
		this.code = code;
	}
	public String getCode() {
		return code;
	}
	
	public void setScaleName(String value) {
		scaleName = value;
	}
	public String getScaleName() {
		return scaleName;
	}

	public void setRawScore(String rawScore) {
		this.rawScore = rawScore;
	}
	public String getRawScore() {
		return rawScore;
	}
	public Double scoreAsNumber() {
		Double dbl = null;
		if (this.rawScore == null  || this.rawScore.isEmpty())
		{
			dbl = 0.0;
		}
		else
		{
			//dbl = Double.parseDouble(this.getRawScore());
			dbl = Double.valueOf(this.getRawScore());
		}
		return dbl;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public String getId() {
		return id;
	}

	
}
