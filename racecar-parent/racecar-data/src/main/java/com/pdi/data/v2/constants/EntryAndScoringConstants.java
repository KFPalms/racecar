/**
 * Copyright (c) 2008 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v2.constants;

import java.util.ArrayList;

/**
 * 	Constants used in fetching score data from external vendors
 * 
 * @author mbpanichi
 */
public class EntryAndScoringConstants
{
	//
	// Static data.
	//
	
	// Array lengths
	public static final int cpiScoreCount = 30;
	public static final int easScoreCount = 3;
	public static final int shlScoreCount = 1;
	
	// Array indices
		// CPI
		// Note that the indices are one less than their position as defined in staticConstructDataArray
	public static final transient int CPI_REL = 0;		// CPI Reliability
	public static final transient int CPI_GENDER = 1;	// CPI Gender
	public static final transient int CPI_TYPE = 2;		// CPI Type
	public static final transient int CPI_DO = 3;		// CPI DO
	public static final transient int CPI_CS = 4;		// CPI CS
	public static final transient int CPI_SY = 5;		// CPI SY
	public static final transient int CPI_SP = 6;		// CPI SP
	public static final transient int CPI_SA = 7;		// CPI SA
	public static final transient int CPI_IN = 8;		// CPI IN
	public static final transient int CPI_EM = 9;		// CPI EM
	public static final transient int CPI_RE = 10;		// CPI RE
	public static final transient int CPI_SO = 11;		// CPI SO
	public static final transient int CPI_SC = 12;		// CPI SC
	public static final transient int CPI_GI = 13;		// CPI GI
	public static final transient int CPI_CM = 14;		// CPI CM
	public static final transient int CPI_WB = 15;		// CPI WB
	public static final transient int CPI_TO = 16;		// CPI TO
	public static final transient int CPI_AC = 17;		// CPI AC
	public static final transient int CPI_AI = 18;		// CPI AI
	public static final transient int CPI_IE = 19;		// CPI IE
	public static final transient int CPI_PY = 20;		// CPI PY
	public static final transient int CPI_FX = 21;		// CPI FX
	public static final transient int CPI_FM = 22;		// CPI FM
	public static final transient int CPI_MP = 23;		// CPI MP
	public static final transient int CPI_WO = 24;		// CPI WO
	public static final transient int CPI_CT = 25;		// CPI CT
	public static final transient int CPI_LP = 26;		// CPI LP
	public static final transient int CPI_AMI = 27;		// CPI AMI
	public static final transient int CPI_LEO = 28;		// CPI LEO
	public static final transient int CPI_TM = 29;		// CPI TM
		// EAS
	public static final transient int EAS1 = 0;
	public static final transient int EAS6 = 1;
	public static final transient int EAS7 = 2;
		//SHL (Verify)
	public static final transient int SHL = 0;

	public static final transient ArrayList<String> VENDOR_TESTING_MODULES = new ArrayList<String>();
	static
	{
		//M05=HSJRIKLO { CPI } 
		//M06=DTYAQNYJ { Vendor Link - EAS - PSI } 
		//M07=IZFTQXVI { Vendor Link - Verify - SHL } 
		VENDOR_TESTING_MODULES.add("HSJRIKLO");
		VENDOR_TESTING_MODULES.add("DTYAQNYJ");
		VENDOR_TESTING_MODULES.add("IZFTQXVI");
	};
	public static final transient int CPI_IDX = 0;
	public static final transient int EAS_IDX = 1;
	public static final transient int SHL_IDX = 2;

	
	/*
	 * 
	 * 
	 * 
	 */
	public static final Object[][] staticConstructDataArray = new Object[][]
	{
		//				 	construct id (0)			construct name (1)		sequence (2)	moduleId (3)
			(new Object [] { new String("ISSTJWID"),	new String("EAS1"),	new Integer(1),	new String("DTYAQNYJ") } ),	// EAS	
			(new Object [] { new String("CQRGVGDB"),	new String("EAS6"),	new Integer(2),	new String("DTYAQNYJ") } ),		
			(new Object [] { new String("EZZXXQIF"),	new String("EAS7"),	new Integer(3),	new String("DTYAQNYJ") } ),	
			
			(new Object [] { new String("KBPMPMRE"),	new String("Verify"),	new Integer(1),	new String("IZFTQXVI") } ),	// Verify	

			(new Object [] { new String("LLZZFUIE"),	new String("CPI Reliability"),	new Integer(1),	new String("HSJRIKLO") } ),		
			(new Object [] { new String("HOZJAOQZ"),	new String("CPI Gender"),		new Integer(2), new String("HSJRIKLO") } ),	
			(new Object [] { new String("DUCNXIIU"),	new String("CPI Type"),			new Integer(3),	new String("HSJRIKLO") } ),		
			(new Object [] { new String("DVAREUYD"),	new String("CPI DO"),	new Integer(4),	new String("HSJRIKLO") } ),		
			(new Object [] { new String("HNANGRCT"),	new String("CPI CS"),	new Integer(5),	new String("HSJRIKLO") } ),		
			(new Object [] { new String("HTAXOLWI"),	new String("CPI SY"),	new Integer(6), new String("HSJRIKLO") } ), // CPI	
			(new Object [] { new String("IVFNVVYQ"),	new String("CPI SP"),	new Integer(7),	new String("HSJRIKLO") } ),
			(new Object [] { new String("DURXDROO"),	new String("CPI SA"),	new Integer(8),	new String("HSJRIKLO") } ),		
			(new Object [] { new String("CNMBHCQL"),	new String("CPI IN"),	new Integer(9),	new String("HSJRIKLO") } ),		
			(new Object [] { new String("FCJNTWXI"),	new String("CPI EM"),	new Integer(10),	new String("HSJRIKLO") } ),
			(new Object [] { new String("FFWYCPSQ"),	new String("CPI RE"),	new Integer(11),	new String("HSJRIKLO") } ),		
			(new Object [] { new String("LKTBHTXO"),	new String("CPI SO"),	new Integer(12),	new String("HSJRIKLO") } ),		
			(new Object [] { new String("BMVSWTAL"),	new String("CPI SC"),	new Integer(13),	new String("HSJRIKLO") } ),	
			(new Object [] { new String("DUNFVGUA"),	new String("CPI GI"),	new Integer(14),	new String("HSJRIKLO") } ),		
			(new Object [] { new String("KCKHSHUD"),	new String("CPI CM"),	new Integer(15),	new String("HSJRIKLO") } ),		
			(new Object [] { new String("FFIEQYVH"),	new String("CPI WB"),	new Integer(16),	new String("HSJRIKLO") } ),				
			(new Object [] { new String("CNACMYBF"),	new String("CPI TO"),	new Integer(17),	new String("HSJRIKLO") } ),		
			(new Object [] { new String("HMGQUHSX"),	new String("CPI AC"),	new Integer(18),	new String("HSJRIKLO") } ),		
			(new Object [] { new String("FDHTEEEQ"),	new String("CPI AI"),	new Integer(19),	new String("HSJRIKLO") } ),	
			(new Object [] { new String("BKCDZIRI"),	new String("CPI IE"),	new Integer(20),	new String("HSJRIKLO") } ),		
			(new Object [] { new String("DXTTTKVO"),	new String("CPI PY"),	new Integer(21),	new String("HSJRIKLO") } ),		
			(new Object [] { new String("HOZBMIBO"),	new String("CPI FX"),	new Integer(22),	new String("HSJRIKLO") } ),		
			(new Object [] { new String("HSHOBDDG"),	new String("CPI FM"),	new Integer(23),	new String("HSJRIKLO") } ),		
			(new Object [] { new String("ITFUGMOX"),	new String("CPI MP"),	new Integer(24),	new String("HSJRIKLO") } ),		
			(new Object [] { new String("IXONGOZU"),	new String("CPI WO"),	new Integer(25),	new String("HSJRIKLO") } ),		
			(new Object [] { new String("KFBNCYWX"),	new String("CPI CT"),	new Integer(26),	new String("HSJRIKLO") } ),		
			(new Object [] { new String("KESEXPCS"),	new String("CPI LP"),	new Integer(27),	new String("HSJRIKLO") } ),		
			(new Object [] { new String("FFWORDYJ"),	new String("CPI AMI"),	new Integer(28),	new String("HSJRIKLO") } ),		
			(new Object [] { new String("DVOAZEZI"),	new String("CPI LEO"),	new Integer(29),	new String("HSJRIKLO") } ),			
			(new Object [] { new String("FBMHLHPK"),	new String("CPI TM"),	new Integer(30),	new String("HSJRIKLO") } ),		
	};                                                         		

}

