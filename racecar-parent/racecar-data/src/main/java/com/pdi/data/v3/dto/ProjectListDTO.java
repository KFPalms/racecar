/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.dto;

import java.util.ArrayList;

import com.pdi.data.util.BaseDTO;
import com.pdi.data.util.RequestStatus;

/**
 * ProjectListDTO is designed to convey the information for a number of projects.
 * It contains an ArrayList of ProjectDTO objects; the status in each of the
 * individual elements describes the state of that element.
 * 
 * @author		Ken Beukelman
  */
public class ProjectListDTO extends BaseDTO
{
	//
	// Static data.
	//


	//
	// Static methods.
	//


	//
	// Instance data.
	//
	private ArrayList<ProjectDTO> _projectList = new ArrayList<ProjectDTO>();



	//
	// Constructors.
	//
	public ProjectListDTO()
	{
		super();
		// does nothing right now
	}
	
	// Set up a new DTO with status but no data
	public ProjectListDTO(RequestStatus stat)
	{
		super(stat);
	}
	
	// Set up a new DTO with data (assumes no data problems)
	// It does check for data
	public ProjectListDTO(ArrayList<ProjectDTO> ary)
	{
		super();
		this.setProjectList(ary);
		if (ary.size() < 1)
		{
			this.getStatus().setStatusCode(RequestStatus.WB_NO_DATA);
		}
	}

	
	//
	// Instance methods.
	//

	/**
	 * Generic toString method
	 */
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append("ProjectListDTO:  ");
		sb.append("Status code=" + this.getStatus().getStatusCode() + ", count=" + _projectList.size() + "\n");
		for(int i=0; i < _projectList.size(); i++)
		{
			sb.append(_projectList.get(i).toString());
		}
		return sb.toString();
	}
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public ArrayList<ProjectDTO> getProjectList()
	{
		return _projectList;
	}
	
	public void setProjectList(ArrayList<ProjectDTO> value)
	{
		_projectList = value;
	}	
}
