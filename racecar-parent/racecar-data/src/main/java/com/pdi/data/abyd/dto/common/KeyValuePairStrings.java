/**
 * Copyright (c) 2008 Personnel Decisions International, Inc.
 * All rights reserved.
 *
 * $Header: //com/pdicorp/domain/dataObjects/KeyValuePairStrings.java 6     8/25/05 3:23p Kbeukelm $
 */
package com.pdi.data.abyd.dto.common;



/**
 * KeyValuePairStrings is a container class for key/value pairs (e.g., HRA Varchar id and name)
 * 
 * Note that while there is a corresponding class on the AS side, the data is
 * indistinguishable from a native AS3 Object and is, therefore mapped to that
 * when it is transported to Flex.
 *
 * @author      mpanichi
 * @author		Ken Beukelman
 */
public class KeyValuePairStrings
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
		private String _theKey;
		private String _theValue;


		//
		// Constructors.
		//
	public KeyValuePairStrings()
	{
		// Does nothing presently
	}
	
	//
	// Instance methods.
	//

	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
		
	/*****************************************************************************************/
	public String getTheKey()
	{
		return _theKey;
	}

	public void setTheKey(String value)
	{
		_theKey = value;
	}

	/*****************************************************************************************/
	public String getTheValue()
	{
		return _theValue;
	}

	public void setTheValue(String value)
	{
		_theValue = value;
	}

}
