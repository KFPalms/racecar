package com.pdi.data.v2.ut;

//import com.pdi.data.constants.V2InstrumentConstants;
//import com.pdi.data.v2.helpers.SaveConstructDataHelper;
//import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.properties.PropertyLoader;
//import com.pdi.reporting.constants.ReportingConstants;
//import com.pdi.reporting.report.IndividualReport;
//import com.pdi.reporting.report.ReportData;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

public class SaveConstructDataHelperUT extends TestCase{

	public SaveConstructDataHelperUT(String name)
	{
		super(name);
	}
	
	public void testProperties()
	throws Exception
	{
		UnitTestUtils.start(this);
		
		//Does the local property work?
		String applicationId = PropertyLoader.getProperty("com.pdi.data.v2.application", "application.code");
		assertEquals(applicationId != null, true);
		assertEquals(applicationId.equals("PDI-DATA-V2"), true);
		
		UnitTestUtils.stop(this);
	}
	
//	public void testLogin()
//	throws Exception
//	{
//		UnitTestUtils.start(this);
//		
//		String username = "cdunn";
//		String password = "cdunn";
//		V2WebserviceUtils v2web = new V2WebserviceUtils();
//		SessionUserDTO user = v2web.registerUser(username, password);
//		System.out.println("The user name is " + user.getName());
//		
//		UnitTestUtils.stop(this);
//	}
	
//	public void testSaveConstructDataHelper() 
//		throws Exception 
//	{
//		
//		UnitTestUtils.start(this);
//		
//		V2DatabaseUtils.setUnitTest(true);
//		
////		String username = "cdunn";
////		String password = "cdunn";
////		V2WebserviceUtils v2web = new V2WebserviceUtils();
////		SessionUserDTO sessionUser = v2web.registerUser(username, password);
////		//System.out.println("The user name is " + sessionUser.getName());
//		
//
///*
// * Need to populate a ManualEntryDTO to use... going to
// * grab the data from the one in the ManualEntryDataHelperUT.
// * 
// * 		
// */
//		ManualEntryDTO lpDto = new ManualEntryDTO();
//		lpDto.setCandidateId("HNKGFQFW");
//		lpDto.setProjectId("KECLMUBZ");
//		lpDto.setFirstName("MB");
//		lpDto.setLastName("vendor test 03-23-09a");
//		lpDto.setManualEntryType("none");
//		
//		ArrayList<KeyValuePairStrings> moduleList = new ArrayList<KeyValuePairStrings>();
//		KeyValuePairStrings kvps = new KeyValuePairStrings();
////		kvps.setTheKey("HSJRIKLO");
////		kvps.setTheValue("CPI - California Psychological Inventory - Form 434");
////		moduleList.add(kvps);
////		kvps.setTheKey("DTYAQNYJ");
////		kvps.setTheValue("Vendor Link - EAS - PSI");
////		moduleList.add(kvps);
////		kvps.setTheKey("IZFTQXVI");
////		kvps.setTheValue("Vendor Link - Verify - SHL");	
//		
//		kvps.setTheKey("FFGLQDEG");
//		kvps.setTheValue("Vendor Link - Checkbox");
//		moduleList.add(kvps);
//		
//		lpDto.setModuleList(moduleList);
//		
//		//lpDto.setModConstDTOList(ArrayList<ModConstsDTO> value)
//		ArrayList<ModConstsDTO> modConstDTOList = new ArrayList<ModConstsDTO>();
//		ArrayList<ConstructDTO> constructsArrayList = new ArrayList<ConstructDTO>();
//		ModConstsDTO mcDto = new ModConstsDTO();
//		ConstructDTO cDto = new ConstructDTO();    
////			mcDto.setModuleId("DTYAQNYJ");
////			mcDto.setModuleName("Vendor Link - EAS - PSI");	
////			mcDto.setHasScores(false);
////				cDto.setConstructId("ISSTJWID");
////				cDto.setConstructName("EAS1");
////				cDto.setConstructScore("2.0");
////				cDto.setConstructScore2("2.0");
////				constructsArrayList.add(cDto);
////				cDto.setConstructId("CQRGVGDB");
////				cDto.setConstructName("EAS6");
////				cDto.setConstructScore("0.0");
////				cDto.setConstructScore2("0.0");
////				constructsArrayList.add(cDto);
////				cDto.setConstructId("EZZXXQIF");
////				cDto.setConstructName("EAS7");
////				cDto.setConstructScore("4.5");
////				cDto.setConstructScore2("4.5");
////				constructsArrayList.add(cDto);						
////			mcDto.setConstructsAry(constructsArrayList);
////			modConstDTOList.add(mcDto);
////			
////			mcDto.setModuleId("IZFTQXVI");
////			mcDto.setModuleName("Vendor Link - Verify - SHL");
////			mcDto.setHasScores(false);
////				cDto.setConstructId("KBPMPMRE");
////				cDto.setConstructName("Verify");
////				cDto.setConstructScore("");
////				cDto.setConstructScore2("");
////				constructsArrayList.add(cDto);
////			mcDto.setConstructsAry(constructsArrayList);
////			modConstDTOList.add(mcDto);
////			
////			mcDto.setModuleId("HSJRIKLO");
////			mcDto.setModuleName("CPI - California Psychological Inventory - Form 434");
////			mcDto.setHasScores(false);
////				cDto.setConstructId("LLZZFUIE");
////				cDto.setConstructName("CPI Reliability");
////				cDto.setConstructScore("1");
////				cDto.setConstructScore2("1");			
////				constructsArrayList.add(cDto);
////				cDto.setConstructId("HOZJAOQZ");
////				cDto.setConstructName("CPI Gender");
////				cDto.setConstructScore("1");
////				cDto.setConstructScore2("1");			
////				constructsArrayList.add(cDto);
////				cDto.setConstructId("DUCNXIIU");
////				cDto.setConstructName("CPI Type");
////				cDto.setConstructScore("1");
////				cDto.setConstructScore2("1");			
////				constructsArrayList.add(cDto);
////				cDto.setConstructId("DVAREUYD");
////				cDto.setConstructName("CPI DO");
////				cDto.setConstructScore("19");
////				cDto.setConstructScore2("19");			
////				constructsArrayList.add(cDto);
////				cDto.setConstructId("HNANGRCT");
////				cDto.setConstructName("CPI CS");
////				cDto.setConstructScore("10");
////				cDto.setConstructScore2("10");			
////				constructsArrayList.add(cDto);			
////				cDto.setConstructId("HTAXOLWI");
////				cDto.setConstructName("CPI SY");
////				cDto.setConstructScore("19");
////				cDto.setConstructScore2("19");			
////				constructsArrayList.add(cDto);			
////				cDto.setConstructId("IVFNVVYQ");
////				cDto.setConstructName("CPI SP");
////				cDto.setConstructScore("20");
////				cDto.setConstructScore2("20");
////				constructsArrayList.add(cDto);
////				cDto.setConstructId("DURXDROO");
////				cDto.setConstructName("CPI SA");
////				cDto.setConstructScore("13");
////				cDto.setConstructScore2("13");			
////				constructsArrayList.add(cDto);
////				cDto.setConstructId("CNMBHCQL");
////				cDto.setConstructName("CPI IN");
////				cDto.setConstructScore("8");
////				cDto.setConstructScore2("8");		
////				constructsArrayList.add(cDto);
////				cDto.setConstructId("FCJNTWXI");
////				cDto.setConstructName("CPI EM");
////				cDto.setConstructScore("20");
////				cDto.setConstructScore2("20");			
////				constructsArrayList.add(cDto);
////				cDto.setConstructId("FFWYCPSQ");
////				cDto.setConstructName("CPI RE");
////				cDto.setConstructScore("15");
////				cDto.setConstructScore2("15");			
////				constructsArrayList.add(cDto);
////				cDto.setConstructId("LKTBHTXO");
////				cDto.setConstructName("CPI SO");
////				cDto.setConstructScore("18");
////				cDto.setConstructScore2("18");
////				constructsArrayList.add(cDto);
////				cDto.setConstructId("BMVSWTAL");
////				cDto.setConstructName("CPI SC");
////				cDto.setConstructScore("4");
////				cDto.setConstructScore2("4");			
////				constructsArrayList.add(cDto);
////				cDto.setConstructId("DUNFVGUA");
////				cDto.setConstructName("CPI GI");
////				cDto.setConstructScore("12");
////				cDto.setConstructScore2("12");
////				constructsArrayList.add(cDto);
////				cDto.setConstructId("KCKHSHUD");
////				cDto.setConstructName("CPI CM");
////				cDto.setConstructScore("19");
////				cDto.setConstructScore2("19");			
////				constructsArrayList.add(cDto);
////				cDto.setConstructId("FFIEQYVH");
////				cDto.setConstructName("CPI WB");
////				cDto.setConstructScore("6");
////				cDto.setConstructScore2("6");			
////				constructsArrayList.add(cDto);
////				cDto.setConstructId("CNACMYBF");
////				cDto.setConstructName("CPI TO");
////				cDto.setConstructScore("7");
////				cDto.setConstructScore2("7");			
////				constructsArrayList.add(cDto);
////				cDto.setConstructId("HMGQUHSX");
////				cDto.setConstructName("CPI AC");
////				cDto.setConstructScore("16");
////				cDto.setConstructScore2("16");			
////				constructsArrayList.add(cDto); 
////				cDto.setConstructId("FDHTEEEQ");
////				cDto.setConstructName("CPI AI");
////				cDto.setConstructScore("8");
////				cDto.setConstructScore2("8");			
////				constructsArrayList.add(cDto);
////				cDto.setConstructId("BKCDZIRI");
////				cDto.setConstructName("CPI IE");
////				cDto.setConstructScore("13");
////				cDto.setConstructScore2("13");			
////				constructsArrayList.add(cDto);
////				cDto.setConstructId("DXTTTKVO");
////				cDto.setConstructName("CPI PY");
////				cDto.setConstructScore("10");
////				cDto.setConstructScore2("10");			
////				constructsArrayList.add(cDto);
////				cDto.setConstructId("HOZBMIBO");
////				cDto.setConstructName("CPI FX");
////				cDto.setConstructScore("6");
////				cDto.setConstructScore2("6");
////				constructsArrayList.add(cDto);
////				cDto.setConstructId("HSHOBDDG");
////				cDto.setConstructName("CPI FM");
////				cDto.setConstructScore("15");
////				cDto.setConstructScore2("15");			
////				constructsArrayList.add(cDto);
////				cDto.setConstructId("ITFUGMOX");
////				cDto.setConstructName("CPI MP");
////				constructsArrayList.add(cDto);
////				cDto.setConstructScore("8");
////				cDto.setConstructScore2("8");
////				cDto.setConstructId("IXONGOZU");
////				cDto.setConstructName("CPI WO");
////				constructsArrayList.add(cDto);
////				cDto.setConstructScore("10");
////				cDto.setConstructScore2("10");
////				cDto.setConstructId("FFWYCPSQ");
////				cDto.setConstructName("CPI CT");
////				constructsArrayList.add(cDto);
////				cDto.setConstructScore("12");
////				cDto.setConstructScore2("12");
////				cDto.setConstructId("KESEXPCS");
////				cDto.setConstructName("CPI LP");
////				constructsArrayList.add(cDto);
////				cDto.setConstructScore("25");
////				cDto.setConstructScore2("25");
////				cDto.setConstructId("FFWORDYJ");
////				cDto.setConstructName("CPI AMI");
////				constructsArrayList.add(cDto);
////				cDto.setConstructScore("9");
////				cDto.setConstructScore2("9");
////				cDto.setConstructId("DVOAZEZI");
////				cDto.setConstructName("CPI LEO");
////				constructsArrayList.add(cDto);
////				cDto.setConstructScore("17");
////				cDto.setConstructScore2("17");
////				cDto.setConstructId("FBMHLHPK");
////				cDto.setConstructName("CPI TM");
////				constructsArrayList.add(cDto);
////				cDto.setConstructScore("8");
////				cDto.setConstructScore2("8");
////			mcDto.setConstructsAry(constructsArrayList);
////			modConstDTOList.add(mcDto);	
//			
//			
//			mcDto.setModuleId("FFGLQDEG");
//			mcDto.setModuleName("Vendor Link - Checkbox");	
//			mcDto.setHasScores(false);
//			cDto = new ConstructDTO();    
//				cDto.setConstructId("HQETRZZN");
//				cDto.setConstructName("Career Goals");
//				cDto.setConstructScore("2.0");
//				cDto.setConstructScore2("2.0");
//				constructsArrayList.add(cDto);
//			cDto = new ConstructDTO();    
//				cDto.setConstructId("CTDHAPYM");
//				cDto.setConstructName("Career Drivers");
//				cDto.setConstructScore("0.0");
//				cDto.setConstructScore2("0.0");
//				constructsArrayList.add(cDto);
//			cDto = new ConstructDTO();    
//				cDto.setConstructId("CQXBXZWB");
//				cDto.setConstructName("learning orientation");
//				cDto.setConstructScore("4.5");
//				cDto.setConstructScore2("4.5");
//				constructsArrayList.add(cDto);
//			cDto = new ConstructDTO();    
//				cDto.setConstructId("IYHKFGAS");
//				cDto.setConstructName("experience orientation");
//				cDto.setConstructScore("5.5");
//				cDto.setConstructScore2("5.5");
//				constructsArrayList.add(cDto);				
//			mcDto.setConstructsAry(constructsArrayList);
//			modConstDTOList.add(mcDto);
//		lpDto.setManualEntryType("abyd");	
//		lpDto.setModConstDTOList(modConstDTOList);
//		
//		
//		SaveConstructDataHelper scHelper = new SaveConstructDataHelper();
//		scHelper.setUpSaveData(lpDto);
//		
//		
//		UnitTestUtils.stop(this);
//	}	
	
//	public void testSaveAdaptV2Data() 
//	throws Exception 
//	{
//	
//		UnitTestUtils.start(this);
//		
//		V2DatabaseUtils.setUnitTest(true);
//		
//	//	String username = "cdunn";
//	//	String password = "cdunn";
//	//	V2WebserviceUtils v2web = new V2WebserviceUtils();
//	//	SessionUserDTO sessionUser = v2web.registerUser(username, password);
//	//	//System.out.println("The user name is " + sessionUser.getName());
//		
//	
//	/*
//	* Need to populate a ManualEntryDTO to use... going to
//	* grab the data from the one in the ManualEntryDataHelperUT.
//	* 
//	* 		
//	*/
//		ManualEntryDTO lpDto = new ManualEntryDTO();
//		lpDto.setCandidateId("ABCDEFGH");
//		lpDto.setProjectId("IJKLMNOP");
//		lpDto.setFirstName("MB");
//		lpDto.setLastName("vendor test 03-23-09a");
//		lpDto.setManualEntryType("none");
//		lpDto.setAdaptProjectId(1234);
//		lpDto.setAdaptUserId(6789);
//		
//	
//		
//		
//		SaveConstructDataHelper scHelper = new SaveConstructDataHelper();
//		scHelper.writeAdaptV2UserData(lpDto.getCandidateId(), lpDto.getProjectId(), 
//										lpDto.getAdaptUserId(), lpDto.getAdaptProjectId());
//
//		UnitTestUtils.stop(this);
//	}	
	
	
//	public void testWriteResultstoDatabaseFromIR_RavB() 
//	throws Exception 
//	{
//		//System.out.println("testWriteResultstoDatabaseFromIR......");
//		
//		UnitTestUtils.start(this);		
//		V2DatabaseUtils.setUnitTest(true);
//
//		// Create the return IndividualReport object and the
//		// ReportData object that will contain the instrument data
//		IndividualReport ir = new IndividualReport();
//		ReportData	rd = new ReportData();
//		
//		ir.getReportData().put(ReportingConstants.RC_RAVENS_B, rd);
//		ir.setReportCode(ReportingConstants.RC_RAVENS_B);
//		rd.getDisplayData().put("V2_MODULE_ID", V2InstrumentConstants.RAVENS_B_V2_ID);
//
//		// These constructs are associated with module HNZYMHDV
//		ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getDisplayData().put("CONSTRUCT_ID_CORRECT", "FCNEVHUL");
//		ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getDisplayData().put("CONSTRUCT_ID_UNANSWERED", "FDVTZYRA");
//		ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getDisplayData().put("CONSTRUCT_ID_INCORRECT", "FFSALJFE");
//		
//		//test user 1
//		String partId = "BISIHRPL";
//////	String projId = "IYKNYCNU";
//		String projId = "HSPMUGMB";
//		
//		rd.getDisplayData().put("V2_CANDIDATE_ID", partId);
//		rd.getDisplayData().put("V2_PROJECT_ID", projId);
//	
//		ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getScoreData().put("CORRECT_ANSWERS", "8");  // 17 or 8
//		ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getScoreData().put("NUMBER_UNANSWERED", "0");
//		ir.getReportData().get(ReportingConstants.RC_RAVENS_B).getScoreData().put("INCORRECT_ANSWERS", "15");	// 6 or 15
//
//		SaveConstructDataHelper scHelper = new SaveConstructDataHelper();
//		scHelper.writeResultsDataToDatabaseFromIR(ir);
//		
//		UnitTestUtils.stop(this);
//	}

	
//	public void testWriteResultstoDatabaseFromIR_WGE() 
//	throws Exception 
//	{
//		//System.out.println("testWriteResultstoDatabaseFromIR......");
//		
//		UnitTestUtils.start(this);		
//		V2DatabaseUtils.setUnitTest(true);
//
//		// Create the return IndividualReport object and the
//		// ReportData object that will contain the instrument data
//		IndividualReport ir = new IndividualReport();
//		ReportData	rd = new ReportData();
//		
//		ir.getReportData().put(ReportingConstants.RC_WG_E, rd);
//		ir.setReportCode(ReportingConstants.RC_WG_E);
//		rd.getDisplayData().put("V2_MODULE_ID", V2InstrumentConstants.WATSON_E_V2_ID);
//
//		// These constructs are associated with module FFJUUSBA
//		ir.getReportData().get(ReportingConstants.RC_WG_E).getDisplayData().put("CONSTRUCT_ID", "FBGFXLHU");
//		
//		//test user 1
//		String partId = "BISIHRPL";
//////	String projId = "IYKNYCNU";
//		String projId = "HSPMUGMB";
//		
//		rd.getDisplayData().put("V2_CANDIDATE_ID", partId);
//		rd.getDisplayData().put("V2_PROJECT_ID", projId);
//	
//		ir.getReportData().get(ReportingConstants.RC_WG_E).getScoreData().put("WGE_CT", "33");
//
//		SaveConstructDataHelper scHelper = new SaveConstructDataHelper();
//		scHelper.writeResultsDataToDatabaseFromIR(ir);
//		
//		UnitTestUtils.stop(this);
//	}

	
//	public void testWriteResultstoDatabaseFromIR_CS() 
//	throws Exception 
//	{
//		//System.out.println("testWriteResultstoDatabaseFromIR......");
//		
//		UnitTestUtils.start(this);		
//		V2DatabaseUtils.setUnitTest(true);
//
//		// Create the return IndividualReport object and the
//		// ReportData object that will contain the instrument data
//		IndividualReport ir = new IndividualReport();
//		ReportData	rd = new ReportData();
//		
//		ir.getReportData().put(ReportingConstants.RC_CAREER_SURVEY, rd);
//		ir.setReportCode(ReportingConstants.RC_CAREER_SURVEY);
//		rd.getDisplayData().put("V2_MODULE_ID", V2InstrumentConstants.CS_ID);
//
//		// These constructs are associated with module FFGLQDEG - see incoming.jsp
//		ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getDisplayData().put("CONSTRUCT_ID_CG", "HQETRZZN");
//		ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getDisplayData().put("CONSTRUCT_ID_CD", "CTDHAPYM");
//		ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getDisplayData().put("CONSTRUCT_ID_LO", "CQXBXZWB");
//		ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getDisplayData().put("CONSTRUCT_ID_EO", "IYHKFGAS");
//
//		//test user 1
//		String partId = "BISIHRPL";
//////	String projId = "IYKNYCNU";
//		String projId = "HSPMUGMB";
//		
//		rd.getDisplayData().put("V2_CANDIDATE_ID", partId);
//		rd.getDisplayData().put("V2_PROJECT_ID", projId);
//	
//		ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().put("CAREER_GOALS", "49.073");
//		ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().put("CAREER_DRIVERS", "11.082");
//		ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().put("LEARNING_ORIENT", "88.994");
//		ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().put("EXPERIENCE_ORIENT", "92.862");
//		
//		ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().put("CAREER_GOALS_RAW", "3.5");
//		ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().put("CAREER_DRIVERS_RAW", "1.6666666");
//		ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().put("LEARNING_ORIENT_RAW", "3.86");
//		ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().put("EXPERIENCE_ORIENT_RAW", "4.29");
//
//		SaveConstructDataHelper scHelper = new SaveConstructDataHelper();
//		scHelper.writeResultsDataToDatabaseFromIR(ir);
//		
//		UnitTestUtils.stop(this);
//	}
	
	
}
