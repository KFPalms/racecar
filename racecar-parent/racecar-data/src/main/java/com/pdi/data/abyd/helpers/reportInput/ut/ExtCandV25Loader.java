package com.pdi.data.abyd.helpers.reportInput.ut;

import java.sql.ResultSet;
import java.util.ArrayList;

import com.pdi.data.abyd.dto.reportInput.ReportInputDTO;
import com.pdi.data.abyd.helpers.reportInput.ReportInputDataHelper;
import com.pdi.data.abyd.helpers.reportInput.ResearchExtractHelper;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

public class ExtCandV25Loader  extends TestCase 
{
	private static int COMPLETE_STATUS = 3;
	//
	// Constructor
	//
	public ExtCandV25Loader(String name)
	{
		super(name);
	}
	
	
	/*
	 * Create extract rows for IGs already completed.
	 * 
	 * Sorry, this is not a "real" Unit test, but it seemed the best way
	 * to do this and still have access to the available infrastructure.
	 * 
	 * NOTE:  Change the 5 application.properties to the appropriate environment before use!
	 * 
	 * the "test..." seems to be required by the JUnit infrastructure.
	 */
	public void testLoadExtractCandForV25()
		throws Exception
	{
//		//we need to update a report input object from the database
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);

		// Get a list of Extract candidates
		ArrayList<Keys> keys = getCandList();
		System.out.print("Found " + keys.size() + " candidates");
		
		if(keys.size() > 0)
		{
			System.out.println("; Processing:");
			int cnt = 1;
			ReportInputDataHelper helper = new ReportInputDataHelper();
			for(Keys key : keys)
			{
				ReportInputDTO ri = helper.getReportInput(key.getPartId(), key.getDnaId());
				System.out.print(".");
				
				ResearchExtractHelper.setupResearchExtractCandidate(ri);			
				System.out.print("|");
				cnt++;
				if(cnt > 10)
				{
					System.out.println();
					cnt = 1;
				}
			}
		}
		System.out.println();
		
		UnitTestUtils.stop(this);
	}
	
	
	private ArrayList<Keys> getCandList()
		throws Exception
	{
		DataResult dr = null;
		ArrayList<Keys> ret = new ArrayList<Keys>();
		
		try
		{
			StringBuffer sqlQuery = new StringBuffer();
		    
			sqlQuery.append("SELECT ari.participantId, ");
			sqlQuery.append("       ari.dnaId ");
			sqlQuery.append("  FROM pdi_abd_rpt_input ari ");
////			sqlQuery.append("  WHERE ari.statusId = " + COMPLETE_STATUS + " ");
			sqlQuery.append("  WHERE ari.oTxtStatusId = " + COMPLETE_STATUS + " ");
			sqlQuery.append("    AND ari.pTxtStatusId = " + COMPLETE_STATUS + " ");
			sqlQuery.append("    AND ari.rateStatusId = " + COMPLETE_STATUS + " ");
			sqlQuery.append("    AND NOT EXISTS (SELECT 'X' FROM pdi_abd_extract_cand ec ");
			sqlQuery.append("                      WHERE ec.participantId = ari.participantId ");
			sqlQuery.append("                      AND ec.dnaId = ari.dnaId)");

			DataLayerPreparedStatement dlps =  AbyDDatabaseUtils.prepareStatement(sqlQuery);
			if (dlps.isInError())
			{
				throw new Exception("Error preparing getCandList.  Code=" + 
							dlps.getStatus().getExceptionCode() + 
							", Msg=" + dlps.getStatus().getExceptionMessage());
			}
	
			dr =  AbyDDatabaseUtils.select(dlps);
			
			// Check for data
			if (dr.hasNoData())
			{
				return ret;
			}
			
			// Check for errors
			if (dr.isInError())
			{
				throw new Exception("Error fetching keys.  Code=" + 
						dlps.getStatus().getExceptionCode() + 
						", Msg=" + dlps.getStatus().getExceptionMessage());
			}

			// Process the data
			ResultSet rs = dr.getResultSet();
			while(rs.next())
			{
				ret.add(new Keys(rs.getString("participantId"), rs.getLong("dnaId")));
			}

			return ret;
		}
		finally
		{
			if (dr != null) {  dr.close();  dr = null; }
		}
	}

	

	private class Keys
	{
		private String _part;
		private long _dna;

		public Keys(String pp, long dd)
		{
			_part = pp;
			_dna = dd;
		}
		
		public String getPartId()
		{
			return _part;
		}
		
		public long getDnaId()
		{
			return _dna;
		}
	}

}
