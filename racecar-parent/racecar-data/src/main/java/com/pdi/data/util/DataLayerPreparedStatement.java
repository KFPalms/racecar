/*
 *  Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.util;

import java.sql.PreparedStatement;

/**
 * DataLayerPreparedStatement is a thin class that transports status data and a
 * PreparedStatement object in the greater app.  It is to be used instead of
 * throwing an exception so that we can avoid throwing exceptions in the data
 * layer and can, instead, pass error information up to the Business Layer classes.
 *
 * @author		Ken Beukelman
 */

// NOTE:	There is no close() method associated with this object.  To free the resources associated with
//			the embedded PreparedStatement you must create a DataResult object by executing a method from one
//			of the xxxDatabsaeUtils classes to create a DataResult object that will allow one to execute a
//			close() and free up the resources.  Also, see the comments on DataResult.

// TODO If it becomes an issue, create a method that will create a dummy DataResult; pass the Prepared statement from this object
//      to a new constructor that will buil;d a dummy DataResult with only the Prepared Statement in it... then it can be closed properly
public class DataLayerPreparedStatement extends BaseDTO
{
	//
	// Static data.
	//
	public static final int AUTO_KEY_NOT_SET = -1;
	public static final int NO_AUTO_KEYS = 0;
	public static final int HAS_AUTO_KEYS = 1;

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private PreparedStatement _ps = null;
	private int _autoKeyFlag = AUTO_KEY_NOT_SET;
	
	//
	// Constructors.
	//
	/*
	 * No parameter "generic" constructor
	 */
	public DataLayerPreparedStatement()
	{
		super();
		// Nothing done on a no-parameter constructor
	}
	
	/*
	 * Constructor that sets the enclosed prepared statement 
	 * NOTE - You must use the defined constans for this to work reliably
	 */
	public DataLayerPreparedStatement(PreparedStatement ps, int autoKeyFlag)
	{
		this.setPreparedStatement(ps);
		_autoKeyFlag = autoKeyFlag;
	}

	/*
	 * Constructor that sets the return status
	 */
	public DataLayerPreparedStatement(RequestStatus stat)
	{
		this.setStatus(stat);
	}

	//
	// Instance methods.
	//

	public boolean isInError()
	{
		return (this.getStatus().getStatusCode() == RequestStatus.RS_OK ? false : true);
	}

	//////////////////////////////////////
	//        Setters and Getters       //
	//////////////////////////////////////
	
	/********************************************************/
	public void setPreparedStatement(PreparedStatement value)
	{
		_ps = value;
	}

	public PreparedStatement getPreparedStatement()
	{
		return _ps;
	}
	
	/********************************************************/
	// Read only access to the auto key flag

	public boolean genAutoKeys()
		throws IllegalStateException
	{
		if (_autoKeyFlag == AUTO_KEY_NOT_SET)
		{
			throw new IllegalStateException("Auto Key flag has not been set");
		}
		
		return _autoKeyFlag == HAS_AUTO_KEYS ? true : false ;
	}
	
	
	/*
	 * Method to dump my contents
	 */
	public String toString()
	{
		String ret = "DataLayerPreparedStatement:\n";
		ret += super.toString();
		ret += "PS = " + _ps.toString();
		ret += "AutoKeyFlag=" + _autoKeyFlag;
		
		
		return ret;
	}
}
