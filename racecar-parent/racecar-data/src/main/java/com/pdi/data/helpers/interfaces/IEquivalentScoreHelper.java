/**
 * Copyright (c) 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.helpers.interfaces;

import java.util.ArrayList;
import java.util.HashMap;

import com.pdi.data.dto.TextPair;
import com.pdi.data.dto.EqInfo;;

public interface IEquivalentScoreHelper
{
	
	/**
	 * Get all of the equivalent instrument lists in a HashMap keyed by instrument code
	 */
	public HashMap<String, ArrayList<TextPair>> fetchInstEquivLists();


	/**
	 * Get the equivalent score for the given old instrument code and score
	 * NOTE - This logic assumes a single score for the instrument.  If instruments
	 *        with multiple scales are brought in, then the equivalency logic (and
	 *        tables) will have to change.
	 *        
	 * @param oldInst  The old instrument code (bogus codes defined in DataConstants)
	 * @param oldScore  The score of the old instrument
	 */
	public String fetchEquivalentScore(String oldInst, String oldScore);


	/**
	 * Save equivalent score log data
	 *        
	 * @param oldInst  The old instrument code (bogus codes defined in DataConstants)
	 */
	public void writeEquivalentLog(EqInfo logInfo);

}
