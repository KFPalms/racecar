package com.pdi.data.v3.helpers.ut;

//import java.util.ArrayList;

import com.pdi.data.util.BaseDTO;
import com.pdi.data.v3.dto.ClientDTO;
import com.pdi.data.v3.dto.ParticipantDTO;
import com.pdi.data.v3.dto.ParticipantListDTO;
import com.pdi.data.v3.dto.ParticipationDTO;
import com.pdi.data.v3.dto.ProjectDTO;
import com.pdi.data.v3.dto.ResultDTO;
import com.pdi.data.v3.dto.ScaleScoreDTO;
import com.pdi.data.v3.dto.SessionUserDTO;
import com.pdi.data.v3.helpers.ClientDataHelper;
import com.pdi.data.v3.helpers.ParticipantDataHelper;
import com.pdi.data.v3.helpers.ProjectDataHelper;
import com.pdi.properties.PropertyLoader;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

public class ParticipantDataHelperUT extends TestCase
{
    public ParticipantDataHelperUT(String name)
    {
    	super(name);
	} 
    
    
    public void testProperties()
    {
    	UnitTestUtils.start(this);
    	
    	//Does the local property work?
    	String applicationId = PropertyLoader.getProperty("com.pdi.data.v3.application", "application.code");
    	assertEquals(applicationId != null, true);
    	assertEquals(applicationId.equals("PDI-DATA-V3"), true);
    	
    	UnitTestUtils.stop(this);
    }
    
    public void testGetParticipant()
    {
    	UnitTestUtils.start(this);
    	
		
		UnitTestUtils.stop(this);
    }

    
    /*
     * Tests the default List Candidates
     * 
     */
    public void testGetParticipants()
    {
    	UnitTestUtils.start(this);
    	
////		System.out.println("Commented out for testing.  Please restore when testing is complete!");

    	//step 1 - get a list of all clients
    	//step 2 - get a list of all projects in that client
    	//step 3 - get a list of all participants in that project
    	//step 4 - get a list of all answer data in that participant
    	//step 5 - profit
    	
    	SessionUserDTO sessionUser = new SessionUserDTO();
    	sessionUser.setUsername(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.username")); 
    	sessionUser.setPassword(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.password"));

    	for(ClientDTO client : ClientDataHelper.getClients(sessionUser).getClientList())
    	{
    		System.out.println("--> client name:  " +client.getName());
    		System.out.println("--> client id:  " + client.getId());
    		for(ProjectDTO project : ProjectDataHelper.getProjects(sessionUser, client).getProjectList())
    		{
    			System.out.println("----> project id :  " + project.getId());
    			if (project.isProxy())
    			{
    				project = ProjectDataHelper.getProject(sessionUser, project);
    			}
    			for (ParticipantDTO participant : project.getParticipantList())
    			{
    				if (participant.isProxy())
    				{
    					participant = ParticipantDataHelper.getParticipant(sessionUser, participant);
    				}
    	   			System.out.println("------> participant id :  " + participant.getId());
    				//System.out.println(participant);
    				for(ParticipationDTO participation : participant.getParticipations())
    				{
    					for(ResultDTO result : participation.getResults())
    					{
    						System.out.println("----------> module name: " +result.getModule().getModuleName());
    						System.out.println("----------> module GUID: " +result.getModule().getModuleGUID());
    						String scores = "";
    						for(ScaleScoreDTO scaleScore : result.getScaleScores()) {
    							scores += "(" + scaleScore.getId() + "," + scaleScore.getValue() + "),";
    						}
    						System.out.println("--------------> " + scores);
    					}
    				}
    			}
    		}
    	}
    	
    	UnitTestUtils.stop(this);
    }

    
    /*
     * Tests the module get for a candidate list
     * 
     */
    public void testGetParticipantWithModules()
    {
    	UnitTestUtils.start(this);
 
    	SessionUserDTO sessionUser = new SessionUserDTO();
    	sessionUser.setUsername(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.username")); 
    	sessionUser.setPassword(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.password"));

    	ProjectDTO project = new ProjectDTO();
    	project.setUri("https://platform.iscopia.com/webservices1/rest/v1/project/PWDQ-KPQN");
    	project.setProxy(BaseDTO.PROXY);
    	ParticipantListDTO pld = ParticipantDataHelper.getParticipantsWithModules(sessionUser, project);
		for (ParticipantDTO participant : pld.getParticipantList())
		{
   			System.out.println("--> participant:  " + participant.getCandidate().getIndividual().getFamilyName() +
   											   ", " + participant.getCandidate().getIndividual().getGivenName() +
   											   " (" + participant.getId() + ")"	  );
   			for (ParticipationDTO participation : participant.getParticipations())
   			{
   				System.out.println("----> participation:  activity=" + participation.getActivityId() +
   						                                ", product=" + participation.getProductId() +
   						                                   ", stat=" + participation.getItemStatus().getCode() +
   						                                ", outcome=" + participation.getItemStatus().getOutcome());
   				for (ResultDTO result : participation.getResults())
   				{
   		   				System.out.println("----> module:  GUID=" + result.getModule().getModuleGUID() +
   		   						                        ", Name=" + result.getModule().getModuleName() +
   		   						                      ", status=" + result.getModule().getModuleStatus() +
   		   						                          ". ID=" + result.getModule().getModuleId() +
   		   						                        ", Code=" + result.getModule().getModuleCode() );
   				}
   			}
		}    	
		UnitTestUtils.stop(this);
    }
   
    public static void main(String[] args)
    throws Exception
    {
        junit.textui.TestRunner.run(ParticipantDataHelperUT.class);
    }
}