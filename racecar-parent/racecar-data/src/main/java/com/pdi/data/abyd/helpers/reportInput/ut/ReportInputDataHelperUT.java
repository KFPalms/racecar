package com.pdi.data.abyd.helpers.reportInput.ut;

import com.pdi.data.abyd.dto.reportInput.ReportInputDTO;
import com.pdi.data.abyd.helpers.reportInput.ReportInputDataHelper;
//import com.pdi.data.util.DatabaseUtils;
//import com.pdi.data.v2.util.V2DatabaseUtils;
//import com.pdi.reporting.utils.ReportingUtils;
import com.pdi.ut.UnitTestUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.data.nhn.util.NhnDatabaseUtils;
import com.pdi.data.v2.util.V2DatabaseUtils;
//import com.pdi.data.abyd.util.AbyDDatabaseUtils;

import junit.framework.TestCase;


public class ReportInputDataHelperUT  extends TestCase
{
	//
	// Constructor
	//
	public ReportInputDataHelperUT(String name)
	{
		super(name);
	}
	
//	public void testAlive()
//	throws Exception
//	{
//		UnitTestUtils.start(this);
//		
//		System.out.println("Hello World!");
//		
//		UnitTestUtils.stop(this);
//	}
	
	
	/*
	 * testAddReportInput
	 */
//	public void testAddReportInput()
//		throws Exception
//	{
//		//we need to add a report input object to the database
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//
//		ReportInputDTO reportInput = new ReportInputDTO();
//		
//		reportInput.setDnaId(6);
//		reportInput.setParticipantId("60254");
//		reportInput.setStatusId(1);	// <----------
//		
//		reportInput.setDerailmentOrgText("setDerailmentOrgText");
//		reportInput.setDerailmentPartText("setDerailmentPartText");
//		reportInput.setDerailmentRating(5);
//		reportInput.setDerailmentRecommendedRating(2);
//		
//		reportInput.setDevelopmentPartText("developmentPartText");
//		reportInput.setFitOrgText("fitOrgText");
//		
//		reportInput.setLeadershipExperienceOrgText("leadershipExperienceOrgText");
//		reportInput.setLeadershipExperiencePartText("leadershipExperiencePartText");
//		reportInput.setLeadershipExperienceRating(4);
//		
//		
//		reportInput.setLeadershipInterestOrgText("leadershipInterestOrgText");
//		reportInput.setLeadershipInterestPartText("leadershipInterestPartText");
//		reportInput.setLeadershipInterestRating(5);
//
//		
//		reportInput.setLeadershipSkillOrgText("leadershipSkillOrgText");
//		reportInput.setLeadershipSkillPartText("leadershipSkillPartText");
//		reportInput.setLeadershipSkillRating(4);
//		
//		
//		reportInput.setLeadershipStyleOrgText("leadershipStyleOrgText");
//		reportInput.setLeadershipStylePartText("leadershipStylePartText");
//		reportInput.setLeadershipStyleRating(3);
//		
//		reportInput.setLongtermOrgText("longtermOrgText");
//		reportInput.setLongtermRating(5);
//
//		
//		reportInput.setPivotalPartText("pivotalPartText");
//		
//		reportInput.setReadinessOrgText("readinessOrgText");
//		
//		reportInput.setSkillsToDevelopOrgText("skillsToDevelopOrgText");
//		reportInput.setSkillsToDevelopPartText("skillsToDevelopPartText");
//		
//		reportInput.setSkillsToLeverageOrgText("skillsToLeverageOrgText");
//		reportInput.setSkillsToLeveragePartText("skillsToLeveragePartText");
//		
//		
//		ReportInputDataHelper hlpr = new ReportInputDataHelper();
//		long id = hlpr.addReportInput(reportInput);
//		System.out.println("Report Input row added.  Key=" + id);
//		
//		UnitTestUtils.stop(this);
//	}
	
	
	/*
	 * testGetReportInput
	 */
	public void testGetReportInput()
		throws Exception
	{
		//We need to get a report input object from the database
		UnitTestUtils.start(this);
		V2DatabaseUtils.setUnitTest(true);
		AbyDDatabaseUtils.setUnitTest(true);
		NhnDatabaseUtils.setUnitTest(true);
		
		ReportInputDataHelper helper = new ReportInputDataHelper();
		
		// This should work fine
		//ReportInputDTO report = helper.getReportInput("QAQAQAQA", 201);	// Dev
		//This will get the dlci but fail to get other data
		//ReportInputDTO report = helper.getReportInput("KEYVOFZF", 41);	// Dev
		
		// OLD Dev Data - dEV DATA, 41 MODIFIED FROM JOB HSIGIWXY TO CURRENT VALUE
		//ReportInputDTO report = helper.getReportInput("IYKWVFJT", 41);	// Dev
		
		// OLD Cert DATA
		//ReportInputDTO report = helper.getReportInput("FBEAXPSI", 174);	// Cert - Not done
		//ReportInputDTO report = helper.getReportInput("FDWOWSBR", 180);	// Cert - Rolson TPOT
		//ReportInputDTO report = helper.getReportInput("LLONVITN", 180);	// Cert - Chrisa Testuser!
		//ReportInputDTO report = helper.getReportInput("FEEXMVVB", 115);	// Cert - No score data
		//ReportInputDTO report = helper.getReportInput("FCXHWLDS", 161);	// Cert - No LEI data
		//ReportInputDTO report = helper.getReportInput("HSAQKQMC", 196);	// Cert - No LEI data
		//ReportInputDTO report = helper.getReportInput("LLCQIBLV", 197);	// Cert - Jira task 195 - uses Ravens B - 1st data set
		//ReportInputDTO report = helper.getReportInput("LGORSLES", 196);	// Cert - Jira task 195 - uses Ravens B - 2nd data set no Career Survey
		//ReportInputDTO report = helper.getReportInput("FBEAXPSI", 174);	// Cert
		//ReportInputDTO report = helper.getReportInput("LFPRMBTU", 223);	// Cert - Suggested ratings not appearing (resolved)
		//ReportInputDTO report = helper.getReportInput("IUMLEPCZ", 169);	// Cert - Suggested ratings not appearing (added blank instruments)
		//ReportInputDTO report = helper.getReportInput("FGAAYDEW", 283);		// Cert - Suggested ratings not appearing (added blank instruments)
		
		// Migration testing
		//ReportInputDTO report = helper.getReportInput("60254", 6, "en");
		//ReportInputDTO report = helper.getReportInput("60254", 6, "de");
		ReportInputDTO report = helper.getReportInput("367924", 53);	// dev - test CDR options
		
		assertNotNull(report);
		System.out.println("ReportInputDTO:\n" + report.toString());

		UnitTestUtils.stop(this);
	}
	

//  -----> NOTE:  Code for delete is commented out in the helper  <-----
//	/*
//	 * testDeleteReportInput
//	 */
////	public void testDeleteReportInput()
////		throws Exception
////	{
////		//we need delete a report input object from the database
////		UnitTestUtils.start(this);
////		ReportInputDTO reportInput = new ReportInputDTO();
////		AbyDDatabaseUtils.setUnitTest(true);
////		
////		reportInput.setDnaId(199);
////		reportInput.setParticipantId("QAQAQAQA");
////		
////		ReportInputDataHelper.deleteReportInput(reportInput);
////		
////		UnitTestUtils.stop(this);
////	}
	
	
	/*
	 * testUpdateReportInput
	 */
//	public void testUpdateReportInput()
//		throws Exception
//	{
//		//we need to update a report input object from the database
//		UnitTestUtils.start(this);
//		ReportInputDTO reportInput = new ReportInputDTO();
//		AbyDDatabaseUtils.setUnitTest(true);
//		NhnDatabaseUtils.setUnitTest(true);
//		
//		reportInput.setDnaId(6);
//		reportInput.setParticipantId("60254");
//		
//		reportInput.setStatusId(2);	// <----------
//		reportInput.setCandidateFitIndex(80);
//		
//		reportInput.setDerailmentOrgText("-setDerailmentOrgText-");
//		reportInput.setDerailmentPartText("-setDerailmentPartText-");
//		reportInput.setDerailmentRating(1);
//		
//		reportInput.setDevelopmentPartText("developmentPartText");
//		reportInput.setFitOrgText("fitOrgText");
//		
//		reportInput.setLeadershipExperienceOrgText("leadershipExperienceOrgText");
//		reportInput.setLeadershipExperiencePartText("leadershipExperiencePartText");
//		reportInput.setLeadershipExperienceRating(2);
//		
//		reportInput.setLeadershipInterestOrgText("leadershipInterestOrgText");
//		reportInput.setLeadershipInterestPartText("leadershipInterestPartText");
//		reportInput.setLeadershipInterestRating(3);
//		
//		reportInput.setLeadershipSkillOrgText("leadershipSkillOrgText");
//		reportInput.setLeadershipSkillPartText("leadershipSkillPartText");
//		reportInput.setLeadershipSkillRating(4);
//		
//		reportInput.setLeadershipStyleOrgText("leadershipStyleOrgText");
//		reportInput.setLeadershipStylePartText("leadershipStylePartText");
//		reportInput.setLeadershipStyleRating(5);
//		
//		reportInput.setLongtermOrgText("longtermOrgText");
//		reportInput.setLongtermRating(1);
//		
//		reportInput.setPivotalPartText("pivotalOrgText");
//		
//		reportInput.setReadinessOrgText("readinessOrgText");
//		
//		reportInput.setSkillsToDevelopOrgText("skillsToDevelopOrgText");
//		reportInput.setSkillsToDevelopPartText("skillsToDevelopPartText");
//		
//		reportInput.setSkillsToLeverageOrgText("skillsToLeverageOrgText");
//		reportInput.setSkillsToLeveragePartText("skillsToLeveragePartText");
//		
//		ReportInputDataHelper hlpr = new ReportInputDataHelper();
//		hlpr.updateReportInput("Bogus URL", reportInput);
//		
//		UnitTestUtils.stop(this);
//	}
	
	
	/*
	 * testRiStatusChange
	 */
//	public void testRiStatusChange()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//
//		//  Update an existing report input object from status 1 to 2
//		ReportInputDTO reportInput = new ReportInputDTO();
//		reportInput.setDnaId(180);
//		reportInput.setParticipantId("FDWOWSBR");
//		reportInput.setStatusId(2);		// <----- Changing to "Ready for Editing"	// <----------
//		reportInput.setCandidateFitIndex(14);
//		reportInput.setSkillsToLeverageOrgText("Rolson Testuser");
//		reportInput.setSkillsToLeveragePartText("asdfgasdfasdfasdf");
//		reportInput.setSkillsToDevelopOrgText("dfhdfhdghdfh");
//		reportInput.setSkillsToDevelopPartText("kgjhkghjkghjk");
//		reportInput.setLeadershipSkillOrgText("gjhkghjkghjk");
//		reportInput.setLeadershipSkillPartText("gjkgl;jkl;'909p");
//		reportInput.setLeadershipSkillRating(2);
//		reportInput.setLeadershipExperienceOrgText("xvcxcvbxcvbxcb");
//		reportInput.setLeadershipExperiencePartText("rtyurtyurtyu");
//		reportInput.setLeadershipExperienceRating(3);
//		reportInput.setLeadershipStyleOrgText("5yn56u6");
//		reportInput.setLeadershipStylePartText("77iol7ol7");
//		reportInput.setLeadershipStyleRating(2);
//		reportInput.setLeadershipInterestOrgText("oqertowqerotu[oqetioperr");
//		reportInput.setLeadershipInterestPartText("sdfljl;sdfpob");
//		reportInput.setLeadershipInterestRating(2);
//		reportInput.setDerailmentOrgText("iuowqrioukl;sd");
//		reportInput.setDerailmentPartText("iop0-kl;m,.");
//		reportInput.setDerailmentRating(6);
//		reportInput.setLongtermOrgText("werwert546346345");
//		reportInput.setLongtermRating(2);
//		reportInput.setFitOrgText("kl;wwo2p2ws");
//		reportInput.setReadinessOrgText("jjkljkljkljkljkljkljkljkljkl");
//		reportInput.setDevelopmentPartText("uiouiouiouiouiouiouio");
//		reportInput.setPivotalPartText("uio897jknjuyiui");
//		ReportInputDataHelper reportInputDataHelper = new ReportInputDataHelper();
//		reportInputDataHelper.updateReportInput("bogusUrl", reportInput);
//		System.out.println("You should recieve an email!");
//		
//		// Revert to status 1
//		reportInput.setStatusId(1);	// <----------
//		reportInputDataHelper.updateReportInput("bogusURL", reportInput);
//		System.out.println("You should NOT recieve a second email!");
//		
//		UnitTestUtils.stop(this);
//	}
	
	
	/*
	 * testUpdateReportInput for extract data
	 */
//	public void testUpdateReportInputExtract()
//		throws Exception
//	{
//		//we need to update a report input object from the database
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//
//		// Get a fully populated ReportInputDTO object
//		ReportInputDataHelper helper = new ReportInputDataHelper();
//
//		// Get a DTO for an existing project/participant
//		
//		//// Cert - lp=15797261  BUL - Test, Jenny
//		//String partId = "HQDIVKZT";
//		//long dnaId = 266;
//		
//		//// Cert - Mueller, Thomas
//		//String partId = "FAJCUSYG";
//		//long dnaId = 274;
//		
//		//// Cert - Test, Jill
//		//String partId = "IUPOSMYP";
//		//long dnaId = 266;
//		
//		//// Cert - Test, Jimmy
//		//String partId = "LIIAZYCC";
//		//long dnaId = 266;
//		
//		////Cert - lp=79490422 - MLL - Testuser, AbyD Keith 091310
//		//String partId = "HQDYEPWJ";
//		//long dnaId = 327;
//
//		//Cert - lp=75201853 - MLL - Testuser, Imsplay
//		String partId = "HNRUCDOE";
//		long dnaId = 268;
//
//		ReportInputDTO report = helper.getReportInput(partId, dnaId);
//
//		// Ensure the status is "Complete"
//		report.setStatusId(3);	// <----------
//		
//		// Update it
//
//		helper.updateReportInput(null, report);
//		
//		UnitTestUtils.stop(this);
//	}

}
