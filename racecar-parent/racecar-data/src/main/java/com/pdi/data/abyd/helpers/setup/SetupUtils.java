/**
 * Copyright (c) 2014 Korn Ferry International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.setup;

//import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.logging.LogWriter;

/**
 * The SetupUtils class provides a place to put miscellaneous
 * utility methods valid for use in more than one helper in setup.
 *
 * @author		Ken Beukelman
 */
public class SetupUtils
{
	//
	// Static data.
	//

	//
	// Static methods.
	//
	
	//
	// Instance data.
	//
	
	//
	// Constructors.
	//
	public SetupUtils()
	{
		// Does nothing special at this time
	}

	//
	// Instance methods.
	//
	/**
	 * cloneUserText - Create a copy of the user text represented by
	 *                 the text ID and return the ID of that copy.
	 * I'ld like to make this static, but there could be repercussions with simultaneous users
	 * @param txtId Text Id of the text to clone
	 * @return Text id of the cloned text
	 */
	public long cloneUserText(long txtId)
	{
		DataLayerPreparedStatement insDfltDlps = null;
		DataLayerPreparedStatement updtDfltDlps = null;
		DataLayerPreparedStatement insNonDfltDlps = null;
		DataLayerPreparedStatement selDlps = null;
		long newId = 0;
		DataResult selDr = null;
		DataResult insDfltDr = null;
		DataResult insOtherDr = null;
		DataResult drUt = null;

		// Get all instances
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT isDefault, ");
		sb.append("       languageId, ");
		sb.append("       typeCode, ");
		sb.append("       userText ");
		sb.append("  FROM user_text ");
		sb.append("  WHERE userTextId = " + txtId + " ");
		sb.append("  ORDER BY isDefault DESC, languageId");
		
		selDlps = AbyDDatabaseUtils.prepareStatement(sb);
		if (selDlps.isInError())
		{
			LogWriter.logBasic(LogWriter.WARNING, this,
					"ReportModelDataHelper.cloneText():  Error preparing SELECT statement.");
			return 0;
		}

		try
		{
			selDr = AbyDDatabaseUtils.select(selDlps);
			ResultSet rs = selDr.getResultSet();

			if (! rs.isBeforeFirst())
			{
				LogWriter.logBasic(LogWriter.WARNING, this,
					"ReportModelDataHelper.cloneText():  No data available.");
				return 0;
			}
			
			boolean isFirst = true;
			while(rs.next())
			{
				// Get the data
				long langId = rs.getLong("languageId");
				String type = rs.getString("typeCode");
				String txt = rs.getString("userText");
				boolean dflt = rs.getBoolean("isDefault");
				
				// if first and !default error
				if (isFirst && ! dflt)
				{
					LogWriter.logBasic(LogWriter.WARNING, this,
						"ReportModelDataHelper.cloneText():  No default text available.");
					return 0;
				}
				
				try
				{
					if (isFirst)
					{
						//
						sb.setLength(0);
						sb.append("INSERT INTO user_text ");
						sb.append("  (languageId, typeCode, userText, isDefault, lastUserId, lastDate) ");
						sb.append("  VALUES(?, ?, ?, ?, SYSTEM_USER, GETDATE())");
						insDfltDlps = AbyDDatabaseUtils.prepareStatement(sb, Statement.RETURN_GENERATED_KEYS );
						if (insDfltDlps.isInError())
						{
							LogWriter.logBasic(LogWriter.WARNING, this,
								"ReportModelDataHelper.genCloneTexts():  Error preparing dflt insert statement.");
							return 0;
						}
						
						// Create the default text update PS
						sb.setLength(0);
						sb.append("UPDATE user_text ");
						sb.append("  SET userTextId = ? ");
						sb.append("  WHERE uniqueId = ?");
						updtDfltDlps = AbyDDatabaseUtils.prepareStatement(sb);
						if (updtDfltDlps.isInError())
						{
							LogWriter.logBasic(LogWriter.WARNING, this,
							"ReportModelDataHelper.genCloneTexts():  Error preparing dflt update statement.");
						return 0;
						}
						// Write it out, get id, update it
						insDfltDlps.getPreparedStatement().setLong(1, langId);
						insDfltDlps.getPreparedStatement().setString(2, type);
						insDfltDlps.getPreparedStatement().setString(3, txt);
						insDfltDlps.getPreparedStatement().setBoolean(4, dflt);
						
						insDfltDr = AbyDDatabaseUtils.insert(insDfltDlps);
						if (insDfltDr.isInError())
						{
							LogWriter.logBasic(LogWriter.WARNING, this,
								"ReportModelDataHelper.cloneText():  Error inserting default text.");
							return 0;
						}
						newId = insDfltDr.getNewId();	// Text ID
						updtDfltDlps.getPreparedStatement().setLong(1, newId);	// Text ID
						updtDfltDlps.getPreparedStatement().setLong(2, newId);	// Unique ID (same for the default row)
						
						drUt = AbyDDatabaseUtils.update(updtDfltDlps);
						if (drUt.isInError())
						{
							LogWriter.logBasic(LogWriter.WARNING, this,
								"ReportModelDataHelper.cloneText():  Error updating default text ID.");
							return 0;
						}
						isFirst = false;
					}
					else
					{
						// Set up the subsequent (non-default) insert statement
						sb.setLength(0);
						sb.append("INSERT INTO user_text ");
						sb.append("  (userTextId, languageId, typeCode, userText, isDefault, lastUserId, lastDate) ");
						sb.append("  VALUES(?, ?, ?, ?, ?, SYSTEM_USER, GETDATE())");
						insNonDfltDlps = AbyDDatabaseUtils.prepareStatement(sb, Statement.RETURN_GENERATED_KEYS );
						if (insNonDfltDlps.isInError())
						{
							LogWriter.logBasic(LogWriter.WARNING, this,
								"ReportModelDataHelper.genCloneTexts():  Error preparing dflt insert statement.");
							return 0;
						}
						// Write it out
						insNonDfltDlps.getPreparedStatement().setLong(1, newId);
						insNonDfltDlps.getPreparedStatement().setLong(2, langId);
						insNonDfltDlps.getPreparedStatement().setString(3, type);
						insNonDfltDlps.getPreparedStatement().setString(4, txt);
						insNonDfltDlps.getPreparedStatement().setBoolean(5, dflt);
						
						insOtherDr = AbyDDatabaseUtils.insert(insNonDfltDlps);
						if (insOtherDr.isInError())
						{
							LogWriter.logBasic(LogWriter.WARNING, this,
								"ReportModelDataHelper.cloneText():  Error inserting non-default text.");
							return 0;
						}
					}
					isFirst = false;
				} catch (SQLException e) {
				LogWriter.logBasic(LogWriter.WARNING, this,
						"ReportModelDataHelper.genCloneTexts():  Error on insert/update.");
					return 0;
				}
				finally
				{
					if(insDfltDr != null) {insDfltDr.close(); insDfltDr = null; }
					if(insOtherDr != null) {insOtherDr.close(); insOtherDr = null; }
					if(drUt != null) {drUt.close(); drUt = null; }
				}
			}	// End of while loop
		} catch (SQLException e) {
		LogWriter.logBasic(LogWriter.WARNING, this,
				"ReportModelDataHelper.genCloneTexts():  Error checking ResultSet.");
			return 0;
		}
		finally
		{
			if(selDr != null) {selDr.close(); selDr = null; }
		}
		
		return newId;
	}
}
