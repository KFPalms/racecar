/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.intGrid;

import java.util.ArrayList;
import java.util.HashMap;

//import com.pdi.reporting.constants.ReportingConstants;


/**
 * IGContants provides a spot for defined constants that can be used in
 * Integration Grid processing.  Currently defined with static data only.
 *
 * NOTE: This class has a companion set of definitions on the UI side
 *       (abd\integrationGrid.IGConstants.as).  Every effort should be
 *       made to keep these files in sync.
 *
 * @author		Ken Beukelman
 */
public class IGConstants
{
	//
	// Static data.
	//
	
	///////////////////////////////////////////////////////
	// This stuff IS defined on the UI side              //
	///////////////////////////////////////////////////////

		// "Testing" column ID
	static public final transient long TESTING_MOD_ID = 0;

		// "Testing" column name
	static public final transient String TESTING_MOD_NAME = "Testing";
	static public final transient String TESTING_MOD_NAME_ALP = "Testing (AUTO)";
	static public final transient String TESTING_MOD_NAME_KF4D = "Testing (KF4D or DLA)";

		// RDI value indicating that there is no RDI available (GPI probably not scored)
	static public final transient String NO_RDI = "0";
	
		// RDI value indicating that there are no gpi intersections
	static public final transient String NO_GPI_SCORES = "-1";

	
	
	///////////////////////////////////////////////////////
	// This stuff is not used nor defined on the UI side //
	///////////////////////////////////////////////////////
	
	// Instrument codes
	static public final String I_CODE_CHQ = "chq";
	static public final String I_CODE_FINEX = "finex";
	static public final String I_CODE_GPI_SHORT = "gpi";
	static public final String I_CODE_GPI = "gpil";
	static public final String I_CODE_LEI = "lei";
	static public final String I_CODE_RAVB = "rv";
	static public final String I_CODE_RAVB2 = "rv2";
	static public final String I_CODE_WGE = "wg";
	static public final String I_CODE_CS = "cs";
	static public final String I_CODE_CS2 = "cs2";
	static public final String I_CODE_DLA = "dla";
	static public final String I_CODE_DPA = "dpa";
	
	// More instrument codes (bizsim)
	static public final String I_IBLVA	= "iblva";
	static public final String I_NIS	= "nis";
	static public final String I_ISMLL	= "ismll";
	static public final String I_ISBUL	= "isbul";
	static public final String I_ISZ	= "isz";
	static public final String I_EIS	= "eis";
	static public final String I_BRE2	= "bre2";
	static public final String I_BRESEA	= "bresea";
	static public final String I_SFECEO	= "sfeceo";
	static public final String I_BREMED	= "bremed";
	
	// Maps FinEx mock bar sequence to FinEx SPSS values
	static public ArrayList<String> FINEX_BAR_POSN = new ArrayList<String>();
	static
	{
		FINEX_BAR_POSN.add(null);		// Bar sequence 0 - Bogus entry for place holder
		FINEX_BAR_POSN.add("FE_MPD");	// bar sequence 1
		FINEX_BAR_POSN.add("FE_DAC");	// bar sequence 2
		FINEX_BAR_POSN.add("FE_AFC");	// bar sequence 3
		FINEX_BAR_POSN.add("FE_DAM");	// bar sequence 4
	}

	// Defined constants associated with LEI processing
	static protected final boolean NO_LEI = false;
	static protected final boolean INCLUDE_LEI = true;


	
	// Success Profile - order of items to send to UI
	// use the length of this to get the min array size 
	// the items come from the database in type order, which is
	// the order in in which they're in this list...
	//  and you put them into the array for the UI according to 
	// the new integer index...
	public static final Object[][] SUCCESS_PROFILE_FIELDS_INDEXES =  new Object[][]
	{		
	    (new Object [] {new Integer(3),"Objectives and Key Responsibilities"}),	
	    (new Object [] {new Integer(4),"Leadership Challenges"}),	
	    (new Object [] {new Integer(5),"Experience Requirements"}),	   
	    (new Object [] {new Integer(6),"Motivators / Career Drivers"}),	       
	    (new Object [] {new Integer(1),"Leadership Style, Culture, and Values"}),
	    (new Object [] {new Integer(2),"Derailers"}),
	    (new Object [] {new Integer(0),"Vision, Mission, Business Challenges, and Strategy"}),
	};
	
	
	
	// Cognitive instrument keys, in display order
	static public ArrayList<String> COG_INST_KEYS = new ArrayList<String>();
	static
	{
		//COG_INST_KEYS.add("RAVENSSF");	// Raven's, Short Form
		//COG_INST_KEYS.add("RAVENS_T");	// Raven's, Long Form - Manual Entry
		COG_INST_KEYS.add("RAVENSB");	// Raven's Form B
		//COG_INST_KEYS.add("WG_AS");		// Watson-Glaser A, Short Form
		COG_INST_KEYS.add("WESMAN");	// Wesman
		//COG_INST_KEYS.add("WG_A");  	// Watson-Glaser A, Long Form
		//COG_INST_KEYS.add("WG_C");  	// Watson-Glaser C, Long Form - Manual Entry
		COG_INST_KEYS.add("WGE_CT");	// Watson-Glaser E, Cognitive Thinking (full instrument)
	}

	// Instrument keys, in display order (for norms in theIG)
	static public ArrayList<String> IG_INST_KEYS = new ArrayList<String>();
	static
	{
		IG_INST_KEYS.addAll(COG_INST_KEYS);
		IG_INST_KEYS.add("GPI");
	}

	// ALR scale keys (all possible)
	// No longer used.  Shows the old KFALR key set
//	static public ArrayList<String> ALR_SCALE_KEYS = new ArrayList<String>();
//	static
//	{
//		ALR_SCALE_KEYS.add("RAVENSB");
//		ALR_SCALE_KEYS.add("WGE_CT");
//		ALR_SCALE_KEYS.add("ALR_CR");
//		ALR_SCALE_KEYS.add("ALR_TA");
//		ALR_SCALE_KEYS.add("ALR_SS");
//		ALR_SCALE_KEYS.add("ALR_CF");
//		ALR_SCALE_KEYS.add("ALR_RI");
//		ALR_SCALE_KEYS.add("ALR_AD");
//		ALR_SCALE_KEYS.add("ALR_AF");
//		ALR_SCALE_KEYS.add("ALR_NA");
//		ALR_SCALE_KEYS.add("ALR_PE");
//		ALR_SCALE_KEYS.add("ALR_AS");
//		ALR_SCALE_KEYS.add("ALR_SA");
//		ALR_SCALE_KEYS.add("ALR_IE");
//		ALR_SCALE_KEYS.add("ALR_TR");
//		ALR_SCALE_KEYS.add("ALR_EN");
//		ALR_SCALE_KEYS.add("ALR_MI");
//		ALR_SCALE_KEYS.add("ALR_CO");
//		ALR_SCALE_KEYS.add("ALR_HU");
//		ALR_SCALE_KEYS.add("ALR_IN");
//		ALR_SCALE_KEYS.add("ALR_DU");
//		ALR_SCALE_KEYS.add("ALR_ST");
//		ALR_SCALE_KEYS.add("ALR_OD");		
//	}

	static public String LEI_INST = "LEI3";


		// Instrument names by abbreviation
	static public HashMap<String,String> INST_NAMES = new HashMap<String,String>();
	static
	{
		//INST_NAMES.put("RAVENSSF","Raven's APM Short Form");
		//INST_NAMES.put("RAVENS_T","Raven's Timed - Long Form");
		//INST_NAMES.put("WG_AS","Watson-Glaser A - Short Form");
		INST_NAMES.put("WESMAN","Personnel Classification Test - Alexander G. Wesman (Wesman)");
		INST_NAMES.put("GPI","Global Personality Inventory (GPI)");
		INST_NAMES.put("LEI3","Leadership Experience Inventory (LEI)");
		//INST_NAMES.put("WG_A","Watson-Glaser A - Long Form");
		//INST_NAMES.put("WG_C","Watson-Glaser C");
		INST_NAMES.put("WGE_CT", "Watson-Glaser Form E (Global English)");
		INST_NAMES.put("RAVENSB","Raven's APM Form B");
	}


	// GPI Scales in order by SPSS label
	static public ArrayList<String> GPI_SCALE_ORDER = new ArrayList<String>();
	static
	{
		GPI_SCALE_ORDER.add("GPI_TA");	// Thought Agility
		GPI_SCALE_ORDER.add("GPI_INOV");	// Innovation/Creativity
		GPI_SCALE_ORDER.add("GPI_TF");		// Thought Focus
		GPI_SCALE_ORDER.add("GPI_VIS");		// Vision
		GPI_SCALE_ORDER.add("GPI_AD");		// Attention to Detail		
		GPI_SCALE_ORDER.add("GPI_WF");		// Work Focus
		GPI_SCALE_ORDER.add("GPI_TC");		// Taking Charge
		GPI_SCALE_ORDER.add("GPI_INFL");	// Influence
		GPI_SCALE_ORDER.add("GPI_EGO");		// Ego-centered
		GPI_SCALE_ORDER.add("GPI_MAN");		// Manipulation
		GPI_SCALE_ORDER.add("GPI_MIC");		// Micro-managing
		GPI_SCALE_ORDER.add("GPI_INTI");	// Intimidating
		GPI_SCALE_ORDER.add("GPI_PA");		// Passive-aggressive
		GPI_SCALE_ORDER.add("GPI_SO");		// Sociability
		GPI_SCALE_ORDER.add("GPI_CONS");	// Consideration
		GPI_SCALE_ORDER.add("GPI_EMP");		// Empathy
		GPI_SCALE_ORDER.add("GPI_TR");		// Trust
		GPI_SCALE_ORDER.add("GPI_AST");		// Social Astuteness	
		GPI_SCALE_ORDER.add("GPI_EL");		// Energy Level
		GPI_SCALE_ORDER.add("GPI_INIT");	// Initiative
		GPI_SCALE_ORDER.add("GPI_DACH");	// Desire for Achievement
		GPI_SCALE_ORDER.add("GPI_ADPT");	// Adaptability		
		GPI_SCALE_ORDER.add("GPI_OPEN");	// Openness
		GPI_SCALE_ORDER.add("GPI_NA");		// Negative Affectivity
		GPI_SCALE_ORDER.add("GPI_OPT");		// Optimism
		GPI_SCALE_ORDER.add("GPI_EC");		// Emotional Control
		GPI_SCALE_ORDER.add("GPI_ST");		// Stress Tolerance
		GPI_SCALE_ORDER.add("GPI_SC");		// Self-confidence
		GPI_SCALE_ORDER.add("GPI_IMPR");	// Impressing
		GPI_SCALE_ORDER.add("GPI_SASI");	// Self-awareness/Insight
		GPI_SCALE_ORDER.add("GPI_IND");		// Independence
		GPI_SCALE_ORDER.add("GPI_COMP");	// Competitive
		GPI_SCALE_ORDER.add("GPI_RISK");	// Risk-taking
		GPI_SCALE_ORDER.add("GPI_DADV");	// Desire for Advancement
		GPI_SCALE_ORDER.add("GPI_INTD");	// Interdependence
		GPI_SCALE_ORDER.add("GPI_DUT");		// Dutifulness
		GPI_SCALE_ORDER.add("GPI_RESP");	// Responsibility
	}

	
	// LEI Scales in order by SPSS label
	static public ArrayList<String> LEI_SCALE_ORDER = new ArrayList<String>();
	static
	{
		LEI_SCALE_ORDER.add("LEI3_OAL");		// Overall
		LEI_SCALE_ORDER.add("LEI3_GMT");		// Super-Scale - General Management Experiences
		LEI_SCALE_ORDER.add("LEI3_CHA");		// Super-Scale - Adversity
		LEI_SCALE_ORDER.add("LEI3_RC");			// Super-Scale - Risky/Critical Experiences
		LEI_SCALE_ORDER.add("LEI3_PCR");		// Super-Scale - Personal and Career Related Experiences
		LEI_SCALE_ORDER.add("LEI3_SGY");		// Scale 01 - Strategy Development
		LEI_SCALE_ORDER.add("LEI3_PGT");		// Scale 02 - Project Management and Implementation
		LEI_SCALE_ORDER.add("LEI3_BDV");		// Scale 03 - Business Development and Marketing
		LEI_SCALE_ORDER.add("LEI3_BGR");		// Scale 04 - Business Growth
		LEI_SCALE_ORDER.add("LEI3_PDT");		// Scale 05 - Product Development
		LEI_SCALE_ORDER.add("LEI3_STA");		// Scale 06 - Start-up Business
		LEI_SCALE_ORDER.add("LEI3_FM");			// Scale 07 - Financial Management
		LEI_SCALE_ORDER.add("LEI3_OPX");		// Scale 08 - Operations
		LEI_SCALE_ORDER.add("LEI3_FEX");		// Scale 09 - Support Functions
		LEI_SCALE_ORDER.add("LEI3_RPR");		// Scale 10 - External Relations
		LEI_SCALE_ORDER.add("LEI3_PRB");		// Scale 11 - Inherited Problems and Challenges
		LEI_SCALE_ORDER.add("LEI3_PPL");		// Scale 12 - Interpersonally Challenging Situations
		LEI_SCALE_ORDER.add("LEI3_FAL");		// Scale 13 - Downturn and/or Failures
		LEI_SCALE_ORDER.add("LEI3_FIN");		// Scale 14 - Difficult Financial Situations
		LEI_SCALE_ORDER.add("LEI3_STF");		// Scale 15 - Difficult Staffing Situations
		LEI_SCALE_ORDER.add("LEI3_HR");			// Scale 16 - High Risk Situations
		LEI_SCALE_ORDER.add("LEI3_NEG");		// Scale 17 - Critical Negotiations
		LEI_SCALE_ORDER.add("LEI3_CMT");		// Scale 18 - Crisis Management
		LEI_SCALE_ORDER.add("LEI3_VAS");		// Scale 19 - Highly Critical/Visible Assignments
		LEI_SCALE_ORDER.add("LEI3_SDV");		// Scale 20 - Self-Development
		LEI_SCALE_ORDER.add("LEI3_DEV");		// Scale 21 - Development of Others
		LEI_SCALE_ORDER.add("LEI3_INN");		// Scale 22 - International/Cross-Cultural
		LEI_SCALE_ORDER.add("LEI3_EXT");		// Scale 23 - Extracurricular Activities
	}


	// Instrument/scale names by SPSS label
	static public HashMap<String,String> SCALE_NAMES = new HashMap<String,String>();
	static
	{
		SCALE_NAMES.put("CAREER_DRIVERS","Career Drivers");
		SCALE_NAMES.put("CAREER_GOALS","Career Goals");		
		SCALE_NAMES.put("EXPERIENCE_ORIENT","Experience orientation");		
		SCALE_NAMES.put("GPI_AD","Attention to Detail");		
		SCALE_NAMES.put("GPI_ADPT","Adaptability");		
		SCALE_NAMES.put("GPI_AST","Social Astuteness");	
		SCALE_NAMES.put("GPI_COMP","Competitive");
		SCALE_NAMES.put("GPI_CONS","Consideration");
		SCALE_NAMES.put("GPI_DACH","Desire for Achievement");
		SCALE_NAMES.put("GPI_DADV","Desire for Advancement");
		SCALE_NAMES.put("GPI_DUT","Dutifulness");
		SCALE_NAMES.put("GPI_EC","Emotional Control");
		SCALE_NAMES.put("GPI_EGO","Ego-centered");
		SCALE_NAMES.put("GPI_EL","Energy Level");
		SCALE_NAMES.put("GPI_EMP","Empathy");
		SCALE_NAMES.put("GPI_IMPR","Impressing");
		SCALE_NAMES.put("GPI_IND","Independence");
		SCALE_NAMES.put("GPI_INFL","Influence");
		SCALE_NAMES.put("GPI_INIT","Initiative");
		SCALE_NAMES.put("GPI_INOV","Innovation/Creativity");
		SCALE_NAMES.put("GPI_INTD","Interdependence");
		SCALE_NAMES.put("GPI_INTI","Intimidating");
		SCALE_NAMES.put("GPI_MAN","Manipulation");
		SCALE_NAMES.put("GPI_MIC","Micro-managing");
		SCALE_NAMES.put("GPI_NA","Negative Affectivity");
		SCALE_NAMES.put("GPI_OPEN","Openness");
		SCALE_NAMES.put("GPI_OPT","Optimism");
		SCALE_NAMES.put("GPI_PA","Passive-aggressive");
		SCALE_NAMES.put("GPI_RESP","Responsibility");
		SCALE_NAMES.put("GPI_RISK","Risk-taking");
		SCALE_NAMES.put("GPI_SASI","Self-awareness/Insight");
		SCALE_NAMES.put("GPI_SC","Self-confidence");
		SCALE_NAMES.put("GPI_SO","Sociability");
		SCALE_NAMES.put("GPI_ST","Stress Tolerance");
		SCALE_NAMES.put("GPI_TA","Thought Agility");
		SCALE_NAMES.put("GPI_TC","Taking Charge");
		SCALE_NAMES.put("GPI_TF","Thought Focus");
		SCALE_NAMES.put("GPI_TR","Trust");
		SCALE_NAMES.put("GPI_VIS","Vision");
		SCALE_NAMES.put("GPI_WF","Work Focus");
		SCALE_NAMES.put("LEARNING_ORIENT","Learning Orientation");
		SCALE_NAMES.put("LEI3_BDV","LEI - Business Development and Marketing");
		SCALE_NAMES.put("LEI3_BGR","LEI - Business Growth");
		SCALE_NAMES.put("LEI3_CHA","LEI - ADVERSITY");
		SCALE_NAMES.put("LEI3_CMT","LEI - Crisis Management");
		SCALE_NAMES.put("LEI3_DEV","LEI - Development of Others");
		SCALE_NAMES.put("LEI3_EXT","LEI - Extracurricular Activities");
		SCALE_NAMES.put("LEI3_FAL","LEI - Downturns and/or Failures");
		SCALE_NAMES.put("LEI3_FIN","LEI - Difficult Financial Situations");
		SCALE_NAMES.put("LEI3_FEX","LEI - Support Functions");
		SCALE_NAMES.put("LEI3_FM","LEI - Financial Management");
		SCALE_NAMES.put("LEI3_GMT","LEI - GENERAL MANAGEMENT EXPERIENCES");
		SCALE_NAMES.put("LEI3_HR","LEI - High Risk Situations");
		SCALE_NAMES.put("LEI3_INN","LEI - International/Cross-Cultural");
		SCALE_NAMES.put("LEI3_NEG","LEI - Critical Negotiations");
		SCALE_NAMES.put("LEI3_OAL","LEI - OVERALL");
		SCALE_NAMES.put("LEI3_OPX","LEI - Operations");
		SCALE_NAMES.put("LEI3_PCR","LEI - PERSONAL AND CAREER RELATED EXPERIENCES");
		SCALE_NAMES.put("LEI3_PDT","LEI - Product Development");
		SCALE_NAMES.put("LEI3_PGT","LEI - Project Management and Implementation");
		SCALE_NAMES.put("LEI3_PPL","LEI - Interpersonally Challenging Situations");
		SCALE_NAMES.put("LEI3_PRB","LEI - Inherited Problems and Challenges");
		SCALE_NAMES.put("LEI3_RC","LEI - RISKY/CRITICAL EXPERIENCES");
		SCALE_NAMES.put("LEI3_RPR","LEI - External Relations");
		SCALE_NAMES.put("LEI3_SDV","LEI - Self-Development");
		SCALE_NAMES.put("LEI3_SGY","LEI - Strategy Development");
		SCALE_NAMES.put("LEI3_STA","LEI - Start-up Business");
		SCALE_NAMES.put("LEI3_STF","LEI - Difficult Staffing Situations");
		SCALE_NAMES.put("LEI3_VAS","LEI - Highly Critical/Visible Assignments or Initiatives");
		//SCALE_NAMES.put("PGPI_RDI","Response Distortion Index Calc 13 - RDI percentile");
		SCALE_NAMES.put("PGPI_RDI","Response Distortion Index");
		//SCALE_NAMES.put("RAVENSSF","Harcourt Raven APM Short Form");
////		SCALE_NAMES.put("WATSON","Watson-Glaser A Short");
		//SCALE_NAMES.put("WG_AS","Watson-Glaser A Short");
		//SCALE_NAMES.put("WG_A","Watson-Glaser A Long");
		//SCALE_NAMES.put("WG_C","Watson-Glaser C");
		SCALE_NAMES.put("WESMAN","Wesman");
		//SCALE_NAMES.put("RAVENS_T","Raven's Timed - Long Form");
		SCALE_NAMES.put("RAVENSB","Raven's APM Form B");
		SCALE_NAMES.put("WGE_CT","Watson-Glaser Form E");
		// Note that there are 3 additional WGE scales not used at this time.  They are sub-
		// sets of the "Critical Thinking" scale while includes all items in the instrument.

		// These are ALR_xx because they represent the normed scale (trait) score for ALR.
		// The raw score would be ALP_xx as it comes from ALP scoring.
		SCALE_NAMES.put("ALR_AD", "Adaptability");
		//SCALE_NAMES.put("ALR_CO", "Affiliation");
		SCALE_NAMES.put("ALR_AF", "Affiliation");
		SCALE_NAMES.put("ALR_AS", "Assertiveness");
		//SCALE_NAMES.put("ALR_ST", "Composure");
		SCALE_NAMES.put("ALR_CP", "Composure");
		SCALE_NAMES.put("ALR_CF", "Confidence");
		SCALE_NAMES.put("ALR_CR", "Credibility");
		//SCALE_NAMES.put("ALR_IE", "Curiosity");
		SCALE_NAMES.put("ALR_CU", "Curiosity");
		//SCALE_NAMES.put("ALR_SS", "Empathy");
		SCALE_NAMES.put("ALR_EM", "Empathy");
		//SCALE_NAMES.put("ALR_DU", "Focus");
		SCALE_NAMES.put("ALR_FO", "Focus");
		SCALE_NAMES.put("ALR_HU", "Humility");
		SCALE_NAMES.put("ALR_IN", "Influence");
		SCALE_NAMES.put("ALR_NA", "Need for Achievement");
		SCALE_NAMES.put("ALR_OD", "Openness to Differences");
		//SCALE_NAMES.put("ALR_AF", "Optimism");
		SCALE_NAMES.put("ALR_OP", "Optimism");
		SCALE_NAMES.put("ALR_PE", "Persistence");
		SCALE_NAMES.put("ALR_RI", "Risk-Taking");
		//SCALE_NAMES.put("ALR_MI", "Situational Self-Awareness");
		SCALE_NAMES.put("ALR_SS", "Situational Self-Awareness");
		//SCALE_NAMES.put("ALR_EN", "Sociability");
		SCALE_NAMES.put("ALR_SO", "Sociability");
		SCALE_NAMES.put("ALR_TA", "Tolerance of Ambiguity");
		SCALE_NAMES.put("ALR_TR", "Trust");
		// Un-used scale... due to be phased out
		SCALE_NAMES.put("ALR_SA", "Self-Awareness");
		// Don't forget the drivers
		SCALE_NAMES.put("ALR_STRC", "Structure");
		SCALE_NAMES.put("ALR_CHAL", "Challenge");
		SCALE_NAMES.put("ALR_POWR", "Power");
		SCALE_NAMES.put("ALR_COLL", "Collaboration");
		SCALE_NAMES.put("ALR_BALA", "Balance");
		SCALE_NAMES.put("ALR_INDY", "Independence");
		
		// KF-CTD Competencies
		SCALE_NAMES.put("ALR_EIN", "Engages and Inspires");
		SCALE_NAMES.put("ALR_COU", "Courage");
		SCALE_NAMES.put("ALR_EAC", "Ensures Accountability");
		SCALE_NAMES.put("ALR_SDV", "Self-Development");
		SCALE_NAMES.put("ALR_NLE", "Nimble Learning");
		SCALE_NAMES.put("ALR_ITR", "Instills Trust");
		SCALE_NAMES.put("ALR_VDI", "Values Differences");
		SCALE_NAMES.put("ALR_DTA", "Develops Talent");
		SCALE_NAMES.put("ALR_BST", "Balances Stakeholders");
		SCALE_NAMES.put("ALR_SAD", "Situational Adaptability");
		SCALE_NAMES.put("ALR_GPE", "Global Perspective");
	}


	//
	// Static methods.
	//

}
