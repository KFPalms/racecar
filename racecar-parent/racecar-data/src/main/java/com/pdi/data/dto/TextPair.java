/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.data.dto;

/**
 * TextPair is a basic data class that contains a related pair of strings, usually
 * associated with a dropdown.
 * 
 * @author		Ken Beukelman
 */
public class TextPair
{
	//
	// Static data.
	//
	private static final long serialVersionUID = 1L;

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String key;
	private String label;

	//
	// Constructors.
	//
	public TextPair()
	{
		// Does nothing presently
	}

	public TextPair(String s1, String s2)
	{
		this.key = s1;
		this.label = s2;
	}

	//
	// Instance methods.
	//
	
	
	public String toString()
	{
		String str = "TextPair: key=" + this.key + ", label=" + this.label;
		
		return str;
	}
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public String getKey()
	{
		return key;
	}
	
	public void setKey(String value)
	{
		key = value;
	}	

	/*****************************************************************************************/
	public String getLabel()
	{
		return label;
	}
	
	public void setLabel(String value)
	{
		label = value;
	}	
}
