/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.dto.setup;

import java.util.ArrayList;


/**
 * ReportModelSuperFactorDTO is a container for a report model superfactor.
 * 
 * @author		Ken Beukelman
 */
public class ReportModelSuperFactorDTO
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private long _rmSfId;
	private long _defaultTextId;
	private long _userTextId;
	private String _displayName;
	private int _seq;
	private ArrayList<ReportModelCompetencyDTO> _compList;

	//
	// Constructors.
	//
	public ReportModelSuperFactorDTO()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
	
	/*****************************************************************************************/
	public long getSuperFactorId()
	{
		return _rmSfId;
	}
	
	public void setSuperFactorId(long value)
	{
		_rmSfId = value;
	}
	
	/*****************************************************************************************/
	public long getDefaultTextId()
	{
		return _defaultTextId;
	}
	
	public void setDefaultTextId(long value)
	{
		_defaultTextId = value;
	}
	
	/*****************************************************************************************/
	public long getUserTextId()
	{
		return _userTextId;
	}
	
	public void setUserTextId(long value)
	{
		_userTextId = value;
	}
	
	/*****************************************************************************************/
	public String getDisplayName()
	{
		return _displayName;
	}

	public void setDisplayName(String value)
	{
		_displayName = value;
	}
	
	/*****************************************************************************************/
	public int getSeq()
	{
		return _seq;
	}

	public void setSeq(int value)
	{
		_seq = value;
	}
	
	/*****************************************************************************************/
	public ArrayList<ReportModelCompetencyDTO> getCompList()
	{
		if (_compList == null)
			_compList = new ArrayList<ReportModelCompetencyDTO>();
		return _compList;
	}

	public void setCompList(ArrayList<ReportModelCompetencyDTO> value)
	{
		_compList = value;
	}
	
	
	public String toString()
	{
		String str = "  ReportModelSuperFactorDTO:  SF ID=" + _rmSfId + " - " + _displayName + " (" + _seq + ")\n";
		if (_compList == null  || _compList.isEmpty())
		{
			str += "    No competency data...\n";
		}
		else
		{
			for(int i = 0; i < _compList.size(); i++)
			{
				str += _compList.get(i).toString();
			}
		}

		
		return str;
	}
}
