package com.pdi.data.v2.helpers.delegated.ut;

import java.util.HashMap;
import java.util.Iterator;

import com.pdi.data.dto.Norm;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.INormHelper;

import com.pdi.data.v2.util.V2DatabaseUtils;

import com.pdi.ut.UnitTestUtils;
import junit.framework.TestCase;

public class NormHelperUT extends TestCase {
	public NormHelperUT(String name)
	{
		super(name);
	}
	
	public void testFromId()
	throws Exception
	{
		
		SessionUser session = new SessionUser();
		session.getMetadata().put("USERNAME", "mbpanichi");
		session.getMetadata().put("PASSWORD", "griffin12");
		UnitTestUtils.start(this);
		V2DatabaseUtils.setUnitTest(true);
	
		INormHelper iNorm = HelperDelegate.getNormHelper();
		
		Norm norm = iNorm.fromId(session, 6221);
		
		//normId	mean	stDev	spssValue
		//6221		4.12	0.981000	GPI_OPEN
		
		System.out.println("test from Id....");
		System.out.println("     " + norm.getId());
		System.out.println("     " + norm.getMean());
		System.out.println("     " + norm.getStandardDeviation());
		System.out.println("     " + norm.getSpssValue());
		UnitTestUtils.stop(this);

	}
	
	public void testFromNormGroupId()
	throws Exception
	{
		SessionUser session = new SessionUser();
		session.getMetadata().put("USERNAME", "mbpanichi");
		session.getMetadata().put("PASSWORD", "griffin12");
		UnitTestUtils.start(this);
		V2DatabaseUtils.setUnitTest(true);
		
		INormHelper iNorm = HelperDelegate.getNormHelper();
		
		HashMap<String, Norm> norms = iNorm.fromNormGroupId(session, 159);
		
		//normId		createdBy	normGroupId	V2ConstructId	mean	stdev	spssValue	archived	V2NormGroupId
		//6216			SYSTEM		159			HPOZYCGM		4.702	1.354	GPI_EL		0			CSTWFHAX

		Iterator<String> it = norms.keySet().iterator();
		
		System.out.println("test from Norm Group Id....");
		
		while(it.hasNext()){
			String e = it.next();
			Norm norm = norms.get(e);		
			System.out.println("     " + norm.getId());
			System.out.println("     " + norm.getMean());
			System.out.println("     " + norm.getStandardDeviation());
			System.out.println("     " + norm.getSpssValue());
			System.out.println("--------------------");
		}

		UnitTestUtils.stop(this);
	}
	
	public void testFromSpssValueGroupNormId()	
	throws Exception
	{
		
		SessionUser session = new SessionUser();
		session.getMetadata().put("USERNAME", "mbpanichi");
		session.getMetadata().put("PASSWORD", "griffin12");
		UnitTestUtils.start(this);
		V2DatabaseUtils.setUnitTest(true);
		
		INormHelper iNorm = HelperDelegate.getNormHelper();
		
		Norm norm = iNorm.fromSpssValueGroupNormId(session, "GPI_EL", 159);
		
		//normId		createdBy	normGroupId	V2ConstructId	mean	stdev	spssValue	archived	V2NormGroupId
		//6216			SYSTEM		159			HPOZYCGM		4.702	1.354	GPI_EL		0			CSTWFHAX
		
		System.out.println("test from SpssValue, NormGroupId....");
		System.out.println("     " + norm.getId());
		System.out.println("     " + norm.getMean());
		System.out.println("     " + norm.getStandardDeviation());
		System.out.println("     " + norm.getSpssValue());

		UnitTestUtils.stop(this);
	}
	
	
}
