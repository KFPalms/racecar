/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.util;

//import java.io.PrintWriter;
//import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.Random;
//import java.util.Stack;
//import java.util.Random;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * The DatabaseUtils class provides a place to put miscellaneous
 * database accessutility methods valid for use in more than one
 * application or application family
 *
 * @author		Gavin Myers
 */
public class DatabaseUtils
{
	//
	// Static data.
	//
	
	// DB Action types
	public static final char DBA_INSERT = 'I';
	public static final char DBA_SELECT = 'S';
	public static final char DBA_UPDATE = 'U';
	public static final char DBA_DELETE = 'D';

	private static boolean isUnitTest = false;
	private static String unitTestClass = "";
	private static String unitTestPath = "";
	private static String unitTestUsername = "";
	private static String unitTestPassword = "";

	private static Connection connection = null;

	//
	// Static methods.
	//

	// Getter for unit test flag
	public static boolean isUnitTest()
	{
		return isUnitTest;
	}

	
	/**
	 * Is the request actually a unit test?
	 */
	public static void prepareUnitTest(String jdbc, String username, String password, String unitTestClass)
	{
		DatabaseUtils.unitTestPath = jdbc;
		DatabaseUtils.unitTestClass = unitTestClass;
		DatabaseUtils.unitTestUsername = username;
		DatabaseUtils.unitTestPassword = password;
		DatabaseUtils.isUnitTest = true;
	}
	
	/**
	 * Get a DB connection for use by a call (for unit testing only)
	 * 
	 * @param the environment (development, preproduction, production)
	 * @return a DataLayerConnection object
	 */
	private static DataLayerConnection getDBConnectionUT(String environment)
	{
		Connection con = null;
		DataLayerConnection dlCon = new DataLayerConnection();
		
		try
		{
    		//Gavin: HUUUUUGE PERF IMPROVEMENT OVER BUILDING A NEW CONNECTION EACH TIME
			//Not thread safe, use only for UTs
			//Maybe we don't have to do this for UTs
			if(connection != null && !connection.isClosed()) {
				//dlCon.setConnection(connection);
				//return dlCon;
			}

			//TODO: the user/pass/url should be determined by the sub data classes,
			//      not the main DatabaseUtils
			Class.forName(DatabaseUtils.unitTestClass);
			//Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			//Class.forName("com.mysql.jdbc.Driver");

    		// open a connection
			con = DriverManager.getConnection(DatabaseUtils.unitTestPath,
											  DatabaseUtils.unitTestUsername,
											  DatabaseUtils.unitTestPassword);

    		if (con == null)
    		{ 
    			dlCon.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
    					        "No export connection; make sure pools are configured correctly."));
     		}
    		else
    		{
    			dlCon.setConnection(con);
    		}
		} 
		catch (SQLException ex)
		{
			ex.printStackTrace();
            // handle any errors
			dlCon.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
							ex.getErrorCode(),
							"Unit Test could not connect to database.  Msg=" + ex.getMessage() + "."));
        }
        catch (Exception ex)
		{
        	ex.printStackTrace();
			dlCon.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
						"Export connection setup failed for " + environment +
						".  Msg=" + ex.getMessage() + "."));
		}

        //Gavin: HUUUUUGE PERF IMPROVEMENT OVER BUILDING A NEW CONNECTION EACH TIME
        connection = dlCon.getConnection();
        
		return dlCon;
	}

	//public static HashMap<String, DataLayerConnection> connections = new HashMap<String, DataLayerConnection>();

	/**
	 * Get a DB connection
	 * 
	 * @param environment The environment (development, preproduction, production)
	 * @param forceNew Does not appear to be used
	 * @return a DataLayerConnection object
	 */
	public static DataLayerConnection getDBConnection(String environment, boolean forceNew)
	{

        
		if(DatabaseUtils.isUnitTest())
		{
			return DatabaseUtils.getDBConnectionUT(environment);
		}
		
		Connection con = null;
		DataLayerConnection dlCon = new DataLayerConnection();

		try
		{

			//System.out.println("java:comp/env/"+environment);
    		DataSource ds = null;
    	    Context initCtx = new InitialContext();
    	    Context envCtx = (Context) initCtx.lookup("java:comp/env");
    	    try {
    	     ds = (DataSource) envCtx.lookup(environment);
    	     } catch (NamingException ne) {
    	    	 ds = (DataSource) initCtx.lookup(environment);
    	     }

    		if (ds == null)
    		{
    			throw new IllegalArgumentException("Unable to initialize Export data source");
    		}

    		// open a connection
    		con = ds.getConnection();

    		if (con == null)
    		{
     			dlCon.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
		        		"No export connection; make sure pools are configured correctly."));
     			System.out.println("No export connection; make sure pools are configured correctly.");
    		}
    		else
    		{
    			dlCon.setConnection(con);
    			
    		}
    		
		}
		catch (SQLException ex)
		{
            // handle any errors
			dlCon.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
					ex.getErrorCode(),
					"Export connection attempt failed.  Msg=" + ex.getMessage() + "."));
			System.out.println("Export connection attempt failed.  Msg=" + ex.getMessage() + ".");
			ex.printStackTrace();
        }
        catch (Exception ex)
		{
        	ex.printStackTrace();
			dlCon.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
					"Export connection setup failed for " + environment +
					".  Msg=" + ex.getMessage() + "."));
			System.out.println("Export connection setup failed for " + environment +
					".  Msg=" + ex.getMessage() + ".");
		}
        
/*
        StringWriter sw = new StringWriter();
        new Throwable("").printStackTrace(new PrintWriter(sw));
        String stackTrace = sw.toString();
		Random generator = new Random();
		int randomIndex = generator.nextInt( 10000 ) * 10000;
		
        connections.put(randomIndex + " " + stackTrace, dlCon);
*/
		return dlCon;
	}


	/**
	 * Get a DB connection for use by a call
	 * @param the environment (development, preproduction, production)
	 * @return a DataLayerConnection object
	 */
	public static DataLayerConnection getDBConnection(String environment)
	{
		return getDBConnection(environment, false);
	}
	
	
	/**
	 * Builds a prepared statement from a SQL string
	 *   and returns it in a DataLayerPreparedStatement object.
	 * @param the environment (development, preproduction, production)
	 * @param the sql to build
	 * @return a DataLayerPreparedStatement object
	 */
	public static DataLayerPreparedStatement prepareStatement(String environment, StringBuffer sql)
	{
		DataLayerConnection dlCon = getDBConnection(environment);
		if (dlCon.isInError())
		{
			return new DataLayerPreparedStatement(dlCon.getStatus());
		}
	
		try
		{
			PreparedStatement ps = dlCon.getConnection().prepareStatement(sql.toString());
			return new DataLayerPreparedStatement(ps, DataLayerPreparedStatement.NO_AUTO_KEYS);
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			return new DataLayerPreparedStatement(
					new RequestStatus(RequestStatus.RS_ERROR, ex.getErrorCode(), ex.getMessage()));
		}
	}
	
	
	/**
	 * Builds a prepared statement from a sql string and an auto-generate keys constant
	 *   and returns it in a DataLayerPreparedStatement object.
	 * @param the environment (development, preproduction, production)
	 * @param the SQL to build
	 * @param an autoGenerateKeys constant
	 * @return a result set
	 * @throws Exception
	 */
	public static DataLayerPreparedStatement prepareStatement(String environment,
															  StringBuffer sql,
															  int autogen)
	{
		DataLayerConnection dlCon = getDBConnection(environment);
		if (dlCon.isInError())
		{
			return new DataLayerPreparedStatement(dlCon.getStatus());
		}
		
		try
		{
			PreparedStatement ps = dlCon.getConnection().prepareStatement(sql.toString(), autogen);
			
			return new DataLayerPreparedStatement(ps, DataLayerPreparedStatement.HAS_AUTO_KEYS);
		}
		catch (SQLException ex)
		{
			return new DataLayerPreparedStatement(
					new RequestStatus(RequestStatus.RS_ERROR, ex.getErrorCode(), ex.getMessage()));
		}
	}


	/**
	 * Execute a query - The centralized database access (new type... returns DataResult)
	 * Note that there are no cross-checks to ensure that the data type reflects the actual
	 *   function encapsulated in the PreparedStatement object.  If they do not match, however,
	 *   expected data may not be present and errors could be thrown (query vs. update).
	 *    
	 * @param the environment (development, preproduction, production)
	 * @param the PreparedStatement to execute
	 * @param the type of query (determines what data is set in the DataResult object) 
	 * @return a DataResult object
	 * @throws Exception
	 */
	public static DataResult execute(String environment, DataLayerPreparedStatement dlps, char type)
	{
		DataResult dr = new DataResult();
		
		PreparedStatement ps = dlps.getPreparedStatement();
		
		dr.setPreparedStatement(ps);
		
		try
		{
			ResultSet rs = null;
			int ret = 0;
			
			switch (type)
			{
				case DBA_INSERT:
					ret = ps.executeUpdate();
					dr.setRowCount(ret);
//					
//					//This a UT specific fix:
//					// We are wrapping the getGeneratedKeys call in a try/catch because the UnitTest
//					// environment is different from the Tomcat environment.  In UnitTest, the
//					// PreparedStatement object is a MySQL is a JDBC4PreparedStatement which will
//					// throw an exception ("!Statement.GeneratedKeysNotRequested!") when the
//					// getGeneratedKeys() method is called if the statement was not prepared with
//					// the RETURN_GENERATED_KEYS parameter present.
//					//
//					// In the Tomcat environment, the object is a DelegatedPreparedStatement object,
//					// which apparently handles this internally with no problem.
//					if(DatabaseUtils.isUnitTest)
//					{
//						try
//						{
//							rs = ps.getGeneratedKeys();
//							if (rs.isBeforeFirst())
//							{
//								rs.next();
//								dr.setNewId(rs.getLong(1));
//							}
//						}
//						catch(SQLException sql)
//						{
//							System.out.println("SQLException detected upon retrieval of generated key.  Exception ignored.");
//							System.out.println("    Error code=" + sql.getErrorCode() + ", msg=" + sql.getMessage());
//						}
//					}
//					else
//					{
//						rs = ps.getGeneratedKeys();
//						if (rs.isBeforeFirst())
//						{
//							rs.next();
//							dr.setNewId(rs.getLong(1));
//						}
//					}
					try
					{
						if (dlps.genAutoKeys())
						{
							rs = ps.getGeneratedKeys();
							if (rs.isBeforeFirst())
							{
								rs.next();
								dr.setNewId(rs.getLong(1));
							}
						}
					}
					catch (IllegalStateException ise)
					{
						dr.setStatus(new RequestStatus(RequestStatus.RS_ERROR,
								"Invalid insert setup - " + ise.getMessage()));
					}

					break;
				case DBA_SELECT:
					rs = ps.executeQuery();
					if (! rs.isBeforeFirst())
					{
						dr.getStatus().setStatusCode(RequestStatus.DBS_NO_DATA);
					}
					else
					{
						dr.setResultSet(rs);
					}
					break;
				case DBA_UPDATE:
				case DBA_DELETE:
					ret = ps.executeUpdate();
					dr.setRowCount(ret);
					break;
				default:
					dr.getStatus().setStatusCode(RequestStatus.RS_ERROR);
					dr.getStatus().setExceptionMessage("Locic Error:  Invalid DB Access type - " + type);
					break;
			}
			
			return dr;
		}
		catch (SQLException ex)
		{
			//Set up the return
			dr.getStatus().setStatusCode(RequestStatus.RS_ERROR);
			dr.getStatus().setExceptionCode(ex.getErrorCode());
			dr.getStatus().setExceptionMessage(ex.getMessage());
			
			// Log the error
			String exc = "\n************************************* " + 
				"\nDatabaseUtils.execute Could not execute SQL query " +
				"\nContext - " + type +
				"\nSQLException: " + dr.getStatus().getExceptionMessage() + " " +
				"\nSQLState: " + ex.getSQLState() + " " +
				"\nVendorError: " + dr.getStatus().getExceptionCode();
			System.err.println(exc);
			ex.printStackTrace();
			System.err.println("************************************* ");
			
			return dr;
		}
	}
	

	//
	// Instance data.
	//


	//
	// Constructors.
	//


	//
	// Instance methods.
	//

}