/**
 * Copyright (c) 2008 Personnel Decisions International, Inc.
 * All rights reserved.
 */

package com.pdi.data.abyd.helpers.setup;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.pdi.data.abyd.dto.setup.DNADescription;
//import com.pdi.data.dto.Project;
//import com.pdi.data.dto.SessionUser;
//import com.pdi.data.helpers.HelperDelegate;
//import com.pdi.data.helpers.interfaces.IProjectHelper;

/**
 * SaveDNADescHelper contains the code needed to save the contents of the
 * DNADescription object.  There are actually two methods that save different
 * portions of the data (and have different functionality as to looking
 * things up and so forth) as apportioned in the UI (DNA screens 1 and 3).
 * 
 * See additional comments at each of the function calls.
 *
 * @author		Ken Beukelman
 * @version	$Revision: 6 $  $Date: 8/25/05 3:23p $
 */
public class SaveDNADescHelper
{
	//
	// Static data.
	//

//	private static final String TRANS_LVL_KEY = "transitionLevel";
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private Connection connection;
	private DNADescription dnaDesc;
	
	//
	// Constructors.
	//
	//protected SaveDNADescHelper(Connection con, DNADescription dd, Properties props)
	public SaveDNADescHelper(Connection con, DNADescription dd)
		throws Exception
	{
		this.connection = con;
		this.dnaDesc = dd;
	}

	//
	// Instance methods.
	//

	/**
	 * Insert or update the first set of DNA data (model id, name, client
	 * manager info).  This may do an insert (upon first entry) or an
	 * update (on subsequent calls for the same DNA).  It returns the
	 * updated DNADescription object even though there may not be updates
	 * to it once the dna is established.
	 *
	 * @return the dnaId
	 * @throws Exception
	 */
	public DNADescription saveNameModelCmgr()
		throws Exception
	{
		// Assume that the user knows what they are doing.  If they send
		// a dna id of 0, this is an insert.  If it's something else, it's
		// an update.  If they send the data in the DNADescription object
		// that they got initially, they should be fine.
		Statement stmt = null;
		ResultSet rs = null;
		StringBuffer sqlQuery = new StringBuffer();
		
		// Muck about with quotes in the name string; " -> '
		// This prevents "Bad SQL" messages for strings that include double quotes
		this.dnaDesc.setDnaName(this.dnaDesc.getDnaName().trim().replace("'","''"));
		//Client manager name/email is no longer used... put out empty strings
		//this.dnaDesc.setcMgrName(this.dnaDesc.getcMgrName().trim().replace("'","''"));
		//this.dnaDesc.setcMgrEmail(this.dnaDesc.getcMgrEmail().trim().replace("'","''"));
		this.dnaDesc.setcMgrName("");
		this.dnaDesc.setcMgrEmail("");
		
		this.dnaDesc.setSuccessStatus(true);	// Precondition for success
		this.dnaDesc.setStatusMsg(null);
		
		try
		{
			if (this.dnaDesc.getDnaId() == 0)
			{
				//insert
				sqlQuery.append("INSERT INTO pdi_abd_dna ");
				sqlQuery.append("  (dnaName, projectId, modelId, cMgrName, cMgrEmail, active, lastUserId, lastDate) ");
				sqlQuery.append("  VALUES(N'" + this.dnaDesc.getDnaName() + "', " +
										   this.dnaDesc.getJobId() + ", " +
										 this.dnaDesc.getModelId() + ", " +
										 "N'" + this.dnaDesc.getcMgrName() + "', " +
										 "N'" + this.dnaDesc.getcMgrEmail() + "', " +
										 "1, " + 
					  					 "SYSTEM_USER, " +
										 "GETDATE())");
				try
				{
					stmt = this.connection.createStatement();
					stmt.executeUpdate(sqlQuery.toString(), Statement.RETURN_GENERATED_KEYS);
					rs = stmt.getGeneratedKeys();
					rs.next();
					//ret = rs.getLong(1);
					this.dnaDesc.setDnaId(rs.getLong(1));
				}
		        catch (SQLException ex)
				{
	        		// Report error
	        		throw new Exception("SQL inserting initial DNA data.  " +
	        				"SQLException: " + ex.getMessage() + ", " +
	        				"SQLState: " + ex.getSQLState() + ", " +
	        				"VendorError: " + ex.getErrorCode());
				}
			}
			else
			{
//				//update
				// get the current model id to see if we are changing the model
				sqlQuery.append("SELECT modelId ");
				sqlQuery.append("  FROM pdi_abd_dna ");
				sqlQuery.append("  WHERE dnaId = " + this.dnaDesc.getDnaId());
				try
				{
					stmt = this.connection.createStatement();
					rs = stmt.executeQuery(sqlQuery.toString());
				}
		        catch (SQLException ex)
				{
		            // handle any errors
		        	throw new Exception("SQL in model check - initial DNA data.  " +
		        			"SQLException: " + ex.getMessage() + ", " +
		        			"SQLState: " + ex.getSQLState() + ", " +
							"VendorError: " + ex.getErrorCode());
		        }

				if (! rs.isBeforeFirst())
				{
					throw new Exception("DNA named " + this.dnaDesc.getDnaName() +
										" (Id=" + this.dnaDesc.getDnaId() +
										") does not exist.");
				}
				
				//boolean sendEmail = false;
		       	
				// Get the result row and see if the model is changing
				rs.next();
				if (this.dnaDesc.getModelId() != rs.getInt(1))
				{
					// If the ModelId is changing and the Success Profile is already
					// complete then we must send an email to notify the Client Manager.
					//
					// We check the SpComplete value here before it's gets changed
					// in the next step, resetDNA(). But wait until the updates to the 
					// database are complete before sending the email.
					/* NOTE: Based on email from Vidula Kale Dec.18, 2008 we will not send an email at this point.
	       	     		 	 Commenting out code and leaving it here based on conversation with Ken B.
	       	     			 See additional code below in this method also.
	       	    	if (this.dnaDesc.getSpComplete() == Boolean.TRUE)
	       	    	{
	       	     		sendEmail = true;
	       	    	}
					 */
	       	    
					// Model change; delete the data associated with this dna id desc,
					// structure, and sp data... no feedback unless there is an error
					resetDNA(this.dnaDesc.getDnaId());
					
					// It is up to the user to manage structure and SP data on the UI side
					// We are just returning the updated DNA
					this.dnaDesc.setDnaComplete(Boolean.FALSE);
					this.dnaDesc.getSynthNorm().setMean(0.0);
					this.dnaDesc.getSynthNorm().setStdDev(0.0);
					this.dnaDesc.setWbVersion(null);
					this.dnaDesc.setDnaNotes(null);
				}

				// clear the statement and result set
				String where=null;
				try
				{
					where = "rs";
					rs.close();
					rs = null;
					where = "stmt";
					stmt.close();
					stmt = null;
				}
				catch (SQLException sqlEx)
				{
					System.out.println("ERROR: " + where + " close in saveNameModelCmgr (model check cleanup) - " + sqlEx.getMessage());
				}

                // Model change check done... update the four fields
				sqlQuery.setLength(0);
				sqlQuery.append("UPDATE pdi_abd_dna ");
				sqlQuery.append("  SET modelId = " + this.dnaDesc.getModelId() + ", ");
				sqlQuery.append("      dnaName = N'" + this.dnaDesc.getDnaName() + "', ");
				sqlQuery.append("      cMgrName = N'" + this.dnaDesc.getcMgrName() + "', ");
				sqlQuery.append("      cMgrEmail = N'" + this.dnaDesc.getcMgrEmail() + "' ");
				sqlQuery.append("  WHERE dnaId = " + this.dnaDesc.getDnaId());

				try
				{
					stmt = this.connection.createStatement();
					stmt.executeUpdate(sqlQuery.toString());
				}
		        catch (SQLException ex)
				{
	        		throw new Exception("SQL - update initial DNA data.  " +
	        							"SQLException: " + ex.getMessage() + ", " +
	        							"SQLState: " + ex.getSQLState() + ", " +
	        							"VendorError: " + ex.getErrorCode());
		        }

		       	//ret = dd.getDnaId();	// Return the input DNA ID
		       	
	       	    /* NOTE: Based on email from Vidula Kale Dec.18, 2008 we will not send an email at this point.
  	     		 		 Commenting out code and leaving it here based on conversation with Ken B.
  	     		 		 See additional code above in this method also.
		       	if (sendEmail)
		       	{
	       		    String host = this.properties.getProperty("abd.emailServer");
	       		    EmailRequestService emailService = new EmailRequestService(host);

	       	        EmailMessage email = new EmailMessage();
	       	        email.setToAddress(this.dnaDesc.getcMgrEmail());
	       	        String fromAddr = this.properties.getProperty("abd.dna.clientMgr.fromAddress");
	       	        email.setFromAddress(fromAddr);
	       	        String emailSubject = this.properties.getProperty("abd.dna.clientMgr.emailSubject");
	       	        email.setSubject(emailSubject);
	       	        String emailText = this.properties.getProperty("abd.dna.clientMgr.emailText");
	       	        email.setText(emailText);
	       	        email.setTextSubstitutions("");
	       	        
	       	        emailService.sendEmail(email);	   		       	    
		       	}
		       	*/
			}

	        //return ret;
			return this.dnaDesc;
		}
		finally
		{
			String where = null;
			try
			{
				if (rs != null)
				{
					where = "rs";
					rs.close();
					rs = null;
				}
	            if (stmt != null)
	            {
					where = "stmt";
	                stmt.close();
	                stmt = null;
	            }
			}
			catch (SQLException sqlEx)
			{
				System.out.println("ERROR: " + where + "close in saveNameModelCmgr (model check) - " + sqlEx.getMessage());
			}
        }
	}


	/**
	 * ResetDNA deletes all structure data and resets description data to the initial state.
	 * It always returns the dnaId.
	 *
	 * @return the dnaId
	 * @throws Exception
	 */
	private void resetDNA(long dnaId)
		throws Exception
	{
		Statement stmt = null;
		StringBuffer sqlQuery = new StringBuffer();
		String phase = null;
		//int cnt;

		try
		{
			// Reset DNA description to near initial state (leave old model id, name and project id)
			phase = "DNA Description";
			sqlQuery.append("UPDATE pdi_abd_dna ");
			sqlQuery.append("  SET dnaSubmitted = 0, ");
			sqlQuery.append("      synthMean = null, ");
			sqlQuery.append("      synthStdDev = null, ");
			sqlQuery.append("      wbVersion = null, ");
			sqlQuery.append("      dnaNotes = null ");
			sqlQuery.append("  WHERE dnaId = " + dnaId);

	       	stmt = this.connection.createStatement();
	       	//cnt = stmt.executeUpdate(sqlQuery.toString());
	       	stmt.executeUpdate(sqlQuery.toString());

			try	{  stmt.close();  }
            catch (SQLException sqlEx)	{  /* swallow the error */  }
            stmt = null;

            // Delete the structure
            sqlQuery.setLength(0);
			sqlQuery.append("DELETE FROM pdi_abd_dna_link ");
			sqlQuery.append("  WHERE dnaId = " + dnaId);

	       	stmt = this.connection.createStatement();
	       	//cnt = stmt.executeUpdate(sqlQuery.toString());
	       	stmt.executeUpdate(sqlQuery.toString());

			try	{  stmt.close();  }
            catch (SQLException sqlEx)	{  /* swallow the error */  }
            stmt = null;
		}
        catch (SQLException ex)
		{
            // handle any errors
        	throw new Exception("SQL " + phase + " reset.  " +
        			"SQLException: " + ex.getMessage() + ", " +
        			"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
        }
        finally
		{
            if (stmt != null)
            {
                try	{  stmt.close();  }
                catch (SQLException sqlEx)	{  /* swallow the error */  }
                stmt = null;
            }
        }
	}


	/**
	 * Update the second screen's worth of data.  Returns nothing as the DNA is
	 * already established.  Update only for the same reason.  When the last DNA
	 * data is saved, we need to set the "DNA Complete" flag as well
	 *
	 * @throws Exception
	 */
	public void saveMoreDNAData()
		throws Exception
	{
		Statement stmt = null;
		ResultSet rs = null;
		String phase = null;
		//String model;
//		int tlti;	// tlti = tartget level type ID
//		String projId;
		
		// Muck about with quotes in the strings; " -> '
		// This prevents "Bad SQL" messages for strings that include double quotes
		this.dnaDesc.setWbVersion(this.dnaDesc.getWbVersion().trim().replace("'","''"));
		this.dnaDesc.setDnaNotes(this.dnaDesc.getDnaNotes().trim().replace("'","''"));
		
		// update the four fields and the "DNA Complete" flag
		StringBuffer sqlQuery = new StringBuffer();
		phase = "Update";
		sqlQuery.append("UPDATE pdi_abd_dna ");
		sqlQuery.append("  SET synthMean = " + this.dnaDesc.getSynthNorm().getMean() + ", ");
		sqlQuery.append("      synthStdDev = " + this.dnaDesc.getSynthNorm().getStdDev() + ", ");
		sqlQuery.append("      wbVersion = '" + this.dnaDesc.getWbVersion() + "', ");
		sqlQuery.append("      dnaNotes = N'" + this.dnaDesc.getDnaNotes() + "', ");
		sqlQuery.append("      dnaSubmitted = 1 ");
		sqlQuery.append("  WHERE dnaId = " + this.dnaDesc.getDnaId());

		try
		{
			// Updating
			stmt = this.connection.createStatement();
			stmt.executeUpdate(sqlQuery.toString());
			
// Removed this code as it was used for the linkage deally code commented out below
//			// Getting data needed for nhn update
//			sqlQuery.setLength(0);
//			phase = "Fetch";
//			sqlQuery.append("SELECT dna.projectId, ");
////			sqlQuery.append("		mdl.internalName, ");
//			sqlQuery.append("		mdl.targetLevelTypeId ");
//			sqlQuery.append("  FROM pdi_abd_dna dna ");
//			sqlQuery.append("    INNER JOIN pdi_abd_model mdl ON mdl.modelId = dna.modelId ");
//			sqlQuery.append("  WHERE dna.dnaId = " + this.dnaDesc.getDnaId());
//			
//			rs = stmt.executeQuery(sqlQuery.toString());
//			if (rs == null || ! rs.isBeforeFirst())
//			{
//				throw new Exception("Unable to fetch model or project data in saveMoreDNAData; dnaId=" + this.dnaDesc.getDnaId());
//			}
//			rs.next();	// Get the single line
////			model = rs.getString("internalName");
//			tlti = rs.getInt("targetLevelTypeId");
//			projId = rs.getString("projectId");
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL saveMoreDNAData - phase=" + phase + ".  " +
        			"SQLException: " + ex.getMessage() + ", " +
        			"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (rs != null)
			{
				try  {  rs.close();  }
				catch (SQLException sqlEx)  {  /* Swallow the error */  }
				rs = null;
			}
			if (stmt != null)
			{
				try  {  stmt.close();  }
				catch (SQLException sqlEx)  {  /* Swallow the error */  }
				stmt = null;
			}
		}

//		// Save the model name to platform metadata as transitionLevel
//		SessionUser su = new SessionUser();		// Dummy SessionUser object
//		IProjectHelper ph = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated");
//		HashMap<String, String> newMeta = new HashMap<String, String>();
//		newMeta.put(TRANS_LVL_KEY, model);
//		//newMeta.put("Bogus", "Bogus");
//		ph.updateMetadataItems(su, projId, newMeta);
		// The above code existed and was commented out when the following code was operative

		// Removed this linkage as it does not correctly map to the PALMS target levels (and it hoses stuff in ALR).
//		// Save the target level type id (transition level identifier) to the PALMS project row
//		SessionUser su = new SessionUser();		// Dummy SessionUser object
//		IProjectHelper ph = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated");
//		// Get the exiting project
//		Project proj = ph.fromId(su, projId);
//		// Update the level id - tlti
//		proj.setTargetLevelTypeId(tlti);
//		// update the core data
//		ph.updateCoreProject(su, proj);


		return;
	}
}
