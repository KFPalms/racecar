package com.pdi.data.v2;

public class ApplicationData {
	private static final ApplicationData singleton = new ApplicationData();
	
	private static ApplicationData getInstance() {
		return singleton;
	}
	
	protected static void setId(String id) {
		ApplicationData.getInstance().id = id;
	}
	protected static String getId() {
		return ApplicationData.getInstance().id;
	}
	private String id = "V2";
}
