/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.dto;

import com.pdi.data.util.BaseDTO;
import com.pdi.data.util.RequestStatus;

/**
 * DTO for project information
 * 
 *
 * @author		Gavin Myers
 */
public class ReportDTO extends BaseDTO
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private long _reportId;
	private String _reportName;
	private String _reportCode;

	private ItemStatusDTO _itemStatus;
	
	//
	// Constructors.
	//

	/**
	 * "Standard" (no-parameter) constructor
	 */
	public ReportDTO()
	{
		super();
		// Does nothing presently
	}
	
	// Status setup constructor - language data remains unset
	public ReportDTO(RequestStatus stat)
	{
		this.setStatus(stat);
	}	
	
	//
	// Instance methods.
	//
	
	/**
	 * Generic toString method
	 */
	public String toString() {
		String str = "\n --- ReportDTO --- ";
		str += this.getStatus().toString();
		str += "\nid:            " + this.getReportId();
		str += "\nName:          " + this.getReportName();
		str += this.getClass().toString();
		//str += this.getItemStatus().toString();
		//str += "\nVersion:       " + this.getVersion();
		return str;
	}
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
	
	/*****************************************************************************************/

	/*****************************************************************************************/
	public void setReportName(String value)
	{
		_reportName = value;
	}
	
	public String getReportName()
	{
		return _reportName;
	}
	
	/*****************************************************************************************/
	public void setItemStatus(ItemStatusDTO value)
	{
		_itemStatus = value;
	}
	
	public ItemStatusDTO getItemStatus()
	{
		return _itemStatus;
	}

	public void setReportId(long _reportId) {
		this._reportId = _reportId;
	}

	public long getReportId() {
		return _reportId;
	}

	public void setReportCode(String _reportCode) {
		this._reportCode = _reportCode;
	}

	public String getReportCode() {
		return _reportCode;
	}

}

