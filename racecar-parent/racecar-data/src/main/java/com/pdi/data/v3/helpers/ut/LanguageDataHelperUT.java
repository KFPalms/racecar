/*
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.helpers.ut;


import java.util.ArrayList;

import com.pdi.data.util.RequestStatus;
import com.pdi.data.v3.dto.LanguageDTO;
import com.pdi.data.v3.dto.LanguageListDTO;
import com.pdi.data.v3.dto.SessionUserDTO;
import com.pdi.data.v3.helpers.LanguageDataHelper;
import com.pdi.data.v3.util.V3DatabaseUtils;
import com.pdi.properties.PropertyLoader;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

public class LanguageDataHelperUT extends TestCase
{
	
	//
	// Constructors
	//

	public LanguageDataHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args) throws Exception
    {
		junit.textui.TestRunner.run(LanguageDataHelperUT.class);
    }

	/*
	 * testProperties
	 */
	public void testProperties()
		throws Exception
	{
    	UnitTestUtils.start(this);
    	
    	//Does the local property work?
    	String applicationId = PropertyLoader.getProperty("com.pdi.data.v3.application", "application.code");
    	assertEquals(applicationId != null, true);
    	assertEquals(applicationId.equals("PDI-DATA-V3"), true);
    	 
    	UnitTestUtils.stop(this);
    }
    
    /*
     * testGet
     */
	public void testGet() throws Exception
	{
		UnitTestUtils.start(this);
		V3DatabaseUtils.setUnitTest(true);
     	
    	SessionUserDTO sessionUser = new SessionUserDTO();
    	sessionUser.setUsername(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.username")); 
    	sessionUser.setPassword(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.password"));

		System.out.println("URL=" + PropertyLoader.getProperty("com.pdi.data.v3.application", "database.url"));

		// Actual test code starts here
		
		// Check the insert
		LanguageDTO insertDto = new LanguageDTO();
		insertDto.setIsoCode("en-UT");
    	insertDto.setName("Unit Test language");
    	LanguageDTO insReturn = LanguageDataHelper.addLanguage(sessionUser, insertDto);
		assertEquals("addLanguage status != OK",
				RequestStatus.RS_OK, insReturn.getStatus().getStatusCode());
		assertFalse("addLanguage id not returned", (0 == insReturn.getId()));
		System.out.println("Insert succeeded; New ID = " + insReturn.getId());
		
 		// Now check the select for that item
		LanguageDTO langData = LanguageDataHelper.getLanguage(sessionUser, insReturn.getId());
		assertEquals("getLanguage status != OK",
				RequestStatus.RS_OK, langData.getStatus().getStatusCode());
		assertEquals("getLanguage returned incorrect name",
				insertDto.getName(), langData.getName());
		assertEquals("getLanguage returned incorrect ISO code",
				insertDto.getIsoCode(), langData.getIsoCode());
		System.out.println("Get succeeded");

    	// Test the "Get All languages" cross-referenced to the get single language
   		LanguageListDTO listDto = LanguageDataHelper.getAllLanguages(sessionUser);
		assertEquals("getAllLanguages status != OK.",
					 RequestStatus.RS_OK, listDto.getStatus().getStatusCode());

		ArrayList<LanguageDTO> langList = listDto.getLangList();
		for (LanguageDTO lang : langList)
		{
		//	//Print out the recipient type
		//	System.out.println(lang.toString());

			//For fun... let's ask the db to get the same thing AGAIN
			langData = null;
			langData = LanguageDataHelper.getLanguage(sessionUser, lang.getId());

			//Are they the same object... well they should be!
			assertEquals("single and all get different data", lang.getId(), langData.getId());
		}
		System.out.println("Get All succeeded");
		
 		// Check the update on that item
    	LanguageDTO updt = new LanguageDTO();
       	updt.setId(insReturn.getId());
        updt.setName("Unit Test language (updated)");
        updt.setIsoCode("en-U" + insReturn.getId());
    	RequestStatus updtOut = LanguageDataHelper.updateLanguage(sessionUser, updt);
		assertEquals("updateLanguage status != OK.  msg=" + updtOut.getExceptionMessage() + ".  ",
				 RequestStatus.RS_OK, updtOut.getStatusCode());
		LanguageDTO updtChk = LanguageDataHelper.getLanguage(sessionUser, insReturn.getId());
		assertEquals("updateLanguage check (get) status != OK.  msg=" + updtChk.getStatus().getExceptionMessage(),
				RequestStatus.RS_OK, updtChk.getStatus().getStatusCode());
		assertEquals("getLanguage returned incorrect name",
				updt.getName(), updtChk.getName());
		assertEquals("getLanguage returned incorrect ISO code",
				updt.getIsoCode(), updtChk.getIsoCode());
		System.out.println("update succeeded");
		
		//Check that the item deletes
		LanguageDTO del = new LanguageDTO();
    	del.setId(updt.getId());
    	RequestStatus delOut = LanguageDataHelper.deleteLanguage(sessionUser, del.getId());
		assertEquals("deleteLanguage status != OK.  msg=" + delOut.getExceptionMessage() + ".  ",
				 RequestStatus.RS_OK, delOut.getStatusCode());
		LanguageDTO delChk = LanguageDataHelper.getLanguage(sessionUser, del.getId());
		assertEquals("deleteLanguage check status != OK.  msg=" + delChk.getStatus().getExceptionMessage(),
				RequestStatus.DBS_NO_DATA, delChk.getStatus().getStatusCode());
		System.out.println("delete succeeded");

		// Actual test code ends here
		
    	UnitTestUtils.stop(this);
    }
}
