/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import com.pdi.reporting.constants.ReportingConstants;
//import com.pdi.reporting.report.ReportData;
//import com.pdi.scoring.request.ScoreRequest;
//import java.util.Date;


//import com.pdi.reporting.report.IndividualReport;

/**
 * ProjectParticipantInstrument is a basic data class that contains
 * related project, participant, and instrument data.
 * 
 * @author      Gavin Myers
 * @author      MB Panichi
  */
public class ProjectParticipantInstrument extends CoreDataObject
{
	private static final Logger log = LoggerFactory.getLogger(ProjectParticipantInstrument.class);
	//
	// Static data.
	//

	//
	// Static methods.
	//

	/**
	 * 
	 */
	private static final long serialVersionUID = 2849786874632181134L;
	//
	// Instance data.
	//
	private Project project;
	private Participant participant;
	private Instrument instrument;
	private Status status;

	private String instrumentTakenDate;  // TODO Will want to make this a date object when we dump v2 for good... 
	
	private ScoreGroup scoreGroup;
	private ResponseGroup responseGroup;

 
	/**
	 * No-parameter constructor
	 */
	public ProjectParticipantInstrument()
	{
		//  Does nothing presently
	}

	//	
	// Instance methods.
	//
	
	/**
	 * Generic toString method
	 */
	public String toString()
	{
			String ret = "ProjectParticipant:";
			ret += "\n  " + (project == null ? "Project null" : project.toString());
			ret += "\n  " + (participant == null ? "Participant null" : participant.toString());
			ret += "\n  " + (instrument == null ? "Instrument null" : instrument.toString());
			ret += "\n  " + (status == null ? "Status null" : status.toString());

			return ret;
	}
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public void setProject(Project value)
	{
		project = value;
	}

	public Project getProject()
	{
		return project;
	}

	/*****************************************************************************************/
	public void setParticipant(Participant value)
	{
		participant = value;
	}

	public Participant getParticipant()
	{
		return participant;
	}

	/*****************************************************************************************/
	public void setInstrument(Instrument value)
	{
		instrument = value;
	}

	public Instrument getInstrument()
	{
		return instrument;
	}

	/*****************************************************************************************/
	public void setInstrumentTakenDate(String value)
	{
		instrumentTakenDate = value;
	}

	public String getInstrumentTakenDate()
	{
		return instrumentTakenDate;
	}
	
	/*****************************************************************************************/
	public void setStatus(Status value)
	{
		status = value;
	}

	public Status getStatus()
	{
		return status;
	}

	public void setScoreGroup(ScoreGroup scoreGroup) {
		
		this.scoreGroup = scoreGroup;
	}

	public ScoreGroup getScoreGroup() {
		return scoreGroup;
	}

	public void setResponseGroup(ResponseGroup responseGroup) {
		this.responseGroup = responseGroup;
	}

	public ResponseGroup getResponseGroup() {
		return responseGroup;
	}





}
