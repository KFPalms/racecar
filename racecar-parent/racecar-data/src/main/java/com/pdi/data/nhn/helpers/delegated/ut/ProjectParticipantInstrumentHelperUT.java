package com.pdi.data.nhn.helpers.delegated.ut;

import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.Map;
//import java.util.Set;
//import java.util.Map.Entry;

//import com.pdi.data.helpers.interfaces.IProjectHelper;
import com.pdi.data.helpers.interfaces.IProjectParticipantInstrumentHelper;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.nhn.util.NhnDatabaseUtils;
//import com.pdi.data.dto.Project;
import com.pdi.data.dto.ProjectParticipantInstrument;
//import com.pdi.data.dto.Response;
//import com.pdi.data.dto.ResponseGroup;
import com.pdi.data.dto.ScoreGroup;
//import com.pdi.data.dto.SessionUser;
import com.pdi.ut.UnitTestUtils;
import junit.framework.TestCase;

public class ProjectParticipantInstrumentHelperUT extends TestCase {

	public ProjectParticipantInstrumentHelperUT(String name) 	{
		super(name);
	}
	
	public void testGetProjectParticipantInstrument() throws Exception {
		UnitTestUtils.start(this);
		NhnDatabaseUtils.setUnitTest(true);
		
//		SessionUser session = new SessionUser();
		
		IProjectParticipantInstrumentHelper iepi = HelperDelegate.getProjectParticipantInstrumentHelper();

		String engagementId = "2";         
		String participantId = "553269";
//		String participantId = "51081";
		String instrumentId = "RV";
		int mode = 1;

		ArrayList<ProjectParticipantInstrument> fromP = iepi.fromParticipantId(null, mode, participantId, null);
		try {
			StringBuffer ps = new StringBuffer();
			int numProjects = fromP.size();
			ps.append("------- >>> fromParticipantId(" + participantId + ")\n");
			ps.append("ResultSet [instrument.description, engagement.name]: \n");
			for (ProjectParticipantInstrument epi : fromP){
				ps.append("[" + epi.getInstrument().getDescription() + ", " + epi.getProject().getName() + "]\n ");
			}
			System.out.println(ps + " " + numProjects + " items total.\n\n");
		}catch (Exception e) {
			System.out.println("fromParticipantId gacked with param " + participantId + ".");
		}		
		
		ArrayList<ProjectParticipantInstrument> fromEP = iepi.fromProjectIdParticipantId(null, mode, engagementId, participantId);
		try {
			StringBuffer ps = new StringBuffer();
			int numProjects = fromEP.size();
			ps.append("------- >>> fromProjectIdParticipantId(" + engagementId + "," + participantId + ")\n");
			ps.append("ResultSet [instrument.description, engagement.name]: \n");
			for (ProjectParticipantInstrument epi : fromEP){
				ps.append("[" + epi.getInstrument().getDescription() + ", " + epi.getProject().getName() + "]\n ");
			}
			System.out.println(ps + " " + numProjects + " items total.\n\n");
		}catch (Exception e) {
			System.out.println("fromProjectIdParticipantId gacked with param " + engagementId + "," + participantId + ".");
		}		
		
		ArrayList<ProjectParticipantInstrument> fromPI = iepi.fromParticipantIdInstrumentId(null, participantId, instrumentId);
		try {
			StringBuffer ps = new StringBuffer();
			int numProjects = fromPI.size();
			ps.append("------- >>> fromParticipantIdInstrumentId(" + participantId + "," + instrumentId + ")\n");
			ps.append("ResultSet [instrument.description, engagement.name]: \n");
			for (ProjectParticipantInstrument epi : fromPI){
				ps.append("[" + epi.getInstrument().getDescription() + ", " + epi.getProject().getName() + "]\n ");
			}
			System.out.println(ps + "\n" + numProjects + " items total.\n\n");
		}catch (Exception e) {
			System.out.println("fromParticipantIdInstrumentId gacked with param " + participantId + "," + instrumentId + ".");
		}		
		
		ProjectParticipantInstrument fromEPI = iepi.fromProjectIdParticipantIdInstrumentId(null, mode, engagementId, participantId, instrumentId, null);
		try {
			StringBuffer ps = new StringBuffer();
			ps.append("------- >>> fromProjectIdParticipantIdInstrumentId(" + engagementId + "," + participantId + "," + instrumentId + ")\n");
			ps.append("ResultSet[instrument.description, engagement.name]: \n");
			ps.append("[" + fromEPI.getInstrument().getDescription() + ", " + fromEPI.getProject().getName() + "]\n ");
			ps.append("Response data: " + fromEPI.getResponseGroup().getResponses().toString() + " " + "\n");
			System.out.println(ps);
			//SCORE the object
			ScoreGroup sg = HelperDelegate.getScoringHelper().score(fromEPI.getInstrument(), fromEPI.getResponseGroup());
			fromEPI.setScoreGroup(sg);
			System.out.println(fromEPI.getScoreGroup().getScores().toString());
		}catch (Exception e) {
			System.out.println("fromProjectIdParticipantIdInstrumentId gacked with param " + engagementId + "," + participantId + "," + instrumentId + ".");
		}
		
		
		
	}
	
	
}
