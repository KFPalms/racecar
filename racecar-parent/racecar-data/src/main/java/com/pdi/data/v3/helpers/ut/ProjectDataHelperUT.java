package com.pdi.data.v3.helpers.ut;

//import java.util.ArrayList;

//import org.w3c.dom.Document;

//import com.pdi.data.v3.dto.DocumentDTO;
import com.pdi.data.v3.dto.ClientDTO;
import com.pdi.data.v3.dto.ParticipantDTO;
import com.pdi.data.v3.dto.ProjectDTO;
import com.pdi.data.v3.dto.SessionUserDTO;
import com.pdi.data.v3.helpers.ProjectDataHelper;
//import com.pdi.data.v3.util.V3WebserviceUtils;
import com.pdi.properties.PropertyLoader;
import com.pdi.ut.UnitTestUtils;
//import com.pdi.xml.XMLUtils;

import junit.framework.TestCase;

public class ProjectDataHelperUT extends TestCase {
    public ProjectDataHelperUT(
            String name) {
            super(name);
        } 
    
    
    public void testProperties() throws Exception {
    	UnitTestUtils.start(this);
    	//Does the local property work?
    	String applicationId = PropertyLoader.getProperty("com.pdi.data.v3.application", "application.code");
    	assertEquals(applicationId != null, true);
    	assertEquals(applicationId.equals("PDI-DATA-V3"), true);
    	UnitTestUtils.stop(this);
    }
    
    
    public void testRegisterCandidates() throws Exception {
    	UnitTestUtils.start(this);
    	//https://platform.iscopia.com/webservices1/rest/v1/project/PEIX-EYWU/activity/a9011f48-29af-4b3f-9586-4b8a447d931e/participations
    	SessionUserDTO sessionUser = new SessionUserDTO();
    	sessionUser.setUsername(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.username")); 
    	sessionUser.setPassword(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.password"));
    	
    	// Thgis one gets only one project with only one participant... try this for a real client
//		for(ProjectDTO project : ProjectDataHelper.getProjects(sessionUser).getProjectList())
    	ClientDTO clnt = new ClientDTO();
    	clnt.setId("OKLU-MJGG");
 		for(ProjectDTO project : ProjectDataHelper.getProjects(sessionUser,clnt).getProjectList())
		{
 			project = ProjectDataHelper.getProject(sessionUser, project);
 			ProjectDataHelper.registerProjectParticipants(sessionUser, project);
		}
    	
    	UnitTestUtils.stop(this);
    }
    
    /*
     * Tests the default List Candidates
     * 
     */
    public void testGetProjects() throws Exception {
    	UnitTestUtils.start(this);
    	
    	SessionUserDTO sessionUser = new SessionUserDTO();
    	sessionUser.setUsername(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.username")); 
    	sessionUser.setPassword(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.password"));

    	// Thgis one gets only one project with only one participant... try this for a real client
//		for(ProjectDTO project : ProjectDataHelper.getProjects(sessionUser).getProjectList())
    	ClientDTO clnt = new ClientDTO();
    	clnt.setId("OKLU-MJGG");
 		for(ProjectDTO project : ProjectDataHelper.getProjects(sessionUser,clnt).getProjectList())
		{
			System.out.println("----> PROJECT " + project.getName() + ".  PartCount=" + project.getParticipantCount());
			try {
				project = ProjectDataHelper.getProject(sessionUser, project);
				ProjectDataHelper.registerProjectParticipants(sessionUser, project);
			} catch (Exception e) {
				System.out.println("ERROR Registering candidates");
				e.printStackTrace();
			}
			if (project.isProxy())
			{
				System.out.println("--------> Proxy... No participant data");
			}
			else
			{
				if (project.getParticipantList() == null)
				{
					System.out.println("--------> NO PARTICIPANTS");
				}
				else
				{
					for(ParticipantDTO participant : project.getParticipantList())
					{
						System.out.println("--------> PARTICIPANT " + participant.getCandidate().getIndividual().getGivenName() + " " + participant.getCandidate().getIndividual().getFamilyName());
					}
				}
			}
		}
    	
    	UnitTestUtils.stop(this);
    }
    
    public void testGetProductFromProjects() throws Exception {
    	UnitTestUtils.start(this);
    	
    	SessionUserDTO sessionUser = new SessionUserDTO();
    	sessionUser.setUsername(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.username")); 
    	sessionUser.setPassword(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.password"));

		//DocumentDTO dto = V3WebserviceUtils.request(sessionUser,"https://platform.iscopia.com/webservices1/rest/v1/project/PYKJ-BVWS","");

		//Document response = dto.getDocument();
		//System.out.println("testGetProductFromProjects:  " + dto.toString());
	
		//String id = XMLUtils.getElementValue(response, "//resultProject/activityWorkflow/activity/productIdentifier/.");
		//System.out.println("Product GUID = " + id);
		
    	UnitTestUtils.stop(this);
    }
    
    
    public static void main(String[] args) throws Exception {

        junit.textui.TestRunner.run(
        		ProjectDataHelperUT.class);
    }
}