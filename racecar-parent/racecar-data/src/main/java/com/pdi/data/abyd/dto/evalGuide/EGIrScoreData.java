/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 *
 * $Header: /Platform/Java/sourceCode/com/pdicorp/app/tcm/export/vo/ExportDataVO.java 6     8/25/05 3:23p Kbeukelm $
 */
package com.pdi.data.abyd.dto.evalGuide;


/**
* EGIrElement data is a container for the data for a single importance rating.
* It is used in the full data and IR data DTOs.
* 
* @author		Ken Beukelman
*/
public class EGIrScoreData
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private int _irSeq;		// The IR id associated with this score
	private int _irScore;	// The score value for the IR

	//
	// Constructors.
	//
	public EGIrScoreData()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public int getIrSeq()
	{
		return _irSeq;
	}
		
	public void setIrSeq(int value)
	{
		_irSeq = value;
	}

	/*****************************************************************************************/
	public int getIrScore()
	{
		return _irScore;
	}
		
	public void setIrScore(int value)
	{
		_irScore = value;
	}
}
