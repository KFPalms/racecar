package com.pdi.data.dto;

/**
 * Attachment is a wrapper around the attachments data for
 * a specific Participant.
 * Attachments are uploaded through the Test Data application.
 * Attachments are accessible in Test Data, and, read-only, 
 * through the DRI.
 * Attachments are .pdf, .doc, or .xls files, and that
 * is controlled via the UI.
 *
 * @author      MB Panichi
 */
public class Attachment extends CoreDataObject
{

	//
	// Static data.
	//
	
	// Attachment documant association constants
	// KEEP IN SYNC WITH ANTEATER (pdi-data/src/com/pdi/data/dto/Attachment.as)
	public static final int ASSOC_PART = 1;
	public static final int ASSOC_PROJ = 2;

	//
	// Static methods.
	//

	/**
	 * 
	 */
	private static final long serialVersionUID = -608359009855063585L;
	//
	// Instance data.
	//
	private long attachmentId = 0;
    private String attachmentName = "";
    private String attachmentDescription = "";
    private int attachmentAssociation = 0;	// Association with proj or participant
    private boolean displayInIg = false;
    private long attachmentTypeId = 0;
    private String attachmentTypeName = "";
    private String attachmentPath = "";
    private boolean attachmentActive = true;	// true -- attachment available
    											// false == attachment logically deleted
    // Probably should be in attachmentHolder, but is here
    // to facilitate the use of already implemented UI code
	private String attachmentUrl;

	//
	// Constructors.
	//
	public Attachment()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//
	
	public String toString()
	{
		String ret = "    Attachment:\n";
		ret += "      ID=" + attachmentId + ", name=" + attachmentName + ", type=" + attachmentTypeName + ", path=" + attachmentPath + "\n";
		ret += "    URL=" + attachmentUrl;
			
		return ret;
	}

	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
	
	/*****************************************************************************************/
	public long getAttachmentId()
	{
		return attachmentId;
	}

	public void setAttachmentId(long value)
	{
		attachmentId = value;
	}

	/*****************************************************************************************/
	public String getAttachmentName()
	{
		return attachmentName;
	}

	public void setAttachmentName(String value)
	{
		attachmentName = value;
	}

	/*****************************************************************************************/
	public String getAttachmentDescription()
	{
		return attachmentDescription;
	}

	public void setAttachmentDescription(String value)
	{
		attachmentDescription = value;
	}
	
	/*****************************************************************************************/
	public int getAttachmentAssociation()
	{
		return attachmentAssociation;
	}

	public void setAttachmentAssociation(int value)
	{
		attachmentAssociation = value;
	}
	
	/*****************************************************************************************/
	public boolean getDisplayInIg()
	{
		return displayInIg;
	}

	public void setDisplayInIg(boolean value)
	{
		displayInIg = value;
	}
	
	/*****************************************************************************************/
	public long getAttachmentTypeId()
	{
		return attachmentTypeId;
	}

	public void setAttachmentTypeId(long value)
	{
		attachmentTypeId = value;
	}

	/*****************************************************************************************/
	public String getAttachmentTypeName()
	{
		return attachmentTypeName;
	}

	public void setAttachmentTypeName(String value)
	{
		attachmentTypeName = value;
	}

	/*****************************************************************************************/
	public String getAttachmentPath()
	{
		return attachmentPath;
	}

	public void setAttachmentPath(String value)
	{
		attachmentPath = value;
	}

	/*****************************************************************************************/
	public boolean getAttachmentActive()
	{
		return attachmentActive;
	}

	public void setAttachmentActive(boolean value)
	{
		attachmentActive = value;
	}	

	/*****************************************************************************************/
	public void setAttachmentUrl(String value)
	{
		attachmentUrl = value;
	}

	public String getAttachmentUrl()
	{
		return attachmentUrl;
	}
}


