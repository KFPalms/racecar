/**
 * Copyright (c) 2008 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v2.dto;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.w3c.dom.Node;

import com.pdi.xml.XMLUtils;

/**
 * ProjectDTO is a wrapper around a number of data items that
 * convey information about a project/job
 * 
 * @author      MB Panichi
 * @author		Ken Beukelman
  */
public class ProjectDTO
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//	
	
	// mb added _customFields:
	//    used for project fields like transition level, transition level labels, business unit
	//Keith added _selected:
	//    Used in CandidateDTO
	private String _id;
	private String _name;
	private ClientDTO _client;
	private String _supplierId;
	private String _supplierName;
	private String _uri;
	private String _version;
	private String _productId;
	private String _resultsUri;
	private String _answersUri;
	private String _activityId;
	private String _transitionLevel;
	private String _TPOTTransLevelCurrent;
	private String _TPOTTransLevelTarget;
	private String _transitionLevelName;
	private String _type;
	private String _cognitivesIncluded;
	
	private HashMap<String, String> _customFields = new HashMap<String,String>();
	private ArrayList<ParticipantDTO> _participantList = null;
	private int _participantCount = 0;
	private int _participantCompleteCount = 0;	// Not available in release 1.0
	
	private boolean _selected = false;
	
	//mb added for launch.jsp
	private String _includeCareerSurvey;
	private String _chooseCogs;
	

	
	//
	// Constructors.
	//

	/**
	 * "Standard" (no-parameter) constructor
	 */
	public ProjectDTO()
	{
		super();
		// Does nothing presently
	}
	
	/**
	 * Builds the object based off an xml node
	 * @param node
	 * @throws Exception
	 */
	public ProjectDTO(Node node)
	{	
		this.setId(XMLUtils.getAttributeValue(node, "UniqueIdStamp"));
		//this.setName(XMLUtils.xmlFormatString(XMLUtils.getAttributeValue(node, "Title")));	
		this.setName(XMLUtils.getAttributeValue(node, "Title"));	
		ClientDTO client = new ClientDTO();
		//client.setName(XMLUtils.xmlFormatString(XMLUtils.getAttributeValue(node, "Name")));
		client.setName(XMLUtils.getAttributeValue(node, "Name"));
		client.setId(XMLUtils.getAttributeValue(node, "ClientID"));
		this.setClient(client);
		this.setTransitionLevel(XMLUtils.getAttributeValue(node, "xml_TPOTTransLevel"));
		this.setTPOTTransLevelCurrent(XMLUtils.getAttributeValue(node, "xml_TPOTTransLevelCurrent"));
		this.setTPOTTransLevelTarget(XMLUtils.getAttributeValue(node, "xml_TPOTTransLevelTarget"));
		if(XMLUtils.getAttributeValue(node, "ParticipantCount") != null) {
			this.setParticipantCount(Integer.parseInt(XMLUtils.getAttributeValue(node, "ParticipantCount")));
		}
		if(this.getTransitionLevel() == null) {
			
		} else if(this.getTransitionLevel().equalsIgnoreCase("1")) {
			this.setTransitionLevelName("IC-FLL");
		} else if(this.getTransitionLevel().equalsIgnoreCase("2")) {
			this.setTransitionLevelName("IC-MLL");
		} else if(this.getTransitionLevel().equalsIgnoreCase("3")) {
			this.setTransitionLevelName("FLL-MLL");
		} else if(this.getTransitionLevel().equalsIgnoreCase("4")) {
			this.setTransitionLevelName("FLL-SR EXEC");
		} else if(this.getTransitionLevel().equalsIgnoreCase("5")) {
			this.setTransitionLevelName("MLL-SR EXEC");
		} else {
			this.setTransitionLevel("1");
			this.setTransitionLevelName("IC-FLL");
		}
		this.setType(XMLUtils.getAttributeValue(node, "JobType"));
		this.setCognitivesIncluded(XMLUtils.getAttributeValue(node, "xml_TPOTCogsInclded"));
		
		this.setChooseCogs(XMLUtils.getAttributeValue(node, "xml_chooseCogs"));
		this.setIncludeCareerSurvey(XMLUtils.getAttributeValue(node, "xml_CareerSurvey"));
	}

	//
	// Instance methods.
	//
//
//	/*
//	 * 
//	 */
//	private ArrayList<ParticipantDTO> getMyParticipantProxies()
//	{
//		DocumentDTO dto = V3WebserviceUtils.request(sessionUser,"project/"+project.getId()+"/participant/list","");
//		if (! dto.getStatus().isOk())
//		{
//			return new ParticipantListDTO(dto.getStatus());
//		}
//
//		Document response = dto.getDocument();
//		NodeList nodes = XMLUtils.getElements(response, "//participantListLine");
//
//		ArrayList<ParticipantDTO> results = new ArrayList<ParticipantDTO>();
//		for(int i = 0; i < nodes.getLength(); i++) {
//			Node node = nodes.item(i);
//			ParticipantDTO participant = new ParticipantDTO(node);
//			results.add(participant);
//		}
//		
//	}

	/**
	 * Generic toString method
	 */
	public String toString()
	{
		String str = "\n --- ProjectDTO --- ";
		str += "\nid:                  " + this.getId();
		str += "\nName:                " + this.getName();
		str += this.getClient().toString();
		str += "\nSupplier ID:         " + this.getSupplierId();
		str += "\nSupplier Name:       " + this.getSupplierName();
		str += "\nURI:                 " + this.getUri();
		str += "\nVersion:            " + this.getVersion();
		str += "\nProduct ID:         " + this.getProductId();
		str += "\nCustomFields ("+ this.getCustomFields().size() + " entries):";
		for (Iterator<Map.Entry<String, String>> itr=this.getCustomFields().entrySet().iterator(); itr.hasNext(); )
		{
			Map.Entry<String, String> ent = itr.next();
			str +="\n    " + ent.getKey() + " = " + ent.getValue();
		}
		str += "\nParticipant cnt:    " + this.getParticipantCount();
////		str += "\nPart Complete cnt:  " + this.getParticipantCompleteCount();
		if (this.getParticipantList() == null)
		{
			str += "Participant List is not initialized";
		}
		else
		{
			str += "\nParticipant Array (" + this.getParticipantList().size() + " entries):";
			for (int i = 0; i < this.getParticipantList().size(); i++)
			{
				this.getParticipantList().get(i).toString();
			}
		}
		str += "\nSelected:      " + this.getSelected();
		str += "\nJob Type:      " + this.getType();

		return str;
	}



	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
	
	
	/*****************************************************************************************/
	public void setId(String value)
	{
		_id = value;
	}
	
	public String getId()
	{
		return _id;
	}
	
	/*****************************************************************************************/
	public void setName(String value)
	{
		_name = value;
	}
	
	public String getName()
	{
		return _name;
	}
	
	/*****************************************************************************************/
	public void setClient(ClientDTO value)
	{
		_client = value;
	}
	
	public ClientDTO getClient()
	{
		return _client;
	}
	
	/*****************************************************************************************/
	public void setSupplierId(String value)
	{
		_supplierId = value;
	}
	
	public String getSupplierId()
	{
		return _supplierId;
	}
	
	/*****************************************************************************************/
	public void setSupplierName(String value)
	{
		_supplierName = value;
	}
	
	public String getSupplierName()
	{
		return _supplierName;
	}
	
	/*****************************************************************************************/
	public void setUri(String value)
	{
		_uri = value;
	}
	
	public String getUri()
	{
		return _uri;
	}
	

	
	/*****************************************************************************************/
	public void setVersion(String value)
	{
		_version = value;
	}
	
	public String getVersion()
	{
		return _version;
	}
	
	/*****************************************************************************************/
	public void setProductId(String value)
	{
		_productId = value;
	}
	
	public String getProductId()
	{
		return _productId;
	}
	
	/*****************************************************************************************/
	public void setResultsUri(String value)
	{
		_resultsUri = value;
	}
	
	public String getResultsUri()
	{
		return _resultsUri;
	}
	
	/*****************************************************************************************/
	public void setAnswersUri(String value)
	{
		_answersUri = value;
	}
	
	public String getAnwersUri()
	{
		return _answersUri;
	}
	
	/*****************************************************************************************/
	public void setCustomFields(HashMap<String, String> value)
	{
		_customFields = value;
	}
	
	public HashMap<String, String> getCustomFields()
	{
		return _customFields;
	}
	
	/*****************************************************************************************/
	public void setParticipantList(ArrayList<ParticipantDTO> value)
	{
		_participantList = value;
	}
	
	public ArrayList<ParticipantDTO> getParticipantList()
	{
		return _participantList;
	}

	/*****************************************************************************************/
	public void setParticipantCount(int value)
	{
		_participantCount = value;
	}

	public int getParticipantCount()
	{
		return _participantCount;
	}

	/*****************************************************************************************/
	public void setParticipantCompleteCount(int value)
	{
		_participantCompleteCount = value;
	}

	public int getParticipantCompleteCount()
	{
		return _participantCompleteCount;
	}

	/*****************************************************************************************/
	public void setSelected(boolean value)
	{
		_selected = value;
	}

	public boolean getSelected()
	{
		return _selected;
	}

	public void setActivityId(String _activityId) {
		this._activityId = _activityId;
	}

	public String getActivityId() {
		return _activityId;
	}

	/*****************************************************************************************/
	public void setTransitionLevel(String value)
	{
		_transitionLevel = value;
	}
	
	public String getTransitionLevel()
	{
		return _transitionLevel;
	}
	
	/*****************************************************************************************/
	public void setTPOTTransLevelCurrent(String value)
	{
		_TPOTTransLevelCurrent = value;
	}
	
	public String getTPOTTransLevelCurrent()
	{
		return _TPOTTransLevelCurrent;
	}
	
	/*****************************************************************************************/
	public void setTPOTTransLevelTarget(String value)
	{
		_TPOTTransLevelTarget = value;
	}
	
	public String getTPOTTransLevelTarget()
	{
		return _TPOTTransLevelTarget;
	}

	/*****************************************************************************************/
	public void setTransitionLevelName(String _transitionLevelName) {
		this._transitionLevelName = _transitionLevelName;
	}

	public String getTransitionLevelName() {
		return _transitionLevelName;
	}

	/*****************************************************************************************/
	public void setType(String value)
	{
		_type = value;
	}

	public String getType() {
		return _type;
	}
	
	/*****************************************************************************************/
	public void setCognitivesIncluded(String value)
	{
		_cognitivesIncluded = value;
	}

	public String getCognitivesIncluded() {
		return _cognitivesIncluded;
	}	

	/*****************************************************************************************/
	public void setIncludeCareerSurvey(String value)
	{
		_includeCareerSurvey = value;
	}

	public String getIncludeCareerSurvey() {
		return _includeCareerSurvey;
	}	
	
	/*****************************************************************************************/
	public void setChooseCogs(String value)
	{
		_chooseCogs = value;
	}

	public String getChooseCogs() {
		return _chooseCogs;
	}		
}
