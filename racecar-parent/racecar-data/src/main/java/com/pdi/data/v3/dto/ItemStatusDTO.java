/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.dto;

import org.w3c.dom.Node;

import com.pdi.data.util.BaseDTO;
import com.pdi.data.util.RequestStatus;
import com.pdi.xml.XMLUtils;

/**
 * DTO for status information
 * 
 * @author		Gavin Myers
 * @author		Ken Beukelman
 */
public class ItemStatusDTO extends BaseDTO
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _id;
	private String _code;
	private String _name;
	private String _outcome;

	//
	// Constructors.
	//

	/**
	 * "Standard" (no-parameter) constructor
	 */
	public ItemStatusDTO()
	{
		// Does nothing presently
	}
	
	/**
	 * Builds the object based off an xml node
	 * @param node
	 * @throws Exception
	 */
	public ItemStatusDTO(Node node)
	{
		/*
		 * The XML looks something like this:
		 * <status>Active</status>
		 */
		this.setId(XMLUtils.getElementValue(node, "status/."));
		this.setCode(XMLUtils.getElementValue(node, "status/."));
		this.setName(XMLUtils.getElementValue(node, "status/."));
		this.setOutcome(XMLUtils.getElementValue(node, "outcome/"));

		
		// check for data
		if (this.getId() == null &&
			this.getCode() == null &&
			this.getName() == null &&
			this.getOutcome() == null)
		{
			this.getStatus().setStatusCode(RequestStatus.WB_NO_DATA);
		}
}

	//
	// Instance methods.
	//
	
	/**
	 * Generic toString method
	 */
	public String toString()
	{
		String str = "\n --- ItemStatusDTO --- \n";
		str += "\nID:       " + this.getId();
		str += "\nCode:     " + this.getCode();
		str += "\nName:     " + this.getName();
		str += "\nOutcome:  " + this.getOutcome();

		return str;
	}
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
	
	/*****************************************************************************************/
	public void setId(String value)
	{
		_id = value;
	}
	
	public String getId()
	{
		return _id;
	}
	
	/*****************************************************************************************/
	public void setCode(String value)
	{
		_code = value;
	}
	
	public String getCode()
	{
		return _code;
	}
	
	/*****************************************************************************************/
	public void setName(String value)
	{
		_name = value;
	}
	
	public String getName()
	{
		return _name;
	}
	
	/*****************************************************************************************/
	public void setOutcome(String value)
	{
		_outcome = value;
	}

	public String getOutcome()
	{
		return _outcome;
	}
}
