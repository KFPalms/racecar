/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.dto;

import java.util.Date;

import org.w3c.dom.Node;

import com.pdi.data.util.BaseDTO;
import com.pdi.data.util.RequestStatus;
import com.pdi.xml.XMLUtils;

/**
 * DTO for candidate information
 *
 * @author		Gavin Myers
 * @author 		Ken Beukelman
 */
public class CandidateDTO extends BaseDTO
{
	//
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _id;
	private IndividualDTO _individual;
	private String _uri;
	private OrganizationDTO _organization;
	private String _language;
	private ItemStatusDTO _itemStatus;
	private String _version;
	private String _createdBy;
	private Date _createdDate;

	//
	// Constructors.
	//

	/**
	 * "Standard" (no-parameter) constructor
	 */
	public CandidateDTO()
	{
		super();
		// Does nothing presently
	}

	/**
	 * Set up a new DTO with status but no data
	 */
	public CandidateDTO(RequestStatus stat)
	{
		super();
		this.setStatus(stat);
	}


	/**
	 * Builds the object based off an xml node
	 * @param node
	 * @throws Exception
	 */
	public CandidateDTO(Node node)
	{
		/*
		 * 
		 * We could be sending a list node or a result node.  Check for both.
		 * 
		 * From the candidate list, the XML looks like this:
		 * <ns2:candidateListLine>
		 *   <identifier>CNFE-UWVF</identifier>
		 *   <familyName>test1</familyName>
		 *   <givenName>test1</givenName>
		 *   <email>test01@gavinm.com</email>
		 *   <uri>https://platform.iscopia.com/webservices1/rest/v1/candidate/CNFE-UWVF</uri>
		 * </ns2:candidateListLine>
		 * 
		 * Note the the URI is unique here and tells us if we are filling a proxy or the real McCoy
		 * 
		 * From the candidate uri, the XML looks like this:
		 * <ns3:resultCandidate>
		 *   <familyName>test1</familyName>
		 *   <givenName>test1</givenName>
		 *   <organisationIdentifier>OZUN-PUAX</organisationIdentifier>
		 *   <email>test01@gavinm.com</email>
		 *   <language>en</language>
		 *   <identifier>CNFE-UWVF</identifier>
		 *   <organisation>T_PDI_dev</organisation>
		 *   <createdByUser>gavin.myers.dev</createdByUser>
		 *   <createdDate>2009-08-27T13:37:38.000-0400</createdDate>
		 *   <status>Active</status>
		 *   <version>1</version>
		 *   <ns2:customFields/>
		 * </ns3:resultCandidate>
		 */

		super();
		
		// If we are invoking this from a participant node, there is much less
		// data involved so we need to build this candidate node differently.
		// We won't get all available candidate data because it is not
		// available when building a CandidateDTO from participant XML.
		if (XMLUtils.getElementValue(node, "candidateIdentifier/.") != null)
		{
			// Treat participant node special
			this.setProxy(PROXY);
			this.setId(XMLUtils.getElementValue(node, "candidateIdentifier/."));
			this.setIndividual(new IndividualDTO(node));

			if (this.getId() == null  &&
                this.getIndividual().getStatus().getStatusCode() == RequestStatus.WB_NO_DATA)
			{
				this.getStatus().setStatusCode(RequestStatus.WB_NO_DATA);
			}

			return;
		}
		
		// "Normal" candidate setup
		// This works, because the candidate list has proxy data only plus the URI.  If the
		// URI attribute is present, the Node must be from the list and contains proxy data only.
		String uri = XMLUtils.getElementValue(node, "uri/.");
		this.setProxy((uri == null ? FILLED : PROXY));

		this.setId(XMLUtils.getElementValue(node, "identifier/."));
		this.setIndividual(new IndividualDTO(node));
		this.setUri(uri);
		this.setOrganization(new OrganizationDTO(node));
		this.setLanguage(XMLUtils.getElementValue(node, "language/."));
		this.setItemStatus(new ItemStatusDTO(node));
		this.setVersion(XMLUtils.getElementValue(node, "version/."));
//		this.setCreatedBy(XMLUtils.getElementValue(node, "createdByUser/."));
////		this.setCreatedDate(XMLUtils.getElementValue(node, "createdDate/."));
		
		boolean cmnDat = ! (this.getId() == null  &&
		                    this.getIndividual().getStatus().getStatusCode() == RequestStatus.WB_NO_DATA  &&
		                    this.getUri() == null);		// I know this is proxy data, but it simplifies logic
		
		if (this.isProxy())
		{
			if (! cmnDat)
			{
				this.getStatus().setStatusCode(RequestStatus.WB_NO_DATA);
			}
		}
		else
		{
			if (! cmnDat  &&
				this.getOrganization().getStatus().getStatusCode() == RequestStatus.WB_NO_DATA &&
				this.getLanguage() == null &&
				this.getItemStatus().getStatus().getStatusCode() == RequestStatus.WB_NO_DATA &&
				this.getVersion() == null &&
				this.getCreatedBy() == null)
			{
				this.getStatus().setStatusCode(RequestStatus.WB_NO_DATA);
			}
		}
		
		return;
	}

	//
	// Instance methods.
	//
	
	/**
	 * Generic toString method
	 */
	public String toString() {
		String str = "\n --- CandidateDTO ---";
		str += "\nProxy:        " + this.getProxy();
		str += this.getStatus().toString();
		str += "\nID:            " + this.getId();
		str += this.getIndividual().toString();
		str += "\nUri:           " + this.getUri();
		if (this.getOrganization() == null)
		{
			str += "\nOrganization not available";
		}
		else
		{
			str += this.getOrganization().toString();
		}
		str += "\nLanguage:      " + this.getLanguage();
		if (this.getOrganization() == null)
		{
			str += "\nItemStatus not available";
		}
		else
		{
			str += this.getItemStatus().toString();
		}
		str += "\nVersion:       " + this.getVersion();
		str += "\nCreated By:    " + this.getCreatedBy();
		//str += "\nCreated Date:  " + this.getCreatedDate().toString();

		return str;
	}


	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
	
	/*****************************************************************************************/
	public void setId(String value)
	{
		_id = value;
	}
	
	public String getId()
	{
		return _id;
	}
	
	/*****************************************************************************************/
	public void setIndividual(IndividualDTO value)
	{
		_individual = value;
	}
	
	public IndividualDTO getIndividual()
	{
		return _individual;
	}
	
	/*****************************************************************************************/
	public void setUri(String value)
	{
		_uri = value;
	}
	
	public String getUri()
	{
		return _uri;
	}
	
	/*****************************************************************************************/
	public void setOrganization(OrganizationDTO value)
	{
		_organization = value;
	}
	
	public OrganizationDTO getOrganization()
	{
		return _organization;
	}
	
	/*****************************************************************************************/
	public void setLanguage(String value)
	{
		_language = value;
	}
	
	public String getLanguage()
	{
		return _language;
	}
	
	/*****************************************************************************************/
	public void setItemStatus(ItemStatusDTO value)
	{
		_itemStatus = value;
	}
	
	public ItemStatusDTO getItemStatus()
	{
		return _itemStatus;
	}
	
	/*****************************************************************************************/
	public void setVersion(String value)
	{
		_version = value;
	}
	
	public String getVersion()
	{
		return _version;
	}
	
	/*****************************************************************************************/
	public void setCreatedBy(String value)
	{
		_createdBy = value;
	}
	
	public String getCreatedBy()
	{
		return _createdBy;
	}
	
	/*****************************************************************************************/
	public void setCreatedDate(Date value)
	{
		_createdDate = value;
	}
	
	public Date getCreatedDate()
	{
		return _createdDate;
	}

	
	
	
	
	
////////////////////	
//  Original code //	
////////////////////	
//
//	//
//	// Static data.
//	//
//
//	//
//	// Static methods.
//	//
//
//	//
//	// Instance data.
//	//
//	private String _id;
//	private String _recruitmentSource;
//	private String _language;
//	private String _notes;
//	private String _createdBy;
//	private Date _created;
//	private String _version;
//	private AddressDTO _address;
//	private ItemStatusDTO _itemStatus;
//	private OrganizationDTO _organization;
//	private IndividualDTO _individual;
//
//	//
//	// Constructors.
//	//
//
//	/**
//	 * "Standard" (no-parameter) constructor
//	 */
//	public CandidateDTO()
//	{
//		// Does nothing presently
//	}
//
//
//	/**
//	 * Builds the object based off an xml node
//	 * @param node
//	 * @throws Exception
//	 */
//	public CandidateDTO(Node node)
//	{
//		/*
//		 * The XML looks something like this:
//		 * <ns2:resultCandidate xmlns:ns2="urn:iscopia.com:candidate-1.0">
//		 *   <familyName>Smith</familyName>
//		 *   <givenName>John</givenName>
//		 *   <email>johnsmith@iscopia.com</email>
//		 *   <mobile>418-444-4446</mobile>
//		 *   <organisationIdentifier>OIDE-OIDE</organisationIdentifier>
//		 *   <telephone>418-333-3335<\telephone>
//		 *   <recruitmentSource>fft</recruitmentSource>
//		 *   <language>en</language>
//		 *   <address>10 11th Avenue</address>
//		 *   <city>New-York</city>
//		 *   <stateOrProvince>New-York</stateOrProvince>
//		 *   <country>Usa</country>
//		 *   <zip>123456</zip>
//		 *   <notes/>
//		 *   <identifier>CIDE-CIDE</identifier>
//		 *   <organisation>My Company name</organisation>
//		 *   <createdByUser>my user</createdByUser>
//		 *   <createdDate>2009-04-16T10:14:54.000-0400</createdDate>
//		 *   <version>0</version>
//		 * <\ns2:resultCandidate>
//		 */
//		this.setRecruitmentSource(XMLUtils.getElementValue(node, "recruitmentSource/."));
//		this.setLanguage(XMLUtils.getElementValue(node, "language/."));
//		this.setNotes(XMLUtils.getElementValue(node, "notes/."));
//		this.setCreatedBy(XMLUtils.getElementValue(node, "createdByUser/."));
//		this.setVersion(XMLUtils.getElementValue(node, "version/."));
//
//		//If this is nested, they'll use candidate identifier
//		if(XMLUtils.getElementValue(node, "candidateIdentifier/.") != null)
//		{
//			this.setId(XMLUtils.getElementValue(node, "candidateIdentifier/."));
//		} else {
//			this.setId(XMLUtils.getElementValue(node, "identifier/."));
//		}
//		
//		this.setOrganization(new OrganizationDTO(node));
//		this.setIndividual(new IndividualDTO(node));
//		this.setAddress(new AddressDTO(node));
//		
//		if (this.getRecruitmentSource() == null &&
//			this.getLanguage() == null &&
//			this.getNotes() == null &&
//			this.getCreatedBy() == null &&
//			this.getVersion() == null &&
//			this.getId() == null &&
//			this.getOrganization().getStatus().getStatusCode() == RequestStatus.WB_NO_DATA &&
//			this.getIndividual().getStatus().getStatusCode() == RequestStatus.WB_NO_DATA &&
//			this.getAddress().getStatus().getStatusCode() == RequestStatus.WB_NO_DATA)
//		{
//			this.getStatus().setStatusCode(RequestStatus.WB_NO_DATA);
//		}
//	}
//
//	//
//	// Instance methods.
//	//
//	
//	/**
//	 * Generic toString method
//	 */
//	public String toString() {
//		String str = "\n --- CandidateDTO --- \n";
//		str += "\nID:                  " + this.getId();
//		str += "\nRecruitment Source:  " + this.getRecruitmentSource();
//		str += "\nLanguage:            " + this.getLanguage();
//		str += "\nNotes:               " + this.getNotes();
//		str += "\nCreated By:          " + this.getCreatedBy();
//		str += "\nCreated:             " + this.getCreated().toString();
//		str += "\nVersion:             " + this.getVersion();
//		str += this.getOrganization().toString();
//		str += this.getIndividual().toString();
//		str += this.getAddress().toString();
//		return str;
//	}
//	
//	//*******************************************************************************************//
//	//																							 //
//	//                                       Get/Set Functions                                   //
//	//																							 //
//	//*******************************************************************************************//
//	
//	/*****************************************************************************************/
//	public void setId(String value)
//	{
//		_id = value;
//	}
//	
//	public String getId()
//	{
//		return _id;
//	}
//	
//	/*****************************************************************************************/
//	public void setRecruitmentSource(String value)
//	{
//		_recruitmentSource = value;
//	}
//	
//	public String getRecruitmentSource()
//	{
//		return _recruitmentSource;
//	}
//	
//	/*****************************************************************************************/
//	public void setLanguage(String value)
//	{
//		_language = value;
//	}
//	
//	public String getLanguage()
//	{
//		return _language;
//	}
//	
//	/*****************************************************************************************/
//	public void setNotes(String value)
//	{
//		_notes = value;
//	}
//	
//	public String getNotes()
//	{
//		return _notes;
//	}
//	
//	/*****************************************************************************************/
//	public void setCreatedBy(String value)
//	{
//		_createdBy = value;
//	}
//	
//	public String getCreatedBy()
//	{
//		return _createdBy;
//	}
//	
//	/*****************************************************************************************/
//	public void setCreated(Date value)
//	{
//		_created = value;
//	}
//	
//	public Date getCreated()
//	{
//		return _created;
//	}
//	
//	/*****************************************************************************************/
//	public void setVersion(String value)
//	{
//		_version = value;
//	}
//	
//	public String getVersion()
//	{
//		return _version;
//	}
//	
//	/*****************************************************************************************/
//	public void setItemStatus(ItemStatusDTO value)
//	{
//		_itemStatus = value;
//	}
//	
//	public ItemStatusDTO getItemStatus()
//	{
//		return _itemStatus;
//	}
//	
//	/*****************************************************************************************/
//	public void setOrganization(OrganizationDTO value)
//	{
//		_organization = value;
//	}
//	
//	public OrganizationDTO getOrganization()
//	{
//		return _organization;
//	}
//	
//	/*****************************************************************************************/
//	public void setAddress(AddressDTO value)
//	{
//		_address = value;
//	}
//	
//	public AddressDTO getAddress()
//	{
//		return _address;
//	}
//	
//	/*****************************************************************************************/
//	public void setIndividual(IndividualDTO value)
//	{
//		_individual = value;
//	}
//	
//	public IndividualDTO getIndividual()
//	{
//		return _individual;
//	}
}
