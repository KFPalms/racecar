/**
 * Copyright (c) 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.helpers.interfaces;


import java.util.ArrayList;

import com.pdi.data.dto.NoteHolder;
import com.pdi.data.dto.Participant;
import com.pdi.data.dto.SessionUser;

public interface IParticipantHelper {
	public Participant fromId(SessionUser session, String participantId);
	public ArrayList<Participant> fromProjectId(SessionUser session, String engagementId);
	public ArrayList<Participant> fromClientId(SessionUser session, String clientId);
	public ArrayList<Participant> fromAdminId(SessionUser session, String adminId);
	public ArrayList<Participant> find(SessionUser session, String AdminId, String target);
	public NoteHolder getNotes(SessionUser session, String participantId);
}
