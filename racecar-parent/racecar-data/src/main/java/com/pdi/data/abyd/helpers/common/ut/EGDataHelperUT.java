package com.pdi.data.abyd.helpers.common.ut;

import java.sql.Connection;

import com.pdi.data.abyd.dto.common.EGFullDataDTO;
import com.pdi.data.abyd.helpers.common.EGDataHelper;
import com.pdi.ut.UnitTestUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.data.nhn.util.NhnDatabaseUtils;

import junit.framework.TestCase;

public class EGDataHelperUT  extends TestCase
{
	//
	// Constructors
	//
	public EGDataHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
	throws Exception
	{
		junit.textui.TestRunner.run(EGDataHelperUT.class);
	}


	/*
	 * testGetEGDisplayData with a link param
	 */
//	public void testGetEGDisplayData_linkparam()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//		NhnDatabaseUtils.setUnitTest(true);
//		
//		Connection con = AbyDDatabaseUtils.getDBConnection();
//		String lpId = "19567579";
//		//String lpId = "30754392";
//		
//		EGDataHelper helper = new EGDataHelper(con, lpId);        
//		EGFullDataDTO dto = helper.getEGDisplayData();
//		System.out.print(dto.toString());
//
//		UnitTestUtils.stop(this);
//	}


	/*
	 * testGetEGDisplayData with explicit data
	 */
	public void testGetEGDisplayData_explicit()
		throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);
		NhnDatabaseUtils.setUnitTest(true);
		
		Connection con = AbyDDatabaseUtils.getDBConnection();
		String partId = "60254";
		long dnaId = 6;
		long moduleId = 14;

		EGDataHelper helper = new EGDataHelper(con, partId, dnaId, moduleId);        
		EGFullDataDTO dto = helper.getEGDisplayData();
		System.out.print(dto.toString());

		UnitTestUtils.stop(this);
	}

}
