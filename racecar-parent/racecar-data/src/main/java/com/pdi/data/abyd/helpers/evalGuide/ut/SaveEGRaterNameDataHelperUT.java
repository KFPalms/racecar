package com.pdi.data.abyd.helpers.evalGuide.ut;

import java.sql.Connection;

import com.pdi.data.abyd.dto.common.EGRaterNameData;
import com.pdi.data.abyd.dto.evalGuide.EGRaterNameDTO;
import com.pdi.data.abyd.helpers.evalGuide.SaveEGRaterNameDataHelper;
import com.pdi.ut.UnitTestUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.data.util.language.LanguageItemDTO;

import junit.framework.TestCase;

public class SaveEGRaterNameDataHelperUT extends TestCase
{
	//
	// Constructors
	//
	public SaveEGRaterNameDataHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
	throws Exception
	{
		junit.textui.TestRunner.run(SaveEGRaterNameDataHelperUT.class);
	}


	/*
	 * testSaveEgRaterName
	 */
	public void testSaveEgRaterName()
		throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);
		
		Connection con = AbyDDatabaseUtils.getDBConnection();
    	EGRaterNameDTO data = new EGRaterNameDTO();
    	data.setParticipantId("111");
    	data.setDnaId(22);
    	data.setModuleId(33);
    	EGRaterNameData nameDat = new EGRaterNameData();
    	nameDat.setRaterFirstName("Marcus");
    	nameDat.setRaterLastName("Welby HUT");
       	data.setRaterNameData(nameDat);
       	// Language stuff
       	LanguageItemDTO lang = new LanguageItemDTO();
       	lang.setId(1);
       	lang.setDisplayName("Lang 1");
       	data.setSimLangItem(lang);
       	//data.setSimLangOther("Bogus - should be ignored");
       	data.setSimLangOther("X");
		
		SaveEGRaterNameDataHelper helper = new SaveEGRaterNameDataHelper(con, data);    
		helper.saveRaterName();
		System.out.println("Check the db...");

		UnitTestUtils.stop(this);
	}
}
