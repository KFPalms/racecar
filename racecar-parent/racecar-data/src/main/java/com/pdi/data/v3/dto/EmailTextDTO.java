/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.dto;


import com.pdi.data.util.BaseDTO;
import com.pdi.data.util.RequestStatus;


/**
 * EmailTextDTO is a class designed to convey the information for one e-mail
 * text translation type and its request status used in the PDI V3 extensions.
 * 
 * @author		Ken Beukelman
 * @author 		MB Panichi
 * 
  */

public class EmailTextDTO   extends BaseDTO 
{


	//
	// Static data.
	//


	//
	// Static methods.
	//


	//
	// Instance data.
	//
	private long _emailTextId;
	private long _emailId;
	private long _languageId;
	private String _isoCode;
	private String _languageName;
	private String _subjectText = null;
	private String _bodyText = null;
	private boolean _baseline;
	private boolean _changed;


	//
	// Constructors.
	//
	public EmailTextDTO()
	{
		// does nothing right now
	}
	
	// Status setup constructor - email text data remains unset
	public EmailTextDTO(RequestStatus stat)
	{
		this.setStatus(stat);
	}
	
	
	//
	// Instance methods.
	//

	/**
	 * Generic toString method
	 */
	public String toString() {
		String str = "EmailTextDTO - " +
					 "Status code=" + this.getStatus().getStatusCode() +
					 ", id=" + this.getEmailTextId();
		return str;
	}
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public long getEmailTextId()
	{
		return _emailTextId;
	}
	
	public void setEmailTextId(long value)
	{
		_emailTextId = value;
	}
	
	/*****************************************************************************************/
	public long getEmailId()
	{
		return _emailId;
	}
	
	public void setEmailId(long value)
	{
		_emailId = value;
	}	

	/*****************************************************************************************/
	public long getLanguageId()
	{
		return _languageId;
	}
	
	public void setLanguageId(long value)
	{
		_languageId = value;
	}	

	/*****************************************************************************************/
	public String getIsoCode()
	{
		return _isoCode;
	}
	
	public void setIsoCode(String value)
	{
		_isoCode = value;
	}	

	/*****************************************************************************************/
	public String getLanguageName()
	{
		return _languageName;
	}
	
	public void setLanguageName(String value)
	{
		_languageName = value;
	}	
	
	/*****************************************************************************************/
	public String getSubjectText()
	{
		return _subjectText;
	}
	
	public void setSubjectText(String value)
	{
		_subjectText = value;
	}
	
	/*****************************************************************************************/
	public String getBodyText()
	{
		return _bodyText;
	}
	
	public void setBodyText(String value)
	{
		_bodyText = value;
	}
	
	/*****************************************************************************************/
	public boolean getBaseline()
	{
		return _baseline;
	}
	
	public void setBaseline(boolean value)
	{
		_baseline = value;
	}
	/*****************************************************************************************/
	public boolean getChanged()
	{
		return _changed;
	}
	
	public void setChanged(boolean value)
	{
		_changed = value;
	}
	
}
