package com.pdi.data.v3.helpers.ut;

import com.pdi.data.v3.dto.ProjectDTO;
import com.pdi.data.v3.dto.ReportDTO;
import com.pdi.data.v3.dto.SessionUserDTO;
import com.pdi.data.v3.helpers.ProjectDataHelper;
import com.pdi.data.v3.helpers.ReportDataHelper;
import com.pdi.properties.PropertyLoader;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

public class ReportDataHelperUT  extends TestCase
{
	public ReportDataHelperUT(String name)
	{
		super(name);
	} 

	public void testProperties() throws Exception
	{
		UnitTestUtils.start(this);

		//Does the local property work?
		String applicationId = PropertyLoader.getProperty("com.pdi.data.v3.application", "application.code");
		assertEquals(applicationId != null, true);
		assertEquals(applicationId.equals("PDI-DATA-V3"), true);

		UnitTestUtils.stop(this);
	}
	    
	public void testGetReports() throws Exception
	{
		UnitTestUtils.start(this);

		SessionUserDTO sessionUser = new SessionUserDTO();
		sessionUser.setUsername(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.username")); 
		sessionUser.setPassword(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.password"));

		// step one : get projects		
		// this gets us the product and client ids as well... 
		for(ProjectDTO pDTO : ProjectDataHelper.getProjects(sessionUser).getProjectList())
		{
			System.out.println("projectID = " + pDTO.getId());
			System.out.println("getProjectURI: " + pDTO.getUri());
			System.out.println("prodGUID: " + pDTO.getProductId());
			//System.out.println("clientID:  " + pDTO.getClient().getId());

			// step 2 :  what reports are available
			// for which products/clients?
			// gonna get this from the reportDataHelper... 		
			//dev V3ProdId = 4F97F776-A007-4387-8146-A5386D6BEFCC 
			//dev clientId = OVNX-JPFM 

			for(ReportDTO rDTO : ReportDataHelper.getReportsForClientProduct("4F97F776-A007-4387-8146-A5386D6BEFCC", "OVNX-JPFM"))
			{
				System.out.println("reportID: " + rDTO.getReportId());
				System.out.println("reportName: " + rDTO.getReportName());			
			}

			UnitTestUtils.stop(this);
		}
	}   
}
