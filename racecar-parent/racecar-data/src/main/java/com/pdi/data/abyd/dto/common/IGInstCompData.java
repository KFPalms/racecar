/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.dto.common;

import java.util.ArrayList;

import com.pdi.data.abyd.dto.common.KeyValuePairStrings;
import com.pdi.data.abyd.dto.intGrid.IGScaleScoreData;

/**
 * IGInstCompData is a thin class containing information about one of the competencies that
 * is being measured using the instruments (GPI and Cognitives) in the Integration Grid.
 * 
 * NOTE:  This class is here not because it is used in multiple spots in the server but
 *        because it is used multiple spots in the client-side.
 * 
 * @author		Ken Beukelman
 */
public class IGInstCompData
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private long _compId;								// The internal competency id
	private String _compName;							// The name of this competency
	private String _compDesc;							// The description for this competency in this model
	private ArrayList<KeyValuePairStrings> _cogScores;	// cog instrument scores
	private ArrayList<KeyValuePairStrings> _gpiScores;	// GPI scale scores
	private ArrayList<IGScaleScoreData> _cogColumnScores;	// cog instrument scores
	private ArrayList<IGScaleScoreData> _gpiColumnScores;	// GPI scale scores
	private String _calcedCompScore = "";				// Calculated score for this competency
	//
	// Constructors.
	//
	public IGInstCompData()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//

	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
	
	/*****************************************************************************************/
	public long getCompId()
	{
		return _compId;
	}
		
	public void setCompId(long value)
	{
		_compId = value;
	}

	/*****************************************************************************************/
	public String getCompName()
	{
		return _compName;
	}
		
	public void setCompName(String value)
	{
		_compName = value;
	}

	/*****************************************************************************************/
	public String getCompDesc()
	{
		return _compDesc;
	}
		
	public void setCompDesc(String value)
	{
		_compDesc = value;
	}

	/*****************************************************************************************/
	public ArrayList<KeyValuePairStrings> getCogScores()
	{
		return _cogScores;
	}
		
	public void setCogScores(ArrayList<KeyValuePairStrings> value)
	{
		_cogScores = value;
	}

	/*****************************************************************************************/
	public ArrayList<KeyValuePairStrings> getGpiScores()
	{
		return _gpiScores;
	}
		
	public void setGpiScores(ArrayList<KeyValuePairStrings> value)
	{
		_gpiScores = value;
	}
	
	/*****************************************************************************************/
	public ArrayList<IGScaleScoreData> getCogColumnScores()
	{
		return _cogColumnScores;
	}
		
	public void setCogColumnScores(ArrayList<IGScaleScoreData> value)
	{
		_cogColumnScores = value;
	}

	/*****************************************************************************************/
	public ArrayList<IGScaleScoreData> getGpiColumnScores()
	{
		return _gpiColumnScores;
	}
		
	public void setGpiColumnScores(ArrayList<IGScaleScoreData> value)
	{
		_gpiColumnScores = value;
	}

	/*****************************************************************************************/
	public String getCalcedCompScore()
	{
		return _calcedCompScore;
	}
		
	public void setCalcedCompScore(String value)
	{
		_calcedCompScore = value;
	}



	/*
	 * Show me what you've got
	 */
	public String toString()
	{
		String ret = "IGInstCompData content (Java)\n";
		ret += "           Competency ID=" + _compId + "\n";
		ret += "         Competency Name=" + _compName + "\n";
		ret += "  Competency Description=" + _compDesc + "\n";
		ret += "  Cog Scores:\n";
		if (_cogScores == null)
		{
			ret += "    No Cog score data present";
		}
		else
		{
			for(KeyValuePairStrings kvp : _cogScores)
			ret += "    " + kvp.getTheKey() + " = " + kvp.getTheValue();
		}
		ret += "  GPI Scores:\n";
		if (_gpiScores == null)
		{
			ret += "    No GPI scale score data present";
		}
		else
		{
			for(KeyValuePairStrings kvp : _gpiScores)
			ret += "    " + kvp.getTheKey() + " = " + kvp.getTheValue();
		}
		ret += "  Cog Column Scores:\n";
		if (_cogColumnScores == null)
		{
			ret += "No Cog column score data present";
		}
		else
		{
			for(IGScaleScoreData score : _cogColumnScores)
			{
				ret += score.toString();
			}
		}
		ret += "  GPI Column Scores:\n";
		if (_gpiColumnScores == null)
		{
			ret += "No GPI column score data present";
		}
		else
		{
			for(IGScaleScoreData score : _gpiColumnScores)
			{
				ret += score.toString();
			}
		}
		ret += " Calced Competency Score=" + _calcedCompScore + "\n";

		return ret;
	}
}
