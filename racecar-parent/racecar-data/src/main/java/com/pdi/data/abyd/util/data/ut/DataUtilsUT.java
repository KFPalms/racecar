package com.pdi.data.abyd.util.data.ut;

import java.util.ArrayList;

import com.pdi.data.abyd.util.data.DataUtils;
import com.pdi.data.dto.Instrument;
import com.pdi.data.dto.Participant;
import com.pdi.data.dto.Project;
import com.pdi.data.dto.ProjectParticipantInstrument;
import com.pdi.data.nhn.util.NhnDatabaseUtils;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

public class DataUtilsUT  extends TestCase
{
	//
	// Constructors
	//
	public DataUtilsUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
	throws Exception
	{
		junit.textui.TestRunner.run(DataUtilsUT.class);
	}


	/*
	 * suppressSubInstUtil
	 */
	public void testGetSummaryData()
		throws Exception
	{
		UnitTestUtils.start(this);
		NhnDatabaseUtils.setUnitTest(true);
		
		ArrayList<ProjectParticipantInstrument> inp = new ArrayList<ProjectParticipantInstrument>();
		Project proj = new Project();
		proj.setName("Bogus testing project");
		Participant part = new Participant();
		part.setFirstName("Bogus");
		part.setLastName("Test-Participant");
		
		// Instruments
		Instrument inst = new Instrument();
		inst.setCode("lei");
		ProjectParticipantInstrument ppi = new ProjectParticipantInstrument();
		ppi.setProject(proj);
		ppi.setParticipant(part);
		ppi.setInstrument(inst);
		inp.add(ppi);
		inst = new Instrument();
		inst.setCode("gpi");
		ppi = new ProjectParticipantInstrument();
		ppi.setProject(proj);
		ppi.setParticipant(part);
		ppi.setInstrument(inst);
		inp.add(ppi);
		inst = new Instrument();
		inst.setCode("gpil");
		ppi = new ProjectParticipantInstrument();
		ppi.setProject(proj);
		ppi.setParticipant(part);
		ppi.setInstrument(inst);
		inp.add(ppi);
		inst = new Instrument();
		inst.setCode("cs");
		ppi = new ProjectParticipantInstrument();
		ppi.setProject(proj);
		ppi.setParticipant(part);
		ppi.setInstrument(inst);
		inp.add(ppi);
		inst = new Instrument();
		inst.setCode("cs2");
		ppi = new ProjectParticipantInstrument();
		ppi.setProject(proj);
		ppi.setParticipant(part);
		ppi.setInstrument(inst);
		inp.add(ppi);
//		inst = new Instrument();
//		inst.setCode("cs3");
//		ppi = new ProjectParticipantInstrument();
//		ppi.setProject(proj);
//		ppi.setParticipant(part);
//		ppi.setInstrument(inst);
//		inp.add(ppi);
		
		System.out.println("Input has " + inp.size() + " entries");
		for(ProjectParticipantInstrument i : inp)
		{
			System.out.println("  " + i.getInstrument().getCode());
		}

		DataUtils ut = new DataUtils();
		ArrayList<ProjectParticipantInstrument> out = ut.suppressSubInstUtil(inp);  
		
		System.out.println("output has " + out.size() + " entries");
		for(ProjectParticipantInstrument o : out)
		{
			System.out.println("  " + o.getInstrument().getCode());
		}

		UnitTestUtils.stop(this);
	}

}
