/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.dto;

import java.util.ArrayList;

import com.pdi.data.util.BaseDTO;
import com.pdi.data.util.RequestStatus;

/**
 * UserListDTO is designed to convey the information for a number of users.
 * It contains an ArrayList of UserDTO objects; the status in each of the
 * individual elements describes the state of that element.
 * 
 * @author		Ken Beukelman
 */
public class UserListDTO extends BaseDTO
{
	//
	// Static data.
	//


	//
	// Static methods.
	//


	//
	// Instance data.
	//
	private ArrayList<UserDTO> _userList = new ArrayList<UserDTO>();


	//
	// Constructors.
	//
	public UserListDTO()
	{
		// does nothing right now
	}
	
	// Set up a new DTO with status but no data
	public UserListDTO(RequestStatus stat)
	{
		this.setStatus(stat);
	}
	
	
	//
	// Instance methods.
	//

	/**
	 * Generic toString method
	 */
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append("UserListDTO:  ");
		sb.append("Status code=" + this.getStatus().getStatusCode() + ", count=" + _userList.size());
		for(int i=0; i < _userList.size(); i++)
		{
			_userList.get(i).toString();
		}
		return sb.toString();
	}
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public ArrayList<UserDTO> getUserList()
	{
		return _userList;
	}
	
	public void setUserList(ArrayList<UserDTO> value)
	{
		_userList = value;
	}	
}
