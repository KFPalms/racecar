/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 *
 * $Header: /Platform/Java/sourceCode/com/pdicorp/app/tcm/export/vo/ExportDataVO.java 6     8/25/05 3:23p Kbeukelm $
 */
package com.pdi.data.abyd.dto.common;


/**
* EGOverallData data is a container for the overall rating and the notes fields
* at the end of the EG.  It is used in the full data and overall data DTOs.
*
* @author		Ken Beukelman
* @version	$Revision: 1 $  $Date: 8/25/05 3:23p $
*/
public class EGOverallData
{
	private int _opr;							// Overall Performance Rating
	private String _briefDesc;					// Brief Description
	private String _keyStrengths;				// Key Strengths
	private String _keyDevNeeds;				// Key Development Needs
	private String _coachNotes;					// Real-time Coaching Notes
	private String _themesFromPeers; 			// Themes From Peers
	private String _themesFromDirectReports; 	// Themes From Direct Reports
	private String _themesFromBoss; 			// Themes From Boss

	public EGOverallData()
	{
		// Does nothing presently
	}

	/*****************************************************************************************/
	public int getOpr()
	{
		return _opr;
	}

	public void setOpr(int value)
	{
		_opr = value;
	}

	/*****************************************************************************************/
	public String getBriefDesc()
	{
		return _briefDesc;
	}

	public void setBriefDesc(String value)
	{
		_briefDesc = value;
	}

	/*****************************************************************************************/
	public String getKeyDevNeeds()
	{
		return _keyDevNeeds;
	}

	public void setKeyDevNeeds(String value)
	{
		_keyDevNeeds = value;
	}

	/*****************************************************************************************/
	public String getKeyStrengths()
	{
		return _keyStrengths;
	}

	public void setKeyStrengths(String value)
	{
		_keyStrengths = value;
	}

	/*****************************************************************************************/
	public String getCoachNotes()
	{
		return _coachNotes;
	}

	public void setCoachNotes(String value)
	{
		_coachNotes = value;
	}

	/*****************************************************************************************/
	public String getThemesFromPeers()
	{
		return this._themesFromPeers;
	}

	public void setThemesFromPeers(String value)
	{
		this._themesFromPeers = value;
	}

	/*****************************************************************************************/
	public String getThemesFromDirectReports()
	{
		return this._themesFromDirectReports;
	}

	public void setThemesFromDirectReports(String value)
	{
		this._themesFromDirectReports = value;
	}

	/*****************************************************************************************/
	public String getThemesFromBoss()
	{
		return this._themesFromBoss;
	}

	public void setThemesFromBoss(String value)
	{
		this._themesFromBoss = value;
	}

	/*
	 * Show me what you've got
	 */
	public String toString()
	{
		String ret = "EGOverallData content (Java)\n";
		ret += "  Overall Performance Rating=" + _opr + "\n";
		ret += "                 Description=" + _briefDesc + "\n";
		ret += "               Key Strengths=" + _keyStrengths + "\n";
		ret += "       Key Development Needs=" + _keyDevNeeds + "\n";
		ret += "    Real-time Coaching Notes=" + _coachNotes + "\n";
		ret += "           Themes From Peers=" + _themesFromPeers + "\n";
		ret += "  Themes From Direct Reports=" + _themesFromDirectReports + "\n";
		ret += "            Themes From Boss=" + _themesFromBoss + "\n";

		return ret;
	}
}