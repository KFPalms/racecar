/**
 * Copyright (c) 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.helpers.interfaces;

import java.util.HashMap;

import com.pdi.data.dto.SessionUser;
import com.pdi.data.dto.Text;
import com.pdi.data.dto.TextGroup;

/*
 * ITextHelper is the interface for the internationaliaztion helper
 */
public interface ITextHelper
{
	TextGroup fromGroupCodeLanguageCode(SessionUser sessionUser, String groupCode, String languageCode);
	TextGroup fromGroupCodeLanguageCodeWithDefault(SessionUser sessionUser, String groupCode, String languageCode);
	HashMap<String, TextGroup> fromGroupCode(SessionUser sessionUser, String groupCode);
	Text fromGroupCodeLanguageCodeLabelCode(SessionUser sessionUser, String groupCode, String languageCode, String labelCode);
	Text fromGroupCodeLanguageCodeLabelCodeWithDefault(SessionUser sessionUser, String groupCode, String languageCode, String labelCode);
	String fromTextIdLanguageCode(SessionUser sessionUser, String textId, String languageCode);
	String fromTextIdLanguageCodeWithDefault(SessionUser sessionUser, String textId, String languageCode);
	void clearGroupCache();
	void clearGroupCache(String groupCode);
}
