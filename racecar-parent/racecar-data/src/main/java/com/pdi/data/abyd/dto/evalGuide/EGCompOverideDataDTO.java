/**
* Copyright (c) 2017 Korn Ferry
* All rights reserved.
*/
package com.pdi.data.abyd.dto.evalGuide;

public class EGCompOverideDataDTO
{
	/**
	* EGCompOverideDataDTO is a thin class containing all of the
	* data needed to add or update the competency overide score 
	* for a competency in an Eval Guide.
	*/
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _participantId;
	private long _dnaId;
	private long _moduleId;
	private long _compId;
	private int _orScoreIdx;	// Consultant entered overide score for the competency


	//
	// Constructors.
	//
	public EGCompOverideDataDTO()
	{
		// Does nothing presently
	}
	
	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public String getParticipantId()
	{
		return _participantId;
	}
		
	public void setParticipantId(String value)
	{
		_participantId = value;
	}

	/*****************************************************************************************/
	public long getDnaId()
	{
		return _dnaId;
	}
		
	public void setDnaId(long value)
	{
		_dnaId = value;
	}

	/*****************************************************************************************/
	public long getModuleId()
	{
		return _moduleId;
	}
		
	public void setModuleId(long value)
	{
		_moduleId = value;
	}

	/*****************************************************************************************/
	public long getCompId()
	{
		return _compId;
	}
		
	public void setCompId(long value)
	{
		_compId = value;
	}
	
	/*****************************************************************************************/
	public int getOrScoreIdx()
	{
		return _orScoreIdx;
	}

	public void setOrScoreIdx(int value)
	{
		_orScoreIdx = value;
	}
}
