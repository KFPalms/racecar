/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.dto;

import org.w3c.dom.Node;

import com.pdi.data.util.BaseDTO;
import com.pdi.data.util.RequestStatus;
import com.pdi.xml.XMLUtils;

/**
 * DTO for raw score information
 * 
 * NOTE: This is a place holder class... scores may be returned as key/value pairs
 *
 * @author		Gavin Myers
 */
public class RawScoreDTO extends BaseDTO
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private long _questionNumber;
	private long _partNumber;
	private String _answer;
	
	//
	// Constructors.
	//

	/**
	 * "Standard" (no-parameter) constructor
	 */
	public RawScoreDTO()
	{
		super();
		// Does nothing presently
	}
	
	/**
	 * Builds the object based off an xml node
	 * @param node
	 * @throws Exception
	 */
	public RawScoreDTO(Node node)
	{
		/*
		 * The XML looks something like this:
		 * <?xml version="1.0" encoding="utf-8" standalone="yes"?>
			<ns2:answerListLine>
				<questionNumber>1</questionNumber>
				<partNumber>1</partNumber>
				<answer>1</answer>
			</ns2:answerListLine>
		 */
		
		this.setQuestionNumber(Long.parseLong(XMLUtils.getElementValue(node, "//questionNumber")));
		this.setPartNumber(Long.parseLong(XMLUtils.getElementValue(node, "//score")));
		this.setAnswer(XMLUtils.getElementValue(node, "//score"));
		
		if (this.getQuestionNumber() == 0 &&
			this.getPartNumber() == 0 &&
			this.getAnswer() == null)
		{
			this.getStatus().setStatusCode(RequestStatus.WB_NO_DATA);
		}
	}

	//
	// Instance methods.
	//

	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
	
	/*****************************************************************************************/
	public void setQuestionNumber(long value)
	{
		_questionNumber = value;
	}
	
	public long getQuestionNumber()
	{
		return _questionNumber;
	}
	
	/*****************************************************************************************/
	public void setPartNumber(long value)
	{
		_partNumber = value;
	}
	
	public long getPartNumber()
	{
		return _partNumber;
	}
	
	/*****************************************************************************************/
	public void setAnswer(String value)
	{
		_answer = value;
	}
	
	public String getAnswer()
	{
		return _answer;
	}
}
