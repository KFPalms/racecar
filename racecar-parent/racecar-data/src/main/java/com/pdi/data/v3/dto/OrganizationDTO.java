/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.dto;

import org.w3c.dom.Node;

import com.pdi.data.util.BaseDTO;
import com.pdi.data.util.RequestStatus;
import com.pdi.xml.XMLUtils;

/**
 * DTO for organization information
 * 
 *
 * @author		Gavin Myers
 */
public class OrganizationDTO extends BaseDTO
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _id;
	private String _name;

	//
	// Constructors.
	//

	/**
	 * "Standard" (no-parameter) constructor
	 */
	public OrganizationDTO()
	{
		// Does nothing presently
	}
	
	/**
	 * Builds the object based off an xml node
	 * @param node
	 * @throws Exception
	 */
	public OrganizationDTO(Node node)
	{
		/*
		 * The XML looks something like this:
		 * <organisationIdentifier>OIDE-OIDE</organisationIdentifier>
		 * <organisation>My Company name</organisation>
		 */	
		this.setId(XMLUtils.getElementValue(node, "organisationIdentifier/."));
		this.setName(XMLUtils.getElementValue(node, "organisation/."));
		
		if (this.getId() == null &&
			this.getName() == null)
		{
			this.getStatus().setStatusCode(RequestStatus.WB_NO_DATA);
		}
	}

	//
	// Instance methods.
	//
	
	/**
	 * Generic toString method
	 */
	public String toString() {
		String str = "\n --- OrganizationDTO --- \n";
		str += "\nID:                  " + this.getId();
		str += "\nName:                " + this.getName();
		
		return str;
	}
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
	
	/*****************************************************************************************/
	public void setId(String value)
	{
		_id = value;
	}
	
	public String getId()
	{
		return _id;
	}
	
	/*****************************************************************************************/
	public void setName(String value)
	{
		_name = value;
	}
	
	public String getName()
	{
		return _name;
	}
}
