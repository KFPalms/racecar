/*
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.helpers.ut;

import java.util.ArrayList;
import java.util.Iterator;

import com.pdi.data.util.RequestStatus;
import com.pdi.data.v3.dto.ClientDTO;
import com.pdi.data.v3.dto.EmailDTO;
import com.pdi.data.v3.dto.EmailTextDTO;
import com.pdi.data.v3.dto.ParticipantDTO;
import com.pdi.data.v3.dto.ParticipantEmailListDTO;
import com.pdi.data.v3.dto.ParticipantListDTO;
import com.pdi.data.v3.dto.ProjectDTO;
import com.pdi.data.v3.dto.SessionUserDTO;
import com.pdi.data.v3.helpers.ClientDataHelper;
import com.pdi.data.v3.helpers.EmailDataHelper;
import com.pdi.data.v3.helpers.ProjectDataHelper;
import com.pdi.data.v3.util.V3DatabaseUtils;
import com.pdi.properties.PropertyLoader;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;


public class EmailDataHelperUT  extends TestCase
{

	
	
	//
	// Constructors
	//

	public EmailDataHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args) throws Exception
    {
		junit.textui.TestRunner.run(EmailDataHelperUT.class);
    }

	/*
	 * testProperties
	 */
	public void testProperties()
		throws Exception
	{
    	UnitTestUtils.start(this);
    	
    	//Does the local property work?
    	String applicationId = PropertyLoader.getProperty("com.pdi.data.v3.application", "application.code");
    	assertEquals(applicationId != null, true);
    	assertEquals(applicationId.equals("PDI-DATA-V3"), true);
    	 
    	UnitTestUtils.stop(this);
    }
 
    
    /*
     * testAll
     */
	public void testAll()
		throws Exception
	{
		UnitTestUtils.start(this);
		V3DatabaseUtils.setUnitTest(true);

	   	SessionUserDTO sessionUser = new SessionUserDTO();
    	sessionUser.setUsername(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.username")); 
    	sessionUser.setPassword(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.password"));

		// Actual test code starts here
		
		// Check the emailDTO insert
    	System.out.println("Test - Email item insert");
		EmailDTO ins = new EmailDTO();
		ins.setDisplayName("Unit Test type");
		ins.setRecipientTypeId(4);
		ins.setDescription("Test email");
		ins.setGroupId(1);	// just put it in the global group for now
		EmailDTO insOut = EmailDataHelper.addEmailItem(sessionUser, ins);
		assertEquals("addEmail status != OK - " + insOut.getStatus().getExceptionMessage(), 
					 RequestStatus.RS_OK, insOut.getStatus().getStatusCode());
		assertFalse("addEmail emailId not returned", (0 == insOut.getEmailId()));
		System.out.println("Email item insert succeeded; New Email ID = " + insOut.getEmailId());


		// Now check the select for that item
    	System.out.println("Test - Proxy get");
		EmailDTO proxy = EmailDataHelper.getEmailProxy(insOut.getEmailId());
		assertEquals("getEmailProxy status != OK",
					 RequestStatus.RS_OK, proxy.getStatus().getStatusCode());
		assertEquals("getEmailProxy returned incorrect name",
				insOut.getDisplayName(), proxy.getDisplayName());
		System.out.println("Proxy get succeeded");

		// do a select and verify that there is no language data
    	System.out.println("Test - Email get (no data)");
		EmailDTO ntInp = new EmailDTO();
		ntInp.setEmailId(insOut.getEmailId());
////		EmailDTO noDataChk = EmailDataHelper.getEmail(insOut.getEmailId());
		EmailDTO noDataChk = EmailDataHelper.getEmail(sessionUser, ntInp);
		assertEquals("getEmail (no text) status != OK",
				 RequestStatus.RS_OK, noDataChk.getStatus().getStatusCode());
		assertEquals("getEmail returned incorrect name",
				insOut.getDisplayName(), noDataChk.getDisplayName());
		assertTrue("getEmail (no text) text exists",(0 == noDataChk.getTextMap().size()));
		System.out.println("Email get (no data) succeeded");

		// Check the email text insert on that email item 
    	System.out.println("Test - Text insert");
		EmailTextDTO textIns = new EmailTextDTO();
		textIns.setEmailId(insOut.getEmailId());
		textIns.setLanguageId(1);
		textIns.setSubjectText("test subject text");
		textIns.setBodyText("test body text");
		EmailTextDTO newEmailTextOut = EmailDataHelper.addEmailText(sessionUser, textIns);
//System.out.println("New email text id=" + newEmailTextOut.getEmailTextId());
		assertEquals("addEmailText status != OK",
					 RequestStatus.RS_OK, newEmailTextOut.getStatus().getStatusCode());
		//assertTrue("addEmailText id not returned", (0 == newEmailTextOut.getEmailTextId()));
		assertFalse("addEmailText id not returned", (0 == newEmailTextOut.getEmailTextId()));
		System.out.println("Text insert succeeded.  ID=" + newEmailTextOut.getEmailTextId());


		// do a select and verify that there is the one language data
    	System.out.println("Test - Email get (one text)");
		EmailDTO oneInp = new EmailDTO();
		oneInp.setEmailId(insOut.getEmailId());
////		EmailDTO oneDataChk = EmailDataHelper.getEmail(insOut.getEmailId());
		EmailDTO oneDataChk = EmailDataHelper.getEmail(sessionUser, oneInp);
		assertEquals("getEmail (single text) status != OK",
				 RequestStatus.RS_OK, oneDataChk.getStatus().getStatusCode());
		assertTrue("getEmail (one text) incorrect number of texts exist",(1 == oneDataChk.getTextMap().size()));
		for(Iterator<String> itr = oneDataChk.getTextMap().keySet().iterator(); itr.hasNext(); )
		{
			String key = itr.next();
			assertTrue("bad text id in single text chack",(newEmailTextOut.getEmailTextId() == oneDataChk.getTextMap().get(key).getEmailTextId()));
		}
		System.out.println("Email get (one text) succeeded.");


		// Check the email text insert on a second language 
    	System.out.println("Test - Text insert #2");
		EmailTextDTO textIns2 = new EmailTextDTO();
		textIns2.setEmailId(insOut.getEmailId());
		textIns2.setLanguageId(50);
		textIns2.setSubjectText("test subject text #2");
		textIns2.setBodyText("test body text #2");
		EmailTextDTO newEmailTextOut2 = EmailDataHelper.addEmailText(sessionUser, textIns2);
		assertEquals("addEmailText status != OK",
					 RequestStatus.RS_OK, newEmailTextOut2.getStatus().getStatusCode());
		//assertTrue("addEmailText2 id not returned", (0 == newEmailTextOut2.getEmailId()));
		assertFalse("addEmailText2 id not returned", (0 == newEmailTextOut2.getEmailId()));
		System.out.println("Text insert #2 succeeded.  ID=" + newEmailTextOut2.getEmailTextId());


		// do a select and verify that there are two language entries
    	System.out.println("Test - getEmail (2 texts)");
		EmailDTO twoInp = new EmailDTO();
		twoInp.setEmailId(insOut.getEmailId());
////		EmailDTO twoDataChk = EmailDataHelper.getEmail(insOut.getEmailId());
		EmailDTO twoDataChk = EmailDataHelper.getEmail(sessionUser, twoInp);
		assertEquals("getEmail (2 texts) status != OK",
				 RequestStatus.RS_OK, oneDataChk.getStatus().getStatusCode());
		assertEquals("getEmail (2 texts) incorrect number of texts exist", 2, twoDataChk.getTextMap().size());
		System.out.println("Get All lang succeeded.  Data:");
		System.out.println(" Email Id            " + twoDataChk.getEmailId());
		System.out.println(" Recipient Type Id   " + twoDataChk.getRecipientTypeId());
		System.out.println(" Recipient Type Name " + twoDataChk.getRecipientTypeName());
		System.out.println(" Display Name        " + twoDataChk.getDisplayName());
		System.out.println(" Description         " + twoDataChk.getDescription());
		for(Iterator<String> iter = twoDataChk.getTextMap().keySet().iterator(); iter.hasNext(); )
		{
			String key = iter.next();
			EmailTextDTO textDTO = twoDataChk.getTextMap().get(key);
			System.out.println("   *************  ");
			System.out.println("   Email Id       " + textDTO.getEmailId());
			System.out.println("   Email Text Id  " + textDTO.getEmailTextId());
			System.out.println("   Language Id    " + textDTO.getLanguageId());
			System.out.println("   Language Name  " + textDTO.getLanguageName());
			System.out.println("   Subject Text   " + textDTO.getSubjectText());
			System.out.println("   Body Text      " + textDTO.getBodyText());
			System.out.println("   *************  ");
		}


		// Test the email text get
    	System.out.println("Test - Text get (by email & lang)");
		EmailTextDTO textGetChk = EmailDataHelper.getEmailText(insOut.getEmailId(), 50);
		assertEquals("getEmailText status != OK",
				 RequestStatus.RS_OK, textGetChk.getStatus().getStatusCode());
		assertEquals("getEmailText subject text error",
				textIns2.getSubjectText(), textGetChk.getSubjectText());
		System.out.println("Text get (by email & lang) succeeded.");

		
		// Check the update on the email header item
		
		// use the previous EmailDTO, since it's there.
		// update the strings by adding the date, and change the recipient type
    	System.out.println("Test - update EmailDTO");
		twoDataChk.setDisplayName("updateDispNameTest: " + twoDataChk.getEmailId());
		twoDataChk.setDescription("updateDescTest: " + twoDataChk.getEmailId());
		twoDataChk.setRecipientTypeId(1);
		
		RequestStatus updtReqStat = EmailDataHelper.updateEmailItem(sessionUser, twoDataChk);
		assertEquals("updateEmailItem - email status != OK.  msg="  + updtReqStat.getExceptionMessage() + ".  ",
				RequestStatus.RS_OK, updtReqStat.getStatusCode());		
		System.out.println("update EmailDTO succeeded");	

		// RequestStatus updateEmail(EmailTextDTO eText) -- body and subject
		// Check the emailText update
		// use the previous emailTextDTO newEmailTextOut2
    	System.out.println("Test - update EmailTextDTO");
		newEmailTextOut2.setSubjectText("test updateEmailText subject " + newEmailTextOut2.getEmailTextId());
		newEmailTextOut2.setBodyText("test updateEmailText body " + newEmailTextOut2.getEmailTextId());
		
		updtReqStat = EmailDataHelper.updateEmailText(sessionUser, newEmailTextOut2);
		assertEquals("updateEmail - email status != OK.  msg="  + updtReqStat.getExceptionMessage() + ".  ",
				RequestStatus.RS_OK, updtReqStat.getStatusCode());		
		System.out.println("update EmailTextDTO succeeded");

		//Check that the item deletes		
		// Reuse an existing item:  twoDataChk
    	System.out.println("Test - delete email");
		updtReqStat = EmailDataHelper.deleteEmail(sessionUser, twoDataChk.getEmailId());
		assertEquals("deleteEmail status != OK.  msg= " + updtReqStat.getExceptionMessage() + ".  ",
					 RequestStatus.RS_OK, updtReqStat.getStatusCode());		
		System.out.println("delete email succeeded.");

		//Check that the item un-deletes		
		// Reuse an existing item:  twoDataChk
    	System.out.println("Test - un-delete email");
		updtReqStat = EmailDataHelper.unDeleteEmail(sessionUser, twoDataChk.getEmailId());
		assertEquals("un-deleteEmail status != OK.  msg= " + updtReqStat.getExceptionMessage() + ".  ",
					 RequestStatus.RS_OK, updtReqStat.getStatusCode());		
		System.out.println("un-delete email succeeded.");

		// Actual test code ends here
		
		UnitTestUtils.stop(this);
	}
	

    /*
     * testGetParticipantEmail
     */
	public void testGetParticipantEmail() throws Exception
	{
		UnitTestUtils.start(this);
		
    	SessionUserDTO sessionUser = new SessionUserDTO();
    	sessionUser.setUsername(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.username")); 
    	sessionUser.setPassword(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.password"));
		

    	for(ClientDTO client : ClientDataHelper.getClients(sessionUser).getClientList())
    	{
    		System.out.println("--> client name:  " +client.getName());
    		System.out.println("--> client id:  " + client.getId());
    		for(ProjectDTO project : ProjectDataHelper.getProjects(sessionUser, client).getProjectList())
    		{
    			System.out.println("----> project id :  " + project.getId());
    			if (project.isProxy())
    			{
    				project = ProjectDataHelper.getProject(sessionUser, project);
    			}
    			ParticipantListDTO participantList= new ParticipantListDTO();
    			participantList.setParticipantList(project.getParticipantList());
    			ArrayList<ParticipantEmailListDTO> emailList = EmailDataHelper.getParticipantEmailList(sessionUser, participantList);
    			for(ParticipantEmailListDTO email : emailList)
    			{
    				System.out.println("------> email : " + email.getEmail().getDisplayName());
    				for(ParticipantDTO participant : email.getParticipantList().getParticipantList())
    				{
    					System.out.println("----------> participant : " + participant.getCandidate().getIndividual().getEmail());
    				}
    			}
    		}
    	}
		
		UnitTestUtils.stop(this);
	}
}
