package com.pdi.data.v2.helpers.delegated;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdi.data.dto.Norm;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.interfaces.INormHelper;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.v2.util.V2DatabaseUtils;

import com.pdi.logging.LogWriter;

public class NormHelper implements INormHelper{

	private static final Logger log = LoggerFactory.getLogger(NormHelper.class);
	/**
	 * Get the Norm from it's normId.
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.INormHelper#fromId(com.pdi.data.dto.SessionUser, java.lang.String)
	 */
	public Norm fromId(SessionUser session, int normId){
	
		Norm norm = new Norm();
		try{			
				StringBuffer sb = new StringBuffer();
				sb.append("select normId, mean, stDev, spssValue " +
						  "from pdi_norm " +
						  "where normId = ? ");
				DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
				if (dlps.isInError())
				{
					LogWriter.logSQL(LogWriter.WARNING, this, "Error preparing query." + dlps.toString(), sb.toString());
				}
				DataResult dr = null;
				ResultSet rs = null;

				try {
					dlps.getPreparedStatement().setInt(1, normId);
					dr = V2DatabaseUtils.select(dlps);
					rs = dr.getResultSet();
					if(rs != null) {
						while (rs.next())
						{
							norm.setId(rs.getInt("normId"));
							norm.setMean(rs.getDouble("mean"));
							norm.setStandardDeviation(rs.getDouble("stDev"));
							norm.setSpssValue(rs.getString("spssValue"));
						}
					}
				}
				catch(java.sql.SQLException se)
				{
					LogWriter.logSQLWithException(LogWriter.ERROR, this, se.getMessage(), sb.toString(), se);
				}
				finally
				{
					if (dr != null) { dr.close(); dr = null; }
				}
								
		}catch(Exception e) {
			LogWriter.logBasicWithException(LogWriter.ERROR, this, e.getMessage(), e);
		}
			return norm;
	}

	/**
	 * Get a Norm from it's SPSS value
	 * and also it's normGroupId (in the database, V2NormId)
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.INormHelper#fromSpssValue(com.pdi.data.dto.SessionUser, java.lang.String)
	 */
	public Norm fromSpssValueGroupNormId(SessionUser session, String spssValue, int normGroupId){
		
		Norm norm = new Norm();
		try{			
				StringBuffer sb = new StringBuffer();
				sb.append("select normId, mean, stDev, spssValue " +
						  "from pdi_norm " +
						  "where spssValue = ? " +
						  "and normGroupId = ? ");
				
				DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
				if (dlps.isInError())
				{
					LogWriter.logSQL(LogWriter.WARNING, this, 
							"Error preparing query." + dlps.toString(), 
							sb.toString());
				}
				DataResult dr = null;
				ResultSet rs = null;

				try {
					dlps.getPreparedStatement().setString(1, spssValue);
					dlps.getPreparedStatement().setInt(2, normGroupId);
					dr = V2DatabaseUtils.select(dlps);
					rs = dr.getResultSet();
					if(rs != null) {
						while (rs.next())
						{
							norm.setId(rs.getInt("normId"));
							norm.setMean(rs.getDouble("mean"));
							norm.setStandardDeviation(rs.getDouble("stDev"));
							norm.setSpssValue(rs.getString("spssValue"));	
						}
					}
				}
				catch(java.sql.SQLException se)
				{
					LogWriter.logSQLWithException(LogWriter.ERROR, this, se.getMessage(), sb.toString(), se);
				}
				finally
				{
					if (dr != null) { dr.close(); dr = null; }
				}
								
		}catch(Exception e) {
			LogWriter.logBasicWithException(LogWriter.ERROR, this, e.getMessage(), e);
		}
			return norm;
	}	
	
	public Map<Integer, HashMap<String,Norm>> fromNormGroupIdBatch(SessionUser session, String instrumentCode){
		Map<Integer, HashMap<String, Norm>> batchNorms = new HashMap<Integer, HashMap<String, Norm>>();
	
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT normId, normGroupId, mean, stDev, spssValue ");
		sb.append("FROM pdi_norm ");
		sb.append("WHERE normGroupId IN (");
		sb.append("SELECT normGroupId FROM pdi_norm_group WHERE instrumentCode= ? AND active=1)");
		
		log.debug("SQL for getting the Norms: {}", sb.toString());
		
		try {
			DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
			DataResult dr = null;
			ResultSet rs = null;
			try {
				dlps.getPreparedStatement().setString(1, instrumentCode);
				dr = V2DatabaseUtils.select(dlps);
				rs = dr.getResultSet();
				
				if(rs != null) {
					while (rs.next())
					{
						Norm norm = new Norm();
						norm.setId(rs.getInt("normId"));
						norm.setMean(rs.getDouble("mean"));
						norm.setStandardDeviation(rs.getDouble("stDev"));
						norm.setSpssValue(rs.getString("spssValue"));
						norm.setNormGroupId(rs.getInt("normGroupId"));
						if (batchNorms.get(norm.getNormGroupId()) == null){
							Map<String, Norm> spssNormMap = new HashMap<String, Norm>();
							batchNorms.put(Integer.valueOf(norm.getNormGroupId()), (HashMap<String, Norm>) spssNormMap);
						}
						batchNorms.get(norm.getNormGroupId()).put(norm.getSpssValue(), norm);
						
					}
				} else {
					log.debug("Resultset for fetching norms was null");
				}
				/* -- print out all the norms to the log
				for (HashMap<String, Norm> hashmap : batchNorms.values()){
					for (Norm norm : hashmap.values()){
						log.debug("Loaded Norm: {}, SPSS value: {}", norm.getId(), norm.getSpssValue());
					}
				}
				*/
			}
			
			catch(java.sql.SQLException se)
			{
				LogWriter.logSQLWithException(LogWriter.ERROR, this, se.getMessage(), sb.toString(), se);
			}
			finally
			{
				if (dr != null) { dr.close(); dr = null; }
			}
		}
		catch(Exception e) {
			LogWriter.logBasicWithException(LogWriter.ERROR, this, e.getMessage(), e);
		}
		
		return batchNorms;
	}
	
	
	/**
	 *  Get the array of Norms associated 
	 *  with a particular NormGroup
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.INormHelper#fromNormGroupId(com.pdi.data.dto.SessionUser, java.lang.String)
	 */
	public HashMap<String, Norm> fromNormGroupId(SessionUser session, int normGroupId){
		HashMap<String, Norm> norms = new HashMap<String, Norm>();
		
		try{			
				StringBuffer sb = new StringBuffer();
				sb.append("select normId, mean, stDev, spssValue  " +
						  "from pdi_norm " +
						  "where normGroupId = ?");
				
				DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
				if (dlps.isInError())
				{
					LogWriter.logSQL(LogWriter.WARNING, this, "Error preparing query." + dlps.toString(), sb.toString());					
				}
				DataResult dr = null;
				ResultSet rs = null;

				try {
					dlps.getPreparedStatement().setInt(1, normGroupId);
					dr = V2DatabaseUtils.select(dlps);
					rs = dr.getResultSet();
					if(rs != null) {
						while (rs.next())
						{
							Norm norm = new Norm();
							norm.setId(rs.getInt("normId"));
							norm.setMean(rs.getDouble("mean"));
							norm.setStandardDeviation(rs.getDouble("stDev"));
							norm.setSpssValue(rs.getString("spssValue"));								
							norms.put(norm.getSpssValue(), norm);
						}
					}
				}
				catch(java.sql.SQLException se)
				{
					LogWriter.logSQLWithException(LogWriter.ERROR, this, se.getMessage(), sb.toString(), se);
				}
				finally
				{
					if (dr != null) { dr.close(); dr = null; }
				}
		}catch(Exception e) {
			LogWriter.logBasicWithException(LogWriter.ERROR, this, e.getMessage(), e);
		}
		return norms;
	}
}

