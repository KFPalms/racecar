/**
 * Copyright (c) 2008 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.dto.common;


/**
 * CompElementDTO is a thin class containing competency data.
 * 
 * @author		Ken Beukelman
 */
public class CompElementDTO
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private long _compId;			// The id of the relevant competency
	private String _compName;		// The name of the competency.  Used only in Integration Grid
	
	// No longer used... essential flags moved to Report Model/Custom Competency
	// Member is retained for backwards compatability purposes (most code was commented
	// out over a year ago... value is always false
	private boolean _isEssential;	// The state of the relevant competency (Essential or not essential)

	//
	// Constructors.
	//
	public CompElementDTO()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public long getCompId()
	{
		return _compId;
	}
	
	public void setCompId(long value)
	{
		_compId = value;
	}	

	/*****************************************************************************************/
	public String getCompName()
	{
		return _compName;
	}
	
	public void setCompName(String value)
	{
		_compName = value;
	}	

	/*****************************************************************************************/
	public boolean getIsEssential()
	{
		return _isEssential;
	}
	
	public void setIsEssential(boolean value)
	{
		_isEssential = value;
	}	



	/*
	 * Show me what you've got
	 */
	public String toString()
	{
		String ret = "CompElementDTO content (Java)\n";
		ret += "    Competency ID=" + _compId + "\n";
		ret += "  Competency Name=" + _compName + "\n";
////		ret += "     is Essential=" + _isEssential + "\n";
		
		return ret;
	}
}