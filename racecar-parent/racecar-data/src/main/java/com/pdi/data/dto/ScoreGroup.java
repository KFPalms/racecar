package com.pdi.data.dto;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ScoreGroup extends CoreDataObject {
	private static final Logger log = LoggerFactory.getLogger(ScoreGroup.class);
	
	private static final long serialVersionUID = -7857968335339008094L;
	private HashMap<String, Score> scores = new HashMap<String, Score>();
	private NormGroup generalPopulation;
	private NormGroup specialPopulation;

	public static HashMap<String, NormScore> calculateNormScores(HashMap<String, Score> scores, NormGroup normGroup) {
		HashMap<String, NormScore> normScores = new HashMap<String, NormScore>();
		log.debug("Creating norm scores for {} scores for Norm Group: {}", scores.size(), normGroup.getName());
		if (scores != null && normGroup != null) {
			for (String st : scores.keySet()) {
				log.debug("Score keyset: {}", st);
				if (normGroup.getNorms() != null) {
					Norm n = normGroup.getNorms().get(st);
					if (n == null){
						log.debug("Norm is null");
					}
					Score s = scores.get(st);
					if (s == null){
						log.debug("Score is null");
					}
					if (s != null && n != null) {
						NormScore ns = new NormScore();
						ns.setNorm(n);
						ns.setScore(s);
						normScores.put(st, ns);
						// System.out.println(" spss: " + n.getSpssValue() + "
						// raw:
						// " + s.getRawScore() + " score: " + s.getScore());
					}
				} else {
					log.debug("Norm Group {} has no Norms", normGroup.getName());
				}
			}
		}
		return normScores;
	}

	public HashMap<String, NormScore> calculateGeneralPopulation() {
		return ScoreGroup.calculateNormScores(this.getScores(), this.getGeneralPopulation());
	}

	public HashMap<String, NormScore> calculateSpecialPopulation() {
		return ScoreGroup.calculateNormScores(this.getScores(), this.getSpecialPopulation());
	}

	public void setScores(HashMap<String, Score> scores) {
		this.scores = scores;
	}

	public HashMap<String, Score> getScores() {
		return scores;
	}

	public void setGeneralPopulation(NormGroup generalPopulation) {
		this.generalPopulation = generalPopulation;
	}

	public NormGroup getGeneralPopulation() {
		return generalPopulation;
	}

	public void setSpecialPopulation(NormGroup specialPopulation) {
		this.specialPopulation = specialPopulation;
	}

	public NormGroup getSpecialPopulation() {
		return specialPopulation;
	}
}
