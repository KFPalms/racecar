/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.evalGuide;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;


/**
 * SubmitEGDataHelper contains the code needed to submit the Eval Guide.
 *
 * @author		Ken Beukelman
 */
public class SubmitEGDataHelper
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private Connection con;
	private String participantId;
	private long dnaId;
	private long moduleId;

	//
	//
	// Constructors.
	//
	//protected SaveDNADescHelper(Connection con, DNADescription dd, Properties props)
	public SubmitEGDataHelper(Connection con, String partId, long dnaId, long modId)
		throws Exception
	{
		if (con == null)
			throw new Exception("A vaild connection is required for the EG submit function.");
		
		// check for key data
		if (partId == null || partId.length() == 0  ||
			dnaId	== 0 ||
			modId == 0)
		{
			throw new Exception("Invalid Comp Data key:  part=" +  (partId == null ? "null" : (partId.length() == 0 ? "<empty string>" : partId)) +
					            ", dna=" + dnaId +
					            ", module=" + modId);
		}

		this.con = con;
		this.participantId = partId;
		this.dnaId = dnaId;
		this.moduleId = modId;

	}

	//
	// Instance methods.
	//

	/**
	 * Set the submit flag
	 * This will be an update because data will have been written by the time we get here.
	 *
	 * @throws Exception
	 */
	public void submitEG()
		throws Exception
	{
		Statement stmt = null;
		StringBuffer sqlQuery = new StringBuffer();

		sqlQuery.append("UPDATE pdi_abd_eg_response ");
		sqlQuery.append("  SET egSubmitted = 1 ");
		sqlQuery.append("  WHERE participantId = " + this.participantId + " ");
		sqlQuery.append("    AND dnaId = " + this.dnaId + " ");
		sqlQuery.append("    AND moduleId = " + this.moduleId);

		try
		{
	       	stmt = this.con.createStatement();
	       	stmt.executeUpdate(sqlQuery.toString());
	
			return;
		}
		catch (SQLException ex)
		{
			// handle any errors
        	throw new Exception("SQL submitEG.  " +
        			"SQLException: " + ex.getMessage() + ", " +
        			"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (stmt != null)
			{
				try  {  stmt.close();  }
				catch (SQLException sqlEx)  {  /* swallow the error */  }
				stmt = null;
			}
		}
	}
}
