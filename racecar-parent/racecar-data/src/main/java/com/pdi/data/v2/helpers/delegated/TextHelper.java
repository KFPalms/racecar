/**
 * Copyright (c) 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v2.helpers.delegated;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.pdi.data.dto.Language;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.dto.Text;
import com.pdi.data.dto.TextGroup;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.ILanguageHelper;
import com.pdi.data.helpers.interfaces.ITextHelper;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.util.RequestStatus;
import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.logging.LogWriter;
import com.pdi.properties.PropertyLoader;


/*
 * TextHelper provides language differentiated text support in the V2 environment
 */
public class TextHelper implements ITextHelper
{
	//
	// Static data
	//
	private static HashMap<String, TextGroupHolder> _textCache = new HashMap<String, TextGroupHolder>();
	
	//
	// Static methods.
	//
	
	//
	// Instance data.
	//
	
	//
	// Instance methods.
	//
	
//	/*
//	 * initializeCache - initializer called by the ApplicationInitializer
//	 * This method either creates an empty group holder (if it does not
//	 * exist) or clears the existing cache.
//	 * 
//	 * This method is NOT in the interface (should it be?)
//	 */
//	public void initializeCache()
//	{
//		if (_textCache == null)
//		{
//			_textCache = new HashMap<String, TextGroupHolder>();
//		}
//		else
//		{
//			clearGroupCache();
//		}
//	}



	/*
	 * fromGroupCodeLanguageCode - Get a TextGroup (all of the labels available
	 * 		for a text group in a particular language).
	 * 
	 * @param sessionUser - A SessionUser object
	 * @param groupCode - The group code desired
	 * @param languageCode - The language desired
	 * @return A TextGroup object
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.ITextHelper#fromGroupCodeLanguageCode(com.pdi.data.dto.SessionUser, java.lang.String, java.lang.String)
	 */
	@Override
	public TextGroup fromGroupCodeLanguageCode(SessionUser sessionUser,
											   String groupCode,
											   String languageCode)
	{
		// Check for nulls
		if (groupCode == null || groupCode.length() < 1)
			return null;
		if (languageCode == null || languageCode.length() < 1)
			return null;

		// See if the group is in cache
		if (! _textCache.containsKey(groupCode))
		{
			// It does not, put it in cache
			if (! cacheGroup(sessionUser, groupCode))
			{
				// No data for the group code or load error
				return null;
			}
		}
		
		// Return the language.  If we get to here we know that the group
		// exists and we should have no problem getting its holder.  We
		// also know that if the language does not exist in the group,
		// the get() returns null, which is the desired result
		// Always return a clone
		//return _textCache.get(groupCode).getGroupHolder().get(languageCode).clone();
		TextGroup tg = _textCache.get(groupCode).getGroupHolder().get(languageCode);
		if (tg != null)
			tg = tg.clone();
		
		return tg;
	}


	/*
	 * fromGroupCodeLanguageCodeWithDefault - Get a TextGroup (all of the labels available
	 * 		for a text group in a particular language).  If the labels are not available
	 * 		in the requested language, send them back in the default language (US English).
	 * 
	 * @param sessionUser - A SessionUser object
	 * @param groupCode - The group code desired
	 * @param languageCode - The language desired
	 * @return A TextGroup object (the language of the Text item might be different from that requested).
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.ITextHelper#fromGroupCodeLanguageCodeWithDefault(com.pdi.data.dto.SessionUser, java.lang.String, java.lang.String)
	 */
	@Override
	public TextGroup fromGroupCodeLanguageCodeWithDefault(SessionUser sessionUser,
														  String groupCode,
														  String languageCode)
	{
		// Check for nulls
		if (groupCode == null || groupCode.length() < 1)
			return null;
		if (languageCode == null || languageCode.length() < 1)
			return null;

		// See if the group is in cache
		if (! _textCache.containsKey(groupCode))
		{
			// It does not, put it in cache
			if (! cacheGroup(sessionUser, groupCode))
			{
				// No data for the group code or load error
				return null;
			}
		}
		
		// Get the default language for the group (We know the default language exists)
		String dfltLangCode = _textCache.get(groupCode).getDefaultLangCode();
		if (dfltLangCode.equals(languageCode))
		{
			// No need to do the replacements... just send back a clone of the TextGroup
			//return _textCache.get(groupCode).getGroupHolder().get(languageCode);
			TextGroup tg = _textCache.get(groupCode).getGroupHolder().get(languageCode);
			if (tg != null)
				tg = tg.clone();
			
			return tg;
		}

		// Got a non_default language request... get the default texts
		// Assumes that there will always be a labelMap
		HashMap<String, Text> out = new HashMap<String, Text>(_textCache.get(groupCode).getGroupHolder().get(dfltLangCode).getLabelMap());
		
		// Check if the requested language exists
		if (_textCache.get(groupCode).getGroupHolder().containsKey(languageCode))
		{
			// Has the requested language... get it and process it
			// No clone needed; this is currently used as referenced and not passed out
			HashMap<String, Text> rqstTxt = _textCache.get(groupCode).getGroupHolder().get(languageCode).getLabelMap();

			// Loop through the requested labels and overlay the default ones
			for (Iterator<Map.Entry<String, Text>> itr=rqstTxt.entrySet().iterator(); itr.hasNext();  )
			{
				Map.Entry<String, Text> ent = itr.next();
				out.put(ent.getKey(), ent.getValue());
			}
		}

		// Build the output TextGroup
		TextGroup ret = new TextGroup(groupCode, languageCode);
		ret.setLabelMap(out);
		
		return ret;
	}

	
	/*
	 * fromGroupCode - Returns TextGroups for all of the languages associated
	 * 		with a particular text group.
	 * 
	 * @param sessionUser - A SessionUser object
	 * @param groupCode - The group code for which text is desired
	 * @return A HashMap of TextGroup objects, keyed by language code
	 * 
	 * @param
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.ITextHelper#fromGroupCode(java.lang.String)
	 */
	@Override
	public HashMap<String, TextGroup> fromGroupCode(SessionUser sessionUser, String groupCode)
	{
		// Check for nulls
		if (groupCode == null || groupCode.length() < 1)
			return null;
		
		// See if the group is in cache
		if (! _textCache.containsKey(groupCode))
		{
			// It does not, put it in cache
			if (! cacheGroup(sessionUser, groupCode))
			{
				// No data for the group code or load error
				return null;
			}
		}
		
		//return _textCache.get(groupCode).getGroupHolder();
		HashMap<String, TextGroup> hm = _textCache.get(groupCode).getGroupHolder();
		if((hm != null))
			hm = new HashMap<String, TextGroup>(hm);
		
		return hm;
	}


	/*
	 * fromGroupCodeLanguageCodeLabelCode - Get a single particular text from
	 * 		a code group in a specified language
	 * 
	 * @param sessionUser - A SessionUser object
	 * @param groupCode - The group code for which text is desired
	 * @param languageCode - The language code for which text is desired
	 * @param labelCode - The label code for which text is desired
	 * @return A Text object containing the desired text
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.ITextHelper#fromGroupCodeLanguageCodeLabelCode(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public Text fromGroupCodeLanguageCodeLabelCode (SessionUser sessionUser,
													String groupCode,
													String languageCode,
													String labelCode)
	{
		// Check for nulls
		if (groupCode == null || groupCode.length() < 1)
			return null;
		if (languageCode == null || languageCode.length() < 1)
			return null;
		if (labelCode == null || labelCode.length() < 1)
			return null;

		// See if the group is in cache
		if (! _textCache.containsKey(groupCode))
		{
			// It does not, put it in cache
			if (! cacheGroup(sessionUser, groupCode))
			{
				// No data for the group code or load error
				return null;
			}
		}
		
		// Does the language exist in the group?
		if (! _textCache.get(groupCode).getGroupHolder().containsKey(languageCode))
			return null;	// Nope... scram
		
		// Return the label... will return null if it does not exist
		//return _textCache.get(groupCode).getGroupHolder().get(languageCode).getLabelMap().get(labelCode);
		Text txt = _textCache.get(groupCode).getGroupHolder().get(languageCode).getLabelMap().get(labelCode);
		if (txt != null)
			txt = txt.clone();
		
		return txt;
	}


	/*
	 * fromGroupCodeLanguageCodeLabelCodeWithDefault - Get a single particular text from
	 * 		a code group in a specified language
	 * 
	 * @param sessionUser - A SessionUser object
	 * @param groupCode - The group code for which text is desired
	 * @param languageCode - The language code for which text is desired
	 * @param labelCode - The label code for which text is desired
	 * @return A Text object containing the desired text
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.ITextHelper#fromGroupCodeLanguageCodeLabelCodeWithDefault(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public Text fromGroupCodeLanguageCodeLabelCodeWithDefault (SessionUser sessionUser,
															   String groupCode,
															   String languageCode,
															   String labelCode)
	{
		// Check for nulls
		if (groupCode == null || groupCode.length() < 1)
			return null;
		if (languageCode == null || languageCode.length() < 1)
			return null;
		if (labelCode == null || labelCode.length() < 1)
			return null;

		// See if the group is in cache
		if (! _textCache.containsKey(groupCode))
		{
			// It does not, put it in cache
			if (! cacheGroup(sessionUser, groupCode))
			{
				// No data for the group code or load error
				return null;
			}
		}

		// Get the default language code (default should exist at this point)
		String dfltLangCode = _textCache.get(groupCode).getDefaultLangCode();

		// Does the language exist in the group?  If not, try to return
		// the text from the default language
		if (! _textCache.get(groupCode).getGroupHolder().containsKey(languageCode))
		{
			// Language not found...
			// return the default language label (if it doesn't exist it'll be null)
			//return _textCache.get(groupCode).getGroupHolder().get(dfltLangCode).getLabelMap().get(labelCode);
			Text txt = _textCache.get(groupCode).getGroupHolder().get(dfltLangCode).getLabelMap().get(labelCode);
			if (txt != null)
				txt = txt.clone();
			return txt;
		}
		
		// Language exists... Does the label?
		Text txt = _textCache.get(groupCode).getGroupHolder().get(languageCode).getLabelMap().get(labelCode);
		if (txt == null)
		{
			// Nope label does not exist.  Send back the text in the default language (or a null)
			txt = _textCache.get(groupCode).getGroupHolder().get(dfltLangCode).getLabelMap().get(labelCode);
			if (txt != null)
				txt = txt.clone();
		}
		else
		{
			txt = txt.clone();
		}
		return txt;
	}

	
	/*
	 * fromTextIdLanguageCode - Get a specific text in the specified language.  Not readily
	 * 	available from the group
	 * 
	 * Note that this method returns a String; a Text object includes a labelCode which is
	 * not available in this context.
	 * 
	 * @param sessionUser - A SessionUser object
	 * @param textId - The arbitrary DB ID for a text string
	 * @param languageCode - The language code for which text is desired
	 * @return A String containing the specified text
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.ITextHelper#fromTextIdLanguageCode(com.pdi.data.dto.SessionUser, java.lang.String, java.lang.String)
	 */
	@Override
	public String fromTextIdLanguageCode(SessionUser sessionUser,
										 String textId,
										 String languageCode)
	{
		return fetchString(sessionUser, textId, languageCode, false);
	}

	
	/*
	 * fromTextIdLanguageCodeWithDefault - Get a specific text in the specified language.
	 * 		If not available in the requested language, send back in the default language.
	 * 
	 * Note that this method returns a String; a Text object includes a labelCode which is
	 * not available in this context.
	 * 
	 * @param sessionUser - A SessionUser object
	 * @param textId - The arbitrary DB ID for a text string
	 * @param languageCode - The language code for which text is desired
	 * @return A String containing the specified text
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.ITextHelper#fromTextIdLanguageCodeWithDefault(com.pdi.data.dto.SessionUser, java.lang.String, java.lang.String)
	 */
	@Override
	public String fromTextIdLanguageCodeWithDefault(SessionUser sessionUser,
													String textId,
													String languageCode)
	{
		return fetchString(sessionUser, textId, languageCode, true);
	}
	
	/*
	 * clearGroupCache - empties the group cache of all text data.
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.ITextHelper#clearGroupCache()
	 */
	@Override
	public void clearGroupCache()
	{
		_textCache.clear();
		return;
	}
	
	
	/*
	 * clearGroupCache - Empties the group text cache of the text data
	 *                   associated with the named group.
	 *
	 * @param groupCode - The group code to clear.
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.ITextHelper#clearGroupCache(java.lang.String)
	 */
	@Override
	public void clearGroupCache(String groupCode)
	{
		// Check for nulls
		if (groupCode == null || groupCode.length() < 1)
			return;
		
		// Get the group
		TextGroupHolder tgh = _textCache.get(groupCode);
		// See if the group is in cache
		if (tgh == null)
		{
			// Not in cache... "Our job is done here!"
			return;
		}
		
		// Group exists... remove it
		_textCache.remove(groupCode);
		return;
	}



	//--------------  debug methods  ----------------------
	/*
	 * showCache - Show the contents of the cache (which groups are present)
	 */
	public String showCache()
	{
		String str = "GroupTexts in cache (" + _textCache.size() + ") entries:\n";
		for (Iterator<Map.Entry<String, TextGroupHolder>> itr=_textCache.entrySet().iterator(); itr.hasNext();  )
		{
			Map.Entry<String, TextGroupHolder> ent = itr.next();
			str += "  key=" + ent.getKey() + "\n" ;
			str += "  code=" + ent.getValue().getGroupCode() + " (" + ent.getValue().getGroupId() + ")" ;
			str += ", dflt=" + ent.getValue().getDefaultLangCode();
			str += "\n";
		}
		
		return str;
	}




	// ------ private methods ------
	/*
	 * cacheGroup - Read text data from the database and put it in the text cache
	 * 
	 * @param sessionUser - A SessionUser object
	 * @param groupCode - The group code to read
	 * @param sessionUser - A SessionUser object
	 * @return A true/false status
	 */
	private boolean cacheGroup(SessionUser sessionUser, String groupCode)
	{
		// check for nulls
		if (groupCode == null || groupCode.length() < 1)
			return false;
		
		// Get the data from the database
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT tg.textGroupId, ");
		sb.append("       tg.groupKey as groupCode, ");
		sb.append("       gtm.textId, ");
		sb.append("       gtm.labelCode, ");
		sb.append("       txt.text, ");
		sb.append("       txt.isDefault, ");
		sb.append("       txt.languageId, ");
		sb.append("       lng.languageCode ");
		sb.append("  FROM text_group tg ");
		sb.append("    INNER JOIN group_text_map gtm ON (gtm.textGroupId = tg.textGroupId AND gtm.active = 1) ");
		sb.append("    INNER JOIN pdi_abd_text txt ON (txt.textId = gtm.textId AND txt.active = 1) ");
		sb.append("    INNER JOIN pdi_abd_language lng ON (lng.languageId = txt.languageId and txt.active = 1) ");
		sb.append("  WHERE tg.groupKey = '" + groupCode + "' ");
		sb.append("    AND tg.active = 1 ");
		sb.append("  ORDER BY txt.languageId");
		
		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
		if (dlps.isInError())
		{
			LogWriter.logSQL(LogWriter.WARNING, this,
					"cacheGroup - Error preparing query",
					sb.toString());
			return false;
		}

		DataResult dr = null;
		try
		{
			dr = V2DatabaseUtils.select(dlps);
			if(dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
				return false;	// No data... all done
			
			ResultSet rs = dr.getResultSet();
	
			TextGroupHolder tgh = null;
			
			if (rs == null)
			{
				// Some sort of data error in the select
				return false;
			}
			
			if (! rs.isBeforeFirst())
			{
				// No data
				return false;
			}
			
			// Get a language helper for later use
			ILanguageHelper lHlpr = HelperDelegate.getLanguageHelper(PropertyLoader.getProperty("com.pdi.data.v2.application", "datasource.v2"));

			// Stuff the data into a textGroupHolder
			boolean firstTime = true;
			String curLangCode = "";
			Language curLangObj = null;
			TextGroup curGroup = null;
			while(rs.next())
			{
				String gc = rs.getString("groupCode");
				String langCode = rs.getString("languageCode");
				if (firstTime)
				{
					// Create the TextGroupHolder and set the code ind ID
					tgh = new TextGroupHolder(rs.getInt("textGroupId"), gc);
					firstTime = false;
				}
				
				// Do the Default language checks.  If it isn't set, set it.  If
				// it is set, make sure it is the same language; otherwise we have
				// two languages set as default in the same group, which is
				// contrary to our assumptions and is, therefore, not allowed
				if (rs.getBoolean("isDefault"))
				{
					if (tgh.getDefaultLangCode() == null)
					{
						// Set the code
						tgh.setDefaultLangCode(langCode);
					}
					else
					{
						// We have one set.  Ensure that is the same.
						if (! langCode.equals(tgh.getDefaultLangCode()))
						{
							LogWriter.logBasic(LogWriter.WARNING, this,
									"cacheGroup - multiple default languages detected in textGroup " + groupCode);
							return false;
						}
					}
				}
				
				// OK so stuff the Text object into the appropriate TextGroup
				// See if the language is the same as the previous row
				if (! curLangCode.equals(langCode))
				{
					// Not the same.  Either the first time through
					// or we need to save one and make a new one
					if (curGroup != null)
					{
						// Save the completed text group holder
						tgh.getGroupHolder().put(curGroup.getLanguageCode(), curGroup);
					}
					
					// Make the new one
					curGroup = new TextGroup(gc, langCode);
					
					// Update the current language code and make a new Language object
					curLangCode = langCode;
					curLangObj = lHlpr.fromCode(sessionUser, langCode);
				}
				
				// Make the Text object
				String labelCode = rs.getString("labelCode");
				Text txt = new Text(rs.getInt("textId"), labelCode, curLangObj, rs.getString("text"));
				// Save the data into the current TextGroup object
				curGroup.getLabelMap().put(labelCode, txt);
			}	// End of while loop
			
			// Save the TextGroup into the TextGroupHolder
			tgh.getGroupHolder().put(curGroup.getLanguageCode(), curGroup);
			
			// Stuff the holder into the cache
			_textCache.put(tgh.getGroupCode(), tgh);
			
			//Must be cool if we made it here
			return true;
		}
		catch(Exception e)
		{
			LogWriter.logBasicWithException(LogWriter.ERROR, this,
					"cacheGroup - Error retrieving data", e);
			return false;
		}
		finally
		{
	        if (dr != null) { dr.close(); dr = null; }
		}
	}	// End of cacheGroup


		/*
		 * fetchString - Read String from the database and return it or its default.
		 * 		The query assumes that the default is the definitive label list.  If
		 * 		the label is not in the list in the default language, it does not really
		 * 		exist (even if there in the requested or other languages
//		 * 
//		 * @param groupCode - The group code to read
//		 * @param sessionUser - A SessionUser object
//		 * @return A true/false status
		 */
		private String fetchString(SessionUser sessionUser,
								   String textId,
								   String languageCode,
								   boolean sendDefault)
		{
			// check for nulls
			if (textId == null || textId.length() < 1)
				return null;
			if (languageCode == null || languageCode.length() < 1)
				return null;

			// Get the data from the database
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT txt2.text as langTxt, ");
			sb.append("       txt1.text as dfltTxt ");
			sb.append("  FROM pdi_abd_text txt1 ");
			sb.append("    LEFT JOIN pdi_abd_text txt2 ");
			sb.append("   			 ON (txt2.textId = txt1.textId AND");
			sb.append("                  txt2.active = 1 AND ");
			sb.append("		    		 txt2.languageId IN (SELECT languageId ");
			sb.append("                                        FROM pdi_abd_language ");
			sb.append("	                                       WHERE languageCode = '" + languageCode + "')) ");
			sb.append("  WHERE txt1.textId = " + textId + " ");
			sb.append("  AND txt1.active = 1 ");
			sb.append("  AND txt1.isDefault = 1");

			DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
			if (dlps.isInError())
			{
				LogWriter.logBasic(LogWriter.WARNING, this,
						"fetchString - Error preparing query.  dlps=" + dlps.toString());
				return null;
			}

			DataResult dr = null;
			try
			{
				dr = V2DatabaseUtils.select(dlps);
				if(dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
					return null;	// No data... all done
	
				ResultSet rs = dr.getResultSet();
				if (rs == null)
				{
					// Some sort of data error in the select
					return null;
				}

				// Assumes a single row
				rs.next();
				String langStr = rs.getString("langTxt");
				String dfltStr = rs.getString("dfltTxt");
				if (sendDefault)
				{
					return (langStr == null ? dfltStr : langStr);
				}
				else
				{
					return langStr;
				}
			}
			catch(SQLException e)
			{
				LogWriter.logBasicWithException(LogWriter.ERROR, this,
						"fetchString - Error retrieving data", e);
				return null;
			}
			finally
			{
		        if (dr != null) { dr.close(); dr = null; }
			}
		}	// End of cacheGroup
	
	
	
	

	// --------------  Private class  ----------------------
	/*
	 * TextGroupHolder - the text group holder holds all of the strings for
	 * 		a text group in all languages.  The container within it holds
	 * 		TextGroups keyed by the language code
	 */
	private class TextGroupHolder
	{
		// Instance variables
		private int _groupId;
		private String _groupCode;
		private String _defaultLangCode;
		private HashMap<String, TextGroup> _groupHolder;
		
		// Constructor
		public TextGroupHolder(int id, String code)
		{
			_groupId = id;
			_groupCode = code;
			_defaultLangCode = null;
			_groupHolder = new HashMap<String, TextGroup>();
		}
		
		// Accessors
		public int getGroupId()
		{
			return _groupId;
		}
//		public void setGroupId(int value)
//		{
//			_groupId = value;
//		}	
		public String getGroupCode()
		{
			return _groupCode;
		}
//		public void setGroupCode(String value)
//		{
//			_groupCode = value;
//		}	
		public String getDefaultLangCode()
		{
			return _defaultLangCode;
		}
		public void setDefaultLangCode(String value)
		{
			_defaultLangCode = value;
		}	
		public HashMap<String, TextGroup> getGroupHolder()
		{
			return _groupHolder;
		}
//		public void setGroupHolder(HashMap<String, TextGroup> value)
//		{
//			_groupHolder = value;
//		}	

	}
}
