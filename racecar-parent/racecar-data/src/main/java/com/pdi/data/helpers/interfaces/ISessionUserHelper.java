package com.pdi.data.helpers.interfaces;

import com.pdi.data.dto.SessionUser;

public interface ISessionUserHelper {
	public SessionUser createSessionUser(SessionUser session);
	public SessionUser createSessionUser();
}
