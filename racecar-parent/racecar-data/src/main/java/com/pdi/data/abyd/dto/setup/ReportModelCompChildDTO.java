/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.dto.setup;


/**
 * ReportModelCompChildDTO is a container for a report model competency child.
 * 
 * @author		Ken Beukelman
 */
public class ReportModelCompChildDTO
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private long _rmCompId;	// The ReportModel competency id
	private long _dnaCompId;	// The "real" (DNA) competency id
	private String _dnaCompName; // The "real" (DNA) Competency Name - default text
	private boolean _dnaCompEssential; //The "real" (DNA) Essentiality
	//
	// Constructors.
	//
	public ReportModelCompChildDTO()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
	
	/*****************************************************************************************/
	public long getRmCompId()
	{
		return _rmCompId;
	}

	public void setRmCompId(long value)
	{
		_rmCompId = value;
	}	
	
	/*****************************************************************************************/
	public long getDnaCompId()
	{
		return _dnaCompId;
	}

	public void setDnaCompId(long value)
	{
		_dnaCompId = value;
	}
	
	
	public String toString()
	{
		String str = "      ReportModelCompChildDTO:  rmComp=" + _rmCompId + ", dnaCompId=" + _dnaCompId + ", dnaCompName=" + _dnaCompName + "\n";
		
		return str;
	}

	public void setDnaCompName(String _dnaCompName) {
		this._dnaCompName = _dnaCompName;
	}

	public String getDnaCompName() {
		return _dnaCompName;
	}

	public void setDnaCompEssential(boolean _dnaCompEssential) {
		this._dnaCompEssential = _dnaCompEssential;
	}

	public boolean isDnaCompEssential() {
		return _dnaCompEssential;
	}
}
