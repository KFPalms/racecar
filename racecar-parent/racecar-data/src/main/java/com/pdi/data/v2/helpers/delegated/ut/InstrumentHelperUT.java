package com.pdi.data.v2.helpers.delegated.ut;


import java.util.ArrayList;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.IInstrumentHelper;
import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.properties.PropertyLoader;
import com.pdi.ut.UnitTestUtils;
import com.pdi.data.dto.Instrument;
import com.pdi.data.dto.SessionUser;
import junit.framework.TestCase;


public class InstrumentHelperUT extends TestCase{

	public InstrumentHelperUT(String name)
	{
		super(name);
	}

//	public void testInstrumentfromId()
//	throws Exception{
//		
//		System.out.println("*********** INSTRUMENT -  testInstrumentListfromProjectId ***********");
//		SessionUser session = new SessionUser();
//		session.getMetadata().put("USERNAME", "cdunn");
//		session.getMetadata().put("PASSWORD", "cdunn");
//		
//		IInstrumentHelper iI = HelperDelegate.getInstrumentHelper(PropertyLoader.getProperty("com.pdi.data.v2.application","datasource.v2"));
//		Instrument instrument = iI.fromId(session, "IXLGVIOQ");
//
//		System.out.println("     " + instrument.getCode() + "  " + instrument.getDescription() + "  " + instrument.getId());
//				
//	}
//	
//	public void testInstrumentListfromProjectId()
//	throws Exception{
//		
//		System.out.println("*********** INSTRUMENT -  testInstrumentListfromProjectId ***********");
//		SessionUser session = new SessionUser();
//		session.getMetadata().put("USERNAME", "cdunn");
//		session.getMetadata().put("PASSWORD", "cdunn");
//		
//		IInstrumentHelper iI = HelperDelegate.getInstrumentHelper(PropertyLoader.getProperty("com.pdi.data.application","datasource.v2"));
//		ArrayList<Instrument> instrumentList = iI.fromProjectId(session, "FBODOOSY");
//
//		for(Instrument instrument : instrumentList){
//			System.out.println("     " + instrument.getCode() + "  " + instrument.getDescription() + "  " + instrument.getId());
//		}
//		
//				
//	}


//	public void testGetANDSaveScoresForInstrument()
//	throws Exception{
//		
////		System.out.println("*********** INSTRUMENT -  testGetScoresForInstrument ***********");
////		SessionUser session = new SessionUser();
////		session.getMetadata().put("USERNAME", "cdunn");
////		session.getMetadata().put("PASSWORD", "cdunn");
////		
////		IInstrumentHelper iI = HelperDelegate.getInstrumentHelper(PropertyLoader.getProperty("com.pdi.data.application","datasource.v2"));
////		HashMap<String, String>  scoreMap = iI.getScoresForInstrument(session, "IXLGVIOQ", "KBBGZZBU", "CPYCWIAD");
////		
////		Iterator <String> iter = scoreMap.keySet().iterator();
////		while(iter.hasNext()){
////			
////			String key = (String) iter.next();
////			String value = scoreMap.get(key);
////			System.out.println("     " + key + " | " + value);
////			
////			
////		}				
//	}


//	public void testSaveScoresForInstrument()
//	throws Exception{
////		UnitTestUtils.start(this);
////		//AbyDDatabaseUtils.setUnitTest(true);
////		V2DatabaseUtils.setUnitTest(true);
////		
////		System.out.println("*********** INSTRUMENT -  testSaveScoresForInstrument ***********");
////		SessionUser session = new SessionUser();
////		session.getMetadata().put("USERNAME", "cdunn");
////		session.getMetadata().put("PASSWORD", "cdunn");
////		
////		HashMap<String, String> scoreMap = new HashMap<String, String>();
////	     scoreMap.put("GPI_TA", "2.75");
////	     scoreMap.put("GPI_DUT", "3");
////	     scoreMap.put("GPI_TC", "5");
////	     scoreMap.put("GPI_INIT", "2.75");
////	     scoreMap.put("GPI_VIS", "3");
////		
////		IInstrumentHelper iI = HelperDelegate.getInstrumentHelper(PropertyLoader.getProperty("com.pdi.data.application","datasource.v2"));
////		boolean success = iI.saveScoresForInstrument(session, "IXLGVIOQ", "KBBGZZBU", "CPYCWIAD", scoreMap);
////		
////		System.out.println("success = " + success);		
////		UnitTestUtils.stop(this);
////	
//		
//	}	
	
	
//	public void testGetResponsesForInstrument()
//	throws Exception{
//		
////		System.out.println("*********** INSTRUMENT -  testGetScoresForInstrument ***********");
////		SessionUser session = new SessionUser();
////		session.getMetadata().put("USERNAME", "cdunn");
////		session.getMetadata().put("PASSWORD", "cdunn");
////		
////		IInstrumentHelper iI = HelperDelegate.getInstrumentHelper(PropertyLoader.getProperty("com.pdi.data.application","datasource.v2"));
////		HashMap<String, String>  scoreMap = iI.getResponsesForInstrument(session, "IXLGVIOQ", "KBBGZZBU", "CPYCWIAD");
////		
////		Iterator <String> iter = scoreMap.keySet().iterator();
////		while(iter.hasNext()){
////			
////			String key = (String) iter.next();
////			String value = scoreMap.get(key);
////			System.out.println("     " + key + " | " + value);
////			
////			
////		}
//	}

//	/**
//	 * save answer data to v2
//	 * 
//	 */
//	public void testSaveResponsesForInstrument()
//	throws Exception{
//
////		UnitTestUtils.start(this);
////		V2DatabaseUtils.setUnitTest(true);
////		System.out.println("*********** INSTRUMENT -  testSaveResponsesForInstrument ***********");
////		SessionUser session = new SessionUser();
////		session.getMetadata().put("USERNAME", "cdunn");
////		session.getMetadata().put("PASSWORD", "cdunn");
////	
////		HashMap<String, String> answerMap = new HashMap<String, String>();
////		answerMap.put("1", "5");
////		answerMap.put("2", "5");
////		answerMap.put("3", "4");
////		answerMap.put("4", "4");
////		answerMap.put("5", "5");
////				
////		IInstrumentHelper iI = HelperDelegate.getInstrumentHelper(PropertyLoader.getProperty("com.pdi.data.application","datasource.v2"));
////		boolean success = iI.saveResponsesForInstrument(session, "IXLGVIOQ", "KBBGZZBU", "CPYCWIAD", answerMap);
////			
////		System.out.println("success = " + success);		
////		UnitTestUtils.stop(this);
////		
//	}



	public void testAll()
		throws Exception
	{
    	UnitTestUtils.start(this);
		V2DatabaseUtils.setUnitTest(true);
		
		String server = PropertyLoader.getProperty("com.pdi.data.v2.application", "webservice.path");
		System.out.println(server);
		// needs session though not using it right yet
		SessionUser session = new SessionUser();
		session.getMetadata().put("USERNAME", "kbeukelm");
		session.getMetadata().put("PASSWORD", "mysql");

		IInstrumentHelper helper = HelperDelegate.getInstrumentHelper();
		ArrayList<Instrument> list = helper.all(session, IInstrumentHelper.TYPE_ALL);
		if (list == null)
		{
			System.out.println("No instrument data found!");
		}
		else
		{
			System.out.println("TYPE_ALL call found " + list.size() + " instruments");
			for(Instrument ins : list)
			{
				System.out.println("  " + ins.toString());
			}
		}

		list = null;
		list = helper.all(session, IInstrumentHelper.TYPE_SCORABLE);
		if (list == null)
		{
			System.out.println("No instrument data found!");
		}
		else
		{
			System.out.println("TYPE_SCORABLE call found " + list.size() + " instruments");
			for(Instrument ins : list)
			{
				System.out.println("  " + ins.toString());
			}
		}
    	
    	UnitTestUtils.stop(this);
	}

}
