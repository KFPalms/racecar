package com.pdi.data.nhn.helpers.delegated.ut;


import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.nhn.util.NhnDatabaseUtils;
import com.pdi.data.dto.Language;
import com.pdi.ut.UnitTestUtils;
import junit.framework.TestCase;

public class LanguageHelperUT extends TestCase {

	public LanguageHelperUT(String name) 	{
		super(name);
	}
	
	public void testGetLanguage() throws Exception {
		UnitTestUtils.start(this);
		NhnDatabaseUtils.setUnitTest(true);
		
		for(Language l : HelperDelegate.getLanguageHelper("com.pdi.data.nhn.helpers.delegated").all(null)) {
			String c1 = l.getCode();
			String c2 = HelperDelegate.getLanguageHelper("com.pdi.data.nhn.helpers.delegated").fromId(null, l.getId()).getCode();
			String c3 = HelperDelegate.getLanguageHelper("com.pdi.data.nhn.helpers.delegated").fromCode(null, c2).getCode();
			System.out.println(c1 + " | " + c2 + " | " + c3);
			assertEquals(c1, c2);
			assertEquals(c1, c3);
			
		}
		UnitTestUtils.stop(this);
	}
	
	
}
