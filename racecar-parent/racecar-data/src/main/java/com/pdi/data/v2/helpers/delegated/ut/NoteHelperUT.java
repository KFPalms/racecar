package com.pdi.data.v2.helpers.delegated.ut;

import java.util.ArrayList;

import com.pdi.data.helpers.interfaces.INoteHelper;

import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.v2.util.V2DatabaseUtils;
//import com.pdi.properties.PropertyLoader;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.dto.Note;
import com.pdi.data.dto.NoteHolder;
//import com.pdi.ut.UnitTestUtils;
import junit.framework.TestCase;

public class NoteHelperUT extends TestCase {

	public NoteHelperUT(String name)
	{
		super(name);
	}
	
	public void testAddNewParticipantNotes()
	throws Exception
	{
		SessionUser session = new SessionUser();
		session.getMetadata().put("USERNAME", "cdunn");
		session.getMetadata().put("PASSWORD", "cdunn");

		// add this so that JDBC works in UT's
		V2DatabaseUtils.setUnitTest(true);
		
		String participantId = "IUTMFZDL";  // mb test abyd 4       09-28-2010
		
		//INoteHelper iNote = HelperDelegate.getNoteHelper(PropertyLoader.getProperty("com.pdi.data.v2.application", "datasource.v2"));
		INoteHelper iNote = HelperDelegate.getNoteHelper();
		NoteHolder holder = new NoteHolder();
		ArrayList<Note> list = new ArrayList<Note>();
		
		// set this up with NEW NOTES	
		holder.setParticipantId(participantId);
		Note n1 = new Note();
		n1.setNoteId(0);
		n1.setNoteText("This is note one text.");
		list.add(n1);
		Note n2 = new Note();
		n2.setNoteId(0);
		n2.setNoteText("This is note two text.");
		list.add(n2);
		Note n3 = new Note();
		n3.setNoteId(0);
		n3.setNoteText("This is note three text.");
		list.add(n3);
		holder.setNotesArray(list);		
		iNote.addNotes(holder);
		
	
		NoteHolder noteHolder = iNote.all(session, participantId);
		
		for(Note note : noteHolder.getNotesArray()) {
			System.out.println(note.getNoteId() + " - " + note.getNoteText());
		}
		
		// UPDATE EXISTING NOTES
		holder = new NoteHolder();
		list = new ArrayList<Note>();
		holder.setParticipantId(participantId);
		n1 = new Note();
		n1.setNoteId(4);
		n1.setNoteText("UPDATING note one text.");
		list.add(n1);
		n2 = new Note();
		n2.setNoteId(5);
		n2.setNoteText("UPDATING note two text.");
		list.add(n2);
		n3 = new Note();
		n3.setNoteId(6);
		n3.setNoteText("UPDATING note three text.");
		list.add(n3);
		holder.setNotesArray(list);		
		iNote.addNotes(holder);

		noteHolder = new NoteHolder();
		noteHolder = iNote.all(session, participantId);
		
		System.out.println("after UPDATE");
		for(Note note : noteHolder.getNotesArray()) {
			System.out.println(note.getNoteId() + " - " + note.getNoteText());
		}

				
//		// adapt
//		// nhn
//		// generic
	}

//	public void testUpdateExistingParticipantNotes()
//	throws Exception
//	{
//		SessionUser session = new SessionUser();
//		session.getMetadata().put("USERNAME", "cdunn");
//		session.getMetadata().put("PASSWORD", "cdunn");
//		
//		///HelperDelegate.setEnvrionment(PropertyLoader.getProperty("com.pdi.data.v2.application","global.application"));
//		// v2
//		//System.out.println(PropertyLoader.getProperty("com.pdi.data.v2.application","datasource"));
//		
//		INoteHelper iNote = HelperDelegate.getNoteHelper(PropertyLoader.getProperty("com.pdi.data.v2.application", "datasource.v2"));
//
//		NoteHolder holder = new NoteHolder();
//		ArrayList<Note> list = new ArrayList<Note>();
//		
//		// set this up with NEW NOTES
//		Note n1 = new Note();
//		n1.setNoteId(0);
//		n1.setNoteText("This is note one text.");
//		list.add(n1);
//		Note n2 = new Note();
//		n2.setNoteId(0);
//		n2.setNoteText("This is note two text.");
//		list.add(n2);
//		Note n3 = new Note();
//		n3.setNoteId(0);
//		n3.setNoteText("This is note three text.");
//		list.add(n3);
//		holder.setNotesArray(list);		
//		iNote.addNotes(holder);
//		
//	
//		NoteHolder noteHolder = iNote.all(session, "xxxxxx");
//		
//		for(Note note : noteHolder.getNotesArray()) {
//			System.out.println(note.getNoteId() + " - " + note.getNoteText());
//		}
//				
////		// adapt
////		// nhn
////		// generic
//	}
	
}

