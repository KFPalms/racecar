/**
 * Copyright (c) 2008 Personnel Decisions International, Inc.
 * All rights reserved.
 *
 * $Header: /com/pdicorp/app/ABD/dnaDesigner/AbdDnaService 6     8/25/05 3:23p Kbeukelm $
 */

package com.pdi.data.abyd.helpers.setup.NoLongerUsed;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;

import com.pdi.data.abyd.dto.common.KeyValuePair;

/**
 * SaveSPTextHelper contains the code needed to save the text portion of the
 * SP data.  The elements data is saved in a different class.
 * 
 * See additional comments at each of the function calls.
 *
 * @author		Ken Beukelman
 * @version	$Revision: 6 $  $Date: 8/25/05 3:23p $
 */
public class SaveSPTextHelper
{
	//
	// Static data.
	//
	/** The source revision. */
	public static final String REVISION = "$Revision: 5 $";

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private Connection connection;
	private long dnaId;
	private ArrayList<KeyValuePair> textAry;
	
	//
	// Constructors.
	//
	public SaveSPTextHelper(Connection con, long dnaId, ArrayList<KeyValuePair> ary)
		throws Exception
	{
		this.connection = con;
		this.dnaId = dnaId;
		this.textAry = ary;
	}

	//
	// Instance methods.
	//

	/**
	 * Insert or update the SP texts.  This logic may do an insert or an
	 * update, depending upon if the text is already in the database.
	 *
	 * @throws Exception
	 */
	public void saveSPTexts()
		throws Exception
	{
		// Presently, there is no "secret decoder ring" embodied in code that correlates the spTextType
		// to the content associated with that type.  here is the definition as of 4/2/09:
		//	Type	Text Type
		//	 1		Business Challenges
		//	 2		Leadership Challenges
		//	 3		Experience Requirements
		//	 4		Motivators / Career Drivers
		//	 5		Leadership Style and Culture
		//	 6		Deraillers
		//	 7		Job Requirements
		// This data is duplicated in SPDataHelper.  Changes here should also be applied there.
		// Secret decoder ring is now in SetupConstants
		
		Statement stmt = null;
		ResultSet rs = null;
		String action = "";
		StringBuffer sqlQuery = new StringBuffer();
		
////		// Before we can send an email to the client mgr indicating that the SP has changed,
////		// we need to verify that something truly is changing. We use the boolean spTextHasChanged
////		// to track if something, anything has changed through updates or inserts below.
////		boolean spTextHasChanged = false;				
////
		Map<Integer, String> hits = new HashMap<Integer, String>();
		
		// Muck about with quotes in the text strings; " -> '
		// This prevents "Bad SQL" messages for strings that include double quotes
		for(int i=0; i < this.textAry.size(); i++)
		{
			KeyValuePair kv = this.textAry.get(i);
			//long key = kv.getTheKey();
			kv.setTheValue(((String) kv.getTheValue()).trim().replace('"','\''));
			this.textAry.set(i, kv);
		}
		
		try
		{
			// Get the current keys for this dna
			action = "getting existing types";
			sqlQuery.append("SELECT spTextType as type, spText");
			sqlQuery.append("  FROM pdi_abd_dna_sp_comp_text ");
			sqlQuery.append("  WHERE dnaId = " + this.dnaId);
			sqlQuery.append("    AND active = 1");

			stmt = this.connection.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());
			if (rs.isBeforeFirst())
	       	{
				// Got something... make the list
				for(int i=0; rs.next(); i++)
				{
					hits.put(new Integer(rs.getInt("type")), rs.getString("spText"));
				}
	       	}  else  {
	       		// got nothing... do nothing
	       	}

			// clear the statement and result set
			try	{  rs.close();  }
			catch (SQLException sqlEx)	{  /* swallow the error */  }
			rs = null;
			try	{  stmt.close();  }
			catch (SQLException sqlEx)	{  /* swallow the error */  }
			stmt = null;
			
			// loop through the input
			for (Iterator<KeyValuePair> itr = this.textAry.iterator(); itr.hasNext();  )
			{
				KeyValuePair obj = itr.next();
				
				// does this key exist?
				int key = (int) obj.getTheKey();
				if ( hits.containsKey(new Integer(key)) )
				{
				    // Has the value actually changed? Don't update unless it has changed.
				    String dbValue = (String) hits.get(new Integer(key));
				    if ( !(dbValue.equals(obj.getTheValue())) )
				    {
						// Got the key, update
						action = "updating";
						sqlQuery.setLength(0);
						sqlQuery.append("UPDATE pdi_abd_dna_sp_comp_text ");
						sqlQuery.append("  SET spText = \"" + obj.getTheValue() + "\" ");
						sqlQuery.append("  WHERE dnaId = " + this.dnaId + " ");
						sqlQuery.append("    AND spTextType = " + key);
	
				       	stmt = this.connection.createStatement();
				       	stmt.executeUpdate(sqlQuery.toString());
////////				       	spTextHasChanged = true;
				    }
				}  else  {
					//No key, insert
					action = "inserting";
					sqlQuery.setLength(0);
					sqlQuery.append("INSERT INTO pdi_abd_dna_sp_comp_text ");
					sqlQuery.append("  (dnaId, spTextType, spText, active, lastUserId, lastDate) ");
					sqlQuery.append("  VALUES(" + this.dnaId + ", "+
							                  key + ", " +
											  "\"" + obj.getTheValue() + "\", " +
											  "1, " + 
											  "(SELECT SUBSTRING_INDEX(USER(),'@',1)), " +
											  "now())");

			       	stmt = this.connection.createStatement();
			       	stmt.executeUpdate(sqlQuery.toString(), Statement.RETURN_GENERATED_KEYS);
////////			       	spTextHasChanged = true;
				}
			}
////			
////			if (spTextHasChanged)
////			{
////				// A change has been made to the SP text so we should set the spTextChanged flag to true.
////				// NOTE: it appears that this will also set the flag to true on the first pass through 
////				//       for a DNA and wondering if we'd handle that properly in SaveSPElementHelper to
////				//       avoid sending duplicate emails to the client manager?
////				setSpTextChangedFlagToTrue();
////			}

			return;
		}
		catch (SQLException ex)
		{
            // handle any errors
        	throw new Exception("SQL saveSPText (" + action + ").  " +
        			"SQLException: " + ex.getMessage() + ", " +
        			"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
        }
		finally
		{
			if (rs != null)
			{
				try  {  rs.close();  }
				catch (SQLException sqlEx)  {  /* Swallow the error  */  }
				rs = null;
			}
			if (stmt != null)
			{
				try  {  stmt.close();  }
				catch (SQLException sqlEx)  {  /* Swallow the error */  }
				stmt = null;
			}
		}
	}
////
////	/**
////	 * Set the spTextChanged flag to true. When true this flag indicates that a user has
////	 * made a change to the SuccessProfile text.  This flag should be set to false by the
////	 * SaveSPElementHelper class when the saveSPElements() method is executed. 
////	 *
////	 * @throws Exception
////	 */
////	private void setSpTextChangedFlagToTrue()
////		throws Exception
////	{
////		Statement stmt = null;
////		
////		StringBuffer sqlUpdate = new StringBuffer();
////		sqlUpdate.append("UPDATE pdi_abd_dna ");
////		sqlUpdate.append("  SET spTextChanged = 1 ");
////		sqlUpdate.append("  WHERE dnaId = " + this.dnaId + " "); 	
////		
////		try
////		{
////			stmt = this.connection.createStatement();
////			stmt.executeUpdate(sqlUpdate.toString());
////		}
////        catch (SQLException ex)
////		{
////            // handle any errors
////        	throw new Exception("SQL updateSpTextChangedFlag.  " +
////        			"SQLException: " + ex.getMessage() + ", " +
////        			"SQLState: " + ex.getSQLState() + ", " +
////					"VendorError: " + ex.getErrorCode());
////        }
////        finally
////		{
////            if (stmt != null)
////            {
////                try  {  stmt.close();  }
////                catch (SQLException sqlEx)  {  /* Swallow the error */  }
////                stmt = null;
////            }
////        }
////	}
////	
}
