/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v2.helpers.delegated;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;

import com.pdi.data.dto.Note;
import com.pdi.data.dto.NoteHolder;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.interfaces.INoteHelper;
//import com.pdi.data.util.DataLayerConnection;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
//import com.pdi.data.util.DatabaseUtils;
import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.logging.LogWriter;

/*
 * NoteHelper
 */
public class NoteHelper implements INoteHelper
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//

	//
	// Constructors.
	//

	//
	// Instance methods.
	//

	/**
	 * all
	 *
	 * @param  long reportInputId
	 * @return ArrayList<ReportInputNoteDTO>
	 * @throws Exception
	 */
	//@Override
	public NoteHolder all(SessionUser session, String participantId)
	{
		NoteHolder noteHolder = new NoteHolder();
		noteHolder.setParticipantId(participantId);

		ArrayList<Note> notesArray = new ArrayList<Note>();
		noteHolder.setNotesArray(notesArray);

		StringBuffer sqlQuery = new StringBuffer();;
		sqlQuery.append(" SELECT noteId, value, lastDate  ");
		sqlQuery.append(" FROM pdi_participant_note  ");
		sqlQuery.append(" WHERE participantId =  ? ");

		DataLayerPreparedStatement dlps = null;
		DataResult dr = null;
		ResultSet rs  = null;
		
		try {

			dlps = V2DatabaseUtils.prepareStatement(sqlQuery);
			if (dlps.isInError())
			{
				LogWriter.logSQL(LogWriter.WARNING, this, dlps.toString(), sqlQuery.toString());
				return noteHolder;
			}
			
			dlps.getPreparedStatement().setString(1,participantId);

			dr =  V2DatabaseUtils.select(dlps);
			rs = dr.getResultSet();
		
				if (rs != null && rs.isBeforeFirst())
				{
					while (rs.next())
					{
						Note note = new Note();
						note.setNoteId(rs.getInt("noteId"));
						note.setNoteText(rs.getString("value"));
						note.setNoteDate(rs.getString("lastDate"));
	
						notesArray.add(note);
					}
				}
		}
		catch (SQLException sqlEx)
		{
			LogWriter.logSQL(LogWriter.ERROR, this, dlps.toString(), sqlQuery.toString());
 		}
		finally
		{
	        if (dr != null) { dr.close(); dr = null; }
		}

		return noteHolder;
	}


	/**
	 * addNotes
	 * Insert or update a Note into pdi_participant_notes
	 *
	 * @param  Connection - the database connection
	 * @param  NoteHolder holder - holds the Notes
	 * @throws Exception
	 */

	public void addNotes(NoteHolder holder)
		throws Exception
	{

		ArrayList<Note> notesArray = holder.getNotesArray();

	    String userId = "ntHlpr";

		Statement stmt = null;
		ResultSet rs = null;
		Connection con = null;
		con = V2DatabaseUtils.getDBConnection();

		try
		{
			// iterate through the dto's -- we need to look at each one...
			Iterator<Note> it = notesArray.iterator();
			while(it.hasNext())
			{

				 Note note = new Note();
				 note = it.next();

				 // look at the note... if it's a new one, it's not
				 // going to have a NoteId, because we need to
				 // get that from the database...
				 boolean insert = false;
				 if(note.getNoteId() == 0)
				 {
					 insert = true;
				 }
				 else
				 {
					 // see if there exists a row in the
					 // pdi_participant_note table...
					 // i'm leaving this check here, because I'm
					 // betting that they're going to allow updates
					 // to previous notes, even if they said they're
					 // not right now...

					 // select sql:
						StringBuffer sqlQuery = new StringBuffer();
						sqlQuery.append("  SELECT value   ");
						sqlQuery.append("  FROM pdi_participant_note  ");
						sqlQuery.append("  WHERE noteId = " + note.getNoteId() + "  ");
						//System.out.println("does this exist? = " + sqlQuery.toString());

						// run the query
						stmt = con.createStatement();
						rs = stmt.executeQuery(sqlQuery.toString());
				 }

				 // if the row DOES NOT EXIST,
				 if ((insert || !rs.isBeforeFirst()) && (note.getNoteText().length() > 0))
				 {
					 System.out.println("INSERT");
					 String noteTxt = note.getNoteText();
					 noteTxt = noteTxt.replace("'","''");
					 //...we will need to INSERT IT...
					StringBuffer sqlInsert = new StringBuffer();
					sqlInsert.append("INSERT into pdi_participant_note ");
					sqlInsert.append("  (   participantId, value, lastUserId, lastDate ) VALUES( ");
					sqlInsert.append("  ?,  ");  
					sqlInsert.append("  ?,  ");  
					sqlInsert.append("  ?,  ");
					sqlInsert.append("  GETDATE()  ");
					sqlInsert.append("   )  ");

					DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlInsert);
					dlps.getPreparedStatement().setString(1, holder.getParticipantId());
					dlps.getPreparedStatement().setString(2, noteTxt);
					dlps.getPreparedStatement().setString(3, userId);
					DataResult dr = null;
					try
					{
						dr = V2DatabaseUtils.insert(dlps);
					}
					finally
					{
						if (dr != null) dr.close(); dr = null; 
					}

		       	} else if (note.getNoteText().length() > 0){
		       		//... we just need to UPDATE IT....

		       		System.out.println("UPDATE");
		       		// need to see what the noteText value is from the select at the top of the main loop....
		       		rs.next();

					// get the database value from the previous query
					String databaseText = rs.getString("value");

					// get the incoming note value
					String incomingText = note.getNoteText();

					// AND the option_value is DIFFERENT from the incoming value
					if(databaseText.equals(incomingText))
					{
						// they're the same... do nothing
						System.out.println(" TEXT VALUES SAME : " + databaseText + " == " + incomingText);
						continue;
					} else {
						// UPDATE the value in the database
						//System.out.println(dto.getReportOptionsId() + " UPDATE: databaseValue == dtoValue : " + databaseValue + " == " + dtoValue);
						System.out.println(" TEXT VALUES DIFFER: " + databaseText + " != " + incomingText);
						
						incomingText = incomingText.replace("'","''");

						StringBuffer sqlUpdate = new StringBuffer();
						sqlUpdate.append("UPDATE pdi_participant_note ");
						sqlUpdate.append("  SET value = ?, ");
						sqlUpdate.append("  lastDate = GETDATE(),  ");
						sqlUpdate.append("  lastUserId = ?  ");
						sqlUpdate.append("  WHERE noteId = ?  ");

						DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sqlUpdate);

						dlps.getPreparedStatement().setString(1,incomingText);
						dlps.getPreparedStatement().setString(2, userId);
						dlps.getPreparedStatement().setLong(3,note.getNoteId());
						DataResult dr = null;
						try
						{
							dr = V2DatabaseUtils.update(dlps);
						}
						finally
						{
							if (dr != null) dr.close(); dr = null; 
						}
					}
		       	}
			 }// end main while iterator
		}
	    catch (SQLException ex)
		{
	        // handle any errors
			//throw new Exception("SQL updating V2 report options data.  " +
			//		"SQLException: " + ex.getMessage() + ", " +
			//		"SQLState: " + ex.getSQLState() + ", " +
			//		"VendorError: " + ex.getErrorCode());
			LogWriter.logBasicWithException(LogWriter.ERROR, this,
					"addNotes:  SQL updating V2 report options data.",
					new Exception("addNotes:  SQL updating V2 report options data.  " +
	    			"SQLException: " + ex.getMessage() + ", " +
	    			"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode()));
	    }
	    finally
		{
	        if (rs != null)
	        {
	            try
				{
	                rs.close();
	            }
	            catch (SQLException sqlEx)
				{
	            	// swallow the error
	            }
	            rs = null;
	        }
	        if (stmt != null)
	        {
	            try
				{
	                stmt.close();
	            }
	            catch (SQLException sqlEx)
				{
	            	// swallow the error
				}
	            stmt = null;
	        }
		    if (con != null)
		    {
		    	try
		    	{
		    		con.close();
		    	}
		    	catch (SQLException sqlEx)
				{
	            	// swallow the error
				}
		    	con = null;
		    }
		}
	}
}