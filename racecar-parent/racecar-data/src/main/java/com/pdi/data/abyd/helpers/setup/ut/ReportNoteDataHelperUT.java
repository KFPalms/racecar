/*
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.setup.ut;

import java.sql.Connection;

import com.pdi.data.abyd.dto.setup.ReportNoteDTO;
import com.pdi.data.abyd.helpers.setup.ReportNoteDataHelper;
import com.pdi.ut.UnitTestUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;

import junit.framework.TestCase;

public class ReportNoteDataHelperUT extends TestCase
{
	//
	// Constructors
	//

	public ReportNoteDataHelperUT(String name)
	{
		super(name);
	} 
	
	//
	// Instance methods
	//
    
	/*
	 * main
	 */
	public static void main(String[] args)
		throws Exception
    {
		junit.textui.TestRunner.run(EntryStateHelperUT.class);
    }


	/*
	 * testSaveReportNote
	 */
	public void testSaveReportNote()
		throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);

		Connection con = AbyDDatabaseUtils.getDBConnection();
		
		ReportNoteDTO dto = new ReportNoteDTO();
		dto.setDnaId(20);	// Dev
		dto.setText("DNA 20 Report note (with \"test\")");
		//dto.setDnaId(41);	// Dev
		//dto.setText("DNA 41 Report note");

		ReportNoteDataHelper reportNoteDataHelper = new ReportNoteDataHelper();
		
		reportNoteDataHelper.saveReportNote(con, dto);
		
		System.out.print("Verify in DB that DNA " + dto.getDnaId() + " reportNote column ");
		System.out.println("now should be -->" + dto.getText() + "<--");

		UnitTestUtils.stop(this);
	}


	/*
	 * testSaveReportNote
	 */
	public void testGetReportNote()
		throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);

		Connection con = AbyDDatabaseUtils.getDBConnection();
		
		//long dnaId = 20;	// Dev
		long dnaId = 41;	// Dev
		
		ReportNoteDataHelper reportNoteDataHelper = new ReportNoteDataHelper();
		
		ReportNoteDTO dto = reportNoteDataHelper.getReportNote(con, dnaId);
		
		System.out.println("Note for DNA " + dto.getDnaId() + ":  "  + dto.getText());

		UnitTestUtils.stop(this);
	}
}
