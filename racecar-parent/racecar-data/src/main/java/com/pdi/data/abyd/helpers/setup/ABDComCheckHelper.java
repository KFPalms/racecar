/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */

/*
 * WE REALLY NEED TO GENERICIZE THIS IF WE ARE GOING TO USE IT IN MULTIPLE SPOTS
 *  (There is currently a copy in EG and in Target Short LP)
 */
package com.pdi.data.abyd.helpers.setup;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * ComCheckHelper provides feedback to ensure that the remoting
 * connection is up and that the database connection is active.
 * Used by bothe SetupService and Setup2Service.
 *
 * @author		Ken Beukelman
 */
public class ABDComCheckHelper
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private Connection connection;
	
	//
	// Constructors.
	//
	public ABDComCheckHelper(Connection con)
		throws Exception
	{
		this.connection = con;
	}

	//
	// Instance methods.
	//

	/**
	 * Return a string if all is well.
	 *
	 * @return a String
	 * @throws Exception
	 */
	public String comCheck()
		throws Exception
	{
		Statement stmt = null;
		ResultSet rs = null;
		String ret;

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT GETDATE() AS dbTime");

		try
		{
			stmt = this.connection.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());
			
			rs.next();
				
			ret = "DBMS time is " + rs.getString("dbTime");
			//System.out.println("comcheck: " + ret);	
			return ret;
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL comCheck.  " +
								"SQLException: " + ex.getMessage() + ", " +
								"SQLState: " + ex.getSQLState() + ", " +
								"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (rs != null)
			{
				try  {  rs.close();  }
				catch (SQLException sqlEx)  {  /* Swallow the error */  }
				rs = null;
			}
			if (stmt != null)
			{
				try  {  stmt.close();  }
				catch (SQLException sqlEx)  {  /* Swallow the error */  }
				stmt = null;
			}
		}
	}
}