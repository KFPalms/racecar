/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 *
 * $Header: /Platform/Java/sourceCode/com/pdicorp/app/tcm/export/vo/ExportDataVO.java 6     8/25/05 3:23p Kbeukelm $
 */
package com.pdi.data.abyd.dto.evalGuide;


/**
* EGUrlData is a thin class containing a module name/URL pair
* 
* @author		Ken Beukelman
*/
public class EGUrlData
{
	//
	// Static data.
	//
	public static final int MODSTAT_NOT_STARTED = 0;
	public static final int MODSTAT_STARTED = 1;
	public static final int MODSTAT_COMPLETE = 2;

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _modName;	// The Module name
	private String _egUrl;		// The URL for the EG (or IG) for the given module
	private boolean _egFlag;	// Flag indicating this is an Eval Guide (true) or an Integration Grid (false)
	private int _modStatus;		// Flag for the status of the EG/IG

	//
	// Constructors.
	//
	public EGUrlData()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//

	/*****************************************************************************************/
	public String getModName()
	{
		return _modName;
	}
		
	public void setModName(String value)
	{
		_modName = value;
	}

	/*****************************************************************************************/
	public String getEgUrl()
	{
		return _egUrl;
	}
		
	public void setEgUrl(String value)
	{
		_egUrl = value;
	}

	/*****************************************************************************************/
	public boolean getEgFlag()
	{
		return _egFlag;
	}
		
	public void setEgFlag(boolean value)
	{
		_egFlag = value;
	}

	/*****************************************************************************************/
	public int getModStatus()
	{
		return _modStatus;
	}
		
	public void setModStatus(int value)
	{
		_modStatus = value;
	}
	
	/*
	 * Show me what you've got
	 */
	public String toString()
	{
		String ret = "EGUrlData content (Java)\n";
		ret += "   Mod.  Name=" + _modName + ", status=" + _modStatus + "\n";
		ret += "          URL=" + _egUrl + "\n";
		ret += "  isEvalGuide=" + _egFlag + "\n";
		
		return ret;
	}
}

