/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.dto.intGrid;

import java.util.ArrayList;

import com.pdi.data.abyd.dto.common.DNAStructureDTO;
import com.pdi.data.abyd.dto.common.StatusDTO;
import com.pdi.data.abyd.dto.intGrid.IGFinalScoreDTO;

/**
 * IGSummaryDataDTO is a wrapper around a number of data items that are needed when
 * the Integration Grid app is invoked.
 * 
 * @author		Ken Beukelman
  */
public class IGSummaryDataDTO
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private StatusDTO _status;
	private long _dnaId;
	private String _partId;
	private String _partFname;
	private String _partLname;
	private String _clientName;
	private String _projectName;
	private String _projectId;
	private String _lcFname;
	private String _lcLname;
	private double _mean;
	private double _stdDev;
	private int _rdi;
	private boolean _gpiIncluded;
	private boolean _gpiAndCogDataAvailable;
	private boolean _alpDataAvailable = false;	// not your usual case; default to false
	private boolean _kf4dDataAvailable = false;	// not your usual case; default to false
	private boolean _submitted;
	private String _submittedDate;	// igLockedDate but if null, then lastDate.  Implemented where this is set
	private String _workingNotes;
	private DNAStructureDTO _gridData = null;
	private ArrayList<IGFinalScoreDTO> _compFinData;
	private String _dLCI = "-";
	private String _finalAverage = "-";
	private boolean _filled = false;
	
	private ArrayList<String> _reportCodes = new ArrayList<String>();

	//
	// Constructors.
	//
	public IGSummaryDataDTO()
	{
		// Set up a successful status by default
		_status = new StatusDTO();
	}

	// Presumes failure
	public IGSummaryDataDTO(StatusDTO s)
	{
		// Set up a successful status by default
		_status = s;
	}

	//
	// Instance methods.
	//
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Get/Set Functions                                  *//
	//*                                                                                         *//
	//*******************************************************************************************//

	/*****************************************************************************************/
	public StatusDTO getStatus()
	{
		return _status;
	}
	
	public void setStatus(StatusDTO value)
	{
		_status = value;
	}	

	/*****************************************************************************************/
	public long getDnaId()
	{
		return _dnaId;
	}
	
	public void setDnaId(long value)
	{
		_dnaId = value;
	}	

	/*****************************************************************************************/
	public String getPartId()
	{
		return _partId;
	}
	
	public void setPartId(String value)
	{
		_partId = value;
	}	

	/*****************************************************************************************/
	public String getPartFname()
	{
		return _partFname;
	}
	
	public void setPartFname(String value)
	{
		_partFname = value;
	}	

	/*****************************************************************************************/
	public String getPartLname()
	{
		return _partLname;
	}
	
	public void setPartLname(String value)
	{
		_partLname = value;
	}	

	/*****************************************************************************************/
	public String getClientName()
	{
		return _clientName;
	}
	
	public void setClientName(String value)
	{
		_clientName = value;
	}	

	/*****************************************************************************************/
	public String getProjectName()
	{
		return _projectName;
	}
	
	public void setProjectName(String value)
	{
		_projectName = value;
	}	

	/*****************************************************************************************/
	public String getProjectId()
	{
		return _projectId;
	}
	
	public void setProjectId(String value)
	{
		_projectId = value;
	}	

	/*****************************************************************************************/
	public String getLcFname()
	{
		return _lcFname;
	}
	
	public void setLcFname(String value)
	{
		_lcFname = value;
	}	

	/*****************************************************************************************/
	public String getLcLname()
	{
		return _lcLname;
	}
	
	public void setLcLname(String value)
	{
		_lcLname = value;
	}	

	/*****************************************************************************************/
	public double getMean()
	{
		return _mean;
	}
	
	public void setMean(double value)
	{
		_mean = value;
	}	

	/*****************************************************************************************/
	public double getStdDev()
	{
		return _stdDev;
	}
	
	public void setStdDev(double value)
	{
		_stdDev = value;
	}	

	/*****************************************************************************************/
	public int getRdi()
	{
		return _rdi;
	}
	
	public void setRdi(int value)
	{
		_rdi = value;
	}	

	/*****************************************************************************************/
	public boolean getGpiIncluded()
	{
		return _gpiIncluded;
	}
	
	public void setGpiIncluded(boolean value)
	{
		_gpiIncluded = value;
	}	

	/*****************************************************************************************/
	public boolean getGpiAndCogDataAvailable()
	{
		return _gpiAndCogDataAvailable;
	}
	
	public void setGpiAndCogDataAvailable(boolean value)
	{
		_gpiAndCogDataAvailable = value;
	}	

	/*****************************************************************************************/
	public boolean getAlpDataAvailable()
	{
		return _alpDataAvailable;
	}
	
	public void setAlpDataAvailable(boolean value)
	{
		_alpDataAvailable = value;
	}	

	/*****************************************************************************************/
	public boolean getKf4dDataAvailable()
	{
		return _kf4dDataAvailable;
	}
	
	public void setKf4dDataAvailable(boolean value)
	{
		_kf4dDataAvailable = value;
	}	
	
	/*****************************************************************************************/
	public boolean getSubmitted()
	{
		return _submitted;
	}
	
	public void setSubmitted(boolean value)
	{
		_submitted = value;
	}	

	/*****************************************************************************************/
	public String getSubmittedDate()
	{
		return _submittedDate;
	}
	
	public void setSubmittedDate(String value)
	{
		_submittedDate = value;
	}
	
	/*****************************************************************************************/
	public String getWorkingNotes()
	{
		return _workingNotes;
	}
	
	public void setWorkingNotes(String value)
	{
		_workingNotes = value;
	}	

	/*****************************************************************************************/
	public DNAStructureDTO getGridData()
	{
		if (_gridData == null)
			_gridData = new DNAStructureDTO();
		return _gridData;
	}
	
	public void setGridData(DNAStructureDTO value)
	{
		_gridData = value;
	}	

	/*****************************************************************************************/
	public ArrayList<IGFinalScoreDTO> getCompFinData()
	{
		if (_compFinData == null)
			_compFinData = new ArrayList<IGFinalScoreDTO>();
		return _compFinData;
	}
	
	public void setCompFinData(ArrayList<IGFinalScoreDTO> value)
	{
		_compFinData = value;
	}	

	/*****************************************************************************************/
	public String getDLCI()
	{
		return _dLCI;
	}
	
	public void setDLCI(String value)
	{
		_dLCI = value;
	}	

	/*****************************************************************************************/
	public String getFinalAverage()
	{
		return _finalAverage;
	}
	
	public void setFinalAverage(String value)
	{
		_finalAverage = value;
	}	

	/*****************************************************************************************/
	public boolean isFilled()
	{
		return _filled;
	}
	
	public void setFilled(boolean value)
	{
		_filled = value;
	}	



	/*
	 * Show me what you've got
	 */
	public String toString()
	{
		String ret = "IGSummaryDataDTO content (Java)\n";
		ret += "                DNA ID=" + _dnaId + "\n";
		ret += "        Participant ID=" + _partId + "\n";
		ret += "      Participant Name=" + _partLname + ", " + _partFname + "\n";
		ret += "           Client Name=" + _clientName + "\n";
		ret += "          Project Name=" + _projectName + "\n";
		ret += "          Project Id=" + _projectId + "\n";
		ret += "  Lead Consultant Name=" + _lcLname + ", " + _lcFname + "\n";
		ret += "                  Norm:  Mean=" + _mean + ", StdDev=" + _stdDev + "\n";
		ret += "                   RDI=" + _rdi + "\n";
		ret += "              GPI incl=" + _gpiIncluded + "\n";
		ret += "              GPI & Cog avail=" + _gpiAndCogDataAvailable + "\n";
		ret += "          is Submitted=" + _submitted + "\n";
		ret += "         submittedDate=" + _submittedDate + "\n";
		ret += "         Working Notes=" + _workingNotes + "\n";
		ret += "            Grid Data:\n";
		if (_gridData != null)
		{
			ret += _gridData.toString();
		}
		ret += "         Final Scores:\n";
		if (_compFinData != null)
		{
			for (IGFinalScoreDTO fsd : _compFinData)
			{
				ret += fsd.toString();
			}
		}
		ret += "         Final Score average=" + _finalAverage + "\n";
		ret += "         dLCI=" + _dLCI + "\n";
		ret += "         Report Codes:\n";
		if (_reportCodes != null)
		{
			for (String ss : _reportCodes)
			{
				ret += "          " + ss + "\n";
			}
		}

		return ret;
	}

	public void setReportCodes(ArrayList<String> _reportCodes) {
		this._reportCodes = _reportCodes;
	}

	public ArrayList<String> getReportCodes() {
		return _reportCodes;
	}
}
