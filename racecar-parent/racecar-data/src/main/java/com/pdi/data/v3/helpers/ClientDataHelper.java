/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v3.helpers;

import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.pdi.data.util.RequestStatus;
import com.pdi.data.v3.dto.ClientDTO;
import com.pdi.data.v3.dto.ClientListDTO;
import com.pdi.data.v3.dto.DocumentDTO;
import com.pdi.data.v3.dto.SessionUserDTO;
import com.pdi.data.v3.util.V3WebserviceUtils;
//import com.pdi.properties.PropertyLoader;
import com.pdi.xml.XMLUtils;

/**
 * DataHelper for Client Information
 * 
 * @author		Gavin Myers
 * @author		Ken Beukelman
 */
public class ClientDataHelper
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//

	//
	// Constructors.
	//

	//
	// Instance methods.
	//
	
	/**
	 * Get all clients that a particular user has access to
	 * Test status on client by client basis
	 * @param sessionUser
	 * @return
	 */
	public static ClientListDTO getClients(SessionUserDTO sessionUser)
	{
		DocumentDTO dto = V3WebserviceUtils.request(sessionUser,"client/list","");
		if (! dto.getStatus().isOk())
		{
			return new ClientListDTO(dto.getStatus());
		}
		
		Document response = dto.getDocument();
		NodeList nodes = XMLUtils.getElements(response, "//clientListLine");
	
		ArrayList<ClientDTO> results = new ArrayList<ClientDTO>();
		for(int i = 0; i < nodes.getLength(); i++)
		{
			Node node = nodes.item(i);
			ClientDTO client = new ClientDTO(node);
			results.add(client);
		}
		
		return new ClientListDTO(results);
	}


	/**
	 * Returns the complete client information
	 * NOTE:  If the uri is present, it will be used.  The client id will be ignored. 
	 *        If there is no uri, then the client id will be used. 
	 * @param client a  client with at least the ID set
	 * @return a ClientDTO fully filled in
	 */
	public static ClientDTO getClient(SessionUserDTO sessionUser, ClientDTO client)
	{
		DocumentDTO dto;
		
		if (client.getUri() == null  &&
			client.getId() == null)
		{
			return new ClientDTO(new RequestStatus(RequestStatus.RS_ERROR, "A client ID or a URI is required to fetch a client."));
		}

		if (client.getUri() == null)
		{
			// get the data directly
			dto = V3WebserviceUtils.request(sessionUser,"client/"+client.getId(),"");
		}
		else
		{
			// get the data using the uri
			dto = V3WebserviceUtils.request(sessionUser,client.getUri(),"");
		}

		if (! dto.getStatus().isOk())
		{
			return new ClientDTO(dto.getStatus());
		}
		
		Document response = dto.getDocument();
		NodeList nodes = XMLUtils.getElements(response, "//resultClient");

		ClientDTO newClient = new ClientDTO(nodes.item(0));
		// Preserve the incoming URI (if any)
		if (client.getUri() != null)
		{
			newClient.setUri(client.getUri());		
		}

		return newClient;
	}
}
