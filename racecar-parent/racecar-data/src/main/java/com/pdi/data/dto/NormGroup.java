/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.dto;

import java.util.HashMap;

/**
 * NormGroup is a basic data class that contains norm group data.
 *
 * @author Gavin Myers
 */
public class NormGroup extends CoreDataObject {
	/**
	 *
	 */
	private static final long serialVersionUID = -5281786294164659398L;
	public static int GENERAL_POPULATION = 1;
	public static int SPECIAL_POPULATION = 2;
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//

	private int id;

	// North American Executive
	private String name;

	// instrument to which this Norm Group is attached
	private String instrumentCode;

	private HashMap<String, Norm> norms = new HashMap<String, Norm>(); // String
																		// =
																		// spssValue
	private int population;
	private String code;
	private boolean defaultNormGroup;
	private int sequenceOrder;

	//
	// Constructors.
	//
	public NormGroup() {
		// does nothing right now
	}

	//
	// Instance methods.
	//

	@Override
	public String toString() {
		String ret = "Norm Group - ID=" + id + ", name=" + name;
		for (Norm norm : norms.values()) {
			ret += norm.toString() + "\n";
		}

		return ret;
	}

	// *******************************************************************************************//
	// * *//
	// * Get/Set Functions *//
	// * *//
	// *******************************************************************************************//

	/*****************************************************************************************/
	public void setId(int value) {
		id = value;
	}

	public int getId() {
		return id;
	}

	/*****************************************************************************************/
	public void setName(String value) {
		name = value;
	}

	public String getName() {
		return name;
	}

	/*****************************************************************************************/
	public void setInstrumentCode(String value) {
		instrumentCode = value;
	}

	public String getInstrumentCode() {
		return instrumentCode;
	}

	/*****************************************************************************************/
	public void setPopulation(int population) {
		this.population = population;
	}

	public int getPopulation() {
		return population;
	}

	public boolean hasValidPop() {
		return this.population == GENERAL_POPULATION || this.population == SPECIAL_POPULATION;
	}

	/*****************************************************************************************/
	public void setNorms(HashMap<String, Norm> norms) {
		this.norms = norms;
	}

	public HashMap<String, Norm> getNorms() {
		return norms;
	}

	/*****************************************************************************************/
	public String getCode() {
		if (code == null) {
			return "";
		}
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	/*****************************************************************************************/
	public boolean isDefaultNormGroup() {
		return defaultNormGroup;
	}

	public void setDefaultNormGroup(boolean defaultNormGroup) {
		this.defaultNormGroup = defaultNormGroup;
	}

	/*****************************************************************************************/
	public int getSequenceOrder() {
		return sequenceOrder;
	}

	public void setSequenceOrder(int sequenceOrder) {
		this.sequenceOrder = sequenceOrder;
	}
}
