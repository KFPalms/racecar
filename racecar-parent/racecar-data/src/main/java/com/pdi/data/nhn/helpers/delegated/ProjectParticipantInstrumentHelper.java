/**
 * Copyright (c) 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.nhn.helpers.delegated;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdi.data.dto.Client;
import com.pdi.data.dto.Project;
import com.pdi.data.dto.ProjectParticipantInstrument;
import com.pdi.data.dto.Instrument;
import com.pdi.data.dto.Participant;
import com.pdi.data.dto.Response;
import com.pdi.data.dto.ResponseGroup;
import com.pdi.data.dto.SessionUser;
//import com.pdi.data.helpers.HelperDelegate;
//import com.pdi.data.helpers.interfaces.IClientHelper;
//import com.pdi.data.helpers.interfaces.IProjectHelper;
import com.pdi.data.helpers.interfaces.IProjectParticipantInstrumentHelper;
import com.pdi.data.nhn.util.NhnDatabaseUtils;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.logging.LogWriter;

public class ProjectParticipantInstrumentHelper implements IProjectParticipantInstrumentHelper
{
	private static final Logger log = LoggerFactory.getLogger(ProjectParticipantInstrumentHelper.class);
	//
	// Static data
	//
	
	private static final String SELECT_STATEMENT_PREFIX = "SELECT " +
		"p.project_id, " +		//	1
		"p.name, " +			//	2
		"c.title, " +			//	3
		"c.abbv, " +			//	4
		"c.course, " +			//	5
		"u.users_id, " +		//	6
		"u.email, " +			//	7
		"u.firstname, " +		//	8
		"u.lastname, " +		//	9
		"u.company_id, " +		//	10
		"cm.coname " +			//	11
		"FROM platform.dbo.project_user pu with(nolock) " +
		"INNER JOIN platform.dbo.project p with(nolock) ON pu.project_id = p.project_id " +
		"INNER JOIN platform.dbo.users u with(nolock) on u.users_id = pu.users_id " +
		"INNER JOIN platform.dbo.roster r with(nolock) ON pu.users_id = r.users_id " +
		"INNER JOIN platform.dbo.course c with(nolock) ON r.courseID = c.course " +
		"INNER JOIN platform.dbo.company cm with(nolock) ON u.company_id = cm.company_id ";

	//
	// Static methods
	//
	
	//
	// Instance variables
	//
	
	//
	// Instance Methods
	//
	
	/*
	 * fromProjectIdParticipantId
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IProjectParticipantInstrumentHelper#fromProjectIdParticipantId(com.pdi.data.dto.SessionUser, int, java.lang.String, java.lang.String)
	 */
	//@Override
	public ArrayList<ProjectParticipantInstrument> fromProjectIdParticipantId(SessionUser session,
																			  int mode,
																			  String projectId,
																			  String participantId)
	{
		ArrayList<ProjectParticipantInstrument> epis = new ArrayList<ProjectParticipantInstrument>();
		StringBuffer sb = new StringBuffer();		
		sb.append(SELECT_STATEMENT_PREFIX + "WHERE (pu.project_id = ?) AND (pu.users_id = ?)");
		
		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(sb);
		if (dlps.isInError()) {
			LogWriter.logSQL(LogWriter.ERROR, this,
					"Error preparing fromProjectIdParticipantId SELECT query." +
					"  Data:  " + dlps.toString(),
					sb.toString());
		}
		DataResult dr = null;
		ResultSet rs = null;
		try
		{
			dlps.getPreparedStatement().setString(1, projectId);
			dlps.getPreparedStatement().setString(2, participantId);
			dr = NhnDatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			if (rs != null && rs.isBeforeFirst())			{
				while (rs.next())				{
					ProjectParticipantInstrument epi = getProjectParticipantInstrument(rs);
					epis.add(epi);
				}
				rs.close();			
			}
		}
		catch(java.sql.SQLException se)
		{
			LogWriter.logBasic(LogWriter.ERROR, this,
					"Data fetch and processing error (projID=" +projectId +
					", partID=" + participantId +
					".  Msg=" + se.getMessage());
		}
		//catch (Exception e)
		//{
		//	LogWriter.logBasic(LogWriter.ERROR, this,
		//			"General error (projID=" +projectId +
		//			", partID=" + participantId +
		//			".  Msg=" + e.getMessage());
		//}
		finally
		{
			if (dr != null) {dr.close(); dr = null;}			
		}
		return epis;
	}

	
	/*
	 * fromProjectIdParticipantIdInstrumentId
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IProjectParticipantInstrumentHelper#fromProjectIdParticipantIdInstrumentId(com.pdi.data.dto.SessionUser, int, java.lang.String, java.lang.String, java.lang.String)
	 */
	//@Override
	public ProjectParticipantInstrument fromProjectIdParticipantIdInstrumentId(SessionUser session,
			                                                                   int mode,
			                                                                   String projectId,
			                                                                   String participantId,
			                                                                   String instrumentId, 
			                                                                   String callingProjectId)
	{
		ProjectParticipantInstrument epi = new ProjectParticipantInstrument();
		StringBuffer sb = new StringBuffer();		
		sb.append(SELECT_STATEMENT_PREFIX + "WHERE (pu.project_id = ?) AND (pu.users_id = ?)");
		
		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(sb);
		if (dlps.isInError()) {
			LogWriter.logSQL(LogWriter.ERROR, this,
					"Error preparing fromProjectIdParticipantIdInstrumentId SELECT query." +
					"  Data:  " + dlps.toString(),
					sb.toString());
		}
		DataResult dr = null;
		ResultSet rs = null;
		try
		{
			if(callingProjectId == null){
				//use the usual project Id
				dlps.getPreparedStatement().setString(1, projectId);
			}else{
				dlps.getPreparedStatement().setString(1, callingProjectId);
			}
			
			dlps.getPreparedStatement().setString(2, participantId);
			dr = NhnDatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			if (rs != null && rs.isBeforeFirst())			{
				if (rs.next())
				{
					epi = getProjectParticipantInstrument(rs);
				}
				rs.close();			
			}
		}
		catch(java.sql.SQLException se)
		{
			LogWriter.logBasic(LogWriter.ERROR, this,
					"SQL fetch or processing error." +
					"  ProjID=" + projectId + ",  partID=" + participantId +
					".  Msg=" + se.getMessage());
		}
		//catch (Exception e)
		//{
		//	LogWriter.logBasic(LogWriter.ERROR, this,
		//			"General error." +
		//			"  ProjID=" + projectId + ",  partID=" + participantId +
		//			".  Msg=" + e.getMessage());
		//}
		finally
		{
			if (dr != null) {dr.close(); dr = null;}			
		}
		return epi;
	}


	/*
	 * fromLastParticipantIdInstrumentId
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IProjectParticipantInstrumentHelper#fromLastParticipantIdInstrumentId(com.pdi.data.dto.SessionUser, java.lang.String, java.lang.String)
	 */
	//@Override
	public ProjectParticipantInstrument fromLastParticipantIdInstrumentId(SessionUser session,
																		  String participantId,
																		  String instrumentId)
	{
		ArrayList<ProjectParticipantInstrument> epis = fromParticipantIdInstrumentId(session, participantId, instrumentId);
		if(epis.size() == 0) {
			return null;
		}
		return epis.get(0);
	}


	/*
	 * fromParticipantIdInstrumentId
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IProjectParticipantInstrumentHelper#fromParticipantIdInstrumentId(com.pdi.data.dto.SessionUser, java.lang.String, java.lang.String)
	 */
	//@Override
	public ArrayList<ProjectParticipantInstrument> fromParticipantIdInstrumentId(SessionUser session,
																				 String participantId,
																				 String instrumentId)
	{
		ArrayList<ProjectParticipantInstrument> epis = new ArrayList<ProjectParticipantInstrument>();
		StringBuffer sb = new StringBuffer();		
		sb.append(SELECT_STATEMENT_PREFIX + "WHERE (pu.users_id = ? ) AND (c.abbv = ? )");		
		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(sb);		
		if (dlps.isInError()) {
			LogWriter.logSQL(LogWriter.ERROR, this,
					"Error preparing fromParticipantIdInstrumentId SELECT query." +
					"  Data:  " + dlps.toString(),
					sb.toString());
		}
		DataResult dr = null;
		ResultSet rs = null;
		try
		{
			dlps.getPreparedStatement().setInt(1, Integer.parseInt(participantId));
			dlps.getPreparedStatement().setString(2, instrumentId);
			dr = NhnDatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			if (rs != null && rs.isBeforeFirst())
			{
				while (rs.next())
				{
					//System.out.println(rs.getString("name"));
					ProjectParticipantInstrument epi = getProjectParticipantInstrument(rs);
					epis.add(epi);
				}
				rs.close();			
			}
			//System.out.println("Done with query");
		}
		catch(java.sql.SQLException se)
		{
			LogWriter.logBasic(LogWriter.ERROR, this,
					"SQL fetch or processing error." +
					"  PartID=" + participantId + ", instID=" + instrumentId + 
					".  Msg=" + se.getMessage());
		}
		//catch (Exception e)
		//{
		//	LogWriter.logBasic(LogWriter.ERROR, this,
		//			"General processing error." +
		//			"  PartID=" + participantId + ", instID=" + instrumentId + 
		//			".  Msg=" + e.getMessage());
		//}
		finally
		{
			if (dr != null) {dr.close(); dr = null;}			
		}
		return epis;
	}


	/*
	 * fromParticipantId
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IProjectParticipantInstrumentHelper#fromParticipantId(com.pdi.data.dto.SessionUser, int, java.lang.String)
	 */
	//@Override
	public ArrayList<ProjectParticipantInstrument> fromParticipantId(SessionUser session,
																	 int mode,
																	 String participantId,
																	 String callingProjectId)
	{
		ArrayList<ProjectParticipantInstrument> epis = new ArrayList<ProjectParticipantInstrument>();
		StringBuffer sb = new StringBuffer();		
		sb.append(SELECT_STATEMENT_PREFIX + "WHERE (pu.users_id = ?)");
		sb.append("   AND (pu.project_id = ?)");

		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(sb);
		if (dlps.isInError()) {
			LogWriter.logSQL(LogWriter.ERROR, this,
					"Error preparing fromParticipantId SELECT query." +
					"  Data:  " + dlps.toString(),
					sb.toString());
		}
		DataResult dr = null;
		ResultSet rs = null;
		try		{
			dlps.getPreparedStatement().setString(1, participantId);
			dlps.getPreparedStatement().setString(2, callingProjectId);
			
			//System.out.println("nhn fromParticipantId :  " + sb.toString() +  "partID: " + participantId);
			
			dr = NhnDatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			if (rs != null && rs.isBeforeFirst())
			{
				while (rs.next())
				{
					ProjectParticipantInstrument epi = getProjectParticipantInstrument(rs);	
					//System.out.println("nhn epis: " + epi.getProject().getId() + "  " + epi.getInstrument().getCode() );					
					epis.add(epi);
				}
				rs.close();			
			}
		}
		catch(java.sql.SQLException se)
		{
			LogWriter.logBasic(LogWriter.ERROR, this,
					"SQL fetch or processing error." +
					"  PartID=" + participantId + ", mode=" + mode + 
					".  Msg=" + se.getMessage());
		}
		//catch (Exception e)
		//{
		//	LogWriter.logBasic(LogWriter.ERROR, this,
		//			"General processing error." +
		//			"  PartID=" + participantId + ", mode=" + mode + 
		//			".  Msg=" + e.getMessage());
		//}
		finally 		{
			if (dr != null) {dr.close(); dr = null;}			
		}
		return epis;
	}

	/*
	 * getProjectParticipantInstrument
	 */
	private ProjectParticipantInstrument getProjectParticipantInstrument(ResultSet rs)
	{
		ProjectParticipantInstrument projectParticipantInstrument = new ProjectParticipantInstrument();
		try
		{
			Client c = new Client();
			c.setId(rs.getString(10));
			c.setName(rs.getString(11));
			Project e = new Project();
			e.setClient(c);
			e.setId(rs.getString(1));
			e.setName(rs.getString(2));
			Participant p = new Participant();
			p.setEmail(rs.getString(7));
			p.setFirstName(rs.getString(8));
			p.setLastName(rs.getString(9));
			p.setId(rs.getString(6));
			Instrument i = new Instrument();
			//i.setId(rs.getString(4));
			i.setCode(rs.getString(4));
			i.setDescription(rs.getString(3));
			HashMap<String, String> hm = new HashMap<String, String>();
			hm.put("numericId",rs.getString(5));
			i.setMetadata(hm);
			projectParticipantInstrument.setProject(e);
			projectParticipantInstrument.setParticipant(p);					
			projectParticipantInstrument.setInstrument(i);
			ResponseGroup rg = getResponseGroupByParticipantId(p.getId(), i.getCode());
			projectParticipantInstrument.setResponseGroup(rg);
			//projectParticipantInstrument.getScoreGroup();
		}
		catch(Exception e)
		{
			LogWriter.logBasic(LogWriter.ERROR, this,
					"Error setting up ProjectParticipantInstrument" +
					".  Msg=" + e.getMessage());
		}
		
		return projectParticipantInstrument;		
	}


	/*
	 * getResponseGroupByParticipantId
	 */
	public ResponseGroup getResponseGroupByParticipantId(String participantId, String instrumentId)
	{
		ResponseGroup rg = new ResponseGroup();
		StringBuffer sb = new StringBuffer();
		sb.append("select " +
				"i.trackDate, " +				//	1
				"i.learnerId, " +				//	2
				"i.course, " +					//	3
				"i.activityId, " +			//	4
				"i.interactionId, " +		//	5
				"i.learnerResponse, " +	//	6
				"i.attemptCompletionStatus, " + //	7
				"i.attemptProgressStatus, " +		//	8
				"c.abbv as courseId " +			//	9
				"from scorm.dbo.interaction_view i WITH(NOLOCK) " +
				"join platform.dbo.users u WITH(NOLOCK) on cast(u.users_id as varchar(64)) = i.learnerId " +
				"join platform.dbo.scorm_course_directories scd WITH(NOLOCK) on scd.scorm_root_id = i.activityId " +
				"join platform.dbo.course c WITH(NOLOCK) on c.course = scd.course_id " +
				"where u.users_id = ? and c.abbv = ?");
	
		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(sb);
		if (dlps.isInError())
		{
			LogWriter.logSQL(LogWriter.ERROR, this,
					"Error preparing getResponseGroupByParticipantId SELECT query." +
					"  Data:  " + dlps.toString(),
					sb.toString());
		}
		DataResult dr = null;
		ResultSet rs = null;
		try
		{
			dlps.getPreparedStatement().setString(1, participantId);
			dlps.getPreparedStatement().setString(2, instrumentId);
			dr = NhnDatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			if (rs != null && rs.isBeforeFirst())
			{
				while (rs.next())
				{					
					Response r = new Response();
					r.setValue(rs.getString(6));
					r.setCode(rs.getString(5));
					//System.out.println("code:  " + r.getCode() + "   value:  " + r.getValue());
					rg.getResponses().put(rs.getString(5),r);
					
				}
				rs.close();			
			}
		}
		catch(java.sql.SQLException se)
		{
			LogWriter.logBasic(LogWriter.ERROR, this,
					"SQL fetch or processing error." +
					"  PartID=" + participantId + ", instID=" + instrumentId + 
					".  Msg=" + se.getMessage());
		}
		//catch (Exception e)
		//{
		//	LogWriter.logBasic(LogWriter.ERROR, this,
		//			"General processing error." +
		//			"  PartID=" + participantId + ", instID=" + instrumentId + 
		//			".  Msg=" + e.getMessage());
		//}
		finally
		{
			if (dr != null) {dr.close(); dr = null;}			
		}
		return rg;		
	}


	/*
	 * addUpdate
	 * 
	 *1-22-2013 - boolean added to determine if the update is from the usual scoring routine, 
	 *	or from Test Data (Manual Entry) for billing purposes. Not used in the nhn database, but
	 *  added because required for scoring and reporting
	 *
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IProjectParticipantInstrumentHelper#addUpdate(com.pdi.data.dto.SessionUser, int, com.pdi.data.dto.ProjectParticipantInstrument)
	 * 
	 */
	//@Override
	public void addUpdate(SessionUser session,
						  int mode,
						  ProjectParticipantInstrument epi,
						  boolean updateFromTestDataApp)
	{
				
		//System.out.println("nhn add update... " + epi.getParticipant().getId());
		StringBuffer sb = new StringBuffer();
		sb.append("update platform.dbo.position "
			+ " set completion_status = 2, "
			+ " completion_status_date = getdate(), "
			+ " percent_taken = 100, "
			+ " percent_taken_date = getdate() "
			+ " where users_id = ?" 
			+ " and course = (select course from platform.dbo.course where abbv = ?)");
		
		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(sb);
		if (dlps.isInError())
		{
			LogWriter.logSQL(LogWriter.ERROR, this,
					"Error preparing getResponseGroupByParticipantId SELECT query." +
					"  Data:  " + dlps.toString(),
					sb.toString());
		}
		DataResult dr = null;
		//ResultSet rs = null;
		try
		{
			//System.out.println("sb = " + sb.toString() + "part, inst  : " + epi.getParticipant().getId() + "  " + epi.getInstrument().getCode());
			dlps.getPreparedStatement().setString(1, epi.getParticipant().getId());
			dlps.getPreparedStatement().setString(2, epi.getInstrument().getCode());
			dr = NhnDatabaseUtils.update(dlps);
		}
		catch(java.sql.SQLException se)
		{
			LogWriter.logBasic(LogWriter.ERROR, this,
					"SQL fetch or processing error." +
					"  PartID=" + epi.getParticipant().getId() + ", instID=" + epi.getInstrument().getMetadata().get("nhnInstId") + 
					".  Msg=" + se.getMessage());
		}
		finally
		{
			if (dr != null) {dr.close(); dr = null;}			
		}
	}


	/*
	 * fromLastParticipantId
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IProjectParticipantInstrumentHelper#fromLastParticipantId(com.pdi.data.dto.SessionUser, int, java.lang.String)
	 */
	//@Override
	public ArrayList<ProjectParticipantInstrument> fromLastParticipantId(SessionUser session,
																		 int mode,
																		 String participantId, 
																		 String callingProjectId)
	{
		
		return fromParticipantId(session, mode, participantId, callingProjectId);
	}


	/*
	 * addUpdate
	 *
	 *	 *1-22-2013 - boolean added to determine if the update is from the usual scoring routine, 
	 *	or from Test Data (Manual Entry) for billing purposes. Not used in the nhn database, but
	 *  added because required for scoring and reporting
	 *
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IProjectParticipantInstrumentHelper#addUpdate(com.pdi.data.dto.SessionUser, int, java.util.ArrayList)
	 */
	//@Override
	public void addUpdate(SessionUser session,
						  int mode,
						  ArrayList<ProjectParticipantInstrument> epis,
						  boolean updateFromTestDataApp)
	{
		for(ProjectParticipantInstrument ppi : epis) {
			addUpdate(session, mode, ppi, updateFromTestDataApp);
		}
	}

	
	/*
	 * fromParticipantListInstrumentId
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IProjectParticipantInstrumentHelper#fromParticipantListInstrumentId(com.pdi.data.dto.SessionUser, java.util.ArrayList, java.lang.String)
	 */
	//@Override
	public ArrayList<ProjectParticipantInstrument> fromParticipantListInstrumentId(
							SessionUser session,
							ArrayList<Participant> participants,
							String instrumentId)
	{
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * Used in Test Data, for AbyD type projects.
	 * For AbyD, need to get all instruments available, even if there
	 * is no participant data for that instrument.
	 * This method gets the Instrument, and also gets the ScoreGroup
	 * data for the unfinished PPI passed in, and returns the "completed"
	 * PPI with no data, but all scales/norms for that instrument.
	 * 
	 * @param  ProjectParticipantInstrument ppi
	 * @return ProjectParticipantInstrument	  
	 */
	public ProjectParticipantInstrument getPPIWithoutScoredData(SessionUser session, Project proj, Participant part, String instId) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * fromProjectId
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IProjectParticipantInstrumentHelper#fromProjectId(com.pdi.data.dto.SessionUser, int, java.lang.String)
	 */
	//@Override
	public ArrayList<ProjectParticipantInstrument> fromProjectId(SessionUser session,
																 int mode,
																 String projectId)
	{
			ArrayList<ProjectParticipantInstrument> epis = new ArrayList<ProjectParticipantInstrument>();
			StringBuffer sb = new StringBuffer();		
			sb.append(SELECT_STATEMENT_PREFIX + "WHERE (pu.project_id = ?) ");
			
			DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(sb);
			if (dlps.isInError()) {
				LogWriter.logSQL(LogWriter.ERROR, this,
						"Error preparing fromProjectId SELECT query." +
						"  Data:  " + dlps.toString(),
						sb.toString());
			}
			DataResult dr = null;
			ResultSet rs = null;
			try
			{
				dlps.getPreparedStatement().setString(1, projectId);
				dr = NhnDatabaseUtils.select(dlps);
				rs = dr.getResultSet();
				if (rs != null && rs.isBeforeFirst())
				{
					while (rs.next())
					{
						ProjectParticipantInstrument epi = getProjectParticipantInstrument(rs);
						epis.add(epi);
					}
					rs.close();			
				}
			}
			catch(java.sql.SQLException se)
			{
				LogWriter.logBasic(LogWriter.ERROR, this,
						"SQL fetch or processing error." +
						"  ProjID=" + projectId + ", mode=" + mode + 
						".  Msg=" + se.getMessage());
			}
			//catch (Exception e)
			//{
			//	LogWriter.logBasic(LogWriter.ERROR, this,
			//			"General processing error." +
			//			"  ProjID=" + projectId + ", mode=" + mode + 
			//			".  Msg=" + e.getMessage());
			//}
			finally
			{
				if (dr != null) {dr.close(); dr = null;}			
			}
			return epis;
	}
	
	
	/**
	 * Return all instruments.
	 * Return the most recent instrument
	 * if there is a duplicate instrument in the list.
	 * 
	 * Note that the "last" instrument may be from a
	 * different project then the other instruments.  In
	 * fact, if you have various instruments from multiple
	 * projects, the latest may all be from different 
	 * projects.  You really just don't know until
	 * you get them.
	 * 
	 * NHN 1738: new use case, with participant in two
	 * different AbyD projects, so different sets of norms
	 * 
	 * * usecase: I the most recent GPI for this participant, but he
	 * is in two different projects, with different norms.  I want the
	 * norms for the current project.
	 * @param SessionUser session
	 * @param int mode
	 * @param String particpantId
	 * @param String callingProjectId
	 * @return ArrayList<ProjectParticipantInstrument> ppiList
	 */
	public ArrayList<ProjectParticipantInstrument> fromLastParticipantIdIncludeProjectIdForNorms
							(SessionUser session,
							 int mode, 
							 String participantId,
							 String callingProjectId){
		// TODO Auto-generated method stub
		return null;
	}
}
