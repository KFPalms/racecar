package com.pdi.data.nhn.ut;

import junit.framework.TestCase;

import com.pdi.properties.PropertyLoader;
import com.pdi.ut.UnitTestUtils;

public class NhnScoreDataHelperUT extends TestCase
{
	public NhnScoreDataHelperUT(String name)
	{
		super(name);
	}

	public void testProperties()
	{
		UnitTestUtils.start(this);
		
		//Does the local property work?
		String applicationId = PropertyLoader.getProperty("com.pdi.data.nhn.application", "application.code");
		assertEquals(applicationId != null, true);
		assertEquals(applicationId.equals("PDI-DATA-CHECKBOX"), true);
		
		UnitTestUtils.stop(this);
	}
//
//	public void testNhnScore()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		
//		//test user 1
//		String partId = "HOWYDDSZ";
//		String projId = "KBBGZZBU";
//		String modId = "HNZYMHDV";
//		
//		// raven's b ?projectId=KBBGZZBU&participantId=HOWYDDSZ&instrumentId=HNZYMHDV
//		IndividualReport ir = NhnScoreDataHelper.fetchNhnData(partId, projId, modId);
//
//		// watson e
//		//ir = NhnScoreDataHelper.fetchNhnData(partId, projId, "ORG_WG_1.SCO_WG_1");
//
//		// Debug dump
//		System.out.println(ir.toString());
//		
//		UnitTestUtils.stop(this);
//	}

}
