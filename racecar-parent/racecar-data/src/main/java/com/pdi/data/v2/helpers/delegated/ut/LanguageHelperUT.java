/**
 * Copyright (c) 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.v2.helpers.delegated.ut;

import java.util.ArrayList;

import junit.framework.TestCase;

import com.pdi.data.dto.Language;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.ILanguageHelper;
import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.properties.PropertyLoader;
import com.pdi.ut.UnitTestUtils;

public class LanguageHelperUT extends TestCase
{
	public LanguageHelperUT(String name)
	{
		super(name);
	}	

	/*
	 * testFromId
	 */
	public void testFromId()
		throws Exception
	{
		UnitTestUtils.start(this);
		V2DatabaseUtils.setUnitTest(true);

		ILanguageHelper helper = HelperDelegate.getLanguageHelper(PropertyLoader.getProperty("com.pdi.data.v2.application","datasource.v2"));
		
		// Get a TextGroup object
		Language l =  helper.fromId(null, "7");
		System.out.println(l.toString());

		UnitTestUtils.stop(this);
	}


	/*
	 * testFromCode
	 */
	public void testFromCode()
		throws Exception
	{
		UnitTestUtils.start(this);
		V2DatabaseUtils.setUnitTest(true);

		ILanguageHelper helper = HelperDelegate.getLanguageHelper(PropertyLoader.getProperty("com.pdi.data.v2.application","datasource.v2"));
		
		// Get a TextGroup object
		Language l =  helper.fromCode(null, "nl");
		System.out.println(l.toString());

		UnitTestUtils.stop(this);
	}


	/*
	 * testAll
	 */
	public void testAll()
		throws Exception
	{
		UnitTestUtils.start(this);
		V2DatabaseUtils.setUnitTest(true);

		ILanguageHelper helper = HelperDelegate.getLanguageHelper(PropertyLoader.getProperty("com.pdi.data.v2.application","datasource.v2"));
		
		// Get a TextGroup object
		ArrayList<Language> ll =  helper.all(null);
		
		System.out.println("Retrieved " + ll.size() + " languages...");
		for(Language l : ll)
		{
			System.out.println(l.toString());
		}

		UnitTestUtils.stop(this);
	}

}
