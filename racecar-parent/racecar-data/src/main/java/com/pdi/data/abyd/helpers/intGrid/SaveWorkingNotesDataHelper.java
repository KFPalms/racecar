/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.data.abyd.helpers.intGrid;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * SaveWorkingNotesDataHelper contains the code needed to save the working notes
 * assiciated with the indicated Integration Grid.
 *
 * @author		Ken Beukelman
 */
public class SaveWorkingNotesDataHelper
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private Connection con;
	private String participantId;
	private long dnaId;
	private String workingNotes;

	//
	//
	// Constructors.
	//
	public SaveWorkingNotesDataHelper(Connection con, String candId, long dnaId, String workingNotes)
		throws Exception
	{
		// Check key data
		if (candId == null || candId.length() == 0  ||
			dnaId	== 0)
		{
			throw new Exception("Invalid Working Notes Data key:  part=" +  (candId == null ? "null" : (candId.length() == 0 ? "<empty string>" : candId)) +
					            ", dna=" + dnaId);
		}

		this.con = con;
		this.participantId = candId;
		this.dnaId = dnaId;
		this.workingNotes = workingNotes;
	}

	//
	// Instance methods.
	//

	/**
	 * Insert or update the working notes.  Returns nothing.
	 *
	 * @throws Exception
	 */
	public void saveWorkingNotes()
		throws Exception
	{
		// Change any double quotes to single quotes
		// This prevents "Bad SQL" messages for strings that include double quotes
		// also, if it's an empty string, just null it
		if (this.workingNotes != null)
		{
			this.workingNotes = this.workingNotes.trim().replace("'","''");
			if (this.workingNotes.length() == 0)
			{
				this.workingNotes = null;
			}
		}

		Statement stmt = null;

		// There probably should be one, but this is general purpose
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("INSERT INTO pdi_abd_igrid_response ");
		sqlQuery.append("  (participantId, dnaId, workingNotes, lastUser, lastDate) ");
		sqlQuery.append("  VALUES(" + this.participantId + ", ");
		sqlQuery.append("         " + this.dnaId + ", ");
		sqlQuery.append("         " + (this.workingNotes == null ? "null" : "N'" + this.workingNotes + "'") + ", ");
		sqlQuery.append("         SYSTEM_USER, ");
		sqlQuery.append("         GETDATE() )");

		try
		{
			// Do the insert
			stmt = this.con.createStatement();
			stmt.executeUpdate(sqlQuery.toString());
		}
		catch (SQLException ex)
		{
			// TODO Refactor so that the update does not depend upon the error code (do a query)
			// Duplicate key error triggers update; error code = 1062 on MySQL, 2627 on SQL Server
			if (ex.getErrorCode() == 2627)
			{
				// It's already there... update it
				sqlQuery.setLength(0);
				sqlQuery.append("UPDATE pdi_abd_igrid_response ");
				sqlQuery.append("  SET workingNotes = " + (this.workingNotes == null ? "null" : "N'" + this.workingNotes + "'") + ", ");
				sqlQuery.append("      lastUser = SYSTEM_USER, ");
				sqlQuery.append("      lastDate = GETDATE() ");
				sqlQuery.append("  WHERE participantId = " + this.participantId + " ");
				sqlQuery.append("    AND dnaId = " + this.dnaId);

				try
				{
					stmt.executeUpdate(sqlQuery.toString());
				}
				catch (SQLException ex2)
				{
					throw new Exception("SQL saveWorkingNotes (update).  " +
							"SQLException: " + ex2.getMessage() + ", " +
							"SQLState: " + ex2.getSQLState() + ", " +
							"VendorError: " + ex2.getErrorCode());
				}
			}
			else
			{
				throw new Exception("SQL saveWorkingNotes (insert).  " +
									"SQLException: " + ex.getMessage() + ", " +
									"SQLState: " + ex.getSQLState() + ", " +
									"VendorError: " + ex.getErrorCode());
			}
		}
		finally
		{
			if (stmt != null)
			{
				try  {  stmt.close();  }
				catch (SQLException sqlEx)
				{
			    	System.out.println("ERROR: stmt close in saveScore - " + sqlEx.getMessage());	
				}
				stmt = null;
			}
		}
	}
}
