package com.pdi.data.v2.ut;

//import java.util.ArrayList;

//import com.pdi.data.v2.dto.ClientDTO;
import com.pdi.data.v2.dto.SessionUserDTO;
//import com.pdi.data.v2.helpers.ClientDataHelper;
import com.pdi.data.v2.util.V2WebserviceUtils;
import com.pdi.properties.PropertyLoader;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;


public class ClientDataHelperUT extends TestCase
{
	public ClientDataHelperUT(String name)
	{
		super(name);
	}
	

	
	public void testGetClients() 
		throws Exception {
		
		UnitTestUtils.start(this);
		
		String username = "KWIDJSKL";
		String password = "WKEIDSLA";
		V2WebserviceUtils utils = new V2WebserviceUtils();
		SessionUserDTO sessionDto = utils.registerUser(username, password);
		
	    String server = PropertyLoader.getProperty("com.pdi.data.v2.application", "webservice.path");
	    String answerXML = "<Answers></Answers>";
	    String modLoadVs = "3.00.38";
	    
	    String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
	    xml += "<ETR SessionId=\"${sessionId}\">";
	    xml += "<Rqt Cmd=\"000\" UN=\"${username}\" Pw=\"${password}\" URL=\""+server+"\" />";
	    xml += "<Rqt Cmd=\"Write\" TN=\"Answers\" Mode=\"Append\" Sub=\"Answers\">";
	    xml += "<FldToRtn FN=\"UniqueIdStamp\" Fmt=\"V\" />";
	    xml += "<FldToRtn FN=\"UniqueIdStamp\" Fmt=\"A\" />";
	    xml += "<FldToRtn FN=\"DateCreatedStamp\" Fmt=\"A\" />";
	    xml += "<FldToRtn FN=\"CreatedByStamp\" Fmt=\"A\" />";
	    xml += "<FldToRtn FN=\"DateModifiedStamp\" Fmt=\"A\" />";
	    xml += "<FldToRtn FN=\"ModifiedByStamp\" Fmt=\"A\" />";
	    xml += "<FldToRtn FN=\"ModifiedByStamp\" Fmt=\"A\" />";
	    xml += "<FldToRtn FN=\"ModifiedByStamp\" Fmt=\"A\" />";
	    xml += "<FldToRtn FN=\"UniqueVsStamp\" Fmt=\"A\" />";
	    xml += "<Fld FN=\"modLoadVs\">${modLoadVs}</Fld>";
	    xml += "<Fld FN=\"Completed\">0</Fld>";
	    xml += "<Fld FN=\"ModuleId\">${modId}</Fld>";
	    xml += "<Fld FN=\"CandidateId\">${candId}</Fld>";
	    xml += "<Fld FN=\"JobId\">${jobId}</Fld>";
	    xml += "<Fld FN=\"XML_Answers\">${answerXML}</Fld>";
	    xml += "</Rqt>";
	    xml += "</ETR>";
	    
	    xml = xml.replace("${username}", "Test");
	    xml = xml.replace("${password}", password);
	    xml = xml.replace("${modLoadVs}", modLoadVs);
	    xml = xml.replace("${answerXML}", answerXML);
	    xml = xml.replace("${modId}", "HMWIJBYN");
	    xml = xml.replace("${candId}", "HRJTGSFR");
	    xml = xml.replace("${jobId}", "HOAHKYTD");
	    xml = xml.replace("${sessionId}", sessionDto.getSessionId());
	    //Document doc = sendData(xml);
	    utils.sendData(xml);

		UnitTestUtils.stop(this);
	}
	
}
