package com.kf.racecar.ejb.storage;

import java.io.File;
import java.io.StringWriter;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.commons.io.IOUtils;

import com.kf.palms.common.dto.ErrorObj;
import com.kf.palms.common.dto.ResultObj;
import com.kf.palms.common.dto.UpdateReportRequestResultObj;
import com.kf.palms.web.reporting.GeneratedReportData;
import com.kf.palms.web.reporting.ReportRequestHelper;
import com.kf.palms.web.reporting.jaxb.ReportRepresentation;
import com.kf.palms.web.rest.RestTemplate;
//import com.kf.palms.web.rest.security.APICredentials;
import com.kf.palms.web.rest.security.APICredentials;

@Stateless
@LocalBean
public class ReportAPIServiceImpl implements ReportAPIServiceLocal {
	
	final static Logger log = LoggerFactory.getLogger(ReportAPIServiceImpl.class);

	private ReportRequestHelper reportRequestHelper;
	
	@Resource(mappedName = "java:global/application/config_properties")
	private Properties adminConfigProperties;
	
	@PostConstruct
	private void init(){
		APICredentials creds = new APICredentials(
				adminConfigProperties.getProperty("PALMS_APISecretKey","b0d4b7a7-d3a6-47ac-b5f0-e461b5d6a028"), 
				adminConfigProperties.getProperty("PALMS_APIPartnerKey", "f65b01da-7ed3-4338-b09d-c675913073bb"), 
				adminConfigProperties.getProperty("PALMS_APIPartnerKeyVersion", "1.0"), 
				adminConfigProperties.getProperty("PALMS_APIToken", "Reports"), 
				adminConfigProperties.getProperty("PALMS_APITokenType", "PALMS"));
		RestTemplate restTemplate = new RestTemplate(creds, false);
		reportRequestHelper = new ReportRequestHelper(adminConfigProperties.getProperty("palmsRestApiHost"), restTemplate, 0, "racecar", false);
	}

	@Override
	public boolean updateRequestStatusProcessing(int reportRequestId) {
		if (reportRequestHelper.updateReportRequest(reportRequestId, "PROCESSING")){
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void updateRequestStatusError(int reportRequestId, String errorMessage, String errorCode) {
		ReportRepresentation rep = new ReportRepresentation();
		rep.setRequestId(reportRequestId);
		rep.setStatus("ERROR");
		rep.setErrorMessage(errorMessage);
		rep.setErrorCode(errorCode);
		ResultObj result = reportRequestHelper.updateReportRequest(rep);
		if (result instanceof UpdateReportRequestResultObj){
			log.debug("Got successful result: {}", result.getStatus());
		} else {
			ErrorObj obj = (ErrorObj) result;
			log.debug("Got error result: {}", obj.getErrorMsg());
		}
	}

	@Override
	public ReportRepresentation uploadReport(File reportFile, int reportRequestId) {
		ReportRepresentation rptRep = null;
		rptRep = reportRequestHelper.uploadReportFile(reportRequestId, reportFile);

		
		return rptRep;
	}

	@Override
	public String retrieveReportContentAsString(int reportRequestId) throws Exception {
		GeneratedReportData grd = reportRequestHelper.downloadReportFile(reportRequestId);
		log.debug("Got response from download report of type: {}, and size: {}", grd.getContentType(), grd.getContentLength());
		StringWriter writer = new StringWriter();
		
		IOUtils.copy(grd.getInSream(), writer, "UTF-8");
		
		return writer.toString();
	}

}
