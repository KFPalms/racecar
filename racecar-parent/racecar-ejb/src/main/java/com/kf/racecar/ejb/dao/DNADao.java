package com.kf.racecar.ejb.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.kf.racecar.ejb.entity.DNA;

@Stateless
@LocalBean
public class DNADao extends GenericDao<DNA, Integer> {


	public DNA findDnaByIdBatchHints(int dnaId){
		TypedQuery<DNA> query = getEntityManager().createNamedQuery("findDnaByIdBatchHints", DNA.class);
		query.setParameter("dnaId", dnaId);
		try{
			return query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}
	
	public DNA findDnaTemplateByModelId(int modelId){
		TypedQuery<DNA> query = getEntityManager().createNamedQuery("findDnaTemplateByModelId", DNA.class);
		query.setParameter("modelId", modelId);
		try{
			return query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}
}
