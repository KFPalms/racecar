package com.kf.racecar.ejb.alr;

import javax.ejb.Local;

import org.w3c.dom.Document;

@Local
public interface GroupExtractServiceLocal {

	public void generateGroupExtract(Document xml, int reportRequestId);
}
