package com.kf.racecar.ejb.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.kf.racecar.ejb.entity.Model;

@Stateless
@LocalBean
public class ModelDao extends GenericDao<Model, Integer> {

	public Model getModelByIdBatchChildren(int modelId){
		TypedQuery<Model> query = getEntityManager().createNamedQuery("getModelByIdBatchChildren", Model.class);
		query.setParameter("id", modelId);
		try{
			return query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}
}
