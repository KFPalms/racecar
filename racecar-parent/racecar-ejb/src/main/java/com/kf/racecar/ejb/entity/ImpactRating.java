package com.kf.racecar.ejb.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.kf.palms.jaxb.TimestampXmlAdapter;
@Entity
@Table(name = "pdi_abd_eg_ir")
@XmlRootElement(name = "impact-rating")
@XmlAccessorType(XmlAccessType.FIELD)
public class ImpactRating implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	@XmlTransient
	private ImpactRatingPK id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "modelId")
	@XmlTransient
	private Model model;
	
	@Column(name = "moduleId")
	@XmlAttribute
	private int moduleId;
	
	@Column(name = "irSequence")
	@XmlAttribute
	private int irSequence;
	
	@Column(name = "lastUserId")
	@XmlAttribute
	private String lastUserId;
	
	@Column(name = "lastDate")
	@XmlAttribute
	@XmlJavaTypeAdapter(TimestampXmlAdapter.class)
	private Timestamp lastDate;
	
	@Column(name = "active")
	@XmlAttribute
	private boolean active;
	
	@ManyToOne
	@JoinColumn(name = "textId")
	private Text text;

	public ImpactRatingPK getId() {
		return id;
	}

	public void setId(ImpactRatingPK id) {
		this.id = id;
	}

	public Model getModel() {
		return model;
	}

	public void setModel(Model model) {
		this.model = model;
	}

	public int getModuleId() {
		return moduleId;
	}

	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}

	public int getIrSequence() {
		return irSequence;
	}

	public void setIrSequence(int irSequence) {
		this.irSequence = irSequence;
	}

	public String getLastUserId() {
		return lastUserId;
	}

	public void setLastUserId(String lastUserId) {
		this.lastUserId = lastUserId;
	}

	public Timestamp getLastDate() {
		return lastDate;
	}

	public void setLastDate(Timestamp lastDate) {
		this.lastDate = lastDate;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Text getText() {
		return text;
	}

	public void setText(Text text) {
		this.text = text;
	}

}
