package com.kf.racecar.ejb.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kf.racecar.ejb.dao.DNADao;
import com.kf.racecar.ejb.dao.ModelDao;
import com.kf.racecar.ejb.dao.TextDao;
import com.kf.racecar.ejb.entity.BAR;
import com.kf.racecar.ejb.entity.DNA;
import com.kf.racecar.ejb.entity.ImpactRating;
import com.kf.racecar.ejb.entity.Model;
import com.kf.racecar.ejb.entity.ModelCompetency;
import com.kf.racecar.ejb.entity.SpssMap;
import com.kf.racecar.ejb.entity.Text;
import com.kf.racecar.ejb.singleton.RacecarUtilSingleton;

@Stateless
@LocalBean
public class ModelServiceImpl implements ModelServiceLocal {
	
	private static final Logger log = LoggerFactory.getLogger(ModelServiceImpl.class);
	
	@EJB
	ModelDao modelDao;
	
	@EJB
	DNADao dnaDao;
	
	@EJB
	TextDao textDao;
	
	@EJB
	RacecarUtilSingleton racecarUtilSingleton;

/**
 * Warning, this doesn't work
 */
	@Override
	public int copyModel(int modelId, String username) throws Exception {
		Model srcModel = modelDao.getModelByIdBatchChildren(modelId);
		String lastUser = null;
		if (username == null){
			lastUser = "apiUser";
		} else {
			lastUser = username;
		}
		
		if (srcModel == null){
			throw new Exception("Invalid Model Id");
		}
		Model newModel = new Model();
		newModel.setActive(false);
		newModel.setTargetLevelTypeId(srcModel.getTargetLevelTypeId());
		newModel.setAllowCso(srcModel.isAllowCso());
		String abbvInternalName = StringUtils.abbreviate(srcModel.getInternalName(), 40);
		//column width = 50.  make sure it will fit
		newModel.setInternalName("Copy of (" + abbvInternalName + ")");
		
		Model existingCopy = racecarUtilSingleton.getModelByName(newModel.getInternalName());
		if (existingCopy != null){
			throw new Exception("Model name: " + newModel.getInternalName() + " already exists");
		}
		
		newModel.setLastDate(new Timestamp(new Date().getTime()));
		newModel.setLastUserId(lastUser);
		newModel.setModelSequence(racecarUtilSingleton.getMaxModelSequence() + 1);
		newModel.setReadinessAssessmentModel(srcModel.isReadinessAssessmentModel());

		
		Text externalName = new Text();
		externalName.setDefault(true);
		externalName.setLanguageId(1);
		externalName.setActive(true);
		externalName.setText(newModel.getInternalName());
		externalName.setLastUserId(lastUser);
		externalName.setLastDate(new Timestamp(new Date().getTime()));
		
		externalName = textDao.create(externalName);
		externalName.setTextId(externalName.getId());
		textDao.update(externalName);
		
		newModel.setExternalName(externalName);
		
		
		log.debug("Building competency array...");
		newModel.setCompetencies(new ArrayList<ModelCompetency>());
		
		for (ModelCompetency mc : srcModel.getCompetencies()){
			ModelCompetency newMc = new ModelCompetency();
			newMc.setModel(newModel);
			newMc.setCompetency(mc.getCompetency());
			newMc.setLastDate(new Timestamp(new Date().getTime()));
			newMc.setLastUserId(lastUser);
			newMc.setTextId(mc.getTextId());
			newModel.getCompetencies().add(newMc);
		}
		log.debug("Created {} ModelCompetency entities. Building Spss Maps...", newModel.getCompetencies().size());
		newModel.setSpssMaps(new ArrayList<SpssMap>());
		
		for (SpssMap spssMap : srcModel.getSpssMaps()){
			SpssMap newSpssMap = new SpssMap();
			newSpssMap.setModel(newModel);
			newSpssMap.setCompetency(spssMap.getCompetency());
			newSpssMap.setActive(spssMap.isActive());
			newSpssMap.setInverted(spssMap.isInverted());
			newSpssMap.setSpssId(spssMap.getSpssId());
			
			newModel.getSpssMaps().add(newSpssMap);
		}
		log.debug("Created {} SpssMap entities.  Building BAR array...", newModel.getSpssMaps().size());
		newModel.setBars(new ArrayList<BAR>());
		
		for (BAR bar : srcModel.getBars()){
			BAR newBar = new BAR();
			newBar.setModel(newModel);
			newBar.setModuleId(bar.getModuleId());
			newBar.setCompetency(bar.getCompetency());
			//log.debug("Setting bar sequence: {}", bar.getBarSequence());
			newBar.setBarSequence(bar.getBarSequence());
			//log.debug("newBar bar sequence: {}", newBar.getBarSequence());
			newBar.setExtBehaviorId(bar.getExtBehaviorId());
			newBar.setHiExText(bar.getHiExText());
			newBar.setHiText(bar.getHiText());
			newBar.setLastDate(new Timestamp(new Date().getTime()));
			newBar.setLastUserId(lastUser);
			newBar.setLoExText(bar.getLoExText());
			newBar.setLoText(bar.getLoText());
			newBar.setMedExText(bar.getMedExText());
			newBar.setMedText(bar.getMedText());
			newBar.setModuleId(bar.getModuleId());
			newBar.setActive(bar.isActive());
			
			newModel.getBars().add(newBar);
		}
		log.debug("Created {} BAR entities.  Building Impact Ratings array...", newModel.getBars().size());
		newModel.setImpactRatings(new ArrayList<ImpactRating>());
		
		for (ImpactRating impactRating : srcModel.getImpactRatings()){
			ImpactRating newImpactRating = new ImpactRating();
			newImpactRating.setModel(newModel);
			newImpactRating.setActive(impactRating.isActive());
			newImpactRating.setIrSequence(impactRating.getIrSequence());
			newImpactRating.setLastDate(new Timestamp(new Date().getTime()));
			newImpactRating.setLastUserId(lastUser);
			newImpactRating.setModuleId(impactRating.getModuleId());
			newImpactRating.setText(impactRating.getText());

			newModel.getImpactRatings().add(newImpactRating);
			
		}
		log.debug("Created {} ImpactRating entities.  About to persist new model: {}...", 
				newModel.getImpactRatings().size(), newModel.getInternalName());
		
		newModel = modelDao.create(newModel);
		
		DNA templateDNA = new DNA();
		templateDNA.setActive(true);
		templateDNA.setDnaName("DNA Template for " + newModel.getInternalName());
		templateDNA.setProjectId(0);
		templateDNA.setModel(newModel);
		
		//not used for now
		templateDNA.setcMgrName("Nate Wondra");
		templateDNA.setcMgrEmail("Nate.Wondra@kornferry.com");
		
		templateDNA.setSubmitted(false);
		templateDNA.setSyntheticMean(new BigDecimal(0.00000));
		templateDNA.setSyntheticStandardDeviation(new BigDecimal(0.00000));
		templateDNA.setWorkbookVersion("000");
		templateDNA.setLastUserId(lastUser);
		templateDNA.setDnaNotes(templateDNA.getDnaName() + " model");
		templateDNA.setLastDate(new Timestamp(new Date().getTime()));
		
		templateDNA = dnaDao.create(templateDNA);
		
		newModel.setDnaTemplateId(templateDNA.getDnaId());
		
		modelDao.update(newModel);
		
		//modelDao.getEntityManager().getTransaction().commit();
		
		log.debug("Created new model id {} named: {}", newModel.getInternalName(), newModel.getModelId());
		return newModel.getModelId();
	}

}
