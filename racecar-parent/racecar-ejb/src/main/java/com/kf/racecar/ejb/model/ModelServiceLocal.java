package com.kf.racecar.ejb.model;

import javax.ejb.Local;

@Local
public interface ModelServiceLocal {

	public int copyModel(int modelId, String username) throws Exception;
}
