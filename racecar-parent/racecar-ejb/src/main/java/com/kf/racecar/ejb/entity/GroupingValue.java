package com.kf.racecar.ejb.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.kf.palms.jaxb.TimestampXmlAdapter;

@Entity
@Table(name = "pdi_abd_dna_grouping_value")
@XmlRootElement(name = "grouping-value")
@XmlAccessorType(XmlAccessType.FIELD)
public class GroupingValue implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "groupId")
	@XmlAttribute
	private int groupId;
	
	@ManyToOne
	@JoinColumn(name = "textId")
	private Text groupName;
	
	@Column(name = "active")
	@XmlAttribute
	private boolean active;
	
	@Column(name = "lastUserId")
	@XmlTransient
	private String lastUserId;
	
	@Column(name = "lastDate")
	@XmlAttribute
	@XmlJavaTypeAdapter(TimestampXmlAdapter.class)
	private Timestamp lastDate;

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public Text getGroupName() {
		return groupName;
	}

	public void setGroupName(Text groupName) {
		this.groupName = groupName;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getLastUserId() {
		return lastUserId;
	}

	public void setLastUserId(String lastUserId) {
		this.lastUserId = lastUserId;
	}

	public Timestamp getLastDate() {
		return lastDate;
	}

	public void setLastDate(Timestamp lastDate) {
		this.lastDate = lastDate;
	}

}
