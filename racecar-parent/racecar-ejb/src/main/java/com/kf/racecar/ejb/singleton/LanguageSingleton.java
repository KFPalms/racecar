package com.kf.racecar.ejb.singleton;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import com.pdi.data.dto.Language;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.ILanguageHelper;
import com.pdi.properties.PropertyLoader;

@Startup
@Singleton
public class LanguageSingleton {
	
	private ArrayList<Language> languageList;
	
	@PostConstruct
	private void init(){
		languageList = new ArrayList<Language>();
		try {
			ILanguageHelper lh = HelperDelegate
					.getLanguageHelper(PropertyLoader.getProperty("com.pdi.data.v2.application", "datasource.v2"));
			languageList = lh.all(new SessionUser());
		} catch (Exception e) {
			
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String getLanguageIdByCode(String langCode){
		for (Language lang : languageList){
			if (lang.getCode().equals(langCode)){
				return lang.getId();
			}
		}
		//return english if no matching code is found
		return "1";
	}
	
	public String getLanugageCodeById(int languageId){
		for (Language lang : languageList){
			if (Integer.valueOf(lang.getId()) == languageId){
				return lang.getCode();
			}
		}
		return "en";
	}
	
	public Language getLanguageById(String id){
		for (Language lang : languageList){
			if (lang.getId().equals(id)){
				return lang;
			}
		}
		return getLanguageByCode("en");
	}
	
	public Language getLanguageByCode(String code){
		for (Language lang : languageList){
			if (lang.getCode().equals(code)){
				return lang;
			}
		}
		return getLanguageById("1");
	}

}
