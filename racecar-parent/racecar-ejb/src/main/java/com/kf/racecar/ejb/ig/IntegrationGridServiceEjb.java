package com.kf.racecar.ejb.ig;

import java.util.ArrayList;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import com.pdi.data.abyd.dto.intGrid.IGFinalScoreDTO;
import com.pdi.data.abyd.dto.intGrid.IGSummaryDataDTO;
import com.pdi.webservice.NhnWebserviceUtils;

@Stateless
@LocalBean
public class IntegrationGridServiceEjb implements IntegrationGridServiceEjbLocal {
	
	private static final Logger log = LoggerFactory.getLogger(IntegrationGridServiceEjb.class);
	
	

	@Override
	public IGSummaryDataDTO fetchIGSummary(String lpId, boolean isStartup) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	/**
	 * This method uses JPA to retrieve the Integration Grid Data
	 * @param isStartup
	 * @param dnaId
	 * @param projId
	 * @param partId
	 * @return
	 * @throws Exception
	 */
	private IGSummaryDataDTO getSummaryData(boolean isStartup, long dnaId, long projId, long partId ) throws Exception {
		
		IGSummaryDataDTO ret = new IGSummaryDataDTO();
		//assuming we will look these up here
		String partFname = null;
		String partLname = null;
		// See if we need to score the ALP
		log.debug("Performing ALR Score check");
		alrScoreCheck(isStartup);
		log.debug("Finished ALR Score check");
		isStartup = false; // No need to force scoring after the check has
								// been done

		// Stick in stuff from the linkparams
		ret.setDnaId(dnaId);
		ret.setPartId(String.valueOf(partId));
		ret.setPartFname(partFname);
		ret.setPartLname(partLname);
		
		Document projectInfo = new NhnWebserviceUtils().fetchProjectInfo(String.valueOf(projId));

		// Get some more IG-unique stuff
		log.debug("Get IG Unique Data");
		getIGUniqueData();
		log.debug("Finished Get IG Unique Data");

		// Now fill the DNA Structure DTO with appropriate data
		// First, get the module and competency lists...
		log.debug("Get Module and Competency Lists");
		getModAndCompLists();
		log.debug("Finished get Module and Competency Lists");

		// ...then get the score data for the intersections (but not yet in ret)
		// Set up the intersections
		log.debug("Setup Intersection Data");
		setUpIntersectionData();
		log.debug("Finished Setup Intersection data");

		// Special processing for FinEx
		log.debug("Finex Result conversion");
		finExResultConversion();
		log.debug("Finished Finex Result conversion");

		// Add the Eval Guide scores to the intersections (but not yet in ret)
		log.debug("Get Eval Guide Scores");
		getEGScores();
		log.debug("Finished get Eval Guide Scores");

		// Add the GPI & cog scores to the intersections (but not yet in ret)
		log.debug("Setup Testing Scores");
		setUpTestingScores();
		log.debug("Finished Setup Testing Scores");

		// Move the intersections to ret.
		log.debug("Move the Intersections to the Return object");
		//ret.getGridData().getModCompIntersection().addAll(this.cellMap.values());
		log.debug("Finished Move the Intersections to the Return object");

		// Build the report list
		log.debug("Build the Report List");
		setUpReportList();
		log.debug("Finished Build the Report List");
		
		// Get the entered final scores
		log.debug("Get the entered final scores");
		ret.setCompFinData(getFinalScores());
		log.debug("Finished Get the entered final scores");

		// See if there should be a dLCI & final score
		log.debug("Calculate dLCI and final Score");
		calcDlciAndFinal();
		log.debug("Finished Calculate dLCI and final Score");

		// Get gpiIncluded boolean for UI display purposes...
		ret.setGpiIncluded(checkForGPIinDna(dnaId));

		return ret;
	}

	private void alrScoreCheck(boolean isStartup) {
		// TODO Auto-generated method stub
		
	}

	private void getIGUniqueData() {
		// TODO Auto-generated method stub
		
	}

	private void getModAndCompLists() {
		// TODO Auto-generated method stub
		
	}

	private void setUpIntersectionData() {
		// TODO Auto-generated method stub
		
	}

	private void finExResultConversion() {
		// TODO Auto-generated method stub
		
	}

	private void getEGScores() {
		// TODO Auto-generated method stub
		
	}

	private void setUpTestingScores() {
		// TODO Auto-generated method stub
		
	}

	private void setUpReportList() {
		// TODO Auto-generated method stub
		
	}

	private boolean checkForGPIinDna(long dnaId) {
		// TODO Auto-generated method stub
		return false;
	}

	private ArrayList<IGFinalScoreDTO> getFinalScores() {
		// TODO Auto-generated method stub
		return null;
	}

	private void calcDlciAndFinal() {
		// TODO Auto-generated method stub
		
	}

}
