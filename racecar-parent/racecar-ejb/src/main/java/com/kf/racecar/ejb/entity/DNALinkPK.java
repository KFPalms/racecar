package com.kf.racecar.ejb.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DNALinkPK implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Column(name = "dnaId",insertable=false, updatable=false)
	private int dnaId;
	
	@Column(name = "moduleId",insertable=false, updatable=false)
	private int moduleId;
	
	@Column(name = "competencyId",insertable=false, updatable=false)
	private int competencyId;

	public int getDnaId() {
		return dnaId;
	}

	public void setDnaId(int dnaId) {
		this.dnaId = dnaId;
	}

	public int getModuleId() {
		return moduleId;
	}

	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}

	public int getCompetencyId() {
		return competencyId;
	}

	public void setCompetencyId(int competencyId) {
		this.competencyId = competencyId;
	}

}
