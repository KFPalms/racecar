package com.kf.racecar.ejb.alr;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.apache.poi.ss.usermodel.Workbook;
import org.codehaus.jackson.JsonNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.kf.palms.common.exception.PalmsErrorCode;
import com.kf.palms.web.util.JSONUtils;
import com.kf.racecar.ejb.storage.ReportAPIServiceLocal;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.data.abyd.util.AlrExtractHelper;
import com.pdi.data.dto.Language;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.dto.Text;
import com.pdi.data.dto.TextGroup;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.ILanguageHelper;
import com.pdi.data.helpers.interfaces.IReportHelper;
import com.pdi.properties.PropertyLoader;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.ExtractItem;
import com.pdi.reporting.report.GroupReport;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.helpers.ALRExtractFormatHelper;
import com.pdi.xml.XMLUtils;
@Stateless
@LocalBean
public class GroupExtractServiceImpl implements GroupExtractServiceLocal {
	
	final static Logger log = LoggerFactory.getLogger(GroupExtractServiceImpl.class);
	
	@EJB
	ReportAPIServiceLocal reportAPIService;
	

	@Override
	@Asynchronous
	public void generateGroupExtract(Document xml, int reportRequestId) {
		NodeList nodes = XMLUtils.getElements(xml, "//ReportRequest");
		
		String reportCode;
		String langCode;
		boolean multiProject = false;

		for (int i = 0; i < nodes.getLength(); i++) {
		
			Node node = nodes.item(i);

			reportCode = node.getAttributes().getNamedItem("code").getTextContent();
			langCode = node.getAttributes().getNamedItem("langCode").getTextContent();
			
			if (node.getAttributes().getNamedItem("multiProject") != null){
				multiProject = Boolean.valueOf(node.getAttributes().getNamedItem("multiProject").getTextContent());
				log.debug("Detected Multi-project report.");
			}
			
			NodeList participants = XMLUtils.getElements(node, "Participants/Participant");
			
			if (reportCode.equals("ALR_REPORT_EXTRACT_V2")){
				GroupReport gr = new GroupReport();
				gr.setReportCode(ReportingConstants.REPORT_CODE_ALR_EXTRACT_V2);
				gr.setReportType(ReportingConstants.REPORT_TYPE_GROUP_EXTRACT);
				gr.setMultiProject(multiProject);
				TextGroup tg = new TextGroup("NoTextCode", langCode);
				Language langObj = new Language("0", langCode, langCode+"-name", langCode+"-transName");
				tg.getLabelMap().put("LANG_CODE", new Text(0, "LANG_CODE", langObj, langCode));
				
				try {
					gr = this.getALRExtractData(gr, tg, langCode, participants);
				} catch (Exception e1) {
					log.error("ALR Extract failed while gathering data: {}", e1.getMessage());
					reportAPIService.updateRequestStatusError(reportRequestId, e1.getMessage(), PalmsErrorCode.BAD_REQUEST.getCode());
					continue;
				}
				
				ALRExtractFormatHelper fh = new ALRExtractFormatHelper();
				ArrayList<ExtractItem> itemList = fh.setUpOrder(gr);
				// do the poi
				IReportHelper irh;
				try {
					irh = HelperDelegate.getReportHelper();
					Workbook workbook = irh.buildPoiXlsForALRExtract(itemList, gr);
					File wbFile = File.createTempFile("" + reportRequestId, ".xlsx");
					FileOutputStream fOut = new FileOutputStream(wbFile);
					workbook.write(fOut);
					reportAPIService.uploadReport(wbFile, reportRequestId);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					log.error("ALR Extract Workbook generation failed: {}", e.getMessage());
					reportAPIService.updateRequestStatusError(reportRequestId, e.getMessage(), PalmsErrorCode.REPORT_GEN_FAILED.getCode());
					e.printStackTrace();
					continue;
				}
				
				
			} else {
				//unsupported report code
				log.error("Unsupported report type {}", reportCode);
				reportAPIService.updateRequestStatusError(reportRequestId, "Unsupported Report Type " + reportCode, PalmsErrorCode.BAD_REQUEST.getCode());
				continue;
			}
			
		}

	}
	
	private GroupReport getALRExtractData(GroupReport gr, TextGroup tg, String langCode, NodeList participants) throws Exception {
		if (participants.getLength() < 1){
			throw new Exception("Participant List is empty. Maybe all the subtasks failed?");
		}
		
		HashMap<String, String> lblMap = new HashMap<String, String>();
		lblMap = tg.getLabelHashMap();
		Connection con = null;
		//HashMap<Integer, String> cmpInternalNames = new HashMap<Integer, String>();
		
		//ILanguageHelper lh;
		try {
			//lh = HelperDelegate
				//	.getLanguageHelper(PropertyLoader.getProperty("com.pdi.data.v2.application", "datasource.v2"));

			//SessionUser sessionUser = new SessionUser();
			//long languageId = new Long(lh.fromCode(sessionUser, langCode).getId()).longValue();
	
			con = AbyDDatabaseUtils.getDBConnection();
			
			gr.getLabelData().putAll(lblMap);
			gr.getDisplayData().put("ExtractDate", new SimpleDateFormat("MM/dd/yyyy").format(new Date()));
			
			AlrExtractHelper aeh = new AlrExtractHelper(con);
			
			
			for (int i = 0; i < participants.getLength(); i++) {
				Node participant = participants.item(i);
				String pptId = participant.getAttributes().getNamedItem("participantId").getTextContent();
				String projectId = participant.getAttributes().getNamedItem("projectId").getTextContent();
				String childReportRequestId = participant.getAttributes().getNamedItem("childReportRequestId").getTextContent();
				
				if (i == 0){
					aeh.getGroupData(projectId, gr);
					long dnaId = new Long(gr.getDisplayData().get("DNA_ID")).longValue();
					aeh.addClientCompetencyStructureALR(gr, dnaId);
					//cmpInternalNames = (HashMap<Integer, String>) aeh.getCompInternalNames(dnaId);
					aeh.addIrDataGroup(gr, dnaId);
					
				}
				
				log.debug("Getting individual report for ppt: {}, project: {}, reportid: {}", pptId, projectId, childReportRequestId);
				
				String json = reportAPIService.retrieveReportContentAsString(Integer.valueOf(childReportRequestId));
					
				log.debug("Got json string");
				JsonNode jsonNode = JSONUtils.convertStringToJsonNode(json);
				IndividualReport iReport = new IndividualReport();
				HashMap<String, String> displayData = JSONUtils.convertNodeToHashMap(jsonNode.get("displayData"));
				iReport.setDisplayData(displayData);
				if (jsonNode.get("rank") != null){
					iReport.setRank(jsonNode.get("rank").asInt());
				}
				if (jsonNode.get("reportCode") != null){
					iReport.setReportCode(jsonNode.get("reportCode").getTextValue());
				}
				if (jsonNode.get("reportType") != null){
					iReport.setReportType(jsonNode.get("reportType").getTextValue());
				}
				
				if (jsonNode.get("name") != null){
					iReport.setName(jsonNode.get("name").getTextValue());
				}

				log.debug("Converted json to Individual Report with {} display items", iReport.getDisplayData().size());
				gr.getIndividualReports().add(iReport);
			}
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return gr;
	}

}
