package com.kf.racecar.ejb.alr;

import java.util.List;

import javax.ejb.Local;

import com.pdi.data.dto.ProjectParticipantInstrument;
import com.pdi.data.dto.TextGroup;
import com.pdi.reporting.report.IndividualReport;

@Local
public interface IndividualExtractServiceLocal {

	public String getIndividualALRExtractDataAsJSON(ProjectParticipantInstrument epi, 
			IndividualReport ret, String langCode, TextGroup tg, List<String> courses) throws Exception;
	
	public String makeExtract(int participantId, int projectId, String reportCode) throws Exception;
}
