package com.kf.racecar.ejb.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.kf.palms.jaxb.TimestampXmlAdapter;

@Entity
@Table(name = "pdi_abd_dna_link")
@XmlRootElement(name = "dnaLink")
@XmlAccessorType(XmlAccessType.FIELD)
public class DNALink implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	@XmlTransient
	private DNALinkPK id;
	
	@ManyToOne
	@JoinColumn(name = "dnaId")
	@XmlTransient
	private DNA dna;
	
	@ManyToOne
	@JoinColumn(name = "moduleId")
	private Module module;
	
	@ManyToOne
	@JoinColumn(name = "competencyId")
	private Competency competency;
	
	@Column(name = "modSeq")
	@XmlAttribute
	private int moduleSequence;
	
	@ManyToOne
	@JoinColumn(name = "groupId")
	private GroupingValue groupingValue;
	
	@Column(name = "groupSeq")
	@XmlAttribute
	private int groupSequence;
	
	@Column(name = "compSeq")
	@XmlAttribute
	private int competencySequence;
	
	@Column(name = "isUsed")
	private boolean used;
	
	@Column(name = "modEdit")
	private boolean editModule;
	
	@Column(name = "active")
	private boolean active;
	
	@Column(name = "lastUserId")
	@XmlTransient
	private String lastUserId;
	
	@Column(name = "lastDate")
	@XmlAttribute
	@XmlJavaTypeAdapter(TimestampXmlAdapter.class)
	private Timestamp lastDate;

	public DNALinkPK getId() {
		return id;
	}

	public void setId(DNALinkPK id) {
		this.id = id;
	}

	public DNA getDna() {
		return dna;
	}

	public void setDna(DNA dna) {
		this.dna = dna;
	}

	public Module getModule() {
		return module;
	}

	public void setModule(Module module) {
		this.module = module;
	}

	public Competency getCompetency() {
		return competency;
	}

	public void setCompetency(Competency competency) {
		this.competency = competency;
	}

	public int getModuleSequence() {
		return moduleSequence;
	}

	public void setModuleSequence(int moduleSequence) {
		this.moduleSequence = moduleSequence;
	}

	public GroupingValue getGroupingValue() {
		return groupingValue;
	}

	public void setGroupingValue(GroupingValue groupingValue) {
		this.groupingValue = groupingValue;
	}

	public int getGroupSequence() {
		return groupSequence;
	}

	public void setGroupSequence(int groupSequence) {
		this.groupSequence = groupSequence;
	}

	public int getCompetencySequence() {
		return competencySequence;
	}

	public void setCompetencySequence(int competencySequence) {
		this.competencySequence = competencySequence;
	}

	public boolean isUsed() {
		return used;
	}

	public void setUsed(boolean used) {
		this.used = used;
	}

	public boolean isEditModule() {
		return editModule;
	}

	public void setEditModule(boolean editModule) {
		this.editModule = editModule;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getLastUserId() {
		return lastUserId;
	}

	public void setLastUserId(String lastUserId) {
		this.lastUserId = lastUserId;
	}

	public Timestamp getLastDate() {
		return lastDate;
	}

	public void setLastDate(Timestamp lastDate) {
		this.lastDate = lastDate;
	}

}
