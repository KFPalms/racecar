package com.kf.racecar.ejb.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
@Entity
@Table(name = "pdi_abd_mod_comp_spss_map")
@XmlRootElement(name = "spss-map")
@XmlAccessorType(XmlAccessType.FIELD)
public class SpssMap implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "modCompSpssId")
	@XmlAttribute
	private int modCompSpssId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "modelId")
	@XmlTransient
	private Model model;

	@ManyToOne
	@JoinColumn(name = "competencyId")
	private Competency competency;
	
	@Column(name = "spssId")
	@XmlElement
	private String spssId;
	
	@Column(name="inverted")
	@XmlAttribute
	private boolean inverted;
	
	@Column(name = "active")
	@XmlAttribute
	private boolean active;

	public Model getModel() {
		return model;
	}

	public void setModel(Model model) {
		this.model = model;
	}

	public Competency getCompetency() {
		return competency;
	}

	public void setCompetency(Competency competency) {
		this.competency = competency;
	}

	public String getSpssId() {
		return spssId;
	}

	public void setSpssId(String spssId) {
		this.spssId = spssId;
	}

	public boolean isInverted() {
		return inverted;
	}

	public void setInverted(boolean inverted) {
		this.inverted = inverted;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public int getModCompSpssId() {
		return modCompSpssId;
	}

	public void setModCompSpssId(int modCompSpssId) {
		this.modCompSpssId = modCompSpssId;
	}
}
