package com.kf.racecar.ejb.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.kf.palms.jaxb.TimestampXmlAdapter;


@Entity
@Table(name = "pdi_abd_mod_comp_desc")
@XmlRootElement(name = "model-competency")
@XmlAccessorType(XmlAccessType.FIELD)
public class ModelCompetency implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	@XmlTransient
	private ModelCompetencyPK id;
	
	@MapsId("modelId")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "modelId")
	@XmlTransient
	private Model model;
	
	@MapsId("competencyId")
	@ManyToOne
	@JoinColumn(name = "competencyId")
	private Competency competency;
	
	@Column(name = "textId")
	@XmlAttribute
	private int textId;
	
	@Column(name = "lastUserId")
	@XmlTransient
	private String lastUserId;
	
	@Column(name = "lastDate")
	@XmlAttribute
	@XmlJavaTypeAdapter(TimestampXmlAdapter.class)
	private Timestamp lastDate;
	
	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ModelCompetency )) return false;
        return id != null && id.equals(((ModelCompetency) o).getId());
    }

	public ModelCompetencyPK getId() {
		return id;
	}

	public void setId(ModelCompetencyPK id) {
		this.id = id;
	}

	public Model getModel() {
		return model;
	}

	public void setModel(Model model) {
		this.model = model;
	}

	public Competency getCompetency() {
		return competency;
	}

	public void setCompetency(Competency competency) {
		this.competency = competency;
	}

	public int getTextId() {
		return textId;
	}

	public void setTextId(int textId) {
		this.textId = textId;
	}

	public String getLastUserId() {
		return lastUserId;
	}

	public void setLastUserId(String lastUserId) {
		this.lastUserId = lastUserId;
	}

	public Timestamp getLastDate() {
		return lastDate;
	}

	public void setLastDate(Timestamp lastDate) {
		this.lastDate = lastDate;
	}

}
