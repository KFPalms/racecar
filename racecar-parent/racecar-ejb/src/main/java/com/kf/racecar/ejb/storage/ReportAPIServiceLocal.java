package com.kf.racecar.ejb.storage;

import java.io.File;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Local;

import com.kf.palms.common.exception.PalmsException;
import com.kf.palms.web.reporting.ReportRequestHelper;
import com.kf.palms.web.reporting.jaxb.ReportRepresentation;
import com.kf.palms.web.rest.RestTemplate;
import com.kf.palms.web.rest.security.APICredentials;

@Local
public interface ReportAPIServiceLocal {


	
	public boolean updateRequestStatusProcessing(int reportRequestId);
	
	public void updateRequestStatusError(int reportRequestId, String errorMessage, String errorCode);
	
	public ReportRepresentation uploadReport(File reportFile, int reportRequestId);
	
	public String retrieveReportContentAsString(int reportRequestId) throws Exception;
	
}
