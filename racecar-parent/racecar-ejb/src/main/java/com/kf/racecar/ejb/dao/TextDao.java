package com.kf.racecar.ejb.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.kf.racecar.ejb.entity.Text;

@Stateless
@LocalBean
public class TextDao extends GenericDao<Text, Integer>{

}
