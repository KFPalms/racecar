package com.kf.racecar.ejb.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.kf.palms.common.jpa.dao.AbstractDao;

public abstract class GenericDao<T, ID extends Serializable> extends AbstractDao<T, ID> {
	protected Class<T> persistentClass;

	@PersistenceContext(unitName = "scoring-reporting-dao")
	protected EntityManager entityManager;

	@SuppressWarnings("unchecked")
	protected GenericDao() {
		if (getClass().getGenericSuperclass() instanceof ParameterizedType) {
			ParameterizedType type = (ParameterizedType) (getClass().getGenericSuperclass());
			System.out.println(type.getActualTypeArguments()[0]);
			this.persistentClass = (Class<T>) type.getActualTypeArguments()[0];
		}
	}

	protected GenericDao(Class<T> persistentClass) {
		this.persistentClass = persistentClass;
	}
	
	public boolean isCached(int id){
		return entityManager.getEntityManagerFactory().getCache().contains(persistentClass, id);
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	protected Class<T> getPersistentClass() {
		return persistentClass;
	}
}
