package com.kf.racecar.ejb.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.kf.palms.jaxb.TimestampXmlAdapter;

@Entity
@Table(name = "pdi_abd_dna")
@NamedQueries({ 
	@NamedQuery(name = "findDnaByIdBatchHints", query = "SELECT dna FROM DNA dna WHERE dna.dnaId = :dnaId", 
		hints = {
				@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"), 
				@QueryHint(name = "eclipselink.batch", value = "dna.model.competencies"),
				@QueryHint(name = "eclipselink.batch", value = "dna.model.spssMaps"),
				@QueryHint(name = "eclipselink.batch", value = "dna.model.bars"),
				@QueryHint(name = "eclipselink.batch", value = "dna.model.bars.hiText"),
				@QueryHint(name = "eclipselink.batch", value = "dna.model.bars.medText"),
				@QueryHint(name = "eclipselink.batch", value = "dna.model.bars.loText"),
				@QueryHint(name = "eclipselink.batch", value = "dna.model.bars.hiExText"),
				@QueryHint(name = "eclipselink.batch", value = "dna.model.bars.medExText"),
				@QueryHint(name = "eclipselink.batch", value = "dna.model.bars.loExText"),
				@QueryHint(name = "eclipselink.batch", value = "dna.model.impactRatings"),
				@QueryHint(name = "eclipselink.batch", value = "dna.model.impactRatings.text"),
				@QueryHint(name = "eclipselink.batch", value = "dna.DNALinks"),
				@QueryHint(name = "eclipselink.batch", value = "dna.DNALinks.module"),
				@QueryHint(name = "eclipselink.batch", value = "dna.DNALinks.module.name"),
				@QueryHint(name = "eclipselink.batch", value = "dna.DNALinks.groupingValue"),
				@QueryHint(name = "eclipselink.batch", value = "dna.DNALinks.groupingValue.groupName")
				
		}), 
	@NamedQuery(name = "findDnaTemplateByModelId", query = "SELECT dna FROM DNA dna WHERE dna.model.modelId = :modelId and dna.projectId=0", 
		hints = {
			@QueryHint(name = "eclipselink.batch.type", value = "EXISTS"), 
			@QueryHint(name = "eclipselink.batch", value = "dna.model.competencies"),
			@QueryHint(name = "eclipselink.batch", value = "dna.model.spssMaps"),
			@QueryHint(name = "eclipselink.batch", value = "dna.model.bars"),
			@QueryHint(name = "eclipselink.batch", value = "dna.model.bars.hiText"),
			@QueryHint(name = "eclipselink.batch", value = "dna.model.bars.medText"),
			@QueryHint(name = "eclipselink.batch", value = "dna.model.bars.loText"),
			@QueryHint(name = "eclipselink.batch", value = "dna.model.bars.hiExText"),
			@QueryHint(name = "eclipselink.batch", value = "dna.model.bars.medExText"),
			@QueryHint(name = "eclipselink.batch", value = "dna.model.bars.loExText"),
			@QueryHint(name = "eclipselink.batch", value = "dna.model.impactRatings"),
			@QueryHint(name = "eclipselink.batch", value = "dna.model.impactRatings.text"),
			@QueryHint(name = "eclipselink.batch", value = "dna.DNALinks"),
			@QueryHint(name = "eclipselink.batch", value = "dna.DNALinks.module"),
			@QueryHint(name = "eclipselink.batch", value = "dna.DNALinks.module.name"),
			@QueryHint(name = "eclipselink.batch", value = "dna.DNALinks.groupingValue"),
			@QueryHint(name = "eclipselink.batch", value = "dna.DNALinks.groupingValue.groupName")
			
	})
})
@XmlRootElement(name = "dna")
@XmlAccessorType(XmlAccessType.FIELD)
public class DNA implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "dnaId")
	@XmlAttribute
	private int dnaId;
	
	@Column(name = "dnaName")
	@XmlAttribute
	private String dnaName;
	
	@ManyToOne
	@JoinColumn(name = "modelId")
	@XmlElement
	private Model model;
	
	@Column(name = "projectId")
	private int projectId;
	
	@Column(name = "cMgrName")
	private String cMgrName;
	
	@Column(name = "cMgrEmail")
	private String cMgrEmail;
	
	@Column(name = "dnaSubmitted")
	private boolean submitted;
	
	@Column(name = "synthMean",precision=13, scale=5, length=9)
	private BigDecimal syntheticMean;
	
	@Column(name = "synthStdDev", precision=13, scale=5, length=9)
	private BigDecimal syntheticStandardDeviation;
	
	@Column(name = "wbVersion")
	private String workbookVersion;
	
	@Column(name = "dnaNotes")
	private String dnaNotes;
	
	@Column(name = "reportNote")
	private String reportNote;
	
	@Column(name = "active")
	private boolean active;
	
	@OneToMany(mappedBy = "dna", cascade = CascadeType.ALL, orphanRemoval = true)
	@XmlElementWrapper
	@XmlElement(name="dna-link")
	private List<DNALink> DNALinks;
	
	@Column(name = "lastUserId")
	@XmlTransient
	private String lastUserId;
	
	@Column(name = "lastDate")
	@XmlAttribute
	@XmlJavaTypeAdapter(TimestampXmlAdapter.class)
	private Timestamp lastDate;

	public String getDnaName() {
		return dnaName;
	}

	public void setDnaName(String dnaName) {
		this.dnaName = dnaName;
	}

	public int getDnaId() {
		return dnaId;
	}

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public String getcMgrName() {
		return cMgrName;
	}

	public void setcMgrName(String cMgrName) {
		this.cMgrName = cMgrName;
	}

	public String getcMgrEmail() {
		return cMgrEmail;
	}

	public void setcMgrEmail(String cMgrEmail) {
		this.cMgrEmail = cMgrEmail;
	}

	public boolean isSubmitted() {
		return submitted;
	}

	public void setSubmitted(boolean submitted) {
		this.submitted = submitted;
	}

	public BigDecimal getSyntheticMean() {
		return syntheticMean;
	}

	public void setSyntheticMean(BigDecimal syntheticMean) {
		this.syntheticMean = syntheticMean;
	}

	public BigDecimal getSyntheticStandardDeviation() {
		return syntheticStandardDeviation;
	}

	public void setSyntheticStandardDeviation(BigDecimal syntheticStandardDeviation) {
		this.syntheticStandardDeviation = syntheticStandardDeviation;
	}

	public String getWorkbookVersion() {
		return workbookVersion;
	}

	public void setWorkbookVersion(String workbookVersion) {
		this.workbookVersion = workbookVersion;
	}

	public String getDnaNotes() {
		return dnaNotes;
	}

	public void setDnaNotes(String dnaNotes) {
		this.dnaNotes = dnaNotes;
	}

	public String getReportNote() {
		return reportNote;
	}

	public void setReportNote(String reportNote) {
		this.reportNote = reportNote;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getLastUserId() {
		return lastUserId;
	}

	public void setLastUserId(String lastUserId) {
		this.lastUserId = lastUserId;
	}

	public Timestamp getLastDate() {
		return lastDate;
	}

	public void setLastDate(Timestamp lastDate) {
		this.lastDate = lastDate;
	}

	public void setDnaId(int dnaId) {
		this.dnaId = dnaId;
	}

	public Model getModel() {
		return model;
	}

	public void setModel(Model model) {
		this.model = model;
	}

}
