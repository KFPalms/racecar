package com.kf.racecar.ejb.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.kf.palms.jaxb.TimestampXmlAdapter;

@Entity
@Table(name = "pdi_abd_module")
@XmlRootElement(name = "module")
@XmlAccessorType(XmlAccessType.FIELD)
public class Module implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "moduleId")
	@XmlAttribute
	private int moduleId;
	
	@Column(name = "internalName")
	@XmlAttribute
	private String internalName;
	
	@ManyToOne
	@JoinColumn(name = "textId")
	private Text name;
	
	@Column(name = "hasEG")
	@XmlAttribute
	private boolean hasEg;
	
	@Column(name = "specialType")
	@XmlAttribute
	private String specialType;
	
	@Column(name = "active")
	@XmlAttribute
	private boolean active;
	
	@Column(name = "lastUserId")
	@XmlTransient
	private String lastUserId;
	
	@Column(name = "lastDate")
	@XmlAttribute
	@XmlJavaTypeAdapter(TimestampXmlAdapter.class)
	private Timestamp lastDate;

	public String getInternalName() {
		return internalName;
	}

	public void setInternalName(String internalName) {
		this.internalName = internalName;
	}

	public Text getName() {
		return name;
	}

	public void setName(Text name) {
		this.name = name;
	}

	public boolean isHasEg() {
		return hasEg;
	}

	public void setHasEg(boolean hasEg) {
		this.hasEg = hasEg;
	}

	public String getSpecialType() {
		return specialType;
	}

	public void setSpecialType(String specialType) {
		this.specialType = specialType;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getLastUserId() {
		return lastUserId;
	}

	public void setLastUserId(String lastUserId) {
		this.lastUserId = lastUserId;
	}

	public Timestamp getLastDate() {
		return lastDate;
	}

	public void setLastDate(Timestamp lastDate) {
		this.lastDate = lastDate;
	}

	public int getModuleId() {
		return moduleId;
	}

}
