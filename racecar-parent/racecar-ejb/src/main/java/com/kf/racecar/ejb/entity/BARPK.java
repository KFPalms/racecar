package com.kf.racecar.ejb.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class BARPK implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Column(name="modelId",insertable=false, updatable=false)
	private int modelId;
	
	@Column(name="competencyId",insertable=false, updatable=false)
	private int competencyId;
	
	@Column(name="moduleId", insertable=false, updatable=false)
	private int moduleId;
	
	@Column(name="barSequence", insertable=false, updatable=false)
	private int barSequence;

	public int getModelId() {
		return modelId;
	}

	public void setModelId(int modelId) {
		this.modelId = modelId;
	}

	public int getCompetencyId() {
		return competencyId;
	}

	public void setCompetencyId(int competencyId) {
		this.competencyId = competencyId;
	}

	public int getModuleId() {
		return moduleId;
	}

	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}

	public int getBarSequence() {
		return barSequence;
	}

	public void setBarSequence(int barSequence) {
		this.barSequence = barSequence;
	}
	

}
