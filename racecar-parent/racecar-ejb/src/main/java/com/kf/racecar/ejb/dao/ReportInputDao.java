package com.kf.racecar.ejb.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.kf.racecar.ejb.entity.ReportInput;

@Stateless
@LocalBean
public class ReportInputDao extends GenericDao<ReportInput, Integer> {
	
	public ReportInput findByDnaIdAndParticipantId(int dnaId, int participantId){
		TypedQuery<ReportInput> query = getEntityManager().createNamedQuery("findByDnaIdAndParticipantId", ReportInput.class);
		
		query.setParameter("dnaId", dnaId);
		query.setParameter("participantId", participantId);
		
		try {
			return query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

}
