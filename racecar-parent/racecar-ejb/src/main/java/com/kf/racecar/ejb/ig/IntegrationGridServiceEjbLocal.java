package com.kf.racecar.ejb.ig;

import javax.ejb.Local;

import com.pdi.data.abyd.dto.intGrid.IGSummaryDataDTO;

@Local
public interface IntegrationGridServiceEjbLocal {
	
	public IGSummaryDataDTO fetchIGSummary(String lpId, boolean isStartup) throws Exception;

}
