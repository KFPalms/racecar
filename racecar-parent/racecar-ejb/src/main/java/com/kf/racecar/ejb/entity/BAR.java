package com.kf.racecar.ejb.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.kf.palms.jaxb.TimestampXmlAdapter;
@Entity
@Table(name = "pdi_abd_eg_bar")
@XmlRootElement(name = "bar")
@XmlAccessorType(XmlAccessType.FIELD)
public class BAR implements Serializable {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	@XmlTransient
	private BARPK id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "modelId")
	@XmlTransient
	private Model model;
	
	@ManyToOne
	@JoinColumn(name = "competencyId")
	private Competency competency;
	
	@Column(name = "moduleId")
	@XmlAttribute
	private int moduleId;
	
	@Column(name = "barSequence", columnDefinition = "SMALLINT")
	@XmlAttribute
	private short barSequence;
	
	@Column(name = "extBehaviorId")
	@XmlAttribute
	private int extBehaviorId;
	
	@Column(name = "active")
	@XmlAttribute
	private boolean active;
	
	@Column(name = "lastUserId")
	@XmlTransient
	private String lastUserId;
	
	@Column(name = "lastDate")
	@XmlAttribute
	@XmlJavaTypeAdapter(TimestampXmlAdapter.class)
	private Timestamp lastDate;
	
	@ManyToOne
	@JoinColumn(name = "hiTextId")
	private Text hiText;
	
	@ManyToOne
	@JoinColumn(name = "loTextId")
	private Text loText;
	
	@ManyToOne
	@JoinColumn(name = "medTextId")
	private Text medText;
	
	@ManyToOne
	@JoinColumn(name = "hiExTextId")
	@XmlTransient
	private Text hiExText;
	
	@ManyToOne
	@JoinColumn(name = "loExTextId")
	@XmlTransient
	private Text loExText;
	
	@ManyToOne
	@JoinColumn(name = "medExTextId")
	@XmlTransient
	private Text medExText;

	public BARPK getId() {
		return id;
	}

	public void setId(BARPK id) {
		this.id = id;
	}

	public Model getModel() {
		return model;
	}

	public void setModel(Model model) {
		this.model = model;
	}

	public Competency getCompetency() {
		return competency;
	}

	public void setCompetency(Competency competency) {
		this.competency = competency;
	}

	public int getModuleId() {
		return moduleId;
	}

	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}

	public short getBarSequence() {
		return barSequence;
	}

	public void setBarSequence(short barSequence) {
		this.barSequence = barSequence;
	}

	public int getExtBehaviorId() {
		return extBehaviorId;
	}

	public void setExtBehaviorId(int extBehaviorId) {
		this.extBehaviorId = extBehaviorId;
	}

	public Text getHiText() {
		return hiText;
	}

	public void setHiText(Text hiText) {
		this.hiText = hiText;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getLastUserId() {
		return lastUserId;
	}

	public void setLastUserId(String lastUserId) {
		this.lastUserId = lastUserId;
	}

	public Timestamp getLastDate() {
		return lastDate;
	}

	public void setLastDate(Timestamp lastDate) {
		this.lastDate = lastDate;
	}

	public Text getLoText() {
		return loText;
	}

	public void setLoText(Text loText) {
		this.loText = loText;
	}

	public Text getMedText() {
		return medText;
	}

	public void setMedText(Text medText) {
		this.medText = medText;
	}

	public Text getHiExText() {
		return hiExText;
	}

	public void setHiExText(Text hiExText) {
		this.hiExText = hiExText;
	}

	public Text getLoExText() {
		return loExText;
	}

	public void setLoExText(Text loExText) {
		this.loExText = loExText;
	}

	public Text getMedExText() {
		return medExText;
	}

	public void setMedExText(Text medExText) {
		this.medExText = medExText;
	}
	
	
}
