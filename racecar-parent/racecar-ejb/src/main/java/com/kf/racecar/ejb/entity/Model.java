package com.kf.racecar.ejb.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.kf.palms.jaxb.TimestampXmlAdapter;

@Entity
@Table(name = "pdi_abd_model")
@XmlRootElement(name = "model")
@XmlAccessorType(XmlAccessType.FIELD)
@NamedQueries({ 
	@NamedQuery(name = "getModelByIdBatchChildren", query = "SELECT model FROM Model model where model.modelId = :id"
			, hints = {
					@QueryHint(name = "eclipselink.batch.type", value = "JOIN"),
					@QueryHint(name = "eclipselink.batch", value = "model.bars.competency"),
					@QueryHint(name = "eclipselink.batch", value = "model.bars"),
					@QueryHint(name = "eclipselink.batch", value = "model.bars.hiText"),
					@QueryHint(name = "eclipselink.batch", value = "model.bars.medText"),
					@QueryHint(name = "eclipselink.batch", value = "model.bars.loText"),
					@QueryHint(name = "eclipselink.batch", value = "model.bars.hiExText"),
					@QueryHint(name = "eclipselink.batch", value = "model.bars.medExText"),
					@QueryHint(name = "eclipselink.batch", value = "model.bars.loExText"),
					@QueryHint(name = "eclipselink.batch", value = "model.competencies.competency")
			})
})
public class Model implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "modelId")
	@XmlAttribute
	private int modelId;
	
	@Column(name = "internalName")
	@XmlAttribute
	private String internalName;
	
	/*
	@OneToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST })
	@JoinColumn(name = "dnaId")
	@XmlTransient
	private DNA dnaTemplate;
	*/
	
	@OneToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST })
	@JoinColumn(name = "textId")
	private Text externalName;
	
	@Column(name = "dnaId")
	@XmlAttribute
	private int dnaTemplateId;
	
	@Column(name = "modelSeq")
	@XmlAttribute
	private int modelSequence;
	
	@Column(name = "targetLevelTypeId")
	@XmlAttribute
	private int targetLevelTypeId;
	
	@Column(name = "allowCso")
	@XmlAttribute
	private boolean allowCso;
	
	@Column(name = "active")
	@XmlAttribute
	private boolean active;
	
	@Column(name = "lastUserId")
	@XmlAttribute
	private String lastUserId;
	
	@Column(name = "lastDate")
	@XmlAttribute
	@XmlJavaTypeAdapter(TimestampXmlAdapter.class)
	private Timestamp lastDate;
	
	@Column(name = "isRa")
	private boolean readinessAssessmentModel;
	
	@OneToMany(mappedBy = "model", cascade = CascadeType.ALL, orphanRemoval = true)
	@XmlElementWrapper
	@XmlElement(name="model-competency")
	private List<ModelCompetency> competencies;
	
	@OneToMany(mappedBy = "model", cascade = CascadeType.ALL, orphanRemoval = true)
	@XmlElementWrapper
	@XmlElement(name="spss-map")
	private List<SpssMap> spssMaps;
	
	@OneToMany(mappedBy = "model", cascade = CascadeType.ALL, orphanRemoval = true)
	@XmlElementWrapper
	@XmlElement(name="bar")
	private List<BAR> bars;
	
	@OneToMany(mappedBy = "model", cascade = CascadeType.ALL, orphanRemoval = true)
	@XmlElementWrapper
	@XmlElement(name="impact-rating")
	private List<ImpactRating> impactRatings;
	

	public int getModelId() {
		return modelId;
	}

	public void setModelId(int modelId) {
		this.modelId = modelId;
	}

	public String getInternalName() {
		return internalName;
	}

	public void setInternalName(String internalName) {
		this.internalName = internalName;
	}

	public int getModelSequence() {
		return modelSequence;
	}

	public void setModelSequence(int modelSequence) {
		this.modelSequence = modelSequence;
	}

	public int getTargetLevelTypeId() {
		return targetLevelTypeId;
	}

	public void setTargetLevelTypeId(int targetLevelTypeId) {
		this.targetLevelTypeId = targetLevelTypeId;
	}

	public boolean isAllowCso() {
		return allowCso;
	}

	public void setAllowCso(boolean allowCso) {
		this.allowCso = allowCso;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getLastUserId() {
		return lastUserId;
	}

	public void setLastUserId(String lastUserId) {
		this.lastUserId = lastUserId;
	}

	public Timestamp getLastDate() {
		return lastDate;
	}

	public void setLastDate(Timestamp lastDate) {
		this.lastDate = lastDate;
	}

	public boolean isReadinessAssessmentModel() {
		return readinessAssessmentModel;
	}

	public void setReadinessAssessmentModel(boolean readinessAssessmentModel) {
		this.readinessAssessmentModel = readinessAssessmentModel;
	}

	public List<ModelCompetency> getCompetencies() {
		return competencies;
	}

	public void setCompetencies(List<ModelCompetency> competencies) {
		this.competencies = competencies;
	}

	public List<SpssMap> getSpssMaps() {
		return spssMaps;
	}

	public void setSpssMaps(List<SpssMap> spssMaps) {
		this.spssMaps = spssMaps;
	}

	public List<BAR> getBars() {
		return bars;
	}

	public void setBars(List<BAR> bars) {
		this.bars = bars;
	}

	public List<ImpactRating> getImpactRatings() {
		return impactRatings;
	}

	public void setImpactRatings(List<ImpactRating> impactRatings) {
		this.impactRatings = impactRatings;
	}

	public int getDnaTemplateId() {
		return dnaTemplateId;
	}

	public void setDnaTemplateId(int dnaTemplateId) {
		this.dnaTemplateId = dnaTemplateId;
	}

	public Text getExternalName() {
		return externalName;
	}

	public void setExternalName(Text externalName) {
		this.externalName = externalName;
	}

	
}
