package com.kf.racecar.ejb.singleton;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kf.racecar.ejb.dao.DNADao;
import com.kf.racecar.ejb.dao.ModelDao;
import com.kf.racecar.ejb.entity.Model;

@Startup
@Singleton
public class RacecarUtilSingleton {

	private final static Logger log = LoggerFactory.getLogger(RacecarUtilSingleton.class);
	
	private int hitCounter;
	
	@EJB
	DNADao dnaDao;
	
	@EJB
	ModelDao modelDao;
	
	private Map<Integer, Model> modelMapId = new HashMap<Integer, Model>();
	private Map<String, Model> modelMapName = new HashMap<String, Model>();
	
	@PostConstruct
	public void init(){
		log.debug("Initializing Racecar Util Singleton!");
		hitCounter = 0;
		EntityManager em = dnaDao.getEntityManager();
		Query query = em.createNativeQuery("Select count(*) from pdi_abd_dna;");
		
		Integer dnaCount = (Integer) query.getSingleResult();
		
		log.debug("There are {} DNA entities in the database", dnaCount);
		
		List<Model> models = modelDao.findAll();
		
		for (Model model: models){
			modelMapId.put(model.getModelId(), model);
			modelMapName.put(model.getInternalName(), model);
		}
	}
	
	public int incrementHitCounter(){
		hitCounter = hitCounter + 1;
		log.debug("Incrementing Hit counter.  Count is now: {}", hitCounter);
		return hitCounter;
	}
	
	public Model getModelById(int modelId){
		return modelMapId.get(Integer.valueOf(modelId));
	}
	
	public Model getModelByName(String modelName) {
		return modelMapName.get(modelName);
	}
	
	public int getMaxModelSequence(){
		int maxSequence = 0;
		for (Integer modelId: modelMapId.keySet()){
			Model model = modelMapId.get(modelId);
			if (model.getModelSequence() > maxSequence){
				maxSequence = model.getModelSequence();
			}
		}
		return maxSequence;
	}
}
