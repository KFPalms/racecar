package com.kf.racecar.ejb.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlAttribute;

@Embeddable
public class ModelCompetencyPK implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Column(name="modelId",insertable=false, updatable=false)
	private int modelId;
	
	@Column(name="competencyId",insertable=false, updatable=false)
	private int competencyId;

	public int getModelId() {
		return modelId;
	}

	public void setModelId(int modelId) {
		this.modelId = modelId;
	}

	public int getCompetencyId() {
		return competencyId;
	}

	public void setCompetencyId(int competencyId) {
		this.competencyId = competencyId;
	}

}
