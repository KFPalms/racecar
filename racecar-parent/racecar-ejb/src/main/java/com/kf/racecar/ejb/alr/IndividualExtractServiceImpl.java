package com.kf.racecar.ejb.alr;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kf.palms.web.util.JSONUtils;
import com.kf.racecar.ejb.singleton.LanguageSingleton;
import com.pdi.data.abyd.dto.reportInput.ReportInputDTO;
import com.pdi.data.abyd.dto.setup.EntryStateDTO;
import com.pdi.data.abyd.dto.setup.ReportModelStructureDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportLabelValueDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportLabelsDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportOptionValueDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportOptionsDTO;
import com.pdi.data.abyd.helpers.common.HelperUtils;
import com.pdi.data.abyd.helpers.common.ImportExportDataHelper;
import com.pdi.data.abyd.helpers.intGrid.IGKf4dDataHelper;
import com.pdi.data.abyd.helpers.reportInput.ReportInputDataHelper;
import com.pdi.data.abyd.helpers.setup.EntryStateHelper;
import com.pdi.data.abyd.helpers.setup.ReportLabelsDataHelper;
import com.pdi.data.abyd.helpers.setup.ReportModelDataHelper;
import com.pdi.data.abyd.helpers.setup.ReportOptionsDataHelper;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.data.abyd.util.AlrExtractHelper;
import com.pdi.data.dto.Instrument;
import com.pdi.data.dto.NormScore;
import com.pdi.data.dto.Participant;
import com.pdi.data.dto.Project;
import com.pdi.data.dto.ProjectParticipantInstrument;
import com.pdi.data.dto.Score;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.dto.TextGroup;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.IProjectParticipantInstrumentHelper;
import com.pdi.data.util.language.DefaultLanguageHelper;
import com.pdi.listener.portal.delegated.helpers.reports.PdaDataHelper;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;
import com.pdi.reporting.report.helpers.AbyDExtractFormatHelper;
import com.pdi.string.StringUtils;
import com.pdi.xml.XMLUtils;
@Stateless
@LocalBean
public class IndividualExtractServiceImpl implements IndividualExtractServiceLocal {
	
	private static final Logger log = LoggerFactory.getLogger(IndividualExtractServiceImpl.class);
	
	private static int EDITABLE = 1;
	
	@EJB
	LanguageSingleton languageSingleton;

	/**
	 * The List should include only one participant and project, but may contain multiple instruments.
	 * Should refactor to make this more apparent.
	 */
	@Override
	public String getIndividualALRExtractDataAsJSON(ProjectParticipantInstrument epi, 
			IndividualReport ret, String langCode, TextGroup tg, List<String> courses) throws Exception {
		
		String json = null;
		Connection con = null;
		try {
			
	
			String pptId = epi.getParticipant().getId();
			String projId = epi.getProject().getId();
			long dnaId = 0;
			
			ReportModelStructureDTO reportModelStructure = null;
			ReportInputDTO reportInput = null;
			
			con = AbyDDatabaseUtils.getDBConnection();
			
			AlrExtractHelper aeh = new AlrExtractHelper(con);
			
	
			
			
			projId = epi.getProject().getId();

			dnaId = new ImportExportDataHelper().getDnaFromV2(con, projId);
			if (dnaId < 1) {
				throw new Exception("No DNA found for project: " + projId);
			}
			Map<Integer, String> cmpInternalNames = (HashMap<Integer, String>) aeh.getCompInternalNames(dnaId);
			log.debug("Got {} cmp Internal Names", cmpInternalNames.size());
			
			ReportModelDataHelper reportModelDataHelper = new ReportModelDataHelper();
			ReportInputDataHelper reportInputDataHelper = new ReportInputDataHelper();

			long langId = Long.valueOf(languageSingleton.getLanguageIdByCode(langCode));
			
			reportModelStructure = reportModelDataHelper.fetchReportModelAndScores(con, dnaId, langId,
					pptId, true);
			reportInput = reportInputDataHelper.getReportInput(pptId, dnaId, langCode);


			String firstName = epi.getParticipant().getFirstName();
			if (firstName == null) {
				firstName = "";
			}
			String lastName = epi.getParticipant().getLastName();
			if (lastName == null) {
				lastName = "";
			}
			String businessUnit = epi.getParticipant().getMetadata().get("OPTIONAL_1");
			if (businessUnit == null) {
				businessUnit = "";
			}
			// ret.setName(firstName + "_" + lastName + "_" +
			// businessUnit.replace("/", "_").replace("\\", "_") + "_" +
			// randomIndex + ".pdf");

			// Get Project data
			ret.addDisplayData("ORGANIZATION",
					XMLUtils.xmlEscapeString(epi.getProject().getClient().getName()));

			ret.addDisplayData("TRANSITION_LEVEL", "" + epi.getProject().getTargetLevelTypeId());

			// System.out.println("Transition level... ." +
			// ret.getDisplayData("TRANSITION_LEVEL"));
			// ret.addDisplayData("PROJECT_NAME",
			// epi.getProject().getName());

			ret.getDisplayData().put("TARGET_LEVEL_NAME", epi.getProject().getTargetLevelName());
			ret.getDisplayData().put("TARGET_LEVEL_CODE", "" + epi.getProject().getTargetLevelCode());
			ret.getDisplayData().put("TARGET_LEVEL_INDEX", "" + epi.getProject().getTargetLevelIndex());

			// Use the name as project name on A by D data
			ret.getDisplayData().put("PROJECT_NAME", XMLUtils.xmlEscapeString(reportInput.getDnaName()));
			ret.getDisplayData().put("CLIENT_NAME", XMLUtils.xmlEscapeString(reportInput.getClientName()));
			ret.getDisplayData().put("MODEL_NAME", XMLUtils.xmlEscapeString(reportInput.getModelName()));

			/*
			 *  Set up labels/option data/entry state
			 *  --------------------------------------
			 */

			ReportOptionsDataHelper optionsDH = new ReportOptionsDataHelper();
			ReportLabelsDataHelper labelsDH = new ReportLabelsDataHelper();
			EntryStateHelper entryH = new EntryStateHelper(con, projId);

			Setup2ReportOptionsDTO options = null;
			Setup2ReportLabelsDTO labels = null;

			options = optionsDH.getReportOptionData(con, dnaId);
			labels = labelsDH.getTranslatedReportLabelData(dnaId, tg.getLanguageCode(), EDITABLE);

			EntryStateDTO entryState = entryH.getEntryStateData();

			ret.getDisplayData().put("CLIENT_LOGO", epi.getProject().getClient().getId() + ".png");

			ret.getDisplayData().put("DNA_NAME",
					XMLUtils.xmlEscapeString(entryState.getDnaDesc().getDnaName()));

			/***
			 * SET THE FILE NAME the new requirments:
			 * http://segjira:8090/browse/NHN-2269 Aug 15 2012
			 *
			 * Participant Last Name_+ Participant First Name_ + TLT or
			 * ABYD_+ Target Level_ + Selected_Report_ +
			 * Language_Report_ + Generation Date.pdf+
			 */
			// Target LEVEL
			String trgLevel = " ";
			IndividualReport nextTLT = reportInput.getCurrentTLT();
			if (nextTLT.getDisplayData().get("TARGET_LEVEL").equalsIgnoreCase("10")) {
				trgLevel = "SEA";

			} else if (nextTLT.getDisplayData().get("TARGET_LEVEL").equalsIgnoreCase("9")) {
				trgLevel = "CEO";

			} else if (nextTLT.getDisplayData().get("TARGET_LEVEL").equalsIgnoreCase("8")) {
				trgLevel = "BUL";

			} else if (nextTLT.getDisplayData().get("TARGET_LEVEL").equalsIgnoreCase("7")) {
				trgLevel = "MLL";
			}
			String fileName = StringUtils.removeFilenameSpecialCharacters(
					// reportInput.getClientName() + " " +
					// reportInput.getProjectName() + " " +
					reportInput.getParticipantNameInverse() + " " + epi.getProject().getProjectTypeName() + " "
							+ trgLevel + " " + ret.getReportCode() + " " + // rptCode+
																			// "
																			// "+
							new SimpleDateFormat("yyyyMMdd").format(new Date()) + " " + langCode);

			ret.setName(fileName.replace(" ", "_") + ".pdf");

			ret.getDisplayData().put("TargetLevel", trgLevel);

			// Hardcode Cogs Included to 1
			ret.getDisplayData().put(ReportingConstants.RC_COGNITIVES_INCLUDED, "1");

			// ADD OPTION DATA TO REPORT
			for (Setup2ReportOptionValueDTO value : options.getOptionValuesArray()) {
				if (value.getOptionString().equals("")) {
					ret.getDisplayData().put("OPTION_" + value.getOptionCode(),
							(value.getOptionValue() ? "true" : null));
				} else {
					// String str = value.getOptionString();
					ret.getDisplayData().put("OPTION_" + value.getOptionCode(), (value.getOptionString()));
				}
			}

			// ADD LABEL DATA TO REPORT
			for (Setup2ReportLabelValueDTO value : labels.getLabelValuesArray()) {
				if (value != null) {
					String cvalue = value.getCustomLabel();
					String svalue = value.getLabel();
					if ((cvalue.length() > 0)
							&& (langId == DefaultLanguageHelper.fetchDefaultLanguageId())) // only
																										// do
																										// if
																										// en
																										// language(1)
					{
						svalue = value.getLabel();
						svalue = cvalue;
					}
					// System.out.println("value.getReportCode(): " +
					// value.getReportCode() + " svalue: " + svalue);

					ret.getDisplayData().put("LABEL_" + value.getReportCode(), svalue);
				}
			}

			if (labels.getCoverPage().getCustomLabel() != null
					&& labels.getCoverPage().getCustomLabel().length() > 0) {
				ret.getDisplayData().put("LABEL_COV_PAGE", labels.getCoverPage().getCustomLabel());
				// ret.getDisplayData().put("LABEL_COV_PAGE",
				// UnitTestUtils.unicodeText());
			} else {
				ret.getDisplayData().put("LABEL_COV_PAGE", labels.getCoverPage().getLabel());
				// ret.getDisplayData().put("LABEL_COV_PAGE",
				// UnitTestUtils.unicodeText());
			}

			if (labels.getLeadershipLegend().getCustomLabel() != null
					&& labels.getLeadershipLegend().getCustomLabel().length() > 0) {
				ret.getDisplayData().put("LABEL_LEG_LEAD", labels.getLeadershipLegend().getCustomLabel());
			} else {
				ret.getDisplayData().put("LABEL_LEG_LEAD", labels.getLeadershipLegend().getLabel());
			}

			if (labels.getTltLegend().getCustomLabel() != null
					&& labels.getTltLegend().getCustomLabel().length() > 0) {
				ret.getDisplayData().put("LABEL_LEG_TLT", labels.getTltLegend().getCustomLabel());
			} else {
				ret.getDisplayData().put("LABEL_LEG_TLT", labels.getTltLegend().getLabel());
			}

			/*
			 * REPORT INPUT DATA
			 */

			Integer intStatus = 0;
			intStatus = (int) reportInput.getCompositeStatusId();

			if (intStatus != null) // then there should be some data to
									// retrieve.
			{

				String[] DRIStatus = { "", "In Progress", "Ready for Editing", "Final" };
				ret.getDisplayData().put("DRIStatus", DRIStatus[intStatus]);

				// Readiness
				ret.getDisplayData().put("ReportedDevelopmentFocus", reportInput.getAlrReadinessFocus());
				Integer intRedi = 0;
				intRedi = (int) reportInput.getAlrReadinessRating();
				ret.getDisplayData().put("ReportedReadinessNumeric", intRedi.toString());
				String[] ReadinessRating = { "Insufficient Data", "Low", "Mixed", "Strong", "Very Strong" };
				ret.getDisplayData().put("ReportedReadinessText", ReadinessRating[intRedi]);
				ret.getDisplayData().put("ReportedReadinessOrganizationText",
						XMLUtils.suppressHtmlTags(reportInput.getAlrReadinessOrgText()));
				intRedi = 0;
				intRedi = (int) reportInput.getAlrSugReadinessRating();
				ret.getDisplayData().put("SuggestedReadinessNumeric", intRedi.toString());
				ret.getDisplayData().put("SuggestedReadinessText", ReadinessRating[intRedi]);
				ret.getDisplayData().put("ReadinessORAuth", reportInput.getAlrReadinessORAuth());
				ret.getDisplayData().put("ReadinessORJust", reportInput.getAlrReadinessORJust());

				// Development summary
				ret.getDisplayData().put("DevelopmentParticipantText",
						XMLUtils.suppressHtmlTags(reportInput.getDevelopmentPartText()));

				// Long Term Advancement Potential
				Integer intLongTermRating = 0;
				intLongTermRating = (int) reportInput.getLongtermRating();
				ret.getDisplayData().put("ReportedAdvancementNumeric", intLongTermRating.toString());

				ret.getDisplayData().put("ReportedAdvancementText", ReadinessRating[intLongTermRating]);

				// Cand fit index
				ret.getDisplayData().put("CalculatedFitIndex",
						new Integer(reportInput.getCandidateFitIndex()).toString());

				// Culture Fit
				Integer intCfit = 0;
				intCfit = (int) reportInput.getAlrCultureFitRating();
				ret.getDisplayData().put("ReportedCultureNumeric", intCfit.toString());
				String[] CultureFit = { "", "Low", "Mixed", "Strong", "Very Strong" };
				ret.getDisplayData().put("ReportedCultureText", CultureFit[intCfit]);
				ret.getDisplayData().put("ReportedCultureOrganizationText",
						XMLUtils.suppressHtmlTags(reportInput.getAlrCultureFitOrgText()));
				ret.getDisplayData().put("ReportedCultureParticipantText",
						XMLUtils.suppressHtmlTags(reportInput.getAlrCultureFitPartText()));
				intCfit = 0;
				intCfit = (int) reportInput.getAlrSugCultureFitRating();
				ret.getDisplayData().put("SuggestedCultureNumeric", intCfit.toString());
				ret.getDisplayData().put("SuggestedCultureText", CultureFit[intCfit]);
				ret.getDisplayData().put("CultureFitORAuth", reportInput.getAlrCultureFitORAuth());
				ret.getDisplayData().put("CultureFitORJust", reportInput.getAlrCultureFitORJust());

				// Reported Leadership Competencies
				Integer intLeadershipSkillRating = 0;
				intLeadershipSkillRating = (int) reportInput.getLeadershipSkillRating();
				ret.getDisplayData().put("ReportedCompetenciesNumeric", intLeadershipSkillRating.toString());
				String[] Numeric2Text = { "", "Weak", "Mixed", "Strong", "Very Strong" };
				ret.getDisplayData().put("ReportedCompetenciesText", Numeric2Text[intLeadershipSkillRating]);
				ret.getDisplayData().put("ReportedCompetenciesNeedsOrganizationText",
						XMLUtils.suppressHtmlTags(reportInput.getSkillsToDevelopOrgText()));
				ret.getDisplayData().put("ReportedCompetenciesNeedsParticipantText",
						XMLUtils.suppressHtmlTags(reportInput.getSkillsToDevelopPartText()));

				// Leadership Experience
				Integer intLeadershipExprienceRating = 0;
				intLeadershipExprienceRating = (int) reportInput.getLeadershipExperienceRating();
				ret.getDisplayData().put("ReportedExperienceNumeric", intLeadershipExprienceRating.toString());
				ret.getDisplayData().put("ReportedExperienceText", Numeric2Text[intLeadershipExprienceRating]);

				// Leadership Traits
				Integer intLeadershipStyleRating = 0;
				intLeadershipStyleRating = (int) reportInput.getLeadershipStyleRating();
				ret.getDisplayData().put("ReportedTraitsNumeric", intLeadershipStyleRating.toString());
				ret.getDisplayData().put("ReportedTraitsText", Numeric2Text[intLeadershipStyleRating]);

				// Leadership Drivers
				Integer intLeadershipInterestRating = 0;
				intLeadershipInterestRating = (int) reportInput.getLeadershipInterestRating();
				ret.getDisplayData().put("ReportedDriversNumeric", intLeadershipInterestRating.toString());
				ret.getDisplayData().put("ReportedDriversText", Numeric2Text[intLeadershipInterestRating]);

				// Derailment Risk
				Integer intDerailmentRating = 0;
				intDerailmentRating = (int) reportInput.getDerailmentRating();
				ret.getDisplayData().put("ReportedDerailmentNumeric", intDerailmentRating.toString());
				String[] DerailmentText = { "", "High", "Moderate", "Low", "Minimal" };
				ret.getDisplayData().put("ReportedDerailmentText", DerailmentText[intDerailmentRating]);

				ret.getDisplayData().put("IDP_DOC", reportInput.getIdpDocumentPath());
				ret.getDisplayData().put("IDP_IMG", reportInput.getIdpImagePath());

				if (reportInput.getIdpImagePath() != null && reportInput.getIdpImagePath().length() > 0) {
					ret.getDisplayData().put("IDP_IMAGE",
							"http://localhost:8080/pdi-web/upload/" + reportInput.getIdpImagePath());
				} else {
					ret.getDisplayData().put("IDP_IMAGE", "");
				}

			}
			

			// System.out.println(" doing abyd extract processing.... ");
			/*
				SOME OF THIS MAY BE REDUNDANT, BUT WAS PUT under DIFFERENT keys FOR
				THE EXTRACT THAN IT WAS FOR THE RPEORTS, SO IF THIS IS AN EXTRACT,
				WE'RE GOING TO GO THROUGH THESE MOTIONS.
				THE INSTRUMENT SCORES, WE NEVER GOT IN THE ABYD REPORTS SO MUST DO HERE
			 */

			// Get all the individual report scores.
			// one instrument per epi.... and keep adding
			// to the same individual report (ret)
			if (courses.contains(ReportingConstants.IC_WGE)) { //"wg"
				scoreWGEForExtract(ret, epi);
			}

			
			ret.getDisplayData().put("ParticipantFname", epi.getParticipant().getFirstName());
			ret.getDisplayData().put("ParticipantLname", epi.getParticipant().getLastName());
			ret.getDisplayData().put("ParticipantId", epi.getParticipant().getId());

			// Get the TLT and RI (Dashboard) report data
			// This also gets the dLCI (put out in IG data)
			ret = aeh.addDRIandTLT(ret, dnaId, pptId, reportInput);

			// Get the client competencies
			ret = aeh.addClientCompetencyScores(ret, pptId, dnaId, reportModelStructure);

			// Get the IR stuff
			
			ret = aeh.addIGDataforALR(ret, pptId, dnaId, cmpInternalNames);
			

			// Get the EG stuff
			ret = aeh.addEGDataforALR(ret, pptId, dnaId, cmpInternalNames);

			// Do we need externally scored stuff?
			boolean hasKfp = false;
			boolean hasKfar = false;
			hasKfp = HelperUtils.alpCheck(dnaId);
			if (!hasKfp) {
				hasKfar = HelperUtils.kfarCheck(dnaId);
			}
			// Get ALP stuff if needed
			if (hasKfp) {
				// We have an ALP... get the data
				ret = aeh.getAlrCompScoreData(ret, pptId, dnaId, cmpInternalNames,
						ReportingConstants.SRC_KFP);

				// Get ALP raw/scoring data
				ret = aeh.getAlpRawData(ret, pptId, projId);
			} else if (hasKfar) {
				// Get the KF4D data
				ret = aeh.getAlrCompScoreData(ret, pptId, dnaId, cmpInternalNames,
						ReportingConstants.SRC_KF4D);

				// Will we get the KF4D raw data?
				try {
					IGKf4dDataHelper kdh = new IGKf4dDataHelper(con, pptId, projId);
					ret = kdh.getKf4dRawData(ret, pptId, projId);
				} catch (Exception e) {
					throw new Exception(e);
				}

				finally {

				}
			} else if (HelperUtils.gpiCogsCheck(dnaId)) {
				ret = aeh.getGpiCogsCompScoreData(ret, pptId, dnaId, cmpInternalNames);
			}

			
			ret = aeh.addIrData(null, ret, pptId, dnaId);
			

			
	
			// Do the PDA stuff
			ret = (new PdaDataHelper()).populatePdaData(ret, pptId, projId, langCode);
	

			// Put out the names
			// Note that the keys will always be of the form "KFLA_COMPn_Name"
			// even on custom models
			Map<String, String> compNames = null;
	
			boolean isCustom = aeh.checkCustom(cmpInternalNames);
			if (isCustom) {
				// if a custom model call the name generator, else do the line
				// below
				compNames = aeh.getCustCompNames(dnaId);
			} else {
				compNames = aeh.getCompNamesWSyn(dnaId);
			}
			ret.getDisplayData().putAll(compNames);
			
			//TreeMap<String, String> treeMap = new TreeMap<String, String>(ret.getDisplayData());
			
			json = JSONUtils.convertObjectToJSON(ret);
			
			log.debug("IndividualReport JSON: {}", json);
			
			log.debug("Configuration Data size: {}", ret.getConfigurationData().size());
			log.debug("Label Data size: {}", ret.getLabelData().size());
			log.debug("Report Data size: {}", ret.getReportData().size());
			log.debug("Scripted Data size: {}", ret.getScriptedData().size());
			log.debug("Scripted Group Data size: {}", ret.getScriptedGroupData().size());
			
			
	
		} catch (Exception e) {
			System.out.println("getAlrExtractData(): Error fetching ALR extract data. Msg=" + e.getMessage());
			throw e;
		} finally {
			try {
				if (con != null) {
					con.close();
				}
	
			} catch (Exception se) {
				se.printStackTrace();
			}
		}
		
		return json;
	}
	
	/**
	 * scoreWGEGraphicalRpt is a method that scores data for the WATSON GLASER E
	 * graphical report
	 *
	 * it uses the scoring code from the do method, and makes it available to
	 * the extract report.
	 *
	 * @param ReportData
	 *            rd
	 * @param ProjectParticipantInstrument
	 *            epi
	 * @param SessionUser
	 *            sessionUser
	 */
	private void scoreWGEForExtract(IndividualReport ret, ProjectParticipantInstrument epi) {

		// System.out.println("scoreWGEForExtract...");
		try {
			ReportData rd = new ReportData();

			// get the normed score objects....
			HashMap<String, NormScore> gpPercentiles = epi.getScoreGroup().calculateGeneralPopulation();
			HashMap<String, NormScore> spPercentiles = epi.getScoreGroup().calculateSpecialPopulation();

			// get ratings from the NormScore objects

			// RDS Key=WGE_CT_GP_RATING
			Iterator<NormScore> iter = gpPercentiles.values().iterator();
			while (iter.hasNext()) {
				NormScore ns = iter.next();
				rd.getScoreData().put(ns.getNorm().getSpssValue() + "_GP_RATING", ns.toRating().toString());
			}

			// RDS Key=WGE_CT_GP_RATING
			iter = spPercentiles.values().iterator();
			while (iter.hasNext()) {
				NormScore ns = iter.next();
				rd.getScoreData().put(ns.getNorm().getSpssValue() + "_SP_RATING", ns.toRating().toString());
			}

			// Norm Names
			rd.getScoreData().put("WGE_CT_GP_NAME", epi.getScoreGroup().getGeneralPopulation() == null ? ""
					: epi.getScoreGroup().getGeneralPopulation().getName());
			rd.getScoreData().put("WGE_CT_SP_NAME", epi.getScoreGroup().getSpecialPopulation() == null ? ""
					: epi.getScoreGroup().getSpecialPopulation().getName());

			// raw scores / scale names
			Iterator<Score> iterScore = epi.getScoreGroup().getScores().values().iterator();
			while (iterScore.hasNext()) {
				Score score = iterScore.next();
				rd.getScoreData().put(score.getCode(), score.getRawScore());
				rd.getDisplayData().put(score.getCode() + "_SCALE_NAME", score.getScaleName());
			}

			// add to report data for individual report
			ret.getReportData().put(AbyDExtractFormatHelper.INST_CODE_TO_RC_NAME.get(epi.getInstrument().getCode()),
					rd);
		} catch (Exception e) {
			System.out.println("scoreWGEForExtract:");
			e.printStackTrace();
		}
	}
	
	
	@Override
	public String makeExtract(int participantId, int projectId, String reportCode) throws Exception {
		String extractJson = null;
		try {
			SessionUser su = new SessionUser();
			Project project = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated")
					.fromId(su, String.valueOf(projectId));
			Participant participant = HelperDelegate.getParticipantHelper("com.pdi.data.nhn.helpers.delegated")
					.fromId(su, String.valueOf(participantId));
			if (project == null || participant == null){
				throw new Exception("Invalid project ID (" + projectId + ") or participant ID: " + participantId);
			}
			IProjectParticipantInstrumentHelper epih = HelperDelegate
					.getProjectParticipantInstrumentHelper("com.pdi.data.v2.helpers.delegated");
			ArrayList<ProjectParticipantInstrument> epis = null;
			
			epis = epih.fromLastParticipantId(su,
					IProjectParticipantInstrumentHelper.MODE_FULL_COMPLETED, String.valueOf(participantId), 
					String.valueOf(projectId));
			
			ProjectParticipantInstrument ppiFinal = new ProjectParticipantInstrument();
			ppiFinal.setParticipant(participant);
			ppiFinal.setProject(project);
			
			List<String> courses = new ArrayList<String>();
			
			if (epis == null || epis.size() == 0){
				//if (epis == null)
					//epis = new ArrayList<ProjectParticipantInstrument>();
				//Instrument ii = new Instrument();
				//ii.setCode("bogus");
				//ProjectParticipantInstrument ppi = new ProjectParticipantInstrument();
				//ppiFinal.setInstrument(ii);
				courses.add("bogus");
				//epis.add(ppi);
			} else {
				for (ProjectParticipantInstrument ppi : epis){
					courses.add(ppi.getInstrument().getCode());
					
				}
			}
			
			IndividualReport ret = new IndividualReport();
			ret.getDisplayData().put("HYBRID_REPORT", "NO"); 
			ret.addDisplayData("PROJECT", project.getName());
			if (!courses.contains(ReportingConstants.IC_GPI) && !courses.contains(ReportingConstants.IC_GPI_SHORT)){

				ret.addDisplayData("GPI_MISSING", "MISSING"); 
			}
			if(!courses.contains(ReportingConstants.IC_LEI))
			{
				
				ret.addDisplayData("LEI_MISSING", "MISSING"); 
			}
			if(!courses.contains(ReportingConstants.IC_CS) && !courses.contains(ReportingConstants.IC_CS2))
			{
				
				ret.addDisplayData("CS_MISSING", "MISSING"); 
			}
			//This is used in every report.
			java.util.Date date = new java.util.Date();
			SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");

			ret.getDisplayData().put("TODAY_DATE", sdf.format(date));
			ret.getDisplayData().put("DATE", sdf.format(date));
			
			ret.setReportCode(reportCode);
			ret.setReportType(ReportingConstants.REPORT_TYPE_GROUP_EXTRACT);
			
			TextGroup tg = new TextGroup("NoTextCode", "en");
			
			extractJson =  this.getIndividualALRExtractDataAsJSON(ppiFinal, ret, "en", tg, courses);
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		}

		return extractJson;
		
	}

}
