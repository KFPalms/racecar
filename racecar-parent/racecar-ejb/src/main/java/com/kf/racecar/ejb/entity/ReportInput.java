package com.kf.racecar.ejb.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "pdi_abd_rpt_input")
@NamedQueries({ @NamedQuery(name = "findByDnaIdAndParticipantId", query = "SELECT input FROM ReportInput input WHERE input.dna.dnaId = :dnaId AND input.participantId = :participantId") })
@XmlRootElement(name = "reportInput")
@XmlAccessorType(XmlAccessType.FIELD)
public class ReportInput implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "inputId")
	@XmlAttribute
	private int inputId;
	
	@OneToOne
	@JoinColumn(name = "dnaId")
	@XmlTransient
	private DNA dna;
	
	@Column(name = "participantId")
	@XmlAttribute
	private int participantId;
	
	@Column(name = "candidateFitIndex")
	@XmlElement
	private int candidateFitIndex;
	
	@Column(name = "skillsToLeverageOrgText")
	@XmlElement
	private String skillsToLeverageOrgText;
	
	@Column(name = "skillsToLeveragePartText")
	@XmlElement
	private String skillsToLeveragePartText;
	
	@Column(name = "skillsToDevelopOrgText")
	@XmlElement
	private String skillsToDevelopOrgText;
	
	@Column(name = "skillsToDevelopPartText")
	@XmlElement
	private String skillsToDevelopPartText;
	
	@Column(name = "leadershipSkillOrgText")
	@XmlElement
	private String leadershipSkillOrgText;
	
	@Column(name = "leadershipSkillPartText")
	@XmlElement
	private String leadershipSkillPartText;
	
	@Column(name = "leadershipSkillRating")
	@XmlElement
	private int leadershipSkillRating;
	
	@Column(name = "leadershipExperienceOrgText")
	@XmlElement
	private String leadershipExperienceOrgText;
	
	@Column(name = "leadershipExperiencePartText")
	@XmlElement
	private String leadershipExperiencePartText;
	
	@Column(name = "leadershipExperienceRating")
	@XmlElement
	private int leadershipExperienceRating;
	
	@Column(name = "leadershipStyleOrgText")
	@XmlElement
	private String leadershipStyleOrgText;
	
	@Column(name = "leadershipStylePartText")
	@XmlElement
	private String leadershipStylePartText;
	
	@Column(name = "leadershipStyleRating")
	@XmlElement
	private int leadershipStyleRating;
	
	@Column(name = "leadershipInterestOrgText")
	@XmlElement
	private String leadershipInterestOrgText;
	
	@Column(name = "leadershipInterestPartText")
	@XmlElement
	private String leadershipInterestPartText;
	
	@Column(name = "leadershipInterestRating")
	@XmlElement
	private int leadershipInterestRating;
	
	@Column(name = "derailmentOrgText")
	@XmlElement
	private String derailmentOrgText;
	
	@Column(name = "derailmentPartText")
	@XmlElement
	private String derailmentPartText;
	
	@Column(name = "derailmentRating")
	@XmlElement
	private int derailmentRating;
	
	@Column(name = "longtermOrgText")
	@XmlElement
	private String longtermOrgText;
	
	@Column(name = "longtermRating")
	@XmlElement
	private int longtermRating;
	
	@Column(name = "fitOrgText")
	@XmlElement
	private String fitOrgText;
	
	@Column(name = "readinessOrgText")
	@XmlElement
	private String readinessOrgText;
	
	@Column(name = "developmentPartText")
	@XmlElement
	private String developmentPartText;
	
	@Column(name = "pivotalPartText")
	@XmlElement
	private String pivotalPartText;
	
	@Column(name = "additionalNotes")
	@XmlElement
	private String additionalNotes;
	
	@Column(name = "idpImagePath")
	@XmlElement
	private String idpImagePath;
	
	@Column(name = "idpDocumentPath")
	@XmlElement
	private String idpDocumentPath;
	
	@Column(name = "rateStatusId")
	@XmlElement
	private int rateStatusId;
	
	@Column(name = "rateLockDate")
	@XmlElement
	private Timestamp rateLockDate;
	
	@Column(name = "rateUnlockDate")
	@XmlElement
	private Timestamp rateUnlockDate;
	
	@Column(name = "oTxtStatusId")
	@XmlElement
	private int oTxtStatusId;
	
	@Column(name = "oTxtLockDate")
	@XmlElement
	private Timestamp oTxtLockDate;
	
	@Column(name = "oTxtUnlockDate")
	@XmlElement
	private Timestamp oTxtUnlockDate;
	
	@Column(name = "pTxtStatusId")
	@XmlElement
	private int pTxtStatusId;
	
	@Column(name = "pTxtLockDate")
	@XmlElement
	private Timestamp pTxtLockDate;
	
	@Column(name = "pTxtUnlockDate")
	@XmlElement
	private Timestamp pTxtUnlockDate;
	
	@Column(name = "lastUserId")
	@XmlElement
	private String lastUserId;
	
	@Column(name = "lastDate")
	@XmlElement
	private Timestamp lastDate;
	
	@Column(name = "alrReadinessOrgText")
	@XmlElement
	private String alrReadinessOrgText;
	
	@Column(name = "alrReadinessRating")
	@XmlElement
	private int alrReadinessRating;
	
	@Column(name = "alrSugReadinessRating")
	@XmlElement
	private int alrSugReadinessRating;
	
	@Column(name = "alrReadinessORAuth")
	@XmlElement
	private String alrReadinessORAuth;
	
	@Column(name = "alrReadinessORJust")
	@XmlElement
	private String alrReadinessORJust;
	
	@Column(name = "alrCultureFitOrgText")
	@XmlElement
	private String alrCultureFitOrgText;
	
	@Column(name = "alrCultureFitPartText")
	@XmlElement
	private String alrCultureFitPartText;
	
	@Column(name = "alrCultureFitRating")
	@XmlElement
	private int alrCultureFitRating;
	
	@Column(name = "alrSugCultureFitRating")
	@XmlElement
	private int alrSugCultureFitRating;
	
	@Column(name = "alrCultureFitORAuth")
	@XmlElement
	private String alrCultureFitORAuth;
	
	@Column(name = "alrCultureFitORJust")
	@XmlElement
	private String alrCultureFitORJust;

	public int getInputId() {
		return inputId;
	}

	public void setInputId(int inputId) {
		this.inputId = inputId;
	}

	public DNA getDna() {
		return dna;
	}

	public void setDna(DNA dna) {
		this.dna = dna;
	}

	public int getParticipantId() {
		return participantId;
	}

	public void setParticipantId(int participantId) {
		this.participantId = participantId;
	}

	public int getCandidateFitIndex() {
		return candidateFitIndex;
	}

	public void setCandidateFitIndex(int candidateFitIndex) {
		this.candidateFitIndex = candidateFitIndex;
	}

	public String getSkillsToLeverageOrgText() {
		return skillsToLeverageOrgText;
	}

	public void setSkillsToLeverageOrgText(String skillsToLeverageOrgText) {
		this.skillsToLeverageOrgText = skillsToLeverageOrgText;
	}

	public String getSkillsToLeveragePartText() {
		return skillsToLeveragePartText;
	}

	public void setSkillsToLeveragePartText(String skillsToLeveragePartText) {
		this.skillsToLeveragePartText = skillsToLeveragePartText;
	}

	public String getSkillsToDevelopOrgText() {
		return skillsToDevelopOrgText;
	}

	public void setSkillsToDevelopOrgText(String skillsToDevelopOrgText) {
		this.skillsToDevelopOrgText = skillsToDevelopOrgText;
	}

	public String getSkillsToDevelopPartText() {
		return skillsToDevelopPartText;
	}

	public void setSkillsToDevelopPartText(String skillsToDevelopPartText) {
		this.skillsToDevelopPartText = skillsToDevelopPartText;
	}

	public String getLeadershipSkillOrgText() {
		return leadershipSkillOrgText;
	}

	public void setLeadershipSkillOrgText(String leadershipSkillOrgText) {
		this.leadershipSkillOrgText = leadershipSkillOrgText;
	}

	public String getLeadershipSkillPartText() {
		return leadershipSkillPartText;
	}

	public void setLeadershipSkillPartText(String leadershipSkillPartText) {
		this.leadershipSkillPartText = leadershipSkillPartText;
	}

	public int getLeadershipSkillRating() {
		return leadershipSkillRating;
	}

	public void setLeadershipSkillRating(int leadershipSkillRating) {
		this.leadershipSkillRating = leadershipSkillRating;
	}

	public String getLeadershipExperienceOrgText() {
		return leadershipExperienceOrgText;
	}

	public void setLeadershipExperienceOrgText(String leadershipExperienceOrgText) {
		this.leadershipExperienceOrgText = leadershipExperienceOrgText;
	}

	public String getLeadershipExperiencePartText() {
		return leadershipExperiencePartText;
	}

	public void setLeadershipExperiencePartText(String leadershipExperiencePartText) {
		this.leadershipExperiencePartText = leadershipExperiencePartText;
	}

	public int getLeadershipExperienceRating() {
		return leadershipExperienceRating;
	}

	public void setLeadershipExperienceRating(int leadershipExperienceRating) {
		this.leadershipExperienceRating = leadershipExperienceRating;
	}

	public String getLeadershipStyleOrgText() {
		return leadershipStyleOrgText;
	}

	public void setLeadershipStyleOrgText(String leadershipStyleOrgText) {
		this.leadershipStyleOrgText = leadershipStyleOrgText;
	}

	public String getLeadershipStylePartText() {
		return leadershipStylePartText;
	}

	public void setLeadershipStylePartText(String leadershipStylePartText) {
		this.leadershipStylePartText = leadershipStylePartText;
	}

	public int getLeadershipStyleRating() {
		return leadershipStyleRating;
	}

	public void setLeadershipStyleRating(int leadershipStyleRating) {
		this.leadershipStyleRating = leadershipStyleRating;
	}

	public String getLeadershipInterestOrgText() {
		return leadershipInterestOrgText;
	}

	public void setLeadershipInterestOrgText(String leadershipInterestOrgText) {
		this.leadershipInterestOrgText = leadershipInterestOrgText;
	}

	public String getLeadershipInterestPartText() {
		return leadershipInterestPartText;
	}

	public void setLeadershipInterestPartText(String leadershipInterestPartText) {
		this.leadershipInterestPartText = leadershipInterestPartText;
	}

	public int getLeadershipInterestRating() {
		return leadershipInterestRating;
	}

	public void setLeadershipInterestRating(int leadershipInterestRating) {
		this.leadershipInterestRating = leadershipInterestRating;
	}

	public String getDerailmentOrgText() {
		return derailmentOrgText;
	}

	public void setDerailmentOrgText(String derailmentOrgText) {
		this.derailmentOrgText = derailmentOrgText;
	}

	public String getDerailmentPartText() {
		return derailmentPartText;
	}

	public void setDerailmentPartText(String derailmentPartText) {
		this.derailmentPartText = derailmentPartText;
	}

	public int getDerailmentRating() {
		return derailmentRating;
	}

	public void setDerailmentRating(int derailmentRating) {
		this.derailmentRating = derailmentRating;
	}

	public String getLongtermOrgText() {
		return longtermOrgText;
	}

	public void setLongtermOrgText(String longtermOrgText) {
		this.longtermOrgText = longtermOrgText;
	}

	public int getLongtermRating() {
		return longtermRating;
	}

	public void setLongtermRating(int longtermRating) {
		this.longtermRating = longtermRating;
	}

	public String getFitOrgText() {
		return fitOrgText;
	}

	public void setFitOrgText(String fitOrgText) {
		this.fitOrgText = fitOrgText;
	}

	public String getReadinessOrgText() {
		return readinessOrgText;
	}

	public void setReadinessOrgText(String readinessOrgText) {
		this.readinessOrgText = readinessOrgText;
	}

	public String getDevelopmentPartText() {
		return developmentPartText;
	}

	public void setDevelopmentPartText(String developmentPartText) {
		this.developmentPartText = developmentPartText;
	}

	public String getPivotalPartText() {
		return pivotalPartText;
	}

	public void setPivotalPartText(String pivotalPartText) {
		this.pivotalPartText = pivotalPartText;
	}

	public String getAdditionalNotes() {
		return additionalNotes;
	}

	public void setAdditionalNotes(String additionalNotes) {
		this.additionalNotes = additionalNotes;
	}

	public String getIdpImagePath() {
		return idpImagePath;
	}

	public void setIdpImagePath(String idpImagePath) {
		this.idpImagePath = idpImagePath;
	}

	public String getIdpDocumentPath() {
		return idpDocumentPath;
	}

	public void setIdpDocumentPath(String idpDocumentPath) {
		this.idpDocumentPath = idpDocumentPath;
	}

	public int getRateStatusId() {
		return rateStatusId;
	}

	public void setRateStatusId(int rateStatusId) {
		this.rateStatusId = rateStatusId;
	}

	public Timestamp getRateLockDate() {
		return rateLockDate;
	}

	public void setRateLockDate(Timestamp rateLockDate) {
		this.rateLockDate = rateLockDate;
	}

	public Timestamp getRateUnlockDate() {
		return rateUnlockDate;
	}

	public void setRateUnlockDate(Timestamp rateUnlockDate) {
		this.rateUnlockDate = rateUnlockDate;
	}

	public int getoTxtStatusId() {
		return oTxtStatusId;
	}

	public void setoTxtStatusId(int oTxtStatusId) {
		this.oTxtStatusId = oTxtStatusId;
	}

	public Timestamp getoTxtLockDate() {
		return oTxtLockDate;
	}

	public void setoTxtLockDate(Timestamp oTxtLockDate) {
		this.oTxtLockDate = oTxtLockDate;
	}

	public Timestamp getoTxtUnlockDate() {
		return oTxtUnlockDate;
	}

	public void setoTxtUnlockDate(Timestamp oTxtUnlockDate) {
		this.oTxtUnlockDate = oTxtUnlockDate;
	}

	public int getpTxtStatusId() {
		return pTxtStatusId;
	}

	public void setpTxtStatusId(int pTxtStatusId) {
		this.pTxtStatusId = pTxtStatusId;
	}

	public Timestamp getpTxtLockDate() {
		return pTxtLockDate;
	}

	public void setpTxtLockDate(Timestamp pTxtLockDate) {
		this.pTxtLockDate = pTxtLockDate;
	}

	public Timestamp getpTxtUnlockDate() {
		return pTxtUnlockDate;
	}

	public void setpTxtUnlockDate(Timestamp pTxtUnlockDate) {
		this.pTxtUnlockDate = pTxtUnlockDate;
	}

	public String getLastUserId() {
		return lastUserId;
	}

	public void setLastUserId(String lastUserId) {
		this.lastUserId = lastUserId;
	}

	public Timestamp getLastDate() {
		return lastDate;
	}

	public void setLastDate(Timestamp lastDate) {
		this.lastDate = lastDate;
	}

	public String getAlrReadinessOrgText() {
		return alrReadinessOrgText;
	}

	public void setAlrReadinessOrgText(String alrReadinessOrgText) {
		this.alrReadinessOrgText = alrReadinessOrgText;
	}

	public int getAlrReadinessRating() {
		return alrReadinessRating;
	}

	public void setAlrReadinessRating(int alrReadinessRating) {
		this.alrReadinessRating = alrReadinessRating;
	}

	public int getAlrSugReadinessRating() {
		return alrSugReadinessRating;
	}

	public void setAlrSugReadinessRating(int alrSugReadinessRating) {
		this.alrSugReadinessRating = alrSugReadinessRating;
	}

	public String getAlrReadinessORAuth() {
		return alrReadinessORAuth;
	}

	public void setAlrReadinessORAuth(String alrReadinessORAuth) {
		this.alrReadinessORAuth = alrReadinessORAuth;
	}

	public String getAlrReadinessORJust() {
		return alrReadinessORJust;
	}

	public void setAlrReadinessORJust(String alrReadinessORJust) {
		this.alrReadinessORJust = alrReadinessORJust;
	}

	public String getAlrCultureFitOrgText() {
		return alrCultureFitOrgText;
	}

	public void setAlrCultureFitOrgText(String alrCultureFitOrgText) {
		this.alrCultureFitOrgText = alrCultureFitOrgText;
	}

	public String getAlrCultureFitPartText() {
		return alrCultureFitPartText;
	}

	public void setAlrCultureFitPartText(String alrCultureFitPartText) {
		this.alrCultureFitPartText = alrCultureFitPartText;
	}

	public int getAlrCultureFitRating() {
		return alrCultureFitRating;
	}

	public void setAlrCultureFitRating(int alrCultureFitRating) {
		this.alrCultureFitRating = alrCultureFitRating;
	}

	public int getAlrSugCultureFitRating() {
		return alrSugCultureFitRating;
	}

	public void setAlrSugCultureFitRating(int alrSugCultureFitRating) {
		this.alrSugCultureFitRating = alrSugCultureFitRating;
	}

	public String getAlrCultureFitORAuth() {
		return alrCultureFitORAuth;
	}

	public void setAlrCultureFitORAuth(String alrCultureFitORAuth) {
		this.alrCultureFitORAuth = alrCultureFitORAuth;
	}

	public String getAlrCultureFitORJust() {
		return alrCultureFitORJust;
	}

	public void setAlrCultureFitORJust(String alrCultureFitORJust) {
		this.alrCultureFitORJust = alrCultureFitORJust;
	}
}
