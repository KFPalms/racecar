package com.kf.racecar.ejb.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
@Embeddable
public class ImpactRatingPK implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Column(name="modelId",insertable=false, updatable=false)
	private int modelId;
	
	@Column(name="moduleId", insertable=false, updatable=false)
	private int moduleId;
	
	@Column(name="irSequence", insertable=false, updatable=false)
	private int irSequence;

	public int getModelId() {
		return modelId;
	}

	public void setModelId(int modelId) {
		this.modelId = modelId;
	}

	public int getModuleId() {
		return moduleId;
	}

	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}

	public int getIrSequence() {
		return irSequence;
	}

	public void setIrSequence(int irSequence) {
		this.irSequence = irSequence;
	}

}
