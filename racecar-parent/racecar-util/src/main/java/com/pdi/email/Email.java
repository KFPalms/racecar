/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.email;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Port of the Email utilities written by Mike Solomonson
 * 
 * This class wraps a number of data items in one place for use in sending e-mails
 * 
 *
 * @author		Gavin Myers
 */
public class Email
{
	//
	//
	// Static data.
	//
	
	// E-mail types (used in ProjectAdministrator)
	public static final String INITIAL = "FIRST_PASSWORD";
	public static final String REMINDER = "PARTICIPANT_REMINDER";
	public static final String ADMIN_PW_RESET = "PASSWORD_RESET";
	
	// Substitution variables
	public static final String SV_P_FIRST = "[partFirstName]";
	public static final String SV_P_LAST = "[partLastName]";
	public static final String SV_P_EMAIL = "[partEmailAddress]";
	public static final String SV_P_UID = "[partId]";
	public static final String SV_P_PW = "[partPassword]";
	public static final String SV_P_OPT1 = "[partOpt1]";
	public static final String SV_P_OPT2 = "[partOpt2]";
	public static final String SV_P_OPT3 = "[partOpt3]";
	public static final String SV_MGR_NAME = "[mgrName]";
	public static final String SV_MGR_EMAIL = "[mgrEmail]";
	public static final String SV_PROJECT = "[projectName]";
	public static final String SV_CLIENT = "[clientName]";
	
	public static final String SV_PART_URL = "[partUrl]";
	// Reset removed per TLT Standup meeting 3/9/11
	// Per Damon on 4/7/11, the IIS side is the only place where this should
	// be used.We will, therefore, leave it as unsubstituted for now.
	//public static final String SV_PW_RESET = "[resetURL]";


	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _from = "";
	private String _to = "";
	private String _subject = "";
	private String _message = "";
	private ArrayList<EmailAttachment> _attachments = new ArrayList<EmailAttachment>();
	private HashMap<String, String> _variables = new HashMap<String, String>();

	//
	// Constructors.
	//

	/**
	 * "Standard" (no-parameter) constructor
	 */
	public Email()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//
	
	/**
	 * Generic toString method
	 */
	public String toString()
	{
		String str = "\n --- Email ---";
		str += "\nFrom:         " + _from;
		str += "\nTo:           " + _to;
		str += "\nSubject:      " + _subject;
		str += "\nBody:         " + _message;
		str += "\nAttachments:  ";
		if (_attachments == null)
		{
			str += "\nNo attachments present";
		}
		else
		{
			for (EmailAttachment attachment : _attachments)
			{
				attachment.toString();
			}
		}
		str += "\nVariables:    ";
		if (_variables == null)
		{
			str += "\nNo variables present";
		}
		else
		{
			for (Iterator<Map.Entry<String, String>> itr = _variables.entrySet().iterator(); itr.hasNext(); )
			{
				Map.Entry<String, String> entry = itr.next();
				str += "\n" + entry.getKey() + "=" + entry.getValue();
			}
		}

		return str;
	}


	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
	
	/*****************************************************************************************/
	public void setFrom(String value)
	{
		_from = value;
	}
	
	public String getFrom()
	{
		return _from;
	}
	
	/*****************************************************************************************/
	public void setTo(String value)
	{
		_to = value;
	}
	
	public String getTo()
	{
		return _to;
	}
	
	/*****************************************************************************************/
	public void setSubject(String value)
	{
		_subject = value;
	}
	
	public String getSubject()
	{
		return _subject;
	}
	
	/*****************************************************************************************/
	public void setMessage(String value)
	{
		_message = value;
	}
	
	public String getMessage()
	{
		return _message;
	}
	
	/*****************************************************************************************/
	public void setAttachments(ArrayList<EmailAttachment> value)
	{
		_attachments = value;
	}
	
	public ArrayList<EmailAttachment> getAttachments()
	{
		return _attachments;
	}
	
	/*****************************************************************************************/
	public void setVariables(HashMap<String, String> value)
	{
		_variables = value;
	}
	
	public HashMap<String, String> getVariables()
	{
		return _variables;
	}
}
