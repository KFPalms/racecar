/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.email;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.*;

import com.pdi.logging.LogWriter;
//import javax.mail.util.ByteArrayDataSource;

/**
 * Port of the Email utilities written by Mike Solomonson
 *
 * @author		Gavin Myers
 */
public class EmailUtils
{

	//
	// Static data.
	//
	// Moved this out of here as all of this information is associated with the
	// substitution variables for the particular product and not with the
	// underlying generic send code.  For now, the substitution variables are
	// defined in the Email object.
	// These definitions were specifivc to V# and they have been moved there.
	//public static final String ID = "[PARTICIPANT_ID]";
	//public static final String FIRST_NAME = "[PARTICIPANT_FIRST_NAME]";
	//public static final String LAST_NAME = "[PARTICIPANT_LAST_NAME]";
	//public static final String EMAIL_ADDRESS = "[PARTICIPANT_EMAIL_ADDRESS]";
	//public static final String ORGANIZATION = "[COMPANY_NAME]";
	//public static final String PROJECT_ID = "[PROJECT_ID]";
	//public static final String PROJECT_NAME = "[PROJECT_NAME]";
	//public static final String PART_EXP_URL = "[PARTICIPANT_EXPERIENCE_URL]";


	//
	// Static methods.
	//

	/**
	 * Send an email
	 * @param email the email (to/from/subject/message)
	 * @param server the local server to send through
	 */
	public static void send(Email email, EmailServer server) throws Exception {
				
    Properties props = new Properties();
    props.put("mail.smtp.host", server.getAddress());

    Session session = Session.getInstance(props);

    String subject = email.getSubject();
		String message = email.getMessage();
		try {
			LogWriter.logBasic(LogWriter.INFO, EmailUtils.class, "EmailUtils beginning string substitution");
			for(String key : email.getVariables().keySet())
			{
				// Skip the substitution if the value is null
				if (email.getVariables().get(key) == null)
					continue;
				subject = subject.replace(key, email.getVariables().get(key));
				message = message.replace(key, email.getVariables().get(key));
			}		
		}catch (Exception e) {
			LogWriter.logBasic(LogWriter.ERROR, EmailUtils.class, "EmailUtils string substitution FAILURE.");
		}
		
  
		MimeMessage msg = new MimeMessage(session);
		LogWriter.logBasic(LogWriter.INFO, EmailUtils.class, "EmailUtils setting msg properties");
		
		msg.setFrom(new InternetAddress(email.getFrom()));
		InternetAddress[] address = {new InternetAddress(email.getTo())};
		msg.setRecipients(Message.RecipientType.TO, address);
		// Add explicit encoding to the subject line
		msg.setSubject(subject, "UTF-8");
		msg.setSentDate(new Date());
    
		LogWriter.logBasic(LogWriter.INFO, EmailUtils.class, "EmailUtils mime and content type setters");
		MimeBodyPart messagePart = new MimeBodyPart();
		// Added explicit encoding parameters
        messagePart.setText(message, "UTF-8");
        messagePart.setHeader("Content-Type","text/plain; charset=\"utf-8\"");
        messagePart.setHeader("Content-Transfer-Encoding", "base64");

        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messagePart);
		
        //Add the multipard data
        /*
         * Moving from JDK 1.6 to 1.5 caused this to break with the error msg:
         * Description	Resource	Path	Location	Type
			The type javax.activation.DataSource cannot be resolved. It is indirectly referenced from required .class files	EmailUtils.java	pdi-util/src/com/pdi/email	line 1	Java Problem
			
			Description	Resource	Path	Location	Type
			The project was not built since its build path is incomplete. Cannot find the class file for javax.activation.DataSource. Fix the build path then try building this project	pdi-util		Unknown	Java Problem

		Sounds like a compile issue... needs to be resolved but not important right now
				
        for(EmailAttachment attachment : email.getAttachments()) {
        	MimeBodyPart attachmentPart = new MimeBodyPart();
        	ByteArrayDataSource dataSource = new ByteArrayDataSource(attachment.getData().toByteArray(), attachment.getName()) {
        		//@Override
        		public String getContentType() {
        			return "application/octet-stream";
        			}
        		};

    		attachmentPart.setDataHandler(new DataHandler(dataSource));
    		attachmentPart.setFileName(attachment.getName());
        	multipart.addBodyPart(attachmentPart);
        }
        
        msg.setContent(multipart);
        */
        
        msg.setContent(multipart);

        //System.out.println("Email Sent");
        LogWriter.logBasic(LogWriter.INFO, EmailUtils.class, "EmailUtils now trying Transport.send");
      try {
      	LogWriter.logBasic(LogWriter.INFO, EmailUtils.class, "Sending email: \"" + email.getSubject() + "\", to: [" + email.getTo() + "]");
    		Transport.send(msg);
      }catch (Exception e) {
				LogWriter.logBasic(LogWriter.ERROR, EmailUtils.class, "EmailUtils Transport send failure for email \"" + email.getSubject() + "\", to: [" + email.getTo() + 
						 "], server " + server.getAddress());
			}  	
        
	}
	
	/**
	 * Sends a batch of emails through one session
	 * @param emails
	 * @param server
	 
	public static void send(ArrayList<Email> emails, EmailServer server) {
        Properties props = new Properties();
        props.put("mail.smtp.host", server.getAddress());
        
        Session session = Session.getInstance(props);
        
		for(Email email : emails) {
			EmailUtils.send(email, session);
		}
	}
	*/


	//
	// Instance data.
	//

	//
	// Constructors.
	//
	
	//
	// Instance methods.
	//

}
