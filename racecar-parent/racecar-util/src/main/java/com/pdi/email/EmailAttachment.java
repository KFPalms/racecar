/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.email;

import java.io.ByteArrayOutputStream;

/**
 * A container class for an e-mail attachment
 *
 * @author		Gavin Myers
 */
public class EmailAttachment
{
	//
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String _name;
	private ByteArrayOutputStream _data;

	//
	// Constructors.
	//

	/**
	 * "Standard" (no-parameter) constructor
	 */
	public EmailAttachment()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//
	
	/**
	 * Generic toString method
	 */
	public String toString()
	{
		String str = "\n --- EmailAttachment ---";
		str += "\nName:     " + _name;
		str += "\nContent:  " + _data.toString();

		return str;
	}


	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
	
	/*****************************************************************************************/
	public void setName(String value)
	{
		_name = value;
	}
	
	public String getName()
	{
		return _name;
	}
	
	/*****************************************************************************************/
	public void setData(ByteArrayOutputStream value)
	{
		_data = value;
	}
	
	public ByteArrayOutputStream getData()
	{
		return _data;
	}
}
