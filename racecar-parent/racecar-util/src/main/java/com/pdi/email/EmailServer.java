/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.email;
/**
 * Port of the Email utilities written by Mike Solomonson
 * 
 *
 * @author		Gavin Myers
 */
public class EmailServer {
	private String address;

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddress() {
		return address;
	}
}
