package com.pdi.encryption;

/**
 * Encrytps/Decrypts based off the public/private keys or your own key
 * Usage:
 	String message = "It was a bright cold day in April, and the clocks were striking thirteen.";
 	String result = EncryptionUtils.encryptPublic(message);
 	String translated = EncryptionUtils.decryptPublic(result);
 *	
 *	
 *
 * @author gmyers
 *
 */
public class EncryptionUtils {

	//Public key used by the flex/java side
	private static String publicKey = "frusAdAdr4sp4br3ph6butucruxEr9Ruy8QAkatrutrudrutrest9de4w9n9pReFFrATrExufraq6CReDrUspurErudRUJasturacubra4espapudeQ2sechebreGAxe";
	
	//Private key used by the java side only
	private static String privateKey = "9BgTiDxVV2fICj8YdKvpCM5OxTZYRcCRSGlJE89LcnN7Z5ZJFcIvPVNSgN0LQ28JJUt1J2lQQvmb9zZOaewwEN4TWXEtRPuzB1gdeVhGIhDxZqoQzLVQRgpY9e9iMM6s";
	
	
	/**
	 * Encrypts a message with the private key
	 * @param input the message
	 * @return
	 */
	public static String encryptPrivate(String input) {
		return EncryptionUtils.encrypt(input, EncryptionUtils.privateKey);
	}
	
	/**
	 * Encrypts a message with the public key
	 * @param input the message
	 * @return
	 */
	public static String encryptPublic(String input) {
		return EncryptionUtils.encrypt(input, EncryptionUtils.publicKey);
	}
	
	/**
	 * Encrypts a message
	 * @param input the message
	 * @param key the key
	 * @return
	 */
	public static String encrypt(String input, String key) {
		//@TODO This currently uses RC4 encryption which is not all entirely secure, this should use AES
   	 	RC4 ownRc4 = new RC4(key.getBytes());
	 	byte[] result_own = ownRc4.rc4(input.getBytes()); 
		return EncryptionUtils.bytesToHex(result_own).toUpperCase();
	}
	
	/**
	 * Decrypts a message with the private key
	 * @param input the message
	 * @return
	 */
	public static String decryptPrivate(String input) {
		return EncryptionUtils.decrypt(input, EncryptionUtils.privateKey);
	}
	
	/**
	 * Decrypts a message with the public key
	 * @param input the message
	 * @return
	 */
	public static String decryptPublic(String input) {
		return EncryptionUtils.decrypt(input, EncryptionUtils.publicKey);
	}
	
	/**
	 * Decrypts a message
	 * @param input the message
	 * @param key the key
	 * @return
	 */
	public static String decrypt(String input, String key) {
   	 	RC4 ownRc4 = new RC4(key.getBytes());
	 	byte[] result_own = ownRc4.rc4(EncryptionUtils.hexToBytes(input)); 
		return new String(result_own);
	}
	
	/**
     * Turns array of bytes into string
     *
     * @param buf	Array of bytes to convert to hex string
     * @return	Generated hex string
     */
     private static String bytesToHex (byte buf[]) {
      StringBuffer strbuf = new StringBuffer(buf.length * 2);
      int i;

      for (i = 0; i < buf.length; i++) {
       if (((int) buf[i] & 0xff) < 0x10)
	    strbuf.append("0");

       strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
      }

      return strbuf.toString();
     }

     public static byte[] hexToBytes(char[] hex) {
       int length = hex.length / 2;
       byte[] raw = new byte[length];
       for (int i = 0; i < length; i++) {
         int high = Character.digit(hex[i * 2], 16);
         int low = Character.digit(hex[i * 2 + 1], 16);
         int value = (high << 4) | low;
         if (value > 127)
           value -= 256;
         raw[i] = (byte) value;
       }
       return raw;
     }

     public static byte[] hexToBytes(String hex) {
       return hexToBytes(hex.toCharArray());
     }

     /*
     public static void main(String[] args) throws Exception {
    	 String message = "It was a bright cold day in April, and the clocks were striking thirteen.";
    	 String result = EncryptionUtils.encryptPublic(message);
    	 String translated = EncryptionUtils.decryptPublic(result);
    	 
    	 System.out.println(message);
    	 System.out.println(result);
    	 System.out.println(translated);
       
     }
     */

}
