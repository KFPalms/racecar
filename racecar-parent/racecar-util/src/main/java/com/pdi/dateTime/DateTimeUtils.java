/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.dateTime;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * The DateTimeUtils class provides a place to put miscellaneous
 * utility methods date- and time-related methods valid for use
 * in more than one application or application family.
 *
 * @author		Ken Beukelman
 */
public class DateTimeUtils
{
	//
	// Static data.
	//
//	private static String CONTEXT_DB_RESOURCE = "jdbc/hra_abd";
//	
//	//Gavin added (see comments below)
//	private static String CONTEXT_DB_RESOURCE_GENERIC = "jdbc/hra_";
//	
	// This is the number of days from 1/1/1900 to 1/1/1970.
	// It is used to calculate the MS format date
	private static final float DAYS_OFFSET = 25569.0f;
	
	//
	// Static methods.
	//
	
	/*
	 *  This method does the heavy lifting of calculating an HRA
	 *  date.  You set up a GregorianCalendar with the right time
	 *  zone and the right time there, then pass it into here for calculations
	 *  
	 *  @param gc a Gregorian Calendar object that has been initialized
	 *            with the time zone and the date in that timezone
	 *  @return a Sting formatted HRA date
	 */
	static private double getHraTimestampFromCalendar(GregorianCalendar gc)
	{
		TimeZone tz = gc.getTimeZone();
		
		long now = gc.getTimeInMillis();
		
		// Adjust the time stamp by the differential to UTC (neg number)
		now += tz.getOffset(now);
	
		// Get the number of days since the Epoch
		// (divide by the number of milliseconds in a day)
		double edays = ((double) now) / 86400000.0d;
	
		// Add the days from 1/1/1900 to 1/1/1970 (the "Epoch")
		//edays += 25569.00000d;
		edays += (double)DAYS_OFFSET;

		// Adjust the output to 5 significant digits
		long bigInt = (long) ((edays * 100000.0d) + .5d);
		edays = (double)bigInt / 100000.0d;

		return edays;
	}

	
// This is the original createHraDate method, preserved just in case	
//	/*
//	 *  createHRADate is a private method that creates a double containing
//	 *  the current time in HRA date format.  Its primary use is for
//	 *  creating dates for use in V2 tables, but it is also used to generate
//	 *  anonymous (numeric digit) keys for linkparams.
//	 *  
//	 *  Note that this signature sends back a double, which is not the
//	 *  desired format for use in the database.
//	 *  
//	 *  @return a double containing an HRA date
//	 */
//	static private double createHRADate()
//	{
//		// Set the time zone.  We use that of our Montreal Partners to
//		// avoid confusion with other similar dates in the database. 
//		TimeZone tz = TimeZone.getTimeZone("America/Montreal");
//		
//		// Create a calendar so we gan get a current timestamp
//		// (the number od millisecs since midnight, 1/1/1970)
//		GregorianCalendar gc = new GregorianCalendar(tz);
//		long now = gc.getTimeInMillis();
//		
//		// Adjust the time stamp by the differential to UTC (neg number)
//		now += tz.getOffset(now);
//		
//		// Get the number of days since the Epoch
//		// (divide by the number of milliseconds in a day)
//		double edays = ((double) now) / 86400000.0d;
//		
//		// Add the days from 1/1/1900 to 1/1/1970 (the "Epoch")
//		edays += 25570.00000d; //<------ Note that this is too big by 1
//	
//		// Adjust the output to 5 significant digits
//		long bigInt = (long) ((edays * 100000.0d) + .5d);
//		edays = (double)bigInt / 100000.0d;
//		
//		return edays;
//	}


	/*
	 *  createHRADate is a private method that creates a double containing
	 *  the current time in HRA date format.  Its primary use is for
	 *  creating dates for use in V2 tables, but it is also used to generate
	 *  anonymous (numeric digit) keys for linkparams.
	 *  
	 *  Note that this signature sends back a double, which is not the
	 *  desired format for use in the database.
	 *  
	 *  @return a double containing an HRA date
	 */
	public static double createHRADate()
	{
		// Set the time zone.  We use that of our Montreal Partners to
		// avoid confusion with other similar dates in the database. 
		TimeZone tz = TimeZone.getTimeZone("America/Montreal");
		
		// Create a calendar so we gan get a current timestamp
		// (the number od millisecs since midnight, 1/1/1970)
		GregorianCalendar gc = new GregorianCalendar(tz);

		return getHraTimestampFromCalendar(gc);
	}
	
	
	/*
	 *  getHRADateString is a convenience method that returns an
	 *  HRA V2 date as a string.  It calls the date method, then
	 *  casts the result to a String.  The output from this
	 *  method is what would actually be used to get the string
	 *  used in the V2 database.  The date/time is from the
	 *  system clock.
	 *  
	 *  @return a Sting formatted HRA date
	 */
	static public String getHRADateString()
	{
		return Double.toString(createHRADate());
	}


	/*
	 * Another convenience method allowing the user to determine the
	 * time zone and the date and time that is to be used.
	 */
	static public String getHRADateString(TimeZone tz,Date date)
	{
		// Create a calendar with the desired time zone, then set it up with
		// a Date object initialized to a time in that time zone.  Example:
		// PSI dates are Pacific Time.  If we create a Date object using a
		// PSI time string and set the time zone to Pacific time, the time
		// stamp should be compatible with the times that we use in the database
		
		GregorianCalendar gc = new GregorianCalendar(tz);
		gc.setTime(date);
		
		return Double.toString(getHraTimestampFromCalendar(gc));
	}
	

	/*
	 *  getNormalDateStringFromHRADate changes a HRA Date String into a Java Data object.
	 *   
	 *  @return a Date
	 */
	static public Date getNormalDateStringFromHRADate(String dateIn)
		throws Exception
	{
		// This was the code supplied to me with 25570 as the number of days from 1/1/1900 to 1/1/1970.
		// However it was producing a date that one earlier than the correct date...at least in my tests.
		// So changed it to 25569.
		//float since70 = Float.parseFloat(dateIn) - 25570f; // Subtract the number of days from 1/1/1900 to 1/1/1970
		//float since70 = Float.parseFloat(dateIn) - 25569f; // Subtract the number of days from 1/1/1900 to 1/1/1970
		float since70 = Float.parseFloat(dateIn) - DAYS_OFFSET; // Subtract the number of days from 1/1/1900 to 1/1/1970
		if (since70 < 0)
		{
			throw new Exception("Date conversion cannot handle dates before 1/1/1970. Date = " + dateIn);
		}

		long ts = (long) ((since70 * 86400000f) + .5f);
		TimeZone tz = TimeZone.getTimeZone("America/Montreal");
		GregorianCalendar gc = new GregorianCalendar(tz);
		gc.setTime(new Date(ts-tz.getOffset(ts)));  // Adjust by the differential to UTC
		return gc.getTime();
	}	

	
	
	//
	// Instance data.
	//
	
	//
	// Constructors.
	//

	//
	// Instance methods.
	//
}
