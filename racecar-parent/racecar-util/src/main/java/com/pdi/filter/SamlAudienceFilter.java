package com.pdi.filter;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.stream.XMLStreamException;






import com.oneLogin.AccountSettings;
import com.oneLogin.AppSettings;
import com.oneLogin.saml.AuthRequest;
import com.pdi.properties.PropertyLoader;

/**
 * Servlet Filter implementation class OneLoginAudienceFilter
 */

public class SamlAudienceFilter implements Filter {

	private final String idpurl = "https://login.microsoftonline.com/e9d21387-43f1-4e06-a253-f9ed9096dc48/saml2";
    /**
     * Default constructor. 
     */
    public SamlAudienceFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		System.out.println("In OneLoginAudienceFilter with new Request");
		HttpServletResponse resp = (HttpServletResponse) response;
		HttpServletRequest req = (HttpServletRequest) request;
		
		/* BEGIN COPIED FROM CHECK COOKIE FILTER */
		String requestUrl = ((HttpServletRequest)request).getRequestURL().toString();
		String queryString = "";
		String returnUrl = "";
		boolean tokenResponse = false;
		if (request instanceof HttpServletRequest) {
			if (requestUrl.contains("token")){
				returnUrl = req.getHeader("scoreKeeperUrl");
				tokenResponse = true;
				if (returnUrl == null){
					System.out.println("scoreKeeperUrl header was not found.  Trying querystring.");
					returnUrl = req.getParameter("scoreKeeperUrl");
					if (returnUrl == null){
						resp.sendError(500, "Scorekeeper url not provided.");
						return;
					}
				}
			} else {
				returnUrl = ((HttpServletRequest)request).getRequestURL().toString();
				queryString = "?" + ((HttpServletRequest)request).getQueryString();
			}
			
		}
		System.out.println("Return URL: " + returnUrl);
		System.out.println("Query String: " + queryString);
		System.out.println("Token Response: " + tokenResponse);
		HttpSession session = req.getSession(true);
		if (session == null){
			System.out.println("Session is NULL");
		}else{
			System.out.println("Session is NOT NULL");
			if (tokenResponse){
				session.setAttribute("tokenResponse", tokenResponse);
				session.setAttribute("q", returnUrl);
			} else {
				session.setAttribute("q", "loc="+returnUrl + queryString);
			}
		}
		/* END COPIED FROM CHECK COOKIE FILTER */
		// the appSettings object contain application specific settings used by the SAML library
		  AppSettings appSettings = new AppSettings();
		  
		// set the URL of the consume.jsp (or similar) file for this app. The SAML Response will be posted to this URL
		  
		  String url = request.getServerName() + ":" + request.getServerPort() + req.getContextPath();

		  String protocol = "http://";
		  if (request.isSecure()){
			  protocol = "https://";
		  }
		 
		  appSettings.setAssertionConsumerServiceUrl(protocol + url + "/sso/consumer/");
		 
		  System.out.println("Set ConsumerServiceUrl: {} "+appSettings.getAssertionConsumerServiceUrl());
		  
		  appSettings.setIssuer(PropertyLoader.getProperty("application", "SAML.issuer"));
		  System.out.println("Set Issuer: {} "+ appSettings.getIssuer());
		  
		  // the accSettings object contains settings specific to the users account. 
		  // At this point, your application must have identified the users origin
		  AccountSettings accSettings = new AccountSettings();
		  
		  // The URL at the Identity Provider where to the authentication request should be sent
		  //OneLogn -- goodbye
		 // accSettings.setIdpSsoTargetUrl("https://app.onelogin.com/trust/saml2/http-post/sso/350444");
		  accSettings.setIdpSsoTargetUrl(idpurl);
		  
		  // Generate an AuthRequest and send it to the identity provider
		  AuthRequest authReq = new AuthRequest(appSettings, accSettings);
		  
		//  String reqString = null;
		try {
			//reqString = accSettings.getIdp_sso_target_url()+"?SAMLRequest=" + URLEncoder.encode(authReq.getRequest(AuthRequest.base64),"UTF-8");
			//log.debug("Request String: {}", reqString);
			this.buildSamlForm(resp, req, authReq);
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			 System.out.println("Got Error building requestString: {} "+ e.getMessage());
			e.printStackTrace();
		}
		

	}
	
	private void buildSamlForm(HttpServletResponse response, HttpServletRequest req, AuthRequest request) throws XMLStreamException {
		
		try {
			String authnRequest = request.getAzureRequest(request.base64);
			req.setAttribute("authNRequest", authnRequest);
			req.setAttribute("action", idpurl);
			req.getRequestDispatcher("/samlFormPost.jsp").forward(req, response);
		} catch (XMLStreamException e) {
			//log.error("Caught XMLStreamException: {}", e);
			throw e;
		} catch (IOException io){
			//log.error("Caught IOException: {}", io);
			io.printStackTrace();
		} catch (ServletException s){
			//log.error("Caught ServletException: {}", s);
			s.printStackTrace();
		}
		
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
