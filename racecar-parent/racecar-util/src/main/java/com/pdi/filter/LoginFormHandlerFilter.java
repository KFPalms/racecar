package com.pdi.filter;

import java.io.IOException;



import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;




/**
 * Servlet Filter implementation class OneLoginAudienceFilter
 */

public class LoginFormHandlerFilter implements Filter {

    /**
     * Default constructor. 
     */
    public LoginFormHandlerFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		
		HttpServletResponse resp = (HttpServletResponse) response;
		HttpServletRequest req = (HttpServletRequest) request;
		HttpSession session = req.getSession();

		System.out.println("If we are here, we are Authenticated!!!!");
		String queryString = "";
		session.setAttribute("isAuthentic", true);
		if (request instanceof HttpServletRequest) {
			// queryString = ((HttpServletRequest)request).getQueryString();
			 queryString = (String) session.getAttribute("q");
			 System.out.println("Query string from session: "+ queryString);
			 queryString = queryString.replace("loc=", "");
			 queryString = queryString.replace("&", "?");
			 queryString = queryString.replace("AbyD", "AbyDSecure");
		}
		System.out.println("SSO Succeeded");
		System.out.println("queryString = " + queryString);
		
		/*request.getRequestDispatcher(arg0)*/
		 resp.sendRedirect(resp.encodeRedirectURL(queryString));
		

		

	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
