package com.pdi.filter;

import java.io.IOException;


import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;




/**
 * Servlet Filter implementation class OneLoginAudienceFilter
 */

public class SecureCheckFilter implements Filter {

    /**
     * Default constructor. 
     */
    public SecureCheckFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		
		HttpServletResponse resp = (HttpServletResponse) response;
		HttpServletRequest req = (HttpServletRequest) request;
		HttpSession session = req.getSession();
		System.out.println("In SecureCheckFilter with new Request - isAuthentic = "+session.getAttribute("isAuthentic"));
		
		if(session.getAttribute("isAuthentic") != null && session.getAttribute("isAuthentic").equals(true)){
			chain.doFilter(request, response);
		}else{
			String base = req.getRequestURL().toString();
			String loc = req.getQueryString();
			base = base.replace("Secure", "");
			resp.sendRedirect(resp.encodeRedirectURL(base+"?"+loc));
		}
		

	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
