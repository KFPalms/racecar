package com.pdi.filter;

import java.io.IOException;

import javax.crypto.spec.SecretKeySpec;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kf.palms.web.rest.security.WebTokenUtil;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import java.security.Key;
import java.util.Date;

public class WebTokenFilter implements Filter {
	
	private static final Logger log = LoggerFactory.getLogger(WebTokenFilter.class);

	//private Key key;

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) resp;

		if (request.getMethod().contains("OPTIONS")) {
			response.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
			response.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Cache-Control, Authorization, Pragma, Expires");
			response.setHeader("Access-Control-Expose-Headers", "newToken");
			response.setHeader("Access-Control-Allow-Origin", "*");
			response.setStatus(HttpServletResponse.SC_OK);
			return;
		}


		String token = request.getHeader("Authorization");

		if (token == null){
			token = request.getParameter("token");
		} else {
			token = token.split(" ")[1]; //use the second half of the string after the space.
			//Assumes Authorization header has the scheme:
			//Authorization: Bearer <token>
		}

		if (StringUtils.isEmpty(token)){
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Token missing");
			return;
		}

		try {
			WebTokenUtil wtUtil = new WebTokenUtil(token);
			//Jws<Claims> claims = Jwts.parser().setSigningKey(key).parseClaimsJws(token);
			Long expirationInt = (Long) wtUtil.getTokenClaimValue("exp");
			Date expiration = new Date(expirationInt);
			log.trace("Got Expiration: {}", expiration);
			Date now = new Date();

			if (now.after(expiration)){
				log.debug("Token has Expired. now ({}) is after ({})", now, expiration);
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Token expired");
				return;
			}


			String username = (String) wtUtil.getTokenClaimValue("sub");
			String displayName = (String) wtUtil.getTokenClaimValue("name");
			if (StringUtils.isEmpty(username) || StringUtils.isEmpty(displayName)){
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Invalid Token");
				return;
			}

			//token is valid, generate a new token and add to response header

			String newToken = WebTokenUtil.generateWebToken(username, displayName);
			
			
			response.addHeader("newToken", newToken);
			response.setHeader("Access-Control-Expose-Headers", "Content-Type, Date, Server, Transfer-Encoding, newToken");

			chain.doFilter(request, response);

		} catch (SignatureException s) {
			//redirect to sso filter
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, s.getMessage());
			return;
		} catch (Exception e) {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, e.getMessage());
			return;
		}

	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		//TODO add secret key to properties.
		//this.key = new SecretKeySpec("a46e4e17-48d4-4958-a9c5-0a4aaf427f1d".getBytes(), SignatureAlgorithm.HS512.getJcaName());
	}


}
