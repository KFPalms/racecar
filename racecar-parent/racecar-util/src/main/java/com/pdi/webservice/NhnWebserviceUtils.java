package com.pdi.webservice;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.pdi.properties.PropertyLoader;

/*
 * A class providing webservices for retrieving data from Ninth House.net
 */
public class NhnWebserviceUtils {
	private static final Logger log = LoggerFactory.getLogger(NhnWebserviceUtils.class);

	/*
	 * Sends request to the Cast Iron server for NHN data
	 * @param - V2 person id
	 * @param - v2 project id
	 *
	 * @return A Document object containing the response
	 */
	// TODO Check into retiring this - Cast Iron no longer a viable platform
	public Document requestNhnData(String pid, String eid) throws Exception {
		String req = PropertyLoader.getProperty("com.pdi.data.nhn.application", "castIron.url.nhn");
		req += "?pid=" + pid + "&eid=" + eid;
		// System.out.println("requestNhnData Request string = " + req);
		PostMethod post = new PostMethod(req);
		// System.out.println(req);
		HttpClient client = new HttpClient();
		int result = client.executeMethod(post);
		if (result != 200) {
			throw new Exception("NHN Data not retrieved.  Status = " + result);
		}

		// String strXMLResponse = post.getResponseBodyAsString(); // Old way
		InputStream in = post.getResponseBodyAsStream();

		// System.out.println("-- COMING BACK --");
		// System.out.println(strXMLResponse);
		// System.out.println("-- DONE COMING BACK --");

		DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
		Document doc = docBuilder.parse(in);

		return doc;
	}

	// /**
	// * fetchCourseListForProject - Convenience method. Fetches the original A
	// by D course list, which is filtered on the
	// * "has_test_data" column in the course table. That column is used to
	// filter for those
	// * test instruments that may be displayed in the A by D "Test Data"
	// screen.
	// *
	// * @param path
	// * @param projectId
	// * @return
	// */
	// public ArrayList<String> fetchCourseListForProject(String path, String
	// projectId)
	// {
	// return fetchCourseListForProject(path, projectId, true);
	// }
	//
	//
	// /**
	// * fetchCourseListForProject - Convenience method. Fetches a course list
	// which is NOT filtered on the "has_test_data"
	// * column in the course table. The results of this query are all of the
	// courses assigned
	// * to a project. It is currently used to fetch the full course list for
	// use in setting (or not)
	// * the ALR cogsDone flag.
	// * This method does not appear to be used at this time (4/2017)
	// *
	// * @param path
	// * @param projectId
	// * @return
	// */
	// public ArrayList<String> fetchUnfilteredCourseListForProject(String path,
	// String projectId)
	// {
	// return fetchCourseListForProject(path, projectId, false);
	// }
	//
	//
	// /**
	// * Private method to request the Course List for a project from nhn
	// webservice. See the convenience methods above.
	// *
	// * <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
	// * <courses>
	// * <course course="49">
	// * <link rel="self"
	// href="http://localhost:8083/PDINHServer-rest/jaxrs/courses/49"/>
	// * <title>Leadership Experience Inventory</title>
	// * <abbv>lei</abbv>
	// * </course>
	// * <course course="47">
	// * <link rel="self"
	// href="http://localhost:8083/PDINHServer-rest/jaxrs/courses/47"/>
	// * <title>Career Survey (TLT)</title>
	// * <abbv>cs</abbv>
	// * </course>
	// * <course course="46">
	// * <link rel="self"
	// href="http://localhost:8083/PDINHServer-rest/jaxrs/courses/46"/>
	// * <title>GPI (short)</title>
	// * <abbv>gpi</abbv>
	// * </course>
	// * </courses>
	// *
	// *
	// * @param path
	// * @param projectId
	// * @return
	// */
	//
	// // TODO Refactor this to use the more generalized get found in
	// fetchProjectInfo() later in this class.
	// // This will simplify the callers and encapsulate all of the references
	// to the service in one spot.
	// // The pattern for the Xpath parse found in the getProductCodes method is
	// a viable pattern
	// // TODO Dump this class and use the new one later in this class
	//
	// private ArrayList<String> fetchCourseListForProject(String path, String
	// projectId, boolean isFiltered)
	// {
	//
	// URL url = null;
	// URLConnection uc = null;
	// InputStream content = null;
	// ArrayList<String> courseList = new ArrayList<String>();
	//
	// String surl = path;
	//
	// surl = surl.replace("{projectId}", projectId);
	// //System.out.println("surl (should have projid) = " + surl);
	// if(!path.contains("http"))
	// {
	// //this is a suburl... without hte header in there, so put the header in
	// it
	// surl = PropertyLoader.getProperty("com.pdi.data.nhn.application",
	// "webservice.url.nhn") + surl;
	// //System.out.println("surl = " + surl);
	// }
	// //System.out.println("NhnWebserviceUtils.fetchCourseListForProject() Full
	// URL=" + surl);
	//
	// try {
	// url = new URL (surl);
	// } catch (MalformedURLException e) {
	// System.out.println("Malformed URL - " + surl + ". Msg=" +
	// e.getMessage());
	// return courseList;
	// }
	//
	//
	// // open the connection and get the authorized user info
	// try {
	// uc = url.openConnection();
	// } catch (IOException e) {
	// System.out.println("Unable to open authorization connection. URL=" + surl
	// + ". Msg=" + e.getMessage());
	// return courseList;
	// }
	//
	// try {
	// content = (InputStream)uc.getInputStream();
	// } catch (Exception e) {
	// System.out.println("Unable to open input stream from authorization
	// connection (CourseList). Msg=" + e.getMessage());
	// return courseList;
	// }
	//
	// // Get the content
	// BufferedReader br = new BufferedReader (new InputStreamReader (content));
	// StringBuffer result = new StringBuffer();
	// String line;
	// try {
	// while ((line = br.readLine()) != null)
	// {
	// result.append(line);
	// }
	//
	// } catch (IOException e) {
	// System.out.println("Unable to read User content. Msg=" + e.getMessage());
	// return courseList;
	// }
	//
	// // suppress the unneeded headers
	// String resultString = result.toString();
	// //System.out.println("resultString = " + resultString);
	//
	// // Put the content into a Document object
	// try {
	// DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
	// DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
	// ////Document doc = docBuilder.parse(new InputSource(new
	// StringReader(result.toString().replace("ns2:", ""))));
	// Document doc = docBuilder.parse(new InputSource(new
	// StringReader(resultString)));
	// // now pull out the abbreviations into the arrayList....
	// //System.out.println(" doc.getChildNodes().getLength() = " +
	// doc.getChildNodes().getLength());
	//
	// Node node = doc.getFirstChild();
	// //System.out.println(" node.getNodeName() = " + node.getNodeName());
	// NodeList nodes = node.getChildNodes();
	// for(int x = 1; x < nodes.getLength(); x++){
	//
	// // set variables for each node:
	// String abrv = "";
	// boolean hasTestData = false;
	//
	// if(nodes.item(x).getChildNodes().getLength() > 0){
	//
	// //System.out.println(" course node.... " +
	// nodes.item(x).getChildNodes().item(5).getTextContent());
	// for(int j = 1; j < nodes.item(x).getChildNodes().getLength(); j++){
	// //System.out.println("x = " + x + " j = " + j + " node=" +
	// nodes.item(x).getChildNodes().item(j).getNodeName());
	//
	// // see if this child node is a scoreable test...
	// //System.out.println(" childtext -->" +
	// nodes.item(x).getChildNodes().item(j).getTextContent() + "<--");
	// if(nodes.item(x).getChildNodes().item(j).getNodeName().equals("hasTestData")){
	// String htd = nodes.item(x).getChildNodes().item(j).getTextContent();
	// if(htd.equals("true"))
	// {
	// //System.out.println(" node has test data...");
	// hasTestData = true;
	// }
	// }else
	// if(nodes.item(x).getChildNodes().item(j).getNodeName().equals("abbv")){
	// abrv = nodes.item(x).getChildNodes().item(j).getTextContent();
	// }
	// }
	// }
	//
	// // set it here...
	// if (isFiltered)
	// {
	// if(hasTestData && !abrv.equals(""))
	// {
	// courseList.add(abrv);
	// }
	// }
	// else if (!abrv.equals(""))
	// {
	// courseList.add(abrv);
	// }
	// }
	//
	//// for(String s : courseList){
	//// System.out.println("webservice: course abbv: " + s);
	////
	//// }
	//
	// } catch (Exception e) {
	// System.out.println("Unable to build courseList document Msg=" +
	// e.getMessage());
	// return courseList;
	// }
	//
	// // Drop this... just muddies up the log
	//// if (courseList.size() < 1)
	//// {
	//// System.out.println("No courses found for project " + projectId +
	//// ". surl=" + surl +
	//// ", result=" + resultString);
	//// }
	// return courseList;
	// }

	/**
	 * fetchParticipantContent - Get bizsim content for consultant review
	 * reports
	 *
	 * @param path
	 * @param pptId
	 * @return
	 */
	public String fetchParticipantContent(String path, String pptId) {
		String ret = null;

		URL url = null;
		URLConnection uc = null;
		InputStream content = null;

		String surl = path;

		surl = surl.replace("{pptId}", pptId);

		try {
			url = new URL(surl);
		} catch (MalformedURLException e) {
			System.out.println("Malformed URL - " + surl + ".  Msg=" + e.getMessage());
			ret = "<html><body><h1 style=\"color:red;\">Error detected.</h1>Malformed URL Error in fetchParticipantContent("
					+ path + ", " + pptId + "). Please contact support.<br>URL=" + surl + "<br> msg=" + e.getMessage()
					+ "</body></html>";
			return ret;
		}

		// open the connection and get the authorized user info
		try {
			uc = url.openConnection();
		} catch (IOException e) {
			System.out.println("Unable to open connection.  URL=" + surl + ".  Msg=" + e.getMessage());
			ret = "<html><body><h1 style=\"color:red;\">Error detected.</h1>Unable to open connection in fetchParticipantContent("
					+ path + ", " + pptId + "). Please contact support.<br>URL=" + surl + "<br> msg=" + e.getMessage()
					+ "</body></html>";
			return ret;
		}

		try {
			content = uc.getInputStream();
		} catch (Exception e) {
			System.out.println(
					"Unable to open input stream from authorization connection (content).  Msg=" + e.getMessage());
			ret = "<html><body><h1 style=\"color:red;\">Error detected.</h1>Unable to fetch data in fetchParticipantContent("
					+ path + ", " + pptId + "). Please contact support.<br> Is the appropriate server running?<br>URL="
					+ surl + "<br> msg=" + e.getMessage() + "</body></html>";
			return ret;
		}

		// Get the content
		BufferedReader br;
		try {
			br = new BufferedReader(new InputStreamReader(content, "UTF-8")); // or
																				// the
																				// charset
																				// could
																				// be
																				// StandardCharsets.UTF_8
																				// (no
																				// quotes)
		} catch (UnsupportedEncodingException e1) {
			System.out.println("Unsupported encoding.  Msg=" + e1.getMessage());
			ret = "<html><body><h1 style=\"color:red;\">Error detected.</h1>Unable to read content in fetchParticipantContent("
					+ path + ", " + pptId + "). Please contact support.<br>URL=" + surl + "<br> msg=" + e1.getMessage()
					+ "</body></html>";
			return ret;
		}
		StringBuffer result = new StringBuffer();
		String line;
		try {
			while ((line = br.readLine()) != null) {
				result.append(line);
			}

		} catch (IOException e) {
			System.out.println("Unable to read User content.  Msg=" + e.getMessage());
			ret = "<html><body><h1 style=\"color:red;\">Error detected.</h1>Unable to read content in fetchParticipantContent("
					+ path + ", " + pptId + "). Please contact support.<br>URL=" + surl + "<br> msg=" + e.getMessage()
					+ "</body></html>";
			return ret;
		}

		// suppress the unneeded headers
		ret = result.toString();

		return ret;
	}

	/**
	 * getFilteredProjectCourseList - Convenience method to call
	 * fetchProjectCourseList() with a long to get a filtered course list for a
	 * project Fetches a course list which is filtered on the "has_test_data"
	 * column in the course table. The results of this query are only the
	 * courses assigned to a project with "has_test_data" set.
	 *
	 * @param projectId
	 * @return
	 */
	public ArrayList<String> getFilteredProjectCourseList(long projectId) {
		return fetchProjectCourseList("" + projectId, true);
	}
	/**
	 * Allows to re-use previously fetched project info
	 * @param projectInfo
	 * @return
	 */
	public ArrayList<String> getFilteredProjectCourseList(Document projectInfo) {
		return fetchProjectCourseList(projectInfo, true);
	}

	/**
	 * getFilteredProjectCourseList - Method to call fetchProjectCourseList() to
	 * get a filtered course list for a project Fetches a course list which is
	 * filtered on the "has_test_data" column in the course table. The results
	 * of this query are only the courses with "has_test_data" set assigned to a
	 * project.
	 *
	 * @param projectId
	 * @return
	 */
	public ArrayList<String> getFilteredProjectCourseList(String projectId) {
		return fetchProjectCourseList(projectId, true);
	}

	/**
	 * getUnfilteredProjectCourseList - Convenience method to call
	 * fetchProjectCourseList() with a long to get a full course list for a
	 * project. Fetches a course list which is NOT filtered on the
	 * "has_test_data" column in the course table. The results of this query are
	 * only the courses with "has_test_data" set assigned to a project.
	 *
	 * @param projectId
	 * @return
	 */
	public ArrayList<String> getUnfilteredProjectCourseList(long projectId) {
		return fetchProjectCourseList("" + projectId, false);
	}
	
	/**
	 * Allows to re-use previously fetched project info
	 * @param projectInfo
	 * @return
	 */
	public ArrayList<String> getUnfilteredProjectCourseList(Document projectInfo) {
		return fetchProjectCourseList(projectInfo, false);
	}

	/**
	 * getUnfilteredProjectCourseList - Method to call fetchProjectCourseList()
	 * to get a full course list for a project Fetches a course list which is
	 * NOT filtered on the "has_test_data" column in the course table. The
	 * results of this query are all of the courses assigned to a project.
	 *
	 * @param projectId
	 * @return
	 */
	public ArrayList<String> getUnfilteredProjectCourseList(String projectId) {
		return fetchProjectCourseList(projectId, false);
	}
	
	private ArrayList<String> fetchProjectCourseList(Document projectInfo, boolean isFiltered) {
		ArrayList<String> ret = new ArrayList<String>();
		if (projectInfo == null) {
			// was an error in the fetch reported there
			return ret;
		}

		// Pull out the instrument/course codes into an arrayList....

		// Start up xpath
		XPathFactory xpathFactory = XPathFactory.newInstance();
		XPath xpath = xpathFactory.newXPath();

		// Get the course nodes
		String xpXpr = "/project/courseList/course";
		try {
			// Set up the query...
			XPathExpression expr = xpath.compile(xpXpr);
			// ...and run it
			NodeList courses = (NodeList) expr.evaluate(projectInfo, XPathConstants.NODESET);
			for (int i = 0; i < courses.getLength(); i++) {
				String abrv = "";
				boolean hasTestData = false;
				// Get data from the nodes for the course
				NodeList childList = courses.item(i).getChildNodes();
				for (int j = 0; j < childList.getLength(); j++) {
					Node childNode = childList.item(j);
					// String kdb = "Name=" + childNode.getNodeName() + ",
					// value=" + childList.item(j).getTextContent().trim();
					// int kdbi = 0;
					if ("abbv".equals(childNode.getNodeName())) {
						abrv = childList.item(j).getTextContent().trim();
					} else if ("hasTestData".equals(childNode.getNodeName())) {
						if ("true".equals(childList.item(j).getTextContent().trim())) {
							hasTestData = true;
						}
					}
				} // End the child node loop

				// Add it to the list, if appropriate
				if (abrv.isEmpty()) {
					// nothing to do here
					continue;
				}
				if (isFiltered) {
					if (hasTestData) {
						ret.add(abrv);
					}
				} else {
					ret.add(abrv);
				}
			} // End of course loop
		} catch (XPathExpressionException e) {
			System.out.println("Bad Xpath expression getting course list - " + xpXpr + ".  Msg=" + e.getMessage());
		}

		return ret;
	}

	/**
	 * fetchProjectCourseList - Replacement for fetchCourseListForProject that
	 * uses the fetchProjectInfo() method and does not require the user to pass
	 * the service URL.
	 *
	 * @param projectId
	 * @param isFiltered
	 * @return
	 */
	private ArrayList<String> fetchProjectCourseList(String projectId, boolean isFiltered) {
		
		Document doc = fetchProjectInfo(projectId);
		return fetchProjectCourseList(doc, isFiltered);
		
	}

	/**
	 * getProjectProductCodes - Request info from the project info service and
	 * returns a list of product codes.
	 *
	 * @param projectId
	 * @return
	 */
	public ArrayList<String> getProjectProductCodes(String projectId) {
		
		Document doc = fetchProjectInfo(projectId);
		return getProjectProductCodes(doc);

	}
	
	/**
	 * Allows to re-use previously fetched project info
	 * @param projectInfo
	 * @return
	 */
	public ArrayList<String> getProjectProductCodes(Document projectInfo) {
		ArrayList<String> ret = new ArrayList<String>();
		
		if (projectInfo == null) {
			// was an error in the fetch reported there
			return ret;
		}

		// Pull out the product codes into the arrayList....

		// Start up xpath
		XPathFactory xpathFactory = XPathFactory.newInstance();
		XPath xpath = xpathFactory.newXPath();

		String xpXpr = "/project/productList/product/productCode/text()";
		try {
			// Set up the query...
			XPathExpression expr = xpath.compile(xpXpr);
			// ...and run it
			NodeList nodes = (NodeList) expr.evaluate(projectInfo, XPathConstants.NODESET);
			for (int i = 0; i < nodes.getLength(); i++) {
				ret.add(nodes.item(i).getNodeValue());
			}
		} catch (XPathExpressionException e) {
			System.out.println("Bad Xpath expression getting product codes - " + xpXpr + ".  Msg=" + e.getMessage());
		}

		return ret;
	}

	// TODO If needed re-factor to return a Map of Strings where the key is a
	// property name and the value is info about the project
	// Tried to use the Project object but it causes cyclic references
	/**
	 * getProjectTypeName - Get the project type name from the project info
	 * document
	 *
	 * @param projectId
	 * @return
	 */
	public String getProjectTypeName(String projectId) {
		
		Document doc = fetchProjectInfo(projectId);
		return getProjectTypeName(doc);
	}
	
	public String getProjectTypeName(Document projectInfo) {
		String ret = null;
		if (projectInfo == null) {
			// was an error in the fetch reported there
			return ret;
		}

		// Start up xpath
		XPathFactory xpathFactory = XPathFactory.newInstance();
		XPath xpath = xpathFactory.newXPath();

		// Get the course nodes
		String xpXpr = "/project/projectTypeName";
		try {
			XPathExpression expr = xpath.compile(xpXpr);
			NodeList nl = (NodeList) expr.evaluate(projectInfo, XPathConstants.NODESET);

			// should be but a single node
			if (nl.getLength() > 0) {
				ret = nl.item(0).getTextContent();
			}
		} catch (XPathExpressionException e) {
			System.out
					.println("Bad Xpath expression getting project type name - " + xpXpr + ".  Msg=" + e.getMessage());
		}

		return ret;
	}

	/**
	 * getExternalManifestVersion - Get the project external key from the
	 * project info document
	 *
	 * @param projectId
	 * @return
	 */
	public String getExternalManifestVersion(String projectId) {
		
		Document doc = fetchProjectInfo(projectId);
		return this.getExternalManifestVersion(doc);
	}
	/**
	 * Allows to re-use previously fetched project info
	 * @param projectInfo
	 * @return
	 */
	public String getExternalManifestVersion(Document projectInfo) {
		String ret = "";
		
		if (projectInfo == null) {
			// was an error in the fetch reported there
			return ret;
		}

		// Start up xpath
		XPathFactory xpathFactory = XPathFactory.newInstance();
		XPath xpath = xpathFactory.newXPath();

		// Get the course nodes
		String xpXpr = "/project/externalManifestVersion";
		try {
			XPathExpression expr = xpath.compile(xpXpr);
			NodeList nl = (NodeList) expr.evaluate(projectInfo, XPathConstants.NODESET);

			// should be but a single node
			if (nl.getLength() > 0) {
				ret = nl.item(0).getTextContent();
			}
		} catch (XPathExpressionException e) {
			System.out.println("Bad Xpath expression getting project External Manifest Version- " + xpXpr + ".  Msg="
					+ e.getMessage());
		}

		return ret;
	}

	/**
	 * fetchProjectInfo - Private method that calls the project info service and
	 * returns a Document from which the desired info can be parsed.
	 *
	 * @param projId
	 * @return
	 */
	public Document fetchProjectInfo(String projId) {
		// Prelim setup
		// Get the webservice prefix (env dependent)
		
		String path = PropertyLoader.getProperty("com.pdi.data.nhn.application", "webservice.url.nhn");
		// Get the subURL (fixed)
		path += PropertyLoader.getProperty("com.pdi.data.nhn.application", "project.info.url");
		// And put in the project ID
		path = path.replace("{projectId}", projId);
		// System.out.println("NhnWebserviceUtils.fetchProjectInfo() Full URL
		// string=" + path);
		log.debug("Fetching project data for project id: {}, from path: {}", projId, path);
		URL url = null;
		URLConnection uc = null;
		InputStream content = null;
		Document doc = null;
		if (projId.trim().equals("0")){
			log.error("Cannot fetch project id 0. returning null");
			return doc;
		}
		try {
			url = new URL(path);
		} catch (MalformedURLException e) {
			log.error("Malformed URL - {}, Msg={}", path, e.getMessage());
			return doc;
		}

		// open the connection
		try {
			uc = url.openConnection();
		} catch (IOException e) {
			log.error("Unable to open project info connection.  URL={}, Msg={}", path, e.getMessage());
			return doc;
		}

		// Open the input stream from the connection
		try {
			content = uc.getInputStream();
		} catch (Exception e) {
			log.error(
					"Unable to open input stream for the project info connection (CourseList).  Msg={}", e.getMessage());
			return doc;
		}

		// Get the content
		BufferedReader br = new BufferedReader(new InputStreamReader(content));
		StringBuffer result = new StringBuffer();
		String line;
		try {
			while ((line = br.readLine()) != null) {
				result.append(line);
			}
		} catch (IOException e) {
			log.error("Unable to read project info content.  Msg={}", e.getMessage());
			return doc;
		}

		// Put the content into a Document object
		String resultString = result.toString();
		try {
			DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
			doc = docBuilder.parse(new InputSource(new StringReader(resultString)));

		} catch (Exception e) {
			log.error("Unable to build project info document  Msg={}", e.getMessage());
			return null;
		}

		return doc;
	}

}