package com.pdi.logging;


import java.util.logging.Handler;
import java.util.logging.Level;
//import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Title:        CP 3.0 Utilities Description: Copyright:    Copyright (c) 2001
 * Company:      Ninth House Network
 *
 * @author Brian Strauss, Anthony Chi, Eric Knight
 * @version 1.0
 * 
 * MB: March 17, 2011
 * Ported from NHN 360 by Ken Beukelman and MB Panichi
 * Also ported the NHFileHandler
 * Properties file in pdi-data: com.pdi.data.application.properties
 * 
 */
public final class LogWriter {
	
	private static final Logger log = LoggerFactory.getLogger(LogWriter.class);

  /**
   */
  public static final int DEBUG = -1;

  /**
   */
  public static final int INFO = 0;

  /**
   */
  public static final int WARNING = 1;

  /**
   */
  public static final int ERROR = 2;

  /**
   */
  public static final int FATAL = 3;


  /**
   */
  private static Logger logger;
  private static Handler fh;

  public static Logger getLogger() {
    return log;
  }

  public static Handler getFh() {
    return fh;
  }

  /**
   * Creates the object and set the logFilePath and pw member variables.
   */
  private LogWriter() {

  }

  static {
	  /*
    boolean handlerAlreadyExists = false;

    logger = Logger.getLogger("");
    Handler[] handlers = logger.getHandlers();
    for (int i = 0; i < handlers.length; i++) {
      Handler handler = handlers[i];
      logger.removeHandler(handler);
    }

    logger = Logger.getLogger("com.pdi");
    handlers = logger.getHandlers();
    for (int i = 0; i < handlers.length; i++) {
      Handler handler = handlers[i];
      if (handler.toString().indexOf("NHFileHandler") > 0) {
        handlerAlreadyExists = true;
      } else {
        logger.removeHandler(handler);
      }
    }

    try {
      if (!handlerAlreadyExists) {
        fh = new NHFileHandler();
        fh.setFormatter(new SimpleFormatter());
        logger.addHandler(fh);
        logger.setLevel(Level.ALL);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
*/
  }


  /**
   * @param errorLevel   DOCUMENT ME!
   * @param callerObj    DOCUMENT ME!
   * @param errorMessage DOCUMENT ME!
   */
  public static synchronized void logBasic(int errorLevel, Object callerObj,
                                           String errorMessage) {
    log(errorLevel, callerObj, null, null, errorMessage, null);
  }

  /**
   * @param errorLevel   DOCUMENT ME!
   * @param callerObj    DOCUMENT ME!
   * @param errorMessage DOCUMENT ME!
   * @param exception    DOCUMENT ME!
   */
  public static synchronized void logBasicWithException(int errorLevel,
                                                        Object callerObj, String errorMessage, Exception exception) {
    log(errorLevel, callerObj, null, null, errorMessage, exception);
  }

  /**
   * @param errorLevel   DOCUMENT ME!
   * @param callerObj    DOCUMENT ME!
   * @param errorMessage DOCUMENT ME!
   * @param SQL          DOCUMENT ME!
   */
  public static synchronized void logSQL(int errorLevel, Object callerObj,
                                         String errorMessage, String SQL) {
    log(errorLevel, callerObj, null, null,
        errorMessage + " sql: [" + SQL + "]", null);
  }

  /**
   * @param errorLevel   DOCUMENT ME!
   * @param callerObj    DOCUMENT ME!
   * @param errorMessage DOCUMENT ME!
   * @param SQL          DOCUMENT ME!
   * @param exception    DOCUMENT ME!
   */
  public static synchronized void logSQLWithException(int errorLevel,
                                                      Object callerObj, String errorMessage, 
                                                      String SQL, Exception exception) {
    log(errorLevel, callerObj, null, null,
        errorMessage + " sql: [" + SQL + "]", exception);
  }

  /**
   * Writes log to the log file
   *
   * @param errorLevel   DOCUMENT ME!
   * @param callerObj    DOCUMENT ME!
   * @param userID       DOCUMENT ME!
   * @param sessionID    DOCUMENT ME!
   * @param errorMessage DOCUMENT ME!
   * @param exception    DOCUMENT ME!
   */
  public static synchronized void log(int errorLevel, Object callerObj,
                                      String userID, String sessionID, String errorMessage,
                                      Exception exception) {
	    Object c = null;
	    if (callerObj == null) {
	      c = "null";
	    } else {
	      c = callerObj.getClass().getName();
	    }
	    if (userID == null) {
	      userID = "null";
	    }
	    if (sessionID == null) {
	      sessionID = "null";
	    }
	    switch (errorLevel) {
	      case DEBUG:
	        log.debug("Caller: {}, User Id: {}, Session Id: {}, Message: {}, Exception: {}", c, userID, sessionID, errorMessage, exception);
	        break;
	      case ERROR:
	        log.error("Caller: {}, User Id: {}, Session Id: {}, Message: {}, Exception: {}", c, userID, sessionID, errorMessage, exception);
	        break;
	      case INFO:
	        log.info("Caller: {}, User Id: {}, Session Id: {}, Message: {}, Exception: {}", c, userID, sessionID, errorMessage, exception);
	        break;
	      case WARNING:
	        log.warn("Caller: {}, User Id: {}, Session Id: {}, Message: {}, Exception: {}", c, userID, sessionID, errorMessage, exception);
	        break;
	      case FATAL:
	        log.error("Caller: {}, User Id: {}, Session Id: {}, Message: {}, Exception: {}", c, userID, sessionID, errorMessage, exception);
	        break;
	    }
  }



  public static void main(String[] args) {
    System.out.println("LogWriter.main");
    System.out.println(LogWriter.getFh().toString());
    System.out.println(LogWriter.getLogger().toString());
    for (int i = 0; i < 80000; i++) {
      LogWriter.logBasic(LogWriter.FATAL, null, "message " + i);
    }
  }
}

