package com.pdi.logging;


import java.io.*;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.*;
import com.pdi.properties.PropertyLoader;
import java.util.logging.SimpleFormatter;

/**
 * Created by IntelliJ IDEA.
 * User: EricK
 * Date: Jun 14, 2004
 * Time: 2:40:24 PM
 * Copyright Jun 14, 2004, Ninth House Inc. All rights reserved.
 * 
 * MB: March 17, 20011
 * Ported from NHN 360 by Ken Beukelman and MB Panichi
 * At some point, we'll probably want to roll 
 * these together and just have one logger for the whole system.
 * Also ported the LogWriter from java 360.
 * Properties file in pdi-data: com.pdi.data.application.properties
 * 
 */
public class NHFileHandler extends StreamHandler {
  private MeteredStream meter;
  private int limit;       // zero => no limit.
//  private int count;
  private int fileNumber = 0;
  private String pattern;
  private String lockFileName;
  private FileOutputStream lockStream;
//  private File files[];
  private static final int MAX_LOCKS = 100;
  private static java.util.HashMap locks = new java.util.HashMap();
  private long lastDate = (new Date()).getTime();
  static private SimpleFormatter formatterTxt;

  private void open(File fname, boolean append) throws IOException {
//    System.out.println("NHFileHandler.open");
    int len = 0;
    if (append) {
      len = (int) fname.length();
    }
    FileOutputStream fout = new FileOutputStream(fname.toString(), append);
    BufferedOutputStream bout = new BufferedOutputStream(fout);
    meter = new MeteredStream(bout, len);
    setOutputStream(meter);
  }

  // Private method to configure a FileHandler from LogManager
  // properties and/or default values as specified in the class
  // javadoc.
  private void configure() throws Exception {
//    System.out.println("NHFileHandler.configure");
    String cname = NHFileHandler.class.getName();

 //   PropertyLoader pl = new PropertyLoader();
    //pattern = pl.getProperty("com.pdi.data.application", cname + ".pattern");
    pattern = PropertyLoader.getProperty("com.pdi.data.application", cname + ".pattern");

    //String sLimit = pl.getProperty("com.pdi.data.application", cname + ".limit");
    String sLimit = PropertyLoader.getProperty("com.pdi.data.application", cname + ".limit");
    // adding a null check here because the PDI properties returns an empty string, and not a null
    if(sLimit != null && sLimit.length() > 0){
    	 limit = Integer.parseInt(sLimit);
    }  
    if (limit < 0) {
        limit = 0;
      }

    //String level =  pl.getProperty("com.pdi.data.application",cname + ".level", "ALL");
    String level =  PropertyLoader.getProperty("com.pdi.data.application",cname + ".level", "ALL");
    if(level != null && level.length() > 0){
    	setLevel(parseLevel(level));
    }
    

    // MB:  Not really sure what the filter does... added a null check
    // and leaving it at that for now.
    //String filter =  pl.getProperty("com.pdi.data.application",cname + ".filter", null);
    String filter =  PropertyLoader.getProperty("com.pdi.data.application",cname + ".filter", null);
    if(filter != null && filter.length() > 0){
      Class clz = ClassLoader.getSystemClassLoader().loadClass(filter);
      setFilter((Filter) clz.newInstance());
    }

//    String formatter = "com.ninthhouse.lms.util.NHXMLFormatter";
//    String formatter = props.getProperty(cname + ".formatter", "java.util.logging.XMLFormatter");
//    Class clz = ClassLoader.getSystemClassLoader().loadClass(formatter);
//    NHXMLFormatter xmlFormatter = (NHXMLFormatter) clz.newInstance();
//      NHNFormatter xmlFormatter = new NHNFormatter();
    
     formatterTxt = new SimpleFormatter();
     setFormatter(formatterTxt);
  
    try {
        //setEncoding(pl.getProperty("com.pdi.data.application", cname + ".encoding", null));
        setEncoding(PropertyLoader.getProperty("com.pdi.data.application", cname + ".encoding", null));
    } catch (Exception ex) {
      try {
        setEncoding(null);
      } catch (Exception ex2) {
        // doing a setEncoding with null should always work.
        // assert false;
      }
    }
  }

  private Level parseLevel(String level) {
    if (level.equals("SEVERE")) {
      return (Level.SEVERE);
    } else if (level.equals("WARNING")) {
      return (Level.WARNING);
    } else if (level.equals("INFO")) {
      return (Level.INFO);
    } else if (level.equals("CONFIG")) {
      return (Level.CONFIG);
    } else if (level.equals("FINE")) {
      return (Level.FINE);
    } else if (level.equals("FINER")) {
      return (Level.FINER);
    } else if (level.equals("FINEST")) {
      return (Level.FINEST);
    }
    return (Level.ALL);
  }

  private void checkAccess() {

  }

  /**
   * Construct a default <tt>FileHandler</tt>.  This will be configured
   * entirely from <tt>LogManager</tt> properties (or their default values).
   * <p/>
   *
   * @throws IOException       if there are IO problems opening the files.
   * @throws SecurityException if a security manager exists and if
   *                           the caller does not have <tt>LoggingPermission("control"))</tt>.
   */
  public NHFileHandler() throws Exception {
//    System.out.println("NHFileHandler.NHFileHandler");
    checkAccess();
    configure();
    openFiles();
  }

  /**
   * Initialize a <tt>FileHandler</tt> to write to the given filename.
   * <p/>
   * The <tt>FileHandler</tt> is configured based on <tt>LogManager</tt>
   * properties (or their default values) except that the given pattern
   * argument is used as the filename pattern, the file limit is
   * set to no limit, and the file count is set to one.
   * <p/>
   * There is no limit on the amount of data that may be written,
   * so use this with care.
   *
   * @param pattern the name of the output file
   * @throws IOException       if there are IO problems opening the files.
   * @throws SecurityException if a security manager exists and if
   *                           the caller does not have <tt>LoggingPermission("control")</tt>.
   */
  public NHFileHandler(String pattern) throws Exception {
    checkAccess();
    configure();
    this.pattern = pattern;
    this.limit = 0;
//    this.count = 1;
    openFiles();
  }

  /**
   * Initialize a <tt>FileHandler</tt> to write to the given filename,
   * with optional append.
   * <p/>
   * The <tt>FileHandler</tt> is configured based on <tt>LogManager</tt>
   * properties (or their default values) except that the given pattern
   * argument is used as the filename pattern, the file limit is
   * set to no limit, the file count is set to one, and the append
   * mode is set to the given <tt>append</tt> argument.
   * <p/>
   * There is no limit on the amount of data that may be written,
   * so use this with care.
   *
   * @param pattern the name of the output file
   * @param append  specifies append mode
   * @throws IOException       if there are IO problems opening the files.
   * @throws SecurityException if a security manager exists and if
   *                           the caller does not have <tt>LoggingPermission("control")</tt>.
   */
  public NHFileHandler(String pattern, boolean append) throws Exception {
    checkAccess();
    configure();
    this.pattern = pattern;
    this.limit = 0;
//    this.count = 1;
    openFiles();
  }

  /**
   * Initialize a <tt>FileHandler</tt> to write to a set of files.  When
   * (approximately) the given limit has been written to one file,
   * another file will be opened.  The output will cycle through a set
   * of count files.
   * <p/>
   * The <tt>FileHandler</tt> is configured based on <tt>LogManager</tt>
   * properties (or their default values) except that the given pattern
   * argument is used as the filename pattern, the file limit is
   * set to the limit argument, and the file count is set to the
   * given count argument.
   * <p/>
   * The count must be at least 1.
   *
   * @param pattern the pattern for naming the output file
   * @param limit   the maximum number of bytes to write to any one file
   * @param count   the number of files to use
   * @throws IOException              if there are IO problems opening the files.
   * @throws SecurityException        if a security manager exists and if
   *                                  the caller does not have <tt>LoggingPermission("control")</tt>.
   * @throws IllegalArgumentException if limit < 0, or count < 1.
   */
  public NHFileHandler(String pattern, int limit, int count)
      throws Exception {
    if (limit < 0 || count < 1) {
      throw new IllegalArgumentException();
    }
    checkAccess();
    configure();
    this.pattern = pattern;
    this.limit = limit;
//    this.count = count;
    openFiles();
  }

  /**
   * Initialize a <tt>FileHandler</tt> to write to a set of files
   * with optional append.  When (approximately) the given limit has
   * been written to one file, another file will be opened.  The
   * output will cycle through a set of count files.
   * <p/>
   * The <tt>FileHandler</tt> is configured based on <tt>LogManager</tt>
   * properties (or their default values) except that the given pattern
   * argument is used as the filename pattern, the file limit is
   * set to the limit argument, and the file count is set to the
   * given count argument, and the append mode is set to the given
   * <tt>append</tt> argument.
   * <p/>
   * The count must be at least 1.
   *
   * @param pattern the pattern for naming the output file
   * @param limit   the maximum number of bytes to write to any one file
   * @param count   the number of files to use
   * @param append  specifies append mode
   * @throws IOException              if there are IO problems opening the files.
   * @throws SecurityException        if a security manager exists and if
   *                                  the caller does not have <tt>LoggingPermission("control")</tt>.
   * @throws IllegalArgumentException if limit < 0, or count < 1.
   */
  public NHFileHandler(String pattern, int limit, int count, boolean append)
      throws Exception {
    if (limit < 0 || count < 1) {
      throw new IllegalArgumentException();
    }
    checkAccess();
    configure();
    this.pattern = pattern;
    this.limit = limit;
//    this.count = count;
    openFiles();
  }

  // Private method to open the set of output files, based on the
  // configured instance variables.
  private void openFiles() throws IOException {
//    System.out.println("NHFileHandler.openFiles");
    LogManager manager = LogManager.getLogManager();
    manager.checkAccess();
//    if (count < 1) {
//      throw new IllegalArgumentException("file count = " + count);
//    }
    if (limit < 0) {
      limit = 0;
    }

    // We register our own ErrorManager during initialization
    // so we can record exceptions.
    InitializationErrorManager em = new InitializationErrorManager();
    setErrorManager(em);

    // Create a lock file.  This grants us exclusive access
    // to our set of output files, as long as we are alive.
    int unique = -1;
    for (; ;) {
      unique++;
      if (unique > MAX_LOCKS) {
        throw new IOException("Couldn't get lock for " + pattern);
      }
      // Generate a lock file name from the "unique" int.
      lockFileName = generate(pattern, 0, unique).toString() + ".lck";
      // Now try to lock that filename.
      // Because some systems (e.g. Solaris) can only do file locks
      // between processes (and not within a process), we first check
      // if we ourself already have the file locked.
      synchronized (locks) {
        if (locks.get(lockFileName) != null) {
          // We already own this lock, for a different FileHandler
          // object.  Try again.
          continue;
        }
        FileChannel fc;
        try {
          String fileSep = System.getProperty("file.separator");
          if (lockFileName.indexOf(fileSep) > -1) {
            File testFile;
            int lastFileSep = lockFileName.lastIndexOf(fileSep);
            testFile = new File(lockFileName.substring(0, lastFileSep));
            if (!testFile.exists()) {
              testFile.mkdirs();
            }
          }

          lockStream = new FileOutputStream(lockFileName);
          fc = lockStream.getChannel();
        } catch (IOException ix) {
          // We got an IOException while trying to open the file.
          // Try the next file.
          continue;
        }
        try {
          FileLock fl = fc.tryLock();
          if (fl == null) {
            // We failed to get the lock.  Try next file.
            continue;
          }
          // We got the lock OK.
        } catch (IOException ix) {
          // We got an IOException while trying to get the lock.
          // This normally indicates that locking is not supported
          // on the target directory.  We have to proceed without
          // getting a lock.   Drop through.
        }
        // We got the lock.  Remember it.
        locks.put(lockFileName, lockFileName);
        break;
      }
    }

//    files = new File[count];
//    for (int i = 0; i < count; i++) {
//      files[i] = generate(pattern, i, unique);
//    }

    // Create the initial log file.
//    if (append) {
//      open(files[count - 1], true);
    fileNumber = 0;
    String cname = NHFileHandler.class.getName();
    File directory = null;
    try {
    	//PropertyLoader pl = new PropertyLoader();
        //File temp = new File(pl.getProperty("com.pdi.data.application",cname + ".pattern", "."));
        File temp = new File(PropertyLoader.getProperty("com.pdi.data.application",cname + ".pattern", "."));
      directory = temp.getParentFile();
    } catch (Exception e) {
      e.printStackTrace();
      directory = new File(".");
    }
    File fle = generate(pattern, fileNumber, unique);
    File[] contents = directory.listFiles();
    /*
    new FilenameFilter() {
      public boolean accept(File dir, String name) {
        if (name.indexOf(pattern) > 0) {
          return true;
        }
        return false;
      }
    }
    */
    while (true) {
      boolean alreadyExists = false;
      for (int i = 0; i < contents.length; i++) {
        if (contents[i].getName().equals(fle.getName())) {
          alreadyExists = true;
          break;
        }
      }
      if (!alreadyExists) {
        break;
      }
      fileNumber++;
      fle = generate(pattern, fileNumber, unique);
    }
    open(fle, true);

    // Did we detect any exceptions during initialization?
    Exception ex = em.lastException;
    if (ex != null) {
      if (ex instanceof IOException) {
        throw (IOException) ex;
      } else if (ex instanceof SecurityException) {
        throw (SecurityException) ex;
      } else {
        throw new IOException("Exception: " + ex);
      }
    }

    // Install the normal default ErrorManager.
    setErrorManager(new ErrorManager());
  }

  // Generate a filename from a pattern.
  private File generate(String pattern, int generation, int unique) throws IOException {
//    System.out.println("NHFileHandler.generate");
    File file = null;
    StringBuffer word = new StringBuffer("");
    int ix = 0;
    boolean sawg = false;
    boolean sawu = false;
    while (ix < pattern.length()) {
      char ch = pattern.charAt(ix);
      ix++;
      char ch2 = 0;
      if (ix < pattern.length()) {
        ch2 = Character.toLowerCase(pattern.charAt(ix));
      }
      if (ch == '/') {
        if (file == null) {
          file = new File(word.toString());
        } else {
          file = new File(file, word.toString());
        }
        word = new StringBuffer("");
        continue;
      } else if (ch == '%') {
        if (ch2 == 't') {
          String tmpDir = System.getProperty("java.io.tmpdir");
          if (tmpDir == null) {
            tmpDir = System.getProperty("user.home");
          }
          file = new File(tmpDir);
          ix++;
          word = new StringBuffer("");
          continue;
        } else if (ch2 == 'h') {
          file = new File(System.getProperty("user.home"));
          if (isSetUID()) {
            // Ok, we are in a set UID program.  For safety's sake
            // we disallow attempts to open files relative to %h.
            throw new IOException("can't use %h in set UID program");
          }
          ix++;
          word = new StringBuffer("");
          continue;
        } else if (ch2 == 'g') {
          word.append(generation);
          sawg = true;
          ix++;
          continue;
        } else if (ch2 == 'u') {
          word.append(unique);
          sawu = true;
          ix++;
          continue;
        } else if (ch2 == '%') {
          word.append("%");
          ix++;
          continue;
        }
      }
      word.append(ch);
    }
//    if (count > 1 && !sawg) {
    if (!sawg) {
      word.insert(0, generation + "_");
    }
    if (unique > 0 && !sawu) {
      word.insert(0, unique + "_");
    }
    Calendar calendar = Calendar.getInstance();
    word.insert(0, calendar.get(Calendar.YEAR) + "-" +
                   (calendar.get(Calendar.MONTH) + 1) + "-" +
                   calendar.get(Calendar.DAY_OF_MONTH) + "-" +
                   calendar.get(Calendar.HOUR_OF_DAY) + "h" +
                   calendar.get(Calendar.MINUTE) + "m" +
                   calendar.get(Calendar.SECOND) + "s");

    if (word.length() > 0) {
      if (file == null) {
        file = new File(word.toString());
      } else {
        file = new File(file, word.toString());
      }
    }
    return file;
  }

  // Rotate the set of output files
  private synchronized void rotate(boolean dateChange) {
//    System.out.println("NHFileHandler.rotate");
    try {
      close();
      openFiles();
    } catch (IOException e) {
      e.printStackTrace();
    }

//    if (dateChange) {
//      try {
//        openFiles();
//      } catch (IOException e) {
//        e.printStackTrace();
//      }
//    }
/*
    Level oldLevel = getLevel();
    setLevel(Level.OFF);

    super.close();
    for (int i = count - 2; i >= 0; i--) {
      File f1 = files[i];
      File f2 = files[i + 1];
      if (f1.exists()) {
        if (f2.exists()) {
          f2.delete();
        }
        f1.renameTo(f2);
      }
    }
    try {
      open(files[0], false);
    } catch (IOException ix) {
      // We don't want to throw an exception here, but we
      // report the exception to any registered ErrorManager.
      reportError(null, ix, ErrorManager.OPEN_FAILURE);

    }
    setLevel(oldLevel);*/
  }

  private boolean isNewDate() {
    Calendar c1 = Calendar.getInstance();
    Calendar c2 = Calendar.getInstance();

    c1.setTimeInMillis(lastDate);
    if (c2.get(Calendar.DAY_OF_YEAR) - c1.get(Calendar.DAY_OF_YEAR) > 0 ||
        c2.get(Calendar.YEAR) - c1.get(Calendar.YEAR) > 0) {
      lastDate = c2.getTimeInMillis();
      return true;
    } else {
      return false;
    }
  }


  /**
   * Format and publish a <tt>LogRecord</tt>.
   *
   * @param record description of the log event
   */
  public synchronized void publish(LogRecord record) {
    if (!isLoggable(record)) {
      return;
    }

    //removeEndTag();
    super.publish(record);
//    putEndTag();
    flush();
    if (isNewDate()) {
      // We performed access checks in the "init" method to make sure
      // we are only initialized from trusted code.  So we assume
      // it is OK to write the target files, even if we are
      // currently being called from untrusted code.
      // So it is safe to raise privilege here.
      AccessController.doPrivileged(new PrivilegedAction() {
        public Object run() {
          rotate(true);
          return null;
        }
      });
    } else if ((limit > 0 && meter.written >= limit)) {
      // We performed access checks in the "init" method to make sure
      // we are only initialized from trusted code.  So we assume
      // it is OK to write the target files, even if we are
      // currently being called from untrusted code.
      // So it is safe to raise privilege here.
      AccessController.doPrivileged(new PrivilegedAction() {
        public Object run() {
          rotate(false);
          return null;
        }
      });
    }
  }

  /**
   * Close all the files.
   *
   * @throws SecurityException if a security manager exists and if
   *                           the caller does not have <tt>LoggingPermission("control")</tt>.
   */
  public synchronized void close() throws SecurityException {
    super.close();
    // Unlock any lock file.
    if (lockFileName == null) {
      return;
    }
    try {
      // Closing the lock file's FileOutputStream will close
      // the underlying channel and free any locks.
      lockStream.close();
    } catch (Exception ex) {
      // Problems closing the stream.  Punt.
    }
    synchronized (locks) {
      locks.remove(lockFileName);
    }
    lockFileName = null;
    lockStream = null;
  }

  private static class InitializationErrorManager extends ErrorManager {
    Exception lastException;

    public void error(String msg, Exception ex, int code) {
      lastException = ex;
    }
  }

  // Private native method to check if we are in a set UID program.
  private static native boolean isSetUID();

  public static void main(String[] args) {
    try {
      Class.forName("com.ninthhouse.utils.LogWriter");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    int count = 100000;
    for (int i = 0; i < count; i++) {
      LogWriter.logBasic(1, null, "This is a log message." + Math.random() + Math.random() + Math.random() + Math.random() + Math.random());
    }
  }


  // A metered stream is a subclass of OutputStream that
  //   (a) forwards all its output to a target stream
  //   (b) keeps track of how many bytes have been written
  class MeteredStream extends OutputStream {
    OutputStream out;
    int written;

    MeteredStream(OutputStream out, int written) {
      this.out = out;
      this.written = written;
    }

    public void write(int b) throws IOException {
      out.write(b);
      written++;
    }

    public void write(byte buff[]) throws IOException {
      out.write(buff);
      written += buff.length;
    }

    public void write(byte buff[], int off, int len) throws IOException {
      out.write(buff, off, len);
      written += len;
    }

    public void flush() throws IOException {
      out.flush();
    }

    public void close() throws IOException {
      out.close();
    }
  }

}


