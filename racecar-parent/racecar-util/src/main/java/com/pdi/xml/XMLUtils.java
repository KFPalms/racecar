/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.xml;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

//import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Helpful XML utilities
 * 
 *
 * @author		Gavin Myers
 */
public class XMLUtils
{

	//
	// Static data.
	//

	//
	// Static methods.
	//
	
	
	/**
	 * Return a NodeList from a given Node.  If there is none, return a null
	 * @param node
	 * @param element
	 * @return
	 * @throws Exception
	 */
	public static NodeList getElements(Node node, String element)
	{
		try {
			XPath xpath = XPathFactory.newInstance().newXPath(); 
			NodeList nodes = (NodeList) xpath.evaluate(element, node, XPathConstants.NODESET);
			return nodes;
		}
		catch(Exception e)
		{
			return null;
		}
	}
	
	/**
	 * Return an Attribute value.  If we don't find the value, return a null
	 * @param node
	 * @param element
	 * @return
	 */
	public static String getAttributeValue(Node node, String attribute)
	{
		try
		{
			return node.getAttributes().getNamedItem(attribute).getTextContent();
		}
		catch(Exception e)
		{
			return null;
		}
	}
	
	/**
	 * Return an element value.  If we don't find the value, return a null
	 * @param node
	 * @param element
	 * @return
	 */
	public static String getElementValue(Node node, String element)
	{
		try
		{
			XPath xpath = XPathFactory.newInstance().newXPath(); 

			Node finalNode = (Node) xpath.evaluate(element, node, XPathConstants.NODE);
			return finalNode.getTextContent();
		}
		catch(Exception e)
		{
			return null;
		}
	}
	
	/**
	 * Convert a node to a string
	 * @param node
	 * @return
	 */
    public static String nodeToString(Node node)
    {
    	StringWriter sw = new StringWriter();
    	try
    	{
    		Transformer t = TransformerFactory.newInstance().newTransformer();
    		t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
    		t.transform(new DOMSource(node), new StreamResult(sw));
    	}
    	catch (TransformerException te)
    	{
    		// Errors are swallowed
    	}
    	return sw.toString();
    }
    
    
    // Note that most references to this method have been
    // removed because of possible UI display issues.  Now
    // This is primarily used to resolve issues in CHQ data
    public static String xmlFormatString(String input) {
    	//Step 1, to prevent double escaping
    	input = input.replaceAll("&amp;","&");
    	input = input.replaceAll("&lt;","<");
    	input = input.replaceAll("&gt;",">");
    	
    	//Step 2, escape
    	input = input.replaceAll("&", "&amp;");
    	input = input.replaceAll("<", "&lt;");
    	input = input.replaceAll(">", "&gt;");
    	return input;
    }

    
	/*
	 * Public method used to escape HTML sensitive characters (see
	 * note on called function).  Includes the following:
	 * 		&
	 * 		<
	 * 		>
	 * 		' (apostrophe)
	 */
	public static String xmlEscapeString(String input)
	{
		return escapeString(input, true);
	}


	/*
	 * Public method used to escape characters used in spreadsheets.  This
	 * is a subset of the ones used in HTML.  Includes the following:
	 * 		&
	 * 		<
	 * 		>
	 * The apostrophe is NOT escaped
	 */
	public static String xmlSsEscapeString(String input)
	{
		return escapeString(input, false);
	}


	/*
	 * Method to replace sensitive characters in xml (html markup characters)
	 * This only escapes non html values, useful for strings that might contain
	 * valid html but also contain invalid things such as "Company & Company"
	 * In that instance it should change it to "Company &amp; Company"
	 * 10/18/10 - Added <, > and '
	 * 06/23/12 - Added the ability to suppress the apostrophe... not needed on spreadsheets
	 * 
	 * @param input - The string to do the business on
	 * @param doEmAll - Flag to tell the logic to do all of the escapement (true)
	 * 					or to drop the stuff that Excel doesn't need (false)
	 * @return Escaped string
     */
	 //public static String xmlEscapeString(String input) {
	private static String escapeString(String input, boolean doEmAll)
	{
    	if (input != null)
    	{
    		// We want to "escape" stuff that needs it but not the stuff that doesn't.  The
    		// logic below assumes that if the input parses it needs no escapement but if it
    		// does not parse it does need it
    		try
			{
    			// Build a doc and parse it
				DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
				DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
				// We won't be looking at the generated doc, so don't assign it to a variable
				docBuilder.parse(new InputSource(new StringReader("<root>" +  input + "</root>")));
			}
			catch (SAXException se)
			{
				// parse "problem"... We need to escape the characters
				// Put out a message saying this was an expected result and that
				// it triggers the special character substitution. 
				System.out.println("Not really a [Fatal Error]; Explanation: Special character detected and replaced -- XMLUtils.xmlEscapeString()");

		    	//Step 1, to prevent double escaping
		    	input = input.replaceAll("&amp;","&");
		    	input = input.replaceAll("&lt;","<");
		    	input = input.replaceAll("&gt;",">");
		    	if (doEmAll)
		    	{
		    		// Do the stuff that the spreadsheets don't need
		    		input = input.replaceAll("&apos;","'");
		    	}
		    	
		    	//Step 2, escape
		    	input = input.replaceAll("&", "&amp;");
		    	input = input.replaceAll("<", "&lt;");
		    	input = input.replaceAll(">", "&gt;");
		    	if (doEmAll)
		    	{
		    		// Again, do the stuff that the spreadsheets don't need
		    		input = input.replaceAll("'", "&apos;");	// or else &#39;
		    	}
			}
			catch (Exception se)
			{
				// ParderConfiguration or IOException...
				se.printStackTrace();
			}
     	}

    	return input;
    }
 
    // This method scans an input strings and replaces any "<" or ">" characters
    // with "[" and "}" respectively.  This is to allow us to put text strings
    // formatted with HTML markup (<br>, <p>, <li>, etc) into a single call in
    // a spreadsheet.  The markup is still present but it is not operative
    // because it has mal-formed tags
    public static String suppressHtmlTags(String input)
    {
    	String noHTMLString = input.replaceAll("\\<.*?>","");
    	// Suppress "<"
    	//input = input.replace('<', '[');
    	
    	// Suppress ">"
    	//input = input.replace('>', ']');
    	return noHTMLString;
    }

	//
	// Instance data.
	//

	//
	// Constructors.
	//

	//
	// Instance methods.
	//
}
