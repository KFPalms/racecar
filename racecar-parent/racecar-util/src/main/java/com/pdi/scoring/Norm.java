/**
 * Copyright (c) 2008,2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.scoring;


/**
 * Norm is a data class encapsulating a norm.  The methods in this
 * class support operations that involve norms (predominately
 * scoring).  Most of the methods in this class are direct ports
 * from the Norm class we used in ADAPT.
 * 
 * @author		Ken Beukelman
 * @author		Mike Solomonson
 */
public class Norm
{

	//
	// Static data.
	//
 
	//
	// Static methods.
	//
	//
	
	/**
	 * Convenience method that returns a standard percentile.  In this
	 * context a "standard" percentile is a rounded and clipped (min = 1,
	 * max = 99) integer
	 * 
	 * @param zScore the z score value.
	 * @returns the percentile (unrounded)
	 */
	public static synchronized int calcIntPctlFromZ(double zScore)
	{
		// Get the full percentile, add 1/2 to round it up and convert it to an integer
		int pctl = (int) (Norm.calcDoublePctlFromZ(zScore) + 0.5);
		
		// clip it
		if (pctl > 99)
		{
			pctl = 99;
		}
		
		if (pctl < 1)
		{
			pctl = 1;
		}
		
		return pctl;
	}

	
	/**
	 * Calculates a stanine from a raw score.  Unlike most methods in this class
	 * which use the norm encapsulated in this class, this method is passed a
	 * norm (a mean and a standard deviation).
	 * 
	 * @param zScore the z score value.
	 * @returns the stanine
	 */
	public static synchronized int calcStanine(double raw, double mean, double std)
	{
		double rawZ = (raw - mean) / std;
		int rawPct = Norm.calcIntPctlFromZ(rawZ);
		int raw9 = Norm.calcStanineFromPercent(rawPct);
		return raw9;
	}


	/**
	 * Converts a standard percentile (1-99) into a stanine
	 * 
	 * Note that there is no data checking for invalid values (<1, > 99)
	 * 
	 * @param pctl the percentile score value.
	 * @returns the stanine
	 */
	// Note that this is duplicated in the following places:
	//	ScoreUtils.getStanine()
	//	Norm.calcStanineFromPercent()  <-- this method
	//  TLT_GROUP_DETAIL_REPORT_SCORING.findStanineFromPctl()
	//  TLT_INDIVIDUAL_SUMMARY_REPORT_SCORING.findStanineFromPctl()
	// TODO Should it be consolidated?
	public static synchronized int calcStanineFromPercent(int pctl)
	{
		if(pctl <= 3)
		{
			return 1;
		}
		else if(pctl > 3 && pctl < 11)
		{
			return 2;
		}
		else if(pctl >= 11 && pctl < 23)
		{
			return 3;
		}
		else if(pctl >= 23 && pctl < 40)
		{
			return 4;
		}
		else if(pctl >= 40 && pctl < 60)
		{
			return 5;
		}
		else if(pctl >= 60 && pctl < 77)
		{
			return 6;
		}
		else if(pctl >= 77 && pctl < 89)
		{
			return 7;
		}
		else if(pctl >= 89 && pctl < 97)
		{
			return 8;
		}
		else	// effectively, >= 97
		{
			return 9;
		}
	}


	/**
	 * Converts a standard percentile (1-99) into a PDI rating
	 * Note that as of 5/1/10, ratings are in lock-step with stanines
	 * 
	 * We probably should re-factor this (and the stanine
	 * calculator) into a scoring utility class
	 * 
	 * @param pctl the percentile score value.
	 * @returns the PDI rating as a string (for display purposes)
	 */
	// Note that this is duplicated in the following places:
	//	ScoreUtils.getPDIRating()
	//	Norm.calcPdiRating()  <-- this method
	// TODO Should it be consolidated?
	public static String calcPdiRating(int pctl)
	{
		int s9 = calcStanineFromPercent(pctl);
		
		switch (s9)
		{
		case 1:
			return "1.0";
		case 2:
			return "1.5";
		case 3:
			return "2.0";
		case 4:
			return "2.5";
		case 5:
			return "3.0";
		case 6:
			return "3.5";
		case 7:
			return "4.0";
		case 8:
			return "4.5";
		case 9:
			return "5.0";
		default:
			return "";	// Should never happen
		}
	}


	/**
	 * Converts a double percentile into a PDI rating by
	 * rounding the percentile, converting it to an int
	 * then calling the int based rating converter
	 * 
	 * @param pctl the percentile score value.
	 * @returns the PDI rating as a string (for display purposes)
	 */
	public static String calcPdiRating(double pctl)
	{
		return calcPdiRating((int) (Math.floor(pctl + 0.5d)));
	}


	/**
	 * Returns an unrounded percentile.  Uses the area calculation
	 * and then multiplies by 100
	 * 
	 * @param zScore the z score value.
	 * @returns the percentile (unrounded)
	 */
	public static synchronized double calcDoublePctlFromZ(double zScore)
	{
		return Norm.zareabot(zScore) * 100.0;
	}

	
	/**
	 * Given a z score on a standard normal distribution, return the proportion
	 * of the distribution that is below the z score (the percentile)
	 * Based on Kennedy & Gentle; probably accurate to 14 or 15 digits.
	 * 
	 * Note that the output is a decimal from 0 to 1
	 * 
	 * @param x the z score value.
	 * @returns the proportion of the distribution that is below the z score.
	 */
	private static synchronized double zareabot(double x)
	{
		double sn;
		double r1;
		double r2;
		double y;
		double y2;
		double erfval;
		double erfcval;
		double z;
		double phi;

		final double P1_0 = 242.667955230532;
		final double P1_1 = 21.9792616182942;
		final double P1_2 = 6.99638348861914;
		final double P1_3 = -3.56098437018154E-02;

		final double Q1_0 = 215.058875869861;
		final double Q1_1 = 91.1649054045149;
		final double Q1_2 = 15.0827976304078;
		final double Q1_3 = 1.0;

		final double P2_0 = 300.459261020162;
		final double P2_1 = 451.918953711873;
		final double P2_2 = 339.320816734344;
		final double P2_3 = 152.98928504694;
		final double P2_4 = 43.1622272220567;
		final double P2_5 = 7.21175825088309;
		final double P2_6 = 0.564195517478994;
		final double P2_7 = -1.36864857382717E-07;

		final double Q2_0 = 300.459260956983;
		final double Q2_1 = 790.950925327898;
		final double Q2_2 = 931.35409485061;
		final double Q2_3 = 638.980264465631;
		final double Q2_4 = 277.585444743988;
		final double Q2_5 = 77.0001529352295;
		final double Q2_6 = 12.7827273196294;
		final double Q2_7 = 1.0;

		final double P3_0 = -2.99610707703542E-03;
		final double P3_1 = -4.94730910623251E-02;
		final double P3_2 = -0.226956593539687;
		final double P3_3 = -0.278661308609648;
		final double P3_4 = -2.23192459734185E-02;

		final double Q3_0 = 1.06209230528468E-02;
		final double Q3_1 = 0.19130892610783;
		final double Q3_2 = 1.05167510706793;
		final double Q3_3 = 1.98733201817135;
		final double Q3_4 = 1.0;

		final double sqrt2  = 1.4142135623731;
		final double sqrtpi = 1.77245385090552;

		y = x / sqrt2;

		if (y < 0)
		{
			y  = -y;
			sn = -1;
		}
		else
		{
			sn = 1;
		}

		y2 = y * y;

		if (y < 0.46875)
		{
			r1 = ((P1_3 * y2 + P1_2) * y2 + P1_1) * y2 + P1_0;
			r2 = ((Q1_3 * y2 + Q1_2) * y2 + Q1_1) * y2 + Q1_0;
			erfval = y * r1 / r2;

			if (sn == 1.0)
			{
				phi = 0.5 + 0.5 * erfval;
			}
			else
			{
				phi = 0.5 - 0.5 * erfval;
			}
		}
		else
		{
			if (y < 4.0)
			{
				r1 = ((((((P2_7 * y + P2_6) * y + P2_5) * y + P2_4) * y + P2_3) * y + P2_2) * y + P2_1) * y + P2_0;
				r2 = ((((((Q2_7 * y + Q2_6) * y + Q2_5) * y + Q2_4) * y + Q2_3) * y + Q2_2) * y + Q2_1) * y + Q2_0;
				erfcval = java.lang.Math.exp(-y2) * r1 / r2;
			}
			else
			{
				z = y2 * y2;
				r1 = (((P3_4 * z + P3_3) * z + P3_2) * z + P3_1) * z + P3_0;
				r2 = (((Q3_4 * z + Q3_3) * z + Q3_2) * z + Q3_1) * z + Q3_0;
				erfcval = (java.lang.Math.exp(-y2) / y) * (1.0 / sqrtpi + r1 / (r2 * y2));
			}

			if (sn == 1.0)
			{
				phi = 1.0 - 0.5 * erfcval;
			}
			else
			{
				phi = 0.5 * erfcval;
			}
		}

		return phi;
	}


	//
	// Instance data.
	//
	private double _mean;
	private double _stdDev;	
	private double _weight;	
	private String _id;
	private String _name;

	
	//
	// Constructors.
	//
	/*
	 * "Standard" (no parameter) constructor
	 */
	public Norm()
	{
		// Does nothing presently
	}


	/*
	 * Initializing norm - Sets mean and stdDev
	 */
	public Norm(double mean, double stdDev)
	{
		_mean = mean;
		_stdDev = stdDev;
	}


	/*
	 * Initializing norm - Sets id, mean, and stdDev
	 */
	public Norm(String id, double mean, double stdDev)
	{
		_id = id;
		_mean = mean;
		_stdDev = stdDev;
	}


	//
	// Instance methods.
	//

	/**
	 * Calculates the Z score for an int score.
	 *
	 * @param score the score value.
	 * @throws Throwable
	 */
	public double calcZScore(int score)
	{
		return calcZScore((double)score);
	}


	/**
	 * Calculates the Z score for an Integer score.
	 *
	 * @param score the score value.
	 * @throws Throwable
	 */
	public double calcZScore(Integer score)
	{
		return calcZScore(score.doubleValue());
	}


	/**
	 * Calculates the Z score for a double score.
	 *
	 * @param score the score value (double).
	 * @throws Throwable
	 */
	public double calcZScore(double score)
	{
		return (score - this.getMean()) / this.getStdDev();
	}

	
	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
		
	/*****************************************************************************************/
	public double getMean()
	{
		return _mean;
	}
	
	public void setMean(double value)
	{
		_mean = value;
	}
			
	/*****************************************************************************************/
	public double getStdDev()
	{
		return _stdDev;
	}
	
	public void setStdDev(double value)
	{
		_stdDev = value;
	}

	/*****************************************************************************************/
	public double getWeight()
	{
		return _weight;
	}
	
	public void setWeight(double value)
	{
		_weight = value;
	}
	
	/*****************************************************************************************/
	public String getId()
	{
		return _id;
	}
	
	public void setId(String value)
	{
		_id = value;
	}

	/*****************************************************************************************/
	public String getName()
	{
		return _name;
	}
	
	public void setName(String value)
	{
		_name = value;
	}


	/**
	 * Describes the object.
	 * @return the description of the object.
	 */
	public String toString()
	{
		String str = "Norm:/n";
		str += "mean=" + _mean + ", Std. Dev.=" + _stdDev + ", wgt=" + (_weight == 0.0 ? "---" : _weight) + "/n";
		str += "name=" + (_name == null ? "---" : _name) + ", id=" + (_id== null ? "---" : _id) + "/n";
		str += "super=" + super.toString() + "/n";

		//return "Norm: " + super.toString();
		return str;
	}
}
