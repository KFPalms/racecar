package com.pdi.scoring;

//import java.sql.Connection;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.sql.Statement;
import java.util.ArrayList;
//import java.util.Iterator;
//import java.util.LinkedHashMap;
//import java.util.List;
//import java.util.Map;

/**
 * NormSet is data class for collections of related norms.
 * Comment:  It appears that this got larded with a bunch of only-
 * mildly-related stuff when the TPOT and Target code was written.
 * 
 * @author		Mike Solomonson
 * @author 		MB Panichi (port from .as2 to .java)
 */
public class NormSet
{
	//
	// Static data.
	//
 
	//
	// Static methods.
	//

	// NOTE:  These methods appear to be code used for Target that was ported
	//        but is not used in Oxcart.  It is present here but commented out.
	//        Retained so that the code is not lost.
//	/**
//	 * Return an ordered Map of norms keyed by norm set name
//	 *
//	 * @param con connection to the database
//	 * @param normSetNames list of norm sets to fetch
//	 * @returns Map of lists of norms
//	 */
//	public static Map<String,List<Norm>> fetchNormSets(Connection con, List<String> normSetNames)
//		throws Exception
//	{
//		Map<String,List<Norm>> returnMap = new LinkedHashMap<String,List<Norm>>();
//	    Statement stmt = null;
//	    ResultSet rs = null;
//	    
//	    String normSetsRequested = "";
//	    for (Iterator<String> iter = normSetNames.iterator(); iter.hasNext(); )
//	    {
//	    	String normSetName = (String) iter.next();
//	    	
//	    	if ( normSetsRequested.trim().length() < 1 )
//	    	{
//	    		normSetsRequested += " '" + normSetName + "' ";
//	    	}
//	    	else
//	    	{
//	    		normSetsRequested += ", '" + normSetName + "' ";
//	    	}	    	
//	    }
//		
//		StringBuffer sqlQuery = new StringBuffer();
//		sqlQuery.append("SELECT ns.scoringStepName, ");
//		sqlQuery.append("       n.normId, ");
//		sqlQuery.append("       n.labelKey, ");
//		sqlQuery.append("       n.mean, ");
//		sqlQuery.append("       n.standardDeviation ");
//		sqlQuery.append("  FROM pdi_normset ns, pdi_norm n ");  
//		sqlQuery.append("  WHERE ns.scoringStepName IN (" + normSetsRequested + ") ");
//		sqlQuery.append("    AND ns.active = 1 ");
//		sqlQuery.append("    AND n.normsetId = ns.normsetId ");  
//		sqlQuery.append("    AND ns.active = 1");
//		sqlQuery.append("  ORDER BY ns.scoringStepName, n.normId ");
//		
//	    try
//		{
//	       	stmt = con.createStatement();
//	       	rs = stmt.executeQuery(sqlQuery.toString());
//	       	
//	       	if (! rs.isBeforeFirst())
//	       	{
//	       		throw new Exception("Norm data not available for: " + normSetsRequested);
//	       	}
//	       	
//	       	while (rs.next())
//	       	{
//	       		List<Norm> normSetList = null;
//	       		String normSetNm = rs.getString("scoringStepName");
//	       		
//	       		if (returnMap.containsKey(normSetNm))
//	       		{
//	       			normSetList = returnMap.get(normSetNm);
//	       		}
//	       		else
//	       		{
//	       			normSetList = new ArrayList<Norm>();
//	       			returnMap.put(normSetNm, normSetList);
//	       		}
//	
//	       		
//	       		Norm aNorm = new Norm();
//	       		aNorm.setName(rs.getString("labelKey"));
//	       		aNorm.setMean(rs.getDouble("mean"));
//	       		aNorm.setStdDev(rs.getDouble("standardDeviation"));
//	       		normSetList.add(aNorm);
//	       	}
//		}
//	    catch (SQLException ex)
//		{
//	    	// handle any errors
//	    	throw new Exception("SQL fetching norm data failed for: " + normSetsRequested +
//	    			". SQL: " + sqlQuery +
//	    			", SQLException: " + ex.getMessage() + ", " +
//					"SQLState: " + ex.getSQLState() + ", " +
//					"VendorError: " + ex.getErrorCode());
//		}
//	    finally
//		{
//	        if (rs != null)
//	        {
//	            try
//				{
//	                rs.close();
//	            }
//	            catch (SQLException sqlEx)
//				{
//	            	// swallow the error
//	            }
//	            rs = null;
//	        }
//	        if (stmt != null)
//	        {
//	            try
//				{
//	                stmt.close();
//	            }
//	            catch (SQLException sqlEx)
//				{
//	            	// swallow the error
//				}
//	            stmt = null;
//	        }
//	    }		
//	
//		return returnMap;
//	}
//	
//	/**
//	 * Get a list of norms in a single normset by normset name
//	 *
//	 * @param con a database connection
//	 * @param normSetName the name of the normset to retrieve
//	 * @returns a list of the norms
//	 */
//	public static List<Norm> fetchNormSet(Connection con, String normSetName)
//		throws Exception
//	{
//		ArrayList<Norm> returnList = new ArrayList<Norm>();
//	    Statement stmt = null;
//	    ResultSet rs = null;
//		
//		StringBuffer sqlQuery = new StringBuffer();
//		sqlQuery.append("SELECT n.normId, ");
//		sqlQuery.append("       n.labelKey, ");
//		sqlQuery.append("       n.mean, ");
//		sqlQuery.append("       n.standardDeviation ");
//		sqlQuery.append("  FROM pdi_normset ns, pdi_norm n ");  
//		sqlQuery.append("  WHERE ns.scoringStepName = '" + normSetName + "' ");
//		sqlQuery.append("    AND ns.active = 1 ");
//		sqlQuery.append("    AND n.normsetId = ns.normsetId ");  
//		sqlQuery.append("    AND ns.active = 1");
//		sqlQuery.append("  ORDER BY n.normId ");
//		
//	    try
//		{
//	       	stmt = con.createStatement();
//	       	rs = stmt.executeQuery(sqlQuery.toString());
//	       	
//	       	if (! rs.isBeforeFirst())
//	       	{
//	       		throw new Exception("Norm data not available for: " + normSetName);
//	       	}
//	       	
//	       	while (rs.next())
//	       	{
//	       		Norm aNorm = new Norm();
//	       		aNorm.setName(rs.getString("labelKey"));
//	       		aNorm.setMean(rs.getDouble("mean"));
//	       		aNorm.setStdDev(rs.getDouble("standardDeviation"));	
//	       		returnList.add(aNorm);
//	       	}
//		}
//	    catch (SQLException ex)
//		{
//	    	// handle any errors
//	    	throw new Exception("SQL fetching norm data failed for: " + normSetName +
//	    			". SQL: " + sqlQuery +
//	    			", SQLException: " + ex.getMessage() + ", " +
//					"SQLState: " + ex.getSQLState() + ", " +
//					"VendorError: " + ex.getErrorCode());
//		}
//	    finally
//		{
//	        if (rs != null)
//	        {
//	            try
//				{
//	                rs.close();
//	            }
//	            catch (SQLException sqlEx)
//				{
//	            	// swallow the error
//	            }
//	            rs = null;
//	        }
//	        if (stmt != null)
//	        {
//	            try
//				{
//	                stmt.close();
//	            }
//	            catch (SQLException sqlEx)
//				{
//	            	// swallow the error
//				}
//	            stmt = null;
//	        }
//	    }		
//
//		return returnList;
//	}
//
//	/**
//	 * Get a map of the norms in a normset keyed on the norm label key
//	 *
//	 * @param con a database connection
//	 * @param normSetName the name of the normset to fetch
//	 * @returns 
//	 */
//	public static Map<String,Norm> fetchNormSetMap(Connection con, String normSetName)
//		throws Exception
//	{
//		Map<String,Norm> returnMap = new LinkedHashMap<String,Norm>();
//	    Statement stmt = null;
//	    ResultSet rs = null;
//		
//		StringBuffer sqlQuery = new StringBuffer();
//		sqlQuery.append("SELECT n.normId, ");
//		sqlQuery.append("       n.labelKey, ");
//		sqlQuery.append("       n.mean, ");
//		sqlQuery.append("       n.standardDeviation ");
//		sqlQuery.append("  FROM pdi_normset ns, pdi_norm n ");  
//		sqlQuery.append("  WHERE ns.scoringStepName = '" + normSetName + "' ");
//		sqlQuery.append("    AND ns.active = 1 ");
//		sqlQuery.append("    AND n.normsetId = ns.normsetId ");  
//		sqlQuery.append("    AND ns.active = 1");
//		sqlQuery.append("  ORDER BY n.normId ");
//		
//	    try
//		{
//	       	stmt = con.createStatement();
//	       	rs = stmt.executeQuery(sqlQuery.toString());
//	       	
//	       	if (! rs.isBeforeFirst())
//	       	{
//	       		throw new Exception("Norm data not available for: " + normSetName);
//	       	}
//	       	
//	       	while (rs.next())
//	       	{
//	       		Norm aNorm = new Norm();
//	       		String normLabel = rs.getString("labelKey");
//	       		aNorm.setName(normLabel);
//	       		aNorm.setMean(rs.getDouble("mean"));
//	       		aNorm.setStdDev(rs.getDouble("standardDeviation"));	
//	       		returnMap.put(normLabel, aNorm);
//	       	}
//		}
//	    catch (SQLException ex)
//		{
//	    	// handle any errors
//	    	throw new Exception("SQL fetching norm data failed for: " + normSetName +
//	    			". SQL: " + sqlQuery +
//	    			", SQLException: " + ex.getMessage() + ", " +
//					"SQLState: " + ex.getSQLState() + ", " +
//					"VendorError: " + ex.getErrorCode());
//		}
//	    finally
//		{
//	        if (rs != null)
//	        {
//	            try
//				{
//	                rs.close();
//	            }
//	            catch (SQLException sqlEx)
//				{
//	            	// swallow the error
//	            }
//	            rs = null;
//	        }
//	        if (stmt != null)
//	        {
//	            try
//				{
//	                stmt.close();
//	            }
//	            catch (SQLException sqlEx)
//				{
//	            	// swallow the error
//				}
//	            stmt = null;
//	        }
//	    }		
//
//		return returnMap;
//	}	


	//
	// Instance data.
	//
	private Integer _normsetId;
	private String _displayName;
	private ArrayList<Norm> _norms;
	private String _projectId;
	private String _moduleId;
	private int _transitionLevel;
	private int _transitionType;
	private String _scoringStepName;
	private String _generalPopulation;
	private int _active = 1;

	//
	// Constructors.
	//
	public NormSet()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//

	//*******************************************************************************************//
	//																							 //
	//                                       Get/Set Functions                                   //
	//																							 //
	//*******************************************************************************************//
	
	/*****************************************************************************************/
	public Integer getNormsetId()
	{
		return _normsetId;
	}

	public void setNormsetId(Integer value)
	{
		_normsetId = value;
	}
	
	/*****************************************************************************************/
	public String getDisplayName()
	{
		return _displayName;
	}
	
	public void setDisplayName(String value)
	{
		_displayName = value;
	}

	/*****************************************************************************************/
	public ArrayList<Norm> getNorms()
	{
		if(_norms == null)
		{
			_norms = new ArrayList<Norm>();
		}
		
		return _norms;
	}

	public void setNorms(ArrayList<Norm> value)
	{
		_norms = value;
	}
	
	/*****************************************************************************************/
	public String getProjectId()
	{
		return _projectId;
	}
	
	public void setProjectId(String value)
	{
		_projectId = value;
	}
	
	/*****************************************************************************************/
	public String getModuleId()
	{
		return _moduleId;
	}
	
	public void setModuleId(String value)
	{
		_moduleId = value;
	}	

	/*****************************************************************************************/
	public int getTransitionLevel()
	{
		return _transitionLevel;
	}

	public void setTransitionLevel(int value)
	{
		_transitionLevel = value;
	}

	/*****************************************************************************************/
	public int getTransitionType()
	{
		return _transitionType;
	}
	
	public void setTransitionType(int value)
	{
		_transitionType = value;
	}	
	
	/*****************************************************************************************/
	public String getScoringStepName()
	{
		return _scoringStepName;
	}
	
	public void setScoringStepName(String value)
	{
		_scoringStepName = value;
	}

	/*****************************************************************************************/
	public String getGeneralPopulation()
	{
		return _generalPopulation;
	}
	
	public void setGeneralPopulation(String value)
	{
		_generalPopulation = value;
	}		

	/*****************************************************************************************/
	public int getActive()
	{
		return _active;
	}
	
	public void setActive(int value)
	{
		_active = value;
	}		

	
	/*
	 * Convenience method to build a key
	 */
	public String getKey()
	{
		return _displayName + "|" + _scoringStepName + "|" + _transitionLevel;
	}
}
