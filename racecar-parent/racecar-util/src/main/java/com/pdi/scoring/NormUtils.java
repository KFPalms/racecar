package com.pdi.scoring;

public class NormUtils {
	public static double getPercentileFloat(double z) {
		 return calcDoublePctlFromZ(z);
	}
	
	public static int getPercentileInt(double z) {
		return calcIntPctlFromZ(z);
	}

	/**
	 * All the following code has been stolen from the com.pdi.scoring.Norm.java.
	 * That project/package is not visible from here.  At some point, we need to
	 * make there be only ONE Norm.java class, which is visible through out the
	 * system. Before Ken and I kill each other.
	 * 
	 */
	
	/**
	 * Convenience method that returns a standard percentile.  In this
	 * context a "standard" percentile is a rounded and clipped (min = 1,
	 * max = 99) integer
	 * 
	 * @param zScore the z score value.
	 * @returns the percentile (unrounded)
	 */
	private static synchronized int calcIntPctlFromZ(double zScore)
	{
		// Get the full percentile, add 1/2 to round it up and convert it to an integer
		int pctl = (int) (calcDoublePctlFromZ(zScore) + 0.5);
		
		// clip it
		if (pctl > 99)
		{
			pctl = 99;
		}
		
		if (pctl < 1)
		{
			pctl = 1;
		}
		
		return pctl;
	}
	
	/**
	 * Returns an unrounded percentile.  Uses the area calculation
	 * and then multiplies by 100
	 * 
	 * @param zScore the z score value.
	 * @returns the percentile (unrounded)
	 */
	public static synchronized double calcDoublePctlFromZ(double zScore)
	{
		return zareabot(zScore) * 100.0;
	}

	
	/**
	 * Given a z score on a standard normal distribution, return the proportion
	 * of the distribution that is below the z score (the percentile)
	 * Based on Kennedy & Gentle; probably accurate to 14 or 15 digits.
	 * 
	 * Note that the output is a decimal from 0 to 1
	 * 
	 * @param x the z score value.
	 * @returns the proportion of the distribution that is below the z score.
	 */
	private static synchronized double zareabot(double x)
	{
		double sn;
		double r1;
		double r2;
		double y;
		double y2;
		double erfval;
		double erfcval;
		double z;
		double phi;

		final double P1_0 = 242.667955230532;
		final double P1_1 = 21.9792616182942;
		final double P1_2 = 6.99638348861914;
		final double P1_3 = -3.56098437018154E-02;

		final double Q1_0 = 215.058875869861;
		final double Q1_1 = 91.1649054045149;
		final double Q1_2 = 15.0827976304078;
		final double Q1_3 = 1.0;

		final double P2_0 = 300.459261020162;
		final double P2_1 = 451.918953711873;
		final double P2_2 = 339.320816734344;
		final double P2_3 = 152.98928504694;
		final double P2_4 = 43.1622272220567;
		final double P2_5 = 7.21175825088309;
		final double P2_6 = 0.564195517478994;
		final double P2_7 = -1.36864857382717E-07;

		final double Q2_0 = 300.459260956983;
		final double Q2_1 = 790.950925327898;
		final double Q2_2 = 931.35409485061;
		final double Q2_3 = 638.980264465631;
		final double Q2_4 = 277.585444743988;
		final double Q2_5 = 77.0001529352295;
		final double Q2_6 = 12.7827273196294;
		final double Q2_7 = 1.0;

		final double P3_0 = -2.99610707703542E-03;
		final double P3_1 = -4.94730910623251E-02;
		final double P3_2 = -0.226956593539687;
		final double P3_3 = -0.278661308609648;
		final double P3_4 = -2.23192459734185E-02;

		final double Q3_0 = 1.06209230528468E-02;
		final double Q3_1 = 0.19130892610783;
		final double Q3_2 = 1.05167510706793;
		final double Q3_3 = 1.98733201817135;
		final double Q3_4 = 1.0;

		final double sqrt2  = 1.4142135623731;
		final double sqrtpi = 1.77245385090552;

		y = x / sqrt2;

		if (y < 0)
		{
			y  = -y;
			sn = -1;
		}
		else
		{
			sn = 1;
		}

		y2 = y * y;

		if (y < 0.46875)
		{
			r1 = ((P1_3 * y2 + P1_2) * y2 + P1_1) * y2 + P1_0;
			r2 = ((Q1_3 * y2 + Q1_2) * y2 + Q1_1) * y2 + Q1_0;
			erfval = y * r1 / r2;

			if (sn == 1.0)
			{
				phi = 0.5 + 0.5 * erfval;
			}
			else
			{
				phi = 0.5 - 0.5 * erfval;
			}
		}
		else
		{
			if (y < 4.0)
			{
				r1 = ((((((P2_7 * y + P2_6) * y + P2_5) * y + P2_4) * y + P2_3) * y + P2_2) * y + P2_1) * y + P2_0;
				r2 = ((((((Q2_7 * y + Q2_6) * y + Q2_5) * y + Q2_4) * y + Q2_3) * y + Q2_2) * y + Q2_1) * y + Q2_0;
				erfcval = java.lang.Math.exp(-y2) * r1 / r2;
			}
			else
			{
				z = y2 * y2;
				r1 = (((P3_4 * z + P3_3) * z + P3_2) * z + P3_1) * z + P3_0;
				r2 = (((Q3_4 * z + Q3_3) * z + Q3_2) * z + Q3_1) * z + Q3_0;
				erfcval = (java.lang.Math.exp(-y2) / y) * (1.0 / sqrtpi + r1 / (r2 * y2));
			}

			if (sn == 1.0)
			{
				phi = 1.0 - 0.5 * erfcval;
			}
			else
			{
				phi = 0.5 * erfcval;
			}
		}

		return phi;
	}
	
	
}
