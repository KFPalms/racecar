package com.pdi.scoring;

public class ScoreUtils {
	/**
	 * getPDIRating - Determines the PDI rating based upon the stanine, which is based upon the percentile.
	 *                This method takes a percentile as input, calulates the stanine, then uses that value
	 *                to determine the PDI rating.
	 * @param percentile
	 * @return
	 */
	// Note that this is duplicated in the following places:
	//	ScoreUtils.getPDIRating()  <-- this method
	//	Norm.calcPdiRating()
	// TODO Should it be consolidated?
	public static Double getPDIRating(int percentile) {
		switch (getStanine(percentile))
		{
		case 1:
			return 1.0;
		case 2:
			return 1.5;
		case 3:
			return 2.0;
		case 4:
			return 2.5;
		case 5:
			return 3.0;
		case 6:
			return 3.5;
		case 7:
			return 4.0;
		case 8:
			return 4.5;
		case 9:
			return 5.0;
		default:
			return 0.0;	// Should never happen
		}
	}


	/**
	 * getStanine - calculates the stanine, based upon the percentile
	 * @param percentileScore
	 * @return
	 */
	// Note that this is duplicated in the following places:
	//	ScoreUtils.getStanine()  <-- this method
	//	Norm.calcStanineFromPercent()
	//  TLT_GROUP_DETAIL_REPORT_SCORING.findStanineFromPctl()
	//  TLT_INDIVIDUAL_SUMMARY_REPORT_SCORING.findStanineFromPctl()
	// TODO Should it be consolidated?
	public static int getStanine(int percentileScore) {
		int stanine = 0;
		
		if (percentileScore < 4)
		{
			stanine = 1;
		}
		else if (percentileScore > 3 && percentileScore < 11)
		{
			stanine = 2;
		}
		else if (percentileScore > 10 && percentileScore < 23)
		{
			stanine = 3;
		}
		else if (percentileScore > 22 && percentileScore < 40)
		{
			stanine = 4;
		}
		else if (percentileScore > 39 && percentileScore < 60)
		{
			stanine = 5;
		}
		else if (percentileScore > 59 && percentileScore < 77)
		{
			stanine = 6;
		}
		else if (percentileScore > 76 && percentileScore < 89)
		{
			stanine = 7;
		}
		else if (percentileScore > 88 && percentileScore < 97)
		{
			stanine = 8;
		}
		else if (percentileScore > 96)
		{
			stanine = 9;
		}
	
		return stanine;
	}

	/**
	 * getNarrFromPctl - Calculate the interp level (1 - 5) from the percentile score
	 * @param pctl
	 * @return
	 */
	public static synchronized int getNarrFromPctl(int pctl)
	{
		if(pctl < 16)
		{
			return 1;
		}
		else if(pctl >= 16 && pctl < 31)
		{
			return 2;
		}
		else if(pctl >= 31 && pctl < 70)
		{
			return 3;
		}
		else if(pctl >= 70 && pctl < 85)
		{
			return 4;
		}
		else // >= 85
		{
			return 5;
		}
	}
}
