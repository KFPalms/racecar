package com.pdi.properties;

import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@Startup
@Singleton(mappedName = "propertyLoader")
public class PropertyLoaderSingleton {
	
	@Resource(mappedName="java:global/custom/abyd_properties")
	private Properties jbossProps;
	
	private static final Logger log = LoggerFactory.getLogger(PropertyLoader.class);
	
	@PostConstruct
	private void init(){
		
	}
	
	public String getProperty(String key){
		if (jbossProps != null){
			log.debug("Properties NOT null, returning: {}", jbossProps.getProperty(key));
			return jbossProps.getProperty(key);
		} else {
			return null;
		}
	}

	public Properties getJbossProps() {
		return jbossProps;
	}
}
