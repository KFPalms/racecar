package com.pdi.properties;

import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kf.palms.utils.JNDILookupEJB;


//import com.ninthhouse.utils.ErrorCode;
//import com.ninthhouse.utils.LogWriter;
//import com.ninthhouse.utils.NHNException;
//import com.ninthhouse.utils.NHNPropertiesReader;
//import com.ninthhouse.utils.NHProperties;

public class PropertyLoader
{
	@Resource(mappedName="java:global/custom/abyd_properties")
	private Properties jbossProps;
	
	private static final Logger log = LoggerFactory.getLogger(PropertyLoader.class);
    
    /**
     * file is now ignored.  only used for logging to identify original property file for missed properties f
     * from migration
    **/
    public static String getProperty( String file, String property) {
    	Properties props = getProperties();
    	if (props != null){
    		if (props.getProperty(property) != null){
    			log.trace("Returning property value for '{}': {}", property, props.getProperty(property));
    			return props.getProperty(property);
    		} else {
    			log.debug("Failed to find property: {} in jboss properties.  Original File specified was: {}", property, file);
    		}
    	}
    	return null;
    }
    
    public static String getProperty(String propertyName){
    	return PropertyLoader.getProperty(null, propertyName);
    }
   
    
    private static Properties getProperties(){
    	
		try {
			PropertyLoaderSingleton singleton = (PropertyLoaderSingleton) JNDILookupEJB.lookupBeanInGlobalContext(PropertyLoaderSingleton.class.getName());
			return singleton.getJbossProps();
			/*
			Properties props = (Properties) new InitialContext().lookup("java:global/custom/abyd_properties");

			log.debug("Got {} properties!", props.size());
			return props;
			*/
		} catch (Exception e) {
			log.error("Error looking up properties reference; {}", e.getMessage());
			e.printStackTrace();
		}
		return null;
    	
    }
    
    
    public static String getProperty( String file, String property, String locale) {
    	log.debug("In get Property with locale: {} and property name: {} and file name: {}", locale, property, file);
    	String environment = PropertyLoader.getEnvironment();
    	ResourceBundle rb = PropertyLoader.loadProperties(file,locale);
    	String returnvalue = "";
    	try
    	{
    		returnvalue = rb.getString(environment+"."+property);
    	}
    	catch(Exception e1)
    	{
    		try
    		{
    			returnvalue = rb.getString(property);
    		}
    		catch(Exception e2)
    		{
			}
    	}
    	return returnvalue;
    }
    
    private static ResourceBundle loadProperties (final String name, String loc)     {
    	Locale locale;
    	try{
    		locale = new Locale(loc);
    	}catch (Exception e) {
				locale = Locale.getDefault();
			}
    	return ResourceBundle.getBundle(name, locale);
    }
    
    /**
     * A convenience overload of {@link #loadProperties(String, ClassLoader)}
     * that uses the current thread's context classloader.
     */
    private static ResourceBundle loadProperties (final String name)
    {
    	return ResourceBundle.getBundle(name, Locale.getDefault());
    }

    
    private static String getEnvironment()
    {
    	try
    	{
    		// Cannot hard-code one or the othe environment in here
    		//return NHNPropertiesReader.getValue("global.environment");
    		
    		return getProperty(null, "global.environment");

    		//log.debug("Returning global environment: {} from internal application properties file", PropertyLoader.loadProperties("application").getString("global.environment"));
    		//return PropertyLoader.loadProperties("application").getString("global.environment");
    	}
    	catch (Exception e)
    	{
    	}
    	//cannot get application... this must be an internal unit test
		return "UNT";
    }
   

	public Properties getJbossProps() {
		return jbossProps;
	}
    
} // End of class



