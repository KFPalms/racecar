package com.pdi.string;

//import java.nio.ByteBuffer;
//import java.nio.CharBuffer;
//import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
//import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;

public class StringUtils {
	
	public static String removeSpecialCharacters(String str) {
		String pattern = "[^A-Za-z0-9]";
        String clean = str.replaceAll(pattern, " ");
        
        return clean;
	}


	/**
	 * removeFilenameSpecialCharacters - Suppress those characters that are illegal in a Windows file name
	 * @param str
	 * @return
	 */
	public static String removeFilenameSpecialCharacters(String str) {
		String pattern = "[\\\\/:*?\"<>|]";
        String clean = str.replaceAll(pattern, " ");
        
        return clean;
	}
	
	  static CharsetEncoder isoEncoder = Charset.forName("ISO-8859-1").newEncoder(); // or "ISO-8859-1" for ISO Latin 1

	  static int LIMIT = 47;
	  static String BGN_SPAN = "<span>";
	  static String END_SPAN ="</span>";
	  static int[] watchForChar = {46, 44, 37, 65292, 65289, 12289, 12290, 12540, 12301, 12353, 12355, 12357, 12359, 12361, 12419, 12421, 12423, 12449, 12451, 12453, 12455, 12457, 12515, 12517, 12519, 12387, 12483};
	 // static int[] allowBreakBefore = {12300, 65288, 12354, 12356, 12358, 12360, 12362, 12420, 12422, 12424, 12388, 12450, 12452, 12454, 12456, 12458, 12516, 12518, 12520, 12484};	
	  //for reference of char as int : http://www.rishida.net/tools/conversion/
	 
	  public static boolean isISOString(String v) {
		  	return isoEncoder.canEncode(v);
	  }
	  
	  /*
	   * FORMAT STINGS WITHOUT SPACES (JAPANESE/CHINESE)
	   * in our current reporting engine, if there are no spaces, text can run right out of it's bounding box and off the page.
	   * adding span tags: <span>lotsofnonspacingcharacters</span><span>thatwillsimplyneverbreakunlessweusespantags</span>
	   * spans create invisible points where the HTML text will break if needed. but not add a visible space where wrapping is not needed. Yay! -ky
	   </span><span> Rules:
		  //		1.) 48 chars w/o spaces
		  
		  //		2.) RESET count at " " (blank space)
		  
		  //		3.) if next char is contained in int[] watchForChar (commas, periods, etc) include it before the </span>  
		  
		  //		4.) before and after 1-9
		  //	add </span><span> after each Japanese comma and period
		  // 	add </span> at end of each string 
		  //	(NEVER end string with </span></span>!!!)
	   */
	  public static String formatNonSpacingTxt(String tIn) {
		  if(tIn == null){
			  return null;
		  }
		  
		  // make a copy
		  String txtIn = new String(tIn);
		  
		  // Translate the carriage returns
		  txtIn = txtIn.replace("\\\\r" , "<br/>");
		  txtIn = txtIn.replace("\\r" , "<br/>");

		  int txtInLength = txtIn.length();
		  if(txtInLength <= LIMIT){
			  return txtIn;
		  }
		  
		  int count = 0;
		  
		  String txtOut = BGN_SPAN;
		  char nextC;
		  int i = 0;
		/* start inspection */
		  while(i < txtInLength){
			  	count ++;
			    char c = txtIn.charAt(i);
		/* 2 */
			    if(c == ' '){
			    	txtOut += c;
			    	count = 0;
			    	i++;
			    	continue;
			    }
			   
			    //Check for HTML tags and do not inspect them
			    if(c == '<'){
				    	Boolean isHTML = true;

				    	//Make HTML TAG STRING
				    	String htmlTag = "";
				    	while(isHTML){
				    		htmlTag += c;
					    	i++;
					    	c = txtIn.charAt(i);
					    	if(i+1 < txtInLength){
						    	 //look at the next character, is it close carrot?
						    	//FINISH TAG
						    	if(c == '>'){
						    		isHTML = false;
						    		htmlTag += c;
						    		if(htmlTag != "<br/>" || htmlTag != "<br>" || htmlTag != "</br>"){
						    			//<span> ... </span><p><span> ... </span></p><span>
						    			txtOut += (END_SPAN + htmlTag + BGN_SPAN);
						    			count = 0;
						    			continue;
						    		}else{
						    			// add the 'br' tag, reset the count.
						    			txtOut += htmlTag;
					    				count = 0;
					    				continue;
						    		}
						    	}
					    	}else{  //should never happen...  unless < is last char... in this case lets add it and be done.
					    		isHTML = false;
					    		htmlTag += c;
					    		txtOut += END_SPAN + htmlTag + BGN_SPAN;
					    		count = 0;
					    		i++; //this is actually unnecessary
					    		continue;
					    	}
				    	}
			    		i++;
				    	continue;	// while(i < txtInLength)
			    }
			    
			    //watch for escaped characters like  &#174; (R) and add them without inspecting
			    if(c == '&'){
		    		String sx = "";
			    	for(int j = 0; j < 8; j++){
			    		if (i+j >= txtIn.length()){
			    			break;
			    		}
			    		char cx = txtIn.charAt(i+j);
			    		sx += cx;
			    	}
			    	if(sx.contains(";")){
			    		Boolean isSpecial = true;
			    		while(isSpecial){
			    			txtOut += c;
					    	i++;
					    	c = txtIn.charAt(i);
					    	if(c == ';'){
					    		txtOut += c;
					    		i++;
					    		if (i < txtIn.length())
					    		{
					    			// Only get the character if there is one to get
					    			c = txtIn.charAt(i);
					    		}
					    		isSpecial = false;
					    	}
			    		}
			    		continue;
			    	}
			    }
		/* 4 */			
//			    if(Character.isDigit(c)){
//			    	txtOut += (END_SPAN + BGN_SPAN + c);
//			    	count = 0;
//			    	Boolean dig = true;
//			    	while(dig == true){
//			    		 if(i+1 < txtInLength){
//						    	nextC = txtIn.charAt(i+1);
//						    	int nx = nextC;
//						    	Boolean contained = false;
//						    	for(int k=0 ; k < watchForChar.length ; k++){
//									if(watchForChar[k] == nx){	contained = true;	}
//								}
//						    	if((Character.isDigit(nextC) || contained) && count != LIMIT){
//					    			txtOut += nextC;
//					    			count++;
//					    			i++;
//						    	}else{
//						    		dig = false;
//						    	}
//						 }else{
//							 dig = false;
//						 }
//			    	}
//			    	txtOut += (END_SPAN + BGN_SPAN);
//			    	count = 0;
//			    	i++;
//			    	continue;
//			    }
		/* 1 */
			  //adding watch for Japanese period and comma and add end/begin span + restart count NHN-1626 2/3/12	
			    	//this is to add more places to break line so text will fill the page/container width better.
			    		//removing period and comma rule - || cx == 12289 || cx == 12290 - from if below. NHN-1793 Jessee, Mel added a comment - 29/Feb/12 2:43 PM
			    
			    //int cx = c;
			    //Boolean addbreak = false;
			    if(i+1 < txtInLength){
					nextC = txtIn.charAt(i+1);
//					int nx = nextC;
//					for(int k=0 ; k < allowBreakBefore.length ; k++){
//						if(allowBreakBefore[k] == nx){	addbreak = true;	}  //commented out per Jira NHN-2177
//					}
			    }
			    //if(count == LIMIT || addbreak || cx == 12289 || cx == 12290){
			    if(count == LIMIT || isCharCJK(c)){
		/* 3 */ 
			    	if(i+1 < txtInLength){
						nextC = txtIn.charAt(i+1);
						int nx = nextC;
						
						Boolean contained = false;
						for(int k=0 ; k < watchForChar.length ; k++){
							if(watchForChar[k] == nx){	contained = true;	}
						}
						if(!contained)
						{ 
				    		txtOut += c;
				    		i++; // for c
				    	}else{
				    		//next char needs to be included before close span
				    		txtOut += c;
				    		i++; // for c
				    		Boolean include = true;
					    	while(include == true){ //check chars following next char...
								String xx = String.valueOf(nextC);
								txtOut += (xx);
								i++; // for xx
								if(i+1 < txtInLength){
									nextC = txtIn.charAt(i); //! CHANGE DEC 2012 -bug fix
									nx = nextC;
									contained = false;
									for(int k=0 ; k < watchForChar.length ; k++){
										if(watchForChar[k] == nx){	contained = true;	}
									}
									if(!contained){
										include = false; //allow close span..
									}else{
										continue; //don't close span, add the following char
									}
						    	
								}else{
									include = false;
								}
					    	}
					    }
							count = 0;
					    	txtOut += (END_SPAN + BGN_SPAN);
					    	continue;
			    	}else{
			    		txtOut += c;
			    		i++; // for c
			    		count = 0;
			    		continue;
			    	}
				}else{
				    txtOut += c;
		    		i++;
		    		continue;
				}
		  }
		/* end inspection */ 
		  
		  if(!txtOut.endsWith(END_SPAN)){
			  txtOut += END_SPAN;
		  }
		  
		  String finalOut = txtOut.replace(BGN_SPAN + END_SPAN, "");

		  return finalOut;
	  }
	  /**
	   * Returns if a character is one of Chinese-Japanese-Korean characters.
	   * 
	   * @param c
	   *            the character to be tested
	   * @return true if CJK, false otherwise
	   */
	  private static boolean isCharCJK(char c) {
	      if ((Character.UnicodeBlock.of(c) == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS)
	              || (Character.UnicodeBlock.of(c) == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A)
	              || (Character.UnicodeBlock.of(c) == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B)
	              || (Character.UnicodeBlock.of(c) == Character.UnicodeBlock.CJK_COMPATIBILITY_FORMS)
	              || (Character.UnicodeBlock.of(c) == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS)
	              || (Character.UnicodeBlock.of(c) == Character.UnicodeBlock.CJK_RADICALS_SUPPLEMENT)
	              || (Character.UnicodeBlock.of(c) == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION)
	              || (Character.UnicodeBlock.of(c) == Character.UnicodeBlock.ENCLOSED_CJK_LETTERS_AND_MONTHS)) {
	          return true;
	      }
	      return false;
	  }
}
