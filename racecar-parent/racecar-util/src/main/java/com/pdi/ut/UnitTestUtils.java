/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.ut;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/**
 * Helpful utilities for Unit Testing
 * 
 *
 * @author		Gavin Myers
 */
public class UnitTestUtils {
	/**
	 * Caller before any unit test begins
	 * @param obj the unit test object
	 */
	public static void start(Object obj) {
		System.out.println(">>>>>>>>>>>>>>>><<<<<<<<<<<<<<");
		System.out.println(">>>>>>>>>>>>>START<<<<<<<<<<<<");
	      try
	      {
	         throw new Exception("Need class/method name");
	      }
	      catch( Exception e )
	      {
	         System.out.println(e.getStackTrace()[1].getClassName() +  "." + e.getStackTrace()[1].getMethodName() + "()" );
	      }

	}
	public static String unicodeText() {
		String utf8 = "";
		try {
		File fileDir = new File("E:\\oxcart\\trunk\\pdi-util\\UT\\UTF8.txt");
		BufferedReader in = new BufferedReader(
		   new InputStreamReader(
                         new FileInputStream(fileDir), "UTF8"));	          
			
	        String str;
	        while ((str = in.readLine()) != null) {
	        	if(str != null) {
	        		utf8 = str;
	        	}
	        }
	        in.close();
		}catch(Exception e) {
			utf8 = "ERROR";
			e.printStackTrace();
		}
	        
	        return utf8;
	}
	/**
	 * Caller to finish any unit test
	 * @param obj the unit test object
	 */
	public static void stop(Object obj) {
		System.out.println(">>>>>>>>>>>>>STOP<<<<<<<<<<<<<");
	}
}
