package com.pdi.ut;
import junit.framework.TestCase;

import com.kf.palms.web.rest.security.ReportAPIHelper;
import com.kf.palms.web.rest.security.ReportDownloadResponse;

public class RptReqHelperUT extends TestCase {

	
	public RptReqHelperUT(String name)
	{
		super(name);
	}
	
	public void testReportRequestHelper(){
		int userId = 390748;
		int projectId = 7377;
		String reportType = "KFP_INDIVIDUAL";
//		String principal = "nwondra@kornferry.com";
//		String urlroot = "http://localhost:8080";
		String principal = "Ken.Beukelman@KornFerry.com";
		String urlroot = "http://localhost:28080";
		String secretKey = "b0d4b7a7-d3a6-47ac-b5f0-e461b5d6a028";
		String partnerKey = "f65b01da-7ed3-4338-b09d-c675913073bb";
		
		ReportAPIHelper helper = new ReportAPIHelper(secretKey, partnerKey, urlroot);
		
		ReportDownloadResponse rdr = helper.getReportByUserProjectType(userId, projectId, reportType, principal);
		
		System.out.println("Got Response of type: " + rdr.getContentType() + " with length: " + rdr.getContentLength());
		
		if (rdr.getContentType() == null){
			System.out.println("Error Message: " + rdr.getErrorMessage());
			System.out.println("Error Detail: " + rdr.getErrorDetail());
		}
		assertEquals(rdr.getContentType(), "application/pdf");
		
		assertTrue(rdr.getContentLength() > 0);
		
	}

}
