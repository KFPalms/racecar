package com.pdi.ut;

import com.pdi.scoring.Norm;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;


public class NormUT  extends TestCase
{
	public NormUT(String name)
	{
		super(name);
	}

	// Testing stanine calculations.... 
	public void testStanineCalcs()
		throws Exception
	{
		UnitTestUtils.start(this);
		
		int stanine = 0;
		stanine = Norm.calcStanineFromPercent(1);
		assertEquals(1,stanine);
		stanine = 0;
		stanine = Norm.calcStanineFromPercent(3);
		assertEquals(1,stanine);
		stanine = 0;
		stanine = Norm.calcStanineFromPercent(4);
		assertEquals(2,stanine);
		stanine = 0;
		stanine = Norm.calcStanineFromPercent(10);
		assertEquals(2,stanine);
		stanine = 0;
		stanine = Norm.calcStanineFromPercent(11);
		assertEquals(3,stanine);
		stanine = 0;
		stanine = Norm.calcStanineFromPercent(22);
		assertEquals(3,stanine);
		stanine = 0;
		stanine = Norm.calcStanineFromPercent(23);
		assertEquals(4,stanine);
		stanine = 0;
		stanine = Norm.calcStanineFromPercent(39);
		assertEquals(4,stanine);
		stanine = 0;
		stanine = Norm.calcStanineFromPercent(40);
		assertEquals(5,stanine);
		stanine = 0;
		stanine = Norm.calcStanineFromPercent(59);
		assertEquals(5,stanine);
		stanine = 0;
		stanine = Norm.calcStanineFromPercent(60);
		assertEquals(6,stanine);
		stanine = 0;
		stanine = Norm.calcStanineFromPercent(76);
		assertEquals(6,stanine);
		stanine = 0;
		stanine = Norm.calcStanineFromPercent(77);
		assertEquals(7,stanine);
		stanine = 0;
		stanine = Norm.calcStanineFromPercent(88);
		assertEquals(7,stanine);
		stanine = 0;
		stanine = Norm.calcStanineFromPercent(89);
		assertEquals(8,stanine);
		stanine = 0;
		stanine = Norm.calcStanineFromPercent(96);
		assertEquals(8,stanine);
		stanine = 0;
		stanine = Norm.calcStanineFromPercent(97);
		assertEquals(9,stanine);
		stanine = 0;
		stanine = Norm.calcStanineFromPercent(99);
		assertEquals(9,stanine);
		//System.out.println("Done");

		UnitTestUtils.stop(this);
	}


	// Testing PDI rating calculations.... 
	public void testIntPdiRating()
		throws Exception
	{
		UnitTestUtils.start(this);

		String rating = "";
		rating = Norm.calcPdiRating(1);
		assertEquals("1.0",rating);
		rating = "";
		rating = Norm.calcPdiRating(3);
		assertEquals("1.0",rating);
		rating = "";
		rating = Norm.calcPdiRating(4);
		assertEquals("1.5",rating);
		rating = "";
		rating = Norm.calcPdiRating(10);
		assertEquals("1.5",rating);
		rating = "";
		rating = Norm.calcPdiRating(11);
		assertEquals("2.0",rating);
		rating = "";
		rating = Norm.calcPdiRating(22);
		assertEquals("2.0",rating);
		rating = "";
		rating = Norm.calcPdiRating(23);
		assertEquals("2.5",rating);
		rating = "";
		rating = Norm.calcPdiRating(39);
		assertEquals("2.5",rating);
		rating = "";
		rating = Norm.calcPdiRating(40);
		assertEquals("3.0",rating);
		rating = "";
		rating = Norm.calcPdiRating(59);
		assertEquals("3.0",rating);
		rating = "";
		rating = Norm.calcPdiRating(60);
		assertEquals("3.5",rating);
		rating = "";
		rating = Norm.calcPdiRating(76);
		assertEquals("3.5",rating);
		rating = "";
		rating = Norm.calcPdiRating(77);
		assertEquals("4.0",rating);
		rating = "";
		rating = Norm.calcPdiRating(88);
		assertEquals("4.0",rating);
		rating = "";
		rating = Norm.calcPdiRating(89);
		assertEquals("4.5",rating);
		rating = "";
		rating = Norm.calcPdiRating(96);
		assertEquals("4.5",rating);
		rating = "";
		rating = Norm.calcPdiRating(97);
		assertEquals("5.0",rating);
		rating = "";
		rating = Norm.calcPdiRating(99);
		assertEquals("5.0",rating);
//		System.out.println("Done");

		UnitTestUtils.stop(this);
	}


	// Testing PDI rating calculations.... 
	// less exhaustive because the core testing was done above
	public void testDblCalcPdiRating()
		throws Exception
	{
		UnitTestUtils.start(this);

		String rating = "";
		rating = Norm.calcPdiRating(3.49d);
		assertEquals("1.0",rating);
		rating = Norm.calcPdiRating(3.5d);	// Rounds up to 4 which is the cut for 1.0 to 1.5
		assertEquals("1.5",rating);
		rating = "";
		rating = Norm.calcPdiRating(3.51d);
		assertEquals("1.5",rating);
		
		rating = Norm.calcPdiRating(22.2d);
		assertEquals("2.0",rating);
		rating = Norm.calcPdiRating(22.8d);	// Rounds up to 23 which is the cut for 2.0 to 2.5
		assertEquals("2.5",rating);
		rating = "";
		rating = Norm.calcPdiRating(24.51d);
		assertEquals("2.5",rating);
		
		rating = Norm.calcPdiRating(96.49d);
		assertEquals("4.5",rating);
		rating = Norm.calcPdiRating(96.5d);	// Rounds up to 97 which is the cut for 4.5 to 5.0
		assertEquals("5.0",rating);
		rating = "";
		rating = Norm.calcPdiRating(96.51d);
		assertEquals("5.0",rating);

		UnitTestUtils.stop(this);
	}
}
