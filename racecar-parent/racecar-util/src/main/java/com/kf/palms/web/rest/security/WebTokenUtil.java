package com.kf.palms.web.rest.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.security.Key;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.crypto.spec.SecretKeySpec;

import com.pdi.properties.PropertyLoader;

public class WebTokenUtil {
	
	private Jws<Claims> claims;
	
	public WebTokenUtil(String token){
		claims = Jwts.parser().setSigningKey(WebTokenUtil.getSigningKey()).parseClaimsJws(token);
	}
	
	public static String generateWebToken(String username, String displayName) {
		
		Calendar cal = new GregorianCalendar();
		cal.add(Calendar.MINUTE, 20);


		return Jwts.builder().setSubject(username)
				.setIssuer(PropertyLoader.getProperty("application", "SAML.issuer"))
				.claim("name", displayName)
				.claim("exp", cal.getTimeInMillis())
				.signWith(SignatureAlgorithm.HS512, WebTokenUtil.getSigningKey())
				.compact();
		
	}
	
	public Object getTokenClaimValue(String key){
		if (this.claims == null){
			return null;
		}
		return claims.getBody().get(key);
	}
	
	private static Key getSigningKey(){
		return new SecretKeySpec("a46e4e17-48d4-4958-a9c5-0a4aaf427f1d".getBytes(), SignatureAlgorithm.HS512.getJcaName());
	}
}
