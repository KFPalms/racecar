package com.kf.palms.web.rest.security;

import java.net.URI;
import java.util.Date;
import java.util.Properties;
import java.util.Random;

import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientRequest;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.filter.ClientFilter;

public class APIAuthorizationClientFilter extends ClientFilter {

	String secretKey;
	String partnerKey;
	String partnerKeyVersion;
	String serviceToken;
	String tokenType;
	
	public APIAuthorizationClientFilter(String secretKey, String partnerKey){
		this.secretKey = secretKey;
		this.partnerKey = partnerKey;
		partnerKeyVersion = "1.0";
		serviceToken = "Reports";
		tokenType = "PALMS";
	}
	
	@Override
	public ClientResponse handle(ClientRequest arg0)
			throws ClientHandlerException {
		// TODO Auto-generated method stub
		return null;
	}
	
	private ClientRequest authorizePalmsAPIRequest(ClientRequest request) {
		// TODO Auto-generated method stub
		URI uri = request.getURI();
		// log.debug("Got uri: Scheme: {}, host: {}, path: {}", uri.getScheme(),
		// uri.getHost(), uri.getPath());
		Random random = new Random();
		Date date = new Date();

		String nonce = String.valueOf(random.nextInt());
		String tstmp = String.valueOf(date.getTime());

		request.getHeaders().putSingle("PartnerKey", partnerKey);
		request.getHeaders().putSingle("Token", serviceToken);
		request.getHeaders().putSingle("Token_Type", tokenType);
		request.getHeaders().putSingle("Nonce", nonce);
		request.getHeaders().putSingle("TimeStamp", tstmp);
		request.getHeaders().putSingle("Version", partnerKeyVersion);
		
		String apiSignature = "";
		try {
			apiSignature = AwsSignature.generateSignature(partnerKey, secretKey, tokenType, serviceToken, uri,
					request.getMethod(), nonce, tstmp, partnerKeyVersion);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		request.getHeaders().putSingle("API_Signature", apiSignature);
		
		return request;

	}

}
