package com.kf.palms.web.rest.security;

//import java.io.BufferedReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.Date;
import java.util.Random;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.sun.jersey.api.client.UniformInterfaceException;

public class ReportAPIHelper {
	
	//Client client;
	String reportApiRootUrl;
	String secretKey;
	String partnerKey;
	String reportApiHost;
	
	public ReportAPIHelper(String secretKey, String partnerKey, String reportApiHost){
		this.secretKey = secretKey;
		this.partnerKey = partnerKey;
		this.reportApiHost = reportApiHost;

	}

	public ReportDownloadResponse getReportByUserProjectType(int userId, int projectId, String reportType, String requestor)  {

		if (!reportApiHost.startsWith("http")){
			reportApiHost = "http://" + reportApiHost;
		}
		
		this.reportApiRootUrl = reportApiHost + "/adminrest/jaxrs";
		
		String strurl = reportApiRootUrl + "/download/" + userId + "/" + projectId + "/" + reportType + "/" + requestor;
		System.out.println("URL: " + strurl);
		
		ReportDownloadResponse rdr = new ReportDownloadResponse();
		rdr.setSuccess(true);	// initialize optimistically
		
		HttpURLConnection conn = null;
		URL url = null;
		//BufferedReader reader = null;
		
		try {
			url = new URL(strurl);
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Content-Type", "application/octet-stream");
			
			this.authorizeConnection(conn);
			
			int statusCode = conn.getResponseCode();
			if (statusCode == 500)
			{
				InputStream content = conn.getErrorStream();
				BufferedReader br   = new BufferedReader (new InputStreamReader (content));
				String line;
				StringBuffer result = new StringBuffer();
				try {
					while ((line = br.readLine()) != null) {
						result.append(line);
					}
				} catch (IOException e) {
					throw new Exception("ReportAPIHelper.getReportByUserProjectType():  IOException on error stream read.  msg=" + e.getMessage());
				}
				
				DocumentBuilderFactory dbfac = null;
				DocumentBuilder docBuilder = null;
				Document doc = null;
				try {
					dbfac = DocumentBuilderFactory.newInstance();
					docBuilder = dbfac.newDocumentBuilder();
					doc = docBuilder.parse(new InputSource(new StringReader(result.toString())));
				} catch (ParserConfigurationException pce) {
					throw new Exception("ReportAPIHelper.getReportByUserProjectType(): Parser config error.  msg=" + pce.getMessage());
				} catch (SAXException se) {
					throw new Exception("ReportAPIHelper.getReportByUserProjectType(): SAX Exception.  msg=" + se.getMessage());
				}
				
				XPathFactory xPathfactory = XPathFactory.newInstance();
				XPath xpath = xPathfactory.newXPath();
				
				// Get the error info
				NodeList msgNodeList = (NodeList)xpath.compile("/error/errorMessage").evaluate(doc, XPathConstants.NODESET);
				String msg = msgNodeList.item(0).getTextContent();
				NodeList dtlNodeList = (NodeList)xpath.compile("/error/errorDetail").evaluate(doc, XPathConstants.NODESET);
				String dtl = dtlNodeList.item(0).getTextContent();
	
				// build the rdr
				rdr.setSuccess(false);
				rdr.setErrorMessage(msg);
				rdr.setErrorDetail(dtl);
			}
			else
			{
				rdr.setInStream(conn.getInputStream());
				rdr.setContentType(conn.getContentType());
				String disp = conn.getHeaderField("Content-Disposition");
				rdr.setFileName(disp.substring(disp.indexOf("filename=") + "filename=".length()));
				rdr.setContentLength(conn.getContentLengthLong());
			}
//		} catch (UniformInterfaceException e){
//			
//			rdr.setSuccess(false);
//			rdr.setErrorMessage("Report not found");
//			rdr.setErrorDetail(e.getMessage());
			
		} catch (Exception ex){
			//All other types of exceptions
			ex.printStackTrace();
			rdr.setSuccess(false);
			rdr.setErrorMessage("Failed to connect to server.");
			rdr.setErrorDetail(ex.getMessage());
		}
		
		return rdr;
	}
	
	private void authorizeConnection(HttpURLConnection conn){
		
		Random random = new Random();
		Date date = new Date();

		String nonce = String.valueOf(random.nextInt());
		String tstmp = String.valueOf(date.getTime());
		
		conn.addRequestProperty("PartnerKey", partnerKey);
		conn.addRequestProperty("Token", "Reports");
		conn.addRequestProperty("Token_Type", "PALMS");
		conn.addRequestProperty("Nonce", nonce);
		conn.addRequestProperty("TimeStamp", tstmp);
		conn.addRequestProperty("Version", "1.0");

		String apiSignature = "";
		try {
			URI uri = conn.getURL().toURI();
			apiSignature = AwsSignature.generateSignature(partnerKey, secretKey, "PALMS", "Reports", uri,
					conn.getRequestMethod(), nonce, tstmp, "1.0");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		conn.addRequestProperty("API_Signature", apiSignature);
		
	}
}
