package com.oneLogin;

import java.util.ArrayList;
import java.util.List;

public class AccountSettings {
	private List<String> certificates = new ArrayList<String>();
	private String idp_sso_target_url;
	

	public String getIdp_sso_target_url() {
		return idp_sso_target_url;
	}
	public void setIdpSsoTargetUrl(String idp_sso_target_url) {
		this.idp_sso_target_url = idp_sso_target_url;
	}
	public List<String> getCertificates() {
		return certificates;
	}
	public void setCertificates(List<String> certificates) {
		this.certificates = certificates;
	}
}
