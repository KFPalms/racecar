package com.oneLogin.saml;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMValidateContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.codec.binary.Base64;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.oneLogin.AccountSettings;

import java.lang.reflect.Method;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class Response {
	
	
	private Document xmlDoc;
	private AccountSettings accountSettings;
	private Certificate certificate;
	
	public String getUsername(){
		return this.getAttributeValue("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name");
	}
	
	public String getDisplayName(){
		return this.getAttributeValue("http://schemas.microsoft.com/identity/claims/displayname");
	}
	
	public String getAttributeValue(String attributeName){
		//System.out.println("In GetUsername() with xml doc: " + this.convertDomToString(xmlDoc));
		NodeList attributes = xmlDoc.getElementsByTagName("Attribute");
		System.out.println("Got " + attributes.getLength() + " attributes.");
		int index = 0;
		while (index < attributes.getLength()){
			Node node = attributes.item(index);
			Node name = node.getAttributes().getNamedItem("Name");
			 System.out.println("Name Node:  Name: {}, Value: {}"+ name.getNodeName()+" "+ name.getNodeValue());
			
			if (name.getNodeValue().equalsIgnoreCase(attributeName)){
				
				String username = node.getChildNodes().item(0).getTextContent();
				 System.out.println("Found Node, value: {}"+ username);
				return username;
			}
			
			index++;
		}
		return null;
	}
	//Use this to print un-encrypted xml response to stdout
	private String convertDomToString(Document doc){
		DOMSource domSource = new DOMSource(doc);
		StringWriter writer = new StringWriter();
		StreamResult result = new StreamResult(writer);
		TransformerFactory tf = TransformerFactory.newInstance();
		
		try {
			Transformer transformer = tf.newTransformer();
			transformer.transform(domSource, result);
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return writer.toString();
	}
	
	public Response(AccountSettings accountSettings) throws CertificateException {
		this.accountSettings = accountSettings;
		certificate = new Certificate();
		certificate.loadCertificate(this.accountSettings.getCertificates().get(0));
	}
	
	public void loadXml(String xml) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory fty = DocumentBuilderFactory.newInstance();
		fty.setNamespaceAware(true);
		DocumentBuilder builder = fty.newDocumentBuilder();
		ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());
		xmlDoc = builder.parse(bais);		
	}
	
	
	public void loadXmlFromBase64(String response) throws ParserConfigurationException, SAXException, IOException {
		Base64 base64 = new Base64();
		byte[] decodedB = null;
		 //System.out.println("String response: {} "+ response);
		decodedB = (byte[]) base64.decode(response);
		 //System.out.println("decoded bytes: {} "+ decodedB);		
		if (decodedB == null){
			 System.out.println("decodedB is NULL");
		}
		
		String decodedS = new String(decodedB);		
		// System.out.println("Decoded String: {} "+ decodedS);
		loadXml(decodedS);	
	}
	
        public boolean isValid() throws Exception {
            NodeList nodes = xmlDoc.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");

            if (nodes == null || nodes.getLength() == 0) {
                throw new Exception("Can't find signature in document.");
            }

            if (setIdAttributeExists()) {
                tagIdAttributes(xmlDoc);
            }
            
            Node cnode = xmlDoc.getElementsByTagName("X509Certificate").item(0);
    		
    		if (cnode != null ){
    			
    			if (this.verifyResponseCertificate(cnode.getTextContent())){
    				try{
    					certificate.loadCertificate(cnode.getTextContent());
    				}catch(Exception e){
    					System.out.println("Got Exception attempting to load X509Certificate from SAML Response.  Will attempt to use original certificate");
    					e.printStackTrace();
    				}
    			} else {
    				System.out.println("Returned Certificate did not match any configured certificate.  Validation Failed");
    				return false;
    			}
    		} else {
    			System.out.println("Could not locate X509Certificate in SAML Response");
    			//No certificate found in the response.  This case uses the first certificate from the properties file
    		}

            X509Certificate cert = certificate.getX509Cert();
            DOMValidateContext ctx = new DOMValidateContext(cert.getPublicKey(), nodes.item(0));
            XMLSignatureFactory sigF = XMLSignatureFactory.getInstance("DOM");
            XMLSignature xmlSignature = sigF.unmarshalXMLSignature(ctx);

            return xmlSignature.validate(ctx);
        }
    	private boolean verifyResponseCertificate(String X509Certificate){
    		for (String cert : accountSettings.getCertificates()){
    			if (cert.equals(X509Certificate)){
    				System.out.println("Found matching certificate in account settings");
    				return true;
    			}
    		}
    		return false;
    	}
	
	public String getNameId() throws Exception {
		NodeList nodes = xmlDoc.getElementsByTagNameNS("urn:oasis:names:tc:SAML:2.0:assertion", "NameID");		

		if(nodes.getLength()==0){
			throw new Exception("No name id found in document");
		}		

		return nodes.item(0).getTextContent();
	}
        
        private void tagIdAttributes(Document xmlDoc) {
            NodeList nodeList = xmlDoc.getElementsByTagName("*");
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    if (node.getAttributes().getNamedItem("ID") != null) {
                        ((Element) node).setIdAttribute("ID", true);
                    }
                }
            }
        }

        private boolean setIdAttributeExists() {
            for (Method method : Element.class.getDeclaredMethods()) {
                if (method.getName().equals("setIdAttribute")) {
                    return true;
                }
            }
            return false;
        }

		public Document getXmlDoc() {
			return xmlDoc;
		}

		public void setXmlDoc(Document xmlDoc) {
			this.xmlDoc = xmlDoc;
		}

        
}
