/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.ut;


import java.util.ArrayList;

import com.pdi.data.adapt.helpers.AdaptScoreDataHelper;
import com.pdi.data.checkbox.helpers.CheckboxScoreDataHelper;
import com.pdi.data.util.RequestStatus;
import com.pdi.data.v2.helpers.ExternalResultsDataHelper;
import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.properties.PropertyLoader;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.IndividualReport;

//// Stuff used in debugging
//import java.util.Iterator;
//import java.util.Map;
//import com.pdi.reporting.constants.ReportingConstants;
//import com.pdi.reporting.report.ReportData;

import junit.framework.TestCase;

public class ExtDataToV2UT  extends TestCase
{
	public ExtDataToV2UT(String name)
	{
		super(name);
	}

	public void testProperties()
	throws Exception
	{
		UnitTestUtils.start(this);
		
		//Does the adapt local property work?
		String adaptApplicationId = PropertyLoader.getProperty("com.pdi.data.adapt.application", "application.code");
		assertEquals(adaptApplicationId != null, true);
		assertEquals(adaptApplicationId.equals("PDI-DATA-ADAPT"), true);
		
		//Does the checkbox local property work?
		String applicationId = PropertyLoader.getProperty("com.pdi.data.checkbox.application", "application.code");
		assertEquals(applicationId != null, true);
		assertEquals(applicationId.equals("PDI-DATA-CHECKBOX"), true);
		
		//Does the v2 local property work?
		String v2ApplicationId = PropertyLoader.getProperty("com.pdi.data.v2.application", "application.code");
		assertEquals(v2ApplicationId != null, true);
		assertEquals(v2ApplicationId.equals("PDI-DATA-V2"), true);
		
		UnitTestUtils.stop(this);
	}

	
	public void testAdaptToV2()
		throws Exception
	{
//		if (true) {
//			return;
//		}
		
		UnitTestUtils.start(this);
		V2DatabaseUtils.setUnitTest(true);
		
		// Project - MB test Project 11/2/05, Person - Ben Skywalker
		long engmnt = 289;
		long pers= 1029;	
//		// Project - Test Freddie Mac Project, Person - Freddie Mac Testuser
//		long engmnt = 410;
//		long pers= 966;	
//		// Project - Test Bob's Project, Person - Bob1 Testuser  NOTE-Very little answer data present
//		long engmnt = 424;
//		long pers= 1010;	
//		// Project - Copyright Project, Person - Copyright Testuser
//		long engmnt = 451;
//		long pers= 1090;	
		
		IndividualReport ir = AdaptScoreDataHelper.pullAdaptData(engmnt, pers, false); 

		//// Dump the results
		//System.out.println("-----------------------------------------------------------------------");
		//System.out.println("                      Results Dump from UT");
		//System.out.println("-----------------------------------------------------------------------");
		//if (ir == null)	// Indicates invalid person/engagement combo
		//{
		//	System.out.println("ERROR:  No IndividualReport object returned.");
		//}
		//if (ir.getReportData().isEmpty())
		//{
		//	System.out.println("No report data found");
		//}
		//for (Iterator<String> itr = ir.getReportData().keySet().iterator(); itr.hasNext(); )
		//{
		//	String key = itr.next();
		//	ReportData rd = ir.getReportData().get(key);
		//	System.out.println("Report type - " + key + ":");
		//
		////if (! key.equals("CHQ"))
		////{
		////	// Skip all but above named module(s) (DEBUG)
		////	continue;
		////}
		//
		//// raw data - input scores
		//System.out.println("  Raw data:");
		//for (Iterator<Map.Entry<String, String>> it2 = rd.getRawData().entrySet().iterator(); it2.hasNext(); )
		//{
		//	Map.Entry<String, String> me = it2.next();
		//	System.out.println("    " + me.getKey() + "=" + me.getValue());
		//}
		//
		//// score data - raw scale scores
		//System.out.println("  Score data:");
		//for (Iterator<Map.Entry<String, String>> it3 = rd.getScoreData().entrySet().iterator(); it3.hasNext(); )
		//{
		//	Map.Entry<String, String> me2 = it3.next();
		//	System.out.println("    " + me2.getKey() + "=" + me2.getValue());
		//	}
		//}
		
		String jobId = "IYKNYCNU";	// AbyD Testing Components Template (for testing)
		String candId = "FAKECAND";	// Some arbitrary candidate id (for testing)
		ArrayList<String> mods = new ArrayList<String>();
		mods.add(ReportingConstants.RC_GPI);
		mods.add(ReportingConstants.RC_LEI);
		mods.add(ReportingConstants.RC_FIN_EX);
		mods.add(ReportingConstants.RC_CHQ);

		//RequestStatus rs = new ExternalResultsDataHelper().saveAdaptData(jobId, candId, ir);
		ArrayList<String> list = new ExternalResultsDataHelper().saveAdaptData(jobId, candId, mods, ir);

//		//System.out.println("ADAPT V2 write Result---" + rs.toString());
//		assertEquals("eCode=" + rs.getExceptionCode() + ", eMsg=" + rs.getExceptionMessage() + ", status:",
//				RequestStatus.RS_OK, rs.getStatusCode());
		if (list.size() < 1)
		{
			System.out.println("Nothing saved from ADAPT");
		}
		else
		{
			System.out.println("The following modules were saved:");
			for (String str : list)
			{
				System.out.println("  " + str);
			}
		}

		UnitTestUtils.stop(this);
	}


	public void testCheckbox()
		throws Exception
	{
//		if (true) {
//			return;
//		}

		UnitTestUtils.start(this);
		V2DatabaseUtils.setUnitTest(true);
	
		// Initial Checkbox test
		long rid= 89671;	
	
		IndividualReport ir = CheckboxScoreDataHelper.fetchCheckboxData(rid); 
	
		//// Dump the results
		//System.out.println("-----------------------------------------------------------------------");
		//System.out.println("                      Results Dump from UT");
		//System.out.println("-----------------------------------------------------------------------");
		//if (ir == null)	// Shouldn't happen
		//{
		//	System.out.println("ERROR:  No IndividualReport object returned.");
		//}
		//if (ir.getReportData().isEmpty())
		//{
		//	System.out.println("No report data found");
		//}

		ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY);
		
		// Use tyhe one above for normal and the one below for debug
		//ReportData rd = ir.getReportData().get(ReportingConstants.RC_CAREER_SURVEY);
		// raw data - input scores
		//System.out.println("  Raw data:");
		//for (Iterator<Map.Entry<String, String>> itr = rd.getRawData().entrySet().iterator(); itr.hasNext(); )
		//{
		//	Map.Entry<String, String> me = itr.next();
		//	System.out.println("    " + me.getKey() + "=" + me.getValue());
		//}
		//
		//// score data - raw scale scores
		//System.out.println("  Score data:");
		//for (Iterator<Map.Entry<String, String>> itr = rd.getScoreData().entrySet().iterator(); itr.hasNext(); )
		//{
		//	Map.Entry<String, String> me = itr.next();
		//	System.out.println("    " + me.getKey() + "=" + me.getValue());
		//}
		//
		////This is the v2 job/candidate id's that will be used to
		//// return the participant to their v2 experience
		//System.out.println("  Display data:"); 
		//for (Iterator<Map.Entry<String, String>> itr = rd.getDisplayData().entrySet().iterator(); itr.hasNext(); )
		//{
		//	Map.Entry<String, String> me = itr.next();
		//	System.out.println("    " + me.getKey() + "=" + me.getValue());
		//}
		
		
		String jobId = "IYKNYCNU";	// AbyD Testing Components Template (for testing)
		String candId = "FAKECAND";	// Some arbitrary candidate id (for testing)

		RequestStatus rs = new ExternalResultsDataHelper().saveCheckboxData(jobId, candId, ir);
		
		//System.out.println("Checkbox V2 write Result---" + rs.toString());
		assertEquals("eCode=" + rs.getExceptionCode() + ", eMsg=" + rs.getExceptionMessage() + ", status:",
				RequestStatus.RS_OK, rs.getStatusCode());

		UnitTestUtils.stop(this);
	}

}
