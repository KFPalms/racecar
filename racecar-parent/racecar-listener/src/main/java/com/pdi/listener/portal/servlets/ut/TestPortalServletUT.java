/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.listener.portal.servlets.ut;

//import com.pdi.ut.UnitTestUtils;
//
//import java.io.StringReader;
//
//import javax.xml.parsers.DocumentBuilder;
//import javax.xml.parsers.DocumentBuilderFactory;
//
//import org.w3c.dom.Document;
//import org.w3c.dom.Node;
//import org.w3c.dom.NodeList;
//import org.xml.sax.InputSource;
//
//import groovy.lang.GroovyClassLoader;
//import groovy.lang.GroovyObject;
//
//import com.pdi.data.abyd.util.AbyDDatabaseUtils;
//import com.pdi.data.v2.dto.SessionUserDTO;
//import com.pdi.data.v2.util.V2DatabaseUtils;
//import com.pdi.data.v2.util.V2WebserviceUtils;
//import com.pdi.xml.XMLUtils;
//import com.pdi.properties.PropertyLoader;
//import com.pdi.reporting.report.GroupReport;
//import com.pdi.reporting.report.GroupReport;
//import com.pdi.reporting.report.IndividualReport;
//import com.pdi.reporting.constants.ReportingConstants;
//import com.pdi.reporting.request.ReportingRequest;
//import com.pdi.reporting.response.ReportingResponse;
//import com.pdi.reporting.utils.ReportingUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.pdi.data.dto.Participant;
import com.pdi.data.dto.Project;
import com.pdi.data.dto.ProjectParticipantInstrument;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.IProjectParticipantInstrumentHelper;
import com.pdi.data.helpers.interfaces.IReportHelper;
import com.pdi.data.nhn.util.NhnDatabaseUtils;
import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.response.ReportingResponse;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

public class TestPortalServletUT extends TestCase 
{
	
	//
	// Constructor
	//
	public TestPortalServletUT(String name)
	{
		super(name);
	}
	
//	public void testAlive()
//	throws Exception
//	{
//		UnitTestUtils.start(this);
//		
//		System.out.println("Hello World!");
//		
//		UnitTestUtils.stop(this);
//	}


//	/*
//	 * testGetExtractJava
//	 * 
//	 * Gather data for extract
//	 * Needs to be put somewhere, but start gather here for testing
//	 */
//	public void testGetExtractJava()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//		V2DatabaseUtils.setUnitTest(true);
//
//		// input for this will be XML with a "Participants/Participant" node (like in
//		// TLT_GROUP_REPORT_DETAIL.groovy).  All of the participants will be from the
//		// same project.  For this phase, use a single project and a list of 1 - n
//		// participant ids.
//		
//		// These are consultant projects; no score data in this project but in the cand exp job
//		ArrayList<String> participants = new ArrayList<String>();
//
//		//String projectId = "FCPHHIOV";		// MLL - 2+ cand - no results
//		//participants.add("FCBBBTXL");	// WG-E & CS only
//		//participants.add("IYIVROGG");
//		//participants.add("LKADUBHG");
//		
//		//String projectId = "DVZXYXAD";		// MLL - 2 cand
//		//participants.add("KAFPKCDS");
//		//participants.add("IWTHGKUE");
//		
//		//String projectId = "IWYPLSPM";		// MLL - 6 cand
//		//participants.add("FERAKUZK");
//		//participants.add("LHWAQZSJ");
//		//participants.add("KAXAJQHF");
//		
//		//String projectId = "EZTOEDSO";		// BUL - 4+ cand
//		//participants.add("LLCKTTTQ");
//		//participants.add("DTKMWSAM");
//		//participants.add("FFBISPOX");
//		//participants.add("LJAAPSBG");
//		
//		//String projectId = "HPRYNCDD";		// BUL - 3+ cand
//		//participants.add("HQRMTCQP");
//		//participants.add("DTSNYJHO");
//		//participants.add("FGDJXOOY");
//
//		//String projectId = "LKLVXNGM";		// BUL - 6 cand
//		//participants.add("COOJJXDC");
//		//participants.add("FANBRCBL");
//		//participants.add("FDWOWSBR");
//		//participants.add("FFPSBIRB");
//		//participants.add("FGKFQLQI");
//		//participants.add("IUMLEPCZ");
//
//		//String projectId = "HMYHNGZK";		// SEA - 2 cand
//		//participants.add("DTWCBSQI");
//		//participants.add("FBBRATOE");
//
//		String projectId = "DTQCIMGR";		// SEA - Many cand, many 0
//		participants.add("BHEWJSIR");
//		participants.add("BLYRAAGV");
//		participants.add("HQTTTRRQ");
//		participants.add("LFTRXIAT");
//		participants.add("LLKVPBBQ");
//
//		ExtractTest et = new ExtractTest();
//		String str = et.getExtract(projectId, participants);
//		
//		System.out.println(str);
//		
//		
//		UnitTestUtils.stop(this);
//	}
	

	/*
	 * testGetExtractGroovy
	 * 
	 * Tests logic from both the portal and reporting sides
	 * 
	 * This is really old and may not work correctly any more
	 * 
	 */
//	@SuppressWarnings("unchecked")
//	public void testGetExtractGroovy()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//		V2DatabaseUtils.setUnitTest(true);
//
//		//System.out.println("Start testGetReport()");
//		// Create an XML
//		// Use dev.  Everything gets hosed when you try to do a
//		//     UNT web service call to something other than dev.
//		String xml = new String();
//
//		xml  = "<Reports>";
//		xml += "  <ReportRequest code=\"" + ReportingConstants.REPORT_CODE_ABYD_EXTRACT + "\">";
//		xml += "    <Participants>";
//		//// --> Initial testing
//		xml += "      <Participant participantId=\"KAFPKCDS\" projectId=\"DVZXYXAD\"/>";
//		xml += "      <Participant participantId=\"IWTHGKUE\" projectId=\"DVZXYXAD\"/>";
//
//		//// --> MLL - 2+ cand - no results (mlltst1)
//		//xml += "      <Participant participantId=\"FCBBBTXL\" projectId=\"FCPHHIOV\"/>";// WG & CS
//		//xml += "      <Participant participantId=\"IYIVROGG\" projectId=\"FCPHHIOV\"/>";
//		//xml += "      <Participant participantId=\"LKADUBHG\" projectId=\"FCPHHIOV\"/>";
//		//// --> MLL - 2 cand (mlltst2)		
//		//xml += "      <Participant participantId=\"KAFPKCDS\" projectId=\"DVZXYXAD\"/>";
//		//xml += "      <Participant participantId=\"IWTHGKUE\" projectId=\"DVZXYXAD\"/>";
//		// --> MLL - 3+ cand
//		//xml += "      <Participant participantId=\"FERAKUZK\" projectId=\"IWYPLSPM\"/>";
//		//xml += "      <Participant participantId=\"LHWAQZSJ\" projectId=\"IWYPLSPM\"/>";
//		//xml += "      <Participant participantId=\"KAXAJQHF\" projectId=\"IWYPLSPM\"/>";
//		
//		//// --> BUL - 4+ cand
//		//xml += "      <Participant participantId=\"LLCKTTTQ\" projectId=\"EZTOEDSO\"/>";
//		//xml += "      <Participant participantId=\"DTKMWSAM\" projectId=\"EZTOEDSO\"/>";
//		//xml += "      <Participant participantId=\"FFBISPOX\" projectId=\"EZTOEDSO\"/>";
//		//xml += "      <Participant participantId=\"LJAAPSBG\" projectId=\"EZTOEDSO\"/>";
//		//// --> BUL - 3+ cand (bultst)
//		//xml += "      <Participant participantId=\"HQRMTCQP\" projectId=\"HPRYNCDD\"/>";
//		//xml += "      <Participant participantId=\"DTSNYJHO\" projectId=\"HPRYNCDD\"/>";
//		//xml += "      <Participant participantId=\"FGDJXOOY\" projectId=\"HPRYNCDD\"/>";
//		////-->BUL - 6 cand
//		//xml += "      <Participant participantId=\"COOJJXDC\" projectId=\"LKLVXNGM\"/>";
//		//xml += "      <Participant participantId=\"FANBRCBL\" projectId=\"LKLVXNGM\"/>";
//		//xml += "      <Participant participantId=\"FDWOWSBR\" projectId=\"LKLVXNGM\"/>";
//		//xml += "      <Participant participantId=\"FFPSBIRB\" projectId=\"LKLVXNGM\"/>";
//		//xml += "      <Participant participantId=\"FGKFQLQI\" projectId=\"LKLVXNGM\"/>";
//		//xml += "      <Participant participantId=\"IUMLEPCZ\" projectId=\"LKLVXNGM\"/>";
//		
//		//// --> SEA model, 2 candidates (seatst)
//		//xml += "      <Participant participantId=\"DTWCBSQI\" projectId=\"HMYHNGZK\"/>";
//		//xml += "      <Participant participantId=\"FBBRATOE\" projectId=\"HMYHNGZK\"/>";
//		//// --> SEA model, Many candidates, many 0
//		//xml += "      <Participant participantId=\"BHEWJSIR\" projectId=\"DTQCIMGR\"/>";
//		//xml += "      <Participant participantId=\"BLYRAAGV\" projectId=\"DTQCIMGR\"/>";
//		//xml += "      <Participant participantId=\"HQTTTRRQ\" projectId=\"DTQCIMGR\"/>";
//		//xml += "      <Participant participantId=\"LFTRXIAT\" projectId=\"DTQCIMGR\"/>";
//		//xml += "      <Participant participantId=\"LLKVPBBQ\" projectId=\"DTQCIMGR\"/>";
//
//		xml += "    </Participants>";
//		xml += "  </ReportRequest>";
//		xml += "</Reports>";
//		
//		// Now invoke the first groovy script (the one in portal)
//		//System.out.println("Create Document from:");
//		//System.out.println(xml);
//		DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
//		DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
//		Document doc = docBuilder.parse(new InputSource(new StringReader(xml)));
//		NodeList nodes = XMLUtils.getElements(doc, "//ReportRequest");
//		//ReportingRequest reportRequest = new ReportingRequest();	
//		try
//		{
//			Node reportNode = nodes.item(0);	// get only the one item to test
//			//System.out.println("  i=" + i + ", class=" + reportNode.getClass().getName() + ", reportNode=" + reportNode.toString());
//			//System.out.println("  class=" + reportNode.getClass().getName() + ", reportNode=" + reportNode.toString());
//			String username = PropertyLoader.getProperty("com.pdi.listener.portal.application", "v2admin.username");
//			String password = PropertyLoader.getProperty("com.pdi.listener.portal.application", "v2admin.password");
//			V2WebserviceUtils v2web = new V2WebserviceUtils();
//			SessionUserDTO sessionUser = v2web.registerUser(username, password);
//			String reportCode =reportNode.getAttributes().getNamedItem("code").getTextContent();
//			//System.out.println("Report Code=" + reportCode);
//			
//			ReportingRequest reportRequest = new ReportingRequest();	
//        	String groovyPath = "/com/pdi/listener/portal/groovy/"+reportCode+".groovy";
//        	//System.out.println("GroovyPath=" + groovyPath); 
//        	String groovy = ReportingUtils.pathToString(groovyPath);
//        	//System.out.println("Groovy script (as a string)--" + groovy); 
//        	GroovyClassLoader gcl = new GroovyClassLoader();
//        	Class groovyClass = gcl.parseClass(groovy);
//        	GroovyObject groovyObject = (GroovyObject) groovyClass.newInstance();
//        	groovyObject.setProperty("sessionUser", sessionUser);
//        	groovyObject.setProperty("reportRequest", reportRequest);
//        	groovyObject.setProperty("reportNode", reportNode);
//        	Object[] args = {};
//        	//System.out.println("...Start generate()...");
//        	groovyObject.invokeMethod("generate", args);
//		
//			//// Check out the stuff returned in the reporting request
//			//GroupReport gr = (GroupReport) reportRequest.getReports().get(0);
//			//for (IndividualReport ir : gr.getIndividualReports())
//			//{
//			//	System.out.println(ir.displayDataToString());
//			//}
//        	
//        	// Now do the second part
//			ReportingResponse reportResponse = reportRequest.generateReports();
//			
//			//// Check out the stuff returned in the reporting request
//			//GroupReport gr = (GroupReport) reportRequest.getReports().get(0);
//			//for (IndividualReport ir : gr.getIndividualReports())
//			//{
//			//	System.out.println(ir.displayDataToString());
//			//}
//        	
//			// This assumes an extract -- html string
//        	System.out.println("Output=\n" + reportResponse.getReports().get(0).getData().toString());
//		}
//		catch (Exception e)
//		{
//			throw e;
//		}
//		UnitTestUtils.stop(this);
//	}

	/*
	 * testReportGen
	 * 
	 * Parrots logic from the portal - doesn't actually invoke the portal
	 * 
	 */
	public void testReportGen()
		throws Exception
	{
		UnitTestUtils.start(this);
		NhnDatabaseUtils.setUnitTest(true);
		V2DatabaseUtils.setUnitTest(true);
		
		// Test for single participant
		SessionUser su = null;
		// dev
		//String participantId = "364005";	// Old
		//String projectId = "60";
		// No project type specified for above participant... It will probably error out
		// cert
		//String participantId = "364875";
		//String projectId = "155";
		//String projectType = Project.TLT;
		//String langCode = "en";
		//String reportCode = "TLT_INDIVIDUAL_SUMMARY_REPORT";
		String participantId = "370363";
		String projectId = "400";
		//String projectType = Project.TLT;
		String langCode = "en";
		String reportCode = "TLT_INDIVIDUAL_SUMMARY_REPORT";
		
		//String langCode = "ja";
		//String reportCode = "TLT_GROUP_DETAIL_REPORT";
		HashMap<String, String> overrides = new HashMap<String, String>();

		// Get instrument data (scores)
		IProjectParticipantInstrumentHelper ppih = HelperDelegate.getProjectParticipantInstrumentHelper("com.pdi.data.v2.helpers.delegated");
		//Assumes that this is for reports... no Manual Entry/Test Data usage here
		ArrayList<ProjectParticipantInstrument> ppis = ppih.fromLastParticipantId(su, IProjectParticipantInstrumentHelper.MODE_FULL_COMPLETED, participantId, null);
		
		// Get participant data
		Participant participant = HelperDelegate.getParticipantHelper("com.pdi.data.nhn.helpers.delegated").fromId(su, participantId);
		//Participant participant = HelperDelegate.getParticipantHelper("com.pdi.data.v2.helpers.delegated").fromId(su, participantId);
//		// Make a fake participant
//		Participant participant = new Participant(participantId);
//		participant.setFirstName("Testing");
//		participant.setLastName("TestPart");
//		participant.setEmail("ttestpart@bogus.com");
//		// no metadata for now

		// Get project data
		Project project = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated").fromId(su, projectId);
//		//Project project = HelperDelegate.getProjectHelper("com.pdi.data.v2.helpers.delegated").fromId(su, projectId);
//		// Make a fake project
//		Project project = new Project(projectId);
//		project.setName("Test Project");
//		Client client = new Client("99999");
//		client.setName("Bogus, Inc.");
//		project.setClient(client);
//		project.setProjectTypeName(projectType);
//		HashMap<String, String> md = new HashMap<String, String>();
//		md.put("transitionLevel", "MLL");
//		project.setMetadata(md);
		
		for(ProjectParticipantInstrument ppi : ppis)
		{
			ppi.setParticipant(participant);
			
			ppi.setProject(project);
		}
		
		IReportHelper irh = HelperDelegate.getReportHelper();
		String combinationReport = "NO";
		irh.addReport(su, reportCode, langCode, ppis, overrides, combinationReport, projectId );
		// does this for a single report
		ReportingResponse reportResponse = irh.generate(su);
		
		IndividualReport ir = (IndividualReport) reportResponse.getReports().get(0);
		for (Iterator<Map.Entry<String, String>> itr=ir.getScriptedData().entrySet().iterator(); itr.hasNext();  )
		//for (Iterator<Map.Entry<String, String>> itr=ir.getLabelData().entrySet().iterator(); itr.hasNext();  )
		{
			Map.Entry<String, String> ent = itr.next();
			System.out.println(ent.getKey() + " = " + ent.getValue());
		}
	
	
	
		UnitTestUtils.stop(this);
	}

	
	/*
	 * testXPath
	 * 
	 * Tests some XPath stuff for eventual use in the Portal servlet
	 * 
	 * This is really old and may not work correctly any more
	 * 
	 */
//	public void testXPath()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//
//		// Create an XML
//		String xml = new String();
//		xml += "<Reports>" +
//				// 1st request
//			   "  <ReportRequest code=\"TLT_INDIVIDUAL_REPORT\" langCode=\"fr\">" +
//			   "    <Overrides>" + 
//			   "      <Override key=\"TARGET_LEVEL\" value=\"MLL\"/>" +
//			   "    </Overrides>" +
//			   "    <Participants>" +
//			   "      <Participant participantId=\"Part 1\" projectId=\"Proj 1\"/>" +
//			   "    </Participants>" +
//			   "  </ReportRequest>" +
//			   		// 2nd request
//			   "  <ReportRequest code=\"TLT_INDIVIDUAL_REPORT\">" +
//			   "    <Overrides>" + 
//			   "      <Override key=\"TARGET_LEVEL\" value=\"FLL\"/>" +
//			   "    </Overrides>" +
//			   "    <Participants>" +
//			   "      <Participant participantId=\"Part 2\" projectId=\"Proj 2\"/>" +
//			   "    </Participants>" +
//			   "  </ReportRequest>" +
//			   		// 3rd request
//			   "  <ReportRequest code=\"TLT_INDIVIDUAL_REPORT\" langCode="en">" +
//			   "    <Participants>" +
//			   "      <Participant participantId=\"Part 3\" projectId=\"Proj 3\" />" +
//			   "    </Participants>" +
//			   "  </ReportRequest>" +
//			   		// 4th request
//			   "  <ReportRequest code=\"TLT_GROUP_REPORT\">" +
//			   "    <Participants>" +
//			   "      <Participant participantId=\"Part 4a\" projectId=\"Proj 4\" />" +
//			   "      <Participant participantId=\"Part 4b\" projectId=\"Proj 4\" />" +
//			   "      <Participant participantId=\"Part 4c\" projectId=\"Proj 4\" />" +
//			   "      <Participant participantId=\"Part 4d\" projectId=\"Proj 4\" />" +
//			   "      <Participant participantId=\"Part 4e\" projectId=\"Proj 4\" />" +
//			   "    </Participants>" +
//			   "  </ReportRequest>" +
//			   "</Reports>";
//
//		DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
//		DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
//		Document doc = docBuilder.parse(new InputSource(new StringReader(xml)));
//		NodeList nodes = XMLUtils.getElements(doc, "//ReportRequest");
////		try
////		{
//		for(int i = 0; i < nodes.getLength(); i++)
//		{
//			Node reportNode = nodes.item(i);	
//			//System.out.println("  i=" + i + ", class=" + reportNode.getClass().getName() + ", reportNode=" + reportNode.toString());
//			//System.out.println("  class=" + reportNode.getClass().getName() + ", reportNode=" + reportNode.toString());
//			String reportCode =reportNode.getAttributes().getNamedItem("code").getTextContent();
//			System.out.println("Report Code=" + reportCode);
//			String lang = "en";		// Set a default
//			if (reportNode.getAttributes().getNamedItem("langCode") != null)
//				lang = reportNode.getAttributes().getNamedItem("langCode").getTextContent();
//			System.out.println("Language Code=" + lang);
//			
//			
//			//NodeList ovrs = XMLUtils.getElements(reportNode, "//ReportRequest[@code='"+reportCode+"']/Overrides/Override");
//			NodeList ovrs = XMLUtils.getElements(reportNode, "Overrides/Override");
//			if(ovrs != null)
//			{
//				System.out.println("  Overides:");
//				for(int k = 0; k < ovrs.getLength(); k++) {
//					Node ovr = ovrs.item(k);
//					System.out.println("    " + ovr.getAttributes().getNamedItem("key").getTextContent() +
//							" = " + ovr.getAttributes().getNamedItem("value").getTextContent());
//				}
//			}
//			
//			// Now do participants
//			//NodeList participants = XMLUtils.getElements(node, "//ReportRequest[@code='"+reportCode+"']/Participants/Participant");
//			NodeList participants = XMLUtils.getElements(reportNode, "Participants/Participant");
//			if(participants.getLength() > 0)
//			{
//				System.out.println("  Participants:");
//				for(int k = 0; k < participants.getLength(); k++)
//				{
//					Node participantNode = participants.item(k);
//					String participantId = participantNode.getAttributes().getNamedItem("participantId").getTextContent();
//					String projectId = participantNode.getAttributes().getNamedItem("participantId").getTextContent();
//					System.out.println("     Part=" + participantId + ", proj=" + projectId);
//				}
//			}
//		}	// End of for loop
//			
//
//		UnitTestUtils.stop(this);
//	}


	/*
	 * testScoring
	 * 
	 * Parrots logic from the portal - doesn't actually invoke the portal
	 * 
	 */
//	public void testScoring()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		NhnDatabaseUtils.setUnitTest(true);
//		V2DatabaseUtils.setUnitTest(true);
//
//		// This code was copied from the portal and is modified as little as possible
//		// so as to simulate (and test) the logic there
//		SessionUser session = new SessionUser();
//		
//		// Cert data
//		String instrumentId = "finex";
//		String participantId = "370372";
//
//		IProjectParticipantInstrumentHelper ipphv2 = HelperDelegate.getProjectParticipantInstrumentHelper(PropertyLoader.getProperty("com.pdi.data.application","datasource.v2"));
//		IProjectParticipantInstrumentHelper ipphnhn = HelperDelegate.getProjectParticipantInstrumentHelper(PropertyLoader.getProperty("com.pdi.data.application","datasource.nhn"));
//
//		IInstrumentHelper inv2 = HelperDelegate.getInstrumentHelper(PropertyLoader.getProperty("com.pdi.data.application","datasource.v2"));
//		Instrument i = inv2.fromId(session, instrumentId);
//		IScoringHelper isc = HelperDelegate.getScoringHelper();
//		ProjectParticipantInstrument ppi = ipphnhn.fromLastParticipantIdInstrumentId(session, participantId, instrumentId);
//		if(ppi != null)
//		{
//			ScoreGroup sg = isc.score(i, ppi.getResponseGroup());
//
//			ppi.setScoreGroup(sg);
//			ipphv2.addUpdate(session, IProjectParticipantInstrumentHelper.MODE_SCORE, ppi);
//		} else {
//			System.out.println("Null data, cannot run");
//		}
//	
//		UnitTestUtils.stop(this);
//	}

}
