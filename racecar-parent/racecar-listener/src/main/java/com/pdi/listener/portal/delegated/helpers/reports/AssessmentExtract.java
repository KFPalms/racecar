/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.listener.portal.delegated.helpers.reports;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import com.pdi.data.dto.NormScore;
import com.pdi.data.dto.ProjectParticipantInstrument;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;
import com.pdi.reporting.report.helpers.AbyDExtractFormatHelper;
import com.pdi.string.StringUtils;

/*
 * AssessmentExtract - class to hold methods relevant to the Assessment extract
 * 
 * NOTE:  EVEN THOUGH LOCATED UNDER THE DELEGATED FOLDER, THERE ARE NO METHODS IN THIS
 *        CLASS THAT ARE DEFINED IN THE DATA INTERFACE.
 *        
 * This class is located here because the folder structure (delegated/helpers) is
 * inverted from normal practice elsewhere in this project stack (helpers/delegated).
 * Putting this helper class here avoided confusion with having two folders in the
 * portal structure that had the name "helper".
 * 
 * Yes, this is not a report but an extract.  Still, it is located in the report folder
 */
public class AssessmentExtract
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	
	//
	// Constructors.
	//
	public AssessmentExtract()
	{
		// No logic here at the present time
	}

	//
	// Instance methods.
	//


	/**
	 * doAssessmentExtractRpt is a method that
	 * handles data for the RAVEN'S B, WATSON II, 
	 * GPI, RDI PERCENTILE, AND LEI, and puts the
	 * individual report PDI Ratings into an Xcel
	 * spreadsheet.
	 * 
	 * 
	 * @param ret
	 * @param epis
	 * @param overrides
	 * @param sessionUser
	 * @param tg
	 */
	public IndividualReport getAssessmentExtractData(IndividualReport inp,  
										  ArrayList<ProjectParticipantInstrument> epis,
										  String langCode)
	{
		//System.out.println("doAssessmentExtractRpt...");
		IndividualReport ret = inp;

		try
		{
			int counter = 0;
			java.util.Date instDate;
			java.util.Date latestInstDate = new Date();

			// set latest inst date for back a ways...
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");			
			Date today = df.parse("1111-11-11");
			latestInstDate =  today;
			//System.out.println("initial latest date: " + latestInstDate.toString());
			
			String pptId = null;
			String projId = null;

			for(ProjectParticipantInstrument epi : epis)
			{
				// They want the latest Instrument Taken date for the Assessment Date
				df = new SimpleDateFormat("yyyy-MM-dd");			
				today = df.parse(epi.getInstrumentTakenDate().substring(0, 10));
				//System.out.println("instrument Taken date: " + epi.getInstrumentTakenDate().substring(0, 10) +  ",    " + today.toString());

				instDate = today;
				if( instDate.after(latestInstDate) )
				{
					latestInstDate = instDate;
					//System.out.println("lastestInstDate = " + latestInstDate.toString());
				}
    				
				ReportData rd = new ReportData();
				boolean validInstrument = false;

				if(counter == 0)
				{
					/*
					 * ADD BASIC PARTICIPANT DATA TO REPORT  -- only need to do once... 
					 */
					ret.addDisplayData("FIRST_NAME", epi.getParticipant().getFirstName());
					ret.addDisplayData("LAST_NAME", epi.getParticipant().getLastName());
					ret.getDisplayData().put("PARTICIPANT_NAME", epi.getParticipant().getFirstName() + " " + epi.getParticipant().getLastName());
					// ORGANIZATION & PROJECT_NAME not escaped... data will go to POI not the report generator
					ret.addDisplayData("ORGANIZATION", epi.getProject().getClient().getName());
					ret.addDisplayData("PROJECT_NAME", epi.getProject().getName());	
					//ret.getDisplayData().put("ADMIN_DATE", epi.getInstrumentTakenDate().substring(0, 10));	  		
					//ret.getDisplayData().put("TODAY_DATE", new SimpleDateFormat("ddMMMyyyy").format(new java.util.Date()));

					// get data for later PDA processing
					pptId = epi.getParticipant().getId();
					projId = epi.getProject().getId();
				}

				// As we spin through the instruments
				// for each instrument, will need to get the norms, 
				// and pdi-ratings.

				/*
				 * Step 1 -  IS THE INSTRUMENT COMPLETE???
				 */
				if(epi.getInstrument().getCode().equals(ReportingConstants.IC_RAVENS))
				{
					// Do Raven's-specific stuff

					//instrument complete??
					if(epi.getScoreGroup().getScores().isEmpty())
					{
						ret.getScriptedData().put("STATUS", "NOT_FINISHED");
						ret.getScriptedData().put("ERROR", "Raven's B has not been completed, unable to generate data.");
					}
					else
					{
						ret.getScriptedData().put("STATUS", "");
						ret.getScriptedData().put("ERROR", "");
						validInstrument = true;
					}
				}	// End of Raven's check

				if(epi.getInstrument().getCode().equals(ReportingConstants.IC_GPI_SHORT) ||
				   epi.getInstrument().getCode().equals(ReportingConstants.IC_GPI))
				{
					// Do GPI-specific stuff

					//instrument complete??
					if(epi.getScoreGroup().getScores().isEmpty())
					{
						ret.getScriptedData().put("STATUS", "NOT_FINISHED");
						ret.getScriptedData().put("ERROR", "GPI has not been completed, unable to generate data.");						
					}
					else
					{
						ret.getScriptedData().put("STATUS", "");
						ret.getScriptedData().put("ERROR", "");
						validInstrument = true;
					}

					if(epi.getScoreGroup().getScores().get("PGPI_RDI") == null)
					{
						ret.getScriptedData().put("STATUS", "NOT_FINISHED");
						ret.getScriptedData().put("ERROR", "GPI Response Distortion Index has not been calculated, unable to generate data.");
					}
					else
					{
						ret.getScriptedData().put("STATUS", "");
						ret.getScriptedData().put("ERROR", "");

						rd.getDisplayData().put("PGPI_RDI", "GPI Response Distortion Index (%ile)");
						rd.getScoreData().put("PGPI_RDI", epi.getScoreGroup().getScores().get("PGPI_RDI").scoreAsNumber().toString());
						//System.out.println("1 " + epi.getScoreGroup().getScores().get("PGPI_RDI").getScore().toString());
						//System.out.println("2 " + epi.getScoreGroup().getScores().get("PGPI_RDI").getRawScore().toString());

						validInstrument = true;
					}
				}	// End GPI processing

				if(epi.getInstrument().getCode().equals(ReportingConstants.IC_LEI))
				{
					// Do LEI-specific stuff

					//instrument complete??
					if(epi.getScoreGroup().getScores().isEmpty())
					{
						ret.getScriptedData().put("STATUS", "NOT_FINISHED");
						ret.getScriptedData().put("ERROR", "LEI has not been completed, unable to generate data.");
					}
					else
					{
						ret.getScriptedData().put("STATUS", "");
						ret.getScriptedData().put("ERROR", "");
						validInstrument = true;
					}
				}

				if(epi.getInstrument().getCode().equals(ReportingConstants.IC_WGE))
				{
					// Do WATSON GLASER-specific stuff

					//instrument complete??
					if(epi.getScoreGroup().getScores().isEmpty())
					{
						ret.getScriptedData().put("STATUS", "NOT_FINISHED");
						ret.getScriptedData().put("ERROR", "Watson-Glaser E has not been completed, unable to generate data.");
					}
					else
					{
						ret.getScriptedData().put("STATUS", "");
						ret.getScriptedData().put("ERROR", "");
						validInstrument = true;
					}
				}

				// only process if there is valid instrument data.....
				if(validInstrument)
				{
					/*
					 * Step 2 - GET THE NORMED SCORES				
					 */
					// get the normed score objects 
					//HashMap<String, NormScore> gpPercentiles = epi.getScoreGroup().calculateGeneralPopulation();
					HashMap<String, NormScore> spPercentiles = new HashMap<String, NormScore>();
					try
					{
						spPercentiles = epi.getScoreGroup().calculateSpecialPopulation();														
					}
					catch ( NullPointerException npe)
					{
						rd.getScriptedData().put("NORM_ERROR", "Noms have not been completed for " + epi.getInstrument().getShortName() + ".  Unable to generate data.");
						String msg = "Noms have not been completed for " + epi.getInstrument().getShortName() + ".  Unable to generate data." + "   Norms can be set through the TestData application."; 
						System.out.println("ReportDataException:ReportHelper:  " + msg);	// Let the sysout log know
						//throw new ReportDataException(msg);
					}

					/*
					 * Step 3 - GET THE PDI RATINGS / FULL SCALE NAMES
					 */
					// so, spin through the NormScore objects
					// get the Score object from the NormScore object to get the Scale Name in long form
					// use the NormScore object NormScore ns.toRating(), returns a double.... 1.0, 1.5, 2.0, etc... 
					Iterator <NormScore> iter = spPercentiles.values().iterator();
					while(iter.hasNext())
					{
						NormScore ns = iter.next();					
						rd.getScoreData().put(ns.getNorm().getSpssValue() + "_SP_RATING", ns.toRating().toString());
						rd.getDisplayData().put(ns.getNorm().getSpssValue() + "_SP_RATING", ns.getScore().getScaleName());
						//System.out.println("ns.getScore().getScaleName()...." + ns.getScore().getScaleName()+  "  : " + ns.toRating().toString());
					}
					/*
					 * Step 4 - ADD THE RD TO THE REPORT
					 */
					// add to report data for individual report
					ret.getReportData().put(AbyDExtractFormatHelper.INST_CODE_TO_RC_NAME.get(epi.getInstrument().getCode()), rd);	
					//ret.getReportData().put("RAVENS_B", rd);				
					//reportingRequest.addReport(ret);
				}	// End of valid instrument processing

				counter++;
			} // end main for-loop (epi loop)

			ret.getDisplayData().put("ASSESSMENT_DATE", new SimpleDateFormat("dd MMM yyyy").format(latestInstDate));
			String fileName = "";
			fileName = StringUtils.removeFilenameSpecialCharacters(
								epis.get(0).getProject().getClient().getName() + " " +
								epis.get(0).getParticipant().getLastName() + "_" + 
								epis.get(0).getParticipant().getFirstName() + " " +
								epis.get(0).getProject().getName() + " " +
								ret.getReportCode()+ " " +
								new SimpleDateFormat("yyyyMMdd").format(new Date())
								);
			ret.setName(fileName.replace(" ", "_")+".xls");
			
			// Call PDA here for assessment extract
			ret = (new PdaDataHelper()).populatePdaData(ret, pptId, projId, langCode);
			
			return ret;
		}
		catch(Exception e)
		{
			System.out.println("Exception in getAssessmentExtractData:");
			e.printStackTrace();
			return null;
		}
	}
}
