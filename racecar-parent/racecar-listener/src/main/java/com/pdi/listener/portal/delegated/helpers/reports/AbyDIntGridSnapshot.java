/**
 * Copyright (c) 2017 Korn Ferry
 * All rights reserved.
 */
package com.pdi.listener.portal.delegated.helpers.reports;

//import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.sql.Connection;
import java.text.SimpleDateFormat;

import com.pdi.data.abyd.dto.common.CompElementDTO;
import com.pdi.data.abyd.dto.common.CompGroupData;
import com.pdi.data.abyd.dto.common.DNACellDTO;
import com.pdi.data.abyd.dto.common.ModDataDTO;
import com.pdi.data.abyd.dto.intGrid.IGCompDataDTO;
import com.pdi.data.abyd.dto.intGrid.IGSummaryDataDTO;
import com.pdi.data.abyd.helpers.intGrid.IGCompLineDataHelper;
import com.pdi.data.abyd.helpers.intGrid.IGKf4dDataHelper;
import com.pdi.data.abyd.helpers.intGrid.IGSummaryDataHelper;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.data.dto.ProjectParticipantInstrument;
import com.pdi.data.dto.TextGroup;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.scoring.Norm;
import com.pdi.string.StringUtils;
import com.pdi.xml.XMLUtils;


/*
 * AbyDIntGridSnapshot - class to hold methods relevant to the AbyDIntGridSnapshot
 *
 * NOTE:  EVEN THOUGH LOCATED UNDER THE DELEGATED FOLDER, THERE ARE NO METHODS IN THIS
 *        CLASS THAT ARE DEFINED IN THE DATA INTERFACE.
 *
 * This class is located here because the folder structure (delegated/helpers) is
 * inverted from normal practice elsewhere in this project stack (helpers/delegated).
 * Putting this helper class here avoided confusion with having two folders in the
 * portal structure that had the name "helper".
 */
public class AbyDIntGridSnapshot
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	private Norm _synthNorm;
	private int _dlci = 0;		// Holder for calculated dLCI
	private double _dlciZ = 0.0;	// Holder for calculated dLCI
	//
	// Constructors.
	//
    public AbyDIntGridSnapshot()
    {
    	// No logic here at the present time
    }

	//
	// Instance methods.
	//


	/**
	 * getIntGridSnapshotData - Method that retrieves the requisite data for the AbyD Int Grid Snapshot
	 *
	 * @param inp - An IndividualReport object with a small amount of relevant data
	 * @param epis - The ParojectParticipantReport objects associated with the request
	 * @param tg -  The static text labels (text groups) used to generate the report
	 */
	public IndividualReport getIntGridSnapshotData(IndividualReport inp,
				ArrayList<ProjectParticipantInstrument> epis,
				TextGroup tg, HashMap<String, String> overrides)
	{

		IndividualReport ret = inp;
		Connection con = null;

		try
		{


			// Get the participant info... assumes that all epis are for the same participant
			ProjectParticipantInstrument epi_x = epis.get(0);
			String participantId = epi_x.getParticipant().getId();

			for(ProjectParticipantInstrument epi : epis)
			{
				if(participantId == null || participantId.equalsIgnoreCase(""))
				{
					participantId = "";
				}

	//ret.setScriptedData(reportInput.getCurrentTLT().getScriptedData());

				/*
				 * ========================================
				 * ADD BASIC PARTICIPANT DATA TO REPORT
				 */

				ret.addDisplayData("FIRST_NAME", XMLUtils.xmlEscapeString(epi.getParticipant().getFirstName()));
				ret.addDisplayData("LAST_NAME", XMLUtils.xmlEscapeString(epi.getParticipant().getLastName()));
				ret.getDisplayData().put("PARTICIPANT_NAME", XMLUtils.xmlEscapeString(epi.getParticipant().getFirstName() + " " + epi.getParticipant().getLastName()));


				//Get Project data
				ret.addDisplayData("ORGANIZATION", XMLUtils.xmlEscapeString(epi.getProject().getClient().getName()));
		  		ret.addDisplayData("PROJECT_NAME", XMLUtils.xmlEscapeString(epi.getProject().getName()));

		  		//File Name
		  		String fileName = StringUtils.removeFilenameSpecialCharacters(
						epis.get(0).getParticipant().getLastName() + " " +
						epis.get(0).getParticipant().getFirstName() + " " +
						inp.getReportCode()+" "+
						new SimpleDateFormat("yyyyMMdd").format(new Date()));

				ret.setName(fileName.replace(" ", "_") + ".pdf");

				ret.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);

				java.util.Date date = new java.util.Date();
				SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
				ret.getDisplayData().put("TODAY_DATE", sdf.format(date));

				con = AbyDDatabaseUtils.getDBConnection();

				int dnaId = Integer.parseInt(overrides.get("dnaId"));

				IGSummaryDataHelper helper = new IGSummaryDataHelper(con, participantId, dnaId);
				IGSummaryDataDTO data = helper.getSummaryData();

				String partId = data.getPartId();
				
				IGKf4dDataHelper kf4dHelper = new IGKf4dDataHelper(con, partId, dnaId);

				double mean = data.getMean();
				int rdi = data.getRdi();
				double stdDev = data.getStdDev();

				String showRDI = "0";

				_synthNorm = new Norm();
				_synthNorm.setMean(mean);
				_synthNorm.setStdDev(stdDev);

				if (rdi >= 98) {
					showRDI = "1";
				}

				ret.getDisplayData().put("RDI", showRDI);

				String leadFirstName = XMLUtils.xmlEscapeString(data.getLcFname());
				String leadLastName = XMLUtils.xmlEscapeString(data.getLcLname());

				ret.getDisplayData().put("LEAD_CONSULTANT", leadFirstName + " " + leadLastName);

				ArrayList<ModDataDTO> modData = data.getGridData().getModArray();

				Map<Integer, Integer> modSequence = new HashMap<Integer, Integer>();

				Map<Integer, String> compSequence = new HashMap<Integer, String>();

				int modCount = modData.size() - 1;
				ret.getDisplayData().put("ModCount", "" + modCount);
				for (int i = 0; i < modData.size(); i++) {

					long modID = modData.get(i).getModId();
					String modName = modData.get(i).getModName();

					modSequence.put((int) modID, i);

					ret.getDisplayData().put("ModName_" + i, modName);
				}

				ArrayList<CompGroupData> fullCompArray = data.getGridData().getFullCompArray();


				int compGroupCount = fullCompArray.size() - 1;
				ret.getDisplayData().put("CompGroupCount", "" + compGroupCount);

				double cumm = 0;
				int cnt = 0;
				double avgScore = 0.00;
				double roundedAvgScore = 0.00;

				for (int i = 0; i < fullCompArray.size(); i++) {

					CompGroupData compGroup = fullCompArray.get(i);

					String compGroupName = compGroup.getCgName();

					ret.getDisplayData().put("CompGroupName_" + i, compGroupName);

					ArrayList<CompElementDTO> compElementArray = compGroup.getCgCompList();


					int compCount = compElementArray.size() - 1;
					ret.getDisplayData().put("CompCount_" + i, "" + compCount);



					for (int j = 0; j < compElementArray.size(); j++) {

						long compId = compElementArray.get(j).getCompId();

						compSequence.put((int) compId, i + "_" + j);

						IGCompLineDataHelper compHelper = new IGCompLineDataHelper(con, compId, partId, dnaId, kf4dHelper);
						IGCompDataDTO compData = compHelper.getCompLine();

						String compDisplayName = XMLUtils.xmlEscapeString(compData.getCompDisplayName());

						int scoreIdx = compData.getIntScoreIdx();
						String[] scoreTranslated = {"N/A","1.00","1.50","2.00","2.50","3.00","3.50","4.00","4.50","5.00"};

						String scoreText = "";

						if (scoreIdx >=0) {
							scoreText = scoreTranslated[scoreIdx];
						}

						ret.getDisplayData().put("IGCompDisplayName_" + i + "_" + j, compDisplayName);
						ret.getDisplayData().put("IGCompScoreIdx_" + i + "_" + j, "" + scoreText);

						if (scoreIdx > 0) {
							cumm += Double.parseDouble(scoreText);
							cnt++;
						}

					}

				}


				// get the total average and dLCI
				String roundedAvgFormatted = "";
				if (cnt > 0) {
					avgScore = (cumm / (double)cnt);
					roundedAvgScore = Math.round(avgScore*100.0)/100.0;
					roundedAvgFormatted = String.format("%.02f", roundedAvgScore);

					_dlciZ = _synthNorm.calcZScore(avgScore);
					_dlci = Norm.calcIntPctlFromZ(_dlciZ);
				}

				ret.getDisplayData().put("TotalAvg", roundedAvgFormatted);
				ret.getDisplayData().put("dLCI", "" + _dlci);




				// get the scores
				ArrayList<DNACellDTO> intersectionData = data.getGridData().getModCompIntersection();
				TreeMap<String, String> compScores = new TreeMap<String, String>();

				for (int k = 0; k < intersectionData.size(); k++) {

					long compID = intersectionData.get(k).getCompetencyId();
					long interModID  = intersectionData.get(k).getModuleId();

					String interScore = intersectionData.get(k).getScore();
					String sequenceComp = compSequence.get((int) compID);
					int sequenceMod = modSequence.get((int) interModID);

					String sequenceFull = sequenceComp + "&" + sequenceMod;
					compScores.put(sequenceFull, interScore);

					float floatIs = new Float(interScore);
					if (floatIs == -1.0)
					{
						interScore = "-";
					}
					ret.getDisplayData().put("InterScore_" +  sequenceComp + "_" + sequenceMod, interScore);
				}

				// get the averages for each competency
				String lastSequence = "";
				String currentSequence = "";
				int compCount = 0;
				String score = "";
				double runningTotal = 0;
				double compAvg = 0.00;
				String compAvgFormatted = "";

				for(Map.Entry<String,String> entry : compScores.entrySet()) {

					 score = entry.getValue();
					 String fullSequence = entry.getKey();

					 String[] splitSequence = fullSequence.split("&");
					 currentSequence = splitSequence[0];

//					 if (currentSequence.equals(lastSequence)) {
//						if (!score.equals("-") && !score.equals("") && !score.equals("0.00") && !score.equals("-1.00")) {
//							compCount++;
//						}
//					 } else {
//
//						  // get avg for previous competency
//						  compAvg = getCompAvg(compCount, runningTotal);
//						  compAvgFormatted = String.format("%.02f", compAvg);
//
//						  ret.getDisplayData().put("IGCompScoreAvg_" +  lastSequence, compAvgFormatted);
//
//						  // begin new competency cycle
//						  runningTotal = 0;
//						  compCount = 1;
//					 }
//
//					 if (!score.equals("-") && !score.equals("") && !score.equals("0.00") && !score.equals("-1.00")) {
//						  runningTotal += Double.parseDouble(score);
//					 }
//
//					 lastSequence = currentSequence;
					 
					if (! currentSequence.equals(lastSequence))
					{
						// get avg for previous competency
						compAvg = getCompAvg(compCount, runningTotal);
						compAvgFormatted = compAvg == 0.0 ? "" : String.format("%.02f", compAvg);
						ret.getDisplayData().put("IGCompScoreAvg_" +  lastSequence, compAvgFormatted);

						// begin new competency cycle
						runningTotal = 0;
						compCount = 0;
						lastSequence = currentSequence;
					}
					if (!score.equals("-") && !score.equals("") && !score.equals("0.00") && !score.equals("-1.00"))
					{
						compCount++;
						runningTotal += Double.parseDouble(score);
					}

				}

				// get the last competency's avg
				 compAvg = getCompAvg(compCount, runningTotal);
				 compAvgFormatted = compAvg == 0.0 ? "" : String.format("%.02f", compAvg);
				 ret.getDisplayData().put("IGCompScoreAvg_" +  lastSequence, compAvgFormatted);

			}


			return ret;
		}
		catch(Exception e)
		{
			System.out.println("doAbyDIntGridReview:");
			e.printStackTrace();
			return null;
		}
		finally
		{
			try
			{
				if(con != null)
				{
					con.close();
				}
			}
			catch(Exception se) {}
			con = null;
		}
	}

	private double getCompAvg(int count, double total) {

		double avgScore = 0.00;
		double roundedAvgScore = 0.00;

		if (count > 0 && total > 0) {

			avgScore = (total / (double)count);
			roundedAvgScore = Math.round(avgScore*100.0)/100.0;

		}

		return roundedAvgScore;

	}

}
