/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.listener.portal.delegated.helpers;

import com.pdi.data.dto.Instrument;
import com.pdi.data.dto.Participant;
import com.pdi.data.dto.Response;
import com.pdi.data.dto.ResponseGroup;
import com.pdi.data.dto.Score;
import com.pdi.data.dto.ScoreGroup;
import com.pdi.data.helpers.interfaces.IScoringHelper;
//import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.ReportData;
import com.pdi.scoring.request.ScoreRequest;
/**
 * This class does not need to be on a per-implementation basis, it does
 * not directly work with data
 * @author gmyers
 *
 */
public class ScoringHelper implements IScoringHelper {

	//@Override
	public ScoreGroup score(Instrument i, ResponseGroup rg) {
		ScoreGroup sg = new ScoreGroup();
		
		try {
			//score this thing!
			ReportData rd = new ReportData();
			for(Response r : rg.getResponses().values()) {

				rd.getRawData().put(r.getCode(), r.getValue());
			}
			rd = ScoreRequest.score(i.getMetadata().get("reportingInstId"), rd);
			
			for(String code : rd.getScoreData().keySet()) {
				String score = rd.getScoreData(code);
				Score s = new Score();
				s.setCode(code);
				s.setRawScore(score);
				sg.getScores().put(code, s);
			}
			
		} catch (Exception e) {
			
		}
		return sg;
	}

	@Override
	public ScoreGroup score(String scoreCode, Participant p) {
		//for now, we don't even use the code, just assume TLT scoring
		
		//Step 1, get all participant information
		
		//Step 2, build IR, or the best way to get the scores back
		
		//Step 3, build ScoreGroup
		
		//Step 4, returns a ScoreGroup
		return null;
	}

}
