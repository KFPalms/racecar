/*
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.listener.portal.groovy 

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.GroupReport;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;
import com.pdi.reporting.request.ReportingRequest;
import com.pdi.reporting.response.ReportingResponse;
import com.pdi.data.abyd.dto.reportInput.ReportInputDTO;
import com.pdi.data.abyd.dto.setup.EntryStateDTO;
import com.pdi.data.abyd.dto.setup.ReportModelStructureDTO;
import com.pdi.data.abyd.dto.setup.SPFullDataDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportLabelsDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportOptionsDTO;
import com.pdi.data.abyd.helpers.common.ImportExportDataHelper;
import com.pdi.data.abyd.helpers.reportInput.ReportInputDataHelper;
import com.pdi.data.constants.V2InstrumentConstants;
import com.pdi.data.v2.dto.ParticipantDTO;
import com.pdi.data.v2.dto.ProjectDTO;
import com.pdi.data.v2.dto.SessionUserDTO;
import com.pdi.data.v2.helpers.ManualEntryDataHelper;
import com.pdi.data.v2.helpers.ParticipantDataHelper;
import com.pdi.data.v2.util.Utils;
import com.pdi.data.v2.util.V2WebserviceUtils;
import com.pdi.data.v2.helpers.ProjectDataHelper;
import com.pdi.listener.portal.dao.DataAccessLayerV2;
import com.pdi.listener.portal.dao.DataAccessLayerV2.ParticipantInformation;
import com.pdi.listener.portal.dao.DataAccessLayerV2.ProjectInformation;
import com.pdi.properties.PropertyLoader;
import com.pdi.scoring.Norm;
import com.pdi.scoring.NormSet;
import com.pdi.string.StringUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.xml.XMLUtils;

/**
 * @author gmyers
 */
public class CHQ_DETAIL_REPORT
{
	private ReportingRequest reportRequest;
	private Node reportNode;
	private SessionUserDTO sessionUser;
	private DataAccessLayerV2 dataAccessLayer;

	private static String [] questionArray = new String [206];
	static{
		questionArray[0]="Q1";
		questionArray[1]="Q2";
		questionArray[2]="Q3";
		questionArray[3]="Q4";
		questionArray[4]="Q5";
		questionArray[5]="Q6";
		questionArray[6]="Q7";
		questionArray[7]="Q8";
		questionArray[8]="Q9";
		questionArray[9]="Q10";
		questionArray[10]="Q11";
		questionArray[11]="Q12";
		questionArray[12]="Q13";
		questionArray[13]="Q14";
		questionArray[14]="Q15";
		questionArray[15]="Q16";
		questionArray[16]="Q17";
		questionArray[17]="Q18";
		questionArray[18]="Q19";
		
		questionArray[19]="Q20";
		questionArray[20]="Q21";
		
		questionArray[21]="Q22";
		
		questionArray[22]="Q23";
		questionArray[23]="Q23_2";
		questionArray[24]="Q24";
		questionArray[25]="Q25";
		questionArray[26]="Q25_2";
		questionArray[27]="Q26";
		questionArray[28]="Q26_2";
		questionArray[29]="Q27";
		questionArray[30]="Q27_2";
		questionArray[31]="Q28";
		questionArray[32]="Q29";
		questionArray[33]="Q30";
		questionArray[34]="Q31";
		questionArray[35]="Q32";
		questionArray[36]="Q33";
		questionArray[37]="Q33_2";
		questionArray[38]="Q34";
		questionArray[39]="Q34_2";
		questionArray[40]="Q35";
		questionArray[41]="Q36";
		questionArray[42]="Q37";
		questionArray[43]="Q38";
		questionArray[44]="Q39";
		questionArray[45]="Q40";
		questionArray[46]="Q41";
		questionArray[47]="Q42";
		questionArray[48]="Q43";
		questionArray[49]="Q44";
		questionArray[50]="Q45";
		questionArray[51]="Q46";
		questionArray[52]="Q47";
		questionArray[53]="Q48";
		questionArray[54]="Q49";
		questionArray[55]="Q50";
		
		questionArray[56]="Q51";
		questionArray[57]="Q52";
		questionArray[58]="Q52_2";
		questionArray[59]="Q53";
		questionArray[60]="Q54";
		questionArray[61]="Q55";
		questionArray[62]="Q56";
		questionArray[63]="Q57";
		questionArray[64]="Q58";
		questionArray[65]="Q59";
		questionArray[66]="Q60";
		questionArray[67]="Q61";
		questionArray[68]="Q62";
		questionArray[69]="Q63";
		questionArray[70]="Q64";
		questionArray[71]="Q65";
		questionArray[72]="Q66";
		questionArray[73]="Q67";
		questionArray[74]="Q68";
		questionArray[75]="Q69";
		questionArray[76]="Q70";
		questionArray[77]="Q71";
	
		questionArray[78]="Q72";
		questionArray[79]="Q73";
		questionArray[80]="Q74";
		questionArray[81]="Q75";
		questionArray[82]="Q76";
		questionArray[83]="Q77";
		questionArray[84]="Q78";
		questionArray[85]="Q78_2";
		questionArray[86]="Q79";
		questionArray[87]="Q79_2";
		questionArray[88]="Q80";
		questionArray[89]="Q80_2";
		questionArray[90]="Q81";
		questionArray[91]="Q82";
		questionArray[92]="Q83";
		questionArray[93]="Q84";
		questionArray[94]="Q84_2";
		
		questionArray[95]="Q21_1_1";
		questionArray[96]="Q21_1_2";
		questionArray[97]="Q21_1_3";
		questionArray[98]="Q21_1_4";
		questionArray[99]="Q21_1_5";
		questionArray[100]="Q21_1_6";
		
		questionArray[101]="Q21_2_1";
		questionArray[102]="Q21_2_2";
		questionArray[103]="Q21_2_3";
		questionArray[104]="Q21_2_4";
		questionArray[105]="Q21_2_5";
		questionArray[106]="Q21_2_6";
		
		questionArray[107]="Q21_3_1";
		questionArray[108]="Q21_3_2";
		questionArray[109]="Q21_3_3";
		questionArray[110]="Q21_3_4";
		questionArray[111]="Q21_3_5";
		questionArray[112]="Q21_3_6";
		
		questionArray[113]="Q21_4_1";
		questionArray[114]="Q21_4_2";
		questionArray[115]="Q21_4_3";
		questionArray[116]="Q21_4_4";
		questionArray[117]="Q21_4_5";
		questionArray[118]="Q21_4_6";
		
		questionArray[119]="Q21_5_1";
		questionArray[120]="Q21_5_2";
		questionArray[121]="Q21_5_3";
		questionArray[122]="Q21_5_4";
		questionArray[123]="Q21_5_5";
		questionArray[124]="Q21_5_6";
		
		questionArray[125]="Q36_01_1";
		questionArray[126]="Q36_01_2";
		questionArray[127]="Q36_01_3";
		questionArray[128]="Q36_01_4";
		questionArray[129]="Q36_01_5";
		questionArray[130]="Q36_01_6";
		
		questionArray[131]="Q36_02_1";
		questionArray[132]="Q36_02_2";
		questionArray[133]="Q36_02_3";
		questionArray[134]="Q36_02_4";
		questionArray[135]="Q36_02_5";
		questionArray[136]="Q36_02_6";
		
		questionArray[137]="Q36_03_1";
		questionArray[138]="Q36_03_2";
		questionArray[139]="Q36_03_3";
		questionArray[140]="Q36_03_4";
		questionArray[141]="Q36_03_5";
		questionArray[142]="Q36_03_6";
		
		questionArray[143]="Q36_04_1";
		questionArray[144]="Q36_04_2";
		questionArray[145]="Q36_04_3";
		questionArray[146]="Q36_04_4";
		questionArray[147]="Q36_04_5";
		questionArray[148]="Q36_04_6";
		
		questionArray[149]="Q36_05_1";
		questionArray[150]="Q36_05_2";
		questionArray[151]="Q36_05_3";
		questionArray[152]="Q36_05_4";
		questionArray[153]="Q36_05_5";
		questionArray[154]="Q36_05_6";
		
		questionArray[155]="Q36_06_1";
		questionArray[156]="Q36_06_2";
		questionArray[157]="Q36_06_3";
		questionArray[158]="Q36_06_4";
		questionArray[159]="Q36_06_5";
		questionArray[160]="Q36_06_6";
		
		questionArray[161]="Q36_07_1";
		questionArray[162]="Q36_07_2";
		questionArray[163]="Q36_07_3";
		questionArray[164]="Q36_07_4";
		questionArray[165]="Q36_07_5";
		questionArray[166]="Q36_07_6";
		
		questionArray[167]="Q36_08_1";
		questionArray[168]="Q36_08_2";
		questionArray[169]="Q36_08_3";
		questionArray[170]="Q36_08_4";
		questionArray[171]="Q36_08_5";
		questionArray[172]="Q36_08_6";
		
		questionArray[173]="Q36_09_1";
		questionArray[174]="Q36_09_2";
		questionArray[175]="Q36_09_3";
		questionArray[176]="Q36_09_4";
		questionArray[177]="Q36_09_5";
		questionArray[178]="Q36_09_6";
		
		questionArray[179]="Q36_10_1";
		questionArray[200]="Q36_10_2";
		questionArray[201]="Q36_10_3";
		questionArray[202]="Q36_10_4";
		questionArray[203]="Q36_10_5";
		questionArray[204]="Q36_10_6";
		
		questionArray[205]="Q36";
	}
	
	
	/*
	 * generate
	 */
	public void generate()
	{
		String reportCode = reportNode.getAttributes().getNamedItem("code").getTextContent();
		//System.out.println(reportCode);
		NodeList participants = XMLUtils.getElements(reportNode, "Participants/Participant");
		
		for( int j= 0; j < participants.getLength(); j++)
		{
			Node participant = participants.item(j);
			String participantId = participant.getAttributes().getNamedItem("participantId").getTextContent();
			String projectId = participant.getAttributes().getNamedItem("projectId").getTextContent();
			if(participantId == null || participantId.equalsIgnoreCase("")) {
				participantId = "";
			}
			
			try
			{
				//Got all of the basic data, time to add abyd data
				ProjectInformation projectInformation = dataAccessLayer.projectInformationHash.get(projectId);
				ParticipantInformation participantInformation = null;
				for(String code : dataAccessLayer.participantInformationList.keySet()) {
					ParticipantInformation pi =  dataAccessLayer.participantInformationList.get(code);
					if(pi.getParticipantId().equalsIgnoreCase(participantId) && pi.getProjectId().equalsIgnoreCase(projectId)) {
						participantInformation = pi;
						break;
					}
				}
				
				Setup2ReportOptionsDTO options = projectInformation.getReportOptions();
				Setup2ReportLabelsDTO labels = projectInformation.getReportLabels();
				SPFullDataDTO spFullData = projectInformation.getSpFullData();
				EntryStateDTO entryState = projectInformation.getEntryState();
				ReportModelStructureDTO reportModelStructure = projectInformation.getReportModelStructure();
				ProjectDTO projectDto = projectInformation.getProject();
				
				ReportInputDTO reportInput;
				ParticipantDTO participantDto;
				IndividualReport ir = new IndividualReport();
				if(participantInformation != null) {
					reportModelStructure = participantInformation.getReportModelStructure();
					reportInput = participantInformation.getReportInput();
					participantDto = participantInformation.getParticipant();
					ir = participantInformation.getIndividualReport();
				}	

				// HEADER DATA //
				//The instrument reports will have the following information in the header:
				//Row 1: Name; Administration Date; Norm Group
				//Row 2: Company; Project; General Population 
				ir.getReportData().put(ReportingConstants.RC_CHQ, new ReportData());
				
				String modId = ir.getDisplayData("MODULE_ID"); // need this to get the norm names
				//String name = ir.getDisplayData("FIRST_NAME") + " " + ir.getDisplayData("LAST_NAME");
				//ir.getDisplayData().put("PARTICIPANT_NAME", name  );				
				//ir.getDisplayData().put("ADMIN_DATE", ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData("ADMIN_DATE"));  // date instrument is completed
				//System.out.println("admin date..... " + ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData("ADMIN_DATE"));
					

				if(projectDto != null && projectDto.getClient() != null && projectDto.getClient().getName() != null) {
					ir.getDisplayData().put("ORGANIZATION", projectDto.getClient().getName());
				} else {
					ir.getDisplayData().put("ORGANIZATION", "");
				}
				if (projectInformation.getDnaName() == null)
				{
					ir.getDisplayData().put("PROJECT_NAME", projectDto.getName());
				}  else  {
					ir.getDisplayData().put("PROJECT_NAME", projectInformation.getDnaName());				
				}
				////println "proj=" + pdto.getName() + ", dna=" + dnaName;
				////println "PROJECT_NAME=" + ir.getDisplayData().get("PROJECT_NAME");
							
				String name = participantDto.getFirstName() + " " + participantDto.getLastName();
				ir.getDisplayData().put("PARTICIPANT_NAME", name  );
				
				String fileName = StringUtils.removeSpecialCharacters(
						projectDto.getClient().getName() + " " +
						participantDto.getLastName() + " " +
						participantDto.getFirstName() + " " +
						new SimpleDateFormat("ddMMMyyyy").format(new java.util.Date()) + " ");
				ir.setName(fileName + reportCode + ".pdf");

				java.util.Date date = new java.util.Date();
				SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
				ir.getDisplayData().put("TODAY_DATE", sdf.format(date));
					
				String hraDateString = "";
				
				Connection con = null;				
				try {
					con = AbyDDatabaseUtils.getDBConnection();
					hraDateString = Utils.getAdminDateForInstrument(con, participantId, V2InstrumentConstants.CHQ_V2_ID, projectId);
				} catch (Exception e) {
				
				} finally {
					if(con != null) {
						try { con.close() } catch(Exception e) {}
					}
				}
									
				if(hraDateString == null)
				{
					ir.getScriptedData().put("STATUS", "NOT_FINISHED");
				} else {
					date = Utils.getNormalDateStringFromHRADate(hraDateString);
					ir.getDisplayData().put("ADMIN_DATE", sdf.format(date));
				}

				// ANSWER DATA //
				// get all the existing raw data, and put it into the ir
				HashMap<String, String> answerMap = new ManualEntryDataHelper().getAdaptV2Answers(participantId, V2InstrumentConstants.CHQ_V2_ID);
				if(answerMap == null)
				{
					ir.getScriptedData().put("STATUS", "NOT_FINISHED");
					
				} else {
					ir.getScriptedData().put("STATUS", "COMPLETE");
					for (Iterator itr=answerMap.entrySet().iterator(); itr.hasNext(); )
					{
						Map.Entry<String, Double> ent = itr.next();
						ir.getReportData().get(ReportingConstants.RC_CHQ).getRawData().put(ent.getKey(), ent.getValue());
						println("  " + ent.getKey() + " : " + ent.getValue());
					}
						
					// now, fill in empty strings for any response that's not there... 
					// note that we never know which of the CHQ's was taken, or how much of a 
					// CHQ is actually finished when submitted... so we need to do this....
					for ( Iterator iter=questionArray.iterator();iter.hasNext(); )
					{
						String q = iter.next();
						if(ir.getReportData().get(ReportingConstants.RC_CHQ).getRawData().get(q) ==  null )
						{
							ir.getReportData().get(ReportingConstants.RC_CHQ).getRawData().put(q, "");
						}
					}
						
					// QUESTION DATA //
					// Identification
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q1",    "Name");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q2",    "Company");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q3",    "Current job title");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q4",    "Mailing Address");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q5",    "City");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q6",    "State/Province");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q7",    "Country");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q8",    "Postal Code");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q9",    "Home Phone:  Country Code");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q10",   "Home Phone");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q11",   "Work Phone:  Country Code");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q12",   "Work Phone");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q13",   "Mobile Phone:  Country Code");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q14",   "Mobile Phone");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q15",   "E-mail address");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q16",   "Your manager's name (for development assessments only)");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q17",   "Your manager's phone number Country Code");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q18",   "Your manager's phone number");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q19",   "Your manager's e-mail address");
					
					// Education
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q20",   "Indicate your highest level of education:");
						// Education Grid
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q21",   "List all education since high school");							
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q21_1_1",   "College/University, Name, City, State/Province, Country");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q21_1_2",   "From (Year)");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q21_1_3",   "To (Year)");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q21_1_4",   "Did You Graduate? (Yes/No)");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q21_1_5",   "Degree");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q21_1_6",   "Major");
					
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q22",   "If you attended a college or university, why did you pursue your field(s) of study?");
					
					// Career and Work History
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q23",   "In what country/territory are you currently based?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q23_2", "Other (specify):");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q24",   "Are you working as an expatriate right now?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q25",   "Throughout your entire career, in which countries/territories have you lived or worked long enough to develop a good understanding of the culture and work practices?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q25_2", "Other (specify):");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q26",   "Which one of the following best describes the industry in which you work?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q26_2", "Other (specify):");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q27",   "In which functional areas do you primarily work?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q27_2", "Other (specify):");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q28",   "How many people does your company employ, including all divisions?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q29",   "Do you supervise or manage people?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q30",   "In total, how many people do you manage, both directly and indirectly (i.e., your direct reports, their direct reports, etc.)?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q31",   "How long have you been in management? (years)");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q32",   "Which ONE of the following best represents your current management level?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q33",   "Which ONE of the following best represents your current role?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q33_2", "Other (specify):");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q34",   "What is your approximate annual compensation for the current year, including commissions, bonuses, stock options, etc., but excluding benefits and perquisites? (select a currency, then provide amount of compensation)");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q34_2", "Other (specify):");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q35",   "My annual compensation (numeric):");
						// Work History Grid
					//ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q36",   "Please list your work history in the table below.  If you would prefer to e-mail your resume/CV to PDI rather than listing your work history below, please check this box:");
						ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q36_01_1",   "Organization");
						ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q36_01_2",   "City, State/Province, Country");
						ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q36_01_3",   "Positions Held");
						ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q36_01_4",   "Date(Year) From");
						ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q36_01_5",   "Date(Year) To");
						ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q36_01_6",   "Responsibilities");
										
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q37", "Within the scope of your current role, what are the biggest business challenges you need to address over the next year?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q38", "Please estimate (from 0 to 100 percent) your chances of successfully addressing these challenges:");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q39", "What factors may prevent you from successfully addressing these challenges?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q40", "What do you consider to be your greatest accomplishment?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q41", "What job tasks did you perform most effectively?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q42", "What job tasks did you perform least effectively?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q43", "What have you liked best about your work?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q44", "What have you liked least about your work?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q45", "Why did you go into this line of work?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q46", "What leadership experiences, roles, and/or assignments were the most formative and/or had the most impact on your career, and why?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q47", "Describe any significant career movements, changes, or transitions.");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q48", "What roles and/or jobs have you enjoyed the most?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q49", "What roles and/or jobs have you enjoyed the least?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q50", "Prior to the last five years, what do you consider to be your greatest accomplishments?");
					
					// Professional Development
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q51", "Describe any professional, civic, or vocational leadership roles or involvement that has contributed to your professional growth.");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q52", "What professional development experiences have you had over the last five years?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q52_2", "Other (specify):");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q53", "Of these development experiences, which have been the most valuable to you in your career?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q54", "Which professional and/or business publications do you regularly read?");
					
					// Career Aspirations
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q55", "Currently, how interested are you in making a career move and why?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q56", "Are you willing to relocate?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q57", "Specify the locations to which you would be willing to relocate and/or any constraints on relocation.");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q58", "Do you anticipate your willingness to relocate will change in the future?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q59", "What are you looking for in your next position?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q60", "Ideally, what do you hope to accomplish in your career over the next three years?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q61", "Ideally, what do you hope to accomplish in your career over the next ten years?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q62", "If you don't reach your goals, which of your personal attributes is most likely to have held you back?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q63", "What other barriers may keep you from reaching your goals?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q64", "In general, what job experiences or development activities do you think would be most helpful to you in meeting your career goals?");
					
					//Self Evaluation
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q65", "What strengths have been consistently observed in you, either through self-reflection or by others?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q66", "What would you or others identify as your areas of greatest relative weakness or development need?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q67", "Sometimes people misinterpret what we do.  How do others see you that is different from how you really are?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q68", "What attributes have you improved upon over the years?");
					
					//Work Environment
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q69", "Describe your ideal job:");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q70", "In the past, what aspects of the work environment (e.g., people, processes, systems, culture, leadership) have made it difficult for you to do your job?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q71", "Please review the list below and rank the five factors that are most important to you in a job.  Enter the numerals 1, 2, 3, 4, and/or 5 to indicate your ranking, with 1 being the highest and 5 the lowest of your choices.  Rank no more than 5 items, although you may rank fewer than five.  Do not give the same ranking to more than one item (e.g., do not rank two factors as '3').");
					
					// Comments
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q72", "Please provide any additional information you feel would help us better understand you and your career.");
					
					// Confidential Demographic Information 
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q73", "First three letters of your mother's maiden/family name:");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q74", "First three letters of your father's family name:");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q75", "First four letters of the city in which you were born:");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q76", "Day of your birth:");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q77", "Month of your birth:");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q78", "In which countries/territories did you spend most of your early years (before age 13)?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q78_2", "Other (specify):");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q79", "In which country/territory (or countries/territories) are you a national citizen?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q79_2", "Other (specify):");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q80", "What is your primary language (mother tongue)?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q80_2", "Other (specify):");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q81", "How many years have you been in the workforce?");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q82", "Date of Birth:");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q83", "Gender:");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q84", "Ethnicity (US Only):");
					ir.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("Q84_2", "Other (specify):");
				
				}
				
				ir.getDisplayData().put("CLIENT_LOGO", projectInformation.getProject().getClient().getId()+".png");	
				ir.setReportCode(reportCode);
				ir.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);

				reportRequest.addReport(ir);				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}	// End of participants loop
	}
}
