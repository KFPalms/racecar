package com.pdi.listener.portal.delegated.helpers.reports;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import com.pdi.data.abyd.helpers.intGrid.IGKf4dDataHelper;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.data.abyd.util.Utils;
import com.pdi.data.dto.ProjectParticipantInstrument;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.Report;
import com.pdi.string.StringUtils;


/*
 * RiskReport - Class to hold methods relevant to the KF4D Risk Factor Report
 * 
 * NOTE:  EVEN THOUGH LOCATED UNDER THE DELEGATED FOLDER, THERE ARE NO METHODS IN THIS
 *        CLASS THAT ARE DEFINED IN THE DATA INTERFACE.
 *        
 * This class is located here because the folder structure (delegated/helpers) is
 * inverted from normal practice elsewhere in this project stack (helpers/delegated).
 * Putting this helper class here avoided confusion with having two folders in the
 * portal structure that had the name "helper".
 */
public class RiskReport {

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	
	//
	// Constructors.
	//
	 public RiskReport()
	 {
	 	// No logic here at the present time
	 }
	
	//
	// Instance methods.
	//


	 /**
	  * getRiskData - Sets up data for the 
	  * @param inp
	  * @param epis
	  * @return
	  */
	public Report getRiskFactorsData(IndividualReport inp, ArrayList<ProjectParticipantInstrument> epis)
//		throws Exception
	{
		IndividualReport ret = inp.clone();
		String pptId = null;
		String projId = null;
		long dnaId = 0;
		Connection con = null;

		try
		{
			// For this report we assume that there is a single participant
			ProjectParticipantInstrument epi = epis.get(0);
			pptId = epi.getParticipant().getId();
			projId = epi.getProject().getId();
			
			// get the dna ID
			long pil = (new Long(projId).longValue());
			dnaId = Utils.getDnaIdFromProjectId(pil);
			
			// Get the data
			Map<String, Integer> dat;
			con = AbyDDatabaseUtils.getDBConnection();

			IGKf4dDataHelper helper = new IGKf4dDataHelper(con, pptId, dnaId);
			dat = helper.getRiskFactors();

			// set up the data for display
			for (Map.Entry<String, Integer> entry : dat.entrySet())
			{
			    String key = entry.getKey();
			    Integer pctl = entry.getValue();
			    ret.addDisplayData(key + "-pctl", "" + pctl);
 			}
 
   			// Generate the file name
	  		String fileName = StringUtils.removeFilenameSpecialCharacters(
						epi.getParticipant().getLastName() + " " +
						epi.getParticipant().getFirstName() + " " +
						epi.getProject().getProjectTypeName()+" "+
						inp.getReportCode()+" "+
						new SimpleDateFormat("yyyyMMdd").format(new Date()) 
						);

			ret.setName(fileName.replace(" ", "_") + ".xls");	// "x" will be added later

			return ret;
		}
		catch(Exception e)
		{
			System.out.println("Exception trapped in getAlrDorTData:  msg=" + e.getMessage());
			return null;
		}
		finally
		{
			if (con != null)
			{
				try
				{
					con.close();
				}
				catch (SQLException se) {}
				con = null;
			}
		}
	}
}
