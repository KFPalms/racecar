/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.listener.portal.delegated.helpers.reports;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

//import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.data.dto.ProjectParticipantInstrument;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.Report;
import com.pdi.string.StringUtils;
import com.pdi.xml.XMLUtils;

/*
 * FinexDetailReport - class to hold methods relevant to the FinEx detail report
 * 
 * NOTE:  EVEN THOUGH LOCATED UNDER THE DELEGATED FOLDER, THERE ARE NO METHODS IN THIS
 *        CLASS THAT ARE DEFINED IN THE DATA INTERFACE.
 *        
 * This class is located here because the folder structure (delegated/helpers) is
 * inverted from normal practice elsewhere in this project stack (helpers/delegated).
 * Putting this helper class here avoided confusion with having two folders in the
 * portal structure that had the name "helper".
 */
public class FinexDetailReport
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	
	//
	// Constructors.
	//
    public FinexDetailReport()
    {
    	// No logic here at the present time
    }

	//
	// Instance methods.
	//


   	/**
   	 * getFinexDetailData is a method that
   	 * handles data for the FIN-EX DETAIL report
   	 * 
   	 * NOTE:  EVEN THOUGH LOCATED UNDER THE DELEGATED FOLDER, THERE ARE NO METHODS IN THIS
   	 *        CLASS THAT ARE DEFINED IN THE DATA INTERFACE.
   	 *        
   	 * This class is located here because the folder structure (delegated/helpers) is
   	 * inverted from normal practice elsewhere in this project stack (helpers/delegated).
   	 * Putting this helper class here avoided confusion with having two folders in the
   	 * portal structure that had the name "helper".
   	 * 
	 * @param inp - In input IndividualReport object with a little preliminary data
	 * @param epis -  A number of ProjectParticipantInstrument objects for this participant
   	 */
   	public ArrayList<Report> getFinexDetailData(IndividualReport inp,  
   			ArrayList<ProjectParticipantInstrument> epis)
   	{
   		//System.out.println("doFinexDetailRpt...");
   		ArrayList<Report> rpts = new ArrayList<Report>();
   		try
   		{
   			for(ProjectParticipantInstrument epi : epis)
   			{
   				// spin through until you find the finex
   				if(! epi.getInstrument().getCode().equals("finex"))
   				{
   					continue;
   				}

   	   			IndividualReport ret = inp.clone();	// get the input data

   				/*
   				 * ========================================
   				 * ADD BASIC PARTICIPANT DATA TO REPORT 
   				 */
				ret.addDisplayData("FIRST_NAME", epi.getParticipant().getFirstName());
				ret.addDisplayData("LAST_NAME", epi.getParticipant().getLastName());
				ret.getDisplayData().put("PARTICIPANT_NAME", epi.getParticipant().getFirstName() + " " + epi.getParticipant().getLastName());			
				ret.addDisplayData("ORGANIZATION", XMLUtils.xmlEscapeString(epi.getProject().getClient().getName()));
		  		ret.addDisplayData("PROJECT_NAME", XMLUtils.xmlEscapeString(epi.getProject().getName()));	
		  		
		  		//ReportData rd = new ReportData();
				if(epi.getInstrumentTakenDate() == null)
				{
					java.util.Date date = new java.util.Date();
					SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");				
					ret.getDisplayData().put("ADMIN_DATE", sdf.format(date));
				} else {
					ret.getDisplayData().put("ADMIN_DATE", epi.getInstrumentTakenDate().substring(0, 10));
				}  		
		  		//ret.getDisplayData().put("FINEX_SP_NAME", epi.getScoreGroup().getSpecialPopulation().getName()); // SPEC-POP NORM					
		  		//ret.getDisplayData().put("FINEX_GP_NAME", epi.getScoreGroup().getGeneralPopulation().getName() ); //GEN-POP NORM

				String fileName = StringUtils.removeFilenameSpecialCharacters(
						epis.get(0).getParticipant().getLastName() + " " +
						epis.get(0).getParticipant().getFirstName() + " " +
						epis.get(0).getProject().getProjectTypeName()+" "+
						inp.getReportCode()+" "+
						new SimpleDateFormat("yyyyMMdd").format(new Date()) + " " 
						);
						
				ret.setName(fileName.replace(" ", "_") + ".pdf");
				
				ret.getDisplayData().put("CLIENT_LOGO", epi.getProject().getClient().getId()+".png");

				//instrument not complete??
				if(epi.getScoreGroup().getScores().isEmpty())
				{
					ret.getScriptedData().put("STATUS", "NOT_FINISHED");
					ret.getScriptedData().put("ERROR", "Financial Exercise has not been completed, unable to generate report");
				} else {
					ret.getScriptedData().put("STATUS", "");
					ret.getScriptedData().put("ERROR", "");
				}

				if(epi.getScoreGroup().getScores().get("FE_CORRECT").getRawScore()  == null)
				{
					ret.getScriptedData().put("STATUS", "NOT_FINISHED");
					ret.getScriptedData().put("ERROR", "No score data for Financial Exercise");
				}

				// number correct
				BigDecimal numCorrect = new BigDecimal (epi.getScoreGroup().getScores().get("FE_CORRECT").getRawScore()); 			
				int intCorrect = numCorrect.intValue();
				ret.getScriptedData().put("NUM_CORRECT", new Integer(intCorrect).toString());

				// percent correct
				BigDecimal percentCorrect = new BigDecimal( new Float( ((numCorrect.floatValue()/13)*100 ) + .5 ));				
				int intPercent = percentCorrect.intValue();
				ret.getScriptedData().put("PERCENT_CORRECT", new Integer(intPercent).toString());

				// pdi rating
				BigDecimal mpd = new BigDecimal (epi.getScoreGroup().getScores().get("FE_MPD").getRawScore());
				BigDecimal afc = new BigDecimal (epi.getScoreGroup().getScores().get("FE_AFC").getRawScore());
				BigDecimal dam = new BigDecimal (epi.getScoreGroup().getScores().get("FE_DAM").getRawScore());
				BigDecimal dac = new BigDecimal (epi.getScoreGroup().getScores().get("FE_DAC").getRawScore());

				BigDecimal pdiRating = mpd.add(afc); 
				BigDecimal pdiRating2 = pdiRating.add(dam);  
				BigDecimal pdiRating3 = pdiRating2.add(dac);	 				
				pdiRating = pdiRating3.divide(new BigDecimal(4));
				double pdiRating4 =  Math.floor( ((pdiRating.doubleValue() * 2) + .5 ))/2 ;
				ret.getScriptedData().put("PDI_RATING", new Double(pdiRating4).toString());

				rpts.add(ret);
   			} // end main for-loop

   			return rpts;
   		}
   		catch(Exception e)
   		{
   			System.out.println("doFinexDetailRpt:");
   			e.printStackTrace();
   			return null;
   		}
   	}
}
