/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.listener.portal.delegated.helpers.ut;

import java.util.ArrayList;
import java.util.HashMap;
//import java.util.Iterator;
//import java.util.Map;

import com.pdi.data.dto.Client;
import com.pdi.data.dto.Participant;
import com.pdi.data.dto.Project;
import com.pdi.data.dto.ProjectParticipantInstrument;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.IProjectParticipantInstrumentHelper;
import com.pdi.data.helpers.interfaces.IReportHelper;
import com.pdi.data.nhn.util.NhnDatabaseUtils;
import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.reporting.constants.ReportingConstants;
//import com.pdi.reporting.report.GroupReport;
//import com.pdi.reporting.report.IndividualReport;
//import com.pdi.reporting.request.ReportingRequest;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

public class ReportHelperUT extends TestCase
{
	
	//
	// Constructor
	//
	public ReportHelperUT(String name)
	{
		super(name);
	}

	
	/*
	 * testAddReports
	 * 
	 * Tests the funtionality of the addReports function
	 */
//	public void testAddReports()
//	throws Exception
//	{
//		UnitTestUtils.start(this);
//		V2DatabaseUtils.setUnitTest(true);
//	
//		SessionUser su = new SessionUser();
//		
//		String participantId = "364005";
//		String projectId = "60"; 
//		
//		//String reportCode = ReportingConstants.REPORT_CODE_TLT_GROUP_SUMMARY;	// No labels
//		String reportCode = ReportingConstants.REPORT_CODE_TLT_GROUP_DETAIL;	// Group labels
//		//String reportCode = ReportingConstants.REPORT_CODE_TLT_INDIVIDUAL_SUMMARY;	// Indiv labels
//
//		//String langCode = "en";
//		String langCode = "fr";
//		
//		HashMap<String, String> overrides = new HashMap<String, String>();
//		 
//		// Get the participant stuff
//		IProjectParticipantInstrumentHelper epih = HelperDelegate.getProjectParticipantInstrumentHelper("com.pdi.data.v2.helpers.delegated");
//		ArrayList<ProjectParticipantInstrument> epis = epih.fromLastParticipantId(su, IProjectParticipantInstrumentHelper.MODE_FULL, participantId);
//
////		Participant participant = HelperDelegate.getParticipantHelper("com.pdi.data.nhn.helpers.delegated").fromId(su, participantId);
//		Participant participant = new Participant();
//		participant.setId(participantId);
//		participant.setFirstName("Bogus");
//		participant.setLastName("de Bogus");
//		participant.setEmail("email@email.org");
//		HashMap<String, String> partMeta =  new HashMap<String, String>();
//		partMeta.put("jobTitle", "This is my job!");
//		partMeta.put("orgUnit", "This is my org!");
//		participant.setMetadata(partMeta);
//		Project project = new Project();
//		project.setId(projectId);
//		project.setName("Bogus Project #1");
//		Client client = new Client();
//		client.setId("12345");
//		client.setName("Boogedy Client");
//		project.setClient(client);
//		HashMap<String, String> projMeta =  new HashMap<String, String>();
//		projMeta.put("transitionLevel", "MLL");
//		projMeta.put("optional_1", "OPT 1");
//		projMeta.put("optional_2", "OPT 2");
//		projMeta.put("optional_3", "OPT 3");
//		project.setMetadata(projMeta);
//
//		// Get project stuff
////		Project project = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated").fromId(su, projectId);
//		for(ProjectParticipantInstrument ppi : epis)
//		{
//			ppi.setParticipant(participant);
//			ppi.setProject(project);
//		}
//		
//		// Do the call
//		IReportHelper irh = HelperDelegate.getReportHelper();
//
//		irh.addReport(su, reportCode, langCode, epis, overrides);
//		
//		ReportingRequest rr = irh.getReportingRequestForDebug();
//		String type = "";
//		HashMap<String, String> hm = null;
//		for(Object rpt : rr.getReports())
//		{
//			if (rpt instanceof IndividualReport)
//			{
//				type = "IR";
//				IndividualReport ir = (IndividualReport)rpt;
//				hm = ir.getLabelData();
//			}
//			else if (rpt instanceof GroupReport)
//			{
//				type = "GR";
//				GroupReport gr = (GroupReport)rpt;
//				hm = gr.getLabelData();
//			}
//			if (type.length() > 0)
//			{
//				System.out.println("Type = " + type);
//				for (Iterator<Map.Entry<String, String>> itr=hm.entrySet().iterator(); itr.hasNext(); )
//				{
//					Map.Entry<String, String> ent = itr.next();
//					System.out.println("    " + ent.getKey() + "=" + ent.getValue());
//				}
//			}
//			else
//				System.out.println("Unknown report type");
//
//		}
//
//		UnitTestUtils.stop(this);
//	}

	
	/*
	 * testAddReportsSpecial
	 * 
	 * Tests the functionality of the addReports function, using a NHN participant
	 */
	public void testAddReportsSpecial()
	throws Exception
	{
		UnitTestUtils.start(this);
		V2DatabaseUtils.setUnitTest(true);
		NhnDatabaseUtils.setUnitTest(true);
	
		SessionUser su = new SessionUser();
		
		// Participant from QA database
		//String participantId = "364867";
		//String participantId = "364005";
		//String projectId = "155"; 
		String participantId = "364863";
		String projectId = "155"; 
		
		//String reportCode = ReportingConstants.REPORT_CODE_TLT_GROUP_SUMMARY;	// No labels
		String reportCode = ReportingConstants.REPORT_CODE_TLT_GROUP_DETAIL;	// Group labels
		//String reportCode = ReportingConstants.REPORT_CODE_TLT_INDIVIDUAL_SUMMARY;	// Indiv labels

		String langCode = "en";
		//String langCode = "fr";
		
		HashMap<String, String> overrides = new HashMap<String, String>();
		 
		// Get the participant stuff
		IProjectParticipantInstrumentHelper epih = HelperDelegate.getProjectParticipantInstrumentHelper("com.pdi.data.v2.helpers.delegated");
		ArrayList<ProjectParticipantInstrument> epis = epih.fromLastParticipantId(su, IProjectParticipantInstrumentHelper.MODE_FULL_COMPLETED, participantId, null);

//		Participant participant = HelperDelegate.getParticipantHelper("com.pdi.data.nhn.helpers.delegated").fromId(su, participantId);
		Participant participant = new Participant();
		participant.setId(participantId);
		participant.setFirstName("Bogus");
		participant.setLastName("de Bogus");
		participant.setEmail("email@email.org");
		HashMap<String, String> partMeta =  new HashMap<String, String>();
		partMeta.put("jobTitle", "This is my job!");
		partMeta.put("orgUnit", "This is my org!");
		participant.setMetadata(partMeta);
		Project project = new Project();
		project.setId(projectId);
		project.setName("Bogus Project #1");
		project.setProjectTypeName("TLT");
		Client client = new Client();
		client.setId("12345");
		client.setName("Boogedy Client");
		project.setClient(client);
		HashMap<String, String> projMeta =  new HashMap<String, String>();
		projMeta.put("transitionLevel", "MLL");
		projMeta.put("optional_1", "OPT 1");
		projMeta.put("optional_2", "OPT 2");
		projMeta.put("optional_3", "OPT 3");
		project.setMetadata(projMeta);

		// Get project stuff
//		Project project = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated").fromId(su, projectId);
		for(ProjectParticipantInstrument ppi : epis)
		{
			ppi.setParticipant(participant);
			ppi.setProject(project);
		}
		
		// Do the call
		IReportHelper irh = HelperDelegate.getReportHelper();
		String combinationReport = "NO";
		irh.addReport(su, reportCode, langCode, epis, overrides, combinationReport, projectId );
		
//		ReportingRequest rr = null;
//		String type = "";
//		HashMap<String, String> hm = null;
//		for(Object rpt : rr.getReports())
//		{
//			if (rpt instanceof IndividualReport)
//			{
//				type = "IR";
//				IndividualReport ir = (IndividualReport)rpt;
//				hm = ir.getLabelData();
//			}
//			else if (rpt instanceof GroupReport)
//			{
//				type = "GR";
//				GroupReport gr = (GroupReport)rpt;
//				hm = gr.getLabelData();
//			}
//			if (type.length() > 0)
//			{
//				System.out.println("Type = " + type);
//				for (Iterator<Map.Entry<String, String>> itr=hm.entrySet().iterator(); itr.hasNext(); )
//				{
//					Map.Entry<String, String> ent = itr.next();
//					System.out.println("    " + ent.getKey() + "=" + ent.getValue());
//				}
//			}
//			else
//				System.out.println("Unknown report type");
//
//		}

		UnitTestUtils.stop(this);
	}

}
