package com.pdi.listener.portal.flex;

import java.sql.Connection;
import java.util.ArrayList;

//import com.pdi.data.abyd.dto.reportInput.ReportInputDTO;
import com.pdi.data.abyd.helpers.common.ImportExportDataHelper;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
//import com.pdi.data.abyd.helpers.reportInput.ReportInputDataHelper;
import com.pdi.data.constants.V2InstrumentConstants;
import com.pdi.data.dto.Participant;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.util.RequestStatus;
import com.pdi.data.v2.helpers.*;
//import com.pdi.data.v2.util.V2WebserviceUtils;
import com.pdi.data.v3.dto.CandidateDTO;
import com.pdi.data.v3.dto.ClientDTO;
import com.pdi.data.v3.dto.ClientListDTO;
import com.pdi.data.v3.dto.IndividualDTO;
import com.pdi.data.v3.dto.ParticipantDTO;
import com.pdi.data.v3.dto.ParticipantListDTO;
import com.pdi.data.v3.dto.ParticipantReportListDTO;
import com.pdi.data.v3.dto.ProjectDTO;
import com.pdi.data.v3.dto.ProjectListDTO;
import com.pdi.data.v3.dto.ReportDTO;
import com.pdi.data.v3.dto.SessionUserDTO;
import com.pdi.data.v3.dto.UserDTO;
import com.pdi.guid.GUID;
//import com.pdi.properties.PropertyLoader;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.IndividualReport;

import flex.messaging.FlexContext;
import flex.messaging.FlexSession;

/**
 * PortalFlexService is the communication layer between flex's portal application and v2
 * NOTE: TO keep the UI unchanged there is a go-between used to convert these DTOs to V3 DTOs before returning
 * the data
 * 
 * @author		Gavin Myers
 */
public class PortalFlexServiceV2 {

		//
		// Static data.
		//

		//
		// Static methods.
		//

		//
		// Instance data.
		//

		//
		// Constructors.
		//
		public PortalFlexServiceV2()
		{
			// Does nothing presently
		}

		//
		// Instance methods.
		//

		/////////////////
		// Private methods
		/////////////////

		/**
		 * internal function to see if the user is connected to the session
		 * @return
		 * @throws Exception
		 */
		private boolean isConnected()
		{
			return FlexContext.getFlexSession().getAttribute("user") != null;
		}
		

		/**
		 * internal function to get the user from the session
		 * @return
		 * @throws Exception
		 */
		private SessionUserDTO getSessionUser()
		{
			SessionUserDTO sessionUser = new SessionUserDTO();
			
			FlexSession session = FlexContext.getFlexSession();
			login((String)session.getAttribute("username"), (String)session.getAttribute("password"));
			if(isConnected())
			{
				sessionUser = (SessionUserDTO)session.getAttribute("user");
			}
			else
			{
				sessionUser.setStatus(new RequestStatus(RequestStatus.RS_ERROR, "User Not in Session"));
			}
			
			return sessionUser;
		}
		
		

		/////////////////
		// Public methods
		/////////////////
		
		/**
		 * Method to test that there is a connection to the RPC
		 * NOTE:  I don't think that is correct.  I think it only talks to the Flex session
		 * @return session id string
		 */

		public String testConnection()
		{
			FlexSession session = FlexContext.getFlexSession();
			if(session.getAttribute("id") == null)
			{
				session.setAttribute("id", new GUID().toString());
			}
			
			return session.getAttribute("id").toString();
		}
		

		/**
		 * Log the user into the system, add the user to the session, return user information
		 * @param username
		 * @param password
		 * @return
		 * @throws Exception
		 */
		public UserDTO login(String username, String password)
		{

			SessionUserDTO sessionUser = new SessionUserDTO();
			RequestStatus status = new RequestStatus();
			status.setStatusCode(RequestStatus.RS_OK);
			try {
				com.pdi.data.v2.dto.SessionUserDTO v2SessionUser = new UserDataHelper().getUser(username, password);
				sessionUser.setSessionId(v2SessionUser.getSessionId());
				sessionUser.setId(v2SessionUser.getId());
				//System.out.println("Session id is ... " + sessionUser.getId());
			} catch(Exception e) {
				status.setStatusCode(RequestStatus.RS_ERROR);
				status.setExceptionMessage(sessionUser.getStatus().getExceptionMessage() + " - Possible Invalid Username/Password.");
			}
			sessionUser.setStatus(status);
			FlexSession session = FlexContext.getFlexSession();
			session.setAttribute("user",sessionUser);
			session.setAttribute("username", username);
			session.setAttribute("password", password);
			
			return new UserDTO(sessionUser);
		}
		

		/**
		 * get all clients
		 * @return
		 */
		public ClientListDTO getClients() throws Exception
		{
			com.pdi.data.v2.dto.SessionUserDTO sessionUserV2 = new com.pdi.data.v2.dto.SessionUserDTO();
			SessionUserDTO sessionUser = getSessionUser();
			sessionUserV2.setSessionId(sessionUser.getSessionId());
			sessionUserV2.setId(sessionUser.getId());
			//System.out.println("Session id is ... " + sessionUser.getId());
			
			ClientListDTO clientList = new ClientListDTO();
			ArrayList<ClientDTO> clients = new ArrayList<ClientDTO>();
						
			for(com.pdi.data.v2.dto.ClientDTO clientV2 : new ClientDataHelper().getClients(sessionUserV2)) {
				ClientDTO client = new ClientDTO();
				client.setId(clientV2.getId());
				client.setName(clientV2.getName());
				clients.add(client);
			}
			
			clientList.setClientList(clients);
			return clientList;
		} 
		
		
		/**
		 * get all projects by client
		 * @param client
		 * @return
		 */
		public ProjectListDTO getProjects(ClientDTO client) throws Exception
		{
			com.pdi.data.v2.dto.SessionUserDTO sessionUserV2 = new com.pdi.data.v2.dto.SessionUserDTO();
			SessionUserDTO sessionUser = getSessionUser();
			sessionUserV2.setSessionId(sessionUser.getSessionId());
			sessionUserV2.setId(sessionUser.getId());
			
			com.pdi.data.v2.dto.ClientDTO clientV2 = new com.pdi.data.v2.dto.ClientDTO();
			clientV2.setId(client.getId());
			
			ProjectListDTO projectList = new ProjectListDTO();
			ArrayList<ProjectDTO> projects = new ArrayList<ProjectDTO>();
			
			for(com.pdi.data.v2.dto.ProjectDTO projectV2 : new ProjectDataHelper().getProjects(sessionUserV2,clientV2)) {
				ProjectDTO project = new ProjectDTO();
				project.setName(projectV2.getName());
				project.setTransitionLevel(projectV2.getTransitionLevel());
				project.setTransitionLevelName(projectV2.getTransitionLevelName());
				project.setTPOTTransLevelCurrent(projectV2.getTPOTTransLevelCurrent());
				project.setTPOTTransLevelTarget(projectV2.getTPOTTransLevelTarget());
				project.setId(projectV2.getId());
				project.setParticipantCount(projectV2.getParticipantCount());
				projects.add(project);
			}
			projectList.setProjectList(projects);
			
			return projectList;
		}
		

		/**
		 * get all participants by project
		 * @param project
		 * @return
		 */
		public ParticipantListDTO getParticipants(ProjectDTO project) throws Exception
		{
			//System.out.println("GET PARTICIPANTS!");
			com.pdi.data.v2.dto.SessionUserDTO sessionUserV2 = new com.pdi.data.v2.dto.SessionUserDTO();
			SessionUserDTO sessionUser = getSessionUser();
			sessionUserV2.setSessionId(sessionUser.getSessionId());
			sessionUserV2.setId(sessionUser.getId());
			
			com.pdi.data.v2.dto.ProjectDTO projectV2 = new com.pdi.data.v2.dto.ProjectDTO();
			projectV2.setId(project.getId());
			
			ParticipantListDTO participantList = new ParticipantListDTO();
			ArrayList<ParticipantDTO> participants = new ArrayList<ParticipantDTO>();
			//ParticipantDataHelper participantDataHelper = new ParticipantDataHelper();
			for(Participant participantV2 : HelperDelegate.getParticipantHelper().fromProjectId(HelperDelegate.getSessionUserHelperHelper().createSessionUser(), project.getId())) {
				ParticipantDTO participant = new ParticipantDTO();
				participant.setId(participantV2.getId());
				//System.out.println("SETTING BUSINESS UNIT TO... " + participantV2.getBusinessUnit());
				participant.setBusinessUnit(participantV2.getMetadata().get("OPTIONAL_1"));
				participant.setProjectId(project.getId());
				CandidateDTO candidate = new CandidateDTO();
				IndividualDTO individual = new IndividualDTO();

				individual.setEmail(participantV2.getEmail());
				individual.setFamilyName(participantV2.getLastName());
				individual.setGivenName(participantV2.getFirstName());

				candidate.setIndividual(individual);
				participant.setCandidate(candidate);
				
				participants.add(participant);
			}
			participantList.setParticipantList(participants);
			
			return participantList;
		}
		
		/**
		 * Get a list of reports with participants
		 * @param participantList
		 * @return
		 */
		public ArrayList<ParticipantReportListDTO> getParticipantReportList(ParticipantListDTO participantList) throws Exception {
			if(! isConnected())
			{
				return null;
			}
			
			ArrayList<ParticipantReportListDTO> participantReportList = new ArrayList<ParticipantReportListDTO>();
			//System.out.println("Time to get report list");
			Connection con = null;
			con = AbyDDatabaseUtils.getDBConnection();
			try {
			//Step 1:
			for(ParticipantDTO participant : participantList.getParticipantList())
			{
				
				String participantId = participant.getId();
				String projectId = participant.getProjectId();
				ArrayList<ReportDTO> reports = new ArrayList<ReportDTO>();
				
				long dnaId = new ImportExportDataHelper().getDnaFromV2(con, projectId, participantId);
				
				ParticipantDataHelper participantDataHelper = new ParticipantDataHelper();
				
				IndividualReport ir = participantDataHelper.getIndividualReportScoreData(participantId);
				
				//if(dnaId == -1)
				//{
					//Not an abyd project
				{
				ReportDTO report = new ReportDTO();
				report.setReportId(200);
				report.setReportName("TLT Individual Report");
				report.setReportCode(ReportingConstants.REPORT_CODE_TLT_INDIVIDUAL_SUMMARY);
				reports.add(report);
				}
				
				{
				ReportDTO report = new ReportDTO();
				report.setReportId(201);
				report.setReportName("TLT Group Detail Report");
				report.setReportCode(ReportingConstants.REPORT_CODE_TLT_GROUP_DETAIL);
				reports.add(report);
				}
				
				{
				ReportDTO report = new ReportDTO();
				report.setReportId(202);
				report.setReportName("TLT Individual Report");
				report.setReportCode(ReportingConstants.REPORT_CODE_TLT_KF_INDIVIDUAL_SUMMARY);
				reports.add(report);
				}
				
				if(dnaId != -1) {
				{
					ReportDTO report = new ReportDTO();
					report.setReportId(104);
					report.setReportName("Development Dashboard Report ");
					report.setReportCode(ReportingConstants.REPORT_CODE_ABYD_DEVELOPMENT_REPORT);
					reports.add(report);
					}
						
					{
					ReportDTO report = new ReportDTO();
					report.setReportId(105);
					report.setReportName("Fit Dashboard Report ");
					report.setReportCode(ReportingConstants.REPORT_CODE_ABYD_FIT_REPORT);
					reports.add(report);
					}
					
					{
					ReportDTO report = new ReportDTO();
					report.setReportId(106);
					report.setReportName("Readiness Dashboard Report");
					report.setReportCode(ReportingConstants.REPORT_CODE_ABYD_READINESS_REPORT);
					reports.add(report);
					}
					
					{
					ReportDTO report = new ReportDTO();
					report.setReportId(107);
					report.setReportName("AbyD Extract Report");
					report.setReportCode(ReportingConstants.REPORT_CODE_ABYD_EXTRACT);
					reports.add(report);
					}
				}
				if(ir.getReportData().get(ReportingConstants.RC_GPI) != null) {
					ReportDTO report = new ReportDTO();
					report.setReportId(51);
					report.setReportName("GPI Graphical Report");
					report.setReportCode(ReportingConstants.REPORT_CODE_GPI_GRAPHICAL);
					reports.add(report);
				}
				
				if(ir.getReportData().get(ReportingConstants.RC_LEI) != null) {
					ReportDTO report = new ReportDTO();
					report.setReportId(52);
					report.setReportName("LEI Graphical Report");
					report.setReportCode(ReportingConstants.REPORT_CODE_LEI_GRAPHICAL);
					reports.add(report);
				}
				
				if(ir.getReportData().get(ReportingConstants.RC_RAVENS_B) != null) {
					ReportDTO report = new ReportDTO();
					report.setReportId(53);
					report.setReportName("Raven's APM Form B");
					report.setReportCode(ReportingConstants.REPORT_CODE_RAV_B_GRAPHICAL);
					reports.add(report);
				}
				ManualEntryDataHelper manualEntryDataHelper = new ManualEntryDataHelper();
				if(manualEntryDataHelper.getAdaptV2Answers(participantId, V2InstrumentConstants.CHQ_V2_ID) != null) {
					ReportDTO report = new ReportDTO();
					report.setReportId(54);
					report.setReportName("CHQ Detail Report");
					report.setReportCode(ReportingConstants.REPORT_CODE_CHQ_DETAIL);
					reports.add(report);
				}
				
				//if(ir.getReportData().get(ReportingConstants.RC_WG_E) != null) {
				//	ReportDTO report = new ReportDTO();
				//	report.setReportId(12);
				//	report.setReportName("Watson-Glaser E Graphical Report");
				//	report.setReportCode("WGE_DETAIL_REPORT");
				//	reports.add(report);
				//}
				
				//System.out.println("The WGE is ... " + ir.getReportData().get(ReportingConstants.RC_WG_E));
				if(ir.getReportData().get(ReportingConstants.RC_WG_E) != null) {
					ReportDTO report = new ReportDTO();
					report.setReportId(12);
					report.setReportName("Watson-Glaser E Graphical Report");
					report.setReportCode(ReportingConstants.REPORT_CODE_WGE_GRAPHICAL);
					reports.add(report);
				}
				/*
				if(ir.getReportData().get(ReportingConstants.RC_RAVENS_SF) != null) {
					ReportDTO report = new ReportDTO();
					report.setReportId(10);
					report.setReportName("Ravens APM Short Form");
					report.setReportCode("RAV_SF_GRAPHICAL_REPORT");
					reports.add(report);
				}
				*/
				//Step 2:
				for(ReportDTO report : reports)
				{
					boolean foundReport = false;
					
					//Step 3:
					for(ParticipantReportListDTO participantReport : participantReportList) {
						if(participantReport.getReport().getReportId() == report.getReportId()) {
							foundReport = true;
							participantReport.getParticipantList().getParticipantList().add(participant);
						}
					}

					if(!foundReport) {
						ParticipantReportListDTO participantReport = new ParticipantReportListDTO();
						participantReport.setReport(report);
						participantReport.getParticipantList().getParticipantList().add(participant);
						participantReportList.add(participantReport);
					}
				}

			}
			} catch(Exception e) {
				e.printStackTrace();
			} finally {
				// close the connection
				if (con != null)
				{
					try  {  con.close();  }
					catch (Exception ex)  {  /* Swallow the exception */  }
					con = null;
				}
			}
			//ArrayList<ParticipantReportListDTO> reportList = com.pdi.data.v3.helpers.ReportDataHelper.getParticipantReportList(getSessionUser(),participantList);
			//System.out.println("RETURNING REPORT LIST");
			

			return participantReportList;
		}
		
}
