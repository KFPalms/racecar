/*
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.listener.portal.groovy 


import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.pdi.data.abyd.dto.reportInput.ReportInputDTO;
import com.pdi.data.abyd.helpers.reportInput.ReportInputDataHelper;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.data.v2.dto.SessionUserDTO;
import com.pdi.data.v2.helpers.ParticipantDataHelper;
import com.pdi.data.v2.util.V2WebserviceUtils;
import com.pdi.listener.portal.dao.DataAccessLayerV2;
import com.pdi.properties.PropertyLoader;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.GroupReport;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.request.ReportingRequest;
import com.pdi.string.StringUtils;
import com.pdi.xml.XMLUtils;

import com.pdi.data.abyd.util.AbyDExtractHelper;


public class ABYD_EXTRACT
{ 
	private static ReportingRequest reportRequest;
	private static Node reportNode;
	private static SessionUserDTO sessionUser;
	private DataAccessLayerV2 dataAccessLayer;

	/*
	 * generate
	 */
	public void generate()
	{
// THIS CODE IS NOT USED.. HAS BEEN MOVED TO THE REPORT HELPER
		//println "Start ABYD_EXTRACT data extraction...";	
		// Prepare for later V2 access (needed for participant name data
		// fetch... but put the session creation in the loop)
		String username = PropertyLoader.getProperty("com.pdi.listener.portal.application", "v2admin.username");
		String password = PropertyLoader.getProperty("com.pdi.listener.portal.application", "v2admin.password");
		V2WebserviceUtils v2web = new V2WebserviceUtils();
		
		// Prepare for db access
		Connection con = null;
		con = AbyDDatabaseUtils.getDBConnection();
		try {
		long dnaId = 0;
		
		String reportCode = reportNode.getAttributes().getNamedItem("code").getTextContent();
		NodeList participants = XMLUtils.getElements(reportNode, "Participants/Participant");
		//System.out.println("reportCode=" + reportCode);
		//System.out.println("participants list length=" + participants.getLength());

		// Get the group data and set up the gr object
		//println "Get Group data...";
		String jobId = participants.item(0).getAttributes().getNamedItem("projectId").getTextContent();
		//println "JobId=" + jobId;
		GroupReport gr = AbyDExtractHelper.getGroupData(con, jobId);
		if (gr == null)
		{
			println "No DNA data for project " + jobId;
			return null;
		}
		// get the DNA ID
		dnaId = Long.parseLong(gr.getDisplayData("DNA_ID"));
		//println "DNA_ID=" + dnaId;
		
		gr.setReportCode(ReportingConstants.REPORT_CODE_ABYD_EXTRACT);
		gr.setReportType(ReportingConstants.REPORT_TYPE_GROUP_EXTRACT);
		
		//Add the client competency structure to the GR
		//println "Get cc/rm structure";
		gr = AbyDExtractHelper.addClientCompetencyStructure(con, gr, dnaId);
		
		
		// Get participant information
		for(Node participant : participants)
		{
			String participantId = participant.getAttributes().getNamedItem("participantId").getTextContent();
			//println "   part=" + participantId;	
			
			// Get the score data (also creates the IR)
			//println "      Get instrument score data";
			IndividualReport ir = new ParticipantDataHelper().getIndividualReportScoreData(participantId);
			////For testing, don't get the scores for now
			//IndividualReport ir = new IndividualReport();
			
			// Put in the Part ID and get the participant name
			//println "      Get part name info";
			SessionUserDTO sessionUser = v2web.registerUser(username, password);
			ir = AbyDExtractHelper.addPartInfo(sessionUser, ir, participantId);

			// Get the TLT and RI (Dashboard) report data
			// This also gets the dLCI (put out in IG data)
			//println "      Get TLT & RI";
			ir = AbyDExtractHelper.addDRIandTLT(ir, dnaId, participantId);

			// Get the client competencies
			//println  "      Get CC/RM scores";
			ir = AbyDExtractHelper.addClientCompetencyScores(con, ir, participantId, dnaId);
			
			// Get the IR stuff
			//println "      Get IR";
			ir = AbyDExtractHelper.addIGData(con, ir, participantId, dnaId);
			
			// Get the EG stuff
			//println "      Get EG";
			ir = AbyDExtractHelper.addEGData(con, ir, participantId, dnaId);
			
			ReportInputDTO reportInput = new ReportInputDataHelper().getReportInput(participantId, dnaId);
			ir.setName(reportInput.getParticipantName() + "_" + reportInput.getDnaName() + "_" + reportInput.getClientName() + "_" + reportCode + ".xls");

			gr.getIndividualReports().add(ir);

			String fileName = StringUtils.removeSpecialCharacters(
					reportInput.getClientName() + " " +
					new SimpleDateFormat("ddMMMyyyy").format(new java.util.Date()) + " ");
			gr.setName(fileName + "ABYD EXTRACT.xls");
		}

		//println "ABYD_EXTRACT data extraction complete";	
		if(gr.getIndividualReports().size() > 0) {
			reportRequest.addReport(gr);
		}
		} catch (Exception e) {
			
		e.printStackTrace();
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)
				{
				}
				con = null;
			}
		}
	}
	
}