/**
 * Copyright (c) 2011, 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.listener.portal.delegated.helpers;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import org.apache.poi.POIXMLDocument;
import org.apache.poi.POIXMLProperties;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdi.data.dto.ProjectParticipantInstrument;
import com.pdi.data.dto.Language;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.dto.Text;
import com.pdi.data.dto.TextGroup;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.IReportHelper;
import com.pdi.data.helpers.interfaces.ITextHelper;
import com.pdi.listener.portal.delegated.helpers.reports.AbyDEvalGuide;
import com.pdi.listener.portal.delegated.helpers.reports.AbyDIntGridReview;
import com.pdi.listener.portal.delegated.helpers.reports.AbyDIntGridSnapshot;
import com.pdi.listener.portal.delegated.helpers.reports.AbyDReports;
import com.pdi.listener.portal.delegated.helpers.reports.AlrDorTReport;
import com.pdi.listener.portal.delegated.helpers.reports.AlrExtract;
import com.pdi.listener.portal.delegated.helpers.reports.AssessmentExtract;
import com.pdi.listener.portal.delegated.helpers.reports.ChqDetailReport;
import com.pdi.listener.portal.delegated.helpers.reports.FinexDetailReport;
import com.pdi.listener.portal.delegated.helpers.reports.GpiGraphicalReport;
import com.pdi.listener.portal.delegated.helpers.reports.KfalpReports;
import com.pdi.listener.portal.delegated.helpers.reports.LeiGraphicalReport;
import com.pdi.listener.portal.delegated.helpers.reports.RavBGraphicalReport;
import com.pdi.listener.portal.delegated.helpers.reports.RiskReport;
import com.pdi.listener.portal.delegated.helpers.reports.TltReports;
import com.pdi.listener.portal.delegated.helpers.reports.WgeGraphicalReport;
import com.pdi.properties.PropertyLoader;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.ExtractData;
import com.pdi.reporting.report.ExtractItem;
import com.pdi.reporting.report.ExtractParams;
import com.pdi.reporting.report.GroupReport;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.Report;
import com.pdi.reporting.report.ReportData;
import com.pdi.reporting.report.ReportDataException;
import com.pdi.reporting.request.ReportingRequest;
import com.pdi.reporting.response.ReportingResponse;


//TODO Re-factor this by taking the "buildPoiXls..." methods out to protected classes.  This will
//     require the callers to instantiate that class, but de-clutters this class something fierce
public class ReportHelper implements IReportHelper
{
	private static final Logger log = LoggerFactory.getLogger(ReportHelper.class);
	//
	// Static data.
	//
	private static int MAX_CELL_SIZE = 32700;	// Actual max is 32767, but we want some headroom
	private static String TRUNC_MSG = "... <Rest of field truncated>";
	
	private static final String H_NAME = "NAME";
	private static final String H_DLCI = "dLCI";
	
	private  static ArrayList<String> DRIVER_KEY_ORDER = new ArrayList<String>();
	static {
		DRIVER_KEY_ORDER.add("STRC");
		DRIVER_KEY_ORDER.add("CHAL");
		DRIVER_KEY_ORDER.add("POWR");
		DRIVER_KEY_ORDER.add("COLL");
		DRIVER_KEY_ORDER.add("BALA");
		DRIVER_KEY_ORDER.add("INDY");
	}


	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private ReportingRequest reportingRequest = new ReportingRequest();
	private HashMap<String, GroupReport> groupReports = new HashMap<String, GroupReport>();

	//
	// Constructors.
	//

	//
	// Instance methods.
	//


	/*
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IReportHelper#addReport(com.pdi.data.dto.SessionUser, java.lang.String, com.pdi.data.dto.ProjectParticipantInstrument, java.util.HashMap)
	 */
	@Override
	public void addReport(SessionUser session, String reportCode,
			ProjectParticipantInstrument epi, HashMap<String, String> overrides, String hybridReport) {
		return;
	}


	/*
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IReportHelper#generate(com.pdi.data.dto.SessionUser)
	 */
	@Override
	public ReportingResponse generate(SessionUser session) throws Exception {
		return reportingRequest.generateReports();
	}
	
	/*
	 * Adding this method so that I can get the reports so that I can implement POI in the Portal Servlet.... 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IReportHelper#generate(com.pdi.data.dto.SessionUser)
	 */
	@Override
	public ReportingRequest getReportingRequest() throws Exception {
		return reportingRequest;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IReportHelper#addReport(com.pdi.data.dto.SessionUser, java.lang.String, java.lang.String, java.util.ArrayList, java.util.HashMap)
	 */
	@Override
	public void addReport(SessionUser sessionUser,
						  String reportCode,
						  String langCode,
						  ArrayList<ProjectParticipantInstrument> epis,
						  HashMap<String, String> overrides,
						  String hybridReport,
						  String projectName)
	{
		//System.out.println("Starting addReport()... " + reportCode + " in " + langCode);
		
		if(epis == null || epis.size() == 0) {
			log.error(" addReport(): No participant/project (epi) data.  Rptcode={}, projName={}", reportCode, projectName);
			return;
		}
		try {
			// See if GPI, LEI, or CS are missing
			boolean gpi_missing = true;
			boolean lei_missing = true;
			boolean cs_missing = true;
			for (ProjectParticipantInstrument ppi : epis)
			{
				if (ppi.getInstrument().getCode().equals(ReportingConstants.IC_GPI_SHORT) ||
					ppi.getInstrument().getCode().equals(ReportingConstants.IC_GPI) )
				{
					gpi_missing = false;
				}
				if (ppi.getInstrument().getCode().equals(ReportingConstants.IC_LEI) )
				{
					lei_missing = false;
				}
				if (ppi.getInstrument().getCode().equals(ReportingConstants.IC_CS) ||
					ppi.getInstrument().getCode().equals(ReportingConstants.IC_CS2) )
				{
					cs_missing = false;
				}
			}
			//System.out.println("Missing check: gpiMissing=" + gpi_missing + ", leiMissing=" + lei_missing + ", cs=" + cs_missing);
			/*
			 * NOTE: FOR individual instrument reports, will only be in english for 
			 * initial a by d release.  They won't have any text group associated.  
			 * We'll use the "fake one" for now, and add the text groups for these
			 * reports in subsequent releases.
			 * 
			 */
			
			// Get the labels
			String textGroupCode = ReportingConstants.reportRequestMap.get(reportCode).getTextGroupId();
			TextGroup tg = null;
			if (textGroupCode == null)
			{
				// Make a fake one
				tg = new TextGroup("NoTextCode", langCode);
			}
			else
			{
				// it is real... fill the TextGroup
				ITextHelper helper = HelperDelegate.getTextHelper(PropertyLoader.getProperty("com.pdi.data.v2.application","datasource.v2"));
				tg = helper.fromGroupCodeLanguageCodeWithDefault(sessionUser, textGroupCode, langCode);
			}
			// Add the language code (we always to put it out)
			Language langObj = new Language("0", langCode, langCode+"-name", langCode+"-transName");
			tg.getLabelMap().put("LANG_CODE", new Text(0, "LANG_CODE", langObj, langCode));
			
			// Make the IR object and set any missing stuff
			//System.out.println("Make IR object");

			IndividualReport ret = new IndividualReport();
			//for viaEDge + TLT individual report report ui toggles + future combination reports
			ret.getDisplayData().put("HYBRID_REPORT", hybridReport); 
			if(projectName != null){
				ret.addDisplayData("PROJECT", projectName);
			}
			if(gpi_missing)
			{
				//ret.addScriptedData("STATUS", "NOT_FINISHED");
				ret.addDisplayData("GPI_MISSING", "MISSING"); 
			}
			if(lei_missing)
			{
				//ret.addScriptedData("STATUS", "NOT_FINISHED");
				ret.addDisplayData("LEI_MISSING", "MISSING"); 
			}
			if(cs_missing)
			{
				//ret.addScriptedData("STATUS", "NOT_FINISHED");
				ret.addDisplayData("CS_MISSING", "MISSING"); 
			}
			
			//This is used in every report.
			java.util.Date date = new java.util.Date();
			SimpleDateFormat sdf;
			if(!hybridReport.equals("YES")){
				sdf = new SimpleDateFormat("dd MMMM yyyy");
			}else{
				sdf = new SimpleDateFormat("dd MMM yyyy");
			}
			ret.getDisplayData().put("TODAY_DATE", sdf.format(date));
			ret.getDisplayData().put("DATE", sdf.format(date));
			
			ret.setReportCode(reportCode);
			
			/*
			 * 1 - is this TLT or AbyD? or one of the individual instrument reports??
			 * 
			 * 2 - the AbyD Transition Scales report is really a TLT report, so let's 
			 * make it go there..... 
			 * 
			 */
			
			// Generate data for a number of TLT reports...
			// A by D Transition Scales is effectively the same report as the Group Detail
			// report; requires most of the same data and so is included in the TLT grouping.
			if(reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_TLT_GROUP_DETAIL ) 
					// || reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_TLT_GROUP_SUMMARY)  // Report not valid...
					|| reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_TLT_KF_INDIVIDUAL_SUMMARY)
					|| reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_TLT_INDIVIDUAL_SUMMARY)
					|| reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_TLT_EXTRACT)
					|| reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_TLT_EXTRACT_RAW)
					|| reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_TRANSITION_SCALES)){
				// this is a TLT report... so do TLT stuff... 
				IndividualReport ir = ret.clone();
				ir.setReportCode(reportCode);
				ir.getLabelData().putAll(tg.getLabelHashMap());							
				if(reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_TLT_EXTRACT_RAW))
				{
					ir.setReportType(ReportingConstants.REPORT_TYPE_GROUP_EXTRACT);
				} else {
					ir.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);
				}

				log.debug("Getting TLT Report Data");
				new TltReports().getTLTReportData(ir,
												  groupReports,
												  reportingRequest,
												  epis,
												  overrides,
												  sessionUser,
												  tg,
												  langCode);
				log.debug("Finished Getting TLT Report Data");
			}

			// Generate data for a number of A by D reports (and extract)
			if(reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_DEVELOPMENT_REPORT ) 
					|| reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_FIT_REPORT)
					|| reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_READINESS_REPORT)
					|| reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_CUSTOM_REPORT)
					|| reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_CUSTOM_MODEL_REPORT)
					|| reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_TARGET_ORG)
					|| reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_TARGET_PPT)
					|| reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_EXTRACT)
					|| reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_ANALYTICS_EXTRACT)
					|| reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_RAW_EXTRACT)
					|| reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ALR_EXTRACT)
					|| reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_KFALP_RAW_EXTRACT)
					)
			{
				boolean addBreaks = false;
				if (   reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_DEVELOPMENT_REPORT ) 
					|| reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_FIT_REPORT)
					|| reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_READINESS_REPORT)
					|| reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_CUSTOM_REPORT)
					|| reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_TARGET_ORG)
					|| reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_TARGET_PPT) )
				{
					addBreaks = true;
				}

				// this is an AbyD report... so do AbyD stuff... 
				//System.out.println("AbyD report data collection");
				IndividualReport ir = ret.clone();
				ir.setReportCode(reportCode);
				if(reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_EXTRACT) ||
				   reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_ANALYTICS_EXTRACT) ||
				   reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_RAW_EXTRACT)	||
				   reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ALR_EXTRACT) ||
				   reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_KFALP_RAW_EXTRACT))
				{
					ir.setReportType(ReportingConstants.REPORT_TYPE_GROUP_EXTRACT);
				} else {
					ir.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL); 
				}
				
				log.debug("This is an ABYD report: {}, Report type: {}, Add breaks? {}", reportCode, ir.getReportType(), addBreaks);

//				if  ( ! reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ALR_EXTRACT))
//				{
//					// "Regular" AbyD report (everything but ALR Extract
//					new AbyDReports().getAbyDReportData(ir,
//							groupReports,
//							reportingRequest,
//							epis,
//							sessionUser,
//							tg,
//							langCode,
//							addBreaks);
//					//System.out.println("Back from getAbyDReportData(), ir: " + ir);
//				} else {
//					// ALR data extract/report
//					new AlrExtract().getAlrExtractData(ir,
//							groupReports,
//							reportingRequest,
//							epis,
//							sessionUser,
//							tg,
//							langCode,
//							addBreaks);
//				}
				// Re-factored to be more positive (and allow additional special circumstances)
				if  (reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ALR_EXTRACT))
				{
					// ALR data extract/report
					new AlrExtract().getAlrExtractData(ir,
							groupReports,
							reportingRequest,
							epis,
							sessionUser,
							tg,
							langCode,
							addBreaks);
				}
				else if(reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_KFALP_RAW_EXTRACT))
				{
						new KfalpReports().getRawReportData(
								groupReports,
								reportingRequest,
								epis);
				} else {
					// "Regular" AbyD report (everything but ALR and Kfalp Extract)
					log.debug("Getting AbyDReportData for : {}", reportCode);
					new AbyDReports().getAbyDReportData(ir,
							groupReports,
							reportingRequest,
							epis,
							sessionUser,
							tg,
							langCode,
							addBreaks);
					log.debug("Finished getting AbyDReportData for {}", reportCode);
					//System.out.println("Back from getAbyDReportData(), ir: " + ir);
				}
			}

			// Generate additional data for the Assessment extract
			if(reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ASSESSMENT_EXTRACT )) {
				// this is an Assessment extract "report"... 
				IndividualReport inp = ret.clone();
				inp.getLabelData().putAll(tg.getLabelHashMap());

				IndividualReport ir = new AssessmentExtract().getAssessmentExtractData(inp, epis, langCode);
				if (ir != null)
				{
					ir.setReportCode(reportCode);
					ir.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL_EXTRACT);
					reportingRequest.addReport(ir);
				}

				//note:   Reporting Request. line 217.... 
				// that's where the xls is created, old school.
				// It uses the ABYD_EXTRACT.html (or whatever one I build for this)
				// and returns a ByteArrayOutputStream os that it puts into the report: report.setData(os)  
				// new school, doing POI, i think I need to see what's returned, and 
				// see if I can use the portal servlet to shoot that out, or if, 
				// at this point, i would just set off the POI stuff, and generate from 
				// there... not sure yet.... 
						// FileOutputStream fileOut = new FileOutputStream("poi-test.xls");
				// could otherwise put it in PortalServlet, before the reporting repsponse generate call.....
				// which may be better... 
				
			}

			// Generate data for the CHQ
			if(reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_CHQ_DETAIL ))
			{
				// this is a CHQ detail report... 
				////this.doCHQDetailRpt(ret, epis, overrides, sessionUser, tg);
				ret = new ChqDetailReport().getCHQDetailData(ret, epis, tg);
				
				if (ret != null)
				{
					ret.setReportCode(reportCode);
					ret.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);
					//System.out.println("RH: adding report to reporting request : ret: " + ret.getDisplayData().get("PARTICIPANT_NAME") + "  "  + ret.getDisplayData().get("PROJECT_NAME"));
					reportingRequest.addReport(ret);
				}
			}	
			
			// Generate data for the Financial Exercise
			if(reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_FINEX_DETAIL ))
			{
				// this is a finex  report... 
				IndividualReport ir = ret.clone();
				ir.getLabelData().putAll(tg.getLabelHashMap());
				ir.setReportCode(reportCode);
				ir.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);
				
				ArrayList<Report> rptList = new FinexDetailReport().getFinexDetailData(ir, epis);
				if (rptList != null)
				{
					if (reportingRequest.getReports() == null)
						reportingRequest.setReports(new ArrayList<Report>());
					reportingRequest.getReports().addAll(rptList);
				}
			}	
			
			// Generate data for the GPI
			if(reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_GPI_GRAPHICAL ))
			{
				// this is a GPI graphical report... 
				IndividualReport ir = ret.clone();
				ir.getLabelData().putAll(tg.getLabelHashMap());
				ir.setReportCode(reportCode);
				ir.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);
				
				ArrayList<Report> rptList = new GpiGraphicalReport().getGpiGraphicalData(ir, epis);
				if (rptList != null)
				{
					if (reportingRequest.getReports() == null)
						reportingRequest.setReports(new ArrayList<Report>());
					reportingRequest.getReports().addAll(rptList);
				}
			}			
			
			// Generate data for the LEI
			if(reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_LEI_GRAPHICAL ))
			{
				// this is a LEI graphical report... 
				IndividualReport ir = ret.clone();
				ir.getLabelData().putAll(tg.getLabelHashMap());
				ir.setReportCode(reportCode);
				ir.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);

				ArrayList<Report> rptList = new LeiGraphicalReport().getLEIGraphicalData(ir, epis);
				if (rptList != null)
				{
					if (reportingRequest.getReports() == null)
						reportingRequest.setReports(new ArrayList<Report>());
					reportingRequest.getReports().addAll(rptList);
				}
			}

			// Generate data for the Raven's Form B
			if(reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_RAV_B_GRAPHICAL ))
			{
				// Get rav2 flag from overrides
				String flg = overrides.get("hasRv2");
				boolean isRv2 = false;
				if (flg != null && flg.equals("yes"))
				{
					log.debug("Found RV2 instrument");
					isRv2 = true;
				}
				// this is a GPI graphical report... 
				IndividualReport ir = ret.clone();
				ir.getLabelData().putAll(tg.getLabelHashMap());
				ir.setReportCode(reportCode);
				ir.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);
				log.debug("About to get Data for Ravens Graphical Report");
				ArrayList<Report> rptList = new RavBGraphicalReport().getRavBGraphicalData(ir, epis, isRv2);
				if (rptList != null)
				{
					if (reportingRequest.getReports() == null)
						reportingRequest.setReports(new ArrayList<Report>());
					reportingRequest.getReports().addAll(rptList);
				}
			}
			
			// Generate data for the Watson-Glaser Form E
			if(reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_WGE_GRAPHICAL ))
			{
				IndividualReport ir = ret.clone();
				ir.getLabelData().putAll(tg.getLabelHashMap());
				ir.setReportCode(reportCode);
				ir.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);

				ArrayList<Report> rptList = new WgeGraphicalReport().getWgeGraphicalData(ir, epis);
				if (rptList != null)
				{
					if (reportingRequest.getReports() == null)
						reportingRequest.setReports(new ArrayList<Report>());
					reportingRequest.getReports().addAll(rptList);
				}
			}
			
			// Generate the data for the ALR Driver Interp
			if(reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ALR_DRIVERS )	)
			{
				IndividualReport ir = ret.clone();
				ir.setReportCode(reportCode);
				ir.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);

				Report rpt = new AlrDorTReport().getAlrDorTData(ir, epis, "D", ReportingConstants.SRC_KFP);
				if (rpt != null)
				{
					if (reportingRequest.getReports() == null)
						reportingRequest.setReports(new ArrayList<Report>());
					reportingRequest.getReports().add(rpt);
				}
			}
			if(reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ALR_TRAITS )	)
			{
				IndividualReport ir = ret.clone();
				ir.setReportCode(reportCode);
				ir.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);

				Report rpt = new AlrDorTReport().getAlrDorTData(ir, epis, "T", ReportingConstants.SRC_KFP);
				if (rpt != null)
				{
					if (reportingRequest.getReports() == null)
						reportingRequest.setReports(new ArrayList<Report>());
					reportingRequest.getReports().add(rpt);
				}
			}

			if(reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_KF4D_TRAITS )	)
			{
				IndividualReport ir = ret.clone();
				ir.setReportCode(reportCode);
				ir.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);

				Report rpt = new AlrDorTReport().getAlrDorTData(ir, epis, "T", ReportingConstants.SRC_KF4D);
				if (rpt != null)
				{
					if (reportingRequest.getReports() == null)
						reportingRequest.setReports(new ArrayList<Report>());
					reportingRequest.getReports().add(rpt);
				}
			}
			if(reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_KF4D_DRIVERS )	)
			{
				IndividualReport ir = ret.clone();
				ir.setReportCode(reportCode);
				ir.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);

				Report rpt = new AlrDorTReport().getAlrDorTData(ir, epis, "D", ReportingConstants.SRC_KF4D);
				if (rpt != null)
				{
					if (reportingRequest.getReports() == null)
						reportingRequest.setReports(new ArrayList<Report>());
					reportingRequest.getReports().add(rpt);
				}
			}
			if(reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_KF4D_RISK )	)
			{
				IndividualReport ir = ret.clone();
				ir.setReportCode(reportCode);
				ir.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);

				Report rpt = new RiskReport().getRiskFactorsData(ir, epis);
				if (rpt != null)
				{
					if (reportingRequest.getReports() == null)
						reportingRequest.setReports(new ArrayList<Report>());
					reportingRequest.getReports().add(rpt);
				}
			}

			// AbyD Eval Guide
			if(reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_EVAL_GUIDE ))
			{
				ret = new AbyDEvalGuide().getEvalGuideRptData(ret, epis, tg, overrides);
				ret.setReportCode(reportCode);
				ret.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);
				reportingRequest.addReport(ret);
			}	
			
			// AbyD IntGrid Review
			if(reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_INT_GRID_REVIEW ))
			{
				log.debug("Calling getIntGridData()");
				ret = new AbyDIntGridReview().getIntGridData(ret, epis, tg, overrides);
				ret.setReportCode(reportCode);
				ret.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);
				log.debug("Calling addReport");
				reportingRequest.addReport(ret);
			}	
			
			// AbyD IntGrid SnapShot
			if(reportCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_INT_GRID_SNAPSHOT ))
			{
				ret = new AbyDIntGridSnapshot().getIntGridSnapshotData(ret, epis, tg, overrides);
				ret.setReportCode(reportCode);
				ret.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);
				reportingRequest.addReport(ret);
			}
			
			
			//System.out.println("End ReportHelper.addReport()");

		} catch (Exception e) {
			System.out.println("addReport error: rptCode=" + reportCode + ", langCode=" + langCode + ".  Msg=" + e.getMessage());
			e.printStackTrace();
		}		
	}


	/*
	 * getReportingRequestForDebug - Name is self explanatory
	 * 
	 * THIS METHOD SHOULD NEVER BE USED IN "REGULAR" CODE
	 * 
	 * (non-Javadoc)
	 * @see com.pdi.data.helpers.interfaces.IReportHelper#getReportingRequestForDebug()
	 */
	public ReportingRequest getReportingRequestForDebug()
	{
		return reportingRequest;
	}


	/**
	 * creating xls for the Assessment Extract, 
	 * using the Apache POI API
	 * 
	 * @param IndividualReport ir
	 * @return Workbook -- the created excel workbook
	 */
	public Workbook buildPoiXlsForAssessmentExtract(IndividualReport ir)
		throws ReportDataException
	{
		//System.out.println("entering... buildPoiXlsForAssessmentExtract....");
		Workbook workbook = new XSSFWorkbook();
		
		// add watermark to title field
		POIXMLProperties xmlProps = ((POIXMLDocument) workbook).getProperties();
		POIXMLProperties.CoreProperties coreProps = xmlProps.getCoreProperties();
		coreProps.setTitle(ReportingConstants.WATERMARK);
		
		DataFormat fmt = workbook.createDataFormat();	// Gonna use later

		Sheet worksheet = workbook.createSheet("Worksheet");
		// NOTE:  setting individual column widths makes the columns disappear,  
		// even if I set them AFTER i've created the default width.  
		worksheet.setDefaultColumnWidth(38);
		
		//Create the bold font
		Font boldFont = workbook.createFont();
		boldFont.setBoldweight(Font.BOLDWEIGHT_BOLD);

		// create all the cell styles....
		CellStyle cellStyleString = workbook.createCellStyle();
		cellStyleString.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		
		CellStyle cellStyleStringBoldRight = workbook.createCellStyle();  // assessment date:
		cellStyleStringBoldRight.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		cellStyleStringBoldRight.setFont(boldFont);
		cellStyleStringBoldRight.setAlignment(CellStyle.ALIGN_RIGHT);
				
		CellStyle cellStyleHeaderBlue = workbook.createCellStyle();  // instrument header 
		cellStyleHeaderBlue.setFillForegroundColor(IndexedColors.CORNFLOWER_BLUE.getIndex());
		cellStyleHeaderBlue.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyleHeaderBlue.setFont(boldFont);
		cellStyleHeaderBlue.setBorderLeft(CellStyle.BORDER_MEDIUM);
		cellStyleHeaderBlue.setLeftBorderColor(IndexedColors.BLUE.getIndex());
		cellStyleHeaderBlue.setBorderTop(CellStyle.BORDER_MEDIUM);
		cellStyleHeaderBlue.setTopBorderColor(IndexedColors.BLUE.getIndex());
		cellStyleHeaderBlue.setBorderRight(CellStyle.BORDER_MEDIUM);
		cellStyleHeaderBlue.setRightBorderColor(IndexedColors.BLUE.getIndex());
		
		CellStyle cellStyleStringBlueBorder = workbook.createCellStyle();  // scale names
		cellStyleStringBlueBorder.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		cellStyleStringBlueBorder.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyleStringBlueBorder.setBorderLeft(CellStyle.BORDER_MEDIUM);
		cellStyleStringBlueBorder.setLeftBorderColor(IndexedColors.BLUE.getIndex());
		cellStyleStringBlueBorder.setBorderTop(CellStyle.BORDER_THIN);
		cellStyleStringBlueBorder.setTopBorderColor(IndexedColors.BLUE.getIndex());
		cellStyleStringBlueBorder.setBorderBottom(CellStyle.BORDER_THIN);
		cellStyleStringBlueBorder.setBottomBorderColor(IndexedColors.BLUE.getIndex());

		CellStyle cellStyleGreen = workbook.createCellStyle();  // scale ratings
		cellStyleGreen.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
		cellStyleGreen.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyleGreen.setBorderLeft(CellStyle.BORDER_MEDIUM);
		cellStyleGreen.setLeftBorderColor(IndexedColors.GREEN.getIndex());
		cellStyleGreen.setBorderTop(CellStyle.BORDER_THIN);
		cellStyleGreen.setTopBorderColor(IndexedColors.GREEN.getIndex());
		cellStyleGreen.setBorderBottom(CellStyle.BORDER_THIN);
		cellStyleGreen.setBottomBorderColor(IndexedColors.GREEN.getIndex());
		cellStyleGreen.setBorderRight(CellStyle.BORDER_MEDIUM);
		cellStyleGreen.setRightBorderColor(IndexedColors.GREEN.getIndex());
		cellStyleGreen.setDataFormat(fmt.getFormat("0.0"));
		
		// Basic Data
		// index from 0,0... cell A1 is cell(0,0)
		Row row1 = worksheet.createRow((short) 0);
				
		Cell cellR1B2 = row1.createCell(1); // R1B1 ()
		cellR1B2.setCellValue(ir.getDisplayData("ORGANIZATION"));
		cellR1B2.setCellStyle(cellStyleString);

		Row row2 = worksheet.createRow((short) 1);  //R2B2
		Cell cellR2B2 = row2.createCell(1);
		cellR2B2.setCellValue(ir.getDisplayData("PROJECT_NAME"));
		cellR2B2.setCellStyle(cellStyleString);
		
		Row row3 = worksheet.createRow((short) 2);  //R3B3
		Cell cellR3B3 = row3.createCell(1);
		cellR3B3.setCellValue(ir.getDisplayData("PARTICIPANT_NAME"));
		cellR3B3.setCellStyle(cellStyleString);

		
		Row row5 = worksheet.createRow((short) 4);  //R5B5
		Cell cellR5B5 = row5.createCell(1);
		cellR5B5.setCellValue("Assessment Date:  ");
		cellR5B5.setCellStyle(cellStyleStringBoldRight);
		Cell cellR5C2 = row5.createCell(2);			//R5C5
		cellR5C2.setCellValue(ir.getDisplayData("ASSESSMENT_DATE"));

		
		// will do an if for each rd 
		// in this order: gpi, rdi %, rv, wge, lei 
		
		ReportData rd = new ReportData();  // just initialize it... 
					
		int rowCounter = 8; // will always start here for loops
		
		if(ir.getReportData().get(ReportingConstants.RC_GPI) == null)
		{
			// no gpi, so note that in report
			// build GPI header....
			//build GPI header....
			Row row8 = worksheet.createRow((short) rowCounter);  //R8
			Cell cellR8B2 = row8.createCell(1);  //R8B2
			cellR8B2.setCellValue("GPI Scales");
			cellR8B2.setCellStyle(cellStyleHeaderBlue);
			Cell cellR8C2 = row8.createCell(2);  //R8C2
			cellR8C2.setCellValue("Rating (PDI 1-5)");
			cellR8C2.setCellStyle(cellStyleHeaderBlue);
				
			rowCounter++;
			
			// do GPI loop, based on the gpi_scales_text_for_assessment_xtract
			// just put in no score.... 			
			for(String s : ReportingConstants.gpi_scales_text_for_assessment_xtract)
			{
				// No scores... Put out empty score data
				Row row = worksheet.createRow((short) rowCounter);
				Cell cell1 = row.createCell(1);
				cell1.setCellStyle(cellStyleStringBlueBorder); 
				cell1.setCellValue(s);	//scale name
				Cell cell2 = row.createCell(2);
				cell2.setCellStyle(cellStyleGreen);
				cell2.setCellValue("");  // scale rating
		          
				rowCounter++;
			}
		}
		else
		{
			//System.out.println("have a gpi, build the xls....");
			// put the gpi in the xls report
			rd = ir.getReportData().get(ReportingConstants.RC_GPI);
			
			if(rd.getScriptedData().get("NORM_ERROR") != null)
			{
				String msg = rd.getScriptedData().get("NORM_ERROR") + "   Norms can be set through the TestData application."; 
				System.out.println("ReportDataException:ReportHelper:  " + msg);	// Let the sysout log know
				throw new ReportDataException(msg);
			}
			
			//build GPI header....
			Row row8 = worksheet.createRow((short) rowCounter);  //R8
			Cell cellR8B2 = row8.createCell(1);  //R8B2
			cellR8B2.setCellValue("GPI Scales");
			cellR8B2.setCellStyle(cellStyleHeaderBlue);
			Cell cellR8C2 = row8.createCell(2);  //R8C2
			cellR8C2.setCellValue("Rating (PDI 1-5)");
			cellR8C2.setCellStyle(cellStyleHeaderBlue);
			
			rowCounter++;
			
			// do GPI loop, based on the gpi_scales_for_assessment_xtract
			for(String s : ReportingConstants.gpi_scales_for_assessment_xtract)
			{
				Row row = worksheet.createRow((short) rowCounter);
				Cell cell1 = row.createCell(1);
				cell1.setCellStyle(cellStyleStringBlueBorder); 
				cell1.setCellValue(rd.getDisplayData().get(s));	//scale name
				Cell cell2 = row.createCell(2);
				cell2.setCellStyle(cellStyleGreen);
				// Scale rating value should be numeric
				String ss = rd.getScoreData().get(s);
				if (ss == null || ss.length() < 1)
				{
					cell2.setCellValue("");
				} else {
					cell2.setCellValue(Double.parseDouble(ss));  // scale rating
				}
		          
				rowCounter++;
			}
			
			// blank line
			rowCounter++;
			
			// do GPI RDI
			if(rd.getDisplayData().get("PGPI_RDI") == null)
			{
				Row row = worksheet.createRow((short) rowCounter);  //R8
				cellR8B2.setCellValue("GPI Response Distortion Index (%ile)");
				cellR8B2.setCellStyle(cellStyleHeaderBlue);
				Cell cell2 = row.createCell(2);
				cell2.setCellStyle(cellStyleGreen);
				cell2.setCellValue("N/A");  // scale rating
			}
			else
			{
				Row row = worksheet.createRow((short) rowCounter);  //R8
				Cell cell = row.createCell(1);  //R8B2
				cell.setCellValue("GPI Response Distortion Index (%ile)");
				cell.setCellStyle(cellStyleHeaderBlue);
				cell = row.createCell(2);
				cell.setCellStyle(cellStyleGreen);
				String s = rd.getScoreData().get("PGPI_RDI");
				if (s == null || s.length() < 1)
				{
					cell.setCellValue("");
				}
				else
				{
					cell.setCellValue(Double.parseDouble(s));
				}
				
				rowCounter++;
			}
		}
		
		// blank line
		rowCounter++;
		// blank line
		rowCounter++;

		// DO THE COGS HEADER
		Row row8 = worksheet.createRow((short) rowCounter);  //R8
		Cell cellR8B2 = row8.createCell(1);  //R8B2
		cellR8B2.setCellValue("Cognitive Tests");
		cellR8B2.setCellStyle(cellStyleHeaderBlue);
		Cell cellR8C2 = row8.createCell(2);  //R8C2
		cellR8C2.setCellValue("Rating (PDI 1-5)");
		cellR8C2.setCellStyle(cellStyleHeaderBlue);
		
		rowCounter++;
		
		// DO THE RAVENS
		if(ir.getReportData().get(ReportingConstants.RC_RAVENS_B) == null)
		{
			Row row = worksheet.createRow((short) rowCounter);
			Cell cell1 = row.createCell(1);
			cell1.setCellStyle(cellStyleStringBlueBorder);  
			cell1.setCellValue("Raven's APM  Form B");	//scale name
			Cell cell2 = row.createCell(2);
			cell2.setCellStyle(cellStyleGreen);
			cell2.setCellValue("");  // scale rating
 			rowCounter++;			
		} else {
			//System.out.println("have a RAVB, build the xls....");
			// put the gpi in the xls report
			rd = ir.getReportData().get(ReportingConstants.RC_RAVENS_B);
			if(rd.getScriptedData().get("NORM_ERROR") != null){
				String msg = rd.getScriptedData().get("NORM_ERROR") + "   Norms can be set through the TestData application."; 
				System.out.println("ReportDataException:ReportHelper:  " + msg);	// Let the sysout log know
				throw new ReportDataException(msg);
			}
			
			Row row = worksheet.createRow((short) rowCounter);
			Cell cell1 = row.createCell(1);
			cell1.setCellStyle(cellStyleStringBlueBorder); 
			cell1.setCellValue("Raven's APM  Form B");	//scale name
			Cell cell2 = row.createCell(2);
			cell2.setCellStyle(cellStyleGreen);
			String s = rd.getScoreData().get("RAVENSB_SP_RATING");
			if (s == null || s.length() < 1)
			{
				cell2.setCellValue("");  // blank
			} else {
				cell2.setCellValue(Double.parseDouble(s));  // scale rating
			}
			rowCounter++;
		}
		
		// DO THE WATSON
		if(ir.getReportData().get(ReportingConstants.RC_WG_E) == null)
		{				
			Row row = worksheet.createRow((short) rowCounter);
			Cell cell1 = row.createCell(1);
			cell1.setCellStyle(cellStyleStringBlueBorder);  
			cell1.setCellValue("Watson-Glaser Form E");	//scale name
			Cell cell2 = row.createCell(2);
			cell2.setCellStyle(cellStyleGreen);
			cell2.setCellValue("");  // scale rating
			rowCounter++;			
		} else {
			//System.out.println("have a RAVB, build the xls....");
			// put the gpi in the xls report
			rd = ir.getReportData().get(ReportingConstants.RC_WG_E);
			
			if(rd.getScriptedData().get("NORM_ERROR") != null)
			{
				String msg = rd.getScriptedData().get("NORM_ERROR") + "   Norms can be set through the TestData application."; 
				System.out.println("ReportDataException:ReportHelper:  " + msg);	// Let the sysout log know
				throw new ReportDataException(msg);
			}
			
			Row row = worksheet.createRow((short) rowCounter);
			Cell cell1 = row.createCell(1);
			cell1.setCellStyle(cellStyleStringBlueBorder); 
			cell1.setCellValue("Watson-Glaser Form E");	//scale name
			Cell cell2 = row.createCell(2);
			cell2.setCellStyle(cellStyleGreen);
			String s = rd.getScoreData().get("WGE_CT_SP_RATING");
			if (s == null || s.length() < 1)
			{
				cell2.setCellValue("");
			} else {
				cell2.setCellValue(Double.parseDouble(s));  // scale rating
			}
			rowCounter++;
		}

		// BLANK LINES
		rowCounter++;
		rowCounter++;
		
		// DO THE LEI HEADER
		Row row = worksheet.createRow((short) rowCounter);  //R8
		Cell cell = row.createCell(1);  //R8B2
		cell.setCellValue("LEI data input");
		cell.setCellStyle(cellStyleHeaderBlue);
		cell = row.createCell(2);  //R8C2
		cell.setCellValue("Rating (PDI 1-5)");
		cell.setCellStyle(cellStyleHeaderBlue);
		rowCounter++;
		
		if(ir.getReportData().get(ReportingConstants.RC_LEI) == null)
		{
			//System.out.println("lei is null use the scale names text to build the xls....");
			// do LEI loop
			for(String s : ReportingConstants.lei_scales_text_for_assessment_xtract)
			{
				//System.out.println("s = " + s);
				row = worksheet.createRow((short) rowCounter);
				Cell cell1 = row.createCell(1);
				cell1.setCellStyle(cellStyleStringBlueBorder); 
				cell1.setCellValue(s);	//scale name
				Cell cell2 = row.createCell(2);
				cell2.setCellStyle(cellStyleGreen);
				cell2.setCellValue("");  // scale rating
				rowCounter++;
			}
		} else {
			//System.out.println("have a lei, build the xls....");
			// put the gpi in the xls report
			rd = ir.getReportData().get(ReportingConstants.RC_LEI);
			
			if(rd.getScriptedData().get("NORM_ERROR") != null)
			{
				String msg = rd.getScriptedData().get("NORM_ERROR") + "   Norms can be set through the TestData application."; 
				System.out.println("ReportDataException:ReportHelper:  " + msg);	// Let the sysout log know
				throw new ReportDataException(msg);
			}
			
			// do LEI loop, based on the gpi_scales_for_assessment_xtract
			for(String s : ReportingConstants.lei_scales_for_assessment_xtract)
			{
				row = worksheet.createRow((short) rowCounter);
				Cell cell1 = row.createCell(1);
				cell1.setCellStyle(cellStyleStringBlueBorder); 
				cell1.setCellValue(rd.getDisplayData().get(s));	//scale name
				Cell cell2 = row.createCell(2);
				cell2.setCellStyle(cellStyleGreen);
				String ss = rd.getScoreData().get(s);
				if (ss == null || ss.length() < 1)
				{
					cell2.setCellValue("");
				} else {
					cell2.setCellValue(Double.parseDouble(ss));  // scale rating
				}
				rowCounter++;
			}
		}

		// BLANK LINES
		rowCounter++;
		rowCounter++;

		// Add PDA data here
		// Header
		Row pdaRow = worksheet.createRow((short) rowCounter);  //R8
		Cell pdaCell = pdaRow.createCell(1);  //R8B2
		pdaCell.setCellValue("PDA Heading");
		pdaCell.setCellStyle(cellStyleHeaderBlue);
		pdaCell = pdaRow.createCell(2);  //R8C2
		pdaCell.setCellValue("PDA Data");
		pdaCell.setCellStyle(cellStyleHeaderBlue);
		rowCounter++;

		for(String s : ReportingConstants.pda_predef_tags)
		{
			pdaRow = worksheet.createRow((short) rowCounter);
			Cell cell1 = pdaRow.createCell(1);
			cell1.setCellStyle(cellStyleStringBlueBorder); 
			cell1.setCellValue(ir.getDisplayData().get(s+"_HDG"));	//scale name
			Cell cell2 = pdaRow.createCell(2);
			cell2.setCellStyle(cellStyleGreen);
			String ss = ir.getDisplayData().get(s);
			if (ss == null || ss.length() < 1)
			{
				cell2.setCellValue("");
			} else {
				cell2.setCellValue(ss);
			}
			rowCounter++;
		}
		
		// Put out the custom field heading

		// BLANK LINES
		rowCounter++;
		rowCounter++;

		pdaRow = worksheet.createRow((short) rowCounter);  //R8
		pdaCell = pdaRow.createCell(1);  //R8B2
		pdaCell.setCellValue("Custom Field Name");
		pdaCell.setCellStyle(cellStyleHeaderBlue);
		pdaCell = pdaRow.createCell(2);  //R8C2
		pdaCell.setCellValue("Custom Field Data");
		pdaCell.setCellStyle(cellStyleHeaderBlue);
		rowCounter++;
		
		int cnt = 0;
		
		// do we have custom fields?
		String str = ir.getDisplayData().get("CPDA_COUNT");
		if (str != null)
		{
			// Got a count... convert it
			try
			{
				cnt = Integer.parseInt(str);
			}
			catch (NumberFormatException e)
			{
				//
				System.out.println("Unable to convert " + str + " ti an int... Skipping custom PDA");
				cnt = 0;
			}
		}

		if (cnt == 0)
		{
			// No custom fields... put out placeholder
			pdaRow = worksheet.createRow((short) rowCounter);
			Cell cell1 = pdaRow.createCell(1);
			cell1.setCellStyle(cellStyleStringBlueBorder); 
			cell1.setCellValue("No Custom field data is available");
			Cell cell2 = pdaRow.createCell(2);
			cell2.setCellStyle(cellStyleGreen);
			cell2.setCellValue("");
			rowCounter++;
		}
		else if (cnt > 0)
		{
			// Have custom fields.. put them out
			for(int i=1; i <= cnt; i++)
			{
				pdaRow = worksheet.createRow((short) rowCounter);
				Cell cell1 = pdaRow.createCell(1);
				cell1.setCellStyle(cellStyleStringBlueBorder); 
				cell1.setCellValue(ir.getDisplayData().get("CPDA_" + i + "_CFNAME"));	//scale name
				Cell cell2 = pdaRow.createCell(2);
				cell2.setCellStyle(cellStyleGreen);
				String ss = ir.getDisplayData().get("CPDA_" + i + "_CFVALUE");
				if (ss == null || ss.length() < 1)
				{
					cell2.setCellValue("");
				} else {
					cell2.setCellValue(ss);
				}
				rowCounter++;
			}	// End of for(i....
			}	// End of else for cnt > 0

		return workbook;
	}


	/**
	 * buildPoiXlsForAbyDExtract - Build a spreadsheet for the A by D extract
	 * 
	 * NOTE:  This logic assumes that all of the participants are in the same
	 * 		  project/DNA.  Stuff with GRP tags is picked up from the GroupReport
	 * 		  object and so the client project and DNA info has to be the same
	 * 		  for all participants.  If the users ever want to pick from multiple
	 * 		  projects, the above named info will have to be collected into the
	 * 		  IndividualReport objects (will be redundant for people of the same DNA).
	 *
	 * @throws Exception
	 */
	public Workbook buildPoiXlsForAbyDExtract(ArrayList<ExtractItem> itemList, GroupReport group)
			throws ReportDataException
	{
		//System.out.println( "buildPoiXlsForAbyDExtract..... 820");
		
		// start setting up spreadsheet
		Workbook workbook = new XSSFWorkbook();
		
		// add watermark to title field
		POIXMLProperties xmlProps = ((POIXMLDocument) workbook).getProperties();
		POIXMLProperties.CoreProperties coreProps = xmlProps.getCoreProperties();
		coreProps.setTitle(ReportingConstants.WATERMARK);
		
		Sheet worksheet = workbook.createSheet("Worksheet");
		worksheet.setDefaultColumnWidth(10);
		
		// create all the cell styles....
		// Create a new font and alter it.
	    Font fontRegular = workbook.createFont();
	    fontRegular.setFontHeightInPoints((short)8);
	    fontRegular.setFontName("Arial");
		
	    Font fontBold = workbook.createFont();
	    fontBold.setFontHeightInPoints((short)8);
	    fontBold.setFontName("Arial");
	    fontBold.setBoldweight(Font.BOLDWEIGHT_BOLD);

	    // Set up the styles
		CellStyle cellStyleString = workbook.createCellStyle();
		cellStyleString.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		//cellStyleString.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyleString.setFont(fontRegular);
		
		CellStyle cellStyleStringBold = workbook.createCellStyle();  
		cellStyleStringBold.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		//cellStyleStringBold.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyleStringBold.setFont(fontBold);
		cellStyleStringBold.setWrapText(true);
		
		DataFormat fmt = workbook.createDataFormat();
		CellStyle cellStyleInt = workbook.createCellStyle();  
		cellStyleInt.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		//cellStyleStringBold.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyleInt.setFont(fontRegular);
		cellStyleInt.setDataFormat(fmt.getFormat("0"));
		
		CellStyle cellStyleFloat = workbook.createCellStyle();  
		cellStyleFloat.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		//cellStyleFloat.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyleFloat.setFont(fontRegular);
		cellStyleFloat.setDataFormat(fmt.getFormat("0.00"));
	
		int pCnt = 0;
		int rowNum = -2;
		//--------------------------------------------
				// 	HEADING ROW #1 
		//---------------------------------------------		
		// Row 1 is assumed all text (default)
		//System.out.println("loop 1 = rowNum = " + rowNum);		
		for (@SuppressWarnings("unused") IndividualReport ir : group.getIndividualReports())
		{
			if(pCnt >= 1)
				break;
			
			pCnt++;
			//build a row  - will need a row for each participant
			Row rowH = null;		
			rowH = worksheet.createRow((short) (rowNum + 2));
		
			int cellNum = 0;
			
			for(ExtractItem itm : itemList)
			{
				// one cell per item... 
				Cell cell = rowH.createCell(cellNum); 
				
				if (itm.getSrc() == ExtractItem.SRC_HDG)  // heading row.... 
				{
					// index from 0,0... cell A1 is cell(0,0)
					// Headings  happen in row 0, 1
					if (rowNum == -2)
					{
						cell.setCellValue(itm.getDispName());
						cell.setCellStyle(cellStyleStringBold);
						cellNum++;
					}
				}
				else if (itm.getSrc() == ExtractItem.SRC_GRP)
				{
					// Headings  happen in row 0, 1
					if (rowNum == -2)
					{	

						if(! itm.getKey().endsWith("_Score")){
							// doing this so that it puts the items next to each other 
							cell.setCellValue(itm.getDispName());
							cell.setCellStyle(cellStyleStringBold);
							cellNum++;
						}
					}
				}
				else if (itm.getSrc() == ExtractItem.SRC_INDV)
				{
					// Headings  happen in row 0,1
					if (rowNum == -2)
					{
						if( itm.getKey().endsWith("_Score") ||
						    itm.getKey().endsWith("_FinalScore") ||
						   (itm.getKey().startsWith("PDA_") && ! itm.getKey().endsWith("_HDG")) ||
						   (itm.getKey().startsWith("CPDA_") && ! itm.getKey().endsWith("_CFNAME")) )
						{	
							// Exclude me!
							// Do nothing
						}  else  {
						// Not part of the exclusions... go ahead and put it out
							cell.setCellValue(itm.getDispName());
							cell.setCellStyle(cellStyleStringBold);	
							cellNum++;
						}
					}
				} else {
					// Must be an RDx (Report Data) label -- name of the instrument				
					if (itm.getSrc() == ExtractItem.SRC_RDS)
					{
						// Headings happen in row 0, 1
						if (rowNum == -2)
						{							
							cell.setCellValue( itm.getDispName());
							cell.setCellStyle(cellStyleStringBold);
							cellNum++;
						}
					}
				}
			}	// End the itm for loop
			
			rowNum++;
		}	// End the IR for loop		
				
		//--------------------------------------------
		// 	HEADING ROW #2 
		//---------------------------------------------		
		// Row 2 is assumed all text (default)

		pCnt = 0;
		//System.out.println("loop 2 = rowNum = " + rowNum);			
		for (IndividualReport ir : group.getIndividualReports())
		{
			if(pCnt >= 1)
				break;
			
			pCnt++;
			//build a row  - will need a row for each participant
			Row rowH = null;		
			rowH = worksheet.createRow((short) (rowNum + 2));
		
			int cellNum = 0;
			
			for(ExtractItem itm : itemList)
			{
				// one cell per item... 
				Cell cell = rowH.createCell(cellNum); 

				if( ( itm.getKey().endsWith("_Score") ||
					  itm.getKey().endsWith("_FinalScore"))  ||
					 (itm.getKey().startsWith("PDA_") && ! itm.getKey().endsWith("_HDG")) ||
					 (itm.getKey().startsWith("CPDA_") && ! itm.getKey().endsWith("_CFNAME")) )
				{
					// skipping these cells on purpose for this row
					continue;
				}
				if (itm.getSrc() == ExtractItem.SRC_HDG)  // heading row.... 
				{
					// no headings in this row
					
				}
				else if (itm.getSrc() == ExtractItem.SRC_GRP)
				{
					// Headings  happen in row 0, 1
					if(rowNum == -1)
					{	
						if( !itm.getKey().equals("ClientName") && !itm.getKey().equals("ProjectName")   && !itm.getKey().equals("Model") ){
							// we don't want to see actual data, so excluding those					
							cell.setCellValue(group.getDisplayData(itm.getKey()));
							cell.setCellStyle(cellStyleStringBold);	
						}
					}
				}
				else if (itm.getSrc() == ExtractItem.SRC_INDV)
				{
					if(rowNum == -1)
					{
						if( itm.getKey().startsWith("IG_Comp") ||
						   (itm.getKey().startsWith("PDA_") && itm.getKey().endsWith("_HDG")) ||
						   (itm.getKey().startsWith("CPDA_") && itm.getKey().endsWith("_CFNAME"))
								){
					
							String s = ir.getDisplayData(itm.getKey());
							if (s != null && s.length() > MAX_CELL_SIZE)
							{
								s = s.substring(0,MAX_CELL_SIZE-1) + TRUNC_MSG;
							} 			
							cell.setCellValue(s);
							cell.setCellStyle(cellStyleStringBold);				
						}
					}
				}
		
				cellNum++;
			}	// End the itm for loop
			
			rowNum++;
		}	// End the IR for loop			
				
		//-----------------------------------------
		// LOOP 3 -- data loop
		//-----------------------------------------	
		// Data loops starts checking the data type
		
		//System.out.println(" LOOP 3 :  rowNum=" + rowNum);		
		// reset pCnt
		pCnt = 0;
		
		for (IndividualReport ir : group.getIndividualReports())
		{
			
			pCnt++;
			//build a row  - will need a row for each participant
			Row rowH = null;		
			rowH = worksheet.createRow((short) (rowNum + 2));
		
			int cellNum = 0;
			boolean isNumeric = true;
			
			for(ExtractItem itm : itemList)
			{
				Cell cell = rowH.createCell(cellNum);

				// Get the data from the appropriate source (will be a string)
				String valStr = "";
				if (itm.getSrc() == ExtractItem.SRC_HDG)  
				{
					//	DON'T NEED HEADINGS
					continue;
				}
				else if (itm.getSrc() == ExtractItem.SRC_GRP)
				{
					if(itm.getKey().endsWith("_Name"))
					{
						// Skip it
						continue;
					}
					else
					{
						// we only want "score" data... 
						valStr = group.getDisplayData(itm.getKey());
					}
				}
				else if (itm.getSrc() == ExtractItem.SRC_INDV)
				{
					// non-header data
					if(itm.getKey().endsWith("_Name")		||	// IG_Comp
					   itm.getKey().endsWith("_HDG")		||	// PDA stuff
					   itm.getKey().endsWith("_CFNAME") 	)	// Custom Field (PDA) stuff
					{
						// Skip it
						continue;
					} else {
						valStr = ir.getDisplayData(itm.getKey());											
					}
				} else {
					// Must be an RDx label
					ReportData rd = null;
					// Get the appropriate ReportData object
					rd = ir.getReportData().get(itm.getRptKey());
					if (rd == null)
						rd = new ReportData();

					if (itm.getSrc() == ExtractItem.SRC_RDD)
					{	
						// non-header data
						valStr = rd.getDisplayData(itm.getKey());
					}
					else if (itm.getSrc() == ExtractItem.SRC_RDS)
					{
						// then put out the score data
						valStr = rd.getScoreData(itm.getKey());
					}
				}
				
				// Got the data... vet it
				if (valStr == null || valStr.length() < 1)
				{
					// set it for nothing and scram
					//cell.setCellType(Cell.CELL_TYPE_BLANK);
					
					if (itm.getCellType() == ExtractItem.DATA_INT)
					{
						//System.out.println(" empty value:   itm.getCellType() == ExtractItem.DATA_INT");
						cell.setCellType(Cell.CELL_TYPE_BLANK);
						
					}
					else if(itm.getCellType() == ExtractItem.DATA_FLOAT)
					{
						//System.out.println(" empty value:   itm.getCellType() == ExtractItem.DATA_FLOAT");
						cell.setCellType(Cell.CELL_TYPE_BLANK);
						
					}
					else
					{
						// The problem with setting it blank is that stuff to the left of the cell will
						// extend right into blank cells.  Make it a string and set it to an empty string.
						cell.setCellValue("");
					}
				} else {
					// We have real data
					if (valStr.length() > MAX_CELL_SIZE)
					{
						valStr = valStr.substring(0,MAX_CELL_SIZE-1) + TRUNC_MSG;
					}
					// set up the type and format
					double valNum = 0.0;
					if (itm.getCellType() == ExtractItem.DATA_INT)
					{
						try {
							isNumeric = true;
							valNum = Double.parseDouble(valStr);
						} catch (NumberFormatException nfe) {
							isNumeric = false;
						}
						if (isNumeric) {
							cell.setCellValue(valNum);
							cell.setCellStyle(cellStyleInt);
						} else {
							cell.setCellValue(valStr);
							cell.setCellStyle(cellStyleString);
						}
					}
					else if(itm.getCellType() == ExtractItem.DATA_FLOAT)
					{
						try
						{
							isNumeric = true;
							valNum = Double.parseDouble(valStr);
						}
						catch (NumberFormatException nfe)
						{
							isNumeric = false;
						}
						if (isNumeric)
						{
							cell.setCellValue(valNum);
							cell.setCellStyle(cellStyleFloat);
						} else {
							cell.setCellValue(valStr);
							cell.setCellStyle(cellStyleString);
						}
						
					} else {
						cell.setCellValue(valStr);
						cell.setCellStyle(cellStyleString);
					}
				}
				cellNum++;

			}	// End the itm for loop
			
			rowNum++;
		}	// End the IR for loop
		
		return workbook;		
	}	


	/**
	 * buildPoiXlsForAbyDRawExtract - Build a spreadsheet for the Raw A by D Extract
	 * 
	 * NOTE:  This logic assumes that all of the participants are in the same
	 * 		  project/DNA.  Stuff with GRP tags is picked up from the GroupReport
	 * 		  object and so the client project and DNA info has to be the same
	 * 		  for all participants.  If the users ever want to pick from multiple
	 * 		  projects, the above named info will have to be collected into the
	 * 		  IndividualReport objects (will be redundant for people of the same DNA).
	 * 
	 *
	 * @throws Exception
	 */
	// TODO This is in a vertical format (participants in columns) because the old office
	// couldn't handle sufficient columns.  Conversion to horizontal format (participants
	// in rows) should be considered when time becomes available (or users request it)
	public Workbook buildPoiXlsForAbyDRawExtract(ArrayList<ExtractItem> itemList, GroupReport group)
			throws ReportDataException
	{
		//System.out.println( "buildPoiXlsForAbyD RAW Extract..... ");
		
		// start setting up spreadsheet
		XSSFWorkbook workbook = new XSSFWorkbook();
		
		// add watermark to title field
		POIXMLProperties xmlProps = ((POIXMLDocument) workbook).getProperties();
		POIXMLProperties.CoreProperties coreProps = xmlProps.getCoreProperties();
		coreProps.setTitle(ReportingConstants.WATERMARK);
		
		Sheet worksheet = workbook.createSheet("Worksheet");
		worksheet.setDefaultColumnWidth(40);
		
		// create all the cell styles....
		// Create a new font and alter it.
	    Font fontRegular = workbook.createFont();
	    fontRegular.setFontHeightInPoints((short)8);
	    fontRegular.setFontName("Arial");
		
	    Font fontBold = workbook.createFont();
	    fontBold.setFontHeightInPoints((short)8);
	    fontBold.setFontName("Arial");
	    fontBold.setBoldweight(Font.BOLDWEIGHT_BOLD);

	    // Set up the styles
		CellStyle cellStyleString = workbook.createCellStyle();
		cellStyleString.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		//cellStyleString.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyleString.setFont(fontRegular);
		
		
		CellStyle cellStyleStringBold = workbook.createCellStyle();  
		cellStyleStringBold.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		//cellStyleStringBold.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyleStringBold.setFont(fontBold);
		
		DataFormat fmt = workbook.createDataFormat();
		CellStyle cellStyleInt = workbook.createCellStyle();  
		cellStyleInt.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		//cellStyleInt.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyleInt.setFont(fontRegular);
		cellStyleInt.setDataFormat(fmt.getFormat("0"));
		
		CellStyle cellStyleFloat = workbook.createCellStyle();  
		cellStyleFloat.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		//cellStyleFloat.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyleFloat.setFont(fontRegular);
		cellStyleFloat.setDataFormat(fmt.getFormat("0.00"));
		
		CellStyle cellStyleFloat3 = workbook.createCellStyle();  
		cellStyleFloat3.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		//cellStyleFloat3.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyleFloat3.setFont(fontRegular);
		cellStyleFloat3.setDataFormat(fmt.getFormat("0.000"));
	
		int pCnt = 0;
	
		for (IndividualReport ir : group.getIndividualReports())
		{
			pCnt++;
			
			// there will be at least 2 columns, headers + one participant's data...
			int rowNum = 0;
			Row rowH = null;
			
			/*
			 * ***************
			 * HEADER COLUMN
			 * ***************
			 */
			// if this is the first person, we have to do the headers  too
			if (pCnt == 1)
			{ 
				// go through the extract items to  create headers in column 1 
				for(ExtractItem itm : itemList)  
				{
					//System.out.println("LOOP 1:   extract item:  key:" + itm.getKey() +  " pCnt:" + pCnt + "   rowNum:" + rowNum);				
					//build a row for the item ( 1 row for each item)
					rowH = worksheet.createRow((short) rowNum);
					//System.out.println("column " + pCnt + "  rowNum "  + rowNum +  "  row heading..... " + itm.getDispName());
					
					// style additions for header row
					cellStyleStringBold.setWrapText(true);
					cellStyleString.setWrapText(true);
					
					// create cell 0 for each row... 
					if (itm.getSrc() == ExtractItem.SRC_HDG)
					{
						Cell cell0 = rowH.createCell(0); 
						cell0.setCellValue(itm.getDispName());
						cell0.setCellStyle(cellStyleStringBold);						
						
					} else {
						
						Cell cell0 = rowH.createCell(0); 
						cell0.setCellValue(itm.getDispName());
						cell0.setCellStyle(cellStyleString);
					}
					
					rowNum++;
				}				
			}
				
			// set the styles that we changed in column 1
			cellStyleStringBold.setWrapText(false);
			cellStyleString.setWrapText(false);
							
			//reset the rowNum, just in case.... 
			rowNum = 0;	
			
			/*
			 * ***************
			 * DATA COLUMNS
			 * ***************
			 */
			// Now do the data - go through the item list a second time if this is pCnt ==1  - will only be the first time around for all other participants
			for(ExtractItem itm : itemList)
			{
				String valStr = "";
				double valNum = 0;
				boolean isNumeric = true;
				//System.out.println("LOOP 2:   extract item:  key:" + itm.getKey() +  " pCnt:" + pCnt + "   rowNum:" + rowNum);	
				//get the row for the item ( 1 row for each item)
				rowH = worksheet.getRow(rowNum);

				if (itm.getSrc() == ExtractItem.SRC_HDG)
				{
					// put nothing into the td for an individual's data
				}
				else if (itm.getSrc() == ExtractItem.SRC_GRP)
				{
					//System.out.println("  GRP Value=itm.getKey()=" +itm.getKey() + " " + group.getDisplayData(itm.getKey())  + " itm cell type: " + itm.getCellType());
					
					
					if(group.getDisplayData(itm.getKey()) == null 
							&& (itm.getCellType() == ExtractItem.DATA_INT  || itm.getCellType() == ExtractItem.DATA_FLOAT)){
						
						Cell cell = rowH.createCell(pCnt); 
						cell.setCellType(Cell.CELL_TYPE_BLANK);
						
					}else if(group.getDisplayData(itm.getKey()) == null){
						
						Cell cell = rowH.createCell(pCnt); 
						cell.setCellValue("");
						cell.setCellStyle(cellStyleString);	
						
					}else if(itm.getKey().contains("dlci")){
						
						valStr = group.getDisplayData(itm.getKey());
						valNum = Double.parseDouble(valStr);
						
						Cell cell = rowH.createCell(pCnt); 
						cell.setCellType(Cell.CELL_TYPE_NUMERIC);
						cell.setCellValue(valNum);
						cell.setCellStyle(cellStyleFloat3);					
					}
					else
					{
						Cell cell = rowH.createCell(pCnt); 
						cell.setCellValue(group.getDisplayData(itm.getKey()));
						cell.setCellStyle(cellStyleString);

					}
				}
				else if (itm.getSrc() == ExtractItem.SRC_INDV)
				{
					//System.out.println("  INDV Value=itm.getKey()="+itm.getKey() + "  " + ir.getDisplayData(itm.getKey())  + " itm cell type: " + itm.getCellType());
					
					if(ir.getDisplayData(itm.getKey()) == null 
							&& (itm.getCellType() == ExtractItem.DATA_INT  || itm.getCellType() == ExtractItem.DATA_FLOAT)){
						
						Cell cell = rowH.createCell(pCnt); 
						cell.setCellType(Cell.CELL_TYPE_BLANK);
						
					}else if(ir.getDisplayData(itm.getKey()) == null){
						
						Cell cell = rowH.createCell(pCnt); 
						cell.setCellValue("");
						cell.setCellStyle(cellStyleString);	
						
					}else if(ir.getDisplayData(itm.getKey()).equals("Insufficient Data")  ){
						
						// this is normally an int, but if there isn't enough data, it's a string
						Cell cell = rowH.createCell(pCnt); 
						cell.setCellValue(ir.getDisplayData(itm.getKey()));
						cell.setCellStyle(cellStyleString);	
						
					}else if(itm.getCellType() == ExtractItem.DATA_INT){
						valStr = ir.getDisplayData(itm.getKey());
						try
						{
							isNumeric = true;
							valNum = Double.parseDouble(valStr);
						}
						catch (NumberFormatException nfe)
						{
							isNumeric = false;
						}
						
						Cell cell = rowH.createCell(pCnt); 
						if (isNumeric)
						{
							cell.setCellValue(valNum);
							cell.setCellStyle(cellStyleInt);
						} else {
							cell.setCellValue(valStr);
							cell.setCellStyle(cellStyleString);
						}
						
					}else if(itm.getCellType() == ExtractItem.DATA_FLOAT){
						
						valStr = ir.getDisplayData(itm.getKey());
						try {
							isNumeric = true;
							valNum = Double.parseDouble(valStr);
						} catch (NumberFormatException nfe) {
							isNumeric = false;
						}

						Cell cell = rowH.createCell(pCnt); 
						if (isNumeric) {
							cell.setCellValue(valNum);
							cell.setCellStyle(cellStyleFloat);
						} else {
							cell.setCellValue(valStr);
							cell.setCellStyle(cellStyleString);
						}
						
					} else {
						
						// default is a string
						Cell cell = rowH.createCell(pCnt); 
						cell.setCellValue(ir.getDisplayData(itm.getKey()));
						cell.setCellStyle(cellStyleString);	
					}
					
				}
				else
				{
					// Must be an RDx label
					ReportData rd = null;
					// Get the appropriate ReportData object
					rd = ir.getReportData().get(itm.getRptKey());
					if (rd == null)
						rd = new ReportData();
					
					if (itm.getSrc() == ExtractItem.SRC_RDD)
					{
						// then put out the display data
						//System.out.println("  RDD Key=" + itm.getKey()  + "  RDD Value=" + ir.getDisplayData(itm.getKey()) + " itm cell type: " + itm.getCellType());
						Cell cell = rowH.createCell(pCnt); 
						cell.setCellValue(rd.getDisplayData(itm.getKey()));
						cell.setCellStyle(cellStyleString);
					}
					else if (itm.getSrc() == ExtractItem.SRC_RDS)
					{
						// then put out the score data
						if(rd.getScoreData(itm.getKey()) == null 
								&& (itm.getCellType() == ExtractItem.DATA_INT  || itm.getCellType() == ExtractItem.DATA_FLOAT)){
							
							Cell cell = rowH.createCell(pCnt); 
							cell.setCellType(Cell.CELL_TYPE_BLANK);
							
						}else if(rd.getScoreData(itm.getKey()) == null){
							
							Cell cell = rowH.createCell(pCnt); 
							cell.setCellValue("");
							cell.setCellStyle(cellStyleString);	
							
						}else if(itm.getCellType() == ExtractItem.DATA_INT){
							
							valStr = rd.getScoreData(itm.getKey());
							try {
								isNumeric = true;
								valNum = Double.parseDouble(valStr);
							} catch (NumberFormatException nfe) {
								isNumeric = false;
							}
							
							Cell cell = rowH.createCell(pCnt); 
							if (isNumeric)
							{
								cell.setCellType(Cell.CELL_TYPE_NUMERIC);
								cell.setCellValue(valNum);
								cell.setCellStyle(cellStyleInt);
							} else {
								cell.setCellValue(valStr);
								cell.setCellStyle(cellStyleString);
							}
							
						}else if(itm.getCellType() == ExtractItem.DATA_FLOAT){
							
							valStr = rd.getScoreData(itm.getKey());
							try {
								isNumeric = true;
								valNum = Double.parseDouble(valStr);
							} catch (NumberFormatException nfe) {
								isNumeric = false;
							}
									
							Cell cell = rowH.createCell(pCnt);
							if (isNumeric) {
								cell.setCellType(Cell.CELL_TYPE_NUMERIC);
								cell.setCellValue(valNum);
								cell.setCellStyle(cellStyleFloat);
							} else {
								cell.setCellValue(valStr);
								cell.setCellStyle(cellStyleString);
							}
							
						}else{
							// default is string
							Cell cell = rowH.createCell(pCnt); 
							cell.setCellValue(rd.getScoreData(itm.getKey()));
							cell.setCellStyle(cellStyleString);	
						}
					}
				}
				rowNum++;				
			}
		}	
		return workbook;
	}
	
	
	/**
	 * buildPoiXlsForTLTExtract - creating spreadsheet for the TLT Extract, 
	 * using the Apache POI API
	 * 
	 * @param GroupReport gr
	 * @return Workbook -- the created excel workbook
	 */
	public Workbook buildPoiXlsForTLTExtract(GroupReport gr)
		throws ReportDataException
	{
		//System.out.println("entering... buildPoiXlsForTLTExtract....");
		Workbook workbook = new XSSFWorkbook();
		
		// add watermark to title field
		POIXMLProperties xmlProps = ((POIXMLDocument) workbook).getProperties();
		POIXMLProperties.CoreProperties coreProps = xmlProps.getCoreProperties();
		coreProps.setTitle(ReportingConstants.WATERMARK);
		
		Sheet worksheet = workbook.createSheet("Worksheet");

		worksheet.setDefaultColumnWidth(38);

		// create all the cell styles....
		CellStyle cellStyleNormal = workbook.createCellStyle();
		cellStyleNormal.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		
		CellStyle cellStyleNormalBoldRight = workbook.createCellStyle();  
		cellStyleNormalBoldRight.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		cellStyleNormalBoldRight.setAlignment(CellStyle.ALIGN_CENTER);

		// The extract is the 18 scale scores 
		// the overall derailment risk 
		// and the raw score for each individual derailer. 
		// Ignore the first three overall scales that have only the 0 or 1 score 
		
		// HEADERS
		Row row1 = worksheet.createRow((short) 0);
		
		Cell cell1 = row1.createCell(0); // 0/0
		cell1.setCellValue("Participant ID");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(1);
		cell1.setCellValue("Last Name");
		cell1.setCellStyle(cellStyleNormalBoldRight);		
		cell1 = row1.createCell(2);
		cell1.setCellValue("First Name");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(3);
		cell1.setCellValue("Email");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(4);
		cell1.setCellValue("Client Name");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(5);
		cell1.setCellValue("Optional 1");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(6);
		cell1.setCellValue("Optional 2");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(7);
		cell1.setCellValue("Optional 3");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(8);
		cell1.setCellValue("Target Level");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		
		cell1 = row1.createCell(9);
		cell1.setCellValue("Leadership Interest");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		
		cell1 = row1.createCell(10);
		cell1.setCellValue("Leadership Experience");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		
		cell1 = row1.createCell(11);
		cell1.setCellValue("Leadership Foundations");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		
		cell1 = row1.createCell(12);
		cell1.setCellValue("Derailment Risk");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		
		cell1 = row1.createCell(13);
		cell1.setCellValue("Leadership Aspiration");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(14);
		cell1.setCellValue("Career Drivers");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(15);
		cell1.setCellValue("Learning Orientation");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(16);
		cell1.setCellValue("Experience Orientation");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		
		cell1 = row1.createCell(17);
		cell1.setCellValue("Business Operations");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(18);
		cell1.setCellValue("Handling Tough Challenges");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(19);
		cell1.setCellValue("High Visibility");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(20);
		cell1.setCellValue("Growing The Business");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(21);
		cell1.setCellValue("Personal Development");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		
		cell1 = row1.createCell(22);
		cell1.setCellValue("Problem Solving");
		cell1.setCellStyle(cellStyleNormalBoldRight);

		cell1 = row1.createCell(23);
		cell1.setCellValue("Intellectual Engagement");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(24);
		cell1.setCellValue("Attention to Detail");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(25);
		cell1.setCellValue("Impact/Influence");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(26);
		cell1.setCellValue("Interpersonal Engagement");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(27);
		cell1.setCellValue("Achievement Drive");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(28);
		cell1.setCellValue("Advancement Drive");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(29);
		cell1.setCellValue("Collective Orientation");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(30);
		cell1.setCellValue("Flexibility/Adaptability");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		
		cell1 = row1.createCell(31);
		cell1.setCellValue("Ego-Centered");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(32);
		cell1.setCellValue("Manipulation");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(33);
		cell1.setCellValue("Micro Managing");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(34);
		cell1.setCellValue("Passive-Agressive");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		
		ArrayList<IndividualReport> reports = gr.getIndividualReports();
		
		int rowCount = 1;
		for(IndividualReport ir: reports){
						
			// index from 0,0... cell A1 is cell(0,0)
			row1 = worksheet.createRow((short) rowCount++);
			
			cell1 = row1.createCell(0); // R1B0 ()
			cell1.setCellValue(new Integer(ir.getDisplayData("PART_ID")));
			cell1.setCellStyle(cellStyleNormal);
			cell1 = row1.createCell(1);
			cell1.setCellValue(ir.getDisplayData("LAST_NAME"));
			cell1.setCellStyle(cellStyleNormal);
			cell1 = row1.createCell(2);
			cell1.setCellValue(ir.getDisplayData("FIRST_NAME"));
			cell1.setCellStyle(cellStyleNormal);
			cell1 = row1.createCell(3);
			cell1.setCellValue(ir.getDisplayData("PART_EMAIL"));
			cell1.setCellStyle(cellStyleNormal);
			cell1 = row1.createCell(4);
			cell1.setCellValue(ir.getDisplayData("ORGANIZATION"));
			cell1.setCellStyle(cellStyleNormal);
			cell1 = row1.createCell(5);
			cell1.setCellValue(ir.getDisplayData("OPTIONAL_1"));
			cell1.setCellStyle(cellStyleNormal);
			cell1 = row1.createCell(6);
			cell1.setCellValue(ir.getDisplayData("OPTIONAL_2"));
			cell1.setCellStyle(cellStyleNormal);
			cell1 = row1.createCell(7);
			cell1.setCellValue(ir.getDisplayData("OPTIONAL_3"));
			cell1.setCellStyle(cellStyleNormal);
			cell1 = row1.createCell(8);
			cell1.setCellValue(ir.getDisplayData("TARGET_LEVEL"));
			cell1.setCellStyle(cellStyleNormal);

			
			if (!(ir.getDisplayData().get("LEI_MISSING") == null)){
				cell1 = row1.createCell(9);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);	
				cell1 = row1.createCell(10);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);	
				cell1 = row1.createCell(11);
				cell1.setCellValue("");
			} else {				
				cell1 = row1.createCell(9);
				cell1.setCellValue(new Double(ir.getScriptedGroupData().get("LE_INTER_FINALSCORE")));
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(10);
				cell1.setCellValue(new Double(ir.getScriptedGroupData().get("LE_EXPER_FINALSCORE")));
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(11);
				cell1.setCellValue(new Double(ir.getScriptedGroupData().get("LE_FOUND_FINALSCORE")));
				cell1.setCellStyle(cellStyleNormal);			
			}

			if (!(ir.getDisplayData().get("GPI_MISSING") == null)){
				cell1 = row1.createCell(12);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);
			} else {
				cell1 = row1.createCell(12);
				cell1.setCellValue(new Double(ir.getScriptedData().get("DERAIL_FACTORS")));
				cell1.setCellStyle(cellStyleNormal);				
			}

			if (!(ir.getDisplayData().get("CS_MISSING") == null)){
				cell1 = row1.createCell(13);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);	
				cell1 = row1.createCell(14);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);	
				cell1 = row1.createCell(15);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);	
				cell1 = row1.createCell(16);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);
			}else{
				
				cell1 = row1.createCell(13);
				cell1.setCellValue(new Double(ir.getScriptedData().get("CAREER_GOALS")).doubleValue());
				cell1.setCellStyle(cellStyleNormal);	
				cell1 = row1.createCell(14);
				cell1.setCellValue(new Double(ir.getScriptedData().get("CAREER_DRIVERS")).doubleValue());
				cell1.setCellStyle(cellStyleNormal);	
				cell1 = row1.createCell(15);
				cell1.setCellValue(new Double(ir.getScriptedData().get("LEARNING_ORIENT")).doubleValue());
				cell1.setCellStyle(cellStyleNormal);	
				cell1 = row1.createCell(16);
				cell1.setCellValue(new Double(ir.getScriptedData().get("EXPERIENCE_ORIENT")).doubleValue());
				cell1.setCellStyle(cellStyleNormal);					
			}

			if (!(ir.getDisplayData().get("LEI_MISSING") == null)){
				cell1 = row1.createCell(17);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);	
				cell1 = row1.createCell(18);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);	
				cell1 = row1.createCell(19);
				cell1.setCellValue("");
				cell1 = row1.createCell(20);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);	
				cell1 = row1.createCell(21);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);
			} else {
				cell1 = row1.createCell(17);
				cell1.setCellValue(new Double(ir.getScriptedData().get("LEI_OPERATIONS")).doubleValue());
				cell1.setCellStyle(cellStyleNormal);	
				cell1 = row1.createCell(18);
				cell1.setCellValue(new Double(ir.getScriptedData().get("LEI_SIT_TOUGH_CHAL")).doubleValue());
				cell1.setCellStyle(cellStyleNormal);	
				cell1 = row1.createCell(19);
				cell1.setCellValue(new Double(ir.getScriptedData().get("LEI_HIGH_VIS")).doubleValue());
				cell1.setCellStyle(cellStyleNormal);	
				cell1 = row1.createCell(20);
				cell1.setCellValue(new Double(ir.getScriptedData().get("LEI_BD_GROW_BUS")).doubleValue());
				cell1.setCellStyle(cellStyleNormal);	
				cell1 = row1.createCell(21);
				cell1.setCellValue(new Double(ir.getScriptedData().get("LEI_PERSONAL")).doubleValue());
				cell1.setCellStyle(cellStyleNormal);	
			}

			//Problem Solving	        
			if(ir.getDisplayData().get("COGNITIVES_INCLUDED").equals("1")){
				cell1 = row1.createCell(22);
				
				if (!(ir.getDisplayData().get("RAVENS_MISSING") == null)){
					cell1.setCellValue("");
				}
				else if (ir.getScriptedData().get("ZGEST") == null || ir.getScriptedData().get("ZGEST").equals("")){
					cell1.setCellValue(0);
				} else { 
					cell1.setCellValue(new Double(ir.getScriptedData().get("ZGEST")).doubleValue());
				}		
				
				cell1.setCellStyle(cellStyleNormal);
			} else {
				cell1 = row1.createCell(22);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);
			}

			if (!(ir.getDisplayData().get("GPI_MISSING") == null)){
				cell1 = row1.createCell(23);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(24);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(25);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(26);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(27);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(28);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(29);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(30);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);
				
				cell1 = row1.createCell(31);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(32);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(33);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(34);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);	
			} else {
				cell1 = row1.createCell(23);
				cell1.setCellValue(new Double(ir.getScriptedData().get("INTELLECTUAL_ENGMT")).doubleValue());
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(24);
				cell1.setCellValue(new Double(ir.getScriptedData().get("DETAIL_ORIENT")).doubleValue());
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(25);
				cell1.setCellValue(new Double(ir.getScriptedData().get("FACIL_LEADERSHP")).doubleValue());
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(26);
				cell1.setCellValue(new Double(ir.getScriptedData().get("INTER_PERSONAL_ENGMT")).doubleValue());
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(27);
				cell1.setCellValue(new Double(ir.getScriptedData().get("DRIVE")).doubleValue());
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(28);
				cell1.setCellValue(new Double(ir.getScriptedData().get("INDIV_ORIENT")).doubleValue());
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(29);
				cell1.setCellValue(new Double(ir.getScriptedData().get("COLLECTIVE_ORIENT")).doubleValue());
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(30);
				cell1.setCellValue(new Double(ir.getScriptedData().get("POSITIVITY")).doubleValue());
				cell1.setCellStyle(cellStyleNormal);
				
				cell1 = row1.createCell(31);
				cell1.setCellValue(new Double(ir.getScriptedData().get("GPI_EGOCENTRIC")).doubleValue());
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(32);
				cell1.setCellValue(new Double(ir.getScriptedData().get("GPI_MANIPULATION")).doubleValue());
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(33);
				cell1.setCellValue(new Double(ir.getScriptedData().get("GPI_MICRO_MANAGE")).doubleValue());
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(34);
				cell1.setCellValue(new Double(ir.getScriptedData().get("GPI_PASS_AGGRESS")).doubleValue());
				cell1.setCellStyle(cellStyleNormal);				
			}
		}

		return workbook;
	}
	

	/**
	 * buildPoiXlsForTLTRawExtract - creating spreadsheet for the TLT Raw Extract, 
	 * using the Apache POI API
	 * 
	 * @param GroupReport gr
	 * @return Workbook -- the created excel workbook
	 */
	public Workbook buildPoiXlsForTLTRawExtract(GroupReport gr)
		throws ReportDataException
	{
		//System.out.println("entering... buildPoiXlsForTLTRawExtract....");
		Workbook workbook = new XSSFWorkbook();
		
		// add watermark to title field
		POIXMLProperties xmlProps = ((POIXMLDocument) workbook).getProperties();
		POIXMLProperties.CoreProperties coreProps = xmlProps.getCoreProperties();
		coreProps.setTitle(ReportingConstants.WATERMARK);
		
		Sheet worksheet = workbook.createSheet("Worksheet");

		worksheet.setDefaultColumnWidth(38);

		// create all the cell styles....
		CellStyle cellStyleNormal = workbook.createCellStyle();
		cellStyleNormal.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		
		CellStyle cellStyleNormalBoldRight = workbook.createCellStyle();  
		cellStyleNormalBoldRight.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		cellStyleNormalBoldRight.setAlignment(CellStyle.ALIGN_CENTER);

		// The extract is the 18 scale scores 
		// the overall derailment risk 
		// and the raw score for each individual derailer. 
		// Ignore the first three overall scales that have only the 0 or 1 score 
		
		// HEADERS
		Row row1 = worksheet.createRow((short) 0);
		
		Cell cell1 = row1.createCell(0); // 0/0
		cell1.setCellValue("Participant ID");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(1);
		cell1.setCellValue("Last Name");
		cell1.setCellStyle(cellStyleNormalBoldRight);		
		cell1 = row1.createCell(2);
		cell1.setCellValue("First Name");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(3);
		cell1.setCellValue("Email");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(4);
		cell1.setCellValue("Client Name");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(5);
		cell1.setCellValue("Optional 1");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(6);
		cell1.setCellValue("Optional 2");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(7);
		cell1.setCellValue("Optional 3");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(8);
		cell1.setCellValue("Target Level");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		
		cell1 = row1.createCell(9);
		cell1.setCellValue("Derailment Risk");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(10);
		cell1.setCellValue("Ego-Centered");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(11);
		cell1.setCellValue("Manipulation");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(12);
		cell1.setCellValue("Micro Managing");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(13);
		cell1.setCellValue("Passive-Agressive");
		cell1.setCellStyle(cellStyleNormalBoldRight);

		cell1 = row1.createCell(14);
		cell1.setCellValue("Leadership Aspiration");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(15);
		cell1.setCellValue("Career Drivers");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(16);
		cell1.setCellValue("Learning Orientation");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(17);
		cell1.setCellValue("Experience Orientation");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		
		cell1 = row1.createCell(18);
		cell1.setCellValue("Business Operations");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(19);
		cell1.setCellValue("Handling Tough Challenges");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(20);
		cell1.setCellValue("High Visibility");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(21);
		cell1.setCellValue("Growing The Business");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(22);
		cell1.setCellValue("Personal Development");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		
		cell1 = row1.createCell(23);
		cell1.setCellValue("Problem Solving");
		cell1.setCellStyle(cellStyleNormalBoldRight);

		cell1 = row1.createCell(24);
		cell1.setCellValue("Intellectual Engagement");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(25);
		cell1.setCellValue("Attention to Detail");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(26);
		cell1.setCellValue("Impact/Influence");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(27);
		cell1.setCellValue("Interpersonal Engagement");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(28);
		cell1.setCellValue("Achievement Drive");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(29);
		cell1.setCellValue("Advancement Drive");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(30);
		cell1.setCellValue("Collective Orientation");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		cell1 = row1.createCell(31);
		cell1.setCellValue("Flexibility/Adaptability");
		cell1.setCellStyle(cellStyleNormalBoldRight);
		
		
		
		ArrayList<IndividualReport> reports = gr.getIndividualReports();		
		int rowCount = 1;
		for(IndividualReport ir: reports){
			// index from 0,0... cell A1 is cell(0,0)
			row1 = worksheet.createRow((short) rowCount++);
			
			cell1 = row1.createCell(0); // R1B0 ()
			cell1.setCellValue(Integer.parseInt(ir.getDisplayData("PART_ID")));
			cell1.setCellStyle(cellStyleNormal);
			cell1 = row1.createCell(1);
			cell1.setCellValue(ir.getDisplayData("LAST_NAME"));
			cell1.setCellStyle(cellStyleNormal);
			cell1 = row1.createCell(2);
			cell1.setCellValue(ir.getDisplayData("FIRST_NAME"));
			cell1.setCellStyle(cellStyleNormal);
			cell1 = row1.createCell(3);
			cell1.setCellValue(ir.getDisplayData("PART_EMAIL"));
			cell1.setCellStyle(cellStyleNormal);
			cell1 = row1.createCell(4);
			cell1.setCellValue(ir.getDisplayData("ORGANIZATION"));
			cell1.setCellStyle(cellStyleNormal);
			cell1 = row1.createCell(5);
			cell1.setCellValue(ir.getDisplayData("OPTIONAL_1"));
			cell1.setCellStyle(cellStyleNormal);
			cell1 = row1.createCell(6);
			cell1.setCellValue(ir.getDisplayData("OPTIONAL_2"));
			cell1.setCellStyle(cellStyleNormal);
			cell1 = row1.createCell(7);
			cell1.setCellValue(ir.getDisplayData("OPTIONAL_3"));
			cell1.setCellStyle(cellStyleNormal);
			cell1 = row1.createCell(8);
			cell1.setCellValue(ir.getDisplayData("TARGET_LEVEL"));
			cell1.setCellStyle(cellStyleNormal);
			
			if (!(ir.getDisplayData().get("GPI_MISSING") == null)){
				cell1 = row1.createCell(9);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(10);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(11);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(12);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(13);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);	
			} else {
				cell1 = row1.createCell(9);
				cell1.setCellValue(new Double(ir.getScriptedData().get("RAW_" + ReportingConstants.DR_DERAIL)).doubleValue());
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(10);
				cell1.setCellValue(new Double( ir.getScriptedData().get("RAW_" + ReportingConstants.DR_EGOCE)).doubleValue());
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(11);
				cell1.setCellValue(new Double(ir.getScriptedData().get("RAW_" + ReportingConstants.DR_MANIP)).doubleValue());
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(12);
				cell1.setCellValue(new Double(ir.getScriptedData().get("RAW_" + ReportingConstants.DR_MICRO)).doubleValue());
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(13);
				cell1.setCellValue(new Double(ir.getScriptedData().get("RAW_" + ReportingConstants.DR_PASAG)).doubleValue());
				cell1.setCellStyle(cellStyleNormal);	
			}
			
			if (!(ir.getDisplayData().get("CS_MISSING") == null)){
				cell1 = row1.createCell(14);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);	
				cell1 = row1.createCell(15);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);	
				cell1 = row1.createCell(16);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);	
				cell1 = row1.createCell(17);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);
			} else {
				cell1 = row1.createCell(14);
				cell1.setCellValue(new Double(ir.getScriptedData().get("RAW_" + ReportingConstants.CS_LDRAS)).doubleValue());
				cell1.setCellStyle(cellStyleNormal);	
				cell1 = row1.createCell(15);
				cell1.setCellValue(new Double(ir.getScriptedData().get("RAW_" + ReportingConstants.CS_CRDRV)).doubleValue());
				cell1.setCellStyle(cellStyleNormal);	
				cell1 = row1.createCell(16);
				cell1.setCellValue(new Double(ir.getScriptedData().get("RAW_" + ReportingConstants.CS_LRNOR)).doubleValue());
				cell1.setCellStyle(cellStyleNormal);	
				cell1 = row1.createCell(17);
				cell1.setCellValue(new Double(ir.getScriptedData().get("RAW_" + ReportingConstants.CS_EXPOR)).doubleValue());
				cell1.setCellStyle(cellStyleNormal);
			}
			
			if (!(ir.getDisplayData().get("LEI_MISSING") == null)){
				cell1 = row1.createCell(18);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);	
				cell1 = row1.createCell(19);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);	
				cell1 = row1.createCell(20);
				cell1.setCellValue("");
				cell1 = row1.createCell(21);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);	
				cell1 = row1.createCell(22);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);
			} else {
				cell1 = row1.createCell(18);
				cell1.setCellValue(new Double(ir.getScriptedData().get("RAW_" + ReportingConstants.LEI_BUSOP)).doubleValue());
				cell1.setCellStyle(cellStyleNormal);	
				cell1 = row1.createCell(19);
				cell1.setCellValue(new Double(ir.getScriptedData().get("RAW_" + ReportingConstants.LEI_HTC)).doubleValue());
				cell1.setCellStyle(cellStyleNormal);	
				cell1 = row1.createCell(20);
				cell1.setCellValue(new Double(ir.getScriptedData().get("RAW_" + ReportingConstants.LEI_HVIS)).doubleValue());
				cell1.setCellStyle(cellStyleNormal);	
				cell1 = row1.createCell(21);
				cell1.setCellValue(new Double(ir.getScriptedData().get("RAW_" + ReportingConstants.LEI_GTBUS)).doubleValue());
				cell1.setCellStyle(cellStyleNormal);	
				cell1 = row1.createCell(22);
				cell1.setCellValue(new Double(ir.getScriptedData().get("RAW_" + ReportingConstants.LEI_PDEV)).doubleValue());
				cell1.setCellStyle(cellStyleNormal);
			}
	
			if (!(ir.getDisplayData().get("RAVENS_MISSING") == null) || 
				ir.getDisplayData().get(ReportingConstants.RC_COGNITIVES_INCLUDED).equals("0")){
				//System.out.println("reportHelper.java - 1979..... RAVENS_MISSING=null or cogs included = 0 " );			
				cell1 = row1.createCell(23);
				cell1.setCellValue("");  
				cell1.setCellStyle(cellStyleNormal);
			} else {
				cell1 = row1.createCell(23);
				//cell1.setCellValue(ir.getScriptedData().get("ZGEST"));	//"RAW_" + ReportingConstants.RAV_PSOLV)	
				cell1.setCellValue(new Double(ir.getScriptedData().get("RAW_RAVENS_CORRECT")).doubleValue());  
					
				cell1.setCellStyle(cellStyleNormal);
			}

			if (!(ir.getDisplayData().get("GPI_MISSING") == null)){
				cell1 = row1.createCell(24);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(25);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(26);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(27);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(28);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(29);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(30);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(31);
				cell1.setCellValue("");
				cell1.setCellStyle(cellStyleNormal);
			} else {
				cell1 = row1.createCell(24);
				cell1.setCellValue(new Double(ir.getScriptedData().get("RAW_" + ReportingConstants.GPI_INTEN)).doubleValue());
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(25);
				cell1.setCellValue(new Double(ir.getScriptedData().get("RAW_" + ReportingConstants.GPI_ATD)).doubleValue());
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(26);
				cell1.setCellValue(new Double(ir.getScriptedData().get("RAW_" + ReportingConstants.GPI_IMIN)).doubleValue());
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(27);
				cell1.setCellValue(new Double(ir.getScriptedData().get("RAW_" + ReportingConstants.GPI_INEN)).doubleValue());
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(28);
				cell1.setCellValue(new Double(ir.getScriptedData().get("RAW_" + ReportingConstants.GPI_ACHD)).doubleValue());
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(29);
				cell1.setCellValue(new Double(ir.getScriptedData().get("RAW_" + ReportingConstants.GPI_ADVD)).doubleValue());
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(30);
				cell1.setCellValue(new Double(ir.getScriptedData().get("RAW_" + ReportingConstants.GPI_COOR)).doubleValue());
				cell1.setCellStyle(cellStyleNormal);
				cell1 = row1.createCell(31);
				cell1.setCellValue(new Double(ir.getScriptedData().get("RAW_" + ReportingConstants.GPI_FLAD)).doubleValue());
				cell1.setCellStyle(cellStyleNormal);
			}
		}	// End of loop on reports

		return workbook;
	}
	


	/**
	 * creating XML output for the TLT Extract, 
	 * to send to the scorecard server
	 * 
	 * NOTE 10-2012: at this point, the actual extract
	 * is for a scorecard, which means that it will not
	 * be a group xml output. The group will only contain 
	 * one participant.
	 * I'm doing this for 2 reasons - 1 --> the code I want in
	 * the portal servlet is running against a group report... 
	 * 2 --> at some point, they're gonna want group xml... (it's inevitable)
	 * 
	 * @param GroupReport gr
	 * @return String -- the created xml
	 */
	public String buildXMLForTLTExtract(GroupReport gr)
		throws ReportDataException
	{
		String openXML = "<tltExtract>";
		String closeXML = "</tltExtract>";		
		//<var id = "Career_Drivers" value = "3"/>
		String varIdStart ="<var id=\"";
		String value = "\" value=\"";
		String varIdEnd = "\"/>";
		
		String theXML = "";
		
		ArrayList<IndividualReport> reports = gr.getIndividualReports();
		
		//int rowCount = 1;
		for(IndividualReport ir : reports){
			
			theXML += openXML;
			
			theXML += varIdStart + "Participant ID" + value + ir.getDisplayData("PART_ID") +  varIdEnd;
			theXML += varIdStart + "Last Name" + value + ir.getDisplayData("LAST_NAME") +  varIdEnd;
			theXML += varIdStart + "First Name" + value + ir.getDisplayData("FIRST_NAME") +  varIdEnd;
			theXML += varIdStart + "Email"  + value +  ir.getDisplayData("PART_EMAIL") +  varIdEnd;
			theXML += varIdStart + "Client Name"  + value +  ir.getDisplayData("ORGANIZATION") +  varIdEnd;
			theXML += varIdStart + "Optional 1"   + value +  ir.getDisplayData("OPTIONAL_1") +  varIdEnd;
			theXML += varIdStart + "Optional 2"   + value +  ir.getDisplayData("OPTIONAL_2") +  varIdEnd;
			theXML += varIdStart + "Optional 3"   + value +  ir.getDisplayData("OPTIONAL_3") +  varIdEnd;
			theXML += varIdStart + "Target Level"  + value +  ir.getDisplayData("TARGET_LEVEL") +  varIdEnd;

			
			if (!(ir.getDisplayData().get("LEI_MISSING") == null)){
				theXML += varIdStart + "Leadership Interest"  + value + varIdEnd;
				theXML += varIdStart + "Leadership Experience"  + value + varIdEnd;
				theXML += varIdStart + "Leadership Foundations"  + value + varIdEnd;
			} else {		
				
				theXML += varIdStart + "Leadership Interest"  + value +  ir.getScriptedGroupData().get("LE_INTER_FINALSCORE") +  varIdEnd;
				theXML += varIdStart + "Leadership Experience"  + value +  ir.getScriptedGroupData().get("LE_EXPER_FINALSCORE") +  varIdEnd;
				theXML += varIdStart + "Leadership Foundations"  + value +  ir.getScriptedGroupData().get("LE_FOUND_FINALSCORE") +  varIdEnd;
			}
			
			if (!(ir.getDisplayData().get("GPI_MISSING") == null)){
				theXML += varIdStart + "Derailment Risk"  + value  +  varIdEnd;
			} else {
				theXML += varIdStart + "Derailment Risk"  + value +   ir.getScriptedData().get("DERAIL_FACTORS") +  varIdEnd;	
			}

			if (!(ir.getDisplayData().get("CS_MISSING") == null)){
				theXML += varIdStart + "Leadership Aspiration"  + value +  varIdEnd;
				theXML += varIdStart + "Career Drivers" + value +  varIdEnd;
				theXML += varIdStart + "Learning Orientation" + value +  varIdEnd;
				theXML += varIdStart + "Experience Orientation" + value + varIdEnd;
			} else {
				theXML += varIdStart + "Leadership Aspiration"  + value + ir.getScriptedData().get("CAREER_GOALS") +  varIdEnd;
				theXML += varIdStart + "Career Drivers" + value +  ir.getScriptedData().get("CAREER_DRIVERS") +  varIdEnd;
				theXML += varIdStart + "Learning Orientation" + value + ir.getScriptedData().get("LEARNING_ORIENT") +  varIdEnd;
				theXML += varIdStart + "Experience Orientation" + value + ir.getScriptedData().get("EXPERIENCE_ORIENT") +  varIdEnd;
			}

			if (!(ir.getDisplayData().get("LEI_MISSING") == null)){
				theXML += varIdStart + "Business Operations" + value +  varIdEnd;
				theXML += varIdStart + "Handling Tough Challenges" + value +  varIdEnd;
				theXML += varIdStart + "High Visibility" + value +  varIdEnd;
				theXML += varIdStart + "Growing The Business" + value + varIdEnd;
				theXML += varIdStart + "Personal Development" + value + varIdEnd;
			} else {
				theXML += varIdStart + "Business Operations" + value +  ir.getScriptedData().get("LEI_OPERATIONS") +  varIdEnd;
				theXML += varIdStart + "Handling Tough Challenges" + value +  ir.getScriptedData().get("LEI_SIT_TOUGH_CHAL") +  varIdEnd;
				theXML += varIdStart + "High Visibility" + value +  ir.getScriptedData().get("LEI_HIGH_VIS") +  varIdEnd;
				theXML += varIdStart + "Growing The Business" + value +  ir.getScriptedData().get("LEI_BD_GROW_BUS") +  varIdEnd;
				theXML += varIdStart + "Personal Development" + value +  ir.getScriptedData().get("LEI_PERSONAL") +  varIdEnd;
			}
			
			//Problem Solving	        
			if(ir.getDisplayData().get("COGNITIVES_INCLUDED").equals("1")){
				if (!(ir.getDisplayData().get("RAVENS_MISSING") == null)){
					theXML += varIdStart + "Problem Solving" + value  +  varIdEnd;
				}
				else if (ir.getScriptedData().get("ZGEST") == null || ir.getScriptedData().get("ZGEST").equals("")){
					theXML += varIdStart + "Problem Solving" + value +  "0"  +  varIdEnd;
				} else { 
					theXML += varIdStart + "Problem Solving" + value + ir.getScriptedData().get("ZGEST") +  varIdEnd;
				}		
			} else {
				// do nothing..
			}

			if (!(ir.getDisplayData().get("GPI_MISSING") == null)){
				theXML += varIdStart + "Intellectual Engagement" + value  +  varIdEnd;
				theXML += varIdStart + "Attention to Detail" + value  +  varIdEnd;
				theXML += varIdStart + "Impact Influence" + value  +  varIdEnd;
				theXML += varIdStart + "Interpersonal Engagement" + value +  varIdEnd;
				theXML += varIdStart + "Achievement Drive" + value +  varIdEnd;
				theXML += varIdStart + "Advancement Drive" + value  +  varIdEnd;
				theXML += varIdStart + "Collective Orientation" + value  +  varIdEnd;
				theXML += varIdStart + "Flexibility Adaptability" + value  +  varIdEnd;

				theXML += varIdStart + "Ego-Centered" + value +  varIdEnd;
				theXML += varIdStart + "Manipulation" + value +  varIdEnd;
				theXML += varIdStart + "Micro Managing" + value + varIdEnd;
				theXML += varIdStart + "Passive-Agressive" + value + varIdEnd;
			} else {
				theXML += varIdStart + "Intellectual Engagement" + value + ir.getScriptedData().get("INTELLECTUAL_ENGMT") +  varIdEnd;
				theXML += varIdStart + "Attention to Detail" + value + ir.getScriptedData().get("DETAIL_ORIENT") +  varIdEnd;
				theXML += varIdStart + "Impact Influence" + value + ir.getScriptedData().get("FACIL_LEADERSHP") +  varIdEnd;
				theXML += varIdStart + "Interpersonal Engagement" + value + ir.getScriptedData().get("INTER_PERSONAL_ENGMT") +  varIdEnd;
				theXML += varIdStart + "Achievement Drive" + value + ir.getScriptedData().get("DRIVE") +  varIdEnd;
				theXML += varIdStart + "Advancement Drive" + value + ir.getScriptedData().get("INDIV_ORIENT") +  varIdEnd;
				theXML += varIdStart + "Collective Orientation" + value + ir.getScriptedData().get("COLLECTIVE_ORIENT") +  varIdEnd;
				theXML += varIdStart + "Flexibility/Adaptability" + value + ir.getScriptedData().get("POSITIVITY") +  varIdEnd;

				theXML += varIdStart + "Ego-Centered" + value + ir.getScriptedData().get("GPI_EGOCENTRIC") +  varIdEnd;
				theXML += varIdStart + "Manipulation" + value + ir.getScriptedData().get("GPI_MANIPULATION") +  varIdEnd;
				theXML += varIdStart + "Micro Managing" + value + ir.getScriptedData().get("GPI_MICRO_MANAGE") +  varIdEnd;
				theXML += varIdStart + "Passive-Agressive" + value + ir.getScriptedData().get("GPI_PASS_AGGRESS") +  varIdEnd;
			}
		}
		
		theXML += closeXML;
		//System.out.println(" theXML just before return......... " + theXML);
		return theXML;
	}

	
	/**
	 * This method is a shameless copy of the code for the regular A by D extract
	 * with a little modification to accommodate the sorted data
	 * 
	 * This now builds a temp file to hold the template.  Options that used the InputStraem
	 * to create the workbook (either directly in WorkbookFactory.create() or by creating an
	 * OPCPackage object from an InputStream and using it in the create() method) invariably
	 * ran out of heap space, while those that used a file as input were fine.
	 *
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public Workbook buildPoiXlsForAbyDAnalyticsExtract(ArrayList<ExtractItem> itemList,
				GroupReport group, ExtractParams parms)
			throws ReportDataException
	{
		//System.out.println("In buildPoiXlsForAbyDAnalyticsExtract()...");
		// Get the properties
		String pw = PropertyLoader.getProperty("com.pdi.listener.portal.application","analytics.password");
		String wbTpltName = PropertyLoader.getProperty("com.pdi.listener.portal.application","analytics.workbookName");
		
		// pull out the extension
		String ext = null;
		int i = wbTpltName.lastIndexOf('.');
		if (i > 0) {
		    ext = wbTpltName.substring(i);	// Include the '.'
		}
		if (ext == null)
		{
			throw new ReportDataException("Template file name has no extension!  Name=" + wbTpltName);
		}

		// Build the ExtractData array
		ArrayList<ExtractData> extractData = fetchExtractData(itemList, group, parms);

		// sort it
		extractData = ExtractData.sortExtractData(parms.getSortId(), extractData);
		
		// Suppress fields as needed
		extractData = hideMe(extractData, H_NAME, parms.getShowName());
		extractData = hideMe(extractData, H_DLCI, parms.getShowLci());
		
		InputStream is = null;
		Workbook workbook = null;

		try
		{
			is = ReportHelper.class.getClassLoader().getResourceAsStream("templates/" + wbTpltName);
			try
			{
				workbook = WorkbookFactory.create(is);
			}
			catch (InvalidFormatException e)
			{  throw new ReportDataException("Unable to create workbook (format).  msg=" + e.getMessage());  }
			catch (IOException e)
			{  throw new ReportDataException("Unable to create workbook (IO).  msg=" + e.getMessage());  }
		
			Sheet worksheet = workbook.getSheetAt(0);
			// Unprotect the sheet that we are going to fill
			if(worksheet.getProtect())
			{
				worksheet.protectSheet(null);
				System.out.println("Protection removed");
			}
	
			// Clear the sheet of extraneous data
			while (worksheet.getPhysicalNumberOfRows() > 0)
			{
				worksheet.removeRow(worksheet.getRow(worksheet.getLastRowNum()));
			}
			
//			// Code to put out a spreadsheet with this data only
//			workbook = new XSSFWorkbook();
//			Sheet worksheet = workbook.createSheet("A by D extract");

			// create all the cell styles....
			// Create a new font and alter it.
		    Font fontRegular = workbook.createFont();
		    fontRegular.setFontHeightInPoints((short)8);
		    fontRegular.setFontName("Arial");
			
		    Font fontBold = workbook.createFont();
		    fontBold.setFontHeightInPoints((short)8);
		    fontBold.setFontName("Arial");
		    fontBold.setBoldweight(Font.BOLDWEIGHT_BOLD);
	
		    // Set up the styles
			CellStyle cellStyleString = workbook.createCellStyle();
			cellStyleString.setFillForegroundColor(IndexedColors.WHITE.getIndex());
			//cellStyleString.setFillPattern(CellStyle.SOLID_FOREGROUND);
			cellStyleString.setFont(fontRegular);
			
			CellStyle cellStyleStringBold = workbook.createCellStyle();  
			cellStyleStringBold.setFillForegroundColor(IndexedColors.WHITE.getIndex());
			//cellStyleStringBold.setFillPattern(CellStyle.SOLID_FOREGROUND);
			cellStyleStringBold.setFont(fontBold);
			cellStyleStringBold.setWrapText(true);
			// Assumes that the Bold String is Header.
			// If that is not the case, bake a header cell or get rid of this
			cellStyleStringBold.setAlignment(CellStyle.ALIGN_CENTER); 
			
			DataFormat fmt = workbook.createDataFormat();
			CellStyle cellStyleInt = workbook.createCellStyle();  
			cellStyleInt.setFillForegroundColor(IndexedColors.WHITE.getIndex());
			//cellStyleInt.setFillPattern(CellStyle.SOLID_FOREGROUND);
			cellStyleInt.setFont(fontRegular);
			cellStyleInt.setDataFormat(fmt.getFormat("0"));
			
			CellStyle cellStyleFloat = workbook.createCellStyle();  
			cellStyleFloat.setFillForegroundColor(IndexedColors.WHITE.getIndex());
			//cellStyleFloat.setFillPattern(CellStyle.SOLID_FOREGROUND);
			cellStyleFloat.setFont(fontRegular);
			cellStyleFloat.setDataFormat(fmt.getFormat("0.00"));
			
			cellStyleFloat.setDataFormat(fmt.getFormat("0.00"));
	
			int rowNum = -2;	// Starts at -2 because there are 2 header rows
			//--------------------------------------------
					// 	HEADING ROW #1 
			//---------------------------------------------		
			// Row 1 is assumed all text (default)
			//System.out.println("loop 1 = rowNum = " + rowNum);	
			//build the first header row
			Row rowH1 = null;		
			rowH1 = worksheet.createRow((short) (rowNum + 2));
			int cellNum = 0;
			
			for(ExtractItem itm : itemList)
			{
				// one cell per item... 
				Cell cell = rowH1.createCell(cellNum); 
					
				if (itm.getSrc() == ExtractItem.SRC_HDG)  // heading row.... 
				{
					// index from 0,0... cell A1 is cell(0,0)
					// Headings  happen in row 0, 1
					if (rowNum == -2)
					{
						cell.setCellValue(itm.getDispName());
						cell.setCellStyle(cellStyleStringBold);
						cellNum++;
					}
				}
				else if (itm.getSrc() == ExtractItem.SRC_GRP)
				{
					// Headings  happen in row 0, 1
					if (rowNum == -2)
					{	
						if(! itm.getKey().endsWith("_Score")){
							// doing this so that it puts the items next to each other 
							cell.setCellValue(itm.getDispName());
							cell.setCellStyle(cellStyleStringBold);
							cellNum++;
						}
					}
				}
				else if (itm.getSrc() == ExtractItem.SRC_INDV)
				{
					// Headings  happen in row 0,1
					if (rowNum == -2)
					{
						if( itm.getKey().endsWith("_Score") ||
						    itm.getKey().endsWith("_FinalScore") ||
							   (itm.getKey().startsWith("PDA_") && ! itm.getKey().endsWith("_HDG")) ||
							   (itm.getKey().startsWith("CPDA_") && ! itm.getKey().endsWith("_CFNAME")))
						{
							// Do nothing
						}  else  {	
							cell.setCellValue(itm.getDispName());
							cell.setCellStyle(cellStyleStringBold);	
							cellNum++;
						}
					}
				}
				else if (itm.getSrc() == ExtractItem.SRC_PARM)
				{
					// Headings happen in row 0 for parms
					cell.setCellValue(itm.getDispName());
					cell.setCellStyle(cellStyleStringBold);	
					cellNum++;
	
				} else {
					// Must be an RDx (Report Data) label -- name of the instrument				
					if (itm.getSrc() == ExtractItem.SRC_RDS)
					{
						// Headings happen in row 0, 1
						if (rowNum == -2)
						{							
							cell.setCellValue( itm.getDispName());
							cell.setCellStyle(cellStyleStringBold);
							cellNum++;
						}
					}
				}
			}	// End the itm for loop
			
			rowNum++;
			
			//--------------------------------------------
			// 	HEADING ROW #2 
			//---------------------------------------------		
			// Row 2 is assumed all text (default)
			// Not going to use the extract data at this point
			// Use the data first row for the data gotten here (comp names only)
			//System.out.println("loop 2 = rowNum = " + rowNum);	
			IndividualReport firstIr = group.getIndividualReports().get(0)	;	
			// Build a row  (2nd header row)
			Row rowH2 = null;		
			rowH2 = worksheet.createRow((short) (rowNum + 2));
	
			cellNum = 0;
	
			for(ExtractItem itm : itemList)
			{
				// one cell per item... (a lot of them are blank)
				Cell cell = rowH2.createCell(cellNum); 
				if(  itm.getKey().endsWith("_Score") ||
					 itm.getKey().endsWith("_FinalScore")  ||
					(itm.getKey().startsWith("PDA_") && ! itm.getKey().endsWith("_HDG"))  ||
					(itm.getKey().startsWith("CPDA_") && ! itm.getKey().endsWith("_CFNAME"))	)
				{
					// skipping these cells on purpose for this row
					continue;
				}
				if (itm.getSrc() == ExtractItem.SRC_HDG)  // heading row.... 
				{
					// no headings type data in this row
				}
				else if (itm.getSrc() == ExtractItem.SRC_GRP)
				{
					// Headings  happen in row 0, 1
					if(rowNum == -1)
					{
						if( ! itm.getKey().equals("ClientName")  &&
							! itm.getKey().equals("ProjectName") &&
							! itm.getKey().equals("Model") )
						{
							// we don't want to see actual data, so excluding those					
							cell.setCellValue(group.getDisplayData(itm.getKey()));
							cell.setCellStyle(cellStyleStringBold);	
						}
					}
				}
				else if (itm.getSrc() == ExtractItem.SRC_INDV)
				{
					if(rowNum == -1)
					{
						if(  itm.getKey().startsWith("IG_Comp")  ||
							(itm.getKey().startsWith("PDA_") && itm.getKey().endsWith("_HDG"))  ||
							(itm.getKey().startsWith("CPDA_") && itm.getKey().endsWith("_CFNAME")) )
						{
							String s = firstIr.getDisplayData(itm.getKey());
							if (s != null && s.length() > MAX_CELL_SIZE)
							{
								s = s.substring(0,MAX_CELL_SIZE-1) + TRUNC_MSG;
							} 			
							cell.setCellValue(s);
							cell.setCellStyle(cellStyleStringBold);				
						}
					}
				}
				else if (itm.getSrc() == ExtractItem.SRC_PARM)
				{
					// no headings type data in this row for parms
				}
				
				cellNum++;
			}	// End the itm for loop
			rowNum++;
							
			//-----------------------------------------
			// LOOP 3 -- data loop
			//-----------------------------------------	
			// This is the real one... We haven't put out real data until now
			
			//System.out.println(" LOOP 3 :  rowNum=" + rowNum);		
	
	
			// Put out data one line per data object
			int ccIdx = 0;
			int igIdx = 0;
			int cfIdx = 0;
			for (ExtractData ed : extractData)
			{
				ccIdx = 0;
				igIdx = 0;
				cfIdx = 0;
				//build a row  - will need a row for each participant
				Row row = null;		
				row = worksheet.createRow((short) (rowNum + 2));
			
				cellNum = 0;
				
				for(ExtractItem itm : itemList)
				{
					Cell cell = row.createCell(cellNum);
	
					Object obj = null;
					if (itm.getSrc() == ExtractItem.SRC_HDG)  
					{
						//	DON'T PUT OUT HEADINGS IN A PPT ROW
						continue;
					}
					else if (itm.getSrc() == ExtractItem.SRC_GRP)
					{
						if(itm.getKey().endsWith("_Name"))
						{
							// Skip it
							continue;
						}
						else
						{
							// we only want "score" data... 
							obj = ed.getAttribute(itm.getKey());
						}
					}
					else if (itm.getSrc() == ExtractItem.SRC_INDV)
					{
						// non-header data
						if(  itm.getKey().endsWith("_Name")  ||
							(itm.getKey().startsWith("PDA_") && itm.getKey().endsWith("_HDG"))  ||
							(itm.getKey().startsWith("CPDA_") && itm.getKey().endsWith("_CFNAME")))
						{
							// Skip it
							continue;
						} else {
							obj = ed.getAttribute(itm.getKey());										
						}
					}
					else if (itm.getSrc() == ExtractItem.SRC_PARM)
					{
						obj = ed.getAttribute(itm.getKey());										
					} else {
						// Must be an RDx label
						if (itm.getSrc() == ExtractItem.SRC_RDD)
						{	
							// non-header data
							obj = ed.getAttribute(itm.getKey());
						}
						else if (itm.getSrc() == ExtractItem.SRC_RDS)
						{
							// then put out the score data
							obj = ed.getAttribute(itm.getKey());
						}
					}
	
					// We now have the data... vet it
					if (obj == null)
					{
						if (itm.getCellType() == ExtractItem.DATA_INT ||
								itm.getCellType() == ExtractItem.DATA_FLOAT)
						{
							cell.setCellType(Cell.CELL_TYPE_BLANK);
						} else {
							// This must be a string type
							// The problem with setting it blank is that stuff to the left of the cell will
							// extend right into blank cells.  Make it a string and set it to an empty string.
							cell.setCellValue("");
						}
					} else {
						// We have real data
						if (obj instanceof ArrayList<?>)
						{
							// Deal with floats
							if ((itm.getKey().startsWith("CC_SF") && itm.getKey().endsWith("_Score"))  ||
								(itm.getKey().startsWith("IG_") && itm.getKey().endsWith("_FinalScore")) )
							{
								// This is a competency score - we deal with floats
		
								// The idx thing only works because we put them in in order
								Float fltVal = null;
								if (itm.getKey().startsWith("CC_SF") && itm.getKey().endsWith("_Score"))
								{
									// This is a client competency
									fltVal = ((ArrayList<Float>)obj).get(ccIdx);
									ccIdx++;
								}
								else if (itm.getKey().startsWith("IG_") && itm.getKey().endsWith("_FinalScore"))
								{
									// This is an integration grid competency
									fltVal = ((ArrayList<Float>)obj).get(igIdx);
									igIdx++;
								}
								
								if (fltVal == null)
								{
									cell.setCellType(Cell.CELL_TYPE_BLANK);
								} else {
									float val = fltVal.floatValue();
									if (val == (float)ExtractData.INSUF_DATA_NUM)
									{
										cell.setCellValue("Insufficient Data");
										cell.setCellStyle(cellStyleString);
									} else {
										cell.setCellValue(val);
										cell.setCellStyle(cellStyleFloat);
									}
								}
							}
							else if(itm.getKey().startsWith("CPDA_") && itm.getKey().endsWith("_CFVALUE"))
							{
								// This is a custom field
								// The idx thing only works because we put them in in order
								String strVal = ((ArrayList<String>)obj).get(cfIdx);
								cfIdx++;
								
								if (strVal == null)
								{
									cell.setCellType(Cell.CELL_TYPE_BLANK);
								}
								else
								{
									cell.setCellValue(strVal);
									cell.setCellStyle(cellStyleString);
								}
							}
							else
							{
								// bogus... blank it
								cell.setCellType(Cell.CELL_TYPE_BLANK);
							}
						}	// End of "object is an arraylist" logic
						else if (itm.getCellType() == ExtractItem.DATA_INT)
						{
							int val = ((Integer)obj).intValue();
							if (val == ExtractData.INSUF_DATA_NUM)
							{
								cell.setCellValue("Insufficient Data");
								cell.setCellStyle(cellStyleString);
							} else {
								cell.setCellValue(val);
								cell.setCellStyle(cellStyleInt);
							}
						}
						else if(itm.getCellType() == ExtractItem.DATA_FLOAT)
						{
							float val = ((Float)obj).floatValue();
							if (val == (float)ExtractData.INSUF_DATA_NUM)
							{
								cell.setCellValue("Insufficient Data");
								cell.setCellStyle(cellStyleString);
							} else {
								cell.setCellValue(val);
								cell.setCellStyle(cellStyleFloat);
							}
						} else {
							// Must be a string
							String val = (String)obj;
							if (val.length() > MAX_CELL_SIZE)
							{
								val = val.substring(0,MAX_CELL_SIZE-1) + TRUNC_MSG;
							}
							cell.setCellValue(val);
							cell.setCellStyle(cellStyleString);
						}
					}
					cellNum++;
				}	// End the itm for loop
				
				rowNum++;
			}	// End the ExtractData 'for' loop
		
			// Force recalc andprotect the sheet
			workbook.setForceFormulaRecalculation(true);
			worksheet.protectSheet(pw);
			//System.out.println("4: sheet protected=" + worksheet.getProtect());
		}	
		finally {
			// Clean up the stuff we don't need any more
			try
			{
				if (is != null)
				{
					is.close();
					is = null;
				}
			} catch (IOException e)
			{  /* Swallow the error... done with it by now */  	}
		}
		
		return workbook;		
	}	


	/**
	 * fetchExtractData - Move the extract data from the HashMaps in the GroupReport object to
	 * 					  an array of ExtractData items
	 * @param itemList
	 * @param group
	 * @return
	 */
	private ArrayList<ExtractData> fetchExtractData(ArrayList<ExtractItem> itemList, GroupReport group, ExtractParams parms)
	{
		ArrayList<ExtractData> ret = new ArrayList<ExtractData>();
		for (IndividualReport ir : group.getIndividualReports())
		{
			ExtractData ed = new ExtractData();
			for(ExtractItem itm : itemList)
			{
				String val = null;
				String key = itm.getKey();
				if (itm.getSrc() == ExtractItem.SRC_GRP)
				{
					val = group.getDisplayData(itm.getKey());
				}
				else if (itm.getSrc() == ExtractItem.SRC_INDV)
				{
					val = ir.getDisplayData(itm.getKey());
				}
				else if (itm.getSrc() == ExtractItem.SRC_PARM)
				{
					if (itm.getKey().equals("SortGroupName"))
					{
						val = parms.getSortGroupName();
					}
					else if (itm.getKey().equals("SortDispName"))
					{
						val = parms.getSortDispName();
					}
					else if (itm.getKey().equals("AxisLabels"))
					{
						val = parms.getAxisLabels();
					}
					else
					{
						val = "N/A";
					}
				}
				else
				{
					// Must be an RDx label
					ReportData rd = null;
					// Get the appropriate ReportData object
					rd = ir.getReportData().get(itm.getRptKey());
					if (rd == null)
						rd = new ReportData();
					if (itm.getSrc() == ExtractItem.SRC_RDD)
					{	
						// non-header data
						val = rd.getDisplayData(itm.getKey());
					}
					else if (itm.getSrc() == ExtractItem.SRC_RDS)
					{
						// then put out the score data
						val = rd.getScoreData(itm.getKey());
					}
				}
				
				ed.setAttribute(key, val, itm.getCellType());
			}	// End of itm 'for' loop
		
			ret.add(ed);
		}	// End of ir "for" loop

		
		return ret;
	}
	
	
	/**
	 * Blank out the data for the indicated field
	 * @param extractData
	 * @param showName
	 * @param showLci
	 * @return
	 */
	private ArrayList<ExtractData> hideMe(ArrayList<ExtractData> extractData, String fieldName, boolean showFlg)
	{
		boolean doIt = true;

		if ( ! (fieldName.equals(H_NAME) ||
				fieldName.equals(H_DLCI)))
		{
			doIt = false;
			System.out.println("ReportHelper.hideMe():  Invalid field name (" + fieldName + ").  Processing skipped.");
		}
		if (showFlg)
		{
			doIt = false;
		}
			
		// Do it only if we have to
		if (doIt)
		{
			for (ExtractData ed : extractData)
			{
				if (fieldName.equals(H_NAME))
				{
					ed.setPptFname(null);
					ed.setPptLname(null);
				}
				else if (fieldName.equals(H_DLCI))
				{
					ed.setIgDlci(null);
				}
			}	// End of for loop
		}
		
		return extractData;
	}
	
	public Workbook buildPoiXlsForALRExtract(ArrayList<ExtractItem> itemList, GroupReport group)
			throws ReportDataException
	{
		//System.out.println( "buildPoiXlsForAbyDExtract..... 820");
		
		// start setting up spreadsheet
		Workbook workbook = new XSSFWorkbook();
		
		
		// add watermark to title field
		POIXMLProperties xmlProps = ((POIXMLDocument) workbook).getProperties();
		POIXMLProperties.CoreProperties coreProps = xmlProps.getCoreProperties();
		coreProps.setTitle(ReportingConstants.WATERMARK);
		
		Sheet worksheet = workbook.createSheet("Worksheet");
		worksheet.setDefaultColumnWidth(10);
		
		// create all the cell styles....
		// Create a new font and alter it.
	    Font fontRegular = workbook.createFont();
	    fontRegular.setFontHeightInPoints((short)8);
	    fontRegular.setFontName("Arial");
		
	    Font fontBold = workbook.createFont();
	    fontBold.setFontHeightInPoints((short)8);
	    fontBold.setFontName("Arial");
	    fontBold.setBoldweight(Font.BOLDWEIGHT_BOLD);

	    // Set up the styles
		CellStyle cellStyleString = workbook.createCellStyle();
		cellStyleString.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		//cellStyleString.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyleString.setFont(fontRegular);
		
		CellStyle cellStyleStringBold = workbook.createCellStyle();  
		cellStyleStringBold.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		//cellStyleStringBold.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyleStringBold.setFont(fontBold);
		cellStyleStringBold.setWrapText(true);
		
		DataFormat fmt = workbook.createDataFormat();
		CellStyle cellStyleInt = workbook.createCellStyle();  
		cellStyleInt.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		//cellStyleStringBold.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyleInt.setFont(fontRegular);
		cellStyleInt.setDataFormat(fmt.getFormat("0"));
		
		CellStyle cellStyleFloat = workbook.createCellStyle();  
		cellStyleFloat.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		//cellStyleFloat.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyleFloat.setFont(fontRegular);
		cellStyleFloat.setDataFormat(fmt.getFormat("0.00"));
		
		CellStyle cellStyleFloat_3 = workbook.createCellStyle();  
		cellStyleFloat_3.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		//cellStyleFloat.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyleFloat_3.setFont(fontRegular);
		cellStyleFloat_3.setDataFormat(fmt.getFormat("0.000"));

		CellStyle cellStyleFloat_5 = workbook.createCellStyle();
		cellStyleFloat_5.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		cellStyleFloat_5.setFont(fontRegular);
		cellStyleFloat_5.setDataFormat(fmt.getFormat("0.00000"));

		CellStyle cellStyleFloat_9 = workbook.createCellStyle();
		cellStyleFloat_9.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		cellStyleFloat_9.setFont(fontRegular);
		cellStyleFloat_9.setDataFormat(fmt.getFormat("0.000000000"));

		CellStyle cellStyleDate = workbook.createCellStyle();  
		cellStyleDate.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		//cellStyleFloat.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyleDate.setFont(fontRegular);
		cellStyleDate.setDataFormat(fmt.getFormat("MM/dd/yyyy"));
		
				
		int pCnt = 0;
		int rowNum = -2;
		//--------------------------------------------
				// 	HEADING ROW #1 
		//---------------------------------------------		
		// Row 1 is assumed all text (default)
		//System.out.println("loop 1 = rowNum = " + rowNum);		
		for (@SuppressWarnings("unused") IndividualReport ir : group.getIndividualReports())
		{
			if(pCnt >= 1)
				break;
			
			pCnt++;
			//build a row  - will need a row for each participant
			Row rowH = null;		
			rowH = worksheet.createRow((short) (rowNum + 2));
		
			int cellNum = 0;
						
			for(ExtractItem itm : itemList)
			{
				// one cell per item... 
				Cell cell = rowH.createCell(cellNum); 
				
				if (itm.getSrc() == ExtractItem.SRC_HDG)  // heading row.... 
				{
					// index from 0,0... cell A1 is cell(0,0)
					// Headings  happen in row 0, 1
					if (rowNum == -2)
					{
						cell.setCellValue(itm.getDispName());
						cell.setCellStyle(cellStyleStringBold);
						cellNum++;
					}
				}
				else if (itm.getSrc() == ExtractItem.SRC_GRP)
				{
					// Headings  happen in row 0, 1
					if (rowNum == -2)
					{	

						if(itm.getKey().endsWith("_Dna_Name")) {
							continue;
						}
						
						if(!itm.getKey().endsWith("_Score")){
							// doing this so that it puts the items next to each other 
							cell.setCellValue(itm.getDispName());
							cell.setCellStyle(cellStyleStringBold);
							cellNum++;
						}
					}
				}
				else if (itm.getSrc() == ExtractItem.SRC_INDV)
				{
					// Headings  happen in row 0,1
					if (rowNum == -2)
					{
						if( itm.getKey().endsWith("_Score") ||
							itm.getKey().endsWith("_KFLAName") ||
						    itm.getKey().endsWith("_FinalScore") ||
						    itm.getKey().endsWith("_CalcScore") ||
						  (itm.getKey().startsWith("CPDA_") && ! itm.getKey().endsWith("_CFNAME")) )
						{	
							// Exclude me!
							// Do nothing
						}  else  {
						// Not part of the exclusions... go ahead and put it out
							cell.setCellValue(itm.getDispName());
							cell.setCellStyle(cellStyleStringBold);	
							cellNum++;
						}
					}
				} else {
					// Must be an RDx (Report Data) label -- name of the instrument				
					if (itm.getSrc() == ExtractItem.SRC_RDS)
					{
						// Headings happen in row 0, 1
						if (rowNum == -2)
						{							
							cell.setCellValue( itm.getDispName());
							cell.setCellStyle(cellStyleStringBold);
							cellNum++;
						}
					}
				}
			}	// End the itm for loop
			
			rowNum++;
		}	// End the IR for loop		
				
		//--------------------------------------------
		// 	HEADING ROW #2 
		//---------------------------------------------		
		// Row 2 is assumed all text (default)

		pCnt = 0;
		//System.out.println("loop 2 = rowNum = " + rowNum);			
		for (IndividualReport ir : group.getIndividualReports())
		{
			if(pCnt >= 1)
				break;
			
			pCnt++;
			//build a row  - will need a row for each participant
			Row rowH = null;		
			rowH = worksheet.createRow((short) (rowNum + 2));
		
			int cellNum = 0;
			
			for(ExtractItem itm : itemList)
			{
				// one cell per item... 
				Cell cell = rowH.createCell(cellNum); 

				if( ( itm.getKey().endsWith("_Score") ||
					  itm.getKey().endsWith("_KFLAName") ||
					  itm.getKey().endsWith("_FinalScore"))  ||
					  itm.getKey().endsWith("_CalcScore") ||
					 //(itm.getKey().startsWith("PDA_") && ! itm.getKey().endsWith("_HDG")) ||
					 (itm.getKey().startsWith("CPDA_") && ! itm.getKey().endsWith("_CFNAME")) ||
					 itm.getKey().endsWith("_Dna_Name"))
				{
					// skipping these cells on purpose for this row
					continue;
				}
				if (itm.getSrc() == ExtractItem.SRC_HDG)  // heading row.... 
				{
					// no headings in this row
					
				}
				else if (itm.getSrc() == ExtractItem.SRC_GRP)
				{
					// Headings  happen in row 0, 1
					if(rowNum == -1)
					{	
						if( !itm.getKey().equals("ClientName") && !itm.getKey().equals("ProjectName")  
								&& !itm.getKey().equals("Model") && !itm.getKey().equals("ExtractDate")
								&& !itm.getKey().equals("Rpt_Note") && !itm.getKey().equals("dlciMean")
								&& !itm.getKey().equals("dlciStdDev") && !itm.getKey().equals("wbVersion")
								&& !itm.getKey().endsWith("_Dna_Name")){
							// we don't want to see actual data, so excluding those					
							cell.setCellValue(group.getDisplayData(itm.getKey()));
							cell.setCellStyle(cellStyleStringBold);	
						}
					}
				}
				else if (itm.getSrc() == ExtractItem.SRC_INDV)
				{
					if(rowNum == -1)
					{
						if( itm.getKey().startsWith("IG_Comp") || itm.getKey().startsWith("KFLA_COMP") ||
						   //(itm.getKey().startsWith("PDA_") && itm.getKey().endsWith("_HDG")) ||
						   (itm.getKey().startsWith("CPDA_") && itm.getKey().endsWith("_CFNAME"))
								){
					
							String s = ir.getDisplayData(itm.getKey());
							if (s != null && s.length() > MAX_CELL_SIZE)
							{
								s = s.substring(0,MAX_CELL_SIZE-1) + TRUNC_MSG;
							} 			
							cell.setCellValue(s);
							cell.setCellStyle(cellStyleStringBold);				
						}
					}
				}
		
				cellNum++;
			}	// End the itm for loop
			
			rowNum++;
		}	// End the IR for loop			
				
		//-----------------------------------------
		// LOOP 3 -- data loop
		//-----------------------------------------	
		// Data loops starts checking the data type
		
		//System.out.println(" LOOP 3 :  rowNum=" + rowNum);		
		// reset pCnt
		pCnt = 0;
		
		for (IndividualReport ir : group.getIndividualReports())
		{
			
			pCnt++;
			//build a row  - will need a row for each participant
			Row rowH = null;		
			rowH = worksheet.createRow((short) (rowNum + 2));
		
			int cellNum = 0;
			boolean isNumeric = true;
			
			for(ExtractItem itm : itemList)
			{
				Cell cell = rowH.createCell(cellNum);

				// Get the data from the appropriate source (will be a string)
				String valStr = "";
				if (itm.getSrc() == ExtractItem.SRC_HDG)  
				{
					//	DON'T NEED HEADINGS
					continue;
				}
				else if (itm.getSrc() == ExtractItem.SRC_GRP)
				{
					if(itm.getKey().endsWith("_Name") && !itm.getKey().endsWith("_Dna_Name"))
					{
						// Skip it
						continue;
					}
					else
					{
						// we only want "score" data... 
						valStr = group.getDisplayData(itm.getKey());
					}
				}
				else if (itm.getSrc() == ExtractItem.SRC_INDV)
				{
					// non-header data
					if(itm.getKey().endsWith("_Name")		||	// IG_Comp
					   itm.getKey().endsWith("_HDG")		||	// PDA stuff
					   itm.getKey().endsWith("_CFNAME") 	)	// Custom Field (PDA) stuff
					{
						// Skip it
						continue;
					}else if (group.isMultiProject() && 
							itm.getKey().startsWith("CC_SF") && 
							itm.getKey().endsWith("_Score")){
						// Data not valid in multi project reports
						valStr = "-";
					} else {
						valStr = ir.getDisplayData(itm.getKey());											
					}
				} else {
					// Must be an RDx label
					ReportData rd = null;
					// Get the appropriate ReportData object
					rd = ir.getReportData().get(itm.getRptKey());
					if (rd == null)
						rd = new ReportData();

					if (itm.getSrc() == ExtractItem.SRC_RDD)
					{	
						// non-header data
						valStr = rd.getDisplayData(itm.getKey());
					}
					else if (itm.getSrc() == ExtractItem.SRC_RDS)
					{
						// then put out the score data
						valStr = rd.getScoreData(itm.getKey());
					}
				}
				
				// Got the data... vet it
				if (valStr == null || valStr.length() < 1 || valStr.equals("NaN"))
				{
					// set it for nothing and scram
					//cell.setCellType(Cell.CELL_TYPE_BLANK);
					
					if (itm.getCellType() == ExtractItem.DATA_INT)
					{
						//System.out.println(" empty value:   itm.getCellType() == ExtractItem.DATA_INT");
						cell.setCellType(Cell.CELL_TYPE_BLANK);
						
					}
					else if(itm.getCellType() == ExtractItem.DATA_FLOAT)
					{
						//System.out.println(" empty value:   itm.getCellType() == ExtractItem.DATA_FLOAT");
						cell.setCellType(Cell.CELL_TYPE_BLANK);
						
					}
					else
					{
						// The problem with setting it blank is that stuff to the left of the cell will
						// extend right into blank cells.  Make it a string and set it to an empty string.
						cell.setCellValue("");
					}
				} else {
					// We have real data
					if (valStr.length() > MAX_CELL_SIZE)
					{
						valStr = valStr.substring(0,MAX_CELL_SIZE-1) + TRUNC_MSG;
					}
					// set up the type and format
					double valNum = 0.0;
					if (itm.getCellType() == ExtractItem.DATA_INT)
					{
						try {
							isNumeric = true;
							valNum = Double.parseDouble(valStr);
						} catch (NumberFormatException nfe) {
							isNumeric = false;
						}
						if (isNumeric) {
							cell.setCellValue(valNum);
							cell.setCellStyle(cellStyleInt);
						} else {
							cell.setCellValue(valStr);
							cell.setCellStyle(cellStyleString);
						}
					}
					else if(itm.getCellType() == ExtractItem.DATA_FLOAT)
					{
						try
						{
							isNumeric = true;
							valNum = Double.parseDouble(valStr);
						}
						catch (NumberFormatException nfe)
						{
							isNumeric = false;
						}
						if (isNumeric)
						{
							cell.setCellValue(valNum);
							cell.setCellStyle(cellStyleFloat);
						} else {
							cell.setCellValue(valStr);
							cell.setCellStyle(cellStyleString);
						}
					}
					
					else if(itm.getCellType() == ExtractItem.DATA_FLOAT_3)
					{
						try
						{
							isNumeric = true;
							valNum = Double.parseDouble(valStr);
						}
						catch (NumberFormatException nfe)
						{
							isNumeric = false;
						}
						if (isNumeric)
						{
							cell.setCellValue(valNum);
							cell.setCellStyle(cellStyleFloat_3);
						} else {
							cell.setCellValue(valStr);
							cell.setCellStyle(cellStyleString);
						}
					}
					
					else if(itm.getCellType() == ExtractItem.DATA_FLOAT_9)
					{
						try
						{
							isNumeric = true;
							valNum = Double.parseDouble(valStr);
						}
						catch (NumberFormatException nfe)
						{
							isNumeric = false;
						}
						if (isNumeric)
						{
							cell.setCellValue(valNum);
							cell.setCellStyle(cellStyleFloat_9);
						} else {
							cell.setCellValue(valStr);
							cell.setCellStyle(cellStyleString);
						}
					}
					else if(itm.getCellType() == ExtractItem.DATA_FLOAT_5)
					{
						try
						{
							isNumeric = true;
							valNum = Double.parseDouble(valStr);
						}
						catch (NumberFormatException nfe)
						{
							isNumeric = false;
						}
						if (isNumeric)
						{
							cell.setCellValue(valNum);
							cell.setCellStyle(cellStyleFloat_5);
						} else {
							cell.setCellValue(valStr);
							cell.setCellStyle(cellStyleString);
						}
					}
						
					else if(itm.getCellType() == ExtractItem.DATA_DATE)
					{
						
						cell.setCellValue(valStr);
						cell.setCellStyle(cellStyleDate);
						
					} 
					else {
						cell.setCellValue(valStr);
						cell.setCellStyle(cellStyleString);
					}
				}
				cellNum++;

			}	// End the itm for loop
			
			rowNum++;
		}	// End the IR for loop
		
		return workbook;		
	}	
	
	
	/*
	 * generateRank
	 */
	public void generateRank(IndividualReport individualReport)
	{
		//System.out.println("start rank...") ;
		//This is a fairly complicated ranking algorithm... 
		double leadershipExperience = individualReport.getScriptedGroupData().get("LE_EXPER_FINALSCORE");
		double leadershipInterest = individualReport.getScriptedGroupData().get("LE_INTER_FINALSCORE");
		double leadershipFoundations = individualReport.getScriptedGroupData().get("LE_FOUND_FINALSCORE");
	
		double derailment = 0;
		if(individualReport.getScriptedData().get(ReportingConstants.DR_DERAIL) != null)
		{
			derailment = 10 - Double.parseDouble(individualReport.getScriptedData().get(ReportingConstants.DR_DERAIL));
		}
		//System.out.println(	"LE_EXP=" + leadershipExperience + ", LE_INT=" + leadershipInterest + ", LE_FND=" + leadershipFoundations + 	", DR=" + derailment );
			
		double rank = 0;
		if(leadershipExperience > 0 && leadershipInterest > 0 && leadershipFoundations > 0) {
			rank = 800;
		} else if(leadershipExperience < 1 && leadershipInterest > 0 && leadershipFoundations > 0) {
			rank = 700;
		} else if(leadershipExperience > 0 && leadershipInterest < 1 && leadershipFoundations > 0) {
			rank = 600;
		} else if(leadershipExperience > 0 && leadershipInterest > 0 && leadershipFoundations < 1) {
			rank = 500;
		} else if(leadershipExperience < 1 && leadershipInterest > 0 && leadershipFoundations < 1) {
			rank = 400;
		} else if(leadershipExperience < 1 && leadershipInterest < 1 && leadershipFoundations > 0) {
			rank = 300;
		} else if(leadershipExperience > 0 && leadershipInterest < 1 && leadershipFoundations < 1) {
			rank = 200;
		} else {
			rank = 100;
		}
		rank = rank + derailment;		
		individualReport.setRank(new Double(rank).intValue());
	}


	/*
	 * create an ArrayList of reports in ranked order, based on the generated ranks
	 */
	public ArrayList<IndividualReport> useGeneratedRanksToCreateReportOrder( ArrayList<IndividualReport> irs )
	{
		 Collections.sort(irs, new RankComparator());
		 return irs;
	}
	
	
	private class RankComparator implements Comparator<Object>{
		@Override
		public int compare(Object arg1, Object arg2)
		{
			int r1 = ((IndividualReport)arg1).getRank();
			int r2 = ((IndividualReport)arg2).getRank();
			
			// this is backward, because we want the biggest (highest) ranks first
			if(r1 > r2)
				return -1;
			if(r1 < r2)
				return 1;
			
			// now they are the same rank... if it gets here
			String s1 = ((IndividualReport)arg1).getDisplayData().get("LAST_NAME"); 
			String s2 = ((IndividualReport)arg2).getDisplayData().get("LAST_NAME"); 
			
			int r = s1.compareToIgnoreCase(s2);
			if( r != 0)
				return r;
			
			// now they are still the same rank... if it gets here
			s1 = ((IndividualReport)arg1).getDisplayData().get("FIRST_NAME"); 
			s2 = ((IndividualReport)arg2).getDisplayData().get("FIRST_NAME"); 
			
			r = s1.compareToIgnoreCase(s2);
			if( r != 0)
				return r;
			else			
				return 1;
		}
	}


	/**
	 * buildPoiXlsForAlrDrivers - Generates the report for the Drivers Interpretation Report
	 */
	public Workbook buildPoiXlsForAlrDrivers(IndividualReport ir)
			throws ReportDataException
	{
		ArrayList<String> DRIVER_KEY_ORDER = new ArrayList<String>();
		{
			// These must be in template order
			DRIVER_KEY_ORDER.add("BALA");
			DRIVER_KEY_ORDER.add("COLL");
			DRIVER_KEY_ORDER.add("POWR");
			DRIVER_KEY_ORDER.add("CHAL");
			DRIVER_KEY_ORDER.add("STRC");
			DRIVER_KEY_ORDER.add("INDY");
		}
		int ALR_HDR_CNT = 2;
		int ALR_LBL_CNT = 1;
		
		//System.out.println("In buildPoiXlsForAlrDrivers()...");
		
		// Get the properties
		String wbTpltName = PropertyLoader.getProperty("com.pdi.listener.portal.application","alr.drivers.workbookName");

		// pull out the extension
		String ext = null;
		int i = wbTpltName.lastIndexOf('.');
		if (i > 0)
		{
			ext = wbTpltName.substring(i);	// Include the '.'
		}
		if (ext == null)
		{
			throw new ReportDataException("Template file name has no extension!  Name=" + wbTpltName);
		}

		InputStream is = null;
		Workbook workbook = null;

		try
		{
			is = ReportHelper.class.getClassLoader().getResourceAsStream("templates/" + wbTpltName);
			try
			{
				workbook = new XSSFWorkbook(is);
			}
			catch (IOException e)
			{
				throw new ReportDataException("Unable to create workbook (IO).  msg=" + e.getMessage());
			}
			
			// add watermark to title field
			POIXMLProperties xmlProps = ((POIXMLDocument) workbook).getProperties();
			POIXMLProperties.CoreProperties coreProps = xmlProps.getCoreProperties();
			coreProps.setTitle(ReportingConstants.WATERMARK);

			Sheet sheet = workbook.getSheetAt(0);

			// Assumes template is pristine

			// create all the cell styles....
			// Create a new font and alter it.
			Font fontRegular = workbook.createFont();
			fontRegular.setFontHeightInPoints((short)10);
			fontRegular.setFontName("Arial");

			DataFormat fmt = workbook.createDataFormat();
			CellStyle pctlCell = workbook.createCellStyle();  
			pctlCell.setFillForegroundColor(IndexedColors.WHITE.getIndex());
			//pctlCell.setFillPattern(CellStyle.SOLID_FOREGROUND);
			pctlCell.setAlignment(XSSFCellStyle.ALIGN_CENTER);
			pctlCell.setFont(fontRegular);
			pctlCell.setDataFormat(fmt.getFormat("0"));
			
			CellStyle rateCell = workbook.createCellStyle();  
			rateCell.setFillForegroundColor(IndexedColors.WHITE.getIndex());
			//rateCell.setFillPattern(CellStyle.SOLID_FOREGROUND);
			rateCell.setAlignment(XSSFCellStyle.ALIGN_CENTER);
			rateCell.setFont(fontRegular);
			rateCell.setDataFormat(fmt.getFormat("0.0"));
			
			CellStyle textCell = workbook.createCellStyle();  
			textCell.setFillForegroundColor(IndexedColors.WHITE.getIndex());
			//textCell.setFillPattern(CellStyle.SOLID_FOREGROUND);
			textCell.setFont(fontRegular);
			textCell.setWrapText(true);

			// The first row in the template wi8th data in it is the third row (0, 1, 2)
			int rowNo = ALR_HDR_CNT;
			for(String key : DRIVER_KEY_ORDER)
			{
				int colNo = ALR_LBL_CNT;
				int pctl;
				float rate;
				String nTxt;
				String str = null;
			    //pctl = (int)Float.parseFloat(ir.getDisplayData(key + "-pctl"));
				str = ir.getDisplayData(key + "-pctl");
				pctl = (str == null) ? 0 : (int)Float.parseFloat(str);
			    //rate = Float.parseFloat(ir.getDisplayData(key + "-rate"));
				str = ir.getDisplayData(key + "-rate");
				rate = (str == null) ? 0 : Float.parseFloat(str);
			    //rate = Float.parseFloat(ir.getDisplayData(key + "-rate"));
			    nTxt = ir.getDisplayData(key + "-text");
			    if (nTxt == null)
			    {
			    	nTxt = "Driver Interpretation not available";
			    }

			    Row row = sheet.getRow(rowNo);
				Cell cell = row.createCell(colNo); 
				cell.setCellStyle(pctlCell);
				cell.setCellValue(pctl);
				colNo++;
				cell = row.createCell(colNo); 
				cell.setCellStyle(rateCell);
				cell.setCellValue(rate);
				colNo++;
				cell = row.createCell(colNo); 
				cell.setCellStyle(textCell);
				cell.setCellValue(nTxt);
				rowNo++;
			}
		}	
		finally
		{
			// Clean up the stuff we don't need any more
			try
			{
				if (is != null)
				{
					is.close();
					is = null;
				}
			} catch (IOException e)
			{  /* Swallow the error... done with it by now */  	}
		}

		return workbook;		
	}	


	/**
	 * 	buildPoiXlsForAlrTraits - Generates the report for the Traits Interpretation Report
	 */
	public Workbook buildPoiXlsForAlrTraits(IndividualReport ir)
		throws ReportDataException
	{
		ArrayList<String> TRAIT_KEY_ORDER = new ArrayList<String>();
		{
			// These must be in template order
			// Note that these now reflect the KF4D trait abberviations.
			TRAIT_KEY_ORDER.add("ALR_OP");	// Optimism (Op)
			TRAIT_KEY_ORDER.add("ALR_SS");	// Situational Self-Awareness (Ss)
			TRAIT_KEY_ORDER.add("ALR_CP");	// Composure (Cp)
			TRAIT_KEY_ORDER.add("ALR_CR");	// Credibility (Cr)
			TRAIT_KEY_ORDER.add("ALR_CF");	// Confidence (Cf)
			TRAIT_KEY_ORDER.add("ALR_NA");	// Need for Achievement (Na)
			TRAIT_KEY_ORDER.add("ALR_PE");	// Persistence (Pe)
			TRAIT_KEY_ORDER.add("ALR_AF");	// Affiliation (Af)
			TRAIT_KEY_ORDER.add("ALR_HU");	// Humility (Hu)
			TRAIT_KEY_ORDER.add("ALR_TR");	// Trust (Tr)
			TRAIT_KEY_ORDER.add("ALR_OD");	// Openness to Differences (Od)
			TRAIT_KEY_ORDER.add("ALR_FO");	// Focus (Fo)
			TRAIT_KEY_ORDER.add("ALR_CU");	// Curiosity (Cu)
			TRAIT_KEY_ORDER.add("ALR_AD");	// Adaptability (Ad)
			TRAIT_KEY_ORDER.add("ALR_RI");	// Risk Taking (Ri)
			TRAIT_KEY_ORDER.add("ALR_TA");	// Tolerance of Ambiguity (Ta)
			TRAIT_KEY_ORDER.add("ALR_EM");	// Empathy (Em)
			TRAIT_KEY_ORDER.add("ALR_AS");	// Assertiveness (As)
			TRAIT_KEY_ORDER.add("ALR_SO");	// Sociability (So)
			TRAIT_KEY_ORDER.add("ALR_IN");	// Influence (In)
			
			// Preserving the ALR names...
			//TRAIT_KEY_ORDER.add("ALR_AF");	// Optimism (Af)
			//TRAIT_KEY_ORDER.add("ALR_MI");	// Situational Self-Awareness (Mi)
			//TRAIT_KEY_ORDER.add("ALR_ST");	// Composure (St)
			//TRAIT_KEY_ORDER.add("ALR_CO");	// Affiliation (Co)
			//TRAIT_KEY_ORDER.add("ALR_DU");	// Focus (Du)
			//TRAIT_KEY_ORDER.add("ALR_IE");	// Curiosity (Ie)
			//TRAIT_KEY_ORDER.add("ALR_SS");	// Empathy (Ss)
			//TRAIT_KEY_ORDER.add("ALR_EN");	// Sociability (En)
		}
		//String FOCUS_KEY = "ALR_DU";
		//String FOCUS_KEY = "ALR_FO";
		
		int ALR_HDR_CNT = 2;
		int ALR_LBL_CNT = 1;
		
		//System.out.println("In buildPoiXlsForAlrTraits()...");
		
		// Get the properties
		String wbTpltName = PropertyLoader.getProperty("com.pdi.listener.portal.application","alr.traits.workbookName");

		// pull out the extension
		String ext = null;
		int i = wbTpltName.lastIndexOf('.');
		if (i > 0)
		{
			ext = wbTpltName.substring(i);	// Include the '.'
		}
		if (ext == null)
		{
			throw new ReportDataException("Template file name has no extension!  Name=" + wbTpltName);
		}
		
		InputStream is = null;
		Workbook workbook = null;
		
		try
		{
			log.debug("loading workbook template: {}", wbTpltName);
			is = ReportHelper.class.getClassLoader().getResourceAsStream("templates/" + wbTpltName);
			try
			{
				workbook = new XSSFWorkbook(is);
			}
			catch (IOException e)
			{
				throw new ReportDataException("Unable to create workbook (IO).  msg=" + e.getMessage());
			}
			
			// add watermark to title field
			POIXMLProperties xmlProps = ((POIXMLDocument) workbook).getProperties();
			POIXMLProperties.CoreProperties coreProps = xmlProps.getCoreProperties();
			coreProps.setTitle(ReportingConstants.WATERMARK);
		
			Sheet sheet = workbook.getSheetAt(0);
		
			// Assumes template is pristine
		
			// create all the cell styles....
			// Create a new font and alter it.
			Font fontRegular = workbook.createFont();
			fontRegular.setFontHeightInPoints((short)10);
			fontRegular.setFontName("Arial");
		
			DataFormat fmt = workbook.createDataFormat();
			CellStyle pctlCell = workbook.createCellStyle();  
			pctlCell.setFillForegroundColor(IndexedColors.WHITE.getIndex());
			//pctlCell.setFillPattern(CellStyle.SOLID_FOREGROUND);
			pctlCell.setAlignment(XSSFCellStyle.ALIGN_CENTER);
			pctlCell.setFont(fontRegular);
			pctlCell.setDataFormat(fmt.getFormat("0"));
			
			CellStyle rateCell = workbook.createCellStyle();  
			rateCell.setFillForegroundColor(IndexedColors.WHITE.getIndex());
			//rateCell.setFillPattern(CellStyle.SOLID_FOREGROUND);
			rateCell.setAlignment(XSSFCellStyle.ALIGN_CENTER);
			rateCell.setFont(fontRegular);
			rateCell.setDataFormat(fmt.getFormat("0.0"));
			
			CellStyle textCell = workbook.createCellStyle();  
			textCell.setFillForegroundColor(IndexedColors.WHITE.getIndex());
			//textCell.setFillPattern(CellStyle.SOLID_FOREGROUND);
			textCell.setFont(fontRegular);
			textCell.setWrapText(true);

			// The first row in the template wi8th data in it is the third row (0, 1, 2)
			int rowNo = ALR_HDR_CNT;
			for(String key : TRAIT_KEY_ORDER)
			{
				int colNo = ALR_LBL_CNT;
				int pctl;
				float rate;
				String nTxt;
				String str = null;
			    //pctl = (new Float(ir.getDisplayData(key + "-pctl"))).intValue();
				str = ir.getDisplayData(key + "-pctl");
			    pctl = str == null ? 0 : (new Float(str)).intValue();
			    // Removing the reversal of "Focus" for reporting sake per Maynard in standup (11/09/17 @ 11:00AM)
				//// Per SBA-307, "invert" (subtract from 100) the percentile on "Focus"
				//if (key.equals(FOCUS_KEY) && pctl != 0)
				//{
				//	pctl = 100 - pctl;
				//}
			    //rate = Float.parseFloat(ir.getDisplayData(key + "-rate"));
			    str = ir.getDisplayData(key + "-rate");
			    rate = str == null ? 0 : Float.parseFloat(str);
			    nTxt = ir.getDisplayData(key + "-text");
			    if (nTxt == null)
			    {
			    	nTxt = "Trait interpretation not available";
			    }

			    Row row = sheet.getRow(rowNo);
				Cell cell = row.createCell(colNo); 
				cell.setCellStyle(pctlCell);
				cell.setCellValue(pctl);
				colNo++;
				cell = row.createCell(colNo); 
				cell.setCellStyle(rateCell);
				cell.setCellValue(rate);
				colNo++;
				cell = row.createCell(colNo); 
				cell.setCellStyle(textCell);
				cell.setCellValue(nTxt);
				rowNo++;
			}
		}	
		finally
		{
			// Clean up the stuff we don't need any more
			try
			{
				if (is != null)
				{
					is.close();
					is = null;
				}
			} catch (IOException e)
			{  /* Swallow the error... done with it by now */  	}
		}
		
		return workbook;		
	}	


	/**
	 * 	buildPoiXlsForRiskFactorss - Generates the report for the Risk Factors Report
	 * 
	 * NOTE:  This is currently performed on KF4D data only
	 */
	public Workbook buildPoiXlsForRiskFactors(IndividualReport ir)
		throws ReportDataException
	{
		ArrayList<String> RISK_KEY_ORDER = new ArrayList<String>();
		{
			// These must be in template order
			RISK_KEY_ORDER.add("AVOID");	// Avoidant
			RISK_KEY_ORDER.add("CLOSE");	// Closed
			RISK_KEY_ORDER.add("DEFEN");	// Defensive
			RISK_KEY_ORDER.add("EGOTI");	// Egotistic
			RISK_KEY_ORDER.add("MICRO");	// Micro-managing
			RISK_KEY_ORDER.add("OPPOR");	// Opportunistic
			RISK_KEY_ORDER.add("RESTR");	// Restrained
			RISK_KEY_ORDER.add("SOCIA");	// Social Pleaser
			RISK_KEY_ORDER.add("SOLIT");	// Solitary
			RISK_KEY_ORDER.add("SUSPI");	// Suspicious
			RISK_KEY_ORDER.add("VOLAT");	// Volatile
		}
		int HDR_CNT = 2;
		int LBL_CNT = 2;

		//System.out.println("In buildPoiXlsForRiskFactors()...");

		// Get the properties
		String wbTpltName = PropertyLoader.getProperty("com.pdi.listener.portal.application","risk.factors.workbookName");

		// pull out the extension
		String ext = null;
		int i = wbTpltName.lastIndexOf('.');
		if (i > 0)
		{
			ext = wbTpltName.substring(i);	// Include the '.'
		}
		if (ext == null)
		{
			throw new ReportDataException("Template file name has no extension!  Name=" + wbTpltName);
		}

		InputStream is = null;
		Workbook workbook = null;

		try
		{
			is = ReportHelper.class.getClassLoader().getResourceAsStream("templates/" + wbTpltName);
			try
			{
				workbook = new XSSFWorkbook(is);
			}
			catch (IOException e)
			{
				throw new ReportDataException("Unable to create workbook (IO).  msg=" + e.getMessage());
			}

			// add watermark to title field
			POIXMLProperties xmlProps = ((POIXMLDocument) workbook).getProperties();
			POIXMLProperties.CoreProperties coreProps = xmlProps.getCoreProperties();
			coreProps.setTitle(ReportingConstants.WATERMARK);

			Sheet sheet = workbook.getSheetAt(0);
		
			// Assumes template is pristine

			// create all the cell styles....
			// Create a new font and alter it.
			Font fontRegular = workbook.createFont();
			fontRegular.setFontHeightInPoints((short)10);
			fontRegular.setFontName("Arial");

			DataFormat fmt = workbook.createDataFormat();
			CellStyle pctlCell = workbook.createCellStyle();  
			pctlCell.setFillForegroundColor(IndexedColors.WHITE.getIndex());
			//pctlCell.setFillPattern(CellStyle.SOLID_FOREGROUND);
			pctlCell.setAlignment(XSSFCellStyle.ALIGN_CENTER);
			pctlCell.setFont(fontRegular);
			pctlCell.setDataFormat(fmt.getFormat("0"));

			// The first row in the template wi8th data in it is the third row (0, 1, 2)
			int rowNo = HDR_CNT;
			for(String key : RISK_KEY_ORDER)
			{
				int colNo = LBL_CNT;
				int pctl;
				String str = null;
				str = ir.getDisplayData(key + "-pctl");
			    pctl = str == null ? 0 : (new Float(str)).intValue();

			    Row row = sheet.getRow(rowNo);
				Cell cell = row.createCell(colNo); 
				cell.setCellStyle(pctlCell);
				cell.setCellValue(pctl);
				
				rowNo++;
			}
		}	
		finally
		{
			// Clean up the stuff we don't need any more
			try
			{
				if (is != null)
				{
					is.close();
					is = null;
				}
			} catch (IOException e)
			{  /* Swallow the error... done with it by now */  	}
		}
		
		return workbook;		
	}	


	/**
	 * 
	 */
	public Workbook buildPoiXlsForAlpRawExtract(ArrayList<ExtractItem> itemList, GroupReport group)
//				throws ReportDataException
	{
		// start setting up spreadsheet
		Workbook workbook = new XSSFWorkbook();

		// add watermark to title field
		POIXMLProperties xmlProps = ((POIXMLDocument) workbook).getProperties();
		POIXMLProperties.CoreProperties coreProps = xmlProps.getCoreProperties();
		coreProps.setTitle(ReportingConstants.WATERMARK);
		
		Sheet worksheet = workbook.createSheet("Worksheet");
		worksheet.setDefaultColumnWidth(12);

		// create all the cell styles....
		// Create a new font and alter it.
	    Font fontRegular = workbook.createFont();
	    fontRegular.setFontHeightInPoints((short)10);
	    fontRegular.setFontName("Arial");

	    Font fontBold = workbook.createFont();
	    fontBold.setFontHeightInPoints((short)10);
	    fontBold.setFontName("Arial");
	    fontBold.setBoldweight(Font.BOLDWEIGHT_BOLD);

	    // Set up the styles
		CellStyle cellStyleString = workbook.createCellStyle();
		cellStyleString.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		//cellStyleString.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyleString.setFont(fontRegular);

		CellStyle cellStyleStringBold = workbook.createCellStyle();  // assessment date:
		cellStyleStringBold.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		cellStyleStringBold.setFont(fontBold);
		cellStyleStringBold.setAlignment(CellStyle.ALIGN_CENTER);

		DataFormat fmt = workbook.createDataFormat();
		CellStyle cellStyleInt = workbook.createCellStyle();  
		cellStyleInt.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		//cellStyleStringBold.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyleInt.setFont(fontRegular);
		cellStyleInt.setDataFormat(fmt.getFormat("0"));

		CellStyle cellStyleFloat_9 = workbook.createCellStyle();
		cellStyleFloat_9.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		cellStyleFloat_9.setFont(fontRegular);
		cellStyleFloat_9.setDataFormat(fmt.getFormat("0.000000000"));

//		int pCnt = 0;
		int rowNum = 0;
		//--------------------------------------------
				// 	HEADING ROW #1 
		//---------------------------------------------		
		// Row 1 is assumed all text (default)
		Row row = null;		
		row = worksheet.createRow((short) (rowNum));

		int cellNum = 0;

		// Run the items just for the header information
		for(ExtractItem itm : itemList)
		{
			// one cell per item... 
			Cell cell = row.createCell(cellNum); 
			cell.setCellValue(itm.getDispName());
			cell.setCellStyle(cellStyleStringBold);
			cellNum++;
		}	// End the itm for loop
				
		rowNum++;
					
		//-----------------------------------------
		// LOOP 2 -- data loop
		//-----------------------------------------	
		// Data loop starts checking the data type
//		pCnt = 0;

		// Create a row per participant
		for (IndividualReport ir : group.getIndividualReports())
		{
//			pCnt++;
			//build a row  - will need a row for each participant
			row = null;		
			row = worksheet.createRow((short) (rowNum));

			cellNum = 0;
			boolean isNumeric = true;

			for(ExtractItem itm : itemList)
			{
				Cell cell = row.createCell(cellNum);

				// Get the data from the appropriate source (will be a string)
				String valStr = "";
				// All items should be sourced from individual in the ALP Raw report
				valStr = ir.getDisplayData(itm.getKey());
				// See if there is data present
				if (valStr == null || valStr.length() < 1)
				{
					// No data
					if (itm.getCellType() == ExtractItem.DATA_INT ||
						itm.getCellType() == ExtractItem.DATA_FLOAT_9)
					{
						cell.setCellType(Cell.CELL_TYPE_BLANK);
					}
					else
					{
						// The problem with setting it blank is that stuff to the left of the cell will
						// extend right into blank cells.  Make it a string and set it to an empty string.
						cell.setCellValue("");
					}
				} else {
					// We have real data
					if (valStr.length() > MAX_CELL_SIZE)
					{
						valStr = valStr.substring(0,MAX_CELL_SIZE-1) + TRUNC_MSG;
					}
					// set up the type and format
					double valNum = 0.0;
					if (itm.getCellType() == ExtractItem.DATA_INT)
					{
						try {
							isNumeric = true;
							valNum = Double.parseDouble(valStr);
						} catch (NumberFormatException nfe) {
							isNumeric = false;
						}
						if (isNumeric) {
							cell.setCellValue(valNum);
							cell.setCellStyle(cellStyleInt);
						} else {
							cell.setCellValue(valStr);
							cell.setCellStyle(cellStyleString);
						}
					}
					else if(itm.getCellType() == ExtractItem.DATA_FLOAT_9)
					{
						try
						{
							isNumeric = true;
							valNum = Double.parseDouble(valStr);
						}
						catch (NumberFormatException nfe)
						{
							isNumeric = false;
						}
						if (isNumeric)
						{
							cell.setCellValue(valNum);
							cell.setCellStyle(cellStyleFloat_9);
						} else {
							cell.setCellValue(valStr);
							cell.setCellStyle(cellStyleString);
						}
					} else {
						// Everything else is a string
						cell.setCellValue(valStr);
						cell.setCellStyle(cellStyleString);
					}
				}

				cellNum++;
			}	// End the itm for loop

			rowNum++;
		}	// End the IR for loop
		
		return workbook;		
	}	
}
