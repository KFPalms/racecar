package com.pdi.listener.portal.flex.ut;

//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.Iterator;

//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.Map;

import junit.framework.TestCase;

//import com.pdi.data.abyd.dto.common.KeyValuePairStrings;
//import com.pdi.data.dto.Project;
//import com.pdi.data.dto.ProjectParticipantInstrument;
//import com.pdi.data.dto.NormGroup;
//import com.pdi.data.dto.NormScore;
//import com.pdi.data.dto.Participant;
//import com.pdi.data.dto.SessionUser;
//import com.pdi.data.helpers.HelperDelegate;
//import com.pdi.data.helpers.interfaces.IProjectParticipantInstrumentHelper;
//import com.pdi.data.helpers.interfaces.IScoringHelper;
//import com.pdi.data.nhn.util.NhnDatabaseUtils;
//import com.pdi.data.v2.helpers.delegated.NormGroupHelper;
//import com.pdi.data.abyd.dto.common.KeyValuePairStrings;
//import com.pdi.data.dto.ProjectParticipantInstrument;
//import com.pdi.data.dto.TextPair;
import com.pdi.data.dto.EqInfo;
import com.pdi.data.v2.util.V2DatabaseUtils;
//import com.pdi.listener.portal.flex.ManualEntryService;
//import com.pdi.listener.portal.dto.EqInfo;
import com.pdi.listener.portal.flex.PortalDelegateService;
import com.pdi.ut.UnitTestUtils;

public class PortalDelegatedServiceUT extends TestCase {
	
	private PortalDelegateService _svc = null;
	
	public PortalDelegatedServiceUT(String name) 	{
		super(name);
		 _svc = new PortalDelegateService();
	}

//	public void testGetData() throws Exception {
//		System.out.println("TEST GET DATA.....................");
//		UnitTestUtils.start(this);
//		V2DatabaseUtils.setUnitTest(true);
//		NhnDatabaseUtils.setUnitTest(true);
//		
//		IProjectParticipantInstrumentHelper nhnppih = HelperDelegate.getProjectParticipantInstrumentHelper("com.pdi.data.nhn.helpers.delegated");
//		IProjectParticipantInstrumentHelper v2ppih = HelperDelegate.getProjectParticipantInstrumentHelper("com.pdi.data.v2.helpers.delegated");
//		
//		String projectId = "2";         
//		String participantId = "553269";
//		String instrumentId = "rv";
//		int mode = 1;
//		ProjectParticipantInstrument nhnppi = nhnppih.fromProjectIdParticipantIdInstrumentId(null, mode, projectId, participantId, instrumentId);
//		ProjectParticipantInstrument v2ppi = v2ppih.fromProjectIdParticipantIdInstrumentId(null, mode, projectId, participantId, instrumentId);
//		
//		
//		//NOW WE HAVE A FULL "PPI"
//		nhnppi.setScoreGroup(v2ppi.getScoreGroup());
//		
//		System.out.println("NORM THE TEST DATA.....................");
//		// now use the PPI to to get the norms.
//		
//		ArrayList<NormGroup> theNormGroups = new ArrayList<NormGroup>();
//		NormGroupHelper ngh = new NormGroupHelper();
//		HashMap<String, NormScore> normedGenPopScores = new HashMap<String, NormScore>();
//		HashMap<String, NormScore> normedSpecPopScores = new HashMap<String, NormScore>();
//		
//		SessionUser session = new SessionUser();
//		session.getMetadata().put("USERNAME", "mbpanichi");
//		session.getMetadata().put("PASSWORD", "griffin12");
//		/*
//			NOTE:  make sure that the participant data is in the pdi_participant_instrument_norm_map table, or this
//			isn't going to work.
//		*/
//		
//		// 1. get the norm groups for this participant/instrument: 
//		theNormGroups = ngh.fromParticipantInstrumentId(session, participantId, instrumentId);
//		
//		// spin through the norms -- should be 2 -- one spec pop and one gen pop
//		for(NormGroup ng : theNormGroups){
//			System.out.println("   "  + ng.getPopulation() + " : " + ng.getName());
//			
//			// get the NormScore objects for each population, and get the normed scores. 			
//			if(ng.getPopulation() == 1){
//				//GEN POP SCORES
//										//send the scores and the norm group and get back a hash map
//										// <spssValue, NormScore>
//				normedGenPopScores = ScoreGroup.calculateNormScores(nhnppi.getScoreGroup().getScores(), ng);
//				System.out.println("genPop Scores:   ");
//				Iterator<String> it = normedGenPopScores.keySet().iterator();
//				while(it.hasNext()){
//					String k = it.next();
//					System.out.println("k = " + k);
//					NormScore ns = normedGenPopScores.get(k);
//					// THIS IS HOW TO GET THE PERCENTILES AND Z-SCORES
//					System.out.println("ns.toPercentile()  :" + ns.toPercentile());
//					System.out.println("         ns.toZ()  :" + ns.toZ());					
//				}
//								
//			}else if(ng.getPopulation() == 2){
//				//SPEC POP SCORES
//				normedSpecPopScores = ScoreGroup.calculateNormScores(nhnppi.getScoreGroup().getScores(), ng);
//				System.out.println("specPop Scores:   ");
//				Iterator<String> it = normedSpecPopScores.keySet().iterator();
//				while(it.hasNext()){
//					String k = it.next();
//					System.out.println("k = " + k);
//					NormScore ns = normedSpecPopScores.get(k);
//					System.out.println("ns.toPercentile()  :" + ns.toPercentile());
//					System.out.println("         ns.toZ()  :" + ns.toZ());					
//				}
//				
//			}
//			
//		}	
//		
//		UnitTestUtils.stop(this);
//	}

//	public void testPPIAddUpdate() throws Exception {
//		//Get an PPI from NHN
//		//Score the PPI
//		UnitTestUtils.start(this);
//		V2DatabaseUtils.setUnitTest(true);
//		
//		NhnDatabaseUtils.setUnitTest(true);
//		
//		//Phase 1: Take Participant Data from the database (response data)
//		System.out.println("PHASE 1.....................");
//		IProjectParticipantInstrumentHelper ippi = HelperDelegate.getProjectParticipantInstrumentHelper("com.pdi.data.nhn.helpers.delegated");
//		
////		//// This set blows up with no reponse group
////		String projectId = "2";         
////		String participantId = "553269";
////		String instrumentId = "rv";
//////		//// This set has an empty response group
//		String projectId = "1";         
//		String participantId = "59639";
//		String instrumentId = "lei";  //"lei";  //"cs";	//cs, lei, and gpil all have empty responseGroups
//////		//String projectId = "1";         
//////		//String participantId = "60232";
//////		//String instrumentId = "lei";		//"gpil";  //"lei";  //"rv";	//rv, lei, and gpil all have empty responseGroups
//////		String projectId = "1";         
//////		String participantId = "60254";
//////		String instrumentId = "cs";		//"gpil";  //"lei";  //"rv";	//rv, lei, and gpil all have empty responseGroups
////		
//		int mode = 1;
//		
//		SessionUser session = new SessionUser();
//		session.getMetadata().put("USERNAME", "mbpanichi");
//		session.getMetadata().put("PASSWORD", "griffin12");
//		
//		
//		ProjectParticipantInstrument ppi = ippi.fromProjectIdParticipantIdInstrumentId(session, mode, projectId, participantId, instrumentId);
//		System.out.println("responses: " + ppi.getResponseGroup().getResponses());
//		
////		//Phase 2: SCORE the response data
////		System.out.println("PHASE 2.....................");
////		IScoringHelper iscoring = HelperDelegate.getScoringHelper();
////		ScoreGroup sg = iscoring.score(ppi.getInstrument(), ppi.getResponseGroup());
////		ppi.setScoreGroup(sg);
////		System.out.println("scores.size() : " + sg.getScores().size());
////		
////		//Phase 3: Save this to the V2 response database
////		System.out.println("PHASE 3.....................");
//		
//		/*
//		 * K.  There is an issue right now 
//		 * with running UT's because the app properties
//		 * and the JUnit properties don't quite match 
//		 * up... so I'm just doing phase 3 manually
//		 * instead of building off phases 1 and 2
//		 */
//	// For testing update, just use this last part
//		IProjectParticipantInstrumentHelper ippi2 = HelperDelegate.getProjectParticipantInstrumentHelper("com.pdi.data.v2.helpers.delegated");		
//		// original, from phase 2
//		//ippi2.addUpdate(session, mode, ppi);
//		
//		// get a ppi for this guy instead, just to do it:
//		ProjectParticipantInstrument ppiTest2 = ippi2.fromProjectIdParticipantIdInstrumentId(session, mode, "1", "60232", "rv");		
//		// have to put in the project/participant info... 
//		Project proj = new Project();
//		proj.setId("1");
//		ppiTest2.setProject(proj);
//		Participant part = new Participant();
//		part.setId("60232");
//		ppiTest2.setParticipant(part);
//		//change the spec pop group norm:
//		ppiTest2.getScoreGroup().getSpecialPopulation().setId(342);	// Original data - 348
//
//		ippi2.addUpdate(session, mode, ppiTest2);
//		
///*
//  test 1:
//  pinId	dateCreated	createdBy	participantId	instrumentCode	projectId	specPopNormGroupId	genPopNormGroupId	active
//	19	2/11/2011 	mbp		60232				rv				1			348					340					1		
//  test 2:
//  pinId	dateCreated	createdBy	participantId	instrumentCode	projectId	specPopNormGroupId	genPopNormGroupId	active
//	19	2/18/2011	nghlpr			60232		rv				1			342						340				1
// *
// */
//
//		
//		
///*		
//		select * from results where candidateid = '553269'
//		inserted.....
//		UniqueIdStamp	UniqueVsStamp	DateCreatedStamp	DateModifiedStamp	CreatedByStamp	ModifiedByStamp	CandidateId	JobId	ConstructId	CScore	JCS	JCSLastMod	JCSUpdate	XML	spssValue
//		35735231	""	40581.61183	40581.61183	ppiUpdate	ppiUpdate	553269	2		23					CORRECT_ANSWERS
//		26005094	""	40581.61183	40581.61183	ppiUpdate	ppiUpdate	553269	2		0					NUMBER_UNANSWERED
//		10635149	""	40581.61183	40581.61183	ppiUpdate	ppiUpdate	553269	2		0					INCORRECT_ANSWERS
//		updated...... 
//		UniqueIdStamp	UniqueVsStamp	DateCreatedStamp	DateModifiedStamp	CreatedByStamp	ModifiedByStamp	CandidateId	JobId	ConstructId	CScore	JCS	JCSLastMod	JCSUpdate	XML	spssValue
//		35735231	""	40581.61183	40581.62141	ppiUpdate	ppiUpdate	553269	2		23					CORRECT_ANSWERS
//		26005094	""	40581.61183	40581.62141	ppiUpdate	ppiUpdate	553269	2		0					NUMBER_UNANSWERED
//		10635149	""	40581.61183	40581.62141	ppiUpdate	ppiUpdate	553269	2		0					INCORRECT_ANSWERS
//*/		
//		UnitTestUtils.stop(this);
//	}


//	public void testGetEquivalencyLists ()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		V2DatabaseUtils.setUnitTest(true);
//		
//		System.out.println("Test list getter...");
//		HashMap<String, ArrayList<TextPair>> x = _svc.getEquivalencyLists();
//		
//		for(Iterator<Map.Entry<String, ArrayList<TextPair>>> itr = x.entrySet().iterator(); itr.hasNext(); )
//		{
//			Map.Entry<String, ArrayList<TextPair>> ent = itr.next();
//			System.out.println("  List for " + ent.getKey() + ":");
//			for(TextPair tp: ent.getValue())
//			{
//				System.out.println("    " + tp.getKey() + " -- " + tp.getLabel());
//			}
//		}
//
//		UnitTestUtils.stop(this);
//	}


//	public void testGetEquivalentScore ()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		V2DatabaseUtils.setUnitTest(true);
//
//		System.out.println("Test score getter...");
//		String x = _svc.getEquivalentScore("rv_a", "22");
//		System.out.println("  Ret=" + x + ".  Should be 10");
//
//		x = _svc.getEquivalentScore("xx", "22");
//		System.out.println("  Ret=" + x + ".  Should be null");
//
//		x = _svc.getEquivalentScore("rv_a", "95");
//		System.out.println("  Ret=" + x + ".  Should be null");
//
//		UnitTestUtils.stop(this);
//	}


	public void testAddUpdateParticipantProjectInstrumentWithEqInfo ()
		throws Exception
	{
		UnitTestUtils.start(this);
		V2DatabaseUtils.setUnitTest(true);

		//ProjectParticipantInstrument ppi = null;	// Debug set to ignore null
		EqInfo eqi = new EqInfo();
		eqi.setParticipantId("9999");
		eqi.setProjectId("9999");
		eqi.setInstCode("tst");
		eqi.setEqScore("22");
		eqi.setOldInstCode("bab");
		eqi.setOldScore("44");
		
		_svc.addUpdateParticipantProjectInstrument(null, eqi);	// Debug set to ignore null
		System.out.println("  Check the database");

		UnitTestUtils.stop(this);
	}

}
