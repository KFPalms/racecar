package com.pdi.listener.abyd.setup.ut;

import java.util.ArrayList;

import com.pdi.data.abyd.dto.common.CompGroupData;
import com.pdi.data.abyd.dto.common.DNACellDTO;
import com.pdi.data.abyd.dto.common.DNAStructureDTO;
import com.pdi.data.abyd.dto.common.KeyValuePair;
import com.pdi.data.abyd.dto.common.ModDataDTO;
import com.pdi.data.abyd.dto.common.CompElementDTO;
import com.pdi.data.abyd.dto.setup.DNADescription;
import com.pdi.data.abyd.dto.setup.EntryStateDTO;
import com.pdi.data.abyd.dto.setup.ReportModelCompChildDTO;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.listener.abyd.setup.Setup2Service;
import com.pdi.listener.abyd.setup.SetupService;
import com.pdi.scoring.Norm;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

public class SetupServiceUT extends TestCase
{
	//
	// Instance variables
	//
	SetupService _svc;
	
	//
	// Constructor
	//
	public SetupServiceUT(String name)
	{
		super(name);

		System.out.println("Starting SetupService testing...");
		
		_svc = new SetupService();
	}
  
  

	public void testConnect()
  		throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);

		System.out.println("  Testing comCheck()...");
		String comStr = _svc.comCheck();
		System.out.println("    Made connection.  Return = " + comStr);

		UnitTestUtils.stop(this);
	}


	/*
	 * testGetEntryState
	 */
//	public void testFetchEntryState()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//	
//		System.out.println("  Testing EntryStateHelper call...");
//		
//		String jobId = "LKLVXNGM";
//		System.out.println("    Input job ID=" + jobId);
//		
//		EntryStateDTO dto = _svc.fetchEntryState(jobId);
//		
//		System.out.println("    Output:");
//		System.out.println("         JobName=" + dto.getJobName());
//		System.out.println("      ClientName=" + dto.getClientName());
//		System.out.println("        DNA Desc=" + dto.getState());
//		System.out.println("           State=" + dto.getState());
//		System.out.println("        Models:=");
//		for (KeyValuePair kvp : dto.getModelArray())
//		{
//			System.out.println("          " + kvp.getTheKey() + " - " + kvp.getTheValue());
//		}
//	
//		UnitTestUtils.stop(this);
//	}


	/*
	 * testFetchFullDNAStructure
	 */
	public void testFetchDNAStructure()
	throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);

		System.out.println("  Testing DNAStructureHelper call...");
	
		long dnaId = 41;
		System.out.println("    Input DNA ID=" + dnaId);
	
		DNAStructureDTO dto = _svc.fetchFullDNAStructure(dnaId);
	
		System.out.println("    Output:");
		System.out.println("        Competencies:");
		for (KeyValuePair kvp : dto.getCompetencyArray())
		{
			System.out.println("          " + kvp.getTheKey() + " - " + kvp.getTheValue());
		}
		System.out.println("        Competency Group Data:");
		for (CompGroupData grp : dto.getFullCompArray())
		{
			System.out.println("          ID=" + grp.getCgId() + ", Name=" + grp.getCgName());
			for (CompElementDTO cmp : grp.getCgCompList())
			{
				System.out.println("              ID=" + cmp.getCompId() + ", essential=" + cmp.getIsEssential() + ",Name=" + cmp.getCompName());
			}
		}
		System.out.println("        Modules:");
		for (ModDataDTO mod : dto.getModArray())
		{
			System.out.println("          ID=" + mod.getModId() + ", editted=" + mod.getModEditable() + ", sumitted=" + mod.getModSubmitted() + ", Name=" + mod.getModName());
		}
		System.out.println("        Selected Modules:");
		for (Long sel : dto.getSelectedModsArray())
		{
			System.out.println("          " + sel.longValue());
		}
		System.out.println("        Intersections:");
		for (DNACellDTO mc : dto.getModCompIntersection())
		{
			System.out.println("          mod=" + mc.getModuleId() + ", comp=" + mc.getCompetencyId() + ", used=" + mc.getIsUsed() + ", score=" + mc.getScore());
		}

		UnitTestUtils.stop(this);
	}

	/**
	 * testFetchDNACompChildren
	 * @throws Exception
	 */
	public void testFetchDNACompChildren()  throws Exception {

		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);
		long dna = 1;
		for(ReportModelCompChildDTO child : new Setup2Service().fetchReportModelCompetencyChildren(dna)) {
			System.out.println(child.getDnaCompName());
		}
		UnitTestUtils.stop(this);
	}
	/*
	 * testSaveDNAStructure
	 */
	public void testSaveDNAStructure()
		throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);

		System.out.println("  Testing SaveDNAStructureHelper calls...");

		ArrayList<DNACellDTO> chgAry= new ArrayList<DNACellDTO>();
    	long[] modAry = {75,77,78,76,78,80};
    	long[] cmpAry = {508,508,508,509,509,509};
    	for (int i = 0; i < modAry.length; i++)
    	{
    		DNACellDTO elt = new DNACellDTO();
    		elt.setModuleId(modAry[i]);
    		elt.setCompetencyId(cmpAry[i]);
    		elt.setIsUsed((i % 2 == 1) ? false : true);
    		chgAry.add(elt);
    	}
    	long dnaId = 9;

		System.out.println("    Input DNA ID=" + dnaId);

		_svc.saveDNAStructure(dnaId, chgAry);

		System.out.println("    Output:  Nothing returned");

		UnitTestUtils.stop(this);
	}


	/*
	 * testSaveDNAData
	 */
	public void testSaveDNADesc()
	throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);

		System.out.println("  Testing SaveDNADescHelper calls...");

		DNADescription inp = new DNADescription();
		inp.setDnaId(48);
		inp.setDnaName("\"It's Murder\", She Wrote - Service UT(1)");
		inp.setJobId("TSTJOBXX");
		inp.setModelId(46);
		inp.setcMgrName("Joseph P. Blough - SUT1");
		inp.setcMgrEmail("blough-hard-SUT1@bogus.com");

		System.out.println("    Input DNA ID=" + inp.getDnaId());

		DNADescription dto = _svc.saveNameModelCmgr(inp);
	
		System.out.println("    Output:");
		System.out.println("      Returned Status=" + dto.getSuccessStatus());
		if (dto.getStatusMsg() != null)
		{
			System.out.println("        Msg=" + dto.getStatusMsg());
		}
		System.out.println("      Returned DNA id =" + dto.getDnaId() + ", Name=" + dto.getDnaName());

		inp.setSynthNorm(new Norm(2.5,.5));
		inp.setWbVersion("2.2.2.2");
		inp.setDnaNotes("Notes-SUT2");
			
		System.out.println("    2nd update...");

		_svc.saveMoreDNADesc(inp);
			
		System.out.println("    Output:  Nothing returned");

		UnitTestUtils.stop(this);
	}


//	/*
//	 * testSPData - No longer used
//	 */
//	public void testSPData()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//
//		System.out.println("  Testing SPDataHelper...");
//
//		long dnaId = 18;
//		System.out.println("    Input DNA ID=" + dnaId);
//
//		SPFullDataDTO dto = _svc.fetchSPData(dnaId);
//	
//		System.out.println("    Output:");
//		System.out.println("      Competencies:");
//		for (KeyValuePair kvp : dto.getCompList())
//		{
//			System.out.println("        " + kvp.getTheKey() + " - " + kvp.getTheValue());
//		}
//		System.out.println("      Comp elements:");
//		for (CompElementDTO elt : dto.getCompElts())
//		{
//			System.out.println("        ID=" + elt.getCompId() + ", essential=" + elt.getIsEssential());
//		}
//		System.out.println("      Text elts:");
//		for (KeyValuePair kvp : dto.getTextElts())
//		{
//			System.out.println("        " + kvp.getTheKey() + " - " + kvp.getTheValue());
//		}
//
//		UnitTestUtils.stop(this);
//	}


//	/*
//	 * testSaveSPText - No longer used
//	 */
//	public void testSaveSPText()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//
//		System.out.println("  Testing SaveSPTextHelper...");
//
//		ArrayList<KeyValuePair> ary = new ArrayList<KeyValuePair>();
//    	for (int i = 0; i < 7; i++)
//    	{
//    		KeyValuePair kv = new KeyValuePair();
//    		kv.setTheKey(i + 1);
//    		kv.setTheValue("Service UT test text for item type " + (i + 1));
//    		ary.add(kv);
//    	}
//		long dnaId = 14;
//		System.out.println("    Input DNA ID=" + dnaId);
//
//		_svc.saveSPTexts(dnaId, ary);
//
//		System.out.println("    Output:  none");
//
//		UnitTestUtils.stop(this);
//	}


//	/*
//	 * testSaveSPElements - No longer used
//	 */
//	public void testSaveSPElements()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//
//		System.out.println("  Testing SaveSPElementHelper...");
//
//		ArrayList<CompElementDTO> ary = new ArrayList<CompElementDTO>();
//		long[] compAry = {508,509,510};
//    	for (int i = 0; i < compAry.length; i++)
//    	{
//    		CompElementDTO ce = new CompElementDTO();
//    		ce.setCompId(compAry[i]);
//    		ce.setIsEssential(i == 0 ? true : false);
//    		ary.add(ce);
//    	}
//		long dnaId = 9;
//		System.out.println("    Input DNA ID=" + dnaId);
//
//		_svc.saveSPElements(dnaId, ary);
//
//		System.out.println("    Output:  none");
//
//		UnitTestUtils.stop(this);
//	}


	/*
	 * testSendEmail
	 */
//	public void testSendEmail()
//		throws Exception
//	{
//		if(true) {
//			return;
//		}
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//	
//		System.out.println("  Testing sendEmail()...");
//	
//		String address = "kbeukelm@pdi-corp.com";
//	
//		String textFlg = "modelChange";
//		String clientName = "Bogus 1 Corp.";
//		System.out.println("    Test 1 - address=" + address + ", type=" + textFlg + ", client=" + clientName);
//		_svc.sendEmail(address, textFlg, clientName);        
//	
//		textFlg = "dnaChange";		//, "spChange", "dnaSubmitted"
//		clientName = "Bogus 2 Corp.";
//		System.out.println("    Test 2 - address=" + address + ", type=" + textFlg + ", client=" + clientName);
//		_svc.sendEmail(address, textFlg, clientName);        
//	
//		textFlg = "spChange";		//, "dnaSubmitted"
//		clientName = "Bogus 3 Corp.";
//		System.out.println("    Test 3 - address=" + address + ", type=" + textFlg + ", client=" + clientName);
//		_svc.sendEmail(address, textFlg, clientName);        
//	
//		textFlg = "dnaSubmitted";
//		clientName = "Bogus 4 Corp.";
//		System.out.println("    Test 4 - address=" + address + ", type=" + textFlg + ", client=" + clientName);
//		_svc.sendEmail(address, textFlg, clientName);        
//	
//		System.out.println("    Email tests conclude");
//	
//		UnitTestUtils.stop(this);
//	}
}
