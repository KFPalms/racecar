/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.listener.portal.delegated.helpers.reports;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.pdi.data.abyd.dto.reportInput.ReportInputDTO;
import com.pdi.data.abyd.helpers.reportInput.ReportInputDataHelper;
import com.pdi.data.abyd.util.data.DataUtils;
import com.pdi.data.dto.ProjectParticipantInstrument;
import com.pdi.data.dto.Response;
import com.pdi.data.dto.Score;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.dto.TestData;
import com.pdi.data.dto.TextGroup;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.IMigrationHelper;
import com.pdi.data.helpers.interfaces.ITestDataHelper;
import com.pdi.properties.PropertyLoader;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.GroupReport;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;
import com.pdi.reporting.request.ReportingRequest;
import com.pdi.string.StringUtils;
import com.pdi.xml.XMLUtils;


/*
 * TltReports - class to hold methods relevant to the TLT reports.  It
 * 				gathers data for multiple reports and for the TLT extracts.
 * 
 * NOTE:  EVEN THOUGH LOCATED UNDER THE DELEGATED FOLDER, THERE ARE NO METHODS IN THIS
 *        CLASS THAT ARE DEFINED IN THE DATA INTERFACE.
 *        
 * This class is located here because the folder structure (delegated/helpers) is
 * inverted from normal practice elsewhere in this project stack (helpers/delegated).
 * Putting this helper class here avoided confusion with having two folders in the
 * portal structure that had the name "helper".
 * 
 * Yes, this is not a report but an extract.  Still, it is located in the report folder
 */
public class TltReports
{

	//
	// Static data.
	//
//	private static int TLT_WITH_COGS = 1;
//	//private static int TLT_NO_COGS = 2;
//	private static int ABYD = 3;
//	private static int ASMT = 4;

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String tlCode = null;
	//
	// Constructors.
	//
	public TltReports()
	{
		// No logic here at the present time
	}

	//
	// Instance methods.
	//


	/**
	 *  getTLTReportData is a method that handles all 
	 *  TLT specific report functions - gathers and  sets up
	 *  all TLT specific data. 
	 * 
	 * @param ret
	 * @param epis
	 * @param overrides
	 * @param sessionUser
	 * @param tg
	 */
	public void getTLTReportData(IndividualReport inp,
								 HashMap<String, GroupReport> groupReports,
								 ReportingRequest reportingRequest,
								 ArrayList<ProjectParticipantInstrument> epis, 
								 HashMap<String, String> overrides,
								 SessionUser sessionUser,
								 TextGroup tg,
								 String langCode)
	{
		//System.out.println("getTLTReportData --  " + inp.getReportCode() + "   " + inp.getReportType());
		IndividualReport ret = inp.clone();	// get the input data

		// Supress subordinate instruments
		epis = new DataUtils().suppressSubInstUtil(epis);

		try
		{
			// A by D Transition Scales is effectively the same report as the Group Detail report
			// and requires the TLT norms in the IR object
			if(ret.getReportCode().equalsIgnoreCase(ReportingConstants.REPORT_CODE_TLT_KF_INDIVIDUAL_SUMMARY) ||
			   ret.getReportCode().equalsIgnoreCase(ReportingConstants.REPORT_CODE_TLT_INDIVIDUAL_SUMMARY) ||
			   ret.getReportCode().equalsIgnoreCase(ReportingConstants.REPORT_CODE_TLT_GROUP_DETAIL) ||
			   ret.getReportCode().equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_TRANSITION_SCALES) ||
			   ret.getReportCode().equalsIgnoreCase(ReportingConstants.REPORT_CODE_TLT_EXTRACT) ||
			   ret.getReportCode().equalsIgnoreCase(ReportingConstants.REPORT_CODE_TLT_EXTRACT_RAW))
			{
				int transLvl = epis.get(0).getProject().getTargetLevelTypeId();				

				/*
				 * ========================================
				 * ADD BASIC PARTICIPANT DATA TO REPORT 
				 */
				ret.addDisplayData("FIRST_NAME", epis.get(0).getParticipant().getFirstName());
				ret.addDisplayData("LAST_NAME", epis.get(0).getParticipant().getLastName());
				ret.getDisplayData().put("NAME", epis.get(0).getParticipant().getLastName() + ", " + epis.get(0).getParticipant().getFirstName());

				//Are we overriding the target level?
				if(overrides.get("TARGET_LEVEL") != null)
				{
					//System.out.println("Override of " + overrides.get("TARGET_LEVEL") + " for " + reportCode);
					//targetLevelCode = overrides.get("TARGET_LEVEL");
					String tLvl = overrides.get("TARGET_LEVEL");
					if(tLvl.equalsIgnoreCase("FLL"))
					{
						transLvl = ReportingConstants.TGT_LVL_FLL;
					}
					else if(tLvl.equalsIgnoreCase("MLL"))
					{
						transLvl = ReportingConstants.TGT_LVL_MLL;
					}
					else if(tLvl.equalsIgnoreCase("BUL")  || tLvl.equalsIgnoreCase("SLL") )
					{
						transLvl = ReportingConstants.TGT_LVL_SLL;
					}
					else if(tLvl.equalsIgnoreCase("SEA"))
					{
						transLvl = ReportingConstants.TGT_LVL_SEA;
					}
					else
					{
						transLvl = 0;
					}
				}	// End of if on TARGET_LEVEL override

				//Get the target level - This will get the individual report norms (?) If so the numbers are wrong
				/*
				 * READ THIS BEFORE MODIFYING THE TARGET LEVEL
				 * 
				 * The migration helper is used to decouple a segment that needs to be refactored, here are notes
				 * about the target level for individual reporting
				 * 
				 * If the reference is actually the identifier (pdi_tlt_normset.normsetId) then the values would be
				 * FLL = 3
				 * MLL = 1
				 * BUL = 2
				 * 
				 * If the references is actually the transition level (pdi_tlt_normset.transitionLevel)
				 * FLL = 6
				 * MLL = 7
				 * BUL = 8
				 * 
				 * If the reference is an internal transition level (not database bound) then the values would be
				 * FLL = 1
				 * MLL = 3
				 * BUL = 5
				 * 
				 * IT APPEARS THAT OPTION #3 (internal transition level) is what must be passed
				 */

				int targetLevel = 0;
				String targetLevelLabel = ReportingConstants.LBL_BLANK;

				//String projType = epis.get(0).getProject().getProjectTypeName();
				int irProjType = epis.get(0).getProject().getProjectTypeId();
				if(transLvl == 0)
				{
					return;
				}
				else if(transLvl == ReportingConstants.TGT_LVL_FLL)
				{
					targetLevel = 1;
					tlCode = "FLL";
					targetLevelLabel = ReportingConstants.LBL_FLL;
				}
				else if(transLvl == ReportingConstants.TGT_LVL_MLL)
				{
					targetLevel = 3;
					tlCode = "MLL";
					targetLevelLabel = ReportingConstants.LBL_MLL;
				}
				else if(transLvl == ReportingConstants.TGT_LVL_SLL  ||
						transLvl == ReportingConstants.TGT_LVL_BUL)
				{
					targetLevel = 5;
					//if (projType.equals(Project.ABYD))
					if (irProjType == ReportingConstants.ABYD_ONLINE)
					{
						tlCode = "BUL";	// a by d stuff gets SLL rather than BUL
					}
					else
					{
						tlCode = "SLL";	// tlt stuff gets SLL rather than BUL
					}
					targetLevelLabel = ReportingConstants.LBL_SLL;
				}
				else if (transLvl == ReportingConstants.TGT_LVL_SEA 
						//&& ( projType.equals(Project.ABYD) || projType.equals(Project.ABYD_LONG)) )
						&& ( irProjType == ReportingConstants.ABYD_ONLINE) )
				{
					targetLevel = 9;
					tlCode = "SEA";
					targetLevelLabel = ReportingConstants.LBL_SLL;
				}

				IMigrationHelper mh = HelperDelegate.getMigrationHelper(PropertyLoader.getProperty("com.pdi.data.application","datasource.v2"));
				boolean includeCogs = false;
				
				/*
				 * platform.dbo.project_type
					project_type_id	name													description	code
					1				TalentView of Leadership Transitions with Cognitives	NULL		TLT
					2				TalentView of Leadership Transitions without Cognitives	NULL		TLT
					3				Assessment By Design - Online							NULL		ABYD
					4				Assessment Testing										NULL		ASMT				
				*/
				//int tltTyp = epis.get(0).getProject().getProjectTypeId();

				//if (tltTyp == TLT_WITH_COGS)
				if( irProjType == ReportingConstants.TLT_WITH_COGS ||
					irProjType == ReportingConstants.TLT_LA_WITH_COGS )
				{
					includeCogs = true;
					ret.getDisplayData().put(ReportingConstants.RC_COGNITIVES_INCLUDED, "1");
				}else{
					ret.getDisplayData().put(ReportingConstants.RC_COGNITIVES_INCLUDED, "0");
				}

				//if(epis.get(0).getProject().getProjectTypeId() == ABYD ||
				//		   epis.get(0).getProject().getProjectTypeId()== ASMT )
				//if(tltTyp == ABYD || tltTyp == ASMT )
				if(irProjType == ReportingConstants.ABYD_ONLINE ||
				   irProjType == ReportingConstants.ASSESSMENT  ||
				   irProjType == ReportingConstants.MLL_LVA)
				{
					for(ProjectParticipantInstrument epi : epis)
					{	
						if(epi.getInstrument().getCode().equals(ReportingConstants.IC_RAVENS))
						{
							includeCogs = true;							
							break;
						}
					}					
				}
				
				
				mh.tltIndividualReportMigration(sessionUser, 1, targetLevel, ret, includeCogs);
				//ret.getDisplayData().put("TARGET_LEVEL", targetLevelCode);
				//ret.getDisplayData().put("TARGET_LEVEL", targetLevelCode);
				ret.getDisplayData().put("TARGET_LEVEL", tlCode);
				ret.getScriptedData().put("TARGET_LVL_LBL", targetLevelLabel);
			}	// End of the if <list of all insturments>  Can the if be removed?  It dupse the list in the call...

			ProjectParticipantInstrument lastEpi = null;
			//Boolean nextLevelAdded = false;
			for(ProjectParticipantInstrument epi : epis)
			{
				lastEpi = epi;
				//System.out.println("Time to set participant data ");
				//System.out.println(epi.getParticipant().getEmail()+  " :  " 
				//	+ epi.getParticipant().getId() + " : "
				//	+ epi.getParticipant().getFirstName()+ " : " + epi.getParticipant().getLastName()
				//	+ " : " + epi.getProject().getClient().getName() + " : "
				//	+ epi.getParticipant().getMetadata().get("jobTitle") + " : " 
				//	+ epi.getParticipant().getMetadata().get("orgUnit")
				//	//+ " : " + epi.getProject().getMetadata().get("transitionLevel") 
				//	);

				ret.addDisplayData("FIRST_NAME", epi.getParticipant().getFirstName());
				ret.addDisplayData("LAST_NAME", epi.getParticipant().getLastName());
				ret.addDisplayData("ORGANIZATION", XMLUtils.xmlEscapeString(epi.getProject().getClient().getName()));
				//ret.setName(epi.getParticipant().getLastName() + "_" + epi.getParticipant().getFirstName() + UUID.randomUUID() + ".pdf");

				// additional data for TLT Extract:
				ret.addDisplayData("PART_ID", epi.getParticipant().getId());
				ret.addDisplayData("PART_EMAIL", epi.getParticipant().getEmail());
				ret.addDisplayData("JOB_TITLE", epi.getParticipant().getMetadata().get("jobTitle"));
				ret.addDisplayData("ORG_UNIT", epi.getParticipant().getMetadata().get("orgUnit"));
				// not added to project yet, but will be:

				ret.addDisplayData("OPTIONAL_1",epi.getParticipant().getMetadata().get("optional_1"));
				ret.addDisplayData("OPTIONAL_2",epi.getParticipant().getMetadata().get("optional_2"));
				ret.addDisplayData("OPTIONAL_3",epi.getParticipant().getMetadata().get("optional_3"));
				
				ReportData rd = new ReportData();
				if(epi.getResponseGroup() != null)
				{
					for(Response r : epi.getResponseGroup().getResponses().values())
					{
						rd.addRawData(r.getCode(), r.getValue());
					}
				}

				if(epi.getScoreGroup() != null)
				{
					for(Score s : epi.getScoreGroup().getScores().values())
					{					
						rd.addScoreData(s.getCode(), s.getRawScore());
					}
				}

				//System.out.println(epi.getInstrument().getMetadata().get("reportingInstId"));
				// Assumes reportingInstId always present
				ret.getReportData().put(epi.getInstrument().getMetadata().get("reportingInstId"), rd);
			}	// End of for loop on epis

			ret.getScriptedData().put(ReportingConstants.PART_COMPL_RPT, ReportingConstants.COMPLETE_REPORT);
			
			//Build a file name
//			fileName = StringUtils.removeSpecialCharacters(
//			//epis.get(0).getProject().getClient().getName() + " " +
//			//epis.get(0).getProject().getName() + " " +
//			epis.get(0).getParticipant().getLastName() + " " +
//			epis.get(0).getParticipant().getFirstName() + " " +
//			epis.get(0).getProject().getProjectTypeName()+" "+
//			tlCode + " "+
//			tg.getGroupCode() + " "+
//			new SimpleDateFormat("yyyyMMdd").format(new Date()) + " " +
//			langCode);
			String prefix = epis.get(0).getParticipant().getLastName() + " " +
			  epis.get(0).getParticipant().getFirstName();
			boolean isCJK = false;
			for (int i=0; i < prefix.length() && ! isCJK; i++)
			{
				if( (Character.UnicodeBlock.of(prefix.charAt(i)) == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS))
				{
					isCJK = true;
					prefix = epis.get(0).getParticipant().getId();
				}
			}
//			String fileName = epis.get(0).getParticipant().getLastName() + " " +
//			  epis.get(0).getParticipant().getFirstName() + " " +
//			  epis.get(0).getProject().getProjectTypeName()+" "+
//			  tlCode + " ";
			String fileName = prefix + " " +
			  epis.get(0).getProject().getProjectTypeName()+" "+
			  tlCode + " ";
			// Need to differential the file name for those reports that are in the same group.  Right now,
			// We need worry about the individual summary report only...
			fileName += (tg.getGroupCode().equals("TLT_IND_SUMMARY_RPT")) ? ret.getReportCode() : tg.getGroupCode();
			fileName += " " + new SimpleDateFormat("yyyyMMdd").format(new Date()) + " " + langCode;	
			ret.setName(fileName.replace(" ", "_") + ".pdf");
				
			// this is for NHN 3158 -- cuz Chinese names come out as "______" in the file name
			String tempFileName = "";
			if(ret.getName().contains("____")){
				tempFileName = epis.get(0).getParticipant().getId() + ret.getName();					
				ret.setName(tempFileName);
			}
				
			if(ret.getReportCode().equalsIgnoreCase(ReportingConstants.REPORT_CODE_TLT_GROUP_DETAIL) ||
			   ret.getReportCode().equalsIgnoreCase(ReportingConstants.REPORT_CODE_TLT_EXTRACT) ||
			   ret.getReportCode().equalsIgnoreCase(ReportingConstants.REPORT_CODE_TLT_EXTRACT_RAW) ||
			   ret.getReportCode().equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_TRANSITION_SCALES))
			{
				// Do group stuff
				if(groupReports.get(ret.getReportCode()) == null)
				{
					//First time entered, create a new group report...
					GroupReport gr = new GroupReport();
					// ...and stick in the labels
					gr.getLabelData().putAll(tg.getLabelHashMap());

					if(ret.getReportCode().equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_TRANSITION_SCALES))
					{
						gr.getScriptedData().put("REPORT_TYPE", ReportingConstants.REPORT_TYPE_ABYD);
					} else {
						gr.getScriptedData().put("REPORT_TYPE", ReportingConstants.REPORT_TYPE_TLT);
					}
					//get the migration helper
					IMigrationHelper mh = HelperDelegate.getMigrationHelper(PropertyLoader.getProperty("com.pdi.data.application","datasource.v2"));

					//What is the target level (use the last participant to determine the target level
					//String targetLevelCode = lastEpi.getProject().getMetadata().get("transitionLevel");
					//System.out.println("targetLevelCode: project metadata: transitionLevel:  " + targetLevelCode);

					int transLvl = epis.get(0).getProject().getTargetLevelTypeId();

					if(overrides.get("TARGET_LEVEL") != null)
					{
						//System.out.println("Override of " + overrides.get("TARGET_LEVEL") + " for " + reportCode);
						//targetLevelCode = overrides.get("TARGET_LEVEL");
						String tLvl = overrides.get("TARGET_LEVEL");
						if(tLvl.equalsIgnoreCase("FLL"))
						{
							transLvl = ReportingConstants.TGT_LVL_FLL;
						}
						else if(tLvl.equalsIgnoreCase("MLL"))
						{
							transLvl = ReportingConstants.TGT_LVL_MLL;
						}
						else if(tLvl.equalsIgnoreCase("BUL") || tLvl.equalsIgnoreCase("SLL"))
						{
							transLvl = ReportingConstants.TGT_LVL_SLL;
						}
						else if(tLvl.equalsIgnoreCase("SEA"))
						{
							transLvl = ReportingConstants.TGT_LVL_SEA;
						}
						else
						{
							transLvl = 0;
						}
					}

					/*
					 * WHY IS THIS HERE
					 * Look at TLT_GROUP_DETAIL_REPORT_SCORING line 1511
					 * in com.pdi.reporting.report.vm; 
					 */

					// Get the group project type.  1st epi is assumed to have the
					// correct value for the entire group
					//String projType = epis.get(0).getProject().getProjectTypeName();
					int grpProjType = epis.get(0).getProject().getProjectTypeId();
					
					if(transLvl == 0)
					{
						return;
					} else if(transLvl == ReportingConstants.TGT_LVL_FLL)
					{
						gr.setTargetTransitionLevel(6);
					} else if(transLvl == ReportingConstants.TGT_LVL_MLL)
					{
						gr.setTargetTransitionLevel(7);						
					} else if(transLvl == ReportingConstants.TGT_LVL_SLL  ||
							  transLvl == ReportingConstants.TGT_LVL_BUL)
					{
						gr.setTargetTransitionLevel(8);
					//}else if (transLvl == ReportingConstants.TGT_LVL_SEA && projType.equals(Project.ABYD)   )
					} else if (transLvl == ReportingConstants.TGT_LVL_SEA && grpProjType == ReportingConstants.ABYD_ONLINE  )
					{
						gr.setTargetTransitionLevel(9);
						// mb: 08-08-2011 :  in the transistions scales groovy, there is also a target level 9: 
					}

					//Begin target level "logic"
					int targetLevel = 0;

					// 3 == BUL
					// 4 == MLL
					// 5 == SEA
					// 7 == IC/FLL
					// 8 == MLL/BUL
					// 9 == SEA

					//Get the target transition level for a group report
					/*
					 * READ THIS BEFORE MODIFYING THIS
					 * 
					 * This data is based off the table pdi_tlt_normset, and creative reconstruction
					 *  
					 * If the reference is actually the identifier (pdi_tlt_normset.normsetId) then the values would be
					 * FLL = 3
					 * MLL = 1
					 * BUL = 2
					 * 
					 * If the references is actually the transition level (pdi_tlt_normset.transitionLevel)
					 * FLL = 6
					 * MLL = 7
					 * BUL = 8
					 * 
					 * Looking at the code for group reports the key is based off ns.transitionLevel, 
					 * these are what we believe the correct transitionLevels to be (see above for reference)
					 * 
					 * IT APPEARS THAT OPTION #3 (internal transition level) is what must be passed
					 */

					String targetLevelLabel = ReportingConstants.LBL_BLANK;

					String tlCode = null;
					if(transLvl == 0)
					{
						return;
					}
					else if(transLvl == ReportingConstants.TGT_LVL_FLL)
					{
						targetLevel = 6;
						targetLevelLabel = ReportingConstants.LBL_FLL;
						ret.getDisplayData().put("MODEL", "First-Level Leader (FLL)");
						tlCode = "FLL";
					}
					else if(transLvl == ReportingConstants.TGT_LVL_MLL)
					{
						targetLevel = 7;
						targetLevelLabel = ReportingConstants.LBL_MLL;
						ret.getDisplayData().put("MODEL", "Mid-Level Leader (MLL)");
						tlCode = "MLL";
					}
					else if(transLvl == ReportingConstants.TGT_LVL_SLL  ||
							transLvl == ReportingConstants.TGT_LVL_BUL)
					{
						targetLevel = 8;
						targetLevelLabel = ReportingConstants.LBL_SLL;
						ret.getDisplayData().put("MODEL", "Executive (BUL)");
						//if (projType.equals(Project.ABYD))
						if (grpProjType == ReportingConstants.ABYD_ONLINE)
						{
							tlCode = "BUL";
						}  else  {
							tlCode = "SLL";
						}
					}
					else if(transLvl == ReportingConstants.TGT_LVL_SEA
							//&& ( projType.equals(Project.ABYD) || projType.equals(Project.ABYD_LONG)) )
							&& ( grpProjType == ReportingConstants.ABYD_ONLINE) )
					{
						targetLevel = 9;
						ret.getDisplayData().put("MODEL", "Senior Executive");
						// mb: 08-08-2011 :  in the transistions scales groovy, there is also a target level 9
						// not setting a label, because I don't see them being used in the code anywhere... 
						tlCode = "SEA";
					}

					/*
					 * per Vidula:  "we made the change just recently that 
					 *  required all TLT reports to have cog data - 
					 *  if it was a TLT project w/Ravens - to generate, that affected AbyD as well. "
					 * So, if an AbyD project, check to see if there is a Raven's instrument,
					 * and if there is, do TLT with cogs, and if not, TLT without cogs.
					 * this will also apply to Assessment projects as well.
					 * 
					 */
					boolean includeCogs = false;

					// The name doesn't work... try the code
					//if(lastEpi.getProject().getProjectTypeName().equals("ABYD") ||
					//		   lastEpi.getProject().getProjectTypeName().equals("ASMT") )
					if(grpProjType == ReportingConstants.ABYD_ONLINE ||
					   grpProjType == ReportingConstants.ASSESSMENT  ||
					   grpProjType == ReportingConstants.MLL_LVA)
					{
						for(ProjectParticipantInstrument epi : epis)
						{
							if(epi.getInstrument().getCode().equals(ReportingConstants.IC_RAVENS))
							{
								includeCogs = true;
								break;
							}
						}					
					}

					//int tltTyp = lastEpi.getProject().getProjectTypeId();

					//if(tltTyp == ReportingConstants.TLT_WITH_COGS)
					if( grpProjType == ReportingConstants.TLT_WITH_COGS ||
						grpProjType == ReportingConstants.TLT_LA_WITH_COGS )
					{
						includeCogs = true;
					}

					mh.tltGroupReportMigration(sessionUser, 1, targetLevel, gr, includeCogs);

					//Migration complete, wrap up report
					gr.setReportCode(ret.getReportCode());

					if(ret.getReportCode().equalsIgnoreCase(ReportingConstants.REPORT_CODE_TLT_GROUP_DETAIL ))
					{
						gr.setReportType(ReportingConstants.REPORT_TYPE_GROUP);
						fileName = StringUtils.removeFilenameSpecialCharacters(
								ret.getDisplayData("ORGANIZATION") + " " + 
								tlCode + " "+
								gr.getReportCode()+" "+
								new SimpleDateFormat("yyyyMMdd").format(new Date()) + " " +
								langCode);
						gr.setName(fileName.replace(" ", "_") + ".pdf");
					}else if(ret.getReportCode().equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_TRANSITION_SCALES)){
						gr.setReportType(ReportingConstants.REPORT_TYPE_GROUP);
						fileName = StringUtils.removeFilenameSpecialCharacters(
								ret.getDisplayData("LAST_NAME") + " " + 
								ret.getDisplayData("FIRST_NAME") + " " + 
								tlCode + " "+
								gr.getReportCode()+" "+
								new SimpleDateFormat("yyyyMMdd").format(new Date()) + " " +
								langCode);
						gr.setName(fileName.replace(" ", "_") + ".pdf");
					}
					else
					{
						gr.setReportType(ReportingConstants.REPORT_TYPE_GROUP_EXTRACT);
						//gr.setName("_TLT_ExtractReport.xls");
						if(ret.getName() != null){
						fileName = StringUtils.removeFilenameSpecialCharacters(
								ret.getDisplayData("ORGANIZATION") + " " + 
								tlCode + " "+
								gr.getReportCode()+" "+
								new SimpleDateFormat("yyyyMMdd").format(new Date()) + " " +
								langCode);
								
						gr.setName(fileName.replace(" ", "_") + ".xls");
						}
					}

					//Add some misc data that is part of the group detail report
					gr.addDisplayData("ORGANIZATION", XMLUtils.xmlEscapeString(lastEpi.getProject().getClient().getName()));
					//gr.getDisplayData().put("TARGET_LEVEL", targetLevelCode);
					gr.getDisplayData().put("TARGET_LEVEL", tlCode);
					gr.getScriptedData().put("TARGET_LVL_LBL", targetLevelLabel);

					/* ========================================
					* ADD BASIC PARTICIPANT DATA TO REPORT 
					*/
					gr.addDisplayData("FIRST_NAME", epis.get(0).getParticipant().getFirstName());
					gr.addDisplayData("LAST_NAME", epis.get(0).getParticipant().getLastName());
					if(ret.getReportCode().equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_TRANSITION_SCALES))
					{
						gr.getDisplayData().put("PARTICIPANT_NAME",   epis.get(0).getParticipant().getFirstName() + " " + epis.get(0).getParticipant().getLastName());
						gr.addDisplayData("CLIENT_NAME", XMLUtils.xmlEscapeString(lastEpi.getProject().getClient().getName()));
						gr.addDisplayData("PROJECT_NAME", XMLUtils.xmlEscapeString(lastEpi.getProject().getName()));
						//gr.addDisplayData("MODEL", lastEpi.getProject().);
					}
					//add to the request object
					reportingRequest.addReport(gr);
					groupReports.put(ret.getReportCode(), gr);
					//TRANSITION SCALES PART

//					if(ret.getReportCode().equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_TRANSITION_SCALES) && !nextLevelAdded){
//						IndividualReport newRET = ret.clone();
//						newRET.getDisplayData().put("TARGET_LEVEL", tlCode);
//						newRET.getScriptedData().put("TARGET_LVL_LBL", targetLevelLabel);
//						groupReports.get(ret.getReportCode()).getIndividualReports().add(newRET);
//						//adding epi for next level...
//						nextLevelAdded = true;
//					}
//					groupReports.get(ret.getReportCode()).getIndividualReports().add(ret);

					if(ret.getReportCode().equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_TRANSITION_SCALES))
					{
						gr.setReportCode(ReportingConstants.REPORT_CODE_ABYD_TRANSITION_SCALES);
						TestData td = new TestData();
						td.setParticipantId(new Long(epis.get(0).getParticipant().getId()).longValue());
						td.setProjectId(new Long(epis.get(0).getProject().getId()).longValue());
						ITestDataHelper tdh = HelperDelegate.getTestDataHelper("com.pdi.data.v2.helpers.delegated");
						Long dnaId = tdh.getDnaIdFromProjectId(sessionUser, td);  // gonna need this to get some participant scores... 

						String participantId = epis.get(0).getParticipant().getId();
						//Long dnaId = new Long(ret.getDisplayData().get("DNA_ID")).longValue();
						ReportInputDTO reportInput = null;
						ReportInputDataHelper reportInputDataHelper = new ReportInputDataHelper();
						reportInput = reportInputDataHelper.getReportInput(participantId, dnaId);

						IndividualReport nextTLT = reportInput.getNextTLT();
						//System.out.println("nextTLT.getDisplayData().get(TARGET_LEVEL) " + nextTLT.getDisplayData().get("TARGET_LEVEL"));					
						if(nextTLT.getDisplayData().get("TARGET_LEVEL").equalsIgnoreCase("9"))
						{
							nextTLT.getDisplayData().put("MODEL", "Senior Executive");
						}
						else if(nextTLT.getDisplayData().get("TARGET_LEVEL").equalsIgnoreCase("8")) {
							nextTLT.getDisplayData().put("MODEL", "Executive (BUL)");
						}
						else if(nextTLT.getDisplayData().get("TARGET_LEVEL").equalsIgnoreCase("7"))
						{
							nextTLT.getDisplayData().put("MODEL", "Mid-Level Leader (MLL)");
						}
						gr.getIndividualReports().add(nextTLT);
					}
				}	// End of "if 1st time through" (gr == null)

				groupReports.get(ret.getReportCode()).getIndividualReports().add(0, ret);	
				
				//private IndividualReport getTLTData(int targetLevel, ArrayList<ProjectParticipantInstrument> ppis)
			}
			else
			{
				//System.out.println("Report Hlper:  this is not a group report... \n leaving doTLTSpecificReportTasks ");
				//This is NOT a group report, it is an individual report that has already been calculated, populated, etc etc
				//Just add to the request
				reportingRequest.addReport(ret);
			}
		}
		catch (Exception e)
		{
			System.out.println("Exception in getTLTReportData. msg=" + e.getMessage());
			e.printStackTrace();
		}
	}
}
