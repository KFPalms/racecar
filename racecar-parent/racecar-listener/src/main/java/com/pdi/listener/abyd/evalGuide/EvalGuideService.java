/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 *
 */
 
package com.pdi.listener.abyd.evalGuide;

import java.sql.Connection;

import com.pdi.data.abyd.dto.evalGuide.EGUrlDTO;
import com.pdi.data.abyd.dto.common.EGFullDataDTO;
import com.pdi.data.abyd.dto.evalGuide.EGCompOverideDataDTO;
import com.pdi.data.abyd.dto.evalGuide.EGOverallDTO;
import com.pdi.data.abyd.dto.evalGuide.EGRaterNameDTO;
import com.pdi.data.abyd.dto.evalGuide.EGCompDataDTO;
import com.pdi.data.abyd.dto.evalGuide.EGIrDataDTO;
import com.pdi.data.abyd.helpers.common.EGDataHelper;
import com.pdi.data.abyd.helpers.evalGuide.EGComCheckHelper;
import com.pdi.data.abyd.helpers.evalGuide.EGUrlDataHelper;
import com.pdi.data.abyd.helpers.evalGuide.SaveEGCompDataHelper;
import com.pdi.data.abyd.helpers.evalGuide.SaveEGIrDataHelper;
import com.pdi.data.abyd.helpers.evalGuide.SaveEGOverallDataHelper;
import com.pdi.data.abyd.helpers.evalGuide.SaveEGRaterNameDataHelper;
import com.pdi.data.abyd.helpers.evalGuide.SubmitEGDataHelper;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;


/**
 * EvalGuideService provides entry point framework for the
 * Assessment-by-Design Project Eval Guide generation app
 *
 * @author		Ken Beukelman
 * @version	$Revision: 6 $  $Date: 8/25/05 3:23p $
 */
public class EvalGuideService
{
	//
	// Static data.
	//
	
	//
	// Static methods.
	//

	//
	// Instance data.
	//
	
	//
	// Constructors.
	//
	public EvalGuideService()
	{
		// Does nothing currently
	}

	//
	// Instance methods.
	//


	/**
	 * Quick check to see if the connection is up and if the dbms is available
	 *
	 * @return a String
	 * @throws Exception
	 */
	public String comCheck()
		throws Exception
	{
		Connection con = null;
		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();

			// get the data
			EGComCheckHelper helper = new EGComCheckHelper(con);        
			String ret = helper.comCheck();
				
			return ret;
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)  {  /* Swallow the exception */  }
				con = null;
			}
		}
	}

	
	/**
	 * Fetch the Eval Guide invocation URLs.
	 *
	 * @param lpId the link params id for the participant and job data
	 * @return the EGUrlDTO object
	 * @throws Exception
	 */
	public EGUrlDTO fetchEgUrls(String projId, String partId)
		throws Exception
	{
		Connection con = null;
		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();

			// get the data
			EGUrlDataHelper helper = new EGUrlDataHelper(con);    
			EGUrlDTO data = helper.getUrlData(projId, partId);
				
			return data;
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)  {  /* Swallow the exception */  }
				con = null;
			}
		}
	}	

	
	/**
	 * Fetch the initial data needed for the Eval Guide generation app.
	 *
	 * @param lpId the link params id for the desired eval guide
	 * @return the EGFullData object
	 * @throws Exception
	 */
	public EGFullDataDTO fetchEvalGuide(String lpId)
		throws Exception
	{
		Connection con = null;
		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();

			// get the data
			EGDataHelper helper = new EGDataHelper(con, lpId);        
			EGFullDataDTO data = helper.getEGDisplayData();
				
			return data;
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)  {  /* Swallow the exception */  }
				con = null;
			}
		}
	}	

	
	/**
	 * Save the rater name for Eval Guides (1x - first page).
	 *
	 * @param page1Data - A EGRaterNameDTO object with the rater name,
	 *                    language data, and keys in it
	 * @throws Exception
	 */
	public void saveEgRaterName(EGRaterNameDTO page1Data)
		throws Exception
	{
		Connection con = null;
		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();

			// save the data
			SaveEGRaterNameDataHelper helper = new SaveEGRaterNameDataHelper(con, page1Data);    
			helper.saveRaterName();
				
			return;
		}
		catch (Exception e)
		{
			System.out.println("Error saving rater Name.class  messg=" + e.getMessage());
			throw new Exception(e);
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)  {  /* Swallow the exception */  }
				con = null;
			}
		}
	}	

	
	/**
	 * Save the note and BAR scores for a competency in the Eval Guide(1-n times).
	 *
	 * @param compData an EGCompDataDTO object with BAR score data in it
	 * @throws Exception
	 */
	public void saveEgCompData(EGCompDataDTO compData)
		throws Exception
	{
		Connection con = null;
		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();

			// save the data
			SaveEGCompDataHelper helper = new SaveEGCompDataHelper(con);    
			helper.saveEgScoreData(compData);
				
			return;
		}
		catch (Exception e)
		{
			throw new Exception(e);
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)  {  /* Swallow the exception */  }
				con = null;
			}
		}
	}	

	
	/**
	 * Save the overide score index for a competency in the Eval Guide(1-n times).
	 *
	 * @param csoData an EGCompOverideDataDTO object with score overide data in it
	 * @throws Exception
	 */
	public void saveEgCompOveride(EGCompOverideDataDTO csoData)
		throws Exception
	{
		Connection con = null;
		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();

			// save the data
			SaveEGCompDataHelper helper = new SaveEGCompDataHelper(con);    
			helper.saveCsoData(csoData);
				
			return;
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)  {  /* Swallow the exception */  }
				con = null;
			}
		}
	}	

	
	/**
	 * Save the Impact Ratings data for the Eval Guide(1x - second to last data page).
	 *
	 * @param irScoreData an EGIrScoreDTO object with Impact Rating score data in it
	 * @throws Exception
	 */
	public void saveEgIrData(EGIrDataDTO irData)
		throws Exception
	{
		Connection con = null;
		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();

			// save the data
			SaveEGIrDataHelper helper = new SaveEGIrDataHelper(con, irData);    
			helper.saveIrData();
				
			return;
		}
		catch (Exception e)
		{
			throw new Exception (e);
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)  {  /* Swallow the exception */  }
				con = null;
			}
		}
	}	

	
	/**
	 * Save the "overall" data for the Eval Guide(1x - last data page).
	 *
	 * @param overallData an EGOverallDTO object with "overall" data in it
	 * @throws Exception
	 */
	public void saveEgOverall(EGOverallDTO overallData)
		throws Exception
	{
		Connection con = null;
		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();

			// save the data
			SaveEGOverallDataHelper helper = new SaveEGOverallDataHelper(con, overallData);    
			helper.saveOverallData();
				
			return;
		}
		catch (Exception e)
		{
			throw new Exception(e);
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)  {  /* Swallow the exception */  }
				con = null;
			}
		}
	}	
	
	
	/**
	 * Submit the Eval Guide
	 *
	 * @param partId the participant id
	 * @param dnaId the DNA id
	 * @param modId the module id
	 * @throws Exception
	 */
	public void submitEG(String partId, long dnaId, long modId)
		throws Exception
	{
		Connection con = null;
		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();

			// do the submit
			SubmitEGDataHelper helper = new SubmitEGDataHelper(con, partId, dnaId, modId);
			helper.submitEG();
				
			return;
		}
		catch (Exception e)
		{
			throw new Exception(e);
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)  {  /* Swallow the exception */  }
				con = null;
			}
		}
	}
}