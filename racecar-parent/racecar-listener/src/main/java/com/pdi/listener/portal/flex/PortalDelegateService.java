/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.listener.portal.flex;

/*
 * We'll probably have to work on these (platform specific)
 */
//import com.pdi.data.v2.helpers.UserDataHelper;
//import com.pdi.data.v3.dto.SessionUserDTO;
//import com.pdi.data.v3.dto.UserDTO;
//import java.io.BufferedInputStream;
//import java.io.BufferedReader;
//import java.io.DataOutputStream;
import java.io.InputStream;
//import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
//import java.io.PrintWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;



//import com.pdi.data.constants.DataConstants;
import com.pdi.data.constants.DataConstants;
import com.pdi.data.dto.Client;
import com.pdi.data.dto.Project;
import com.pdi.data.dto.ProjectParticipant;
import com.pdi.data.dto.ProjectParticipantInstrument;
import com.pdi.data.dto.Instrument;
import com.pdi.data.dto.NormGroup;
import com.pdi.data.dto.ProjectParticipantReport;
import com.pdi.data.dto.Participant;
import com.pdi.data.dto.Response;
import com.pdi.data.dto.ResponseGroup;
//import com.pdi.data.dto.Score;
//import com.pdi.data.dto.ScoreGroup;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.dto.TestData;
import com.pdi.data.dto.TextPair;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.IClientHelper;
import com.pdi.data.helpers.interfaces.IEquivalentScoreHelper;
import com.pdi.data.helpers.interfaces.IProjectParticipantInstrumentHelper;
import com.pdi.data.helpers.interfaces.IInstrumentHelper;
import com.pdi.data.helpers.interfaces.INormGroupHelper;
import com.pdi.data.helpers.interfaces.IParticipantHelper;
//import com.pdi.data.helpers.interfaces.IScoringHelper;
import com.pdi.data.helpers.interfaces.ITestDataHelper;
import com.pdi.data.nhn.helpers.delegated.InstrumentHelper;
import com.pdi.data.nhn.util.NhnContentTokenUtil;
import com.pdi.data.nhn.util.NhnDatabaseUtils;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
//import com.pdi.data.v2.helpers.delegated.SessionUserHelper;
//import com.pdi.data.v2.constants.TestDataConstants;
//import com.pdi.data.dto.EquivValueDTO;
import com.pdi.data.dto.EqInfo;
import com.pdi.logging.LogWriter;
import com.pdi.properties.PropertyLoader;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.webservice.NhnWebserviceUtils;

import flex.messaging.FlexContext;
import flex.messaging.FlexSession;

/**
 * PortalServiceDelegated is the second generation of the PortalService.  It uses the
 * delegated data objects to pass data back to the caller.
 *
 * This service provides a generalized communication layer between flex's portal
 * application and the platform data store.
 *
 * @author		Gavin Myers
 */
public class PortalDelegateService
{

		//
		// Static data.
		//

		//
		// Static methods.
		//

		//
		// Instance data.
		//

		//
		// Constructors.
		//
		public PortalDelegateService()
		{
			// Does nothing currently
		}

		//
		// Instance methods.
		//

		/////////////////
		// Private methods
		/////////////////

		/**
		 * internal function to see if the user is connected to the session
		 *
		 * @return boolean that says if we are connected (have a Session User in the Session Context)
		 */
		private boolean isConnected()
		{
			//return 	(FlexContext.getFlexSession().getAttribute("sessionUser") != null);
			return 	true;
		}

		/**
		 * Convenience method to get the Session user from the Flex Context
		 *
		 * @return the SessionUser object
		 */
		private SessionUser getSessionUser()
		{
			//return (SessionUser)FlexContext.getFlexSession().getAttribute("sessionUser");
			return new SessionUser();
		}


		/////////////////
		// Public methods
		/////////////////

		/**
		 * Method to test that there is a connection to the RPC
		 * NOTE:  I don't think that is correct.  I think it only talks to the Flex session
		 * @return string
		 */

		public String comCheck()
		{
			String ret = "Com check succeeded.  User is ";
			if (! isConnected())
				ret += "NOT ";
			ret += "loged in.";

			return ret;
		}


		/**
		 * Log the user into the system, add the user to the session
		 *
		 * NOTE THAT THIS IS INCOMPLETE CODE.  It merely makes a
		 * sessionUser object.  no validity checking yet performed.
		 *
		 * @param username
		 * @param password
		 * @return
		 * @throws Exception
		 */
		public void login(String username, String password)
		{
			// For now assume that everything is peachy and set up the SessionUser object
			// THIS NEEDS WORK BEFORE WE HIT THE BIG TIME
			SessionUser su = new SessionUser();
			su.getMetadata().put("USERNAME", username);
			su.getMetadata().put("PASSWORD", password);

			FlexSession session = FlexContext.getFlexSession();
			session.setAttribute("sessionUser", su);
		}


		/**
		 * get all clients
		 * @return
		 */
		public ArrayList<Client> getClients()
			throws Exception
		{
			if(! this.isConnected())
			{
				throw new Exception("User not logged in (getClients)");
			}

			IClientHelper clientHelper = HelperDelegate.getClientHelper();
			return clientHelper.all(this.getSessionUser());
		}


//		/**
//		 * get all engagements by client
//		 * @param client
//		 * @return
//		 */
//		public ArrayList<Project> getProjects(String clientId)
//			throws Exception
//		{
//			if(! this.isConnected())
//			{
//				throw new Exception("User not logged in (getProjects");
//			}
//
//			IProjectHelper engagementHelper = HelperDelegate.getProjectHelper();
//			return engagementHelper.fromClientId(this.getSessionUser(), clientId);
//		}


		/**
		 * get all participants by engagement (project)
		 * @param engagement ID (project ID)
		 * @return
		 */
		public ArrayList<Participant> getParticipants(String engagementId)
			throws Exception
		{
			if(! this.isConnected())
			{
				throw new Exception("User not logged in (getParticipants");
			}

			IParticipantHelper engPartHelper = HelperDelegate.getParticipantHelper("com.pdi.data.nhn.helpers.delegated");
			return engPartHelper.fromProjectId(this.getSessionUser(), engagementId);
		}


		/**
		 * get all reports for a group of participants
		 * @param engagement ID (project ID)
		 * @return

		public ArrayList<ProjectParticipantReport> getReportsByProjectParticipantList(ArrayList<ProjectParticipant> epList)
			throws Exception
		{
			if(! this.isConnected())
			{
				throw new Exception("User not logged in (getParticipants");
			}

			IProjectParticipantReportHelper eprh = HelperDelegate.getProjectParticipantReportHelper();
			return eprh.fromProjectParticipantList(this.getSessionUser(), epList);

		}		 */

		/**
		 * Does all the "magic" to combine nhn participants with v2 scores
		 */
		private ArrayList<ProjectParticipantInstrument> fromLastParticipantId(String id) throws Exception {

			IProjectParticipantInstrumentHelper ppiHelper = HelperDelegate.getProjectParticipantInstrumentHelper("com.pdi.data.v2.helpers.delegated");
			//IProjectHelper projectHelper = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated");
			IParticipantHelper participantHelper = HelperDelegate.getParticipantHelper("com.pdi.data.nhn.helpers.delegated");

			ArrayList<ProjectParticipantInstrument> ppis = ppiHelper.fromLastParticipantId(this.getSessionUser(),
					IProjectParticipantInstrumentHelper.MODE_SCORE,
					id,
					null);

			Participant participant = participantHelper.fromId(this.getSessionUser(), id);

			for(ProjectParticipantInstrument ppi : ppis) {
				ppi.setParticipant(participant);
			}

			return ppis;
		}

		public ArrayList<ProjectParticipantReport> getReports(ArrayList<ProjectParticipant> projectParticipants) throws Exception {
			try {
			ArrayList<ProjectParticipantReport> pprs = new ArrayList<ProjectParticipantReport>();

			for(ProjectParticipant pp : projectParticipants) {
				//System.out.println(pp.getProject());
				//System.out.println(pp.getParticipant());
				/*if(pp.getProject().getMetadata().get("PROJECT_TYPE") == "TLT") {

				}*/
				//This is a tlt project, only tlt projects exist at this time
				{
					ProjectParticipantReport ppr = new ProjectParticipantReport();
					ppr.setParticipant(pp.getParticipant());
					ppr.setProject(pp.getProject());
					ppr.setReportCode(ReportingConstants.REPORT_CODE_TLT_INDIVIDUAL_SUMMARY);
					ppr.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);
					pprs.add(ppr);
				}

				{
					ProjectParticipantReport ppr = new ProjectParticipantReport();
					ppr.setParticipant(pp.getParticipant());
					ppr.setProject(pp.getProject());
					ppr.setReportCode(ReportingConstants.REPORT_CODE_TLT_GROUP_DETAIL);
					ppr.setReportType(ReportingConstants.REPORT_TYPE_GROUP);
					pprs.add(ppr);
				}

				//ALL INSTRUMENT REPORTS AVAILABLE REGARDLESS OF PROJECT

				for(ProjectParticipantInstrument ppi : fromLastParticipantId(pp.getParticipant().getId())) {
					String reportInstrumentCode = ppi.getInstrument().getMetadata().get("reportingInstId");
					ProjectParticipantReport ppr = new ProjectParticipantReport();
					ppr.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);
					ppr.setParticipant(pp.getParticipant());
					ppr.setProject(pp.getProject());

					//THIS NEEDS TO BE DATA DRIVEN
					if(reportInstrumentCode.equalsIgnoreCase(ReportingConstants.RC_CAREER_SURVEY)) {
						//nothing to do
					} else if (reportInstrumentCode.equalsIgnoreCase(ReportingConstants.RC_CHQ)) {
						ppr.setReportCode(ReportingConstants.REPORT_CODE_CHQ_DETAIL);
						pprs.add(ppr);
					} else if (reportInstrumentCode.equalsIgnoreCase(ReportingConstants.RC_FIN_EX)) {
						//nothing to do
					} else if (reportInstrumentCode.equalsIgnoreCase(ReportingConstants.RC_GPI)) {
						ppr.setReportCode(ReportingConstants.REPORT_CODE_GPI_GRAPHICAL);
						pprs.add(ppr);
					} else if (reportInstrumentCode.equalsIgnoreCase(ReportingConstants.RC_LEI)) {
						ppr.setReportCode(ReportingConstants.REPORT_CODE_LEI_GRAPHICAL);
						pprs.add(ppr);
					} else if (reportInstrumentCode.equalsIgnoreCase(ReportingConstants.RC_RAVENS_B)) {
						ppr.setReportCode(ReportingConstants.REPORT_CODE_RAV_B_GRAPHICAL);
						pprs.add(ppr);
					} else if (reportInstrumentCode.equalsIgnoreCase(ReportingConstants.RC_RAVENS_SF)) {
						ppr.setReportCode(ReportingConstants.REPORT_CODE_RAV_SF_GRAPHICAL);
						pprs.add(ppr);
					} else if (reportInstrumentCode.equalsIgnoreCase(ReportingConstants.RC_WATSON_A_SF)) {
						//nothing to do
					} else if (reportInstrumentCode.equalsIgnoreCase(ReportingConstants.RC_WESMAN)) {
						//nothing to do
					} else if (reportInstrumentCode.equalsIgnoreCase(ReportingConstants.RC_WG_E)) {
						ppr.setReportCode(ReportingConstants.REPORT_CODE_WGE_GRAPHICAL);
						pprs.add(ppr);
					}
				}


			}
			return pprs;
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}
		/**
		 * get a list of all active norms for a particular instrument
		 * @param instrumentCode
		 * @return ArrayList<NormGroup>
		 */
		public ArrayList<NormGroup> getNormsByInstrument(String instrumentCode)
			throws Exception
		{
			//System.out.println("Get Instrument " + instrumentCode);
			if(! this.isConnected())
			{
				throw new Exception("User not logged in (getNormsByInstrument");
			}

			INormGroupHelper helper = HelperDelegate.getNormGroupHelper("com.pdi.data.v2.helpers.delegated");
			return helper.fromInstrumentId(this.getSessionUser(), instrumentCode);
		}

		/*
		 * Alternative method signature that allows the user to pass equivalent score info
		 */
		public void addUpdateParticipantProjectInstrument(ProjectParticipantInstrument ppi, EqInfo eqi)
			throws Exception
		{


			// UPDATE THE RESPONSE DATA FOR FINEX if this is a finex....
			if(ppi.getInstrument().getCode().equals("finex")){

				HashMap<String, String> scoreList = new HashMap<String, String>();

				// add the responses to an hashMap, so we can build the scorm tracks
				ResponseGroup rg = ppi.getResponseGroup();
				Iterator<String> iter = rg.getResponses().keySet().iterator();
				while(iter.hasNext()){
					String q = iter.next();
					Response r = rg.getResponses().get(q);
					//System.out.println("response.get key:  " + r.getCode() + "  " + r.getValue() + "    " + r.getQuestionText());
					scoreList.put(r.getCode(), r.getValue());

				}

				// build the track xml's
				String track1XML = getTrackXML(1, scoreList);
				String track2XML = getTrackXML(2, scoreList);
				String track3XML = getTrackXML(3, scoreList);

				// set up variables
				String userToken = ppi.getParticipant().getId();
				int companyToken =  new Integer(ppi.getProject().getClient().getId()).intValue();
				InstrumentHelper ih = new InstrumentHelper();
				String courseToken = new Integer(ih.fromInstrumentCode(ppi.getInstrument().getCode())).toString() ;

				// first, need to get the tracking token... We use an existing stored procedure..
				// Note that this throws an error because the stored proc won't return the token,
				// even if it's doing all the other parts of the stored procedure
				String trackingToken = NhnContentTokenUtil.makeNewContentToken(userToken,  companyToken, courseToken );
				/*  the stored proc isn't returning the id, so I'm just getting it. */
				trackingToken = getTrackingToken(userToken, companyToken, courseToken);

				String reqTrackParameters = "?action=track&activity=" + courseToken + "&trackingToken=" + trackingToken;
				String reqScoreParameters = "?user_id=" + userToken + "&instrument_id=" + ppi.getInstrument().getCode();
				//"http://mspjappdev1/scorm/track";
				//"http://localhost:8080/pdi-web/PortalServlet";
				String reqTrack = PropertyLoader.getProperty("com.pdi.listener.portal.application", "trackingServlet.url");
				String reqScore = PropertyLoader.getProperty("com.pdi.listener.portal.application", "portalServlet.url");

				//System.out.println("user, company, course, tracking Tokens:  " + userToken + "   " + companyToken + "    " + courseToken + "  " + trackingToken);

				// send the track xml's to the scorm player, to write the
				// response data to the platform database
				makeServletCall(track1XML, reqTrack, reqTrackParameters);
				makeServletCall(track2XML, reqTrack, reqTrackParameters);
				makeServletCall(track3XML, reqTrack, reqTrackParameters);

				// do the scale scoring through the portal servlet...
				String xml = "";
				makeServletCall(xml, reqScore, reqScoreParameters);

			} else {
				// IF THIS IS NOT A FINEX..

				// do what we always do
				// do the NORMAL update
				addUpdateParticipantProjectInstrument(ppi);
			}

			if (eqi == null)
			{
				// Nothing more to do
				return;
			}

			// Add the eq info to the database
			IEquivalentScoreHelper hlpr = HelperDelegate.getEquivalentScoreHelper("com.pdi.data.v2.helpers.delegated");
			hlpr.writeEquivalentLog(eqi);
		}

		/*
		 *
		 *
		 */
		private void addUpdateParticipantProjectInstrument(ProjectParticipantInstrument ppi) throws Exception {
			//System.out.println(" addUpdateParticipantProjectInstrument(ProjectParticipantInstrument ppi)");
			HelperDelegate.getProjectParticipantInstrumentHelper("com.pdi.data.v2.helpers.delegated").addUpdate(this.getSessionUser(), IProjectParticipantInstrumentHelper.MODE_SCORE, ppi, true);
			//UI allows all or none, so we only need to find 1 score/null- if score. set status to complete. if null, don't set status


			for (String key : ppi.getScoreGroup().getScores().keySet()) {

				if(ppi.getScoreGroup().getScores().get(key).getRawScore() != null){
					HelperDelegate.getProjectParticipantInstrumentHelper("com.pdi.data.nhn.helpers.delegated").addUpdate(this.getSessionUser(), IProjectParticipantInstrumentHelper.MODE_SCORE, ppi, true);

					// Check to see if this is a cs2 or cs -- we need to update both....
					if(ppi.getInstrument().getCode().equals("cs")){

						// now make it a cs2, and update the status in nhn
						ppi.getInstrument().setCode("cs2");
						HelperDelegate.getProjectParticipantInstrumentHelper("com.pdi.data.nhn.helpers.delegated").addUpdate(this.getSessionUser(), IProjectParticipantInstrumentHelper.MODE_SCORE, ppi, true);

					}else if(ppi.getInstrument().getCode().equals("cs2")){

						// now make it a cs, and update the status in nhn
						ppi.getInstrument().setCode("cs");
						HelperDelegate.getProjectParticipantInstrumentHelper("com.pdi.data.nhn.helpers.delegated").addUpdate(this.getSessionUser(), IProjectParticipantInstrumentHelper.MODE_SCORE, ppi, true);

					}

					// pps:  we don't have to worry about having changed the instrument id above, because we are not sending the ppi back.
					// ppps:  there WILL be status rows for both instruments in nhn, so this remains an update, and the code doesn't need to be changed to insert

					return;
				}else{
					return;
				}
			}

			//System.out.println("outside for loop......");
		}


		/**
		 * get a list of all active instruments
		 * @return ArrayList<Instrument>
		 */
		public ArrayList<Instrument> getAllInstrumentList()
			throws Exception
		{
			if(! this.isConnected())
			{
				throw new Exception("User not logged in (getAllInstruments");
			}

			IInstrumentHelper helper = HelperDelegate.getInstrumentHelper();
			return helper.all(this.getSessionUser(), IInstrumentHelper.TYPE_ALL);
		}


		/**
		 * get a list of all active scoreable instruments
		 * @return ArrayList<Instrument>
		 */
		public ArrayList<Instrument> getScoreableInstrumentList()
			throws Exception
		{
			if(! this.isConnected())
			{
				throw new Exception("User not logged in (getScoreableInstruments");
			}

			IInstrumentHelper helper = HelperDelegate.getInstrumentHelper();
			return helper.all(this.getSessionUser(), IInstrumentHelper.TYPE_SCORABLE);
		}

		/**
		 * Get the CogsDone flag for this participant/dna
		 * This is called by the TestData application, on entry,
		 * to set the cogs done button.
		 * The Test Data application will pass over a TestData object,
		 * which will be returned with the cogsCompleted attribute properly set.
		 *
		 * @param TestData testData - the test data object from the Test Data app.
		 * @return boolean
		 */
		public TestData getCogsDoneFlag(TestData testData)
			throws Exception
		{
			ITestDataHelper helper = HelperDelegate.getTestDataHelper("com.pdi.data.v2.helpers.delegated");

			// 1  check to see that we have a dnaId set in testData.
			// 2  if not, use the helper to get it
			if(testData.getDnaId() == -1){
				// it's not been set
				testData.setDnaId(helper.getDnaIdFromProjectId(this.getSessionUser(), testData));
			}

			// 3  do the select and set the flag
			testData.setCogsCompleted(helper.getCogsDoneFlag(this.getSessionUser(), testData));

			return testData;
		}

		/**
		 * Update the CogsDone flag for this participant/dna
		 * If there is no existing data row, add/insert that row
		 *
		 * This is called by the TestData application, when the admin
		 * clicks on the Cogs Completed button.
		 *
		 * The Test Data application will pass over a TestData object
		 *
		 * @param TestData testData - the test data object from the Test Data app.
		 */
		public void addUpdateCogsDoneFlag(TestData testData)
			throws Exception
		{

			ITestDataHelper helper = HelperDelegate.getTestDataHelper("com.pdi.data.v2.helpers.delegated");

			// 1  check to see that we have a dnaId set in testData.
						// it should be there i think
			// 2  if not, use the helper to get it
			if(testData.getDnaId() == -1){
				// it's not been set
				testData.setDnaId(helper.getDnaIdFromProjectId(this.getSessionUser(), testData));
			}

			// 3  do the save
			try
			{
				helper.saveCogsDoneFlag(getSessionUser(), testData);

			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		/**
		 * Get a list of all instruments a participant has taken.
		 * If there are dups, return only the most recent one.
		 *
		 * This is used for the Test Data application, on initial open
		 *
		 *
		 * NOTE: 09-21-2011 -- RULES AROUND GPI/GPIL, CS/CS2
		 *
		 * For both TLT and AbyD : show all enrolled instruments
		 * EXCEPT:
		 * 1 - never show the GPI (SHORT)
		 * 2 - Career survey - whichever one is assigned (if none are completed)/completed will show.
		 *	If both are assigned, and only one is completed, the completed one will show.
		 *	If both are assigned, and neither are completed, only show the cs2.
		 *	if both are assigned and the cs is completed, then the cs will show up, cuz it's done, and the cs2 won't.
		 *
		 * 01-23-2012 : NHN 1738: new use case, with participant in two
		 * different AbyD projects, so different sets of norms
		 * adding new path of methods... fromLastParticipantIdIncludeProjectIdForNorms()
		 * usecase: I the most recent GPI for this participant, but he
		 * is in two different projects, with different norms.  I want the
		 * norms for the current project.
		 *
		 * @return ArrayList<ArrayList<ProjectParticipantInstrument>>
		 */

		// NOTE:  Functionality should always be done in a helper method.  Move this
		//        code to a helper (TestDataHelper?) when time allows.

		public ArrayList<ProjectParticipantInstrument> getLatestScoresForPart(String partId, String projId)

		{
			try
			{
				if(! this.isConnected())
				{
					throw new Exception("User not logged in (getLatestScoresForPart(" + partId + ", " + projId + ")");
				}

				// Get participant info
				Participant participant = HelperDelegate.getParticipantHelper("com.pdi.data.nhn.helpers.delegated").fromId(this.getSessionUser(), partId);

				// Get project info
				Project project = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated").fromId(getSessionUser(), projId);
				if(project == null)
				{
					//System.out.println("No Projects");
					return null;
				}

				//String clientId = project.getClient().getId();

				// Get instrument info for this participant (ppi)
				IProjectParticipantInstrumentHelper helper = HelperDelegate.getProjectParticipantInstrumentHelper("com.pdi.data.v2.helpers.delegated");


				ArrayList<ProjectParticipantInstrument> ppis = helper.fromLastParticipantIdIncludeProjectIdForNorms(this.getSessionUser(),
						IProjectParticipantInstrumentHelper.MODE_SCORE,
						partId, projId);

				// get course list from webservice
				// includes both gpi's and both cs's, so still need to check for those... .
				//NhnWebserviceUtils wsu = new NhnWebserviceUtils();
				//ArrayList<String> courseListFromProj = wsu.fetchCourseListForProject(PropertyLoader.getProperty("com.pdi.listener.portal.application", "courseList.url"), project.getId());
				//ArrayList<String> courseListFromProj = new NhnWebserviceUtils()
				//				.fetchCourseListForProject(PropertyLoader.getProperty("com.pdi.listener.portal.application", "courseList.url"), project.getId());
				ArrayList<String> courseListFromProj = new NhnWebserviceUtils().getFilteredProjectCourseList(project.getId());
				if (courseListFromProj.size() < 1)
				{
					//System.out.println("getLatestScoresForPart:  No courses returned for project " + projId + " (NhnWebserviceUtils.fetchCourseListForProject() call).");
					throw new Exception("getLatestScoresForPart:  No courses returned for project " + projId + " (NhnWebserviceUtils.fetchCourseListForProject() call).");
				}
//				for(String s : courseListFromProj){
//					System.out.println("540 course abbv:  " + s);
//
//				}


				// remove any instruments from the list of ppis that aren't in  the
				// courseListFromProj (except cs2)
				Iterator<ProjectParticipantInstrument> ppiterator = ppis.iterator();
				while(ppiterator.hasNext()) {
					ProjectParticipantInstrument ppi = ppiterator.next();

					boolean found = false;
					// set fully formed project and participant from NHN db
					ppi.setParticipant(participant);
					ppi.setProject(project);
					if(courseListFromProj.contains(ppi.getInstrument().getCode())){
						//System.out.println("in procList... " + ppi.getInstrument().getCode());
						found = true;
					}
					if(!found){
						//System.out.println("not in procList... " + ppi.getInstrument().getCode());
						if(ppi.getInstrument().getCode().equals("cs2")){
							// leave this because of the heirarchy rules around cs/cs2
						}else{
							ppiterator.remove();
						}

					}
				}

				// compare against courseListFromProj
				// and make sure there are scales...
				ArrayList<ProjectParticipantInstrument> removeList = new ArrayList<ProjectParticipantInstrument>();

				for(ProjectParticipantInstrument ppi : ppis)
				{

					if(courseListFromProj.contains(ppi.getInstrument().getCode()) && ppi.getScoreGroup() == null )
					{
						// it's in the list, but HAS NO SCORES
						// go and get the scales....
						ProjectParticipantInstrument newPpi = helper.getPPIWithoutScoredData(this.getSessionUser(), project, participant, ppi.getInstrument().getCode());
						ppi.setScoreGroup(newPpi.getScoreGroup());
						//System.out.println("no scores, getting scales:  " + ppi.getInstrument().getCode());
					}
					else if( courseListFromProj.contains(ppi.getInstrument().getCode()) )
					{
						// it's in the list and HAS SCORES
						// leave it...
					}
					else
					{

						// it's NOT in the allowable instrument list, so remove it....
						// NOTE: don't mess with the cs/cs2 thing... or the gpi thing....
						// those rules should have been handled through the stored proc
						if(!ppi.getInstrument().getCode().equals("cs")
								&& !ppi.getInstrument().getCode().equals("cs2")
								&& !ppi.getInstrument().getCode().equals("gpi")
								&& !ppi.getInstrument().getCode().equals("gpil")){

							ProjectParticipantInstrument tempPPI = ppi;
							//System.out.println("not in testdata list, removing: " + ppi.getInstrument().getCode());
							removeList.add(tempPPI);
						}

						if(ppi.getInstrument().getCode().equals("cs2") && ppi.getScoreGroup() == null){
                           // Updated for     NHN-2403  - AbyD - Flash error occurs after importing existing participant into project
                           //  and attempting to set norm for CS2 if participant has no CS2 data.
							//System.out.println("it's NOT in the course list, but is a CS2 and HAS NO SCORES.........." + ppi.getInstrument().getCode());
							// it's NOT in the course list, but is a CS2 and HAS NO SCORES
							// go and get the scales....
							ProjectParticipantInstrument newPpi = helper.getPPIWithoutScoredData(this.getSessionUser(), project, participant, ppi.getInstrument().getCode());
							ppi.setScoreGroup(newPpi.getScoreGroup());
							//System.out.println("no scores, getting scales:  " + ppi.getInstrument().getCode());


						}
					}
				}

				// if there are extraneous instruments, remove those....
				if(removeList.size() > 0)
				{
					for(ProjectParticipantInstrument ppi : removeList)
					{
						if(ppis.contains(ppi))
						{
							ppis.remove(ppi);
						}
					}
				}

				// Finally take out any instruments that the participant is not enrolled/registered for
				// get the enrolled courses - per NHN 1346
				HashSet<String> ec = getEnrolledCourseIds(partId);

				// loop through the ppis and if the inst is not in the enrolled list then remove it
				Iterator<ProjectParticipantInstrument> itr = ppis.iterator();
				while(itr.hasNext())
				{
					ProjectParticipantInstrument ppi = itr.next();

					String code = ppi.getInstrument().getCode();
					if (! ec.contains(code))
					{
						//System.out.println("not enrolled: removing: " + code);
						itr.remove();
					}

				}



				// Note that we have to crank back in the rules on cs/cs2 and gpi/gpil again


				//===============================================================
				//===============================================================
				// check for CS/CS2
				// * 2 - Career survey - whichever one is assigned (if none are completed)/completed will show.
				// *	If both are assigned, and only one is completed, the completed one will show.
				// *	If both are assigned, and neither are completed, only show the cs2.
				// *	if both are assigned and the cs is completed, then the cs will show up, cuz it's done, and the cs2 won't.
				// *    If both are assigned and both are completed, cs2 has precedence, only show cs2
				// THERE MAY BE A MORE ELEGANT WAY TO DO THIS.  I WENT FOR SIMPLE/LOGICAL/WORKABLE.
				boolean hasCS_withScores = false;
				//boolean hasCS_noScores = false;
				boolean hasCS2_withScores = false;
				//boolean hasCS2_noScores = false;
				// look for cs
				Iterator<ProjectParticipantInstrument> itr1 = ppis.iterator();
				while(itr1.hasNext()) {
					ProjectParticipantInstrument ppi = itr1.next();
					if(ppi.getInstrument().getCode().equals("cs")){
						if(ppi.getScoreGroup()== null || ppi.getScoreGroup().getScores().isEmpty()){
							// do nothing
							//hasCS_noScores = true;
						}else{
							hasCS_withScores = true;
						}
					}

					// look for cs2
					if(ppi.getInstrument().getCode().equals("cs2")){
						if(ppi.getScoreGroup()== null || ppi.getScoreGroup().getScores().isEmpty()){
							// do nothing
							//hasCS2_noScores = true;
						}else{
							hasCS2_withScores = true;
						}
					}
				}


				// look for the gpi...
				// rule is that gpi (short) does not appear in test Data
				// remove gpi from list
				Iterator<ProjectParticipantInstrument> itr6 = ppis.iterator();
				while(itr6.hasNext()) {
					ProjectParticipantInstrument ppi6 = itr6.next();
					if(ppi6.getInstrument().getCode().equals("gpi")){
						itr6.remove();
						//System.out.println("remove gpi ... ");
					}
				}


				// deal with cs/cs2 rules.......
				//System.out.println("cs, hasCS_withScores=" + hasCS_withScores);
				//System.out.println("cs2, hasCS2_withScores=" + hasCS2_withScores);

				if(hasCS_withScores && hasCS2_withScores){
					// cs2 takes precedence -- remove cs from list
					Iterator<ProjectParticipantInstrument> itr2 = ppis.iterator();
					while(itr2.hasNext()) {
						ProjectParticipantInstrument ppi = itr2.next();
						if(ppi.getInstrument().getCode().equals("cs")){
							itr2.remove();
							//System.out.println("has both w/ scores: remove cs ... ");
						}
					}
				}


				// this shouldn't apply at this point, because
				// there would be
				if(!hasCS_withScores && !hasCS2_withScores){
					// cs2 takes precedence -- remove cs from list
					Iterator<ProjectParticipantInstrument> itr3 = ppis.iterator();
					while(itr3.hasNext()) {
						ProjectParticipantInstrument ppi = itr3.next();
						if(ppi.getInstrument().getCode().equals("cs")){
							itr3.remove();
							//System.out.println("has both w/out scores: remove cs ... ");
						}
					}
				}

				if(hasCS_withScores && !hasCS2_withScores){
					// use the scored instrument -- remove cs2 from list
					Iterator<ProjectParticipantInstrument> itr4 = ppis.iterator();
					while(itr4.hasNext()) {
						ProjectParticipantInstrument ppi = itr4.next();
						if(ppi.getInstrument().getCode().equals("cs2")){
							itr4.remove();
							//System.out.println("has cs scores: remove cs2 ... ");
						}
					}
				}
				if(hasCS2_withScores && !hasCS_withScores){
					// use the scored instrument -- remove cs from list
					Iterator<ProjectParticipantInstrument> itr5 = ppis.iterator();
					while(itr5.hasNext()) {
						ProjectParticipantInstrument ppi = itr5.next();
						if(ppi.getInstrument().getCode().equals("cs")){
							itr5.remove();
							//System.out.println("has cs2 w/ scores: remove cs ... ");
						}
					}
				}

				//===============================================================
				//===============================================================


				// Add the equivalency data here for each instrument

				// Get a list of instruments with equivalency
				// loop through the inst list (PPI list) and compare to the equiv list
				// No match? Cntinue
				//Match?  Get the list of key/value pairs and stuff it into instrument

				// Requires mods to Instrument object

				//===============================================================
				//===============================================================

				// Check here for the finEx
				for(ProjectParticipantInstrument ppi : ppis)
				{
					if(ppi.getInstrument().getCode().equals("finex")){

						//If it’s there, create the new helper, and get the response data.
						IProjectParticipantInstrumentHelper nhnHelper = HelperDelegate.getProjectParticipantInstrumentHelper("com.pdi.data.nhn.helpers.delegated");
						ResponseGroup rg = nhnHelper.getResponseGroupByParticipantId(ppi.getParticipant().getId(), ppi.getInstrument().getCode());

						//ArrayList<String> scoreList = new ArrayList<String>();

						if(rg.getResponses().size() == 0){

							Iterator<String> i = DataConstants.FINEX_QUES_TEXT.keySet().iterator();
								while(i.hasNext()){
									String key = i.next();
									String ques = DataConstants.FINEX_QUES_TEXT.get(key);
									Response r = new Response();
									r.setQuestionText(ques);
									r.setCode(key);
									rg.getResponses().put(key, r);
									//System.out.println("response.get key:  " + r.getCode() +  "    " + r.getQuestionText());


								}

							}else{

							//Also grab the constants hash map, and spin through the response data, filling in the question text data.
							Iterator<String> iter = rg.getResponses().keySet().iterator();
							while(iter.hasNext()){

								String q = iter.next();
								Response r = rg.getResponses().get(q);
								r.setQuestionText(DataConstants.FINEX_QUES_TEXT.get(q));
								//System.out.println("response.get key:  " + r.getCode() + "  " + r.getValue() + "    " + r.getQuestionText());

							}
						}



						ppi.setResponseGroup(rg);

					}
				}

				// just checking.....
//				for(ProjectParticipantInstrument ppi : ppis)
//				{
//					if(ppi.getInstrument().getCode().equals("finex")){
//
//						System.out.println(" i have a finEX.......");
//						System.out.println(" ppi.getResponseGroup().getResponses().size()......" + ppi.getResponseGroup().getResponses().size());
//
//					}
//				}

				return ppis;
			}
			catch (Exception e)
			{
				e.printStackTrace();
				return null;
			}
		}


		/*
		 * Method to get courses in which the participant is enrolled
		 * Is not a part of the interface
		 */
		//
		private HashSet<String> getEnrolledCourseIds(String participantId)
		{
			HashSet<String> ret = new HashSet<String>();

			StringBuffer sb = new StringBuffer();
			sb.append("SELECT cc.abbv AS cId");
			sb.append("  FROM platform.dbo.roster rr ");
			sb.append("    LEFT JOIN platform.dbo.course cc ON cc.course = rr.courseID ");
			sb.append("  WHERE rr.users_id = " + participantId);

			DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(sb);
			if (dlps.isInError())
			{
				LogWriter.logSQL(LogWriter.WARNING, this,
						"Error preparing Instrument fromId(SessionUser session, String instrumentId) query." + dlps.toString(), sb.toString());
				return ret;
			}

			DataResult dr = null;
			ResultSet rs = null;
			try
			{
				dr = NhnDatabaseUtils.select(dlps);
				rs = dr.getResultSet();
				while (rs.next())
				{
					ret.add(rs.getString("cId"));
				}
				rs.close();

				return ret;
			}
			catch(java.sql.SQLException se)
			{
				LogWriter.logSQLWithException(LogWriter.ERROR, this, se.getMessage(), sb.toString(), se);
				return ret;
			}
			finally
			{
				if (dr != null) {dr.close(); dr = null;}
			}
		}


	// stuff for alternative instrument score input

	/**
	 * Get the legacy equivalent instrument lists.
	 *
	 * Called in the setup for the Test Data application.
	 * Returns a HashMap; key is the instrument code, value is an ArrayList (in order)
	 * of the legacy instruments associated with the key instrument.  Each entry in the
	 * ArrayList is a KeyValuePairString object with a pseudo instrument code for the
	 * old instrument and the instrument name display string as contents.
	 *
	 * Derived from the Target use stories, Section 1.8
	 * 		"As a PC/PM I need to be able to enter scores from older versions of the
	 * 		Watson-Glaser and Ravens into Test Data in PALMS."
	 *
	 * @return The above described HashMap.  A value of null means that there was an error
	 *         or there is no data to return (no equivalencies are present in the system).
	 */
	public HashMap<String, ArrayList<TextPair>> getEquivalencyLists()
	{
		try
		{
			IEquivalentScoreHelper hlpr = HelperDelegate.getEquivalentScoreHelper("com.pdi.data.v2.helpers.delegated");
			return hlpr.fetchInstEquivLists();
		}
		catch (Exception e)
		{
			return null;
		}
	}


	/**
	 * Get an equivalency value.
	 *
	 * Called in the Test Data UI when the user has selected an old instrument,
	 * and has entered a score.
	 *
	 * Derived from the Target use stories, Section 1.8
	 * 		"As a PC/PM I need to be able to enter scores from older versions of the
	 *
	 * 		Watson-Glaser and Ravens into Test Data in PALMS."
	 *
	 * @param key A key value to use to look for an equivalent score
	 * @param val A value to use to look for the equivalent value in the database
	 * @return An Sring with the equivalent score value.  A value of null indicates
	 *         one of the following:
	 *         -- Invalid input code.
	 *         -- Invalid input score (either not a valid number or there is no corresponding lookup).
	 */
	public String getEquivalentScore(String oldInst, String score)
	{
		try
		{
			IEquivalentScoreHelper hlpr = HelperDelegate.getEquivalentScoreHelper("com.pdi.data.v2.helpers.delegated");
			return hlpr.fetchEquivalentScore(oldInst, score);
		}
		catch (Exception e)
		{
			return null;
		}
	}

	/**
	 * This is a back-up because the stored procedure that
	 * CREATES the tracking token isn't returning it,
	 * and I need a way to get it.
	 * Probably, should figure out what's up with the
	 * stupid stored proc., but it's being used by other
	 * parts of the code base, so I can't change it.
	 *
	 * @param String usersId (participant id)
	 * @param int companyId
	 * @param String contentId (this is the numeric instrument id from nhn)
	 * @return String (the tracking token)
	 */

	private String getTrackingToken(String usersId, int companyId, String contentId)
	{
		String ret = "";

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT guid");
		sb.append("  FROM platform.dbo.contentToken  ");
		sb.append("  WHERE users_id = " + Integer.parseInt(usersId) + " ");
		sb.append("  AND content_id = '" + contentId + "' ");
		sb.append("  AND company_id = " + companyId);

		DataLayerPreparedStatement dlps = NhnDatabaseUtils.prepareStatement(sb);
		if (dlps.isInError())
		{
			LogWriter.logSQL(LogWriter.WARNING, this,
					"Error preparing getTrackingToken query." + dlps.toString(), sb.toString());
			return ret;
		}

		DataResult dr = null;
		ResultSet rs = null;
		try
		{
			dr = NhnDatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			while (rs.next())
			{
				ret = rs.getString("guid");
			}
			rs.close();

			return ret;
		}
		catch(java.sql.SQLException se)
		{
			LogWriter.logSQLWithException(LogWriter.ERROR, this, se.getMessage(), sb.toString(), se);
			return ret;
		}
		finally
		{
			if (dr != null) {dr.close(); dr = null;}
		}
	}

	/**
	 * This is used for the TestData application only.
	 * This builds the tracking xml for the FINEX only.
	 * There are 3 tracks that need to be built
	 * to be sent to the scorm engine with response data.
	 *
	 * @param int trackId
	 * @param HashMap<String, String> scoreList
	 * @return String (the tracking XML)
	 */
	public String getTrackXML(int trackId, HashMap<String, String> scoreList){
		StringBuffer sbTrack = new StringBuffer();

		if(trackId == 1){
			//give back track 1 xml
			sbTrack.append("<course_track root_id=\"ORG_FINEX_1\" currentActivity=\"ORG_FINEX_1.SCO_FINEX_1\" courseType=\"40\" suspendedActivity=\"none\">");
			sbTrack.append("<activity id=\"ORG_FINEX_1.SCO_FINEX_1\" suspended=\"false\" active=\"true\" availableChildren=\"\">");
			sbTrack.append("<imssActivityProgressObj progressStatus=\"true\" absoluteDuration=\"0\" experiencedDuration=\"0\" attemptCount=\"1\"/>");
			sbTrack.append("<imssAttemptProgressObj attemptProgressStatus=\"true\" attemptCompletionStatus=\"true\" attemptCompletionAmount=\"1\" attemptAbsoluteDuration=\"0\" attemptExperiencedDuration=\"0\"/>");
			sbTrack.append("<imssObjectiveProgressObj id=\"primaryObjective\" objectiveProgressStatus=\"true\" objectiveSatisfiedStatus=\"unknown\" objectiveMeasureStatus=\"true\" objectiveNormalizedMeasure=\"0.0\" isFromPersistedData=\"true\"/>");
			sbTrack.append("<cmiTrackingObj identifierref=\"SCO_FINEX_1_RES\" completion_status=\"completed\" success_status=\"unknown\" location=\"900[::|::]1756153;0\" progress_measure=\"1\" session_time=\"66973\" exit=\"\" entry=\"ab-initio\">");
			sbTrack.append("<suspend_data>");
			sbTrack.append("<![CDATA[n1|c;i1|ncf;n2|c;i2|ict;n3|c;i100|cnt;i101|cnt;i200|cnt;i900|cnt;]]>");
			sbTrack.append("</suspend_data>");
			sbTrack.append("<cmiScore scaled=\"0.0\" raw=\"0.0\" min=\"0\" max=\"100\"/>");
			sbTrack.append("<cmiInteractionsObj/>");
			sbTrack.append("<cmiObjectives/>");
			sbTrack.append("<sspBucketCollection bucketCount=\"0\">");
			sbTrack.append("<buckets/>");
			sbTrack.append("<success/>");
			sbTrack.append("</sspBucketCollection>");
			sbTrack.append("</cmiTrackingObj>");
			sbTrack.append("</activity>");
			sbTrack.append("</course_track>");

			//System.out.println("TRACK1 = " + sbTrack.toString());

		}

		if(trackId == 2){
			// give back track 2 xml
			sbTrack.append("<course_track root_id=\"ORG_FINEX_1\" currentActivity=\"ORG_FINEX_1\" courseType=\"40\" suspendedActivity=\"ORG_FINEX_1.SCO_FINEX_1\">");
			sbTrack.append("<activity id=\"ORG_FINEX_1.SCO_FINEX_1\" suspended=\"true\" active=\"false\" availableChildren=\"\">");
			sbTrack.append("<imssActivityProgressObj progressStatus=\"true\" absoluteDuration=\"0\" experiencedDuration=\"0\" attemptCount=\"1\"/>");
			sbTrack.append("<imssAttemptProgressObj attemptProgressStatus=\"true\" attemptCompletionStatus=\"true\" attemptCompletionAmount=\"1\" attemptAbsoluteDuration=\"0\" attemptExperiencedDuration=\"0\"/>");
			sbTrack.append("<imssObjectiveProgressObj id=\"primaryObjective\" objectiveProgressStatus=\"true\" objectiveSatisfiedStatus=\"unknown\" objectiveMeasureStatus=\"true\" objectiveNormalizedMeasure=\"0.0\" isFromPersistedData=\"true\"/>");
			sbTrack.append("</activity>");
			sbTrack.append("<activity id=\"ORG_FINEX_1\" suspended=\"true\" active=\"false\" availableChildren=\"ORG_FINEX_1.SCO_FINEX_1\">");
			sbTrack.append("<imssActivityProgressObj progressStatus=\"true\" absoluteDuration=\"0\" experiencedDuration=\"0\" attemptCount=\"1\"/>");
			sbTrack.append("<imssAttemptProgressObj attemptProgressStatus=\"true\" attemptCompletionStatus=\"true\" attemptCompletionAmount=\"1\" attemptAbsoluteDuration=\"0\" attemptExperiencedDuration=\"0\"/>");
			sbTrack.append("<imssObjectiveProgressObj id=\"primaryObjective\" objectiveProgressStatus=\"true\" objectiveSatisfiedStatus=\"false\" objectiveMeasureStatus=\"true\" objectiveNormalizedMeasure=\"0\" isFromPersistedData=\"true\"/>");
			sbTrack.append("</activity>");
			sbTrack.append("</course_track>");

			//System.out.println("TRACK2 = " + sbTrack.toString());

		}

		if(trackId == 3){

			// give back response track
			sbTrack.append("<course_track root_id=\"ORG_FINEX_1\" currentActivity=\"ORG_FINEX_1.SCO_FINEX_1\" courseType=\"40\" suspendedActivity=\"none\">");
			sbTrack.append("<activity id=\"ORG_FINEX_1.SCO_FINEX_1\" suspended=\"false\" active=\"true\" availableChildren=\"\">");
			sbTrack.append("<imssActivityProgressObj progressStatus=\"true\" absoluteDuration=\"0\" experiencedDuration=\"0\" attemptCount=\"1\" />");
			sbTrack.append("<imssAttemptProgressObj attemptProgressStatus=\"true\" attemptCompletionStatus=\"false\" attemptCompletionAmount=\"0.5\" attemptAbsoluteDuration=\"0\" attemptExperiencedDuration=\"0\" />");
			sbTrack.append("<imssObjectiveProgressObj id=\"primaryObjective\" objectiveProgressStatus=\"true\" objectiveSatisfiedStatus=\"unknown\" objectiveMeasureStatus=\"true\" objectiveNormalizedMeasure=\"0.0\" isFromPersistedData=\"true\" />");
			sbTrack.append("<cmiTrackingObj identifierref=\"SCO_FINEX_1_RES\" completion_status=\"incomplete\" success_status=\"unknown\" location=\"200[::|::]1800000;0\" progress_measure=\"0.5\" session_time=\"19328\" exit=\"\" entry=\"ab-initio\">");
			sbTrack.append("<suspend_data><![CDATA[n1|s;i1|nsf;n2|s;i2|ist;n3|s;i100|cnt;i101|cnt;i200|snt;i900|nnf;]]></suspend_data>");
			sbTrack.append("<cmiScore scaled=\"0.0\" raw=\"0.0\" min=\"0\" max=\"100\" />");
			sbTrack.append("<cmiInteractionsObj>");

			// for 13 questions
			for(int i=1; i<=13; i++){
				sbTrack.append("<cmiInteractionObj id=\"Q" + i + "\" type=\"other\" timestamp=\"\" weighting=\"\" result=\"neutral\" latency=\"\" description=\"\">");
				sbTrack.append("<cmiObjectives />");
				sbTrack.append("<cmiCorrectResponsesObj>");
				sbTrack.append("<cmiResponsePatternObj pattern=\"\" />");
				sbTrack.append("</cmiCorrectResponsesObj>");
				sbTrack.append("<learner_response><![CDATA[" + scoreList.get("Q"+i) + "]]></learner_response>");
				sbTrack.append("</cmiInteractionObj>");
			}

			sbTrack.append("</cmiInteractionsObj>");
			sbTrack.append("<cmiObjectives />");
			sbTrack.append("<sspBucketCollection bucketCount=\"0\">");
			sbTrack.append("<buckets />");
			sbTrack.append("<success />");
			sbTrack.append("</sspBucketCollection>");
			sbTrack.append("</cmiTrackingObj>");
			sbTrack.append("</activity>");
			sbTrack.append("</course_track>");

			//System.out.println("RESPONSE TRACK: " + sbTrack.toString());
		}

		return sbTrack.toString();
	}

	/**
	 * This is used for the TestData application.
	 * It creates a url connection to the incoming baseUrl.
	 * In the case of TestData, it's sending to the scorm
	 * tracking servlet, or to the portalServlet.
	 *
	 * @param String trackXML
	 * @param String baseUrl
	 * @param String urlParameters
	 */
	public void makeServletCall(String trackXML, String baseUrl, String urlParameters)	{

		try
		{
			URL url = new URL(baseUrl + urlParameters);
			System.out.println(" url: " + url.toString());
			HttpURLConnection connection = (HttpURLConnection) url.openConnection(); // connect to the servlet
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setInstanceFollowRedirects(false);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "text/xml; charset=UTF-8");
			connection.setRequestProperty("charset", "UTF-8");
			connection.setRequestProperty("Content-Length", "" + Integer.toString(trackXML.getBytes().length));
			connection.setUseCaches(true);

//			DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
//			wr.writeBytes(trackXML);  // write the xml (technically, track xml) to the output
//			wr.flush();

//			PrintWriter pw = new PrintWriter(conn.getOutputStream());
//	        pw.write(trackXML);
//	        pw.close();

			OutputStream out = connection.getOutputStream();
	        Writer writer = new OutputStreamWriter(out, "UTF-8");
	        writer.write(trackXML);
	        writer.flush();
	        writer.close();

	        // seems to need the instream to be here, used or not.
	        try{
		        InputStream in = connection.getInputStream();
		        in.close();
	        }catch (Exception e){
	        	System.out.println(" error on the input stream, being swallowed. ");
	        }

//	        try{
//		        BufferedInputStream bis = new BufferedInputStream(connection.getInputStream());
//		        bis.close();
//	        }catch (Exception e){
//	        	System.out.println(" error on the input stream, being swallowed. ");
//	        }

//	       try
//	       {
//				InputStreamReader isr = new InputStreamReader(connection.getInputStream(), "UTF-8"); // get this back from the servlets
//				BufferedReader br = new BufferedReader(isr);
//				String output = "";
//				String line = "";
//				while ((line = br.readLine()) != null) {
//					output += line;
//				}
//				System.out.println("request output: " + output);
//				connection.disconnect();
//	       } catch (Exception e){
//	    	   System.out.println(" error on the input stream, being swallowed. ");
//	       }

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
