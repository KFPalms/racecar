package com.pdi.listener.portal.servlets.ut;

//import org.apache.commons.httpclient.HttpClient;
//import org.apache.commons.httpclient.methods.PostMethod;

import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

class PostScores extends Thread {
    public PostScores() {
    	super();
    }
//    public void run() {
//    	try {
//		 // Configure the form parameters
//		 method.addParameter( "user_id", this.getUserId());
//		 method.addParameter( "instrument_id", this.getInstrumentId() );
//	     client.executeMethod( method );
//    	} catch (Exception e ) { e.printStackTrace(); }
//    }
    public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserId() {
		return userId;
	}
	public void setInstrumentId(String instrumentId) {
		this.instrumentId = instrumentId;
	}
	public String getInstrumentId() {
		return instrumentId;
	}
//    private HttpClient client = new HttpClient();
//    PostMethod method = new PostMethod( "http://localhost:8080/pdi-web/PortalServlet" );
    
	private String userId;
    private String instrumentId;
}

public class PortalServletUT extends TestCase 
{

	public PortalServletUT(String name)
	{
		super(name);
	}
	
	public void testAlive()
	throws Exception
	{
		UnitTestUtils.start(this);
		
		PostScores ps = new PostScores();
		ps.setInstrumentId("rv");
		ps.setUserId("559133");
		ps.start();
		
		UnitTestUtils.stop(this);
	}

	
}
