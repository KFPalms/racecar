/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.listener.portal.delegated.helpers.reports;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.pdi.data.dto.NormScore;
import com.pdi.data.dto.ProjectParticipantInstrument;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.Report;
import com.pdi.reporting.report.ReportData;
import com.pdi.reporting.report.utils.BWUtils;
import com.pdi.reporting.report.vm.LEI_SCORING;
import com.pdi.string.StringUtils;
import com.pdi.xml.XMLUtils;

/*
 * LeiGraphicalReport - class to hold methods relevant to the LEI graphical report
 * 
 * NOTE:  EVEN THOUGH LOCATED UNDER THE DELEGATED FOLDER, THERE ARE NO METHODS IN THIS
 *        CLASS THAT ARE DEFINED IN THE DATA INTERFACE.
 *        
 * This class is located here because the folder structure (delegated/helpers) is
 * inverted from normal practice elsewhere in this project stack (helpers/delegated).
 * Putting this helper class here avoided confusion with having two folders in the
 * portal structure that had the name "helper".
 */
public class LeiGraphicalReport
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	
	//
	// Constructors.
	//
    public LeiGraphicalReport()
    {
    	// No logic here at the present time
    }

	//
	// Instance methods.
	//

//	/**
//	 * doGPIGraphicalRpt is a method that
//	 * handles data for the GPI graphical report
//	 * 
//	 * This is a new method, pulling together and porting
//	 * older code from previously existing groovy scripts.
//	 * Code from GPI_GRAPHICAL_REPORT.groovy and GPI.groovy
//	 * 
//	 * @param ret
//	 * @param epis
//	 * @param overrides
//	 * @param sessionUser
//	 * @param tg
//	 */
    @SuppressWarnings("unchecked")
	public ArrayList<Report>  getLEIGraphicalData(IndividualReport inp,  
			ArrayList<ProjectParticipantInstrument> epis )
	{
		//System.out.println("doLEIGraphicalRpt...");
    	
    	ArrayList<Report> rpts = new ArrayList<Report>();

    	try
		{
			for(ProjectParticipantInstrument epi : epis)
			{
				// spin through until you find the lei
				if(!epi.getInstrument().getCode().equals("lei"))
				{
					continue;
				}
				
   	   			IndividualReport ret = inp.clone();	// get the input data

				/*
				 * ========================================
				 * ADD BASIC PARTICIPANT DATA TO REPORT 
				 */
				ret.addDisplayData("FIRST_NAME", epi.getParticipant().getFirstName());
				ret.addDisplayData("LAST_NAME", epi.getParticipant().getLastName());
				ret.getDisplayData().put("PARTICIPANT_NAME", epi.getParticipant().getFirstName() + " " + epi.getParticipant().getLastName());			
				ret.addDisplayData("ORGANIZATION", XMLUtils.xmlEscapeString(epi.getProject().getClient().getName()));
		  		ret.addDisplayData("PROJECT_NAME", XMLUtils.xmlEscapeString(epi.getProject().getName()));	

		  		ReportData rd = new ReportData();
		  		rd.getDisplayData().put("ADMIN_DATE", epi.getInstrumentTakenDate().substring(0, 10));
		  		rd.getScoreData().put("LEI_SP_NAME", epi.getScoreGroup().getSpecialPopulation().getName()); // SPEC-POP NORM					
		  		rd.getScoreData().put("LEI_GP_NAME", epi.getScoreGroup().getGeneralPopulation().getName() ); //GEN-POP NORM

		  		String fileName = StringUtils.removeFilenameSpecialCharacters(
						epis.get(0).getParticipant().getLastName() + " " +
						epis.get(0).getParticipant().getFirstName() + " " +
						epis.get(0).getProject().getProjectTypeName()+" "+
						inp.getReportCode()+" "+
						new SimpleDateFormat("yyyyMMdd").format(new Date()) + " " 
						);
						
				ret.setName(fileName.replace(" ", "_") + ".pdf");

				ret.getDisplayData().put("CLIENT_LOGO", epi.getProject().getClient().getId()+".png");

				// from LEI.groovy : public IndividualReport generate()

				//LEI not complete
				if(epi.getScoreGroup().getScores().isEmpty())
				{
					//System.out.println("LEI IS NULL!");
					// They haven't finished the LEI
					ret.getScriptedData().put("STATUS", "NOT_FINISHED");
					ret.getScriptedData().put("ERROR", "LEI has not been completed, unable to generate report");
					ret.getReportData().put(ReportingConstants.RC_LEI, rd);	
					rpts.add(ret);
					continue;
				} else {
					ret.getScriptedData().put("STATUS", "");
					ret.getScriptedData().put("ERROR", "");
				}

				// Calc the reference line positions and put them out
				float[] ref = BWUtils.getRefPosArray();
				rd.getScriptedData().put("REF_1", new Float(ref[0]).toString());
				rd.getScriptedData().put("REF_3", new Float(ref[1]).toString());
				rd.getScriptedData().put("REF_16", new Float(ref[2]).toString());
				rd.getScriptedData().put("REF_50", new Float(ref[3]).toString());
				rd.getScriptedData().put("REF_84", new Float(ref[4]).toString());
				rd.getScriptedData().put("REF_97", new Float(ref[5]).toString());
				rd.getScriptedData().put("REF_99", new Float(ref[6]).toString());

				// get the normed score objects for the dimensions....
				HashMap<String, NormScore> gpPercentiles = epi.getScoreGroup().calculateGeneralPopulation();
				HashMap<String, NormScore> spPercentiles = epi.getScoreGroup().calculateSpecialPopulation();

				// Set up the dimension data
				rd.getDisplayData().put("DIM_COUNT", new Integer(ReportingConstants.DIMENSIONS.length).toString());	
				for (int i=0; i < ReportingConstants.DIMENSIONS.length; i++)
				{
					rd.getDisplayData().put("DIM" + (i+1) + "_NAME", (String) ReportingConstants.DIMENSIONS[i][0]);
					ArrayList ary = (ArrayList) ReportingConstants.DIMENSIONS[i][1];
					rd.getDisplayData().put("DIM" + (i+1) + "_SCALE_COUNT", new Integer(ary.size()).toString());	

					// Do the scale data
					for (int j=0; j < ary.size(); j++)
					{
						String scaleId = ( (ArrayList<String>)ReportingConstants.DIMENSIONS[i][1]).get(j);
						rd.getDisplayData().put("DIM" + (i+1) + "_SCALE" + (j+1) + "_ID", scaleId);
						rd.getDisplayData().put(scaleId  + "_SCALE_NAME", epi.getScoreGroup().getScores().get(scaleId).getScaleName());

						if(epi.getScoreGroup().getScores().get(scaleId).getRawScore() == null)
						{
							//invalid data
							ret.getScriptedData().put("STATUS", "NOT_FINISHED");
							ret.getScriptedData().put("ERROR", "Corrupted individual report");
						}

						// Round the percentiles
						double pctl = gpPercentiles.get(scaleId).toPercentile();
						int pOut = new Double(Math.floor(pctl + 0.5)).intValue();
						if (pOut < 1)
							pOut = 1;
						else if (pOut > 99.0)
							pOut = 99;
						rd.getScriptedData().put(scaleId+"_GP_PCTL", new Integer(pOut).toString());
						ret.addScriptedData(scaleId+"_GP_PCTL", new Integer(pOut).toString());
						pctl = spPercentiles.get(scaleId).toPercentile();
						pOut = new Double(Math.floor(pctl + 0.5)).intValue();
						if (pOut < 1.0)
							pOut = 1;
						else if (pOut > 99.0)
							pOut = 99;
						rd.getScriptedData().put(scaleId + "_SP_PCTL", new Integer(pOut).toString());
						ret.addScriptedData(scaleId+"_SP_PCTL", new Integer(pOut).toString());

						// Get the norms
						// but we don't need to do this because we have them already.
						// however.... 							
						// ... we have to
						// convert the com.pdi.data.Norm to a com.pdi.scoring.Norm 
						// because BWUtils can't see the pdi.data Norm object, and if you 
						// add the pdi-data project to pdi-util-reporting the whole system all of a sudden
						// sees two Norm objects, and blows the whole thing up.
						com.pdi.scoring.Norm gpNorm = new com.pdi.scoring.Norm(epi.getScoreGroup().getGeneralPopulation().getNorms().get(scaleId).getMean(),
																			   epi.getScoreGroup().getGeneralPopulation().getNorms().get(scaleId).getStandardDeviation());

						com.pdi.scoring.Norm spNorm = new com.pdi.scoring.Norm(epi.getScoreGroup().getSpecialPopulation().getNorms().get(scaleId).getMean(),
								   epi.getScoreGroup().getSpecialPopulation().getNorms().get(scaleId).getStandardDeviation());

						// Calc the score position
						double score =  epi.getScoreGroup().getScores().get(scaleId).scoreAsNumber();
						float scorPos = BWUtils.calcScorePos(score, gpNorm);

						rd.getScriptedData().put( scaleId + "_POSN", new Float(scorPos).toString());
						//println "        Position=" + scorPos;

						// Calc the cut-point positions
						float[] csPos = BWUtils.getCutPosArray(gpNorm, spNorm);
						//System.out.println("Cut Points for " + scaleId + ":");
						//for(int r= 0; r < csPos.length; r++)
						//{
						//	System.out.println("Idx[" + r + "]=" + csPos[r])
						//}
						rd.getScriptedData().put(scaleId + "_CP_3",  new Float(csPos[0]).toString());
						rd.getScriptedData().put(scaleId + "_CP_11", new Float(csPos[1]).toString());
						rd.getScriptedData().put(scaleId + "_CP_23", new Float(csPos[2]).toString());
						rd.getScriptedData().put(scaleId + "_CP_40", new Float(csPos[3]).toString());
						rd.getScriptedData().put(scaleId + "_CP_60", new Float(csPos[4]).toString());
						rd.getScriptedData().put(scaleId + "_CP_77", new Float(csPos[5]).toString());
						rd.getScriptedData().put(scaleId + "_CP_89", new Float(csPos[6]).toString());
						rd.getScriptedData().put(scaleId + "_CP_97", new Float(csPos[7]).toString());
					}	// End of scale loop (index = j)
				}	// End of Dimension loop (index = i)

				ret.getReportData().put(ReportingConstants.RC_LEI, rd);	
				LEI_SCORING.addRptDevSugList(ret);
				
				rpts.add(ret);
			} // end main for-loop (epis)
		
			return rpts;
		}
		catch(Exception e)
		{
			System.out.println("doLEIGraphicalRpt:");
			e.printStackTrace();
			return null;
		}
	}
}
