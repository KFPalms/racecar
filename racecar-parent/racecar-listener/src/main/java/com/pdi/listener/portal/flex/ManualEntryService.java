package com.pdi.listener.portal.flex;

/**
 * Copyright (c) 2008, 2011 Personnel Decisions International, Inc.
 * All rights reserved.
 *
 * $Header: /com/pdi/listener/portal/flex/manualEntryService     01/27/09 7:37a mpanichi $
 */

import java.util.*;

import com.pdi.data.adapt.helpers.AdaptScoreDataHelper;
import com.pdi.data.dto.AttachmentHolder;
import com.pdi.data.dto.NoteHolder;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.IAttachmentHelper;
import com.pdi.data.helpers.interfaces.INoteHelper;
import com.pdi.data.v2.helpers.ExternalResultsDataHelper;
import com.pdi.data.v2.helpers.ManualEntryDataHelper;
import com.pdi.data.v2.helpers.SaveConstructDataHelper;
import com.pdi.data.v2.dto.*;
import com.pdi.data.v2.dataObjects.KeyValuePairStrings;
import com.pdi.reporting.report.IndividualReport;

import flex.messaging.FlexContext;

/**
 * ManualEntryService provides entry point framework
 * for the ManualEntryVendor app 
 * Written initially for Target, but intended to 
 * be universal for being able to input/update data
 * from outside vendor instruments.
 *
 * @author		MB Panichi
 * @author      Ken Beukelman (original code)
 * @version	$Revision: 6 $  $Date: 8/25/05 3:23p $
 */
public class ManualEntryService
{
	//
	// Static data.
	//
	/** The source revision. */
	// THIS SHOULD BE CHANGED - It is a hold-over from VSS and would have been removed
	// if it wasn't referenced below.  It is no longer dynamic and is not a valid
	// reference for the version of the code, app, etc.
	public static final String REVISION = "$Revision: 5 $";
	
	private static String CONSENT_MOD_ID = "HMWIJBYN";
	
	
	//
	// Static methods.
	//  

	//
	// Instance data.
	//
	protected Properties properties = null;
	protected Properties globalProperties = null;
	
	
	
	private ManualEntryDTO theManualEntryDTO = new ManualEntryDTO();
	
	//
	// Constructors.
	//
	public ManualEntryService()
		throws Exception
	{
		////System.out.println("manual entry service.... ");
	    // Get application properties
	    //   As of Dec. 5, 2008 we need properties to make calls to
	    //   EmailRequestService in the Helper classes called from here.
	    readProperties(); 

	}

	
	
	//
	// Instance methods.
	//


	/**
	 * Read the properties (especially for e-mail parms)
	 *
	 * NOTE: 
	 * We're getting the properties with each new instance of this class.
	 * We may find for performance reasons we need to only get the properties once which
	 * may require a separate singleton class. That would also isolate the code to one
	 * location. Otherwise this code may need to be duplicated in other services.
	 *
	 * @throws Exception
	 */
	private void readProperties()
		throws Exception
	{
		//this.globalProperties = PropertyLoader.loadProperties("application");
		//this.properties = PropertyLoader.loadProperties("abdServer");
		
		//PropertyLoader.getProperty("com.pdi.data.v2.application", "webservice.url")
	}  	

	/**
	 * Get a DB connection for use by a call
	 *
	 * @return a Connection object
	 * @throws Exception
	 */
//	private Connection getDBConnection()
//		throws Exception
//	{
//		Connection con = null;
//
//		try
//		{
//    		// open a connection
//    		con = Utils.getDBConnection();
//    		if (con == null)
//    		{
//    			throw new Exception("No export connection; make sure pools are configured correctly.");
//    		}
//		}
//		catch (SQLException ex)
//		{
//            // handle any errors
//        	throw new Exception("Export connection attempt failed.  SQLException: " + ex.getMessage() + ", " +
//        						"SQLState: " + ex.getSQLState() + ", " +
//								"VendorError: " + ex.getErrorCode());
//        }
//        catch (Exception ex)
//		{
//	       	throw new Exception("Export connection setup failed:  " + ex.getMessage());
//		}
//
//		return con;
//	}

	
	/**
	 * Fetch the initial data from the LinkParams table, to 
	 * be used by the ManualEntryVendor app.
	 *
	 * @param linkParamId the V2 linkParam id
	 * @return the EntryStateDTO object
	 * @throws Exception
	 */

	public ManualEntryDTO fetchManualEntryData(String linkParamId)
		throws Exception
	{
		//System.out.println("MANUAL ENTRY SERVICE: fetchManualEntryData(String linkParamId)");

		try
		{
	
			// get the data from the linkParams table
			ManualEntryDataHelper helper = new ManualEntryDataHelper();        
			theManualEntryDTO =   helper.getLinkParamsData(linkParamId);
				
			// get the modules that are manual entry modules
			ArrayList<KeyValuePairStrings> temparyl = theManualEntryDTO.getModuleList();			
			temparyl = helper.getModulesForProjectId(theManualEntryDTO.getProjectId());			
			theManualEntryDTO.setModuleList(temparyl);			
			// get the constructs for the manual entry modules
			theManualEntryDTO.setModConstDTOList(helper.getConstructsforModules(theManualEntryDTO.getModuleList()));
		
			// check results data and
			// update the modConstDTOList with construct scores if there are any.
			theManualEntryDTO.setModConstDTOList(helper.getResultsForConstructs(theManualEntryDTO.getModConstDTOList(), theManualEntryDTO.getCandidateId(), theManualEntryDTO.getProjectId()));

			/*   TESTING */
//			Iterator <ModConstsDTO> it = theManualEntryDTO.getModConstDTOList().iterator();
//			while (it.hasNext()){
//				ModConstsDTO dto = new ModConstsDTO();
//				dto = it.next();
//				System.out.println("dto.getModuleId()  " + dto.getModuleId());
//				System.out.println("      MOD & GENPOP:  " + dto.getModuleId() + " : " + dto.getGenPopNormId());				
//			}
			
			
			// clean up after ourselves
			//helper.deleteLinkParamsRow(linkParamId);	
			
			//System.out.println(" theManualEntryDTO.getAdaptFirstName()   "  +  theManualEntryDTO.getAdaptFirstName());
			
			
			return theManualEntryDTO;
		}
		finally
		{

		}
	}

	
	/**
	 * Fetch the initial data from the LinkParams table, to 
	 * be used by the vendor integration scripts -- registerform.jsp.
	 *
	 * @param linkParamId the V2 linkParam id
	 * @return the EntryStateDTO object
	 * @throws Exception
	 */

	public ManualEntryDTO fetchLinkParamsDataForVendorService(String linkParamId)
		throws Exception
	{
		try
		{

			// get the data from the linkParams table
			ManualEntryDataHelper helper = new ManualEntryDataHelper();        
			theManualEntryDTO =   helper.getLinkParamsData(linkParamId);
			
			
			// clean up after ourselves
			//helper.deleteLinkParamsRow(linkParamId);		
			return theManualEntryDTO;
		}
		finally
		{

		}
	}
	
	/**
	 *  Save the construct data into the results table 
	 *  in V2.
	 *
	 * @param dtoIn - the updated ManualEntryDTO from the UI
	 * @throws Exception
	 */

	public String saveConstructScores(ManualEntryDTO dtoIn)
		throws Exception
	{
		theManualEntryDTO = dtoIn;
		
//		System.out.println("entering saveConstructScores......");
//		System.out.println("     dtoIn.getManualEntryType() " + dtoIn.getManualEntryType());
//		System.out.println("     dtoIn.getModuleId() " + dtoIn.getModuleId());
//		System.out.println("     dtoIn.getModConstDTOList().get(0).getModuleId() " + dtoIn.getModConstDTOList().get(0).getModuleId());
//		System.out.println("     dtoIn.getModConstDTOList().get(0).getHasScores() " + dtoIn.getModConstDTOList().get(0).getHasScores());
		
		try
		{
		
			SaveConstructDataHelper helper = new SaveConstructDataHelper();
			helper.setUpSaveData(dtoIn);
			
			return theManualEntryDTO.getModConstDTOList().get(0).getModuleId();
		}
		catch (Exception ex)  
		{    
			ex.printStackTrace();
			/* Swallow the exception */
			return "";
		}

	}

	/**
	 * saveAnswerDataForVendorIntegration is the controlling method to write
	 * answer data back to the V2 database.  It calls
	 * the other methods to do the actual work.
	 * This version of this method was written for the SHL vendor integration.
	 * It writes only answer data, and only sets the completed flag, based
	 * on the returned status from the completeform.jsp
	 * 
	 * @param Connection con, the database connection
	 * @param ManualEntryDTO dtoIn,  the data being passed 
	 * 			from the Submit button in the Manual Entry App
	 * @return 
	 * @throws Exception
	 */
	public  void saveAnswerDataForVendorIntegration(ManualEntryDTO dtoIn, boolean success)
		throws Exception
	{        

//		System.out.println("saveAnswerDataForVendorIntegration:\nsuccess = " + success +
//						"\nprojectId = " + dtoIn.getProjectId() 
//						+ ", moduleId = " + dtoIn.getModuleId()
//						+ ", candidateId = " + dtoIn.getCandidateId());
	
		
		//Connection con = null;
		theManualEntryDTO = dtoIn;

		try
		{
			
			SaveConstructDataHelper helper = new SaveConstructDataHelper();
			helper.writeAnswerDataToDatabaseForVendorIntegration(dtoIn, success);
			
		}catch (Exception ex)  {  /* Swallow the exception */  }

		
	}
	
	
	/**
	 * saveResultsDataForVendorIntegration is the controlling method to write
	 * results data back to the V2 database.  It calls
	 * the other methods to do the actual work.
	 * This version of this method was written for the SHL vendor integration.
	 * It writes only results data, and is called from the SHLListener.java
	 * 
	 * @param Connection con, the database connection
	 * @param ManualEntryDTO dtoIn,  the data being passed 
	 * 			from the Submit button in the Manual Entry App
	 * @return 
	 * @throws Exception
	 */
	public  void saveResultsDataForVendorIntegration(String linkParamId, HashMap<String, String> scoreData)
		throws Exception
	{        

//		System.out.println("saveResultsDataForVendorIntegration:" +
//						"\nlinkParamId = " + linkParamId);
		
		
		//Connection con = null;

		try
		{
			// get the data from the linkParams table first... 
			ManualEntryDataHelper LPhelper = new ManualEntryDataHelper();        
			theManualEntryDTO =   LPhelper.getLinkParamsData(linkParamId);
			
			// all we have at this point is what's in the linkParam xml - 
			// we don't HAVE the module list set up in the ManualEntryDTO.
			
			// so, let's build our own, with the single module that we need.
			ArrayList<KeyValuePairStrings> temparyl = new ArrayList<KeyValuePairStrings>();
			KeyValuePairStrings kvps = new KeyValuePairStrings();
			kvps.setTheKey(theManualEntryDTO.getModuleId());
			// doesn't matter what the name is -- in this instance, 
			// we don't use it, but I need to have something there,
			// so I'm just going to use the moduleId.
			// This way, it also makes it so we don't care which
			// module it is... 
			kvps.setTheValue(theManualEntryDTO.getModuleId());
			temparyl.add(kvps);
			theManualEntryDTO.setModuleList(temparyl);
				
			// get the constructs for the manual entry/vendor integration module
			theManualEntryDTO.setModConstDTOList(LPhelper.getConstructsforModules(theManualEntryDTO.getModuleList()));
			
			// at this point, we have the empty data structure, but we
			// don't have the results data from the vendor added in.
					
			// check results data and
			// update the modConstDTOList with construct scores if there are any.
			theManualEntryDTO.setModConstDTOList(LPhelper.getResultsForConstructsForVendorIntegration
					(theManualEntryDTO.getModConstDTOList(), theManualEntryDTO.getCandidateId(), 
							theManualEntryDTO.getProjectId(), scoreData));

			// Now we have everything we need.  Create the helper, and
			// use the data from the results XML to save.....
			
			// create the save data helper
			SaveConstructDataHelper CDhelper = new SaveConstructDataHelper();
			
			// build the results XML - we do an XML for each construct for
			// the module
			//String resultsXML = CDhelper.buildResultsXMLForSHLIntegration(scoreData, theManualEntryDTO);
			CDhelper.buildResultsXMLForSHLIntegration(scoreData, theManualEntryDTO);
			
			// we have the XML, now write the data to the database... 
			CDhelper.writeResultsDataToDatabase(theManualEntryDTO);
						
		}
		catch (Exception ex)  {  ex.printStackTrace();  }
		
	}	
		

	/**
	 *  Check to see if the adapt data for the entered engagement and
	 *  user is present.  If not, the function returns "false".  If
	 *  so, it sets the ADAPT user name in the dto and returns true.
	 *  
	 *  NOTE: This method relies upon the notion that the DTO is passed by
	 * 		  reference and the data placed into the dto will be preserved
	 * 		  when the method returns.
	 *
	 * @param dtoIn - the updated ManualEntryDTO from the UI
	 * @return An updated ManualEntryDTO
	 * @throws Exception
	 */

	public ManualEntryDTO checkAdaptIdData(ManualEntryDTO dto)
		throws Exception
	{
		// Hit ADAPT for the data
		IndividualReport ir = AdaptScoreDataHelper.pullAdaptData(
								dto.getAdaptProjectId(), dto.getAdaptUserId(), true);
		if (ir == null)
		{
			return null;
		}

		// We have data... put it in the DTO
		//System.out.println("checkAdaptIdData  " + ir.getDisplayData("ADAPT_FIRST_NAME") 
		//							+ "  " + ir.getDisplayData("ADAPT_LAST_NAME"));
		dto.setAdaptFirstName(ir.getDisplayData("ADAPT_FIRST_NAME"));
		dto.setAdaptLastName(ir.getDisplayData("ADAPT_LAST_NAME"));

		// save it to the db... 
		saveAdaptV2CandidateDataOnly(dto);
		
		return dto;
	}	
	

	/**
	 *  Save the adapt/v2 user mapping data into the v2 database. 
	 *  
	 *  This code assumes that the validity checking of the person/engagement
	 *  combination was done prior to the call.  If the combo is not valid,
	 *  the data should not be saved.
	 *
	 * @param dtoIn - the updated ManualEntryDTO from the UI
	 * @throws Exception
	 */

	public String saveAdaptV2Data(ManualEntryDTO dtoIn)
		throws Exception
	{
		//System.out.println("entering saveAdaptV2UserData......");

		// Go get the score data (just for the name info
		
		IndividualReport ir = null; 
			//AdaptScoreDataHelper.fetchAdaptData(dtoIn.getAdaptProjectId(), dtoIn.getAdaptUserId(), false);
		if (ir == null)
		{
			dtoIn.setAdaptFirstName("Not");
			dtoIn.setAdaptLastName("Available");
		} else {
			dtoIn.setAdaptFirstName(ir.getDisplayData("ADAPT_FIRST_NAME"));
			dtoIn.setAdaptLastName(ir.getDisplayData("ADAPT_LAST_NAME"));
		}

		
		try
		{
			SaveConstructDataHelper helper = new SaveConstructDataHelper();
			//helper.writeAdaptV2UserData(theManualEntryDTO.getCandidateId(), theManualEntryDTO.getProjectId(), 
			//		theManualEntryDTO.getAdaptUserId(), theManualEntryDTO.getAdaptProjectId());
			helper.writeAdaptV2UserData(theManualEntryDTO);
			//helper.writeConsentAnswerRow("HMWIJBYN", theManualEntryDTO.getCandidateId(), theManualEntryDTO.getProjectId());
			helper.writeConsentAnswerRow(CONSENT_MOD_ID, dtoIn.getCandidateId(), dtoIn.getProjectId());
			return theManualEntryDTO.getModConstDTOList().get(0).getModuleId();
		}
		catch (Exception ex)  
		{    
			ex.printStackTrace();
			/* Swallow the exception */
			return "";
		}

	}

	/**
	 *  Save the adapt/v2 user mapping data into the v2 database. 
	 *
	 * @param dtoIn - the updated ManualEntryDTO from checkAdaptIdData()
	 * @throws Exception
	 */

	public void saveAdaptV2CandidateDataOnly(ManualEntryDTO dtoIn)
		throws Exception
	{
		//System.out.println("entering saveAdaptV2CandidateDataOnly......");
		
		try
		{
			SaveConstructDataHelper helper = new SaveConstructDataHelper();
			helper.writeConsentAnswerRow(CONSENT_MOD_ID, dtoIn.getCandidateId(), dtoIn.getProjectId());
			helper.writeAdaptV2UserData(dtoIn);
			
		}
		catch (Exception ex)  
		{    
			/* Swallow the exception */
			return;
		}

	}

	/**
	 *  Save the adapt score data into the v2 database.
	 *  
	 *  Because the fetch returns a null if the person/engagement
	 *  combination is not valid, this method assumes that the
	 *  validity checking was done previous to the call (there
	 *  is no check for null).
	 *
	 * @param dtoIn - the updated ManualEntryDTO from the UI
	 * @throws Exception
	 */

	public ArrayList<String> fetchAdaptData(ManualEntryDTO dtoIn)
		throws Exception
	{
		//System.out.println("entering fetchAdaptData......");
		
		if (dtoIn.getAdaptDataModuleList() == null || dtoIn.getAdaptDataModuleList().size() < 1)
		{
			// Nothing to do... Return an empty list
			return new ArrayList<String>();
		}

		try
		{
			// first, have to fetch the adapt data
			IndividualReport ir = AdaptScoreDataHelper.pullAdaptData(dtoIn.getAdaptProjectId(),
																	 dtoIn.getAdaptUserId(),
																	 false);
			//if (ir == null)
			//{
			//	System.out.println("No Adapt data for userId " + theManualEntryDTO.getAdaptUserId() +
			//							" in engagement " + theManualEntryDTO.getAdaptProjectId());
			//	return;
			//}
			
			// second, have to save the data to V2
			ArrayList<String> savedMods =
				new ExternalResultsDataHelper().saveAdaptData (dtoIn.getProjectId(),
															   dtoIn.getCandidateId(),
															   dtoIn.getAdaptDataModuleList(),
															   ir);
			
			return savedMods;
		}
		catch (Exception ex)  
		{    
			ex.printStackTrace();
			return new ArrayList<String>();
		}

	}
	
	/**
	 *	Fetch the Notes for a participant
	 *
	 * @param  String participantId
	 * @return NoteHolder holder
	 * @throws Exception
	 */

	public NoteHolder fetchParticipantNotes(NoteHolder noteHolder)
		throws Exception
	{
		//System.out.println("entering fetchParticipantNotes......");
		NoteHolder holder = new NoteHolder();

		try
		{
			SessionUser session = (SessionUser)FlexContext.getFlexSession().getAttribute("sessionUser");
			INoteHelper iNote = HelperDelegate.getNoteHelper();
			holder = iNote.all(session, noteHolder.getParticipantId());
			
			return holder;
		}
		catch (Exception ex)  
		{    
			ex.printStackTrace();
			return holder;
		}

	}	

	
	/**
	 *	Save the Notes for a participant
	 *
	 * @param  NoteHolder noteHolder
	 * @throws Exception
	 */

	public void saveParticipantNotes(NoteHolder noteHolder)
		throws Exception
	{
		System.out.println("entering saveParticipantNotes......");

		try
		{
			//SessionUser session = (SessionUser)FlexContext.getFlexSession().getAttribute("sessionUser");
			INoteHelper iNote = HelperDelegate.getNoteHelper();
			iNote.addNotes(noteHolder);
			
		}
		catch (Exception ex)  
		{    
			ex.printStackTrace();
		}

	}	
	
	
	/**
	 *	Fetch the Attachments for a participant
	 *
	 * @param  String participantId
	 * @return AttachmentHolder holder
	 * @throws Exception
	 */

	public AttachmentHolder fetchParticipantAttachments(AttachmentHolder attachmentHolder)
		throws Exception
	{
		//System.out.println("entering fetchParticipantAttachments......");
		AttachmentHolder holder = new AttachmentHolder();

		try
		{
			SessionUser session = (SessionUser)FlexContext.getFlexSession().getAttribute("sessionUser");
			IAttachmentHelper iAttachment = HelperDelegate.getAttachmentHelper("com.pdi.data.v2.helpers.delegated");
			holder = iAttachment.fromParticipantId(session, attachmentHolder.getParticipant().getId());
			
			return holder;
		}
		catch (Exception ex)  
		{    
			ex.printStackTrace();
			return holder;
		}

	}		
	
	/**
	 *	Save the Attachments for a participant
	 *
	 * @param  AttachmentHolder attachmentHolder
	 * @throws Exception
	 */

	public void saveParticipantAttachments(AttachmentHolder attachmentHolder)
		throws Exception
	{
		throw new Exception("Save Attachments is no longer operative.  Functionality moved to Admin tools.");
//		//System.out.println("entering saveParticipantAttachments......");
//
//		try
//		{
//			//SessionUser session = (SessionUser)FlexContext.getFlexSession().getAttribute("sessionUser");
//			IAttachmentHelper iAttachment = HelperDelegate.getAttachmentHelper();
//			iAttachment.modify(attachmentHolder);
//			
//		}
//		catch (Exception ex)  
//		{    
//			ex.printStackTrace();
//		}

	}	

	/**
	 *	Delete Attachments for a participant
	 *
	 * @param  AttachmentHolder attachmentHolder
	 * @throws Exception
	 */

	public void deleteParticipantAttachments(AttachmentHolder attachmentHolder)
		throws Exception
	{
		throw new Exception("Delete Attachments is no longer operative.  Functionality moved to Admin tools.");
//		//System.out.println("entering deleteParticipantAttachments......");
//
//		try
//		{
//			//SessionUser session = (SessionUser)FlexContext.getFlexSession().getAttribute("sessionUser");
//			IAttachmentHelper iAttachment = HelperDelegate.getAttachmentHelper();
//			iAttachment.delete(attachmentHolder.getAttachments());
//			
//		}
//		catch (Exception ex)  
//		{    
//			ex.printStackTrace();
//		}

	}
	
	
	
	/*
	 * getVersion - This should probably be made into a more complete com check (call DB to see if we can get over there)
	 */
	public String getVersion() {
		//System.out.println("Debug stuff");
		return "Revision 1: " + com.pdi.listener.portal.flex.ManualEntryService.REVISION;
	} 
	
}

