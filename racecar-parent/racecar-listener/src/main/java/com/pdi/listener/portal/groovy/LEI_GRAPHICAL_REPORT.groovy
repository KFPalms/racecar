/*
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.listener.portal.groovy 

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.GroupReport;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;
import com.pdi.reporting.request.ReportingRequest;
import com.pdi.reporting.response.ReportingResponse;
import com.pdi.data.abyd.dto.reportInput.ReportInputDTO;
import com.pdi.data.abyd.dto.setup.EntryStateDTO;
import com.pdi.data.abyd.dto.setup.ReportModelStructureDTO;
import com.pdi.data.abyd.dto.setup.SPFullDataDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportLabelsDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportOptionsDTO;
import com.pdi.data.abyd.helpers.common.ImportExportDataHelper;
import com.pdi.data.abyd.helpers.reportInput.ReportInputDataHelper;
import com.pdi.data.v2.dto.ParticipantDTO;
import com.pdi.data.v2.dto.ProjectDTO;
import com.pdi.data.v2.dto.SessionUserDTO;
import com.pdi.data.v2.helpers.ParticipantDataHelper;
import com.pdi.data.v2.helpers.ProjectDataHelper;
import com.pdi.listener.portal.dao.DataAccessLayerV2;
import com.pdi.listener.portal.dao.DataAccessLayerV2.ParticipantInformation;
import com.pdi.listener.portal.dao.DataAccessLayerV2.ProjectInformation;
import com.pdi.properties.PropertyLoader;
import com.pdi.reporting.report.vm.LEI_SCORING;
import com.pdi.scoring.Norm;
import com.pdi.scoring.NormSet;
import com.pdi.string.StringUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.xml.XMLUtils;


 /**
 * @author gmyers
 */
public class LEI_GRAPHICAL_REPORT
{
	private ReportingRequest reportRequest;
	private Node reportNode;
	private SessionUserDTO sessionUser;
	private DataAccessLayerV2 dataAccessLayer;

	/*
	 * generate
	 */
	public void generate()
	{
		String reportCode = reportNode.getAttributes().getNamedItem("code").getTextContent();
		//System.out.println(reportCode);
		NodeList participants = XMLUtils.getElements(reportNode, "Participants/Participant");
		

		
		for( int j= 0; j < participants.getLength(); j++)
		{
			Node participant = participants.item(j);
			String participantId = participant.getAttributes().getNamedItem("participantId").getTextContent();
			String projectId = participant.getAttributes().getNamedItem("projectId").getTextContent();
			if(participantId == null || participantId.equalsIgnoreCase("")) {
				participantId = "";
			}
	
				//Got all of the basic data, time to add abyd data
				ProjectInformation projectInformation = dataAccessLayer.projectInformationHash.get(projectId);
				ParticipantInformation participantInformation = null;
				for(String code : dataAccessLayer.participantInformationList.keySet()) {
					ParticipantInformation pi =  dataAccessLayer.participantInformationList.get(code);
					if(pi.getParticipantId().equalsIgnoreCase(participantId) && pi.getProjectId().equalsIgnoreCase(projectId)) {
						participantInformation = pi;
						break;
					}
				}
				
				Setup2ReportOptionsDTO options = projectInformation.getReportOptions();
				Setup2ReportLabelsDTO labels = projectInformation.getReportLabels();
				SPFullDataDTO spFullData = projectInformation.getSpFullData();
				EntryStateDTO entryState = projectInformation.getEntryState();
				ReportModelStructureDTO reportModelStructure = projectInformation.getReportModelStructure();
				ProjectDTO projectDto = projectInformation.getProject();
				
				ReportInputDTO reportInput;
				ParticipantDTO participantDto;
				IndividualReport ir = new IndividualReport();
				if(participantInformation != null) {
					reportModelStructure = participantInformation.getReportModelStructure();
					reportInput = participantInformation.getReportInput();
					participantDto = participantInformation.getParticipant();
					ir = participantInformation.getIndividualReport();
				}	
			/*
			 *Step 0: Parse the xml 
			 */
				
			if(ir == null || ir.getReportData() == null)
			{
				continue;
			}
			ir.getDisplayData().put("CLIENT_LOGO", projectInformation.getProject().getClient().getId()+".png");
			ir.setReportCode(reportCode);
			ir.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);
			
			//	Add the dev sug list
			ir = LEI_SCORING.addRptDevSugList(ir);
				
			if(projectInformation.getDnaId() > -1)
			{
				String modId = ir.getDisplayData("MODULE_ID"); // need this to get the norm names
				
				String name = participantDto.getFirstName() + " " + participantDto.getLastName();
				ir.getDisplayData().put("PARTICIPANT_NAME", name  );
				
				ir.getDisplayData().put("ADMIN_DATE", ir.getReportData().get(ReportingConstants.RC_LEI).getDisplayData("ADMIN_DATE"));  // date instrument is completed
				ir.getDisplayData().put("ORGANIZATION", reportInput.getClientName());
				ir.getDisplayData().put("PROJECT_NAME", reportInput.getDnaName());	// Use the DNA name as the project name on A by D data
				////println "reportInput: proj=" + reportInput.getProjectName() + ", dna=" + reportInput.getDnaName();
				////println "PROJECT_NAME=" + ir.getDisplayData().get("PROJECT_NAME");
								
				java.util.Date date = new java.util.Date();
				SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
				ir.getDisplayData().put("TODAY_DATE", sdf.format(date));
				
				ir.getDisplayData().put("LEI_SP_NAME", ir.getReportData().get(ReportingConstants.RC_LEI).getScoreData(modId + "_SP_NAME")); // SPEC-POP NORM					
				ir.getDisplayData().put("LEI_GP_NAME", ir.getReportData().get(ReportingConstants.RC_LEI).getScoreData(modId + "_GP_NAME") );
				
				
				String fileName = StringUtils.removeSpecialCharacters(
						projectDto.getClient().getName() + " " +
						participantDto.getLastName() + " " +
						participantDto.getFirstName() + " " +
						new SimpleDateFormat("ddMMMyyyy").format(new java.util.Date()) + " ");
				ir.setName(fileName + reportCode + ".pdf");
				
			} else {
				//this is a tlt project
				
				java.util.Date date = new java.util.Date();
				SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
				ir.getDisplayData().put("TODAY_DATE", sdf.format(date));
				
				String fileName = StringUtils.removeSpecialCharacters(
				projectDto.getClient().getName() + " " +
						participantDto.getLastName() + " " +
						participantDto.getFirstName() + " " +
						new SimpleDateFormat("ddMMMyyyy").format(new java.util.Date()) + " ");
				ir.setName(fileName + reportCode + ".pdf");

			}
			reportRequest.addReport(ir);
		}

	}
}
