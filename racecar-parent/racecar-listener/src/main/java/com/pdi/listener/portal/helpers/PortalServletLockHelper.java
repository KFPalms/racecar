package com.pdi.listener.portal.helpers;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.pdi.data.abyd.dto.reportInput.ReportInputDTO;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.xml.XMLUtils;

public class PortalServletLockHelper {
	
	//
	// Static variables
	//
	private static String ERR_KEY = "error";
	private static String INFO_KEY = "info";
	
	private static String LOCKED = "Locked";
	private static String UNLOCKED = "Unlocked";
	private static int IG_SUBMITTED = 1;
	private static int IG_NOT_SUBMITTED = 0;
	
	private static String INT_GRID = "IG";
	private static String RATINGS = "RR";
	private static String ORG_TEXT = "OT";
	private static String PART_TEXT = "PT";
	
	private static ArrayList<String> keys = new ArrayList<String>();
	{
		keys.add(INT_GRID);
		keys.add(RATINGS);
		keys.add(ORG_TEXT);
		keys.add(PART_TEXT);
	}
	
	private static SimpleDateFormat SDF = new SimpleDateFormat("MM/dd/yyyy");
	
	//
	// Instance variables
	//
	
	//
	// Constructors
	//
	public PortalServletLockHelper()
	{
		// Placeholder for now
	}
	
	//
	// Instance methods
	//
	
	/*
	 * Fetch the status information
	 * Currently there is only one request which returns both IG and DRI status
	 */
	/**
	 * fetchIgDriLockInfo - Fetch the status information.  Web service that returns the
	 *                      status of the various DRI related lockable items (Ratings,
	 *                      Org text, ppt text) and the Int Grid.
	 * @param doc
	 * @param response
	 */
	public void fetchIgDriLockInfo(Document doc, HttpServletResponse response)
	{
		String participantId = "";
		String projectId = "";
		String ret = "<locks><participants>";

		
		NodeList nodes = XMLUtils.getElements(doc, "//FetchIgDriLocks");
		Node node = nodes.item(0);	// Get the first and only request element
		// look for the ppts
		NodeList pptList = XMLUtils.getElements(node, "Participants/Participant");
		for (int idx = 0; idx < pptList.getLength(); idx++)
		{
			Map<String, LockData> lockMap = new LinkedHashMap<String, LockData>();
			Node pptNode = pptList.item(idx);
			Node attrib = pptNode.getAttributes().getNamedItem("participantId");
			participantId = (attrib == null) ? null : attrib.getTextContent();
			if (participantId == null || participantId.trim().isEmpty())
			{
				participantId = "INVALID";
				LockData dat = new LockData();
				dat.setMsg("Invalid participant ID passed");
				lockMap.put(ERR_KEY, dat);
			}
			
			attrib = pptNode.getAttributes().getNamedItem("projectId");
			projectId = (attrib == null) ? null : attrib.getTextContent();
			if (projectId == null || projectId.trim().isEmpty())
			{
				projectId = "INVALID";
				// Don't double up on errors
				if (lockMap.size() == 0)
				{
					LockData dat = new LockData();
					dat.setMsg("Invalid project ID passed");
					lockMap.put(ERR_KEY, dat);
				}
			}
			
			// create the participant tag
			ret += "<participant pptId=\"" + participantId + "\" projId=\"" + projectId + "\">";

			if (lockMap.size() == 0)
			{
				// Only do this if there are no errors in input
				lockMap = fetchLockData(participantId, projectId);
			}
	
			// Put it into the response
			ret += addLockInfoXML(lockMap);
			// Close the participant tag
			ret += "</participant>";
		}

		ret += "</participants></locks>";
		fillResp(ret,  response);

		return;
	}

	
	/**
	 * fetchLockData - Gets raw info on what DRI related data is locked (internal only)
	 * @param pptId
	 * @param projId
	 * @return
	 */
	private Map<String, LockData> fetchLockData(String pptId, String projId)
	{
		Map<String, LockData> lockMap = new LinkedHashMap<String, LockData>();

		// StringBuilder is now preferred, but legacy code wants a StringBuffer
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT ir.igSubmitted AS IG, ir.igLockDate, ir.igUnlockDate, ");
		sb.append("       ri.rateStatusId AS RR, ri.rateLockDate, ri.rateUnlockDate, ");
		sb.append("       ri.oTxtStatusId AS OT, ri.oTxtLockDate, ri.oTxtUnlockDate, ");
		sb.append("       ri.pTxtStatusId AS PT, ri.pTxtLockDate, ri.pTxtUnlockDate ");
		sb.append("  FROM pdi_abd_igrid_response ir ");
		sb.append("    LEFT JOIN pdi_abd_rpt_input ri ON ri.participantId = ir.participantId AND ri.dnaId = ir.dnaId ");
		sb.append("    WHERE ir.participantId = " + pptId + " ");
		sb.append("      AND ir.dnaId = (SELECT dnaId FROM pdi_abd_dna WHERE projectId = " + projId + ") ");

		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
		if (dlps.isInError())
		{
			LockData dat = new LockData();
			String errStr = "Unable to setup status fetch.";
			dat.setMsg(errStr);
			lockMap.put(ERR_KEY, dat);
			System.out.println("Error: " + errStr + "  PptId=" + pptId
					           + ", projId=" + projId + ".  Error message:  "
					           + dlps.toString());
			return lockMap;
		}

		DataResult dr = null;
		ResultSet rs = null;

		try
		{
			dr = V2DatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			if (rs == null || !rs.isBeforeFirst())
			{
				// No data is present... no lock data returned (just status of OK)
				LockData dat = new LockData();
				String errStr = "Integration Grid not started; no status available.";
				dat.setMsg(errStr);
				lockMap.put(INFO_KEY, dat);
				System.out.println("Info:  " + errStr + "  pptId=" + pptId + ", projId=" + projId);
				return lockMap;
			}

			// We have data of some sort...  should be a single row; get it
			rs.next();
			// loop through the list of keys
			for(String key:keys)
			{
				LockData dat = new LockData();
				int value = rs.getInt(key);
				
				String stat = "";
				if(key.equals(INT_GRID))
				{
					stat = (value == IG_SUBMITTED ? LOCKED : UNLOCKED);
				}
				else if(value == 0)
				{
					// no lock data for the key (this is not an INT_GRID item)
					stat = UNLOCKED;
				}
				else
				{
					stat = (value == ReportInputDTO.COMPLETED ? LOCKED : UNLOCKED);
				}
				dat.setStat(stat);
				
				Date lock = null;
				Date unlock = null;
				if(key.equals(INT_GRID))
				{
					lock = rs.getDate("igLockDate");
					unlock = rs.getDate("igUnlockDate");
				}
				else if (key.equals(RATINGS))
				{
					lock = rs.getDate("rateLockDate");
					unlock = rs.getDate("rateUnlockDate");
				}
				else if (key.equals(ORG_TEXT))
				{
					lock = rs.getDate("oTxtLockDate");
					unlock = rs.getDate("oTxtUnlockDate");
				}
				else if (key.equals(PART_TEXT))
				{
					lock = rs.getDate("pTxtLockDate");
					unlock = rs.getDate("pTxtUnlockDate");
				}
				String str = (lock == null) ? "NONE" : SDF.format(lock);
				dat.setLdate(str);
				str = (unlock == null) ? "NONE" : SDF.format(unlock);
				dat.setUldate(str);

				lockMap.put(key, dat);
			}
			
			return lockMap;
		}
		catch (java.sql.SQLException se)
		{
			LockData dat = new LockData();
			String errStr = "Unable to fetch status information.";
			dat.setMsg(errStr);
			lockMap.put(ERR_KEY, dat);
			System.out.println("Error: " + errStr + "  pptId=" + pptId + ", projId=" + projId + ".   Error message:  " + se.getMessage());
			return lockMap;
		} finally {
			if (dr != null) { dr.close(); dr = null; }
		}
	}


	/**
	 * addLockInfoXML -- Builds the return XML for the lock fetch.
	 *                   NOTE: Assumes a single participant request
	 * 
	 * @param locks
	 * @return
	 */
	private String addLockInfoXML(Map<String, LockData> locks) 
	{
		String ret = "";
		String state = "OK";	// default to successful fetch
		String msg = null;
		for (Map.Entry<String, LockData> entry : locks.entrySet())
		{
		    String key = entry.getKey();
		    LockData ld = entry.getValue();
		    if (key.equals(ERR_KEY))
		    {
		    	state = "ERROR";
		    	msg = ld.getMsg();
		    }
		    else if (key.equals(INFO_KEY))
		    {
		    	state = "INFO";
		    	msg = ld.getMsg();

		    }
		    else
		    {
		    	ret += "<" + key + " status=\"" + ld.getStat() + "\" lockDate=\"" + ld.getLdate() + "\" unlockDate=\"" + ld.getUldate() + "\" />";
		    }
		}
		ret += "<state>" + state + "</state>";
		if (msg != null)
		{
			ret += "<msg>" + msg + "</msg>";
		}
		
		return ret;
	}


	/**
	 * fillResp - Puts return info into the HttpServletResponse object
	 * 
	 * @param xml
	 * @param response
	 */
	private void fillResp(String xml, HttpServletResponse response)
	{
		response.setContentType("text/html");
		response.setContentType("text/xml;charset=UTF-8");
		PrintWriter out;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			System.out
					.println("Warning:  PortalServletStatusHelper.fetchAbydStatusInfo() unable to fetch response writer.  msg="
							+ e.getMessage());
			return;
		}
		out.write(xml);
		
		return;
	}


	/**
	 * unlockIgDri - "unlock" (set status to appropriate values) the various DRI/IG items.
	 * 				 Modified 1/21/16 per PA-1543 to allow the unlock to "ripple" (e.g.,
	 *               unlocking RR also unlocks the texts)
	 * 
	 * @param doc
	 * @param response
	 */
	public void unlockIgDri(Document doc, HttpServletResponse response)
	{
		// Unpack the request
		String pptId = "";
		String projId = "";
		
		// Assume for now that there is a single request per invocation...
		// We can refactor later if we need to
		NodeList nodes = XMLUtils.getElements(doc, "//IgDriUnlock");
		Node node = nodes.item(0);	// Get the first (and, for now, only) request
									// element
		NodeList ppts = XMLUtils.getElements(node, "Participant");
		Node ppt = ppts.item(0); // Again, the first and only node
		pptId = ppt.getAttributes().getNamedItem("participantId").getTextContent();
		projId = ppt.getAttributes().getNamedItem("projectId").getTextContent();

		// unlock is a child of Participant.  Could have done it as an attribute, but that doesn't really fit with ppt.
		NodeList nodes2 = XMLUtils.getElements(doc, "//unlock");
		Node unlock  = nodes2.item(0);
		String unlockKey = unlock.getTextContent();
		if(! keys.contains(unlockKey))
		{
			String xml = buildUnlockXml(pptId, projId, "ERROR", "'" + unlockKey + "' is not a valid, unlockable data type.");
			fillResp(xml, response);
			return;
		}
		
		// Get the current state
		Map<String, LockData> locks = fetchLockData(pptId, projId);

		// See what is locked
		boolean otLocked = (locks.get(ORG_TEXT)  != null && locks.get(ORG_TEXT).getStat()  != null && locks.get(ORG_TEXT).getStat().equals(LOCKED))  ? true : false;
		boolean ptLocked = (locks.get(PART_TEXT) != null && locks.get(PART_TEXT).getStat() != null && locks.get(PART_TEXT).getStat().equals(LOCKED)) ? true : false;
		boolean rrLocked = (locks.get(RATINGS)   != null && locks.get(RATINGS).getStat()   != null && locks.get(RATINGS).getStat().equals(LOCKED))   ? true : false;
		boolean igLocked = (locks.get(INT_GRID)  != null && locks.get(INT_GRID).getStat()  != null && locks.get(INT_GRID).getStat().equals(LOCKED))  ? true : false;

		String unlockRiStr = "";
		boolean doIg = false;
		if (unlockKey.equals(ORG_TEXT) && otLocked)
		{
			unlockRiStr += "oTxtStatusId = " + ReportInputDTO.READY_FOR_EDITING + ", oTxtUnlockDate = GETDATE(), ";
		}
		if (unlockKey.equals(PART_TEXT) && ptLocked)
		{
			unlockRiStr += "pTxtStatusId = " + ReportInputDTO.READY_FOR_EDITING + ", pTxtUnlockDate = GETDATE(), ";
		}
		if (unlockKey.equals(RATINGS) && rrLocked)
		{
			unlockRiStr += "rateStatusId = " + ReportInputDTO.IN_PROGRESS + ", rateUnlockDate = GETDATE(), ";
			unlockRiStr += "oTxtStatusId = " + ReportInputDTO.IN_PROGRESS + (otLocked ? ", oTxtUnlockDate = GETDATE(), " : ", ");
			unlockRiStr += "pTxtStatusId = " + ReportInputDTO.IN_PROGRESS + (ptLocked ? ", pTxtUnlockDate = GETDATE(), " : ", ");
		}
		if (unlockKey.equals(INT_GRID))
		{
			if (igLocked)
			{
				doIg = true;
			}
			unlockRiStr += "rateStatusId = " + ReportInputDTO.IN_PROGRESS + (rrLocked ? ", rateUnlockDate = GETDATE(), " : ", ");
			unlockRiStr += "oTxtStatusId = " + ReportInputDTO.IN_PROGRESS + (otLocked ? ", oTxtUnlockDate = GETDATE(), " : ", ");
			unlockRiStr += "pTxtStatusId = " + ReportInputDTO.IN_PROGRESS + (ptLocked ? ", pTxtUnlockDate = GETDATE()," : ", ");
		}

		 StringBuffer sb = new StringBuffer();
		 DataResult dr = null;
		 DataLayerPreparedStatement dlps = null;	 
		try
		{
			if (unlockRiStr.length() > 0)
			{
				// do the RI stuff
				sb.append("UPDATE pdi_abd_rpt_input ");
				sb.append("  SET " + unlockRiStr);
				sb.append(" lastUserId = SYSTEM_USER, lastDate = GETDATE()");
				sb.append("  WHERE participantId = " + pptId + " ");
				sb.append("    AND dnaId = (SELECT dnaId FROM pdi_abd_dna WHERE projectId = " + projId + ")");

				dlps = V2DatabaseUtils.prepareStatement(sb);
				if (dlps.isInError())
				{
					String errStr = "Unable to setup status unlock (RI).";
					System.out.println("Error: " + errStr + "  pptId=" + pptId + ", projId=" + projId + ".  Error msg="
									+ dlps.toString());
					String xml = buildUnlockXml(pptId, projId, "ERROR", errStr);
					fillResp(xml, response);
					return;
				}
				dr = V2DatabaseUtils.update(dlps);
				if (dlps.isInError())
				{
					String errStr = "Unlock update failed (RI).";
					System.out.println("Error: " + errStr + "  pptId=" + pptId + ", projId=" + projId + ".  Error msg="
									+ dlps.toString());
					String xml = buildUnlockXml(pptId, projId, "ERROR", errStr);
					fillResp(xml, response);
					return;
				}
				// At some point we may wish to reinstate as error, but change the condition to allow both 0 and 1 as valid values for the row count
				if (dr.getRowCount() != 1)
				{
					// Per PA-1991 I have removed all error handling except the message to the console log
					String errStr = "Unlock was inconsistent (RI).";
					System.out.println("Posible Error:  " + errStr + "  pptId=" + pptId + ", projId=" + projId + ", row count=" + dr.getRowCount());
					// String xml = buildUnlockXml(pptId, projId, "ERROR", errStr);
					// fillResp(xml, response);
					//return;
				}
			}	// Done with the RI stuff
			
			if (doIg)
			{
				sb.setLength(0);
				dr = null;
				dlps = null;

				sb.append("UPDATE pdi_abd_igrid_response ");
				sb.append("  SET igSubmitted  = " + IG_NOT_SUBMITTED + ", ");
				sb.append("      igUnlockDate = GETDATE(), ");
				sb.append("      lastUser     = SYSTEM_USER," );
				sb.append("      lastDate     = GETDATE()");
				sb.append("  WHERE participantId = " + pptId + " ");
				sb.append("    AND dnaId = (SELECT dnaId FROM pdi_abd_dna WHERE projectId = " + projId + ")");
				 
				dlps = V2DatabaseUtils.prepareStatement(sb);
				if (dlps.isInError())
				{
					String errStr = "Unable to setup status unlock (IG).";
					System.out.println("Error: " + errStr + "  pptId=" + pptId + ", projId=" + projId + ".  Error msg="
									+ dlps.toString());
					String xml = buildUnlockXml(pptId, projId, "ERROR", errStr);
					fillResp(xml, response);
					return;
				}

				dr = V2DatabaseUtils.update(dlps);
				if (dlps.isInError())
				{
					String errStr = "Unlock update failed (IG).";
					System.out.println("Error: " + errStr + "  pptId=" + pptId + ", projId=" + projId + ".  Error msg="
									+ dlps.toString());
					String xml = buildUnlockXml(pptId, projId, "ERROR", errStr);
					fillResp(xml, response);
					return;
				}
				if (dr.getRowCount() != 1)
				{
					String errStr = "Unlock was inconsistent (IG).";
					System.out.println("Error: " + errStr + "  pptId=" + pptId + ", projId=" + projId + ", row count=" + dr.getRowCount());
					String xml = buildUnlockXml(pptId, projId, "ERROR", errStr);
					fillResp(xml, response);
					return;
				}
			}

			// Assume success if here
			String xml = buildUnlockXml(pptId, projId, "OK", null);
			fillResp(xml, response);
			return;

		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}

	
	/**
	 * buildUnlockXml - Build the return XML for the unlock action (IgDriUnlock)
	 * 
	 * @param pptId
	 * @param projId
	 * @param state
	 * @param msg
	 * @return
	 */
	private String buildUnlockXml(String pptId, String projId, String state, String msg)
	{
		String ret = "<unlock>";
		ret += "<participantId>" + pptId + "</participantId>";
		ret += "<projectId>" + projId + "</projectId>";
		ret += "<state>" + state + "</state>";
		if (msg != null)
		{
			ret += "<msg>" + msg + "</msg>";
		}
		ret += "</unlock>";
		return ret;
	}


	/**
	 * unlocksFetch - Main driver for the 'FetchUnlocks' service
	 * @param doc - Input XML (in a Document object)
	 * @param response - The HttpServletResponse object
	 */
	public void unlocksFetch(Document doc, HttpServletResponse response)
	{
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		
		// Unpack the request
		int max;
		String start = "";
		String end = "";
		
		NodeList nodes = XMLUtils.getElements(doc, "//FetchUnlocks");
		//String debug = XMLUtils.nodeToString(nodes.item(0));

		String str="";
		Node node = nodes.item(0);	// Get the first (and only) fetch node
		
		// Look for the count parameter and validate it
		NodeList cList = XMLUtils.getElements(node, "count");
		Node cntNode = cList.item(0); // Again, the first and only node
		if(cntNode == null)
		{
			String xml = buildFetchUnlockXML("ERROR", "A 'count' tag is required in 'FetchUnlocks'.");
			fillResp(xml, response);
			return;
		}

		if (cntNode.getAttributes() == null  ||
			cntNode.getAttributes().getNamedItem("max") == null  ||
			cntNode.getAttributes().getNamedItem("max").getTextContent() == null )
		{
			String xml = buildFetchUnlockXML("ERROR", "A 'max' attribute is required in the 'count' tag in 'FetchUnlocks'.");
			fillResp(xml, response);
			return;
		}
		str = cntNode.getAttributes().getNamedItem("max").getTextContent();

		try
		{
			max = Integer.parseInt(str);
		}
		catch (NumberFormatException e)
		{
			String xml = buildFetchUnlockXML("ERROR", "The 'max' attribute value must be a valid integer; input is '" + str +"'.");
			fillResp(xml, response);
			return;
		}
		
		// look for the searchDates parameter and validate it
		NodeList dList = XMLUtils.getElements(node, "searchDates");
		Node dtNode = dList.item(0); // The first and only node
		if(dtNode == null)
		{
			String xml = buildFetchUnlockXML("ERROR", "A 'searchDates' tag is required in 'FetchUnlocks'.");
			fillResp(xml, response);
			return;
		}

		if (dtNode.getAttributes() == null  ||
			dtNode.getAttributes().getNamedItem("start") == null  ||
			dtNode.getAttributes().getNamedItem("start").getTextContent() == null)
		{
			start = null;
		}
		else
		{
			start = dtNode.getAttributes().getNamedItem("start").getTextContent();
		}

		if (dtNode.getAttributes() == null  ||
			dtNode.getAttributes().getNamedItem("end") == null  ||
			dtNode.getAttributes().getNamedItem("end").getTextContent() == null)
		{
			end = null;
		}
		else
		{
			end = dtNode.getAttributes().getNamedItem("end").getTextContent();
		}
		if (start == null && end == null)
		{
			String xml = buildFetchUnlockXML("ERROR", "The 'searchDates' tag must have a 'start' tag, an 'end' tag, or both.");
			fillResp(xml, response);
			return;
		}

		@SuppressWarnings("unused")
		Date date;
		fmt.setLenient(false);;
		if(start != null)
		{
			try
			{
				date = fmt.parse(start);
			}
			catch (ParseException e)
			{
				String xml = buildFetchUnlockXML("ERROR", "The 'start' attribute value must be a valid date in yyyy-mm-dd format; input is " + start + ".");
				fillResp(xml, response);
				return;
			}
		}
		if(end != null)
		{
			try
			{
				date = fmt.parse(end);
			}
			catch (ParseException e)
			{
				String xml = buildFetchUnlockXML("ERROR", "The 'end' attribute value must be a valid date in yyyy-mm-dd format; input is " + end + ".");
				fillResp(xml, response);
				return;
			}
		}
		fmt.setLenient(true);
		
		// get the rows and put out the xml
		UnlockData ud = fetchUnlockdata(max, start, end);
		String xml = buildFetchUnlockXML(ud.getState(), ud.getMsg(), ud);
		fillResp(xml, response);
		return;
	}


	/**
	 * buildFetchUnlockXML -  Convenience method used when the UnlockData object is not yet available (usually for error returns)
	 * @param state
	 * @param msg
	 * @return
	 */
	private String buildFetchUnlockXML(String state, String msg)
	{
		return buildFetchUnlockXML(state, msg, null);
	}


	/**
	 * buildFetchUnlockXML - Method to build the XML to be returned
	 * @param state
	 * @param msg
	 * @param uld - UnlockData object
	 * @return - XML string
	 */
	private String buildFetchUnlockXML(String state, String msg, UnlockData uld)
	{
		String ret = "<unlocks>";
		ret += "<state>" + state + "</state>";
		if (msg != null)
		{
			ret += "<msg>" + msg + "</msg>";
		}
		if (uld != null)
		{
			ret += "<count actual=\"" + uld.getCount() + "\" maxEceeded=\"" + uld.getOverMax() + "\"/>";
			ret += "<participants>";
			for(UnlockRow ulp: uld.getUnlocks())
			{
				ret += "<participant projId=\"" + ulp.getProjId() + "\" pptId=\"" + ulp.getPptId() +"\">";
				LockData ld = ulp.getIgLockData();
				ret += "<IG unlockDate=\"" + ld.getUldate() + "\" lockDate=\"" + ld.getLdate() + "\" status=\"" + ld.getStat() + "\"/>";
				ld = ulp.getRrLockData();
				ret += "<RR unlockDate=\"" + ld.getUldate() + "\" lockDate=\"" + ld.getLdate() + "\" status=\"" + ld.getStat() + "\"/>";
				ld = ulp.getOtLockData();
				ret += "<OT unlockDate=\"" + ld.getUldate() + "\" lockDate=\"" + ld.getLdate() + "\" status=\"" + ld.getStat() + "\"/>";
				ld = ulp.getPtLockData();
				ret += "<PT unlockDate=\"" + ld.getUldate() + "\" lockDate=\"" + ld.getLdate() + "\" status=\"" + ld.getStat() + "\"/>";
				ret += "</participant>";
			}
			ret += "</participants>";
		}
		ret += "</unlocks>";
		return ret;
	}


	/**
	 * fetchUnlockdata - Method that does the data access for the unlock service
	 * 
	 * @param max - Max rows to return
	 * @param start - Start date (inclusive)
	 * @param end - End date (inclusive)
	 * @return - UnlockData object
	 */
	private UnlockData fetchUnlockdata(int max, String start, String end)
	{
		
		UnlockData ret = new UnlockData();

		// StringBuilder is now preferred, but legacy code wants a StringBuffer
		StringBuffer sb = new StringBuffer();
		//sb.append("SELECT ir.participantId, dna.projectId, ");
		sb.append("SELECT TOP " + (max + 1) + " ");
		sb.append("       ir.participantId, dna.projectId, ");
		sb.append("       ir.igSubmitted, ir.igLockDate, ir.igUnlockDate, ");
		sb.append("       ri.rateStatusId, ri.rateLockDate, ri.rateUnlockDate, ");
		sb.append("       ri.oTxtStatusId, ri.otxtLockDate, ri.otxtUnlockDate, ");
		sb.append("       ri.pTxtStatusId, ri.ptxtLockDate, ri.ptxtUnlockDate ");
		sb.append("  FROM pdi_abd_igrid_response ir ");
		sb.append("	   LEFT JOIN pdi_abd_rpt_input ri ON ri.participantId = ir.participantId AND ri.dnaId = ir.dnaId ");
		sb.append("	   LEFT JOIN pdi_abd_dna dna ON dna.dnaId = ir.dnaId ");
		sb.append("	 WHERE ((");
		if (start != null)
		{
			sb.append("ir.igUnlockDate >= '" + start + "'");
		}
		if (end != null)
		{
			if(start != null)
			{
				sb.append(" AND ");
			}
			sb.append("ir.igUnlockDate <= '" + end + "'");
		}
		sb.append(")  OR  (");
		if (start != null)
		{
			sb.append("ri.rateUnlockDate >= '" + start + "'");
		}
		if (end != null)
		{
			if(start != null)
			{
				sb.append(" AND ");
			}
			sb.append("ri.rateUnlockDate <= '" + end + "'");
		}
		sb.append(")  OR  (");
		if (start != null)
		{
			sb.append("ri.otxtUnlockDate >= '" + start + "'");
		}
		if (end != null)
		{
			if(start != null)
			{
				sb.append(" AND ");
			}
			sb.append("ri.otxtUnlockDate <= '" + end + "'");
		}
		sb.append(")  OR  (");
		if (start != null)
		{
			sb.append("ri.ptxtUnlockDate >= '" + start + "'");
		}
		if (end != null)
		{
			if(start != null)
			{
				sb.append(" AND ");
			}
			sb.append("ri.ptxtUnlockDate <= '" + end + "'");
		}
		sb.append(") ) ");
		//sb.append("  LIMIT " + (max + 1));

		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
		if (dlps.isInError())
		{
			String errStr = "Unable to set up unlock fetch.";
			ret.setState("ERROR");
			ret.setMsg(errStr);
			System.out.println("Error: " + errStr + "  max=" + max
								+ ", start=" + start + ", end=" + end
								+ ".  Error message:  " + dlps.toString());
			return ret;
		}

		DataResult dr = null;
		ResultSet rs = null;

		try
		{
			dr = V2DatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			if (rs == null || !rs.isBeforeFirst())
			{
				// No data is present... 
				String errStr = "No data available in range.";
				ret.setState("INFO");
				ret.setMsg(errStr);
				System.out.println("Info:  " + errStr + "  max=" + max
						+ ", start=" + start + ", end=" + end
						+ ".");
				return ret;
			}

			// We have data of some sort...  process it
			int cnt = 0;
			while(rs.next())
			{
				if(cnt == max)
				{
					ret.setOverMax(true);
					break;
				}
				UnlockRow ur = new UnlockRow();
				Date dt = null;
				int flag = -1;

				ur.setPptId(rs.getLong("participantId"));
				ur.setProjId(rs.getLong("projectId"));
				
				LockData ig = new LockData();
				flag = rs.getInt("igSubmitted");
				ig.setStat(flag == IG_SUBMITTED ? LOCKED : UNLOCKED);
				dt = rs.getDate("igLockDate");
				ig.setLdate((dt == null) ? "NONE" : SDF.format(dt));
				dt = rs.getDate("igUnlockDate");
				ig.setUldate((dt == null) ? "NONE" : SDF.format(dt));
				ur.setIgLockData(ig);
				
				LockData rr = new LockData();
				flag = rs.getInt("rateStatusId");
				rr.setStat(flag == ReportInputDTO.COMPLETED ? LOCKED : UNLOCKED);
				dt = rs.getDate("rateLockDate");
				rr.setLdate((dt == null) ? "NONE" : SDF.format(dt));
				dt = rs.getDate("rateUnlockDate");
				rr.setUldate((dt == null) ? "NONE" : SDF.format(dt));
				ur.setRrLockData(rr);
				
				LockData ot = new LockData();
				flag = rs.getInt("oTxtStatusId");
				ot.setStat(flag == ReportInputDTO.COMPLETED ? LOCKED : UNLOCKED);
				dt = rs.getDate("oTxtLockDate");
				ot.setLdate((dt == null) ? "NONE" : SDF.format(dt));
				dt = rs.getDate("oTxtUnlockDate");
				ot.setUldate((dt == null) ? "NONE" : SDF.format(dt));
				ur.setOtLockData(ot);
				
				LockData pt = new LockData();
				flag = rs.getInt("pTxtStatusId");
				pt.setStat(flag == ReportInputDTO.COMPLETED ? LOCKED : UNLOCKED);
				dt = rs.getDate("pTxtLockDate");
				pt.setLdate((dt == null) ? "NONE" : SDF.format(dt));
				dt = rs.getDate("pTxtUnlockDate");
				pt.setUldate((dt == null) ? "NONE" : SDF.format(dt));
				ur.setPtLockData(pt);
				
				ret.getUnlocks().add(ur);
				cnt++;
			}
			ret.setCount(cnt);
			ret.setState("OK");

			return ret;
		}
		catch (java.sql.SQLException se)
		{
			String errStr = "Unable to fetch unlock information.";
			ret.setState("ERROR");
			ret.setMsg(errStr);
			System.out.println("Error: " + errStr + "  max=" + max + ", start=" + start + ", end=" + end + ".   Error message:  " + se.getMessage());
			return ret;
		}
		finally
		{
			if (dr != null) { dr.close(); dr = null; }
		}
	}


	/**
	 * lockData - Private class to handle lock data (also used in unlock data)
	 */
	private class LockData
	{
		private String stat;
		private String msg;		// Only populated when there is no date data
		private String ldate;
		private String uldate;
		
		public LockData()
		{
			// default constructor that does nothing
		}
		
		//****************************************
		protected String getStat()
		{
			return stat;
		}
		
		protected void setStat(String value)
		{
			this.stat = value;
		}
		
		//****************************************
		protected String getMsg()
		{
			return msg;
		}
		
		protected void setMsg(String value)
		{
			this.msg = value;
		}
		
		//****************************************
		protected String getLdate()
		{
			return ldate;
		}
		
		protected void setLdate(String value)
		{
			this.ldate = value;
		}
		
		//****************************************
		protected String getUldate()
		{
			return uldate;
		}
		
		protected void setUldate(String value)
		{
			this.uldate = value;
		}
	}


	/**
	 * UnlockData - Private class to contain all of the status and the data for unlock data
	 */
	private class UnlockData
	{
		private String state;
		private String msg = null;		// Only populated on error
		private int count = 0;
		private boolean overMax = false;
		private List<UnlockRow> unlocks = null;
		
		public UnlockData()
		{
			// default constructor that does nothing
		}
		
		//****************************************
		protected String getState()
		{
			return state;
		}
		
		protected void setState(String value)
		{
			this.state = value;
		}
		
		//****************************************
		protected String getMsg()
		{
			return msg;
		}
		
		protected void setMsg(String value)
		{
			this.msg = value;
		}
		
		//****************************************
		protected int getCount()
		{
			return count;
		}
		
		protected void setCount(int value)
		{
			this.count = value;
		}
		
		//****************************************
		protected boolean getOverMax()
		{
			return overMax;
		}
		
		protected void setOverMax(boolean value)
		{
			this.overMax = value;
		}
		
		//****************************************
		protected List<UnlockRow> getUnlocks()
		{
			if (this.unlocks == null)
			{
				this.unlocks = new ArrayList<UnlockRow>();
			}
			return unlocks;
		}
		
//		protected void setUnlocks(List value)
//		{
//			this.unlocks = value;
//		}
	}
	
	
	
	
	/**
	 * UnlockRow is a private class used to hold data about the unlock dates for a particular ppt and project 
	 */
	private class UnlockRow
	{
		private long pptId;
		private long projId;
		private LockData igLockData;
		private LockData rrLockData;
		private LockData otLockData;
		private LockData ptLockData;
		
		public UnlockRow()
		{
			// default constructor that does nothing
		}
		
		//****************************************
		protected long getPptId()
		{
			return pptId;
		}
		
		protected void setPptId(long value)
		{
			this.pptId = value;
		}
		
		//****************************************
		protected long getProjId()
		{
			return projId;
		}
		
		protected void setProjId(long value)
		{
			this.projId = value;
		}
		
		//****************************************
		protected LockData getIgLockData()
		{
			return igLockData;
		}
		
		protected void setIgLockData(LockData value)
		{
			this.igLockData = value;
		}
		
		//****************************************
		protected LockData getRrLockData()
		{
			return rrLockData;
		}
		
		protected void setRrLockData(LockData value)
		{
			this.rrLockData = value;
		}
		
		//****************************************
		protected LockData getPtLockData()
		{
			return ptLockData;
		}
		
		protected void setPtLockData(LockData value)
		{
			this.ptLockData = value;
		}
		
		//****************************************
		protected LockData getOtLockData()
		{
			return otLockData;
		}
		
		protected void setOtLockData(LockData value)
		{
			this.otLockData = value;
		}
	}

}
