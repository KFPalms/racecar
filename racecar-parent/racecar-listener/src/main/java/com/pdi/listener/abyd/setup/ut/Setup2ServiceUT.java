package com.pdi.listener.abyd.setup.ut;

import java.util.ArrayList;

//import com.pdi.data.abyd.dto.setup.ReportModelStructureDTO;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.data.util.language.LanguageItemDTO;
import com.pdi.listener.abyd.setup.Setup2Service;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

public class Setup2ServiceUT extends TestCase
{
	//
	// Instance variables
	//
	Setup2Service _svc;
	
	//
	// Constructor
	//
	public Setup2ServiceUT(String name)
	{
		super(name);

		System.out.println("Starting SetupService testing...");
		_svc = new Setup2Service();
	}
  
  

	public void testConnect()
  		throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);

		System.out.println("  Testing comCheck()...");
		System.out.println(_svc.comCheck2());

		UnitTestUtils.stop(this);
	}


	/*
	 * testFetchReportModel
	 */
//	public void testFetchReportModel()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//	
//		long dnaId = 41;
//
//		ReportModelStructureDTO dto = _svc.fetchReportModel(dnaId);
//
//		System.out.println(dto.toString());
//	
//		UnitTestUtils.stop(this);
//	}


//	/*
//	 * testReplaceReportModel
//	 */
//	public void testReplaceReportModel()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//	
//
//		// Make up some data
//		ReportModelStructureDTO struct = new ReportModelStructureDTO();
//		struct.setDnaId(8888);
//		ReportModelSuperFactorDTO sf1 = new ReportModelSuperFactorDTO();
//		sf1.setSuperFactorId(88881);
//		sf1.setDisplayName("Svc SF1");
//		struct.getSuperFactorList().add(sf1);
//		ReportModelCompetencyDTO cmp1_1 = new ReportModelCompetencyDTO();
//		//cmp1_1.setRmCompId(888811);
//		cmp1_1.setDisplayName("Comp 1 for Svc SF 1");
//		sf1.getCompList().add(cmp1_1);
//		ReportModelCompChildDTO cc1_1_1 =  new ReportModelCompChildDTO();
//		cc1_1_1.setDnaCompId(8888111);
//		cmp1_1.getChildren().add(cc1_1_1);
//		ReportModelCompChildDTO cc1_1_2 =  new ReportModelCompChildDTO();
//		cc1_1_2.setDnaCompId(8888112);
//		cmp1_1.getChildren().add(cc1_1_2);
//		ReportModelCompetencyDTO cmp1_2 = new ReportModelCompetencyDTO();
//		//cmp1_2.setRmCompId(888812);
//		cmp1_2.setDisplayName("Comp 2 for Svc SF 1");
//		sf1.getCompList().add(cmp1_2);
//		ReportModelCompChildDTO cc1_2_1 =  new ReportModelCompChildDTO();
//		cc1_2_1.setDnaCompId(8888121);
//		cmp1_2.getChildren().add(cc1_2_1);
//		ReportModelSuperFactorDTO sf2 = new ReportModelSuperFactorDTO();
//		sf2.setSuperFactorId(88882);
//		sf2.setDisplayName("Svc SF2");
//		struct.getSuperFactorList().add(sf2);
//		ReportModelCompetencyDTO cmp2_1 = new ReportModelCompetencyDTO();
//		//cmp2_1.setRmCompId(888821);
//		cmp2_1.setDisplayName("Comp 1 for Svc SF 2");
//		sf2.getCompList().add(cmp2_1);
//		ReportModelCompChildDTO cc2_1_1 =  new ReportModelCompChildDTO();
//		cc2_1_1.setDnaCompId(8888211);
//		cmp2_1.getChildren().add(cc2_1_1);
//		ReportModelCompChildDTO cc2_1_2 =  new ReportModelCompChildDTO();
//		cc2_1_2.setDnaCompId(8888212);
//		cmp2_1.getChildren().add(cc2_1_2);
//		ReportModelCompetencyDTO cmp2_2 = new ReportModelCompetencyDTO();
//		//cmp2_2.setRmCompId(888822);
//		cmp2_2.setDisplayName("Comp 2 for SvcSF 2");
//		sf2.getCompList().add(cmp2_2);
//		ReportModelCompChildDTO cc2_2_1 =  new ReportModelCompChildDTO();
//		cc2_2_1.setDnaCompId(8888221);
//		cmp2_2.getChildren().add(cc2_2_1);
//		//System.out.println("Input - " + struct.toString());
//
//		_svc.replaceReportModel(struct);
//
//		System.out.println("No feedback...dnaId Check the database");
//	
//		UnitTestUtils.stop(this);
//	}


	/*
	 * testfetchLanguageList (both full list and report list
	 */
	public void testfetchLanguageList()
		throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);
	
		ArrayList<LanguageItemDTO> out = _svc.fetchFullLanguageList(true);
		System.out.println("Full list - include default - " + out.size() + " entries.");
		for(LanguageItemDTO itm : out)
		{
			System.out.println("  " + itm.toString());
		}
		
		System.out.println("  >-------------<  ");
		
		out = _svc.fetchFullLanguageList(false);
		System.out.println("Full list - No default - " + out.size() + " entries.");
		for(LanguageItemDTO itm : out)
		{
			System.out.println("  " + itm.toString());
		}

		System.out.println("  >-------------<  ");

		out = _svc.fetchRptLanguageList(true);
		System.out.println("Rpt list - include default - " + out.size() + " entries.");
		for(LanguageItemDTO itm : out)
		{
			System.out.println("  " + itm.toString());
		}
		
		System.out.println("  >-------------<  ");
		
		out = _svc.fetchRptLanguageList(false);
		System.out.println("Rpt list - No default - " + out.size() + " entries.");
		for(LanguageItemDTO itm : out)
		{
			System.out.println("  " + itm.toString());
		}
	
		UnitTestUtils.stop(this);
	}


//	/*
//	 * testFetchReportNote
//	 */
//	public void testFetchReportNote()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//	
//		long dnaId = 20;	// Dev
//		//long dnaId = 41;	// Dev
//
//		ReportNoteDTO dto = _svc.fetchReportNote(dnaId);
//
//		System.out.println("Report note for DNA " + dto.getDnaId() + ":  " + dto.getText());
//	
//		UnitTestUtils.stop(this);
//	}


//	/*
//	 * testUpdateReportNote
//	 */
//	public void testFetchReportModel()
//	throws Exception
//{
//	UnitTestUtils.start(this);
//	AbyDDatabaseUtils.setUnitTest(true);
//
//	long dnaId = 41;
//
//	ReportModelStructureDTO dto = _svc.fetchReportModel(dnaId);
//
//	System.out.println(dto.toString());
//
//	UnitTestUtils.stop(this);
//}

}
