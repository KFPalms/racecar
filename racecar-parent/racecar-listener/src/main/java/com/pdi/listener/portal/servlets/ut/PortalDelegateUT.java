/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.listener.portal.servlets.ut;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import com.pdi.data.dto.ProjectParticipantInstrument;
//import com.pdi.data.dto.Report;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.IProjectParticipantInstrumentHelper;
import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.properties.PropertyLoader;
//import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.response.ReportingResponse;

import junit.framework.TestCase;

public class PortalDelegateUT extends TestCase {
	public PortalDelegateUT(String name)
	{
		super(name);
	}
public void testGenerateReport() throws Exception {
	SessionUser session = new SessionUser();
	session.getMetadata().put("USERNAME", "cdunn");
	session.getMetadata().put("PASSWORD", "cdunn");
	
	String participantId = "XXXXXXX1";
	String engagementId = "ABCDEFGH";
	V2DatabaseUtils.setUnitTest(true);
	IProjectParticipantInstrumentHelper epih = HelperDelegate.getProjectParticipantInstrumentHelper(PropertyLoader.getProperty("com.pdi.data.application","datasource.v2"));
	ArrayList<ProjectParticipantInstrument> epis = epih.fromProjectIdParticipantId(session, IProjectParticipantInstrumentHelper.MODE_FULL, engagementId, participantId);
	for(ProjectParticipantInstrument epi : epis) {
		System.out.println(epi.toString());
	}
	ReportingResponse r =  HelperDelegate.getReportHelper().generate(session); 
	OutputStream outputStream = new FileOutputStream ("SerializeableReport.pdf");
	r.toFile().writeTo(outputStream);
}
	//********************************//
	//****** CLIENT HELPER      ******//
	//********************************//
//	public void testGetClient()
//	throws Exception
//	{
//		SessionUser session = new SessionUser();
//		session.getMetadata().put("USERNAME", "cdunn");
//		session.getMetadata().put("PASSWORD", "cdunn");
//		
//		IClientHelper iclient = HelperDelegate.getClientHelper(PropertyLoader.getProperty("com.pdi.data.application","datasource.v2"));
//		ArrayList<Client> clients = iclient.all(session);
//		for(Client client : clients) {
//			System.out.println(client.getId() + " - " + client.getName());
//		}
//		
//		iclient = HelperDelegate.getClientHelper(PropertyLoader.getProperty("com.pdi.data.application", "datasource.adapt"));
//		clients = iclient.all(session);
//		for(Client client : clients) {
//			System.out.println(client.getId() + " - " + client.getName());
//		}
//		
//		iclient = HelperDelegate.getClientHelper(PropertyLoader.getProperty("com.pdi.data.application","datasource.nhn"));
//		clients = iclient.all(session);
//		for(Client client : clients) {
//			System.out.println(client.getId() + " - " + client.getName());
//		}
//		
//		iclient = HelperDelegate.getClientHelper();
//		clients = iclient.all(session);
//		for(Client client : clients) {
//			System.out.println(client.getId() + " - " + client.getName());
//		}
//		
//		
//	}
//
//	
//	//********************************//
//	//****** PARTICIPANT HELPER  *****//
//	//********************************//
//	
//	public void testGetParticipantsByProjectId()
//	throws Exception{
//		
//		System.out.println("*********** testGetParticipantsByProjectId ***********");
//		SessionUser session = new SessionUser();
//		session.getMetadata().put("USERNAME", "cdunn");
//		session.getMetadata().put("PASSWORD", "cdunn");
//		
//		IProjectParticipantHelper iP = HelperDelegate.getProjectParticipantHelper(PropertyLoader.getProperty("com.pdi.data.application","datasource.v2"));
//		ArrayList<ProjectParticipant> participants = iP.fromProjectId(session, "HSPMUGMB");
//
//		for(ProjectParticipant ePart: participants){
//			
//			Project e = ePart.getProject();
//			System.out.println("     " + e.getId());
//			Participant p = ePart.getParticipant();
//			System.out.println("     " + p.getId() + "  " + p.getFirstName() + " " + p.getLastName());
//			System.out.println("     " + p.getEmail() + "  " + p.getBusinessUnit() + " " + p.getJobTitle());
//			System.out.println("      ------ ");
//			
//		}
//		
//				
//	}
//	
//	public void testGetParticipantByProjectIdParticipantId()
//	throws Exception{
//		
//		System.out.println("*********** testGetParticipantByProjectIdParticipantId ***********");
//		SessionUser session = new SessionUser();
//		session.getMetadata().put("USERNAME", "cdunn");
//		session.getMetadata().put("PASSWORD", "cdunn");
//		
//		IProjectParticipantHelper iP = HelperDelegate.getProjectParticipantHelper(PropertyLoader.getProperty("com.pdi.data.application","datasource.v2"));
//		ProjectParticipant ePart = iP.fromProjectIdParticipantId(session, "HSPMUGMB", "KEXUKZVI");
//		
//		Project e = ePart.getProject();
//		System.out.println("     " + e.getId());
//		Participant p = ePart.getParticipant();
//		System.out.println("     " + p.getId() + "  " + p.getFirstName() + " " + p.getLastName());
//		System.out.println("     " + p.getEmail() + "  " + p.getBusinessUnit() + " " + p.getJobTitle());
//			
//			
//	}
//	
//	public void testGetParticipantById()
//	throws Exception{
//		
//		System.out.println("*********** testGetParticipantById ***********");
//		SessionUser session = new SessionUser();
//		session.getMetadata().put("USERNAME", "cdunn");
//		session.getMetadata().put("PASSWORD", "cdunn");
//		
//		IParticipantHelper iP = HelperDelegate.getParticipantHelper(PropertyLoader.getProperty("com.pdi.data.application","datasource.v2"));
//		Participant p = iP.fromId(session, "FDBACQOM");
//		
//		System.out.println("     " + p.getId() + "  " + p.getFirstName() + " " + p.getLastName());
//		System.out.println("     " + p.getEmail() + "  " + p.getBusinessUnit() + " " + p.getJobTitle());
//			
//			
//	}
//	
//	//********************************//
//	//****** INSTRUMENT HELPER  *****//
//	//********************************//
//
//	public void testInstrumentfromId()
//	throws Exception{
//		
//		System.out.println("*********** INSTRUMENT -  testInstrumentListfromProjectId ***********");
//		SessionUser session = new SessionUser();
//		session.getMetadata().put("USERNAME", "cdunn");
//		session.getMetadata().put("PASSWORD", "cdunn");
//		
//		IInstrumentHelper iI = HelperDelegate.getInstrumentHelper(PropertyLoader.getProperty("com.pdi.data.application","datasource.v2"));
//		Instrument instrument = iI.fromId(session, "IXLGVIOQ");
//
//		System.out.println("     " + instrument.getCode() + "  " + instrument.getName() + "  " + instrument.getId());
//				
//	}
//	
//	public void testInstrumentListfromProjectId()
//	throws Exception{
//		
//		System.out.println("*********** INSTRUMENT -  testInstrumentListfromProjectId ***********");
//		SessionUser session = new SessionUser();
//		session.getMetadata().put("USERNAME", "cdunn");
//		session.getMetadata().put("PASSWORD", "cdunn");
//		
//		IInstrumentHelper iI = HelperDelegate.getInstrumentHelper(PropertyLoader.getProperty("com.pdi.data.application","datasource.v2"));
//		ArrayList<Instrument> instrumentList = iI.fromProjectId(session, "FBODOOSY");
//
//		for(Instrument instrument : instrumentList){
//			System.out.println("     " + instrument.getCode() + "  " + instrument.getName() + "  " + instrument.getId());
//		}
//		
//				
//	}
//	
//	public void testGetANDSaveScoresForInstrument()
//	throws Exception{
//		
//		System.out.println("*********** INSTRUMENT -  testGetScoresForInstrument ***********");
//		SessionUser session = new SessionUser();
//		session.getMetadata().put("USERNAME", "cdunn");
//		session.getMetadata().put("PASSWORD", "cdunn");
//		
//		IInstrumentHelper iI = HelperDelegate.getInstrumentHelper(PropertyLoader.getProperty("com.pdi.data.application","datasource.v2"));
//		HashMap<String, String>  scoreMap = iI.getScoresForInstrument(session, "IXLGVIOQ", "KBBGZZBU", "CPYCWIAD");
//		
//		Iterator <String> iter = scoreMap.keySet().iterator();
//		while(iter.hasNext()){
//			
//			String key = (String) iter.next();
//			String value = scoreMap.get(key);
//			System.out.println("     " + key + " | " + value);
//			
//			
//		}				
//	}
//	
//	public void testSaveScoresForInstrument()
//	throws Exception{
//		UnitTestUtils.start(this);
//		//AbyDDatabaseUtils.setUnitTest(true);
//		V2DatabaseUtils.setUnitTest(true);
//		
//		System.out.println("*********** INSTRUMENT -  testSaveScoresForInstrument ***********");
//		SessionUser session = new SessionUser();
//		session.getMetadata().put("USERNAME", "cdunn");
//		session.getMetadata().put("PASSWORD", "cdunn");
//		
//		HashMap<String, String> scoreMap = new HashMap<String, String>();
//	     scoreMap.put("GPI_TA", "2.75");
//	     scoreMap.put("GPI_DUT", "3");
//	     scoreMap.put("GPI_TC", "5");
//	     scoreMap.put("GPI_INIT", "2.75");
//	     scoreMap.put("GPI_VIS", "3");
//		
//		IInstrumentHelper iI = HelperDelegate.getInstrumentHelper(PropertyLoader.getProperty("com.pdi.data.application","datasource.v2"));
//		boolean success = iI.saveScoresForInstrument(session, "IXLGVIOQ", "KBBGZZBU", "CPYCWIAD", scoreMap);
//		
//		System.out.println("success = " + success);		
//		UnitTestUtils.stop(this);
//	
//		
//	}	
//	
//	
//	
//	public void testGetResponsesForInstrument()
//	throws Exception{
//		
//		System.out.println("*********** INSTRUMENT -  testGetScoresForInstrument ***********");
//		SessionUser session = new SessionUser();
//		session.getMetadata().put("USERNAME", "cdunn");
//		session.getMetadata().put("PASSWORD", "cdunn");
//		
//		IInstrumentHelper iI = HelperDelegate.getInstrumentHelper(PropertyLoader.getProperty("com.pdi.data.application","datasource.v2"));
//		HashMap<String, String>  scoreMap = iI.getResponsesForInstrument(session, "IXLGVIOQ", "KBBGZZBU", "CPYCWIAD");
//		
//		Iterator <String> iter = scoreMap.keySet().iterator();
//		while(iter.hasNext()){
//			
//			String key = (String) iter.next();
//			String value = scoreMap.get(key);
//			System.out.println("     " + key + " | " + value);
//			
//			
//		}
//	}
	/**
	 * save answer data to v2
	 * 
	 */
//	public void testSaveResponsesForInstrument()
//	throws Exception{
//
//		UnitTestUtils.start(this);
//		V2DatabaseUtils.setUnitTest(true);
//		System.out.println("*********** INSTRUMENT -  testSaveResponsesForInstrument ***********");
//		SessionUser session = new SessionUser();
//		session.getMetadata().put("USERNAME", "cdunn");
//		session.getMetadata().put("PASSWORD", "cdunn");
//	
//		HashMap<String, String> answerMap = new HashMap<String, String>();
//		answerMap.put("1", "5");
//		answerMap.put("2", "5");
//		answerMap.put("3", "4");
//		answerMap.put("4", "4");
//		answerMap.put("5", "5");
//				
//		IInstrumentHelper iI = HelperDelegate.getInstrumentHelper(PropertyLoader.getProperty("com.pdi.data.application","datasource.v2"));
//		boolean success = iI.saveResponsesForInstrument(session, "IXLGVIOQ", "KBBGZZBU", "CPYCWIAD", answerMap);
//			
//		System.out.println("success = " + success);		
//		UnitTestUtils.stop(this);
//		
//	}
	
	
//	//******************************************************//
//	//****** ENGAGEMENT-PARTICIPANT-INSTRUMENT HELPER  ****//
//	//*****************************************************//
//	
//	
//	public void testGetEPIfromParticipantId()
//	throws Exception{
//		
//		System.out.println("*********** ENGAGEMENT-PARTICIPANT-INSTRUMENT -  testGetEPIfromParticipantId ***********");
//		SessionUser session = new SessionUser();
//		session.getMetadata().put("USERNAME", "cdunn");
//		session.getMetadata().put("PASSWORD", "cdunn");
//		
//		IProjectParticipantInstrumentHelper epiHI = HelperDelegate.getProjectParticipantInstrumentHelper(PropertyLoader.getProperty("com.pdi.data.application","datasource.v2"));
//		ArrayList<ProjectParticipantInstrument> epiList = epiHI.fromParticipantId(session,  "CPYCWIAD");
//		
//		for(ProjectParticipantInstrument epi : epiList){
//			System.out.println("     " + epi.getProject().getName());
//			System.out.println("          " + epi.getParticipant().getFirstName() + " " + epi.getParticipant().getLastName());
//			System.out.println("          " + epi.getInstrument().getName());
//			System.out.println("          " + epi.getStatus().getCode());
//		}
//		
//
//	}
//	
//	public void testGetEPIfromProjectIdParticipantId()
//	throws Exception{
//		
//		System.out.println("*********** ENGAGEMENT-PARTICIPANT-INSTRUMENT -  testGetEPIfromProjectIdParticipantId ***********");
//		SessionUser session = new SessionUser();
//		session.getMetadata().put("USERNAME", "cdunn");
//		session.getMetadata().put("PASSWORD", "cdunn");
//		
//		IProjectParticipantInstrumentHelper epiHI = HelperDelegate.getProjectParticipantInstrumentHelper(PropertyLoader.getProperty("com.pdi.data.application","datasource.v2"));
//		ArrayList<ProjectParticipantInstrument> epiList = epiHI.fromProjectIdParticipantId(session, "KBBGZZBU", "CPYCWIAD");
//		
//		for(ProjectParticipantInstrument epi : epiList){
//			System.out.println("     " + epi.getProject().getName());
//			System.out.println("          " + epi.getParticipant().getFirstName() + " " + epi.getParticipant().getLastName());
//			System.out.println("          " + epi.getInstrument().getName());
//			System.out.println("          " + epi.getStatus().getCode());
//		}
//		
//
//	}	
//	
//
//	public void testGetEPIfromParticipantIdInstrumentId()
//	throws Exception{
//		
//		System.out.println("*********** ENGAGEMENT-PARTICIPANT-INSTRUMENT -  testGetEPIfromParticipantIdInstrumentId ***********");
//		SessionUser session = new SessionUser();
//		session.getMetadata().put("USERNAME", "cdunn");
//		session.getMetadata().put("PASSWORD", "cdunn");
//
//		IProjectParticipantInstrumentHelper epiHI = HelperDelegate.getProjectParticipantInstrumentHelper(PropertyLoader.getProperty("com.pdi.data.application","datasource.v2"));
//		ArrayList<ProjectParticipantInstrument> epiList = epiHI.fromParticipantIdInstrumentId(session, "CPYCWIAD", "IXLGVIOQ");
//		
//		for(ProjectParticipantInstrument epi : epiList){
//			System.out.println("     " + epi.getProject().getName());
//			System.out.println("          " + epi.getParticipant().getFirstName() + " " + epi.getParticipant().getLastName());
//			System.out.println("          " + epi.getInstrument().getName());
//			System.out.println("          " + epi.getStatus().getCode());
//		}
//		
//
//	}
//
//	public void testGetEPIfromProjectIdParticipantIdInstrumentId()
//	throws Exception{
//		
//		System.out.println("*********** ENGAGEMENT-PARTICIPANT-INSTRUMENT -  testGetEPIfromProjectIdParticipantIdInstrumentId ***********");
//		SessionUser session = new SessionUser();
//		session.getMetadata().put("USERNAME", "cdunn");
//		session.getMetadata().put("PASSWORD", "cdunn");
//
//		IProjectParticipantInstrumentHelper epiHI = HelperDelegate.getProjectParticipantInstrumentHelper(PropertyLoader.getProperty("com.pdi.data.application","datasource.v2"));
//		ProjectParticipantInstrument epi = epiHI.fromProjectIdParticipantIdInstrumentId(session, "KBBGZZBU", "CPYCWIAD", "IXLGVIOQ");
//		
//		System.out.println("     " + epi.getProject().getName());
//		System.out.println("        " + epi.getParticipant().getFirstName() + " " + epi.getParticipant().getLastName());
//		System.out.println("        " + epi.getInstrument().getName());
//		System.out.println("        " + epi.getStatus().getCode());
//	}
//	
//	
//	
//	//******************************************************//
//	//****** ENGAGEMENT-PARTICIPANT-REPORT HELPER  ****//
//	//*****************************************************//
//	
//	public void testGetReportsfromProjectIdParticipantId()
//	throws Exception{
//		
//		System.out.println("*********** ENGAGEMENT-PARTICIPANT-REPORT -  testGetEPIfromProjectIdParticipantIdInstrumentId ***********");
//		SessionUser session = new SessionUser();
//		session.getMetadata().put("USERNAME", "cdunn");
//		session.getMetadata().put("PASSWORD", "cdunn");
//
//		IProjectParticipantReportHelper eprh = HelperDelegate.getProjectParticipantReportHelper(PropertyLoader.getProperty("com.pdi.data.application","datasource.v2"));
//		ArrayList<ProjectParticipantReport> eprList = eprh.fromProjectIdParticipantId(session, "KBBGZZBU", "CPYCWIAD");
//		
//		for(ProjectParticipantReport epr : eprList){
//			System.out.println("     " + epr.getProject().getName());
//			System.out.println("          " + epr.getParticipant().getFirstName() + " " + epr.getParticipant().getLastName());
//			System.out.println("          " + epr.getReport().getCode());
//
//		}
//	}
	
}
