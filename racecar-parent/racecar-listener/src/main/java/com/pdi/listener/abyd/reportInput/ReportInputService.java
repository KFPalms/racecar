package com.pdi.listener.abyd.reportInput;

import java.sql.Connection;

import com.pdi.data.abyd.dto.reportInput.ReportInputDTO;
//import com.pdi.data.abyd.dto.reportInput.ReportInputNoteDTO;
import com.pdi.data.abyd.dto.reportInput.ReportInputNoteHolderDTO;
import com.pdi.data.abyd.dto.setup.ReportModelStructureDTO;
import com.pdi.data.abyd.helpers.reportInput.ReportInputDataHelper;
import com.pdi.data.abyd.helpers.reportInput.ReportInputNoteDataHelper;
import com.pdi.data.abyd.helpers.setup.ReportModelDataHelper;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.data.dto.AttachmentHolder;
import com.pdi.data.dto.Note;
import com.pdi.data.dto.NoteHolder;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.IAttachmentHelper;
import com.pdi.data.helpers.interfaces.INoteHelper;
import com.pdi.xml.XMLUtils;

import flex.messaging.FlexContext;

public class ReportInputService
{

	public static final String VERSION = "Version";
	
	public String getVersion() {
		//System.out.println("Debug stuff");
		return "Revision 1: " + VERSION;
	} 
	
	/**
	 * Get data for the report input
	 * 
	 * @param dnaId, the dnaId
	 * @return An ReportInputDTO object
	 * @throws Exception
	 */

	public void fetchTLTData(long dnaId, String partId)
	{
		return;
	}
	
	public ReportInputDTO fetchReportInput(String participantId, long dnaId)
		throws Exception
	{
		try
		{
			ReportInputDataHelper helper = new ReportInputDataHelper();
			//System.out.println("Let's get the report input for " + dnaId);
			ReportInputDTO reportInput = helper.getReportInput(participantId, dnaId);
			if(reportInput == null || reportInput.getProjectId() == null || reportInput.getInputId() == 0)
			{
				//First time, build a new one
				//System.out.println("NEW REPORT INPUT");
				reportInput = new ReportInputDTO();
				reportInput.setDnaId(dnaId);
				reportInput.setParticipantId(participantId);
				// Should this be done in the constructor?
				reportInput.setOtxtStatusId(ReportInputDTO.IN_PROGRESS);  
				reportInput.setPtxtStatusId(ReportInputDTO.IN_PROGRESS);  
				reportInput.setRateStatusId(ReportInputDTO.IN_PROGRESS);  
				 
				helper.addReportInput(reportInput);
				reportInput = helper.getReportInput(participantId, dnaId);
				helper.updateReportInput(null, reportInput);
			}
			//System.out.println("Leadership Competency Rating is " + reportInput.getLeadershipSkillRating());
			return reportInput;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}


	/**
	 *	Fetch the Notes for a participant
	 *
	 * @param  String participantId
	 * @return NoteHolder holder
	 * @throws Exception
	 */

	public NoteHolder fetchParticipantNotes(NoteHolder noteHolder)
		throws Exception
	{
		//System.out.println("entering fetchParticipantNotes......");
		//System.out.println("participantid = " + noteHolder.getParticipantId());
		NoteHolder holder = new NoteHolder();

		try
		{
			INoteHelper iNote = HelperDelegate.getNoteHelper();
			// The session  does not appear to be used upstream (it appears to be an artifact of earlier coding)
			SessionUser session = null;
			if (FlexContext.getFlexSession() != null)
			{
				session = (SessionUser)FlexContext.getFlexSession().getAttribute("sessionUser");
			}
			holder = iNote.all(session, noteHolder.getParticipantId());
			
			//System.out.println("holdersize: " + holder.getNotesArray().size());
			if(holder != null){
				for(Note dto : holder.getNotesArray()){
					dto.setNoteText(XMLUtils.xmlEscapeString(dto.getNoteText()));
				}
			}
			return holder;
		}
		catch (Exception ex)  
		{    
			ex.printStackTrace();
			return holder;
		}

	}	
	
	/**
	 *	Fetch the Attachments for a participant
	 *
	 * @param  AttachmentHolder used for the participant ID and project ID
	 * @return AttachmentHolder holder
	 * @throws Exception
	 */

	public AttachmentHolder fetchParticipantAttachments(AttachmentHolder attachmentHolderIn)
		throws Exception
	{
		//System.out.println("entering fetchParticipantAttachments......");
		AttachmentHolder holder = new AttachmentHolder();

		try
		{
			IAttachmentHelper iAttachment = HelperDelegate.getAttachmentHelper("com.pdi.data.nhn.helpers.delegated");
			// The session does not appear to be used upstream (it appears to be an artifact of earlier coding)
			SessionUser session = null;
			if (FlexContext.getFlexSession() != null)
			{
				session = (SessionUser)FlexContext.getFlexSession().getAttribute("sessionUser");
			}
			holder = iAttachment.fromProjectIdParticipantId(session, attachmentHolderIn.getProject().getId(), attachmentHolderIn.getParticipant().getId() );
			
			return holder;
		}
		catch (Exception ex)  
		{    
			ex.printStackTrace();
			return holder;
		}
	}	


/************************************************************************************/
	
	public void updateReportInput(String lpId, ReportInputDTO reportInput)
		throws Exception
	{
		try
		{
			ReportInputDataHelper helper = new ReportInputDataHelper();
			helper.updateReportInput(lpId, reportInput);
		}
		catch (Exception e)
		{
			throw new Exception(e);
		}
	}
	
	
	/*******************************/
	/*     Report Input Notes      */
	/*******************************/
	/**
	 * 
	 * Fetch the data for the Report Input Notes 
	 * in EvalGuide/IntegrationGrid
	 *
	 * @param long reportInputId
	 * @return Setup2ReportLabelsDTO object
	 * @throws Exception
	 */
	public ReportInputNoteHolderDTO fetchReportInputNoteData(long reportInputId)
		throws Exception
	{
		//System.out.println("service.... fetchReportInputNoteData ");
		//System.out.println("reportInputId " + reportInputId);
		Connection con = null;
		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();
			ReportInputNoteDataHelper helper = new ReportInputNoteDataHelper();
			ReportInputNoteHolderDTO ret = new ReportInputNoteHolderDTO();
			// fetch the data
			//System.out.println("GET REPORT INPUT NOTES!");
			ret = helper.getReportInputNotes(reportInputId);
			//System.out.println("GET REPORT INPUT NOTES!");
			//System.out.println(ret);
			return ret;
		}
		catch (Exception e)
		{
			throw new Exception(e);
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)
				{
					ex.printStackTrace();
				}
				con = null;
			}
		}
	}
	

	/**
	 * Fetch a Report model with scores
	 *
	 * @param dnaId The dnaId with which the Report Model is associated
	 * @param participantId The participant for whom the scores are to be retrieved
	 * @return A ReportModelStructureDTO object
	 * @throws Exception
	 */
	public ReportModelStructureDTO fetchReportModelWithScores(long dnaId, String participantId)
		throws Exception
	{
		Connection con = null;
		try
		{
			ReportModelDataHelper helper = new ReportModelDataHelper();
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();
			// get the data
			ReportModelStructureDTO ret = helper.getReportModelWithScores(con, dnaId, participantId);
			return ret;
		}
		catch (Exception e)
		{
			throw new Exception(e);
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)
				{
				}
				con = null;
			}
		}
	}       
	
	/**
	 * 
	 * Fetch the data for the Report Input Notes 
	 * in EvalGuide/IntegrationGrid
	 *
	 * @param long reportInputId
	 * @return Setup2ReportLabelsDTO object
	 * @throws Exception
	 */
	public ReportInputNoteHolderDTO updateReportInputNoteData(ReportInputNoteHolderDTO dtoIn)
		throws Exception
	{
		Connection con = null;
		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();
			//add the data
			ReportInputNoteDataHelper helper = new ReportInputNoteDataHelper();
			
			helper.addReportInputNotes(con, dtoIn);
			// because new notes are going to be getting new id's, 
			// we need to fetch a new holder to pass back to the 
			// updated array of notes
			ReportInputNoteHolderDTO dtoOut  = helper.getReportInputNotes(dtoIn.getReportInputId());
			return dtoOut;
		}
		catch (Exception e)
		{
			throw new Exception(e);
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)
				{
					ex.printStackTrace();
				}
				con = null;
			}
		}
	}	
	/************************************************************************************/

	/**
	 * Technically this isn't a service in the sense that it never was exposed in
	 * Flex, but we are putting it here to maintain compatibility; it is called from
	 * a RESTful service found in ReportInputResource.
	 *  
	 * @return
	 */
	public String genDormantExtCand()
	{
		
		ReportInputDataHelper helper = new ReportInputDataHelper();
		String ret = helper.dormantCandExt();

		return ret;
	}
}
