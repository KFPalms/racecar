/**
 * Copyright (c) 2008 Personnel Decisions International, Inc.
 * All rights reserved.
 */

package com.pdi.listener.abyd.setup;

import java.sql.Connection;
import java.util.ArrayList;

import com.pdi.data.abyd.dto.setup.ReportModelCompChildDTO;
import com.pdi.data.abyd.dto.setup.ReportModelStructureDTO;
import com.pdi.data.abyd.dto.setup.ReportModelTranslationDTO;
import com.pdi.data.abyd.dto.setup.ReportNoteDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportLabelsDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportOptionsDTO;
import com.pdi.data.abyd.helpers.setup.ABDComCheckHelper;
import com.pdi.data.abyd.helpers.setup.ReportLabelsDataHelper;
import com.pdi.data.abyd.helpers.setup.ReportModelDataHelper;
import com.pdi.data.abyd.helpers.setup.ReportModelTranslationHelper;
import com.pdi.data.abyd.helpers.setup.ReportNoteDataHelper;
import com.pdi.data.abyd.helpers.setup.ReportOptionsDataHelper;
import com.pdi.data.abyd.helpers.setup.SetupConstants;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;

import com.pdi.data.util.language.LanguageItemDTO;
import com.pdi.data.util.language.LanguageListHelper;

/**
 * SetupService provides entry point framework
 * for the Assessment-by-Design Project Setup app
 *
 * @author		Ken Beukelman
  */
public class Setup2Service
{
	//
	// Static data.
	//
	

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	
	//
	// Constructors.
	//
	public Setup2Service()
	{
		// Does nothing currently
	}

	//
	// Instance methods.
	//

	
	/**
	 * Com Check.  See if the remoting connection and the db are up
	 *
	 * @return A String object
	 * @throws Exception
	 */
	public String comCheck2()
		throws Exception 
	{
		Connection con = null;
		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();
			String ret = "URL=" + con.getMetaData().getURL() + "\n";

			// get the data
			ABDComCheckHelper helper = new ABDComCheckHelper(con);        
			ret += helper.comCheck();
				
			return ret;
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)  {  /* Swallow the exception */  }
				con = null;
			}
		}
	}

	
	/**************************/
	/*      Report Model      */
	/**************************/
	/**
	 * Fetch a Report model
	 *
	 * @param dnaId The dnaId with which the Report Model is associated
	 * @return A ReportModelStructureDTO object
	 * @throws Exception
	 */
	public ReportModelStructureDTO fetchReportModel(long dnaId)
		throws Exception
	{
		Connection con = null;
		
		//new HelperDelegate().setSessionUser(su);
		
		try
		{
			ReportModelDataHelper reportModelDataHelper = new ReportModelDataHelper();
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();
			// get the data
			ReportModelStructureDTO ret = reportModelDataHelper.getReportModel(con, dnaId);
			// unescape for display - assumes called from Flex only
			ret = reportModelDataHelper.unescapeSfAndCompNames(ret);
			return ret;
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)
				{
				}
				con = null;
			}
		}
	}       


	/**
	 * Fetch report model sub-competencies
	 *
	 * @param dnaId The dnaId with which the Report Model is associated
	 * @return A ReportModelStructureDTO object
	 * @throws Exception
	 */
	public ArrayList<ReportModelCompChildDTO> fetchReportModelCompetencyChildren(long dnaId)
		throws Exception
	{
		Connection con = null;
		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();
			// get the data
			ReportModelDataHelper reportModelDataHelper = new ReportModelDataHelper();
			return reportModelDataHelper.getReportModelCompetencyChildren(con, dnaId);
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)
				{
				}
				con = null;
			}
		}
	}       

	
	/**
	 * Replace a Report model
	 * We use this because at this time there are no incremental update of the
	 * report model, just a delete of all data in the model for the DAN and a
	 * write of the new report model data.
	 *
	 * @param newModel The ReportModelStructureDTO object containing the new structure
	 * @throws Exception
	 */
	public void replaceReportModel(ReportModelStructureDTO newModel)
		throws Exception
	{
		Connection con = null;
		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();

			// Replace the model
			ReportModelDataHelper reportModelDataHelper = new ReportModelDataHelper();
			reportModelDataHelper.replaceReportModel(con, newModel);
			
			return;
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)
				{
				}
				con = null;
			}
		}
	}       

	
	/**************************/
	/*     Report Options     */
	/**************************/
	/**
	 * 
	 * Fetch the data for the Dashboard Report Configuration tab
	 * in Setup2
	 *
	 * @param long dnaId
	 * @return Setup2ReportOptionsDTO object
	 * @throws Exception
	 */
	public Setup2ReportOptionsDTO fetchReportOptionsData(long dnaId)
		throws Exception
	{
		Connection con = null;
		try
		{
			ReportOptionsDataHelper reportOptionsDataHelper = new ReportOptionsDataHelper();
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();

			Setup2ReportOptionsDTO ret = new Setup2ReportOptionsDTO();
			// fetch the data
			ret = reportOptionsDataHelper.getReportOptionData(con, dnaId);
			
			return ret;
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)
				{
				}
				con = null;
			}
		}
	}


	/**
	 * 
	 * Fetch the default data settings
	 * for the Dashboard Report Configuration tab
	 * in Setup2
	 *
	 * @param long dnaId
	 * @return Setup2ReportOptionsDTO object
	 * @throws Exception
	 */
	public Setup2ReportOptionsDTO resetToDefaultReportOptionsData(long dnaId)
		throws Exception
	{
		//System.out.println("public Setup2ReportOptionsDTO resetToDefaultReportOptionsData(long dnaId).....");
		Connection con = null;
		try
		{
			ReportOptionsDataHelper reportOptionsDataHelper = new ReportOptionsDataHelper();
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();

			Setup2ReportOptionsDTO ret = new Setup2ReportOptionsDTO();
			// fetch the data
			ret = reportOptionsDataHelper.resetToDefaultReportOptionData(con, dnaId);
			
			
			return ret;
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)
				{
				}
				con = null;
			}
		}
	}


	/**
	 * 
	 * Update the data for the Dashboard Report Configuration tab
	 * in Setup2
	 *
	 * @param Setup2ReportOptionsDTO dtoIn
	 * @throws Exception
	 */
	public void updateReportOptionsData(Setup2ReportOptionsDTO dtoIn)
		throws Exception
	{
		Connection con = null;
		try
		{
			ReportOptionsDataHelper reportOptionsDataHelper = new ReportOptionsDataHelper();
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();

			// update the data
			reportOptionsDataHelper.updateReportOptionsData(con, dtoIn);
			
			return;
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)
				{
				}
				con = null;
			}
		}
	}

	
	/**************************/
	/*     Report Labels      */
	/**************************/
	/**
	 * 
	 * Fetch the data for the Dashboard Report Labels tab
	 * in Setup2
	 *
	 * @param long dnaId
	 * @return Setup2ReportLabelsDTO object
	 * @throws Exception
	 */
	public Setup2ReportLabelsDTO fetchReportLabelsData(long dnaId)
		throws Exception
	{
		Connection con = null;
		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();

			ReportLabelsDataHelper reportLabelsDataHelper = new ReportLabelsDataHelper();
			Setup2ReportLabelsDTO ret = new Setup2ReportLabelsDTO();
			// fetch the data
			// ReportLabelsDataHelper.EDITABLE will get editable data, 
			//      which is the original reportLabel data
			ret = reportLabelsDataHelper.getSetupReportLabelData(dnaId, SetupConstants.EDITABLE); 
			
			//System.out.println("Label size = " + ret.getLabelValuesArray().size());
			return ret;
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)
				{
					ex.printStackTrace();
				}
				con = null;
			}
		}
	}


	/**
	 * 
	 * Update the data for the Dashboard Report Labels tab
	 * in Setup2
	 *
	 * @param Setup2ReportLabelsDTO dtoIn - incoming dto with
	 * data to save
	 * @throws Exception
	 */
	public void updateReportLabelsData(Setup2ReportLabelsDTO dtoIn)
		throws Exception
	{
		Connection con = null;
		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();

			// update the data
			ReportLabelsDataHelper reportLabelsDataHelper = new ReportLabelsDataHelper();
			reportLabelsDataHelper.updateReportLabelData(dtoIn);
			
			return;
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)
				{
				}
				con = null;
			}
		}
	}

	
	/**************************/
	/*      Report Note       */
	/**************************/
	/**
	 * Fetch a Report note
	 *
	 * @param dnaId The dnaId with which the Report Note is associated
	 * @return A ReportNoteDTO object
	 * @throws Exception
	 */
	public ReportNoteDTO fetchReportNote(long dnaId)
		throws Exception
	{
		Connection con = null;
		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();
			ReportNoteDataHelper reportNoteDataHelper = new ReportNoteDataHelper();
			// get the data
			ReportNoteDTO ret = reportNoteDataHelper.getReportNote(con, dnaId);
			return ret;
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)
				{
				}
				con = null;
			}
		}
	}       


	/**
	 * Update a Report note
	 *
	 * @param newNote The ReportNoteDTO object containing the new note text
	 * @throws Exception
	 */
	public void updateReportNote(ReportNoteDTO newNote)
		throws Exception
	{
		Connection con = null;
		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();
			ReportNoteDataHelper reportNoteDataHelper = new ReportNoteDataHelper();
			// Update the note
			reportNoteDataHelper.saveReportNote(con, newNote);
			
			return;
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)
				{
				}
				con = null;
			}
		}
	}       


	/**************************/
	/*     Language List      */
	/**************************/

	// This does not appear to be used (see fetchRptLanguageList, which is used)
	// Delete after migration testing
//	/**
//	 * Fetch a language list of report languages only
//	 *
//	 * @param includeDefault - A flag indicating whether or not to include the default language in the laist
//	 * @return An ArrayList of LanguageListItemDTO objects
//	 * @throws Exception
//	 */
//	public ArrayList<LanguageItemDTO> fetchReportLanguageList(boolean includeDefault)
//		throws Exception
//	{
//		Connection con = null;
//		ArrayList<LanguageItemDTO> ret = new ArrayList<LanguageItemDTO>();
//		try
//		{
//			// Connect to the database
//			con = AbyDDatabaseUtils.getDBConnection();
//
//			LanguageListHelper helper = new LanguageListHelper(con);
//			ret = helper.getLangList(true, includeDefault); 
//
//			//System.out.println("Language array size = " + ret.size());
//			return ret;
//		}
//		finally
//		{
//			// close the connection
//			if (con != null)
//			{
//				try  {  con.close();  }
//				catch (Exception ex)
//				{
//					ex.printStackTrace();
//				}
//				con = null;
//			}
//		}
//	}


	/**
	 * Fetch a language list of the available languages (report and non-report)
	 *
	 * @param includeDefault - A flag indicating whether or not to include the default language in the laist
	 * @return An ArrayList of LanguageListItemDTO objects
	 * @throws Exception
	 */
	public ArrayList<LanguageItemDTO> fetchFullLanguageList(boolean includeDefault)
		throws Exception
	{
		Connection con = null;
		ArrayList<LanguageItemDTO> ret = new ArrayList<LanguageItemDTO>();
		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();

			LanguageListHelper helper = new LanguageListHelper(con);
			ret = helper.getLangList(true, includeDefault); 

			//System.out.println("Language array size = " + ret.size());
			return ret;
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)
				{
					ex.printStackTrace();
				}
				con = null;
			}
		}
	}


	/**
	 * Fetch a language list of the report languages only
	 *
	 * @param includeDefault - A flag indicating whether or not to include the default language in the laist
	 * @return An ArrayList of LanguageListItemDTO objects
	 * @throws Exception
	 */
	public ArrayList<LanguageItemDTO> fetchRptLanguageList(boolean includeDefault)
		throws Exception
	{
		Connection con = null;
		ArrayList<LanguageItemDTO> ret = new ArrayList<LanguageItemDTO>();
		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();

			LanguageListHelper helper = new LanguageListHelper(con);
			ret = helper.getLangList(false, includeDefault); 

			//System.out.println("Language array size = " + ret.size());
			return ret;
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)
				{
					ex.printStackTrace();
				}
				con = null;
			}
		}
	}


	/*****************************/
	/* Report Model Translation  */
	/*****************************/

	/**
	 * Fetch translation data for a DNA and language
	 *
	 * @param xlateDto - A partially populated ReportModelTranslationDTO object (DNA ID & LanguageItemDTO)
	 * @return An ArrayList of LanguageListItemDTO objects
	 * @throws Exception
	 */
	public ReportModelTranslationDTO fetchRmXlateData(ReportModelTranslationDTO xlateDto)
		throws Exception
	{
		Connection con = null;
		ReportModelTranslationDTO ret = new ReportModelTranslationDTO();

		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();

			ReportModelTranslationHelper helper = new ReportModelTranslationHelper(con);
			ret = helper.getTranslationData(xlateDto); 

			return ret;
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)  {  ex.printStackTrace();  }
				con = null;
			}
		}
	}


	/**
	 * Update translation data for a DNA and language
	 *
	 * @param xlateDto - A fully populated ReportModelTranslationDTO object
	 *                   w/change flags set as appropriate
	 * @throws Exception
	 */
	public void updateRmXlateData(ReportModelTranslationDTO xlateDto)
		throws Exception
	{
		Connection con = null;

		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();

			ReportModelTranslationHelper helper = new ReportModelTranslationHelper(con);
			helper.updateTranslationData(xlateDto); 

			return;
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)  {  ex.printStackTrace();  }
				con = null;
			}
		}
	}
}
