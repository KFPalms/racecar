/**
 * Copyright (c) 2017 Korn Ferry Hay Group
 * All rights reserved.
 */
package com.pdi.listener.portal.delegated.helpers.reports;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.pdi.data.dto.ProjectParticipantInstrument;
import com.pdi.properties.PropertyLoader;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.GroupReport;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.request.ReportingRequest;
import com.pdi.string.StringUtils;

/*
 * KfalpReports - class to hold methods relevant to some KFALP reports (see
 *                AlrExtract and AbyDReports for more generalized reports).
 *                It currently gathers data only for the raw data extract.
 *
 * NOTE:  EVEN THOUGH LOCATED UNDER THE DELEGATED FOLDER, THERE ARE NO METHODS IN THIS
 *        CLASS THAT ARE DEFINED IN THE DATA INTERFACE.
 *
 * This class is located here because the folder structure (delegated/helpers) is
 * inverted from normal practice elsewhere in this project stack (helpers/delegated).
 * Putting this helper class here avoided confusion with having multiple folders in the
 * portal structure that had the name "helper".
 *
 */
public class KfalpReports {
	//
	// Static data.
	//

	private static final String SCORE_PARM_STR = "/KfalpConsultScoresServlet?pptId={pptId}&projId={projId}";

	//
	// Static methods.
	//

	//
	// Instance data.
	//

	//
	// Constructors.
	//
	public KfalpReports() {
		// No logic here at the present time
	}

	//
	// Instance methods.
	//

	public void getRawReportData(HashMap<String, GroupReport> groupReports, ReportingRequest reportingRequest,
			ArrayList<ProjectParticipantInstrument> epis) throws Exception {
		IndividualReport ir = new IndividualReport();
		ir.setReportCode(ReportingConstants.REPORT_CODE_KFALP_RAW_EXTRACT);
		if (epis.size() < 1) {
			ir.setName("NoData");
		} else {
			// Get the relevant ppt and project data (assumes
			// all epis in the group have the same ppt & proj)
			ProjectParticipantInstrument ppi = epis.get(0);
			String pptId = ppi.getParticipant().getId();
			String projId = ppi.getProject().getId();

			ir.getDisplayData().put("Ppt",
					ppi.getParticipant().getLastName() + ", " + ppi.getParticipant().getFirstName());
			ir.getDisplayData().put("ProjId", projId);
			ir.getDisplayData().put("ProjName", ppi.getProject().getName());
			ir.getDisplayData().put("ClientName", ppi.getProject().getClient().getName());

			// Get the data and dump it into the ir
			try {
				Map<String, String> scoreData = getScoreData(pptId, projId);
				ir.getDisplayData().putAll(scoreData);
			} catch (Exception e) {
				String err = "KfalpReports: Error fetching score data.  Msg=" + e.getMessage();
				System.out.println(err);
				throw new Exception(err);
			}

			// Put the ir into the gr
			if (groupReports.get(ir.getReportCode()) == null) {
				// This happens only the first time
				GroupReport gr = new GroupReport();
				gr.setReportCode(ir.getReportCode());
				String fileName = StringUtils.removeFilenameSpecialCharacters(
						gr.getReportCode() + " " + new SimpleDateFormat("yyyyMMdd").format(new Date()) + ".xls");
				gr.setName(fileName.replace(" ", "_"));

				reportingRequest.addReport(gr);
				groupReports.put(ir.getReportCode(), gr);
			}
			groupReports.get(ir.getReportCode()).getIndividualReports().add(0, ir);
		}
	}

	/**
	 *
	 * @param pptId
	 * @param projId
	 * @return
	 * @throws Exception
	 */
	private Map<String, String> getScoreData(String pptId, String projId) throws Exception {
		Map<String, String> ret = new HashMap<String, String>();

		// Call the raw consulting data servlet
		URL url = null;
		URLConnection uc = null;
		InputStream content = null;

		// Call the reportweb servlet
		String surl = PropertyLoader.getProperty("com.pdi.data.abyd.application", "palms.reportServer.url");
		if (surl.charAt(surl.length() - 1) == '/') {
			surl = surl.substring(0, surl.length() - 2);
		}
		surl += SCORE_PARM_STR;

		surl = surl.replace("{pptId}", pptId);
		surl = surl.replace("{projId}", "" + projId);

		try {
			url = new URL(surl);
		} catch (MalformedURLException e) {
			String err = "KfalpReports.getScoreData - Malformed URL - " + surl;
			System.out.println(err);
			throw new Exception(err);
		}

		// open the connection.
		try {
			uc = url.openConnection();
		} catch (IOException e) {
			String err = "KfalpReports.getScoreData - Unable to open connection.  URL=" + surl + ".  Msg="
					+ e.getMessage();
			System.out.println(err);
			throw new Exception(err);
		}

		// Get the data
		HttpURLConnection huc = (HttpURLConnection) uc;
		boolean hasScores = true;
		try {
			int statusCode = huc.getResponseCode();
			if (statusCode == HttpServletResponse.SC_NO_CONTENT
					|| statusCode == HttpServletResponse.SC_INTERNAL_SERVER_ERROR) {
				hasScores = false;
			} else if (statusCode >= 200 && statusCode < 300) {
				content = huc.getInputStream();
			} else {
				String str = "KfalpReports.getScoreData - Error returned from score fetch: Code=" + statusCode
						+ " for URL=" + surl + ".  Message=" + huc.getResponseMessage();
				System.out.println(str);
				throw new Exception(str);
			}
		} catch (IOException e) {
			String err = "KfalpReports.getScoreData: Error fetching score RGR data.  Msg=" + e.getMessage();
			System.out.println(err);
			throw new Exception(err);
		}

		// get the XML
		StringBuffer result = new StringBuffer();
		if (hasScores) {
			BufferedReader br = new BufferedReader(new InputStreamReader(content));
			String line;
			try {
				while ((line = br.readLine()) != null) {
					result.append(line);
				}
			} catch (IOException e) {
				String err = "KfalpReports.getScoreData - Unable to read content.  URL=" + surl + ".  Msg="
						+ e.getMessage();
				System.out.println(err);
				throw new Exception(err);
			}
		} else {
			// The problem is in KfalpConsultingExtractServlet in PALMS
			// report-web
			System.out.println("KfalpReports.getScoreData - servlet returned SC_NO_CONTENT");
		}

		// pull out the data (simple key/value pairs)
		// TODO - should we be checking for an empty result?
		if (result.length() > 0) {
			DocumentBuilderFactory dbfac = null;
			DocumentBuilder docBuilder = null;
			Document doc = null;
			String xml = "";
			try {
				dbfac = DocumentBuilderFactory.newInstance();
				docBuilder = dbfac.newDocumentBuilder();
				xml = result.toString();
				doc = docBuilder.parse(new InputSource(new StringReader(xml)));
			} catch (ParserConfigurationException pce) {
				String err = "KfalpReports.getScoreData() - XML parser config ERROR.  msg=" + pce.getMessage();
				System.out.println(err);
				throw new Exception(err);
			} catch (SAXException se) {
				String err = "KfalpReports.getScoreData() - XML SAX ERROR.  msg=" + se.getMessage() + " \n" + " ppt="
						+ pptId + ", proj=" + projId + ", XML=" + xml;
				System.out.println(err);
				throw new Exception(err);
			} catch (IOException e) { // everything else
				String err = "KfalpReports.getScoreData() - IOException ERROR.  msg=" + e.getMessage();
				System.out.println(err);
				throw new Exception(err);
			}

			Element root = doc.getDocumentElement();
			NodeList children = root.getChildNodes();
			for (int i = 0; i < children.getLength(); i++)

			{
				Node child = children.item(i);
				NamedNodeMap anm = child.getAttributes();
				String keyVal = anm.getNamedItem("key").getNodeValue();
				String valVal = anm.getNamedItem("value").getNodeValue();
				ret.put(keyVal, valVal);
			}
		}

		return ret;
	}
}
