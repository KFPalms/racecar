package com.pdi.listener.portal.flex.ut;

//import java.util.ArrayList;

//import com.pdi.data.v3.dto.SessionUserDTO;
//import com.pdi.data.v3.dto.UserDTO;
//import com.pdi.data.v3.helpers.UserDataHelper;
//import java.util.Date;
//import java.util.Random;

//import java.io.FileOutputStream;
//import java.io.OutputStream;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.Iterator;

//import com.pdi.data.v3.dto.CandidateDTO;
//import com.pdi.data.v3.dto.ParticipantDTO;
//import com.pdi.data.v3.dto.ParticipantListDTO;
//import com.pdi.data.v3.dto.ParticipationDTO;
//import com.pdi.data.v3.dto.ProjectDTO;
//import com.pdi.data.v3.dto.ResultDTO;
//import com.pdi.data.v3.dto.ScaleScoreDTO;
//import com.pdi.data.v3.dto.SessionUserDTO;
//import com.pdi.data.v3.helpers.ParticipantDataHelper;
//import com.pdi.data.v3.helpers.ProjectDataHelper;
//import com.pdi.data.v3.helpers.ReportDataHelper;
//import com.pdi.data.v3.util.V3DatabaseUtils;
//import com.pdi.listener.portal.flex.PortalFlexService;
//import com.pdi.properties.PropertyLoader;
//import com.pdi.reporting.report.GroupReport;
//import com.pdi.reporting.report.IndividualReport;
//import com.pdi.reporting.report.ReportData;
//import com.pdi.reporting.request.ReportingRequest;
//import com.pdi.reporting.response.ReportingResponse;
//import com.pdi.scoring.Norm;
//import com.pdi.scoring.NormSet;
//import com.pdi.ut.UnitTestUtils;

//import flex.messaging.FlexContext;
//import flex.messaging.FlexSession;

import junit.framework.TestCase;

public class V3PortalFlexServiceUT extends TestCase
{
	public V3PortalFlexServiceUT(String name)
	{
		super(name);
	} 
  
	/*
	 * NOTE -- This is a V3 service tester
	 */
	public static void main(String[] args) throws Exception
	{
		junit.textui.TestRunner.run(V3PortalFlexServiceUT.class);
	}


//	/**
//	 * Builds the TLT Group Report, complete
//	 * @throws Exception
//	 */
//	public void testBuildTLTGroupFull() throws Exception
//	{
//		UnitTestUtils.start(this);
//		V3DatabaseUtils.setUnitTest(true);
//
//		/*
//		 * ****************************************
//		 * REPORT SPECIFIC CODE
//		 * ****************************************
//		 */
//		ReportingRequest reportRequest = new ReportingRequest();
//		
//		GroupReport groupSummary = new GroupReport();
//		groupSummary.setReportCode("TLT_GROUP_DETAIL_REPORT");
//		groupSummary.setName("TestTLTGroupDetail.pdf");
//		
//		  /*
//		   * ****************************************
//		   * V3 SPECIFIC CODE
//		   * ****************************************
//		   */
//		SessionUserDTO sessionUser = new SessionUserDTO();
//		sessionUser.setUsername(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.username")); 
//		sessionUser.setPassword(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.password"));
//		
//		//set up fake project for testing
//		ProjectDTO project = new ProjectDTO();
//		project.setUri("https://platform.iscopia.com/webservices1/rest/v1/project/PWDQ-KPQN");
//		project = ProjectDataHelper.getProject(sessionUser, project);
//			
//
//		  /*
//		   * ****************************************
//		   * NORMSET DATA
//		   * ****************************************
//		   */
//		
//		String transitionLevelString = project.getCustomFields().get(ProjectDTO.TRANSITION_TYPE_KEY);
//		int transitionLevel = ReportDataHelper.getTLTIndividualNormSetId(transitionLevelString);
//		int targetTransitionLevel = ReportDataHelper.getTargetLevelIDForTransition(transitionLevelString);
//		
//		//set these in the report object... 
//		groupSummary.setTransitionLevel(transitionLevel);
//		groupSummary.setTargetTransitionLevel(targetTransitionLevel);
//	
//		// GET INDIVIDUAL NORMS THE SAME WAY AS IN THE INDIVIDUAL REPORT
//		
//		// create the individual report just to hold the data... 
//		HashMap<String, String> individualNorms = new HashMap<String, String>();
//	
//		Integer normSetId = ReportDataHelper.getTLTIndividualNormSetId(transitionLevelString);
//		NormSet normset;
//		normset = ReportDataHelper.getPdiNormset(normSetId);
//		normset = ReportDataHelper.populateNormset(normset);
//		for(Norm norm : normset.getNorms()){
////			System.out.println("NORMNAMES:  " + norm.getName()+ "_MEAN  " + norm.getMean());			
//			individualNorms.put(norm.getName()+ "_MEAN", new Double(norm.getMean()).toString());
//			individualNorms.put(norm.getName()+ "_STDEV", new Double(norm.getStdDev()).toString());
//			
//		}		
//				
//		  /*
//		   * ****************************************
//		   * RAVEN'S NORM  (HARD-CODED FOR NOW)
//		   * ****************************************
//		   */
//		
//		groupSummary.addDisplayData("RAVENS_SHORT_MEAN", "14.26");
//		groupSummary.addDisplayData("RAVENS_SHORT_STANDARD_DEVIATION", "4.14");
//
//		  /*
//		   * ****************************************
//		   * GROUP NORMSETS....
//		   * ****************************************
//		   */		
//
//		HashMap<String, NormSet> groupNormSets = new HashMap<String, NormSet>();
//		groupNormSets = ReportDataHelper.getPdiNormsets(new Integer(transitionLevel), new Double(targetTransitionLevel));
//		Iterator<String> it = groupNormSets.keySet().iterator();
//		while (it.hasNext()) 
//		{		
//			String s = it.next();
////			System.out.println("s " + s);
//			 NormSet ns = groupNormSets.get(s);
//			 for(Norm n : ns.getNorms()){			 
////				System.out.println("NORMNAMES:  " + ns.getDisplayName() + "_" +  n.getName()+ "_MEAN  " + n.getMean());	
////				System.out.println("NORMNAMES:  " + ns.getDisplayName() + "_" +  n.getName()+ "_STDEV  " + n.getStdDev());	
//				groupSummary.addDisplayData(ns.getDisplayName() + "_" + n.getName()+ "_MEAN", new Double(n.getMean()).toString());
//				groupSummary.addDisplayData(ns.getDisplayName() + "_" + n.getName()+ "_STDEV", new Double(n.getStdDev()).toString());
//				groupSummary.addDisplayData(ns.getDisplayName() + "_" + n.getName()+ "_WEIGHT", new Double(n.getWeight()).toString());
////				System.out.println("NORMNAMES:  " + ns.getDisplayName() + "_" + n.getName()+ "_WEIGHT " + n.getWeight());			
//			 }
//		
//		}
//		
//		  /*
//		   * ****************************************
//		   * PROJECT LEVEL DISPLAY DATA
//		   * ****************************************
//		   */		
//		
//		groupSummary.getDisplayData().put("ORGANIZATION", project.getClient().getName());
//		groupSummary.getDisplayData().put("CURRENT_LEVEL", project.getCustomFields().get(ProjectDTO.TRANSITION_CURRENT_LEVEL_KEY));
//		groupSummary.getDisplayData().put("TARGET_LEVEL", project.getCustomFields().get(ProjectDTO.TRANSITION_TARGET_LEVEL_KEY));
//		//TODO: Put real data here (from Project)
//		groupSummary.getDisplayData().put("DATE_STRING", "01-Jul-2055");
//		
////		System.out.println("project.getClient().getName() : " + project.getClient().getName());
////		System.out.println("project.getCustomFields().get(current.level.t_pdi_dev) : " + project.getCustomFields().get(ProjectDTO.TRANSITION_CURRENT_LEVEL_KEY));
////		System.out.println("project.getCustomFields().get(target.level.t_pdi_dev) : " + project.getCustomFields().get(ProjectDTO.TRANSITION_TARGET_LEVEL_KEY));
//		
//
//		  /*
//		   * ****************************************
//		   * V3 PARTICIPANT DATA
//		   * ****************************************
//		   */
//		
//		ParticipantListDTO participants = ParticipantDataHelper.getParticipants(sessionUser, project);
//		for(ParticipantDTO participant : participants.getParticipantList()) {
//			participant = ParticipantDataHelper.getParticipantResults(sessionUser, participant);
//			IndividualReport individual = new IndividualReport();
//			individual.addDisplayData("FIRST_NAME", participant.getCandidate().getIndividual().getGivenName());
//			individual.addDisplayData("LAST_NAME", participant.getCandidate().getIndividual().getFamilyName());
//			individual.addDisplayData("EMAIL", participant.getCandidate().getIndividual().getEmail());
//			individual.addDisplayData("MOBILE", participant.getCandidate().getIndividual().getMobile());
//			individual.addDisplayData("PHONE", participant.getCandidate().getIndividual().getTelephone());
////System.out.println("173 - individual.addDisplayData(FIRST_NAME)" + individual.getDisplayData("FIRST_NAME"));
//
//			//TODO: Fix business unit
//			individual.addDisplayData("BUSINESS_UNIT", "TEST BUSINESS UNIT");
//			
//			/*
//			 * For each participant, add the required norm data 
//			 * that was retrieved above.... 
//			 */			
//			Iterator<String> iter = individualNorms.keySet().iterator();
//			while(iter.hasNext()){
//				String key = iter.next();
//				individual.addDisplayData(key, individualNorms.get(key));
//			}
//			
//			
//			/*
//			 * GET SCORES out of V3
//			 */
//			for(ParticipationDTO participation : participant.getParticipations()) {
//				
////				int count = 0;
//				for(ResultDTO result : participation.getResults()) {
//					
//					//System.out.println("----------> " +result.getModule().getModuleName());
//
//					//Build a new report data for each instrument
//					ReportData reportData = new ReportData();
//					reportData.addDisplayData("INSTRUMENT_NAME", result.getModule().getModuleName());
//
//					for(ScaleScoreDTO scaleScore : result.getScaleScores()) {
//						//add scale score
//						reportData.addScoreData(scaleScore.getId(), scaleScore.getValue());
//						//System.out.println("scaleScore.getId() " + scaleScore.getId() + "  value: " + scaleScore.getValue());
//					}
//					//add entire instrument to the report data block
//					individual.getReportData().put(result.getModule().getModuleName(), reportData);
//				}
//				//add the participant data to the group report
//				groupSummary.getIndividualReports().add(individual);
//				
//			}
//			
//		}
//		reportRequest.addReport(groupSummary);
//		
//		//Get response
//		ReportingResponse reportResponse = reportRequest.generateReports();
//		OutputStream outputStream = new FileOutputStream ("TestTLTGroupDetail.pdf");
//		reportResponse.toFile().writeTo(outputStream);
//
//		
//	  UnitTestUtils.stop(this);
//  }
 

//  /**
//   * Build the group summary report
//   * @throws Exception
//   */
//  public void testBuildTLTGroupSummary() throws Exception {
//	  UnitTestUtils.start(this);
////	  if(true) {
////		  return;
////	  }
//	  /*
//	   * ****************************************
//	   * REPORT SPECIFIC CODE
//	   * ****************************************
//	   */
//		ReportingRequest reportRequest = new ReportingRequest();
//		
//		GroupReport groupSummary = new GroupReport();
//		groupSummary.setReportCode("TLT_GROUP_SUMMARY_REPORT");
//		groupSummary.setName("TestTLTGroupSummary.pdf");
//		
//		  /*
//		   * ****************************************
//		   * V3 SPECIFIC CODE
//		   * ****************************************
//		   */
//		SessionUserDTO sessionUser = new SessionUserDTO();
//		sessionUser.setUsername(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.username")); 
//		sessionUser.setPassword(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.password"));
//		ProjectDTO project = new ProjectDTO();
//		project.setId("PWDQ-KPQN");
//		ParticipantListDTO participants = ParticipantDataHelper.getParticipants(sessionUser, project);
//		
//		  /*
//		   * ****************************************
//		   * REPORT DATA
//		   * ****************************************
//		   */
//		
//  		//TODO: Put real norms here
//		groupSummary.addDisplayData("TRANSITION_MEAN", "0.16568741");
//		groupSummary.addDisplayData("TRANSITION_STANDARD_DEVIATION", "0.16568741");
//  		
//  		//TODO: Put real data here
//		groupSummary.addDisplayData("ORGANIZATION", "AccuTech");
//		groupSummary.addDisplayData("CURRENT_LEVEL", "Mid-Level Leader");
//		groupSummary.addDisplayData("TARGET_LEVEL", "Business Unit Leader");
//		groupSummary.addDisplayData("DATE_STRING", "01-Jul-2055");
//		
//
//		for(ParticipantDTO participant : participants.getParticipantList()) {
//			participant = ParticipantDataHelper.getParticipantResults(sessionUser, participant);
//			IndividualReport individual = new IndividualReport();
//			individual.addDisplayData("FIRST_NAME", participant.getCandidate().getIndividual().getGivenName());
//			individual.addDisplayData("LAST_NAME", participant.getCandidate().getIndividual().getFamilyName());
//			individual.addDisplayData("EMAIL", participant.getCandidate().getIndividual().getEmail());
//			individual.addDisplayData("MOBILE", participant.getCandidate().getIndividual().getMobile());
//			individual.addDisplayData("PHONE", participant.getCandidate().getIndividual().getTelephone());
//			
//			//TODO: Get business unit (from somewhere)?
//			individual.addDisplayData("BUSINESS_UNIT", "TEST BUSINESS UNIT");
//			
//			/*
//			 * GET SCORES out of V3
//			 */
//			for(ParticipationDTO participation : participant.getParticipations()) {
//				for(ResultDTO result : participation.getResults()) {
//					
//					System.out.println("----------> " +result.getModule().getModuleName());
//
//					//Build a new repot data for each instrument
//					ReportData reportData = new ReportData();
//					reportData.addDisplayData("INSTRUMENT_NAME", result.getModule().getModuleName());
//					
//					for(ScaleScoreDTO scaleScore : result.getScaleScores()) {
//						//add scale score
//						reportData.addScoreData(scaleScore.getId(), scaleScore.getValue());
//					}
//					//add entire instrument to the report data block
//					individual.getReportData().put(result.getModule().getModuleName(), reportData);
//				}
//			}
//			//add the participant data to the group report
//			groupSummary.getIndividualReports().add(individual);
//		}
//		reportRequest.addReport(groupSummary);
//		
//		//Get response
//		ReportingResponse reportResponse = reportRequest.generateReports();
//		OutputStream outputStream = new FileOutputStream ("TestTLTGroupSummary.pdf");
//		reportResponse.toFile().writeTo(outputStream);
//
//	  UnitTestUtils.stop(this);
//  }


//  /*
//   * 
//   */
//  public void testBuildTLTIndividual() throws Exception {
//	  UnitTestUtils.start(this);
//	 // V3DatabaseUtils.setUnitTest(true);
////	  if(true) {
////		  return;
////	  }
//
//	  /*
//	   * ****************************************
//	   * REPORT SPECIFIC CODE
//	   * ****************************************
//	   */
//		ReportingRequest reportRequest = new ReportingRequest();
//		
//		IndividualReport tltIndividual = new IndividualReport();
//		
//		//Setup the report code
//		tltIndividual.setReportCode("TLT_INDIVIDUAL_SUMMARY_REPORT");
//		tltIndividual.setName("TestTLTReportIndividual.pdf");
//			
//	  /*
//	   * ****************************************
//	   * V3 SPECIFIC CODE
//	   * ****************************************
//	   */
//		SessionUserDTO sessionUser = new SessionUserDTO();
//		sessionUser.setUsername(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.username")); 
//		sessionUser.setPassword(PropertyLoader.getProperty("com.pdi.data.v3.application", "webservice.password"));
//
//		// set up fake participant
//		ParticipantDTO participant = new ParticipantDTO();
//		participant.setUri("https://platform.iscopia.com/webservices1/rest/v1/project/PWDQ-KPQN/participant/TSJSP-MFTXY");
//		System.out.println("GET PARTICIPANT");
//
//		participant = ParticipantDataHelper.getParticipant(sessionUser, participant);
//		System.out.println("GET RESULTS");
//		participant = ParticipantDataHelper.getParticipantResults(sessionUser, participant);
//		System.out.println(participant.getCandidate().getIndividual().getFamilyName());
//		
////		//set up fake project
/////		ProjectDTO project = ProjectDataHelper.getProjectFromURI("https://platform.iscopia.com/webservices1/rest/v1/project/PWDQ-KPQN");
//		ProjectDTO project = new ProjectDTO();
//		project.setUri("https://platform.iscopia.com/webservices1/rest/v1/project/PWDQ-KPQN");
//		project = ProjectDataHelper.getProject(sessionUser, project);
//
//	 /*
//	   * ****************************************
//	   * BIND PARTICIPANT DATA TO REPORT OBJECT
//	   * THIS IS JUST BINDING DISPLAY DATA (First name, last name, email etc etc etc)
//	   * ****************************************
//	   */
//  		tltIndividual.addDisplayData("FIRST_NAME", participant.getCandidate().getIndividual().getGivenName());
//  		tltIndividual.addDisplayData("LAST_NAME", participant.getCandidate().getIndividual().getFamilyName());
//  		tltIndividual.addDisplayData("EMAIL", participant.getCandidate().getIndividual().getEmail());
//  		tltIndividual.addDisplayData("MOBILE", participant.getCandidate().getIndividual().getMobile());
//  		tltIndividual.addDisplayData("PHONE", participant.getCandidate().getIndividual().getTelephone());
//  		
//  	  /*
//  	   * ****************************************
//  	   * BIND NORMS DATA TO REPORT OBJECT
//  	   * ****************************************
//  	   */  		
//  		// get the transition level from Project  		
//		String transitionLevel = project.getCustomFields().get(ProjectDTO.TRANSITION_TYPE_KEY);
//		
//		Integer normSetId = 0;
//		NormSet normset;
//		// we get the transition level back as a string, so we'll need to translate that
//		// into a valid normset value... not the best way to do this, but the way I can 
//		// make this work for now.... 
//		if(transitionLevel.equals("IC - MLL") || transitionLevel.equals("FLL - MLL")){
//			normSetId = new Integer(1);
//			
//		}else if (transitionLevel.equals("FLL - Sr Exec") || transitionLevel.equals("MLL - Sr Exec")){			
//			normSetId = new  Integer(2);
//		}
//		else if(transitionLevel.equals("IC - FLL")){			
//			normSetId = new Integer(3);
//			
//		}else {
//			
//			// error condition
//			// can't go on w/out a valid normset
//		}
//  		
//		normset = ReportDataHelper.getPdiNormset(normSetId);
//		normset = ReportDataHelper.populateNormset(normset);
//		for(Norm norm : normset.getNorms()){
//			//System.out.println("NORMNAMES:  " + norm.getName()+ "_MEAN");
//			tltIndividual.addDisplayData(norm.getName()+ "_MEAN", new Double(norm.getMean()).toString());
//			tltIndividual.addDisplayData(norm.getName()+ "_STDEV", new Double(norm.getStdDev()).toString());
//			
//		}
//		
//		// ALSO ADD HARD-CODED RAVEN'S MEAN...
//		//score = (ravensScore - 14.26) / 4.14;.
//  		tltIndividual.addDisplayData("RAVENS_SHORT_MEAN", "14.26");
//  		tltIndividual.addDisplayData("RAVENS_SHORT_STANDARD_DEVIATION", "4.14");
//  		
//  		// TODO:  For the test instance we're dealing with here, I don't have the organization name.  
//  		// the question is if we'll have a  more fully build up project object when we actually run
//  		// the code from the app... 
//  		tltIndividual.addDisplayData("ORGANIZATION", project.getClient().getName());		
//  		tltIndividual.addDisplayData("CURRENT_LEVEL", project.getCustomFields().get(ProjectDTO.TRANSITION_CURRENT_LEVEL_KEY));
//  		tltIndividual.addDisplayData("TARGET_LEVEL", project.getCustomFields().get(ProjectDTO.TRANSITION_TARGET_LEVEL_KEY));
//  		//TODO: Put real data here -- if this is today's date, just do it in Groovy....
//  		tltIndividual.addDisplayData("DATE_STRING", "01-Jul-2055");
//  		
//		/*
//		 * GET SCORES out of V3
//		 */
//		for(ParticipationDTO participation : participant.getParticipations()) {
//			for(ResultDTO result : participation.getResults()) {				
//				//System.out.println("----------> " +result.getModule().getModuleName());
//				//Build a new repot data for each instrument
//				ReportData reportData = new ReportData();
//				reportData.addDisplayData("INSTRUMENT_NAME", result.getModule().getModuleName());
//				
//				for(ScaleScoreDTO scaleScore : result.getScaleScores()) {
//					//add scale score
//					reportData.addScoreData(scaleScore.getId(), scaleScore.getValue());
//					//reportData.addRawData("Q1", "3");
//				}
//				//add entire instrument to the report data block
//				//GIVE THE MODULE HASHMAP VALUE THE MODULE NAME
//				tltIndividual.getReportData().put(result.getModule().getModuleName(), reportData);
//			}
//		}
//		
//		//Add report(s) to request
//		reportRequest.addReport(tltIndividual);
//		
//		//Get response
//		ReportingResponse reportResponse = reportRequest.generateReports();
//		OutputStream outputStream = new FileOutputStream ("TestTLTGroupIndividual.pdf");
//		reportResponse.toFile().writeTo(outputStream);
//
//	  UnitTestUtils.stop(this);
//  }
}