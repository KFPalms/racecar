/*
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.listener.portal.groovy 

import com.pdi.data.abyd.dto.common.KeyValuePair;
import com.pdi.data.abyd.dto.intGrid.IGSummaryDataDTO;


import com.pdi.data.abyd.dto.setup.ReportModelCompetencyDTO;
import com.pdi.data.abyd.dto.setup.ReportModelSuperFactorDTO;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.GroupReport;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;
import com.pdi.reporting.request.ReportingRequest;
import com.pdi.reporting.response.ReportingResponse;
import com.pdi.data.abyd.dto.reportInput.ReportInputDTO;
import com.pdi.data.abyd.dto.setup.ReportModelStructureDTO;
import com.pdi.data.abyd.dto.setup.SPFullDataDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportLabelValueDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportLabelsDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportOptionValueDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportOptionsDTO;
import com.pdi.data.abyd.helpers.common.ImportExportDataHelper;
import com.pdi.data.abyd.helpers.intGrid.IGSummaryDataHelper;
import com.pdi.data.abyd.helpers.reportInput.ReportInputDataHelper;
import com.pdi.data.abyd.helpers.setup.ReportLabelsDataHelper;
import com.pdi.data.abyd.helpers.setup.ReportModelDataHelper;
import com.pdi.data.abyd.helpers.setup.ReportOptionsDataHelper;
import com.pdi.data.abyd.helpers.setup.SPDataHelper;
import com.pdi.data.v2.dto.ParticipantDTO;
import com.pdi.data.v2.dto.SessionUserDTO;
import com.pdi.data.v2.helpers.ParticipantDataHelper;
import com.pdi.properties.PropertyLoader;
import com.pdi.scoring.Norm;
import com.pdi.scoring.NormSet;
import com.pdi.string.StringUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.xml.XMLUtils;

public class ABYD_INTEGRATION_GRID_REPORT
{ 
	private ReportingRequest reportRequest;
	private Node reportNode;
	private SessionUserDTO sessionUser;


	/*
	 * generate
	 */
	public void generate()
	{
		String reportCode = reportNode.getAttributes().getNamedItem("code").getTextContent();
		//System.out.println(reportCode);
		NodeList participants = XMLUtils.getElements(reportNode, "Participants/Participant");
		
		for( int j= 0; j < participants.getLength(); j++)
		{
			Node participant = participants.item(j);
			String participantId = participant.getAttributes().getNamedItem("participantId").getTextContent();
			String projectId = participant.getAttributes().getNamedItem("projectId").getTextContent();
			//System.out.println(participantId + " vs " + projectId);

			Connection con = null;
			try
			{
				//Got all of the basic data, time to add abyd data
				con = AbyDDatabaseUtils.getDBConnection();
				
				long dnaId = new ImportExportDataHelper().getDnaFromV2(con, projectId, participantId);
				
				SPDataHelper helper = new SPDataHelper(con, dnaId);        
				SPFullDataDTO spFullData = helper.getAllSPData();				
				
				
				ReportModelDataHelper reportModelDataHelper = new ReportModelDataHelper();
				ReportOptionsDataHelper reportOptionsDataHelper = new ReportOptionsDataHelper();
				ReportLabelsDataHelper reportLabelsDataHelper = new ReportLabelsDataHelper();
				ReportInputDataHelper reportInputDataHelper = new ReportInputDataHelper();
				
				ReportModelStructureDTO reportModelStructure = reportModelDataHelper.getReportModelWithScores(con, dnaId, participantId);
				Setup2ReportOptionsDTO options = reportOptionsDataHelper.getReportOptionData(con, dnaId);
				Setup2ReportLabelsDTO labels = reportLabelsDataHelper.getReportLabelData(dnaId, ReportLabelsDataHelper.ALL_LABELS);
				ReportInputDTO reportInput = reportInputDataHelper.getReportInput(participantId, dnaId);
				
				IndividualReport ir = reportInput.getCurrentTLT();
				if(ir == null)
				{
					continue;
				}
				ir = new ParticipantDataHelper().populateIndividualReportData(sessionUser, participantId, projectId, ir);
				ir.getDisplayData().put("CLIENT_LOGO", projectInformation.getProject().getClient().getId()+".png");
				ir.setReportCode(reportCode);
				ir.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);
				
			
				String fileName = StringUtils.removeSpecialCharacters(
						reportInput.getParticipantNameInverse() + " " +
						reportInput.getClientName() + " " + 
						reportInput.getDnaName() + " " +
						new SimpleDateFormat("dd MMMM yyyy").format(new java.util.Date()) + " ");
				ir.setName(fileName.toUpperCase() + reportCode + ".pdf");
				
				// Override project name with DNA name
				////println "PROJECT_NAME (B4)=" + ir.getDisplayData().get("PROJECT_NAME");
				ir.getDisplayData().put("PROJECT_NAME", reportInput.getDnaName());	// Use the DNA name as the project name on A by D data
				////println "reportInput: proj=" + reportInput.getProjectName() + ", dna=" + reportInput.getDnaName();
				////println "PROJECT_NAME=" + ir.getDisplayData().get("PROJECT_NAME");
								
				reportRequest.addReport(ir);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				// close the connection
				if (con != null)
				{
					try  {  con.close();  }
					catch (Exception ex)
					{
					}
					con = null;
				}
			}
		}
	}
}