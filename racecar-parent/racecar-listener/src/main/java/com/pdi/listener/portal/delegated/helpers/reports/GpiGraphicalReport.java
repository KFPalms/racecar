/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.listener.portal.delegated.helpers.reports;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.pdi.data.dto.NormScore;
import com.pdi.data.dto.ProjectParticipantInstrument;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.Report;
import com.pdi.reporting.report.ReportData;
import com.pdi.reporting.report.utils.BWUtils;
import com.pdi.reporting.report.vm.GPI_SCORING;
import com.pdi.string.StringUtils;
import com.pdi.xml.XMLUtils;


/*
 * GpiGraphicalReport - class to hold methods relevant to the GPI graphical report
 * 
 * NOTE:  EVEN THOUGH LOCATED UNDER THE DELEGATED FOLDER, THERE ARE NO METHODS IN THIS
 *        CLASS THAT ARE DEFINED IN THE DATA INTERFACE.
 *        
 * This class is located here because the folder structure (delegated/helpers) is
 * inverted from normal practice elsewhere in this project stack (helpers/delegated).
 * Putting this helper class here avoided confusion with having two folders in the
 * portal structure that had the name "helper".
 */
public class GpiGraphicalReport
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	
	//
	// Constructors.
	//
    public GpiGraphicalReport()
    {
    	// No logic here at the present time
    }

	//
	// Instance methods.
	//


	/**
	 * getGpiDetailData - Method that retrieves the requisite data for the GPI graphical report
	 * 
	 * @param inp - An IndividualReport object
	 * @param epis - The ParojectParticipantReport objects associated with the request
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<Report> getGpiGraphicalData(IndividualReport inp,  
			ArrayList<ProjectParticipantInstrument> epis )
	{
		//System.out.println("doGPIGraphicalRpt...");
		
		ArrayList<Report> rpts = new ArrayList<Report>();
		try
		{
			for(ProjectParticipantInstrument epi : epis)
			{
				// spin through until you find the gpil
				if(!epi.getInstrument().getCode().equals("gpil")){
					continue;
				}
				
   	   			IndividualReport ret = inp.clone();	// get the input data

				/*
				 * ========================================
				 * ADD BASIC PARTICIPANT DATA TO REPORT 
				 */
				ret.addDisplayData("FIRST_NAME", epi.getParticipant().getFirstName());
				ret.addDisplayData("LAST_NAME", epi.getParticipant().getLastName());
				ret.getDisplayData().put("PARTICIPANT_NAME", epi.getParticipant().getFirstName() + " " + epi.getParticipant().getLastName());			
				ret.addDisplayData("ORGANIZATION", XMLUtils.xmlEscapeString(epi.getProject().getClient().getName()));
		  		ret.addDisplayData("PROJECT_NAME", XMLUtils.xmlEscapeString(epi.getProject().getName()));	
		  		
		  		ReportData rd = new ReportData();
		  		rd.getDisplayData().put("ADMIN_DATE", epi.getInstrumentTakenDate().substring(0, 10));
		  		rd.getScoreData().put("GPI_SP_NAME", epi.getScoreGroup().getSpecialPopulation().getName()); // SPEC-POP NORM					
		  		rd.getScoreData().put("GPI_GP_NAME", epi.getScoreGroup().getGeneralPopulation().getName() ); //GEN-POP NORM

				String fileName = StringUtils.removeFilenameSpecialCharacters(
						epis.get(0).getParticipant().getLastName() + " " +
						epis.get(0).getParticipant().getFirstName() + " " +
						epis.get(0).getProject().getProjectTypeName()+" "+
						inp.getReportCode()+" "+
						new SimpleDateFormat("yyyyMMdd").format(new Date()) + " " 
				);
						
				ret.setName(fileName.replace(" ", "_") + ".pdf");
				
				// 2 ways to test for multibyte characters
//				if (fileName.length() != fileName.getBytes("UTF-8").length)
//				{
//					// we have multibyte characters
//				}
//				for (int i=0; i < fileName.length(); i++)
//				{
//					if (fileName.charAt(i) > 255)
//					{
//						// we have an invalid filename character
//						break;
//					}
//				}

				// from gpi.groovy : public IndividualReport generate()
				
				//Gpi not complete
				if(epi.getScoreGroup().getScores().isEmpty())
				{
					//System.out.println("GPI IS NULL!");
					// They haven't finished the GPI
					ret.getScriptedData().put("STATUS", "NOT_FINISHED");
					ret.getScriptedData().put("ERROR", "GPI has not been completed, unable to generate report");
					ret.getReportData().put(ReportingConstants.RC_GPI, rd);				
					rpts.add(ret);
					continue;
				} else {
					ret.getScriptedData().put("STATUS", "");
					ret.getScriptedData().put("ERROR", "");
				}

				// Calc the reference line positions and put them out
				float[] ref = BWUtils.getRefPosArray();
				rd.getScriptedData().put("REF_1", new Float(ref[0]).toString());
				rd.getScriptedData().put("REF_3", new Float(ref[1]).toString());
				rd.getScriptedData().put("REF_16", new Float(ref[2]).toString());
				rd.getScriptedData().put("REF_50", new Float(ref[3]).toString());
				rd.getScriptedData().put("REF_84", new Float(ref[4]).toString());
				rd.getScriptedData().put("REF_97", new Float(ref[5]).toString());
				rd.getScriptedData().put("REF_99", new Float(ref[6]).toString());

				// get the normed score objects for the dimensions....
				HashMap<String, NormScore> gpPercentiles = epi.getScoreGroup().calculateGeneralPopulation();
				HashMap<String, NormScore> spPercentiles = epi.getScoreGroup().calculateSpecialPopulation();

				if(epi.getScoreGroup().getScores().get("PGPI_RDI") != null)
				{
					//System.out.println("pi.getScoreGroup().getScores().get(PGPI_RDI).getRawScore() " + epi.getScoreGroup().getScores().get("PGPI_RDI").getRawScore());
					String rdi = epi.getScoreGroup().getScores().get("PGPI_RDI").getRawScore();
					float rdiFloat = Float.parseFloat(rdi); // convert string to float
					DecimalFormat rdiRound = new DecimalFormat("0");
					//System.out.println(grade.format(rdiFloat)); // round to nearest whole number
					rd.addScoreData("PGPI_RDI", rdiRound.format(rdiFloat));
				} else {
					rd.addScoreData("PGPI_RDI", "");
				}

				// Set up the dimension data
				rd.getDisplayData().put("DIM_COUNT", new Integer(GPI_SCORING.DIMENSIONS.length).toString());	
				for (int i=0; i < GPI_SCORING.DIMENSIONS.length; i++)
				{
					rd.getDisplayData().put("DIM" + (i+1) + "_NAME", GPI_SCORING.DIMENSIONS[i][0].toString());
					ArrayList<String> ary = (ArrayList<String>) GPI_SCORING.DIMENSIONS[i][1];
					rd.getDisplayData().put("DIM" + (i+1) + "_SCALE_COUNT", new Integer(ary.size()).toString());	

					// Do the scale data
					for (int j=0; j < ary.size(); j++)
					{
						String scaleId = ( (ArrayList<String>)GPI_SCORING.DIMENSIONS[i][1]).get(j);

						if(epi.getScoreGroup().getScores().get(scaleId).getRawScore()!= null)
						{
							rd.getDisplayData().put("DIM" + (i+1) + "_SCALE" + (j+1) + "_ID", scaleId);
							rd.getDisplayData().put(scaleId  + "_SCALE_NAME", epi.getScoreGroup().getScores().get(scaleId).getScaleName());
							
							// Round the percentiles		 rd.getScoreData(scaleId + "_GP_PCTL")
							double pctl =  gpPercentiles.get(scaleId).toPercentile();
							int pOut = new Double(Math.floor(pctl + 0.5)).intValue();
							if (pOut < 1.0)
								pOut = 1;
							else if (pOut > 99.0) 
								pOut = 99;
							rd.getScriptedData().put(scaleId+"_GP_PCTL", new Integer(pOut).toString());
							ret.addScriptedData(scaleId+"_GP_PCTL", new Integer(pOut).toString());
							//System.out.println(scaleId+"_GP_PCTL : " + new Integer(pOut).toString() +  "  :  " + pctl);

							pctl = spPercentiles.get(scaleId).toPercentile();
							pOut = new Double(Math.floor(pctl + 0.5)).intValue();
							if (pOut < 1.0)
								pOut = 1;
							else if (pOut > 99.0)
								pOut = 99;
							rd.getScriptedData().put(scaleId + "_SP_PCTL", new Integer(pOut).toString());
							ret.addScriptedData(scaleId+"_SP_PCTL", new Integer(pOut).toString());
							// old school:  Get the norms 
							// but we don't need to do this because we have them already.
							// however.... 							
							// ... we have to
							// convert the com.pdi.data.Norm to a com.pdi.scoring.Norm 
							// because BWUtils can't see the pdi.data Norm object, and if you 
							// add the pdi-data project to pdi-util-reporting the whole system all of a sudden
							// sees two Norm objects, and blows the whole thing up.
							com.pdi.scoring.Norm gpNorm = new com.pdi.scoring.Norm(epi.getScoreGroup().getGeneralPopulation().getNorms().get(scaleId).getMean(),
																				   epi.getScoreGroup().getGeneralPopulation().getNorms().get(scaleId).getStandardDeviation());

							com.pdi.scoring.Norm spNorm = new com.pdi.scoring.Norm(epi.getScoreGroup().getSpecialPopulation().getNorms().get(scaleId).getMean(),
									   epi.getScoreGroup().getSpecialPopulation().getNorms().get(scaleId).getStandardDeviation());

							// Calc the score position
							double score = epi.getScoreGroup().getScores().get(scaleId).scoreAsNumber() ; // Double.parseDouble(rd.getScoreData().get(scaleId));
							float scorPos = BWUtils.calcScorePos(score, gpNorm); 
							rd.getScriptedData().put(scaleId + "_POSN", new Float(scorPos).toString());

							// Calc the cut-point positions
							float[] csPos = BWUtils.getCutPosArray(gpNorm,spNorm); 
							//System.out.println("Cut Points for " + scaleId + ":");
							//for(int r= 0; r < csPos.length; r++)
							//{
							//	System.out.println("Idx[" + r + "]=" + csPos[r])
							//}
							rd.getScriptedData().put(scaleId + "_CP_3", new Float(csPos[0]).toString());
							rd.getScriptedData().put(scaleId + "_CP_11", new Float(csPos[1]).toString());
							rd.getScriptedData().put(scaleId + "_CP_23", new Float(csPos[2]).toString());
							rd.getScriptedData().put(scaleId + "_CP_40", new Float(csPos[3]).toString());
							rd.getScriptedData().put(scaleId + "_CP_60", new Float(csPos[4]).toString());
							rd.getScriptedData().put(scaleId + "_CP_77", new Float(csPos[5]).toString());
							rd.getScriptedData().put(scaleId + "_CP_89", new Float(csPos[6]).toString());
							rd.getScriptedData().put(scaleId + "_CP_97", new Float(csPos[7]).toString());
						} else {
							ret.getScriptedData().put("STATUS", "NOT_FINISHED");
							ret.getScriptedData().put("ERROR", "GPI has not been completed, unable to generate report");
							break;
						}	// End of if rawScore != null
					}	// End of ary.size() loop
				}	// End of GPI_SCORING.DIMENSIONS loop

				ret.getReportData().put(ReportingConstants.RC_GPI, rd);	
				rpts.add(ret);
			} // end main for-loop

			return rpts;
		}
		catch(Exception e)
		{
			System.out.println("doGPIGraphicalRpt:");
			e.printStackTrace();
			return null;
		}
	}
}
