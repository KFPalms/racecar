/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.listener.portal.servlets;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.net.URLEncoder;
//import java.security.Principal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.kf.palms.web.rest.security.ReportAPIHelper;
import com.kf.palms.web.rest.security.ReportDownloadResponse;
import com.kf.palms.web.util.JSONUtils;
import com.pdi.data.abyd.dto.common.DNACellDTO;
import com.pdi.data.abyd.dto.reportInput.ReportInputDTO;
import com.pdi.data.abyd.dto.setup.DNADescription;
import com.pdi.data.abyd.dto.setup.EntryStateDTO;
import com.pdi.data.abyd.dto.setup.ReportModelStructureDTO;
import com.pdi.data.abyd.dto.setup.ReportNoteDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportLabelsDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportOptionsDTO;
import com.pdi.data.abyd.helpers.common.Rv2Helper;
import com.pdi.data.abyd.helpers.reportInput.ReportInputDataHelper;
import com.pdi.data.abyd.helpers.reportInput.ResearchExtractHelper;
import com.pdi.data.abyd.helpers.setup.DNAStructureHelper;
import com.pdi.data.abyd.helpers.setup.EntryStateHelper;
import com.pdi.data.abyd.helpers.setup.ReportLabelsDataHelper;
import com.pdi.data.abyd.helpers.setup.ReportModelDataHelper;
import com.pdi.data.abyd.helpers.setup.ReportNoteDataHelper;
import com.pdi.data.abyd.helpers.setup.ReportOptionsDataHelper;
import com.pdi.data.abyd.helpers.setup.SaveDNADescHelper;
import com.pdi.data.abyd.helpers.setup.SaveDNAStructureHelper;
import com.pdi.data.abyd.helpers.setup.SetupConstants;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.data.abyd.util.Utils;
import com.pdi.data.dto.CourseNormsDTO;
import com.pdi.data.dto.Instrument;
import com.pdi.data.dto.Norm;
import com.pdi.data.dto.NormGroup;
import com.pdi.data.dto.NormScore;
import com.pdi.data.dto.Participant;
import com.pdi.data.dto.Project;
import com.pdi.data.dto.ProjectParticipantInstrument;
import com.pdi.data.dto.ScoreGroup;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.IInstrumentHelper;
import com.pdi.data.helpers.interfaces.INormGroupHelper;
import com.pdi.data.helpers.interfaces.IProjectParticipantInstrumentHelper;
import com.pdi.data.helpers.interfaces.IReportHelper;
import com.pdi.data.helpers.interfaces.IScoringHelper;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.util.RequestStatus;
import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.listener.portal.helpers.ExtractRegenHelper;
import com.pdi.listener.portal.helpers.PortalServletLockHelper;
import com.pdi.logging.LogWriter;
import com.pdi.properties.PropertyLoader;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.ExtractItem;
import com.pdi.reporting.report.ExtractParams;
import com.pdi.reporting.report.GroupReport;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.Report;
import com.pdi.reporting.report.ReportDataException;
import com.pdi.reporting.report.helpers.ALRExtractFormatHelper;
import com.pdi.reporting.report.helpers.AbyDExtractFormatHelper;
import com.pdi.reporting.report.helpers.AbyDRawExtractFormatHelper;
import com.pdi.reporting.report.helpers.AlpRawExtractFormatHelper;
import com.pdi.reporting.request.ReportingRequest;
import com.pdi.reporting.response.ReportingResponse;
import com.pdi.webservice.NhnWebserviceUtils;
import com.pdi.xml.XMLUtils;

import edu.emory.mathcs.backport.java.util.Arrays;

/**
 * PortalServlet takes requests directly from the web (http get or post) and
 * formats them as needed to work with the ReportingListener class
 *
 * PortalServlet is configured in pdi-web-reporting/web-inf/web.xml
 *
 *
 * @author Gavin Myers
 */

public class PortalServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	private static final Logger log = LoggerFactory.getLogger(PortalServlet.class);

	//
	// Static data.
	//
	private static final String DEFAULT_LANGUAGE_CODE = "en";

	private static int IG_AGE = 14;

	// private static String PPT_PARAM_STR =
	// "?extParticipantId={pptId}&extInstrumentId=iblva&type=participantReview";
	// // Not needed (yet)
	private static String LVA_CONS_PARAM_STR = "?extParticipantId={pptId}&extInstrumentId=iblva&type=consultantReview";
	// private static String IBX_CONS_PARAM_STR_1 =
	// "?extParticipantId={pptId}&extInstrumentId=";
	// private static String IBX_CONS_PARAM_STR_2 =
	// "&type=inboxConsultantReview";
	private static String IBX_CONS_PARAM_STR = "?extParticipantId={pptId}&extInstrumentId={inst}&type=inboxConsultantReview";

	// List of normable courses in GL_GPS
	private static HashSet<String> glGpsInstSet = new HashSet<String>();
	{
		glGpsInstSet.add("gpi");
		glGpsInstSet.add("gpil");
		glGpsInstSet.add("rv");
	}

	// private static String TDLA_RPT_TYPE = "TDLAReport";
	// private static String TD_RPT_TYPE = "TDReport";
	// private static String LA_RPT_TYPE = "LAReport";

	//
	// Static methods.
	//

	//
	// Instance data.
	//

	// Data for cloning
	private long cloneSrcProjId = 0;
	private long cloneDestProjId = 0;
	private long sourceDnaId = 0;
	private long cloneDnaId = 0;

	//
	// Constructors.
	//
	public PortalServlet() {
	}

	//
	// Instance methods.
	//

	/*
	 * This should be able to handle multiple report requests... the best way
	 * would probably be by sending an xml file, similar to this
	 * PortalServlet?xml= <Reports> <ReportRequest code="TLT_INDIVIDUAL_REPORT"
	 * langCode="en"> <Overrides> <Override key="TARGET_LEVEL" value="MLL"/>
	 * </Overrides> <Participants> <Participant participantId="FSINVG"
	 * projectId="SFHGVK /> </Participants> </ReportRequest> <ReportRequest
	 * code="TLT_INDIVIDUAL_REPORT" langCode="fr"> <Participants> <Participant
	 * participantId="FSINVG" projectId="FSGHSD /> </Participants>
	 * </ReportRequest> <ReportRequest code="TLT_INDIVIDUAL_REPORT">
	 * <Participants> <Participant participantId="FSINVG" projectId="SGHDFHJ />
	 * </Participants> </ReportRequest> <ReportRequest code="TLT_GROUP_REPORT"
	 * langCode="es"> <Participants>
	 *
	 * <Participant participantId="SHHFJ" projectId="SFHGVK />
	 *
	 * <Participant participantId="DSHD5SF" projectId="SFHGVK />
	 *
	 * <Participant participantId="SGHJD2" projectId="SGHSF />
	 *
	 * <Participant participantId="DHDHFD" projectId="SFHGVK />
	 *
	 * <Participant participantId="FEWHBS3" projectId="SGSHDF /> </Participants>
	 * </ReportRequest> </Reports>
	 */
	/*
	 * We also now have a <FetchScores> tag, formatted similarly: <?xml
	 * version="1.0" encoding="UTF-8" standalone="yes"?> <FetchScores>
	 * <FetchRequest code = "GPS_INST" normset = "GPS_MLL"> <Participants>
	 * <Participant participantId = "367018" projectId = "491"/> </Participants>
	 * </FetchRequest> </FetchScores>
	 */

	/*
	 * And don't forget about the nor list request and the default norm setter
	 * either
	 */
	/*
	 * Default norm setter request - maintains some of the characteristics of
	 * the norm list <SetProjectNormDefaults project_id = "557"> <course abbv =
	 * "gpil"> <normGroupType type = "genPop"> <normGroup id = "16"/>
	 * </normGroupType> <normGroupType type = "specPop"> <normGroup id = "17"/>
	 * </normGroupType> </course> <course abbv = "rv"> <normGroupType type =
	 * "genPop"> <normGroup id = "39"/> </normGroupType> <normGroupType type =
	 * "specPop"> <normGroup id = "70"/> </normGroupType> </course>
	 * </SetProjectNormDefaults>
	 */

	/*
	 * // Get the norms for a ppt <SelectedNormsList> <project id="557"/>
	 * <participant id="345678"/> </SelectedNormsList>
	 */

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
		Date date = new Date();

		System.out.println("POST! - " + sdf.format(date));

		// declaring these here so that they can be used in log message in catch
		String instrumentId = "";
		String participantId = "";
		String dnaId = "";
		String reportCode = "";
		String langCode = null; // Refreshes to default language at top of the
								// ReportRequest loop
		int sortId = 0;
		boolean showName = true;
		boolean showLci = true;
		String axisLabels = null;

		// SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
		String xmlOutput = null; // TLT_XML_OUTPUT

		if (request.getParameter("xml") == null) {
			// There is no xml parameter in the request... This is scoring.
			// System.out.println("Time to score");
			SessionUser session = new SessionUser();
			instrumentId = request.getParameter("instrument_id");
			participantId = request.getParameter("user_id");
			// System.out.println("user_id " + participantId +
			// " instrument_id " + instrumentId);
			// System.out.println("timestamp: " + sdf.format( new Date() ) +
			// " userId: " + participantId + " for intrument id: " +
			// instrumentId);
			// String projectId = request.getParameter("project_id");

			log.debug("Scoring:  inst={}, ppt={}", instrumentId, participantId);
			IProjectParticipantInstrumentHelper ipphv2 = null;
			IProjectParticipantInstrumentHelper ipphnhn = null;
			IInstrumentHelper inv2 = null;
			Instrument i = null;
			IScoringHelper isc = null;
			try {
				ipphv2 = HelperDelegate.getProjectParticipantInstrumentHelper(
						PropertyLoader.getProperty("com.pdi.data.application", "datasource.v2"));
				ipphnhn = HelperDelegate.getProjectParticipantInstrumentHelper(
						PropertyLoader.getProperty("com.pdi.data.application", "datasource.nhn"));
				inv2 = HelperDelegate
						.getInstrumentHelper(PropertyLoader.getProperty("com.pdi.data.application", "datasource.v2"));
				i = inv2.fromId(session, instrumentId);
				isc = HelperDelegate.getScoringHelper();
			} catch (Exception e) {
				log.error("PortalServlet scoring ERROR: instrumentId={}, participantId={}, message={}", instrumentId,participantId,e.getMessage());
				e.printStackTrace();
				return;
			}

			// Ultimately this comes from the nh database (ScoreGroup has
			// responses from scorm)
			ProjectParticipantInstrument ppi = ipphnhn.fromLastParticipantIdInstrumentId(session, participantId,
					instrumentId);
			if (ppi != null) {
				ScoreGroup sg = isc.score(i, ppi.getResponseGroup());

				ppi.setScoreGroup(sg);
				ipphv2.addUpdate(session, IProjectParticipantInstrumentHelper.MODE_SCORE, ppi, false);

				// NHN 1510 -- if it's a cs, also put results in cs2; if it's a
				// cs2, also put results in cs;
				// because this is what they're doing on the nhn side
				if (ppi.getInstrument().getCode().equals("cs")) {
					// System.out.println("did a cs, so also putting results in
					// cs2");
					Instrument inst2 = inv2.fromId(session, "cs2");
					ppi.setInstrument(inst2);
					ipphv2.addUpdate(session, IProjectParticipantInstrumentHelper.MODE_SCORE, ppi, false);
				} else if (ppi.getInstrument().getCode().equals("cs2")) {
					// System.out.println("did a cs2, so also putting results in
					// cs");
					Instrument inst2 = inv2.fromId(session, "cs");
					ppi.setInstrument(inst2);
					ipphv2.addUpdate(session, IProjectParticipantInstrumentHelper.MODE_SCORE, ppi, false);
				}
			} else {
				log.error("Null ppi data, cannot run scoring");
			}

			return;
		} // End scoring

		// We have XML... Prepare to extract data
		log.debug("incoming parameter xml: {}", request.getParameter("xml"));

		// build the Doc object
		DocumentBuilderFactory dbfac = null;
		DocumentBuilder docBuilder = null;
		Document doc = null;
		String xml = request.getParameter("xml");
		try {
			dbfac = DocumentBuilderFactory.newInstance();
			docBuilder = dbfac.newDocumentBuilder();
			doc = docBuilder.parse(new InputSource(new StringReader(xml)));
		} catch (ParserConfigurationException pce) {
			log.error("PortalServlet report XML parser config ERROR: instrumentId={}, xml={}, message: {}", instrumentId, xml,
							pce.getMessage());
			return;
		} catch (SAXException se) {
			log.error("PortalServlet report XML SAX ERROR: instrumentId={},  xml = {}, message: {}",instrumentId , xml, se.getMessage());
			return;
		}
		//

		// What request type is this?
		Element root = doc.getDocumentElement();
		NodeList principalList = root.getElementsByTagName("principal");
		String principal = "";
		if (principalList != null){
			if (principalList.getLength() == 1){
				principal = principalList.item(0).getTextContent();
				log.debug("Principal = {}", principal);
			} else {
				log.debug("Principal list size: {}", principalList.getLength());
			}
		} else {
			log.debug("Principal List was Null");
		}
		String str = root.getTagName();

		// Is it an A by D normed score fetch
		if (str.equals("FetchScores")) {
			// Process a score fetch request
			log.debug("Processing A by D score fetch...");
			processScoreRequest(doc, response);

			return;
		}

		// Is it a norm list fetch?
		if (str.equals("CourseNormsRequest")) {
			// Process a norm list request
			log.debug("Processing norm list fetch...");
			processNormListRequest(doc, response);

			return;
		}

		// Is it a fetch selected norms request?
		if (str.equals("FetchSelectedNormsList")) {
			// Process a norm list request
			log.debug("Processing fetch selected norms...");
			processFetchSelectedNormsList(doc, response);

			return;
		}

		// Is it a set norm defaults update?
		if (str.equals("SetProjectNormDefaults")) {
			// Process a norm list request
			log.debug("Processing set norm defaults...");
			processSetNormDefaultsRequest(doc, response);

			return;
		}

		// Is it a set norms request?
		if (str.equals("SetParticipantNorms")) {
			// Process the set norms request
			log.debug("Processing fetch selected norms...");
			processSetParticipantNorms(doc, response);

			return;
		}

		// Is it a request to generate extract candidate rows?
		if (str.equals("ForceAbyDResearchExtractCandidates")) {
			// Process the set norms request
			log.debug("Processing force extract candidtates...");
			processForceExtractCand(doc, response);

			return;
		}
		// Is it an Report Label Lookup?
		if (str.equals("ReportLabelLookup")) {
			// Process a label lookup request
			log.debug("Processing ReportLabelLookup...");
			reportLabelLookup(doc, response);

			return;
		}
		// Is it a Clone request?
		if (str.equals("CloneDna")) {
			// Process a clone request
			log.debug("Processing CloneDna...");
			cloneDna(doc, response);

			return;
		}
		// Is it an sort info request?
		if (str.equals("FetchSortInfo")) {
			// Process a sort info fetch request
			log.debug("Processing SortInfo...");
			fetchSortInfo(doc, response);

			return;
		}
		// Is it a extract candidate regen request?
		if (str.equals("ExtractRegen")) {
			// Process a regen request
			ExtractRegenHelper er = new ExtractRegenHelper();
			log.debug("Processing ExtractRegen...");
			er.regen(doc, response);

			return;
		}
		// Is it a lock status request?
		if (str.equals("FetchIgDriLocks")) {
			// Process a status fetch request
			PortalServletLockHelper sh = new PortalServletLockHelper();
			log.debug("Processing FetchIgDriLocks...");
			sh.fetchIgDriLockInfo(doc, response);

			return;
		}
		// Is it an unlock request?
		if (str.equals("IgDriUnlock")) {
			// Process an unlock request
			PortalServletLockHelper sh = new PortalServletLockHelper();
			log.debug("Processing IgDriUnlocks...");
			sh.unlockIgDri(doc, response);

			return;
		}
		// Is it a lock search request?
		if (str.equals("FetchUnlocks")) {
			// Process a lock search request
			PortalServletLockHelper sh = new PortalServletLockHelper();
			log.debug("Processing FetchLocks...");
			sh.unlocksFetch(doc, response);

			return;
		}
		// None of the above... Fetch a report.
		// Header should be "Reports"
		if (!str.equals("Reports")) {
			log.error("Unknown Root code {}.  Nothing done...", str);
			return;
		}

		// Generate report: The critical function that takes the report data and
		// builds a report
		// Uses the .groovy script and velocity template in util reporting to
		// generate the
		// desired output
		String extRptProjId = null;
		try {
			IReportHelper irh = HelperDelegate.getReportHelper();
			
			log.debug("Using Helper class: {}", irh.getClass().getName());

			SessionUser su = new SessionUser();

			NodeList nodes = XMLUtils.getElements(doc, "//ReportRequest");

			for (int i = 0; i < nodes.getLength(); i++) {
				HashMap<String, String> overrides = new HashMap<String, String>();
				langCode = DEFAULT_LANGUAGE_CODE; // Reset to default at top of
													// loop
				Node node = nodes.item(i);

				reportCode = node.getAttributes().getNamedItem("code").getTextContent();

				// mb - 10-2012 - adding xml data output for TLT
				if (node.getAttributes().getNamedItem("xmlOutput") != null) {
					xmlOutput = node.getAttributes().getNamedItem("xmlOutput").getTextContent();
					log.debug("yes, this is...  xmlOUtput={}", xmlOutput);
				}
				// ####### ADDED 9-20-2013 #####################
				// Set hybridReport (if it is set) -added for TLT Individual +
				// viaEDge Individual combination report
				// Default to NO
				String hybridReport = "NO";
				if (node.getAttributes().getNamedItem("hybridReport") != null
						&& !node.getAttributes().getNamedItem("hybridReport").getTextContent().equalsIgnoreCase("")) {
					hybridReport = node.getAttributes().getNamedItem("hybridReport").getTextContent();
				}
				// System.out.println("hybridReport! " + hybridReport);

				// #############################################
				// Set langCode if set
				if (node.getAttributes().getNamedItem("langCode") != null
						&& !node.getAttributes().getNamedItem("langCode").getTextContent().equalsIgnoreCase("")) {
					langCode = node.getAttributes().getNamedItem("langCode").getTextContent();
				}

				// Set sortId if present
				if (node.getAttributes().getNamedItem("sortId") != null
						&& !node.getAttributes().getNamedItem("sortId").getTextContent().equalsIgnoreCase("")) {
					String id = node.getAttributes().getNamedItem("sortId").getTextContent();
					if (id != null) {
						try {
							sortId = Integer.parseInt(id);
						} catch (NumberFormatException e) {
							sortId = 0;
							System.out.println("Invalid sort ID (" + id + ").  Default to 'No sort'.");
							e.printStackTrace();
						}
					}
				}

				// Set showName if present
				if (node.getAttributes().getNamedItem("showName") != null
						&& !node.getAttributes().getNamedItem("showName").getTextContent().equalsIgnoreCase("")) {
					String flg = node.getAttributes().getNamedItem("showName").getTextContent();
					if (flg != null) {
						showName = Boolean.parseBoolean(flg);
					}
				}

				// Set showLci if present
				if (node.getAttributes().getNamedItem("showLci") != null
						&& !node.getAttributes().getNamedItem("showLci").getTextContent().equalsIgnoreCase("")) {
					String flg = node.getAttributes().getNamedItem("showLci").getTextContent();
					if (flg != null) {
						showLci = Boolean.parseBoolean(flg);
					}
				}

				// Set axisLabels if present
				if (node.getAttributes().getNamedItem("axisLabels") != null
						&& !node.getAttributes().getNamedItem("axisLabels").getTextContent().equalsIgnoreCase("")) {
					axisLabels = node.getAttributes().getNamedItem("axisLabels").getTextContent();
				}

				// Check for overrides
				NodeList ovrs = XMLUtils.getElements(node, "Overrides/Override");
				if (ovrs != null) {
					for (int k = 0; k < ovrs.getLength(); k++) {
						Node ovr = ovrs.item(k);
						overrides.put(ovr.getAttributes().getNamedItem("key").getTextContent(),
								ovr.getAttributes().getNamedItem("value").getTextContent());
					}
				}

				// And process participants
				NodeList participants = XMLUtils.getElements(node, "Participants/Participant");

				if (reportCode.equals(ReportingConstants.REPORT_CODE_IBLVA_CONSULTANT_REVIEW)
						|| reportCode.equals(ReportingConstants.REPORT_CODE_NIS_CONSULTANT_REVIEW)
						|| reportCode.equals(ReportingConstants.REPORT_CODE_ISMLL_CONSULTANT_REVIEW)
						|| reportCode.equals(ReportingConstants.REPORT_CODE_ISBUL_CONSULTANT_REVIEW)
						|| reportCode.equals(ReportingConstants.REPORT_CODE_ISZ_CONSULTANT_REVIEW)
						|| reportCode.equals(ReportingConstants.REPORT_CODE_EIS_CONSULTANT_REVIEW)
						|| reportCode.equals(ReportingConstants.REPORT_CODE_BRE2_CONSULTANT_REVIEW)
						|| reportCode.equals(ReportingConstants.REPORT_CODE_BRESEA_CONSULTANT_REVIEW)
						|| reportCode.equals(ReportingConstants.REPORT_CODE_SFECEO_CONSULTANT_REVIEW)
						|| reportCode.equals(ReportingConstants.REPORT_CODE_BREMED_CONSULTANT_REVIEW)) {
					// Assumes that there is only one request and short-circuits
					// the involved node processing
					Node participantNode = participants.item(0);
					// Get the ppt ID
					participantId = participantNode.getAttributes().getNamedItem("participantId").getTextContent();
					// Do the call at the end
				}
				/*
					else if (reportCode.equals(ReportingConstants.REPORT_CODE_KF4D_TDLA) ||
							 reportCode.equals(ReportingConstants.REPORT_CODE_KF4D_TD) ||
							 reportCode.equals(ReportingConstants.REPORT_CODE_KF4D_LA))
				*/
				else if (Arrays.asList(ReportingConstants.PALMS_REPORT_CODE_LIST).contains(reportCode)) {
					Node participantNode = participants.item(0);
					participantId = participantNode.getAttributes().getNamedItem("participantId").getTextContent();
					extRptProjId = participantNode.getAttributes().getNamedItem("projectId").getTextContent();
				} else {
					for (int k = 0; k < participants.getLength(); k++) {
						// Overwrite lang code on participant level (if it is
						// set)

						// System.out.println("Time to get a participant");
						Node participantNode = participants.item(k);

						if (participantNode.getAttributes().getNamedItem("langCode") != null && !participantNode
								.getAttributes().getNamedItem("langCode").getTextContent().equalsIgnoreCase("")) {
							langCode = participantNode.getAttributes().getNamedItem("langCode").getTextContent();
						}
						String projectName = null;
						// System.out.println("projectName!x " + projectName);
						if (participantNode.getAttributes().getNamedItem("projectName") != null && !participantNode
								.getAttributes().getNamedItem("projectName").getTextContent().equalsIgnoreCase("")) {
							// System.out.println("projectName!0 " +
							// projectName);
							projectName = participantNode.getAttributes().getNamedItem("projectName").getTextContent();
							// System.out.println("projectName!00 " +
							// projectName);
						}
						// System.out.println("projectName!y " + projectName);

						// System.out.println("The lang code is.... " +
						// langCode);

						participantId = participantNode.getAttributes().getNamedItem("participantId").getTextContent();

						String projectId = null;

						if (reportCode.equals(ReportingConstants.REPORT_CODE_INT_GRID_SNAPSHOT)) {
							projectId = participantNode.getAttributes().getNamedItem("projectId").getTextContent();
							dnaId = getDnaIdFromProjectId(projectId);
							overrides.put("dnaId", dnaId);
						} else if (reportCode.equals(ReportingConstants.REPORT_CODE_EVAL_GUIDE)
								|| reportCode.equals(ReportingConstants.REPORT_CODE_INT_GRID_REVIEW)) {
							dnaId = participantNode.getAttributes().getNamedItem("dnaId").getTextContent();
							overrides.put("dnaId", dnaId);
							if (reportCode.equals(ReportingConstants.REPORT_CODE_EVAL_GUIDE)) {
								String moduleId = participantNode.getAttributes().getNamedItem("instrumentId")
										.getTextContent();
								overrides.put("moduleId", moduleId);
							}
							projectId = getProjectIdFromDna(dnaId);
						} else {
							projectId = participantNode.getAttributes().getNamedItem("projectId").getTextContent();
							// System.out.println("PS 212: " + participantId + "
							// " +
							// projectId);
						}

						// See if we are going to gen a Ravens Graphical report
						// on an rv2
						if (reportCode.equals(ReportingConstants.REPORT_CODE_RAV_B_GRAPHICAL)) {

							boolean hasRv2 = (new Utils()).isInstValid("rv2", participantId, projectId);
							if (hasRv2) {
								// Set flag in overrides
								overrides.put("hasRv2", "yes");
								// ...and persist the data, if needed
								Rv2Helper hlpr = new Rv2Helper();
								hlpr.captureRv2Scores(participantId, projectId);
							}
						}

						IProjectParticipantInstrumentHelper epih = HelperDelegate
								.getProjectParticipantInstrumentHelper("com.pdi.data.v2.helpers.delegated");

						// Assumes that this is for reports... no Manual
						// Enty/Test
						// Data usage here
						ArrayList<ProjectParticipantInstrument> epis = null;

						ArrayList<ProjectParticipantInstrument> tempPPIs = new ArrayList<ProjectParticipantInstrument>();

						Participant participant = null;
						boolean isPreviewReport = false;

						// check to see if this is a preview report, which is
						// handled differently...
						if ((participantId == null || participantId.equals(""))
								&& (projectId != null && projectId.length() > 0)) {
							isPreviewReport = true;
							// we have a null partipant with a valid project...
							// we need to pass an array list of ppi's to
							// addReport,
							// so
							// will build this array list with the fake project,
							// because
							// it's the best way i know to build a bunch of
							// epi's
							IProjectParticipantInstrumentHelper previewHelper = HelperDelegate
									.getProjectParticipantInstrumentHelper("com.pdi.data.nhn.helpers.delegated");
							epis = previewHelper.fromProjectId(su,
									IProjectParticipantInstrumentHelper.MODE_FULL_COMPLETED, PropertyLoader.getProperty(
											"com.pdi.listener.portal.application", "sampleReport.projectId"));
						} else {
							// do these the regular way...
							// note the change in signature -- explanation in
							// the
							// Interface -- nhn-3263
							log.debug("About to build ProjectParticipantInstrument data object array: Participant: {}, Project {}", participantId, projectId);
							epis = epih.fromLastParticipantId(su,
									IProjectParticipantInstrumentHelper.MODE_FULL_COMPLETED, participantId, projectId);
							participant = HelperDelegate.getParticipantHelper("com.pdi.data.nhn.helpers.delegated")
									.fromId(su, participantId);
						}

						Project project = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated")
								.fromId(su, projectId);
						// System.out.println("com.pdi.data.nhn.helpers.delegated...............
						// ");
						// get the epis from nhn to pick up chq/background info
						// which resides in the Responses on the nhn side,
						// rather
						// than the results in scoring/reporting
						ArrayList<ProjectParticipantInstrument> ppisNhn = new ArrayList<ProjectParticipantInstrument>();
						if (reportCode.equals(ReportingConstants.REPORT_CODE_CHQ_DETAIL)) {
							IProjectParticipantInstrumentHelper ppihNhn = HelperDelegate
									.getProjectParticipantInstrumentHelper("com.pdi.data.nhn.helpers.delegated");
							ppisNhn = ppihNhn.fromLastParticipantId(su,
									IProjectParticipantInstrumentHelper.MODE_FULL_COMPLETED, participantId, projectId);
						}

						// Step 1: Set it to the participant
						if (langCode == null || langCode.equalsIgnoreCase("")) {
							langCode = participant.getMetadata().get("lang_pref");
						}

						// Step 2: Set it to the client
						if (langCode == null || langCode.equalsIgnoreCase("")) {
							langCode = project.getClient().getMetadata().get("lang_pref");
						}

						// Step 3: Set it to system default
						if (langCode == null || langCode.equalsIgnoreCase("")) {
							langCode = DEFAULT_LANGUAGE_CODE;
						}
						// System.out.println("language code.. portal servlet...
						// " +
						// langCode);

						// if this is a chq,
						// and there is data, add that to the
						// epis list from nhn
						if (reportCode.equals(ReportingConstants.REPORT_CODE_CHQ_DETAIL)) {
							for (ProjectParticipantInstrument ppi : ppisNhn) {
								if (ppi.getInstrument().getCode().equals("chq")
										|| ppi.getInstrument().getCode().equals("demo")) {
									if (!ppi.getResponseGroup().getResponses().isEmpty()) {
										epis.add(ppi);
									}
								}
							}
						}

						// System.out.println("Time to populate data");
						ArrayList<ProjectParticipantInstrument> previewPPIs = new ArrayList<ProjectParticipantInstrument>();
						if (isPreviewReport) {
							// we need to set the participant's id
							// in the participant object to null, so that
							// the report helper knows this is a preview
							// report...
							// only need one participant
							// can't update a ppi while looping, so just
							// creating a
							// preview list
							String currEmail = "0000";
							// boolean firstGuy = false;
							for (ProjectParticipantInstrument ppi : epis) {
								if (currEmail.equals("0000")) {
									currEmail = ppi.getParticipant().getEmail();
								}

								// set all the current guy's stuff to id == 0;
								if (ppi.getParticipant().getEmail().equals(currEmail)) {
									ppi.getParticipant().setId("");
									ppi.setProject(project); // use the real
																// project
																// to get setup
																// choices, just
																// the
																// fake
																// participant
									previewPPIs.add(ppi); // add it to the
															// preview
															// list.
								}
							}
						} else if (reportCode.equals(ReportingConstants.REPORT_CODE_ASSESSMENT_EXTRACT)) {

							log.debug(" REPORT CODE ASSESSMENT EXTRACT.... ");

							/*
							 * This bit is added per (NHN-2241). Business needed for
							 * the Assessment Report to only report on instruments
							 * that are part of the Assessment project. The
							 * getInstrumentCodesForProjectId method was added
							 * specifically for this purpose.
							 */
							com.pdi.data.v2.helpers.delegated.ProjectHelper v2ph = new com.pdi.data.v2.helpers.delegated.ProjectHelper();
							ArrayList<String> courses = v2ph.getInstrumentCodesForProjectId(project.getId(),
									participantId);

							for (ProjectParticipantInstrument ppi : epis) {

								if (courses.contains(ppi.getInstrument().getCode())) {

									ProjectParticipantInstrument ppiTemp = new ProjectParticipantInstrument();
									ppiTemp = ppi;
									tempPPIs.add(ppiTemp);
								}
							}

							epis = tempPPIs;

							for (ProjectParticipantInstrument ppi : epis) {
								ppi.setParticipant(participant);
								ppi.setProject(project);
							}
						} else {
							// No preview... not an assessment extract... Do
							// what
							// we'd normally do...
							// replace project/participant with the complete nhn
							// data
							// System.out.println("PS 291: setting all ppi's in
							// the array to this: "
							// + participantId + " " + projectId);

							// If there are no instruments, we still want to gen
							// the
							// custom model report... and a few others
							// Fake up a ppi
							if ((epis == null || epis.size() == 0)
									&& (reportCode.equals(ReportingConstants.REPORT_CODE_ABYD_CUSTOM_MODEL_REPORT)
											|| reportCode.equals(ReportingConstants.REPORT_CODE_ABYD_FIT_REPORT)
											|| reportCode.equals(ReportingConstants.REPORT_CODE_ABYD_READINESS_REPORT)
											|| reportCode.equals(ReportingConstants.REPORT_CODE_ABYD_DEVELOPMENT_REPORT)
											|| reportCode.equals(ReportingConstants.REPORT_CODE_ABYD_CUSTOM_REPORT)
											|| reportCode.equals(ReportingConstants.REPORT_CODE_ALR_EXTRACT)
											|| reportCode.equals(ReportingConstants.REPORT_CODE_ALR_TRAITS)
											|| reportCode.equals(ReportingConstants.REPORT_CODE_ALR_DRIVERS)
											|| reportCode.equals(ReportingConstants.REPORT_CODE_KF4D_TRAITS)
											|| reportCode.equals(ReportingConstants.REPORT_CODE_KF4D_DRIVERS)
											|| reportCode.equals(ReportingConstants.REPORT_CODE_KF4D_RISK)
											|| reportCode.equals(ReportingConstants.REPORT_CODE_EVAL_GUIDE)
											|| reportCode.equals(ReportingConstants.REPORT_CODE_INT_GRID_REVIEW)
											|| reportCode.equals(ReportingConstants.REPORT_CODE_INT_GRID_SNAPSHOT))) {
								// Make up a bogus instrument so downstream
								// processing can work (needs a ppi)
								if (epis == null)
									epis = new ArrayList<ProjectParticipantInstrument>();
								Instrument ii = new Instrument();
								ii.setCode("bogus");
								ProjectParticipantInstrument ppi = new ProjectParticipantInstrument();
								ppi.setInstrument(ii);
								epis.add(ppi);
							}

							if (reportCode.equals(ReportingConstants.REPORT_CODE_KFALP_RAW_EXTRACT)) {
								// ensure that there is an entry for kfp, which
								// is not found in the usual way
								if (epis == null)
									epis = new ArrayList<ProjectParticipantInstrument>();
								Instrument ii = new Instrument();
								ii.setCode("kfp");
								ProjectParticipantInstrument ppi = new ProjectParticipantInstrument();
								ppi.setInstrument(ii);
								epis.add(ppi);
							}

							for (ProjectParticipantInstrument ppi : epis) {
								ppi.setParticipant(participant);
								ppi.setProject(project);
								// System.out
								// .println("project name.. line 594 portal
								// servlet "
								// + ppi.getProject().getName());
							}
						}

						// adding individual reports to the report helper...
						if (isPreviewReport) {
							irh.addReport(su, reportCode, langCode, previewPPIs, overrides, "NO", null);
						} else {
							log.debug("PS 360 ; calling add report... for participant  {}, proj={}, code={}", 
									participantId, projectName, reportCode);
									
							irh.addReport(su, reportCode, langCode, epis, overrides, hybridReport, projectName);
							//log.debug("Back from addReport.  Now there are {} reports...", irh.getReportingRequest().getReports().size());
							
							// ReportingRequest xx = irh.getReportingRequest();
							// if (xx == null)
							// {
							// System.out.println(" No ReportingRequest
							// returned...");
							// }
							// else if(xx.getReports() == null)
							// {
							// System.out.println(" No reports (null)...");
							// }
							// else
							// {
							// System.out.println(" Reports bucket; cnt=" +
							// xx.getReports().size());
							// }
						}

						if (isPreviewReport) {
							// we only need one participant...
							// so just kill this loop now...
							break;
						}
					} // participant loop
				}

				// ReportingRequest rr = irh.getReportingRequest();
				// System.out.println("Bottom of rpt loop; Rpt=" + reportCode +
				// ", rptCnt =" + ((rr.getReports() == null) ? "null" : "" +
				// rr.getReports().size()));
			} // End of node parsing for loop

			/*
			 * // note: old school, we would call the generate() in Reporting
			 * Request. // at line 217.... that's where the xls is created, old
			 * school. // It uses the ABYD_EXTRACT.html // and returns a
			 * ByteArrayOutputStream os that it puts into the report:
			 * report.setData(os)
			 *
			 * new school: check here for the ASSESSMENT_EXTRACT report type new
			 * new school: do this for the abyd extract as well.... and generate
			 * the POI xls file from here... bypassing the reporting Request
			 * stuff, and just doing the work and shoving it out... cuz I won't
			 * be using velocity in this case....
			 */

			// See if this is spreadsheet (POI) output
			if (reportCode.equals(ReportingConstants.REPORT_CODE_ASSESSMENT_EXTRACT)
					|| reportCode.equals(ReportingConstants.REPORT_CODE_ABYD_RAW_EXTRACT)
					|| reportCode.equals(ReportingConstants.REPORT_CODE_ABYD_EXTRACT)
					|| reportCode.equals(ReportingConstants.REPORT_CODE_ABYD_ANALYTICS_EXTRACT)
					|| reportCode.equals(ReportingConstants.REPORT_CODE_TLT_EXTRACT)
					|| reportCode.equals(ReportingConstants.REPORT_CODE_TLT_EXTRACT_RAW)
					|| reportCode.equals(ReportingConstants.REPORT_CODE_ALR_EXTRACT)
					|| reportCode.equals(ReportingConstants.REPORT_CODE_ALR_TRAITS)
					|| reportCode.equals(ReportingConstants.REPORT_CODE_ALR_DRIVERS)
					|| reportCode.equals(ReportingConstants.REPORT_CODE_KF4D_TRAITS)
					|| reportCode.equals(ReportingConstants.REPORT_CODE_KF4D_DRIVERS)
					|| reportCode.equals(ReportingConstants.REPORT_CODE_KF4D_RISK)
					|| reportCode.equals(ReportingConstants.REPORT_CODE_KFALP_RAW_EXTRACT)) {
				boolean hasMacroTemplate = false;
				// do POI...
				Workbook workbook = null;
				// System.out.println("In POI... rptCode=" + reportCode);

				ReportingRequest rr = irh.getReportingRequest();
				if (rr.getReports() == null || rr.getReports().size() < 1) {
					// no data
					response.setHeader("Error",
							"No Data available, so no spreadsheet generated.  Verify that the participant has completed the required instruments.");
					response.setContentType("text/html");
					response.setContentType("text/xml;charset=UTF-8");
					response.setHeader("Content-Disposition", "attachment;filename=NoContent.html");
					PrintWriter pw = response.getWriter();
					pw.print("<html><body><h1 style=\"color:red;\">No spreadsheet generated.</h1><p>Report code: "
							+ reportCode
							+ "</p><p>Cause: No data available - Verify that the participant selected has completed the required instruments.</p></body></html>");

					return;
				}
				Report report = rr.getReports().get(0);
				GroupReport group = null;
				IndividualReport ir = null;

				// abyd and assessment and tlt
				if (report.getReportType() == ReportingConstants.REPORT_TYPE_GROUP
						|| report.getReportType() == ReportingConstants.REPORT_TYPE_GROUP_DATA
						|| report.getReportType() == ReportingConstants.REPORT_TYPE_GROUP_EXTRACT) {
					// Group report
					group = (GroupReport) report;
					String groupJson = JSONUtils.convertObjectToJSON(group);
					log.debug("Group Report JSON: {}", groupJson);
				} else {
					// Individual report
					ir = (IndividualReport) report;
				}

				// tlt - extract -->
				// if it's xml output, extract spreadsheet, or raw data extract
				// spreadsheet
				if ((reportCode.equals(ReportingConstants.REPORT_CODE_TLT_EXTRACT)) && (xmlOutput != null)) {
					// mb: 10-2012 : this "if" takes the tlt data and creates a
					// simple xml output for the scorecard server.
					// System.out.println("REPORT_CODE_TLT_EXTRACT && xmlOutput
					// != null............... ");

					com.pdi.reporting.report.vm.TLT_GROUP_DETAIL_REPORT_SCORING tltScoring = new com.pdi.reporting.report.vm.TLT_GROUP_DETAIL_REPORT_SCORING();

					// for the tlt extract, have to run tlt scoring...
					tltScoring.setGroupReport(group);
					tltScoring.groupReport = tltScoring.generate();

					// at this point, we have the individual report, with the
					// scores..
					// now get the xml from the ReportHelper method...
					String tltXML = irh.buildXMLForTLTExtract(tltScoring.groupReport);

					// System.out.println("tltXML:: " + tltXML);

					// this is where we will push the xml back....
					response.setContentType("text/xml;charset=UTF-8");
					PrintWriter out = response.getWriter();
					out.write(tltXML);

					// no need to do any more...
					return;

				} else if (reportCode.equals(ReportingConstants.REPORT_CODE_TLT_EXTRACT)) {
					com.pdi.reporting.report.vm.TLT_GROUP_DETAIL_REPORT_SCORING tltScoring = new com.pdi.reporting.report.vm.TLT_GROUP_DETAIL_REPORT_SCORING();

					// for the tlt extract, have to run tlt scoring...
					tltScoring.setGroupReport(group);
					tltScoring.groupReport = tltScoring.generate();
					ArrayList<IndividualReport> irs = tltScoring.groupReport.getIndividualReports();

					for (IndividualReport indiv : irs) {
						// System.out.println("irs: participantID " +
						// indiv.getDisplayData().get("PART_ID"));
						irh.generateRank(indiv);
					}

					ArrayList<IndividualReport> newOrder = irh.useGeneratedRanksToCreateReportOrder(irs);

					// now reset the report order in the group report....
					tltScoring.groupReport.setIndividualReports(newOrder);

					try {
						// do the poi
						workbook = irh.buildPoiXlsForTLTExtract(tltScoring.groupReport);
					} catch (ReportDataException e) {
						response.setHeader("Error", e.getMessage());
						response.setContentType("text/html");
						PrintWriter pw = response.getWriter();
						pw.print("<html><body><h1 style=\"color:red;\">No reports generated.</h1>" + e.getMessage()
								+ "</body></html>");
					}
				} else if (reportCode.equals(ReportingConstants.REPORT_CODE_TLT_EXTRACT_RAW)) {
					com.pdi.reporting.report.vm.TLT_GROUP_DETAIL_REPORT_SCORING tltScoring = new com.pdi.reporting.report.vm.TLT_GROUP_DETAIL_REPORT_SCORING();

					// for the tlt extract, have to run tlt scoring...
					tltScoring.setGroupReport(group);
					tltScoring.groupReport = tltScoring.generate();
					ArrayList<IndividualReport> irs = tltScoring.groupReport.getIndividualReports();
					for (IndividualReport indiv : irs) {
						irh.generateRank(indiv);
					}

					irs = tltScoring.groupReport.getIndividualReports();
					ArrayList<IndividualReport> newOrder = irh.useGeneratedRanksToCreateReportOrder(irs);

					// now reset the report order in the group report....
					tltScoring.groupReport.setIndividualReports(newOrder);

					try {
						// do the poi
						workbook = irh.buildPoiXlsForTLTRawExtract(tltScoring.groupReport);
					} catch (ReportDataException e) {
						response.setHeader("Error", e.getMessage());
						response.setContentType("text/html");
						PrintWriter pw = response.getWriter();
						pw.print("<html><body><h1 style=\"color:red;\">No reports generated.</h1>" + e.getMessage()
								+ "</body></html>");
					}
				}

				try {
					// assessment
					if (reportCode.equals(ReportingConstants.REPORT_CODE_ASSESSMENT_EXTRACT)) {
						workbook = irh.buildPoiXlsForAssessmentExtract(ir);
					}

					// abyd
					if (reportCode.equals(ReportingConstants.REPORT_CODE_ABYD_EXTRACT)) {
						// do the item list
						AbyDExtractFormatHelper fh = new AbyDExtractFormatHelper();
						ArrayList<ExtractItem> itemList = fh.setUpOrder(group);
						// do the poi
						workbook = irh.buildPoiXlsForAbyDExtract(itemList, group);
						// System.out.println("after the poi stuff.......");
					}

					if (reportCode.equals(ReportingConstants.REPORT_CODE_ABYD_RAW_EXTRACT)) {
						// do the item list
						// AbyDRawExtractFormatHelper fh = new
						// AbyDRawExtractFormatHelper();
						// ArrayList<ExtractItem> itemList =
						// fh.setUpOrder(group);
						ArrayList<ExtractItem> itemList = AbyDRawExtractFormatHelper.setUpOrder(group);
						// do the poi
						workbook = irh.buildPoiXlsForAbyDRawExtract(itemList, group);
						// System.out.println("after the poi stuff.......");
					}

					// abyd analytics Same extract as "standard" A by D extract
					// If we need different stuff, then we will need to create
					// a new xxxExtractFormatHelper
					if (reportCode.equals(ReportingConstants.REPORT_CODE_ABYD_ANALYTICS_EXTRACT)) {
						// Build the params
						ExtractParams parms = new ExtractParams(reportCode);
						parms.setSortId(sortId);
						parms.setShowName(showName);
						parms.setShowLci(showLci);
						parms.setAxisLabels(axisLabels);
						// Get the sortGroupName and sortTypeDispName
						parms = fillInSortNames(parms);

						// Do the item list
						AbyDExtractFormatHelper fh = new AbyDExtractFormatHelper();
						ArrayList<ExtractItem> itemList = fh.setUpOrder(group, true);
						// do the poi
						workbook = irh.buildPoiXlsForAbyDAnalyticsExtract(itemList, group, parms);
						hasMacroTemplate = true;
					}

					// ALR - Old school ALR
					if (reportCode.equals(ReportingConstants.REPORT_CODE_ALR_EXTRACT)) {
						// do the item list
						// AbyDExtractFormatHelper fh = new
						// AbyDExtractFormatHelper();
						ALRExtractFormatHelper fh = new ALRExtractFormatHelper();
						ArrayList<ExtractItem> itemList = fh.setUpOrder(group);
						// do the poi
						workbook = irh.buildPoiXlsForALRExtract(itemList, group);
						// System.out.println("after the poi stuff.......");
					}

					// KFALP Raw (extract for Consultants)
					if (reportCode.equals(ReportingConstants.REPORT_CODE_KFALP_RAW_EXTRACT)) {
						// do the item list
						AlpRawExtractFormatHelper fh = new AlpRawExtractFormatHelper();
						ArrayList<ExtractItem> itemList = fh.setUpOrder(group);
						// do the poi
						workbook = irh.buildPoiXlsForAlpRawExtract(itemList, group);
						// System.out.println("after the poi stuff.......");
					}

					// Traits and Drivers
					if (reportCode.equals(ReportingConstants.REPORT_CODE_ALR_TRAITS)
							|| reportCode.equals(ReportingConstants.REPORT_CODE_KF4D_TRAITS)) {
						workbook = irh.buildPoiXlsForAlrTraits(ir);
					}
					if (reportCode.equals(ReportingConstants.REPORT_CODE_ALR_DRIVERS)
							|| reportCode.equals(ReportingConstants.REPORT_CODE_KF4D_DRIVERS)) {
						workbook = irh.buildPoiXlsForAlrDrivers(ir);
					}

					if (reportCode.equals(ReportingConstants.REPORT_CODE_KF4D_RISK)) {
						workbook = irh.buildPoiXlsForRiskFactors(ir);
					}
				} catch (ReportDataException e) {
					response.setHeader("Error", e.getMessage());
					response.setContentType("text/html");
					PrintWriter pw = response.getWriter();
					pw.print("<html><body><h1 style=\"color:red;\">No reports generated.</h1>" + e.getMessage()
							+ "</body></html>");
					log.error("No Reports Generated due to: {}", e.getMessage());
					e.printStackTrace();
				}

				String fn = report.getName();
				// This code assumes:
				// -- All report names come here as .xls
				// -- All workboks have been converted to 2007 or later format
				// (.xlsx)
				// -- Anything that uses a template should be .xlsm
				if (workbook instanceof XSSFWorkbook) {
					fn += hasMacroTemplate ? "m" : "x";
				}
				response.setHeader("Expires", "0");
				response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
				response.setHeader("Pragma", "public");
				// response.setContentType("application/x-excel");
				response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
				response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fn, "UTF-8"));
				ServletOutputStream out = response.getOutputStream();
				// System.out.println(" workbook.getBytes().length " +
				// workbook.getBytes().length);
				workbook.write(out);
				// System.out.println(" here.... ");
				out.flush();
				out.close();

				//
				workbook = null;

			} else if (reportCode.equals(ReportingConstants.REPORT_CODE_IBLVA_CONSULTANT_REVIEW)) {
				// special circumstances... This one goes and gets data from
				// scorecard
				// String url =
				// PropertyLoader.getProperty("com.pdi.listener.portal.application",
				// "pptContent.url");
				String url = PropertyLoader.getProperty("com.pdi.listener.portal.application", "bizsimContent.url");
				if (url.charAt(url.length() - 1) == '/') {
					url = url.substring(0, url.length() - 2);
				}
				url += LVA_CONS_PARAM_STR;

				String html = new NhnWebserviceUtils().fetchParticipantContent(url, participantId);

				response.setHeader("Expires", "0");
				response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
				response.setHeader("Pragma", "public");
				response.setContentType("text/html");
				PrintWriter pw = response.getWriter();
				pw.print(html);
			} else if (reportCode.equals(ReportingConstants.REPORT_CODE_NIS_CONSULTANT_REVIEW)
					|| reportCode.equals(ReportingConstants.REPORT_CODE_ISMLL_CONSULTANT_REVIEW)
					|| reportCode.equals(ReportingConstants.REPORT_CODE_ISBUL_CONSULTANT_REVIEW)
					|| reportCode.equals(ReportingConstants.REPORT_CODE_ISZ_CONSULTANT_REVIEW)
					|| reportCode.equals(ReportingConstants.REPORT_CODE_EIS_CONSULTANT_REVIEW)
					|| reportCode.equals(ReportingConstants.REPORT_CODE_BRE2_CONSULTANT_REVIEW)
					|| reportCode.equals(ReportingConstants.REPORT_CODE_BRESEA_CONSULTANT_REVIEW)
					|| reportCode.equals(ReportingConstants.REPORT_CODE_SFECEO_CONSULTANT_REVIEW)
					|| reportCode.equals(ReportingConstants.REPORT_CODE_BREMED_CONSULTANT_REVIEW)) {
				// Special circumstances... These get data from bizsim
				String url = PropertyLoader.getProperty("com.pdi.listener.portal.application", "bizsimContent.url");
				if (url.charAt(url.length() - 1) == '/') {
					url = url.substring(0, url.length() - 2);
				}

				// TODO - Can we refactor this somehow?
				String inst = "";
				if (reportCode.equals(ReportingConstants.REPORT_CODE_NIS_CONSULTANT_REVIEW)) {
					inst = "nis"; // Novartis
				} else if (reportCode.equals(ReportingConstants.REPORT_CODE_ISMLL_CONSULTANT_REVIEW)) {
					inst = "ismll"; // MLL inbox
				} else if (reportCode.equals(ReportingConstants.REPORT_CODE_ISBUL_CONSULTANT_REVIEW)) {
					inst = "isbul"; // BUL inbox
				} else if (reportCode.equals(ReportingConstants.REPORT_CODE_ISZ_CONSULTANT_REVIEW)) {
					inst = "isz"; // Zenio inbox (MLL)
				} else if (reportCode.equals(ReportingConstants.REPORT_CODE_EIS_CONSULTANT_REVIEW)) {
					inst = "eis"; // Ecolog inbox (MLL)
				} else if (reportCode.equals(ReportingConstants.REPORT_CODE_BRE2_CONSULTANT_REVIEW)) {
					inst = "bre2"; // BRE (not an inbox)
				} else if (reportCode.equals(ReportingConstants.REPORT_CODE_BRESEA_CONSULTANT_REVIEW)) {
					inst = "bresea"; // BRE (not an inbox)
				} else if (reportCode.equals(ReportingConstants.REPORT_CODE_SFECEO_CONSULTANT_REVIEW)) {
					inst = "sfeceo"; // BRE (not an inbox)
				} else if (reportCode.equals(ReportingConstants.REPORT_CODE_BREMED_CONSULTANT_REVIEW)) {
					inst = "bremed"; // BRE (not an inbox)
				}

				// Change this?
				// url += IBX_CONS_PARAM_STR_1 + inst + IBX_CONS_PARAM_STR_2;
				url += IBX_CONS_PARAM_STR;
				url = url.replace("{inst}", inst);

				String html = new NhnWebserviceUtils().fetchParticipantContent(url, participantId);

				response.setHeader("Expires", "0");
				response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
				response.setHeader("Pragma", "public");
				response.setCharacterEncoding("UTF-8");
				response.setContentType("text/html");
				PrintWriter pw = response.getWriter();
				pw.print(html);
			} else if (Arrays.asList(ReportingConstants.PALMS_REPORT_CODE_LIST).contains(reportCode)) {
				response = fetchExternalReport(principal, participantId, extRptProjId, reportCode, response);
				return;
			}
		
			else {
				// Not a POI generated spreadsheet... do what we'd usually do
				// (old school)

				ReportingResponse reportResponse = irh.generate(su);

				// From here down we are just feeding the resulting report to
				// the user
				ByteArrayOutputStream bos = reportResponse.toFile();
				response.setHeader("Expires", "0");
				response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
				response.setHeader("Pragma", "public");
				response.setContentType("application/x-unknown");
				if (reportResponse.getReports().size() > 1) {
					response.setHeader("Content-Disposition", "attachment;filename=Reports_"
							+ new SimpleDateFormat("yyyyMMddhhmmss").format(new Date()) + ".zip");
				} else {
					// if(reportResponse.getReports().get(0).getReportType().equalsIgnoreCase(ReportingConstants.REPORT_TYPE_GROUP_EXTRACT)
					// ||
					// reportResponse.getReports().get(0).getReportType().equalsIgnoreCase(ReportingConstants.REPORT_TYPE_INDIVIDUAL_EXTRACT)
					// ) {
					// if(reportResponse.getReports().get(0).getReportType().equalsIgnoreCase(ReportingConstants.REPORT_TYPE_GROUP_EXTRACT)
					// &&
					// reportResponse.getReports().get(0).getReportCode().equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_RAW_EXTRACT)
					// )
					// {
					// System.out.println("PS 809 :
					// reportResponse.getReports().get(0).getName() "
					// + reportResponse.getReports().get(0).getName());

					// To handle multibyte characters we need to encode the file
					// name being sent back
					// response.setHeader("Content-Disposition",
					// "attachment;filename=" +
					// reportResponse.getReports().get(0).getName());

					response.setHeader("Content-Disposition", "attachment;filename="
							+ URLEncoder.encode(reportResponse.getReports().get(0).getName(), "UTF-8"));
					// } else {
					// response.setHeader("Content-Disposition",
					// "attachment;filename="+reportResponse.getReports().get(0).getName());
					// }
				}
				response.setContentLength(bos.size());
				ServletOutputStream out = response.getOutputStream();

				bos.writeTo(out);
				out.flush();
				out.close();
			}
		} catch (ReportDataException e) {
			response.setHeader("Error", e.getMessage());
			response.setContentType("text/html");
			PrintWriter pw = response.getWriter();
			pw.print("<html><body><h1 style=\"color:red;\">No reports generated.</h1>" + e.getMessage()
					+ "</body></html>");
		} catch (Exception e) {
			String eStr = "PortalServlet Reporting ERROR: timestamp=" + sdf.format(new Date()) + "    instrumentId="
					+ instrumentId + "  participantId=" + participantId + "  reportCode=" + reportCode;
			e.printStackTrace();
			log.error("PortalServlet Reporting ERROR:  instrumentId={}, participantId={}, reportCode={}", instrumentId, participantId, reportCode);
			
			response.setHeader("Error", e.getMessage());
			response.setContentType("text/html");
			PrintWriter pw = response.getWriter();
			pw.print("<html><body><h1 style=\"color:red;\">Error generating report.</h1><p>" + e.getMessage()
					+ "</p><p>" + eStr + "</p></body></html>");
		}
		
	}

	/**
	 * fetchExternalReport - Fetch a report that exists in the download
	 * persistent store.
	 *
	 * @param request
	 * @param pptId
	 * @param projId
	 * @param type
	 * @param response
	 * @return
	 * @throws Exception
	 */
	private HttpServletResponse fetchExternalReport(String principal, String pptId, String projId,
			String type, HttpServletResponse response) throws Exception {
		String backupPrincipal = null;

		// Use backup? For situations where the authentication info is not
		// available in the session
		boolean useBackup = PropertyLoader.getProperty("com.pdi.listener.portal.application", "useBackupPrinIfNeeded")
				.equals("1");
		if (useBackup) {
			System.out.println("useBackupPrinIfNeeded is ON... SET IT OFF before deploy!");
			backupPrincipal = PropertyLoader.getProperty("com.pdi.listener.portal.application", "backupPrincipal");
			if (backupPrincipal.isEmpty()) {
				backupPrincipal = null;
			}
		}

		// Get the session principal
		//String principal = (String) request.getSession().getAttribute("principal");

		if (principal == null) {
			if (backupPrincipal != null) {
				principal = backupPrincipal;
			} else {
				String msg = "Unable to fetch external report for type " + type + ";  Principal is null.  ppt=" + pptId
						+ ", proj=" + projId;
				genRptErrResponse(msg, response);
				return response;
			}
		}

		String secret = PropertyLoader.getProperty("com.pdi.listener.portal.application", "palms.api.secretKey");
		String partner = PropertyLoader.getProperty("com.pdi.listener.portal.application", "palms.api.partnerKey");
		String server = PropertyLoader.getProperty("com.pdi.listener.portal.application", "palms.server");
		ReportAPIHelper hlpr = new ReportAPIHelper(secret, partner, server);

		int userId = Integer.parseInt(pptId);
		int projectId = Integer.parseInt(projId);

		// ***********************************
		// userId = 390748;
		// projectId = 7377;
		//// userId = 4; // invalid ppt id
		//// projectId = 7; // invalid proj id
		// ReportDownloadResponse resp = hlpr.getReportByUserProjectType(userId,
		// projectId, "KFP_INDIVIDUAL", principal); // test for known content
		// (does the process work?)
		// ***********************************
		// ReportDownloadResponse resp = hlpr.getReportByUserProjectType(userId,
		// projectId, "KFP_INDIVIDUAL", "bouncy@ball.com"); // Test for invalid
		// principal -- doesn't seem to work
		// ReportDownloadResponse resp = hlpr.getReportByUserProjectType(userId,
		// projectId, "XYZ_REPORT", principal); // Test for invalid rpt name --
		// gives "Failed to connect..." (500)
		// ReportDownloadResponse resp = hlpr.getReportByUserProjectType(userId,
		// projectId, TDLA_RPT_TYPE, principal); // Test for rpt content not
		// available -- 500

		ReportDownloadResponse resp = hlpr.getReportByUserProjectType(userId, projectId, type, principal);
		if (!resp.isSuccess()) {
			String msg = "Error fetching report from PALMS Report Center.  Ppt=" + userId + ", proj=" + projectId + ", type = " + type
					+ ".  Cause=" + resp.getErrorMessage()
					+ ((resp.getErrorDetail() == null) ? " " : ".  Detail=" + resp.getErrorDetail());
			genRptErrResponse(msg, response);
			return response;
		}

		response.setHeader("Expires", "0");
		response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		response.setHeader("Pragma", "public");
		response.setContentType("application/x-unknown");
		response.setHeader("Content-Disposition",
				"attachment;filename=" + URLEncoder.encode(resp.getFileName(), "UTF-8"));
		ServletOutputStream out = response.getOutputStream();

		IOUtils.copy(resp.getInStream(), out);
		out.flush();
		out.close();

		return response;
	}

	/**
	 * genRptErrResponse - Create the error report response
	 *
	 * @param msg
	 * @param response
	 */
	private void genRptErrResponse(String msg, HttpServletResponse response) {
		response.setHeader("Error", msg);
		response.setContentType("text/html");
		try {
			PrintWriter pw = response.getWriter();
			pw.print("<html><body><h1 style=\"color:red;\">Error generating report.</h1><p>" + msg
					+ "</p></body></html>");
		} catch (IOException e) {
			log.error("PortalServlet.genRptErrResponse() IOException reporting error - {}. Exception msg: {}", 
					msg, e.getMessage());
				
		}

		return;
	}

	/*
	 * This code is used for testing the text helper stuff. It should be
	 * commented out in normal (QA/Prod) usage.
	 *
	 * Going forward, this is a good place to put Q-and-D testing stuff... Just
	 * preserve this code as it may be used again.
	 */
	// protected void doGet(HttpServletRequest request, HttpServletResponse
	// response)
	// throws ServletException, IOException
	// {
	// PrintWriter out = response.getWriter();
	//
	// //System.out.println("stuff hapepns here");
	//
	// out.print("<h1>Text stuff</h1>");
	//
	// out.print("The commentary below assumes that this is being tun on a
	// pristine system.<br>");
	// out.print("If there is already text in the system, then the indicated
	// counts will be off until the 6th check when we clear the cache.<br>");
	//
	// ITextHelper helper = null;
	// try
	// {
	// helper =
	// HelperDelegate.getTextHelper(PropertyLoader.getProperty("com.pdi.data.v2.application","datasource.v2"));
	// }
	// catch (Exception e)
	// {
	// out.print("<p>Blew up getting delegated text helper");
	// return;
	// }
	//
	// TextHelper th = new TextHelper();
	// out.print("<p>1st Check: Should be nothing...<br>");
	// out.print(" " + th.showCache());
	//
	// // Get a text (ignoring content)
	// helper.fromGroupCodeLanguageCodeLabelCode(null, "TLT_IND_SUMMARY_RPT",
	// "en", "MANIP");
	// out.print("<p>2nd Check: Should be one group...<br>");
	// out.print(" " + th.showCache());
	//
	// // Dump the instances to see if the data goes away
	// helper = null;
	// th = null;
	//
	// th = new TextHelper();
	// out.print("<p>3rd Check: Still 1 group...<br>");
	// out.print(" " + th.showCache());
	//
	// try
	// {
	// helper =
	// HelperDelegate.getTextHelper(PropertyLoader.getProperty("com.pdi.data.v2.application","datasource.v2"));
	// }
	// catch (Exception e)
	// {
	// out.print("Blew up getting delegated text helper (2)");
	// return;
	// }
	//
	// // Get a text (again ignoring content)
	// helper.fromGroupCodeLanguageCodeLabelCode(null, "TLT_GRP_DETAIL_RPT",
	// "fr", "GRP_SUMMARY_RPT");
	// out.print("<p>4th Check: Should be 2 groups...<br>");
	// out.print(" " + th.showCache());
	//
	// // Now clear a specific group
	// helper.clearGroupCache("TLT_IND_SUMMARY_RPT");
	// out.print("<p>5th Check: Should be 1 group (NOT IND_SUMM)...<br>");
	// out.print(" " + th.showCache());
	//
	// // Clear 'em all
	// helper.clearGroupCache();
	// out.print("<p>6th Check: Should be NO grops...<br>");
	// out.print(" " + th.showCache());
	//
	// // put one back
	//
	// // Get a text (ignoring content)
	// helper.fromGroupCodeLanguageCodeLabelCode(null, "TLT_IND_SUMMARY_RPT",
	// "en", "MANIP");
	// out.print("<p>7th Check: Should be 1 group again...<br>");
	// out.print(" " + th.showCache());
	// }
	/*
	 * // heres a way to write out the content of a stream BufferedWriter out =
	 * new BufferedWriter(new OutputStreamWriter(new
	 * FileOutputStream("<file name>"),"UTF8")); out.write(input); out.close();
	 */

	// -----------------------------------------------------
	// A by D score request
	// -----------------------------------------------------

	/**
	 * processScoreRequest - Process an external score fetch request. Normed
	 * scores returned. NOTE: Generalize this to handle more than GL_GPS
	 * (glGpsInstSet)
	 *
	 * @param doc
	 *            - A Document object containing the incoming XML
	 * @param response
	 *            - The HttpServletResponse object for this request
	 */
	private void processScoreRequest(Document doc, HttpServletResponse response) {
		/*
		 * <FetchScores> <FetchRequest code = "GPS_INST" normset = "GPS_MLL">
		 * <Participants> <Participant participantId = "367018" projectId =
		 * "491"/> </Participants> </FetchRequest> </FetchScores>
		 */

		// Assume for now that there is a single request per invocation...
		// We can refactor later if we need to
		NodeList nodes = XMLUtils.getElements(doc, "//FetchRequest");

		String normsetName = "";
		String participantId = "";
		HashMap<String, NormScore> allScores = new HashMap<String, NormScore>();

		Node node = nodes.item(0); // Get the first (and, for now, only) request
									// element
		normsetName = node.getAttributes().getNamedItem("normset").getTextContent();

		NodeList ppts = XMLUtils.getElements(node, "Participants/Participant");
		Node ppt = ppts.item(0); // Again, the first and only node
		participantId = ppt.getAttributes().getNamedItem("participantId").getTextContent();

		IProjectParticipantInstrumentHelper epih = null;
		SessionUser su = new SessionUser();
		try {
			epih = HelperDelegate.getProjectParticipantInstrumentHelper("com.pdi.data.v2.helpers.delegated");
		} catch (Exception e) {
			System.out.println("Error:  PortalServelet.processFetchRequest() instantiating epih");
			epih = null;
		}
		// using null for the calling project, as it will be the same as the
		// included project ID in this case.
		// the scoring part of the code doesn't appear to be affected by this
		// issue. just reporting.
		ArrayList<ProjectParticipantInstrument> epis = (epih == null) ? new ArrayList<ProjectParticipantInstrument>()
				: epih.fromLastParticipantId(su, IProjectParticipantInstrumentHelper.MODE_FULL_COMPLETED, participantId,
						null);

		for (ProjectParticipantInstrument epi : epis) {
			String instCode = epi.getInstrument().getCode();
			if (glGpsInstSet.contains(instCode)) {
				// Get the normset for this instrument
				NormGroup nset = getNormSet(instCode, normsetName);
				if (nset == null) {
					System.out.println("Warning:  PortalServlet.processFetchRequest() cannot find norms:  instrument="
							+ instCode + ", normset=" + normsetName);
					continue;
				}

				// Add it to the epi
				epi.getScoreGroup().setSpecialPopulation(nset);

				// Score it
				HashMap<String, NormScore> instScores = epi.getScoreGroup().calculateSpecialPopulation();

				// Add the scores to a HashMap
				allScores.putAll(instScores);
			}
		}

		// We now have the scores... set up the response
		String respXML = buildScoreFetchXML(allScores);
		// System.out.println("respXML:: " + respXML);

		// this is where we will push the xml back....
		response.setContentType("text/html");
		response.setContentType("text/xml;charset=UTF-8");
		PrintWriter out;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			System.out.println("Warning:  PortalServlet.processFetchRequest() unable to fetch response writer.  msg="
					+ e.getMessage());
			return;
		}
		out.write(respXML);

		// no need to do any more...
		return;
	}

	/**
	 * getNormSet - Assumes there is a specPop norm by this name in the database
	 */
	private NormGroup getNormSet(String instCode, String normsetName) {
		NormGroup ng = null;

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT nn.normGroupId, ");
		sb.append("       nn.normId, ");
		sb.append("	      nn.spssValue, ");
		sb.append("	      nn.mean, ");
		sb.append("	      nn.stdev ");
		sb.append("	 FROM pdi_norm nn ");
		sb.append("  WHERE active = 1 ");
		sb.append("    AND normGroupId = (SELECT normGroupId ");
		sb.append("  	    				FROM pdi_norm_group ");
		sb.append("  						WHERE instrumentCode like '" + instCode + "%' ");
		sb.append("	    					  AND name = '" + normsetName + "' ");
		sb.append("							  AND normType = 2 ");
		sb.append("	    					  AND active = 1) ");

		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
		if (dlps.isInError()) {
			System.out.println("Warning:  PortalServlet.getNormSet() failed to prepare the SQL.  " + "instCode="
					+ instCode + ", normsetName=" + normsetName + ".  " + dlps.toString());
			return null;
		}
		DataResult dr = null;
		ResultSet rs = null;

		try {
			dr = V2DatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			if (rs == null || !rs.isBeforeFirst()) {
				System.out.println("Warning:  PortalServlet.getNormSet() did not retrieve norms.  " + "instCode="
						+ instCode + ", normsetName=" + normsetName + ".  " + dlps.toString());
				return null;
			}

			// We have data of some sort...
			boolean first = true;
			while (rs.next()) {
				if (first) {
					ng = new NormGroup();
					ng.setId(rs.getInt("normGroupId"));
					ng.setInstrumentCode(instCode);
					ng.setPopulation(NormGroup.SPECIAL_POPULATION);
					ng.setName(normsetName);
					first = false;
				}

				Norm n = new Norm();
				n.setId(rs.getInt("normId"));
				n.setSpssValue(rs.getString("spssValue"));
				n.setMean(rs.getDouble("mean"));
				n.setStandardDeviation(rs.getDouble("stDev"));

				ng.getNorms().put(n.getSpssValue(), n);
			}

			return ng;
		} catch (java.sql.SQLException se) {
			System.out.println("Error:  PortalServlet.getNormSet() resultSet fetch.  msg=" + se.getMessage());
			return null;
		} finally {
			if (dr != null) {
				dr.close();
				dr = null;
			}
		}
	}

	/**
	 * buildScoreFetchXML -- Builds the return XML for the score fetch NOTE:
	 * This assumes a single input ppt and a single set of scores returned.
	 * Refactor as needed to add scores for multiple participants.
	 *
	 */
	private String buildScoreFetchXML(HashMap<String, NormScore> scores) {
		String ret = "<scores>"; // Start the sore string

		for (Iterator<Map.Entry<String, NormScore>> itr = scores.entrySet().iterator(); itr.hasNext();) {
			Map.Entry<String, NormScore> ent = itr.next();
			NormScore ns = ent.getValue();

			ret += "<score name=\"" + ns.getNorm().getSpssValue() + "\">";
			ret += "<raw>" + ns.getScore().getRawScore() + "</raw>";
			ret += "<pctl>" + ns.toPercentile() + "</pctl>";
			ret += "<rating>" + ns.toRating() + "</rating>";
			ret += "<stanine>" + ns.toStanine() + "</stanine>";

			ret += "</score>";
		}

		ret += "</scores>";
		return ret;
	}

	// -----------------------------------------------------
	// Norm list request
	// -----------------------------------------------------

	/**
	 * processNormListRequest - Get a list of valid normsets for the instruments
	 * in the designated project. The project level defaults are marked as
	 * selected. NOTE: There may be only one type of population for some
	 * instruments
	 */
	private void processNormListRequest(Document doc, HttpServletResponse response) {
		/*
		 * Request XML (input) looks like this: <CourseNormsList> <project
		 * id="557"/> </courseNormsList>
		 *
		 * Return looks like this: <CourseNormsList> <course abbv="rv">
		 * <normGroup type="specPop" id="123" isSelected="false">Norm
		 * name</normGroup>"; <normGroup type="specPop" id="234"
		 * isSelected="true">Norm name</normGroup>"; <normGroup type="genPop"
		 * id="345" isSelected="true">Norm name</normGroup>"; </course>
		 * </CourseNormsList>
		 */
		String retXml = "<CourseNormsList>";

		// get the project id
		NodeList nodes = XMLUtils.getElements(doc, "//CourseNormsRequest");
		Node node = nodes.item(0); // Get the first (and, for now, only) request
									// element
		NodeList prj = XMLUtils.getElements(node, "project");
		String projId = prj.item(0).getAttributes().getNamedItem("id").getTextContent();

		// get the courses and make a unique list
		// ArrayList<String> courseList = new NhnWebserviceUtils()
		// .fetchCourseListForProject(PropertyLoader.getProperty("com.pdi.listener.portal.application",
		// "courseList.url"), projId);
		ArrayList<String> courseList = new NhnWebserviceUtils().getFilteredProjectCourseList(projId);
		Collections.sort(courseList);

		HashSet<Integer> defaults = new HashSet<Integer>();
		SessionUser su = new SessionUser();
		INormGroupHelper helper = null;
		try {
			helper = HelperDelegate.getNormGroupHelper("com.pdi.data.v2.helpers.delegated");
		} catch (Exception ex) {
			// Put out a warning message
			LogWriter.logBasicWithException(LogWriter.WARNING, this,
					"PortalServlet.processNormListRequest() - Unable to instantiate NormGroupHelper: " + "ProjectId="
							+ projId,
					ex);
			// create an empty course list (continue processing)
			courseList = new ArrayList<String>(); // Clear the course list
			helper = null;
		}

		// look up the existing setup (or none)
		if (helper != null)
			log.debug("About to get Default Norm Group Set: {}", projId);
			defaults = helper.getDefaultNormGroupSet(projId);
			log.debug("Finished getting Default Norm Group Set: {}", projId);

		// Get the normsets
		String curCourse = "";
		LinkedHashMap<String, ArrayList<NormGroup>> instNorms = new LinkedHashMap<String, ArrayList<NormGroup>>();
		for (String inst : courseList) {
			if (curCourse.equals(inst))
				continue;
			else
				curCourse = inst;

			// get the norm groups
			if (helper != null) {
				ArrayList<NormGroup> ngList = helper.fromInstrumentIdOrdered(su, inst);
				if (ngList == null || ngList.size() < 1) {
					continue;
				}
				instNorms.put(inst, ngList);
			}
		}

		// Get the instrument names
		IInstrumentHelper instHelper;
		HashMap<String, String> instMap = new HashMap<String, String>();
		try {
			instHelper = HelperDelegate.getInstrumentHelper("com.pdi.data.v2.helpers.delegated");
		} catch (Exception ex) {
			// Put out a warning message
			LogWriter.logBasicWithException(LogWriter.WARNING, this,
					"PortalServlet.processNormListRequest() - Unable to instantiate InstrumentHelper: " + "ProjectId="
							+ projId,
					ex);
			// create an empty course list (continue processing)
			instHelper = null;
		}

		ArrayList<Instrument> allList = null;
		if (instHelper != null) {
			try {
				allList = instHelper.all(null, IInstrumentHelper.TYPE_SCORABLE);
			} catch (Exception e) {
				LogWriter.logBasicWithException(LogWriter.WARNING, this,
						"PortalServlet.processNormListRequest() - Unable to fetch instrument list: " + "ProjectId="
								+ projId,
						e);
				allList = null;
			}
		}
		if (allList != null) {
			// Load the minimal data HashMap
			for (Instrument inst : allList) {
				instMap.put(inst.getCode(), inst.getShortName());
			}
		}

		// generate the output XML (merge results from defaults and all norms)
		// loop through the generated norm list (by course)
		// Data should be ordered by pop type and then alphabetic by name
		for (Iterator<Map.Entry<String, ArrayList<NormGroup>>> itr = instNorms.entrySet().iterator(); itr.hasNext();) {
			Map.Entry<String, ArrayList<NormGroup>> ent = itr.next();
			String abbv = ent.getKey();
			String sn = instMap.get(abbv);
			if (sn == null) {
				sn = abbv;
			}
			retXml += "<course abbv=\"" + abbv + "\" shortName=\"" + sn + "\">";

			ArrayList<NormGroup> nga = ent.getValue();
			for (NormGroup ng : nga) {
				int popType = ng.getPopulation();
				String pop = (popType == NormGroup.GENERAL_POPULATION) ? "gen" : "spec";
				pop += "Pop";

				// put out the norm group data
				int id = ng.getId();
				boolean selected = defaults.contains(id) || defaults.size() == 0 && ng.isDefaultNormGroup();
				retXml += "<normGroup type=\"" + pop + "\" id=\"" + id + "\" isSelected=\"" + selected + "\" code=\""
						+ ng.getCode() + "\" defaultNormGroup=\"" + ng.isDefaultNormGroup() + "\" sequenceOrder=\""
						+ ng.getSequenceOrder() + "\">" + ng.getName() + "</normGroup>";
			}

			// Close the course
			retXml += "</course>";
		} // End outer (course) loop

		// Close the list
		retXml += "</CourseNormsList>";

		// Push the xml back
		// response.setContentType("text/html");
		response.setContentType("text/xml;charset=UTF-8");
		PrintWriter out;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			System.out.println("Warning:  PortalServlet.processFetchRequest() unable to fetch response writer.  msg="
					+ e.getMessage());
			LogWriter.logBasicWithException(LogWriter.WARNING, this,
					"PortalServlet.processFetchRequest() unable to fetch response writer.", e);
			return;
		}
		out.write(retXml);

		// scram
		return;
	}

	// -----------------------------------------------------
	// Fetch norm selection list
	// -----------------------------------------------------
	/*
	 * <FetchSelectedNormsList project_id="370"> <participantList> <participant
	 * id="367714"/> </participantList> </FetchSelectedNormsList>
	 *
	 * Return format looks like this: <selectedNorms> <projectNorms> <course
	 * abbv="rv"> <normGroup id="101" type="genPop"/> <normGroup id="102"
	 * type="specPop"/> </course> </projectNorms> <participantNorms> <participant
	 * id="123456"> <course abbv="rv"> <normGroup id="101" type="genPop"/>
	 * <normGroup id="102" type="specPop"/> </course> </participant>
	 * </participantNorms> </selectedNorms>
	 */
	private void processFetchSelectedNormsList(Document doc, HttpServletResponse response) {
		boolean isOK = true; // Assume that that routine will succeed
		String projId = null;
		String pptId = null;
		Node node = null;

		// Get the project ID
		node = doc.getDocumentElement();
		projId = XMLUtils.getAttributeValue(node, "project_id");
		if (projId == null) {
			LogWriter.logBasic(LogWriter.ERROR, this,
					"PortalServlet.processFetchSelectedNormsList():  Unable to fetch project ID.  Node:  "
							+ XMLUtils.nodeToString(node));
			isOK = false;
		}

		NodeList ppts = null;
		if (isOK) // We have the project
		{
			// Get the ppt list
			ppts = XMLUtils.getElements(node, "participantList/participant");
			if (ppts == null) {
				LogWriter.logBasic(LogWriter.ERROR, this,
						"PortalServlet.processFetchSelectedNormsList():  Unable to fetch ppt list.  Node:  "
								+ XMLUtils.nodeToString(node));
				isOK = false;
			}
		}

		// Get the norm group helper
		INormGroupHelper ngHelper = null;
		if (isOK) {
			try {
				ngHelper = HelperDelegate.getNormGroupHelper("com.pdi.data.v2.helpers.delegated");
			} catch (Exception e) {
				LogWriter.logBasicWithException(LogWriter.ERROR, this,
						"PortalServlet.processFetchSelectedNormsList() failed to instantiate NormGroupHelper. projId="
								+ projId,
						e);
				ngHelper = null;
				isOK = false;
			}
		}

		// Get the list of all posible instruments that have norms attached.
		HashSet<String> instSet = null;
		if (isOK) {
			instSet = ngHelper.getNormableCourseSet();
			if (instSet == null) {
				isOK = false;
			}
		}

		// We have the pptList... Get all of the courses in the project...
		ArrayList<String> courses = new ArrayList<String>();
		if (isOK) {
			// ArrayList<String> courseList = new NhnWebserviceUtils()
			// .fetchCourseListForProject(PropertyLoader.getProperty(
			// "com.pdi.listener.portal.application", "courseList.url"),
			// projId);
			ArrayList<String> courseList = new NhnWebserviceUtils().getFilteredProjectCourseList(projId);
			if (courseList == null) {
				courseList = new ArrayList<String>();
			}
			Collections.sort(courseList);

			// ...And reduce it to the list of courses that have norms
			for (String inst : courseList) {
				if (instSet.contains(inst)) {
					courses.add(inst);
				}
			}
		}

		// Start generating the output XML
		String retXml = "<selectedNorms>";

		if (isOK) {
			retXml += "<projectNorms>";

			// Get the project default norms
			log.debug("Begin getDefaultNormGroupMap(projId)");
			HashMap<String, ArrayList<NormGroup>> norms = ngHelper.getDefaultNormGroupMap(projId);
			log.debug("End getDefaultNormGroupMap(projId)");
			

			for (String crs : courses) {
				ArrayList<NormGroup> normIds = norms.get(crs);
				if (normIds != null) {
					retXml += "<course abbv=\"" + crs + "\">";
					for (NormGroup ng : normIds) {
						if (ng.getId() == 0) {
							continue;
						}
						retXml += "<normGroup id=\"" + ng.getId() + "\" " + "type=\""
								+ (ng.getPopulation() == NormGroup.GENERAL_POPULATION ? "gen" : "spec") + "Pop\""
										+ " code=\"" + ng.getCode() + "\" name=\"" + ng.getName() + "\"/>";
					}
					retXml += "</course>";
				} else {
					int genNormGroupId = ngHelper.getDefaultNormGroupForInstrument(crs, NormGroup.GENERAL_POPULATION);
					int specNormGroupId = ngHelper.getDefaultNormGroupForInstrument(crs, NormGroup.SPECIAL_POPULATION);

					if (genNormGroupId != 0 || specNormGroupId != 0) {
						retXml += "<course abbv=\"" + crs + "\">";

						if (genNormGroupId != 0) {
							NormGroup ng = ngHelper.fromId(null, genNormGroupId);

							if (ng != null) {
								ngHelper.update(null, ng, new String(projId), "0", crs);

								retXml += "<normGroup id=\"" + genNormGroupId + "\" " + "type=\"genPop\"" + " code=\""
										+ ng.getCode() + "\" name=\"" + ng.getName() + "\"/>";
							}
						}

						if (specNormGroupId != 0) {
							NormGroup ng = ngHelper.fromId(null, specNormGroupId);

							if (ng != null) {
								ngHelper.update(null, ng, new String(projId), "0", crs);

								retXml += "<normGroup id=\"" + specNormGroupId + "\" " + "type=\"specPop\"" + " code=\""
										+ ng.getCode() + "\" name=\"" + ng.getName() + "\"/>";
							}
						}
						retXml += "</course>";
					}
				}
			} // End of course loop

			retXml += "</projectNorms>";
		}

		// Don't do if no ppts
		if (isOK && ppts.getLength() > 0) {
			retXml += "<participantNorms>";
			// Start looping through the participants
			for (int i = 0; i < ppts.getLength(); i++) {
				Node ppt = ppts.item(i);
				pptId = XMLUtils.getAttributeValue(ppt, "id");
				if (pptId == null) {
					LogWriter.logBasic(LogWriter.ERROR, this,
							"PortalServlet.processSetNormDefaultsRequest():  Unable to fetch ppt ID.  Node:  "
									+ XMLUtils.nodeToString(ppt));
					isOK = false;
					continue;
				}

				// Get the ppt info
				String pptXml = getPptNorms(projId, pptId, courses);
				if (pptXml == null) {
					// The message has been put out... skip this guy
				} else {
					retXml += "<participant id=\"" + pptId + "\">";
					retXml += pptXml;
					retXml += "</participant>";
				}
			}
			retXml += "</participantNorms>";
		}

		retXml += "</selectedNorms>";

		// Push the XML back
		response.setContentType("text/xml;charset=UTF-8");
		PrintWriter out;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			System.out.println("Warning:  PortalServlet.processFetchRequest() unable to fetch response writer.  msg="
					+ e.getMessage());
			LogWriter.logBasicWithException(LogWriter.WARNING, this,
					"PortalServlet.processFetchSelectedNormsList() unable to fetch response writer.", e);
			return;
		}
		out.write(retXml);

		// scram
		return;
	}

	/**
	 * getPptNorms - get the norms for a single ppt, generate the XML, and put
	 * it into the xml string passed
	 *
	 * @param projId
	 * @param pptId
	 * @param workingXml
	 * @return
	 */
	private String getPptNorms(String projId, String pptId, ArrayList<String> courses) {
		INormGroupHelper ngh = null;
		String xml = "";

		try {
			ngh = HelperDelegate.getNormGroupHelper("com.pdi.data.v2.helpers.delegated");
		} catch (Exception e) {
			LogWriter.logBasicWithException(LogWriter.ERROR, this,
					"PortalServlet.getPptNorms() failed to instantiate NormGroupHelper. projId=" + projId + "pptId="
							+ pptId,
					e);
			return null;
		}

		// get the ppt selected norms
		HashMap<String, ArrayList<NormGroup>> data = ngh.getSelectedNormGroups(pptId, projId);

		// Put out the info by instrument
		for (String inst : courses) {
			ArrayList<NormGroup> instNgs = data.get(inst);
			if (instNgs == null || instNgs.size() < 1) {
				// Nothing to do here... go on to the next
				continue;
			}

			xml += "<course abbv=\"" + inst + "\">";

			// put out the norms
			for (NormGroup ng : instNgs) {
				xml += "<normGroup id=\"" + ng.getId() + "\"" + " type=\""
						+ (ng.getPopulation() == NormGroup.GENERAL_POPULATION ? "gen" : "spec") + "Pop\"/>";
			}

			xml += "</course>";
		}

		return xml;
	}

	// -----------------------------------------------------
	// Set norm defaults request
	// -----------------------------------------------------

	// NOTES
	// -- The structure is based upon the list request structure with fewer
	// attributes processed.
	// -- Only the attributes shown in the example will be processed. additional
	// attributes are
	// ignored, data in them has no relevance to this process; e.g., the
	// "isSelected" attribute
	// has no effect.
	// -- If population types other than "genPop or "specPop are detected, they
	// are ignored.
	// -- If multiples of a valid population type are found in a course the
	// values contained in
	// the LAST one are used.
	// -- No tags other than <course> and <normGroup> are parsed. Attributes
	// other that
	// abbv (<course>), type, and id (both in <normGroup>) are ignored, no
	// matter what
	// those attributes are or what the data contained in them may imply.
	// -- Assumes that the data retured are valid... there will be no courses
	// that don't have
	// norms and no nonexistant or invalid norm IDs in the incoming data
	// -----------------------------------------------------
	/*
	 * current "Set project default norm"" request <SetProjectNormDefaults
	 * project_id = "557"> <course abbv = "gpil"> <normGroup id = "16" type =
	 * "genPop"/> <normGroup id = "17" type = "specPop"/> </course> <course abbv
	 * = "rv"> <normGroup id = "39" type = "genPop"/> <normGroup id = "70" type
	 * = "specPop"/> </course> </SetProjectNormDefaults>
	 *
	 * Returns "OK" or "Failed"
	 */
	private void processSetNormDefaultsRequest(Document doc, HttpServletResponse response) {
		boolean isOK = true; // Assume that that routine will succeed
		// int projId = 0;
		ArrayList<CourseNormsDTO> crsNorms = new ArrayList<CourseNormsDTO>();
		NodeList nodes = null;
		Node node = null;

		// Get the project ID
		node = doc.getDocumentElement();
		String projStr = XMLUtils.getAttributeValue(node, "project_id");
		if (projStr == null) {
			LogWriter.logBasic(LogWriter.ERROR, this,
					"PortalServlet.processSetNormDefaultsRequest():  Unable to fetch project ID.  Node:  "
							+ XMLUtils.nodeToString(node));
			isOK = false;
		}
		// else
		// {
		// try
		// {
		// projId = Integer.parseInt(pStr);
		// }
		// catch(NumberFormatException e)
		// {
		// LogWriter.logBasic(LogWriter.ERROR, this,
		// "PortalServlet.processSetNormDefaultsRequest(): Unable to convert
		// project ID to int: value="
		// + pStr);
		// isOK = false;
		// }
		// }

		if (isOK) {
			// Get the courses
			nodes = XMLUtils.getElements(node, "course");
			for (int i = 0; i < nodes.getLength(); i++) {
				node = nodes.item(i);
				String abbv = XMLUtils.getAttributeValue(node, "abbv");
				if (abbv == null) {
					// Course abbreviation required
					LogWriter.logBasic(LogWriter.WARNING, this,
							"PortalServlet.processSetNormDefaultsRequest():  projId= " + projStr
									+ ".  Unable to fetch course abbv from node:  " + XMLUtils.nodeToString(node));
					isOK = false;
					break;
				}
				// System.out.println("Processing course " + abbv);

				// Get the data. Should be a max of two (1 spec, 1 gen) per
				// course.
				CourseNormsDTO cn = new CourseNormsDTO(abbv);
				// Gets the norm groups in the current course
				NodeList ngNodes = XMLUtils.getElements(node, "normGroup");
				for (int j = 0; j < ngNodes.getLength(); j++) {
					Node ngNode = ngNodes.item(j);
					String pop = XMLUtils.getAttributeValue(ngNode, "type");
					if (pop == null) {
						// pop type not found... the <normGroup> is ignored
						// Not an error
						LogWriter.logBasic(LogWriter.INFO, this,
								"PortalServlet.processSetNormDefaultsRequest():  No pop type found.  ProjId=" + projStr
										+ ", crs=" + abbv + ",  Node:  " + XMLUtils.nodeToString(ngNode));
						continue;
					}
					if (!(pop.equals("specPop") || pop.equals("genPop"))) {
						// invalid pop type... ignore
						LogWriter.logBasic(LogWriter.INFO, this,
								"PortalServlet.processSetNormDefaultsRequest():  Invalid population type.  ProjId="
										+ projStr + ", crs=" + abbv + ",  pop type=" + pop);
						continue;
					}

					// We have a valid type.get the ID
					String id = XMLUtils.getAttributeValue(ngNode, "id");
					if (pop.equals("specPop")) {
						cn.setSpNgId(id);
					} else if (pop.equals("genPop")) {
						cn.setGpNgId(id);
					}
				} // End of norm group loop

				crsNorms.add(cn);
			} // End of course loop
		} // End of if(isOK)

		// Do the actual maintenance
		INormGroupHelper ngHelper = null;
		if (isOK) {
			// get the helper
			try {
				ngHelper = HelperDelegate.getNormGroupHelper("com.pdi.data.v2.helpers.delegated");
			} catch (Exception e) {
				LogWriter.logBasicWithException(LogWriter.ERROR, this,
						"PortalServlet.processSetNormDefaultsRequest() failed to instantiate NormGroupHelper. projId="
								+ projStr,
						e);
				isOK = false;
			}
		}
		if (isOK) {
			// Loop through the incoming norms to set
			for (CourseNormsDTO dto : crsNorms) {
				String abbv = dto.getCourse();
				int newGp = dto.getGpNgId();
				int newSp = dto.getSpNgId();
				if (newGp == 0 && newSp == 0) {
					// Ignore if couse coming in with no norms set
					// NOTE: If the business wishes to allow deletion of project
					// default norms,
					// we can add code here to delete the default row (pptId =
					// 0) for this
					// project and instrument.
					continue; // Next course
				}

				// We neet to set at least one project default...
				NormGroup ng;
				String pptId = "0"; // 0 = ppt ID of project default norm
				if (newGp != 0) {
					// Process the genPop
					ng = new NormGroup();
					ng.setPopulation(NormGroup.GENERAL_POPULATION);
					ng.setId(newGp);
					ng.setInstrumentCode(abbv);
					ngHelper.update(null, ng, projStr, pptId, abbv);
					// Ripple change into participants
					isOK &= ngHelper.updatePptNormByProjIdNormGroup(projStr, ng);
				}
				if (newSp != 0) {
					// Process the specPop
					ng = new NormGroup();
					ng.setPopulation(NormGroup.SPECIAL_POPULATION);
					ng.setId(newSp);
					ng.setInstrumentCode(abbv);
					ngHelper.update(null, ng, projStr, pptId, abbv);
					// Ripple change into participants
					isOK &= ngHelper.updatePptNormByProjIdNormGroup(projStr, ng);
				}
			}
		}

		// Push the status back
		response.setContentType("text/plain");
		PrintWriter out;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			LogWriter.logBasicWithException(LogWriter.WARNING, this,
					"PortalServlet.processSetNormDefaultsRequest() unable to fetch response writer.", e);
			return;
		}
		out.write(isOK ? "OK" : "Failed");

		// scram
		return;
	}

	// -----------------------------------------------------
	// Set participant norm request
	// -----------------------------------------------------

	/*
	 * Current "Set participant norm" request <SetParticipantNorms project_id =
	 * "557"> <participant id = "123456"> <course abbv = "gpil"> <normGroup id =
	 * "101" type = "specPop"/> <normGroup id = "103" type = "genPop"/>
	 * </course> <course abbv = "rv"> <normGroup id = "102" type = "specPop"/>
	 * <normGroup id = "104" type = "genPop"/> </course> </participant>
	 * <participant id = "654321"> <course abbv = "rv"> <normGroup id = "102"
	 * type = "specPop"/> <normGroup id = "104" type = "genPop"/> </course>
	 * <course abbv = "gpil"> <normGroup id = "101" type = "specPop"/>
	 * <normGroup id = "103" type = "genPop"/> </course> </participant>
	 * </SetParticipantNorms>
	 */
	/**
	 * processSetParticipantNorms - Set the selected participant norms HTTP
	 * request returns "OK" or "Failed"
	 *
	 * @param
	 * @param
	 */
	private void processSetParticipantNorms(Document doc, HttpServletResponse response) {
		boolean isOK = true; // Assume that that routine will succeed
		String projId = null;
		String pptId = null;

		// Get a NormGroupHlper
		INormGroupHelper ngh = null;

		try {
			ngh = HelperDelegate.getNormGroupHelper("com.pdi.data.v2.helpers.delegated");
		} catch (Exception e) {
			LogWriter.logBasicWithException(LogWriter.ERROR, this,
					"PortalServlet.processSetParticipantNorms() failed to instantiate NormGroupHelper.", e);
			isOK = false;
		}

		Node prNode = null;
		if (isOK) {
			// Get the project ID
			prNode = doc.getDocumentElement();
			projId = XMLUtils.getAttributeValue(prNode, "project_id");
			if (projId == null) {
				LogWriter.logBasic(LogWriter.ERROR, this,
						"PortalServlet.processSetParticipantNorms():  Unable to fetch project ID.  Node:  "
								+ XMLUtils.nodeToString(prNode));
				isOK = false;
			}
		}

		if (isOK) {
			// Gets the participants
			NodeList pptNodes = XMLUtils.getElements(prNode, "participant");
			for (int i = 0; i < pptNodes.getLength(); i++) {
				Node pptNode = pptNodes.item(i);
				// Get the ppt ID
				pptId = XMLUtils.getAttributeValue(pptNode, "id");
				if (pptId == null) {
					// Participant ID is required for this ppt, We could have
					// multiple participants; don't kill the run just skip this
					// one.
					LogWriter.logBasic(LogWriter.WARNING, this, "PortalServlet.processSetParticipantNorms():  projId= "
							+ projId + ".  Unable to fetch ppt ID from node:  " + XMLUtils.nodeToString(pptNode));
					isOK = false;
					continue;
				}

				// We have the ppt... process the course
				// Gets the courses in the participant
				NodeList cNodes = XMLUtils.getElements(pptNode, "course");
				for (int j = 0; j < cNodes.getLength(); j++) {
					Node crsNode = cNodes.item(j);
					String abbv = XMLUtils.getAttributeValue(crsNode, "abbv");
					if (abbv == null) {
						// Course abbreviation required
						LogWriter.logBasic(LogWriter.WARNING, this,
								"PortalServlet.processSetParticipantNorms():  projId= " + projId
										+ ".  Unable to fetch course abbv from node:  "
										+ XMLUtils.nodeToString(crsNode));
						isOK = false;
						// ignore this course
						break;
					}

					// Get the norm data. Should be a max of two (1 spec, 1 gen)
					// per course.
					int spId = 0;
					int gpId = 0;
					NodeList ngNodes = XMLUtils.getElements(crsNode, "normGroup");
					for (int k = 0; k < ngNodes.getLength(); k++) {
						Node ngNode = ngNodes.item(k);
						String pop = XMLUtils.getAttributeValue(ngNode, "type");
						if (pop == null) {
							// pop type not found... the <normGroup> is
							// ignored...
							LogWriter.logBasic(LogWriter.INFO, this,
									"PortalServlet.processSetParticipantNorms():  No pop type found.  ProjId=" + projId
											+ ", crs=" + abbv + ",  Node:  " + XMLUtils.nodeToString(ngNode));
							continue;
						}
						if (!(pop.equals("specPop") || pop.equals("genPop"))) {
							// invalid pop type... ignore
							LogWriter.logBasic(LogWriter.INFO, this,
									"PortalServlet.processSetParticipantNorms():  Invalid population type.  ProjId="
											+ projId + ", crs=" + abbv + ",  pop type=" + pop);
							continue; // Next norm group
						}

						// We have a valid type.get the ID
						String idStr = XMLUtils.getAttributeValue(ngNode, "id");
						if (idStr == null) {
							// No id... ignore
							LogWriter.logBasic(LogWriter.INFO, this,
									"PortalServlet.processSetParticipantNorms():  Missing norm group ID.  ProjId="
											+ projId + ", crs=" + abbv + ",  pop type=" + pop);
							continue; // Next norm group
						}
						int id = Integer.parseInt(idStr);
						if (id == 0) {
							// invalid id... ignore
							LogWriter.logBasic(LogWriter.INFO, this,
									"PortalServlet.processSetParticipantNorms():  Invalid orm group ID.  ProjId="
											+ projId + ", crs=" + abbv + ",  pop type=" + pop);
							continue; // Next norm group
						}

						// Now we have an id, and it is either genPop or specPop
						if (pop.equals("genPop"))
							gpId = id;
						else
							spId = id;
					} // End of norm group loop

					// Note that no check is made to ensure both norms are set.
					// That is because
					// some instruments (e.g., cs and cs2) have only one norm.
					if (gpId == 0 && spId == 0) {
						// We need at least one norm to set... ignore this set
						LogWriter.logBasic(LogWriter.INFO, this,
								"PortalServlet.processSetParticipantNorms():  Both norm IDs == 0.  ProjId=" + projId
										+ ", crs=" + abbv + ".");
						continue; // Next course
					}

					// Now we are ready to rumble... insert/update the norms
					// NOTE: Norms are alwyas updated, even if they are the same
					// as they were
					// previously and even if they are the same as the project
					// defaults.
					// If they are zero, however, don't bother.
					NormGroup ng;
					// First the genPop...
					if (gpId != 0) {
						ng = new NormGroup();
						ng.setPopulation(NormGroup.GENERAL_POPULATION);
						ng.setId(gpId);
						ngh.update(null, ng, projId, pptId, abbv);
					}
					// ...Then the specPop
					if (spId != 0) {
						ng = new NormGroup();
						ng.setPopulation(NormGroup.SPECIAL_POPULATION);
						ng.setId(spId);
						ngh.update(null, ng, projId, pptId, abbv);
					}
				} // End of course loop
			} // End of participant loop
		} // End of if(isOK)

		// Push the status back
		response.setContentType("text/plain");
		PrintWriter out;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			LogWriter.logBasicWithException(LogWriter.WARNING, this,
					"PortalServlet.processSetParticipantNorms() unable to fetch response writer.", e);
			return;
		}
		out.write(isOK ? "OK" : "Failed");

		// scram
		return;
	}

	/**
	 * processForceExtractCand - Push out extract candiddates for ppt with
	 * uncompleted DRIs and aged IGs
	 *
	 * @param doc
	 * @param response
	 */
	private void processForceExtractCand(Document doc, HttpServletResponse response) {
		boolean isOK = true;
		boolean hasData = true;
		int candCnt = 0;
		int procCnt = 0;

		// Get the initial list
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT ig.participantId, ");
		sb.append("       ig.dnaId, ");
		sb.append("       ec.extractStatus ");
		sb.append("  FROM pdi_abd_igrid_response ig ");
		sb.append("    INNER JOIN pdi_abd_rpt_input rdi ON ( rdi.dnaId = ig.dnaId ");
		sb.append("                                      AND rdi.participantId = ig.participantId ");
		sb.append(
				"                                      AND (rdi.rateStatusId != " + ReportInputDTO.COMPLETED + "  OR ");
		sb.append(
				"                                           rdi.oTxtStatusId != " + ReportInputDTO.COMPLETED + "  OR ");
		sb.append("                                           rdi.pTxtStatusId != " + ReportInputDTO.COMPLETED + ")) ");
		sb.append("    LEFT JOIN pdi_abd_extract_cand ec ON ( ec.dnaId = ig.dnaId ");
		sb.append("                                       AND ec.participantId = ig.participantId ) ");
		sb.append("    WHERE ig.lastDate < SUBDATE(CURDATE(), " + IG_AGE + ")");

		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
		if (dlps.isInError()) {
			LogWriter.logBasic(LogWriter.WARNING, this,
					"PortalServlet.processForceExtractCand() unable to create dlps.  code="
							+ dlps.getStatus().getExceptionCode() + ", msg=" + dlps.getStatus().getExceptionMessage());
			isOK = false;
		}

		DataResult dr = null;
		ResultSet rs = null;
		try {
			if (isOK) {
				// Get the data
				dr = V2DatabaseUtils.select(dlps);
				if (dr.hasNoData()) {
					// Our job is done here
					hasData = false;
				} else {
					if (dr.isInError()) {
						LogWriter.logBasic(LogWriter.WARNING, this,
								"PortalServlet.processForceExtractCand() error in SELECT.  status="
										+ dr.getStatus().getStatusCode() + ", error code="
										+ dr.getStatus().getExceptionCode() + ", error message="
										+ dr.getStatus().getExceptionMessage());
						isOK = false;
					} else {
						// Process the data
						rs = dr.getResultSet();
						try {
							if (rs == null || !rs.isBeforeFirst()) {
								hasData = false;
							}
						} catch (SQLException se) {
							LogWriter.logBasicWithException(LogWriter.WARNING, this,
									"PortalServlet.processForceExtractCand() error in initial data check.", se);
							isOK = false;
						}
					}
				}
			}

			ArrayList<ExtPair> cands = new ArrayList<ExtPair>();
			if (isOK && hasData) {
				try {
					while (rs.next()) {
						if (rs.getInt("extractStatus") == ResearchExtractHelper.EXT_CPLT) {
							// if extracted, skip this one
							continue;
						}

						ExtPair ep = new ExtPair();
						ep.setPptId(rs.getInt("participantId"));
						ep.setDnaId(rs.getInt("dnaId"));
						cands.add(ep);
					}
				} catch (SQLException se) {
					LogWriter.logBasicWithException(LogWriter.WARNING, this,
							"PortalServlet.processForceExtractCand() error in data fetch.", se);
					isOK = false;
				}
			}

			// We now have the candidate list... process it
			if (isOK) {
				// Get a ReportInputHelper
				ReportInputDataHelper riHelper = new ReportInputDataHelper();
				// ResearchExtractHelper reHelper = new ResearchExtractHelper();
				// Loop through it
				for (ExtPair ep : cands) {
					candCnt++;
					ReportInputDTO dto;
					// Create a ReportInputDTO (read it)
					try {
						dto = riHelper.getReportInput("" + ep.getPptId(), ep.getDnaId());
					} catch (Exception ex) {
						LogWriter.logBasicWithException(LogWriter.WARNING, this,
								"PortalServlet.processForceExtractCand() error getting ReportInputDTO...  pptId="
										+ ep.getPptId() + ", dnaId=" + ep.getDnaId(),
								ex);
						isOK = false;

						// Do the rest
						continue;
					}

					// set the status to COMPLETE
					dto.setOtxtStatusId(ReportInputDTO.COMPLETED);
					dto.setPtxtStatusId(ReportInputDTO.COMPLETED);
					dto.setRateStatusId(ReportInputDTO.COMPLETED);

					// update the DRI (to tweak the status and create the
					// extract
					// candidate row)
					try {
						riHelper.updateReportInput(null, dto);
					} catch (Exception ex) {
						LogWriter.logBasicWithException(LogWriter.WARNING, this,
								"PortalServlet.processForceExtractCand() error updating ReportInputDTO... pptId="
										+ ep.getPptId() + ", dnaId=" + ep.getDnaId(),
								ex);
						isOK = false;

						// Do the rest
						continue;
					}

					procCnt++;
				} // end of candidate loop
			}

			// Push the status back
			response.setContentType("text/plain");
			PrintWriter out;
			try {
				out = response.getWriter();
			} catch (IOException e) {
				LogWriter.logBasicWithException(LogWriter.WARNING, this,
						"PortalServlet.processSetParticipantNorms() unable to fetch response writer.", e);
				return;
			}

			String statStr = "Extract Candidate procesing complete.  ";
			statStr += (candCnt == 0) ? "No candidates detected."
					: "Successfully processed " + procCnt + " of " + candCnt + " candidates.";

			out.write(statStr);
		} finally {
			if (dr != null) {
				dr.close();
				dr = null;
			}
		}

		// scram
		return;
	}

	/*
	 * Lookup specified report label(s)
	 */
	private void reportLabelLookup(Document doc, HttpServletResponse response) {
		boolean isOK = true;
		String value = null;
		Node prNode = null;
		prNode = doc.getDocumentElement();
		System.out.println("XML request incoming = " + prNode.toString());
		String labelCode = XMLUtils.getAttributeValue(prNode, "labelId");
		String iso = XMLUtils.getAttributeValue(prNode, "lang");
		System.out.println("label = " + labelCode);
		System.out.println("iso = " + iso);
		if (labelCode == null || iso == null) {
			LogWriter.logBasic(LogWriter.ERROR, this,
					"PortalServlet.reportLabelLookup():  Unable to fetch report label.  Node:  "
							+ XMLUtils.nodeToString(prNode));
		}
		/*
		 * The first get the data in the simple fashion that we had envisioned�
		 * pass in a label code and a language abbreviation (ISO code):
		 */
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT txt.text");
		sb.append("  FROM pdi_abd_text txt ");
		sb.append("    WHERE textId = (SELECT textId FROM group_text_map ");
		sb.append("             WHERE labelCode = '" + labelCode + "')");
		sb.append("   AND (languageId = (SELECT languageId FROM pdi_abd_language");
		sb.append("    WHERE languageCode = '" + iso + "')");
		sb.append("          OR isDefault = 1)");
		sb.append("    ORDER BY isDefault ASC");
		System.out.println("query is " + sb.toString());
		/*
		 *
		 * The second is for those cases where there may be more than one text
		 * ID associated with a label and you need to specify a text group as
		 * well:
		 *
		 * SELECT txt.text FROM pdi_abd_text txt WHERE textId = (SELECT textId
		 * from group_text_map WHERE labelCode = 'ADVANCE_DRIVE' <--- The label
		 * code AND textGroupId = (SELECT textGroupId FROM text_group WHERE
		 * groupKey = 'ABYD_READ_RPT') ) <--- The text group AND (languageId =
		 * (SELECT languageId FROM pdi_abd_language WHERE languageCode = 'fr')
		 * <--- The language code OR isDefault = 1) ORDER BY isDefault ASC
		 */
		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
		if (dlps.isInError()) {
			LogWriter.logBasic(LogWriter.WARNING, this,
					"PortalServlet.processForceExtractCand() unable to create dlps.  code="
							+ dlps.getStatus().getExceptionCode() + ", msg=" + dlps.getStatus().getExceptionMessage());
			isOK = false;
		}

		DataResult dr = null;
		ResultSet rs = null;
		try {
			if (isOK) {
				// Get the data
				dr = V2DatabaseUtils.select(dlps);
				System.out.println("nodata = " + dr.hasNoData());
				if (dr.hasNoData()) {
					// Our job is done here
				} else {
					if (dr.isInError()) {
						LogWriter.logBasic(LogWriter.WARNING, this,
								"PortalServlet.reportLabelLookup() error in SELECT.  status="
										+ dr.getStatus().getStatusCode() + ", error code="
										+ dr.getStatus().getExceptionCode() + ", error message="
										+ dr.getStatus().getExceptionMessage());
						isOK = false;
					} else {
						// System.out.println("KEITH! 5 ");
						// Process the data
						rs = dr.getResultSet();
						try {
							if (rs == null || !rs.isBeforeFirst()) {
								// hasData = false;
							} else {
								rs.next();
								value = rs.getString("text");
							}
						} catch (SQLException se) {
							LogWriter.logBasicWithException(LogWriter.WARNING, this,
									"PortalServlet.reportLabelLookup() error in initial data check.", se);
							isOK = false;
						}
					}
				}

				// Push the label back
				// response.setContentType("text/html");
				response.setContentType("text/xml;charset=UTF-8");
				PrintWriter out;
				try {
					out = response.getWriter();
				} catch (IOException e) {
					System.out.println(
							"Warning:  PortalServlet.processFetchRequest() unable to fetch response writer.  msg="
									+ e.getMessage());
					LogWriter.logBasicWithException(LogWriter.WARNING, this,
							"PortalServlet.processFetchRequest() unable to fetch response writer.", e);
					return;
				}
				out.write(value);
			}
		} finally {
			if (dr != null) {
				dr.close();
				dr = null;
			}
		}

		// scram
		return;
	}

	/**
	 * Clones the abyd data from one project to another. Makes the following
	 * assumptions: 1) Both projects are in the same client 2) Caller passes
	 * correct project IDs
	 *
	 * Input XML looks like this: <CloneDna source="390" destination="444"/>
	 * Output XML looks like this: <CloneResponse okStatus="true" /> OR
	 * <CloneResponse okStatus="false">Error message goes here</CloneResponse>
	 *
	 * @param doc
	 * @param response
	 */
	private void cloneDna(Document doc, HttpServletResponse response) {
		Connection con = null;
		this.cloneDnaId = 0; // Clean out the generated ID

		try {
			// Validate the inputs
			RequestStatus stat = checkCloneInputs(doc);

			// Get a db connection
			if (stat.isOk()) {
				try {
					con = AbyDDatabaseUtils.getDBConnection();
				} catch (Exception e) {
					LogWriter.logBasicWithException(LogWriter.WARNING, this,
							"PortalServlet.cloneSetup1() error getting connection.", e);
					stat.setStatusCode(RequestStatus.RS_ERROR);
					stat.setExceptionMessage("Error establishing connection for cloning.");
				}
			}

			if (stat.isOk()) {
				// Do the setup1 stuff
				stat = cloneSetup1(con);
			}
			if (stat.isOk()) {
				// Now do the setup2 stuff
				stat = cloneSetup2(con);
			}
			if (stat.isOk()) {
				// And finally, do the norms stuff
				stat = cloneNorms(con);
			}

			// Put out the response
			// Create the output xml
			String outXml = "<CloneResponse okStatus=\"" + stat.isOk() + "\">"
					+ (stat.isOk() ? "" : stat.getExceptionMessage()) + "</CloneResponse>";
			PrintWriter out = null;
			try {
				out = response.getWriter();
			} catch (IOException e) {
				e.printStackTrace();
			}
			response.setContentType("text/xml;charset=UTF-8");
			out.write(outXml);
			return;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
				}
				con = null;
			}
		}
	}

	/**
	 * checkCloneInputs - Does the parameter checking for the DNA cloning method
	 *
	 * Request status is used to return status and messages. While not exactly a
	 * correct fit, it has the needed functionality so it was used.
	 *
	 * @param doc
	 * @return
	 */
	private RequestStatus checkCloneInputs(Document doc) {
		RequestStatus ret = new RequestStatus();
		int srcId = 0;
		int destId = 0;

		// Get the parameters
		// Is the source project ID present?
		Node cdNode = doc.getDocumentElement();
		String srcStr = XMLUtils.getAttributeValue(cdNode, "source");
		if (srcStr == null) {
			String msg = "No source project ID was supplied";
			return cloneError(msg, null);
		}

		// How about the destination ID?
		String destStr = XMLUtils.getAttributeValue(cdNode, "destination");
		if (destStr == null) {
			String msg = "No destination project ID was supplied";
			return cloneError(msg, null);
		}

		// See if the IDs are numeric
		try {
			srcId = Integer.parseInt(srcStr);
		} catch (NumberFormatException e) {
			String msg = "Source project ID (" + srcStr + ") is an invalid number";
			return cloneError(msg, null);
		}
		try {
			destId = Integer.parseInt(destStr);
		} catch (NumberFormatException e) {
			String msg = "Destination project ID (" + destStr + ") is an invalid number";
			return cloneError(msg, null);
		}
		System.out.println("Target IDs:  src=" + srcId + ", dest=" + destId);

		// Now check to see if the IDs are logically consistent (src exists and
		// dest does not)
		DataLayerPreparedStatement dlps = null;
		StringBuffer sb = new StringBuffer();
		DataResult dr = null;
		ResultSet rs = null;

		try {
			sb.append("SELECT COUNT(*) as cnt");
			sb.append("  FROM pdi_abd_dna ");
			sb.append("    WHERE projectId = ?");
			System.out.println("query is " + sb.toString());

			dlps = V2DatabaseUtils.prepareStatement(sb);
			if (dlps.isInError()) {
				String msg = "Error in SELECT.  status=" + dlps.getStatus().getStatusCode() + ", error code="
						+ dlps.getStatus().getExceptionCode() + ", error message="
						+ dlps.getStatus().getExceptionMessage();
				return cloneError(msg, null);
			}

			PreparedStatement ps = dlps.getPreparedStatement();
			try {
				ps.setInt(1, srcId);
			} catch (SQLException e) {
				String msg = "Unable to set source ID parameter in project check statement.";
				return cloneError(msg, e);
			}

			// Get something...
			dr = V2DatabaseUtils.select(dlps);
			if (dr.isInError()) {
				String msg = "Error in src SELECT.  status=" + dr.getStatus().getStatusCode() + ", error code="
						+ dr.getStatus().getExceptionCode() + ", error message=" + dr.getStatus().getExceptionMessage();

				return cloneError(msg, null);
			}
			// Got it... process it
			boolean gotIt = false;
			rs = dr.getResultSet();
			try {
				rs.next();
				gotIt = rs.getBoolean("cnt");
			} catch (SQLException e) {
				String msg = "Unable to fetch src project exisitence data count.";
				return cloneError(msg, e);
			}
			if (!gotIt) {
				ret.setStatusCode(RequestStatus.RS_ERROR);
				ret.setExceptionMessage("Unable to clone; source project does not exist in A by D.");
				return ret;
			}

			// Now check the dest
			try {
				ps.setInt(1, destId);
			} catch (SQLException e) {
				String msg = "Error setting dest ID (" + destId + ").  error code=" + e.getErrorCode()
						+ ", error message=" + e.getMessage();
				return cloneError(msg, e);
			}

			// Get something...
			dr = V2DatabaseUtils.select(dlps);
			if (dr.isInError()) {
				String msg = "Error in dest SELECT.  status=" + dr.getStatus().getStatusCode() + ", error code="
						+ dr.getStatus().getExceptionCode() + ", error message=" + dr.getStatus().getExceptionMessage();
				return cloneError(msg, null);
			}
			// Got it... process it
			gotIt = false;
			rs = dr.getResultSet();
			try {
				rs.next();
				gotIt = rs.getBoolean("cnt");
			} catch (SQLException e) {
				String msg = "Unable to fetch dest project exisitence data (count).";
				return cloneError(msg, e);
			}
			if (gotIt) {
				ret.setStatusCode(RequestStatus.RS_ERROR);
				ret.setExceptionMessage("Unable to clone; destination project already exists in A by D.");
				return ret;
			}
		} finally {
			if (dr != null) {
				dr.close();
				dr = null;
			}
		}
		this.cloneSrcProjId = srcId;
		this.cloneDestProjId = destId;
		return ret;
	}

	/**
	 * cloneSetup1 - Copy the content of a project and make a new project (DNA)
	 * from it... Setup1 only. NOTE: Insofar as possible, I used the helpers
	 * that the Flex Setup routines uses
	 *
	 * @return
	 */
	private RequestStatus cloneSetup1(Connection con) {
		RequestStatus ret = new RequestStatus();

		// get the Source data (both description and structure)
		EntryStateHelper srcStateHelper;
		DNAStructureHelper srcDnaHelper;
		EntryStateDTO srcDescData;
		ArrayList<DNACellDTO> linkAry;
		try {
			// Description...
			srcStateHelper = new EntryStateHelper(con, "" + this.cloneSrcProjId);
			srcDescData = srcStateHelper.getEntryStateData();
			if (!srcDescData.getDnaDesc().get_modelActive()){
				throw new Exception("Model is not active.");
			}
			// ...and links
			srcDnaHelper = new DNAStructureHelper(con, srcDescData.getDnaDesc().getDnaId());
			linkAry = srcDnaHelper.getDnaLinks();
		} catch (Exception e) {
			String msg = "Error fetching source data (proj=" + this.cloneSrcProjId + ").";
			return cloneError(msg, e);
		}
		this.sourceDnaId = srcDescData.getDnaDesc().getDnaId();
		if (this.sourceDnaId == 0) {
			ret.setStatusCode(RequestStatus.RS_ERROR);
			ret.setExceptionMessage("Error fetching source DNADescription data; DNA ID = 0");
			return ret;
		}

		// Get setup destination data (essentially just a skeleton)
		EntryStateHelper destStateHelper;
		EntryStateDTO destDescData;
		try {
			destStateHelper = new EntryStateHelper(con, "" + this.cloneDestProjId);
			destDescData = destStateHelper.getEntryStateData();
		} catch (Exception e) {
			String msg = "Error fetching dest data (proj=" + this.cloneDestProjId + ").";
			return cloneError(msg, e);
		}

		// Transfer initial decription data to dest and write it.
		// Some of this information doesn't get written till later.
		DNADescription ddd = destDescData.getDnaDesc();
		ddd.setDnaComplete(Boolean.FALSE);
		ddd.setModelId(srcDescData.getDnaDesc().getModelId());
		ddd.setModelName(srcDescData.getDnaDesc().getModelName());
		ddd.setSynthNorm(srcDescData.getDnaDesc().getSynthNorm());
		ddd.setWbVersion(srcDescData.getDnaDesc().getWbVersion());
		ddd.setDnaName(destDescData.getJobName());
		String notes = srcDescData.getDnaDesc().getDnaNotes();
		String cloned = (notes == null || notes.length() < 1 ? "" : notes + "\n\n ");
		cloned += "Cloned from project/DNA " + srcDescData.getDnaDesc().getJobId() + "/"
				+ srcDescData.getDnaDesc().getDnaId();
		ddd.setDnaNotes(cloned);

		// Write out the new DNA desription data (helper reused later)
		SaveDNADescHelper sddHelper;
		try {
			sddHelper = new SaveDNADescHelper(con, ddd);
			ddd = sddHelper.saveNameModelCmgr();
		} catch (Exception e) {
			String msg = "Error setting dest DNA Description data (src DNA=" + this.sourceDnaId + ").";
			return cloneError(msg, e);
		}
		// Capture the new DNA ID for later use
		this.cloneDnaId = ddd.getDnaId();

		// Shouldn't happen, but let's just check
		if (this.cloneDnaId == 0) {
			ret.setStatusCode(RequestStatus.RS_ERROR);
			ret.setExceptionMessage("Error in DNADescription data; new DNA ID = 0");
			return ret;
		}

		// And the new intersections for that DNA (same as the src)
		SaveDNAStructureHelper sdsHelper;
		// Save the data
		try {
			sdsHelper = new SaveDNAStructureHelper(con, ddd.getDnaId(), linkAry);
			sdsHelper.saveStructure();
		} catch (Exception e) {
			String msg = "PortalServlet.cloneSetup1() error setting dest intersection data (DNA=" + this.cloneDnaId
					+ ").";
			return cloneError(msg, e);
		}
		sdsHelper = null;

		// Write out the final DNS info
		try {
			sddHelper.saveMoreDNAData();
		} catch (Exception e) {
			String msg = "Error setting final dest descriptive data (DNA=" + this.cloneDnaId + ").";
			return cloneError(msg, e);
		}

		return ret;
	}

	/**
	 * cloneSetup2 - Copy the content of a project and make a new project (DNA)
	 * from it... Setup2 only.
	 *
	 * NOTE: Insofar as possible, I used the helpers that the Flex Setup
	 * routines uses
	 *
	 * @return
	 */
	private RequestStatus cloneSetup2(Connection con) {
		RequestStatus ret = new RequestStatus();

		// Check the DNA ID
		if (this.cloneDnaId == 0) {
			ret.setStatusCode(RequestStatus.RS_ERROR);
			ret.setExceptionMessage("The target DNA ID has not been set.");
			return ret;
		}

		// -- Do Model (Report or client version) --
		ReportModelStructureDTO rms;
		ReportModelDataHelper reportModelDataHelper = new ReportModelDataHelper();
		// get the source data
		try {
			rms = reportModelDataHelper.getReportModel(con, this.sourceDnaId);
		} catch (Exception e) {
			String msg = "Error fetching client model description (DNA=" + this.sourceDnaId + ").";
			return cloneError(msg, e);
		}

		// clone the user text
		ret = reportModelDataHelper.cloneModelUserText(rms);
		if (!ret.isOk()) {
			return ret;
		}

		// Write out the report model
		rms.setDnaId(this.cloneDnaId);
		try {
			reportModelDataHelper.replaceReportModel(con, rms, ReportModelDataHelper.KEEP_ALL_LANG_TXT);
		} catch (Exception e) {
			String msg = "Error cloning the client model sctrucure (DNA=" + this.cloneDnaId + ").";
			return cloneError(msg, e);
		}
		reportModelDataHelper = null;

		// -- Labels --
		ReportLabelsDataHelper reportLabelsDataHelper;
		Setup2ReportLabelsDTO lblData = null;
		try {
			reportLabelsDataHelper = new ReportLabelsDataHelper();
			lblData = reportLabelsDataHelper.getSetupReportLabelData(this.sourceDnaId, SetupConstants.ALL_LABELS);
		} catch (Exception e) {
			String msg = "Error reading label data (DNA=" + this.sourceDnaId + ").";
			return cloneError(msg, e);
		}

		// update the data
		lblData.setDnaId(cloneDnaId);
		try {
			reportLabelsDataHelper.updateReportLabelData(lblData);
		} catch (Exception e) {
			String msg = "Error writing label data (DNA=" + this.cloneDnaId + ").";
			return cloneError(msg, e);
		}
		reportLabelsDataHelper = null;

		// -- Options --
		ReportOptionsDataHelper reportOptionsDataHelper = new ReportOptionsDataHelper();
		Setup2ReportOptionsDTO opts = null;
		try {
			opts = reportOptionsDataHelper.getReportOptionData(con, this.sourceDnaId);
		} catch (Exception e) {
			String msg = "Error fetching options data (DNA=" + this.sourceDnaId + ").";
			return cloneError(msg, e);
		}
		opts.setDnaId(this.cloneDnaId);
		try {
			reportOptionsDataHelper.updateReportOptionsData(con, opts);
		} catch (Exception e) {
			String msg = "Error writing options data (DNA=" + this.cloneDnaId + ").";
			return cloneError(msg, e);
		}
		reportOptionsDataHelper = null;

		// Note
		ReportNoteDataHelper reportNoteDataHelper = new ReportNoteDataHelper();
		ReportNoteDTO note = null;
		// get the data
		try {
			note = reportNoteDataHelper.getReportNote(con, this.sourceDnaId);
			String str = note.getText();
			str += (str.length() < 1) ? "" : " \n\n";
			str += "Cloned from project/DNA " + this.cloneSrcProjId + "/" + this.sourceDnaId;
			note.setDnaId(this.cloneDnaId);
			note.setText(str);
			reportNoteDataHelper.saveReportNote(con, note);
		} catch (Exception e) {
			String msg = "Error cloning note (DNA: src=" + this.sourceDnaId + ", dest=" + this.cloneDnaId + ").";
			return cloneError(msg, e);
		}

		return ret;
	}

	/**
	 * cloneNorms - Copy the project level norms from the source project to the
	 * target project.
	 *
	 * @return
	 */
	private RequestStatus cloneNorms(Connection con) {
		// Read up the default in the current project
		INormGroupHelper ngHelper = null;
		// get the helper
		try {
			ngHelper = HelperDelegate.getNormGroupHelper("com.pdi.data.v2.helpers.delegated");
		} catch (Exception e) {
			String msg = "Error instaniatinf NormGroupHelper for norm clone (proj src=" + this.cloneSrcProjId
					+ ", dest=" + this.cloneDestProjId + ").";
			return cloneError(msg, e);
		}

		// get the data
		HashMap<String, ArrayList<NormGroup>> dfltNormGroups = null;
		dfltNormGroups = ngHelper.getDefaultNormGroupMap("" + this.cloneSrcProjId);
		if (dfltNormGroups.isEmpty()) {
			// Nothing moer to do... scram
			return new RequestStatus();
		}

		// persist the data
		// Insert from logic in processSetNormDefaultsRequest (here)
		// this.cloneSrcProjId = srcId;
		// this.cloneDestProjId
		/*
		ng = new NormGroup();
		ng.setPopulation(NormGroup.SPECIAL_POPULATION);
		ng.setId(newSp);
		ng.setInstrumentCode(abbv);
		ngHelper.update(null, ng, projStr, pptId, abbv);
		*/
		for (Iterator<Map.Entry<String, ArrayList<NormGroup>>> itr = dfltNormGroups.entrySet().iterator(); itr
				.hasNext();) {
			Map.Entry<String, ArrayList<NormGroup>> ent = itr.next();
			String abbv = ent.getKey();
			ArrayList<NormGroup> list = ent.getValue();
			for (NormGroup ng : list) {
				if (ng.getId() == 0) {
					continue;
				}
				ng.setInstrumentCode(abbv);
				ngHelper.update(null, ng, "" + this.cloneDestProjId, "0", abbv);
			}
		}

		return new RequestStatus();
	}

	/**
	 * cloneError - Convenience method to Log an error and set up a
	 * RequestStatus object for return
	 *
	 * @param msg
	 *            Explanatory message
	 * @param e
	 *            Exception thrown to generate the error. If "null" the error
	 *            was not derived directly from an Exception and a different
	 *            logger is used.
	 * @return
	 */
	private RequestStatus cloneError(String msg, Exception e) {
		RequestStatus ret = new RequestStatus();
		if (e == null) {
			LogWriter.logBasic(LogWriter.WARNING, this, "PortalServlet DNA Clone):  " + msg);
		} else {
			LogWriter.logBasicWithException(LogWriter.WARNING, this, "PortalServlet DNA Clone):  " + msg, e);
		}
		ret.setStatusCode(RequestStatus.RS_ERROR);
		ret.setExceptionMessage(msg);

		return ret;
	}

	/*
	 * Fetch the requisite sort information
	 * There currently is only one type - analytics
	 */
	private void fetchSortInfo(Document doc, HttpServletResponse response) {
		boolean isOK = true;
		Node theNode = doc.getDocumentElement();
		String type = XMLUtils.getAttributeValue(theNode, "type");
		if (type == null) {
			LogWriter.logBasic(LogWriter.ERROR, this,
					"PortalServlet.fetchSortInfo():  Unable to fetch type.  Node:  " + XMLUtils.nodeToString(theNode));
		}
		if (!type.equals("analytics")) {
			LogWriter.logBasic(LogWriter.ERROR, this, "PortalServlet.fetchSortInfo():  Invalid data type (" + type
					+ " requested.  Node:  " + XMLUtils.nodeToString(theNode));
		}

		/*
		 * Getting all the data with a single query
		 */
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT sg.sortGroupId, sg.sortGroupName, ss.sortId, ss.sortName ");
		sb.append("  FROM pdi_abd_analytics_sort_group sg ");
		sb.append("    LEFT JOIN pdi_abd_analytics_sort ss ON ss.sortGroupId = sg.sortGroupId ");
		sb.append("  ORDER BY sg.seq, ss.seq");
		System.out.println("query is " + sb.toString());

		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
		if (dlps.isInError()) {
			LogWriter.logBasic(LogWriter.WARNING, this, "PortalServlet.fetchSortInfo() unable to create dlps.  code="
					+ dlps.getStatus().getExceptionCode() + ", msg=" + dlps.getStatus().getExceptionMessage());
			isOK = false;
		}

		DataResult dr = null;
		ResultSet rs = null;
		String outXml = "";
		try {
			if (isOK) {
				// Get the data
				dr = V2DatabaseUtils.select(dlps);
				// System.out.println("nodata = " + dr.hasNoData());
				if (dr.hasNoData()) {
					// Our job is done here
				} else {
					if (dr.isInError()) {
						LogWriter.logBasic(LogWriter.WARNING, this,
								"PortalServlet.fetchSortInfo() error in SELECT.  status="
										+ dr.getStatus().getStatusCode() + ", error code="
										+ dr.getStatus().getExceptionCode() + ", error message="
										+ dr.getStatus().getExceptionMessage());
						isOK = false;
					} else {
						// Process the data
						rs = dr.getResultSet();
						try {
							if (rs == null || !rs.isBeforeFirst()) {
								// No data... our job is done
							} else {
								int curGrp = 0;
								outXml += "<sortInfo><sortGroups>";
								while (rs.next()) {
									// build the XML
									int grp = rs.getInt("sortGroupId");
									if (grp != curGrp) {
										if (curGrp != 0) {
											outXml += "</sortItems></sortGroup>";
										}
										String grpName = rs.getString("sortGroupName");
										outXml += "<sortGroup id=\"" + grp + "\" displayName=\"" + grpName
												+ "\"><sortItems>";
										curGrp = grp;
									}
									int id = rs.getInt("sortId");
									String str = rs.getString("sortName");
									outXml += "<sortItem id=\"" + id + "\" displayName=\"" + str + "\"/>";
								}
								outXml += "</sortItems></sortGroup>";
								outXml += "</sortGroups></sortInfo>";
							}
						} catch (SQLException se) {
							LogWriter.logBasicWithException(LogWriter.WARNING, this,
									"PortalServlet.fetchSortInfo() error in sort data fetch.", se);
							isOK = false;
						}
					} // Else on dr.hasNoData
				} // outer "isOK"

				// Push the xml back
				response.setContentType("text/xml;charset=UTF-8");
				PrintWriter out;
				try {
					out = response.getWriter();
				} catch (IOException e) {
					LogWriter.logBasicWithException(LogWriter.WARNING, this,
							"PortalServlet.fetchSortInfo() unable to fetch response writer.", e);
					return;
				}
				out.write(outXml);
			}
		} finally {
			if (dr != null) {
				dr.close();
				dr = null;
			}
		}
		// scram
		return;
	}

	/*
	 * Fetch the requisite sort information
	 * There currently is only one type - analytics
	 */
	private ExtractParams fillInSortNames(ExtractParams params) {
		// Prefill output strings with failure info
		params.setSortGroupName("N/A");
		params.setSortDispName("N/A");

		/*
		 * Getting all the data with a single query
		 */
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT ss.sortName, ");
		sb.append("       sg.sortGroupName ");
		sb.append("  FROM pdi_abd_analytics_sort ss ");
		sb.append("    LEFT JOIN pdi_abd_analytics_sort_group sg ON sg.sortGroupId = ss.sortGroupId ");
		sb.append("  WHERE sortId = " + params.getSortId());

		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
		if (dlps.isInError()) {
			LogWriter.logBasic(LogWriter.WARNING, this, "PortalServlet.fillInSortNames() unable to create dlps.  code="
					+ dlps.getStatus().getExceptionCode() + ", msg=" + dlps.getStatus().getExceptionMessage());
			return params;
		}

		DataResult dr = null;
		ResultSet rs = null;
		try {
			// Get the data
			dr = V2DatabaseUtils.select(dlps);
			if (dr.hasNoData()) {
				// Our job is done here
				return params;
			}

			if (dr.isInError()) {
				LogWriter.logBasic(LogWriter.WARNING, this,
						"PortalServlet.fillInSortNames() error in SELECT.  status=" + dr.getStatus().getStatusCode()
								+ ", error code=" + dr.getStatus().getExceptionCode() + ", error message="
								+ dr.getStatus().getExceptionMessage());
				return params;
			}

			// Process the data
			rs = dr.getResultSet();
			try {
				if (rs == null || !rs.isBeforeFirst()) {
					// No data... our job is done
					return params;
				}

				rs.next();
				params.setSortGroupName(rs.getString("sortGroupName"));
				params.setSortDispName(rs.getString("sortName"));
			} catch (SQLException se) {
				LogWriter.logBasicWithException(LogWriter.WARNING, this,
						"PortalServlet.fillInSortNames() error in data fetch.", se);
			}
		} finally {
			if (dr != null) {
				dr.close();
				dr = null;
			}
		}

		// scram
		return params;
	}

	/**
	 * getProjectID - Get the projectId from the DnaId
	 */
	private String getProjectIdFromDna(String dnaId) {
		int prjId = 0;

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT projectId ");
		sb.append("	 FROM pdi_abd_dna ");
		sb.append("  WHERE dnaId = " + dnaId);

		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
		if (dlps.isInError()) {
			System.out.println("Warning:  PortalServlet.getProjectID() failed to prepare the SQL.  " + "dnaId=" + dnaId
					+ ".  " + dlps.toString());
			return null;
		}
		DataResult dr = null;
		ResultSet rs = null;

		try {
			dr = V2DatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			if (rs == null || !rs.isBeforeFirst()) {
				System.out.println("Warning:  PortalServlet.getProjectID() did not retrieve norms.  " + "dnaId=" + dnaId
						+ ".  " + dlps.toString());
				return null;
			}

			while (rs.next()) {
				prjId = rs.getInt("projectId");

			}

			String projectId = "" + prjId;

			return projectId;

		} catch (java.sql.SQLException se) {
			System.out.println("Error:  PortalServlet.getProjectID() resultSet fetch.  msg=" + se.getMessage());
			return null;
		} finally {
			if (dr != null) {
				dr.close();
				dr = null;
			}
		}
	}

	/**
	 * getProjectID - Get the projectId from the DnaId
	 */
	private String getDnaIdFromProjectId(String projId) {
		int dnaId = 0;

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT dnaId ");
		sb.append("	 FROM pdi_abd_dna ");
		sb.append("  WHERE projectId = " + projId);

		DataLayerPreparedStatement dlps = V2DatabaseUtils.prepareStatement(sb);
		if (dlps.isInError()) {
			System.out.println("Warning:  PortalServlet.getProjectID() failed to prepare the SQL.  " + "projId="
					+ projId + ".  " + dlps.toString());
			return null;
		}
		DataResult dr = null;
		ResultSet rs = null;

		try {
			dr = V2DatabaseUtils.select(dlps);
			rs = dr.getResultSet();
			if (rs == null || !rs.isBeforeFirst()) {
				System.out.println("Warning:  PortalServlet.getDnaID() did not retrieve data.  " + "projId=" + projId
						+ ".  " + dlps.toString());
				return null;
			}

			while (rs.next()) {
				dnaId = rs.getInt("dnaId");
			}

			String id = "" + dnaId;

			return id;

		} catch (java.sql.SQLException se) {
			System.out.println("Error:  PortalServlet.getDnaID() resultSet fetch.  msg=" + se.getMessage());
			return null;
		} finally {
			if (dr != null) {
				dr.close();
				dr = null;
			}
		}
	}

	/*
	 * Little class to hold the pair of parameters needed to define an extract
	 */
	private class ExtPair {
		private int pptId;
		private int dnaId;

		public ExtPair() {
		}

		// public ExtPair(int ppt, int dna)
		// {
		// this.pptId = ppt;
		// this.dnaId = dna;
		// }

		public void setPptId(int value) {
			this.pptId = value;
		}

		public int getPptId() {
			return this.pptId;
		}

		public void setDnaId(int value) {
			this.dnaId = value;
		}

		public int getDnaId() {
			return this.dnaId;
		}
	}
}
