package com.pdi.listener.portal.delegated.helpers.reports;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pdi.data.abyd.dto.common.PrnData;
import com.pdi.data.abyd.helpers.intGrid.IGAlrDataHelper;
import com.pdi.data.abyd.helpers.intGrid.IGKf4dDataHelper;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.data.dto.ProjectParticipantInstrument;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.Report;
import com.pdi.string.StringUtils;

/*
 * AlrDorTReport - Class to hold methods relevant to the ALR Trait Interpretation
 *                      and the ALR Drivers Interpretation reports
 * 
 * NOTE:  EVEN THOUGH LOCATED UNDER THE DELEGATED FOLDER, THERE ARE NO METHODS IN THIS
 *        CLASS THAT ARE DEFINED IN THE DATA INTERFACE.
 *        
 * This class is located here because the folder structure (delegated/helpers) is
 * inverted from normal practice elsewhere in this project stack (helpers/delegated).
 * Putting this helper class here avoided confusion with having two folders in the
 * portal structure that had the name "helper".
 */
public class AlrDorTReport
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	
	//
	// Constructors.
	//
	 public AlrDorTReport()
	 {
	 	// No logic here at the present time
	 }
	
	//
	// Instance methods.
	//

	/**
	 * getAlrData - Method that retrieves the requisite data for the ALR Trait Interpretation or
	 *              the ALR Driver Interpretation reports
	 * @param inp
	 * @param epis
	 * @return
	 */
	public Report getAlrDorTData(IndividualReport inp, ArrayList<ProjectParticipantInstrument> epis, String tORd, String dataSrc)
	{
		IndividualReport ret = inp.clone();
		String pptId = null;
		String projId = null;
		long dnaId = 0;
		Connection con = null;

		try
		{
			// For this report we assume that there is a single participant
			ProjectParticipantInstrument epi = epis.get(0);
			pptId = epi.getParticipant().getId();
			projId = epi.getProject().getId();
			
			// get the dna ID
			dnaId = getDnaIdFromProjId(projId);
//			
//   			// Get the ALR data... get it for both types
//			Map<String, PrnData> dat;
//			con = AbyDDatabaseUtils.getDBConnection();
//			IGAlrDataHelper helper = new IGAlrDataHelper(con, pptId, dnaId);
//			if (tORd.equals("T"))
//			{
//				dat = helper.getTraits();
//			}  else  {
//				dat = helper.getDrivers();
//			}
			// Get the appropriate data
			Map<String, PrnData> dat;
			Map<String, PrnData> traits;
			Map<String, PrnData> drivers;
			con = AbyDDatabaseUtils.getDBConnection();

			if (ReportingConstants.SRC_KFP.equals(dataSrc))
			{
				IGAlrDataHelper helper = new IGAlrDataHelper(con, pptId, dnaId);
				traits = helper.getTraits();
				drivers = helper.getDrivers();
			}
			else if (ReportingConstants.SRC_KF4D.equals(dataSrc))
			{
				IGKf4dDataHelper helper = new IGKf4dDataHelper(con, pptId, dnaId);
				traits = helper.getTraits();
				drivers = helper.getDrivers();
			}
			else
			{
				throw new Exception(dataSrc + " is an invalid Data source.  Unable to generate traits/drivers report.");
			}
			dat = (tORd.equals("T") ? traits : drivers);

			// go get the texts
			HashMap<String, List<String>> narTxt = getNarrText(tORd);
			
			// set up the data for display
			for (Map.Entry<String, PrnData> entry : dat.entrySet()) {
			    String key = entry.getKey();
			    PrnData prn = entry.getValue();
			    ret.addDisplayData(key + "-pctl", "" + prn.getPctl());
			    ret.addDisplayData(key + "-rate", "" + prn.getRate());
			    
			    String ntKey = "";
			    if (tORd.equals("T"))
			    {
				    // Narrative text is stored with the key from the original workbook.
				    // Convert the scale name ("ALR_XX") to that format ("Xx")
					//    ntKey = key.substring(4);
					//    char c1 = Character.toUpperCase(ntKey.charAt(0));
					//    char c2 = Character.toLowerCase(ntKey.charAt(1));
					//    ntKey = "" + c1 + c2;
			    	// Refactored after database change.  Still strips down to 2 character key code, but the database has been updated to:
			    	//		1) use caps for the 2 char code, and 2) use the KF4D codes (8 are different from KFALP)
					ntKey = key.substring(4);
			    }  else  {
			    	// must be Drivers
			    	ntKey = key;
			    }
			    
			    
			    String text = "";
			    List<String> strList = narTxt.get(ntKey);
			    if (strList == null)
			    {
			    	text = "Narrative text not available for key " + ntKey;
			    }  else  {
			    	text = strList.get(prn.getNarr());
			    	if (text == null)
			    	{
			    		text = "Narrative text not available for key " + ntKey + " at index " + prn.getNarr();
			    	}
			    }
			    ret.addDisplayData(key + "-text", text);	// Leave it at the scale key
			}
 
   			// Generate the file name
	  		String fileName = StringUtils.removeFilenameSpecialCharacters(
						epi.getParticipant().getLastName() + " " +
						epi.getParticipant().getFirstName() + " " +
						epi.getProject().getProjectTypeName()+" "+
						inp.getReportCode()+" "+
						new SimpleDateFormat("yyyyMMdd").format(new Date()) 
						);

			ret.setName(fileName.replace(" ", "_") + ".xls");	// "x" will be added later

			return ret;
		}
		catch(Exception e)
		{
			System.out.println("Exception trapped in getAlrDorTData:  msg=" + e.getMessage());
			return null;
		}
		finally
		{
			if (con != null)
			{
				try
				{
					con.close();
				}
				catch (SQLException se) {}
				con = null;
			}
		}
	}


	/**
	 * getDnaIdFromProjId - Does what it says
	 * @param projId
	 * @return
	 */
	private long getDnaIdFromProjId(String projId)
		throws Exception
	{
		Statement stmt = null;
		ResultSet rs = null;
		Connection con = null;
	
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT dna.dnaId ");
		sqlQuery.append("  FROM pdi_abd_dna dna ");
		sqlQuery.append("    WHERE dna.projectId = '" + projId + "' ");
	
		try
		{
			con = AbyDDatabaseUtils.getDBConnection();
			stmt = con.createStatement();
	    	rs = stmt.executeQuery(sqlQuery.toString());
	    	
	    	if(rs == null || ! rs.isBeforeFirst())
	    	{
	    		return -1;
	    	}
	
	    	// Get the text (should be one and only one row)
	    	rs.next();
	
			return rs.getLong("dnaId");
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("AlrDorTReport.getDnaIdFromProjId():  " +
					"SQLException: " + ex.getMessage() + ", " +
					"SQLState: " + ex.getSQLState() + ", " +
					"VendorError: " + ex.getErrorCode());
		}
		finally
		{
			if (rs != null)
			{
				try
				{
					rs.close();
				}
				catch (SQLException ex)
				{
					System.out.println("Closing rs in AlrDorTReport.getDnaIdFromProjId().");
				}
				rs = null;
			}
			if (stmt != null)
			{
				try
				{
					stmt.close();
				}
				catch (SQLException ex)
				{
					System.out.println("Closing stmt in AlrDorTReport.getDnaIdFromProjId().");
				}
				stmt = null;
			}
			if (con != null)
			{
				try
				{
					con.close();
				}
				catch (SQLException ex) {}
				con = null;
			}
		}
	}


	/**
	 * 	getNarrText - Fetch the narrative text from the database
	 * @param tord - a "T" or a "D" (traits/drivers flag)
	 * @return Map; key is the item key (2 char trait or 4 char driver) and value is a list of narrative texts in ascending order)
	 * @throws Exception
	 */
	private HashMap<String, List<String>> getNarrText(String tord)
		throws Exception
	{
		Connection con = null;
		HashMap<String, List<String>> ret = new HashMap<String, List<String>>();
		Statement stmt = null;
		ResultSet rs = null;
	
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT im.itemKey, ");
		sqlQuery.append("       im.interpLevel, ");
		sqlQuery.append("       im.textId, ");
		sqlQuery.append("       tt.text ");
		sqlQuery.append("  FROM pdi_abd_interp_map im ");
		sqlQuery.append("    LEFT JOIN pdi_abd_text tt ON (tt.textId = im.textId AND tt.languageId = 1) ");
		sqlQuery.append("  WHERE im.interpType = '" + tord + "' ");
		sqlQuery.append("  ORDER BY im.itemKey, im.interpLevel");
		//System.out.println("getNarrText SQL " + sqlQuery.toString());
	
		try
		{
			con = AbyDDatabaseUtils.getDBConnection();
		    stmt = con.createStatement();
			rs = stmt.executeQuery(sqlQuery.toString());
	
			String curKey = "";
			ArrayList<String> txtList = null;
			while (rs.next())
			{
				String key = rs.getString("itemKey");
				int lvl = rs.getInt("interpLevel");
				String txt = rs.getString("text");
				if (! key.equals(curKey))
				{
					if(! curKey.isEmpty())
					{
						//save the old one
						ret.put(curKey,  txtList);
					}
					// make a new list and update the current key
					txtList = new ArrayList<String>();
					txtList.add("Invalid txt index");
					curKey = key;
				}
				txtList.add(lvl, txt);
			}
			// Save the last one
			ret.put(curKey,  txtList);
			
			return ret;	
		}
		catch (SQLException ex)
		{
			// handle any errors
			throw new Exception("SQL error for getNarrText.  msg=" + ex.getMessage());
		}
		finally
		{
			if (rs != null)
			{
			    try {  rs.close();  }
			    catch (SQLException sqlEx)
			    {
			    	System.out.println("ERROR: rs close in noCogsInDna - " + sqlEx.getMessage());
			    }
				rs = null;
			}
			
			if (stmt != null)
			{
			    try {  stmt.close();  }
			    catch (SQLException sqlEx)
			    {
			    	System.out.println("ERROR: stmt close in noCogsInDna - " + sqlEx.getMessage());
			    }
			    stmt = null;
			}
			
			if (con != null)
			{
			    try {  con.close();  }
			    catch (SQLException sqlEx)
			    {
			    	System.out.println("ERROR: con close in noCogsInDna - " + sqlEx.getMessage());
			    }
			    con = null;
			}
			
		}
	}
}
