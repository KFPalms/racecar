package com.pdi.listener.abyd.evalGuide.ut;


//import com.pdi.data.abyd.dto.evalGuide.EGUrlDTO;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
//import com.pdi.data.nhn.util.NhnDatabaseUtils;
//import com.pdi.data.abyd.dto.evalGuide.EGUrlDTO;
import com.pdi.listener.abyd.evalGuide.EvalGuideService;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

public class EvalGuideServiceUT extends TestCase
{
	//
	// Instance variables
	//
	EvalGuideService _svc = null;
	
	//
	// Constructor
	//
	public EvalGuideServiceUT(String name)
	{
		super(name);
		
		_svc = new EvalGuideService();
	}
  

	/*
	 * testComCheck
	 */
//	public void testComCheck()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//	
//		System.out.println("  Testing comCheck()...");
//		String comStr = _svc.comCheck();
//		System.out.println("    Made connection.  Return = " + comStr);
//	
//		UnitTestUtils.stop(this);
//	}


	/*
	 * testFetchEgURLs
	 */
//	public void testFetchEgURLs()
//	throws Exception
//	{
//		UnitTestUtils.start(this);
//		NhnDatabaseUtils.setUnitTest(true);
//		AbyDDatabaseUtils.setUnitTest(true);
//		
//		System.out.println("  Testing fetchEgURLs()...");
//		String projId = "1";
//		String partId = "60254";
//		EGUrlDTO dto = _svc.fetchEgUrls(projId, partId);
//		System.out.print(dto.toString());
//
//		UnitTestUtils.stop(this);
//	}


//	public void testFetchEvalGuide()
//	throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//		
//		System.out.println("  Testing fetchEvalGuide()...");
//		//String lpId = "KENSTEST";
//		String lpId =  "55084571"; // certify ABD-189
//		EGFullDataDTO dto = _svc.fetchEvalGuide(lpId);
//		System.out.print(dto.toString());
//
//		UnitTestUtils.stop(this);
//	}


//	public void testSaveEgRaterName()
//	throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//		
//		System.out.println("  Testing saveEgRaterName()...");
//
//		EGRaterNameData nameDat = new EGRaterNameData();
//		nameDat.setRaterFirstName("Marcus");
//		nameDat.setRaterLastName("Welby SUT");
//
//		EGRaterNameDTO data = new EGRaterNameDTO();
//		data.setParticipantId("KENSPART");
//		data.setDnaId(999);
//		data.setModuleId(888);
//		data.setRaterNameData(nameDat);
//
//		_svc.saveEgRaterName(data);
//		System.out.println("Check the database...");
//
//		UnitTestUtils.stop(this);
//	}


//	public void testSaveEgCompData()
//	throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//
//		System.out.println("  Testing saveEgCompData()...");
//
//    	ArrayList<EGBarScoreData> barDat = new ArrayList<EGBarScoreData>();
//    	for (int i=0; i < 4; i++)
//    	{
//    		EGBarScoreData cur = new EGBarScoreData();
//    		cur.setBarSeq(i + 1);
//    		cur.setBarScore((i + 1) * 3);
//    		barDat.add(cur);
//    	}
//
//    	EGCompDataDTO inp = new EGCompDataDTO();
//    	inp.setParticipantId("KENSPART");
//    	inp.setDnaId(999);
//    	inp.setModuleId(888);
//    	inp.setCompId(14);
//    	inp.setCompNote("Comp.Note - SUT");
//    	inp.setCompBarArray(barDat);
//
//		_svc.saveEgCompData(inp);
//		System.out.println("Check the database...");
//
//		UnitTestUtils.stop(this);
//	}


//	public void testSaveEgIrData()
//	throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//
//		System.out.println("  Testing saveEgIrData()...");
//
//    	ArrayList<EGIrScoreData> irDat = new ArrayList<EGIrScoreData>();
//    	for (int i=0; i < 4; i++)
//    	{
//    		EGIrScoreData cur = new EGIrScoreData();
//    		cur.setIrSeq(i + 1);
//    		cur.setIrScore((i + 1) * 3);
//    		irDat.add(cur);
//    	}
//
//    	EGIrDataDTO inp = new EGIrDataDTO();
//    	inp.setParticipantId("KENSPART");
//    	inp.setDnaId(999);
//    	inp.setModuleId(888);
//    	inp.setIrArray(irDat);
//
//		_svc.saveEgIrData(inp);
//		System.out.println("Check the database...");
//
//		UnitTestUtils.stop(this);
//	}


//	public void testSaveEgOverall()
//	throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//
//		System.out.println("  Testing saveEgOverall()...");
//
//		EGOverallData ovDat = new EGOverallData();
//		ovDat.setBriefDesc("Brief Description - SUT");
//		ovDat.setKeyStrengths("Key Strengths - SUT");
//		ovDat.setKeyDevNeeds("Key Development Needs - SUT");
//		ovDat.setCoachNotes("Coaching Notes - SUT");
//
//		EGOverallDTO inp = new EGOverallDTO();
//    	inp.setParticipantId("KENSPART");
//    	inp.setDnaId(999);
//    	inp.setModuleId(888);
//    	inp.setOverallData(ovDat);
//
//		_svc.saveEgOverall(inp);
//		System.out.println("Check the database...");
//
//		UnitTestUtils.stop(this);
//	}


	/*
	 * testSubmitEG
	 */
//	public void testSubmitEG()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//
//		String partId = "KENSPART";
//		long dnaId = 999;
//		long modId = 888;
//
//		_svc.submitEG(partId, dnaId, modId);
//		System.out.println("Check the database...");
//
//		UnitTestUtils.stop(this);
//	}
}
