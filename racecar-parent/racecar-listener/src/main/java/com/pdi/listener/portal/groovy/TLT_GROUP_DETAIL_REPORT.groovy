/*
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.listener.portal.groovy 

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.GroupReport;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;
import com.pdi.reporting.request.ReportingRequest;
import com.pdi.reporting.response.ReportingResponse;
import com.pdi.data.v2.dto.SessionUserDTO;
import com.pdi.data.v2.helpers.ParticipantDataHelper;
import com.pdi.listener.portal.dao.DataAccessLayerV2;
import com.pdi.listener.portal.dao.DataAccessLayerV2.ParticipantInformation;
import com.pdi.listener.portal.dao.DataAccessLayerV2.ProjectInformation;
import com.pdi.properties.PropertyLoader;
import com.pdi.scoring.Norm;
import com.pdi.scoring.NormSet;
import com.pdi.xml.XMLUtils;

/**
 * @author gmyers
 */
public class TLT_GROUP_DETAIL_REPORT
{
	private ReportingRequest reportRequest;
	private Node reportNode;
	private SessionUserDTO sessionUser;
	private DataAccessLayerV2 dataAccessLayer;

	/*
	 * generate
	 */
	public void generate()
	{
		String reportCode = reportNode.getAttributes().getNamedItem("code").getTextContent();
		//System.out.println(reportCode);
		NodeList participants = XMLUtils.getElements(reportNode, "Participants/Participant");
		
		//System.out.println(participants.item(0).getAttributes().getNamedItem("projectId").getTextContent());
		GroupReport gr = new ParticipantDataHelper().getGroupReportData(sessionUser, reportCode, ReportingConstants.REPORT_TYPE_GROUP, participants.item(0).getAttributes().getNamedItem("projectId").getTextContent());
		
		//need to escape the org name '&'
		gr.getDisplayData().put("ORGANIZATION", XMLUtils.xmlEscapeString(gr.getDisplayData().get("ORGANIZATION")));
		
		for( int j= 0; j < participants.getLength(); j++)
		{
			Node participant = participants.item(j);
			String participantId = participant.getAttributes().getNamedItem("participantId").getTextContent();
			String projectId = participant.getAttributes().getNamedItem("projectId").getTextContent();
			if(participantId == null || participantId.equalsIgnoreCase("")) {
				participantId = "";
			}
			
			
			//Got all of the basic data, time to add abyd data
			ProjectInformation projectInformation = dataAccessLayer.projectInformationHash.get(projectId);
			ParticipantInformation participantInformation = null;
			for(String code : dataAccessLayer.participantInformationList.keySet()) {
				ParticipantInformation pi =  dataAccessLayer.participantInformationList.get(code);
				if(pi.getParticipantId().equalsIgnoreCase(participantId) && pi.getProjectId().equalsIgnoreCase(projectId)) {
					participantInformation = pi;
					break;
				}
			}
			

			IndividualReport ir = participantInformation.getIndividualReport();

		// note: 18/jan/2011 : per ABD 468, if it's an abyd tlt group report, then the current and target levels 
		// should be the same as if it was an abyd tlt individual report
		// so, i'll check if there's a dna id present.  If there is, then i'll get the codes from an 
		// individual report, and plug those into the group report
		if(participantInformation.getDnaId() != null) {
				//System.out.println("tlt_groupdetailreport.groovy: IR CURRENT: " + ir.getDisplayData().get("CURRENT_LEVEL")   +  "   TARGET: " + ir.getDisplayData().get("TARGET_LEVEL")	);

				// 3 == BUL
				// 4 == MLL
				// 5 == SEA
				// 7 == IC/FLL
				// 8 == MLL/BUL
				// 9 == SEA
			
				if(ir.getDisplayData().get("TARGET_LEVEL").equalsIgnoreCase("9")) {
					gr.getDisplayData().put("TARGET_LEVEL","SEA");
					gr.getDisplayData().put("CURRENT_LEVEL","MLL/BUL");					
				}else if(ir.getDisplayData().get("TARGET_LEVEL").equalsIgnoreCase("8")) {
					gr.getDisplayData().put("TARGET_LEVEL","BUL");
					gr.getDisplayData().put("CURRENT_LEVEL","MLL");
				}else if(ir.getDisplayData().get("TARGET_LEVEL").equalsIgnoreCase("7")) {
					gr.getDisplayData().put("TARGET_LEVEL","MLL");
					gr.getDisplayData().put("CURRENT_LEVEL","IC/FLL");
				}else if(ir.getDisplayData().get("TARGET_LEVEL").equals("SEA") ||
							ir.getDisplayData().get("TARGET_LEVEL").equals("BUL")  ||
							ir.getDisplayData().get("TARGET_LEVEL").equals("MLL") ){
				
					gr.getDisplayData().put("TARGET_LEVEL",ir.getDisplayData().get("TARGET_LEVEL"));
					gr.getDisplayData().put("CURRENT_LEVEL",ir.getDisplayData().get("CURRENT_LEVEL"));				
				
				}
				
				//System.out.println("tlt_groupdetailreport.groovy: GR NOW: CURRENT: " + gr.getDisplayData().get("CURRENT_LEVEL")   +  "   TARGET: " + gr.getDisplayData().get("TARGET_LEVEL")	);
						
		}
			


			ir.setReportCode(reportCode);
			ir.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);
			gr.getIndividualReports().add(ir);
		}
		gr.getScriptedData().put("REPORT_TYPE", "TLT");
		reportRequest.addReport(gr);
	}
}
