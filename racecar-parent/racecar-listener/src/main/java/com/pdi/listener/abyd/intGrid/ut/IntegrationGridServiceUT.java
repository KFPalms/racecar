package com.pdi.listener.abyd.intGrid.ut;

//import java.util.ArrayList;

//import com.pdi.data.abyd.dto.common.DNACellDTO;
//import com.pdi.data.abyd.dto.common.EGFullDataDTO;
//import com.pdi.data.abyd.dto.intGrid.IGCompDataDTO;
//import com.pdi.data.abyd.dto.intGrid.IGExtractDataDTO;
//import com.pdi.data.abyd.dto.intGrid.IGFinalScoreDTO;
//import com.pdi.data.abyd.dto.intGrid.IGInstDisplayData;
//import com.pdi.data.abyd.dto.intGrid.IGLcNameDTO;
//import com.pdi.data.abyd.dto.intGrid.IGCompDataDTO;
import com.pdi.data.abyd.dto.intGrid.IGSummaryDataDTO;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.listener.abyd.intGrid.IntegrationGridService;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

public class IntegrationGridServiceUT extends TestCase
{
	//
	// Instance variables
	//
	IntegrationGridService _svc = null;
	
	//
	// Constructor
	//
	public IntegrationGridServiceUT(String name)
		throws Exception
	{
		super(name);

		_svc = new IntegrationGridService();
	}


	/*
	 * testComCheck
	 */
	public void testComCheck()
		throws Exception
	{
		UnitTestUtils.start(this);
		AbyDDatabaseUtils.setUnitTest(true);

		String comStr = _svc.comCheck();
		System.out.println("made connection.  Return = " + comStr);

		UnitTestUtils.stop(this);
	}


	/*
	 * testFetchIGSummary
	 */
//	public void testFetchIGSummary()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//
//		//String lpId = "19821716";
//		String lpId = "62402597"; // mb's
//		IGSummaryDataDTO dto = _svc.fetchIGSummary(lpId);
//		System.out.print(dto.toString());
//
//		UnitTestUtils.stop(this);
//	}


	/*
	 * testFetchTestingScoreData
	 * NOTE:  Tests with cogsOn flag set "true" only
	 */
//	public void testFetchTestingScoreData()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//
//		String partId = "KEYVOFZF";
//		long dnaId = 41;
//		boolean cogsOn = true;
//
//		ArrayList<DNACellDTO> ret = _svc.fetchTestingScoreData(partId, dnaId, cogsOn);
//		System.out.print(ret.toString());
//
//		UnitTestUtils.stop(this);
//	}


	/*
	 * testFetchCompRowData
//	 */
//	public void testFetchCompRowData()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//
////		String partId = "KEYVOFZF";
////		long dnaId = 41;
////		long compId = 485;		
//		
//		String partId = "FFTAEVVN";
//		long dnaId = 201;
//		long compId = 100;
//		String lpId =  "55084571"; // certify ABD-189
////		<CandId>FFTAEVVN</CandId>
////		<CandFname>Test_vkale 050610</CandFname>
////		<CandLname>Test_vkale 050610</CandLname>
////		<DnaId>201</DnaId>
////		<SimId>100</SimId>
//
//
//		IGCompDataDTO ret = _svc.fetchCompRowData(compId, partId, dnaId, "Test Comp");
//		System.out.print(ret.toString());
//
//		UnitTestUtils.stop(this);
//	}

//
//	/*
//	 * testFetchTestingColumnData
//	 */
//	public void testFetchTestingColumnData()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//
//		String partId = "DXNPJFNM";
//		long dnaId = 41;
//
//		IGInstDisplayData ret = _svc.fetchTestingColumnData(partId, dnaId);
//		System.out.print(ret.toString());
//
//		UnitTestUtils.stop(this);
//	}
//
//
//	/*
//	 * testFetchExtractData
//	 */
//	public void testFetchExtractData()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//
//		String candId = "CNUZQRGD";
//		long dnaId = 16;
//
//		IGExtractDataDTO ret = _svc.fetchExtractData(candId, dnaId);
//		System.out.print(ret.toString());
//
//		UnitTestUtils.stop(this);
//	}
//
//
//	/*
//	 * testSaveLcName
//	 */
//	public void testSaveLcName()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//
//		IGLcNameDTO inp = new IGLcNameDTO();
//		inp.setPartId("KENLCTST");
//    	inp.setDnaId(99);
//    	inp.setLcFirstName("Timmy");
//    	inp.setLcLastName("O'Reilly - SUT");
//
//    	_svc.saveLcName(inp);
//		System.out.print("No return value... check database");
//
//		UnitTestUtils.stop(this);
//	}
//
//
//	/*
//	 * testSaveFinalScore
//	 */
//	public void testSaveFinalScore()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//
//		IGFinalScoreDTO inp = new IGFinalScoreDTO();
//		inp.setPartId("KENINSXX");
//    	inp.setDnaId(99);
//    	inp.setCompId(100);
//       	inp.setCompScoreIdx(11);
//    	inp.setCompNote("Comp Note - SUT");
//
//    	_svc.saveFinalScore(inp);
//		System.out.print("No return value... check database");
//
//		UnitTestUtils.stop(this);
//	}
//
//
//	/*
//	 * testSubmitIG
//	 */
//	public void testSubmitIG()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//
//		String partId = "KENIGTST";
//    	long dnaId = 41;
//
//    	_svc.submitIG(partId, dnaId);
//		System.out.print("No return value... check database");
//
//		UnitTestUtils.stop(this);
//	}
//
//
//	/*
//	 * testSaveWorkingNotes
//	 */
//	public void testSaveWorkingNotes()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//
//		String partId = "KENXXTST";
//    	long dnaId = 99;
//    	String note = "A working note from SUT";
//
//    	_svc.saveWorkingNotes(partId, dnaId, note);
//		System.out.print("No return value... check database");
//
//		UnitTestUtils.stop(this);
//	}
//
//
//	/*
//	 * testFetchEvalGuide
//	 */
//	public void testFetchEvalGuide()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		AbyDDatabaseUtils.setUnitTest(true);
//
//		String partId = "KEYVOFZF";
//    	long dnaId = 41;
//     	long modId = 75;
//
//		EGFullDataDTO ret = _svc.fetchEvalGuide(partId, dnaId, modId);
//		System.out.print(ret.toString());
//
//		UnitTestUtils.stop(this);
//	}
}
