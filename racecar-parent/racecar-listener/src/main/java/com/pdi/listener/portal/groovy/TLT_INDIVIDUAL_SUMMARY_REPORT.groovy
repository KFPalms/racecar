/*
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.listener.portal.groovy 

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.GroupReport;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;
import com.pdi.reporting.request.ReportingRequest;
import com.pdi.reporting.response.ReportingResponse;
import com.pdi.data.v2.dto.ParticipantDTO;
import com.pdi.data.v2.dto.ProjectDTO;
import com.pdi.data.v2.dto.SessionUserDTO;
import com.pdi.data.v2.helpers.ParticipantDataHelper;
import com.pdi.data.v2.helpers.ProjectDataHelper;
import com.pdi.listener.portal.dao.DataAccessLayerV2;
import com.pdi.listener.portal.dao.DataAccessLayerV2.ParticipantInformation;
import com.pdi.listener.portal.dao.DataAccessLayerV2.ProjectInformation;
import com.pdi.properties.PropertyLoader;
import com.pdi.scoring.Norm;
import com.pdi.scoring.NormSet;
import com.pdi.string.StringUtils;
import com.pdi.xml.XMLUtils;

/**
 * @author gmyers
 */
public class TLT_INDIVIDUAL_SUMMARY_REPORT
{
	private ReportingRequest reportRequest;
	private Node reportNode;
	private SessionUserDTO sessionUser;
	private DataAccessLayerV2 dataAccessLayer;

	/*
	 * generate
	 */
	public void generate()
	{
System.out.println(" in portal/groovy/TLT_INDIVIDUAL_SUMMARY_REPORT.groovy......" );		
		
		String reportCode = reportNode.getAttributes().getNamedItem("code").getTextContent();

		NodeList participants = XMLUtils.getElements(reportNode, "Participants/Participant");
		
		for( int j= 0; j < participants.getLength(); j++)
		{
			Node participant = participants.item(j);
			String participantId = participant.getAttributes().getNamedItem("participantId").getTextContent();
			String projectId = participant.getAttributes().getNamedItem("projectId").getTextContent();
			if(participantId == null || participantId.equalsIgnoreCase("")) {
				participantId = "";
			}
			
			
			//Got all of the basic data, time to add abyd data
			ProjectInformation projectInformation = dataAccessLayer.projectInformationHash.get(projectId);
			ParticipantInformation participantInformation = null;
			for(String code : dataAccessLayer.participantInformationList.keySet()) {
				ParticipantInformation pi =  dataAccessLayer.participantInformationList.get(code);
				if(pi.getParticipantId().equalsIgnoreCase(participantId) && pi.getProjectId().equalsIgnoreCase(projectId)) {
					participantInformation = pi;
					break;
				}
			}

			ProjectDTO projectDto = projectInformation.getProject();
			
			ParticipantDTO participantDto;
			IndividualReport ir = new IndividualReport();
			if(participantInformation != null) {
				participantDto = participantInformation.getParticipant();
				ir = participantInformation.getIndividualReport();
				
			}
			try {
				ir.getDisplayData().put("FIRST_NAME", participantDto.getFirstName());
				ir.getDisplayData().put("LAST_NAME", participantDto.getLastName());				
				ir.getDisplayData().put("PARTICIPANT_ID", participantId);
				ir.getDisplayData().put("PROJECT_ID", projectId);
				ir.setReportCode(reportCode);
				ir.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);			
				ir.getDisplayData().put("ORGANIZATION", XMLUtils.xmlEscapeString(projectDto.getClient().getName()));

// CURRENT LEVEL				
				// this is for the TLT report, for an AbyD project... 
				// we need to hard-code the target/current values based on the 
				//		modelId of the dna
				//	at some point, we will add the ability to choose target/current
				// tlt levels for abyd-based projects 
				if(projectDto.getType() != "23"){
					// this is an ABYD
						Long modelId = projectInformation.getEntryState().getDnaDesc().getModelId();					
						for(int i = 0; i < projectInformation.getEntryState().getModelArray().size(); i++){						
							if(projectInformation.getEntryState().getModelArray().get(i).getTheKey() == modelId.longValue()){
								
								//System.out.println("modelId = " + modelId.longValue());
								// 3 == BUL
								// 4 == MLL
								// 5 == SEA
								if(modelId.intValue() == 4){
									ir.getDisplayData().put("CURRENT_LEVEL","IC/FLL");			
								}else if(modelId.intValue() == 3){
									ir.getDisplayData().put("CURRENT_LEVEL","MLL");
								}else if(modelId.intValue() == 5){
									ir.getDisplayData().put("CURRENT_LEVEL","MLL/BUL");
								}
								
							}
						}
				
				}else{
					// this is a TLT, and this is already set
					// by the ParticipantDataHelper
				}

// END CURRENT LEVEL

// TARGET LEVEL....
				// this is for the TLT report, for an AbyD project... 
				// we need to hard-code the target/current values based on the 
				//		modelId of the dna
				//	at some point, we will add the ability to choose target/current
				// tlt levels for abyd-based projects 
				if(projectDto.getType() != "23"){
					// this is an ABYD						
					Long modelId = projectInformation.getEntryState().getDnaDesc().getModelId();					
					for(int i = 0; i < projectInformation.getEntryState().getModelArray().size(); i++){						
						if(projectInformation.getEntryState().getModelArray().get(i).getTheKey() == modelId.longValue()){
							ir.getDisplayData().put("TARGET_LEVEL",projectInformation.getEntryState().getModelArray().get(i).getTheValue());
						}
					}
				
				}else{
					// this is a TLT, and this is already set
					// by the ParticipantDataHelper				
				}
// END TARGET LEVEL....												
				
				String fileName = StringUtils.removeSpecialCharacters(
						projectDto.getClient().getName() + " " +
						participantDto.getLastName() + " " +
						participantDto.getFirstName() + " " +
						new SimpleDateFormat("ddMMMyyyy").format(new java.util.Date()) + " ");
				ir.setName(fileName + reportCode + ".pdf");			
				reportRequest.addReport(ir);

			} catch(Exception e) {
				//System.out.println(participantId + "-- error");
				//e.printStackTrace();
			}
		}
	}
}
