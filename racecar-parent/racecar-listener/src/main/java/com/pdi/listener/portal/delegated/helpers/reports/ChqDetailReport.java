/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.listener.portal.delegated.helpers.reports;

//import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.text.SimpleDateFormat;

//import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.data.dto.ProjectParticipantInstrument;
import com.pdi.data.dto.Response;
import com.pdi.data.dto.Text;
import com.pdi.data.dto.TextGroup;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;
import com.pdi.string.StringUtils;
import com.pdi.xml.XMLUtils;

/*
 * ChqDetailReport - class to hold methods relevant to the CHQ detail report
 * 
 * NOTE:  EVEN THOUGH LOCATED UNDER THE DELEGATED FOLDER, THERE ARE NO METHODS IN THIS
 *        CLASS THAT ARE DEFINED IN THE DATA INTERFACE.
 *        
 * This class is located here because the folder structure (delegated/helpers) is
 * inverted from normal practice elsewhere in this project stack (helpers/delegated).
 * Putting this helper class here avoided confusion with having two folders in the
 * portal structure that had the name "helper".
 */
public class ChqDetailReport
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	
	//
	// Constructors.
	//
    public ChqDetailReport()
    {
    	// No logic here at the present time
    }

	//
	// Instance methods.
	//


	/**
	 * getCHQDetailData - Method that retrieves the requisite data for the CHQ report
	 * 
	 * @param inp - An IndividualReport object with a small amount of relevant data
	 * @param epis - The ParojectParticipantReport objects associated with the request
	 * @param tg -  The static text labels (text groups) used to generate the report
	 */
	public IndividualReport getCHQDetailData(IndividualReport inp,  
				ArrayList<ProjectParticipantInstrument> epis, 
				TextGroup tg )
	{
		//System.out.println("doCHQdetailRpt...");
//		Connection con = null;
		IndividualReport ret = inp;
		
		try
		{
//			con = AbyDDatabaseUtils.getDBConnection();

			boolean hasChqData = false;
			boolean hasDemoData = false;

			// Get the participant info... assumes that all epis are for the same participant
			ProjectParticipantInstrument epi_x = epis.get(0);
			String participantId = epi_x.getParticipant().getId();

			for(ProjectParticipantInstrument epi : epis)
			{
				if(participantId == null || participantId.equalsIgnoreCase(""))
				{
					participantId = "";
				}				

// TODO:  NEED TO RETHINK HOW THE PREVIEW REPORT WORKS WITH THE EPI'S				
//				if(previewReport)
//				{
//					//They want to preview the report output with a fake participant
//						participantInformation = dataAccessLayer.loadParticipant(
//											PropertyLoader.getProperty("com.pdi.listener.portal.application", "sampleReport.projectId"), 
//											PropertyLoader.getProperty("com.pdi.listener.portal.application", "sampleReport.participantId"));					
//						previewReport = true;
//				}
//				if(previewReport)
//				{
//					//this is a preview report, we cannot get the "participants" data because there is no participant
//					reportModelStructure = projectInformation.getReportModelStructure();
//				}

//				// add the scripted data from the reportInput curTLT, so that we have TLT scores for the report.
//				//ret.setScriptedData(reportInput.getCurrentTLT().getScriptedData());

				/*
				 * ========================================
				 * ADD BASIC PARTICIPANT DATA TO REPORT 
				 */

				ret.addDisplayData("FIRST_NAME", XMLUtils.xmlEscapeString(epi.getParticipant().getFirstName()));
				ret.addDisplayData("LAST_NAME", XMLUtils.xmlEscapeString(epi.getParticipant().getLastName()));
				ret.getDisplayData().put("PARTICIPANT_NAME", XMLUtils.xmlEscapeString(epi.getParticipant().getFirstName() + " " + epi.getParticipant().getLastName()));
				ret.addDisplayData("EMAIL", epi.getParticipant().getEmail());
				ret.addDisplayData("BUS_UNIT", XMLUtils.xmlEscapeString(epi.getParticipant().getMetadata().get("optional_1")));
				ret.addDisplayData("JOB_TITLE", XMLUtils.xmlEscapeString(epi.getParticipant().getMetadata().get("optional_2")));
				ret.addDisplayData("OPTIONAL_1", XMLUtils.xmlEscapeString(epi.getParticipant().getMetadata().get("optional_1")));
				ret.addDisplayData("OPTIONAL_2", XMLUtils.xmlEscapeString(epi.getParticipant().getMetadata().get("optional_2")));
				ret.addDisplayData("OPTIONAL_3", XMLUtils.xmlEscapeString(epi.getParticipant().getMetadata().get("optional_3")));

				if(epi.getInstrumentTakenDate() == null)
				{
					java.util.Date date = new java.util.Date();
					SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");				
					ret.getDisplayData().put("ADMIN_DATE", sdf.format(date));
					//System.out.println("epi.getInstrumentTakenDate()1"+ sdf.format(date));
				}else {
					ret.getDisplayData().put("ADMIN_DATE", epi.getInstrumentTakenDate().substring(0, 10));
					//System.out.println("epi.getInstrumentTakenDate()2" +  epi.getInstrumentTakenDate().substring(0, 10));
				}

				Random generator = new Random();
				int randomIndex = generator.nextInt( 10000 ) * 10000;
				//System.out.println("Participant:  name=" + participant.getFirstName() + " " + participant.getLastName()+ ".  Biz Unit=" + participant.getBusinessUnit());
				String firstName = epi.getParticipant().getFirstName();
				if(firstName == null)
				{
					firstName = "";
				}
				String lastName = epi.getParticipant().getLastName();
				if(lastName == null)
				{
					lastName = "";
				}
				String businessUnit = epi.getParticipant().getMetadata().get("OPTIONAL_1");
				if(businessUnit == null)
				{
					businessUnit = "";
				}
				//ret.setName(firstName + "_" + lastName + "_" + businessUnit.replace("/", "_").replace("\\", "_") + "_" + randomIndex + ".pdf");

				//Get Project data
				ret.addDisplayData("ORGANIZATION", XMLUtils.xmlEscapeString(epi.getProject().getClient().getName()));
		  		ret.addDisplayData("PROJECT_NAME", XMLUtils.xmlEscapeString(epi.getProject().getName()));

				/*
				 *  Set up labels/option data/entry state
				 *  --------------------------------------
				 */
				//ReportOptionsDataHelper optionsDH = new ReportOptionsDataHelper();
				//ReportLabelsDataHelper labelsDH = new ReportLabelsDataHelper();
				//EntryStateHelper  entryH = new EntryStateHelper(con, projectId);
				
				//Setup2ReportOptionsDTO options = optionsDH.getReportOptionData(con,dnaId);
				//Setup2ReportLabelsDTO labels = labelsDH.getTranslatedReportLabelData(dnaId, tg.getLanguageCode(), this.EDITABLE );
				//EntryStateDTO entryState = entryH.getEntryStateData();
								
				ret.getDisplayData().put("CLIENT_LOGO", epi.getProject().getClient().getId()+".png");
				ret.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);
				
				java.util.Date date = new java.util.Date();
				SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
				ret.getDisplayData().put("TODAY_DATE", sdf.format(date));
				//ret.getDisplayData().put("DNA_NAME",XMLUtils.xmlFormatString(entryState.getDnaDesc().getDnaName()));
				
				/***
				 * SET THE FILE NAME
				 */
//				String fileName = StringUtils.removeSpecialCharacters(
//					epi.getProject().getClient().getName() + " " +
//					epi.getProject().getName() + " " +
//					epi.getParticipant().getLastName() + "_" +  epi.getParticipant().getFirstName() + " " +
//					new SimpleDateFormat("ddMMMyyyy").format(new java.util.Date()) + " ");					
//					ret.setName(fileName + ret.getReportCode() + ".pdf");
				
				String fileName = StringUtils.removeFilenameSpecialCharacters(
						epis.get(0).getParticipant().getLastName() + " " +
						epis.get(0).getParticipant().getFirstName() + " " +
						epis.get(0).getProject().getProjectTypeName()+" "+
						inp.getReportCode()+" "+
						new SimpleDateFormat("yyyyMMdd").format(new Date()) + " " 
						);
						
				ret.setName(fileName.replace(" ", "_") + ".pdf");

				// ANSWER DATA //
				// get all the existing raw data, and put it into the individual report
				// answer data should be in the epi   --- epi.getResponseGroup().getResponses()
				// we need answer data from both the chq and the demo module... 
				// if the epi is for any instrument other than chq or demo, there will be no response
				// group.  chq and demo answers are saved into the responses, and not the scores, because these
				// are answer values, and not scored values.
				HashMap<String, Response> answerMap = new HashMap<String, Response>();
				if(epi.getInstrument().getCode().equals("chq") ||
				   epi.getInstrument().getCode().equals("demo"))
				{
					answerMap = epi.getResponseGroup().getResponses();
				}
				else
				{
					// we don't want other instruments, so keep going until we find the ones
					// we need... 
					continue;
				}

				if( (answerMap == null || answerMap.isEmpty() )&& 
					(  epi.getInstrument().getCode().equals("chq") ||
					   epi.getInstrument().getCode().equals("demo" )) )
				{
					ret.getScriptedData().put("STATUS", "NOT_FINISHED");
				}
				else
				{
					// just trying to set status... but need both instruments, so
					// makes it a bit more involved....
					if(epi.getInstrument().getCode().equals("chq"))
					{
						hasChqData = true;
					}
					else if(epi.getInstrument().getCode().equals("demo"))
					{
						hasDemoData = true;
					}
					if (hasChqData && hasDemoData)
					{
						ret.getScriptedData().put("STATUS", "COMPLETE");
					}
					
					//System.out.println("PUTTING IN RAW DATA TO THE IR");
					// check for the ReportData._rawData hash map, and make sure it's there and
					// initialized.....
					if(ret.getReportData().get(ReportingConstants.RC_CHQ)== null)
					{
						ret.getReportData().put(ReportingConstants.RC_CHQ, new ReportData());
					}
					if(ret.getReportData().get(ReportingConstants.RC_CHQ).getRawData() == null)
					{
						ret.getReportData().get(ReportingConstants.RC_CHQ).setRawData(new HashMap<String, String>());
					}
					
					// this is hokey, but it should work.
					// for the IMP_ ranking of 5 items, to make them in order, 
					// i'm going to create one string, with <br/> embedded between
					// the items.  to get them in order, i need to use the answer's Value, 
					// but i also need to have the answer string from the constants list.
					// so i will create the hash map, using the value as integer, and the 
					// constant string as string... then pull the pieces together into 
					// a single string under IMP_STRING
					HashMap<Integer, String> impHash = new HashMap<Integer, String>(); 
					
					for (Iterator<String> itr=answerMap.keySet().iterator(); itr.hasNext(); )
					{
						Response r = answerMap.get(itr.next());
						if( r == null)
						{
							continue;
						}
						else
						{
							// 3/11/2013  NOTE:
							// There was a change somewhere that affected the way that Reflex put out data
							// for the appended variables (the ones that have the value as a part of the
							// key).  It now put out a value of 1 instead of the country code.  Fixing
							// CULT_xxx to pull off the key.
							// If CITZ or ERLYR ever get picked up and used it will have to be done there
							// as well
							
							if(r.getCode().startsWith("EDUC"))
							{
								ret.getReportData().get(ReportingConstants.RC_CHQ).getRawData().put(r.getCode(), ReportingConstants.EDUC_CODES.get(r.getValue()));
							}
							else if (r.getCode().equals("CURLOC_000_OTHER"))
							{
								ret.getReportData().get(ReportingConstants.RC_CHQ).getRawData().put(r.getCode(), r.getValue());
							}
							else if (r.getCode().startsWith("CURLOC"))
							{
								ret.getReportData().get(ReportingConstants.RC_CHQ).getRawData().put(r.getCode(), ReportingConstants.COUNTRY_CODES.get(r.getValue()));
							}else if (r.getCode().equals("CULT_000_OTHER")){
								ret.getReportData().get(ReportingConstants.RC_CHQ).getRawData().put(r.getCode(), r.getValue());
							}else if (r.getCode().startsWith("CULT_")){
								// This is the old way... worked so lon as the value was the country code
								//ret.getReportData().get(ReportingConstants.RC_CHQ).getRawData().put(r.getCode(), ReportingConstants.COUNTRY_CODES.get(r.getValue()));
								// Get the country code from the label as it is no longer in the value
								int idx = r.getCode().indexOf("_");
								String code = r.getCode().substring(idx+1);
								ret.getReportData().get(ReportingConstants.RC_CHQ).getRawData().put(r.getCode(), ReportingConstants.COUNTRY_CODES.get(code));
							}else if (r.getCode().equals("INDUST_OTHER")){
								ret.getReportData().get(ReportingConstants.RC_CHQ).getRawData().put(r.getCode(), r.getValue());
							}else if (r.getCode().startsWith("INDUST")){
								ret.getReportData().get(ReportingConstants.RC_CHQ).getRawData().put(r.getCode(), ReportingConstants.INDUST_CODES.get(r.getValue()));
							}else if (r.getCode().equals("FUNC_19_OTHER")){
								ret.getReportData().get(ReportingConstants.RC_CHQ).getRawData().put(r.getCode(), r.getValue());
							}else if (r.getCode().startsWith("FUNC_")){									
								ret.getReportData().get(ReportingConstants.RC_CHQ).getRawData().put(r.getCode(), ReportingConstants.FUNC_CODES.get(r.getValue()));
							}else if (r.getCode().startsWith("FUNC")){									
								// There was a change... demo puts out FUNCxxx.  Gotta handle that as well
								//ret.getReportData().get(ReportingConstants.RC_CHQ).getRawData().put(r.getCode(), ReportingConstants.FUNC_CODES.get(r.getValue()));											
								String code = r.getCode().substring(4);
								if (code.startsWith("0"))  code = code.substring(1);
								ret.getReportData().get(ReportingConstants.RC_CHQ).getRawData().put(r.getCode(), ReportingConstants.FUNC_CODES.get(code));											
							}else if (r.getCode().startsWith("ORGSIZE")){
								ret.getReportData().get(ReportingConstants.RC_CHQ).getRawData().put(r.getCode(), ReportingConstants.ORGSIZE_CODES.get(r.getValue()));
							}else if (r.getCode().equals("SAL_CURR")){
								ret.getReportData().get(ReportingConstants.RC_CHQ).getRawData().put(r.getCode(), ReportingConstants.SAL_CURR_CODES.get(r.getValue()));
							}else if (r.getCode().equals("RELOCATE")){
								ret.getReportData().get(ReportingConstants.RC_CHQ).getRawData().put(r.getCode(), ReportingConstants.YESNO_CODES.get(r.getValue()));
							}else if (r.getCode().startsWith("IMP_")){								
								// create the hashmap, with the value and the string...  
								if(r.getValue() == null || r.getValue().trim() == "" || r.getValue().isEmpty() || r.getValue().length() < 1){
									continue;
								}else {		
									impHash.put(new Integer(r.getValue()), ReportingConstants.IMP_CODES.get(r.getCode()));
								}								
							}else if (r.getCode().equals("MGRRESP")){
								ret.getReportData().get(ReportingConstants.RC_CHQ).getRawData().put(r.getCode(), ReportingConstants.YESNO_CODES.get(r.getValue()));
							}else if (r.getCode().equals("REL_CHNG")){
								ret.getReportData().get(ReportingConstants.RC_CHQ).getRawData().put(r.getCode(), ReportingConstants.YESNO_CODES.get(r.getValue()));
							}else if (r.getCode().equals("MGLEV")){
								ret.getReportData().get(ReportingConstants.RC_CHQ).getRawData().put(r.getCode(), ReportingConstants.MGLEV_CODES.get(r.getValue()));
							}
							else if (r.getCode().startsWith("UN_GRAD")){
								ret.getReportData().get(ReportingConstants.RC_CHQ).getRawData().put(r.getCode(), ReportingConstants.YESNO_CODES.get(r.getValue()));
							}else {
								ret.getReportData().get(ReportingConstants.RC_CHQ).getRawData().put(r.getCode(), XMLUtils.xmlFormatString(r.getValue()));
								//System.out.println("r.getCode()  " + r.getCode() + "  r.getValue()  " + r.getValue());
							}
						}	// End else (r != null)
					}	// End of AnswerMap loop

					// deal with the IMP_ hashMap, to create the string, and put that into the IR.
					// create a string, in value order... from an array that i'm creating in value order... 
					String impString = "";
					if(!impHash.isEmpty())
					{
						for (int i = 1; i <= 5; i++)
						{
							Integer index = new Integer(i);
							// check for null ( in case they didn't put in all 5 items )
							if(impHash.get(index) == null)
							{
								continue;
							}
							else
							{
								impString += "<br/>";
								impString += i + " - " + impHash.get(index);
							}
						}

						//put it in the IR
						ret.getReportData().get(ReportingConstants.RC_CHQ).getRawData().put("IMP_STRING", impString);
					}

					// now, fill in empty strings for any response that's not there... 
					// note that we never know which of the CHQ's was taken, or how much of a 
					// CHQ is actually finished when submitted... so we need to do this....
					for ( String code : ReportingConstants.CHQquestionArray )
					{
						if(ret.getReportData().get(ReportingConstants.RC_CHQ).getRawData().get(code) ==  null )
						{
							ret.getReportData().get(ReportingConstants.RC_CHQ).getRawData().put(code, "");
						}
					}
					
					// QUESTION DATA //
					//only need to do this once... 
					if(ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().get("PARTICIPANT_NAME") == null )
					{
						// Identification
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("PARTICIPANT_NAME",     "Name"); // get this from above
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("ORGANIZATION",    		"Company");  // from above
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("JOB_TITLE",    		"Current job title"); // chq JOB_TITLE
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("EMAIL",   "E-mail address"); // from above
						
						// Education
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("EDUC",   "Indicate your highest level of education:"); // EDUC_01 - _04
						
						// Education Grid
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("EDUC_GRID",  "List all education since high school.");	// no data - this is a header/instruction					
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("UNIV",   	  "College/University, Name, City, State/Province, Country");// UNIV_1 - _10	
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("UN1DTBEG",   "From (Year)");  // UN1DTBEG to UN10DTBEG
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("UN1DTEND",   "To (Year)");    // UN1DTEND to UN10DTEND
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("UN_GRAD1",   "Did You Graduate? (Yes/No)");  // to UN_GRAD10
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("UNI_DEG1",   "Degree");  // to UNI_DEG10
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("UNI_MAJ1",   "Major");  // UNI_MAJ10
						
						// Career and Work History
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("CURLOC",   "In what country/territory are you currently based?");
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("CURLOC_OTHER_000", "Other (specify):");
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("CULT",   "Throughout your entire career, in which countries/territories have you lived or worked long enough to develop a good understanding of the culture and work practices?");
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("CULT_000_OTHER", "Other (specify):");
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("INDUST",   "Which one of the following best describes the industry in which you work?"); // INDUST_01
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("INDUST_OTHER", "Other (specify):");
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("FUNC",   "In which functional areas do you primarily work?");  // FUNC_01
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("FUNC_19_OTHER", "Other (specify):");
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("ORGSIZE",   "How many people does your company employ, including all divisions?"); //ORGSIZE_04
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("MGRRESP",   "Do you supervise or manage people?");
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("MGDIR",   "In total, how many people do you manage, both directly and indirectly (i.e., your direct reports, their direct reports, etc.)?");
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("MGTENURE",   "How long have you been in management? (years)");
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("MGLEV",   "Which ONE of the following best represents your current management level?");
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("SAL_CURR",   "What is your approximate annual compensation for the current year, including commissions, bonuses, stock options, etc., but excluding benefits and perquisites? (select a currency, then provide amount of compensation)");
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("SCURR_OTHR", "Other (specify):");
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("SALARY_E",   "My annual compensation (numeric):");
						
						// Work History Grid
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("WORK_HIST_GRID",   "Please list your work history."); // instruction text
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("JOB",   "Organization"); // JOB_1 to _30 
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("JBCTY",   "City, State/Province, Country");  //JBCTY_1 to _30
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("POSTN",   "Positions Held");  // POSTN_1 to _30
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("JBDTFR",   "Date(Year) From"); // JBDTFR_1 to _30
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("JBDTTO",   "Date(Year) To");  // JBDTTO_1 to _30
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("JBRESP",   "Responsibilities"); // JBRESP_1 to _30
											
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("CURR_CHAL", "Within the scope of your current role, what are the biggest business challenges you need to address over the next year?");
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("GR_ACCOM", "What do you consider to be your greatest accomplishment?");
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("LIK_BEST", "What have you liked best about your work?");
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("LIK_LEST", "What have you liked least about your work?");
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("LEAD_EXP", "What leadership experiences, roles, and/or assignments were the most formative and/or had the most impact on your career, and why?");
						
						
						// Career Aspirations
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("CRER_MVE", "Currently, how interested are you in making a career move and why?");
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("RELOCATE", "Are you willing to relocate?");
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("REL_LOC", "Specify the locations to which you would be willing to relocate and/or any constraints on relocation.");
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("REL_CHNG", "Do you anticipate your willingness to relocate will change in the future?");
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("NEXT_POS", "What are you looking for in your next position?");
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("GOAL_3YR", "Ideally, what do you hope to accomplish in your career over the next three years?");
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("GOAL_10YR", "Ideally, what do you hope to accomplish in your career over the next ten years?");
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("PERS_BAR", "If you don't reach your goals, which of your personal attributes is most likely to have held you back?");
						
						//Self Evaluation
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("STRENGTHS", "What strengths have been consistently observed in you, either through self-reflection or by others?");
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("WEAKNESS", "What would you or others identify as your areas of greatest relative weakness or development need?");
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("OTHR_INT", "Sometimes people misinterpret what we do.  How do others see you that is different from how you really are?");
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("IMPRV_ATT", "What attributes have you improved upon over the years?");
						
						//Work Environment
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("IDEAL_JB", "Describe your ideal job:");
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("IMP", "Please review the list below and rank the five factors that are most important to you in a job.  Enter the numerals 1, 2, 3, 4, and/or 5 to indicate your ranking, with 1 being the highest and 5 the lowest of your choices.  Rank no more than 5 items, although you may rank fewer than five.  Do not give the same ranking to more than one item (e.g., do not rank two factors as '3').");
																							   // _ADVC  _FAST  _FRND... 
						
						// Comments
						ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("OTHR_COM", "Please provide any additional information you feel would help us better understand you and your career.");

						// Confidential Demographic Information 
						// these are not actually used in the report, because they are confidential so i will not put them in the display list
//							ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("ERLYR", "In which countries/territories did you spend most of your early years (before age 13)?");
//							ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("ERLYR_000_OTHER", "Other (specify):");
//							ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("CITZ", "In which country/territory (or countries/territories) are you a national citizen?");
//							ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("CITZ_000_OTHER", "Other (specify):");
//							ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("LANG", "What is your primary language (mother tongue)?");
//							ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("LANG_OTHER_99", "Other (specify):");
//							ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("YRWKFRC", "How many years have you been in the workforce?");
//							ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("YOB", "Year of Birth:");
//							ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("SEX", "Gender:");
//							ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("ETHNIC", "Ethnicity (US Only):");
//							ret.getReportData().get(ReportingConstants.RC_CHQ).getDisplayData().put("ETHNIC_99", "Other (specify):");
					}
				}	// End of else (there IS answer data and there is a chq or demo instrument
			} // end main for-loop (epis)

			// Do suppression on the labels (tg was passed in)
			HashMap<String,String> lblMap = new HashMap<String,String>();
			for (Iterator<Map.Entry<String, Text>> itr= tg.getLabelMap().entrySet().iterator(); itr.hasNext(); )
			{
				// get the next entry
				Map.Entry<String, Text> ent = itr.next();

				// get the data
				String key = ent.getKey();
				Text txt = ent.getValue();

				// massage it
				String out = StringUtils.formatNonSpacingTxt(txt.getText());
				// replace the string in the label value hashmap
				lblMap.put(key, out);
			}
			ret.getLabelData().putAll(lblMap);
			
			// now do the same with the answer data
			HashMap<String, String> raw = ret.getReportData().get(ReportingConstants.RC_CHQ).getRawData();
			if (raw != null)
			{
				for (Iterator<Map.Entry<String, String>> itr= raw.entrySet().iterator(); itr.hasNext(); )
				{
					// get the next entry
					Map.Entry<String, String> ent = itr.next();
	
					// get the data
					String key = ent.getKey();
					String str = ent.getValue();
					if (str == null)
					{
						str = "Invalid data";
						System.out.println("ERROR:  Invalid data (null) detected in CHQ generation.  Key=" + key);
					}
					// massage it to remove xml unfriendly stuff...
					String out = XMLUtils.xmlEscapeString(str);					
					// massage it to add spaces for conji...
					out = StringUtils.formatNonSpacingTxt(out);
					
					// Suppress escaped quotes & double quotes
					// Can do this in here because there is no downstream XML
					out = out.replaceAll("\\\\\'", "\'");
					out = out.replaceAll("\\\\\"", "\"");
					
					// replace the string in the rawData hashmap
					ret.getReportData().get(ReportingConstants.RC_CHQ).getRawData().put(key, out);
				}
			}

			return ret;
		}
		catch(Exception e)
		{
			System.out.println("doCHQDetailRpt:");
			e.printStackTrace();
			return null;
		}
//		finally
//		{
//			try
//			{
//				if(con != null)
//				{
//					con.close();
//				}					
//			}
//			catch(Exception se)
//			{
//				se.printStackTrace();
//			}
//		}
	}
}
