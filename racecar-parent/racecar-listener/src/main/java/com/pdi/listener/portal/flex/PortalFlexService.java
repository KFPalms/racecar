/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.listener.portal.flex;

import java.util.ArrayList;

import com.pdi.data.util.RequestStatus;
import com.pdi.data.v3.dto.CandidateListDTO;
import com.pdi.data.v3.dto.ClientDTO;
import com.pdi.data.v3.dto.ClientListDTO;
import com.pdi.data.v3.dto.EmailDTO;
import com.pdi.data.v3.dto.EmailGroupListDTO;
import com.pdi.data.v3.dto.EmailListDTO;
import com.pdi.data.v3.dto.EmailRecipientTypeListDTO;
import com.pdi.data.v3.dto.EmailTextDTO;
import com.pdi.data.v3.dto.LanguageListDTO;
import com.pdi.data.v3.dto.ParticipantDTO;
import com.pdi.data.v3.dto.ParticipantEmailListDTO;
import com.pdi.data.v3.dto.ParticipantListDTO;
import com.pdi.data.v3.dto.ParticipantReportListDTO;
import com.pdi.data.v3.dto.ProductDTO;
import com.pdi.data.v3.dto.ProjectDTO;
import com.pdi.data.v3.dto.ProjectListDTO;
import com.pdi.data.v3.dto.SessionUserDTO;
import com.pdi.data.v3.dto.UserDTO;
import com.pdi.data.v3.helpers.CandidateDataHelper;
import com.pdi.data.v3.helpers.EmailDataHelper;
import com.pdi.data.v3.helpers.EmailGroupDataHelper;
import com.pdi.data.v3.helpers.EmailRecipientTypeDataHelper;
import com.pdi.data.v3.helpers.LanguageDataHelper;
import com.pdi.data.v3.helpers.ParticipantDataHelper;
import com.pdi.data.v3.helpers.ProjectDataHelper;
import com.pdi.guid.GUID;
import flex.messaging.FlexContext;
import flex.messaging.FlexSession;

/**
 * PortalFlexService is the communication layer between flex's portal application and v3
 * 
 * @author		Gavin Myers
 */
public class PortalFlexService
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//

	//
	// Constructors.
	//
	public PortalFlexService()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//

	/////////////////
	// Private methods
	/////////////////

	/**
	 * internal function to see if the user is connected to the session
	 * @return
	 * @throws Exception
	 */
	private boolean isConnected()
	{
		return FlexContext.getFlexSession().getAttribute("user") != null;
	}

	
	/**
	 * internal function to get the user from the session
	 * @return
	 * @throws Exception
	 */
	private SessionUserDTO getSessionUser()
	{
		SessionUserDTO sessionUser = new SessionUserDTO();
		
		FlexSession session = FlexContext.getFlexSession();
		if(isConnected())
		{
			sessionUser = (SessionUserDTO)session.getAttribute("user");
		}
		else
		{
			sessionUser.setStatus(new RequestStatus(RequestStatus.RS_ERROR, "User Not in Session"));
		}
		
		return sessionUser;
	}

	
	/**
	 * Internal function to check access to a client for the user in the session
	 * NOTE: Fill this out when more info is known
	 * @param client
	 * @return
	 */
//	private boolean hasAccess(ClientDTO client)
//	{
//		return true;
//	}

	
	/**
	 * Internal function to check access to a project for the user in the session
	 * NOTE: Fill this out when more info is known
	 * @param project
	 * @return
	 */
//	private boolean hasAccess(ProjectDTO project)
//	{
//		return true;
//	}


	/////////////////
	// Public methods
	/////////////////
	
	/**
	 * Method to test that there is a connection to the RPC
	 * NOTE:  I don't think that is correct.  I think it only talks to the Flex session
	 * @return session id string
	 */

	public String testConnection()
	{
		FlexSession session = FlexContext.getFlexSession();
		if(session.getAttribute("id") == null)
		{
			session.setAttribute("id", new GUID().toString());
		}
		
		return session.getAttribute("id").toString();
	}
	

	/**
	 * Log the user into the system, add the user to the session, return user information
	 * @param username
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public UserDTO login(String username, String password)
	{
		return new PortalFlexServiceV2().login(username, password);
		//TODO: THIS IS A STOPGAP SOLUTION
		/*
		 * REMOVED FOR EASY ACCESS TO V2. THIS 
		SessionUserDTO sessionUser = UserDataHelper.getUser(username, password);	

		if(sessionUser.getStatus().isOk())
		{
			FlexSession session = FlexContext.getFlexSession();
			if(session == null)
			{
				sessionUser.setStatus(new RequestStatus(RequestStatus.RS_ERROR, "Session is not available, are you not communicating to this via flash services?"));
			}
			else
			{
				session.setAttribute("user",sessionUser);
			}
		}
		else
		{
			// Unable to log in
			sessionUser.getStatus().setExceptionMessage(sessionUser.getStatus().getExceptionMessage() + " - Possible Invalid Username/Password.");
		}

		return new UserDTO(sessionUser);
		*/
	}
	

	/**
	 * Get all candidates that a particular user has access to
	 * @param sessionUser
	 * @return
	 */
	public CandidateListDTO getCandidates()
	{

		if(! isConnected())
		{
			return new CandidateListDTO(new RequestStatus(RequestStatus.RS_ERROR,"Invalid session - no user (Not logged in)"));
		}
		return CandidateDataHelper.getCandidates(getSessionUser());

	} 

	
	/**
	 * get all clients
	 * @return
	 */
	public ClientListDTO getClients() throws Exception
	{
		return new PortalFlexServiceV2().getClients();
		//TODO: THIS IS A STOPGAP SOLUTION
		//return ClientDataHelper.getClients(getSessionUser());
	} 
	
	
	/**
	 * get all projects by client
	 * @param client
	 * @return
	 */
	public ProjectListDTO getProjects(ClientDTO client) throws Exception
	{
		return new PortalFlexServiceV2().getProjects(client);
		//TODO: THIS IS A STOPGAP SOLUTION
		//return ProjectDataHelper.getProjects(getSessionUser(),client);
	}
	
	
	/**
	 * fill out a project (uses URI or Project id)
	 * @param project DTO
	 * @return
	 */
	public ProjectDTO getProject(ProjectDTO proj)
	{
		return ProjectDataHelper.getProject(getSessionUser(),proj);
	}
	
	
	/**
	 * get all participants by project
	 * @param project
	 * @return
	 */
	public ParticipantListDTO getParticipants(ProjectDTO project) throws Exception
	{
		return new PortalFlexServiceV2().getParticipants(project);
		//TODO: THIS IS A STOPGAP SOLUTION
		//return ParticipantDataHelper.getParticipants(getSessionUser(), project);
	}

	
	/**
	 * get participants + module status for a project - NO RESULTS DATA
	 * @param project
	 * @return
	 */
	public ParticipantListDTO getParticipantsWithModules(ProjectDTO project)
	{
		return ParticipantDataHelper.getParticipantsWithModules(getSessionUser(), project);
	}
	
	
	/**
	 * get participant "full data" all score values, all answer values
	 * @param participant
	 * @return
	 */
	public ParticipantDTO getParticipantResultData(ParticipantDTO participant)
	{
		return ParticipantDataHelper.getParticipantResults(getSessionUser(), participant);
	}


	/**
	 * returns a list of language objects
	 * @param participant
	 * @return
	 * @throws Exception
	 */
	public LanguageListDTO getLanguages()
	{
		if(! isConnected())
		{
			return new LanguageListDTO(new RequestStatus(RequestStatus.RS_ERROR,"Invalid session - no user (Not logged in)"));
		}

		return LanguageDataHelper.getAllLanguages(getSessionUser());
	}


	/**
	 * returns a list of recipient types
	 * @param participant
	 * @return
	 */
	public EmailRecipientTypeListDTO getAllEmailRecipientTypes()
	{
		if(! isConnected())
		{
			return new EmailRecipientTypeListDTO(new RequestStatus(RequestStatus.RS_ERROR,"Invalid session - no user (Not logged in)"));
		}
		return EmailRecipientTypeDataHelper.getAllRecipientTypes(getSessionUser());
	}


	/**
	 * returns a list of groups
	 * @param participant
	 * @return
	 */
	public EmailGroupListDTO getAllEmailGroups()
	{
		if(! isConnected())
		{
			return new EmailGroupListDTO(new RequestStatus(RequestStatus.RS_ERROR,"Invalid session - no user (Not logged in)"));
		}

		return EmailGroupDataHelper.getAllGroups(getSessionUser());
	}

	
	/**
	 * get all active e-mail proxies in all groups
	 * 	NOTE:  Use this with caution as it exposes ALL client specific data
	 * 
	 * @return an EmailListDTO object
	 * @throws Exception
	 */
	public EmailListDTO getGroupedEmails()
	{
		if(! isConnected())
		{
			return new EmailListDTO(new RequestStatus(RequestStatus.RS_ERROR,"Invalid session - no user (Not logged in)"));
		}
		return EmailDataHelper.getAllProxyList(getSessionUser());
	}

	
	/**
	 * get e-mail proxies for a client
	 * @param client A ClientDTO object with at least the client id present.
	 * @return an EmailListDTO object
	 * @throws Exception
	 */
	public EmailListDTO getClientEmails(ClientDTO client)
	{
		if(! isConnected())
		{
			return new EmailListDTO(new RequestStatus(RequestStatus.RS_ERROR,"Invalid session - no user (Not logged in)"));
		}
		return EmailDataHelper.getClientProxyList(getSessionUser(),client);
	}
	
	
	/**
	 * Get e-mail proxies for a product
	 * Note that the client id is required as well
	 * 
	 * @param client A ClientDTO object with at least the client id present.
	 * @param product A ProductDTO object with at least the product id present.
	 * @return an EmailListDTO object
	 * @throws Exception
	 */
	public EmailListDTO getProductEmails(ClientDTO client, ProductDTO product)
	{
		if(! isConnected())
		{
			return new EmailListDTO(new RequestStatus(RequestStatus.RS_ERROR,"Invalid session - no user (Not logged in)"));
		}
		return EmailDataHelper.getProductProxyList(getSessionUser(),client, product);
	}
	
	
	/**
	 * Get e-mail proxies for a client/product
	 * 
	 * @param client A ClientDTO object with at least the client id present.
	 * @param product A ProductDTO object with at least the product id present.
	 * @return an EmailListDTO object
	 * @throws Exception
	 */
	public EmailListDTO getProdClientEmails(ClientDTO client, ProductDTO product)
	{
		if(! isConnected())
		{
			return new EmailListDTO(new RequestStatus(RequestStatus.RS_ERROR,"Invalid session - no user (Not logged in)"));
		}
		return EmailDataHelper.getProdClientProxyList(getSessionUser(),client, product);
	}
	
	/**
	 * Get a list of emails with participants
	 * @param sessionUser
	 * @param participantList
	 * @return
	 */
	public ArrayList<ParticipantEmailListDTO> getParticipantEmailList(ParticipantListDTO participantList) {
		if(! isConnected())
		{
			return null;
		}		
		return EmailDataHelper.getParticipantEmailList(getSessionUser(), participantList);
	}
	
	/**
	 * Get a list of reports with participants
	 * @param participantList
	 * @return
	 */
	public ArrayList<ParticipantReportListDTO> getParticipantReportList(ParticipantListDTO participantList) throws Exception {
		if(! isConnected())
		{
			return null;
		}

		return new PortalFlexServiceV2().getParticipantReportList(participantList);
		//return ReportDataHelper.getParticipantReportList(getSessionUser(),participantList);
	}
	
	/**
	 * Get the text content of an email in all languages
	 * Note that the data in the EmailDTO is completely
	 * refreshed;  only the emailId is used in the method.
	 * @param An EmailDTO object with the desired email id set
	 * @return
	 * @throws Exception
	 */
	public EmailDTO getEmailText(EmailDTO email)
	{
		if(! isConnected())
		{
			return new EmailDTO(new RequestStatus(RequestStatus.RS_ERROR,"Invalid session - no user (Not logged in)"));
		}
		return EmailDataHelper.getEmail(getSessionUser(),email);
	}


	/**
	 * change the basic values of an email
	 * @param email The update EmailDTO object (text ignored)
	 * @return
	 * @throws Exception
	 */
	public RequestStatus updateEmail(EmailDTO email)
	{
		if(! isConnected())
		{
			return new RequestStatus(RequestStatus.RS_ERROR,"Invalid session - no user (Not logged in)");
		}
		
		return EmailDataHelper.updateEmailItem(getSessionUser(),email);
	}


	/**
	 * Create a new email item (no text)
	 * @param email The EmailDTO object to insert (text ignored)
	 * @return
	 * @throws Exception
	 */
	public EmailDTO insertEmail(EmailDTO email)
	{
		if(! isConnected())
		{
			return new EmailDTO(new RequestStatus(RequestStatus.RS_ERROR,"Invalid session - no user (Not logged in)"));
		}
		
		return EmailDataHelper.addEmailItem(getSessionUser(),email);
	}
	
	
	/**
	 * Change the values of an email text
	 * @param An EmailTextDTO with the emailTextId and the desired new text present
	 * @return
	 * @throws Exception
	 */
	public RequestStatus updateEmailText(EmailTextDTO emailText)
	{
		if(! isConnected())
		{
			return new RequestStatus(RequestStatus.RS_ERROR,"Invalid session - no user (Not logged in)");
		}

		return EmailDataHelper.updateEmailText(getSessionUser(),emailText);
	}
	
	
	/**
	 * Insert a new email text
	 * @param An EmailTextDTO with the text id, type id, and desired texts both present
	 * @return
	 * @throws Exception
	 */
	public EmailTextDTO insertEmailText(EmailTextDTO emailText)
	{
		if(! isConnected())
		{
			return new EmailTextDTO(new RequestStatus(RequestStatus.RS_ERROR,"Invalid session - no user (Not logged in)"));
		}

		return EmailDataHelper.addEmailText(getSessionUser(),emailText);
	}
	
	
	/**
	 * send an email to a list of one or more participants
	 * @param participant
	 * @return
	 * @throws Exception
	 */
	public ArrayList<RequestStatus> sendEmail(EmailDTO email, ParticipantListDTO participants)
	{
		if(! isConnected())
		{
			ArrayList<RequestStatus> al = new ArrayList<RequestStatus>();
			al.add(new RequestStatus(RequestStatus.RS_ERROR,"Invalid session - no user (Not logged in)"));
			return al;
		}

		// Eventually, this should be threaded, but for now, loop through
		// the participant list and send the emails off one at a time.
		
		ArrayList<RequestStatus> rsList = new ArrayList<RequestStatus>();
		for (ParticipantDTO part : participants.getParticipantList())
		{
			RequestStatus rs = EmailDataHelper.sendEmail(getSessionUser(), email, part);
			rsList.add(rs);
		}
		
		return rsList;
	}
}
