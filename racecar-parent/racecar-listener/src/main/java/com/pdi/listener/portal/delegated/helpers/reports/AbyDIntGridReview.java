/**
 * Copyright (c) 2017 Korn Ferry
 * All rights reserved.
 */
package com.pdi.listener.portal.delegated.helpers.reports;

import java.sql.Connection;
import java.text.SimpleDateFormat;
//import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdi.data.abyd.dto.common.CompElementDTO;
import com.pdi.data.abyd.dto.common.CompGroupData;
import com.pdi.data.abyd.dto.common.EGBarDisplayData;
import com.pdi.data.abyd.dto.common.EGCompDisplayData;
import com.pdi.data.abyd.dto.common.EGFullDataDTO;
import com.pdi.data.abyd.dto.common.EGIrDisplayData;
import com.pdi.data.abyd.dto.common.IGInstCompData;
import com.pdi.data.abyd.dto.intGrid.IGCompDataDTO;
import com.pdi.data.abyd.dto.intGrid.IGInstDisplayData;
import com.pdi.data.abyd.dto.intGrid.IGModuleData;
import com.pdi.data.abyd.dto.intGrid.IGScaleScoreData;
import com.pdi.data.abyd.dto.intGrid.IGSummaryDataDTO;
import com.pdi.data.abyd.helpers.common.EGDataHelper;
import com.pdi.data.abyd.helpers.intGrid.IGCompLineDataHelper;
import com.pdi.data.abyd.helpers.intGrid.IGKf4dDataHelper;
import com.pdi.data.abyd.helpers.intGrid.IGSummaryDataHelper;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
//import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.data.dto.ProjectParticipantInstrument;
import com.pdi.data.dto.TextGroup;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.string.StringUtils;
import com.pdi.xml.XMLUtils;

/*
 * AbyDIntGridReview - class to hold methods relevant to the AbyDIntGridReview
 *
 * NOTE:  EVEN THOUGH LOCATED UNDER THE DELEGATED FOLDER, THERE ARE NO METHODS IN THIS
 *        CLASS THAT ARE DEFINED IN THE DATA INTERFACE.
 *
 * This class is located here because the folder structure (delegated/helpers) is
 * inverted from normal practice elsewhere in this project stack (helpers/delegated).
 * Putting this helper class here avoided confusion with having two folders in the
 * portal structure that had the name "helper".
 */
public class AbyDIntGridReview {

	private static final Logger log = LoggerFactory.getLogger(AbyDIntGridReview.class);
	
	public AbyDIntGridReview() {
		// No logic here at the present time
	}

	//
	// Instance methods.
	//

	/**
	 * getEvalGuideData - Method that retrieves the requisite data for the AbyD
	 * Eval Guide
	 *
	 * @param inp
	 *            - An IndividualReport object with a small amount of relevant
	 *            data
	 * @param epis
	 *            - The ParojectParticipantReport objects associated with the
	 *            request
	 * @param tg
	 *            - The static text labels (text groups) used to generate the
	 *            report
	 */
	public IndividualReport getIntGridData(IndividualReport inp, ArrayList<ProjectParticipantInstrument> epis,
			TextGroup tg, HashMap<String, String> overrides) {

		IndividualReport ret = inp;
		Connection con = null;

		try {

			// Get the participant info... assumes that all epis are for the
			// same participant
			ProjectParticipantInstrument epi_x = epis.get(0);
			String participantId = epi_x.getParticipant().getId();

			for (ProjectParticipantInstrument epi : epis) {
				log.debug("Begin participant project instrument loop: {}", epi.toString());
				if (participantId == null || participantId.equalsIgnoreCase("")) {
					participantId = "";
				}

				// ret.setScriptedData(reportInput.getCurrentTLT().getScriptedData());

				/*
				 * ========================================
				 * ADD BASIC PARTICIPANT DATA TO REPORT
				 */

				ret.addDisplayData("FIRST_NAME", XMLUtils.xmlEscapeString(epi.getParticipant().getFirstName()));
				ret.addDisplayData("LAST_NAME", XMLUtils.xmlEscapeString(epi.getParticipant().getLastName()));
				ret.getDisplayData().put("PARTICIPANT_NAME", XMLUtils.xmlEscapeString(
						epi.getParticipant().getFirstName() + " " + epi.getParticipant().getLastName()));

				// Get Project data
				ret.addDisplayData("ORGANIZATION", XMLUtils.xmlEscapeString(epi.getProject().getClient().getName()));
				ret.addDisplayData("PROJECT_NAME", XMLUtils.xmlEscapeString(epi.getProject().getName()));

				// File Name
				String fileName = StringUtils.removeFilenameSpecialCharacters(epis.get(0).getParticipant().getLastName()
						+ " " + epis.get(0).getParticipant().getFirstName() + " " + inp.getReportCode() + " "
						+ new SimpleDateFormat("yyyyMMdd").format(new Date()));

				ret.setName(fileName.replace(" ", "_") + ".pdf");

				/*
				 *  Set up labels/option data/entry state
				 *  --------------------------------------
				 */
				// ReportOptionsDataHelper optionsDH = new
				// ReportOptionsDataHelper();
				// ReportLabelsDataHelper labelsDH = new
				// ReportLabelsDataHelper();
				// EntryStateHelper entryH = new EntryStateHelper(con,
				// projectId);

				// Setup2ReportOptionsDTO options =
				// optionsDH.getReportOptionData(con,dnaId);
				// Setup2ReportLabelsDTO labels =
				// labelsDH.getTranslatedReportLabelData(dnaId,
				// tg.getLanguageCode(), this.EDITABLE );
				// EntryStateDTO entryState = entryH.getEntryStateData();

				ret.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);

				java.util.Date date = new java.util.Date();
				SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
				ret.getDisplayData().put("TODAY_DATE", sdf.format(date));

				// ReportData rd = new ReportData();

				con = AbyDDatabaseUtils.getDBConnection();
				
				

				int dnaId = Integer.parseInt(overrides.get("dnaId"));

				Map<Integer, String> mModuleIds = new HashMap<Integer, String>();
				TreeMap<Integer, Integer> tmModSeq = new TreeMap<Integer, Integer>();

				IGSummaryDataHelper helper = new IGSummaryDataHelper(con, participantId, dnaId);
				IGSummaryDataDTO data = helper.getSummaryData();

				String partId = data.getPartId();
				
				//IGKf4dDataHelper kf4dHelper = new IGKf4dDataHelper(con, partId, dnaId, helper.getProjectInfo());

				String leadFirstName = XMLUtils.xmlEscapeString(data.getLcFname());
				String leadLastName = XMLUtils.xmlEscapeString(data.getLcLname());

				ret.getDisplayData().put("LEAD_CONSULTANT", leadFirstName + " " + leadLastName);

				ArrayList<CompGroupData> fullCompArray = data.getGridData().getFullCompArray();

				int compCounter = 0;

				// boolean hasALP = data.getAlpDataAvailable();

				for (int i = 0; i < fullCompArray.size(); i++) {
					
					log.trace("Begin full comp array loop: {}", i);

					CompGroupData compGroup = fullCompArray.get(i);

					ArrayList<CompElementDTO> compElementArray = compGroup.getCgCompList();

					for (int j = 0; j < compElementArray.size(); j++) {
						
						log.trace("Begin comp element array loop: {}", j);

						long compId = compElementArray.get(j).getCompId();
						String compName = XMLUtils.xmlEscapeString(compElementArray.get(j).getCompName());

						IGCompLineDataHelper compHelper = new IGCompLineDataHelper(con, compId, partId, dnaId, helper.getKf4dHelper());
						IGCompDataDTO compData = compHelper.getCompLine();

						String compDisplayName = XMLUtils.xmlEscapeString(compData.getCompDisplayName());
						String compNotes = XMLUtils.xmlEscapeString(compData.getIntNotes());

						int showCompNotes = 0;

						if (!(compNotes == null) && compNotes.length() > 0) {
							showCompNotes = 1;
							compNotes = "Notes: " + compNotes;
						}

						int scoreIdx = compData.getIntScoreIdx();
						String[] scoreTranslated = { "N/A", "1.00", "1.50", "2.00", "2.50", "3.00", "3.50", "4.00",
								"4.50", "5.00" };
						String scoreText = scoreTranslated[scoreIdx];

						ret.getDisplayData().put("IGCompName_" + compCounter, compName);
						ret.getDisplayData().put("IGCompDisplayName_" + compCounter, compDisplayName);
						ret.getDisplayData().put("IGCompScoreIdx_" + compCounter, "" + scoreText);
						ret.getDisplayData().put("IGCompNotes_" + compCounter, "" + compNotes);
						ret.getDisplayData().put("IGShowCompNotes_" + compCounter, "" + showCompNotes);

						ArrayList<IGModuleData> moduleData = compData.getModArray();

						int moduleCount = moduleData.size() - 1;
						ret.getDisplayData().put("ModuleCount_" + compCounter, "" + moduleCount);

						for (int k = 0; k < moduleData.size(); k++) {
							

							long moduleId = moduleData.get(k).getModuleId();
							String moduleName = XMLUtils.xmlEscapeString(moduleData.get(k).getModuleName());
							String specialType = moduleData.get(k).getSpecialType();
							long modSeq = moduleData.get(k).getModSeq();
							
							log.trace("Begin module data loop: {}, Module id: {}, Module Name: {}, Special type: {}", k, moduleId, moduleName, specialType);

							ret.getDisplayData().put("ModuleID_" + compCounter + "_" + k, "" + moduleId);
							ret.getDisplayData().put("ModuleName_" + compCounter + "_" + k, moduleName);

							// moduleData.get(k).getInstDispData();

							// Use the display name
							// if ("Testing (KF360)".equals(moduleName)) {
							// // External data module
							// mModuleIds.put((int) moduleId, moduleName);
							// tmModSeq.put((int) modSeq, (int) moduleId);
							//
							// ret.getDisplayData().put("ModuleType_" +
							// compCounter + "_" + k, "kf360");
							// }
							// else if (moduleId > 0)
							if (moduleId > 0) {
								// This is a "regular" EG
								mModuleIds.put((int) moduleId, moduleName);
								tmModSeq.put((int) modSeq, (int) moduleId);

								ret.getDisplayData().put("ModuleType_" + compCounter + "_" + k, "bar");

								EGCompDisplayData egData = moduleData.get(k).getCompDispData();

								String moduleNotes = XMLUtils.xmlEscapeString(egData.getCompNotes());

								ret.getDisplayData().put("ModuleNotes_" + compCounter + "_" + k,
										"Notes: " + moduleNotes);

								int compOverride = egData.getOverideScoreIndex();

								ret.getDisplayData().put("egCompOverride_" + compCounter + "_" + k, "" + compOverride);

								ArrayList<EGBarDisplayData> bars = egData.getCompBars();
								int barsCount = bars.size();
								int adjBarsCount = barsCount - 1;
								ret.getDisplayData().put("BarCount_" + compCounter + "_" + k, "" + adjBarsCount);

								double avgScore = 0.00;
								double roundedAvgScore = 0.00;
								double cum = 0;
								int cnt = 0;
								String avgFormatted = "";

								String preString = "";
								if ("ES".equals(specialType) || "KF360".equalsIgnoreCase(moduleName)) {
									ret.getDisplayData().put("BarCount_" + compCounter + "_" + k, "0");
									ret.getDisplayData().put("ModuleType_" + compCounter + "_" + k, "kf360");

									// Externally scored data
									String extScoreString = egData.getExtCompScore();
									if (extScoreString == null || extScoreString.isEmpty()
											|| extScoreString.equals("-1.00")) {
										extScoreString = "0.0";
									}
									roundedAvgScore = Double.valueOf(extScoreString);

									if (roundedAvgScore == 0.00) {
										avgFormatted = "N/A";
									} else {
										avgFormatted = String.format("%.02f", roundedAvgScore);
									}

									ret.getDisplayData().put("ModuleAvg_" + compCounter + "_" + k,
											preString + avgFormatted);

								} else {
									for (int l = 0; l < bars.size(); l++) {

										int barScore = bars.get(l).getBarScore();

										if (barScore == -1) {
											barScore = 0;
										}

										String hiText = "Highly Effective (5) "
												+ XMLUtils.xmlEscapeString(bars.get(l).getBarHiText());
										String hiExText = XMLUtils.xmlEscapeString(bars.get(l).getBarHiExText());
										String midText = "Solid (3) "
												+ XMLUtils.xmlEscapeString(bars.get(l).getBarMidText());
										String midExText = XMLUtils.xmlEscapeString(bars.get(l).getBarMidExText());
										String loText = "Needs Improvement (1) "
												+ XMLUtils.xmlEscapeString(bars.get(l).getBarLoText());
										String loExText = XMLUtils.xmlEscapeString(bars.get(l).getBarLoExText());

										ret.getDisplayData().put("BarScore_" + compCounter + "_" + k + "_" + l,
												"" + barScore);
										ret.getDisplayData().put("BarHiText_" + compCounter + "_" + k + "_" + l,
												hiText);
										ret.getDisplayData().put("BarHiExText_" + compCounter + "_" + k + "_" + l,
												hiExText);
										ret.getDisplayData().put("BarMidText_" + compCounter + "_" + k + "_" + l,
												midText);
										ret.getDisplayData().put("BarMidExText_" + compCounter + "_" + k + "_" + l,
												midExText);
										ret.getDisplayData().put("BarLoText_" + compCounter + "_" + k + "_" + l,
												loText);
										ret.getDisplayData().put("BarLoExText_" + compCounter + "_" + k + "_" + l,
												loExText);

										if (barScore > 0) {
											cum += barScore;
											cnt++;
										}
									}

									if (compOverride == 0) {
										if (cnt > 0) {
											// avgScore = (zeroCnt > 1) ? 0.00 :
											// (cum / (double)cnt);
											avgScore = (cum / cnt);
											roundedAvgScore = Math.round(avgScore * 100.0) / 100.0;
										}
									} else {
										avgScore = (compOverride + 1.00) / 2.00;
										roundedAvgScore = Math.round(avgScore * 100.0) / 100.0;
										preString = "*";
									}

									if (roundedAvgScore == 0.00) {
										avgFormatted = "N/A";
									} else {
										avgFormatted = String.format("%.02f", roundedAvgScore);
									}

									ret.getDisplayData().put("ModuleAvg_" + compCounter + "_" + k,
											preString + avgFormatted);
								}

							} else if (moduleId == 0) {
								// This is a testing column
								ret.getDisplayData().put("ModuleType_" + compCounter + "_" + k, "assess");

								IGInstDisplayData igInstData = moduleData.get(k).getInstDispData();
								ArrayList<IGInstCompData> igInstCompData = igInstData.getCompArray();

								// boolean cogsDone = igInstData.getCogsDone();
								// boolean hasGPI = igInstData.getGpiIncluded();
								boolean hasKfp = igInstData.getHasKfp();
								boolean hasKf4d = igInstData.getHasKfar();

								for (int m = 0; m < igInstCompData.size(); m++) {

									ArrayList<IGScaleScoreData> igScaleScoreData = igInstCompData.get(m)
											.getCogColumnScores();

									String assessCog = "0";
									int cogCount = -1;

									if (igScaleScoreData.size() > 0) {
										assessCog = "1";
										cogCount = igScaleScoreData.size() - 1;
									}

									ret.getDisplayData().put("AssessCog_" + compCounter + "_" + k, assessCog);
									ret.getDisplayData().put("AssessCogCount_" + compCounter + "_" + k, "" + cogCount);

									for (int n = 0; n < igScaleScoreData.size(); n++) {

										String scaleName = XMLUtils
												.xmlEscapeString(igScaleScoreData.get(n).getScaleName());
										String scaleRating = igScaleScoreData.get(n).getRating();

										ret.getDisplayData().put("CogScaleName_" + compCounter + "_" + k + "_" + n,
												scaleName);
										ret.getDisplayData().put("CogScaleRating_" + compCounter + "_" + k + "_" + n,
												scaleRating);
									}

									ArrayList<IGScaleScoreData> igGPIData = igInstCompData.get(m).getGpiColumnScores();

									String assessGPI = "0";
									String cogInstrument = "";
									int gpiCount = -1;

									if (igGPIData.size() > 0) {
										assessGPI = "1";
										gpiCount = igGPIData.size() - 1;
									}

									if (hasKfp) {
										cogInstrument = "KFALP";
									} else if (hasKf4d) {
										cogInstrument = "KF4D";
									} else {
										cogInstrument = "GPI";
									}

									ret.getDisplayData().put("CogInstrument_" + compCounter + "_" + k, cogInstrument);

									ret.getDisplayData().put("AssessGPI_" + compCounter + "_" + k, assessGPI);
									ret.getDisplayData().put("AssessGPICount_" + compCounter + "_" + k, "" + gpiCount);

									for (int p = 0; p < igGPIData.size(); p++) {

										String scaleName = XMLUtils.xmlEscapeString(igGPIData.get(p).getScaleName());
										String scaleRating = igGPIData.get(p).getRating();

										ret.getDisplayData().put("GPIScaleName_" + compCounter + "_" + k + "_" + p,
												scaleName);
										ret.getDisplayData().put("GPIScaleRating_" + compCounter + "_" + k + "_" + p,
												scaleRating);
									}
									
								} // End of comp loop
								log.trace("End of comp loop");
							} // End of testing column processing
							log.trace("end of testing column processing");
						} // End of module loop
						log.trace("end of module loop. compCounter: {}", compCounter);

						compCounter = compCounter + 1;
					}

				}

				int adjCompCounter = compCounter - 1;
				ret.getDisplayData().put("CompCount", "" + adjCompCounter);

				// Get Impact Ratings and Overall data

				int modCount = 0;

				for (Map.Entry<Integer, Integer> entry : tmModSeq.entrySet()) {

					int modId = entry.getValue();

					String modName = mModuleIds.get(modId);
					ret.getDisplayData().put("ModName_" + modCount, "" + modName);

					EGDataHelper egHelper = new EGDataHelper(con, participantId, dnaId, modId);
					EGFullDataDTO egData = egHelper.getEGImpactandOverallData();

					int impactCount = egData.getIrArray().size();
					int adjImpactCount = impactCount - 1;
					ret.getDisplayData().put("ImpactCount_" + modCount, "" + adjImpactCount);
					String impactNotes = XMLUtils.xmlEscapeString("Notes: " + egData.getIrNotes());
					ret.getDisplayData().put("ImpactNotes_" + modCount, "" + impactNotes);

					for (int i = 0; i < egData.getIrArray().size(); i++) {

						EGIrDisplayData ratings = egData.getIrArray().get(i);

						String irText = XMLUtils.xmlEscapeString(ratings.getIrText());
						int irScore = ratings.getIrScore();

						ret.getDisplayData().put("ImpactText_" + modCount + "_" + i, irText);
						ret.getDisplayData().put("ImpactScore_" + modCount + "_" + i, "" + irScore);
						
						log.trace("egData loop {}", i);

					} // end impact for loop

					String[] overallChoices = { "", "Very Weak", "Needs Development", "Satisfactory", "Strong",
							"Very Strong" };

					int overallScore = (egData.getOverallData().getOpr());

					String overallText = overallChoices[overallScore];
					String overallDescription = XMLUtils.xmlEscapeString(egData.getOverallData().getBriefDesc());
					String overallStrengths = XMLUtils.xmlEscapeString(egData.getOverallData().getKeyStrengths());
					String overallNeeds = XMLUtils.xmlEscapeString(egData.getOverallData().getKeyDevNeeds());

					String themesFromPeers = XMLUtils.xmlEscapeString(egData.getOverallData().getThemesFromPeers());
					String themesFromPeersLabel = "Themes From Peers (optional):";

					if (themesFromPeers == null) {
						themesFromPeers = "-1";
					}

					String themesFromDirectReports = XMLUtils
							.xmlEscapeString(egData.getOverallData().getThemesFromDirectReports());
					String themesFromDirectReportsLabel = "Themes From Direct Reports (optional):";

					if (themesFromDirectReports == null) {
						themesFromDirectReports = "-1";
					}

					String themesFromBoss = XMLUtils.xmlEscapeString(egData.getOverallData().getThemesFromBoss());
					String themesFromBossLabel = "Themes From Boss (optional):";

					if (themesFromBoss == null) {
						themesFromBoss = "-1";
					}

					String overallCoachNotes = XMLUtils.xmlEscapeString(egData.getOverallData().getCoachNotes());
					String overallCoachNotesLabel = "Real-time Coaching Notes (optional):";

					if (overallCoachNotes == null || overallCoachNotes == "") {
						overallCoachNotes = "-1";
					}

					ret.getDisplayData().put("OverallText_" + modCount, overallText);
					ret.getDisplayData().put("OverallDescription_" + modCount, overallDescription);
					ret.getDisplayData().put("OverallStrengths_" + modCount, overallStrengths);
					ret.getDisplayData().put("OverallNeeds_" + modCount, overallNeeds);
					ret.getDisplayData().put("ThemesFromPeers_", themesFromPeers);
					ret.getDisplayData().put("ThemesFromPeersLabel_", themesFromPeersLabel);
					ret.getDisplayData().put("ThemesFromDirectReports_", themesFromDirectReports);
					ret.getDisplayData().put("ThemesFromDirectReportsLabel_", themesFromDirectReportsLabel);
					ret.getDisplayData().put("ThemesFromBoss_", themesFromBoss);
					ret.getDisplayData().put("ThemesFromBossLabel_", themesFromBossLabel);
					ret.getDisplayData().put("OverallCoachNotes_" + modCount, overallCoachNotes);
					ret.getDisplayData().put("OverallCoachNotesLabel_" + modCount, overallCoachNotesLabel);

					modCount = modCount + 1;
					log.trace("modCount = {}", modCount);

				}

				int adjModCount = modCount - 1;
				log.trace("Adjusted module count: {}", adjModCount);
				ret.getDisplayData().put("ModCount", "" + adjModCount);
			}
			return ret;
		} catch (Exception e) {
			log.error("doAbyDIntGridReview: {}", e.getMessage());
			e.printStackTrace();
			return null;
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception se) {
			}
			con = null;
		}
	}

}
