package com.pdi.listener.portal.dao;

import java.sql.Connection;
//import java.util.ArrayList;
import java.util.HashMap;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.pdi.data.abyd.dto.reportInput.ReportInputDTO;
import com.pdi.data.abyd.dto.setup.EntryStateDTO;
import com.pdi.data.abyd.dto.setup.ReportModelStructureDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportLabelsDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportOptionsDTO;
import com.pdi.data.abyd.helpers.common.ImportExportDataHelper;
import com.pdi.data.abyd.helpers.intGrid.IGSummaryDataHelper;
import com.pdi.data.abyd.helpers.reportInput.ReportInputDataHelper;
import com.pdi.data.abyd.helpers.setup.EntryStateHelper;
import com.pdi.data.abyd.helpers.setup.ReportLabelsDataHelper;
import com.pdi.data.abyd.helpers.setup.ReportModelDataHelper;
import com.pdi.data.abyd.helpers.setup.ReportOptionsDataHelper;
import com.pdi.data.abyd.helpers.setup.SetupConstants;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.data.dto.Participant;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.v2.dto.ProjectDTO;
import com.pdi.data.v2.dto.SessionUserDTO;
import com.pdi.data.v2.helpers.ParticipantDataHelper;
import com.pdi.data.v2.helpers.ProjectDataHelper;
import com.pdi.data.v2.util.V2WebserviceUtils;
import com.pdi.properties.PropertyLoader;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.xml.XMLUtils;

public class DataAccessLayerV2 {

	private  SessionUserDTO sessionUser;

	public class ParticipantInformation {

		private ReportModelStructureDTO reportModelStructure;
		private ReportInputDTO reportInput;
		private IndividualReport individualReport;
		private Participant participant;
		private String participantId;
		private String projectId;
		private String submittedDate;
		private long dnaId;


		public void setReportModelStructure(ReportModelStructureDTO reportModelStructure) {
			this.reportModelStructure = reportModelStructure;
		}
		public ReportModelStructureDTO getReportModelStructure() {
			return reportModelStructure;
		}
		public void setReportInput(ReportInputDTO reportInput) {
			this.reportInput = reportInput;
		}
		public ReportInputDTO getReportInput() {
			return reportInput;
		}
		public void setIndividualReport(IndividualReport individualReport) {
			this.individualReport = individualReport;
		}
		public IndividualReport getIndividualReport() {
			IndividualReport ir = new IndividualReport();
			if(individualReport.getDisplayData() != null) {
				ir.setDisplayData(individualReport.getDisplayData());
			}
			if(individualReport.getReportData() != null) {
				ir.setReportData(individualReport.getReportData());
			}
			if(individualReport.getScriptedData() != null) {
				ir.setScriptedData(individualReport.getScriptedData());
			}
			if(individualReport.getScriptedGroupData() != null) {
				ir.setScriptedGroupData(individualReport.getScriptedGroupData());
			}
			if(individualReport.getConfigurationData() != null) {
				ir.setConfigurationData(individualReport.getConfigurationData());
			}

			return ir;
		}
		public void setParticipant(Participant participant) {
			this.participant = participant;
		}
		public Participant getParticipant() {
			return participant;
		}
		public void setParticipantId(String participantId) {
			this.participantId = participantId;
		}
		public String getParticipantId() {
			return participantId;
		}
		public void setProjectId(String projectId) {
			this.projectId = projectId;
		}
		public String getProjectId() {
			return projectId;
		}
		public void setDnaId(long dnaId) {
			this.dnaId = dnaId;
		}
		public long getDnaId() {
			return dnaId;
		}
		public void setSubmittedDate(String submittedDate) {
			this.submittedDate = submittedDate;
		}
		public String getSubmittedDate() {
			return submittedDate;
		}
	}

	public class ProjectInformation {
		private Setup2ReportOptionsDTO reportOptions;
		private Setup2ReportLabelsDTO reportLabels;
		private ProjectDTO project;
		private EntryStateDTO entryState;
		private String projectId;
		private String dnaName;
		private long dnaId;
	////	private SPFullDataDTO spFullData;
		private ReportModelStructureDTO reportModelStructure;
		
	////	public void setSpFullData(SPFullDataDTO spFullData) {
	////		this.spFullData = spFullData;
	////	}
	////	public SPFullDataDTO getSpFullData() {
	////		return spFullData;
	////	}
		public void setReportOptions(Setup2ReportOptionsDTO reportOptions) {
			this.reportOptions = reportOptions;
		}
		public Setup2ReportOptionsDTO getReportOptions() {
			return reportOptions;
		}
		public void setReportLabels(Setup2ReportLabelsDTO reportLabels) {
			this.reportLabels = reportLabels;
		}
		public Setup2ReportLabelsDTO getReportLabels() {
			return reportLabels;
		}
		public void setProject(ProjectDTO project) {
			this.project = project;
		}
		public ProjectDTO getProject() {
			return project;
		}
		public void setEntryState(EntryStateDTO entryState) {
			this.entryState = entryState;
		}
		public EntryStateDTO getEntryState() {
			return entryState;
		}
		public void setProjectId(String projectId) {
			this.projectId = projectId;
		}
		public String getProjectId() {
			return projectId;
		}
		public void setDnaId(long dnaId) {
			this.dnaId = dnaId;
		}
		public long getDnaId() {
			return dnaId;
		}
		public void setReportModelStructure(ReportModelStructureDTO reportModelStructure) {
			this.reportModelStructure = reportModelStructure;
		}
		public ReportModelStructureDTO getReportModelStructure() {
			return reportModelStructure;
		}
		public void setDnaName(String dnaName) {
			this.dnaName = dnaName;
		}
		public String getDnaName() {
			return dnaName;
		}

	}

	public HashMap<String, ParticipantInformation> participantInformationList;
	public HashMap<String, ProjectInformation> projectInformationHash;
	
	/*
	 *  We need to know if we are going to require a 
	 *  complete report (i.e., all instruments must be completed in order for the report to run)
	 *  or a partial report (i.e., missing items will be marked with red x on report)
	 *  true == partial report
	 *  false == complete report
	 */
	public boolean partialReportData;
	
	public void loadProject(String projectId) {

	}
	public ParticipantInformation loadParticipant(String projectId, String participantId) throws Exception {	
		Connection con = null;
		con = AbyDDatabaseUtils.getDBConnection();
		ParticipantInformation pi = new ParticipantInformation();
		try {

			long dnaId = 0;
			dnaId = new ImportExportDataHelper().getDnaFromV2(con, projectId);
			pi.setDnaId(dnaId);
			pi.setParticipantId(participantId);
			pi.setProjectId(projectId);
			if(pi.getDnaId() > 0) {

				ReportModelDataHelper reportModelDataHelper = new ReportModelDataHelper();
				ReportInputDataHelper reportInputDataHelper = new ReportInputDataHelper();
				ReportModelStructureDTO reportModelStructure = reportModelDataHelper.getReportModelForceScores(con, pi.getDnaId(), pi.getParticipantId());
				ReportInputDTO reportInput = reportInputDataHelper.getReportInput(pi.getParticipantId(), pi.getDnaId());
				IndividualReport ir = reportInput.getCurrentTLT();
				ir = new ParticipantDataHelper().populateIndividualReportData(sessionUser, pi.getParticipantId(), pi.getProjectId(), ir);

				Participant participantDto = HelperDelegate.getParticipantHelper().fromId(HelperDelegate.getSessionUserHelperHelper().createSessionUser(), pi.getParticipantId());

				pi.setIndividualReport(ir);
				pi.setSubmittedDate(new IGSummaryDataHelper(con).getIGSubmittedDate(pi.getDnaId(), pi.getParticipantId()));
				pi.setParticipant(participantDto);
				pi.setReportInput(reportInput);
				pi.setReportModelStructure(reportModelStructure);

			} else {
				IndividualReport ir = new ParticipantDataHelper().getIndividualReportData(sessionUser, pi.getParticipantId(), pi.getProjectId());
				pi.setIndividualReport(ir);
			}
			participantInformationList.put(projectId + "-" + participantId, pi);
		}catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)
				{
				}
				con = null;
			}
		}
		return pi;
	}
	public DataAccessLayerV2(Document doc) throws Exception {
		participantInformationList = new HashMap<String,ParticipantInformation>();
		projectInformationHash = new HashMap<String, ProjectInformation>();
		
		// Placed for migration... the langId variable is required for the label
		// fetch but didn't exist when this code is written.  I define it here so the
		// getTranslatedReportLabelData can compile with no errors.  It is hard-coded
		// to 1 (US English) for now.  When this code is adapted (or adopted) for
		// migration a real value should exist for the requested language ID
		long langId = 1;

		String username = PropertyLoader.getProperty("com.pdi.listener.portal.application", "v2admin.username");
		String password = PropertyLoader.getProperty("com.pdi.listener.portal.application", "v2admin.password");
		V2WebserviceUtils v2web = new V2WebserviceUtils();
		setSessionUser(v2web.registerUser(username, password));

		/*
		 *  The point of this:  we need to know if we are going to require a 
		 *  complete report (i.e., all instruments must be completed in order for the report to run)
		 *  or a partial report (i.e., missing items will be marked with red x on report)
		 *  
		 *  As of the time this is coded (01-10-2011) the TLT reports, 
		 *  TLT individual & TLT group detail reports,  need to have ALL data to
		 *  be run.   The ABYD TRANSITIONS report can be run as a PARTIAL report.
		 *  
		 */
		partialReportData = false; // set this true by default for now.
		NodeList reports = XMLUtils.getElements(doc, "//Reports/ReportRequest");
		for( int j= 0; j < reports.getLength(); j++)
		{
			Node report = reports.item(j);
			String code = report.getAttributes().getNamedItem("code").getTextContent();
			if(code.equals("TRANSITION_SCALES")  
					|| code.equals("ABYD_DEVELOPMENT_REPORT") 
					|| code.equals("ABYD_FIT_REPORT") 
					|| code.equals("ABYD_READINESS_REPORT") ) {			 
				partialReportData = true;
				//System.out.println("dataAccessLayerv2(doc)...partialReportData: (true) " +  partialReportData);
			}else {
				partialReportData = false;
				//System.out.println("dataAccessLayerv2(doc)...partialReportData: (false) " +  partialReportData);
			}
			
		}		
		NodeList participants = XMLUtils.getElements(doc, "//Participants/Participant");
		Connection con = null;
		con = AbyDDatabaseUtils.getDBConnection();
		try {
		for( int j= 0; j < participants.getLength(); j++)
		{


					Node participant = participants.item(j);

					String projectId = "";
					long dnaId = 0;
					if(participant.getAttributes().getNamedItem("projectId") != null)
					{
						projectId = participant.getAttributes().getNamedItem("projectId").getTextContent();
						dnaId = new ImportExportDataHelper().getDnaFromV2(con, projectId);
					} else {
						dnaId = Long.parseLong(participant.getAttributes().getNamedItem("dnaId").getTextContent());
					}
					

					if(participant.getAttributes().getNamedItem("participantId") != null && !participant.getAttributes().getNamedItem("participantId").getTextContent().equalsIgnoreCase("")) {
						String participantId = participant.getAttributes().getNamedItem("participantId").getTextContent();

						ParticipantInformation participantInfo = new ParticipantInformation();
						participantInfo.setParticipantId(participantId);
						participantInfo.setDnaId(dnaId);
						participantInfo.setProjectId(projectId);
						participantInformationList.put(projectId + "-" + participantId, participantInfo);

					}


					ProjectInformation projectInformation = new ProjectInformation();
					projectInformation.setProjectId(projectId);
					projectInformation.setDnaId(dnaId);
					projectInformationHash.put(projectId, projectInformation);

		}

		for(String code : projectInformationHash.keySet()) {
			ProjectInformation pi = projectInformationHash.get(code);

			ProjectDataHelper pdh = new ProjectDataHelper();
			ProjectDTO pdto = pdh.getProject(sessionUser, pi.getProjectId());

			if(pi.getDnaId() > 0) {
				ReportModelDataHelper reportModelDataHelper = new ReportModelDataHelper();
				ReportOptionsDataHelper reportOptionsDataHelper = new ReportOptionsDataHelper();
				ReportLabelsDataHelper reportLabelsDataHelper = new ReportLabelsDataHelper();

			////	SPDataHelper helper = new SPDataHelper(con, pi.getDnaId());
			////	SPFullDataDTO spFullData = helper.getAllSPData();
				EntryStateHelper entryStateHelper = new EntryStateHelper(con, pi.getProjectId());

				Setup2ReportOptionsDTO options = reportOptionsDataHelper.getReportOptionData(con, pi.getDnaId());
				Setup2ReportLabelsDTO labels = reportLabelsDataHelper.getTranslatedReportLabelData(pi.getDnaId(), langId, SetupConstants.ALL_LABELS);

				ReportModelStructureDTO reportModelStructure = reportModelDataHelper.getReportModel(con, pi.getDnaId());
				EntryStateDTO entryState = entryStateHelper.getEntryStateData();
			////	pi.setSpFullData(spFullData);
				pi.setEntryState(entryState);
				pi.setReportLabels(labels);
				pi.setReportOptions(options);
				pi.setReportModelStructure(reportModelStructure);
				String dnaName = new ImportExportDataHelper().getDnaNameFromV2(con, pi.getProjectId());
				pi.setDnaName(XMLUtils.xmlEscapeString(dnaName));	// "escape" the "&"
			}

			pi.setProject(pdto);

		}

		for(String code : participantInformationList.keySet()) {
			if(code.equalsIgnoreCase("")) {
				continue;
			}
			ParticipantInformation pi = participantInformationList.get(code);

			if(pi.getDnaId() > 0) {
				ReportModelDataHelper reportModelDataHelper = new ReportModelDataHelper();
				ReportInputDataHelper reportInputDataHelper = new ReportInputDataHelper();
				ReportModelStructureDTO reportModelStructure = reportModelDataHelper.getReportModelForceScores(con, pi.getDnaId(), pi.getParticipantId());
				
				//if(partialReportData) {
				//	reportInputDataHelper.setUsePartialData(true);
				//	//System.out.println("dataAccessLayerv2(doc).participantInformationList..partialReportData: " +  partialReportData);
				//}else {
				//	reportInputDataHelper.setUsePartialData(false);
				//	//System.out.println("dataAccessLayerv2(doc).participantInformationList..partialReportData: " +  partialReportData);
				//}

				ReportInputDTO reportInput = reportInputDataHelper.getReportInput(pi.getParticipantId(), pi.getDnaId());
				IndividualReport ir = reportInput.getCurrentTLT();

				//ir = new ParticipantDataHelper().populateIndividualReportData(sessionUser, pi.getParticipantId(), pi.getProjectId(), ir);
				Participant participantDto = HelperDelegate.getParticipantHelper().fromId(HelperDelegate.getSessionUserHelperHelper().createSessionUser(), pi.getParticipantId());
				// "escape" embedded "&"
				reportInput.setDnaName(XMLUtils.xmlEscapeString(reportInput.getDnaName()));
				reportInput.setClientName(XMLUtils.xmlEscapeString(reportInput.getClientName()));
				reportInput.setProjectId(XMLUtils.xmlEscapeString(reportInput.getProjectName()));
				participantDto.setFirstName(XMLUtils.xmlEscapeString(participantDto.getFirstName()));
				participantDto.setLastName(XMLUtils.xmlEscapeString(participantDto.getLastName()));
				// put out the data
				pi.setIndividualReport(ir);
				pi.setParticipant(participantDto);
				pi.setReportInput(reportInput);
				pi.setSubmittedDate(new IGSummaryDataHelper(con).getIGSubmittedDate(pi.getDnaId(), pi.getParticipantId()));
				pi.setReportModelStructure(reportModelStructure);
			} else {

				Participant participantDto = HelperDelegate.getParticipantHelper().fromId(HelperDelegate.getSessionUserHelperHelper().createSessionUser(), pi.getParticipantId());

				// "escape" embedded "&"
				participantDto.setFirstName(XMLUtils.xmlEscapeString(participantDto.getFirstName()));
				participantDto.setLastName(XMLUtils.xmlEscapeString(participantDto.getLastName()));
				IndividualReport ir = new ParticipantDataHelper().getIndividualReportData(sessionUser, pi.getParticipantId(), pi.getProjectId());
				pi.setParticipant(participantDto);
				/*
				 * set IR based on partial report boolean  
				 * -- as of 01-11-2011, the default here is that all TLT reports
				 * require COMPLETE data.
				 * 
				 */
				if(partialReportData) {
					ir.getScriptedData().put(ReportingConstants.PART_COMPL_RPT, ReportingConstants.PARTIAL_REPORT);
				}else {
					ir.getScriptedData().put(ReportingConstants.PART_COMPL_RPT, ReportingConstants.COMPLETE_REPORT);
				}

				pi.setIndividualReport(ir);
			}


		}


		}catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)
				{
				}
				con = null;
			}
		}


	}

	public void setSessionUser(SessionUserDTO sessionUser) {
		this.sessionUser = sessionUser;
	}

	public SessionUserDTO getSessionUser() {
		return sessionUser;
	}

}
