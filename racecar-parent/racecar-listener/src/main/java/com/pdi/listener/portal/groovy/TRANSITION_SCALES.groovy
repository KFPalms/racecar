/*
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.listener.portal.groovy 

import com.pdi.data.abyd.dto.common.KeyValuePair;


import com.pdi.data.abyd.dto.setup.ReportModelCompetencyDTO;
import com.pdi.data.abyd.dto.setup.ReportModelSuperFactorDTO;

import java.text.SimpleDateFormat;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.GroupReport;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;
import com.pdi.reporting.request.ReportingRequest;
import com.pdi.reporting.response.ReportingResponse;
import com.pdi.data.abyd.dto.reportInput.ReportInputDTO;
import com.pdi.data.abyd.dto.setup.EntryStateDTO;
import com.pdi.data.abyd.dto.setup.ReportModelStructureDTO;
import com.pdi.data.abyd.dto.setup.SPFullDataDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportLabelValueDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportLabelsDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportOptionValueDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportOptionsDTO;
import com.pdi.data.abyd.helpers.common.ImportExportDataHelper;
import com.pdi.data.abyd.helpers.reportInput.ReportInputDataHelper;
import com.pdi.data.abyd.helpers.setup.ReportLabelsDataHelper;
import com.pdi.data.abyd.helpers.setup.ReportModelDataHelper;
import com.pdi.data.abyd.helpers.setup.ReportOptionsDataHelper;
import com.pdi.data.abyd.helpers.setup.SPDataHelper;
import com.pdi.data.v2.dto.ParticipantDTO;
import com.pdi.data.v2.dto.ProjectDTO;
import com.pdi.data.v2.dto.SessionUserDTO;
import com.pdi.data.v2.helpers.ParticipantDataHelper;
import com.pdi.listener.portal.dao.DataAccessLayerV2;
import com.pdi.listener.portal.dao.DataAccessLayerV2.ParticipantInformation;
import com.pdi.listener.portal.dao.DataAccessLayerV2.ProjectInformation;
import com.pdi.properties.PropertyLoader;
import com.pdi.scoring.Norm;
import com.pdi.scoring.NormSet;
import com.pdi.string.StringUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.xml.XMLUtils;

/**
 * 
 */
public class TRANSITION_SCALES
{ 
	private ReportingRequest reportRequest;
	private Node reportNode;
	private SessionUserDTO sessionUser;
	private DataAccessLayerV2 dataAccessLayer;

	/*
	 * generate
	 */
	public void generate() {
		String reportCode = reportNode.getAttributes().getNamedItem("code").getTextContent();	
		NodeList participants = XMLUtils.getElements(reportNode, "Participants/Participant");
		
		GroupReport gr = new GroupReport();
		gr.setReportType(ReportingConstants.REPORT_TYPE_GROUP);
		gr.setReportCode(ReportingConstants.REPORT_CODE_TLT_GROUP_DETAIL);
		
		for( int j= 0; j < participants.getLength(); j++)
		{
			Node participant = participants.item(j);
			String participantId = participant.getAttributes().getNamedItem("participantId").getTextContent();
			String projectId = participant.getAttributes().getNamedItem("projectId").getTextContent();
			if(participantId == null || participantId.equalsIgnoreCase("")) {
				participantId = "";
			}
			
			try
			{
				//Got all of the basic data, time to add abyd data
				ProjectInformation projectInformation = dataAccessLayer.projectInformationHash.get(projectId);
				ParticipantInformation participantInformation = null;
				for(String code : dataAccessLayer.participantInformationList.keySet()) {
					ParticipantInformation pi =  dataAccessLayer.participantInformationList.get(code);
					if(pi.getParticipantId().equalsIgnoreCase(participantId) && pi.getProjectId().equalsIgnoreCase(projectId)) {
						participantInformation = pi;
						break;
					}
				}
				
				Setup2ReportOptionsDTO options = projectInformation.getReportOptions();
				Setup2ReportLabelsDTO labels = projectInformation.getReportLabels();
				SPFullDataDTO spFullData = projectInformation.getSpFullData();
				EntryStateDTO entryState = projectInformation.getEntryState();
				ReportModelStructureDTO reportModelStructure = projectInformation.getReportModelStructure();
				ProjectDTO projectDto = projectInformation.getProject();
				
				ReportInputDTO reportInput;
				ParticipantDTO participantDto;
				IndividualReport ir = new IndividualReport();
				if(participantInformation != null) {
					//println "part info != null";
					reportModelStructure = participantInformation.getReportModelStructure();
					reportInput = participantInformation.getReportInput();
					participantDto = participantInformation.getParticipant();
					ir = participantInformation.getIndividualReport();
					//println "ir " + (ir==null? "IS" : "is NOT") + " null";
					if (ir != null)
					{
						// Transition scales report is based off the IG (one person,
						// one project).  Any group level information in the
						// participant is valid for the GroupReport object too.
						String targetLevel = ir.getDisplayData().get("TARGET_LEVEL");
						//println "ir transition level=" + (targetLevel==null?"null":targetLevel);
						if(targetLevel == null || targetLevel.equals("")){
							gr.setTargetTransitionLevel("");
						}else{
							try{
								gr.setTargetTransitionLevel(Integer.parseInt(targetLevel));
							}catch ( Exception e ) {
								gr.setTargetTransitionLevel(targetLevel);
							}
							
						}
						
					}
				}
				
				//0 or 1 depending on circumstance
				gr.getDisplayData().put(ReportingConstants.RC_COGNITIVES_INCLUDED, "1");
				
				gr.getDisplayData().put("PARTICIPANT_NAME", reportInput.getParticipantName());
				gr.getDisplayData().put("CLIENT_NAME", reportInput.getClientName());
				gr.getDisplayData().put("PROJECT_NAME", reportInput.getDnaName());	// Use the DNA name as the project name on A by D data
				gr.setName(reportInput.getParticipantName() + "_" + reportCode + ".pdf");
				////println "reportInput: proj=" + reportInput.getProjectName() + ", dna=" + reportInput.getDnaName();
				////println "PROJECT_NAME=" + gr.getDisplayData().get("PROJECT_NAME");
				java.util.Date date = new java.util.Date();
				SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
				gr.getDisplayData().put("TODAY_DATE", sdf.format(date));
				
				ir.getDisplayData().put("CLIENT_LOGO", projectInformation.getProject().getClient().getId()+".png");
				ir.setReportCode(reportCode);
				ir.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);
				
				if(reportInput.getCurrentTLT() != null)
				{
					IndividualReport currentTLT = reportInput.getCurrentTLT();
					//System.out.println("currentTLT.getDisplayData().get(TARGET_LEVEL) " + currentTLT.getDisplayData().get("TARGET_LEVEL"));					
					if(currentTLT.getDisplayData().get("TARGET_LEVEL").equalsIgnoreCase("9"))
					{
						currentTLT.getDisplayData().put("MODEL", "Senior Executive");
						
					} else if(currentTLT.getDisplayData().get("TARGET_LEVEL").equalsIgnoreCase("8")) {
						currentTLT.getDisplayData().put("MODEL", "Executive (BUL)");
					} else if(currentTLT.getDisplayData().get("TARGET_LEVEL").equalsIgnoreCase("7")) {
						currentTLT.getDisplayData().put("MODEL", "Mid-Level Leader (MLL)");
					}
					//We don't know if the person is a Mid-Level Leader
					gr.getIndividualReports().add(currentTLT);
				}
				if(reportInput.getNextTLT() != null)
				{
					IndividualReport nextTLT = reportInput.getNextTLT();
					//System.out.println("nextTLT.getDisplayData().get(TARGET_LEVEL) " + nextTLT.getDisplayData().get("TARGET_LEVEL"));					
					if(nextTLT.getDisplayData().get("TARGET_LEVEL").equalsIgnoreCase("9"))
					{
						nextTLT.getDisplayData().put("MODEL", "Senior Executive");
					} else if(nextTLT.getDisplayData().get("TARGET_LEVEL").equalsIgnoreCase("8")) {
						nextTLT.getDisplayData().put("MODEL", "Executive (BUL)");
					} else if(nextTLT.getDisplayData().get("TARGET_LEVEL").equalsIgnoreCase("7")) {
						nextTLT.getDisplayData().put("MODEL", "Mid-Level Leader (MLL)");
					}
					gr.getIndividualReports().add(nextTLT);
					gr.getDisplayData().put("STATUS", "FINISHED");
					
					String fileName = StringUtils.removeSpecialCharacters(
							reportInput.getClientName() + " " +
							reportInput.getParticipantNameInverse() + " " + 
							new SimpleDateFormat("ddMMMyyyy").format(new java.util.Date()) + " ");
					
					gr.setName(fileName + reportCode + ".pdf");
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}

		}	// End of participants for loop
		reportRequest.addReport(gr);
	}
}