/*
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.listener.portal.groovy 

import com.pdi.data.abyd.dto.common.KeyValuePair;


import com.pdi.data.abyd.dto.setup.ReportModelCompetencyDTO;
import com.pdi.data.abyd.dto.setup.ReportModelSuperFactorDTO;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.GroupReport;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;
import com.pdi.reporting.request.ReportingRequest;
import com.pdi.reporting.response.ReportingResponse;
import com.pdi.data.abyd.dto.reportInput.ReportInputDTO;
import com.pdi.data.abyd.dto.setup.EntryStateDTO;
import com.pdi.data.abyd.dto.setup.ReportModelStructureDTO;
import com.pdi.data.abyd.dto.setup.SPFullDataDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportLabelValueDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportLabelsDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportOptionValueDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportOptionsDTO;
import com.pdi.data.abyd.helpers.common.ImportExportDataHelper;
import com.pdi.data.abyd.helpers.intGrid.IGSummaryDataHelper;
import com.pdi.data.abyd.helpers.reportInput.ReportInputDataHelper;
import com.pdi.data.abyd.helpers.setup.ReportLabelsDataHelper;
import com.pdi.data.abyd.helpers.setup.ReportModelDataHelper;
import com.pdi.data.abyd.helpers.setup.ReportOptionsDataHelper;
import com.pdi.data.abyd.helpers.setup.SPDataHelper;
import com.pdi.data.v2.dto.ParticipantDTO;
import com.pdi.data.v2.dto.ProjectDTO;
import com.pdi.data.v2.dto.SessionUserDTO;
import com.pdi.data.v2.helpers.ParticipantDataHelper;
import com.pdi.listener.portal.dao.DataAccessLayerV2.ParticipantInformation;
import com.pdi.listener.portal.dao.DataAccessLayerV2.ProjectInformation;
import com.pdi.properties.PropertyLoader;
import com.pdi.scoring.Norm;
import com.pdi.scoring.NormSet;
import com.pdi.string.StringUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.xml.XMLUtils;

public class ABYD_TRANSITION_SCALES
{ 
	
	private ReportingRequest reportRequest;
	private Node reportNode;
	private SessionUserDTO sessionUser;


	/*
	 * generate ---**** this script is not used... please use TRANSISTION_SCALES.groovy ********
	 */
	public void generate()
	{
		//System.out.println("ENTERING GENERATE() ---  ABYD_TRANSITION_SCALES   " );
		String reportCode = reportNode.getAttributes().getNamedItem("code").getTextContent();
		//System.out.println(" ABYD_TRANSITION_SCALES   " + reportCode);
		NodeList participants = XMLUtils.getElements(reportNode, "Participants/Participant");
		
		for( int j= 0; j < participants.getLength(); j++)
		{
			Node participant = participants.item(j);
			String participantId = participant.getAttributes().getNamedItem("participantId").getTextContent();
			String projectId = participant.getAttributes().getNamedItem("projectId").getTextContent();
			if(participantId == null || participantId.equalsIgnoreCase("")) {
				participantId = "";
			}
			
			try
			{
				//Got all of the basic data, time to add abyd data
				ProjectInformation projectInformation = dataAccessLayer.projectInformationHash.get(projectId);
				ParticipantInformation participantInformation = null;
				for(String code : dataAccessLayer.participantInformationList.keySet()) {
					ParticipantInformation pi =  dataAccessLayer.participantInformationList.get(code);
					if(pi.getParticipantId().equalsIgnoreCase(participantId) && pi.getProjectId().equalsIgnoreCase(projectId)) {
						participantInformation = pi;
						break;
					}
				}
				
				Setup2ReportOptionsDTO options = projectInformation.getReportOptions();
				Setup2ReportLabelsDTO labels = projectInformation.getReportLabels();
				SPFullDataDTO spFullData = projectInformation.getSpFullData();
				EntryStateDTO entryState = projectInformation.getEntryState();
				ReportModelStructureDTO reportModelStructure = projectInformation.getReportModelStructure();
				ProjectDTO projectDto = projectInformation.getProject();
				
				ReportInputDTO reportInput;
				ParticipantDTO participantDto;
				IndividualReport ir = new IndividualReport();
				if(participantInformation != null) {
					reportModelStructure = participantInformation.getReportModelStructure();
					reportInput = participantInformation.getReportInput();
					participantDto = participantInformation.getParticipant();
					ir = participantInformation.getIndividualReport();
				}			
				ir.getDisplayData().put("CLIENT_LOGO", projectInformation.getProject().getClient().getId()+".png");
				ir.setReportCode(reportCode);
				ir.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);
				
				// Override project name with DNA name -- Does this need to go into Group Report later?
				ir.getDisplayData().put("PROJECT_NAME", reportInput.getDnaName());	// Use the DNA name as the project name on A by D data
				////println "reportInput: proj=" + reportInput.getProjectName() + ", dna=" + reportInput.getDnaName();
				////println "PROJECT_NAME=" + ir.getDisplayData().get("PROJECT_NAME");
												
				int sp = 0;
				for(KeyValuePair kvp : spFullData.getTextElts())
				{
					ir.getDisplayData().put("SP_"+sp, kvp.getTheValue())
					sp++;
				}
				
				for(Setup2ReportOptionValueDTO value : options.getOptionValuesArray())
				{
					ir.getDisplayData().put("OPTION_" + value.getOptionCode(), value.getOptionValue());
				}
				
				for(Setup2ReportLabelValueDTO value : labels.getLabelValuesArray())
				{
					if(value != null)
					{
						String svalue = value.getCustomLabel();
						if(svalue.length() < 1)
						{
							svalue = value.getLabel();
						}
						ir.getDisplayData().put("LABEL_" + value.getReportCode(), svalue);
					}
				}
				ir.getDisplayData().put("MODEL_SUPERFACTORS_COUNT", reportModelStructure.getSuperFactorList().size() - 1);
				
				int i = 0;
				for(ReportModelSuperFactorDTO superfactor : reportModelStructure.getSuperFactorList())
				{
					ir.getDisplayData().put("MODEL_SUPERFACTOR_"+i+"_DISPLAY_NAME", superfactor.getDisplayName());
					ir.getDisplayData().put("MODEL_SUPERFACTOR_"+i+"_COMPETENCY_COUNT", superfactor.getCompList().size() - 1);
					int k = 0;
					for(ReportModelCompetencyDTO competency : superfactor.getCompList())
					{
						ir.getDisplayData().put("MODEL_SUPERFACTOR_"+i+"_COMPETENCY_"+k+"_DISPLAY_NAME", competency.getDisplayName());
						ir.getDisplayData().put("MODEL_SUPERFACTOR_"+i+"_COMPETENCY_"+k+"_VALUE", "4.5");
						k++;
					}
					i++;
				}
				
//				ir.getDisplayData().put("ADMIN_DATE", new IGSummaryDataHelper(con).getIGSubmittedDate(dnaId));
//				ir.getDisplayData().put("PROJECT", ir.getDisplayData("PROJECT_NAME"));
				
				//Display data
				ir.getDisplayData().put("STATUS", reportInput.getStatusId());
				
				ir.getDisplayData().put("READINESS_FOCUS", reportInput.getReadinessFocus());
				ir.getDisplayData().put("DERAIL_ORG_TXT",reportInput.getDerailmentOrgText());
				ir.getDisplayData().put("DERAIL_PRT_TXT",reportInput.getDerailmentPartText());
				ir.getDisplayData().put("DEV_PRT_TXT", reportInput.getDevelopmentPartText());
				ir.getDisplayData().put("FIT_ORG_TXT", reportInput.getFitOrgText());
				ir.getDisplayData().put("PIV_PRT_TXT", reportInput.getPivotalPartText());
				ir.getDisplayData().put("IDP_DOC", reportInput.getIdpDocumentPath());
				ir.getDisplayData().put("IDP_IMG", reportInput.getIdpImagePath());
				ir.getDisplayData().put("LEADER_EXP_ORG_TXT", reportInput.getLeadershipExperienceOrgText());
				ir.getDisplayData().put("LEADER_EXP_PRT_TXT", reportInput.getLeadershipExperiencePartText());
				ir.getDisplayData().put("LEADER_INT_ORG_TXT", reportInput.getLeadershipInterestOrgText());
				ir.getDisplayData().put("LEADER_INT_PRT_TXT", reportInput.getLeadershipInterestPartText());
				ir.getDisplayData().put("LEADER_SKL_ORG_TXT", reportInput.getLeadershipSkillOrgText());
				ir.getDisplayData().put("LEADER_SKL_PRT_TXT", reportInput.getLeadershipSkillPartText());
				ir.getDisplayData().put("LEADER_STY_ORG_TXT", reportInput.getLeadershipStyleOrgText());
				ir.getDisplayData().put("LEADER_STY_PRT_TXT", reportInput.getLeadershipStylePartText());
				ir.getDisplayData().put("LONGTERM_ORG_TXT", reportInput.getLongtermOrgText());
				if(reportInput.getIdpImagePath() != null && reportInput.getIdpImagePath().length() > 0)
				{
					ir.getDisplayData().put("IDP_IMAGE", "http://localhost:8080/pdi-web/upload/" +  reportInput.getIdpImagePath());
				} else {
					ir.getDisplayData().put("IDP_IMAGE", "");
				}

				//Scripted data
				//System.out.println("SETTING SCRIPTED DATA");
				ir.getScriptedData().put("CAN_FIT_IDX", reportInput.getCandidateFitIndex().toString());
				ir.getScriptedData().put("DERAIL_RATING", reportInput.getDerailmentRating().toString());
				ir.getScriptedData().put("FIT_RATING", reportInput.getFitRecommendedRating().toString());
				ir.getScriptedData().put("LEADER_EXP_RATING", reportInput.getLeadershipExperienceRating().toString());
				ir.getScriptedData().put("LEADER_INT_RATING", reportInput.getLeadershipInterestRating().toString());
				ir.getScriptedData().put("LEADER_SKL_RATING", reportInput.getLeadershipSkillRating().toString());
				ir.getScriptedData().put("LEADER_STY_RATING", reportInput.getLeadershipStyleRating().toString());
				
				//System.out.println("ir.getScriptedData().get(LEADER_STY_RATING)  " + ir.getScriptedData().get("LEADER_STY_RATING"));
				//reportInput.getCurrentTLT().getDisplayData().put("LAST_NAME", participantDto.getLastName());
				//reportInput.getNextTLT().getDisplayData().put("LAST_NAME", participantDto.getLastName());
				
				GroupReport gr = new GroupReport();
				gr.getScriptedData().put("REPORT_TYPE", "TRANSITION");
				
//				String name = participantDto.getFirstName() + " " + participantDto.getLastName();
//				gr.getDisplayData().put("PARTICIPANT_NAME", name  );
				
				gr.setReportType(ReportingConstants.REPORT_TYPE_GROUP);
				gr.setReportCode(ReportingConstants.REPORT_CODE_ABYD_TRANSITION_SCALES);
				gr.getIndividualReports().add(reportInput.getCurrentTLT());
				
				String fileName = StringUtils.removeSpecialCharacters(
						reportInput.getClientName() + " " +
						reportInput.getParticipantNameInverse() + " " + 
						new SimpleDateFormat("ddMMMyyyy").format(new java.util.Date()) + " ");
				
				gr.setName(fileName + reportCode + ".pdf");
				//System.out.println("The current TLT is " + reportInput.getCurrentTLT());
				if(reportInput.getNextTLT() != null)
				{
					gr.getIndividualReports().add(reportInput.getNextTLT());
					//System.out.println("The next TLT is " + reportInput.getNextTLT());
				}
				reportRequest.addReport(gr);
			
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
			}
		}
	}
}