/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.listener.portal.delegated.helpers.reports;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kf.palms.web.util.JSONUtils;
import com.pdi.data.abyd.dto.reportInput.ReportInputDTO;
import com.pdi.data.abyd.dto.setup.EntryStateDTO;
import com.pdi.data.abyd.dto.setup.ReportModelStructureDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportLabelValueDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportLabelsDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportOptionValueDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportOptionsDTO;
import com.pdi.data.abyd.helpers.common.HelperUtils;
import com.pdi.data.abyd.helpers.common.ImportExportDataHelper;
import com.pdi.data.abyd.helpers.intGrid.IGKf4dDataHelper;
import com.pdi.data.abyd.helpers.reportInput.ReportInputDataHelper;
import com.pdi.data.abyd.helpers.setup.EntryStateHelper;
import com.pdi.data.abyd.helpers.setup.ReportLabelsDataHelper;
import com.pdi.data.abyd.helpers.setup.ReportModelDataHelper;
import com.pdi.data.abyd.helpers.setup.ReportOptionsDataHelper;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.data.abyd.util.AlrExtractHelper;
import com.pdi.data.dto.NormScore;
import com.pdi.data.dto.ProjectParticipantInstrument;
import com.pdi.data.dto.Score;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.dto.Text;
import com.pdi.data.dto.TextGroup;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.ILanguageHelper;
import com.pdi.data.util.language.DefaultLanguageHelper;
import com.pdi.properties.PropertyLoader;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.GroupReport;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;
import com.pdi.reporting.report.helpers.AbyDExtractFormatHelper;
import com.pdi.reporting.request.ReportingRequest;
import com.pdi.string.StringUtils;
import com.pdi.xml.XMLUtils;

/*
 * AlrRExtract - class to hold methods relevant to the Alr extract.
 *
 * NOTE:  EVEN THOUGH LOCATED UNDER THE DELEGATED FOLDER, THERE ARE NO METHODS IN THIS
 *        CLASS THAT ARE DEFINED IN THE DATA INTERFACE.
 *
 * This class is located here because the folder structure (delegated/helpers) is
 * inverted from normal practice elsewhere in this project stack (helpers/delegated).
 * Putting this helper class here avoided confusion with having two folders in the
 * portal structure that had the name "helper".
 *
 * Yes, this is not a report but an extract.  Still, it is located in the report folder
 */
public class AlrExtract {
	
	private static final Logger log = LoggerFactory.getLogger(AlrExtract.class);

	//
	// Static data.
	//
	private static int EDITABLE = 1;

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private long languageId = 0;

	//
	// Constructors.
	//
	public AlrExtract() {
		// No logic here at the present time
	}

	//
	// Instance methods.
	//

	/**
	 * getAlrExtractData is a method that handles all AAlrExtract specific
	 * report functions - gathers and sets up all Alr specific Data.
	 *
	 * This is a new method, pulling together and porting older code from
	 * previously existing groovy scripts.
	 *
	 * @param ret
	 * @param epis
	 * @param overrides
	 * @param sessionUser
	 * @param tg
	 */
	public void getAlrExtractData(IndividualReport inp, HashMap<String, GroupReport> groupReports,
			ReportingRequest reportingRequest, ArrayList<ProjectParticipantInstrument> epis, SessionUser sessionUser,
			TextGroup tg, String langCode, boolean addBreaks) {
		log.debug("Called getAlrExtractData. with {} epis. Report Code: {}", epis.size(), inp.getReportCode());
		HashMap<String, String> lblMap = new HashMap<String, String>();
		if (addBreaks) {
			log.debug("Add breaks is true");
			// Loop through tg and do break enhancement
			// Done here so that both the GroupReport and IndividualReport
			// objects are covered
			for (Iterator<Map.Entry<String, Text>> itr = tg.getLabelMap().entrySet().iterator(); itr.hasNext();) {
				// get the next entry
				Map.Entry<String, Text> ent = itr.next();

				// get the data
				String key = ent.getKey();
				Text txt = ent.getValue();
				// massage it
				String out = StringUtils.formatNonSpacingTxt(txt.getText());
				// replace the string in the label value hashmap
				lblMap.put(key, out);
			}
		} else {
			lblMap = tg.getLabelHashMap();
		}

		// System.out.println("doAbyDSpecificReportTasks..." +
		// ret.getReportCode());
		Connection con = null;
		HashMap<Integer, String> cmpInternalNames = new HashMap<Integer, String>();

		try {
			
			// Get the language id (we have the code)
			ILanguageHelper lh = HelperDelegate
					.getLanguageHelper(PropertyLoader.getProperty("com.pdi.data.v2.application", "datasource.v2"));
			//// tg.getLabelMap().put("LANG_CODE", new Text(0, "LANG_CODE",
			//// langObj, langCode));
			// public Language fromCode(SessionUser su, String code)

			languageId = new Long(lh.fromCode(sessionUser, langCode).getId()).longValue();

			con = AbyDDatabaseUtils.getDBConnection();
			IndividualReport ret = inp.clone(); // get the input data

			String projectId = epis.get(0).getProject().getId();
			long dnaId = 0;
			GroupReport gr = new GroupReport(); // we will need this if an
												// extract
			ReportModelStructureDTO reportModelStructure = null;
			ReportInputDTO reportInput = null;
			HashMap<String, ReportData> reportDataMap = new HashMap<String, ReportData>();
			String rptCode = ret.getReportCode();

			// FIRST, IF THIS IS THE EXTRACT CREATE THE group report...
			// AND ADD IT TO THE GROUP REPORTS MAP....

			// System.out.println(" create group report... ");
			if (groupReports.get(rptCode) == null) {
				// First time entered, create a new group report...
				// (created above the if... )
				// ...and stick in the labels
				gr.getLabelData().putAll(lblMap);
				// only label added is the language code
				// add it to the groupReports map....
				groupReports.put(ret.getReportCode(), gr);

				// set the report data map so we can add
				// to it in the instrument loop
				ret.setReportData(reportDataMap);
			} else {
				gr = groupReports.get(ret.getReportCode());
				
			}
			// System.out.println("Got groupReport data... code=" +
			// ret.getReportCode());

			// Get the extract data helper
			AlrExtractHelper aeh = new AlrExtractHelper(con);

			// now start pulling stuff from the
			// com.pdi.listener.portal.groovy.ABYD_EXTRACT.groovy
			// TODO Rework this so that the data is in the individual report
			aeh.getGroupData(projectId, gr);
			// don't forget to set the dnaId variable:
			dnaId = new Long(gr.getDisplayData().get("DNA_ID")).longValue();
			// if (gr == null)
			if (gr.getDisplayData().get("DNA_ID") == null || dnaId == 0) {
				System.out.println("getAlrExtractData():  No DNA data for project " + projectId);
			} else {
				// go ahead and do all the group stuff....
				gr.setReportCode(rptCode);
				gr.setReportType(ReportingConstants.REPORT_TYPE_GROUP_EXTRACT);

				// Add the client competency structure to the GR
				aeh.addClientCompetencyStructureALR(gr, dnaId);

				// Get the Internal Comp Names for use lower down
				cmpInternalNames = (HashMap<Integer, String>) aeh.getCompInternalNames(dnaId);
			}

			// NOW DEAL WITH INDIVIDUAL REPORT DATA... TO BE ADDED TO GROUP
			// REPORT IF
			// WE'RE DEALING WITH AN EXTRACT.
			// ONE INDIVIDUAL REPORT PER PARTICIPANT, TO BE ADDED TO THE GROUP
			// REPORT.
			// String currentPid = "00000";
			int loopCounter = 0;

			String pptId = null;
			String projId = null;

			for (ProjectParticipantInstrument epi : epis) {
				log.debug("epi loop: {}.  Instrument: {}, PPt ID: {}", loopCounter, epi.getInstrument().getCode(), epi.getParticipant().getId());

				// boolean previewReport = false;

				String participantId = epi.getParticipant().getId();

				// currentPid = participantId;

				projectId = epi.getProject().getId();
				if (participantId == null || participantId.equalsIgnoreCase("")) {
					participantId = "";
					// previewReport = true;
				}

				// ONLY DO THESE THINGS THE FIRST TIME AROUND (FOR EXTRACT...
				// ONLY ONE TIME AROUND FOR ALL OTHER REPORTS)
				if (loopCounter == 0) {
					pptId = participantId;
					projId = projectId;

					// System.out.println(" loop counter 1 " + loopCounter);
					dnaId = new ImportExportDataHelper().getDnaFromV2(con, projectId);
					if (dnaId < 1) {
						throw new Exception("getAlrExtractData():  No DNA found for project " + projectId);
					}
					ReportModelDataHelper reportModelDataHelper = new ReportModelDataHelper();
					ReportInputDataHelper reportInputDataHelper = new ReportInputDataHelper();

					// String fakeProjId = "";
					// String fakePartId = "";
					// long fakeDnaId = 0;

					// TODO Need to force scores only if the ALP instrument is
					// present... fix this
					// TODO Determine if the report/client model will be
					// presented if multiple projects are to be supported
					reportModelStructure = reportModelDataHelper.fetchReportModelAndScores(con, dnaId, languageId,
							participantId, true);
					reportInput = reportInputDataHelper.getReportInput(participantId, dnaId, langCode);

					// add the scripted data from the reportInput curTLT, so
					// that we have TLT scores for the report.
					// ret.setScriptedData(reportInput.getCurrentTLT().getScriptedData());

					// System.out.println("Participant: name=" +
					// participant.getFirstName() + " " +
					// participant.getLastName()+ ". Biz Unit=" +
					// participant.getBusinessUnit());
					String firstName = epi.getParticipant().getFirstName();
					if (firstName == null) {
						firstName = "";
					}
					String lastName = epi.getParticipant().getLastName();
					if (lastName == null) {
						lastName = "";
					}
					String businessUnit = epi.getParticipant().getMetadata().get("OPTIONAL_1");
					if (businessUnit == null) {
						businessUnit = "";
					}
					// ret.setName(firstName + "_" + lastName + "_" +
					// businessUnit.replace("/", "_").replace("\\", "_") + "_" +
					// randomIndex + ".pdf");

					// Get Project data
					ret.addDisplayData("ORGANIZATION",
							XMLUtils.xmlEscapeString(epi.getProject().getClient().getName()));

					ret.addDisplayData("TRANSITION_LEVEL", "" + epi.getProject().getTargetLevelTypeId());

					// System.out.println("Transition level... ." +
					// ret.getDisplayData("TRANSITION_LEVEL"));
					// ret.addDisplayData("PROJECT_NAME",
					// epi.getProject().getName());

					ret.getDisplayData().put("TARGET_LEVEL_NAME", epi.getProject().getTargetLevelName());
					ret.getDisplayData().put("TARGET_LEVEL_CODE", "" + epi.getProject().getTargetLevelCode());
					ret.getDisplayData().put("TARGET_LEVEL_INDEX", "" + epi.getProject().getTargetLevelIndex());

					// Use the name as project name on A by D data
					ret.getDisplayData().put("PROJECT_NAME", XMLUtils.xmlEscapeString(reportInput.getDnaName()));
					ret.getDisplayData().put("CLIENT_NAME", XMLUtils.xmlEscapeString(reportInput.getClientName()));
					ret.getDisplayData().put("MODEL_NAME", XMLUtils.xmlEscapeString(reportInput.getModelName()));

					/*
					 *  Set up labels/option data/entry state
					 *  --------------------------------------
					 */

					ReportOptionsDataHelper optionsDH = new ReportOptionsDataHelper();
					ReportLabelsDataHelper labelsDH = new ReportLabelsDataHelper();
					EntryStateHelper entryH = new EntryStateHelper(con, projectId);

					Setup2ReportOptionsDTO options = null;
					Setup2ReportLabelsDTO labels = null;

					options = optionsDH.getReportOptionData(con, dnaId);
					labels = labelsDH.getTranslatedReportLabelData(dnaId, tg.getLanguageCode(), EDITABLE);

					EntryStateDTO entryState = entryH.getEntryStateData();

					ret.getDisplayData().put("CLIENT_LOGO", epi.getProject().getClient().getId() + ".png");

					ret.getDisplayData().put("DNA_NAME",
							XMLUtils.xmlEscapeString(entryState.getDnaDesc().getDnaName()));

					/***
					 * SET THE FILE NAME the new requirments:
					 * http://segjira:8090/browse/NHN-2269 Aug 15 2012
					 *
					 * Participant Last Name_+ Participant First Name_ + TLT or
					 * ABYD_+ Target Level_ + Selected_Report_ +
					 * Language_Report_ + Generation Date.pdf+
					 */
					// Target LEVEL
					String trgLevel = " ";
					IndividualReport nextTLT = reportInput.getCurrentTLT();
					if (nextTLT.getDisplayData().get("TARGET_LEVEL").equalsIgnoreCase("10")) {
						trgLevel = "SEA";

					} else if (nextTLT.getDisplayData().get("TARGET_LEVEL").equalsIgnoreCase("9")) {
						trgLevel = "CEO";

					} else if (nextTLT.getDisplayData().get("TARGET_LEVEL").equalsIgnoreCase("8")) {
						trgLevel = "BUL";

					} else if (nextTLT.getDisplayData().get("TARGET_LEVEL").equalsIgnoreCase("7")) {
						trgLevel = "MLL";
					}
					String fileName = StringUtils.removeFilenameSpecialCharacters(
							// reportInput.getClientName() + " " +
							// reportInput.getProjectName() + " " +
							reportInput.getParticipantNameInverse() + " " + epi.getProject().getProjectTypeName() + " "
									+ trgLevel + " " + ret.getReportCode() + " " + // rptCode+
																					// "
																					// "+
									new SimpleDateFormat("yyyyMMdd").format(new Date()) + " " + langCode);

					ret.setName(fileName.replace(" ", "_") + ".pdf");

					ret.getDisplayData().put("TargetLevel", trgLevel);

					// Hardcode Cogs Included to 1
					ret.getDisplayData().put(ReportingConstants.RC_COGNITIVES_INCLUDED, "1");

					// ADD OPTION DATA TO REPORT
					for (Setup2ReportOptionValueDTO value : options.getOptionValuesArray()) {
						if (value.getOptionString().equals("")) {
							ret.getDisplayData().put("OPTION_" + value.getOptionCode(),
									(value.getOptionValue() ? "true" : null));
						} else {
							// String str = value.getOptionString();
							ret.getDisplayData().put("OPTION_" + value.getOptionCode(), (value.getOptionString()));
						}
					}

					// ADD LABEL DATA TO REPORT
					for (Setup2ReportLabelValueDTO value : labels.getLabelValuesArray()) {
						if (value != null) {
							String cvalue = value.getCustomLabel();
							String svalue = value.getLabel();
							if ((cvalue.length() > 0)
									&& (this.languageId == DefaultLanguageHelper.fetchDefaultLanguageId())) // only
																												// do
																												// if
																												// en
																												// language(1)
							{
								svalue = value.getLabel();
								svalue = cvalue;
							}
							// System.out.println("value.getReportCode(): " +
							// value.getReportCode() + " svalue: " + svalue);

							ret.getDisplayData().put("LABEL_" + value.getReportCode(), svalue);
						}
					}

					if (labels.getCoverPage().getCustomLabel() != null
							&& labels.getCoverPage().getCustomLabel().length() > 0) {
						ret.getDisplayData().put("LABEL_COV_PAGE", labels.getCoverPage().getCustomLabel());
						// ret.getDisplayData().put("LABEL_COV_PAGE",
						// UnitTestUtils.unicodeText());
					} else {
						ret.getDisplayData().put("LABEL_COV_PAGE", labels.getCoverPage().getLabel());
						// ret.getDisplayData().put("LABEL_COV_PAGE",
						// UnitTestUtils.unicodeText());
					}

					if (labels.getLeadershipLegend().getCustomLabel() != null
							&& labels.getLeadershipLegend().getCustomLabel().length() > 0) {
						ret.getDisplayData().put("LABEL_LEG_LEAD", labels.getLeadershipLegend().getCustomLabel());
					} else {
						ret.getDisplayData().put("LABEL_LEG_LEAD", labels.getLeadershipLegend().getLabel());
					}

					if (labels.getTltLegend().getCustomLabel() != null
							&& labels.getTltLegend().getCustomLabel().length() > 0) {
						ret.getDisplayData().put("LABEL_LEG_TLT", labels.getTltLegend().getCustomLabel());
					} else {
						ret.getDisplayData().put("LABEL_LEG_TLT", labels.getTltLegend().getLabel());
					}

					/*
					 * REPORT INPUT DATA
					 */

					Integer intStatus = 0;
					intStatus = (int) reportInput.getCompositeStatusId();

					if (intStatus != null) // then there should be some data to
											// retrieve.
					{

						String[] DRIStatus = { "", "In Progress", "Ready for Editing", "Final" };
						ret.getDisplayData().put("DRIStatus", DRIStatus[intStatus]);

						// Readiness
						ret.getDisplayData().put("ReportedDevelopmentFocus", reportInput.getAlrReadinessFocus());
						Integer intRedi = 0;
						intRedi = (int) reportInput.getAlrReadinessRating();
						ret.getDisplayData().put("ReportedReadinessNumeric", intRedi.toString());
						String[] ReadinessRating = { "Insufficient Data", "Low", "Mixed", "Strong", "Very Strong" };
						ret.getDisplayData().put("ReportedReadinessText", ReadinessRating[intRedi]);
						ret.getDisplayData().put("ReportedReadinessOrganizationText",
								XMLUtils.suppressHtmlTags(reportInput.getAlrReadinessOrgText()));
						intRedi = 0;
						intRedi = (int) reportInput.getAlrSugReadinessRating();
						ret.getDisplayData().put("SuggestedReadinessNumeric", intRedi.toString());
						ret.getDisplayData().put("SuggestedReadinessText", ReadinessRating[intRedi]);
						ret.getDisplayData().put("ReadinessORAuth", reportInput.getAlrReadinessORAuth());
						ret.getDisplayData().put("ReadinessORJust", reportInput.getAlrReadinessORJust());

						// Development summary
						ret.getDisplayData().put("DevelopmentParticipantText",
								XMLUtils.suppressHtmlTags(reportInput.getDevelopmentPartText()));

						// Long Term Advancement Potential
						Integer intLongTermRating = 0;
						intLongTermRating = (int) reportInput.getLongtermRating();
						ret.getDisplayData().put("ReportedAdvancementNumeric", intLongTermRating.toString());

						ret.getDisplayData().put("ReportedAdvancementText", ReadinessRating[intLongTermRating]);

						// Cand fit index
						ret.getDisplayData().put("CalculatedFitIndex",
								new Integer(reportInput.getCandidateFitIndex()).toString());

						// Culture Fit
						Integer intCfit = 0;
						intCfit = (int) reportInput.getAlrCultureFitRating();
						ret.getDisplayData().put("ReportedCultureNumeric", intCfit.toString());
						String[] CultureFit = { "", "Low", "Mixed", "Strong", "Very Strong" };
						ret.getDisplayData().put("ReportedCultureText", CultureFit[intCfit]);
						ret.getDisplayData().put("ReportedCultureOrganizationText",
								XMLUtils.suppressHtmlTags(reportInput.getAlrCultureFitOrgText()));
						ret.getDisplayData().put("ReportedCultureParticipantText",
								XMLUtils.suppressHtmlTags(reportInput.getAlrCultureFitPartText()));
						intCfit = 0;
						intCfit = (int) reportInput.getAlrSugCultureFitRating();
						ret.getDisplayData().put("SuggestedCultureNumeric", intCfit.toString());
						ret.getDisplayData().put("SuggestedCultureText", CultureFit[intCfit]);
						ret.getDisplayData().put("CultureFitORAuth", reportInput.getAlrCultureFitORAuth());
						ret.getDisplayData().put("CultureFitORJust", reportInput.getAlrCultureFitORJust());

						// Reported Leadership Competencies
						Integer intLeadershipSkillRating = 0;
						intLeadershipSkillRating = (int) reportInput.getLeadershipSkillRating();
						ret.getDisplayData().put("ReportedCompetenciesNumeric", intLeadershipSkillRating.toString());
						String[] Numeric2Text = { "", "Weak", "Mixed", "Strong", "Very Strong" };
						ret.getDisplayData().put("ReportedCompetenciesText", Numeric2Text[intLeadershipSkillRating]);
						ret.getDisplayData().put("ReportedCompetenciesNeedsOrganizationText",
								XMLUtils.suppressHtmlTags(reportInput.getSkillsToDevelopOrgText()));
						ret.getDisplayData().put("ReportedCompetenciesNeedsParticipantText",
								XMLUtils.suppressHtmlTags(reportInput.getSkillsToDevelopPartText()));

						// Leadership Experience
						Integer intLeadershipExprienceRating = 0;
						intLeadershipExprienceRating = (int) reportInput.getLeadershipExperienceRating();
						ret.getDisplayData().put("ReportedExperienceNumeric", intLeadershipExprienceRating.toString());
						ret.getDisplayData().put("ReportedExperienceText", Numeric2Text[intLeadershipExprienceRating]);

						// Leadership Traits
						Integer intLeadershipStyleRating = 0;
						intLeadershipStyleRating = (int) reportInput.getLeadershipStyleRating();
						ret.getDisplayData().put("ReportedTraitsNumeric", intLeadershipStyleRating.toString());
						ret.getDisplayData().put("ReportedTraitsText", Numeric2Text[intLeadershipStyleRating]);

						// Leadership Drivers
						Integer intLeadershipInterestRating = 0;
						intLeadershipInterestRating = (int) reportInput.getLeadershipInterestRating();
						ret.getDisplayData().put("ReportedDriversNumeric", intLeadershipInterestRating.toString());
						ret.getDisplayData().put("ReportedDriversText", Numeric2Text[intLeadershipInterestRating]);

						// Derailment Risk
						Integer intDerailmentRating = 0;
						intDerailmentRating = (int) reportInput.getDerailmentRating();
						ret.getDisplayData().put("ReportedDerailmentNumeric", intDerailmentRating.toString());
						String[] DerailmentText = { "", "High", "Moderate", "Low", "Minimal" };
						ret.getDisplayData().put("ReportedDerailmentText", DerailmentText[intDerailmentRating]);

						ret.getDisplayData().put("IDP_DOC", reportInput.getIdpDocumentPath());
						ret.getDisplayData().put("IDP_IMG", reportInput.getIdpImagePath());

						if (reportInput.getIdpImagePath() != null && reportInput.getIdpImagePath().length() > 0) {
							ret.getDisplayData().put("IDP_IMAGE",
									"http://localhost:8080/pdi-web/upload/" + reportInput.getIdpImagePath());
						} else {
							ret.getDisplayData().put("IDP_IMAGE", "");
						}

					}
				} // end DO ONLY ONCE... (loopCounter == 0)

				// System.out.println(" doing abyd extract processing.... ");
				/*
					SOME OF THIS MAY BE REDUNDANT, BUT WAS PUT under DIFFERENT keys FOR
					THE EXTRACT THAN IT WAS FOR THE RPEORTS, SO IF THIS IS AN EXTRACT,
					WE'RE GOING TO GO THROUGH THESE MOTIONS.
					THE INSTRUMENT SCORES, WE NEVER GOT IN THE ABYD REPORTS SO MUST DO HERE
				 */

				// Get all the individual report scores.
				// one instrument per epi.... and keep adding
				// to the same individual report (ret)
				if (epi.getInstrument().getCode().equals(ReportingConstants.IC_WGE)) {
					scoreWGEForExtract(ret, epi);
				}

				// System.out.println("reportData.size... " +
				// ret.getReportData().size());

				if (loopCounter == 0) {
					// ONLY NEED TO DO THESE THINGS ONE TIME FOR THIS
					// PARTICIPANT.
					ret.getDisplayData().put("ParticipantFname", epi.getParticipant().getFirstName());
					ret.getDisplayData().put("ParticipantLname", epi.getParticipant().getLastName());
					ret.getDisplayData().put("ParticipantId", epi.getParticipant().getId());

					// Get the TLT and RI (Dashboard) report data
					// This also gets the dLCI (put out in IG data)
					ret = aeh.addDRIandTLT(ret, dnaId, participantId, reportInput);

					// Get the client competencies
					ret = aeh.addClientCompetencyScores(ret, participantId, dnaId, reportModelStructure);

					// Get the IR stuff
					try {
						ret = aeh.addIGDataforALR(ret, participantId, dnaId, cmpInternalNames);
					} catch (Exception e) {
						// Report it but go on
						System.out.println("Error found in ALR IR data fetch:  pptId=" + participantId + ", dnaId="
								+ dnaId + ", msg=" + e.getMessage());
					}

					// Get the EG stuff
					ret = aeh.addEGDataforALR(ret, participantId, dnaId, cmpInternalNames);

					// Do we need externally scored stuff?
					boolean hasKfp = false;
					boolean hasKfar = false;
					hasKfp = HelperUtils.alpCheck(dnaId);
					if (!hasKfp) {
						hasKfar = HelperUtils.kfarCheck(dnaId);
					}
					// Get ALP stuff if needed
					if (hasKfp) {
						// We have an ALP... get the data
						ret = aeh.getAlrCompScoreData(ret, participantId, dnaId, cmpInternalNames,
								ReportingConstants.SRC_KFP);

						// Get ALP raw/scoring data
						ret = aeh.getAlpRawData(ret, participantId, projectId);
					} else if (hasKfar) {
						// Get the KF4D data
						ret = aeh.getAlrCompScoreData(ret, participantId, dnaId, cmpInternalNames,
								ReportingConstants.SRC_KF4D);

						// Will we get the KF4D raw data?
						try {
							IGKf4dDataHelper kdh = new IGKf4dDataHelper(con, participantId, projectId);
							ret = kdh.getKf4dRawData(ret, participantId, projectId);
						} catch (Exception e) {
							throw new Exception(e);
						}

						finally {

						}
					} else if (HelperUtils.gpiCogsCheck(dnaId)) {
						ret = aeh.getGpiCogsCompScoreData(ret, participantId, dnaId, cmpInternalNames);
					}

					// Target LEVEL
					String trgLevel = ret.getDisplayData("TARGET_LEVEL_CODE");
					String fileName = "";
					// IndividualReport nextTLT = reportInput.getCurrentTLT();
					// if(nextTLT.getDisplayData().get("TARGET_LEVEL").equalsIgnoreCase("9"))
					// {
					// trgLevel = "SEA";
					// }
					// else
					// if(nextTLT.getDisplayData().get("TARGET_LEVEL").equalsIgnoreCase("8"))
					// {
					// trgLevel = "BUL";
					// }
					// else
					// if(nextTLT.getDisplayData().get("TARGET_LEVEL").equalsIgnoreCase("7"))
					// {
					// trgLevel = "MLL";
					// }
					// fileName = StringUtils.removeFilenameSpecialCharacters(
					// reportInput.getParticipantNameInverse() + " " +
					// epi.getProject().getProjectTypeName() + " " +
					// trgLevel + " "+
					// ret.getReportCode()+ " " +
					// new SimpleDateFormat("yyyyMMdd").format(new Date()) + " "
					// +
					// langCode
					// );
					// ret.setName(fileName.replace(" ", "_")+".xls");
					fileName = StringUtils.removeFilenameSpecialCharacters(epi.getProject().getClient().getName() + " "
							+ epi.getProject().getProjectTypeName() + " " + trgLevel + " " + ret.getReportCode() + " "
							+ new SimpleDateFormat("yyyyMMdd").format(new Date()) + " " + langCode);
					gr.setName(fileName.replace(" ", "_") + ".xls");
					// ret.setName(epi.getParticipant().getFirstName() + "_" +
					// epi.getParticipant().getLastName() + "_" +
					// reportInput.getDnaName() + "_" +
					// reportInput.getClientName() + "_" + ret.getReportCode()+
					// ".xls");

					gr.getDisplayData().put("ClientName", reportInput.getClientName());

					gr.getDisplayData().put("ExtractDate", new SimpleDateFormat("MM/dd/yyyy").format(new Date()));

					ret = aeh.addIrData(gr, ret, participantId, dnaId);
				} // End of if loopCounter == 0

				loopCounter++;
			} // end main epi for loop

			// Do the PDA stuff
			ret = (new PdaDataHelper()).populatePdaData(ret, pptId, projId, langCode);

			// add individual report (with all the instrument data) to group
			// report
			gr.getIndividualReports().add(ret);
			// Add group report to the ReportRequest, if there are individual
			// reports
			if (gr.getIndividualReports().size() > 0) {
				// System.out.println(" add the individual report to the group
				// report... add the group report to the reporting request...");
				groupReports.put(ret.getReportCode(), gr);
				reportingRequest.addReport(gr);
			}

			// Put out the names
			// Note that the keys will always be of the form "KFLA_COMPn_Name"
			// even on custom models
			Map<String, String> compNames = null;

			boolean isCustom = aeh.checkCustom(cmpInternalNames);
			if (isCustom) {
				// if a custom model call the name generator, else do the line
				// below
				compNames = aeh.getCustCompNames(dnaId);
			} else {
				compNames = aeh.getCompNamesWSyn(dnaId);
			}
			ret.getDisplayData().putAll(compNames);
			
			String json = JSONUtils.convertObjectToJSON(ret);
			
			log.debug("IndividualReport JSON: {}", json);
			
			log.debug("Configuration Data size: {}", ret.getConfigurationData().size());
			log.debug("Label Data size: {}", ret.getLabelData().size());
			log.debug("Report Data size: {}", ret.getReportData().size());
			log.debug("Scripted Data size: {}", ret.getScriptedData().size());
			log.debug("Scripted Group Data size: {}", ret.getScriptedGroupData().size());

		} catch (Exception e) {
			System.out.println("getAlrExtractData(): Error fetching ALR extract data. Msg=" + e.getMessage());
			e.printStackTrace();
		} finally {
			try {
				if (con != null) {
					con.close();
				}

			} catch (Exception se) {
				se.printStackTrace();
			}
		}
	}

	/**
	 * scoreWGEGraphicalRpt is a method that scores data for the WATSON GLASER E
	 * graphical report
	 *
	 * it uses the scoring code from the do method, and makes it available to
	 * the extract report.
	 *
	 * @param ReportData
	 *            rd
	 * @param ProjectParticipantInstrument
	 *            epi
	 * @param SessionUser
	 *            sessionUser
	 */
	private void scoreWGEForExtract(IndividualReport ret, ProjectParticipantInstrument epi) {

		// System.out.println("scoreWGEForExtract...");
		try {
			ReportData rd = new ReportData();

			// get the normed score objects....
			HashMap<String, NormScore> gpPercentiles = epi.getScoreGroup().calculateGeneralPopulation();
			HashMap<String, NormScore> spPercentiles = epi.getScoreGroup().calculateSpecialPopulation();

			// get ratings from the NormScore objects

			// RDS Key=WGE_CT_GP_RATING
			Iterator<NormScore> iter = gpPercentiles.values().iterator();
			while (iter.hasNext()) {
				NormScore ns = iter.next();
				rd.getScoreData().put(ns.getNorm().getSpssValue() + "_GP_RATING", ns.toRating().toString());
			}

			// RDS Key=WGE_CT_GP_RATING
			iter = spPercentiles.values().iterator();
			while (iter.hasNext()) {
				NormScore ns = iter.next();
				rd.getScoreData().put(ns.getNorm().getSpssValue() + "_SP_RATING", ns.toRating().toString());
			}

			// Norm Names
			rd.getScoreData().put("WGE_CT_GP_NAME", epi.getScoreGroup().getGeneralPopulation() == null ? ""
					: epi.getScoreGroup().getGeneralPopulation().getName());
			rd.getScoreData().put("WGE_CT_SP_NAME", epi.getScoreGroup().getSpecialPopulation() == null ? ""
					: epi.getScoreGroup().getSpecialPopulation().getName());

			// raw scores / scale names
			Iterator<Score> iterScore = epi.getScoreGroup().getScores().values().iterator();
			while (iterScore.hasNext()) {
				Score score = iterScore.next();
				rd.getScoreData().put(score.getCode(), score.getRawScore());
				rd.getDisplayData().put(score.getCode() + "_SCALE_NAME", score.getScaleName());
			}

			// add to report data for individual report
			ret.getReportData().put(AbyDExtractFormatHelper.INST_CODE_TO_RC_NAME.get(epi.getInstrument().getCode()),
					rd);
		} catch (Exception e) {
			System.out.println("scoreWGEForExtract:");
			e.printStackTrace();
		}
	}

}
