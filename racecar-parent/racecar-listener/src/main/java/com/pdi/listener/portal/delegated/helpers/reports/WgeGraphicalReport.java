/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.listener.portal.delegated.helpers.reports;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.pdi.data.dto.NormScore;
import com.pdi.data.dto.ProjectParticipantInstrument;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.Report;
import com.pdi.reporting.report.ReportData;
import com.pdi.reporting.report.utils.BWUtils;
import com.pdi.string.StringUtils;
import com.pdi.xml.XMLUtils;

/*
 * WgeGraphicalReport - Class to hold methods relevant to the Watson-Glaser
 *                      Form E graphical report
 * 
 * NOTE:  EVEN THOUGH LOCATED UNDER THE DELEGATED FOLDER, THERE ARE NO METHODS IN THIS
 *        CLASS THAT ARE DEFINED IN THE DATA INTERFACE.
 *        
 * This class is located here because the folder structure (delegated/helpers) is
 * inverted from normal practice elsewhere in this project stack (helpers/delegated).
 * Putting this helper class here avoided confusion with having two folders in the
 * portal structure that had the name "helper".
 */
public class WgeGraphicalReport
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	
	//
	// Constructors.
	//
    public WgeGraphicalReport()
    {
    	// No logic here at the present time
    }

	//
	// Instance methods.
	//


	/**
	 * getWgeDetailData - Method that retrieves the requisite data for the
	 *                    Watson-Glaser Form E graphical report
	 * 
	 * @param inp - An IndividualReport object
	 * @param epis - The ParojectParticipantReport objects associated with the request
	 */
    public ArrayList<Report> getWgeGraphicalData(IndividualReport inp,  
			ArrayList<ProjectParticipantInstrument> epis )
	{
		//System.out.println("doWGEGraphicalRpt...");
		ArrayList<Report> rpts = new ArrayList<Report>();
		
		try
		{
			for(ProjectParticipantInstrument epi : epis)
			{
				// spin through until you find the WG-E
				if(!epi.getInstrument().getCode().equals("wg"))
				{
					continue;
				}

   	   			IndividualReport ret = inp.clone();	// get the input data

				/*
				 * ========================================
				 * ADD BASIC PARTICIPANT DATA TO REPORT 
				 */
				ret.addDisplayData("FIRST_NAME", epi.getParticipant().getFirstName());
				ret.addDisplayData("LAST_NAME", epi.getParticipant().getLastName());
				ret.getDisplayData().put("PARTICIPANT_NAME", epi.getParticipant().getFirstName() + " " + epi.getParticipant().getLastName());			
				ret.addDisplayData("ORGANIZATION", XMLUtils.xmlEscapeString(epi.getProject().getClient().getName()));
		  		ret.addDisplayData("PROJECT_NAME", XMLUtils.xmlEscapeString(epi.getProject().getName()));	

		  		ReportData rd = new ReportData();
		  		ret.getDisplayData().put("ADMIN_DATE", epi.getInstrumentTakenDate().substring(0, 10));	  		
		  		ret.getDisplayData().put("WGE_SP_NAME", epi.getScoreGroup().getSpecialPopulation().getName()); // SPEC-POP NORM					
		  		ret.getDisplayData().put("WGE_GP_NAME", epi.getScoreGroup().getGeneralPopulation().getName() ); //GEN-POP NORM

		  		String fileName = StringUtils.removeFilenameSpecialCharacters(
						epis.get(0).getParticipant().getLastName() + " " +
						epis.get(0).getParticipant().getFirstName() + " " +
						epis.get(0).getProject().getProjectTypeName()+" "+
						inp.getReportCode()+" "+
						new SimpleDateFormat("yyyyMMdd").format(new Date()) + " " 
						);
						
				ret.setName(fileName.replace(" ", "_") + ".pdf");

				ret.getDisplayData().put("CLIENT_LOGO", epi.getProject().getClient().getId()+".png");

				// from WG_E.groovy : public IndividualReport generate()

				//WGE not complete??
				if(epi.getScoreGroup().getScores().isEmpty())
				{
					// They haven't finished the WG_E
					ret.getScriptedData().put("STATUS", "NOT_FINISHED");
					ret.getScriptedData().put("ERROR", "Watson Glaser II has not been completed, unable to generate report");
					ret.getReportData().put(ReportingConstants.RC_WG_E, rd);				
					rpts.add(ret);
					continue;
				} else {
					ret.getScriptedData().put("STATUS", "");
					ret.getScriptedData().put("ERROR", "");
				}

				if(epi.getScoreGroup().getScores().get("WGE_CT").getRawScore()  == null)
				{
					ret.getScriptedData().put("STATUS", "NOT_FINISHED");
					ret.getScriptedData().put("ERROR", "No score data for Ravens");
				}

				// Calc the reference line positions and put them out
				float[] ref = BWUtils.getRefPosArray();
				rd.getScriptedData().put("REF_1", new Float(ref[0]).toString());
				rd.getScriptedData().put("REF_3", new Float(ref[1]).toString());
				rd.getScriptedData().put("REF_16", new Float(ref[2]).toString());
				rd.getScriptedData().put("REF_50", new Float(ref[3]).toString());
				rd.getScriptedData().put("REF_84", new Float(ref[4]).toString());
				rd.getScriptedData().put("REF_97", new Float(ref[5]).toString());
				rd.getScriptedData().put("REF_99", new Float(ref[6]).toString());

				// get the normed score objects for the dimensions....
				HashMap<String, NormScore> gpPercentiles = epi.getScoreGroup().calculateGeneralPopulation();
				HashMap<String, NormScore> spPercentiles = epi.getScoreGroup().calculateSpecialPopulation();

				// Round the percentiles
				double pctl = gpPercentiles.get("WGE_CT").toPercentile();
				int pOut = new Double(Math.floor(pctl + 0.5)).intValue();
				if (pOut < 1)
					pOut = 1;
				else if (pOut > 99)
					pOut = 99;
				rd.getScriptedData().put("WGE_GP_PCTL", new Integer(pOut).toString());

				pctl = spPercentiles.get("WGE_CT").toPercentile();
				pOut = new Double(Math.floor(pctl + 0.5)).intValue();
				if (pOut < 1)
					pOut = 1;
				else if (pOut > 99)
					pOut = 99;
				rd.getScriptedData().put("WGE_SP_PCTL", new Integer(pOut).toString());
			
				// Get the norms
				// but we don't need to do this because we have them already.
				// however.... 							
				// ... we have to
				// convert the com.pdi.data.Norm to a com.pdi.scoring.Norm 
				// because BWUtils can't see the pdi.data Norm object, and if you 
				// add the pdi-data project to pdi-util-reporting the whole system all of a sudden
				// sees two Norm objects, and blows the whole thing up.
				com.pdi.scoring.Norm gpNorm = new com.pdi.scoring.Norm(epi.getScoreGroup().getGeneralPopulation().getNorms().get("WGE_CT").getMean(),
																	   epi.getScoreGroup().getGeneralPopulation().getNorms().get("WGE_CT").getStandardDeviation());

				com.pdi.scoring.Norm spNorm = new com.pdi.scoring.Norm(epi.getScoreGroup().getSpecialPopulation().getNorms().get("WGE_CT").getMean(),
						   epi.getScoreGroup().getSpecialPopulation().getNorms().get("WGE_CT").getStandardDeviation());

				// Calc the score position								
				double score = epi.getScoreGroup().getScores().get("WGE_CT").scoreAsNumber();
				float scorPos = BWUtils.calcScorePos(score, gpNorm);
				rd.getScriptedData().put("WGE_POSN", new Float(scorPos).toString());

				// Calc the cut-point positions
				float[] csPos = BWUtils.getCutPosArray(gpNorm, spNorm);
				rd.getScriptedData().put("WGE_CP_3",  new Float(csPos[0]).toString());
				rd.getScriptedData().put("WGE_CP_11", new Float(csPos[1]).toString());
				rd.getScriptedData().put("WGE_CP_23", new Float(csPos[2]).toString());
				rd.getScriptedData().put("WGE_CP_40", new Float(csPos[3]).toString());
				rd.getScriptedData().put("WGE_CP_60", new Float(csPos[4]).toString());
				rd.getScriptedData().put("WGE_CP_77", new Float(csPos[5]).toString());
				rd.getScriptedData().put("WGE_CP_89", new Float(csPos[6]).toString());
				rd.getScriptedData().put("WGE_CP_97", new Float(csPos[7]).toString());

				ret.getReportData().put(ReportingConstants.RC_WG_E, rd);				
				rpts.add(ret);
			}	// End main for-loop

			return rpts;
		}
		catch(Exception e)
		{
			System.out.println("doWGEGraphicalRpt:");
			e.printStackTrace();
			return null;
		}
	}
}
