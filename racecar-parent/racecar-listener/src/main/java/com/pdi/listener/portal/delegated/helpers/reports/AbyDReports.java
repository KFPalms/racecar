/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.listener.portal.delegated.helpers.reports;

import java.math.BigDecimal;
import java.sql.Connection;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
//import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdi.data.abyd.dto.common.DNACellDTO;
import com.pdi.data.abyd.dto.common.IGInstCompData;
import com.pdi.data.abyd.dto.intGrid.IGInstDisplayData;
import com.pdi.data.abyd.dto.intGrid.IGIntersectionAndOtherDataDTO;
import com.pdi.data.abyd.dto.reportInput.ReportInputDTO;
import com.pdi.data.abyd.dto.setup.EntryStateDTO;
import com.pdi.data.abyd.dto.setup.ReportModelCompChildDTO;
import com.pdi.data.abyd.dto.setup.ReportModelCompetencyDTO;
import com.pdi.data.abyd.dto.setup.ReportModelStructureDTO;
import com.pdi.data.abyd.dto.setup.ReportModelSuperFactorDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportLabelValueDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportLabelsDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportOptionValueDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportOptionsDTO;
import com.pdi.data.abyd.helpers.common.ImportExportDataHelper;
import com.pdi.data.abyd.helpers.intGrid.IGScoring;
import com.pdi.data.abyd.helpers.intGrid.IGSummaryDataHelper;
import com.pdi.data.abyd.helpers.intGrid.IGTestColumnDataHelper;
import com.pdi.data.abyd.helpers.reportInput.ReportInputDataHelper;
import com.pdi.data.abyd.helpers.setup.EntryStateHelper;
import com.pdi.data.abyd.helpers.setup.ReportLabelsDataHelper;
import com.pdi.data.abyd.helpers.setup.ReportModelDataHelper;
import com.pdi.data.abyd.helpers.setup.ReportOptionsDataHelper;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.data.abyd.util.AbyDExtractHelper;
import com.pdi.data.dto.NormScore;
import com.pdi.data.dto.Participant;
import com.pdi.data.dto.ProjectParticipantInstrument;
import com.pdi.data.dto.Score;
import com.pdi.data.dto.SessionUser;
import com.pdi.data.dto.TestData;
import com.pdi.data.dto.Text;
import com.pdi.data.dto.TextGroup;
import com.pdi.data.helpers.HelperDelegate;
import com.pdi.data.helpers.interfaces.ILanguageHelper;
import com.pdi.data.helpers.interfaces.ITestDataHelper;
import com.pdi.data.util.language.DefaultLanguageHelper;
import com.pdi.properties.PropertyLoader;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.GroupReport;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;
import com.pdi.reporting.report.helpers.AbyDExtractFormatHelper;
import com.pdi.reporting.request.ReportingRequest;
import com.pdi.string.StringUtils;
import com.pdi.xml.XMLUtils;

/*
 * AbyDReports - class to hold methods relevant to the A by D reports.  It
 * 				 gathers data for multiple reports and for the A by D extract.
 *
 * NOTE:  EVEN THOUGH LOCATED UNDER THE DELEGATED FOLDER, THERE ARE NO METHODS IN THIS
 *        CLASS THAT ARE DEFINED IN THE DATA INTERFACE.
 *
 * This class is located here because the folder structure (delegated/helpers) is
 * inverted from normal practice elsewhere in this project stack (helpers/delegated).
 * Putting this helper class here avoided confusion with having two folders in the
 * portal structure that had the name "helper".
 *
 * Yes, this is not a report but an extract.  Still, it is located in the report folder
 */
public class AbyDReports {
	
	private static final Logger log = LoggerFactory.getLogger(AbyDReports.class);

	//
	// Static data.
	//
	private static int EDITABLE = 1;

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private long languageId = 0;

	//
	// Constructors.
	//
	public AbyDReports() {
		// No logic here at the present time
	}

	//
	// Instance methods.
	//

	/**
	 * getAbyDReportData is a method that handles all AbyD specific report
	 * functions - gathers and sets up all AbyD specific Data.
	 *
	 * This is a new method, pulling together and porting older code from
	 * previously existing groovy scripts.
	 *
	 * @param ret
	 * @param epis
	 * @param overrides
	 * @param sessionUser
	 * @param tg
	 */
	public void getAbyDReportData(IndividualReport inp, HashMap<String, GroupReport> groupReports,
			ReportingRequest reportingRequest, ArrayList<ProjectParticipantInstrument> epis, SessionUser sessionUser,
			TextGroup tg, String langCode, boolean addBreaks) {
		log.debug("Top of getAbyDReportData()...");
		// Stick the labels somewhere because we don't want to put them back
		// into memory
		HashMap<String, String> lblMap = new HashMap<String, String>();
		if (addBreaks) {
			// Loop through tg and do break enhancement
			// Done here so that both the GroupReport and IndividualReport
			// objects are covered
			for (Iterator<Map.Entry<String, Text>> itr = tg.getLabelMap().entrySet().iterator(); itr.hasNext();) {
				// get the next entry
				Map.Entry<String, Text> ent = itr.next();

				// get the data
				String key = ent.getKey();
				Text txt = ent.getValue();
				// massage it
				String out = "";

				/* Adding the Pearson copyright (SBA-164, 165, 166) created an issue.
				 * It's 3 or 4 lines in the footer.  I needed to decrease the line height
				 * but it didn't work because formatNonSpacingTxt adds in span tags.
				 * Therefore I bypass that routine for the copyrights except in the case of Japanese which
				 * needs the span tags and is therefore the only language where the line
				 * height is not adjusted -- but it almost runs off the page. Mark Arnold
				 * */

				if (key.equals("COPYRIGHT_PEARSON")) {
					if (langCode.equals("ja")) {
						out = StringUtils.formatNonSpacingTxt(txt.getText());
					} else {
						out = txt.getText();
					}
				} else if (key.equals("COPYRIGHT")) {
					out = txt.getText();
				} else {
					out = StringUtils.formatNonSpacingTxt(txt.getText());
				}

				// replace the string in the label value hashmap
				lblMap.put(key, out);
			}
		} else {
			lblMap = tg.getLabelHashMap();
		}

		// System.out.println("doAbyDSpecificReportTasks..." +
		// ret.getReportCode());
		Connection con = null;

		try {
			// Get the language id (we have the code)
			ILanguageHelper lh = HelperDelegate
					.getLanguageHelper(PropertyLoader.getProperty("com.pdi.data.v2.application", "datasource.v2"));
			//// tg.getLabelMap().put("LANG_CODE", new Text(0, "LANG_CODE",
			//// langObj, langCode));
			// public Language fromCode(SessionUser su, String code)

			languageId = new Long(lh.fromCode(sessionUser, langCode).getId()).longValue();

			con = AbyDDatabaseUtils.getDBConnection();
			IndividualReport ret = inp.clone(); // get the input data

			String projectId = epis.get(0).getProject().getId();
			long dnaId = 0;
			GroupReport gr = new GroupReport(); // we will need this if an
												// extract
			ReportModelStructureDTO reportModelStructure = null;
			ReportInputDTO reportInput = null;
			HashMap<String, ReportData> reportDataMap = new HashMap<String, ReportData>();
			String rptCode = ret.getReportCode();

			// FIRST, IF THIS IS THE EXTRACT CREATE THE group report...
			// AND ADD IT TO THE GROUP REPORTS MAP....
			if (rptCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_EXTRACT)
					|| rptCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_ANALYTICS_EXTRACT)
					|| rptCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_RAW_EXTRACT)) {
				// System.out.println(" create group report... ");
				if (groupReports.get(rptCode) == null) {
					// First time entered, create a new group report...
					// (created above the if... )
					// ...and stick in the labels
					gr.getLabelData().putAll(lblMap);
					// only label added is the language code
					// add it to the groupReports map....
					groupReports.put(ret.getReportCode(), gr);

					// set the report data map so we can add
					// to it in the instrument loop
					ret.setReportData(reportDataMap);
				} else {
					gr = groupReports.get(ret.getReportCode());
				}
				// System.out.println("Got groupReport data... code=" +
				// ret.getReportCode());

				// now start pulling stuff from the
				// com.pdi.listener.portal.groovy.ABYD_EXTRACT.groovy
				AbyDExtractHelper.getGroupData(con, projectId, gr);
				// don't forget to set the dnaId variable:
				dnaId = new Long(gr.getDisplayData().get("DNA_ID")).longValue();
				// if (gr == null)
				if (gr.getDisplayData().get("DNA_ID") == null || dnaId == 0) {
					System.out.println("No DNA data for project " + projectId);
				} else {
					// go ahead and do all the stuff....
					gr.setReportCode(rptCode);
					gr.setReportType(ReportingConstants.REPORT_TYPE_GROUP_EXTRACT);

					// Add the client competency structure to the GR
					AbyDExtractHelper.addClientCompetencyStructure(con, gr, dnaId);
				}
			} // End of extract specific processing block

			// NOW DEAL WITH INDIVIDUAL REPORT DATA... TO BE ADDED TO GROUP
			// REPORT IF
			// WE'RE DEALING WITH AN EXTRACT.
			// ONE INDIVIDUAL REPORT PER PARTICIPANT, TO BE ADDED TO THE GROUP
			// REPORT.
			String currentPid = "00000";
			int loopCounter = 0;

			String pptId = null;
			String projId = null;

			for (ProjectParticipantInstrument epi : epis) {
				// System.out.println("epi loop... " +
				// epi.getInstrument().getCode());

				boolean previewReport = false;

				String participantId = epi.getParticipant().getId();
				if (org.apache.commons.lang3.StringUtils.isEmpty(participantId)){
					//avoid nullpointerexception
					participantId = "";
				}
				log.debug("Participant id: {}, Current Pid: {}, Report Code: {}", participantId, currentPid, rptCode);
				if (participantId.equals(currentPid)
						&& !(rptCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_EXTRACT)
								|| rptCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_ANALYTICS_EXTRACT)
								|| rptCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_RAW_EXTRACT))) {
					// i only want to do this one time for each participant id,
					// UNLESS THIS IS AN EXTRACT, IN WHICH CASE, I NEED TO DO IT
					// ONCE FOR EACH INSTRUMENT...
					break;
				} else {
					currentPid = participantId;
				}
				projectId = epi.getProject().getId();
				if (participantId == null || participantId.equalsIgnoreCase("")) {
					participantId = "";
					previewReport = true;
				}

				// Save the project ID for downstream use
				ret.addConfigurationData("ProjId", projectId);

				// ONLY DO THESE THINGS THE FIRST TIME AROUND (FOR EXTRACT...
				// ONLY ONE TIME AROUND FOR ALL OTHER REPORTS)
				if (loopCounter == 0) {
					pptId = participantId;
					projId = projectId;

					// System.out.println(" loop counter 1 " + loopCounter);
					dnaId = new ImportExportDataHelper().getDnaFromV2(con, projectId);
					ReportModelDataHelper reportModelDataHelper = new ReportModelDataHelper();
					ReportInputDataHelper reportInputDataHelper = new ReportInputDataHelper();

					// String fakeProjId = "";
					String fakePartId = "";
					long fakeDnaId = 0;

					if (previewReport) {
						// They want to preview the report output with a fake
						// participant
						// fakeProjId =
						// PropertyLoader.getProperty("com.pdi.listener.portal.application",
						// "sampleReport.projectId");
						fakePartId = PropertyLoader.getProperty("com.pdi.listener.portal.application",
								"sampleReport.participantId");

						// gonna need the real DNAId, to get the right
						// setup2 options....
						TestData td = new TestData();
						td.setParticipantId(new Long(fakePartId).longValue());
						td.setProjectId(new Long(projectId).longValue());
						ITestDataHelper tdh = HelperDelegate.getTestDataHelper("com.pdi.data.v2.helpers.delegated");
						fakeDnaId = tdh.getDnaIdFromProjectId(sessionUser, td); // gonna
																				// need
																				// this
																				// to
																				// get
																				// some
																				// participant
																				// scores...

						// create participant fake participant object sub it
						// into the EPI...
						Participant fakePartObj = HelperDelegate
								.getParticipantHelper("com.pdi.data.nhn.helpers.delegated")
								.fromId(sessionUser, fakePartId);
						epi.setParticipant(fakePartObj);
					}

					if (previewReport) {
						if (rptCode.equals(ReportingConstants.REPORT_CODE_ABYD_CUSTOM_MODEL_REPORT)) {
							// don't need user data, just the model...
							// and it needs to be the "real" dnaId, not the fake
							// one...
							// System.out.println(" use this dna instead... " +
							// dnaId);
							reportModelStructure = reportModelDataHelper.getReportModel(con, dnaId);
						} else {
							// need to get scores, so using the fake data to get
							// some scores.....
							reportModelStructure = reportModelDataHelper.fetchReportModelAndScores(con, fakeDnaId,
									languageId, fakePartId, true);
						}

						reportInput = reportInputDataHelper.getReportInput(fakePartId, fakeDnaId, langCode, true);
					} else {
						reportModelStructure = reportModelDataHelper.fetchReportModelAndScores(con, dnaId, languageId,
								participantId, true);
						reportInput = reportInputDataHelper.getReportInput(participantId, dnaId, langCode);
					}

					// add the scripted data from the reportInput curTLT, so
					// that we have TLT scores for the report.
					ret.setScriptedData(reportInput.getCurrentTLT().getScriptedData());

					/*
					 * ========================================
					 * ADD BASIC PARTICIPANT DATA TO REPORT
					 */
					ret.addDisplayData("FIRST_NAME", epi.getParticipant().getFirstName());
					ret.addDisplayData("LAST_NAME", epi.getParticipant().getLastName());
					ret.getDisplayData().put("PARTICIPANT_NAME",
							epi.getParticipant().getFirstName() + " " + epi.getParticipant().getLastName());
					ret.addDisplayData("EMAIL", epi.getParticipant().getEmail());
					// BUS_UNIT AND JOB_TITLE are v2 specific inputs, which have
					// been shifted to
					// the optional fields in the new admin tools, leaving them,
					// for legacy, for now...
					ret.addDisplayData("BUS_UNIT", epi.getParticipant().getMetadata().get("optional_1"));
					ret.addDisplayData("JOB_TITLE", epi.getParticipant().getMetadata().get("optional_2"));
					// adding optional 1 & 2, because of how they are being
					// saved and requested in the html
					// and because job title and business unit are v2 things
					ret.addDisplayData("OPTIONAL_1", epi.getParticipant().getMetadata().get("optional_1"));
					ret.addDisplayData("OPTIONAL_2", epi.getParticipant().getMetadata().get("optional_2"));
					ret.addDisplayData("OPTIONAL_3", epi.getParticipant().getMetadata().get("optional_3"));

					if (previewReport) {
						// Same data in preview
						ret.getDisplayData().put("ADMIN_DATE",
								new SimpleDateFormat("ddMMMyyyy").format(new java.util.Date()));
						ret.getDisplayData().put("ADMIN_LONG_DATE", ret.getDisplayData("ADMIN_DATE"));
					} else {
						ret.getDisplayData().put("ADMIN_DATE",
								new IGSummaryDataHelper(con).getIGSubmittedDate(dnaId, participantId));
						ret.getDisplayData().put("ADMIN_LONG_DATE",
								new IGSummaryDataHelper(con).getFullIGSubmittedDate(dnaId, participantId));
					}

					// Add Target-specific flag
					boolean isTarget = false;
					if (rptCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_TARGET_ORG)) {
						ret.getDisplayData().put("TARGET_IS_ORG", "1");
						isTarget = true;
					}
					if (rptCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_TARGET_PPT)) {
						ret.getDisplayData().put("TARGET_IS_ORG", "0");
						isTarget = true;
					}

					if (isTarget) {
						// Replace the Admin date with the Target required admin
						// date
						ret.getDisplayData().put("ADMIN_DATE", reportInput.getInterviewDateStr());
						ret.getDisplayData().put("ADMIN_LONG_DATE", reportInput.getInterviewDateStr());
					}

					// These lines do not appear to be used... Remove them?
					// Random generator = new Random();
					// int randomIndex = generator.nextInt( 10000 ) * 10000;

					// System.out.println("Participant: name=" +
					// participant.getFirstName() + " " +
					// participant.getLastName()+ ". Biz Unit=" +
					// participant.getBusinessUnit());
					String firstName = epi.getParticipant().getFirstName();
					if (firstName == null) {
						firstName = "";
					}
					String lastName = epi.getParticipant().getLastName();
					if (lastName == null) {
						lastName = "";
					}
					String businessUnit = epi.getParticipant().getMetadata().get("OPTIONAL_1");
					if (businessUnit == null) {
						businessUnit = "";
					}
					// ret.setName(firstName + "_" + lastName + "_" +
					// businessUnit.replace("/", "_").replace("\\", "_") + "_" +
					// randomIndex + ".pdf");

					// Get Project data
					ret.addDisplayData("ORGANIZATION",
							XMLUtils.xmlEscapeString(epi.getProject().getClient().getName()));

					ret.addDisplayData("TRANSITION_LEVEL", "" + epi.getProject().getTargetLevelTypeId());

					System.out.println("Transition level... ." + ret.getDisplayData("TRANSITION_LEVEL"));
					// ret.addDisplayData("PROJECT_NAME",
					// epi.getProject().getName());
					ret.getDisplayData().put("PROJECT_NAME", XMLUtils.xmlEscapeString(reportInput.getDnaName())); // Use
																													// the
																													// DNA
																													// name
																													// as
																													// the
																													// project
																													// name
																													// on
																													// A
																													// by
																													// D
																													// data
					ret.getDisplayData().put("CLIENT_NAME", XMLUtils.xmlEscapeString(reportInput.getClientName()));

					/*
					 *  Set up labels/option data/entry state
					 *  --------------------------------------
					 */

					ReportOptionsDataHelper optionsDH = new ReportOptionsDataHelper();
					ReportLabelsDataHelper labelsDH = new ReportLabelsDataHelper();
					EntryStateHelper entryH = new EntryStateHelper(con, projectId);

					Setup2ReportOptionsDTO options = null;
					Setup2ReportLabelsDTO labels = null;

					options = optionsDH.getReportOptionData(con, dnaId);
					labels = labelsDH.getTranslatedReportLabelData(dnaId, tg.getLanguageCode(), EDITABLE);
					log.debug("Calling get Entry state data");
					EntryStateDTO entryState = entryH.getEntryStateData();
					log.debug("Finished calling get Entry state data");

					ret.getDisplayData().put("CLIENT_LOGO", epi.getProject().getClient().getId() + ".png");

					ret.getDisplayData().put("DNA_NAME",
							XMLUtils.xmlEscapeString(entryState.getDnaDesc().getDnaName()));

					/***
					 * SET THE FILE NAME the new requirments:
					 * http://segjira:8090/browse/NHN-2269 Aug 15 2012
					 *
					 * Participant Last Name_+ Participant First Name_ + TLT or
					 * ABYD_+ Target Level_ + Selected_Report_ +
					 * Language_Report_ + Generation Date.pdf+
					 */
					// Target LEVEL
					String trgLevel = " ";
					IndividualReport nextTLT = reportInput.getCurrentTLT();
					if (nextTLT.getDisplayData().get("TARGET_LEVEL").equalsIgnoreCase("10")) {
						trgLevel = "SEA";

					} else if (nextTLT.getDisplayData().get("TARGET_LEVEL").equalsIgnoreCase("9")) {
						trgLevel = "CEO";

					} else if (nextTLT.getDisplayData().get("TARGET_LEVEL").equalsIgnoreCase("8")) {
						trgLevel = "BUL";

					} else if (nextTLT.getDisplayData().get("TARGET_LEVEL").equalsIgnoreCase("7")) {
						trgLevel = "MLL";
					}
					String fileName = StringUtils
							.removeFilenameSpecialCharacters(reportInput.getParticipantNameInverse() + " "
									+ epi.getProject().getProjectTypeName() + " " + trgLevel + " " + ret.getReportCode()
									+ " " + new SimpleDateFormat("yyyyMMdd").format(new Date()) + " " + langCode);

					ret.setName(fileName.replace(" ", "_") + ".pdf");

					// Hardcode Cogs Included to 1
					ret.getDisplayData().put(ReportingConstants.RC_COGNITIVES_INCLUDED, "1");

					// ADD OPTION DATA TO REPORT
					for (Setup2ReportOptionValueDTO value : options.getOptionValuesArray()) {
						if (value.getOptionString().equals("")) {
							ret.getDisplayData().put("OPTION_" + value.getOptionCode(),
									(value.getOptionValue() ? "true" : null));
						} else {
							// String str = value.getOptionString();
							ret.getDisplayData().put("OPTION_" + value.getOptionCode(), (value.getOptionString()));
						}
					}

					// ADD LABEL DATA TO REPORT
					for (Setup2ReportLabelValueDTO value : labels.getLabelValuesArray()) {
						if (value != null) {
							String cvalue = value.getCustomLabel();
							String svalue = value.getLabel();
							if ((cvalue.length() > 0)
									&& (this.languageId == DefaultLanguageHelper.fetchDefaultLanguageId())) // only
																												// do
																												// if
																												// en
																												// language(1)
							{
								svalue = value.getLabel();
								svalue = cvalue;
							}
							// System.out.println("value.getReportCode(): " +
							// value.getReportCode() + " svalue: " + svalue);

							ret.getDisplayData().put("LABEL_" + value.getReportCode(), svalue);
						}
					}

					if (labels.getCoverPage().getCustomLabel() != null
							&& labels.getCoverPage().getCustomLabel().length() > 0) {
						ret.getDisplayData().put("LABEL_COV_PAGE", labels.getCoverPage().getCustomLabel());
						// ret.getDisplayData().put("LABEL_COV_PAGE",
						// UnitTestUtils.unicodeText());
					} else {
						ret.getDisplayData().put("LABEL_COV_PAGE", labels.getCoverPage().getLabel());
						// ret.getDisplayData().put("LABEL_COV_PAGE",
						// UnitTestUtils.unicodeText());
					}

					if (labels.getLeadershipLegend().getCustomLabel() != null
							&& labels.getLeadershipLegend().getCustomLabel().length() > 0) {
						ret.getDisplayData().put("LABEL_LEG_LEAD", labels.getLeadershipLegend().getCustomLabel());
					} else {
						ret.getDisplayData().put("LABEL_LEG_LEAD", labels.getLeadershipLegend().getLabel());
					}

					if (labels.getTltLegend().getCustomLabel() != null
							&& labels.getTltLegend().getCustomLabel().length() > 0) {
						ret.getDisplayData().put("LABEL_LEG_TLT", labels.getTltLegend().getCustomLabel());
					} else {
						ret.getDisplayData().put("LABEL_LEG_TLT", labels.getTltLegend().getLabel());
					}

					// ADD Client/report model COMPETENCY STRUCTURE
					ret.getDisplayData().put("MODEL_SUPERFACTORS_COUNT",
							new Integer(reportModelStructure.getSuperFactorList().size() - 1).toString());
					// System.out.println("MODEL_SUPERFACTORS_COUNT" + new
					// Integer(reportModelStructure.getSuperFactorList().size()
					// - 1).toString());
					int i = 0;
					for (ReportModelSuperFactorDTO superfactor : reportModelStructure.getSuperFactorList()) {
						ret.getDisplayData().put("MODEL_SUPERFACTOR_" + i + "_DISPLAY_NAME",
								superfactor.getDisplayName());
						// System.out.println(" SF name is... "+
						// "MODEL_SUPERFACTOR_"+i+"_DISPLAY_NAME");
						ret.getDisplayData().put("MODEL_SUPERFACTOR_" + i + "_COMPETENCY_COUNT",
								new Integer(superfactor.getCompList().size() - 1).toString());
						int k = 0;
						for (ReportModelCompetencyDTO competency : superfactor.getCompList()) {
							ret.getDisplayData().put("MODEL_SUPERFACTOR_" + i + "_COMPETENCY_" + k + "_DISPLAY_NAME",
									competency.getDisplayName());
							ret.getDisplayData().put("MODEL_SUPERFACTOR_" + i + "_COMPETENCY_" + k + "_VALUE",
									competency.getCompScore());
							// System.out.println(" Comp name is... "+
							// "MODEL_SUPERFACTOR_"+i+"_COMPETENCY_"+k+"_DISPLAY_NAME");
							ret.getDisplayData().put("MODEL_SUPERFACTOR_" + i + "_COMPETENCY_" + k + "_ESSENTIAL",
									(competency.getEssential() ? "true" : null));
							ret.getDisplayData().put("MODEL_SUPERFACTOR_" + i + "_COMPETENCY_" + k + "_COUNT",
									new Integer(competency.getChildren().size() - 1).toString());
							if (competency.getChildren().size() == 1 && competency.getChildren().get(0).getDnaCompName()
									.equalsIgnoreCase(competency.getDisplayName())) {
								// Just one comp, and the name is the same,
								// nothing to do
							} else {
								int l = 0;
								for (ReportModelCompChildDTO competencyChild : competency.getChildren()) {
									if (competencyChild.getDnaCompName().length() > 0) {
										ret.getDisplayData()
												.put("MODEL_SUPERFACTOR_" + i + "_COMPETENCY_" + k + "_CHILD_"
														+ new Integer(l).toString() + "_DISPLAY_NAME",
														competencyChild.getDnaCompName());
										// System.out.println(" comp child name
										// ... " +
										// competencyChild.getDnaCompName() );
										l++;
									}
								}

								ret.getDisplayData().put("MODEL_SUPERFACTOR_" + new Integer(i).toString()
										+ "_COMPETENCY_" + new Integer(k).toString() + "_COUNT",
										new Integer(l).toString());
							}
							k++;
						}
						i++;
					} // End of Superfactor loop

					/*
					 * REPORT INPUT DATA
					 */
					// ret.getDisplayData().put("STATUS", new
					// Long(reportInput.getCompositeStatusId()).toString());
					ret.getDisplayData().put("STATUS", "" + reportInput.getCompositeStatusId());
					ret.getDisplayData().put("STATUS_OT", "" + reportInput.getOtxtStatusId());
					ret.getDisplayData().put("STATUS_PT", "" + reportInput.getPtxtStatusId());

					ret.getDisplayData().put("DERAIL_ORG_TXT", reportInput.getDerailmentOrgText());
					ret.getDisplayData().put("DERAIL_PRT_TXT", reportInput.getDerailmentPartText());
					ret.getDisplayData().put("DEV_PRT_TXT", reportInput.getDevelopmentPartText());
					ret.getDisplayData().put("PIV_PRT_TXT", reportInput.getPivotalPartText());

					ret.getDisplayData().put("SKL_LEV_ORG_TXT", reportInput.getSkillsToLeverageOrgText());
					ret.getDisplayData().put("SKL_LEV_PRT_TXT", reportInput.getSkillsToLeveragePartText());

					ret.getDisplayData().put("SKL_DEV_ORG_TXT", reportInput.getSkillsToDevelopOrgText());
					ret.getDisplayData().put("SKL_DEV_PRT_TXT", reportInput.getSkillsToDevelopPartText());

					if (reportInput.getIsAlr()) {
						// ALR dashboards
						// Readiness
						int readRat = reportInput.getAlrReadinessRating();
						if (readRat == 0) {
							readRat = reportInput.getAlrSugReadinessRating();
						}
						ret.getDisplayData().put("RED_RATING", new Integer(readRat).toString());
						ret.getDisplayData().put("READINESS_FOCUS", reportInput.getAlrReadinessFocus());
						ret.getDisplayData().put("RED_ORG_TXT", reportInput.getAlrReadinessOrgText());
						// Fit
						ret.getDisplayData().put("FIT_ORG_TXT", reportInput.getAlrCultureFitOrgText());
						// Fit rating is in ScriptedData
						int fitRat = reportInput.getAlrCultureFitRating();
						if (fitRat == 0) {
							fitRat = reportInput.getAlrSugCultureFitRating();
						}
						ret.getScriptedData().put("FIT_RATING", new Integer(fitRat).toString());
						// The fit index is the same for ALR as non-ALR
						ret.getScriptedData().put("CAN_FIT_IDX",
								new Integer(reportInput.getCandidateFitIndex()).toString());
						ret.getScriptedData().put("HAS_FR_VALUE", "1");
					} else {
						// "Normal" dashboards
						// Readiness
						ret.getDisplayData().put("RED_RATING",
								new Integer(reportInput.getReadinessRating()).toString());
						ret.getDisplayData().put("READINESS_FOCUS", reportInput.getReadinessFocus());
						ret.getDisplayData().put("RED_ORG_TXT", reportInput.getReadinessOrgText());
						// Fit
						ret.getDisplayData().put("FIT_ORG_TXT", reportInput.getFitOrgText());
						// Fit rating is in ScriptedData
						ret.getScriptedData().put("CAN_FIT_IDX",
								new Integer(reportInput.getCandidateFitIndex()).toString());
						ret.getScriptedData().put("FIT_RATING",
								new Integer(reportInput.getFitRecommendedRating()).toString());
						ret.getScriptedData().put("HAS_FR_VALUE", "0");
					}

					ret.getDisplayData().put("IDP_DOC", reportInput.getIdpDocumentPath());
					ret.getDisplayData().put("IDP_IMG", reportInput.getIdpImagePath());
					ret.getDisplayData().put("LEADER_EXP_ORG_TXT", reportInput.getLeadershipExperienceOrgText());
					ret.getDisplayData().put("LEADER_EXP_PRT_TXT", reportInput.getLeadershipExperiencePartText());
					ret.getDisplayData().put("LEADER_INT_ORG_TXT", reportInput.getLeadershipInterestOrgText());
					ret.getDisplayData().put("LEADER_INT_PRT_TXT", reportInput.getLeadershipInterestPartText());
					ret.getDisplayData().put("LEADER_SKL_ORG_TXT", reportInput.getLeadershipSkillOrgText());
					ret.getDisplayData().put("LEADER_SKL_PRT_TXT", reportInput.getLeadershipSkillPartText());
					ret.getDisplayData().put("LEADER_STY_ORG_TXT", reportInput.getLeadershipStyleOrgText());
					ret.getDisplayData().put("LEADER_STY_PRT_TXT", reportInput.getLeadershipStylePartText());
					ret.getDisplayData().put("LONGTERM_ORG_TXT", reportInput.getLongtermOrgText());

					if (reportInput.getIdpImagePath() != null && reportInput.getIdpImagePath().length() > 0) {
						ret.getDisplayData().put("IDP_IMAGE",
								"http://localhost:8080/pdi-web/upload/" + reportInput.getIdpImagePath());
					} else {
						ret.getDisplayData().put("IDP_IMAGE", "");
					}

					// Scripted data
					ret.getScriptedData().put("PIV_PRT_TXT", reportInput.getPivotalPartText());
					ret.getScriptedData().put("DERAIL_RATING",
							new Integer(reportInput.getDerailmentRating()).toString());
					ret.getScriptedData().put("LONGTERM_ADV_POT",
							new Integer(reportInput.getLongtermRating()).toString());
					ret.getScriptedData().put("LEADER_EXP_RATING",
							new Integer(reportInput.getLeadershipExperienceRating()).toString());
					ret.getScriptedData().put("LEADER_INT_RATING",
							new Integer(reportInput.getLeadershipInterestRating()).toString());
					ret.getScriptedData().put("LEADER_SKL_IDX",
							new Integer(reportInput.getLeadershipSkillIndex()).toString());
					ret.getScriptedData().put("LEADER_SKL_RATING",
							new Integer(reportInput.getLeadershipSkillRating()).toString());
					ret.getScriptedData().put("LEADER_STY_RATING",
							new Integer(reportInput.getLeadershipStyleRating()).toString());

					// Additional AbyD RAW extract processing, per NHN-2860,
					// adding testing column data to the extract
					if (rptCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_RAW_EXTRACT)) {

						IGTestColumnDataHelper IGTestColHlpr = new IGTestColumnDataHelper(con,
								epi.getParticipant().getId(), dnaId);
						IGInstDisplayData IGDisplayData = IGTestColHlpr.getTestingColumnData();

						// the 'true' is for cogsOn, which controls if cogs are
						// shown in the IG display,
						// and also is a flag in the scoring
						// IGScoring.calcTestingScores(con, partId, dnaId,
						// cogsOn, sDat)
						// The false is for ALP data... We don't have ALP data
						// in the AbyD Raw Data Extract
						// The second false is for KF4D data... We don't have
						// Kf4D data in the AbyD Raw Data Extract ei9ther
						IGIntersectionAndOtherDataDTO testingIntersection = IGScoring.fetchTestingIntersectionData(con,
								epi.getParticipant().getId(), dnaId, true, false, false, null);

						int IGTestColCnt = 0;
						int x = 1;
						// THIS IS THE CORRECT ORDER FOR THE COMPETENCIES IN THE
						// IG
						for (IGInstCompData compData : IGDisplayData.getCompArray()) {
							// OKAY, FOR EACH ONE OF THESE, IN ORDER, LET'S FIND
							// IT IN THE
							// TESTING INTERSECTION CELLS....
							for (DNACellDTO cell : testingIntersection.getTestingCells()) {
								// if the cell competency id matches the
								// compData competency id,
								// add the data in numeric order...
								if (cell.getCompetencyId() == compData.getCompId()) {
									// add this to the display data....
									ret.getDisplayData().put("IGTestCol_CompName_" + x, compData.getCompName());
									ret.getDisplayData().put("IGTestCol_Score_" + x, cell.getScore());
								}

							}

							IGTestColCnt++;
							x++;
						}
						ret.getDisplayData().put("IGTestColCnt", new Integer(IGTestColCnt).toString());

					} // end 2680 data

				} // end DO ONLY ONCE... (loopCounter == 0)

				// Additional extract processing
				if (rptCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_EXTRACT)
						|| rptCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_ANALYTICS_EXTRACT)
						|| rptCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_RAW_EXTRACT)) {
					// System.out.println(" doing abyd extract processing....
					// ");
					/*
						SOME OF THIS MAY BE REDUNDANT, BUT WAS PUT under DIFFERENT keys FOR
						THE EXTRACT THAN IT WAS FOR THE RPEORTS, SO IF THIS IS AN EXTRACT,
						WE'RE GOING TO GO THROUGH THESE MOTIONS.
						THE INSTRUMENT SCORES, WE NEVER GOT IN THE ABYD REPORTS SO MUST DO HERE
					 */

					// Get all the individual report scores.
					// one instrument per epi.... and keep adding
					// to the same individual report (ret)
					if (epi.getInstrument().getCode().equals(ReportingConstants.IC_WGE)) {
						scoreWGEForExtract(ret, epi);
					} else if (epi.getInstrument().getCode().equals(ReportingConstants.IC_FINEX)) {
						scoreFinExForExtract(ret, epi);
					} else if (epi.getInstrument().getCode().equals(ReportingConstants.IC_GPI)) {
						scoreGPIForExtract(ret, epi);
					} else if (epi.getInstrument().getCode().equals(ReportingConstants.IC_LEI)) {
						scoreLEIForExtract(ret, epi);
					} else if (epi.getInstrument().getCode().equals(ReportingConstants.IC_RAVENS)) {
						scoreRAVBForExtract(ret, epi);
					} else if (epi.getInstrument().getCode().equals(ReportingConstants.IC_CS2)) {
						scoreCSForExtract(ret, epi);
					}

					// System.out.println("reportData.size... " +
					// ret.getReportData().size());

					if (loopCounter == 0) {
						// ONLY NEED TO DO THESE THINGS ONE TIME FOR THIS
						// PARTICIPANT.
						ret.getDisplayData().put("ParticipantFname", epi.getParticipant().getFirstName());
						ret.getDisplayData().put("ParticipantLname", epi.getParticipant().getLastName());
						ret.getDisplayData().put("ParticipantId", epi.getParticipant().getId());

						// Get the TLT and RI (Dashboard) report data
						// This also gets the dLCI (put out in IG data)
						ret = AbyDExtractHelper.addDRIandTLT(ret, dnaId, participantId, reportInput);

						// Get the client competencies
						ret = AbyDExtractHelper.addClientCompetencyScores(con, ret, participantId, dnaId,
								reportModelStructure);

						// Get the IR stuff
						ret = AbyDExtractHelper.addIGData(con, ret, participantId, dnaId);

						// Get the EG stuff
						ret = AbyDExtractHelper.addEGData(con, ret, participantId, dnaId);

						// Target LEVEL
						String trgLevel = " ";
						IndividualReport nextTLT = reportInput.getCurrentTLT();
						if (nextTLT.getDisplayData().get("TARGET_LEVEL").equalsIgnoreCase("10")) {
							trgLevel = "SEA";

						} else if (nextTLT.getDisplayData().get("TARGET_LEVEL").equalsIgnoreCase("9")) {
							trgLevel = "CEO";

						} else if (nextTLT.getDisplayData().get("TARGET_LEVEL").equalsIgnoreCase("8")) {
							trgLevel = "BUL";

						} else if (nextTLT.getDisplayData().get("TARGET_LEVEL").equalsIgnoreCase("7")) {
							trgLevel = "MLL";
						}
						String fileName = "";
						fileName = StringUtils.removeFilenameSpecialCharacters(
								reportInput.getParticipantNameInverse() + " " + epi.getProject().getProjectTypeName()
										+ " " + trgLevel + " " + ret.getReportCode() + " "
										+ new SimpleDateFormat("yyyyMMdd").format(new Date()) + " " + langCode);
						ret.setName(fileName.replace(" ", "_") + ".xls");
						fileName = StringUtils.removeFilenameSpecialCharacters(
								epi.getProject().getClient().getName() + " " + epi.getProject().getProjectTypeName()
										+ " " + trgLevel + " " + ret.getReportCode() + " "
										+ new SimpleDateFormat("yyyyMMdd").format(new Date()) + " " + langCode);
						gr.setName(fileName.replace(" ", "_") + ".xls");
						// ret.setName(epi.getParticipant().getFirstName() + "_"
						// + epi.getParticipant().getLastName() + "_" +
						// reportInput.getDnaName() + "_" +
						// reportInput.getClientName() + "_" +
						// ret.getReportCode()+ ".xls");

						gr.getDisplayData().put("ClientName", reportInput.getClientName());

					} // End of if loopCounter == 0
				} // End of extract additional processing

				// Done with the data... Do the break logic, if needed
				if (addBreaks) {
					// Loop through displsyData and do break enhancement
					for (Iterator<Map.Entry<String, String>> itr = ret.getDisplayData().entrySet().iterator(); itr
							.hasNext();) {
						// get the next entry
						Map.Entry<String, String> ent = itr.next();

						// get the data
						String key = ent.getKey();
						String txt = ent.getValue();
						// massage it
						txt = StringUtils.formatNonSpacingTxt(txt);
						ret.getDisplayData().put(key, txt);
					}
				}

				// Also add the tgs to ret
				ret.getLabelData().putAll(lblMap);

				// add the individual report to the reporting request but only
				// if not an extract
				if (!(rptCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_EXTRACT)
						|| rptCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_ANALYTICS_EXTRACT)
						|| rptCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_RAW_EXTRACT))) {
					// System.out.println(" add the individual report to the
					// reporting Request.... ");
					reportingRequest.addReport(ret);
				}

				loopCounter++;
			} // end main epi for loop

			if (rptCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_EXTRACT)
					|| rptCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_ANALYTICS_EXTRACT)
					|| rptCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_RAW_EXTRACT)) {
				// Skip the PDA stuff if Raw Extract
				if (!rptCode.equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_RAW_EXTRACT)) {
					// Do the PDA stuff (A by D & Analytics)
					ret = (new PdaDataHelper()).populatePdaData(ret, pptId, projId, langCode);
				}

				// add individual report (with all the instrument data) to group
				// report
				gr.getIndividualReports().add(ret);
				// send it off to be rendered.....
				if (gr.getIndividualReports().size() > 0) {
					// System.out.println(" add the individual report to the
					// group report... add the group report to the reporting
					// request...");
					groupReports.put(ret.getReportCode(), gr);
					reportingRequest.addReport(gr);
				}
			}
		} catch (Exception e) {
			System.out.println("doAbyDSpecificReportTasks:");
			e.printStackTrace();
		} finally {
			try {
				if (con != null) {
					con.close();
				}

			} catch (Exception se) {
				se.printStackTrace();
			}
		}
		log.debug("Finished Get Abyd Report Data");
	}

	/**
	 * scoreWGEGraphicalRpt is a method that scores data for the WATSON GLASER E
	 * graphical report
	 *
	 * it uses the scoring code from the do method, and makes it available to
	 * the extract report.
	 *
	 * @param ReportData
	 *            rd
	 * @param ProjectParticipantInstrument
	 *            epi
	 * @param SessionUser
	 *            sessionUser
	 */
	private void scoreWGEForExtract(IndividualReport ret, ProjectParticipantInstrument epi) {

		// System.out.println("scoreWGEForExtract...");
		try {
			ReportData rd = new ReportData();

			// get the normed score objects....
			HashMap<String, NormScore> gpPercentiles = epi.getScoreGroup().calculateGeneralPopulation();
			HashMap<String, NormScore> spPercentiles = epi.getScoreGroup().calculateSpecialPopulation();

			// get ratings from the NormScore objects

			// RDS Key=WGE_CT_GP_RATING
			Iterator<NormScore> iter = gpPercentiles.values().iterator();
			while (iter.hasNext()) {
				NormScore ns = iter.next();
				rd.getScoreData().put(ns.getNorm().getSpssValue() + "_GP_RATING", ns.toRating().toString());
			}

			// RDS Key=WGE_CT_GP_RATING
			iter = spPercentiles.values().iterator();
			while (iter.hasNext()) {
				NormScore ns = iter.next();
				rd.getScoreData().put(ns.getNorm().getSpssValue() + "_SP_RATING", ns.toRating().toString());
			}

			// Norm Names
			rd.getScoreData().put("WGE_CT_GP_NAME", epi.getScoreGroup().getGeneralPopulation().getName());
			rd.getScoreData().put("WGE_CT_SP_NAME", epi.getScoreGroup().getSpecialPopulation().getName());

			// raw scores / scale names
			Iterator<Score> iterScore = epi.getScoreGroup().getScores().values().iterator();
			while (iterScore.hasNext()) {
				Score score = iterScore.next();
				rd.getScoreData().put(score.getCode(), score.getRawScore());
				rd.getDisplayData().put(score.getCode() + "_SCALE_NAME", score.getScaleName());
			}

			// add to report data for individual report
			ret.getReportData().put(AbyDExtractFormatHelper.INST_CODE_TO_RC_NAME.get(epi.getInstrument().getCode()),
					rd);
		} catch (Exception e) {
			System.out.println("scoreWGEForExtract:");
			e.printStackTrace();
		}
	}

	/**
	 * scoreFinExForExtract is a method that handles data for the FIN-EX detail
	 * report
	 *
	 * it uses the scoring code from the do method, and makes it available to
	 * the extract report.
	 *
	 * @param IndividualReport
	 *            ret
	 * @param ProjectParticipantInstrument
	 *            epi
	 */
	// ######################################
	// NOTE: this has been pulled from the current extracts. 1/06/12 THIS IS NOT
	// USED!
	// ######################################
	private void scoreFinExForExtract(IndividualReport ret, ProjectParticipantInstrument epi) {
		// System.out.println("doFinexDetailRpt...");

		try {
			ReportData rd = new ReportData();

			// CALC number correct
			BigDecimal numCorrect = new BigDecimal(epi.getScoreGroup().getScores().get("FE_CORRECT").getRawScore()); // this
																														// comes
																														// back
																														// as
																														// 6.0000
			rd.getScoreData().put("NUM_CORRECT", numCorrect.stripTrailingZeros().toString());

			// CALC percent correct
			BigDecimal percentCorrect = new BigDecimal(new Float(((numCorrect.floatValue() / 13) * 100) + .5));
			int intPercent = percentCorrect.intValue();
			rd.getScoreData().put("PERCENT_CORRECT", new Integer(intPercent).toString());

			// CALC pdi rating
			BigDecimal mpd = new BigDecimal(epi.getScoreGroup().getScores().get("FE_MPD").getRawScore());
			BigDecimal afc = new BigDecimal(epi.getScoreGroup().getScores().get("FE_AFC").getRawScore());
			BigDecimal dam = new BigDecimal(epi.getScoreGroup().getScores().get("FE_DAM").getRawScore());
			BigDecimal dac = new BigDecimal(epi.getScoreGroup().getScores().get("FE_DAC").getRawScore());

			BigDecimal pdiRating = mpd.add(afc);
			BigDecimal pdiRating2 = pdiRating.add(dam);
			BigDecimal pdiRating3 = pdiRating2.add(dac);
			pdiRating = pdiRating3.divide(new BigDecimal(4));
			double pdiRating4 = Math.floor(((pdiRating.doubleValue() * 2) + .5)) / 2;
			rd.getScoreData().put("PDI_RATING", new Double(pdiRating4).toString());

			// NOTE: NO NORMS FOR FIN-EX

			// raw scores / scale names
			Iterator<Score> iterScore = epi.getScoreGroup().getScores().values().iterator();
			while (iterScore.hasNext()) {
				Score score = iterScore.next();
				rd.getScoreData().put(score.getCode(), score.getRawScore());
				rd.getDisplayData().put(score.getCode() + "_SCALE_NAME", score.getScaleName());
			}

			// add to report data for individual report
			ret.getReportData().put(AbyDExtractFormatHelper.INST_CODE_TO_RC_NAME.get(epi.getInstrument().getCode()),
					rd);
		} catch (Exception e) {
			System.out.println("scoreFinExForExtract:");
			e.printStackTrace();
		}
	}

	/**
	 * scoreGPIForExtract is a method that handles scoring for the extract data
	 * for the GPI graphical report
	 *
	 * it uses the scoring code from the do method, and makes it available to
	 * the extract report.
	 *
	 * @param IndividualReport
	 *            ret
	 * @param ProjectParticipantInstrument
	 *            epi
	 */
	private void scoreGPIForExtract(IndividualReport ret, ProjectParticipantInstrument epi) {
		// System.out.println("scoreGPIForExtract...");

		try {
			ReportData rd = new ReportData();

			// get the normed score objects for the dimensions....
			HashMap<String, NormScore> gpPercentiles = epi.getScoreGroup().calculateGeneralPopulation();
			HashMap<String, NormScore> spPercentiles = epi.getScoreGroup().calculateSpecialPopulation();

			// get ratings from the NormScore objects

			// RDS Key= _GP_RATING
			Iterator<NormScore> iter = gpPercentiles.values().iterator();
			while (iter.hasNext()) {
				NormScore ns = iter.next();
				rd.getScoreData().put(ns.getNorm().getSpssValue() + "_GP_RATING", ns.toRating().toString());
			}

			// RDS Key= _GP_RATING
			iter = spPercentiles.values().iterator();
			while (iter.hasNext()) {
				NormScore ns = iter.next();
				rd.getScoreData().put(ns.getNorm().getSpssValue() + "_SP_RATING", ns.toRating().toString());
			}

			// Norm Names
			rd.getScoreData().put("GPI_GP_NAME", epi.getScoreGroup().getGeneralPopulation().getName());
			rd.getScoreData().put("GPI_SP_NAME", epi.getScoreGroup().getSpecialPopulation().getName());

			// raw scores / scale names
			Iterator<Score> iterScore = epi.getScoreGroup().getScores().values().iterator();
			while (iterScore.hasNext()) {
				Score score = iterScore.next();
				rd.getScoreData().put(score.getCode(), score.getRawScore());
				rd.getDisplayData().put(score.getCode() + "_SCALE_NAME", score.getScaleName());
			}

			// set rdi data
			if (epi.getScoreGroup().getScores().get("PGPI_RDI") != null) {
				// System.out.println("pi.getScoreGroup().getScores().get(PGPI_RDI).getRawScore()
				// " +
				// epi.getScoreGroup().getScores().get("PGPI_RDI").getRawScore());
				String rdi = epi.getScoreGroup().getScores().get("PGPI_RDI").getRawScore();
				float rdiFloat = Float.parseFloat(rdi); // convert string to
														// float
				DecimalFormat rdiRound = new DecimalFormat("0");
				// System.out.println(grade.format(rdiFloat)); // round to
				// nearest whole number
				rd.addScoreData("PGPI_RDI", rdiRound.format(rdiFloat));
			} else {
				rd.addScoreData("PGPI_RDI", "");
			}

			// add to report data for individual report
			ret.getReportData().put(AbyDExtractFormatHelper.INST_CODE_TO_RC_NAME.get(epi.getInstrument().getCode()),
					rd);
		} catch (Exception e) {
			System.out.println("scoreGPIForExtract:");
			e.printStackTrace();
		}
	}

	/**
	 * scoreLEIForExtract is a method that scores LEI data for the AbyD Extract
	 * report
	 *
	 * This uses code from the doLEI.. method
	 *
	 * @param IndividualReport
	 *            ret
	 * @param ProjectParticipantInstrument
	 *            epi
	 */
	private void scoreLEIForExtract(IndividualReport ret, ProjectParticipantInstrument epi) {
		// System.out.println("scoreLEIForExtract...");
		try {
			ReportData rd = new ReportData();

			// get the normed score objects for the dimensions....
			HashMap<String, NormScore> gpPercentiles = epi.getScoreGroup().calculateGeneralPopulation();
			HashMap<String, NormScore> spPercentiles = epi.getScoreGroup().calculateSpecialPopulation();

			// get ratings from the NormScore objects

			// RDS Key= _GP_RATING
			Iterator<NormScore> iter = gpPercentiles.values().iterator();
			while (iter.hasNext()) {
				NormScore ns = iter.next();
				rd.getScoreData().put(ns.getNorm().getSpssValue() + "_GP_RATING", ns.toRating().toString());
			}

			// RDS Key= _GP_RATING
			iter = spPercentiles.values().iterator();
			while (iter.hasNext()) {
				NormScore ns = iter.next();
				rd.getScoreData().put(ns.getNorm().getSpssValue() + "_SP_RATING", ns.toRating().toString());
			}

			// Norm Names
			rd.getScoreData().put("LEI_GP_NAME", epi.getScoreGroup().getGeneralPopulation().getName());
			rd.getScoreData().put("LEI_SP_NAME", epi.getScoreGroup().getSpecialPopulation().getName());

			// raw scores / scale names
			Iterator<Score> iterScore = epi.getScoreGroup().getScores().values().iterator();
			while (iterScore.hasNext()) {
				Score score = iterScore.next();
				rd.getScoreData().put(score.getCode(), score.getRawScore());
				rd.getDisplayData().put(score.getCode() + "_SCALE_NAME", score.getScaleName());
			}

			// add to report data for individual report
			ret.getReportData().put(AbyDExtractFormatHelper.INST_CODE_TO_RC_NAME.get(epi.getInstrument().getCode()),
					rd);
		} catch (Exception e) {
			System.out.println("scoreLEIForExtract:");
			e.printStackTrace();
		}
	}

	/**
	 * scoreRAVBForExtract is a method that scores RAVEN'S data for the Abyd
	 * extract report
	 *
	 * @param IndividualReport
	 *            ret
	 * @param ProjectParticipantInstrument
	 *            epi
	 */
	private void scoreRAVBForExtract(IndividualReport ret, ProjectParticipantInstrument epi) {
		// System.out.println("scoreRAVBForExtract...");

		try {
			ReportData rd = new ReportData();

			// get the normed score objects for the dimensions....
			HashMap<String, NormScore> gpPercentiles = epi.getScoreGroup().calculateGeneralPopulation();
			HashMap<String, NormScore> spPercentiles = epi.getScoreGroup().calculateSpecialPopulation();

			// get ratings from the NormScore objects

			// RDS Key= _GP_RATING
			Iterator<NormScore> iter = gpPercentiles.values().iterator();
			while (iter.hasNext()) {
				NormScore ns = iter.next();
				rd.getScoreData().put(ns.getNorm().getSpssValue() + "_GP_RATING", ns.toRating().toString());
			}

			// RDS Key= _GP_RATING
			iter = spPercentiles.values().iterator();
			while (iter.hasNext()) {
				NormScore ns = iter.next();
				rd.getScoreData().put(ns.getNorm().getSpssValue() + "_SP_RATING", ns.toRating().toString());
			}

			// Norm Names
			rd.getScoreData().put("RAVENSB_GP_NAME", epi.getScoreGroup().getGeneralPopulation().getName());
			rd.getScoreData().put("RAVENSB_SP_NAME", epi.getScoreGroup().getSpecialPopulation().getName());

			// raw scores / scale names
			Iterator<Score> iterScore = epi.getScoreGroup().getScores().values().iterator();
			while (iterScore.hasNext()) {
				Score score = iterScore.next();
				rd.getScoreData().put(score.getCode(), score.getRawScore());
				rd.getDisplayData().put(score.getCode() + "_SCALE_NAME", score.getScaleName());
			}

			// add to report data for individual report
			ret.getReportData().put(AbyDExtractFormatHelper.INST_CODE_TO_RC_NAME.get(epi.getInstrument().getCode()),
					rd);
		} catch (Exception e) {
			System.out.println("scoreRAVBForExtract:");
			e.printStackTrace();
		}
	}

	/**
	 * scoreCSForExtract is a method that scores CAREER SURVEY data for the AbyD
	 * extract report
	 *
	 * @param IndividualReport
	 *            ret
	 * @param ProjectParticipantInstrument
	 *            epi
	 */
	private void scoreCSForExtract(IndividualReport ret, ProjectParticipantInstrument epi) {
		// System.out.println("scoreCSForExtract...");

		try {
			ReportData rd = new ReportData();

			// get the normed score objects for the dimensions....
			HashMap<String, NormScore> gpPercentiles = epi.getScoreGroup().calculateGeneralPopulation();
			HashMap<String, NormScore> spPercentiles = epi.getScoreGroup().calculateSpecialPopulation();

			// get ratings from the NormScore objects

			// RDS Key= _GP_RATING
			Iterator<NormScore> iter = gpPercentiles.values().iterator();
			while (iter.hasNext()) {
				NormScore ns = iter.next();
				rd.getScoreData().put(ns.getNorm().getSpssValue() + "_GP_RATING", ns.toRating().toString());
			}

			// RDS Key= _GP_RATING
			iter = spPercentiles.values().iterator();
			while (iter.hasNext()) {
				NormScore ns = iter.next();
				rd.getScoreData().put(ns.getNorm().getSpssValue() + "_SP_RATING", ns.toRating().toString());
			}

			// Norm Names
			rd.getScoreData().put("CAR_GP_NAME", epi.getScoreGroup().getGeneralPopulation().getName());
			rd.getScoreData().put("CAR_SP_NAME", epi.getScoreGroup().getSpecialPopulation().getName());

			// raw scores / scale names
			Iterator<Score> iterScore = epi.getScoreGroup().getScores().values().iterator();
			while (iterScore.hasNext()) {
				Score score = iterScore.next();
				rd.getScoreData().put(score.getCode(), score.getRawScore());
				rd.getDisplayData().put(score.getCode() + "_SCALE_NAME", score.getScaleName());
			}

			// add to report data for individual report
			ret.getReportData().put(AbyDExtractFormatHelper.INST_CODE_TO_RC_NAME.get(epi.getInstrument().getCode()),
					rd);

		} catch (Exception e) {
			System.out.println("scoreCSForExtract:");
			e.printStackTrace();
		}
	}
}
