package com.pdi.listener.portal.servlets;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.util.IOUtils;

import com.pdi.properties.PropertyLoader;


/**
 * A servlet wrapper that allows access to the file attachment service in PALMS
 *
 */
public class PalmsGetFileAttachmentServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	/**
	 * Required class for HttpServlet
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException
	{
		doProcess(request, response);
		return;
	}


	/**
	 * Required class for HttpServlet
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException
	{
		doProcess(request, response);
		return;
	}


	/**
	 * doProcess - Common class that does the work for both get and post (this will usually be a GET
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void doProcess(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException
	{
		URL url = null;
		URLConnection uc = null;
		InputStream inp = null;
		OutputStream out = null;
		
		response.setHeader("Expires", "0");
		response.setHeader("Cache-Control",	"must-revalidate, post-check=0, pre-check=0");
		response.setHeader("Pragma", "public");
		response.setCharacterEncoding("UTF-8");
		
		// Get the URL for the PALMS servlet
		String surl = PropertyLoader.getProperty("com.pdi.listener.portal.application", "file.attachment.url");
		surl += "?id=" + request.getParameter("id");

		try
		{
			// Create the URL
			try {
				url = new URL (surl);
			}  catch (MalformedURLException e) {
				System.out.println("Malformed URL - " + surl + ".  Msg=" + e.getMessage());
				response.setContentType("text/html");
				PrintWriter pw = response.getWriter();
				pw.print("<html><body><h1 style=\"color:red;\">Error detected.</h1>Malformed URL in PalmsGetFileAttachmentServlet(). Please contact support.<br/>URL=" + surl + "<br> msg=" + e.getMessage() + "</body></html>");
				return;
			}
	
			// open the connection and get the authorized user info
			try {
				uc = url.openConnection();
			} catch (IOException e) {
				System.out.println("Unable to open connection.  URL=" + surl + ".  Msg=" + e.getMessage());
				response.setContentType("text/html");
				PrintWriter pw = response.getWriter();
				pw.print("<html><body><h1 style=\"color:red;\">Error detected.</h1>Unable to open connection in PalmsGetFileAttachmentServlet(). Please contact support.<br>URL=" + surl + "<br> msg=" + e.getMessage() + "</body></html>");
				return;
			}
			
			// Connect to the output of the called service
			try {
				 inp = (InputStream)uc.getInputStream();
			} catch (Exception e) {
				System.out.println("Unable to open input stream from authorization connection (content).  Msg=" + e.getMessage());
				response.setContentType("text/html");
				PrintWriter pw = response.getWriter();
				pw.print("<html><body><h1 style=\"color:red;\">Error detected.</h1>Unable to fetch data in PalmsGetFileAttachmentServlet(). Please contact support.<br> Is the appropriate server running?<br/> msg=" + e.getMessage() + "</body></html>");
				return;
			}
	
			// Move across the headers
			Map<String, List<String>> hdrs = uc.getHeaderFields();
			for(Map.Entry<String, List<String>> hdr : hdrs.entrySet())
			{
				String key = hdr.getKey();
				if (key == null)
					continue;
				if(! key.equalsIgnoreCase("Transfer-Encoding"))
				{
					// Gen the value
					String hdrVal = "";
					for(String val : hdr.getValue())
					{
						if(hdrVal.length() > 0){ hdrVal += ",";}
						hdrVal += val;
					}
					response.setHeader(key, hdrVal);
				}
			}
	
			out = response.getOutputStream();
			IOUtils.copy(inp, out);
	
		}
		finally
		{
			if (inp != null)
			{
				inp.close();
			}
			if (out != null)
			{
				out.flush();
				out.close();
			}
		}
		
		return;
	}
}
