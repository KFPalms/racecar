/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.listener.abyd.intGrid;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdi.data.abyd.dto.common.DNACellDTO;
import com.pdi.data.abyd.dto.common.StatusDTO;
import com.pdi.data.abyd.dto.intGrid.IGInstDisplayData;
import com.pdi.data.abyd.dto.intGrid.IGCompDataDTO;
import com.pdi.data.abyd.dto.intGrid.IGExtractDataDTO;
import com.pdi.data.abyd.dto.intGrid.IGFinalScoreDTO;
import com.pdi.data.abyd.dto.intGrid.IGLcNameDTO;
import com.pdi.data.abyd.dto.intGrid.IGSummaryDataDTO;
import com.pdi.data.abyd.dto.common.EGFullDataDTO;
import com.pdi.data.abyd.helpers.common.EGDataHelper;
import com.pdi.data.abyd.helpers.intGrid.IGComCheckHelper;
import com.pdi.data.abyd.helpers.intGrid.IGCompLineDataHelper;
import com.pdi.data.abyd.helpers.intGrid.IGKf4dDataHelper;
import com.pdi.data.abyd.helpers.intGrid.IGSummaryDataHelper;
import com.pdi.data.abyd.helpers.intGrid.IGTestColumnDataHelper;
import com.pdi.data.abyd.helpers.intGrid.IGTestingIntersectionDataHelper;
import com.pdi.data.abyd.helpers.intGrid.SaveIGCompScoreDataHelper;
import com.pdi.data.abyd.helpers.intGrid.SaveIGLcNameDataHelper;
import com.pdi.data.abyd.helpers.intGrid.SaveWorkingNotesDataHelper;
import com.pdi.data.abyd.helpers.intGrid.SubmitIGDataHelper;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;

/**
 * IntegrationGridService provides entry point framework
 * for the Assessment-by-Design Project Integration Grid app
 *
 * @author		Ken Beukelman
  */
public class IntegrationGridService
{
	private static final Logger log = LoggerFactory.getLogger(IntegrationGridService.class);
	//
	// Static data.
	//
	static private String IG_VERSION = "IG version from Java";

	//
	// Static methods.
	//
	
	//
	// Instance data.
	//
	
	//
	// Constructors.
	//
	public IntegrationGridService()
	{
		// Does nothing currently
	}

	//
	// Instance methods.
	//

	
	/**
	 * Comm Check.  See if the remoting connection and the db are up
	 *
	 * @return A String object
	 * @throws Exception
	 */
	public String comCheck()
		throws Exception
	{
		Connection con = null;
		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();

			// get the data
			IGComCheckHelper helper = new IGComCheckHelper(con);        
			String ret = helper.comCheck();
				
			return ret;
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)  {  /* Swallow the exception */  }
				con = null;
			}
		}
	}


	/**
	 * Get version - Q & D check of remoting connection
	 * 
	 * @return The defined IG version string
	 */
	public String getVersion()
	{
		return IG_VERSION;
	}


	/**
	 * Get data for the first (summary) page of the IG.
	 * 
	 * @param lpId - A linkparams id.  The associated row has all of the required data included
	 * @return An IGSummaryDataDTO object
	 * @throws Exception
	 */
	public IGSummaryDataDTO fetchIGSummary(String lpId, boolean isStartup) throws Exception
	{
		log.debug("Fetching IG summary for: {}.  Is Startup? {}", lpId, isStartup);
		Connection con = null;
		// Connect to the database
		con = AbyDDatabaseUtils.getDBConnection();
		
		try
		{
			// get the data
			log.debug("Constructing IG Summary Data Helper object");
			IGSummaryDataHelper helper = new IGSummaryDataHelper(con, lpId, isStartup);      
			log.debug("Getting Summary Data");
			IGSummaryDataDTO ret = helper.getSummaryData();

			// Return the data
			return ret;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			// Send back the error
			return new IGSummaryDataDTO(new StatusDTO(e.getMessage()));
		}
		finally
		{
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)  {  /* Swallow the exception */  }
				con = null;
			}
		}
	}


	/**
	 * Get the intersection data for the "Testing" column on the Integration Grid when the
	 * enable/disable cogs button is pressed.  It returns an ArrayList of DNACellDTO objects
	 * 
	 * @param partId - A participant id
	 * @param dnaId - A DNA Id
	 * @param cogsOn - A flag to indicate that the results should have the cognitive
	 *                 instrument scores included ("On"/true) or not
	 * @return An ArrayList of DNACellDTO objects
	 * @throws Exception
	 */
	public ArrayList<DNACellDTO> fetchTestingScoreData(String partId, long dnaId, boolean cogsOn, boolean hasAlp, boolean hasKf4d)
		throws Exception
	{
		Connection con = null;
		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();

			// get the data
			IGTestingIntersectionDataHelper helper = new IGTestingIntersectionDataHelper(con, partId, dnaId, cogsOn, hasAlp, hasKf4d);        
			ArrayList<DNACellDTO> ret = helper.getTestingData();
				
			return ret;
		}
		catch (Exception e)
		{
			throw new Exception(e);
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)  {  /* Swallow the exception */  }
				con = null;
			}
		}
	}


	/**
	 * Get the data for a competency (a line) on the Integration Grid.
	 * 
	 * @param compId - The id of the competency in question
	 * @param partId - A participant id
	 * @param dnaId - A DNA Id
	 * @return An IGCompDataDTO object
	 * @throws Exception
	 */
	public IGCompDataDTO fetchCompRowData(long compId, String partId, long dnaId)
		throws Exception
	{
		Connection con = null;
		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();
			IGKf4dDataHelper kf4dHelper = new IGKf4dDataHelper(con, partId, dnaId);
			// get the data
			IGCompLineDataHelper helper = new IGCompLineDataHelper(con, compId, partId, dnaId, kf4dHelper);
			IGCompDataDTO ret = helper.getCompLine();
			return ret;
		}
		catch (Exception e)
		{
			throw new Exception(e);
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)  {  /* Swallow the exception */  }
				con = null;
			}
		}
	}


	/**
	 * Get the data for the "Testing" column on the Integration Grid.
	 * 
	 * @param partId - A participant ID
	 * @param dnaId - The dna Id
	 * @return A IGInstDisplayData object
	 * @throws Exception
	 */
	public IGInstDisplayData fetchTestingColumnData(String partId, long dnaId)
		throws Exception
	{
		Connection con = null;
		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();

			// get the data
			IGTestColumnDataHelper helper = new IGTestColumnDataHelper(con, partId, dnaId);
			IGInstDisplayData ret = helper.getTestingColumnData();
				
			return ret;
		}
		catch (Exception e)
		{
			throw new Exception(e);
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
			    catch (SQLException sqlEx)
			    {
			    	System.out.println("ERROR: stmt close in fetchTestingColumnData - " + sqlEx.getMessage());
			    }
				con = null;
			}
		}
 	}


	/**
	 * Extracts a subset of the required data for the Integration Grid data extract.
	 * See notes in IGExtractDataHelper for more information.
	 * 
	 * @param candId - A participant Id
	 * @param dnaId - A DNA id
	 * @return An IGExtractDataDTO
	 * @throws Exception
	 */
	public IGExtractDataDTO fetchExtractData(String candId, long dnaId)
		throws Exception
	{
		System.out.println("----> fetchExtractData no longer refernced from Anteater! This is an anomalous referece!  Return value will be an empty IGExtractDataDTO object.");
		Connection con = null;
		try
		{
			// Commented out; see output line, above
			//// Connect to the database
			//con = AbyDDatabaseUtils.getDBConnection();
			//
			//// Save the data
			//IGExtractDataHelper helper = new IGExtractDataHelper(con, candId, dnaId);        
			//IGExtractDataDTO ret =  helper.getIGExtractData();
			
			IGExtractDataDTO ret =  new IGExtractDataDTO();

			return ret;
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)  {  /* Swallow the exception */  }
				con = null;
			}
		}
	}


	/**
	 * Save the entered lead consultant name.
	 * 
	 * @param nameData - An IGLCName object
	 * @throws Exception
	 */
	public void saveLcName(IGLcNameDTO nameData)
		throws Exception
	{
		Connection con = null;
		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();

			// Save the data
			SaveIGLcNameDataHelper helper = new SaveIGLcNameDataHelper(con, nameData);        
			helper.saveLcName();
			//	
			//return;
		}
		catch (Exception e)
		{
			throw new Exception(e);
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)  {  /* Swallow the exception */  }
				con = null;
			}
		}
	}


	/**
	 * Save a consultant-entered final score (index into a picklist) and
	 * also a possible note for a competency in the IG.
	 * 
	 * @param compScoreData - An IGFinalScoreDTO object
	 * @throws Exception
	 */
	public void saveFinalScore(IGFinalScoreDTO compScoreData)
		throws Exception
	{
		Connection con = null;
		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();

			// Save the data
			SaveIGCompScoreDataHelper helper = new SaveIGCompScoreDataHelper(con, compScoreData);        
			helper.saveScore();
	
			return;
		}
		catch (Exception e)
		{
			throw new Exception(e);
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)  {  /* Swallow the exception */  }
				con = null;
			}
		}
	}


	/**
	 * Single-purpose RPC to set the "submitted" flag for this Integration grid.
	 * 
	 * @param candId - A participant Id
	 * @param dnaId - A DNA id
	 * @throws Exception
	 */
	public void submitIG(String candId, long dnaId)
		throws Exception
	{
		Connection con = null;
		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();

			// Save the data
			SubmitIGDataHelper helper = new SubmitIGDataHelper(con, candId, dnaId);        
			helper.submitIG();
	
			return;
		}
		catch (Exception e)
		{
			throw new Exception(e);
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)  {  /* Swallow the exception */  }
				con = null;
			}
		}
	}


	/**
	 * Single-purpose RPC to save the working notes for this Integration grid.
	 * 
	 * @param candId - A V2 participant Id
	 * @param dnaId - A DNA id
	 * @throws Exception
	 */
	public void saveWorkingNotes(String candId, long dnaId, String notes)
		throws Exception
	{
		Connection con = null;
		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();

			// Save the data
			SaveWorkingNotesDataHelper helper = new SaveWorkingNotesDataHelper(con, candId, dnaId, notes);        
			helper.saveWorkingNotes();
	
			return;
		}
		catch (Exception e)
		{
			throw new Exception(e);
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)  {  /* Swallow the exception */  }
				con = null;
			}
		}
	}


	/**
	 * Fetch the data for an Eval Guide.  This also furnishes all of the data
	 * required for displaying a column in the Integration Grid (except for the
	 * "Testing" column... see the "fetchTestingColumnData" method).
	 * 
	 * This method is redundant to the same method in the Eval Guide service but
	 * is placed here for call consistency in the Flex UI.
	 *
	 * @param partId the participant id
	 * @param dnaId the dna Id
	 * @param moduleId the module Id
	 * @return the EGFullData object
	 * @throws Exception
	 */
	public EGFullDataDTO fetchEvalGuide(String partId, long dnaId, long moduleId)
		throws Exception
	{
		Connection con = null;
		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();

			// get the data
			EGDataHelper helper = new EGDataHelper(con, partId, dnaId, moduleId);        
			EGFullDataDTO data = helper.getEGDisplayData();
				
			return data;
		}
		catch (Exception e)
		{
			throw new Exception(e);
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)  {  /* Swallow the exception */  }
				con = null;
			}
		}
	}	


//	/*
//	 * This method appears not to be used... but fetchCompRowData() is
//	 */
//		/**
//		 * Fetch all competencies
//		 * TODO: We should be calling this method
//		 * @param compIds
//		 * @param partId
//		 * @param dnaId
//		 * @return
//		 * @throws Exception
//		 */
//		public ArrayList<IGCompDataDTO> fetchAllCompRowData(ArrayList<Long> compIds, String partId, long dnaId) throws Exception {
//			ArrayList<IGCompDataDTO> igCompDataDTOs = new ArrayList<IGCompDataDTO>();
//			for(long compId : compIds) {
//				Connection con = null;
//				try
//				{
//					// Connect to the database
//					con = AbyDDatabaseUtils.getDBConnection();
	//
//					// get the data
//					IGCompLineDataHelper helper = new IGCompLineDataHelper(con, compId, partId, dnaId);
//					IGCompDataDTO ret = helper.getCompLine();
//					igCompDataDTOs.add(ret);
//				}
//				finally
//				{
//					// close the connection
//					if (con != null)
//					{
//						try  {  con.close();  }
//						catch (Exception ex)  {  /* Swallow the exception */  }
//						con = null;
//					}
//				}
//			}
//			return igCompDataDTOs;
//		}
}
