
/*
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.listener.portal.groovy 
import com.pdi.listener.portal.dao.DataAccessLayerV2.ParticipantInformation;

import com.pdi.data.abyd.dto.common.KeyValuePair;
import com.pdi.data.abyd.dto.intGrid.IGSummaryDataDTO;


import com.pdi.data.abyd.dto.setup.ReportModelCompetencyDTO;
import com.pdi.data.abyd.dto.setup.ReportModelSuperFactorDTO;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.GroupReport;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;
import com.pdi.reporting.request.ReportingRequest;
import com.pdi.reporting.response.ReportingResponse;
import com.pdi.data.abyd.dto.reportInput.ReportInputDTO;
import com.pdi.data.abyd.dto.setup.EntryStateDTO;
import com.pdi.data.abyd.dto.setup.ReportModelCompChildDTO;
import com.pdi.data.abyd.dto.setup.ReportModelStructureDTO;
import com.pdi.data.abyd.dto.setup.SPFullDataDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportLabelValueDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportLabelsDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportOptionValueDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportOptionsDTO;
import com.pdi.data.abyd.helpers.common.ImportExportDataHelper;
import com.pdi.data.abyd.helpers.intGrid.IGSummaryDataHelper;
import com.pdi.data.abyd.helpers.reportInput.ReportInputDataHelper;
import com.pdi.data.abyd.helpers.setup.EntryStateHelper;
import com.pdi.data.abyd.helpers.setup.ReportLabelsDataHelper;
import com.pdi.data.abyd.helpers.setup.ReportModelDataHelper;
import com.pdi.data.abyd.helpers.setup.ReportOptionsDataHelper;
import com.pdi.data.abyd.helpers.setup.SPDataHelper;
import com.pdi.data.v2.dto.ParticipantDTO;
import com.pdi.data.v2.dto.SessionUserDTO;
import com.pdi.data.v2.helpers.ParticipantDataHelper;
import com.pdi.listener.portal.dao.DataAccessLayerV2;
import com.pdi.listener.portal.dao.DataAccessLayerV2.ProjectInformation;
import com.pdi.properties.PropertyLoader;
import com.pdi.scoring.Norm;
import com.pdi.scoring.NormSet;
import com.pdi.string.StringUtils;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.xml.XMLUtils;

public class ABYD_INDIVIDUAL_REPORT
{ 
	private ReportingRequest reportRequest;
	private Node reportNode;
	private SessionUserDTO sessionUser;
	private DataAccessLayerV2 dataAccessLayer;

	/*
	 * generate
	 */
	public void generate()
	{
		
		//println "Top of generate()"
		String reportCode = reportNode.getAttributes().getNamedItem("code").getTextContent();
		//System.out.println("reportCode=" + reportCode);
		NodeList participants = XMLUtils.getElements(reportNode, "Participants/Participant");
		
		//println "# of part=" + participants.getLength();
		for( int j= 0; j < participants.getLength(); j++)
		{
			Node participant = participants.item(j);
			String participantId = participant.getAttributes().getNamedItem("participantId").getTextContent();
			String projectId = participant.getAttributes().getNamedItem("projectId").getTextContent();
			boolean previewReport = false;
			if(participantId == null || participantId.equalsIgnoreCase("")) {
				participantId = "";
				previewReport = true;
			}
			
			try
			{

				ProjectInformation projectInformation = dataAccessLayer.projectInformationHash.get(projectId);
				ParticipantInformation participantInformation = null;
				for(String code : dataAccessLayer.participantInformationList.keySet()) {
					ParticipantInformation pi =  dataAccessLayer.participantInformationList.get(code);
					if(pi.getParticipantId().equalsIgnoreCase(participantId) && pi.getProjectId().equalsIgnoreCase(projectId)) {
						participantInformation = pi;
						break;
					}
				}

				if(previewReport) {
					//They want to preview the report output with a fake participant
					participantInformation = dataAccessLayer.loadParticipant(
										PropertyLoader.getProperty("com.pdi.listener.portal.application", "sampleReport.projectId"), 
										PropertyLoader.getProperty("com.pdi.listener.portal.application", "sampleReport.participantId"));					
					previewReport = true;
				}
				
				//println "dnaId=" + dnaId;
				
				ReportModelStructureDTO reportModelStructure = participantInformation.getReportModelStructure();
				if(previewReport) {
					//this is a preview report, we cannot get the "participants" data because there is no participant
					reportModelStructure = projectInformation.getReportModelStructure();
				}
				Setup2ReportOptionsDTO options = projectInformation.getReportOptions();
				Setup2ReportLabelsDTO labels = projectInformation.getReportLabels();
				SPFullDataDTO spFullData = projectInformation.getSpFullData();
				EntryStateDTO entryState = projectInformation.getEntryState();
				
				ReportInputDTO reportInput = participantInformation.getReportInput();				
				IndividualReport ir = participantInformation.getIndividualReport();
				
				if(ir == null)
				{
					continue;
				}
				
				ir.getDisplayData().put("CLIENT_LOGO", projectInformation.getProject().getClient().getId()+".png");
				//println "got ir data";
				
				//Hardcode Cogs Included to 1
				ir.getDisplayData().put(ReportingConstants.RC_COGNITIVES_INCLUDED, "1");
				

				ir.setReportCode(reportCode);
				ir.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);
				
				int sp = 0;
				for(KeyValuePair kvp : spFullData.getTextElts())
				{
					ir.getDisplayData().put("SP_"+kvp.getTheKey(), kvp.getTheValue())
					//System.out.println("SP_"+kvp.getTheKey() + " = " + kvp.getTheValue());
					sp++;
				}
				
				for(Setup2ReportOptionValueDTO value : options.getOptionValuesArray())
				{
					ir.getDisplayData().put("OPTION_" + value.getOptionCode(), value.getOptionValue());
				}
				
				for(Setup2ReportLabelValueDTO value : labels.getLabelValuesArray())
				{
					if(value != null)
					{
						String svalue = value.getCustomLabel();
						if(svalue.length() < 1)
						{
							svalue = value.getLabel();
						}
						ir.getDisplayData().put("LABEL_" + value.getReportCode(), svalue);
						
						if(value.getReportCode().equals("DES_RANG")){
							ir.getDisplayData().put("DES_RANG", value.getLabel());
							//System.out.println("value.getLabel()-" + value.getLabel());
						}
						
					}
					
				}
				
				if(labels.getCoverPage().getCustomLabel() != null && labels.getCoverPage().getCustomLabel().length() > 0)
				{
					ir.getDisplayData().put("LABEL_COV_PAGE", labels.getCoverPage().getCustomLabel());
				} else {
					ir.getDisplayData().put("LABEL_COV_PAGE", labels.getCoverPage().getLabel());
				}
				
				if(labels.getLeadershipLegend().getCustomLabel() != null && labels.getLeadershipLegend().getCustomLabel().length() > 0)
				{
					ir.getDisplayData().put("LABEL_LEG_LEAD", labels.getLeadershipLegend().getCustomLabel());
				} else {
					ir.getDisplayData().put("LABEL_LEG_LEAD", labels.getLeadershipLegend().getLabel());
				}
				
				if(labels.getTltLegend().getCustomLabel() != null && labels.getTltLegend().getCustomLabel().length() > 0)
				{
					ir.getDisplayData().put("LABEL_LEG_TLT", labels.getTltLegend().getCustomLabel());
				} else {
					ir.getDisplayData().put("LABEL_LEG_TLT", labels.getTltLegend().getLabel());
				}
				
				
				ir.getDisplayData().put("MODEL_SUPERFACTORS_COUNT", reportModelStructure.getSuperFactorList().size() - 1);
				
				int i = 0;
				for(ReportModelSuperFactorDTO superfactor : reportModelStructure.getSuperFactorList())
				{
					ir.getDisplayData().put("MODEL_SUPERFACTOR_"+i+"_DISPLAY_NAME", superfactor.getDisplayName());
					ir.getDisplayData().put("MODEL_SUPERFACTOR_"+i+"_COMPETENCY_COUNT", superfactor.getCompList().size() - 1);
					int k = 0;
					for(ReportModelCompetencyDTO competency : superfactor.getCompList())
					{
						ir.getDisplayData().put("MODEL_SUPERFACTOR_"+i+"_COMPETENCY_"+k+"_DISPLAY_NAME", competency.getDisplayName());
						ir.getDisplayData().put("MODEL_SUPERFACTOR_"+i+"_COMPETENCY_"+k+"_VALUE", competency.getCompScore());
						ir.getDisplayData().put("MODEL_SUPERFACTOR_"+i+"_COMPETENCY_"+k+"_ESSENTIAL",competency.getEssential());
						
						if(competency.getChildren().size() == 1 && competency.getChildren().get(0).getDnaCompName().equalsIgnoreCase(competency.getDisplayName()))
						{
							// Just one comp, and the name is the same, nothing to do
						} else {
							ir.getDisplayData().put("MODEL_SUPERFACTOR_"+i+"_COMPETENCY_"+k+"_COUNT",competency.getChildren().size() - 1);
							int l = 0;
							for(ReportModelCompChildDTO competencyChild : competency.getChildren()) {
								ir.getDisplayData().put("MODEL_SUPERFACTOR_"+i+"_COMPETENCY_"+k+"_CHILD_"+l+"_DISPLAY_NAME", competencyChild.getDnaCompName());
								l++;
							}
						}
						k++;
					}
					i++;
				}
				
				//Display data				
				ir.getDisplayData().put("ADMIN_DATE", participantInformation.getSubmittedDate());
				ir.getDisplayData().put("PARTICIPANT_NAME", reportInput.getParticipantName());

				String fileName = StringUtils.removeSpecialCharacters(
						reportInput.getClientName() + " " +
						reportInput.getParticipantNameInverse() + " " + 
						new SimpleDateFormat("ddMMMyyyy").format(new java.util.Date()) + " ");
				
				ir.setName(fileName + reportCode + ".pdf");
				
				ir.getDisplayData().put("CLIENT_NAME", reportInput.getClientName());
				ir.getDisplayData().put("ORGANIZATION", reportInput.getClientName());
				ir.getDisplayData().put("PROJECT_NAME", reportInput.getDnaName());	// Use the DNA name as the project name on A by D data
				////println "reportInput: proj=" + reportInput.getProjectName() + ", dna=" + reportInput.getDnaName();
				////println "PROJECT_NAME=" + ir.getDisplayData().get("PROJECT_NAME");
												
				java.util.Date date = new java.util.Date();
				SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
				ir.getDisplayData().put("TODAY_DATE", sdf.format(date));

				ir.getDisplayData().put("STATUS", reportInput.getStatusId());
				
				ir.getDisplayData().put("READINESS_FOCUS", reportInput.getReadinessFocus());
				ir.getDisplayData().put("DERAIL_ORG_TXT",reportInput.getDerailmentOrgText());
				ir.getDisplayData().put("DERAIL_PRT_TXT",reportInput.getDerailmentPartText());
				ir.getDisplayData().put("DEV_PRT_TXT", reportInput.getDevelopmentPartText());
				ir.getDisplayData().put("FIT_ORG_TXT", reportInput.getFitOrgText());
				ir.getDisplayData().put("PIV_PRT_TXT", reportInput.getPivotalPartText());
				
				ir.getDisplayData().put("SKL_LEV_ORG_TXT", reportInput.getSkillsToLeverageOrgText());
				ir.getDisplayData().put("SKL_LEV_PRT_TXT", reportInput.getSkillsToLeveragePartText());
				
				ir.getDisplayData().put("SKL_DEV_ORG_TXT", reportInput.getSkillsToDevelopOrgText());
				ir.getDisplayData().put("SKL_DEV_PRT_TXT", reportInput.getSkillsToDevelopPartText());
				
				ir.getDisplayData().put("RED_RATING", reportInput.getReadinessRating());			
				ir.getDisplayData().put("RED_ORG_TXT", reportInput.getReadinessOrgText());
				
				ir.getDisplayData().put("IDP_DOC", reportInput.getIdpDocumentPath());
				ir.getDisplayData().put("IDP_IMG", reportInput.getIdpImagePath());
				ir.getDisplayData().put("LEADER_EXP_ORG_TXT", reportInput.getLeadershipExperienceOrgText());
				ir.getDisplayData().put("LEADER_EXP_PRT_TXT", reportInput.getLeadershipExperiencePartText());
				ir.getDisplayData().put("LEADER_INT_ORG_TXT", reportInput.getLeadershipInterestOrgText());
				ir.getDisplayData().put("LEADER_INT_PRT_TXT", reportInput.getLeadershipInterestPartText());
				ir.getDisplayData().put("LEADER_SKL_ORG_TXT", reportInput.getLeadershipSkillOrgText());
				ir.getDisplayData().put("LEADER_SKL_PRT_TXT", reportInput.getLeadershipSkillPartText());
				ir.getDisplayData().put("LEADER_STY_ORG_TXT", reportInput.getLeadershipStyleOrgText());
				ir.getDisplayData().put("LEADER_STY_PRT_TXT", reportInput.getLeadershipStylePartText());
				ir.getDisplayData().put("LONGTERM_ORG_TXT", reportInput.getLongtermOrgText());
				if(reportInput.getIdpImagePath() != null && reportInput.getIdpImagePath().length() > 0)
				{
					ir.getDisplayData().put("IDP_IMAGE", "http://localhost:8080/pdi-web/upload/" +  reportInput.getIdpImagePath());
				} else {
					ir.getDisplayData().put("IDP_IMAGE", "");
				}
				
				//Scripted data
				ir.getScriptedData().put("PIV_PRT_TXT", reportInput.getPivotalPartText());
				ir.getScriptedData().put("CAN_FIT_IDX", reportInput.getCandidateFitIndex().toString());
				ir.getScriptedData().put("DERAIL_RATING", reportInput.getDerailmentRating().toString());
				ir.getScriptedData().put("FIT_RATING", reportInput.getFitRecommendedRating().toString());
				ir.getScriptedData().put("LONGTERM_ADV_POT", reportInput.getLongtermRating().toString());
				ir.getScriptedData().put("LEADER_EXP_RATING", reportInput.getLeadershipExperienceRating().toString());
				ir.getScriptedData().put("LEADER_INT_RATING", reportInput.getLeadershipInterestRating().toString());
				ir.getScriptedData().put("LEADER_SKL_IDX", reportInput.getLeadershipSkillIndex().toString());
				ir.getScriptedData().put("LEADER_SKL_RATING", reportInput.getLeadershipSkillRating().toString());
				ir.getScriptedData().put("LEADER_STY_RATING", reportInput.getLeadershipStyleRating().toString());
				
				
				if(previewReport) {
					Connection con = AbyDDatabaseUtils.getDBConnection();
					try{
					EntryStateHelper entryStateHelper = new EntryStateHelper(con, projectId);        
					entryState = entryStateHelper.getEntryStateData();
					ir.getDisplayData().put("CLIENT_NAME", entryState.getClientName());
					ir.getDisplayData().put("ORGANIZATION", entryState.getClientName());
					ir.getDisplayData().put("PROJECT_NAME", entryState.getJobName());
					ir.getDisplayData().put("STATUS", "1");
					} catch (Exception e) {
						
					} finally {
						if(con != null)  {
							try { con.close() } catch (Exception e) {}
						}
					}
				}
				

				reportRequest.addReport(ir);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}

		}
	}
}