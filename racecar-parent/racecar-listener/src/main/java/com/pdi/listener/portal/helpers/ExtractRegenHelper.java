package com.pdi.listener.portal.helpers;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import com.pdi.data.abyd.dto.reportInput.ReportInputDTO;
import com.pdi.data.abyd.helpers.reportInput.ReportInputDataHelper;
import com.pdi.data.abyd.helpers.reportInput.ResearchExtractHelper;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;



/**
 * ExtractRegen regenerates the xml for an extract candidate.  It uses the existing XML generation
 * 	code call to do so and wraps it in logic that:
 * 		-- Reads a table, populated externally, that contains participants for whom to regenerate XML
 * 		-- Verifies that data exists for the participant
 * 		-- Recreates the XML
 * 		-- Updates the row in the extract candidate table, modifying only the XML, lastDate and lastUser columns
 * 	If precursor data does not exist, the row is ignored and a message is generated.
 * 	If the precursor data does exist and the RDI status is correct but the row does not exist, a new row is generated.
 */
public class ExtractRegenHelper {
	private static final Logger log = LoggerFactory.getLogger(ExtractRegenHelper.class);
	//
	// Static variables
	//
	private static final int RPP_ERROR = -1;
//	private static final int RPP_CAND = 0;
	private static final int RPP_DONE = 1;


	//
	// Constructors
	//
	public ExtractRegenHelper()
	{
		// Placeholder for now
	}


	//
	// Instance variables
	//
	private String REGEN_USER = "regen";


	//
	// Instance methods
	//

	/**
	 * regen - Controller class for the regeneration process
	 * @param doc
	 * @param response
	 */
	public void regen(Document doc, HttpServletResponse response)
	{
		boolean isOK = true;
		ArrayList<String> resp = new ArrayList<String>();
		
		resp.add("DEBUG:  Starting...");
		// Get a list of Extract candidates
		ArrayList<KeyData> keys = null;
		try
		{
			keys = getCandList();
		}
		catch (Exception e)
		{
			isOK = false;
			resp.add("ERROR:  Exception in getCandList()... Msg=" + e.getMessage());
		}
		
		if(isOK)
		{
			int size = keys.size();
			if (size < 1)
			{
				resp.add(" INFO:  No data to regenerate.");
				isOK = false;
			}
		}
		
		int cnt = 0;
		int errCnt = 0;
		if(isOK)
		{
			resp.add("DEBUG:  Found " + keys.size() + " candidate(s)");
			// Loop through the results
			for (KeyData dat : keys)
			{
				cnt++;
				if (dat.getDnaId() == 0)	// Null value returns a 0 in a long
				{
					errCnt++;
					String msg = "No DNA data available for target row.";
					resp.add(" INFO:  Key=" + dat.getPkey() + ".  Skipped.  " + msg);
					try
					{
						updateTargetTable(dat.getPkey(), RPP_ERROR, msg);
					}
					catch (Exception e)
					{
						isOK = false;
						String err = "ERROR:  Key=" + dat.getPkey() + ".  Skipped.  Exception in updateTargetTable:  Msg=" + e.getMessage();
						resp.add(err);
						break;
					}
					continue;
				}

				try
				{
					if (! processRow(dat, resp))
					{
						// bump Error count... No need to skip any processing because this is the bottom of the loop
						errCnt++;
					}
				}
				catch (Exception e)
				{
					isOK = false;
					resp.add("ERROR:  Key=" + dat.getPkey() + ".  Exception in processRow():  Msg=" + e.getMessage());
					break;
				}
			}
		}

		resp.add("DEBUG:  Done!  cnt=" + cnt + ", errCnt=" + errCnt);
		
		fillResp(resp, response);
		
		return;
	}


	/**
	 * getCandList - Retrieves the canidate data from the regen_project_participant table
	 * @return - A list of canidates from the table.  Data contained in a private class
	 * @throws Exception
	 */
	private ArrayList<KeyData> getCandList()
			throws Exception
	{
		DataResult dr = null;
		ArrayList<KeyData> ret = new ArrayList<KeyData>();

		try
		{
			StringBuffer query = new StringBuffer();
	
			query.append("SELECT rpp.projectParticipantId, ");
			query.append("       rpp.projectId, ");
			query.append("       rpp.participantId, ");
			query.append("       dna.dnaId ");
			query.append("  FROM regen_project_participant rpp ");
			query.append("    LEFT JOIN pdi_abd_dna dna ON dna.projectId = rpp.projectId ");
			query.append("  WHERE rpp.status = 0");
	
			DataLayerPreparedStatement dlps =  AbyDDatabaseUtils.prepareStatement(query);
			if (dlps.isInError())
			{
				throw new Exception("Error preparing getCandList.  Code=" + 
							dlps.getStatus().getExceptionCode() + 
							", Msg=" + dlps.getStatus().getExceptionMessage());
			}
		
			dr =  AbyDDatabaseUtils.select(dlps);
			
			// Check for data
			if (dr.hasNoData())
			{
				return ret;
			}
			// Check for errors
			if (dr.isInError())
			{
				throw new Exception("Error fetching key data.  Code=" + 
						dlps.getStatus().getExceptionCode() + 
						", Msg=" + dlps.getStatus().getExceptionMessage());
			}
			// Process the data
			ResultSet rs = dr.getResultSet();
			while(rs.next())
			{
				long pKey = rs.getLong("projectParticipantId");
				String ppt = rs.getString("participantId");
				long dna = rs.getLong("dnaId");
				long proj = rs.getLong("projectId");
	
				KeyData kd = new KeyData(pKey, ppt, dna, proj);
				ret.add(kd);
			}
	
			return ret;
		}
		finally
		{
			if (dr != null)
				dr.close();
		}
	}


	/**
	 * processRow - Processes the data for one row of the candidate table
	 * @param dat - A KeyData object containing the requisite data for one candidtate from the table
	 * @param resp - The status string array
	 * @return - Success status
	 * @throws Exception
	 */
	private boolean processRow(KeyData dat, ArrayList<String> resp)
		throws Exception
	{
		long pKey = dat.getPkey();
		String ppt = dat.getPartId();
		long dna = dat.getDnaId();

		// Get the Report Input DTO
		ReportInputDataHelper helper = new ReportInputDataHelper();
		ReportInputDTO ri = null;
		// See if the Report Input DTO exists
		if (! doesRiExist(ppt, dna))
		{
			// update the table and scram
			String msg = "Either no ReportInput row or multiple rows are present.";
			//updateTargetTable(pKey, RPP_ERROR, msg);
			//resp.add(" INFO:  Key=" + pKey + ".   Skipped.  " + msg);
			//return false;
			ri = new ReportInputDTO();
			ri.setParticipantId(ppt);
			ri.setDnaId(dna);
			helper.addReportInput(ri);
		}
		
		
		try
		{
			ri = helper.getReportInput(ppt, dna);
			
		}
		catch (Exception e)
		{
			String msg = "Exception in getReportInput().  Msg=" + e.getMessage();
			updateTargetTable(pKey, RPP_ERROR, msg);
			resp.add("ERROR:  Key=" + pKey + ".   Skipped.  " + msg);
			return false;
		}
		
		// Create the XML
		String xml = "";
		try
		{
			xml = ResearchExtractHelper.createExtractXML(ri);
			helper.updateReportInputLastDate(ri.getParticipantId(), ri.getDnaId());

		}
		catch (Exception e)
		{
			String msg = "Exception in ResearchExtractHelper.createExtractXML().  Msg=" + e.getMessage();
			updateTargetTable(pKey, RPP_ERROR, msg);
			resp.add("ERROR:  Key=" + pKey + ".   Skipped.  " + msg);
			return false;
		}
		
		// Update/create extract row
		try
		{
			writeExtractRow(dat, xml, resp);
		}
		catch (Exception e)
		{
			String msg = "Exception in writeExtractRow().  Msg=" + e.getMessage();
			updateTargetTable(pKey, RPP_ERROR, msg);
			resp.add("ERROR:  Key=" + pKey + ".   Skipped.  " + msg);
			return false;
		}

		updateTargetTable(pKey, RPP_DONE, "OK");
		resp.add(" INFO:  Key=" + pKey + ".  OK");
		return true;
	}


	/**
	 * doesRiExist - Makes a query to see if the relevant ReportInput data exists
	 * @param ppt - Participant ID
	 * @param dna - DNA ID
	 * @return - Existence status
	 * @throws Exception
	 */
	private boolean doesRiExist(String ppt, long dna)
		throws Exception
	{
		boolean ret = true;
		DataResult dr = null;

		StringBuffer query = new StringBuffer();
		query.append("SELECT COUNT(*) AS cnt");
		query.append("  FROM pdi_abd_rpt_input ri ");
		query.append("  WHERE ri.participantId = " + ppt + " ");
		query.append("    AND ri.dnaId = " + dna);

		try
		{
			DataLayerPreparedStatement dlps =  AbyDDatabaseUtils.prepareStatement(query);
			if (dlps.isInError())
			{
				throw new Exception("Error preparing doesRiExist.  Code=" + 
							dlps.getStatus().getExceptionCode() + 
							", Msg=" + dlps.getStatus().getExceptionMessage());
			}

			dr =  AbyDDatabaseUtils.select(dlps);
			// Check for errors
			if (dr.isInError())
			{
				throw new Exception("Error reading the RI table.  Code=" + 
						dlps.getStatus().getExceptionCode() + 
						", Msg=" + dlps.getStatus().getExceptionMessage());
			}
			if (dr.hasNoData())
			{
				// This should not happen, but test for it anyway
				ret = false;
			}		

			// Get the only row
			ResultSet rs = dr.getResultSet();
			rs.next();
			int cnt = rs.getInt("cnt");
			if (cnt != 1)
			{
				ret = false;
			}
		}
		finally
		{
			if (dr != null)
				dr.close();
		}

		return ret;
	}


	/**
	 * writeExtractRow - write out the updated/new row to the extract candidate table
	 * @param dat - A KeyData with the information about the ppt/proj to be written
	 * @param xml - The XML data for that ppt/proj intersection
	 * @param resp - The status strings
	 * @throws Exception
	 */
	private void writeExtractRow(KeyData dat, String xml, ArrayList<String> resp)
		throws Exception
	{
		long pKey = dat.getPkey();
		String partId = dat.getPartId();
		long dnaId = dat.getDnaId();
		
		// See if it already exists
		boolean exists = true;	// assume that it does
		try
		{
			exists = (doesEcExist(partId, dnaId));
		}
		catch (Exception e)
		{
			String msg = "Exception in doesEcExist().  Msg=" + e.getMessage();
			updateTargetTable(pKey, RPP_ERROR, msg);
			resp.add("ERROR:  Key=" + pKey + ".   Skipped.  " + msg);
			return;
		}

		// Massage the xml
		xml = xml.replace("'", "''");	// "Escape" embedded single quotes
		StringBuffer sqlQuery = new StringBuffer();
		if (exists)
		{
			// Doing an update
			sqlQuery.append("UPDATE pdi_abd_extract_cand ");
			sqlQuery.append("  SET finalizeDate = GETDATE(), ");
			sqlQuery.append("      xmlData = '" + xml + "', ");
			sqlQuery.append("      lastUserId = '" + REGEN_USER + "', ");
			sqlQuery.append("      lastDate = GETDATE() ");
			sqlQuery.append("  WHERE participantId = '" + partId + "' ");
			sqlQuery.append("    AND dnaId = " + dnaId);
		}
		else
		{
			// It's a whole new row
			sqlQuery.append("INSERT INTO pdi_abd_extract_cand ");
			sqlQuery.append("  (participantId, dnaId, finalizeDate, ");
			sqlQuery.append("   extractStatus, xmlData, ");
			sqlQuery.append("   lastUserId, lastDate) ");
			sqlQuery.append("  VALUES('" + partId + "', " + dnaId + ", GETDATE(), ");
			sqlQuery.append("         0, '" + xml + "', '" + REGEN_USER + "', GETDATE()");
			sqlQuery.append(")");
			
		}

		// prep it
		DataLayerPreparedStatement dlps =  AbyDDatabaseUtils.prepareStatement(sqlQuery);
		if (dlps.isInError())
		{
			throw new Exception("Error preparing extract query.  Query=" + sqlQuery.toString() +
					".  Code=" + dlps.getStatus().getExceptionCode() + 
					", Msg=" + dlps.getStatus().getExceptionMessage());
		}

		// do it
		DataResult dr = null;
		try
		{
			if (exists)
			{
				dr = AbyDDatabaseUtils.update(dlps);
			}
			else
			{
				dr =  AbyDDatabaseUtils.insert(dlps);
			}
			if (dr.isInError())
			{
				throw new Exception("Error "+ (exists ? "updating" : "inserting") + " extract data.  Code=" + 
						dr.getStatus().getExceptionCode() + 
						", Msg=" + dr.getStatus().getExceptionMessage());
			}
		} finally
		{
			if (dr != null)
			{
				try  {  dr.close();  }
				catch (Exception ex)  { /* Swallow the exception */ }
			}
		}
		
		return;
	}


	/**
	 * doesEcExist -  Checks to see if the extract candidate row exists
	 * @param ppt - Ppt ID
	 * @param dna - DNA ID
	 * @return - Existence status
	 * @throws Exception
	 */
	private boolean doesEcExist(String ppt, long dna)
			throws Exception
	{
		boolean ret = true;		// Initialize for success
		DataResult dr = null;

		StringBuffer query = new StringBuffer();
		query.append("SELECT COUNT(*) AS cnt");
		query.append("  FROM pdi_abd_extract_cand ec ");
		query.append("  WHERE ec.participantId = " + ppt + " ");
		query.append("    AND ec.dnaId = " + dna);
	
		try
		{
			DataLayerPreparedStatement dlps =  AbyDDatabaseUtils.prepareStatement(query);
			if (dlps.isInError())
			{
				throw new Exception("Error preparing doesEcExist.  Code=" + 
							dlps.getStatus().getExceptionCode() + 
							", Msg=" + dlps.getStatus().getExceptionMessage());
			}
	
			dr =  AbyDDatabaseUtils.select(dlps);
			// Check for errors
			if (dr.isInError())
			{
				throw new Exception("Error reading the RI table.  Code=" + 
						dlps.getStatus().getExceptionCode() + 
						", Msg=" + dlps.getStatus().getExceptionMessage());
			}
			if (dr.hasNoData())
			{
				// This should not happen, but test for it anyway
				ret = false;
			}		
	
			// Get the only row
			ResultSet rs = dr.getResultSet();
			rs.next();
			int cnt = rs.getInt("cnt");
			if (cnt != 1)
			{
				ret = false;
			}
		}
		finally
		{
	        if (dr != null) { dr.close(); dr = null; }
		}
	
		return ret;
	}


	/**
	 * updateTargetTable - Method that updates the target table as needed
	 * @param pKey - Primary key into the table
	 * @param stat - Status to be update (-1, 0, 1)
	 * @param msg - Optional message to be written to the regen target table
	 * @throws Exception
	 */
	private void updateTargetTable(long pKey, int stat, String msg)
		throws Exception
	{
		DataResult dr = null;

		String setClause = "SET status = " + stat;
		if (! (msg == null))
		{
			msg = msg.replace("'", "\\'");
			setClause += ", comments = N'" + msg + "' ";
		}
		StringBuffer query = new StringBuffer();
		query.append("UPDATE regen_project_participant ");
		query.append(setClause);
		query.append("  WHERE projectParticipantId = " + pKey);

		try
		{
			DataLayerPreparedStatement dlps =  AbyDDatabaseUtils.prepareStatement(query);
			if (dlps.isInError())
			{
				throw new Exception("Error preparing updateTargetTable.  Code=" + 
							dlps.getStatus().getExceptionCode() + 
							", Msg=" + dlps.getStatus().getExceptionMessage());
			}
	
			dr =  AbyDDatabaseUtils.update(dlps);
			// Check for errors
			if (dr.isInError())
			{
				throw new Exception("Error updating the target table.  Code=" + 
						dlps.getStatus().getExceptionCode() + 
						", Msg=" + dlps.getStatus().getExceptionMessage());
			}
		}
		finally
		{
	        if (dr != null) { dr.close(); dr = null; }
		}

		return;
	}


	/**
	 * fillResp - Routine invoked at the end of the process to load the response object with the data about the regen run
	 * @param respList - Satus text strings
	 * @param response
	 */
	private void fillResp(ArrayList<String> respList, HttpServletResponse response)
	{
		response.setContentType("text/plain");
		PrintWriter out;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			System.out.println("ERROR:  Unable to get the response writer for ExtractRegen");
			return;
		}

		String outStr = "Extract Candidate procesing complete.  ";
		outStr += (respList.size() == 0) ? "No messages detected."
				: "Messages follow:\n";
		for (String str : respList)
		{
			outStr += str + "\n";
		}
		
		out.write(outStr);

		// scram
		return;
	}



	

	/**
	 * KeyData
	 * A private class that holds information about the regen target (key, ppt ID, proj ID, and DNA ID)
	 *
	 */
	private class KeyData
	{
		private long _key;
		private String _part;
		private long _dna;
		private long _proj;

		public KeyData(long key, String ppt, long dna, long proj)
		{
			_key = key;
			_part = ppt;
			_dna = dna;
			_proj = proj;
		}
		
		public long getPkey()
		{
			return _key;
		}
		
		public String getPartId()
		{
			return _part;
		}
		
		public long getDnaId()
		{
			return _dna;
		}
		
		public long getProjId()
		{
			return _proj;
		}
	}
}
