package com.pdi.listener.portal.flex.ut;

//import com.pdi.data.v2.dto.ManualEntryDTO;
//import com.pdi.data.v2.util.V2DatabaseUtils;
//import com.pdi.data.v2.util.V2DatabaseUtils;
import com.pdi.listener.portal.flex.ManualEntryService;
//import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

public class ManualEntryServiceUT  extends TestCase
{
	//
	// Instance variables
	//
	ManualEntryService _svc;
	
	//
	// Constructor
	//
	
	public ManualEntryServiceUT(String name)
	  	throws Exception
	{
		super(name);

	     System.out.println("Starting ManualEntryService testing...");
			
	     _svc = new ManualEntryService();
	} 

	  
	  
//	/*
//	 * testFetchLinkParamsData
//	 */
//	public void testFetchLinkParamsData()
//	{
//		UnitTestUtils.start(this);
//			
//		System.out.println("testFetchLinkParamsData not implemented...");
//				//public ManualEntryDTO fetchLinkParamsData(String linkParamId)
//			
//		UnitTestUtils.stop(this);
//	}


//	/*
//	 * testFetchLinkParamsDataForVendorService
//	 */
//	public void testFetchLinkParamsDataForVendorService()
//	{
//		UnitTestUtils.start(this);
//			
//		System.out.println("testFetchLinkParamsDataForVendorService not implemented...");
//				//public ManualEntryDTO fetchLinkParamsDataForVendorService(String linkParamId)
//
//		UnitTestUtils.stop(this);
//	}


//	/*
//	 * testSaveConstructScores
//	 */
//	public void testSaveConstructScores()
//	{
//		UnitTestUtils.start(this);
//			
//		System.out.println("testSaveConstructScores not implemented...");
//				//public String saveConstructScores(ManualEntryDTO dtoIn)
//
//		UnitTestUtils.stop(this);
//	}
//	  
//	  
//	/*
//	 * testSaveAnswerDataForVendorIntegration
//	 */
//	public void testSaveAnswerDataForVendorIntegration()
//	{
//		UnitTestUtils.start(this);
//			
//		System.out.println("testSaveAnswerDataForVendorIntegration not implemented...");
//				//public  void saveAnswerDataForVendorIntegration(ManualEntryDTO dtoIn, boolean success)
//
//		UnitTestUtils.stop(this);
//	}


//	/*
//	 * testSaveResultsDataForVendorIntegration
//	 */
//	public void testSaveResultsDataForVendorIntegration()
//	{
//		UnitTestUtils.start(this);
//			
//		System.out.println("testSaveResultsDataForVendorIntegration not implemented...");
//				//public  void saveResultsDataForVendorIntegration(String linkParamId, HashMap<String, String> scoreData)
//
//		UnitTestUtils.stop(this);
//	}
	  
	  
	/*
	 * testSaveAdaptV2Data
	 */
//	public void testCheckAdaptIdData()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//			
//		ManualEntryDTO dto = new ManualEntryDTO();
//		// Sould exist
//		//dto.setAdaptProjectId(410);
//		//dto.setAdaptUserId(966);
//		// Should Exist
//		dto.setAdaptProjectId(457);
//		dto.setAdaptUserId(1141);
//		// Should NOT Exist
//		//dto.setAdaptProjectId(425);
//		//dto.setAdaptUserId(1141);
//		// Should NOT Exist
//		//dto.setAdaptProjectId(457);
//		//dto.setAdaptUserId(966);
//		boolean stat = true;
//		dto = _svc.checkAdaptIdData(dto);
//		if(dto == null){
//			stat = false;
//		}
//		//boolean stat = _svc.checkAdaptIdData(dto);
//		
//		if (stat)
//		{
//			System.out.println("Got name:  " + dto.getAdaptFirstName() + " " + dto.getAdaptLastName());
//		}
//		else
//		{
//			System.out.println("No ADAPT data available for user " + dto.getAdaptUserId() +
//					" in engagement " + dto.getAdaptProjectId());
//		}
//		
//		
//		UnitTestUtils.stop(this);
//	}
	  
	  
	/*
	 * testSaveAdaptV2Data - compound test of this and the check
	 */
//	public void testSaveAdaptV2Data()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		V2DatabaseUtils.setUnitTest(true);
//
//		ManualEntryDTO dto = new ManualEntryDTO();
//		dto.setCandidateId("FAKECAND");
//		dto.setProjectId("FAKEPROJ");
//		dto.setAdaptProjectId(410);
//		dto.setAdaptUserId(966);
//
//		boolean stat = true;
//		dto = _svc.checkAdaptIdData(dto);
//		//boolean stat = _svc.checkAdaptIdData(dto);
//		if(dto == null){
//			stat = false;
//		}
//
//		if (stat)
//		{
//			//System.out.println("Got name:  " + dto.getAdaptFirstName() + " " + dto.getAdaptLastName());
//			//String mod = _svc.saveAdaptV2Data(dto);
//			//System.out.println("Mod returned=" + mod);
//			_svc.saveAdaptV2Data(dto);
//		}
//		else
//		{
//			System.out.println("No ADAPT data available for user " + dto.getAdaptUserId() +
//				" in engagement " + dto.getAdaptProjectId());
//		}
//
//		UnitTestUtils.stop(this);
//	}


//	/*
//	 * testFetchAdaptData
//	 */
//	public void testFetchAdaptData()
//		throws Exception
//	{
//		UnitTestUtils.start(this);
//		V2DatabaseUtils.setUnitTest(true);
//
//		// ADAPT:  Project - Test Freddie Mac Project, Person - Freddie Mac Testuser
//		ManualEntryDTO dto = new ManualEntryDTO();
//		dto.setAdaptProjectId(410);
//		dto.setAdaptUserId(966);
//		dto.setProjectId("IYKNYCNU");
//		dto.setCandidateId("FAKECAND");
//
//		_svc.fetchAdaptData(dto);
//
//		UnitTestUtils.stop(this);
//	}
	
}
