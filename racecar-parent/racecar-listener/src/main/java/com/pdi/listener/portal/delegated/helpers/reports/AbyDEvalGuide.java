/**
 * Copyright (c) 2017 Korn Ferry
 * All rights reserved.
 */
package com.pdi.listener.portal.delegated.helpers.reports;

//import java.util.Iterator;
//import java.util.Map;
//import java.util.Random;
import java.sql.Connection;
import java.text.SimpleDateFormat;
//import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.pdi.data.abyd.dto.common.EGBarDisplayData;
import com.pdi.data.abyd.dto.common.EGCompDisplayData;
import com.pdi.data.abyd.dto.common.EGFullDataDTO;
import com.pdi.data.abyd.dto.common.EGIrDisplayData;
import com.pdi.data.abyd.helpers.common.EGDataHelper;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;
//import com.pdi.data.abyd.util.AbyDDatabaseUtils;
import com.pdi.data.dto.ProjectParticipantInstrument;
//import com.pdi.data.dto.Response;
//import com.pdi.data.dto.Text;
import com.pdi.data.dto.TextGroup;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.IndividualReport;
//import com.pdi.reporting.report.ReportData;
import com.pdi.string.StringUtils;
import com.pdi.xml.XMLUtils;

/*
 * AbyDEvalGuide - class to hold methods relevant to the AbyDEvalGuide
 *
 * NOTE:  EVEN THOUGH LOCATED UNDER THE DELEGATED FOLDER, THERE ARE NO METHODS IN THIS
 *        CLASS THAT ARE DEFINED IN THE DATA INTERFACE.
 *
 * This class is located here because the folder structure (delegated/helpers) is
 * inverted from normal practice elsewhere in this project stack (helpers/delegated).
 * Putting this helper class here avoided confusion with having two folders in the
 * portal structure that had the name "helper".
 */
public class AbyDEvalGuide {

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//

	//
	// Constructors.
	//
	public AbyDEvalGuide() {
		// No logic here at the present time
	}

	//
	// Instance methods.
	//

	/**
	 * getEvalGuideData - Method that retrieves the requisite data for the AbyD
	 * Eval Guide report (only)
	 *
	 * @param inp
	 *            - An IndividualReport object with a small amount of relevant
	 *            data
	 * @param epis
	 *            - The ParojectParticipantReport objects associated with the
	 *            request
	 * @param tg
	 *            - The static text labels (text groups) used to generate the
	 *            report
	 */
	public IndividualReport getEvalGuideRptData(IndividualReport inp, ArrayList<ProjectParticipantInstrument> epis,
			TextGroup tg, HashMap<String, String> overrides) {

		IndividualReport ret = inp;
		Connection con = null;

		try {

			// Get the participant info... assumes that all epis are for the
			// same participant
			ProjectParticipantInstrument epi_x = epis.get(0);
			String participantId = epi_x.getParticipant().getId();

			for (ProjectParticipantInstrument epi : epis) {
				if (participantId == null || participantId.equalsIgnoreCase("")) {
					participantId = "";
				}

				// ret.setScriptedData(reportInput.getCurrentTLT().getScriptedData());

				/*
				 * ========================================
				 * ADD BASIC PARTICIPANT DATA TO REPORT
				 */

				ret.addDisplayData("FIRST_NAME", XMLUtils.xmlEscapeString(epi.getParticipant().getFirstName()));
				ret.addDisplayData("LAST_NAME", XMLUtils.xmlEscapeString(epi.getParticipant().getLastName()));
				ret.getDisplayData().put("PARTICIPANT_NAME", XMLUtils.xmlEscapeString(
						epi.getParticipant().getFirstName() + " " + epi.getParticipant().getLastName()));

				// Get Project data
				ret.addDisplayData("ORGANIZATION", XMLUtils.xmlEscapeString(epi.getProject().getClient().getName()));
				ret.addDisplayData("PROJECT_NAME", XMLUtils.xmlEscapeString(epi.getProject().getName()));

				/*
				 *  Set up labels/option data/entry state
				 *  --------------------------------------
				 */
				// ReportOptionsDataHelper optionsDH = new
				// ReportOptionsDataHelper();
				// ReportLabelsDataHelper labelsDH = new
				// ReportLabelsDataHelper();
				// EntryStateHelper entryH = new EntryStateHelper(con,
				// projectId);

				// Setup2ReportOptionsDTO options =
				// optionsDH.getReportOptionData(con,dnaId);
				// Setup2ReportLabelsDTO labels =
				// labelsDH.getTranslatedReportLabelData(dnaId,
				// tg.getLanguageCode(), this.EDITABLE );
				// EntryStateDTO entryState = entryH.getEntryStateData();

				ret.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);

				java.util.Date date = new java.util.Date();
				SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
				ret.getDisplayData().put("TODAY_DATE", sdf.format(date));

				// ReportData rd = new ReportData();

				con = AbyDDatabaseUtils.getDBConnection();

				// get the data

				int dnaId = Integer.parseInt(overrides.get("dnaId"));
				int moduleId = Integer.parseInt(overrides.get("moduleId"));

				// String lpId = "10381371";
				EGDataHelper helper = new EGDataHelper(con, participantId, dnaId, moduleId);
				EGFullDataDTO data = helper.getEGDisplayData();

				// String part = data.getParticipantId();
				String modelName = XMLUtils.xmlEscapeString(data.getModelName());
				String simName = XMLUtils.xmlEscapeString(data.getSimName());

				ret.getDisplayData().put("ModelName", modelName);
				ret.getDisplayData().put("SimName", simName);

				String fileName = StringUtils.removeFilenameSpecialCharacters(epis.get(0).getParticipant().getLastName()
						+ " " + epis.get(0).getParticipant().getFirstName() + " " + modelName + " " + simName + " "
						+ inp.getReportCode() + " " + new SimpleDateFormat("yyyyMMdd").format(new Date()));

				ret.setName(fileName.replace(" ", "_") + ".pdf");

				// get the comp data
				int compCount = data.getCompArray().size();
				int adjCompCount = compCount - 1;
				ret.getDisplayData().put("CompCount", "" + adjCompCount);

				for (int i = 0; i < data.getCompArray().size(); i++) {

					EGCompDisplayData comps = data.getCompArray().get(i);

					// long compId = comps.getCompId();
					String compName = XMLUtils.xmlEscapeString(comps.getCompName());
					// String compDesc = comps.getCompDesc();
					String compNotes = XMLUtils.xmlEscapeString(comps.getCompNotes());

					if (compNotes == null) {
						compNotes = "";
					}

					ret.getDisplayData().put("CompName_" + i, compName);
					ret.getDisplayData().put("CompNotes_" + i, "Notes: " + compNotes);

					int compOverride = comps.getOverideScoreIndex();

					int barsCount = comps.getCompBars().size();
					int adjBarsCount = barsCount - 1;
					ret.getDisplayData().put("BarCount_" + i, "" + adjBarsCount);

					double avgScore = 0.00;
					double roundedAvgScore = 0.00;
					double cum = 0;
					int cnt = 0;
					String avgFormatted = "";

					// get the bar data
					for (int j = 0; j < comps.getCompBars().size(); j++) {

						EGBarDisplayData bar = comps.getCompBars().get(j);

						int barScore = bar.getBarScore();

						if (barScore == -1) {
							barScore = 0;
						}

						String hiText = "Highly Effective (5) " + XMLUtils.xmlEscapeString(bar.getBarHiText());
						String hiExText = XMLUtils.xmlEscapeString(bar.getBarHiExText());
						String midText = "Solid (3) " + XMLUtils.xmlEscapeString(bar.getBarMidText());
						String midExText = XMLUtils.xmlEscapeString(bar.getBarMidExText());
						String loText = "Needs Improvement (1) " + XMLUtils.xmlEscapeString(bar.getBarLoText());
						String loExText = XMLUtils.xmlEscapeString(bar.getBarLoExText());

						ret.getDisplayData().put("BarScore_" + i + "_" + j, "" + barScore);
						ret.getDisplayData().put("BarHiText_" + i + "_" + j, hiText);
						ret.getDisplayData().put("BarHiExText_" + i + "_" + j, hiExText);
						ret.getDisplayData().put("BarMidText_" + i + "_" + j, midText);
						ret.getDisplayData().put("BarMidExText_" + i + "_" + j, midExText);
						ret.getDisplayData().put("BarLoText_" + i + "_" + j, loText);
						ret.getDisplayData().put("BarLoExText_" + i + "_" + j, loExText);

						if (barScore > 0) {
							cum += barScore;
							cnt++;
						}

					}

					String preString = "";
					if ("ES".equals(data.getSpecialType())) {
						// Externally scored data
						String extScoreString = comps.getExtCompScore();
						if (extScoreString == null || extScoreString.isEmpty() || extScoreString.equals("-1.00")) {
							extScoreString = "0.0";
						}
						roundedAvgScore = Double.valueOf(extScoreString);

					} else if (compOverride == 0) {
						// "Regular" data (scores calculated from BARs)
						if (cnt > 0) {
							// avgScore = (zeroCnt > 1) ? 0.00 : (cum /
							// (double)cnt);
							avgScore = (cum / cnt);
							roundedAvgScore = Math.round(avgScore * 100.0) / 100.0;
						}
					} else {
						// Competency overide score
						avgScore = (compOverride + 1.00) / 2.00;
						roundedAvgScore = Math.round(avgScore * 100.0) / 100.0;
						preString = "*";
					}

					if (roundedAvgScore == 0.00) {
						avgFormatted = "N/A";
					} else {
						avgFormatted = String.format("%.02f", roundedAvgScore);
					}

					ret.getDisplayData().put("CompAvg_" + i, preString + avgFormatted);

				} // end comp for loop

				int impactCount = data.getIrArray().size();
				int adjImpactCount = impactCount - 1;
				ret.getDisplayData().put("ImpactCount", "" + adjImpactCount);

				String impactNotes = XMLUtils.xmlEscapeString("Notes: " + data.getIrNotes());
				ret.getDisplayData().put("ImpactNotes", impactNotes);

				for (int i = 0; i < data.getIrArray().size(); i++) {

					EGIrDisplayData ratings = data.getIrArray().get(i);

					String irText = ratings.getIrText();
					irText = XMLUtils.xmlEscapeString(irText);

					int irScore = ratings.getIrScore();

					ret.getDisplayData().put("ImpactText_" + i, irText);
					ret.getDisplayData().put("ImpactScore_" + i, "" + irScore);

				} // end impact for loop

				String[] overallChoices = { "", "Very Weak", "Needs Development", "Satisfactory", "Strong",
						"Very Strong" };

				int overallScore = (data.getOverallData().getOpr());

				String overallText = overallChoices[overallScore];
				String overallDescription = XMLUtils.xmlEscapeString(data.getOverallData().getBriefDesc());
				String overallStrengths = XMLUtils.xmlEscapeString(data.getOverallData().getKeyStrengths());
				String overallNeeds = XMLUtils.xmlEscapeString(data.getOverallData().getKeyDevNeeds());

				String themesFromPeers = XMLUtils.xmlEscapeString(data.getOverallData().getThemesFromPeers());
				String themesFromPeersLabel = "Themes From Peers (optional):";

				if (themesFromPeers == null) {
					themesFromPeers = "-1";
				}

				String themesFromDirectReports = XMLUtils
						.xmlEscapeString(data.getOverallData().getThemesFromDirectReports());
				String themesFromDirectReportsLabel = "Themes From Direct Reports (optional):";

				if (themesFromDirectReports == null) {
					themesFromDirectReports = "-1";
				}

				String themesFromBoss = XMLUtils.xmlEscapeString(data.getOverallData().getThemesFromBoss());
				String themesFromBossLabel = "Themes From Boss (optional):";

				if (themesFromBoss == null) {
					themesFromBoss = "-1";
				}

				String overallCoachNotes = XMLUtils.xmlEscapeString(data.getOverallData().getCoachNotes());
				String overallCoachNotesLabel = "Real-time Coaching Notes (optional):";

				if (overallCoachNotes == null) {
					overallCoachNotes = "-1";
				}

				ret.getDisplayData().put("OverallText", overallText);
				ret.getDisplayData().put("OverallDescription", overallDescription);
				ret.getDisplayData().put("OverallStrengths", overallStrengths);
				ret.getDisplayData().put("OverallNeeds", overallNeeds);
				ret.getDisplayData().put("ThemesFromPeers", themesFromPeers);
				ret.getDisplayData().put("ThemesFromPeersLabel", themesFromPeersLabel);
				ret.getDisplayData().put("ThemesFromDirectReports", themesFromDirectReports);
				ret.getDisplayData().put("ThemesFromDirectReportsLabel", themesFromDirectReportsLabel);
				ret.getDisplayData().put("ThemesFromBoss", themesFromBoss);
				ret.getDisplayData().put("ThemesFromBossLabel", themesFromBossLabel);
				ret.getDisplayData().put("OverallCoachNotes", overallCoachNotes);
				ret.getDisplayData().put("OverallCoachNotesLabel", overallCoachNotesLabel);
			}
			return ret;
		} catch (Exception e) {
			System.out.println("doAbyDEvalGuide:");
			e.printStackTrace();
			return null;
		} finally {
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception se) {
				se.printStackTrace();
			}
		}
	}
}
