/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.listener.portal.delegated.helpers.reports;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdi.data.dto.NormScore;
import com.pdi.data.dto.ProjectParticipantInstrument;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.Report;
import com.pdi.reporting.report.ReportData;
import com.pdi.reporting.report.utils.BWUtils;
import com.pdi.string.StringUtils;
import com.pdi.xml.XMLUtils;

/*
 * RavBGraphicalReport - class to hold methods relevant to the Ravens B graphical report
 * 
 * NOTE:  EVEN THOUGH LOCATED UNDER THE DELEGATED FOLDER, THERE ARE NO METHODS IN THIS
 *        CLASS THAT ARE DEFINED IN THE DATA INTERFACE.
 *        
 * This class is located here because the folder structure (delegated/helpers) is
 * inverted from normal practice elsewhere in this project stack (helpers/delegated).
 * Putting this helper class here avoided confusion with having two folders in the
 * portal structure that had the name "helper".
 */
public class RavBGraphicalReport
{

	private static final Logger log = LoggerFactory.getLogger(RavBGraphicalReport.class);
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	
	//
	// Constructors.
	//
    public RavBGraphicalReport()
    {
    	// No logic here at the present time
    }

	//
	// Instance methods.
	//


	/**
	 * getRavBDetailData - Method that retrieves the requisite data for the
	 *                     Raven's B graphical report.
	 * 
	 * @param inp - An IndividualReport object
	 * @param epis - The ParojectParticipantReport objects associated with the request
	 */
	public ArrayList<Report> getRavBGraphicalData(IndividualReport inp,  
			ArrayList<ProjectParticipantInstrument> epis, boolean hasRv2 )
	{
		//System.out.println("doRAVBGraphicalRpt...");

		ArrayList<Report> rpts = new ArrayList<Report>();

		try
		{
			for(ProjectParticipantInstrument epi : epis)
			{
				log.debug("Getting data for Project: {}, Participant: {} {}, Instrument: {}", epi.getProject().getId(), epi.getParticipant().getFirstName(), epi.getParticipant().getLastName(), epi.getInstrument().getCode());
				// spin through until you find the ravens
				if(! epi.getInstrument().getCode().equals("rv") && ! epi.getInstrument().getCode().equals("rv2"))
				{
					continue;
				}
				if (epi.getInstrument().getCode().equals("rv") && hasRv2)
				{
					// ignore rv if rv2 in project
					continue;
				}
				
   	   			IndividualReport ret = inp.clone();	// get the input data

				/*
				 * ========================================
				 * ADD BASIC PARTICIPANT DATA TO REPORT 
				 */
				ret.addDisplayData("FIRST_NAME", epi.getParticipant().getFirstName());
				ret.addDisplayData("LAST_NAME", epi.getParticipant().getLastName());
				ret.getDisplayData().put("PARTICIPANT_NAME", epi.getParticipant().getFirstName() + " " + epi.getParticipant().getLastName());			
				ret.addDisplayData("ORGANIZATION", XMLUtils.xmlEscapeString(epi.getProject().getClient().getName()));
		  		ret.addDisplayData("PROJECT_NAME", XMLUtils.xmlEscapeString(epi.getProject().getName()));	

		  		ReportData rd = new ReportData();
		  		ret.getDisplayData().put("ADMIN_DATE", epi.getInstrumentTakenDate().substring(0, 10));	  		
		  		ret.getDisplayData().put("RAVENSB_SP_NAME", epi.getScoreGroup().getSpecialPopulation().getName()); // SPEC-POP NORM					
		  		ret.getDisplayData().put("RAVENSB_GP_NAME", epi.getScoreGroup().getGeneralPopulation().getName() ); //GEN-POP NORM

		  		String fileName = StringUtils.removeFilenameSpecialCharacters(
						epis.get(0).getParticipant().getLastName() + " " +
						epis.get(0).getParticipant().getFirstName() + " " +
						epis.get(0).getProject().getProjectTypeName()+" "+
						inp.getReportCode()+" "+
						new SimpleDateFormat("yyyyMMdd").format(new Date()) + " " 
						);
						
				ret.setName(fileName.replace(" ", "_") + ".pdf");

				ret.getDisplayData().put("CLIENT_LOGO", epi.getProject().getClient().getId()+".png");
				
				// Add flag for rv2 (unused for now)
				ret.getDisplayData().put("RV", (hasRv2 ? "2" : ""));

				// from RAVENS_B.groovy : public IndividualReport generate()

				//RAVENS_B not complete??
				if(epi.getScoreGroup().getScores().isEmpty()) {
					//System.out.println("RAVENS_B IS NULL!");
					// They haven't finished the RAVENS_B
					ret.getScriptedData().put("STATUS", "NOT_FINISHED");
					ret.getScriptedData().put("ERROR", "Raven's B has not been completed, unable to generate report");
					ret.getReportData().put(ReportingConstants.RC_RAVENS_B, rd);				
					rpts.add(ret);
					continue;
				} else {
					ret.getScriptedData().put("STATUS", "");
					ret.getScriptedData().put("ERROR", "");
				}

				// Calc the reference line positions and put them out
				float[] ref = BWUtils.getRefPosArray();
				rd.getScriptedData().put("REF_1", new Float(ref[0]).toString());
				rd.getScriptedData().put("REF_3", new Float(ref[1]).toString());
				rd.getScriptedData().put("REF_16", new Float(ref[2]).toString());
				rd.getScriptedData().put("REF_50", new Float(ref[3]).toString());
				rd.getScriptedData().put("REF_84", new Float(ref[4]).toString());
				rd.getScriptedData().put("REF_97", new Float(ref[5]).toString());
				rd.getScriptedData().put("REF_99", new Float(ref[6]).toString());

				// get the normed score objects for the dimensions....
				HashMap<String, NormScore> gpPercentiles = epi.getScoreGroup().calculateGeneralPopulation();
				HashMap<String, NormScore> spPercentiles = epi.getScoreGroup().calculateSpecialPopulation();

				// Round the percentiles
				if(epi.getScoreGroup().getScores().get("RAVENSB").getRawScore() == null)
				{
					log.debug("Ravens b raw score was NULL");
					ret.getScriptedData().put("STATUS", "NOT_FINISHED");
					ret.getScriptedData().put("ERROR", "No GP percentile"); 
				}
				NormScore ravensNormScore = gpPercentiles.get("RAVENSB");
				if (ravensNormScore == null){
					log.error("Ravens Norm Score is Null.  About to blow up");
				}
				double pctl = gpPercentiles.get("RAVENSB").toPercentile();
				int pOut = new Double(Math.floor(pctl + 0.5)).intValue();
				if (pOut < 1)
					pOut = 1;
				else if (pOut > 99)
					pOut = 99;
				rd.getScriptedData().put("RAVENSB_GP_PCTL", new Integer(pOut).toString());

				if(epi.getScoreGroup().getScores().get("RAVENSB").getRawScore()  == null)
				{
					ret.getScriptedData().put("STATUS", "NOT_FINISHED");
					ret.getScriptedData().put("ERROR", "No SP percentile");

				}
				pctl = spPercentiles.get("RAVENSB").toPercentile();
				pOut = new Double(Math.floor(pctl + 0.5)).intValue();
				if (pOut < 1)
					pOut = 1;
				else if (pOut > 99)
					pOut = 99;
				rd.getScriptedData().put("RAVENSB_SP_PCTL", new Integer(pOut).toString());
				ret.addScriptedData("RAVENSB"+"_SP_PCTL", new Integer(pOut).toString());
				// Get the norms
				// but we don't need to do this because we have them already.
				// however.... 							
				// ... we have to
				// convert the com.pdi.data.Norm to a com.pdi.scoring.Norm 
				// because BWUtils can't see the pdi.data Norm object, and if you 
				// add the pdi-data project to pdi-util-reporting the whole system all of a sudden
				// sees two Norm objects, and blows the whole thing up.
				com.pdi.scoring.Norm gpNorm = new com.pdi.scoring.Norm(epi.getScoreGroup().getGeneralPopulation().getNorms().get("RAVENSB").getMean(),
																	   epi.getScoreGroup().getGeneralPopulation().getNorms().get("RAVENSB").getStandardDeviation());

				com.pdi.scoring.Norm spNorm = new com.pdi.scoring.Norm(epi.getScoreGroup().getSpecialPopulation().getNorms().get("RAVENSB").getMean(),
						   epi.getScoreGroup().getSpecialPopulation().getNorms().get("RAVENSB").getStandardDeviation());

				// Calc the score position
				if(epi.getScoreGroup().getScores().get("RAVENSB").getRawScore() == null)
				{
					ret.getScriptedData().put("STATUS", "NOT_FINISHED");
					ret.getScriptedData().put("ERROR", "No score data for Ravens");
				}

				double score = epi.getScoreGroup().getScores().get("RAVENSB").scoreAsNumber() ; // Double.parseDouble(rd.getScoreData().get(scaleId));
				float scorPos = BWUtils.calcScorePos(score, gpNorm);
				rd.getScriptedData().put("RAVENSB_POSN", new Float(scorPos).toString());

				// Calc the cut-point positions
				float[] csPos = BWUtils.getCutPosArray(gpNorm, spNorm);
				rd.getScriptedData().put("RAVENSB_CP_3", new Float(csPos[0]).toString());
				rd.getScriptedData().put("RAVENSB_CP_11", new Float(csPos[1]).toString());
				rd.getScriptedData().put("RAVENSB_CP_23", new Float(csPos[2]).toString());
				rd.getScriptedData().put("RAVENSB_CP_40", new Float(csPos[3]).toString());
				rd.getScriptedData().put("RAVENSB_CP_60", new Float(csPos[4]).toString());
				rd.getScriptedData().put("RAVENSB_CP_77", new Float(csPos[5]).toString());
				rd.getScriptedData().put("RAVENSB_CP_89", new Float(csPos[6]).toString());
				rd.getScriptedData().put("RAVENSB_CP_97", new Float(csPos[7]).toString());		
				
				ret.getReportData().put(ReportingConstants.RC_RAVENS_B, rd);
				rpts.add(ret);
			} // end main for-loop
			log.debug("Returning {} reports", rpts.size());
			return rpts;
		}
		catch(Exception e)
		{
			log.error("doRAVBGraphicalRpt returning null due to: {}", e);
			e.printStackTrace();
			return null;
		}
	}
}
