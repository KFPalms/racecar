/**
 * Copyright (c) 2014 Korn Ferry International
 * All rights reserved.
 */
package com.pdi.listener.portal.delegated.helpers.reports;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import com.pdi.data.nhn.util.NhnDatabaseUtils;
import com.pdi.data.util.DataLayerPreparedStatement;
import com.pdi.data.util.DataResult;
import com.pdi.data.util.RequestStatus;
import com.pdi.logging.LogWriter;
import com.pdi.reporting.report.IndividualReport;


/*
 * PdaDataHelper - Gets data from the LRM (PDA) database.  Currently used only in support of extracts.
 */
public class PdaDataHelper {
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	
	//
	// Constructors.
	//
	public PdaDataHelper()
	{
		// No logic here at the present time
	}

	//
	// Instance methods.
	//

	
	/**
	 * 
	 * @param ir
	 * @param pptId
	 * @param projId
	 * @param langcode
	 * @return
	 */
	public IndividualReport populatePdaData (IndividualReport ir, String pptId, String projId, String langCode)
	{
		IndividualReport ret = ir;
		
		// get the predefined data and put it into the IR (DisplayData)
		
		// first the headers
		HashMap<String, String> hdat = fetchPredefHdrData(langCode);
		ret.getDisplayData().putAll(hdat);
		
		// then the data itself (includes the custom fields, if any)
		HashMap<String, String> ldat = fetchExtractData(pptId, projId);
		ret.getDisplayData().putAll(ldat);
		
		return ret;
	}

	private HashMap<String, String> fetchExtractData(String pptId, String projId)
	{
		if (pptId == null || projId == null)
		{
			return null;
		}
		
		HashMap<String, String> ret = new HashMap<String, String>();
		
		DataLayerPreparedStatement dlps = null;
		DataResult dr = null;
		ResultSet rs = null;

		boolean hasPredef = true;
		int assessmentId = 0;
		int pdaProjId = 0;
		int companyId = 0;
		
		// flags
		boolean empIdFlg = false;
		boolean costCtrFlg = false;
		boolean poFlg = false;
		boolean intExtCandFlg = false;
		boolean tgtPosnFlg = false;
		boolean timeOrgFlg = false;
		boolean timeInPosnFlg = false;
		boolean billCntctFlg = false;
		boolean sprvNameFlg = false;
		boolean dirRptsFlg = false;
		boolean jobLocFlg = false;
		boolean homPhnFlg = false;
		boolean mobPhnFlg = false;
		
		// Get the "predefined" flags
		StringBuffer sb = new StringBuffer();		
		sb.append("SELECT is_employeeid_enabled, ");
		sb.append("       is_cost_center_enabled, ");
		sb.append("       is_purchase_order_enabled, ");
		sb.append("       is_int_ext_candidate_enabled, ");
		sb.append("       is_target_position_enabled, ");
		sb.append("       is_time_organization_enabled, ");
		sb.append("       is_time_in_cur_posn_enabled, ");
		sb.append("       is_billing_contact_enabled, ");
		sb.append("       is_supervisor_name_enabled, ");
		sb.append("       is_has_direct_reports_enabled, ");
		sb.append("       is_job_location_enabled, ");
		sb.append("       is_home_phone_enabled, ");
		sb.append("       is_mobile_phone_enabled ");
		sb.append("  FROM lrm.dbo.lrm_project_optional_fields WITH(NOLOCK) ");
		sb.append("  WHERE lrm_project_id = (SELECT lrm_project_id ");
		sb.append("                            FROM lrm.dbo.lrm_project WITH(NOLOCK) ");
		sb.append("                            WHERE ext_project_id = " + projId + ") ");
		sb.append("                              AND is_active != 0");

		dlps = NhnDatabaseUtils.prepareStatement(sb);
		if (dlps.isInError())
		{
			System.out.println("Error preparing predefined  flags query." + dlps.toString());
			LogWriter.logSQL(LogWriter.ERROR, this,
					"PdaDataHelper Error:  Preparing predefined PDA flags query." + dlps.toString(),
					sb.toString());
			hasPredef = false;
		}

		if (hasPredef)
		{
			try		
			{
				dr = NhnDatabaseUtils.select(dlps);
				if(dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
				{
					// No data to find... leave flags for predefined fields set to false
					// Do nothing
				}
				else
				{
					// Get the flags
					rs = dr.getResultSet();
					if (rs == null || ! rs.isBeforeFirst())			
					{
						hasPredef = false;
					}
					else
					{
						// Get the flags -- assume that there is a single row
						rs.next();				
						empIdFlg = (rs.getInt("is_employeeid_enabled") != 0 ? true : false);
						costCtrFlg = (rs.getInt("is_cost_center_enabled") != 0 ? true : false);
						poFlg = (rs.getInt("is_purchase_order_enabled") != 0 ? true : false);
						intExtCandFlg = (rs.getInt("is_int_ext_candidate_enabled") != 0 ? true : false);
						tgtPosnFlg = (rs.getInt("is_target_position_enabled") != 0 ? true : false);
						timeOrgFlg = (rs.getInt("is_time_organization_enabled") != 0 ? true : false);
						timeInPosnFlg = (rs.getInt("is_time_in_cur_posn_enabled") != 0 ? true : false);
						billCntctFlg = (rs.getInt("is_billing_contact_enabled") != 0 ? true : false);
						sprvNameFlg = (rs.getInt("is_supervisor_name_enabled") != 0 ? true : false);
						dirRptsFlg = (rs.getInt("is_has_direct_reports_enabled") != 0 ? true : false);
						jobLocFlg = (rs.getInt("is_job_location_enabled") != 0 ? true : false);
						homPhnFlg = (rs.getInt("is_home_phone_enabled") != 0 ? true : false);
						mobPhnFlg = (rs.getInt("is_mobile_phone_enabled") != 0 ? true : false);
					}
				}
			}
			catch(java.sql.SQLException se)
			{	
				LogWriter.logBasic(LogWriter.INFO, this, 
						"PdaDataHelper Error: Predef flags fetch for " + projId
						+ "\n" + se.getMessage());
				hasPredef = false;
			}
			finally
			{
//				if (rs != null)
//				{
//					try {  rs.close();  }
//					catch(Exception e) { /* swallow it */ }
//					rs = null;
//				}
				if (dr != null) { dr.close(); dr = null; }
				dlps = null;
			}
		}	// End flag fetch (first "if(hasPredef)")
		
		
		// get the metadata if possible
		if (hasPredef)
		{
			sb.setLength(0);
			sb.append("SELECT asmt.assessment_id, ");
			sb.append("       asmt.lrm_project_id, ");
			sb.append("       proj.company_id, ");
			sb.append("       um.title, ");
			sb.append("       cc1.name as resCountry, ");
			sb.append("       aa.cost_center, ");
			sb.append("       aa.purchase_order_nbr, ");
			sb.append("       um.ext_employee_id, ");
			sb.append("       um.is_internal_candidate, ");
			sb.append("       um.target_position, ");
			sb.append("       um.timeorg,  ");
			sb.append("       um.tiPosition, ");
			sb.append("       ctct.first_name, ");
			sb.append("       ctct.last_name, ");
			sb.append("       aa.invoice_email, ");
			sb.append("       addr.address_1, ");
			sb.append("       addr.address_2, ");
			sb.append("       addr.address_3, ");
			sb.append("       addr.city, ");
			sb.append("       addr.state, ");
			sb.append("       addr.postal_code, ");
			sb.append("       addr.country_id, ");
			sb.append("       cc2.name AS billCountry, ");
			sb.append("       um.supervisor_name, ");
			sb.append("       um.has_direct_reports, ");
			sb.append("       um.job_location,  ");
			sb.append("       um.home_phone,  ");
			sb.append("       um.mobile_phone ");
			sb.append("  FROM lrm.dbo.assessment asmt WITH(NOLOCK) ");
			sb.append("    LEFT JOIN lrm.dbo.assessment_accounting aa WITH(NOLOCK) ON (aa.assessment_id = asmt.assessment_id AND aa.is_active != 0) ");
			sb.append("    LEFT JOIN lrm.dbo.lrm_user_metadata um WITH(NOLOCK) ON (um.lrm_user_id = asmt.lrm_user_id AND um.is_active != 0) ");
			sb.append("    LEFT JOIN platform.dbo.country cc1 WITH(NOLOCK) ON cc1.country_id = um.country_id ");
			sb.append("    LEFT JOIN lrm.dbo.contact ctct WITH(NOLOCK) ON (ctct.contact_id = aa.contact_id AND ctct.is_active != 0) ");
			sb.append("    LEFT JOIN lrm.dbo.address addr WITH(NOLOCK) ON (addr.address_id = aa.billing_mailing_address_id AND addr.is_active != 0) ");
			sb.append("    LEFT JOIN platform.dbo.country cc2 WITH(NOLOCK) ON cc2.country_id = addr.country_id ");
			sb.append("    LEFT JOIN platform.dbo.project proj ON proj.project_id = " + projId + " ");
			sb.append("  WHERE asmt.is_active != 0 ");
			sb.append("    AND asmt.lrm_user_id in (SELECT lrm_user_id ");
			sb.append("                              FROM lrm.dbo.lrm_user WITH(NOLOCK) ");
			sb.append("                              WHERE ext_users_id = " + pptId + ") ");
			sb.append("    AND asmt.lrm_project_id in (SELECT lrm_project_id ");
			sb.append("                              FROM lrm.dbo.lrm_project WITH(NOLOCK) ");
			sb.append("                              WHERE ext_project_id = " + projId + ") ");
			sb.append("  ORDER BY asmt.request_w_name_received_date DESC");
			dlps = NhnDatabaseUtils.prepareStatement(sb);
			if (dlps.isInError())
			{
				System.out.println("Error preparing predefined PDA user metadata query." + dlps.toString());
				LogWriter.logSQL(LogWriter.ERROR, this,
						"PdaDataHelper Error:  Preparing predefined PDA user metadata query." + dlps.toString(),
						sb.toString());
				hasPredef = false;
			}

			if (hasPredef)
			{
				try		
				{
					dr = NhnDatabaseUtils.select(dlps);
					if(dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
					{
						// No data to find... we are done with predefined fields
					}
					else
					{
						// Get the metadata
						rs = dr.getResultSet();
						if (rs == null || ! rs.isBeforeFirst())			
						{
							// Do nothing
						}
						else
						{
							// Get the metadata -- get only the top (most recent) row
							rs.next();
							// Get the asmt ID for later use
							assessmentId = rs.getInt("assessment_id");
							pdaProjId = rs.getInt("lrm_project_id");
							companyId = rs.getInt("company_id");
							
							// Get the info for the predefined fields
							ret.put("PDA_JOB_TITLE", rs.getString("title"));
							ret.put("PDA_COUNTRY", rs.getString("resCountry"));
							if (empIdFlg && rs.getString("ext_employee_id") != null)
									ret.put("PDA_EMP_ID", rs.getString("ext_employee_id"));
							if (costCtrFlg && rs.getString("cost_center") != null)
								ret.put("PDA_COST_CTR", rs.getString("cost_center"));
							if (poFlg && rs.getString("purchase_order_nbr") != null)
								ret.put("PDA_PO_NBR", rs.getString("purchase_order_nbr"));
							if (intExtCandFlg)
								ret.put("PDA_INT_EXT_CAND", (rs.getBoolean("is_internal_candidate") ? "Internal" : "External"));
							if (tgtPosnFlg && rs.getString("target_position") != null)
								ret.put("PDA_TRGT_POS", rs.getString("target_position"));
							if (timeOrgFlg  &&  rs.getString("timeorg") != null)
								ret.put("PDA_TIM_ORG", rs.getString("timeorg"));
							if (timeInPosnFlg  && rs.getString("tiPosition") != null)
								ret.put("PDA_TIP", rs.getString("tiPosition"));
							if (billCntctFlg)
							{
								if(rs.getString("first_name") != null)
									ret.put("PDA_BC_FNAME", rs.getString("first_name"));
								if(rs.getString("last_name") != null)
									ret.put("PDA_BC_LNAME", rs.getString("last_name"));
								if(rs.getString("invoice_email") != null)
									ret.put("PDA_BC_INV_EMAIL", rs.getString("invoice_email"));
								if(rs.getString("address_1") != null)
									ret.put("PDA_BC_ADDR_1", rs.getString("address_1"));
								if(rs.getString("address_2") != null)
									ret.put("PDA_BC_ADDR_2", rs.getString("address_2"));
								if(rs.getString("address_3") != null)
									ret.put("PDA_BC_ADDR_3", rs.getString("address_3"));
								if(rs.getString("city") != null)
									ret.put("PDA_BC_CITY", rs.getString("city"));
								if(rs.getString("state") != null)
									ret.put("PDA_BC_STATE", rs.getString("state"));
								if(rs.getString("postal_code") != null)
									ret.put("PDA_BC_PCODE", rs.getString("postal_code"));
								if(rs.getString("billCountry") != null)
									ret.put("PDA_BC_COUNTRY", rs.getString("billCountry"));
							}
							if (sprvNameFlg  && rs.getString("supervisor_name") != null)
								ret.put("PDA_SUPR_NAME", rs.getString("supervisor_name"));
							if (dirRptsFlg)
								ret.put("PDA_DIR_RPTS", (rs.getBoolean("has_direct_reports") ? "Yes" : "No"));
							if (jobLocFlg  &&  rs.getString("job_location") != null)
								ret.put("PDA_JOB_LOC", rs.getString("job_location"));
							if (homPhnFlg  &&  rs.getString("home_phone") != null)
								ret.put("PDA_HOME_PHONE", rs.getString("home_phone"));
							if (mobPhnFlg  &&  rs.getString("mobile_phone") != null)
								ret.put("PDA_MOBL_PHONE", rs.getString("mobile_phone"));
						}
					}
				}
				catch(java.sql.SQLException se)
				{	
					LogWriter.logBasic(LogWriter.INFO, this, 
							"PdaDataHelper Error: Metadata fetch for " + pptId
							+ "\n" + se.getMessage());
					hasPredef = false;
				}
				finally
				{
//					if (rs != null)
//					{
//						try {  rs.close();  }
//						catch(Exception e) { /* swallow it */ }
//						rs = null;
//					}
					if (dr != null) { dr.close(); dr = null; }
				}
			}	// End of inner check of hasPredef on metadata
		
		}	// Ind of metadata fetch (second "if(hasPredef)")
		
		// get custom fields
		boolean hasCustom = true;
		if (assessmentId == 0)
		{
			LogWriter.logBasic(LogWriter.INFO, this, 
					"PdaDataHelper Error: No assessment ID for ppt=" + pptId + ", projId=" + projId);
			hasCustom = false;
		}
		if (hasCustom)
		{
			sb.setLength(0);
			// Refactor of keys (LRMI-678) required that this query change
		  	// Query for when the value_entity_types change.  Also includes use of the custom_field_values view to simplify the fetch of  the type 2 (value) data
			sb.append("  SELECT  1 AS typer, ");
			sb.append("          cfm.custom_field_mapping_id, ");
			sb.append("          cfm.sequence_order, ");
			sb.append("          cf.name, ");
			sb.append("          NULL AS value ");
			sb.append("    FROM uffda.dbo.custom_field_mapping cfm ");
			sb.append("      INNER JOIN uffda.dbo.custom_field cf ON cf.custom_field_id = cfm.custom_field_id AND cf.is_active = 1 ");
			sb.append("      INNER JOIN uffda.dbo.field_entity_type fet ON (fet.field_entity_type_id = cfm.field_entity_type_id AND fet.is_active = 1) ");
			sb.append("    WHERE  cfm.is_active = 1 ");
			sb.append("      AND ( ");
			sb.append("               (fet.name = 'CLIENT' AND cfm.entity_id = " + companyId + ") ");
			sb.append("            OR (fet.name = 'PALMS_PROJECT' AND cfm.entity_id = " + projId + " ) ");
			sb.append("            OR (fet.name = 'PDA_PROJECT' AND cfm.entity_id = " + pdaProjId + ") ");
			sb.append("          ) ");
			sb.append("UNION ");
			sb.append("  SELECT 2 as typer, ");
			sb.append("         cfv.custom_field_mapping_id, ");
			sb.append("         0 as sequence_order, ");
			sb.append("         null as name, ");
			sb.append("         cfv.resolved_value as value ");
			sb.append("    FROM uffda.dbo.custom_field_values cfv ");
			sb.append("    WHERE cfv.users_id =  " + pptId + " ");
			sb.append("       OR cfv.assessment_id = " + assessmentId + " ");
			sb.append("ORDER BY typer, sequence_order,custom_field_mapping_id ");

			dlps = NhnDatabaseUtils.prepareStatement(sb);
			if (dlps.isInError())
			{
				System.out.println("Error preparing custom PDA user custom data query." + dlps.toString());
				LogWriter.logSQL(LogWriter.ERROR, this,
						"PdaDataHelper Error:  Preparing custom PDA user custom data query." + dlps.toString(),
						sb.toString());
				hasCustom = false;
			}

			if (hasCustom)
			{
				try		
				{
					dr = NhnDatabaseUtils.select(dlps);
					if(dr.getStatus().getStatusCode() == RequestStatus.DBS_NO_DATA)
					{
						// No data to find... we are done with predefined fields
					}
					else
					{
						LinkedHashMap <Integer, ArrayList<String>> customData = new LinkedHashMap <Integer, ArrayList<String>>();
						// Get the custom fields
						rs = dr.getResultSet();
						if (rs == null || ! rs.isBeforeFirst())			
						{
							// Do nothing
						}
						else
						{
							// Get the custom data
							while (rs.next())
							{
								int type = rs.getInt("typer");
								if (type == 1)
								{
									//set up the name data
									ArrayList<String> ent = new ArrayList<String>();
									ent.add(rs.getString("name"));
									ent.add(null);
									customData.put(rs.getInt("custom_field_mapping_id"), ent);
								}
								else if (type == 2)
								{
									// fill in the value data
									int key = rs.getInt("custom_field_mapping_id");
									ArrayList<String> ent = customData.get(key);
									if (ent == null)
									{
										continue;
									}
									else
									{
										ent.set(1,rs.getString("value"));
									}
								}
								else
								{
									LogWriter.logSQL(LogWriter.ERROR, this,
											"PdaDataHelper Error:  invalid 'typer' value", "value=" + type);
								}
							}
							
							if (customData.size() > 0)
							{
								// dump that data into the holder
								ret.put("CPDA_COUNT", "" + customData.size());
								int i = 0;
								Iterator<Entry<Integer, ArrayList<String>>> itr = customData.entrySet().iterator();
								while (itr.hasNext())
								{
									i++;
									Entry<Integer, ArrayList<String>> entry = itr.next();
									ret.put("CPDA_" + i + "_CFNAME", entry.getValue().get(0));
									ret.put("CPDA_" + i + "_CFVALUE", entry.getValue().get(1));
								}
							}	// End if(customData.size())
						}	// End else on resultSet check
					}	// End else on dr has no data
				}	// End of the try
				catch(java.sql.SQLException se)
				{	
					LogWriter.logBasic(LogWriter.INFO, this, 
							"PdaDataHelper Error: Custom field fetch for " + pptId
							+ "\n" + se.getMessage());
					hasCustom = false;
				}
				finally
				{
//					if (rs != null)
//					{
//						try {  rs.close();  }
//						catch(Exception e) { /* swallow it */ }
//						rs = null;
//					}
					if (dr != null) { dr.close(); dr = null; }
				}
			}	// End of inner hasCustom
		}	// End of  check of hasCustom
	
		
		return ret;
	}
	

	/*
	 * Facade class - get the headers based upon language... Always returns English for now.
	 *                Headings stolen from LRM (now PDA) in PALMS
	 * 
	 * NOTE:  Heading labels are the regular label name with "_HDG" post-pended
	 */
	private HashMap<String, String> fetchPredefHdrData(String langId)
	{
		HashMap<String, String> ret = new HashMap<String, String>();
		
		ret.put("PDA_JOB_TITLE_HDG",	"Current Job Title");			// Prop key = "TITLE_LABEL"
		ret.put("PDA_COUNTRY_HDG",		"Country of Residence");		// Prop key = "COUNTRY_OF_RESIDENCE_LABEL"
		ret.put("PDA_EMP_ID_HDG",		"Employee ID");					// Prop key = "EMPLOYEE_ID_LABEL"
		ret.put("PDA_COST_CTR_HDG",		"Cost Center");					// Prop key = "COST_CENTER_LABEL"
		ret.put("PDA_PO_NBR_HDG",		"Purchase Order");				// Prop key = "PURCHASE_ORDER_LABEL"
		ret.put("PDA_INT_EXT_CAND_HDG",	"Internal/External Candidate");	// Prop key = "INTERNAL_CANDIDATE_LABEL"
		ret.put("PDA_TRGT_POS_HDG",		"Target Position");				// Prop key = "TARGET_POSITION_LABEL"
		ret.put("PDA_TIM_ORG_HDG",		"Tenure with organization");	// Prop key = "TIME_ORGANIZATION_LABEL"
		ret.put("PDA_TIP_HDG",			"Tenure in current position");	// Prop key = "TIME_IN_CUR_POSN_LABEL"
		ret.put("PDA_BC_FNAME_HDG",		"Billing Contact First Name");	// Prop key = "GIVEN_NAME_LABEL"
		ret.put("PDA_BC_LNAME_HDG",		"BillingContact Last Name");	// Prop key = "SURNAME_LABEL"
		ret.put("PDA_BC_INV_EMAIL_HDG",	"Billing (Invoice) Email Address");	// Prop key = "INVOICE_EMAIL_LABEL"
		ret.put("PDA_BC_ADDR_1_HDG",	"Billing Address 1");			// Prop key = "ADDRESS1_LABEL"
		ret.put("PDA_BC_ADDR_2_HDG",	"Billing Address 2");			// Prop key = "ADDRESS2_LABEL"
		ret.put("PDA_BC_ADDR_3_HDG",	"Billing Address 3");			// Prop key = "ADDRESS3_LABEL"
		ret.put("PDA_BC_CITY_HDG",		"Billing City");				// Prop key = "CITY_LABEL"
		ret.put("PDA_BC_STATE_HDG",		"Billing State/Region/Province");	// Prop key = "STATE_REGION_PROVINCE_LABEL"
		ret.put("PDA_BC_PCODE_HDG",		"Billing Postal Code");			// Prop key = "POSTAL_CODE_LABEL"
		ret.put("PDA_BC_COUNTRY_HDG",	"Billing Country");				// Prop key = "COUNTRY_LABEL"
		ret.put("PDA_SUPR_NAME_HDG",	"Boss Name");					// Prop key = "SUPERVISOR_NAME_LABEL"
		ret.put("PDA_DIR_RPTS_HDG",		"Has Direct Reports?");			// Prop key = "HAS_DIRECT_REPORTS_LABEL"
		ret.put("PDA_JOB_LOC_HDG",		"Job Location");				// Prop key = "JOB_LOCATION_LABEL"
		ret.put("PDA_HOME_PHONE_HDG",	"Home Phone");					// Prop key = "HOME_PHONE_LABEL"
		ret.put("PDA_MOBL_PHONE_HDG",	"Mobile Phone");				// Prop key = "MOBILE_PHONE_LABEL"
		
		return ret;
	}
}
