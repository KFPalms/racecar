/**
 * Copyright (c) 2008 Personnel Decisions International, Inc.
 * All rights reserved.
 */

package com.pdi.listener.abyd.setup;

import java.sql.Connection;
import java.util.ArrayList;

import com.pdi.data.abyd.dto.common.DNACellDTO;
import com.pdi.data.abyd.dto.common.DNAStructureDTO;
import com.pdi.data.abyd.dto.setup.DNADescription;
import com.pdi.data.abyd.dto.setup.EntryStateDTO;
import com.pdi.data.abyd.helpers.setup.ABDComCheckHelper;
import com.pdi.data.abyd.helpers.setup.DNAStructureHelper;
import com.pdi.data.abyd.helpers.setup.EntryStateHelper;
import com.pdi.data.abyd.helpers.setup.SaveDNADescHelper;
import com.pdi.data.abyd.helpers.setup.SaveDNAStructureHelper;
//import com.pdi.data.abyd.helpers.setup.SetupSendEmailHelper;
import com.pdi.data.abyd.util.AbyDDatabaseUtils;

/**
 * SetupService provides entry point framework
 * for the Assessment-by-Design Project Setup app
 *
 * @author		Ken Beukelman
  */
public class SetupService
{
	//
	// Static data.
	//
	

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	
	//
	// Constructors.
	//
	public SetupService()
	{
		// Does nothing currently
	}

	//
	// Instance methods.
	//

	
	/**
	 * Com Check.  See if the remoting connection and the db are up
	 *
	 * @return A String object
	 * @throws Exception
	 */
	public String comCheck()
		throws Exception
	{
		Connection con = null;
		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();

			// get the data
			ABDComCheckHelper helper = new ABDComCheckHelper(con);        
			String ret = helper.comCheck();
				
			return ret;
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)  {  /* Swallow the exception */  }
				con = null;
			}
		}
	}

	
	/**
	 * Fetch the initial data needed for the By Design Setup app.
	 *
	 * @param job the V2 job id
	 * @return the EntryStateDTO object
	 * @throws Exception
	 */
	public EntryStateDTO fetchEntryState(String jobId)
		throws Exception
	{

		Connection con = null;
		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();

			// get the data
			EntryStateHelper helper = new EntryStateHelper(con, jobId);        
			EntryStateDTO data = helper.getEntryStateData();
			//System.out.println("RETURNING DATA");
			return data;
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)  { ex.printStackTrace(); /* Swallow the exception */  }
				con = null;
			}
		}
	}       

	
	/**
	 * Fetch the dna structure data.  Fetches template data overlayed by dna data
	 *
	 * @param dnaId 
	 * @return the EntryStateDTO object
	 * @throws Exception
	 */
	public DNAStructureDTO fetchFullDNAStructure(long dnaId)
		throws Exception
	{
		Connection con = null;
		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();

			// get the data
			DNAStructureHelper helper = new DNAStructureHelper(con, dnaId);        
			DNAStructureDTO data = helper.getSetupDNAStructData();
				
			return data;
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)  {  /* Swallow the exception */  }
				con = null;
			}
		}
	}       

	
	/**
	 * First DNA Update - Model id, DNA Name, Client Manager Name, Client Manager id
	 *   NOTE:  No matter what info is in the DNADescription object, only these get updated.
	 *
	 * @param dd a DNADescription object with the data to save
	 * @return the updated DNADescription object
	 * @throws Exception
	 */
	public DNADescription saveNameModelCmgr(DNADescription dd)
		throws Exception
	{
		Connection con = null;
		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();

			// Save the data
			//SaveDNADescHelper helper = new SaveDNADescHelper(con, dd, this.properties);
			SaveDNADescHelper helper = new SaveDNADescHelper(con, dd);
			return helper.saveNameModelCmgr();
		}
		catch(Exception e) {
			e.printStackTrace();
			return null;
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)  {
					ex.printStackTrace();
					throw ex;
					/* Swallow the exception */  
				}
				con = null;
			}
		}
	}       

	
	/**
	 * Second DNA Update - Structure
	 *
	 * @param dnaId the ID of the DNA to update
	 * @param intersections a list of DNACellDTO objects denoting the desired state
	 * @throws Exception
	 */
	public boolean saveDNAStructure(long dnaId, ArrayList<DNACellDTO> cells)
		throws Exception
	{
		Connection con = null;
		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();

			// Save the data
			SaveDNAStructureHelper helper = new SaveDNAStructureHelper(con, dnaId, cells);
			return helper.saveStructure();
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)  {  
					throw ex;
				}
				con = null;
			}
		}
	}       

	
	/**
	 * Third DNA Update - More description data
	 *   NOTE:  No matter what info is in the DNADescription object, only
	 *          the norm (mean & Std dev) the WB version, and the notes get updated.
	 *
	 * @param dd a DNADescription object with the data to save
	 * @throws Exception
	 */
	public void saveMoreDNADesc(DNADescription dd)
		throws Exception
	{
		Connection con = null;
		try
		{
			// Connect to the database
			con = AbyDDatabaseUtils.getDBConnection();

			// Save the data
			//SaveDNADescHelper helper = new SaveDNADescHelper(con, dd, this.properties);
			SaveDNADescHelper helper = new SaveDNADescHelper(con, dd);
			helper.saveMoreDNAData();
			return;
		}
		finally
		{
			// close the connection
			if (con != null)
			{
				try  {  con.close();  }
				catch (Exception ex)  {  /* Swallow the exception */  
					throw ex;
				}
				con = null;
			}
		}
	}       

	
//	/**
//	 * Fetch the SP data.
//	 *
//	 * @param dnaId 
//	 * @return the EntryStateDTO object
//	 * @throws Exception
//	 */
// SP (Success Profile) has been eliminated from A by D
//
//	public SPFullDataDTO fetchSPData(long dnaId)
//		throws Exception
//	{
//		Connection con = null;
//		try
//		{
//			// Connect to the database
//			con = AbyDDatabaseUtils.getDBConnection();
//
//			// get the data
//			SPDataHelper helper = new SPDataHelper(con, dnaId);        
//			SPFullDataDTO data = helper.getAllSPData();
//				
//			return data;
//		}
//		finally
//		{
//			// close the connection
//			if (con != null)
//			{
//				try  {  con.close();  }
//				catch (Exception ex)  {  /* Swallow the exception */  }
//				con = null;
//			}
//		}
//	}       

	
//	/**
//	 * Save the SP text data.
//	 *
//	 * @param dnaId 
//	 * @return the EntryStateDTO object
//	 * @throws Exception
//	 */
// SP (Success Profile) has been eliminated from A by D
//
//	public void saveSPTexts(long dnaId, ArrayList<KeyValuePair> textAry)
//		throws Exception
//	{
//		Connection con = null;
//		try
//		{
//			// Connect to the database
//			con = AbyDDatabaseUtils.getDBConnection();
//
//			// get the data
//			SaveSPTextHelper helper = new SaveSPTextHelper(con, dnaId, textAry);        
//			helper.saveSPTexts();
//				
//			return;
//		}
//		finally
//		{
//			// close the connection
//			if (con != null)
//			{
//				try  {  con.close();  }
//				catch (Exception ex)  { 
//					throw ex;
//				}
//				con = null;
//			}
//		}
//	}  
	

//	/**
//	 * Save the SP element data.
//	 *
//	 * @param dnaId 
//	 * @return the EntryStateDTO object
//	 * @throws Exception
//	 */
// SP (Success Profile) has been eliminated from A by D
//
//	public void saveSPElements(long dnaId, ArrayList<CompElementDTO> eltAry)
//		throws Exception
//	{
//		Connection con = null;
//		try
//		{
//			// Connect to the database
//			con = AbyDDatabaseUtils.getDBConnection();
//
//			// get the data
//			SaveSPElementHelper helper = new SaveSPElementHelper(con, dnaId, eltAry);        
//			helper.saveSPElements();
//				
//			return;
//		}
//		finally
//		{
//			// close the connection
//			if (con != null)
//			{
//				try  {  con.close();  }
//				catch (Exception ex)  {  /* Swallow the exception */  }
//				con = null;
//			}
//		}
//	}  
	

//	/**
//	 * Send an e-mail.
//	 * 
//	 * This has become an independent RPC because, per the Defect Review meeting of 4/7/09 the
//	 * business logic will be moved to the UI.
//	 *
//	 * @param address the "TO:" email address 
//	 * @param textFlg a flag denoting which text to use in the body of the e-mail. 
//	 * @throws Exception
//	 */
//	public void sendEmail(String address, String textFlg, String clientName)
//		throws Exception
//	{
//		Connection con = null;
//		try
//		{
//			// Connect to the database
//			con = AbyDDatabaseUtils.getDBConnection();
//
//			// Set up and send the e-mail
//			SetupSendEmailHelper helper = new SetupSendEmailHelper(address, textFlg, clientName);        
//			helper.sendEmail();
//		
//			return;
//		}
//		finally
//		{
//			// close the connection
//			if (con != null)
//			{
//				try  {  con.close();  }
//				catch (Exception ex)  {  /* Swallow the exception */  }
//				con = null;
//			}
//		}
//	}  

}
