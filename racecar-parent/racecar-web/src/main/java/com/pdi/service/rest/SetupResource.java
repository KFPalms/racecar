package com.pdi.service.rest;

import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.pdi.data.abyd.dto.common.DNAStructureDTO;
import com.pdi.data.abyd.dto.common.DNACellHolderDTO;
import com.pdi.data.abyd.dto.setup.DNADescription;
import com.pdi.data.abyd.dto.setup.EntryStateDTO;
import com.pdi.listener.abyd.setup.SetupService;

/**
 * SetupResource - Provides the "middle-ware" for the new (Angular) version of
 * required Setup functionality. It allows us to use the same server-side code
 * and calls but with the new style front-end.
 */
// NOTE: We are converting this "just in time". The service methods for the
// as-yet unconverted services
// are present but commented out. These will eventually be replaces by the
// resource endpoints that
// call them as we need them

@Path("setup")
public class SetupResource {
	//
	// Static data
	//
	private static String SUCCESS = "Success";

	//
	// Instance Data
	//
	SetupService setupService = new SetupService();

	/**
	 * optionsResponse - Build the desired Response object for the @OPTIONS
	 * annotation
	 *
	 * @return
	 */
	private Response optionsResponse() {
		return Response.ok(SUCCESS).header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT")
				.header("Access-Control-Allow-Headers",
						"Access-Control-Allow-Headers, Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Cache-Control, Authorization, Pragma, Expires")
				.header("Access-Control-Expose-Headers", "newToken").header("Access-Control-Allow-Origin", "*").build();
	}

	@GET
	@Path("/fetchEntryState/{jobId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response fetchEntryState(@PathParam("jobId") String jobId) {
		EntryStateDTO dto = null;

		try {
			dto = setupService.fetchEntryState(jobId);
		} catch (Exception e) {
			System.out.println("Exception caught fetching Entry State.  jobId=" + jobId + ", msg=" + e.getMessage());
			return null;
		}
		// System.out.println("<=======>\fetchEntryState(): EntryStateDTO for jobId
		// " + jobId + "\n" + dto.toString());

		// Convert to JSON
		ObjectMapper mapper = new ObjectMapper();
		String retStr = null;
		try {
			retStr = mapper.writeValueAsString(dto);
		} catch (JsonMappingException e) {
			System.out.println("fetchEntryState():  JsonMappingException.  msg=" + e.getMessage());
			retStr = null;
		} catch (JsonGenerationException e) {
			System.out.println("fetchEntryState():  JsonGenerationException.  msg=" + e.getMessage());
			retStr = null;
		} catch (IOException e) {
			System.out.println("fetchEntryState():  IOException.  msg=" + e.getMessage());
			retStr = null;
		}
		// System.out.println("<=======>\nfetchEntryState(): JSON generated
		// (jobId=" + jobId + "): " + retStr);

		return Response.ok(retStr).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/fetchEntryState/{jobId}")
	public Response optionsFetchEntryState() {
		return optionsResponse();
	}

	@GET
	@Path("/fetchFullDNAStructure/{dnaId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response fetchFullDNAStructure(@PathParam("dnaId") long dnaId) throws Exception {
		DNAStructureDTO dto = null;

		try {
			dto = setupService.fetchFullDNAStructure(dnaId);
		} catch (Exception e) {
			System.out.println("Exception caught fetchFullDNAStructure.  dnaId=" + dnaId + ", msg=" + e.getMessage());
			return null;
		}
		// System.out.println("<=======>\fetchFullDNAStructure(): DNAStructureDTO for dnaId
		// " + dnaId + "\n" + dto.toString());

		// Convert to JSON
		ObjectMapper mapper = new ObjectMapper();
		String retStr = null;
		try {
			retStr = mapper.writeValueAsString(dto);
		} catch (JsonMappingException e) {
			System.out.println("fetchFullDNAStructure():  JsonMappingException.  msg=" + e.getMessage());
			retStr = null;
		} catch (JsonGenerationException e) {
			System.out.println("fetchFullDNAStructure():  JsonGenerationException.  msg=" + e.getMessage());
			retStr = null;
		} catch (IOException e) {
			System.out.println("fetchFullDNAStructure():  IOException.  msg=" + e.getMessage());
			retStr = null;
		}
		// System.out.println("<=======>\nfetchFullDNAStructure(): JSON
		// generated (dnaId=" + dnaId + "): " + retStr);

		return Response.ok(retStr).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/fetchFullDNAStructure/{jobId}")
	public Response optionsFetchFullDNAStructure() {
		return optionsResponse();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/saveNameModelCmgr")
	public Response saveNameModelCmgr(String input) {
		ObjectMapper mapper = new ObjectMapper();
		DNADescription obj = null;
		String eStr = "saveNameModelCmgr():  ";
		try {
			// System.out.println("JSON str is =" + input);
			obj = mapper.readValue(input, DNADescription.class);
			setupService.saveNameModelCmgr(obj);
		} catch (JsonMappingException e) {
			eStr += "JsonMappingException - saveNameModelCmgr.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		} catch (JsonParseException e) {
			eStr += "JsonParseException - saveNameModelCmgr.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		} catch (IOException e) {
			eStr += "IOException - saveNameModelCmgr.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		} catch (Exception e) {
			eStr += "Exception - saveNameModelCmgr.  msg=" + e.getMessage();
			System.out.println(eStr);
			e.printStackTrace();
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}

		return Response.ok(SUCCESS).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/saveNameModelCmgr")
	public Response optionsSaveNameModelCmgr() {
		return optionsResponse();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/saveDNASelections")
	public Response saveDNASelections(String input) {
		ObjectMapper mapper = new ObjectMapper();
		DNACellHolderDTO obj = null;
		String eStr = "saveDNASelections():  ";
		try {
			// System.out.println("JSON str is =" + input);
			obj = mapper.readValue(input, DNACellHolderDTO.class);
			setupService.saveDNAStructure(obj.getDnaId(), obj.getModCompIntersection());
		} catch (JsonMappingException e) {
			eStr += "JsonMappingException - saveDNASelections.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		} catch (JsonParseException e) {
			eStr += "JsonParseException - saveDNASelections.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		} catch (IOException e) {
			eStr += "IOException - saveDNASelections.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		} catch (Exception e) {
			eStr += "Exception - saveDNASelections.  msg=" + e.getMessage();
			System.out.println(eStr);
			e.printStackTrace();
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}

		return Response.ok(SUCCESS).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/saveDNASelections")
	public Response optionsSaveDNASelections() {
		return optionsResponse();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/saveMoreDNADesc")
	public Response saveMoreDNADesc(String input) {
		ObjectMapper mapper = new ObjectMapper();
		DNADescription obj = null;
		String eStr = "saveMoreDNADesc():  ";
		try {
			// System.out.println("JSON str is =" + input);
			obj = mapper.readValue(input, DNADescription.class);
			setupService.saveMoreDNADesc(obj);
		} catch (JsonMappingException e) {
			eStr += "JsonMappingException - saveMoreDNADesc.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		} catch (JsonParseException e) {
			eStr += "JsonParseException - saveMoreDNADesc.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		} catch (IOException e) {
			eStr += "IOException - saveMoreDNADesc.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		} catch (Exception e) {
			eStr += "Exception - saveMoreDNADesc.  msg=" + e.getMessage();
			System.out.println(eStr);
			e.printStackTrace();
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}

		return Response.ok(SUCCESS).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/saveMoreDNADesc")
	public Response optionsSaveMoreDNADesc() {
		return optionsResponse();
	}

}