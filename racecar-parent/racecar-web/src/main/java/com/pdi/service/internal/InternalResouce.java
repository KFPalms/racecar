package com.pdi.service.internal;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kf.palms.utils.JNDILookupEJB;
import com.kf.racecar.ejb.singleton.RacecarUtilSingleton;
import com.pdi.listener.abyd.reportInput.ReportInputService;

/**
 * InternalResouce - Provides a facility where services that are used internally can reside.
 *                   NOTE: This resource and all others in the "internal" package are not subject
 *                         to authorization (the ones in "rest" are). 
 */
@Path("int")
public class InternalResouce {
	
	private final static Logger log = LoggerFactory.getLogger(InternalResouce.class); 
	
	@EJB
	private RacecarUtilSingleton utilSingleton;

	//
	// Static data
	//
	private static String SUCCESS = "Success";
	
	//
	// Static methods.
	//

	//
	// Instance Data
	//

	//
	// Instance methods.
	//

	/**
	 * optionsResponse - Build the desired Response object for the @OPTIONS annotation
	 * @return
	 */
	@PostConstruct
	private void init(){
		log.debug("Initializing Internal Resource");
		getUtilSingleton();
	}
	
	private RacecarUtilSingleton getUtilSingleton() {
		if (utilSingleton == null){
			utilSingleton = (RacecarUtilSingleton) JNDILookupEJB
					.lookupBeanInGlobalContext(RacecarUtilSingleton.class.getName());
		}
		return utilSingleton;
	}
	private Response optionsResponse()
	{
		return Response.ok(SUCCESS)
				.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT")
				.header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Cache-Control, Authorization, Pragma, Expires")
				.header("Access-Control-Expose-Headers", "newToken")
				.header("Access-Control-Allow-Origin", "*")
				.build();
	}


	/**
	 * Generates extract candidate rows for long-inactive DRI rows
	 * NOTE:  Uses @POST because data is changed
	 */
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/dormantExtCand")
	public Response dormantExtCand()
	{
		String retStr = "";
		
		ReportInputService ris = new ReportInputService();
		retStr = ris.genDormantExtCand();

		return Response.ok(retStr).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/dormantExtCand")
	public Response optionsDormantExtCand() {
		return optionsResponse();
	}
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/doNothing")
	public Response doNothing(){
		
		int hitCount = utilSingleton.incrementHitCounter();
		
		return Response.ok("Called Singleton Successfully.  Hit Count: " + hitCount).build();
		
	}

}
