package com.pdi.service.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdi.data.abyd.dto.common.DNACellDTO;
import com.pdi.data.abyd.dto.common.EGFullDataDTO;
import com.pdi.data.abyd.dto.intGrid.IGCompDataDTO;
import com.pdi.data.abyd.dto.intGrid.IGFinalScoreDTO;
import com.pdi.data.abyd.dto.intGrid.IGInstDisplayData;
import com.pdi.data.abyd.dto.intGrid.IGLcNameDTO;
import com.pdi.data.abyd.dto.intGrid.IGSummaryDataDTO;
import com.pdi.listener.abyd.intGrid.IntegrationGridService;

/**
 * IntegrationGridResource - Provides the "middle-ware" for the new (React) version of the
 *                           Integration grid.  It allows us to use the same server-side
 *                           code and calls but with the new style front-end.
 */
@Path("igrid")
public class IntegrationGridResource {
	
	private static final Logger log = LoggerFactory.getLogger(IntegrationGridResource.class);

	//
	// Static data
	//
	private static String SUCCESS = "Success";

	//
	// Static methods.
	//

	//
	// Instance Data
	//
	IntegrationGridService igs = new IntegrationGridService();

	//
	// Instance methods.
	//

	/**
	 * optionsResponse - Build the desired Response object for the @OPTIONS annotation
	 * @return
	 */
	private Response optionsResponse()
	{
		return Response.ok(SUCCESS)
				.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT")
				.header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Cache-Control, Authorization, Pragma, Expires")
				.header("Access-Control-Expose-Headers", "newToken")
				.header("Access-Control-Allow-Origin", "*")
				.build();
	}


	/**
	 * getIgrid - For single parameter (lpId).
	 * 			  Get data for the first (summary) page of the IG.
	 * 			  A return of a null Response indicates a failure (should we fix this?)
	 *   This entry point can be used after the first time the IG is brought up.
	 * @param lpId
	 * @return JSON Int. Grid contents or null
	 */
	@GET
	@Path("/getIgrid/{lpId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getIgrid(@PathParam("lpId") String lpId)
	{
		log.debug("Recieved GET request for Integration Grid data.  Link param id: {}", lpId);
		return fetchIgrid(lpId, false);
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/getIgrid/{lpId}")
	public Response optionsGetIgrid1() {
		return optionsResponse();
	}

	/**
	 * getIgrid - For two single parameters (lpId, isStartup)
	 * 			  Get data for the first (summary) page of the IG.
	 * 			  A return of a null Response indicates a failure (should we fix this?)
	 *   This entry point (with an "iStartup" value of true) should be used when first
	 *   bringing up an Integration grid
	 * @param lpId
	 * @return JSON Int. Grid contents or null
	 */
	@GET
	@Path("/getIgrid/{lpId}/{isStartup}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getIgrid(@PathParam("lpId") String lpId, @PathParam("isStartup") boolean isStartup)
	{
		return fetchIgrid(lpId, isStartup);
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/getIgrid2/{lpId}/{isStartup}")
	public Response optionsGetIgrid() {
		return optionsResponse();
	}

	/**
	 * fetchIgrid - Does the work for the getIgrid restful entry points
	 * @param lpId
	 * @param isStartup
	 * @return
	 */
	private Response fetchIgrid(String lpId, boolean isStartup)
	{
		IGSummaryDataDTO dto = null;

		try
		{
			dto = igs.fetchIGSummary(lpId, isStartup);
		}
		catch (Exception e)
		{
			log.error("Exception caught fetching Int. Grid.  lpId={}, msg={}", lpId, e.getMessage());
			return null;
		}
		//System.out.println("<=======>\nfetchIgrid():  IGSummaryDataDTO for lp " + lpId + "\n" + dto.toString());

		// Convert to JSON
		ObjectMapper mapper = new ObjectMapper();
		String retStr = null;
		try
		{
			retStr = mapper.writeValueAsString(dto);
		}
		catch (JsonMappingException e)
		{
			log.error("getIgrid():  JsonMappingException.  msg={}", e.getMessage());
			retStr = null;
		}
		catch (JsonGenerationException e)
		{
			log.error("getIgrid():  JsonGenerationException.  msg={}", e.getMessage());
			retStr = null;
		}
		catch (IOException e)
		{
			log.error("getIgrid():  IOException.  msg={}", e.getMessage());
			retStr = null;
		}
		log.trace("<=======>\nfetchIgrid():  JSON generated (lpId={}):  {}",lpId, retStr);

		return Response.ok(retStr).header("Access-Control-Allow-Origin", "*").build();
	}


	/**
	 * getTestingScoreData -  An array of the score data for the testing column only
	 * @param partId - From the resident data
	 * @param dnaId - From the resident data
	 * @param cogsOn
	 * @param hasAlp - from the flag in the resident data
	 * @return -  An array of the score data for the testing column only
	 */
	@GET
	@Path("/getTestScores/{partId}/{dnaId}/{cogsOn}/{hasAlp}/{hasKf4d}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTestingScoreData(@PathParam("partId") String partId,
			                            @PathParam("dnaId") long dnaId,
			                            @PathParam("cogsOn") boolean cogsOn,
			                            @PathParam("hasAlp") boolean hasAlp,
    									@PathParam("hasKf4d") boolean hasKf4d)
	{
		List<DNACellDTO> list = new ArrayList<DNACellDTO>();
		String eStr = "getTestingScoreData():  ";

		try
		{
			list = igs.fetchTestingScoreData(partId, dnaId, cogsOn, hasAlp, hasKf4d);
		}
		catch (Exception e) {
			eStr += "Exception processing rater name.  msg=" + e.getMessage();
			System.out.println(eStr);
			e.printStackTrace();
			return null;
		}
		//System.out.println("<=======>\ngetTestingScoreData():  Test scores for ppt/dna=" + partId + "/" + dnaId + ":");
		//for (DNACellDTO cell : list)
		//{
		//	cell.toString();
		//}

		// Convert to JSON
		ObjectMapper mapper = new ObjectMapper();
		String retStr = null;
		try
		{
			retStr = mapper.writeValueAsString(list);
		}
		catch (JsonMappingException e)
		{
			eStr += "JsonMappingException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return null;
		}
		catch (JsonGenerationException e)
		{
			eStr += "JsonGenerationException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return null;
		}
		catch (IOException e)
		{
			eStr += "IOException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return null;
		}
		//System.out.println("<=======>\ngetTestingScoreData():  JSON generated (ppt/dna=" + partId + "/" + dnaId + "):  " + retStr);

		return Response.ok(retStr).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/getTestScores/{partId}/{dnaId}/{cogsOn}/{hasAlp}/{hasKf4d}")
	public Response optionsGetTestScores() {
		return optionsResponse();
	}


	/**
	 * Get the data for a competency (a line) on the Integration Grid.
	 *
	 * @param compId - The id of the competency in question
	 * @param partId - A participant id
	 * @param dnaId - A DNA Id
	 * @return A JSON representation of an IGCompDataDTO object
	 */
	@GET
	@Path("/getCompRow/{compId}/{partId}/{dnaId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCompRow(@PathParam("compId") long compId, @PathParam("partId") String partId, @PathParam("dnaId") long dnaId)
	{
		IGCompDataDTO dto = new IGCompDataDTO();
		String eStr = "getCompRow():  ";

		try
		{
			dto = igs.fetchCompRowData(compId, partId, dnaId);
		}
		catch (Exception e)
		{
			System.out.println("Exception caught fetching IG row data.  ppt/dna/comp=" + partId + "/" + dnaId + "/" + compId + ", msg=" + e.getMessage());
			return null;
		}
		//System.out.println("<=======>\ngetCompRow():  IGCompDataDTO for ppt/dna/comp=" + partId + "/" + dnaId + "/" + compId + ":\n" + dto.toString());

		// Convert to JSON
		ObjectMapper mapper = new ObjectMapper();
		String retStr = null;
		try
		{
			retStr = mapper.writeValueAsString(dto);
		}
		catch (JsonMappingException e)
		{
			eStr += "JsonMappingException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return null;
		}
		catch (JsonGenerationException e)
		{
			eStr += "JsonGenerationException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return null;
		}
		catch (IOException e)
		{
			eStr += "IOException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return null;
		}
		//System.out.println("<=======>\ngetCompRow():  JSON generated for ppt/dna/comp=" + partId + "/" + dnaId + "/" + compId + ":\n" + retStr);

		return Response.ok(retStr).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/getCompRow/{compId}/{partId}/{dnaId}")
	public Response optionsGetCompRow() {
		return optionsResponse();
	}


	/**
	 * Get the data for the "Testing" column on the Integration Grid.
	 *
	 * @param partId - A participant ID
	 * @param dnaId - The dna Id
	 * @return A IGInstDisplayData object
	 */
	@GET
	@Path("/getTestColumn/{partId}/{dnaId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTestCol(@PathParam("partId") String partId, @PathParam("dnaId") long dnaId)
	{
		IGInstDisplayData dto = null;
		String eStr = "getTestCol():  ";

		try
		{
			dto = igs.fetchTestingColumnData(partId, dnaId);
		}
		catch (Exception e)
		{
			log.error("Exception caught fetching IG test column data.  ppt/{} dna=/{}, msg={}", partId, dnaId, e.getMessage());
			e.printStackTrace();
			return null;
		}
		
		if (dto == null)
		{
			eStr += "No data fetched.";
			System.out.println(eStr);
			return null;
		}
		// Convert to JSON
		ObjectMapper mapper = new ObjectMapper();
		String retStr = null;
		try
		{
			retStr = mapper.writeValueAsString(dto);
		}
		catch (JsonMappingException e)
		{
			eStr += "JsonMappingException.  msg=" + e.getMessage();
			log.error(eStr);
			
			return null;
		}
		catch (JsonGenerationException e)
		{
			eStr += "JsonGenerationException.  msg=" + e.getMessage();
			log.error(eStr);
			return null;
		}
		catch (IOException e)
		{
			eStr += "IOException.  msg=" + e.getMessage();
			log.error(eStr);
			e.printStackTrace();
			return null;
		}
		//System.out.println("<=======>\ngetTestCol():  JSON generated for ppt/dna=" + partId + "/" + dnaId + "):\n" + retStr);

		return Response.ok(retStr).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/getTestColumn/{partId}/{dnaId}")
	public Response optionsGetTestColumn() {
		return optionsResponse();
	}


	/**
	 * Fetch the data for an Eval Guide.  This incidentally furnishes all of the
	 * data required for displaying a column in the Integration Grid (except for
	 * the "Testing" column... see the "getTestCol" entry point).
	 *
	 * This service is redundant to the one in the Eval Guide resource but it has
	 * been placed here for service call consistency.  We could probably move it
	 * (note that it has different call parameters from the one in the EG resource)
	 * there, but for now we'll leave it here so that all of the methods use the
	 * "iGrid" entry pattern.
	 *
	 * @param partId the participant id
	 * @param dnaId the dna Id
	 * @param moduleId the module Id
	 * @return A JSON string containing the full content of the EGFullData object
	 */
	// TODO Evaluate if this should be moved to the evalGuide service
	@GET
	@Path("/getEg/{partId}/{dnaId}/{moduleId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getEg(@PathParam("partId") String partId, @PathParam("dnaId") long dnaId, @PathParam("moduleId") long moduleId)
	{
		EGFullDataDTO dto = new EGFullDataDTO();
		String eStr = "getEg()-from IG:  ";

		try
		{
			dto = igs.fetchEvalGuide(partId, dnaId, moduleId);
		}
		catch (Exception e)
		{
			System.out.println("Exception caught fetching Eval. Guide data.  ppt/dna/mod=" + partId + "/" + dnaId + "/" + moduleId + ", msg=" + e.getMessage());
			return null;
		}
		//System.out.println("<=======>\ngetEg():  EGFullDataDTO for ppt/dna/mod=" + partId + "/" + dnaId + "/" + moduleId + ":\n" + dto.toString());

		// Convert to JSON
		ObjectMapper mapper = new ObjectMapper();
		String retStr = null;
		try
		{
			retStr = mapper.writeValueAsString(dto);
		}
		catch (JsonMappingException e)
		{
			eStr += "JsonMappingException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return null;
		}
		catch (JsonGenerationException e)
		{
			eStr += "JsonGenerationException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return null;
		}
		catch (IOException e)
		{
			eStr += "IOException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return null;
		}
		//System.out.println("<=======>\ngetEg():  JSON generated for ppt/dna/mod=" + partId + "/" + dnaId + "/" + moduleId + ":\n" + retStr);

		return Response.ok(retStr).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/getEg/{partId}/{dnaId}/{moduleId}")
	public Response optionsGetEg() {
		return optionsResponse();
	}


	/**
	 * Save the entered lead consultant name.
	 *
	 * @param nameData - An IGLCName object in JSON format
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/saveLeadName")
	public Response saveLeadName(String input)
	{
		ObjectMapper mapper = new ObjectMapper();
		IGLcNameDTO obj = null;
		String eStr = "saveLeadName():  ";
		try
		{
			//System.out.println("JSON str is =" + input);
			obj = mapper.readValue(input, IGLcNameDTO.class);
			igs.saveLcName(obj);
		}
		catch (JsonMappingException e)
		{
			eStr += "JsonMappingException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		catch (JsonParseException e)
		{
			eStr += "JsonParseException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		catch (IOException e)
		{
			eStr += "IOException - BAR data.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		catch (Exception e)
		{
			eStr += "Exception saving BAR data.  msg=" + e.getMessage();
			System.out.println(eStr);
			e.printStackTrace();
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}

		return Response.ok(SUCCESS).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/saveLeadName")
	public Response optionsSaveLeadName() {
		return optionsResponse();
	}

	/**
	 * Save a consultant-entered final score (index into a picklist) and
	 * also a possible note for a competency deviation in the IG.
	 *
	 * @param compScoreData - An IGFinalScoreDTO object in JSON format
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/saveFinalScore")
	public Response saveFinalScore(String input)
	{
		ObjectMapper mapper = new ObjectMapper();
		IGFinalScoreDTO obj = null;
		String eStr = "saveFinalScore():  ";
		try
		{
			//System.out.println("JSON str is =" + input);
			obj = mapper.readValue(input, IGFinalScoreDTO.class);
			igs.saveFinalScore(obj);
		}
		catch (JsonMappingException e)
		{
			eStr += "JsonMappingException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		catch (JsonParseException e)
		{
			eStr += "JsonParseException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		catch (IOException e)
		{
			eStr += "IOException - BAR data.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		catch (Exception e)
		{
			eStr += "Exception saving BAR data.  msg=" + e.getMessage();
			System.out.println(eStr);
			e.printStackTrace();
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}

		return Response.ok(SUCCESS).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/saveFinalScore")
	public Response optionsSaveFinalScore() {
		return optionsResponse();
	}


	/**
	 * Save the working notes for this Integration grid.
	 *
	 * @param candId - A participant Id
	 * @param dnaId - A DNA id
	 * @param note - The note text
	 */
	// TODO  Consider making an object; If the note text gets long it could surpass the maximum (though 65k is hard to surpass)
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/saveWorkingNote/{partId}/{dnaId}/{note}")
	public Response saveWorkingNote(@PathParam("partId") String partId, @PathParam("dnaId") long dnaId, @PathParam("note") String note)
	{
		String eStr = "saveWorkingNotes():  ";

		try
		{
			igs.saveWorkingNotes(partId, dnaId, note);
		}
		catch (Exception e)
		{
			eStr += "Exception on note save.  msg=" + e.getMessage();
			System.out.println(eStr);
			e.printStackTrace();
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}

		return Response.ok(SUCCESS).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/saveWorkingNote/{partId}/{dnaId}/{note}")
	public Response optionsSaveWorkingNote(@PathParam("partId") String partId, @PathParam("dnaId") long dnaId, @PathParam("note") String note) {
		return optionsResponse();
	}

	/**
	 * Set the Submitted flag for this Integration grid.
	 *
	 * @param candId - A participant Id
	 * @param dnaId - A DNA id
	 */
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/submitIg/{partId}/{dnaId}")
	public Response submitIg(@PathParam("partId") String partId, @PathParam("dnaId") long dnaId)
	{
		String eStr = "submitIg():  ";

		try
		{
			igs.submitIG(partId, dnaId);
		}
		catch (Exception e)
		{
			eStr += "Exception on note save.  msg=" + e.getMessage();
			System.out.println(eStr);
			e.printStackTrace();
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}

		return Response.ok(SUCCESS).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/submitIg/{partId}/{dnaId}")
	public Response optionsSubmitIg(@PathParam("partId") String partId, @PathParam("dnaId") long dnaId) {
		return optionsResponse();
	}

	// Not Implemented - the following methods in the service were not given a corresponding RESTful entry point
	//-------------------
	//public IGExtractDataDTO fetchExtractData(String candId, long dnaId)
	//	fetchExtractData is no longer referenced in the Anteater code; therefore, this is an anomalous reference.
	//

}
