package com.pdi.service.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.pdi.data.dto.Instrument;
import com.pdi.data.dto.NormGroup;
import com.pdi.data.dto.ProjectParticipantInstrument;
import com.pdi.data.dto.TestData;
import com.pdi.data.dto.TextPair;
import com.pdi.listener.portal.flex.PortalDelegateService;

/**
 * TestDataResource - Provides the "middle-ware" for the new (Angular) version of
 *                    "Test Data Entry" (Manual Score Entry).  It allows us to use the
 *                    same server-side code and calls but with the new style front-end.
 * Encapsulates calls to the PortalDelegateService
 */
@Path("testData")
public class TestDataResource {

	//
	// Static data
	//
	private static String SUCCESS = "Success";

	//
	// Static methods.
	//

	//
	// Instance Data
	//
	PortalDelegateService pds = new PortalDelegateService();

	//
	// Instance methods.
	//

	/**
	 * optionsResponse - Build the desired Response object for the @OPTIONS annotation
	 * @return
	 */
	private Response optionsResponse()
	{
		return Response.ok(SUCCESS)
				.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT")
				.header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Cache-Control, Authorization, Pragma, Expires")
				.header("Access-Control-Expose-Headers", "newToken")
				.header("Access-Control-Allow-Origin", "*")
				.build();
	}


	/**
	 * getNormsForInstrument - Get the norms available for the specified instrument code.
	 * 			  A return of a null Response indicates a failure (should we fix this?)
	 * @param instCode
	 * @return Array of NormGroup objects in JSON
	 */
	@GET
	@Path("/normsForInst/{instCode}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getNormsForInstrument(@PathParam("instCode") String instCode)
	{
		ArrayList<NormGroup> list = null;

		try
		{
			list = pds.getNormsByInstrument(instCode);
		}
		catch (Exception e)
		{
			System.out.println("Exception caught fetching norms for instrument " + instCode + ".  msg=" + e.getMessage());
			return null;
		}
		//System.out.println("<=======>\fetchNormsForInstrument(" + instCode + "):\n");
		//for(NormGroup ng : list)  {  System.out.println("----" + ng.toString() + "\n");  }

		// Convert to JSON
		ObjectMapper mapper = new ObjectMapper();
		String retStr = null;
		try
		{
			retStr = mapper.writeValueAsString(list);
		}
		catch (JsonMappingException e)
		{
			System.out.println("getNormsForInstrument():  JsonMappingException.  msg=" + e.getMessage());
			retStr = null;
		}
		catch (JsonGenerationException e)
		{
			System.out.println("getNormsForInstrument():  JsonGenerationException.  msg=" + e.getMessage());
			retStr = null;
		}
		catch (IOException e)
		{
			System.out.println("getNormsForInstrument():  IOException.  msg=" + e.getMessage());
			retStr = null;
		}
		//System.out.println("<=======>\nfetchNormsForInstrument(" + instCode + "):  " + retStr);

		return Response.ok(retStr).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/normsForInst/{instCode}")
	public Response optionsGetNormsForInstrument() {
		return optionsResponse();
	}


	/**
	 * getAllInstrumentList - Get a list of all available instruments.    A return of
	 *                        a null Response indicates a failure (should we fix this?)
	 * @return Array of Instrument objects in JSON
	 */
	@GET
	@Path("/allInstList")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllInstrumentList()
	{
		ArrayList<Instrument> list = null;

		try
		{
			list = pds.getAllInstrumentList();
		}
		catch (Exception e)
		{
			System.out.println("Exception caught fetching instrument list.  msg=" + e.getMessage());
			return null;
		}
		//System.out.println("<=======>\ngetAllInstrumentList():\n");
		//for(Instrument ll : list)  {  System.out.println("----" + ll.toString() + "\n");  }

		// Convert to JSON
		ObjectMapper mapper = new ObjectMapper();
		String retStr = null;
		try
		{
			retStr = mapper.writeValueAsString(list);
		}
		catch (JsonMappingException e)
		{
			System.out.println("getAllInstrumentList():  JsonMappingException.  msg=" + e.getMessage());
			retStr = null;
		}
		catch (JsonGenerationException e)
		{
			System.out.println("getAllInstrumentList():  JsonGenerationException.  msg=" + e.getMessage());
			retStr = null;
		}
		catch (IOException e)
		{
			System.out.println("getAllInstrumentList():  IOException.  msg=" + e.getMessage());
			retStr = null;
		}
		//System.out.println("<=======>\ngetAllInstrumentList():  " + retStr);

		return Response.ok(retStr).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/allInstList")
	public Response optionsGetAllInstrumentList() {
		return optionsResponse();
	}


	/**
	 * getScorableInstrumentList - Get a list of available scorable instruments.  A return
	 *                             of a null Response indicates a failure (should we fix this?)
	 * @return Array of Instrument objects in JSON
	 */
	@GET
	@Path("/scorableInstList")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getScorableInstrumentList()
	{
		ArrayList<Instrument> list = null;

		try
		{
			list = pds.getAllInstrumentList();
		}
		catch (Exception e)
		{
			System.out.println("Exception caught fetching instrument list.  msg=" + e.getMessage());
			return null;
		}
		//System.out.println("<=======>\ngetScorableInstrumentList():\n");
		//for(Instrument ll : list)  {  System.out.println("----" + ll.toString() + "\n");  }

		// Convert to JSON
		ObjectMapper mapper = new ObjectMapper();
		String retStr = null;
		try
		{
			retStr = mapper.writeValueAsString(list);
		}
		catch (JsonMappingException e)
		{
			System.out.println("getScorableInstrumentList():  JsonMappingException.  msg=" + e.getMessage());
			retStr = null;
		}
		catch (JsonGenerationException e)
		{
			System.out.println("getScorableInstrumentList():  JsonGenerationException.  msg=" + e.getMessage());
			retStr = null;
		}
		catch (IOException e)
		{
			System.out.println("getScorableInstrumentList():  IOException.  msg=" + e.getMessage());
			retStr = null;
		}
		//System.out.println("<=======>\ngetScorableAllInstrumentList():  " + retStr);

		return Response.ok(retStr).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/scorableInstList")
	public Response optionsGetScorableInstrumentList() {
		return optionsResponse();
	}


	/**
	 * getLastPartScores - Get a list of the latest scores available for the participant.
	 *                     A return of a null Response indicates a failure (should we fix this?)
	 * @return Array of ProjectParticipantInstrument objects in JSON
	 */
	@GET
	@Path("/lastPartScores/{pptId}/{projId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getLastPartScores(@PathParam("pptId") String pptId, @PathParam("projId") String projId)
	{
		ArrayList<ProjectParticipantInstrument> list = null;

		try
		{
			list = pds.getLatestScoresForPart(pptId, projId);
		}
		catch (Exception e)
		{
			System.out.println("Exception caught fetching last participant score list (ppt=" + pptId + ", proj=" + projId + ".  msg=" + e.getMessage());
			return null;
		}
		//System.out.println("<=======>\ngetLastPartScores(" + pptId + ", " + projId + "):\n");
		//for(ProjectParticipantInstrument ll : list)  {  System.out.println("----" + ll.toString() + "\n");  }

		// Convert to JSON
		ObjectMapper mapper = new ObjectMapper();
		String retStr = null;
		try
		{
			retStr = mapper.writeValueAsString(list);
		}
		catch (JsonMappingException e)
		{
			System.out.println("getLastPartScores():  JsonMappingException.  msg=" + e.getMessage());
			retStr = null;
		}
		catch (JsonGenerationException e)
		{
			System.out.println("getLastPartScores():  JsonGenerationException.  msg=" + e.getMessage());
			retStr = null;
		}
		catch (IOException e)
		{
			System.out.println("getLastPartScores():  IOException.  msg=" + e.getMessage());
			retStr = null;
		}
		//ProjectParticipantInstrumentSystem.out.println("<=======>\ngetScorableAllInstrumentList():  " + retStr);

		return Response.ok(retStr).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/lastPartScores/{pptId}/{projId}")
	public Response optionsGetLastPartScores() {
		return optionsResponse();
	}


	/**
	 * getCogsDone - Get the cogsDone flag for the TestData object
	 * 				 passed in and return it in another TestData object
	 * @param input
	 * @return
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getCogsDone/{pptId}/{projId}")
	public Response getCogsDone(@PathParam("pptId") String pptId, @PathParam("projId") String projId)
	{
		String eStr = "getCogsDone():  ";

		// Create/populate the input object
		TestData inp = new TestData();
		long ppt = 0;
		long proj = 0;
		try
		{
			ppt = Long.valueOf(pptId);
			proj = Long.valueOf(projId);
		}
		catch (NumberFormatException e)
		{
			eStr += "NumberFormatException: the value for participant ID (" + pptId + ") or project ID (" + projId + ") is not a proper number.  msg=" + e.getMessage();
			System.out.println(eStr);
			e.printStackTrace();
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		if (ppt == 0 || proj == 0)
		{
			eStr += "A value of zero for participant ID (" + pptId + ") or project ID (" + projId + ") is not allowed.";
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		inp.setParticipantId(ppt);
		inp.setProjectId(proj);
		
		// Process
		TestData out = null;
		try
		{
			out = pds.getCogsDoneFlag(inp);
		}
		catch (Exception e)
		{
			eStr += "Exception saving DRI data.  msg=" + e.getMessage();
			System.out.println(eStr);
			e.printStackTrace();
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}

		// Convert to JSON
		ObjectMapper outMapper = new ObjectMapper();
		String retStr = null;
		try
		{
			retStr = outMapper.writeValueAsString(out);
		}
		catch (JsonMappingException e)
		{
			eStr += "JsonMappingException.  msg=" + e.getMessage();
			System.out.println(eStr);
			e.printStackTrace();
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		catch (JsonGenerationException e)
		{
			eStr += "JsonGenerationException.  msg=" + e.getMessage();
			System.out.println(eStr);
			e.printStackTrace();
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		catch (IOException e)
		{
			eStr += "IOException.  msg=" + e.getMessage();
			System.out.println(eStr);
			e.printStackTrace();
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		//ProjectParticipantInstrumentSystem.out.println("<=======>\ngetScorableAllInstrumentList():  " + retStr);

		return Response.ok(retStr).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/getCogsDone/{pptId}/{projId}")
	public Response optionsGetCogsDone() {
		return optionsResponse();
	}


	/**
	 * saveFinalScores -  Save some scores passed in a ProjectParticipantInstrument object.
	 * @param ppi A ProjectParticipantInstrument object in JSON
	 * @return
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/saveLastScores")
	public Response saveFinalScores(String ppi)
	{
		ObjectMapper mapper = new ObjectMapper();
		ProjectParticipantInstrument obj = null;
		String eStr = "saveFinalScore():  ";
		try
		{
			System.out.println("JSON str is =" + ppi);
			obj = mapper.readValue(ppi, ProjectParticipantInstrument.class);
			pds.addUpdateParticipantProjectInstrument(obj, null);
		}
		catch (JsonMappingException e)
		{
			eStr += "JsonMappingException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		catch (JsonParseException e)
		{
			eStr += "JsonParseException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		catch (IOException e)
		{
			eStr += "IOException - PPI data.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		catch (Exception e)
		{
			eStr += "Exception saving PPI (score) data.  msg=" + e.getMessage();
			System.out.println(eStr);
			e.printStackTrace();
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}

		return Response.ok(SUCCESS).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/saveLastScores")
	public Response optionsSaveFinalScores() {
		return optionsResponse();
	}


	/**
	 * saveCogsDone -  Save cogs done flag using a passed in TestData object.
	 * @param td A TestData object in JSON
	 * @return
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/saveCogsDone")
	public Response saveCogsDone(String td)
	{
		ObjectMapper mapper = new ObjectMapper();
		TestData obj = null;
		String eStr = "saveCogsDone():  ";
		try
		{
			System.out.println("JSON str is =" + td);
			obj = mapper.readValue(td, TestData.class);
			pds.addUpdateCogsDoneFlag(obj);
		}
		catch (JsonMappingException e)
		{
			eStr += "JsonMappingException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		catch (JsonParseException e)
		{
			eStr += "JsonParseException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		catch (IOException e)
		{
			eStr += "IOException - TestData.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		catch (Exception e)
		{
			eStr += "Exception saving TestData (cogs flag) data.  msg=" + e.getMessage();
			System.out.println(eStr);
			e.printStackTrace();
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}

			return Response.ok(SUCCESS).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/saveCogsDone")
	public Response optionsSaveCogsDone() {
		return optionsResponse();
	}


	/**
	 * getEquivalencyLists - Get a list of equvalency istruments
	 * @return Map of ArrayLists of TextPair objects in JSON
	 */
	@GET
	@Path("/equivalencyLists")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getEquivalencyLists()
	{
		HashMap<String, ArrayList<TextPair>> map = null;

		try
		{
			map = pds.getEquivalencyLists();
		}
		catch (Exception e)
		{
			System.out.println("Exception caught fetching equivalency lists.  msg=" + e.getMessage());
			return null;
		}
		//System.out.println("<=======>\ngetEquivalencyLists():\n");
		//Iterator<Map.Entry<String, ArrayList<TextPair>>> eqIt = map.entrySet().iterator();
		//while (eqIt.hasNext())
		//{
		//	Map.Entry<String, ArrayList<TextPair>> ent = (Map.Entry<String, ArrayList<TextPair>>)eqIt.next();
		//	System.out.println("-- Inst=" + ent.getKey());
		//	List<TextPair> list = ent.getValue();
		//	for(TextPair ll : list)  {  System.out.println("----" + ll.toString() + "\n");  }
		//}

		// Convert to JSON
		ObjectMapper mapper = new ObjectMapper();
		String retStr = null;
		try
		{
			retStr = mapper.writeValueAsString(map);
		}
		catch (JsonMappingException e)
		{
			System.out.println("getEquivalencyLists():  JsonMappingException.  msg=" + e.getMessage());
			retStr = null;
		}
		catch (JsonGenerationException e)
		{
			System.out.println("getEquivalencyLists():  JsonGenerationException.  msg=" + e.getMessage());
			retStr = null;
		}
		catch (IOException e)
		{
			System.out.println("getEquivalencyLists():  IOException.  msg=" + e.getMessage());
			retStr = null;
		}
		//System.out.println("<=======>\ngetEquivalencyLists() output:  " + retStr);

		return Response.ok(retStr).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/equivalencyLists")
	public Response optionsGetEquivalencyLists() {
		return optionsResponse();
	}


	/**
	 * getEqivalentScore - Get the equivalent score for the inputs
	 * @return The eqivalent score in JSON (just the score)
	 */
	@GET
	@Path("/equivalentScore/{oldInst}/{score}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getEqivalentScore(@PathParam("oldInst") String oldInst, @PathParam("score") String score)
	{
		String ret = null;

		try
		{
			ret = pds.getEquivalentScore(oldInst, score);
		}
		catch (Exception e)
		{
			System.out.println("Exception caught fetching equivalent scoret (oldInst=" + oldInst + ", score=" + score + ".  msg=" + e.getMessage());
			return null;
		}
		//System.out.println("<=======>\ngetEqivalentScore(" + oldInst + ", " + score + "):  " + ret);

		// Convert to JSON
		ObjectMapper mapper = new ObjectMapper();
		String retStr = null;
		try
		{
			retStr = mapper.writeValueAsString(ret);
		}
		catch (JsonMappingException e)
		{
			System.out.println("getEqivalentScore():  JsonMappingException.  msg=" + e.getMessage());
			retStr = null;
		}
		catch (JsonGenerationException e)
		{
			System.out.println("getEqivalentScore():  JsonGenerationException.  msg=" + e.getMessage());
			retStr = null;
		}
		catch (IOException e)
		{
			System.out.println("getEqivalentScore():  IOException.  msg=" + e.getMessage());
			retStr = null;
		}
		//System.out.println("<=======>\ngetEqivalentScore():  " + retStr);

		return Response.ok(retStr).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/equivalentScore/{oldInst}/{score}")
	public Response optionsGetEqivalentScore() {
		return optionsResponse();
	}
	
}
