package com.pdi.service.rest;

import java.io.IOException;

//import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
//import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.pdi.data.abyd.dto.reportInput.ReportInputDTO;
import com.pdi.data.abyd.dto.reportInput.ReportInputNoteHolderDTO;
import com.pdi.data.abyd.dto.setup.ReportModelStructureDTO;
import com.pdi.data.dto.AttachmentHolder;
import com.pdi.data.dto.NoteHolder;
import com.pdi.data.dto.Participant;
import com.pdi.data.dto.Project;
import com.pdi.listener.abyd.reportInput.ReportInputService;

/**
 * ReportInputResource - Provides the "middle-ware" for the new (React) version of the
 *                       Dashboard Report Input functionality.  It allows us to use the
 *                       same server-side code and calls but with the new style front-end.
 */
@Path("dri")
public class ReportInputResource
{
	//
	// Static data
	//
	private static String SUCCESS = "Success";

//	@Context
//	private ServletContext context;

	//
	// Static methods.
	//

	//
	// Instance Data
	//
	ReportInputService ris = new ReportInputService();

	//
	// Instance methods.
	//

	/**
	 * optionsResponse - Build the desired Response object for the @OPTIONS annotation
	 * @return
	 */
	private Response optionsResponse()
	{
		return Response.ok(SUCCESS)
				.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT")
				.header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Cache-Control, Authorization, Pragma, Expires")
				.header("Access-Control-Expose-Headers", "newToken")
				.header("Access-Control-Allow-Origin", "*")
				.build();
	}

	/**
	 * keepAlive - Used to keep the session alive
	 */
	@GET
	@Path("/keepAlive")
	@Produces(MediaType.TEXT_PLAIN)
	public Response keepAlive()
	{
		return Response.ok(SUCCESS).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/keepAlive")
	public Response optionskeepAlive() {
		return optionsResponse();
	}


	/**
	 * getReportInput - Get DRI data for a particular participant and DNA.the first (summary) page of the IG.
	 * 			  		A return of a null Response indicates a failure (should we fix this?)
	 * @param pptId
	 * @param dnaId
	 * @return JSON DRI contents or null
	 */
	@GET
	@Path("/getRptInput/{pptId}/{dnaId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getReportInput(@PathParam("pptId") String pptId, @PathParam("dnaId") long dnaId)
	{
		//System.out.println("<=======>\nStart fetch...");
		ReportInputDTO dto = new ReportInputDTO();

		try
		{
			dto = ris.fetchReportInput(pptId, dnaId);
		}
		catch (Exception e)
		{
			System.out.println("getReportInput(): Exception caught fetching DRI info.  ppt=" + pptId + ", dnaId=" + dnaId + ", msg=" + e.getMessage());
			return null;
		}
		//System.out.println("<=======>\nEnd getReportInput():  IGSummaryDataDTO for ppt=" + pptId + ", dnaId=" + dnaId + "\n" + (dto == null ? "null" : dto.toString()));

		// Convert to JSON
		ObjectMapper mapper = new ObjectMapper();
		String retStr = null;
		String eStr = "getRptInput:  ";
		try
		{
			retStr = mapper.writeValueAsString(dto);
		}
		catch (JsonMappingException e)
		{
			System.out.println(eStr + "JsonMappingException.  msg=" + e.getMessage());
			retStr = null;
		}
		catch (JsonGenerationException e)
		{
			System.out.println(eStr + "JsonGenerationException.  msg=" + e.getMessage());
			retStr = null;
		}
		catch (IOException e)
		{
			System.out.println(eStr + "IOException.  msg=" + e.getMessage());
			retStr = null;
		}
		//System.out.println("<=======>\ngetReportInput():  JSON generated (ppt=" + pptId + ", dnaId=" + dnaId + "):\n" + retStr);

		return Response.ok(retStr).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/getRptInput/{pptId}/{dnaId}")
	public Response optionsGetRptInput() {
		return optionsResponse();
	}


	/**
	 * getParticipantNotes - Get participant notes for a DRI.
	 * 			  A return of a null Response indicates a failure (should we fix this?)
	 * 		The Original Anteater sent in a NoteHolder object, but the participant ID is the only thing used there.
	 * 		This passes the participant ID only and the code creates a a new NoteHolder, simplifying the porting process.
	 *
	 * NOTE:  This is only a small part of all of the notes returned.  It appears that these are the notes entered in
	 * 		  Manual/Test data Entry only...
	 *
	 * @param pptId
	 * @return JSON NoteHolder contents or null
	 */
	@GET
	@Path("/getPptNotes/{pptId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getParticipantNotes(@PathParam("pptId") String pptId)
	{
		NoteHolder inp = new NoteHolder();
		inp.setParticipantId(pptId);

		NoteHolder dto = new NoteHolder();

		try
		{
			dto = ris.fetchParticipantNotes(inp);
		}
		catch (Exception e)
		{
			System.out.println("getParticipantNotes():  Exception fetching participant notes.  ppt=" + pptId + ", msg=" + e.getMessage());
			return null;
		}
		//System.out.println("<=======>\ngetParticipantNotes():  Noteholder for ppt=" + pptId + "\n" + (dto == null ? "null" : dto.toString()));

		// Convert to JSON
		ObjectMapper mapper = new ObjectMapper();
		String retStr = null;
		String eStr = "getPptNotes:  ";
		try
		{
			retStr = mapper.writeValueAsString(dto);
		}
		catch (JsonMappingException e)
		{
			System.out.println(eStr + "JsonMappingException.  msg=" + e.getMessage());
			retStr = null;
		}
		catch (JsonGenerationException e)
		{
			System.out.println(eStr + "JsonGenerationException.  msg=" + e.getMessage());
			retStr = null;
		}
		catch (IOException e)
		{
			System.out.println(eStr + "IOException.  msg=" + e.getMessage());
			retStr = null;
		}
		//System.out.println("<=======>\ngetParticipantNotes():  JSON generated (ppt=" + pptId + "):\n" + retStr);

		return Response.ok(retStr).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/getPptNotes/{pptId}")
	public Response optionsGetPptNotes() {
		return optionsResponse();
	}


	/**
	 * 	getParticipantAttachments - Get participant attachments.
	 * 			  A return of a null Response indicates a failure (should we fix this?)
	 * 		The Original Anteater sent in an AttachmentHolder object, but the participant ID and the project ID are
	 * 		the only things used from there.  This rest call passes the participant ID and the project ID  and the code
	 * 		creates a a new AttachmentHolder, simplifying the porting process.
	 *
	 * NOTE:  This retrieves the PALMS participant and project attachments only; it appears that IDP attachments are NOT
	 * 		  a part of this retrieval.

	 * @param pptId
	 * @param projId
	 * @return
	 */
	@GET
	@Path("/getPptAttachments/{pptId}/{projId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getParticipantAttachments(@PathParam("pptId") String pptId, @PathParam("projId") String projId)
	{
		Participant ppt = new Participant(pptId);
		Project proj = new Project(projId);
		AttachmentHolder inp = new AttachmentHolder();
		inp.setParticipant(ppt);
		inp.setProject(proj);

		AttachmentHolder dto = new AttachmentHolder();
		try
		{
			dto = ris.fetchParticipantAttachments(inp);
		}
		catch (Exception e)
		{
			System.out.println("getParticipantAttachments():  Exception fetching attachments.  ppt=" + pptId + ", proj=" + projId + ", msg=" + e.getMessage());
			return null;
		}
		//System.out.println("<=======>\ngetParticipantAttachments():  AttachmentHolder for ppt=" + pptId + ", proj" + projId + "\n" + (dto == null ? "null" : dto.toString()));

		// Convert to JSON
		ObjectMapper mapper = new ObjectMapper();
		String retStr = null;
		String eStr = "getParticipantAttachments:  ";
		try
		{
			retStr = mapper.writeValueAsString(dto);
		}
		catch (JsonMappingException e)
		{
			System.out.println(eStr + "JsonMappingException.  msg=" + e.getMessage());
			retStr = null;
		}
		catch (JsonGenerationException e)
		{
			System.out.println(eStr + "JsonGenerationException.  msg=" + e.getMessage());
			retStr = null;
		}
		catch (IOException e)
		{
			System.out.println(eStr + "IOException.  msg=" + e.getMessage());
			retStr = null;
		}
		//System.out.println("<=======>\ngetParticipantAttachments():  JSON generated (ppt=" + pptId + ", projId=" + projId + "):\n" + retStr);

		return Response.ok(retStr).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/getPptAttachments/{pptId}/{projId}")
	public Response optionsGetPptAttachments() {
		return optionsResponse();
	}


	/**
	 * getRptInputNotes - Gets a couple of different types of notes for the participant
	 * @param rptInpId - This should be available in the DRI DTO sent
	 * @return
	 */
	@GET
	@Path("/getRptInputNotes/{rptInpId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRptInputNotes(@PathParam("rptInpId") long rptInpId)
	{
		ReportInputNoteHolderDTO dto = new ReportInputNoteHolderDTO();
		try
		{
			dto = ris.fetchReportInputNoteData(rptInpId);
		}
		catch (Exception e)
		{
			System.out.println("getRptInputNotes():  Exception fetching notes.  rptInpId=" + rptInpId + ", msg=" + e.getMessage());
			return null;
		}
		//System.out.println("<=======>\ngetRptInputNotes():  ReportInputNoteHolderDTO for rptInpId=" + rptInpId + "\n" + (dto == null ? "null" : dto.toString()));

		// Convert to JSON
		ObjectMapper mapper = new ObjectMapper();
		String retStr = null;
		String eStr = "getRptInputNotes:  ";
		try
		{
			retStr = mapper.writeValueAsString(dto);
		}
		catch (JsonMappingException e)
		{
			System.out.println(eStr + "JsonMappingException.  msg=" + e.getMessage());
			retStr = null;
		}
		catch (JsonGenerationException e)
		{
			System.out.println(eStr + "JsonGenerationException.  msg=" + e.getMessage());
			retStr = null;
		}
		catch (IOException e)
		{
			System.out.println(eStr + "IOException.  msg=" + e.getMessage());
			retStr = null;
		}
		//System.out.println("<=======>\ngetRptInputNotes():  JSON generated (rptInpId=" + rptInpId + "):\n" + retStr);

		return Response.ok(retStr).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/getRptInputNotes/{rptInpId}")
	public Response optionsGetRptInputNotes() {
		return optionsResponse();
	}


	@GET
	@Path("/getRptModelWithScores/{dnaId}/{pptId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRptModelWithScores(@PathParam("dnaId") long dnaId, @PathParam("pptId") String pptId)
	{
		ReportModelStructureDTO dto = new ReportModelStructureDTO();
		try
		{
			dto = ris.fetchReportModelWithScores(dnaId, pptId);
		}
		catch (Exception e)
		{
			System.out.println("getRptModelWithScores():  Exception fetching notes.  dnaId=" + dnaId + ", pptId=" + pptId + ", msg=" + e.getMessage());
			return null;
		}
		//System.out.println("<=======>\ngetRptModelWithScores():  ReportModelStructureDTO for dnaId=" + dnaId + ", pptId=" + pptId + "\n" + (dto == null ? "null" : dto.toString()));

		// Convert to JSON
		ObjectMapper mapper = new ObjectMapper();
		String retStr = null;
		String eStr = "getRptModelWithScores:  ";
		try
		{
			retStr = mapper.writeValueAsString(dto);
		}
		catch (JsonMappingException e)
		{
			System.out.println(eStr + "JsonMappingException.  msg=" + e.getMessage());
			retStr = null;
		}
		catch (JsonGenerationException e)
		{
			System.out.println(eStr + "JsonGenerationException.  msg=" + e.getMessage());
			retStr = null;
		}
		catch (IOException e)
		{
			System.out.println(eStr + "IOException.  msg=" + e.getMessage());
			retStr = null;
		}
		//System.out.println("<=======>\ngetRptModelWithScores():  JSON generated (dnaId=" + dnaId + ", pptId=" + pptId + "):\n" + retStr);

		return Response.ok(retStr).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/getRptModelWithScores/{dnaId}/{pptId}")
	public Response optionsGetRptModelWithScores() {
		return optionsResponse();
	}


	/**
	 * Save the Dashboard Report Input data
	 * @param input - JSON string of DRI data
	 * @return
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/updateRptInput")
	public Response updateRptInput(String input)
	{
		//System.out.println("<=======>\nupdateRptInput():  JSON=" + input);
		//System.out.println("<=======>\nStart updateRptInput()");
		ObjectMapper mapper = new ObjectMapper();
		ReportInputDTO obj = null;
		String eStr = "saveRptInput:  ";
		try
		{
			obj = mapper.readValue(input, ReportInputDTO.class);
			ris.updateReportInput(""+obj.getLpId(), obj);
			//System.out.println("<=======>\nEnd updateRptInput()");
		}
		catch (JsonMappingException e)
		{
			eStr += "JsonMappingException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		catch (JsonParseException e)
		{
			eStr += "JsonParseException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		catch (IOException e)
		{
			eStr += "IOException - BAR data.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		catch (Exception e)
		{
			eStr += "Exception saving DRI data.  msg=" + e.getMessage();
			System.out.println(eStr);
			e.printStackTrace();
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}

		return Response.ok(SUCCESS).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/updateRptInput")
	public Response optionsUpdateRptInput() {
		return optionsResponse();
	}


	// New IDs.  Need to fetch the notes once updated
	// Could do that here as the service returns it, but maintaining parallelism and sending back 'Success' string
	/**
	 * Update the DRI notes
	 * 	NOTE:  The original service code sent back a ReportInputNoteHolderDTO object; that
	 * 		   was in case any of the notes were new, the newly minted ids would be available.
	 * 		   This was written to parallel the operations everywhere else, so this operation
	 * 		   should be followed immediately (and sequentially) with a call to getPptNotes.
	 * 	NOTE: Updates only DRI notes (types 0 & 1) not the report note (type 3)
	 * @param input - JSON representation of a ReportInputNoteHolderDTO object.  A new note
	 *                is a new ReportInputNoteDTO object (id=0) in the inputNotesArray
	 * @return
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/updateRptNotes")
	public Response updateRptInputNotes(String input)
	{
		System.out.println("<=======>\nupdateRptInputNotes():  JSON=" + input);
		ObjectMapper mapper = new ObjectMapper();
		ReportInputNoteHolderDTO obj = null;
		String eStr = "updateRptInputNotes:  ";
		try
		{
			obj = mapper.readValue(input, ReportInputNoteHolderDTO.class);
			ris.updateReportInputNoteData(obj);
		}
		catch (JsonMappingException e)
		{
			eStr += "JsonMappingException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		catch (JsonParseException e)
		{
			eStr += "JsonParseException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		catch (IOException e)
		{
			eStr += "IOException - BAR data.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		catch (Exception e)
		{
			eStr += "Exception saving DRI notes.  msg=" + e.getMessage();
			System.out.println(eStr);
			e.printStackTrace();
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}

		return Response.ok(SUCCESS).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/updateRptNotes")
	public Response optionsUpdateRptNotes() {
		return optionsResponse();
	}

}