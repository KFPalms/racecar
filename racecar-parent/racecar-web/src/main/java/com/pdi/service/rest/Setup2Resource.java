package com.pdi.service.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.pdi.data.util.language.LanguageItemDTO;
import com.pdi.data.util.language.LanguageList;
import com.pdi.listener.abyd.reportInput.ReportInputService;
import com.pdi.listener.abyd.setup.Setup2Service;
import com.pdi.data.abyd.dto.setup.ReportModelCompChildDTO;
import com.pdi.data.abyd.dto.setup.ReportModelStructureDTO;
import com.pdi.data.abyd.dto.setup.ReportModelTranslationDTO;
import com.pdi.data.abyd.dto.setup.ReportNoteDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportLabelsDTO;
import com.pdi.data.abyd.dto.setup.Setup2ReportOptionsDTO;

import com.pdi.data.util.language.LanguageItemDTO;

/**
 * Setup2Resource - Provides the "middle-ware" for the new (Angular) version of
 *                  required Setup functionality.  It allows us to use the same
 *                  server-side code and calls but with the new style front-end.
 */
//NOTE:  We are converting this "just in time".  The service methods for the as-yet unconverted services
//       are present but commented out.  These will eventually be replaces by the resource endpoints that
//       call them as we need them

@Path( "setup2")
public class Setup2Resource
{
	//
	// Static data
	//
	private static String SUCCESS = "Success";

	//
	// Instance Data
	//
	Setup2Service setup2Service = new Setup2Service();

	/**
	 * optionsResponse - Build the desired Response object for the @OPTIONS
	 * annotation
	 *
	 * @return
	 */
	private Response optionsResponse() {
		return Response.ok(SUCCESS).header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT")
				.header("Access-Control-Allow-Headers",
						"Access-Control-Allow-Headers, Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Cache-Control, Authorization, Pragma, Expires")
				.header("Access-Control-Expose-Headers", "newToken").header("Access-Control-Allow-Origin", "*").build();
	}


	@GET
	@Path("/fetchReportModel/{dnaId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response fetchReportModel(@PathParam("dnaId") long dnaId)
	{
		ReportModelStructureDTO dto = null;

		try {
			dto = setup2Service.fetchReportModel(dnaId);
		} catch (Exception e) {
			System.out.println("Exception caught fetchReportModel.  dnaId=" + dnaId + ", msg=" + e.getMessage());
			return null;
		}
		// System.out.println("<=======>\fetchEntryState(): EntryStateDTO for dnaId
		// " + dnaId + "\n" + dto.toString());

		// Convert to JSON
		ObjectMapper mapper = new ObjectMapper();
		String retStr = null;
		try {
			retStr = mapper.writeValueAsString(dto);
		} catch (JsonMappingException e) {
			System.out.println("fetchReportModel():  JsonMappingException.  msg=" + e.getMessage());
			retStr = null;
		} catch (JsonGenerationException e) {
			System.out.println("fetchReportModel():  JsonGenerationException.  msg=" + e.getMessage());
			retStr = null;
		} catch (IOException e) {
			System.out.println("fetchReportModel():  IOException.  msg=" + e.getMessage());
			retStr = null;
		}
		// System.out.println("<=======>\fetchReportModel(): JSON generated
		// (dnaId=" + dnaId + "): " + retStr);

		return Response.ok(retStr).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/fetchReportModel/{dnaId}")
	public Response optionsFetchReportModel(@PathParam("dnaId") long dnaId) {
		return optionsResponse();
	}


	@GET
	@Path("/fetchReportModelCompetencyChildren/{dnaId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response fetchReportModelCompetencyChildren(@PathParam("dnaId") long dnaId)
	{
		List<ReportModelCompChildDTO> list = new ArrayList<ReportModelCompChildDTO>();
		String eStr = "fetchReportModelCompetencyChildren():  ";

		try
		{
			list = setup2Service.fetchReportModelCompetencyChildren(dnaId);
		}
		catch (Exception e) {
			eStr += "Exception processing rater name.  msg=" + e.getMessage();
			System.out.println(eStr);
			e.printStackTrace();
			return null;
		}

		// Convert to JSON
		ObjectMapper mapper = new ObjectMapper();
		String retStr = null;
		try
		{
			retStr = mapper.writeValueAsString(list);
		}
		catch (JsonMappingException e)
		{
			eStr += "JsonMappingException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return null;
		}
		catch (JsonGenerationException e)
		{
			eStr += "JsonGenerationException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return null;
		}
		catch (IOException e)
		{
			eStr += "IOException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return null;
		}

		return Response.ok(retStr).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/fetchReportModelCompetencyChildren/{dnaId}")
	public Response optionsFetchReportModelCompetencyChildren(@PathParam("dnaId") long dnaId) {
		return optionsResponse();
	}


	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/replaceReportModel")
	public Response replaceReportModel(String input)
	{
		ObjectMapper mapper = new ObjectMapper();
		ReportModelStructureDTO obj = null;
		String eStr = "replaceReportModel():  ";
		try
		{
			//System.out.println("JSON str is =" + input);
			obj = mapper.readValue(input, ReportModelStructureDTO.class);
			setup2Service.replaceReportModel(obj);
		}
		catch (JsonMappingException e)
		{
			eStr += "JsonMappingException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		catch (JsonParseException e)
		{
			eStr += "JsonParseException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		catch (IOException e)
		{
			eStr += "IOException - ReportModelStructureDTO data.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		catch (Exception e)
		{
			eStr += "Exception saving ReportModelStructureDTO data.  msg=" + e.getMessage();
			System.out.println(eStr);
			e.printStackTrace();
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}

		return Response.ok(SUCCESS).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/replaceReportModel")
	public Response optionsReplaceReportModel() {
		return optionsResponse();
	}


	@GET
	@Path("/fetchReportOptionsData/{dnaId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response fetchReportOptionsData(@PathParam("dnaId") long dnaId)
	{
		Setup2ReportOptionsDTO dto = null;

		try {
			dto = setup2Service.fetchReportOptionsData(dnaId);
		} catch (Exception e) {
			System.out.println("Exception caught fetchReportOptionsData.  dnaId=" + dnaId + ", msg=" + e.getMessage());
			return null;
		}
		// System.out.println("<=======>\fetchReportOptionsData(): Setup2ReportOptionsDTO for dnaId
		// " + dnaId + "\n" + dto.toString());

		// Convert to JSON
		ObjectMapper mapper = new ObjectMapper();
		String retStr = null;
		try {
			retStr = mapper.writeValueAsString(dto);
		} catch (JsonMappingException e) {
			System.out.println("fetchReportOptionsData():  JsonMappingException.  msg=" + e.getMessage());
			retStr = null;
		} catch (JsonGenerationException e) {
			System.out.println("fetchReportOptionsData():  JsonGenerationException.  msg=" + e.getMessage());
			retStr = null;
		} catch (IOException e) {
			System.out.println("fetchReportOptionsData():  IOException.  msg=" + e.getMessage());
			retStr = null;
		}
		// System.out.println("<=======>\fetchReportOptionsData(): JSON generated
		// (dnaId=" + dnaId + "): " + retStr);

		return Response.ok(retStr).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/fetchReportOptionsData/{dnaId}")
	public Response optionsFetchReportOptionsData(@PathParam("dnaId") long dnaId) {
		return optionsResponse();
	}

	@GET
	@Path("/resetToDefaultReportOptionsData/{dnaId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response resetToDefaultReportOptionsData(@PathParam("dnaId") long dnaId)
	{
		Setup2ReportOptionsDTO dto = null;

		try {
			dto = setup2Service.resetToDefaultReportOptionsData(dnaId);
		} catch (Exception e) {
			System.out.println("Exception caught fetchReportOptionsData.  dnaId=" + dnaId + ", msg=" + e.getMessage());
			return null;
		}
		// System.out.println("<=======>\fetchReportOptionsData(): Setup2ReportOptionsDTO for dnaId
		// " + dnaId + "\n" + dto.toString());

		// Convert to JSON
		ObjectMapper mapper = new ObjectMapper();
		String retStr = null;
		try {
			retStr = mapper.writeValueAsString(dto);
		} catch (JsonMappingException e) {
			System.out.println("resetToDefaultReportOptionsData():  JsonMappingException.  msg=" + e.getMessage());
			retStr = null;
		} catch (JsonGenerationException e) {
			System.out.println("resetToDefaultReportOptionsData():  JsonGenerationException.  msg=" + e.getMessage());
			retStr = null;
		} catch (IOException e) {
			System.out.println("resetToDefaultReportOptionsData():  IOException.  msg=" + e.getMessage());
			retStr = null;
		}
		// System.out.println("<=======>\fetchReportOptionsData(): JSON generated
		// (dnaId=" + dnaId + "): " + retStr);

		return Response.ok(retStr).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/resetToDefaultReportOptionsData/{dnaId}")
	public Response optionsResetToDefaultReportOptionsData(@PathParam("dnaId") long dnaId) {
		return optionsResponse();
	}


	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/updateReportOptionsData")
	public Response updateReportOptionsData(String input)
	{
		ObjectMapper mapper = new ObjectMapper();
		Setup2ReportOptionsDTO obj = null;
		String eStr = "updateReportOptionsData():  ";
		try
		{
			//System.out.println("JSON str is =" + input);
			obj = mapper.readValue(input, Setup2ReportOptionsDTO.class);
			setup2Service.updateReportOptionsData(obj);
		}
		catch (JsonMappingException e)
		{
			eStr += "JsonMappingException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		catch (JsonParseException e)
		{
			eStr += "JsonParseException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		catch (IOException e)
		{
			eStr += "IOException - updateReportOptionsData data.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		catch (Exception e)
		{
			eStr += "Exception saving updateReportOptionsData data.  msg=" + e.getMessage();
			System.out.println(eStr);
			e.printStackTrace();
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}

		return Response.ok(SUCCESS).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/updateReportOptionsData")
	public Response optionsUpdateReportOptionsData() {
		return optionsResponse();
	}


	@GET
	@Path("/fetchReportLabelsData/{dnaId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response fetchReportLabelsData(@PathParam("dnaId") long dnaId)
	{
		Setup2ReportLabelsDTO dto = null;

		try {
			dto = setup2Service.fetchReportLabelsData(dnaId);
		} catch (Exception e) {
			System.out.println("Exception caught Setup2ReportLabelsDTO.  dnaId=" + dnaId + ", msg=" + e.getMessage());
			return null;
		}
		// System.out.println("<=======>\fetchReportLabelsData(): Setup2ReportLabelsDTO for dnaId
		// " + dnaId + "\n" + dto.toString());

		// Convert to JSON
		ObjectMapper mapper = new ObjectMapper();
		String retStr = null;
		try {
			retStr = mapper.writeValueAsString(dto);
		} catch (JsonMappingException e) {
			System.out.println("fetchReportLabelsData():  JsonMappingException.  msg=" + e.getMessage());
			retStr = null;
		} catch (JsonGenerationException e) {
			System.out.println("fetchReportLabelsData():  JsonGenerationException.  msg=" + e.getMessage());
			retStr = null;
		} catch (IOException e) {
			System.out.println("fetchReportLabelsData():  IOException.  msg=" + e.getMessage());
			retStr = null;
		}
		// System.out.println("<=======>\fetchReportLabelsData(): JSON generated
		// (dnaId=" + dnaId + "): " + retStr);

		return Response.ok(retStr).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/fetchReportLabelsData/{dnaId}")
	public Response optionsfetchReportLabelsData(@PathParam("dnaId") long dnaId) {
		return optionsResponse();
	}


	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/updateReportLabelsData")
	public Response updateReportLabelsData(String input)
	{
		ObjectMapper mapper = new ObjectMapper();
		Setup2ReportLabelsDTO obj = null;
		String eStr = "updateReportLabelsData():  ";
		try
		{
			//System.out.println("JSON str is =" + input);
			obj = mapper.readValue(input, Setup2ReportLabelsDTO.class);
			setup2Service.updateReportLabelsData(obj);
		}
		catch (JsonMappingException e)
		{
			eStr += "JsonMappingException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		catch (JsonParseException e)
		{
			eStr += "JsonParseException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		catch (IOException e)
		{
			eStr += "IOException - Setup2ReportLabelsDTO.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		catch (Exception e)
		{
			eStr += "Exception saving Setup2ReportLabelsDTO.  msg=" + e.getMessage();
			System.out.println(eStr);
			e.printStackTrace();
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}

		return Response.ok(SUCCESS).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/updateReportLabelsData")
	public Response optionsUpdateReportLabelsData() {
		return optionsResponse();
	}


	@GET
	@Path("/fetchReportNote/{dnaId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response fetchReportNote(@PathParam("dnaId") long dnaId)
	{
		ReportNoteDTO dto = null;

		try {
			dto = setup2Service.fetchReportNote(dnaId);
		} catch (Exception e) {
			System.out.println("Exception caught ReportNoteDTO.  dnaId=" + dnaId + ", msg=" + e.getMessage());
			return null;
		}
		// System.out.println("<=======>\fetchReportNote(): ReportNoteDTO for dnaId
		// " + dnaId + "\n" + dto.toString());

		// Convert to JSON
		ObjectMapper mapper = new ObjectMapper();
		String retStr = null;
		try {
			retStr = mapper.writeValueAsString(dto);
		} catch (JsonMappingException e) {
			System.out.println("fetchReportNote():  JsonMappingException.  msg=" + e.getMessage());
			retStr = null;
		} catch (JsonGenerationException e) {
			System.out.println("fetchReportNote():  JsonGenerationException.  msg=" + e.getMessage());
			retStr = null;
		} catch (IOException e) {
			System.out.println("fetchReportNote():  IOException.  msg=" + e.getMessage());
			retStr = null;
		}
		// System.out.println("<=======>\fetchReportNote(): JSON generated
		// (dnaId=" + dnaId + "): " + retStr);

		return Response.ok(retStr).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/fetchReportNote/{dnaId}")
	public Response optionsFetchReportNote(@PathParam("dnaId") long dnaId) {
		return optionsResponse();
	}


	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/updateReportNote")
	public Response updateReportNote(String input)
	{
		ObjectMapper mapper = new ObjectMapper();
		ReportNoteDTO obj = null;
		String eStr = "updateReportNote():  ";
		try
		{
			//System.out.println("JSON str is =" + input);
			obj = mapper.readValue(input, ReportNoteDTO.class);
			setup2Service.updateReportNote(obj);
		}
		catch (JsonMappingException e)
		{
			eStr += "JsonMappingException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		catch (JsonParseException e)
		{
			eStr += "JsonParseException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		catch (IOException e)
		{
			eStr += "IOException - ReportNoteDTO.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		catch (Exception e)
		{
			eStr += "Exception saving ReportNoteDTO.  msg=" + e.getMessage();
			System.out.println(eStr);
			e.printStackTrace();
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}

		return Response.ok(SUCCESS).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/updateReportNote")
	public Response optionsUpdateReportNote() {
		return optionsResponse();
	}


	@GET
	@Path("/fetchFullLanguageList/{includeDefault}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response fetchFullLanguageList(@PathParam("includeDefault") boolean includeDefault)
	{
		ArrayList<LanguageItemDTO> list = new ArrayList<LanguageItemDTO>();

		try
		{
			list = setup2Service.fetchFullLanguageList(includeDefault);
		}
		catch (Exception e)
		{
			System.out.println("fetchFullLanguageList(): Exception caught fetching report language list (default=" + includeDefault + ").  Msg=" + e.getMessage());
			return null;
		}
		LanguageList wrapper = new LanguageList(list);
		//System.out.println("<=======>\nfetchFullLanguageList():  ArrayList of LanguageItemDTOs:\n" + wrapper.toString());

		// Convert to JSON
		ObjectMapper mapper = new ObjectMapper();
		String retStr = null;
		String eStr = "fetchFullLanguageList:  ";
		try
		{
			retStr = mapper.writeValueAsString(wrapper);
		}
		catch (JsonMappingException e)
		{
			System.out.println(eStr + "JsonMappingException.  msg=" + e.getMessage());
			retStr = null;
		}
		catch (JsonGenerationException e)
		{
			System.out.println(eStr + "JsonGenerationException.  msg=" + e.getMessage());
			retStr = null;
		}
		catch (IOException e)
		{
			System.out.println(eStr + "IOException.  msg=" + e.getMessage());
			retStr = null;
		}
		//System.out.println("<=======>\nfetchFullLanguageList():  JSON generated (default=" + includeDefault + "):\n" + retStr);

		return Response.ok(retStr).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/fetchFullLanguageList/{includeDefault}")
	public Response optionsFetchFullLanguageList(@PathParam("includeDefault") boolean includeDefault) {
		return optionsResponse();
	}


	@GET
	@Path("/fetchRptLanguageList/{includeDefault}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response fetchRptLanguageList(@PathParam("includeDefault") boolean includeDefault)
	{
		ArrayList<LanguageItemDTO> list = new ArrayList<LanguageItemDTO>();

		try
		{
			list = setup2Service.fetchRptLanguageList(includeDefault);
		}
		catch (Exception e)
		{
			System.out.println("fetchRptLanguageList(): Exception caught fetching report language list (default=" + includeDefault + ").  Msg=" + e.getMessage());
			return null;
		}
		LanguageList wrapper = new LanguageList(list);
		//System.out.println("<=======>\ngetRptLanguageList():  ArrayList of LanguageItemDTOs:\n" + wrapper.toString());

		// Convert to JSON
		ObjectMapper mapper = new ObjectMapper();
		String retStr = null;
		String eStr = "fetchRptLanguageList:  ";
		try
		{
			retStr = mapper.writeValueAsString(wrapper);
		}
		catch (JsonMappingException e)
		{
			System.out.println(eStr + "JsonMappingException.  msg=" + e.getMessage());
			retStr = null;
		}
		catch (JsonGenerationException e)
		{
			System.out.println(eStr + "JsonGenerationException.  msg=" + e.getMessage());
			retStr = null;
		}
		catch (IOException e)
		{
			System.out.println(eStr + "IOException.  msg=" + e.getMessage());
			retStr = null;
		}
		//System.out.println("<=======>\ngetRptLanguageList():  JSON generated (default=" + includeDefault + "):\n" + retStr);

		return Response.ok(retStr).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/fetchRptLanguageList/{includeDefault}")
	public Response optionsFetchRptLanguageList(@PathParam("includeDefault") boolean includeDefault) {
		return optionsResponse();
	}


	@GET
	@Path("/fetchRmXlateData/{dnaId}/{langId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response fetchRmXlateData(@PathParam("dnaId") long dnaId, @PathParam("langId") long langId)
	{
		ReportModelTranslationDTO dto = new ReportModelTranslationDTO();
		dto.setDnaId(dnaId);
		LanguageItemDTO language = new LanguageItemDTO();
		language.setId(langId);
		dto.setLanguageItem(language);

		try {
			dto = setup2Service.fetchRmXlateData(dto);
		} catch (Exception e) {
			System.out.println("Exception caught fetchRmXlateData.  dnaId=" + dnaId + ", msg=" + e.getMessage());
			return null;
		}
		// System.out.println("<=======>\fetchReportNote(): ReportNoteDTO for dnaId
		// " + dnaId + "\n" + dto.toString());

		// Convert to JSON
		ObjectMapper mapper = new ObjectMapper();
		String retStr = null;
		try {
			retStr = mapper.writeValueAsString(dto);
		} catch (JsonMappingException e) {
			System.out.println("fetchRmXlateData():  JsonMappingException.  msg=" + e.getMessage());
			retStr = null;
		} catch (JsonGenerationException e) {
			System.out.println("fetchRmXlateData():  JsonGenerationException.  msg=" + e.getMessage());
			retStr = null;
		} catch (IOException e) {
			System.out.println("fetchRmXlateData():  IOException.  msg=" + e.getMessage());
			retStr = null;
		}
		// System.out.println("<=======>\fetchRmXlateData(): JSON generated
		// (dnaId=" + dnaId + "): " + retStr);

		return Response.ok(retStr).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/fetchRmXlateData/{dnaId}/{langId}")
	public Response optionsFetchRmXlateData(@PathParam("dnaId") long dnaId, @PathParam("langId") long langId) {
		return optionsResponse();
	}


	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/updateRmXlateData")
	public Response updateRmXlateData(String input)
	{
		ObjectMapper mapper = new ObjectMapper();
		ReportModelTranslationDTO obj = null;
		String eStr = "updateReportNote():  ";
		try
		{
			//System.out.println("JSON str is =" + input);
			obj = mapper.readValue(input, ReportModelTranslationDTO.class);
			setup2Service.updateRmXlateData(obj);
		}
		catch (JsonMappingException e)
		{
			eStr += "JsonMappingException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		catch (JsonParseException e)
		{
			eStr += "JsonParseException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		catch (IOException e)
		{
			eStr += "IOException - ReportModelTranslationDTO.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}
		catch (Exception e)
		{
			eStr += "Exception saving ReportModelTranslationDTO.  msg=" + e.getMessage();
			System.out.println(eStr);
			e.printStackTrace();
			return Response.ok(eStr).header("Access-Control-Allow-Origin", "*").build();
		}

		return Response.ok(SUCCESS).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/updateRmXlateData")
	public Response optionsUpdateRmXlateData() {
		return optionsResponse();
	}

}
