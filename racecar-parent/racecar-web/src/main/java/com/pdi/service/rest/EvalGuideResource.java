package com.pdi.service.rest;

import java.io.IOException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdi.data.abyd.dto.common.EGFullDataDTO;
import com.pdi.data.abyd.dto.evalGuide.EGCompDataDTO;
import com.pdi.data.abyd.dto.evalGuide.EGCompOverideDataDTO;
import com.pdi.data.abyd.dto.evalGuide.EGIrDataDTO;
import com.pdi.data.abyd.dto.evalGuide.EGOverallDTO;
import com.pdi.data.abyd.dto.evalGuide.EGRaterNameDTO;
import com.pdi.listener.abyd.evalGuide.EvalGuideService;

@Path("eval")
public class EvalGuideResource {
	private static final Logger log = LoggerFactory.getLogger(EvalGuideResource.class);

	//
	// Instance variables
	//
	private static String SUCCESS = "Success";
	EvalGuideService egs = new EvalGuideService();

	//
	// Instance methods
	//

	/**
	 * optionsResponse - Build the desired Response object for the @OPTIONS annotation
	 * @return
	 */
	private Response optionsResponse()
	{
		return Response.ok(SUCCESS)
				.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT")
				.header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Cache-Control, Authorization, Pragma, Expires")
				.header("Access-Control-Expose-Headers", "newToken")
				.header("Access-Control-Allow-Origin", "*")
				.build();
	}


	@GET
	@Path("/getEg/{lpId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getEg(@PathParam("lpId") long lpId) {

		log.debug("Recieved Get request for Eval Guide: {}", lpId);
		String retStr = "N/A";
		String str = null;

		EGFullDataDTO dto;
		try {
			dto = egs.fetchEvalGuide("" + lpId);
		} catch (Exception e) {
			log.error("Exception caught fetching EG.  lpId={}, msg={}", lpId, e.getMessage());
			return null;
		}
		//System.out.println("<=======>\nEGFullDataDTO for lp " + lpId + "\n"	+ dto.toString());

		// Convert to JSON
		ObjectMapper mapper = new ObjectMapper();
		try {
			str = mapper.writeValueAsString(dto);
		} catch (JsonMappingException e) {
			System.out.println("3:  JsonMappingException.  msg="
					+ e.getMessage());
			str = null;
		} catch (JsonGenerationException e) {
			System.out.println("3:  JsonGenerationException.  msg="
					+ e.getMessage());
			str = null;
		} catch (IOException e) {
			System.out.println("3:  IOException.  msg=" + e.getMessage());
			str = null;
		}
		//System.out.println("<=======>\nJSON generated (lpId=" + lpId + "):  " + str);

		retStr = str;

		log.debug("Finished fetching Eval guide.  Returning: {}", retStr);
		return Response.ok(retStr).header("Access-Control-Allow-Origin", "*")
				.build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/getEg/{lpId}")
	public Response optionsGetEg() {
		return optionsResponse();
	}


	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/saveEgRaterName")
	public Response saveEgRaterName(String egRaterName) {

		ObjectMapper mapper = new ObjectMapper();
		EGRaterNameDTO obj = null;
		String eStr = "saveEgRaterName():  ";
		try {
			//System.out.println(eStr + "JSON str is =" + egRaterName);
			obj = mapper.readValue(egRaterName, EGRaterNameDTO.class);
			egs.saveEgRaterName(obj);
		} catch (JsonMappingException e) {
			eStr += "JsonMappingException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr)
					.header("Access-Control-Allow-Origin", "*").build();
		} catch (JsonParseException e) {
			eStr += "JsonParseException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr)
					.header("Access-Control-Allow-Origin", "*").build();
		} catch (IOException e) {
			eStr += "IOException - name data.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr)
					.header("Access-Control-Allow-Origin", "*").build();
		} catch (Exception e) {
			eStr += "Exception processing rater name.  msg=" + e.getMessage();
			System.out.println(eStr);
			e.printStackTrace();
			return Response.ok(eStr)
					.header("Access-Control-Allow-Origin", "*").build();
		}

		return Response.ok(SUCCESS)
				.header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/saveEgRaterName")
	public Response optionsSaveEgRaterName() {
		return optionsResponse();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/saveEgCompData")
	public Response saveEgCompData(String egCompData) {

		ObjectMapper mapper = new ObjectMapper();
		EGCompDataDTO obj = null;
		String eStr = "saveEgCompData():  ";
		try {
			//System.out.println(eStr + "JSON str is =" + egCompData);
			obj = mapper.readValue(egCompData, EGCompDataDTO.class);
			egs.saveEgCompData(obj);
		} catch (JsonMappingException e) {
			eStr += "JsonMappingException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr)
					.header("Access-Control-Allow-Origin", "*").build();
		} catch (JsonParseException e) {
			eStr += "JsonParseException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr)
					.header("Access-Control-Allow-Origin", "*").build();
		} catch (IOException e) {
			eStr += "IOException - BAR data.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr)
					.header("Access-Control-Allow-Origin", "*").build();
		} catch (Exception e) {
			eStr += "Exception saving BAR data.  msg=" + e.getMessage();
			System.out.println(eStr);
			e.printStackTrace();
			return Response.ok(eStr)
					.header("Access-Control-Allow-Origin", "*").build();
		}

		return Response.ok(SUCCESS)
				.header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/saveEgCompData")
	public Response optionsSaveEgCompData() {
		return optionsResponse();
	}


	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/saveEgCompOveride")
	public Response saveEgCompOveride(String csoData)
	{
		ObjectMapper mapper = new ObjectMapper();
		EGCompOverideDataDTO obj = null;
		String eStr = "saveEgCompOveride():  ";
		try {
			//System.out.println(eStr + "JSON str is =" + csoData);
			obj = mapper.readValue(csoData, EGCompOverideDataDTO.class);
			egs.saveEgCompOveride(obj);
		} catch (JsonMappingException e) {
			eStr += "JsonMappingException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr)
					.header("Access-Control-Allow-Origin", "*").build();
		} catch (JsonParseException e) {
			eStr += "JsonParseException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr)
					.header("Access-Control-Allow-Origin", "*").build();
		} catch (IOException e) {
			eStr += "IOException - BAR data.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr)
					.header("Access-Control-Allow-Origin", "*").build();
		} catch (Exception e) {
			eStr += "Exception saving BAR data.  msg=" + e.getMessage();
			System.out.println(eStr);
			e.printStackTrace();
			return Response.ok(eStr)
					.header("Access-Control-Allow-Origin", "*").build();
		}

		return Response.ok(SUCCESS)
				.header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/saveEgCompOveride")
	public Response optionsSaveEgCompOveride()
	{
		return optionsResponse();
	}


	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/saveEgIrData")
	public Response saveEgIrData(String egIrData) {

		ObjectMapper mapper = new ObjectMapper();
		EGIrDataDTO obj = null;
		String eStr = "saveEgIrData():  ";
		try {
			//System.out.println("eStr + JSON str is =" + egIrData);
			obj = mapper.readValue(egIrData, EGIrDataDTO.class);
			egs.saveEgIrData(obj);
		} catch (JsonMappingException e) {
			eStr += "JsonMappingException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr)
					.header("Access-Control-Allow-Origin", "*").build();
		} catch (JsonParseException e) {
			eStr += "JsonParseException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr)
					.header("Access-Control-Allow-Origin", "*").build();
		} catch (IOException e) {
			eStr += "IOException - Impact Ratings.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr)
					.header("Access-Control-Allow-Origin", "*").build();
		} catch (Exception e) {
			eStr += "Exception saving IR data.  msg=" + e.getMessage();
			System.out.println(eStr);
			e.printStackTrace();
			return Response.ok(eStr)
					.header("Access-Control-Allow-Origin", "*").build();
		}

		return Response.ok(SUCCESS)
				.header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/saveEgIrData")
	public Response optionsSaveEgIrData() {
		return optionsResponse();
	}


	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/saveEgOverall")
	public Response saveEgOverall(String egOverall) {

		ObjectMapper mapper = new ObjectMapper();
		EGOverallDTO obj = null;
		String eStr = "saveEgOverall():  ";
		try {
			//System.out.println("eStr + JSON str is =" + egOverall);
			obj = mapper.readValue(egOverall, EGOverallDTO.class);
			egs.saveEgOverall(obj);
		} catch (JsonMappingException e) {
			eStr += "JsonMappingException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr)
					.header("Access-Control-Allow-Origin", "*").build();
		} catch (JsonParseException e) {
			eStr += "JsonParseException.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr)
					.header("Access-Control-Allow-Origin", "*").build();
		} catch (IOException e) {
			eStr += "IOException - Overall ratings.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr)
					.header("Access-Control-Allow-Origin", "*").build();
		} catch (Exception e) {
			eStr += "Exception saving Overall data.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr)
					.header("Access-Control-Allow-Origin", "*").build();
		}

		return Response.ok(SUCCESS)
				.header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/saveEgOverall")
	public Response optionsSaveEgOverall() {
		return optionsResponse();
	}


	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/submit/{partId}/{dnaId}/{modId}")
	public Response submitEG(@PathParam("partId") String partId,
			@PathParam("dnaId") long dnaId, @PathParam("modId") long modId)
					throws Exception {
		String eStr = "submitEG():  ";
		try {
			egs.submitEG(partId, dnaId, modId);
		} catch (IOException e) {
			eStr += "IOException - EG Submit.  msg=" + e.getMessage();
			System.out.println(eStr);
			return Response.ok(eStr)
					.header("Access-Control-Allow-Origin", "*").build();
		} catch (Exception e) {
			eStr += "Exception on Submit.  msg=" + e.getMessage();
			System.out.println(eStr);
			e.printStackTrace();
			return Response.ok(eStr)
					.header("Access-Control-Allow-Origin", "*").build();
		}

		return Response.ok(SUCCESS).header("Access-Control-Allow-Origin", "*")
				.build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/submit/{partId}/{dnaId}/{modId}")
	public Response optionsSubmitEG(@PathParam("partId") String partId,
			@PathParam("dnaId") long dnaId, @PathParam("modId") long modId) {
		return optionsResponse();
	}

}
