package com.pdi.service.rest;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.Key;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kf.palms.common.exception.PalmsErrorCode;
import com.kf.palms.common.exception.PalmsException;
import com.kf.palms.common.exception.ServerUnavailableException;
import com.kf.palms.jaxb.racecar.RacecarReportsRequest;
import com.kf.palms.utils.JaxbUtil;
import com.kf.palms.web.reporting.GeneratedReportData;
import com.kf.palms.web.rest.security.APICredentials;
import com.kf.palms.web.rest.security.URLConnectionAuthorizationUtil;
import com.kf.palms.web.rest.security.WebTokenUtil;
import com.pdi.properties.PropertyLoader;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Path("report")
public class ReportGenerateResource {
	
	private static final Logger logger = LoggerFactory.getLogger(ReportGenerateResource.class);

	private static String SUCCESS = "Success";
	/**
	 * optionsResponse - Build the desired Response object for the @OPTIONS annotation
	 * @return
	 */
	private Response optionsResponse()
	{
		return Response.ok(SUCCESS)
				.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT")
				.header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Cache-Control, Authorization, Pragma, Expires")
				.header("Access-Control-Expose-Headers", "newToken")
				.header("Access-Control-Allow-Origin", "*")
				.build();
	}
	
	private String getPrincipalFromToken(HttpServletRequest request) throws Exception {
		String token = request.getHeader("Authorization");

		if (token == null){
			token = request.getParameter("token");
		} else {
			token = token.split(" ")[1]; //use the second half of the string after the space.
			//Assumes Authorization header has the scheme:
			//Authorization: Bearer <token>
		}
		
		WebTokenUtil wtUtil = new WebTokenUtil(token);
		
		return (String) wtUtil.getTokenClaimValue("sub");
	}
	
	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	public Response optionskeepAlive() {
		return optionsResponse();
	}
	
		
	@POST
	@Path("/download")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getReport(@FormParam("xml") String xml, @Context HttpServletRequest request, @Context HttpServletResponse response){
		RacecarReportsRequest reportsRequest = (RacecarReportsRequest) JaxbUtil.unmarshal(xml, RacecarReportsRequest.class);
		GeneratedReportData data;
		
		//String principal = (String) request.getAttribute("principal");
		String principal = "unknown";
		try {
			principal = this.getPrincipalFromToken(request);
		} catch (Exception e){
			logger.error("Error getting principal from web token: {}", e.getMessage());
			e.printStackTrace();
		}
		logger.debug("{} is requesting a report", principal);
		if (principal != null){
			reportsRequest.setPrincipal(principal);
		}
		try {
			data = this.getReportStream(reportsRequest);
		} catch (PalmsException e) {
			response.setHeader("Error", e.getMessage());
			response.setContentType("text/html");
			//PrintWriter pw = response.getWriter();
			String errorResponse = ("<html><body><h1 style=\"color:red;\">Failed to retrieve the report.</h1>" + e.getMessage()
					+ "</body></html>");
			e.printStackTrace();
			return Response.serverError().type("text/html").entity(errorResponse).build();
		}
		final InputStream istream = data.getInSream();
		StreamingOutput stream = new StreamingOutput() {
			@Override
			public void write(OutputStream os) throws IOException, WebApplicationException {

				byte[] buffer = new byte[4096];
				int numbytes = 0;
				while ((numbytes = istream.read(buffer)) != -1) {

					os.write(buffer, 0, numbytes);
				}

				os.flush();
			}
		};
		
		logger.debug("About to stream the response...Disposition: {}", data.getHdrStr());

		return Response.ok(stream)
				.type(data.getContentType()).header("Content-Disposition", data.getHdrStr()).build();
		//return null;
		
	}
	
	public GeneratedReportData getReportStream(RacecarReportsRequest reportsRequest) throws PalmsException {
		// ByteArrayOutputStream stream = new ByteArrayOutputStream(4096);
		String xml = JaxbUtil.marshalToXmlString(reportsRequest, RacecarReportsRequest.class);
		String urlTarget = null;

		HttpURLConnection conn = null;
		GeneratedReportData grd = new GeneratedReportData();

		try {
		
			//urlTarget = "http://localhost:8080/pdi-web/PortalServlet";
			urlTarget = PropertyLoader.getProperty("portalServlet.url");
			logger.debug("Got urlTarget: {}", urlTarget);
			URL currUrl = new URL(urlTarget);

			conn = (HttpURLConnection) currUrl.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
			conn.setRequestProperty("Accept-Charset", "UTF-8");
			conn.setDoOutput(true);

			URLConnectionAuthorizationUtil.authorizePALMSAPIRequest(conn, this.makeAPICredentials());
			// Generate the report
			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
			writer.write("xml=" + URLEncoder.encode(xml, "UTF-8"));
			writer.flush();
		} catch (MalformedURLException e) {
			logger.debug("Exception creating the target URL (url={}): {}", urlTarget, e.getMessage());
			throw new PalmsException(e.getMessage(), PalmsErrorCode.BAD_REQUEST.getCode());

		} catch (IOException e) {
			logger.error("Exception on the connection - establishing or writing (url={}): {}", urlTarget,
					e.getMessage());
			ServerUnavailableException sue = new ServerUnavailableException(urlTarget, e.getMessage());
			sue.setErrorCode(PalmsErrorCode.REPORT_SERVER_UNAVAILABLE.getCode());
			throw sue;

		}
		

		// ...otherwise, get the generated report as a stream
		try {


			if (!(conn.getHeaderField("Error") == null)) {
				String errorMessage = conn.getHeaderField("Error");
				if (errorMessage.length() > 250) {
					errorMessage = errorMessage.substring(0, 249);
				}
				logger.error("Got Error Header in response: {}", errorMessage);
				throw new PalmsException(errorMessage, PalmsErrorCode.REPORT_GEN_FAILED.getCode());
			}
			grd.setInStream(conn.getInputStream());
			if (!(conn.getHeaderField("Content-Length") == null)) {
				grd.setContentLength(Integer.valueOf(conn.getHeaderField("Content-Length")));
			}
			if (!(conn.getHeaderField("content-disposition") == null)) {
				grd.setHdrStr(conn.getHeaderField("content-disposition"));
			}
			if (!(conn.getHeaderField("Content-Disposition") == null)) {
				grd.setHdrStr(conn.getHeaderField("Content-Disposition"));
			}
			if (!(conn.getHeaderField("Content-Type") == null)){
				grd.setContentType(conn.getHeaderField("Content-Type"));
			}


			return grd;
		} catch (IOException ioe) {
			logger.debug("Exception fetching report input stream: {}", ioe.getMessage());
			throw new PalmsException("Exception fetching report input stream: " + ioe.getMessage(),
					PalmsErrorCode.REPORT_GEN_FAILED.getCode());

		}

	}
	
	private APICredentials makeAPICredentials(){
		String secretKey = PropertyLoader.getProperty(null, "palms.api.secretKey");
		String partnerKeys = PropertyLoader.getProperty(null, "partnerKeys");
		String partnerKey = "";
		String partnerKeyVersion = "1.0";
		String serviceToken = "Reports";
		String tokenType = "PALMS";
		
		if (!(partnerKeys == null)) {
			String[] partnerStringArray = partnerKeys.split(",");
			logger.debug("Partner String Array: {}", (Object) partnerStringArray);
			for (String s : partnerStringArray) {
				String[] tokenKey = s.split("\\|");
				if (tokenKey[0].equals("PALMS")){
					partnerKey=  tokenKey[2];
				}
			
			}

		}
		
		APICredentials credentials = new APICredentials(secretKey, partnerKey, partnerKeyVersion, serviceToken, tokenType);
		
		return credentials;
		
	}
}
