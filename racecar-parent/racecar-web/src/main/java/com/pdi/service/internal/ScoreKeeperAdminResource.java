package com.pdi.service.internal;

import java.io.IOException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.pdi.data.abyd.dto.evalGuide.EGUrlDTO;
import com.pdi.listener.abyd.evalGuide.EvalGuideService;

/**
 * Provides services for ScoreKeeper admin functions
 *  NOTE: This resource and all others in the "internal" package are not subject
 *        to authorization (the ones in the "rest" package are). 
 *
 */
@Path("skadmin")
public class ScoreKeeperAdminResource {

	//
	// Static data
	//
	private static String SUCCESS = "Success";
	
	//
	// Static methods.
	//

	//
	// Instance Data
	//

	//
	// Instance methods.
	//

	/**
	 * getUrls - Fetches data from the URL generator to populate the URL screen
	 * @param projId
	 * @param pptId
	 * @return
	 */
	@GET
	@Path("/getUrls/project/{projId}/ppt/{pptId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUrls(@PathParam("projId") String projId, @PathParam("pptId") String pptId) {

		String retStr = "N/A";
		String str = null;

		EGUrlDTO dto;

		EvalGuideService egs = new EvalGuideService();

		try {
			dto = egs.fetchEgUrls(projId, pptId);
		} catch (Exception e) {
			System.out.println("Exception caught fetching URLs.  projId=" + projId + ", pptId=" + pptId
					+ ", msg=" + e.getMessage());
			return null;
		}
		//System.out.println("<=======>EGUrlDTO for proj/ppt " + projId + "/" + pptId + "\n"	+ dto.toString());

		// Convert to JSON
		ObjectMapper mapper = new ObjectMapper();
		try {
			str = mapper.writeValueAsString(dto);
		} catch (JsonMappingException e) {
			System.out.println("3:  JsonMappingException.  msg="
					+ e.getMessage());
			str = null;
		} catch (JsonGenerationException e) {
			System.out.println("3:  JsonGenerationException.  msg="
					+ e.getMessage());
			str = null;
		} catch (IOException e) {
			System.out.println("3:  IOException.  msg=" + e.getMessage());
			str = null;
		}
		//System.out.println("<=======>\nJSON generated (proj/ppt " + projId + "/" + pptId + "):  " + str);

		retStr = str;

		return Response.ok(retStr).header("Access-Control-Allow-Origin", "*").build();
	}

	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/getUrls/project/{projId}/ppt/{pptId}")
	public Response optionsGetEg() {
		return optionsResponse();
	}



	/**
	 * optionsResponse - Build the desired Response object for the @OPTIONS annotation
	 * @return
	 */
	private Response optionsResponse()
	{
		return Response.ok(SUCCESS)
				.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT")
				.header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Cache-Control, Authorization, Pragma, Expires")
				.header("Access-Control-Expose-Headers", "newToken")
				.header("Access-Control-Allow-Origin", "*")
				.build();
	}

}
