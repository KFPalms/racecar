package com.pdi.service.rest;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Files;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pdi.properties.PropertyLoader;

@Path("repository")
public class RepositoryResource {

	private static final Logger log = LoggerFactory.getLogger(RepositoryResource.class);
	
	@GET
	@Path("/download/{filename}")
	@Produces({ MediaType.APPLICATION_OCTET_STREAM, MediaType.APPLICATION_JSON, MediaType.TEXT_HTML })
	public Response downloadRepositoryFile(@PathParam("filename") String filename){
		String repoDir = PropertyLoader.getProperty(null, "repository.directory");
		log.debug("Get File: {}/{}", repoDir, filename);
		
		final File file = new File(repoDir + "/" + filename);
		log.debug("Absolute Path: {}", file.getAbsolutePath());
		
		
		
	    try {
	    	String ct = Files.probeContentType(file.toPath());
	    	log.debug("Content Type STring: {}", ct);
			final InputStream is = 
			            new BufferedInputStream(new FileInputStream(file));
			return Response.ok(is).type(ct).header("Content-Disposition", "attachment;filename=" + filename)
					.header("Content-Transfer-Encoding", "binary").header("Content-Length", file.length()).build();
		} catch (Exception e) {
			log.error("Download failed due to: {}", e.getMessage());
			e.printStackTrace();
			return Response.serverError().build();
		}
		//return Response.ok("OK").build();
	}
}
