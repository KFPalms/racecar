package com.pdi.jaxrs.abyd;

import java.io.IOException;

import com.sun.jersey.api.container.httpserver.HttpServerFactory;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;
//import com.sun.jersey.api.core.ResourceConfig;
import com.sun.net.httpserver.HttpServer;

/**
 * Class to test the initial setup of the rest service.  This does not set up
 * under pdi-web and so is no longer useful other than a historical artifact.
 */
public class AbyDRestStartUp {
	//
	// static variables
	//

    static final String BASE_URI = "http://localhost:8090/";

    public static void main(String[] args)
    {
    	ResourceConfig rc = new PackagesResourceConfig("");
    	// CORSFilter is needed to allow cross-domain access
    	rc.getContainerResponseFilters().add(new CORSFilter());

        try
        {
        	HttpServer server = HttpServerFactory.create(BASE_URI, rc);
        	server.start();
        	System.out.println("Press Enter to stop the server. ");
        	System.in.read();
        	server.stop(0);
        }
        catch (IllegalArgumentException e)
        {
        	System.out.println("Illegal Argument.  msg=" + e.getMessage());
        	e.printStackTrace();
        }
        catch (IOException e)
        {
        	System.out.println("Illegal Argument.  msg=" + e.getMessage());
        	e.printStackTrace();
        }
    }
}
