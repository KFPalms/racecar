package com.pdi.jaxrs.abyd.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlAccessorType(XmlAccessType.FIELD)
public class ModelListDTO {
	
	List<EntityListItemDto> modelList = new ArrayList<EntityListItemDto>();

	public List<EntityListItemDto> getModelList() {
		return modelList;
	}

	public void setModelList(List<EntityListItemDto> modelList) {
		this.modelList = modelList;
	}

}
