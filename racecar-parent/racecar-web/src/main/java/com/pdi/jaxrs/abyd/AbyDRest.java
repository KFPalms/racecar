package com.pdi.jaxrs.abyd;

import java.io.IOException;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.pdi.data.abyd.dto.common.DNACellDTO;
import com.pdi.data.abyd.dto.common.EGFullDataDTO;
import com.pdi.data.abyd.dto.intGrid.IGCompDataDTO;
import com.pdi.data.abyd.dto.intGrid.IGInstDisplayData;
import com.pdi.data.abyd.dto.intGrid.IGSummaryDataDTO;
import com.pdi.data.abyd.dto.reportInput.ReportInputDTO;
import com.pdi.jaxrs.abyd.dto.ScoreDataWrapper;
import com.pdi.jaxrs.abyd.dto.UiDto;
import com.pdi.jaxrs.abyd.dto.UiEval;
import com.pdi.jaxrs.abyd.dto.UiImpact;
import com.pdi.jaxrs.abyd.dto.UiOverall;
import com.pdi.jaxrs.abyd.dto.UiPpt;
import com.pdi.listener.abyd.evalGuide.EvalGuideService;
import com.pdi.listener.abyd.intGrid.IntegrationGridService;
import com.pdi.listener.abyd.reportInput.ReportInputService;

@Path("abyd")
public class AbyDRest {
	
	static String STR_333 = "{\"client\":\"Chrysanthemum Corporation\",\"project\":\"Coriolanus Crystal II\",\"simulation\":\"Shard345 I\",\"participant\":{\"firstName\":\"Chaz\",\"lastName\":\"Agilamyus\",\"id\":\"333\"},\"evaluations\":[{\"id\":\"entrepAndCommerce\",\"scores\":[\"5\",\"4\",\"3\",\"NA\"],\"comments\":\"entrepAndCommerce Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\"},{\"id\":\"formulatingStratAndConcepts\",\"scores\":[3,1,2,4],\"comments\":\"formulatingStratAndConcepts Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\"},{\"id\":\"persuadingAndInfluencing\",\"scores\":[5,2,4,3],\"comments\":\"persuadingAndInfluencing Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\"},{\"id\":\"relatingAndNetworking\",\"scores\":[1,2,3,\"NA\"],\"comments\":\"relatingAndNetworking applyingExpertiseAndTech Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\"},{\"id\":\"decidingAndInitAction\",\"scores\":[4,3,2,1],\"comments\":\"decidingAndInitAction Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\"}],\"impactRatings\":{\"scores\":[4,3,1],\"comments\":\"impactRatings Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\"},\"reviewAndOverallPerformance\":{\"overallPerformanceRating\":4,\"description\":\"reviewAndOverallPerformance description Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\",\"strengths\":\"reviewAndOverallPerformance strengths Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\",\"developmentNeeds\":\"reviewAndOverallPerformance developmentNeeds Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\",\"realTimeCoachingNotes\":\"reviewAndOverallPerformance realTimeCoachingNotes Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\"}}";
	static String STR_444 = "{\"client\":\"Icebreaker Industries\",\"project\":\"Ice Chipper Project\",\"simulation\":\"ICE123\",\"participant\":{\"firstName\":\"Ike\",\"lastName\":\"Ichthyosaurus\",\"id\":444},\"evaluations\":[{\"id\":\"creatingAndInnovating\",\"scores\":[1,3,\"NA\"],\"comments\":\"creatingAndInnovating Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\"},{\"id\":\"entrepAndCommerce\",\"scores\":[5,4,3,\"NA\"],\"comments\":\"entrepAndCommerce Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\"},{\"id\":\"persuadingAndInfluencing\",\"scores\":[5,2,4,3],\"comments\":\"persuadingAndInfluencing Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\"},{\"id\":\"applyingExpertiseAndTech\",\"scores\":[1,2,3,\"NA\"],\"comments\":\"applyingExpertiseAndTech Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\"},{\"id\":\"relatingAndNetworking\",\"scores\":[4,3,2,1],\"comments\":\"relatingAndNetworking Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\"},{\"id\":\"copingWithPressuresAndSetbacks\",\"scores\":[5,1,1,1],\"comments\":\"entrepAndCommerce Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\"},{\"id\":\"achievingPersonalWorkGoals\",\"scores\":[1,3,5],\"comments\":\"achievingPersonalWorkGoals Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\"}],\"impactRatings\":{\"scores\":[1,2,3],\"comments\":\"impactRatings Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\"},\"reviewAndOverallPerformance\":{\"overallPerformanceRating\":4,\"description\":\"reviewAndOverallPerformance description Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\",\"strengths\":\"reviewAndOverallPerformance strengths Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\",\"developmentNeeds\":\"reviewAndOverallPerformance developmentNeeds Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\",\"realTimeCoachingNotes\":\"reviewAndOverallPerformance realTimeCoachingNotes Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\"}}";

	// TODO This is a test class.  It was written in this fashion because the actual data required to drive Chris' UI bears only a passing
	// resemblance to the data available from Oxcart.  In order to conform with the API/contract that was implemented by Chris the test
	// class always returns one of two fixed strings.  There is other logic written to ensure that the POST logic works as well.
	//
	//In order to conform with the API/contract that was implemented by Chris it always returns one of two fixed
	// JSON strings. This test class has them as hard-coded strings (to test the availability of the rest service - Jersey) and also populates
	// some test DTO classes (so we can test the ability to transform from class to class - Jackson).  

	@GET
	@Path("/eg/getEg/{lpId}")
	@Produces(MediaType.APPLICATION_JSON)
	//public String getEg(@PathParam("lpId") long lpId)
	public Response getEg(@PathParam("lpId") long lpId)
	{

		String retStr = "N/A";
		String str = null;
		
		if (lpId == 333)
		{
			retStr = STR_333;
		}
		else if (lpId == 3333)
		{
			// Java to JSON
			UiDto dd = fillUi3();
			System.out.println("<=======>\n3333 obj:  " + dd.toString());
			ObjectMapper objectMapper = new ObjectMapper();
			try
			{
				str = objectMapper.writeValueAsString(dd);
			}
			catch (JsonMappingException e)
			{
				System.out.println("1:  JsonMappingException.  msg=" + e.getMessage());
				str = null;
			}
			catch (JsonGenerationException e)
			{
				System.out.println("1:  JsonGenerationException.  msg=" + e.getMessage());
				str = null;
			}
			catch (IOException e)
			{
				System.out.println("1:  IOException.  msg=" + e.getMessage());
				str = null;
			}
			
			System.out.println("<=======>\n3333 JSON:  " + str);
			retStr = str;
		}
		else if (lpId == 444)
		{
			retStr = STR_444;
		}
		else if (lpId == 4444)
		{
			// Java to JSON
			UiDto dd = fillUi4();
			System.out.println("<=======>\n4444 obj:  " + dd.toString());
			ObjectMapper objectMapper = new ObjectMapper();
			
			try
			{
				str = objectMapper.writeValueAsString(dd);
			}
			catch (JsonMappingException e)
			{
				System.out.println("2:  JsonMappingException.  msg=" + e.getMessage());
				str = null;
			}
			catch (JsonGenerationException e)
			{
				System.out.println("2:  JsonGenerationException.  msg=" + e.getMessage());
				str = null;
			}
			catch (IOException e)
			{
				System.out.println("2:  IOException.  msg=" + e.getMessage());
				str = null;
			}

			System.out.println("<=======>\n4444 JSON:  " + str);
			retStr = str;
		}
		else
		{
			// Go get the eg data
			// TODO The UI needs mods to reflect the way that the data is served or else some significant changes will need to be made to the existing Oxcart methods.
			EvalGuideService egs = new EvalGuideService();
			EGFullDataDTO dto;
			try
			{
				dto = egs.fetchEvalGuide("" + lpId);
			}
			catch (Exception e)
			{
				// TODO Figure out how the Reflex UI stuff handles exceptions.  Can we throw the exception here and display something meaningful there?
				System.out.println("Exception caught fetching EG.  lpId=" + lpId + ", msg=" + e.getMessage());
				return null;
			}
			System.out.println("<=======>\nEGFullDataDTO for lp " + lpId + "\n" + dto.toString());
			
			// Convert to JSON
			ObjectMapper mapper = new ObjectMapper();
			try
			{
				str = mapper.writeValueAsString(dto);
			}
			catch (JsonMappingException e)
			{
				System.out.println("3:  JsonMappingException.  msg=" + e.getMessage());
				str = null;
			}
			catch (JsonGenerationException e)
			{
				System.out.println("3:  JsonGenerationException.  msg=" + e.getMessage());
				str = null;
			}
			catch (IOException e)
			{
				System.out.println("3:  IOException.  msg=" + e.getMessage());
				str = null;
			}
			System.out.println("<=======>\nJSON generated (lpId=" + lpId + "):  " + str);

			// Convert it back to EGFullDataDTO object
//			ObjectMapper mapper2 = new ObjectMapper();
			EGFullDataDTO obj = null;
			try
			{
				obj = mapper.readValue(str, EGFullDataDTO.class);
			}
			catch (JsonMappingException e)
			{
				System.out.println("4:  JsonMappingException.  msg=" + e.getMessage());
				str = null;
			}
			catch (JsonParseException e)
			{
				System.out.println("4:  JsonParseException.  msg=" + e.getMessage());
				str = null;
			}
			catch (IOException e)
			{
				System.out.println("4:  IOException.  msg=" + e.getMessage());
				str = null;
			}
			System.out.println("<=======>\nReconstituted EGFullDataDTO for lpId " + lpId + ":  " + obj.toString());
			
			
			// Return a string that the UI likes
			//retStr = STR_333;
			
			// We should return "str from above, but Chris wrote a non-conforming contract into his interface
			// If doing testing so that the JSON doesn't go to Chris's Roxcart, use this
			retStr = str;
		}

		//return retStr;c
		return Response.ok(retStr).header("Access-Control-Allow-Origin",  "*").build();
	}

	
	/*
	 * Test class; fills a UiDto object for testing Jackson
	 */
	private UiDto fillUi3()
	{
		UiDto ret = new UiDto();
		
		ret.setClient("Chrysanthemum Corporation - J");
		ret.setProject("Coriolanus Crystal - J");
		ret.setSimulation("Shard345 - J");
		UiPpt ppt = new UiPpt("Chaz - J","Agilamyus - J", 3333);
		ret.setParticipant(ppt);
		
		UiEval e1 = new UiEval();
		// TODO UiEval should be defined with scores as ints (need Strings to
		// support the current UI logic)  N/A should be a value of 0
		e1.setId("entrepAndCommerce");
		e1.getScores().add("5");
		e1.getScores().add("4");
		e1.getScores().add("3");
		e1.getScores().add("NA");
		e1.setComments("entrepAndCommerce Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua - J.");
		ret.getEvaluations().add(e1);
		
		UiEval e2 = new UiEval();
		e2.setId("formulatingStratAndConcepts");
		e2.getScores().add("3");
		e2.getScores().add("1");
		e2.getScores().add("2");
		e2.getScores().add("4");
		e2.setComments("formulatingStratAndConcepts Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua - J.");
		ret.getEvaluations().add(e2);
		
		UiEval e3 = new UiEval();
		e3.setId("persuadingAndInfluencing");
		e3.getScores().add("5");
		e3.getScores().add("2");
		e3.getScores().add("4");
		e3.getScores().add("3");
		e3.setComments("persuadingAndInfluencing Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua - J.");
		ret.getEvaluations().add(e3);
		
		UiEval e4 = new UiEval();
		e4.setId("relatingAndNetworking");
		e4.getScores().add("1");
		e4.getScores().add("2");
		e4.getScores().add("3");
		e4.getScores().add("NA");
		e4.setComments("relatingAndNetworking Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua - J.");
		ret.getEvaluations().add(e4);
		
		UiEval e5 = new UiEval();
		e5.setId("decidingAndInitAction");
		e5.getScores().add("4");
		e5.getScores().add("3");
		e5.getScores().add("2");
		e5.getScores().add("1");
		e5.setComments("decidingAndInitAction Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua - J.");
		ret.getEvaluations().add(e5);
		
		UiImpact i1 = new UiImpact();
		i1.getScores().add(4);
		i1.getScores().add(3);
		i1.getScores().add(1);
		i1.setComments("impactRatings Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua - J.");
		ret.setImpactRatings(i1);
		
		UiOverall o1 = new UiOverall();
		o1.setOverallPerformanceRating(4);
		o1.setDescription("reviewAndOverallPerformance description Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua - J.");
		o1.setStrengths("reviewAndOverallPerformance strengths Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua - J.");
		o1.setDevelopmentNeeds("reviewAndOverallPerformance developmentNeeds Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua - J.");
		o1.setRealTimeCoachingNotes("reviewAndOverallPerformance realTimeCoachingNotes Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua - J.");
		ret.setReviewAndOverallPerformance(o1);
		
		return ret;
	}


	/*
	 * Test class; fills a UiDto object for testing Jackson
	 */
	private UiDto fillUi4()
	{
		UiDto ret = new UiDto();
		
		ret.setClient("Icebreaker Industries - J");
		ret.setProject("Ice Chipper Project - J");
		ret.setSimulation("ICE123 - J");
		UiPpt ppt = new UiPpt("Ike - J","Ichthyosaurus - J", 4444);
		ret.setParticipant(ppt);

		UiEval e1 = new UiEval();
		// UiEval should be defined with scores as ints (need Strings to
		// support the current UI logic)  N/A should be a value of 0
		e1.setId("creatingAndInnovating");
		e1.getScores().add("1");
		e1.getScores().add("3");
		e1.getScores().add("NA");
		e1.setComments("creatingAndInnovating Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. - J.");
		ret.getEvaluations().add(e1);

		UiEval e2 = new UiEval();
		e2.setId("entrepAndCommerce");
		e2.getScores().add("5");
		e2.getScores().add("4");
		e2.getScores().add("3");
		e2.getScores().add("NA");
		e2.setComments("entrepAndCommerce Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. - J.");
		ret.getEvaluations().add(e2);

		UiEval e3 = new UiEval();
		e3.setId("persuadingAndInfluencing");
		e3.getScores().add("5");
		e3.getScores().add("2");
		e3.getScores().add("4");
		e3.getScores().add("3");
		e3.setComments("persuadingAndInfluencing Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua - J.");
		ret.getEvaluations().add(e3);

		UiEval e4 = new UiEval();
		e4.setId("applyingExpertiseAndTech");
		e4.getScores().add("1");
		e4.getScores().add("2");
		e4.getScores().add("3");
		e4.getScores().add("NA");
		e4.setComments("applyingExpertiseAndTech Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua - J.");
		ret.getEvaluations().add(e4);

		UiEval e5 = new UiEval();
		e5.setId("relatingAndNetworking");
		e5.getScores().add("4");
		e5.getScores().add("3");
		e5.getScores().add("2");
		e5.getScores().add("1");
		e5.setComments("relatingAndNetworking Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua - J.");
		ret.getEvaluations().add(e5);

		UiEval e6 = new UiEval();
		e6.setId("copingWithPressuresAndSetbacks");
		e6.getScores().add("5");
		e6.getScores().add("1");
		e6.getScores().add("1");
		e6.getScores().add("1");
		e6.setComments("copingWithPressuresAndSetbacks Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua - J.");
		ret.getEvaluations().add(e6);

		UiEval e7 = new UiEval();
		e7.setId("achievingPersonalWorkGoals");
		e7.getScores().add("1");
		e7.getScores().add("3");
		e7.getScores().add("5");
		e7.setComments("achievingPersonalWorkGoals Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua - J.");
		ret.getEvaluations().add(e7);

		UiImpact i1 = new UiImpact();
		i1.getScores().add(1);
		i1.getScores().add(2);
		i1.getScores().add(3);
		i1.setComments("impactRatings Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua - J.");
		ret.setImpactRatings(i1);

		UiOverall o1 = new UiOverall();
		o1.setOverallPerformanceRating(4);
		o1.setDescription("reviewAndOverallPerformance description Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua - J.");
		o1.setStrengths("reviewAndOverallPerformance strengths Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua - J.");
		o1.setDevelopmentNeeds("reviewAndOverallPerformance developmentNeeds Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua - J.");
		o1.setRealTimeCoachingNotes("reviewAndOverallPerformance realTimeCoachingNotes Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua - J.");
		ret.setReviewAndOverallPerformance(o1);

		return ret;
	}
	
	
	// Testing thingy for POST
	@POST
	@Path("/eg/testPost")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response postTest(String input)
	{
		String okRet = "{\"response\":\"Got it!\"}";
		String erRet = "{\"response\":\"Ooops\"}";
		
		Response resp;
		
		/// show the input
		System.out.println("<------->\nPost Input=" + input);
		
		// Convert it & show it
		//
		ObjectMapper mapper = new ObjectMapper();
		UiDto obj = null;
		try
		{
			obj = mapper.readValue(input, UiDto.class);
		}
		catch (JsonMappingException e)
		{
			System.out.println("Post test:  JsonMappingException.  msg=" + e.getMessage());
			obj = null;
		}
		catch (JsonParseException e)
		{
			System.out.println("Post test:  JsonParseException.  msg=" + e.getMessage());
			obj = null;
		}
		catch (IOException e)
		{
			System.out.println("Post test:  IOException.  msg=" + e.getMessage());
			obj = null;
		}
		if (obj == null)
		{
			System.out.println("Post test error detected (see above)");
			resp = Response.status(Response.Status.NOT_ACCEPTABLE).entity(erRet).build();
		} else {
			System.out.println("<=======>\nReconstituted UiDto in POST test:  " + obj.toString());
			resp = Response.ok(okRet).build();
		}
		
		return resp;
	}
	
	
	// Testing hookups for IG data fetches

	// Fetch for the first data
	@GET
	@Path("/ig/fetchFirst")
	@Produces(MediaType.APPLICATION_JSON)
	public Response igFetchFirstScreen(@QueryParam("lpId") String lpId, @QueryParam("startFlag") boolean isStartup)
	{
		//Response resp = null;

		/// show the input
		System.out.println("<------->\nIG/fetchFirst(): lp=" + lpId + ", startFlag=" + isStartup);

		// Go get the eg data
		IntegrationGridService igs = null;
		try
		{
			igs = new IntegrationGridService();
		}
		catch (Exception e)
		{
			System.out.println("Error instantiating IntegrationGridService.  msg=" + e.getMessage());
			return null;
		}

		IGSummaryDataDTO dto;
		try
		{
			dto = igs.fetchIGSummary(lpId, isStartup);
		}
		catch (Exception e)
		{
			System.out.println("Exception caught fetching IG.  lpId=" + lpId + ", msg=" + e.getMessage());
			return null;
		}

		System.out.println("<=======>\nIGSummaryDataDTO for lp " + lpId + "\n" + dto.toString());

		// Convert to JSON
		String str;
		ObjectMapper mapper = new ObjectMapper();
		try
		{
			str = mapper.writeValueAsString(dto);
		}
		catch (JsonMappingException e)
		{
			System.out.println("JsonMappingException.  msg=" + e.getMessage());
			str = null;
		}
		catch (JsonGenerationException e)
		{
			System.out.println("JsonGenerationException.  msg=" + e.getMessage());
			str = null;
		}
		catch (IOException e)
		{
			System.out.println("IOException.  msg=" + e.getMessage());
			str = null;
		}

		System.out.println("<=======>\nJSON generated (lpId=" + lpId + "):  " + str);

		// This step done for testing purposes.  Not needed.
		// Convert it back to IGSummaryDataDTO object
		//ObjectMapper mapper2 = new ObjectMapper();
		IGSummaryDataDTO obj = null;
		try
		{
			obj = mapper.readValue(str, IGSummaryDataDTO.class);
		}
		catch (JsonMappingException e)
		{
			System.out.println("JsonMappingException(2).  msg=" + e.getMessage());
			return null;
		}
		catch (JsonParseException e)
		{
			System.out.println("JsonParseException(2).  msg=" + e.getMessage());
			return null;
		}
		catch (IOException e)
		{
			System.out.println("IOException(2).  msg=" + e.getMessage());
			return null;
		}
		System.out.println("<=======>\nReconstituted EGFullDataDTO for lpId " + lpId + ":  " + obj.toString());
		
		return Response.ok(str).header("Access-Control-Allow-Origin",  "*").build();
	}


	// Fetch for the testing dat (no longer used (?)
	@GET
	@Path("/ig/fetchTesting")
	@Produces(MediaType.APPLICATION_JSON)
	public Response igFetchTestData(@QueryParam("pptId") String pptId, @QueryParam("dnaId") Long dnaId,
					@QueryParam("cosOn") boolean cogsOn, @QueryParam("hasAlp") boolean hasAlp, @QueryParam("hasAlp") boolean hasKf4d)
	{
		/// show the input
		System.out.println("<------->\nIG/fetchTest(): pptId=" + pptId + ", dnaId=" + dnaId + ", cogsOn=" + cogsOn + ", hasAlp=" + hasAlp + ", hasKf4d=" + hasKf4d);

		// Go get the ig data
		IntegrationGridService igs = null;
		try
		{
			igs = new IntegrationGridService();
		}
		catch (Exception e)
		{
			System.out.println("Error instantiating IntegrationGridService.  msg=" + e.getMessage());
			return null;
		}

		ArrayList<DNACellDTO> list;
		try
		{
			list = igs.fetchTestingScoreData(pptId, dnaId, cogsOn, hasAlp, hasKf4d);
		} catch (Exception e) {
			System.out.println("Exception caught fetching IG.  msg=" + e.getMessage());
			return null;
		}

		// ToDo NOTE: This DTO result needs a wrapper
		ScoreDataWrapper dto = new ScoreDataWrapper(list);

		System.out.println("<=======>\nScoreDataWrapper for pptId=" + pptId + ", dnaId=" + dnaId + ", cogsOn=" + cogsOn + ", hasAlp=" + hasAlp);
		System.out.println(dto.toString());

		// Convert to JSON
		String str;
		ObjectMapper mapper = new ObjectMapper();
		try
		{
			str = mapper.writeValueAsString(dto);
		}
		catch (JsonMappingException e)
		{
			System.out.println("JsonMappingException.  msg=" + e.getMessage());
			str = null;
		}
		catch (JsonGenerationException e)
		{
			System.out.println("JsonGenerationException.  msg=" + e.getMessage());
			str = null;
		}
		catch (IOException e)
		{
			System.out.println("IOException.  msg=" + e.getMessage());
			str = null;
		}

		System.out.println("<=======>\nJSON generated:  " + str);
//
//		// This step done for testing purposes.  Not needed.
//		// Convert it back to ScoreDataWrapper object
//		ObjectMapper mapper2 = new ObjectMapper();
//		String str2 = null;
//		ScoreDataWrapper obj = null;
//		try
//		{
//			obj = mapper.readValue(str, ScoreDataWrapper.class);
//		}
//		catch (JsonMappingException e)
//		{
//			System.out.println("JsonMappingException(2).  msg=" + e.getMessage());
//			str2 = null;
//		}
//		catch (JsonParseException e)
//		{
//			System.out.println("JsonParseException(2).  msg=" + e.getMessage());
//			str2 = null;
//		}
//		catch (IOException e)
//		{
//			System.out.println("IOException(2).  msg=" + e.getMessage());
//			str2 = null;
//		}
//		System.out.println("<=======>\nReconstituted ScoreDataWrapper:  " + obj.toString());
		
		return Response.ok(str).header("Access-Control-Allow-Origin",  "*").build();
	}


	// Fetch data for a row (competency)
	@GET
	@Path("/ig/fetchRow")
	@Produces(MediaType.APPLICATION_JSON)
	public Response igFetchRowData(@QueryParam("compId") Long compId, @QueryParam("pptId") String pptId, @QueryParam("dnaId") Long dnaId)
	{
		/// show the input
		System.out.println("<------->\nIGfetchRow(): compId=" + compId + ", pptId=" + pptId + ", dnaId=" + dnaId);

		// Go get the ig data
		IntegrationGridService igs = null;
		try
		{
			igs = new IntegrationGridService();
		}
		catch (Exception e)
		{
			System.out.println("Error instantiating IntegrationGridService.  msg=" + e.getMessage());
			return null;
		}

		IGCompDataDTO dto = null;
		try
		{
			dto = igs.fetchCompRowData(compId, pptId, dnaId);
		} catch (Exception e) {
			System.out.println("Exception caught fetching IG.  msg=" + e.getMessage());
			return null;
		}

		System.out.println("<=======>\nIGCompDataDTO for compId=" + compId + ", pptId=" + pptId + ", dnaId=" + dnaId);
		System.out.println(dto.toString());

		// Convert to JSON
		String str;
		ObjectMapper mapper = new ObjectMapper();
		try
		{
			str = mapper.writeValueAsString(dto);
		}
		catch (JsonMappingException e)
		{
			System.out.println("JsonMappingException.  msg=" + e.getMessage());
			str = null;
		}
		catch (JsonGenerationException e)
		{
			System.out.println("JsonGenerationException.  msg=" + e.getMessage());
			str = null;
		}
		catch (IOException e)
		{
			System.out.println("IOException.  msg=" + e.getMessage());
			str = null;
		}

		System.out.println("<=======>\nJSON generated:  " + str);
//
//		// This step done for testing purposes.  Not needed.
//		// Convert it back to ScoreDataWrapper object
//		ObjectMapper mapper2 = new ObjectMapper();
//		String str2 = null;
//		ScoreDataWrapper obj = null;
//		try
//		{
//			obj = mapper.readValue(str, ScoreDataWrapper.class);
//		}
//		catch (JsonMappingException e)
//		{
//			System.out.println("JsonMappingException(2).  msg=" + e.getMessage());
//			str2 = null;
//		}
//		catch (JsonParseException e)
//		{
//			System.out.println("JsonParseException(2).  msg=" + e.getMessage());
//			str2 = null;
//		}
//		catch (IOException e)
//		{
//			System.out.println("IOException(2).  msg=" + e.getMessage());
//			str2 = null;
//		}
//		System.out.println("<=======>\nReconstituted ScoreDataWrapper:  " + obj.toString());
			
		return Response.ok(str).header("Access-Control-Allow-Origin",  "*").build();
	}


	// Data for the test column (column data for the other insturments/sims is fetched via EG data calls
	@GET
	@Path("/ig/fetchTestCol")
	@Produces(MediaType.APPLICATION_JSON)
	public Response igFetchTstColData(@QueryParam("pptId") String pptId, @QueryParam("dnaId") Long dnaId)
	{
		/// show the input
		System.out.println("<------->\nfetchTestCol(): pptId=" + pptId + ", dnaId=" + dnaId);

		// Go get the ig data
		IntegrationGridService igs = null;
		try
		{
			igs = new IntegrationGridService();
		}
		catch (Exception e)
		{
			System.out.println("Error instantiating IntegrationGridService.  msg=" + e.getMessage());
			return null;
		}

		IGInstDisplayData dto = null;
		try
		{
			dto = igs.fetchTestingColumnData(pptId, dnaId);
		} catch (Exception e) {
			System.out.println("Exception caught fetching IG.  msg=" + e.getMessage());
			return null;
		}

		System.out.println("<=======>\nIGCompDataDTO for pptId=" + pptId + ", dnaId=" + dnaId);
		System.out.println(dto.toString());

		// Convert to JSON
		String str = null;
		ObjectMapper mapper = new ObjectMapper();
		try
		{
			str = mapper.writeValueAsString(dto);
		}
		catch (JsonMappingException e)
		{
			System.out.println("JsonMappingException.  msg=" + e.getMessage());
			str = null;
		}
		catch (JsonGenerationException e)
		{
			System.out.println("JsonGenerationException.  msg=" + e.getMessage());
			str = null;
		}
		catch (IOException e)
		{
			System.out.println("IOException.  msg=" + e.getMessage());
			str = null;
		}

		System.out.println("<=======>\nJSON generated:  " + str);
//
//		// This step done for testing purposes.  Not needed.
//		// Convert it back to ScoreDataWrapper object
//		ObjectMapper mapper2 = new ObjectMapper();
//		String str2 = null;
//		ScoreDataWrapper obj = null;
//		try
//		{
//			obj = mapper.readValue(str, ScoreDataWrapper.class);
//		}
//		catch (JsonMappingException e)
//		{
//			System.out.println("JsonMappingException(2).  msg=" + e.getMessage());
//			str2 = null;
//		}
//		catch (JsonParseException e)
//		{
//			System.out.println("JsonParseException(2).  msg=" + e.getMessage());
//			str2 = null;
//		}
//		catch (IOException e)
//		{
//			System.out.println("IOException(2).  msg=" + e.getMessage());
//			str2 = null;
//		}
//		System.out.println("<=======>\nReconstituted ScoreDataWrapper:  " + obj.toString());
					
		return Response.ok(str).header("Access-Control-Allow-Origin",  "*").build();
	}

	
	
	// Code for DRI testing

	// Call fetchReportInput and get a ReportInputDTO object
	@GET
	@Path("/dri/fetchDri")
	@Produces(MediaType.APPLICATION_JSON)
	public Response driFetchData(@QueryParam("pptId") String pptId, @QueryParam("dnaId") Long dnaId)
	{
		// show the input
		System.out.println("<------->\ndriFetchData(): pptId=" + pptId + ", dnaId=" + dnaId);

		// Go get the dri data
		ReportInputService svc = null;
		try
		{
			svc = new ReportInputService();
		}
		catch (Exception e)
		{
			System.out.println("Error instantiating ReportInputService.  msg=" + e.getMessage());
			return null;
		}

		ReportInputDTO dto = null;
		try
		{
			dto = svc.fetchReportInput(pptId, dnaId);
		}
		catch (Exception e) {
			System.out.println("Exception caught fetching DRI.  msg=" + e.getMessage());
			return null;
		}

		System.out.println("<=======>\nReportInputDTO for pptId=" + pptId + ", dnaId=" + dnaId);
		System.out.println(dto.toString());

		// Convert to JSON
		String str = null;
		ObjectMapper mapper = new ObjectMapper();
		try
		{
			str = mapper.writeValueAsString(dto);
		}
		catch (JsonMappingException e)
		{
			System.out.println("JsonMappingException.  msg=" + e.getMessage());
			str = null;
		}
		catch (JsonGenerationException e)
		{
			System.out.println("JsonGenerationException.  msg=" + e.getMessage());
			str = null;
		}
		catch (IOException e)
		{
			System.out.println("Mapper IOException.  msg=" + e.getMessage());
			str = null;
		}

		System.out.println("<=======>\nJSON generated:  " + str);
					
		return Response.ok(str).header("Access-Control-Allow-Origin",  "*").build();
	}

}



