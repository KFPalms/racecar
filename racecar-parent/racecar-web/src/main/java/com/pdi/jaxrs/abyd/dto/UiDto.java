package com.pdi.jaxrs.abyd.dto;

import java.util.ArrayList;

/**
 * Class to be able to unmarshal JSON to Chris F.'s data format.
 * Note that there are subsidiary classes as well for various portions
 */
public class UiDto
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String client;			// Client name
	private String project;		// Project name
	private String simulation;		// Sim name
	private UiPpt participant = new UiPpt();
	private ArrayList<UiEval> evaluations = new ArrayList<UiEval>();
	private UiImpact impactRatings = new UiImpact();
	private UiOverall reviewAndOverallPerformance = new UiOverall();


	//
	// Constructors.
	//
	public UiDto()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//
	
	public String toString()
	{
		String ret = "UiDto\n";
		
		ret += "  Client name  = " + this.client + "\n";
		ret += "  Project name = " + this.project + "\n";
		ret += "  Sim name     = " + this.simulation + "\n";
		ret += "  " + this.participant.toString() + "\n";
		ret += "  Evals\n";
		for (UiEval eval : evaluations)
		{
			ret += "    "  + eval.toString() + "\n";
		}
		ret += "  " + this.impactRatings.toString() + "\n";
		ret += "  " + this.reviewAndOverallPerformance.toString();
		
		return ret;
	}

	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Getters/Setters                                    *//
	//*                                                                                         *//
	//*******************************************************************************************//
	
	/*****************************************************************************************/
	public String getClient()
	{
		return this.client;
	}
	
	public void setClient(String value)
	{
		this.client = value;
	}
	
	/*****************************************************************************************/
	public String getProject()
	{
		return this.project;
	}
	
	public void setProject(String value)
	{
		this.project = value;
	}
	
	/*****************************************************************************************/
	public String getSimulation()
	{
		return this.simulation;
	}
	
	public void setSimulation(String value)
	{
		this.simulation = value;
	}
	
	/*****************************************************************************************/
	public UiPpt getParticipant()
	{
		return this.participant;
	}
	
	public void setParticipant(UiPpt value)
	{
		this.participant = value;
	}
	
	/*****************************************************************************************/
	public ArrayList<UiEval> getEvaluations()
	{
		return this.evaluations;
	}
	
// For now, let's comment this out
//	public void setEvaluations(ArrayList<UiEval> value)
//	{
//		this.evaluations = value;
//	}
	
	/*****************************************************************************************/
	public UiImpact getImpactRatings()
	{
		return this.impactRatings;
	}
	
	public void setImpactRatings(UiImpact value)
	{
		this.impactRatings = value;
	}
	
	/*****************************************************************************************/
	public UiOverall getReviewAndOverallPerformance()
	{
		return this.reviewAndOverallPerformance;
	}

	public void setReviewAndOverallPerformance(UiOverall value)
	{
		this.reviewAndOverallPerformance = value;
	}
}
