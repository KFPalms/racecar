package com.pdi.jaxrs.abyd.dto;

import java.util.ArrayList;

import com.pdi.data.abyd.dto.common.DNACellDTO;

/**
 * wrapper class aroud a list of DNACellDTO elements 
 * @author loa-kbeukelm
 *
 */
public class ScoreDataWrapper {
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private ArrayList<DNACellDTO> scoreData;


	//
	// Constructors.
	//
//	private ScoreDataWrapper()
//	{
//		// Does nothing presently; is hidden
//	}

	public ScoreDataWrapper(ArrayList<DNACellDTO> data)
	{
		this.scoreData = data;
	}

	//
	// Instance methods.
	//
	
	public String toString()
	{
		String ret = "ScoreDataWrapper\n";

		for (DNACellDTO cell : scoreData)
		{
			ret += "    "  + cell.toString() + "\n";
		}
		
		return ret;
	}

	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Getters/Setters                                    *//
	//*                                                                                         *//
	//*******************************************************************************************//
	
	/*****************************************************************************************/
	public ArrayList<DNACellDTO> getScoreData()
	{
		return this.scoreData;
	}
	
	public void setScoreData(ArrayList<DNACellDTO> value)
	{
		this.scoreData = value;
	}

}
