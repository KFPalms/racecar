package com.pdi.jaxrs.abyd;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class IgFetchTest {
	/**
	 * Simple class to fetch the various IG returned strings.  Run manually from the menu in Eclipse 
	 */
	//
	// static variables
	//

	// Test for EG - all data
	//static String firstEgScreenUrl = "http://localhost:8080/pdi-web/rest/abyd/eg/getEg/98558305";
	
	// Get an IGSummaryDataDTO
	// A valid lpId is any in linkparams with moduleId = 0
	// SA DAC... no scores
	//static String firstScreenUrl = "http://localhost:8080/pdi-web/rest/abyd/ig/fetchFirst?lpId=95415816&startFlag=false";
	// Coca Cola Hellenic
	//static String firstScreenUrl = "http://localhost:8080/pdi-web/rest/abyd/ig/fetchFirst?lpId=10191708&startFlag=false";
	// CEO model (overrides)
	//static String firstScreenUrl = "http://localhost:8080/pdi-web/rest/abyd/ig/fetchFirst?lpId=71765567&startFlag=false";
	//Test for the dlci and finalAverage with scores
	//static String firstScreenUrl = "http://localhost:8080/pdi-web/rest/abyd/ig/fetchFirst/4245033";
	//Test for the dlci and finalAverage with NO scores
	//static String firstScreenUrl = "http://localhost:8080/pdi-web/rest/abyd/ig/fetchFirst/55456572";
	
	// Fetch testing score data
	//static String testingDataUrl = "http://localhost:8080/pdi-web/rest/abyd/ig/fetchTesting?pptId=374227&dnaId=9345&cogsOn=true&hasAlp=false";
	//static String testingDataUrl = "http://localhost:8080/pdi-web/rest/abyd/ig/fetchTesting?pptId=390639&dnaId=9519&cogsOn=true&hasAlp=true";
	
	// Fetch row (comp) data
	//static String rowDataUrl = "http://localhost:8080/pdi-web/rest/abyd/ig/fetchRow?compId=1570&pptId=374227&dnaId=9345";
	//static String rowDataUrl = "http://localhost:8080/pdi-web/rest/abyd/ig/fetchRow?compId=1237&pptId=390639&dnaId=9519";
	
	// Fetch testing column data
	//static String tstColDataUrl = "http://localhost:8080/pdi-web/rest/abyd/ig/fetchTestCol?pptId=374227&dnaId=9345";
	//static String tstColDataUrl = "http://localhost:8080/pdi-web/rest/abyd/ig/fetchTestCol?pptId=390639&dnaId=9519";
	
	// Fetch URL
	static String urlDataUrl = "http://localhost:8080/pdi-web/internal/skadmin/getUrls/project/{projId}/ppt/{pptId}";

	public static void main(String[] args)
	{

		String resp = "";
		try
		{
			// Data URL
			//URL url = new URL(firstEgScreenUrl);
			//URL url = new URL(firstScreenUrl);
			//URL url = new URL(testingDataUrl);
			//URL url = new URL(rowDataUrl);
			//URL url = new URL(tstColDataUrl);
			URL url = new URL(urlDataUrl);

			System.out.println("URL=" + url.toString());
			
			// Get a connection
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");

			// Get result
			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String buf = null;
			while ((buf = br.readLine())!=null)
			{
				resp += buf;
			}
			
			br.close();
		}
		catch (MalformedURLException e)
		{
			System.out.println("Malformed URL");
		}
		catch (IOException e)
		{
			System.out.println("IO Exception");
		}

		System.out.println(resp);
    }	// End of Main
}
