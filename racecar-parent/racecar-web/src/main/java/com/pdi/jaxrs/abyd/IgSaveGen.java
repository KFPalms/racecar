package com.pdi.jaxrs.abyd;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.pdi.data.abyd.dto.intGrid.IGFinalScoreDTO;
import com.pdi.data.abyd.dto.intGrid.IGLcNameDTO;


// A clas wherein I fill various DTOs and gen the JSON from it
public class IgSaveGen {
	
	// IGLcNameDTO - used in saveLcName()
	// IGFinalScoreDTO - used in saveFinalScore()
	// There are a number of methods that simply pass parameters; no JSON generated here
	//	-- submitIG(String candId, long dnaId)
	//	-- saveWorkingNotes(String candId, long dnaId, String notes)
	
	public static void main(String[] args)
	{
//		// Do the name dto
//		IGLcNameDTO dto = new IGLcNameDTO();
//		dto.setPartId("9999");
//		dto.setDnaId(99);
//		dto.setLcFirstName("Frank");
//		dto.setLcLastName("Farquar");
//	
//		ObjectMapper objectMapper = new ObjectMapper();
//		String str;
//		try
//		{
//			str = objectMapper.writeValueAsString(dto);
//		}
//		catch (JsonMappingException e)
//		{
//			System.out.println("1:  JsonMappingException.  msg=" + e.getMessage());
//			str = null;
//		}
//		catch (JsonGenerationException e)
//		{
//			System.out.println("1:  JsonGenerationException.  msg=" + e.getMessage());
//			str = null;
//		}
//		catch (IOException e)
//		{
//			System.out.println("1:  IOException.  msg=" + e.getMessage());
//			str = null;
//		}
//			
//		System.out.println("<=======>\nIGLcNameDTO JSON:  " + str);


		// Do the final score dto
		IGFinalScoreDTO dto = new IGFinalScoreDTO();
		dto.setPartId("9999");
		dto.setDnaId(99);
		dto.setCompId(1234);
		dto.setCompScoreIdx(9);
		dto.setCompNote("Comp note");

		ObjectMapper objectMapper = new ObjectMapper();
		String str;
		try
		{
			str = objectMapper.writeValueAsString(dto);
		}
		catch (JsonMappingException e)
		{
			System.out.println("1:  JsonMappingException.  msg=" + e.getMessage());
			str = null;
		}
		catch (JsonGenerationException e)
		{
			System.out.println("1:  JsonGenerationException.  msg=" + e.getMessage());
			str = null;
		}
		catch (IOException e)
		{
			System.out.println("1:  IOException.  msg=" + e.getMessage());
			str = null;
		}
			
		System.out.println("<=======>\nIGFinalScoreDTO JSON:  " + str);

	}
}
