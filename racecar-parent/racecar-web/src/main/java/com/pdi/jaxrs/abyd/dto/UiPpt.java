package com.pdi.jaxrs.abyd.dto;

/**
 * Subsidiary class to UiDto to be able to unmarshal JSON to Chris F.'s data format.
 */
public class UiPpt
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String firstName;	// Ppt first name
	private String lastName;	// Ppt last name
	private long id;			// Ppt ID


	//
	// Constructors.
	//
	public UiPpt()
	{
		// Does nothing presently
	}
	
	public UiPpt(String fn, String ln, long id)
	{
		this.firstName = fn;
		this.lastName = ln;
		this.id = id;
	}

	//
	// Instance methods.
	//
	
	public String toString()
	{
		return "UiPpt:  FN=" + this.firstName + ", LN=" + this.lastName + ", ID=" + this.id;
	}

	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Getters/Setters                                    *//
	//*                                                                                         *//
	//*******************************************************************************************//
	
	/*****************************************************************************************/
	public String getFirstName()
	{
		return this.firstName;
	}
	
	public void setFirstName(String value)
	{
		this.firstName = value;
	}
	
	/*****************************************************************************************/
	public String getLastName()
	{
		return this.lastName;
	}
	
	public void setLastName(String value)
	{
		this.lastName = value;
	}

	/*****************************************************************************************/
	public long getId()
	{
		return this.id;
	}
	
	public void setId(long value)
	{
		this.id = value;
	}

}
