package com.pdi.jaxrs.abyd;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * Simple clas to ensure that posted data is transmitted and also to check the server side
 * to ensure that it was present and viable.  Was run manually from the menu in Eclipse 
 */
public class PostTest {

	//
	// static variables
	//

	static String testUrl = "http://localhost:8080/pdi-web/rest/abyd/eg/testPost";
	// Test string in the Chris F test format (so I can use UiDto to unpack it to an object)
	static String Tester = "{\"client\":\"PostTest Corp.\",\"project\":\"Test POST\",\"simulation\":\"Sim sim II\",\"participant\":{\"firstName\":\"Test\",\"lastName\":\"Post\",\"id\":\"1234567\"},\"evaluations\":[{\"id\":\"entrepAndCommerce\",\"scores\":[\"5\",\"4\",\"3\",\"NA\"],\"comments\":\"entrepAndCommerce Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\"},{\"id\":\"formulatingStratAndConcepts\",\"scores\":[3,1,2,4],\"comments\":\"formulatingStratAndConcepts Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\"},{\"id\":\"persuadingAndInfluencing\",\"scores\":[5,2,4,3],\"comments\":\"persuadingAndInfluencing Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\"},{\"id\":\"relatingAndNetworking\",\"scores\":[1,2,3,\"NA\"],\"comments\":\"relatingAndNetworking applyingExpertiseAndTech Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\"},{\"id\":\"decidingAndInitAction\",\"scores\":[4,3,2,1],\"comments\":\"decidingAndInitAction Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\"}],\"impactRatings\":{\"scores\":[4,3,1],\"comments\":\"impactRatings Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\"},\"reviewAndOverallPerformance\":{\"overallPerformanceRating\":4,\"description\":\"reviewAndOverallPerformance description Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\",\"strengths\":\"reviewAndOverallPerformance strengths Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\",\"developmentNeeds\":\"reviewAndOverallPerformance developmentNeeds Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\",\"realTimeCoachingNotes\":\"reviewAndOverallPerformance realTimeCoachingNotes Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\"}}";


	public static void main(String[] args)
	{
		
		Client client = Client.create();
		WebResource wr = client.resource(testUrl);
		
		ClientResponse resp = wr.type("application/json").post(ClientResponse.class, Tester);
		
		System.out.println("<+++++++>\n    Status code returned=" + resp.getStatus());
		System.out.println("    Data Returned=" + resp.getEntity(String.class));
		
		int bp = 0;
    }

}
