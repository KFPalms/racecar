package com.pdi.jaxrs.abyd.dto;

/**
 * Subsidiary class to UiDto to be able to unmarshal JSON to Chris F.'s data format.
 */
public class UiOverall
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private int overallPerformanceRating = 0;
	private String description;
	private String strengths;
	private String developmentNeeds;
	private String realTimeCoachingNotes;

		//
		// Constructors.
		//
		public UiOverall()
		{
			// Does nothing presently
		}
	
		//
		// Instance methods.
		//

		public String toString()
		{
			String ret = "UiOverall:  rating=" + this.overallPerformanceRating + "\n";
			ret += "      desc       =" + this.description + "\n";
			ret += "      strengths  =" + this.strengths + "\n";
			ret += "      needs      =" + this.developmentNeeds + "\n";
			ret += "      coachNotes =" + this.realTimeCoachingNotes + "\n";
			
			return ret;
		}

		//*******************************************************************************************//
		//*                                                                                         *//
		//*                                      Getters/Setters                                    *//
		//*                                                                                         *//
		//*******************************************************************************************//
		
		/*****************************************************************************************/
		public int getOverallPerformanceRating()
		{
			return this.overallPerformanceRating;
		}

		public void setOverallPerformanceRating(int value)
		{
			this.overallPerformanceRating = value;
		}

		/*****************************************************************************************/
		public String getDescription()
		{
			return this.description;
		}

		public void setDescription(String value)
		{
			this.description = value;
		}

		/*****************************************************************************************/
		public String getStrengths()
		{
			return this.strengths;
		}

		public void setStrengths(String value)
		{
			this.strengths = value;
		}

		/*****************************************************************************************/
		public String getDevelopmentNeeds()
		{
			return this.developmentNeeds;
		}

		public void setDevelopmentNeeds(String value)
		{
			this.developmentNeeds = value;
		}

		/*****************************************************************************************/
		public String getRealTimeCoachingNotes()
		{
			return this.realTimeCoachingNotes;
		}

		public void setRealTimeCoachingNotes(String value)
		{
			this.realTimeCoachingNotes = value;
		}
}
