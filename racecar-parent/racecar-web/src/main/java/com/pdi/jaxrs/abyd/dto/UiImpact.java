package com.pdi.jaxrs.abyd.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Subsidiary class to UiDto to be able to unmarshal JSON to Chris F.'s data format.
 */
public class UiImpact
{

	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private List<Integer> scores =  new ArrayList<Integer>();
	private String comments;


	//
	// Constructors.
	//
	public UiImpact()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//

	public String toString()
	{
		String ret = "UiImpact:  scores=";
		for (Integer str : this.scores)
		{
			ret += str + ", ";
		}
		ret = ret.substring(0, ret.length() - 2);
		ret += ", comment=" + this.comments;

		return ret;
	}
	
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Getters/Setters                                    *//
	//*                                                                                         *//
	//*******************************************************************************************//
	
	/*****************************************************************************************/
	public List<Integer> getScores()
	{
		return this.scores;
	}
	
// For now, let's comment out the setter
//	public void setScores(ArrayList<String> value)
//	{
//		this.scores = value;
//	}

	/*****************************************************************************************/
	public String getComments()
	{
		return this.comments;
	}
	
	public void setComments(String value)
	{
		this.comments = value;
	}


}
