package com.pdi.jaxrs.abyd.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Subsidiary class to UiDto to be able to unmarshal JSON to Chris F.'s data format.
 */
public class UiEval {
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private String id;	
	private List<String> scores =  new ArrayList<String>();
	private String comments;


	//
	// Constructors.
	//
	public UiEval()
	{
		// Does nothing presently
	}

	//
	// Instance methods.
	//

	public String toString()
	{
		String ret = "UiEval:  ID=" + this.id + ", scores: ";
		for (String str : this.scores)
		{
			ret += str + ", ";
		}
		ret = ret.substring(0, ret.length() - 2);
		ret += ", comment=" + this.comments;

		return ret;
	}
	
	
	//*******************************************************************************************//
	//*                                                                                         *//
	//*                                      Getters/Setters                                    *//
	//*                                                                                         *//
	//*******************************************************************************************//
	
	/*****************************************************************************************/
	public String getId()
	{
		return this.id;
	}
	
	public void setId(String value)
	{
		this.id = value;
	}
	
	/*****************************************************************************************/
	public List<String> getScores()
	{
		return this.scores;
	}
	
// For now, let's comment out the setter
//	public void setScores(ArrayList<String> value)
//	{
//		this.scores = value;
//	}

	/*****************************************************************************************/
	public String getComments()
	{
		return this.comments;
	}
	
	public void setComments(String value)
	{
		this.comments = value;
	}

}
