package com.pdi.jaxrs.abyd;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class DriFetch {
	/**
	 * Simple class to fetch the various IG returned strings.  Run manually from the menu in Eclipse 
	 */

	//
	// static variables
	//

	// Get a ReportInputDTO
		// MOHG MLL Test - 386084/105
	//static String tstUrl = "http://localhost:8080/pdi-web/rest/abyd/dri/fetchDri?pptId=386084&dnaId=105";
		// Early ALR - 386339/9383
	//static String tstUrl = "http://localhost:8080/pdi-web/rest/abyd/dri/fetchDri?pptId=386339&dnaId=9383";
		// ALR BUL (Auto) - 386520/9490
	static String tstUrl = "http://localhost:8080/pdi-web/rest/abyd/dri/fetchDri?pptId=386520&dnaId=9490";

	// Fetch row (comp) data
//	//static String rowDataUrl = "http://localhost:8080/pdi-web/rest/abyd/ig/fetchRow?compId=1570&pptId=374227&dnaId=9345";
//	//static String rowDataUrl = "http://localhost:8080/pdi-web/rest/abyd/ig/fetchRow?compId=1237&pptId=390639&dnaId=9519";

	public static void main(String[] args)
	{
		String resp = "";
		try
		{
			// Data URL
			URL url = new URL(tstUrl);

			System.out.println("URL=" + url.toString());
			
			// Get a connection
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");

			// Get result
			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String buf = null;
			while ((buf = br.readLine())!=null)
			{
				resp += buf;
			}
			
			br.close();
		}
		catch (MalformedURLException e)
		{
			System.out.println("Malformed URL");
		}
		catch (IOException e)
		{
			System.out.println("IO Exception");
		}
		finally {};

		System.out.println(resp);
   }	// End of Main

}
