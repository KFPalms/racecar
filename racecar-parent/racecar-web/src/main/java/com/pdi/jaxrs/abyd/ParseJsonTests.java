package com.pdi.jaxrs.abyd;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

public class ParseJsonTests
{
	static String INP_FILE = "C:/Users/kbeukelm/Documents/Enterprise.AssessmentScored.Data.json";

	public static void main(String[] args)
	{
		//// Just read the file
		//try
		//{
		//	BufferedReader br = new BufferedReader(new FileReader(INP_FILE));
		//	
		//	String line = null;
		//	int cnt = 0;
		//	while (cnt < 10)
		//	{
		//		line = br.readLine();
		//		if (line == null)
		//		{
		//			System.out.println("EOF found...");
		//		}
		//		cnt++;
		//		System.out.println("Line " + cnt + ":  " + line);
		//	}
		//	System.out.println("DONE:  Read " + cnt + " lines.");
		//}
		//catch (FileNotFoundException e)
		//{
		//	System.out.println("File " + INP_FILE + " not found.");
		//}
		//catch (IOException e)
		//{
		//	System.out.println("Error reading file.  Msg=" + e.getMessage());
		//}
		//finally {}
		
		// Read up a JSON tree
		ObjectMapper mapper = new ObjectMapper();
		try
		{
			// Read from String
			String jsonStr = new String(Files.readAllBytes(Paths.get(INP_FILE)));
			JsonNode root = mapper.readTree(jsonStr);
			
			//// Read from file
			//JsonNode root = mapper.readTree(new File(INP_FILE));
			
			JsonNode driversNode = root.path("results").path("candidate").path("drivers");
			//System.out.println("Drivers node:  " + driversNode.asText());
			if (driversNode.isArray())
			{
				//System.out.println("Got an array");
				System.out.println("Drivers:");
				for(JsonNode node: driversNode)
				{
					System.out.println("  label=" + node.path("Clabel").asText());
				}
			}

			JsonNode traitsNode = root.path("results").path("candidate").path("traits");
			if (traitsNode.isArray())
			{
				//System.out.println("Got an array");
				System.out.println("Traits:");
				for(JsonNode node: traitsNode)
				{
					System.out.println("  label=" + node.path("Clabel").asText() + ", score=" + node.path("normedScore").asDouble());
				}
			}

		}
		catch (JsonProcessingException e)
		{
			System.out.println("Error parsing " + INP_FILE + ":  Msg=" + e.getMessage());
		}
		catch (IOException e)
		{
			System.out.println("Error reading file.  Msg=" + e.getMessage());
		}
		finally {}
	}
	
	//File json = new File(INP_FILE);
}
