package com.pdi.jaxrs.abyd;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class GenDormantDRIExtract
{
	/**
	 * Simple class to invoke the RESTful service that generates extract
	 * candidate rows from dormant DRI entries.  Run manually. 
	 */

	//
	// static variables
	//

	static String invocationString = "http://localhost:8080/pdi-web/internal/int/dormantExtCand";
	

	public static void main(String[] args)
	{
		String resp = "";
		try
		{
			URL url = new URL(invocationString);

			//System.out.println("URL=" + url.toString());

			// Get a connection
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("POST");

			// Get result
			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String buf = null;
			while ((buf = br.readLine())!=null)
			{
				resp += buf + "\n";
			}
			
			br.close();
		}
		catch (MalformedURLException e)
		{
			System.out.println("Malformed URL");
		}
		catch (IOException e)
		{
			System.out.println("IO Exception");
		}

		System.out.println(resp);
    }	// End of Main

}
