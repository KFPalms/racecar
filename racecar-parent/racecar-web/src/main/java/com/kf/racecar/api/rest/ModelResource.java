package com.kf.racecar.api.rest;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HEAD;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kf.palms.utils.JNDILookupEJB;
import com.kf.racecar.ejb.dao.DNADao;
import com.kf.racecar.ejb.dao.ModelDao;
import com.kf.racecar.ejb.entity.DNA;
import com.kf.racecar.ejb.entity.Model;
import com.kf.racecar.ejb.model.ModelServiceImpl;
import com.kf.racecar.ejb.model.ModelServiceLocal;
import com.pdi.jaxrs.abyd.dto.EntityListItemDto;
import com.pdi.jaxrs.abyd.dto.ModelListDTO;

@Path("model")
public class ModelResource {
	
	private static final Logger log = LoggerFactory.getLogger(ModelResource.class);
	
	@EJB
	DNADao dnaDao;
	
	@EJB
	ModelDao modelDao;
	
	@EJB
	ModelServiceLocal modelService;
	
	@PostConstruct
	private void init(){
		log.debug("Initializing Model Service");
		getDNADao();
		getModelDao();
		getModelService();
	}
	
	@GET
	@Path("/template/{dnaId}")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public DNA getDNA(@PathParam(value = "dnaId")int dnaId){
		DNA dna = dnaDao.findDnaByIdBatchHints(dnaId);
		log.debug("Got DNA entity: {}", dna.getDnaName());
		return dna;
	}
	
	@GET
	@Path("/{modelId}")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public DNA getDNATemplateForModel(@PathParam(value = "modelId")int modelId){
		DNA dna = dnaDao.findDnaTemplateByModelId(modelId);
		log.debug("Got DNA entity: {}", dna.getDnaName());
		return dna;
	}
	
	@GET
	@Path("/list")
	public Response getModelList(){
		ObjectMapper mapper = new ObjectMapper();
		String retStr = null;
		ModelListDTO modelList = new ModelListDTO();
		
		List<Model> modelEntities = modelDao.findAll();
		
		for (Model m : modelEntities){
			EntityListItemDto model = new EntityListItemDto();
			model.setId(m.getModelId());
			model.setLabel(m.getInternalName());
			model.setActive(m.isActive());
			model.setSequence(m.getModelSequence());
			model.setModified(m.getLastDate());
			
			modelList.getModelList().add(model);
		}
		
		try{
			retStr = mapper.writeValueAsString(modelList);
		} catch (Exception e){
			log.error("Failed converting model list to json string, with this message: {}", e.getMessage());
		}
		
		return Response.ok(retStr).header("Access-Control-Allow-Origin", "*").build();
	}
	
	/**
	 * This doesn't work yet.  Proceed with caution
	 * @param modelId
	 * @param user
	 * @return
	 */
	@POST
	@Produces({MediaType.TEXT_PLAIN})
	@Path("/copy/{modelId}/{user}")
	public Response copyModel(@PathParam(value = "modelId") int modelId, @PathParam(value = "user") String user){
		
		
		try {
			int newModelId = modelService.copyModel(modelId, user);
			return Response.ok(String.valueOf(newModelId), MediaType.TEXT_PLAIN_TYPE).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.serverError().entity(e.getMessage()).build();
		}

	}
	
	@OPTIONS
	@Consumes(MediaType.WILDCARD)
	@Path("/list")
	public Response optionsGetModelList(){
		return optionsResponse();
	}
	
	private Response optionsResponse()
	{
		return Response.ok("Success")
				.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT")
				.header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Cache-Control, Authorization, Pragma, Expires")
				.header("Access-Control-Expose-Headers", "newToken")
				.header("Access-Control-Allow-Origin", "*")
				.build();
	}

	private DNADao getDNADao() {
		if (dnaDao == null){
			dnaDao = (DNADao) JNDILookupEJB
					.lookupBeanInGlobalContext(DNADao.class.getName());
		}
		return dnaDao;
		
	}
	
	private ModelDao getModelDao(){
		if (modelDao == null){
			modelDao = (ModelDao) JNDILookupEJB
					.lookupBeanInGlobalContext(ModelDao.class.getName());
		}
		return modelDao;
	}
	
	private ModelServiceLocal getModelService() {
		if (modelService == null){
			modelService = (ModelServiceImpl) JNDILookupEJB.lookupBeanInGlobalContext(ModelServiceImpl.class.getName());
		}
		return modelService;
	}

}
