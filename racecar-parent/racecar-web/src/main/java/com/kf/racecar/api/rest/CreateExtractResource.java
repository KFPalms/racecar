package com.kf.racecar.api.rest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.StringReader;
import java.net.URLEncoder;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.kf.palms.utils.JNDILookupEJB;
import com.kf.racecar.ejb.alr.GroupExtractServiceLocal;
import com.kf.racecar.ejb.alr.IndividualExtractServiceLocal;
import com.pdi.xml.XMLUtils;

@Path("extract")
public class CreateExtractResource {
	private static final Logger log = LoggerFactory.getLogger(CreateExtractResource.class);
	
	@EJB
	IndividualExtractServiceLocal extractService;
	
	@EJB
	GroupExtractServiceLocal groupExtractService;
	
	@PostConstruct
	private void init(){
		log.debug("Initializing Extract Service");
		getExtractService();
		getGroupExtractService();
	}
	
	private IndividualExtractServiceLocal getExtractService() {
		if (extractService == null){
			extractService = (IndividualExtractServiceLocal) JNDILookupEJB
					.lookupBeanInGlobalContext(IndividualExtractServiceLocal.class.getName());
		}
		return extractService;
	}
	
	private GroupExtractServiceLocal getGroupExtractService() {
		if (groupExtractService == null){
			groupExtractService = (GroupExtractServiceLocal) JNDILookupEJB
					.lookupBeanInGlobalContext(GroupExtractServiceLocal.class.getName());
		}
		return groupExtractService;
	}
	
	@GET
	@Path("/{reportCode}/{participantId}/{projectId}")
	public Response createParticipantExtract(@PathParam(value = "reportCode") String reportCode, 
			@PathParam(value = "participantId") int participantId, @PathParam(value = "projectId") int projectId){
		
		log.debug("Got extract request of type: {}, ppt: {}, project: {}", reportCode, participantId, projectId);
		
		try {
			extractService.makeExtract(participantId, projectId, reportCode);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.serverError()
					.type(MediaType.TEXT_HTML).header("Error", e.getMessage()).build();
		}
		
		Response response = Response.ok()
				.type(MediaType.APPLICATION_XML).build();
		return response;
	}
	
	@POST
	@Path("/group")
	@Produces({ MediaType.APPLICATION_OCTET_STREAM, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Response handleGroupExtractRequest(@FormParam(value = "xml") String xml, @FormParam(value = "reportRequestId") int reportRequestId){
		Response response = null;
		
		log.debug("incoming parameter xml: {}", xml);

		// build the Doc object
		DocumentBuilderFactory dbfac = null;
		DocumentBuilder docBuilder = null;
		Document doc = null;
		try {
			dbfac = DocumentBuilderFactory.newInstance();
			docBuilder = dbfac.newDocumentBuilder();
			doc = docBuilder.parse(new InputSource(new StringReader(xml)));
			
			groupExtractService.generateGroupExtract(doc, reportRequestId);
			
		} catch (Exception e){
			log.error("CreateExtractResource report XML Sax error: , message: {}", e.getMessage());
			e.printStackTrace();
			return Response.serverError()
					.type(MediaType.TEXT_HTML).header("Error", e.getMessage()).build();
		}
		
		log.debug("Started asynchronous processing of the group extract.  Return OK to PALMS API ");
		
		response = Response.ok()
				.type(MediaType.APPLICATION_XML).build();
		return response;
	}
	
	@POST
	@Path("/")
	@Produces({ MediaType.APPLICATION_OCTET_STREAM, MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Response handleExtractRequest(@FormParam(value = "xml") String xml, @FormParam(value = "reportRequestId") int reportRequestId){
		// We have XML... Prepare to extract data
		
		Response response;
		log.debug("incoming parameter xml: {}", xml);

		// build the Doc object
		DocumentBuilderFactory dbfac = null;
		DocumentBuilder docBuilder = null;
		Document doc = null;
		try {
			dbfac = DocumentBuilderFactory.newInstance();
			docBuilder = dbfac.newDocumentBuilder();
			doc = docBuilder.parse(new InputSource(new StringReader(xml)));
			
			NodeList nodes = XMLUtils.getElements(doc, "//ReportRequest");
			
			String reportCode;

			for (int i = 0; i < nodes.getLength(); i++) {
			
				Node node = nodes.item(i);

				reportCode = node.getAttributes().getNamedItem("code").getTextContent();
				
				NodeList participants = XMLUtils.getElements(node, "Participants/Participant");
				
				if (reportCode.equals("REPORT_SUB_TASK")){
					Node participantNode = participants.item(0);
					// Get the ppt ID
					String participantId = participantNode.getAttributes().getNamedItem("participantId").getTextContent();
					String projectId = participantNode.getAttributes().getNamedItem("projectId").getTextContent();
					/** TODO : this is where we would need to know the type of the parent report, ... if we had different types of subtasks **/
					
					String json = extractService.makeExtract(Integer.valueOf(participantId), Integer.valueOf(projectId), reportCode);
					
					if (json == null){
						throw new Exception("Extract failed for participant: " + participantId + ", project: " + projectId);
					}
					
					String fn = participantId + "_" + projectId + "_ALRExtract";
					
				    response = Response.ok(json, MediaType.APPLICATION_JSON)
				    		.header("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fn + ".json", "UTF-8"))
				    		.build();
				    return response;
				    
				} else {
					log.debug("Unrecognized report type.  Not supported by this API endpoint.");
				}
			}
		} catch (Exception e) {
			log.error("CreateExtractResource report XML Sax error: , message: {}", e.getMessage());
			e.printStackTrace();
			return Response.serverError()
					.type(MediaType.TEXT_HTML).header("Error", e.getMessage()).build();
		} 
		
		response = Response.ok()
				.type(MediaType.APPLICATION_XML).build();
		return response;
	}
	
}
