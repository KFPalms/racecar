package com.kf.racecar.web;

import java.io.IOException;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kf.palms.web.rest.security.AbstractServiceAuthorizationFilter;

public class RacecarRequestSecurityFilter extends AbstractServiceAuthorizationFilter {
	
	private static final Logger log = LoggerFactory.getLogger(RacecarRequestSecurityFilter.class);
	
	@Resource(mappedName = "java:global/custom/abyd_properties")
	private Properties properties;

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		this.setupFilterKeys(properties.getProperty("partnerKeys"), properties.getProperty("palms.api.secretKey"));
		
	}
	
	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2) throws IOException,
	ServletException {
		HttpServletRequest request = (HttpServletRequest) arg0;
		String token = request.getHeader("Token");
		String tokenType = request.getHeader("Token_Type");
		String nonce = request.getHeader("Nonce");
		String tstamp = request.getHeader("TimeStamp");
		String apiSignature = request.getHeader("API_Signature");
		String version = request.getHeader("Version");
		String partnerKey = request.getHeader("PartnerKey");
		String requestMethod = request.getMethod();
		
		log.trace("Token: {}, Token_Type: {}, Nonce: {}, TimeStamp: {}, API_Signature: {}, Version: {}, "
				+ "PartnerKey: {}, Request Method: {}", 
				token, tokenType, nonce, tstamp, apiSignature, version, partnerKey, requestMethod);
		
		super.doFilter(arg0, arg1, arg2);
	}

}
