package com.kf.racecar.web;

import javax.servlet.annotation.WebServlet;

import com.kf.palms.web.servlet.AbstractConfigureLogbackServlet;

@WebServlet(urlPatterns = { "/ConfigureLogBack" }, loadOnStartup = 1)
public class RacecarConfigLogbackServlet extends AbstractConfigureLogbackServlet {
	private static final long serialVersionUID = 1L;
}
