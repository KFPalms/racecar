package com.kf.racecar.web;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

public class SecurityHeaderFilter implements Filter {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletResponse res = (HttpServletResponse) response;
		
		//Appease the security nazis
        res.setHeader("X-Content-Type-Options", "nosniff");
        res.setHeader("Strict-Transport-Security", "max-age=31536000; includeSubDomains");
        res.addHeader("X-FRAME-OPTIONS", "DENY" );
        res.addHeader("Content-Security-Policy", "default-src *.kornferry.com localhost:8080 *.kfnaqa.com; script-src 'unsafe-inline'; style-src 'unsafe-inline'");
        res.addHeader("Referrer-Policy", "strict-origin-when-cross-origin");

        
        chain.doFilter(req, res);
		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}

}
