<?xml version="1.0" encoding="UTF-8"?>
<%@page import="com.pdi.properties.PropertyLoader"%>
<%@page import="com.pdi.listener.portal.flex.PortalFlexServiceV2"%>
<%@page import="com.pdi.data.abyd.util.AbyDDatabaseUtils"%>
<%@page import="com.pdi.data.nhn.helpers.NhnScoreDataHelper"%>
<%@page import="com.pdi.data.v2.helpers.UserDataHelper"%>
<%
String participantId = request.getParameter("participantId");
String projectId = request.getParameter("projectId");
String moduleId = request.getParameter("moduleId");
String value = "";
%>

<status>
<%
try {
	value = PropertyLoader.getProperty("application","global.environment");
%>
	<check type="ENVIRONMENT" value="<%=value%>"/>
<%
} catch (Exception e) {
	%><check type="ENVIRONMENT" value="ERROR"></check><%
}
%>

<%
try {
	value = PropertyLoader.getProperty("application","global.version");
%>
	<check type="VERSION" value="<%=value%>"/>
<%
} catch (Exception e) {
	%><check type="VERSION" value="ERROR"></check><%
}
%>

<%
try {
	value = new UserDataHelper().getUser("cdunn", "cdunn") != null ? "SUCCESSFUL" : "ERROR";
%>
	<check type="V2_ETR" value="<%=value%>"/>
<%
} catch (Exception e) {
	%><check type="V2_ETR" value="ERROR"></check><%
}
%>
	
<%
try {
	value = AbyDDatabaseUtils.testConnection() ? "SUCCESSFUL" : "ERROR";
	%>
	<check type="V2_DATABASE" value="<%=value%>"/>
<%
} catch (Exception e) {
	%><check type="V2_DATABASE" value="ERROR"></check><%
}
%>

<%
try {

	value = NhnScoreDataHelper.fetchNhnData(participantId, projectId, moduleId).getDisplayData("NHN_STATUS").equalsIgnoreCase("1") ? "SUCCESSFUL" : "ERROR";
	%>
	<check type="CAST_IRON" value="<%=value%>"/>
<%
} catch (Exception e) {
	%><check type="CAST_IRON" value="ERROR"><%e.printStackTrace(); %></check><%
}
%>
	
	
	
</status>