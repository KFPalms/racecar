<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login Attempt Failed</title>

<style type="text/css">
<!--
body {
	font: 100%/1.4 Arial, Helvetica, sans-serif;
	background-color: #C7C2BA;
	margin: 0;
	padding: 0;
	color: #000;
}

ul,ol,dl {
	padding: 0;
	margin: 0;
}

a img {
	border: none;
}

a:link {
	color: #42413C;
	text-decoration: underline;
}

a:visited {
	color: #6E6C64;
	text-decoration: underline;
}

a:hover,a:active,a:focus {
	text-decoration: none;
}

.container {
	
}

input {
	width: 250px;
	padding:4px;
}

.btn {
	width: 100px !important;
}

.content {
	background: none repeat scroll 0 0 #F9F9F9;
	border-radius: 4px 4px 4px 4px;
	border: #000 1px solid;
	display: block;
	margin: 15px auto;
	padding: 20px 15px 30px;
	width: 360px;
}

table {
	border-collapse: collapse;
	width: 100%;
	display: block;
	table-layout: fixed;
	border: 0;
	padding: 4px 4px 4px 0;
	margin: 0 auto;
}

table td {
	padding: 6px;
}

.instructs {
	font-size: 14px;
	padding-top: 5px;
}
-->
</style>
</head>

<body>

	<div class="container">
		<div class="content">
			<form method="post"
				action='<%=response.encodeURL("j_security_check")%>'>
				<table>
				<tr><td colspan="2"><img src="/pdi-web/images/kf_logo_black.png" alt="PDI Ninth House"/></td></tr>
					<tr><td colspan="2">Sorry, that username/password is invalid.<hr/></td></tr>
					<tr>
						<td align="right">Username</td>
						<td align="left"><input type="text" name="j_username"
							size="20">
						</td>
					</tr>
					<tr>
						<td align="right">Password</td>
						<td align="left"><input type="password" name="j_password"
							size="20">
						</td>
					</tr>
					<tr>
						<td></td>
						<td><input type="submit" value="Login">
						</td>

					</tr>
				</table>
			</form>
			<!-- end .content -->
		</div>
		<!-- end .container -->
	</div>
</body>
</html>
