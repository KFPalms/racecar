<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isErrorPage="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>File not found...</title>
<style type="text/css">
<!--
body {
	font: 100%/1.4 Arial, Helvetica, sans-serif;
	background-color: #C7C2BA;
	margin: 0;
	padding: 0;
	color: #000;
}
ul, ol, dl {
	padding: 0;
	margin: 0;
}
a img {
	border: none;
}
a:link {
	color: #42413C;
	text-decoration: underline;
}
a:visited {
	color: #6E6C64;
	text-decoration: underline;
}
a:hover, a:active, a:focus {
	text-decoration: none;
}
.container {
}
input {
	width: 250px;
	padding: 4px;
}
.btn {
	width: 100px !important;
}
.content {
	background: none repeat scroll 0 0 #F9F9F9;
	border-radius: 4px 4px 4px 4px;
	border: #000 1px solid;
	display: block;
	margin: 15px auto;
	padding: 20px 15px 30px;
	width: 360px;
}
table {
	border-collapse: collapse;
	width: 100%;
	display: block;
	table-layout: fixed;
	border: 0;
	padding: 4px 4px 4px 0;
	margin: 0 auto;
}
table td {
	padding: 6px;
}
.instructs {
	font-size: 14px;
	padding-top: 5px;
}
.tiny-text {
	font-size: 10px;
	color: #666666;
}
-->
</style>
</head>

<body>
<div class="container">
  <div class="content">
    <p> 
      <!-- the path needs the ../ to work on QA. is different on localhost --> 
      <img src="../images/kf_logo_black.png" alt="Korn Ferry" /> </p>
    <h2>Unsecure login</h2>
    <p class="instructs">The authentication method you chose is invalid for this hostname.</p>
    <div class="tiny-text">
      
      
    </div>
  </div>
</div>

<!--
Unless this text is here, if your page is less than 513 bytes, Internet Explorer will display it's "Friendly HTTP Error Message",
and your custom error will never be displayed.  This text is just used as filler.
This is a useless buffer to fill the page to 513 bytes to avoid display of Friendly Error Pages in Internet Explorer
This is a useless buffer to fill the page to 513 bytes to avoid display of Friendly Error Pages in Internet Explorer
This is a useless buffer to fill the page to 513 bytes to avoid display of Friendly Error Pages in Internet Explorer
-->
</body>
</html>