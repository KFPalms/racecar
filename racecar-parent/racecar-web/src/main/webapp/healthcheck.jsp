<%@page import="com.pdi.properties.PropertyLoader"%>
<%@page import="com.pdi.listener.portal.flex.PortalFlexServiceV2"%>
<%@page import="com.pdi.data.abyd.util.AbyDDatabaseUtils"%>
<%@page import="com.pdi.data.nhn.helpers.NhnScoreDataHelper"%>
<%@page import="com.pdi.data.v2.helpers.UserDataHelper"%>

<%
String participantId = request.getParameter("participantId");
String projectId = request.getParameter("projectId");
String moduleId = request.getParameter("moduleId");
String value = "";

try 
{
	value = PropertyLoader.getProperty("application","global.environment");
	System.out.println(value);
} 
catch (Exception e) 
{
	System.out.println(e.getMessage());
      %>ERROR<% return;
}

try 
{
       value = PropertyLoader.getProperty("application","global.version");
	   System.out.println(value);
} 
catch (Exception e) 
{
	System.out.println(e.getMessage());
     %>ERROR<% return;
}

try 
{
      value = AbyDDatabaseUtils.testConnection() ? "SUCCESSFUL" : "ERROR";
} 
catch (Exception e) 
{
	System.out.println(e.getMessage());
       %>ERROR<% return;
}

%>
SUCCESSFUL
