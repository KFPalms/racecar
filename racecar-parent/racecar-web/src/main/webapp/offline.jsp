﻿<html>
	<head></head>
	<body>
<h1>
This site is offline for maintenance but will be available within 12 hours; please try again at that time.  We apologize for the inconvenience
<hr/><p/>
Aufgrund von Wartungsarbeiten steht diese Website derzeit nicht zur Verfügung. Der Online-Betrieb wird  voraussichtlich innerhalb von 12 Stunden wiederhergestellt. Bitte versuchen Sie es später nochmals. Wir bitten um Entschuldigung für die Unannehmlichkeiten.
<hr/><p/>
Este sitio está fuera de línea por mantenimiento, pero quedará disponible nuevamente dentro de 12 horas; sírvase intentar de nuevo  después de ese plazo. Lamentamos cualquier inconveniente que esto pueda producir.
<hr/><p/>
Ce site est hors ligne pour entretien, mais sera disponible dans un délai de 12 heures ; veuillez essayer de nouveau à ce moment-là. Nous vous prions de nous en excuser.
<hr/><p/>
本网站正在维护，暂时无法访问，但在 12 小时内可恢复正常；请届时再尝试访问。对此给您带来的不便我们深表歉意
<hr/><p/>
Este site está off-line para manutenção, mas estará disponível dentro de 12 horas; por favor, tente novamente mais tarde. Pedimos desculpas pelo inconveniente
<hr/><p/>
Questo sito è offline per la manutenzione ma sarà disponibile entro 12 ore; riprovare successivamente. Ci scusiamo per l'inconveniente
<hr/><p/>
本サイトはメンテナンスのためオフラインとなっています。12時間以内にサービスを再開しますので、後ほどもう一度お試しください。ご迷惑をおかけして大変申し訳ありません。
<hr/><p/>
Denna webbsida är nere för underhåll men kommer att vara tillgänglig inom 12 timmar, vänligen försök igen då. Vi ber om ursäkt för besväret.
<hr/><p/>
Deze site zal offline zijn voor onderhoud maar zal binnen 12 uur weer beschikbaar zijn; probeert u het a.u.b. tegen die tijd weer. Onze excuses voor het ongemak.
</h1>
	</body>
</html>