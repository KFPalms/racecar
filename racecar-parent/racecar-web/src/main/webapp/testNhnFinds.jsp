<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Test Participant-Project Find()</title>
</head>
<body>

<%@page import="com.pdi.data.dto.SessionUser"%>
<%@page import="com.pdi.data.helpers.interfaces.IProjectHelper"%>
<%@page import="com.pdi.data.helpers.interfaces.IParticipantHelper"%>
<%@page import="com.pdi.data.helpers.HelperDelegate"%>
<%@page import="com.pdi.properties.PropertyLoader"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.pdi.data.dto.Participant"%>
<%@page import="com.pdi.data.nhn.helpers.delegated.ProjectHelper"%>
<%@page import="com.pdi.data.nhn.helpers.delegated.ParticipantHelper"%>
<%@page import="com.pdi.data.dto.Project"%>
<%@page import="com.pdi.data.dto.Client"%>

<h3>ArrayList(Project) find(SessionUser session, String target) </h3>

				(data for dev database: MSPDBDEV4)  <br></br>
				target = "NHN" Should find 4 rows	<br></br>
				target = "Ken"  Should find no rows	<br></br>
				target = "with" Should find 1 row	<br></br>
				
<br></br><br></br>


<form method="get">
<input type="text" name="projSearch" size="25">
<!--
<input type="submit" value="Submit">
<input type="reset" value="Reset">
</form> 
  -->

<%

		SessionUser sessionUser = new SessionUser();
		IProjectHelper iproj = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated"); 
		ArrayList<Project> projects = new ArrayList<Project>();
		String enterText = "Please enter text into the input box to search for projects (Client = 751).";
		
	    if ( request.getParameter("projSearch") != null ) {
			projects = iproj.find(sessionUser, "751", request.getParameter("partSearch") );
	    }else{
%>	    	
			<br></br>	<br></br>
	    	<STRONG><%= enterText %></STRONG>	<br></br>	<br></br>
<%	    	
	    }
	    
	    if(projects.size() == 0){
	    	// no data - don't do anything
%>	    	
			There are NO project matches.
<%
	    }else{
	    	// build us a table with the data in it.
%>	    	
		<br></br><br></br>
		There are <%= projects.size() %> project matches.
		<br></br><br></br>
		<table border="1">
			<tr>
				<td>projId</td>
				<td>projName</td>
				<td>clientId</td>
				<td>projGUID</td>
			</tr>
<%
			for(Project p : projects) {		
%>						
			<tr>		
				<td><%=p.getId()%></td>
				<td><%=p.getName()%></td>
				<td><%=p.getClient().getId()%></td>
				<td><%=p.getMetadata().get("rowguid")%></td>
			</tr>
<%
			}
%>
		</table>
<% 
	    }
%>


<h3>ArrayList(Participant) find(SessionUser session, String target) </h3>

				(data for dev database: MSPDBDEV4)  <br></br>
				target = "Birgit";		// First name -- Should find 21 rows<br></br>
				target = "Brymesser";	// Last name -- Should find 1 row<br></br>
				target = "jkjolly";		// e-mail address -- Should find 1 row<br></br>
				target = "lmnop";		// Non-existent -- Should find no rows<br></br>
				target = "Kenneth";		// Higher volume - Should find 1480 rows<br></br>
<br></br><br></br>
				
<!-- <form method="get"> -->
<input type="text" name="partSearch" size="25">
<input type="submit" value="Submit">
<input type="reset" value="Reset">
</form>

<%

		IParticipantHelper ipart = HelperDelegate.getParticipantHelper("com.pdi.data.nhn.helpers.delegated"); 
		ArrayList<Participant> participants = new ArrayList<Participant>();
		String enterTextPart = "Please enter text into the input box to search for participants.";
		
	    if ( request.getParameter("partSearch") != null &&  !request.getParameter("partSearch").equals("") ) {
	    	participants = ipart.find(sessionUser, "751", request.getParameter("partSearch") );
	    }else{
%>	    	
			<br></br>	<br></br>
	    	<STRONG><%= enterTextPart %></STRONG>	<br></br>	<br></br>
<%	    	
	    }
	    
	    if(participants.size() == 0){
	    	// no data - don't do anything
%>	    	

			There are NO participant matches.
<%
	    }else{
	    	// build us a table with the data in it.
	    	/*
	    		Participant part = new Participant(rs.getString("users_id"));
				part.setLastName(rs.getString("lastname"));
				part.setFirstName(rs.getString("firstname"));
				part.setEmail(rs.getString("email"));
				part.setMetadata(new HashMap<String, String>());
				part.getMetadata().put("rowguid", rs.getString("rowguid"));
			*/
%>	    	
		<br></br><br></br>
		There are <%= participants.size() %> participant matches.
		<br></br><br></br>
		<table border="1">
			<tr>
				<td>partId</td>
				<td>partFname</td>
				<td>partLname</td>
				<td>partEmail</td>
				<td>partGUID</td>
			</tr>
<%
			for(Participant p : participants) {		
%>						
			<tr>		
				<td><%=p.getId()%></td>
				<td><%=p.getFirstName()%></td>
				<td><%=p.getLastName()%></td>
				<td><%=p.getEmail()%></td>
				<td><%=p.getMetadata().get("rowguid")%></td>
			</tr>
<%
			}
%>
		</table>
<%
	    }
%>

</body>
</html>