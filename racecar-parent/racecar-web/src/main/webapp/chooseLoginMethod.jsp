<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login</title>
<style type="text/css">
<!--
body {
	font: 100%/1.4 Arial, Helvetica, sans-serif;
	background-color: #C7C2BA;
	margin: 0;
	padding: 0;
	color: #000;
}

ul,ol,dl {
	padding: 0;
	margin: 0;
}

a img {
	border: none;
}

a:link {
	color: #42413C;
	text-decoration: underline;
}

a:visited {
	color: #6E6C64;
	text-decoration: underline;
}

a:hover,a:active,a:focus {
	text-decoration: none;
}

.container {
	
}

input {
	width: 250px;
	padding:4px;
}

.btn {
	width: 100px !important;
}

.content {
	background: none repeat scroll 0 0 #F9F9F9;
	border-radius: 4px 4px 4px 4px;
	border: #000 1px solid;
	display: block;
	margin: 15px auto;
	padding: 20px 15px 30px;
	width: 360px;
}

table {
	border-collapse: collapse;
	width: 100%;
	display: block;
	table-layout: fixed;
	border: 0;
	padding: 4px 4px 4px 0;
	margin: 0 auto;
}

table td {
	padding: 6px;
}

.instructs {
	font-size: 14px;
	padding-top: 5px;
}
-->
</style>
</head>
<script language="JavaScript">
	function setCookie(loginType){
		document.cookie="loginType=" + loginType +"; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/";
	}
</script>
<body>
	<p/>
	<br/>
	<br/>
	<div class="container">
		<div class="content">
				<p>
					<!-- the path needs the ../ to work on QA. is different on localhost -->
					<img src="images/kf_logo_black.png" alt="Korn Ferry Logo" />
				</p>
			<center>
			
				<a href="sso/audience" onclick="setCookie('kf')">Legacy Korn Ferry SSO</a><br></br>
				<a href="sso/hg/audience" onclick="setCookie('hg')">Legacy Hay Group SSO</a>
			
			</center>
			<!-- end .content -->
		</div>
		<!-- end .container -->
	</div>
</body>
</html>
