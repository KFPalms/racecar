<%@ page contentType="text/html;charset=windows-1252"%>
<%@ page import="org.apache.commons.fileupload.DiskFileUpload"%>
<%@ page import="org.apache.commons.fileupload.FileItem"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.io.File"%>
<%@ page import="com.pdi.properties.PropertyLoader"%>


<%
	System.out.println("Content Type ="+request.getContentType());
	System.out.println("Cookies" + request.getCookies());
	String method = request.getMethod();
	System.out.println("Method=" + method);
	
	String repositoryDirectory = "";

	try 
	{
		repositoryDirectory = PropertyLoader.getProperty(null,"repository.directory");
		System.out.println(repositoryDirectory);
	} 
	catch (Exception e) 
	{
		System.out.println(e.getMessage());
	      %>ERROR<% return;
	}
	if (method.equals("POST"))
	{
		String fileName = request.getParameter("guid");
		String fileExt = request.getParameter("fileExt");
		DiskFileUpload fu = new DiskFileUpload();
		// If file size exceeds, a FileUploadException will be thrown
		fu.setSizeMax(10000000);
	
		List fileItems = fu.parseRequest(request);
		Iterator itr = fileItems.iterator();
		while(itr.hasNext()) {
			FileItem fi = (FileItem)itr.next();
		
			//Check if not form field so as to only handle the file inputs
			//else condition handles the submit button input
			if(!fi.isFormField()) {
				System.out.println("\nNAME: "+fi.getName());
				System.out.println("SIZE: "+fi.getSize());
				System.out.println("CONTENT: "+fi.getFieldName());
				System.out.println("EXTENSION: " + fileExt);
//				if(fileExt.equalsIgnoreCase(".xls") || fileExt.equalsIgnoreCase(".jpeg") || fileExt.equalsIgnoreCase(".jpg") || fileExt.equalsIgnoreCase(".png") || fileExt.equalsIgnoreCase(".gif") || fileExt.equalsIgnoreCase(".doc") || fileExt.equalsIgnoreCase(".pdf") || fileExt.equalsIgnoreCase(".rtf")) {
				System.out.println("UPLOAD " + fileName + fileExt);	
				File fNew= new File(repositoryDirectory, fileName + fileExt );
				System.out.println("Absolute Path: " + fNew.getAbsolutePath());
				fi.write(fNew);
 //				}
			}
			else {
				System.out.println("Field ="+fi.getFieldName());
			}
		}	// End of FileItems loop
    }	// End of if POST
    else
    {
       	System.out.println("Upload skipped: method=" + method + ".");
       	response.setStatus(200);
	}
   	response.addHeader("Access-Control-Allow-Origin", "*");
    
 %>