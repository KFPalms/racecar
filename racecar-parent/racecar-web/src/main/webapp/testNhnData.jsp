<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.pdi.data.dto.SessionUser"%>
<%@page import="com.pdi.data.helpers.interfaces.IProjectHelper"%>
<%@page import="com.pdi.data.helpers.HelperDelegate"%>
<%@page import="com.pdi.properties.PropertyLoader"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.pdi.data.dto.Participant"%>
<%@page import="com.pdi.data.nhn.helpers.delegated.ProjectHelper"%>
<%@page import="com.pdi.data.dto.Project"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Testing project manipulation in NHN</title>
</head>
<body>

<h3>NHN:  get project from ID...</h3>

<%
		SessionUser sessionUser = new SessionUser();
		IProjectHelper iproj = HelperDelegate.getProjectHelper("com.pdi.data.nhn.helpers.delegated"); 
		String projectId = "1"; 
		// get the project
		Project project = iproj.fromId(sessionUser, projectId);

		try{
%>
		Project Id: <%= project.getId() %><br></br>
		Project Name: <%= project.getName() %>
		Client Name: <%= project.getClient().getName() %><br></br> 
		Project Meta Data: <%= project.getMetadata().toString() %><br></br>
		Project Content Collection: <%= project.getContentCollection().toString() %><br></br>
		Project Participant Collection: <%= project.getParticipantCollection().toString() %><br></br>
		
<%		}catch (Exception e) {
			System.out.println("projectId " + projectId + " did not return a valid projectId.");
		}
%>



<h3>NHN: Project Helper :  addParticipant(SessionUser session User, Project project, Long participantId)</h3>
<%
//      from the UT:
		String participantId = "52349";
		iproj.addParticipant(sessionUser, project, participantId);
%>
Participant <%=participantId %> added. <br></br><br></br>
<h3>NHN: Project Helper :  removeParticipant(SessionUser session User, Project project, Long participantId)</h3>
<%
		iproj.removeParticipant(sessionUser, project, participantId);
%>
Participant <%=participantId %> removed. <br></br><br></br>

<h3>NHN: Project Helper :  addParticipants(SessionUser session, Project project,ArrayList(Long) partIds)</h3>
<%
		
//		from the UT:
		ArrayList<String> partIds = new ArrayList<String>(); 
		partIds.add("545562");
		partIds.add("451034");

		iproj.addParticipants(sessionUser, project, partIds);

%>
Participants ( <%= partIds.size() %> ) added.<br></br><br></br>

<h3>NHN: Project Helper :  removeParticipants(SessionUser session, Project project,ArrayList(Participants) participants)</h3>
<%
		ArrayList<Participant> participants = new ArrayList<Participant>();
		for (String pId : partIds)
		{
			Participant p = new Participant();
			p.setId(pId.toString());
			participants.add(p);
		}
		
		iproj.removeParticipants(sessionUser, project, participants);
		

%>
	Participants ( <%= participants.size() %> ) removed.<br></br><br></br>


</body>
</html>