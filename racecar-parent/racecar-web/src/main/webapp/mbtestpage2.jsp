<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>


<%@page import="java.util.ArrayList"%>

<%@page import="javax.xml.xpath.XPath"%>
<%@page import="javax.xml.xpath.XPathConstants"%>
<%@page import="javax.xml.xpath.XPathFactory"%>

<%@page import="org.w3c.dom.Document"%>
<%@page import="org.w3c.dom.Node"%>
<%@page import="org.w3c.dom.NodeList"%>

<%@page import="com.pdi.data.v2.dto.ClientDTO"%>
<%@page import="com.pdi.data.v2.dto.ProjectDTO"%>
<%@page import="com.pdi.data.v2.dto.SessionUserDTO"%>
<%@page import="com.pdi.data.v2.util.V2WebserviceUtils"%>

<head></head>
<body>

test page

<% 

		String username = "cdunn";
		String password = "cdunn";
		V2WebserviceUtils v2web = new V2WebserviceUtils();
		SessionUserDTO sessionUser = v2web.registerUser(username, password);
		System.out.println("The user name is " + sessionUser);

		//String candidateId = request.getParameter("participantId").trim();
		String jobtype = "25";
		
/*
		
SELECT
c.firstname, 
c.lastname, 
CL.name as ClientName, 
j.title as ProjectName
from  candidates c, cand_jobs cj, jobs j, clients cl
where c.uniqueIdStamp = cj.candidateId
and cj.jobId = j.uniqueIdStamp
and j.clientId = cl.uniqueIdstamp
and j.jobtype = '25'
order by cl.uniqueIdStamp, j.uniqueIdStamp

c.FirstName, c.LastName, CL.Name as ClientName, j.Title as ProjectName from  candidates c, cand_jobs cj, jobs j, clients cl where c.uniqueIdStamp = cj.candidateId and cj.jobId = j.uniqueIdStamp and j.clientId = cl.uniqueIdstamp and j.jobtype =  ${jobtype} order by cl.uniqueIdStamp, j.uniqueIdStamp 

*/
		String xml = "";
		xml += "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
		xml += "		<ETR SessionId=\"${sessionId}\">";
		xml += "			<Rqt Cmd=\"OpenSQL\" AN=\"JobListSQL\" FA=\"Y\" FR=\"1\" RC=\"N\" SQ=\"SELECT c.FirstName, c.uniqueIdStamp as ParticipantID, c.LastName, CL.uniqueIdStamp as ClientID, CL.Name as ClientName, j.UniqueIdStamp as ProjectID, j.Title as ProjectName from  candidates c, cand_jobs cj, jobs j, clients cl where c.uniqueIdStamp = cj.candidateId and cj.jobId = j.uniqueIdStamp and j.clientId = cl.uniqueIdstamp and j.jobtype =  ${jobtype} order by cl.uniqueIdStamp, j.uniqueIdStamp, c.LastName \" />";		
		xml += "			<Rqt Cmd=\"Read\" AN=\"JobListSQL\" Sub=\"JobList\" Rpt=\"*\" RQ=\"N\">";	
		xml += "				<Fld FN=\"ParticipantID\" Fmt=\"A\" />";
		xml += "				<Fld FN=\"FirstName\" Fmt=\"A\" />";
		xml += "				<Fld FN=\"LastName\" Fmt=\"A\" />";
		xml += "				<Fld FN=\"ClientID\" Fmt=\"A\" />";
		xml += "				<Fld FN=\"ClientName\" Fmt=\"A\" />";
		xml += "				<Fld FN=\"ProjectID\" Fmt=\"A\" />";
		xml += "				<Fld FN=\"ProjectName\" Fmt=\"A\" />";
		xml += "			</Rqt>";
		xml += "			<Rqt Cmd=\"Close\" AN=\"JobListSQL\" />";
		xml += "		</ETR>";
		
		xml = xml.replace("${sessionId}", sessionUser.getSessionId());
		xml = xml.replace("${jobtype}", jobtype);

		//System.out.println("xml :\n" + xml);
		
		XPath xpath = XPathFactory.newInstance().newXPath(); 
		
		Document doc = v2web.sendData(xml);
		
		NodeList records = (NodeList) xpath.evaluate("//Record", doc, XPathConstants.NODESET); 
		
		for(int i = 0; i < records.getLength(); i++)
		{
			Node record = records.item(i);
			//System.out.println("record:  " + record.toString());
		%>
		
			<%= record.getTextContent() %>
		
		<%
			//projects.add(new ProjectDTO(record));
		}

		%>


</body>
</html>		
 	