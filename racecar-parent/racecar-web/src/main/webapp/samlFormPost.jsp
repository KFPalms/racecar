<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Saml SSO Form Post</title>
</head>
<script language="JavaScript">
	function submitForm(){
		document.getElementById("samlForm").submit();
	}
</script>
<body onload="submitForm();">
	<form id="samlForm" method="post" action="${action}"> 
		<input type="hidden" name="SAMLRequest" value="${authNRequest}" /> 
		
		<input type="hidden" value="Submit" /> 
	</form> 
</body>
</html>