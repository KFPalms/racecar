<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/2001/XSL/Transform">
  <xsl:output method="xml" encoding="utf-8" version="1.0" indent="yes" cdata-section-elements="learner_response"/>

 <xsl:element name="course_track">
 	<xsl:attribute name="root_id">ORG_LEI_1</xsl:attribute>
 </xsl:element>


  <xsl:template match="/">

    <cmiInteractionsObj>
      	<xsl:call-template name="set_1"/>
      	<xsl:call-template name="set_2"/>
      	<xsl:call-template name="set_3"/>
    </cmiInteractionsObj>

 </xsl:template>

  <xsl:template name="set_1">
  	<xsl:for-each select="//Answers/A[@N&lt;34]">
	    <xsl:element name="cmiInteractionObj">
	      <xsl:attribute name="id">Q<xsl:value-of select="@N"/></xsl:attribute>
	      <xsl:attribute name="type">other</xsl:attribute>
	      <xsl:attribute name="timestamp"></xsl:attribute>
	      <xsl:attribute name="weighting"></xsl:attribute>
	      <xsl:attribute name="result">neutral</xsl:attribute>
	      <xsl:attribute name="latency"></xsl:attribute>
	      <xsl:attribute name="description"></xsl:attribute>
	      <xsl:element name="cmiObjectives"/>
	      <xsl:element name="cmiCorrectResponsesObj">
	        <xsl:element name="cmiResponsePatternObj">
	          <xsl:attribute name="pattern"></xsl:attribute>
	        </xsl:element>
	      </xsl:element>
	      <xsl:element name="learner_response">
	        <xsl:value-of select="."/>
	      </xsl:element>
	    </xsl:element>
    </xsl:for-each>
  </xsl:template>
  
  <xsl:template name="set_2">
  	<xsl:for-each select="//Answers/A[@N&gt;33 and @N &lt; 81]">
		<xsl:choose>
		 <xsl:when test="@P">
		    <xsl:element name="cmiInteractionObj">
		      <xsl:attribute name="id">Q_<xsl:value-of select="@N"/>_<xsl:value-of select="@P"/></xsl:attribute>
		      <xsl:attribute name="type">other</xsl:attribute>
		      <xsl:attribute name="timestamp"></xsl:attribute>
		      <xsl:attribute name="weighting"></xsl:attribute>
		      <xsl:attribute name="result">neutral</xsl:attribute>
		      <xsl:attribute name="latency"></xsl:attribute>
		      <xsl:attribute name="description"></xsl:attribute>
		      <xsl:element name="cmiObjectives"/>
		      <xsl:element name="cmiCorrectResponsesObj">
		        <xsl:element name="cmiResponsePatternObj">
		          <xsl:attribute name="pattern"></xsl:attribute>
		        </xsl:element>
		      </xsl:element>
		      <xsl:element name="learner_response">
		        <xsl:value-of select="."/>
		      </xsl:element>
		    </xsl:element>
		 </xsl:when>
		 <xsl:otherwise>
		    <xsl:element name="cmiInteractionObj">
		      <xsl:attribute name="id">Q_<xsl:value-of select="@N"/>_1</xsl:attribute>
		      <xsl:attribute name="type">other</xsl:attribute>
		      <xsl:attribute name="timestamp"></xsl:attribute>
		      <xsl:attribute name="weighting"></xsl:attribute>
		      <xsl:attribute name="result">neutral</xsl:attribute>
		      <xsl:attribute name="latency"></xsl:attribute>
		      <xsl:attribute name="description"></xsl:attribute>
		      <xsl:element name="cmiObjectives"/>
		      <xsl:element name="cmiCorrectResponsesObj">
		        <xsl:element name="cmiResponsePatternObj">
		          <xsl:attribute name="pattern"></xsl:attribute>
		        </xsl:element>
		      </xsl:element>
		      <xsl:element name="learner_response">
		        <xsl:value-of select="."/>
		      </xsl:element>
		    </xsl:element>
		 </xsl:otherwise>
		</xsl:choose>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="set_3">
  	<xsl:for-each select="//Answers/A[@N&gt;80]">
	    <xsl:element name="cmiInteractionObj">
	      <xsl:attribute name="id">Q<xsl:value-of select="@N"/></xsl:attribute>
	      <xsl:attribute name="type">other</xsl:attribute>
	      <xsl:attribute name="timestamp"></xsl:attribute>
	      <xsl:attribute name="weighting"></xsl:attribute>
	      <xsl:attribute name="result">neutral</xsl:attribute>
	      <xsl:attribute name="latency"></xsl:attribute>
	      <xsl:attribute name="description"></xsl:attribute>
	      <xsl:element name="cmiObjectives"/>
	      <xsl:element name="cmiCorrectResponsesObj">
	        <xsl:element name="cmiResponsePatternObj">
	          <xsl:attribute name="pattern"></xsl:attribute>
	        </xsl:element>
	      </xsl:element>
	      <xsl:element name="learner_response">
	        <xsl:value-of select="."/>
	      </xsl:element>
	    </xsl:element>
    </xsl:for-each>
  </xsl:template>


</xsl:stylesheet>

