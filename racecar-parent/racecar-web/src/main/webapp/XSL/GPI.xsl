<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" encoding="utf-8" version="1.0" indent="yes" cdata-section-elements="learner_response"/>

  <xsl:template match="/">

    <cmiInteractionsObj>
      <xsl:apply-templates select="//Answers/A"/>
    </cmiInteractionsObj>

 </xsl:template>

  <xsl:template match="A">
    <xsl:element name="cmiInteractionObj">
      <xsl:attribute name="id">Q<xsl:value-of select="@N"/></xsl:attribute>
      <xsl:attribute name="type">other</xsl:attribute>
      <xsl:attribute name="timestamp"></xsl:attribute>
      <xsl:attribute name="weighting"></xsl:attribute>
      <xsl:attribute name="result">neutral</xsl:attribute>
      <xsl:attribute name="latency"></xsl:attribute>
      <xsl:attribute name="description"></xsl:attribute>
      <xsl:element name="cmiObjectives"/>
      <xsl:element name="cmiCorrectResponsesObj">
        <xsl:element name="cmiResponsePatternObj">
          <xsl:attribute name="pattern"></xsl:attribute>
        </xsl:element>
      </xsl:element>
      <xsl:element name="learner_response">
        <xsl:value-of select="."/>
      </xsl:element>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>

