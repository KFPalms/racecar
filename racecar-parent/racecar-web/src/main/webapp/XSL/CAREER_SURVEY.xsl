<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/2001/XSL/Transform">
  <xsl:output method="xml" encoding="utf-8" version="1.0" indent="yes" cdata-section-elements="learner_response"/>

 <xsl:element name="course_track">
 	<xsl:attribute name="root_id">ORG_LEI_1</xsl:attribute>
 </xsl:element>


  <xsl:template match="/">
    <cmiInteractionsObj>
      	<xsl:call-template name="set_1"/>
      	<xsl:call-template name="set_6a"/>
      	<xsl:call-template name="set_6b"/>
      	<xsl:call-template name="set_7a"/>
      	<xsl:call-template name="set_7b"/>
      	<xsl:call-template name="set_8"/>
      	<xsl:call-template name="set_H"/>
      	<xsl:call-template name="set_16a"/>
      	<xsl:call-template name="set_16b"/>
      	<xsl:call-template name="set_17a"/>
      	<xsl:call-template name="set_17b"/>
      	<xsl:call-template name="set_18"/>
    </cmiInteractionsObj>
 </xsl:template>

  <xsl:template name="set_1">
  	<xsl:for-each select="//Answers/A[@N&lt;6]">
	    <xsl:element name="cmiInteractionObj">
	      <xsl:attribute name="id">Q<xsl:value-of select="@N"/></xsl:attribute>
	      <xsl:attribute name="type">other</xsl:attribute>
	      <xsl:attribute name="timestamp"></xsl:attribute>
	      <xsl:attribute name="weighting"></xsl:attribute>
	      <xsl:attribute name="result">neutral</xsl:attribute>
	      <xsl:attribute name="latency"></xsl:attribute>
	      <xsl:attribute name="description"></xsl:attribute>
	      <xsl:element name="cmiObjectives"/>
	      <xsl:element name="cmiCorrectResponsesObj">
	        <xsl:element name="cmiResponsePatternObj">
	          <xsl:attribute name="pattern"></xsl:attribute>
	        </xsl:element>
	      </xsl:element>
	      <xsl:element name="learner_response">
	        <xsl:value-of select="."/>
	      </xsl:element>
	    </xsl:element>
    </xsl:for-each>
  </xsl:template>
  

  <xsl:template name="set_6a">
  	<xsl:for-each select="//Answers/A[@N&gt;5 and @N &lt; 15]">
		    <xsl:element name="cmiInteractionObj">
		      <xsl:attribute name="id">Q_6a_<xsl:number value="@N - 5"/></xsl:attribute>
		      <xsl:attribute name="type">other</xsl:attribute>
		      <xsl:attribute name="timestamp"></xsl:attribute>
		      <xsl:attribute name="weighting"></xsl:attribute>
		      <xsl:attribute name="result">neutral</xsl:attribute>
		      <xsl:attribute name="latency"></xsl:attribute>
		      <xsl:attribute name="description"></xsl:attribute>
		      <xsl:element name="cmiObjectives"/>
		      <xsl:element name="cmiCorrectResponsesObj">
		        <xsl:element name="cmiResponsePatternObj">
		          <xsl:attribute name="pattern"></xsl:attribute>
		        </xsl:element>
		      </xsl:element>
		      <xsl:element name="learner_response">
				<xsl:value-of select="."/> 
		    </xsl:element>
		    </xsl:element>
    </xsl:for-each>
  </xsl:template>
 
 <xsl:template name="set_6b">
  	<xsl:for-each select="//Answers/A[@N&gt;14 and @N &lt; 24]">
		    <xsl:element name="cmiInteractionObj">
		      <xsl:attribute name="id">Q_6b_<xsl:number value="@N - 14"/></xsl:attribute>
		      <xsl:attribute name="type">other</xsl:attribute>
		      <xsl:attribute name="timestamp"></xsl:attribute>
		      <xsl:attribute name="weighting"></xsl:attribute>
		      <xsl:attribute name="result">neutral</xsl:attribute>
		      <xsl:attribute name="latency"></xsl:attribute>
		      <xsl:attribute name="description"></xsl:attribute>
		      <xsl:element name="cmiObjectives"/>
		      <xsl:element name="cmiCorrectResponsesObj">
		        <xsl:element name="cmiResponsePatternObj">
		          <xsl:attribute name="pattern"></xsl:attribute>
		        </xsl:element>
		      </xsl:element>
		      <xsl:element name="learner_response">
		        <xsl:value-of select="."/>
		      </xsl:element>
		    </xsl:element>
    </xsl:for-each>
  </xsl:template>
  
   <xsl:template name="set_7a">
  	<xsl:for-each select="//Answers/A[@N&gt;23 and @N &lt; 33]">
		    <xsl:element name="cmiInteractionObj">
		      <xsl:attribute name="id">Q_7a_<xsl:number value="@N - 23"/></xsl:attribute>
		      <xsl:attribute name="type">other</xsl:attribute>
		      <xsl:attribute name="timestamp"></xsl:attribute>
		      <xsl:attribute name="weighting"></xsl:attribute>
		      <xsl:attribute name="result">neutral</xsl:attribute>
		      <xsl:attribute name="latency"></xsl:attribute>
		      <xsl:attribute name="description"></xsl:attribute>
		      <xsl:element name="cmiObjectives"/>
		      <xsl:element name="cmiCorrectResponsesObj">
		        <xsl:element name="cmiResponsePatternObj">
		          <xsl:attribute name="pattern"></xsl:attribute>
		        </xsl:element>
		      </xsl:element>
		      <xsl:element name="learner_response">
				<xsl:value-of select="."/> 
		      </xsl:element>
		    </xsl:element>
    </xsl:for-each>
  </xsl:template>
 
    <xsl:template name="set_7b">
  	<xsl:for-each select="//Answers/A[@N&gt;32 and @N &lt; 42]">
		    <xsl:element name="cmiInteractionObj">
		      <xsl:attribute name="id">Q_7b_<xsl:number value="@N - 32"/></xsl:attribute>
		      <xsl:attribute name="type">other</xsl:attribute>
		      <xsl:attribute name="timestamp"></xsl:attribute>
		      <xsl:attribute name="weighting"></xsl:attribute>
		      <xsl:attribute name="result">neutral</xsl:attribute>
		      <xsl:attribute name="latency"></xsl:attribute>
		      <xsl:attribute name="description"></xsl:attribute>
		      <xsl:element name="cmiObjectives"/>
		      <xsl:element name="cmiCorrectResponsesObj">
		        <xsl:element name="cmiResponsePatternObj">
		          <xsl:attribute name="pattern"></xsl:attribute>
		        </xsl:element>
		      </xsl:element>
		      <xsl:element name="learner_response">
		        <xsl:value-of select="."/>
		      </xsl:element>
		    </xsl:element>
    </xsl:for-each>
  </xsl:template>
   
    <xsl:template name="set_8">
  	<xsl:for-each select="//Answers/A[@N&gt;41 and @N &lt; 47]">
		    <xsl:element name="cmiInteractionObj">
		      <xsl:attribute name="id">Q_8_<xsl:number value="@N - 41"/></xsl:attribute>
		      <xsl:attribute name="type">other</xsl:attribute>
		      <xsl:attribute name="timestamp"></xsl:attribute>
		      <xsl:attribute name="weighting"></xsl:attribute>
		      <xsl:attribute name="result">neutral</xsl:attribute>
		      <xsl:attribute name="latency"></xsl:attribute>
		      <xsl:attribute name="description"></xsl:attribute>
		      <xsl:element name="cmiObjectives"/>
		      <xsl:element name="cmiCorrectResponsesObj">
		        <xsl:element name="cmiResponsePatternObj">
		          <xsl:attribute name="pattern"></xsl:attribute>
		        </xsl:element>
		      </xsl:element>
		      <xsl:element name="learner_response">
		        <xsl:value-of select="."/>
		      </xsl:element>
		    </xsl:element>
    </xsl:for-each>
  </xsl:template>
   
    <xsl:template name="set_H">
  	<xsl:for-each select="//Answers/A[@N&gt;46 and @N &lt; 54]">
		    <xsl:element name="cmiInteractionObj">
		      <xsl:attribute name="id">H_<xsl:number value="@N - 38"/></xsl:attribute>
		      <xsl:attribute name="type">other</xsl:attribute>
		      <xsl:attribute name="timestamp"></xsl:attribute>
		      <xsl:attribute name="weighting"></xsl:attribute>
		      <xsl:attribute name="result">neutral</xsl:attribute>
		      <xsl:attribute name="latency"></xsl:attribute>
		      <xsl:attribute name="description"></xsl:attribute>
		      <xsl:element name="cmiObjectives"/>
		      <xsl:element name="cmiCorrectResponsesObj">
		        <xsl:element name="cmiResponsePatternObj">
		          <xsl:attribute name="pattern"></xsl:attribute>
		        </xsl:element>
		      </xsl:element>
		      <xsl:element name="learner_response">
		        <xsl:value-of select="."/>
		      </xsl:element>
		    </xsl:element>
    </xsl:for-each>
  </xsl:template>
  
      <xsl:template name="set_16a">
  	<xsl:for-each select="//Answers/A[@N&gt;53 and @N &lt; 61]">
		    <xsl:element name="cmiInteractionObj">
		      <xsl:attribute name="id">Q_16a_<xsl:number value="@N - 53"/></xsl:attribute>
		      <xsl:attribute name="type">other</xsl:attribute>
		      <xsl:attribute name="timestamp"></xsl:attribute>
		      <xsl:attribute name="weighting"></xsl:attribute>
		      <xsl:attribute name="result">neutral</xsl:attribute>
		      <xsl:attribute name="latency"></xsl:attribute>
		      <xsl:attribute name="description"></xsl:attribute>
		      <xsl:element name="cmiObjectives"/>
		      <xsl:element name="cmiCorrectResponsesObj">
		        <xsl:element name="cmiResponsePatternObj">
		          <xsl:attribute name="pattern"></xsl:attribute>
		        </xsl:element>
		      </xsl:element>
		      <xsl:element name="learner_response">
					<xsl:value-of select="."/>
		      </xsl:element>
		    </xsl:element>
    </xsl:for-each>
  </xsl:template>
  
   <xsl:template name="set_16b">
  	<xsl:for-each select="//Answers/A[@N&gt;60 and @N &lt; 68]">
		    <xsl:element name="cmiInteractionObj">
		      <xsl:attribute name="id">Q_16b_<xsl:number value="@N - 60"/></xsl:attribute>
		      <xsl:attribute name="type">other</xsl:attribute>
		      <xsl:attribute name="timestamp"></xsl:attribute>
		      <xsl:attribute name="weighting"></xsl:attribute>
		      <xsl:attribute name="result">neutral</xsl:attribute>
		      <xsl:attribute name="latency"></xsl:attribute>
		      <xsl:attribute name="description"></xsl:attribute>
		      <xsl:element name="cmiObjectives"/>
		      <xsl:element name="cmiCorrectResponsesObj">
		        <xsl:element name="cmiResponsePatternObj">
		          <xsl:attribute name="pattern"></xsl:attribute>
		        </xsl:element>
		      </xsl:element>
		      <xsl:element name="learner_response">
		        <xsl:value-of select="."/>
		      </xsl:element>
		    </xsl:element>
    </xsl:for-each>
  </xsl:template>
  
   <xsl:template name="set_17a">
  	<xsl:for-each select="//Answers/A[@N&gt;67 and @N &lt; 75]">
		    <xsl:element name="cmiInteractionObj">
		      <xsl:attribute name="id">Q_17a_<xsl:number value="@N - 67"/></xsl:attribute>
		      <xsl:attribute name="type">other</xsl:attribute>
		      <xsl:attribute name="timestamp"></xsl:attribute>
		      <xsl:attribute name="weighting"></xsl:attribute>
		      <xsl:attribute name="result">neutral</xsl:attribute>
		      <xsl:attribute name="latency"></xsl:attribute>
		      <xsl:attribute name="description"></xsl:attribute>
		      <xsl:element name="cmiObjectives"/>
		      <xsl:element name="cmiCorrectResponsesObj">
		        <xsl:element name="cmiResponsePatternObj">
		          <xsl:attribute name="pattern"></xsl:attribute>
		        </xsl:element>
		      </xsl:element>
		      <xsl:element name="learner_response">
					<xsl:value-of select="."/>  
		      </xsl:element>
		    </xsl:element>
    </xsl:for-each>
  </xsl:template>
  
     <xsl:template name="set_17b">
  	<xsl:for-each select="//Answers/A[@N&gt;74 and @N &lt; 82]">
		    <xsl:element name="cmiInteractionObj">
		      <xsl:attribute name="id">Q_17b_<xsl:number value="@N - 74"/></xsl:attribute>
		      <xsl:attribute name="type">other</xsl:attribute>
		      <xsl:attribute name="timestamp"></xsl:attribute>
		      <xsl:attribute name="weighting"></xsl:attribute>
		      <xsl:attribute name="result">neutral</xsl:attribute>
		      <xsl:attribute name="latency"></xsl:attribute>
		      <xsl:attribute name="description"></xsl:attribute>
		      <xsl:element name="cmiObjectives"/>
		      <xsl:element name="cmiCorrectResponsesObj">
		        <xsl:element name="cmiResponsePatternObj">
		          <xsl:attribute name="pattern"></xsl:attribute>
		        </xsl:element>
		      </xsl:element>
		      <xsl:element name="learner_response">
		        <xsl:value-of select="."/>
		      </xsl:element>
		    </xsl:element>
    </xsl:for-each>
  </xsl:template>
  
    <xsl:template name="set_18">
  	<xsl:for-each select="//Answers/A[@N&gt;81]">
	    <xsl:element name="cmiInteractionObj">
	      <xsl:attribute name="id">Q<xsl:number value="@N - 64"/></xsl:attribute>
	      <xsl:attribute name="type">other</xsl:attribute>
	      <xsl:attribute name="timestamp"></xsl:attribute>
	      <xsl:attribute name="weighting"></xsl:attribute>
	      <xsl:attribute name="result">neutral</xsl:attribute>
	      <xsl:attribute name="latency"></xsl:attribute>
	      <xsl:attribute name="description"></xsl:attribute>
	      <xsl:element name="cmiObjectives"/>
	      <xsl:element name="cmiCorrectResponsesObj">
	        <xsl:element name="cmiResponsePatternObj">
	          <xsl:attribute name="pattern"></xsl:attribute>
	        </xsl:element>
	      </xsl:element>
	      <xsl:element name="learner_response">
	        <xsl:value-of select="."/>
	      </xsl:element>
	    </xsl:element>
    </xsl:for-each>
  </xsl:template>
  
</xsl:stylesheet>

