<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@page import="java.util.Enumeration"%>
<%@page import="java.security.Principal"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tester</title>
</head>
<body>
	<h2>Tester</h2>
	<br></br>
	<%
	String uName = request.getRemoteUser();
	%>
	<p>UserName:  <%=uName%></p>
	<p>Session attribute names:<br></br></p>
	<p>
	<%
	Enumeration atts = session.getAttributeNames();
	int i = 0;
	while ( atts.hasMoreElements() )
	{
		i++;
		%>
		<%=atts.nextElement()%><br></br>
		<%
	}
	if(i == 0)
	{
		%>
		No session attributes detected<br></br>
		<%
	}
	%>
	</p>
	<p>
	Principal info:<br></br>
	<%
	Principal pp = request.getUserPrincipal();
	if (pp == null)
	{
		%>
		Principal is null<br>
		<%
 	}
 	else
 	{
		%>
		Principal is NOT null<br>
 		Content: <%=pp.toString()%>
 		<%
	}
%>
	</p>

</body>
</html>