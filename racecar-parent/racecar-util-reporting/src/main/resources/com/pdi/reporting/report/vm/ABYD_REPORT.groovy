/*
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.reporting.report.vm

import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.IndividualReport
import com.pdi.reporting.report.utils.ReportingUtils;

/**
 * CHQ - This groovy script contains code to generate the report data required for use
 * by the UI when it is generating the CHQ report.
 * NOTE:  There no no scoring associated with the CHQ at this time.
 * NOTE:  There is no generation code present as the report has not yet been defined.
 * 
 * @author kbeukelm
 */
public class ABYD_REPORT
{
	//
	// Static data.
	//
	private IndividualReport individualReport;



	//	
	// Static methods.
	//
	
	/*
	 * score - Rolls answer data up in proper manner to create raw scale scores.
	 * 
	 * NOTE:  This method probably won't be used by the CHQ, but it is here as a place-holder
	 * 
	 * @returns An IndividualReport object
	 */
	public IndividualReport score()
	{
		//print out all the data
		println "'score' code not implemented at this time!!!" 
		
		//finally return an empty IndividualReport object for now
		return new IndividualReport();
		
	}	// End score()
	
	
	//*************************************************************************************//
	
	/*
	 * compares two org/part text fields to set the correct value
	 */
	private void compare(String primary, String secondary, String finalValue)
	{
		if(individualReport.getDisplayData().get(primary) != null && individualReport.getDisplayData().get(primary).length() > 0) {
			individualReport.getDisplayData().put(finalValue, individualReport.getDisplayData().get(primary));
			
		} else if ( (secondary.length() > 1) && (individualReport.getDisplayData().get(secondary) != null && individualReport.getDisplayData().get(secondary).length() > 0)) {
			individualReport.getDisplayData().put(finalValue, individualReport.getDisplayData().get(secondary));
			
		} else {
			individualReport.getDisplayData().put(finalValue, "N/A");
		}
	}
	
	
	/*
	 * generate - Formats data for use by the UI to generate the report PDF
	 * 
	 * NOTE:  Not yet implemented
	 * 
	 * @returns An IndividualReport object
	 */
		public IndividualReport generate()
	{
		//Format the values
		individualReport = ReportingUtils.xmlEscapeString(individualReport);
		
		//set the text headers
		if(individualReport.getReportCode().equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_FIT_REPORT) || 
		individualReport.getReportCode().equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_READINESS_REPORT) ||
		individualReport.getReportCode().equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_CUSTOM_REPORT)) {
			compare("DERAIL_ORG_TXT", "DERAIL_PRT_TXT", "DERAIL_TXT");
			compare("DEV_PRT_TXT", "", "DEV_TXT");
			compare("FIT_ORG_TXT", "", "FIT_TXT");
			compare("RED_ORG_TXT", "", "RED_TXT");
			
			compare("LEADER_EXP_ORG_TXT", "LEADER_EXP_PRT_TXT", "LEADER_EXP_TXT");
			compare("LEADER_INT_ORG_TXT", "LEADER_INT_PRT_TXT", "LEADER_INT_TXT");
			compare("LEADER_SKL_ORG_TXT", "LEADER_SKL_PRT_TXT", "LEADER_SKL_TXT");
			compare("LEADER_STY_ORG_TXT", "LEADER_STY_PRT_TXT", "LEADER_STY_TXT");
			compare("SKL_LEV_ORG_TXT", "SKL_LEV_PRT_TXT", "SKL_LEV_TXT");
			compare("SKL_DEV_ORG_TXT", "SKL_DEV_PRT_TXT", "SKL_DEV_TXT");
			//compare("LEADER_STY_PRT_TXT", "", "LEADER_STY_TXT");
			compare("PIV_PRT_TXT", "", "PIV_TXT");
			compare("LONGTERM_ORG_TXT", "", "LONGTERM_TXT");
		} else {
			compare("DERAIL_PRT_TXT", "DERAIL_ORG_TXT", "DERAIL_TXT");
			compare("DEV_PRT_TXT", "", "DEV_TXT");
			compare("FIT_ORG_TXT", "", "FIT_TXT");
			compare("PIV_PRT_TXT", "", "PIV_TXT");
			
			compare("LEADER_EXP_PRT_TXT", "LEADER_EXP_ORG_TXT", "LEADER_EXP_TXT");
			compare("LEADER_INT_PRT_TXT", "LEADER_INT_ORG_TXT", "LEADER_INT_TXT");
			compare("LEADER_SKL_PRT_TXT", "LEADER_SKL_ORG_TXT", "LEADER_SKL_TXT");
			compare("LEADER_STY_PRT_TXT", "LEADER_STY_ORG_TXT", "LEADER_STY_TXT");
			compare("SKL_LEV_PRT_TXT", "SKL_LEV_ORG_TXT", "SKL_LEV_TXT");
			compare("SKL_DEV_PRT_TXT", "SKL_DEV_ORG_TXT", "SKL_DEV_TXT");
		//	compare("LEADER_STY_PRT_TXT", "", "LEADER_STY_TXT");
			compare("PIV_PRT_TXT", "", "PIV_TXT");
			compare("LONGTERM_ORG_TXT", "", "LONGTERM_TXT");
		}
			

		// Return an empty IndividualReport object for now
		return individualReport;
	}	// End generate()
}
