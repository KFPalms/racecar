/*
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.reporting.report.vm 

import com.pdi.scoring.Norm;

import java.util.ArrayList;
import com.pdi.reporting.report.helpers.AbyDExtractFormatHelper;

import com.pdi.reporting.report.ExtractItem;
import com.pdi.reporting.report.GroupReport;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.constants.ReportingConstants;


public class TLT_EXTRACT
{ 
	private GroupReport groupReport;
	
	private double transitionLevelMean;
	private double transitionLevelStdDev;
	private double gpiGenPopMean;
	private double gpiGenPopStdDev;
	private double gpiSpecPopMean;
	private double gpiSpecPopStdDev;
	
	/*
	 * parse
	 */
	private Double parse(String value)
	{
		Double d;
		try
		{
			d = Double.parseDouble(value);
		}
		catch(Exception e)
		{
			d = 0;
		}
		return d;
	}

	/*
	 * generate
	 */
	public GroupReport generate()
	{
		
		//System.out.println ("--->TLT_EXTRACT.groovy:  this is what tlt group detail report does<---");
		TLT_GROUP_DETAIL_REPORT_SCORING tltScoring = new TLT_GROUP_DETAIL_REPORT_SCORING();
		tltScoring.groupReport = groupReport;
		groupReport = tltScoring.generate(); 
		//call once for each participant.... 
		groupReport.getIndividualReports().each { individualReport ->
			generateRank(individualReport);
		}	
		
		groupReport.getIndividualReports().sort();
		
		return groupReport;
	}
	
	/*
	 * generateRank
	 */
	public void generateRank(IndividualReport individualReport)
	{
		//println "start rank..."
		//This is a fairly complicated ranking algorithm... 
		double leadershipExperience = individualReport.getScriptedGroupData().get("LE_EXPER_FINALSCORE");
		double leadershipInterest = individualReport.getScriptedGroupData().get("LE_INTER_FINALSCORE");
		double leadershipFoundations = individualReport.getScriptedGroupData().get("LE_FOUND_FINALSCORE");
		
		double derailment = 0;
		if(individualReport.getScriptedData().get(ReportingConstants.DR_DERAIL) != null)
		{
			derailment = 10 - Double.parseDouble(individualReport.getScriptedData().get(ReportingConstants.DR_DERAIL));
		}
		//println	"LE_EXP=" + leadershipExperience +
		//			", LE_INT=" + leadershipInterest +
		//			", LE_FND=" + leadershipFoundations +
		//			", DR=" + derailment
		
		double rank = 0;
		if(leadershipExperience > 0 && leadershipInterest > 0 && leadershipFoundations > 0) {
			rank = 800;
		} else if(leadershipExperience < 1 && leadershipInterest > 0 && leadershipFoundations > 0) {
			rank = 700;
		} else if(leadershipExperience > 0 && leadershipInterest < 1 && leadershipFoundations > 0) {
			rank = 600;
		} else if(leadershipExperience > 0 && leadershipInterest > 0 && leadershipFoundations < 1) {
			rank = 500;
		} else if(leadershipExperience < 1 && leadershipInterest > 0 && leadershipFoundations < 1) {
			rank = 400;
		} else if(leadershipExperience < 1 && leadershipInterest < 1 && leadershipFoundations > 0) {
			rank = 300;
		} else if(leadershipExperience > 0 && leadershipInterest < 1 && leadershipFoundations < 1) {
			rank = 200;
		} else {
			rank = 100;
		}
		rank = rank + derailment;
		//println   "rank=" + rank
		
		//The last thing we need to do is sort by last name... this will work FOR NOW
		//TODO: We should parse this array and do this the right way
		String finalSort = "0.";
		String order = "zyxwvutsrqponmlkjihgfedcba'!#*&()'";
		
		String lastName = individualReport.displayData.get("LAST_NAME");
		int counter = 0;
		if(lastName != null) {
			for(int i = 0; i < lastName.length(); i++)
			{
				if(counter == 4)
				{
					break;
				}
				counter++;
				String c = lastName[i];
				int n = order.indexOf(c.toLowerCase()) + 10;
				//System.out.println(c + " = " + n + " vs " + order.indexOf(c));
				finalSort += n;
				
			}
		}
		//println   "LN done..."
		
		finalSort += "0";
		
		String firstName = individualReport.displayData.get("FIRST_NAME");
		if(firstName != null) {
			for(int i = 0; i < firstName.length(); i++)
			{
				if(counter == 4)
				{
					break;
				}
				String c = firstName[i];
				int n = order.indexOf(c) + 10;
				finalSort += n;
			}
		}
		//println   "FN done..."
		
		double finalSortCalc = Double.parseDouble(finalSort);
		//System.out.println(lastName + ", " + firstName);
		//System.out.println(rank + " + " + finalSortCalc);
		rank = rank + finalSortCalc;
		//System.out.println(rank + " * 10000");
		rank = rank * 10000;
		//System.out.println(rank);
		int rankFinal = ((Double)rank).intValue();
		//System.out.println(rankFinal);
		//println   "ranking done..."
		
		individualReport.setRank(rankFinal);
	}
}