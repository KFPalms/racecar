/*
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.reporting.report.vm

import com.pdi.reporting.report.vm.GPI_SCORING;	// Import present for maintainability

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.GroupReport
import com.pdi.reporting.report.IndividualReport
import com.pdi.reporting.report.ReportData;
import com.pdi.reporting.report.utils.BWUtils;
import com.pdi.scoring.Norm;


/**
 * GPI -- This groovy script contains code to create the raw scale scores
 * from response data ("score()")and the code required to generate the
 * report data used by the UI when creating a report ("generate()").
 * 
 * @author gmyers
 */
public class GPI
{
	//
	// Static data.
	//
	private IndividualReport individualReport;


	// ------------- Data used in scoring -------------
	



	//	
	// Static methods.
	//

	/*
	 * score - Rolls answer data up in proper manner to create raw scale scores.
	 * 
	 * @returns An IndividualReport object
	 */
	public IndividualReport score()
	{
		//get question responses
		int[] answerAry = getAnswers(individualReport.getReportData().get(ReportingConstants.RC_GPI).getRawData());
		
		// Score the individual items
		double[] itemScores = calcItemScores(answerAry);
		//println "Item Scores:"
		//for(int i=1; i < itemScores.length; i++)
		//{
		//	println "  Idx=" + i + ":  A=" + answerAry[i] + ", S=" + itemScores[i];
		//}
						
		// Calc the scale scores
		HashMap<String, Double> scaleScores = calcScaleScores(itemScores);
		//println "Scale Scores";
		//for (Iterator itr=scaleScores.entrySet().iterator(); itr.hasNext(); )
		//{
		//	Map.Entry<String, Integer> ent = itr.next();
		//	println "  " + ent.getKey() + "=" + ent.getValue();
		//}
		
		// Set the scale scores in the output
		HashMap<String, String> scales = new HashMap<String, String>();
		for (Iterator itr=scaleScores.entrySet().iterator(); itr.hasNext(); )
		{
			Map.Entry<String, Double> ent = itr.next();
			scales.put(ent.getKey(), ent.getValue().toString());
		}
		
		////String rdi = GPI_RDI.calcRdi(answerAry);
		String rdi = GPI_SCORING.calcRdi(itemScores);
		////println "RDI=" + rdi;
		
		// Add the RDI to the output scores
		scales.put("PGPI_RDI", rdi);
		individualReport.getReportData().get(ReportingConstants.RC_GPI).setScoreData(scales);
		
		//finally return the individual report
		return individualReport;
	}


	/*
	 * Gets the answers from a HashMap (called with the
	 * raw data array from ReportData).
	 * ASSUMES:
	 * 		Answers are labeled Q1 - Q300 (this is the GPI after all)
	 * 		Answers are ints
	 * 		Output array will have 301 entries (0 - 300) - 0 is wasted.
	 * 		Missing entries will be present as zeros.
	 */
	private static int[] getAnswers(HashMap<String, String> rDat)
	{
		int[] ret = new int[301];
		
		for(int i=1; i <= 300; i++)
		{
			String ans = rDat.get("Q" + (i));
			ret[i] = (ans == null || ans.isEmpty()) ? 0 : Integer.parseInt(ans);
		}
		
		return ret;
	}
	
	
	/*
	 * Convert the answers to scores
	 * Note that the index runs by question number; there are 301 items
	 * in the array (0 - 300) and the element with index 0 is ignored
	 */
	private static double[] calcItemScores(int[] ans)
	{
		double[] ret = new double[301];
		
		for(int i=1; i <=300; i++)
		{
			double scor;
			if (ans[i] == 0  || GPI_SCORING.GPI_SCORING_KEY[i][0] == null  || GPI_SCORING.GPI_SCORING_KEY[i][1] == null)
			{
				// Invalid/empty response or scoring key data missing
				ret[i] = 0.0;
				continue;
			}
			
			// Normal scoring
			if (GPI_SCORING.GPI_SCORING_KEY[i][1])
			{
								// isAscending
				switch (ans[i])
				{
					case 1:
						ret[i] = 0.0;
						break;
					case 2:
						ret[i] = 0.25;
						break;
					case 3:
						ret[i] = 0.5;
						break;
					case 4:
						ret[i] = 0.75;
						break;
					case 5:
						ret[i] = 1.0;
						break;
					default:
						ret[i] = 0.0;
						break;
				}
			}
			else
			{
								// Score attribution is descending
				switch (ans[i])
				{
					case 1:
						ret[i] = 1.0;
						break;
					case 2:
						ret[i] = 0.75;
						break;
					case 3:
						ret[i] = 0.5;
						break;
					case 4:
						ret[i] = 0.25;
						break;
					case 5:
						ret[i] = 0.0;
						break;
					default:
						ret[i] = 0.0;
						break;
				}				
			}
		}
		
		return ret;
	}
	
	
	/*
	 * Calc the scale scores
	 * Note that the index runs by question number; there are 301 items
	 * in the array (0 - 300) and the element with index 0 is ignored
	 */
	private static HashMap calcScaleScores(double[] iScores)
	{
		HashMap<String, Double> hold = new HashMap<String, Double>();
		HashMap<String, String> ret = new HashMap<String, String>();
						
		for(int i=1; i <=300; i++)
		{
			Double curScor = hold.get(GPI_SCORING.GPI_SCORING_KEY[i][0]);
			if (curScor == null)
			{
				hold.put(GPI_SCORING.GPI_SCORING_KEY[i][0], new Double(iScores[i]));
			}
			else
			{
				hold.put(GPI_SCORING.GPI_SCORING_KEY[i][0], new Double(curScor.doubleValue() + iScores[i]));
			}
		}

		// Done - set up the output as Strings
		for (Iterator itr=hold.entrySet().iterator(); itr.hasNext(); )
		{
			Map.Entry<String, Double> ent = itr.next();
			ret.put(ent.getKey(),(ent.getValue().toString()));
		}
		
		return ret;
	}	// End score()
	
	
	//*************************************************************************************//
	
	
	/*
	 * generate - Formats data for use by the UI to generate the report PDF
	 * 
	 * @returns An IndividualReport object
	 */
	public IndividualReport generate()
	{
/*		
		// Make the display name
		String disp = individualReport.getDisplayData().get("FIRST_NAME") + " " + individualReport.getDisplayData().get("LAST_NAME")
		individualReport.getDisplayData().put("DISPLAY_NAME", disp);
		
	
		
		//Get the ReportData object for the GPI report 
		ReportData rd = individualReport.getReportData().get(ReportingConstants.RC_GPI);

		//Gpi not complete
		if(rd == null) {
			//System.out.println("GPI IS NULL!");
			// They haven't finished the GPI
			individualReport.getScriptedData().put("STATUS", "NOT_FINISHED");
			individualReport.getScriptedData().put("ERROR", "GPI has not been completed, unable to generate report");
			return ;
		} else {
			individualReport.getScriptedData().put("STATUS", "");
			individualReport.getScriptedData().put("ERROR", "");
			//System.out.println("GPI IS NOT NULL!");
		}

		// Calc the reference line positions and put them out
		float[] ref = BWUtils.getRefPosArray();
		rd.getScriptedData().put("REF_1", ref[0].toString());
		rd.getScriptedData().put("REF_3", ref[1].toString());
		rd.getScriptedData().put("REF_16", ref[2].toString());
		rd.getScriptedData().put("REF_50", ref[3].toString());
		rd.getScriptedData().put("REF_84", ref[4].toString());
		rd.getScriptedData().put("REF_97", ref[5].toString());
		rd.getScriptedData().put("REF_99", ref[6].toString());

		// Set up the dimension data
		rd.getDisplayData().put("DIM_COUNT", GPI_SCORING.DIMENSIONS.length.toString());	
		ArrayList dimData = new ArrayList();
		for (int i=0; i < GPI_SCORING.DIMENSIONS.length; i++)
		{
			rd.getDisplayData().put("DIM" + (i+1) + "_NAME", GPI_SCORING.DIMENSIONS[i][0]);
			ArrayList ary = GPI_SCORING.DIMENSIONS[i][1];
			rd.getDisplayData().put("DIM" + (i+1) + "_SCALE_COUNT", ary.size().toString());	

			// Do the scale data
			for (int j=0; j < ary.size(); j++)
			{
				String scaleId = GPI_SCORING.DIMENSIONS[i][1].get(j);
				if(rd.getScoreData(scaleId + "_GP_PCTL") != null) {
					rd.getDisplayData().put("DIM" + (i+1) + "_SCALE" + (j+1) + "_ID", scaleId);
					
					// Round the percentiles
					double pctl = Double.parseDouble(rd.getScoreData(scaleId + "_GP_PCTL"));
					int pOut = Math.floor(pctl + 0.5)
					if (pOut < 1.0)
						pOut = 1.0;
					else if (pOut > 99.0)
						pOut = 99.0;
					rd.getScriptedData().put(scaleId+"_GP_PCTL", pOut.toString());
					pctl = Double.parseDouble(rd.getScoreData(scaleId + "_SP_PCTL"));
					pOut = Math.floor(pctl + 0.5)
					if (pOut < 1.0)
						pOut = 1.0;
					else if (pOut > 99.0)
						pOut = 99.0;
					rd.getScriptedData().put(scaleId + "_SP_PCTL", pOut.toString());
	
					// Get the norms
					double mean = Double.parseDouble(rd.getScoreData(scaleId + "_GP_SCALE_MEAN"));
					double stDev = Double.parseDouble(rd.getScoreData(scaleId + "_GP_SCALE_STDEV"));
					Norm gpNorm = new Norm(mean,stDev);
					mean = stDev = 0.0;
					mean = Double.parseDouble(rd.getScoreData(scaleId + "_SP_SCALE_MEAN"));
					stDev = Double.parseDouble(rd.getScoreData(scaleId + "_SP_SCALE_STDEV"));
					Norm spNorm = new Norm(mean,stDev);
	
					// Calc the score position
					double score = Double.parseDouble(rd.getScoreData().get(scaleId));
					float scorPos = BWUtils.calcScorePos(score, gpNorm);
					rd.getScriptedData().put(scaleId + "_POSN", scorPos.toString());
									
					// Calc the cut-point positions
					float[] csPos = BWUtils.getCutPosArray(gpNorm, spNorm);
					//System.out.println("Cut Points for " + scaleId + ":");
					//for(int r= 0; r < csPos.length; r++)
					//{
					//	System.out.println("Idx[" + r + "]=" + csPos[r])
					//}
					rd.getScriptedData().put(scaleId + "_CP_3", csPos[0].toString());
					rd.getScriptedData().put(scaleId + "_CP_11", csPos[1].toString());
					rd.getScriptedData().put(scaleId + "_CP_23", csPos[2].toString());
					rd.getScriptedData().put(scaleId + "_CP_40", csPos[3].toString());
					rd.getScriptedData().put(scaleId + "_CP_60", csPos[4].toString());
					rd.getScriptedData().put(scaleId + "_CP_77", csPos[5].toString());
					rd.getScriptedData().put(scaleId + "_CP_89", csPos[6].toString());
					rd.getScriptedData().put(scaleId + "_CP_97", csPos[7].toString());
				} else {
					individualReport.getScriptedData().put("STATUS", "NOT_FINISHED");
					individualReport.getScriptedData().put("ERROR", "GPI has not been completed, unable to generate report");
					break;
				}
			}
		}
		
		//System.out.println(rd);
*/
		return individualReport;
	}	// End generate()
}
