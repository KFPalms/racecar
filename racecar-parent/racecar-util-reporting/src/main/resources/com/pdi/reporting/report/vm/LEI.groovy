/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.reporting.report.vm;

import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;
import com.pdi.reporting.report.utils.BWUtils;


import com.pdi.scoring.Norm

/**
 * LEI -- This groovy script contains code to create the raw scale scores
 *        from response data ("score()")and the code required to generate
 *        the report data used by the UI when creating a report ("generate()").
 *
 * NOTE:  There is no valid report generation code at this time.
 * 
 * @author gmyers
 * @author mbpanichi
 * @author kbeukelm
 */
public class LEI
{
	//
	// Static data
	//
	private IndividualReport individualReport;

	// ------------- Data used in scoring -------------
	
	// buckets for score accumulation
	private float _scale1  = 0.0;	// LEI3_SGY - Strategy Development
	private float _scale2  = 0.0;	// LEI3_PGT - Project Management and Implementation
	private float _scale3  = 0.0;	// LEI3_BDV - Business Development and Marketing
	private float _scale4  = 0.0;	// LEI3_BGR - Business Growth
	private float _scale5  = 0.0;	// LEI3_PDT - Product Development
	private float _scale6  = 0.0;	// LEI3_STA - Start-up Business
	private float _scale7  = 0.0;	// LEI3_FM  - Financial Management
	private float _scale8  = 0.0;	// LEI3_OPX - Operations
	private float _scale9  = 0.0;	// LEI3_FEX - Support Functions
	private float _scale10 = 0.0;	// LEI3_RPR - External Relations
	private float _scale11 = 0.0;	// LEI3_PRB - Inherited Problems and Challenges
	private float _scale12 = 0.0;	// LEI3_PPL - Interpersonally Challenging Situations
	private float _scale13 = 0.0;	// LEI3_FAL - Downturn and/or Failures
	private float _scale14 = 0.0;	// LEI3_FIN - Difficult Financial Situations
	private float _scale15 = 0.0;	// LEI3_STF - Difficult Staffing Situations
	private float _scale16 = 0.0;	// LEI3_HR  - High Risk Situations
	private float _scale17 = 0.0;	// LEI3_NEG - Critical Negotiations
	private float _scale18 = 0.0;	// LEI3_CMT - Crisis Management
	private float _scale19 = 0.0;	// LEI3_VAS - Highly Critical/Visible Assignments�
	private float _scale20 = 0.0;	// LEI3_SDV - Self-Development Scale 
	private float _scale21 = 0.0;	// LEI3_DEV - Development of Others
	private float _scale22 = 0.0;	// LEI3_INN - International/Cross-Cultural
	private float _scale23 = 0.0;	// LEI3_EXT - Extracurricular Activities

	private float _overall = 0.0;	// LEI3_OAL

	// ------------- Data used in data generation -------------
	
	// Associates the dimensions with the scales that form them
	private static Object[][] DIMENSIONS = new Object[5][2]
	static {
		// "Overall" dimension with scales
		DIMENSIONS[0][0] = "Overall";							DIMENSIONS[0][1] = new ArrayList();
		((ArrayList) DIMENSIONS[0][1]).add("LEI3_OAL");	// LEI3_OAL - Overall
		((ArrayList) DIMENSIONS[0][1]).add("LEI3_GMT");	// LEI3_GMT - General Management Experiences Super Scale
		((ArrayList) DIMENSIONS[0][1]).add("LEI3_CHA");	// LEI3_CHA - Adversity Super Scale
		((ArrayList) DIMENSIONS[0][1]).add("LEI3_RC");	// LEI3_RC - Risky/Critical Super Scale
		((ArrayList) DIMENSIONS[0][1]).add("LEI3_PCR");	// LEI3_PCR - Personal & Career Related Experiences Super Scale
		
		// "General Management Experience" dimension with scales
		DIMENSIONS[1][0] = "General Management Experience";		DIMENSIONS[1][1] = new ArrayList();
		((ArrayList) DIMENSIONS[1][1]).add("LEI3_SGY");	// LEI3_SGY - Strategy Development
		((ArrayList) DIMENSIONS[1][1]).add("LEI3_PGT");	// LEI3_PGT -Project Management & Implementation
		((ArrayList) DIMENSIONS[1][1]).add("LEI3_BDV");	// LEI3_BDV - Business Development & Marketing
		((ArrayList) DIMENSIONS[1][1]).add("LEI3_BGR");	// LEI3_BGR - Business Growth
		((ArrayList) DIMENSIONS[1][1]).add("LEI3_PDT");	// LEI3_PDT - Product Development
		((ArrayList) DIMENSIONS[1][1]).add("LEI3_STA");	// LEI3_STA - Start-up Business
		((ArrayList) DIMENSIONS[1][1]).add("LEI3_FM");	// LEI3_FM  - Financial Management
		((ArrayList) DIMENSIONS[1][1]).add("LEI3_OPX");	// LEI3_OPX - Operations
		((ArrayList) DIMENSIONS[1][1]).add("LEI3_FEX");	// LEI3_FEX - Support Functions
		((ArrayList) DIMENSIONS[1][1]).add("LEI3_RPR");	// LEI3_RPR - External Relations
		
		// "Adversity" dimension with scales
		DIMENSIONS[2][0] = "Adversity";							DIMENSIONS[2][1] = new ArrayList();
		((ArrayList) DIMENSIONS[2][1]).add("LEI3_PRB");	// LEI3_PRB - Inherited Problems & Challenges
		((ArrayList) DIMENSIONS[2][1]).add("LEI3_PPL");	// LEI3_PPL - Interpersonnally Challenging Situations
		((ArrayList) DIMENSIONS[2][1]).add("LEI3_FAL");	// LEI3_FAL - Downturns and/or Failures
		((ArrayList) DIMENSIONS[2][1]).add("LEI3_FIN");	// LEI3_FIN - Difficult Financial Situations
		((ArrayList) DIMENSIONS[2][1]).add("LEI3_STF");	// LEI3_STF - Difficult Staffing Situations
		
		// "Risky/Critical Experiences" dimension with scales
		DIMENSIONS[3][0] = "Risky/Critical Experiences";		DIMENSIONS[3][1] = new ArrayList();
		((ArrayList) DIMENSIONS[3][1]).add("LEI3_HR");	// LEI3_HR - High Risk Situations
		((ArrayList) DIMENSIONS[3][1]).add("LEI3_NEG");	// LEI3_NEG - Critical Negotiations
		((ArrayList) DIMENSIONS[3][1]).add("LEI3_CMT");	// LEI3_CMT - Crisis Management
		((ArrayList) DIMENSIONS[3][1]).add("LEI3_VAS");	// LEI3_VAS - Highly Critical/Visible Assignments or Initiatives
		
		// "Personal and Career Related Experiences" dimension with scales
		DIMENSIONS[4][0] = "Personal and Career Related Experiences";	DIMENSIONS[4][1] = new ArrayList();
		((ArrayList) DIMENSIONS[4][1]).add("LEI3_SDV");	// LEI3_SDV - Self-Development
		((ArrayList) DIMENSIONS[4][1]).add("LEI3_DEV");	// LEI3_DEV - Development of Others
		((ArrayList) DIMENSIONS[4][1]).add("LEI3_INN");	// LEI3_INN - International/Cross Cultural
		((ArrayList) DIMENSIONS[4][1]).add("LEI3_EXT");	// LEI3_EXT - Extracurricular Activities
	}
	
	//
	// Static methods
	//


	
	/*
	 * Create the internal response map.
	 * For all sections except Section 2:
	 *		convert the response value into a floating point number and put it into
	 *		this map.  Nulls or empty responses become a value of 0.0.
	 * For Section 2:
	 * 		"fold" the data together and dump the "folded" information into
	 *		the same map.  When done, there will be entries in this map for
	 *		questions 1 to 118.
	 *
	 * @param inp A HashMap of the responses keyed by the question id
	 * @returns A HashMap floats of the weighted results of the operations keyed by question id
	 */
	private HashMap<String, Float> genQuestMap(HashMap<String, String> inp)
	{
		String ques = "";
		HashMap ret = new HashMap(); 

		for (int i=1; i <= 118; i++)
		{
			String q;
			String r;
			Float f;
			
			//println "i=" + i;
			if (i < 34 || i > 80)
			{
				// "Normal" responses - Sections 1, 3, and 4.  Simply convert
				// the response to a float and push it into the output map.
				q = "Q" + i;
				r = inp.get(q);
				if (r == null || r.length() < 1)
				{
					r = "0";
				}
				f = new Float(r);
				ret.put(q, f);
			}
			else
			{
				// Process section 2 (questions 34 - 80
				Float cumWeightedResp = 0.0;
				String rStr;
				int resp;
				for (int j=1; j <= 3; j++)
				{
					q = "Q_" + i + "_" + j;
					rStr = inp.get(q);
					if (rStr == null || rStr.length() < 1)
					{
						rStr = "0";
					}
					resp = Integer.parseInt(rStr);

					// Weight the response.  There are different weights for each question part
					if (j == 1)	// All Q_xx_1 questions
					{
						switch (resp)
						{
							case 0:
							case 1:
								// Does nothing (effectively adding 0 to cumWeightedResp)
								break;
							case 2:
								cumWeightedResp += 0.25;
								break;
							case 3:
								cumWeightedResp += 0.5;
								break;
							default:
								// Does nothing (effectively adding 0 to cumWeightedResp)
								break;
						};
					};
					else if (j == 2)	// All Q_xx_2 questions
					{
						switch (resp)
						{
							case 0:
							case 1:
								// Does nothing (effectively adding 0 to cumWeightedResp)
								break;
							case 2:
								cumWeightedResp += 0.75;
								break;
							case 3:
								cumWeightedResp += 1.0;
								break;
							default:
								// Does nothing (effectively adding 0 to cumWeightedResp)
								break;
						};
					};
					else if (j == 3)	// All Q_xx_3 questions
					{
						switch (resp)
						{
							case 0:
							case 1:
								// Does nothing (effectively adding 0 to cumWeightedResp)
								break;
							case 2:
								cumWeightedResp += 1.25;
								break;
							case 3:
								cumWeightedResp += 1.5;
								break;
							default:
								// Does nothing (effectively adding 0 to cumWeightedResp)
								break;
						};
					};
				};	// end of 1 - 3 loop

				// put the result into the question map
				cumWeightedResp += 1.0;		// Add 1 to the cumulative total
				q = "Q" + i;
				ret.put(q, cumWeightedResp);
			};	// end of else (it IS 34 through 80)
		}; // End of outer loop (1 - 118)
		
		return ret;
	}


	/*
	 * Calculates the 3 work orientation scores.  Score data is placed
	 * directly into the scores section of the Individual Report object.
	 * 
	 * @param questions A HashMap of floats that represent the weighted question responses
	 */
	private void calcWorkOrientationScores(HashMap<String, Float> questions)
	{		
		float score = 0.0;
		
		/*******************************************************************
		 1.) Work Orientation - Avoid (Performance) Goal Orient
		 ********************************************************************/	
		float wo = 0.0
		float wo2 = 0.0;
		
		for (int i=115; i <= 118; i++)
		{
			String key = "Q" + i;
			score = questions.get(key);
			if (score < 1.0 || score > 7.0)  score = 4.0;	// input of null, zero or anything else
			wo += score
		}

		wo2 = wo/4;
		individualReport.getReportData().get(ReportingConstants.RC_LEI).getScoreData().put("LEI_WO_AVOID", wo2.toString());

		/*******************************************************************
		 2.) Work Orientation -  Learning Goal Orientation
		 ********************************************************************/	
		wo = 0.0
		wo2 = 0.0;

		for (int i=106; i <= 110; i++)
		{
			String key = "Q" + i;
			score = questions.get(key);
			if (score < 1.0 || score > 7.0)  score = 4.0;	// input of null, zero or anything else
			wo += score
		}

		wo2 = wo/5;
		individualReport.getReportData().get(ReportingConstants.RC_LEI).getScoreData().put("LEI_WO_LEARN", wo2.toString());
		
		/*******************************************************************
		 3.) Work Orientation - Prove (Performance) Goal Orient
		 ********************************************************************/	
		wo = 0.0
		wo2 = 0.0;
		
		for (int i=111; i <= 114; i++)
		{
			String key = "Q" + i;
			score = questions.get(key);
			if (score < 1.0 || score > 7.0)  score = 4.0;	// input of null, zero or anything else
			wo += score
		}

		wo2 = wo/4;
		individualReport.getReportData().get(ReportingConstants.RC_LEI).getScoreData().put("LEI_WO_PROVE", wo2.toString());
	}	// End calcWorkOrientationScores
	
	
	/*
	 * Calculate the scale scores:
	 *	 1 - LEI3_SGY - Strategy Development
	 *	 2 - LEI3_PGT - Project Management and Implementation
	 *	 3 - LEI3_BDV - Business Development and Marketing
	 *	 4 - LEI3_BGR - Business Growth
	 *	 5 - LEI3_PDT - Product Development
	 *	 6 - LEI3_STA - Start-up Business
	 *	 7 - LEI3_FM  - Financial Management
	 *	 8 - LEI3_OPX - Operations
	 *	 9 - LEI3_FEX - Support Functions
	 *	10 - LEI3_RPR - External Relations
	 *	11 - LEI3_PRB - Inherited Problems and Challenges
	 *	12 - LEI3_PPL - Interpersonally Challenging Situations
	 *	13 - LEI3_FAL - Downturn and/or Failures
	 *	14 - LEI3_FIN - Difficult Financial Situations
	 *	15 - LEI3_STF - Difficult Staffing Situations
	 *	16 - LEI3_HR  - High Risk Situations
	 *	17 - LEI3_NEG - Critical Negotiations
	 *	18 - LEI3_CMT - Crisis Management
	 *	19 - LEI3_VAS - Highly Critical/Visible Assignments�
	 *	20 - LEI3_SDV - Self-Development Scale 
	 *	21 - LEI3_DEV - Development of Others
	 *	22 - LEI3_INN - International/Cross-Cultural
	 *	23 - LEI3_EXT - Extracurricular Activities
	 *
	 * @param questions A HashMap of responses
	 */
		private void calcScales(HashMap<String, Float> questions)
	{
		_scale1 = LEI_SCORING.calcScale1(questions);
		roundAndSave(_scale1, "LEI3_SGY");
			
		_scale2 = LEI_SCORING.calcScale2(questions);
		roundAndSave(_scale2, "LEI3_PGT");
			
		_scale3 = LEI_SCORING.calcScale3(questions);
		roundAndSave(_scale3, "LEI3_BDV");
			
		_scale4 = LEI_SCORING.calcScale4(questions);
		roundAndSave(_scale4, "LEI3_BGR");
			
		_scale5 = LEI_SCORING.calcScale5(questions);
		roundAndSave(_scale5, "LEI3_PDT");
			
		_scale6 = LEI_SCORING.calcScale6(questions);
		roundAndSave(_scale6, "LEI3_STA");
			
		_scale7 = LEI_SCORING.calcScale7(questions);
		roundAndSave(_scale7, "LEI3_FM");
			
		_scale8 = LEI_SCORING.calcScale8(questions);
		roundAndSave(_scale8, "LEI3_OPX");
			
		_scale9 = LEI_SCORING.calcScale9(questions);
		roundAndSave(_scale9, "LEI3_FEX");

		_scale10 = LEI_SCORING.calcScale10(questions);
		roundAndSave(_scale10, "LEI3_RPR");
			
		_scale11 = LEI_SCORING.calcScale11(questions);
		roundAndSave(_scale11, "LEI3_PRB");
			
		_scale12 = LEI_SCORING.calcScale12(questions);
		roundAndSave(_scale12, "LEI3_PPL");
			
		_scale13 = LEI_SCORING.calcScale13(questions);
		roundAndSave(_scale13, "LEI3_FAL");
			
		_scale14 = LEI_SCORING.calcScale14(questions);
		roundAndSave(_scale14, "LEI3_FIN");
			
		_scale15 = LEI_SCORING.calcScale15(questions);
		roundAndSave(_scale15, "LEI3_STF");
			
		_scale16 = LEI_SCORING.calcScale16(questions);
		roundAndSave(_scale16, "LEI3_HR");
			
		_scale17 = LEI_SCORING.calcScale17(questions);
		roundAndSave(_scale17, "LEI3_NEG");
			
		_scale18 = LEI_SCORING.calcScale18(questions);
		roundAndSave(_scale18, "LEI3_CMT");
			
		_scale19 = LEI_SCORING.calcScale19(questions);
		roundAndSave(_scale19, "LEI3_VAS");
			
		_scale20 = LEI_SCORING.calcScale20(questions);
		roundAndSave(_scale20, "LEI3_SDV");

		_scale21 = LEI_SCORING.calcScale21(questions);
		roundAndSave(_scale21, "LEI3_DEV");

		_scale22 = LEI_SCORING.calcScale22(questions);
		roundAndSave(_scale22, "LEI3_INN");

		_scale23 = LEI_SCORING.calcScale23(questions);
		roundAndSave(_scale23, "LEI3_EXT");
	}


	/*
	 * Rounds the passed score to the nearest 1000th and stores it in the
	 * scoreData element of the LEI ReportData object with the passed key.
	 */
	private void roundAndSave(float score, String key)
	{
		// Round the scoring to the nearest 1000th
		String scoreStr = (Math.round(score * 1000.0)) / 1000.0;
		
		individualReport.getReportData().get(ReportingConstants.RC_LEI).getScoreData().put(key, scoreStr);
	}


	/*
	 * Calculate the 4 super scale scores:
	 * 1 - LEI3_GMT - General Management Experiences
	 * 2 - LEI3_CHA - Adversity
	 * 3 - LEI3_RC - Risky/Critical Experiences
	 * 4 - LEI3_PCR - Personal and Career Related Experiences
	 * The results go directly into the IndividualReport object
	 */
		private void calcSuperScales()
	{

		/*******************************************************************
		 1.)Calculate Super-Scale - General Management Experiences
		 ********************************************************************/	
		float gme = _scale1 + _scale2 + _scale3 +
					_scale4 + _scale5 + _scale6 +
					_scale7 + _scale8 +	_scale9 +
					_scale10;
		
		roundAndSave(gme, "LEI3_GMT");
		
		/*******************************************************************
		 2.)Calculate Super-Scale - Adversity
		 ********************************************************************/	
		float adv = _scale11 + _scale12 + _scale13 +
					_scale14 + _scale15;
		
		roundAndSave(adv, "LEI3_CHA");

		/*******************************************************************
		 3.)Calculate Super-Scale - Risky/Critical Experiences
		 ********************************************************************/	
		float rce = _scale16 + _scale17 + _scale18 + _scale19;
		
		roundAndSave(rce, "LEI3_RC");

		/*******************************************************************
		 4.)Calculate Super-Scale - Personal and Career Related Experiences
		 ********************************************************************/			
		float pce = _scale20 + _scale21 + _scale22 + _scale23;

		roundAndSave(pce, "LEI3_PCR");
	}
	
	
	/*
	 * Calculate the Overall score
	 */
	private void calcOverall()
	{
		/*******************************************************************
		 Calculate Overall
		 ********************************************************************/			
		_overall = _scale1  + _scale2  + _scale3  + _scale4  + _scale5  +
				   _scale6  + _scale7  + _scale8  + _scale9  + _scale10 +
				   _scale11 + _scale12 + _scale13 + _scale14 + _scale15 +
				   _scale16 + _scale17 + _scale18 + _scale19 + _scale20 +
				   _scale21 + _scale22 + _scale23;

		roundAndSave(_overall, "LEI3_OAL");
	}


	/*
	 * Calculate the Dev Sug Scale Standardization
	 */
	private void calcDevSuggScales()
	{
		HashMap<String, String> dest = individualReport.getReportData().get(ReportingConstants.RC_LEI).getScoreData()
		float dssScore;
		
		// STRATEGY - 1 - LEI_DS_SGY
		dssScore = LEI_SCORING.calcDss1(_scale1);
		dest.put("LEI_DS_SGY", dssScore.toString());
		
		// PMGT - 2 - LEI_DS_PGT
		dssScore = LEI_SCORING.calcDss2(_scale2);
		dest.put("LEI_DS_PGT", dssScore.toString());

		// BDEV - 3 - LEI_DS_BDV
		dssScore = LEI_SCORING.calcDss3(_scale3);
		dest.put("LEI_DS_BDV", dssScore.toString());

		// BGROW - 4 - LEI_DS_BGR
		dssScore = LEI_SCORING.calcDss4(_scale4);
		dest.put("LEI_DS_BGR", dssScore.toString());

		// PRODUCT - 5 - LEI_DS_PDT
		dssScore = LEI_SCORING.calcDss5(_scale5);
		dest.put("LEI_DS_PDT", dssScore.toString());

		// START - 6 - LEI_DS_STA
		dssScore = LEI_SCORING.calcDss6(_scale6);
		dest.put("LEI_DS_STA", dssScore.toString());

		// FM - 7 - LEI_DS_FM
		dssScore = LEI_SCORING.calcDss7(_scale7);
		dest.put("LEI_DS_FM", dssScore.toString());

		// OPEXP - 8 - LEI_DS_OPX
		dssScore = LEI_SCORING.calcDss8(_scale8);
		dest.put("LEI_DS_OPX", dssScore.toString());

		// FCNEXP - 9 - LEI_DS_FEX
		dssScore = LEI_SCORING.calcDss9(_scale9);
		dest.put("LEI_DS_FEX", dssScore.toString());

		// REPRE - 10 - LEI_DS_RPR
		dssScore = LEI_SCORING.calcDss10(_scale10);
		dest.put("LEI_DS_RPR", dssScore.toString());

		// PROB - 11 - LEI_DS_PRB
		dssScore = LEI_SCORING.calcDss11(_scale11);
		dest.put("LEI_DS_PRB", dssScore.toString());

		// PEOPLE - 12 - LEI_DS_PPL
		dssScore = LEI_SCORING.calcDss12(_scale12);
		dest.put("LEI_DS_PPL", dssScore.toString());

		// FAILURE - 13 - LEI_DS_FAL
		dssScore = LEI_SCORING.calcDss13(_scale13);
		dest.put("LEI_DS_FAL", dssScore.toString());

		// FINSIT - 14 - LEI_DS_FIN
		dssScore = LEI_SCORING.calcDss14(_scale14);
		dest.put("LEI_DS_FIN", dssScore.toString());

		// STAFF - 15 - LEI_DS_STF
		dssScore = LEI_SCORING.calcDss15(_scale15);
		dest.put("LEI_DS_STF", dssScore.toString());

		// HIRISK - 16 - LEI_DS_HR
		dssScore = LEI_SCORING.calcDss16(_scale16);
		dest.put("LEI_DS_HR", dssScore.toString());

		// NEGOT - 17 - LEI_DS_NEG
		dssScore = LEI_SCORING.calcDss17(_scale17);
		dest.put("LEI_DS_NEG", dssScore.toString());

		// CMGMT - 18 - LEI_DS_CMT
		dssScore = LEI_SCORING.calcDss18(_scale18);
		dest.put("LEI_DS_CMT", dssScore.toString());

		// VISASS - 19 - LEI_DS_VAS
		dssScore = LEI_SCORING.calcDss19(_scale19);
		dest.put("LEI_DS_VAS", dssScore.toString());

		// SELFDEV - 20 - LEI_DS_SDV
		dssScore = LEI_SCORING.calcDss20(_scale20);
		dest.put("LEI_DS_SDV", dssScore.toString());

		// DEV - 21 - LEI_DS_DEV
		dssScore = LEI_SCORING.calcDss21(_scale21);
		dest.put("LEI_DS_DEV", dssScore.toString());

		// INTERN - 22 - LEI_DS_INN
		dssScore = LEI_SCORING.calcDss22(_scale22);
		dest.put("LEI_DS_INN", dssScore.toString());

		// EXTRA - 23 - LEI_DS_EXT
		dssScore = LEI_SCORING.calcDss23(_scale23);
		dest.put("LEI_DS_EXT", dssScore.toString());

		// OVERALL - LEI_DS_OAL
		dssScore = LEI_SCORING.calcDssOverall(_overall);
		dest.put("LEI_DS_OAL", dssScore.toString());
				
		return;
	}
	
	// End of methods associated with score()
	
	
	//*************************************************************************************//
	
	
	/*
	 * score - Rolls answer data up in proper manner to create raw scale scores.
	 * 
	 * @returns An IndividualReport object
	 */
	public IndividualReport score()
	{
		/*********************************************************************
		 1.) CONVERT THE RESPONSES -- Converts the raw response input to a map
		 with keys of question IDs (Q1 - Q118) and values of responses as
		 Floats.  The three-part Section 2 responses are collapsed per the
		 algorithm into single answers.
		 **********************************************************************/
		HashMap<String, String> responses = individualReport.getReportData().get(ReportingConstants.RC_LEI).getRawData();
		HashMap<String, Float> questions = genQuestMap(responses);
		
		//questions.each {question, val ->
		//		println question + "=" + val;
		//}
		
		/*******************************************************************
		 2.) DO WORK ORIENTATION SCORING
		 ********************************************************************/		
		calcWorkOrientationScores(questions);		
		
		/*******************************************************************
		 3.) DO SCALE SCORING -- the calculated scores are both 1) written
		 to the IndividualReport object and 2) saved in instance
		 variables for use in subsequent scale calculations (suer-scales,
		 overall, and dev sugs). 
		 ********************************************************************/		
		calcScales(questions);
		calcSuperScales();
		
		/*******************************************************************
		 4.) DO OVERALL CALC
		 ********************************************************************/				
		calcOverall();
		
		/*******************************************************************
		 5.) DO DEV SUGG SCALE SCORING
		 ********************************************************************/					
		calcDevSuggScales();
		
		/*******************************************************************
		 6.) Return the IndividualReport object (where the scores were placed)
		 ********************************************************************/					
		return individualReport;
	}
	/*
	 * generate - Formats data for use by the UI to generate the report PDF
	 * 
	 * @returns An IndividualReport object
	 */
		public IndividualReport generate()
	{
/*		
		// put out the display name
		
		String fn = individualReport.getDisplayData().get("FIRST_NAME");
		if (fn == null || fn.length() < 1)
			fn = "--";
		String ln = individualReport.getDisplayData().get("LAST_NAME");
		if (ln == null || ln.length() < 1)
			ln = "--";
		individualReport.addDisplayData("DISPLAY_NAME", fn + " " + ln);

		//Get the ReportData object for the LEI report 
		ReportData rd = individualReport.getReportData().get(ReportingConstants.RC_LEI);
		if(rd == null) {
			//System.out.println("LEI IS NULL!");
			// They haven't finished the GPI
			individualReport.getScriptedData().put("STATUS", "NOT_FINISHED");
			individualReport.getScriptedData().put("ERROR", "LEI has not been completed, unable to generate report");
			return ;
		} else {
			individualReport.getScriptedData().put("STATUS", "");
			individualReport.getScriptedData().put("ERROR", "");
			//System.out.println("LEI IS NOT NULL!");
		}
		//println "SP Name=" + rd.getScoreData().get("LEI_SP_NAME");
		//println "GP Name=" + rd.getScoreData().get("LEI_GP_NAME");


		// Calc the reference line positions and put them out
		float[] ref = BWUtils.getRefPosArray();
		
		rd.getScriptedData().put("REF_1", ref[0].toString());
		rd.getScriptedData().put("REF_3", ref[1].toString());
		rd.getScriptedData().put("REF_16", ref[2].toString());
		rd.getScriptedData().put("REF_50", ref[3].toString());
		rd.getScriptedData().put("REF_84", ref[4].toString());
		rd.getScriptedData().put("REF_97", ref[5].toString());
		rd.getScriptedData().put("REF_99", ref[6].toString());
		//System.out.println("REF_1 IS: " + rd.getScriptedData().get("REF_1"));
		//System.out.println("REF_3 IS: " + rd.getScriptedData().get("REF_3"));
		//System.out.println("REF_16 IS: " + rd.getScriptedData().get("REF_16"));
		//System.out.println("REF_50 IS: " + rd.getScriptedData().get("REF_50"));
		//System.out.println("REF_84 IS: " + rd.getScriptedData().get("REF_84"));
		//System.out.println("REF_99 IS: " + rd.getScriptedData().get("REF_99"));
		
		
		// Set up the dimension data
		rd.getDisplayData().put("DIM_COUNT", DIMENSIONS.length.toString());	
		ArrayList dimData = new ArrayList();
		for (int i=0; i < DIMENSIONS.length; i++)
		{
			rd.getDisplayData().put("DIM" + (i+1) + "_NAME", DIMENSIONS[i][0]);
			//println "Dim - " + DIMENSIONS[i][0];
			ArrayList ary = DIMENSIONS[i][1];
			rd.getDisplayData().put("DIM" + (i+1) + "_SCALE_COUNT", ary.size().toString());	

			// Do the scale data
			for (int j=0; j < ary.size(); j++)
			{
				
				String scaleId = DIMENSIONS[i][1].get(j);
				//println "  Scale - " + scaleId;
				rd.getDisplayData().put("DIM" + (i+1) + "_SCALE" + (j+1) + "_ID", scaleId);
				if(rd.getScoreData(scaleId + "_GP_PCTL") == null) {
					//invalid data
					individualReport.getScriptedData().put("STATUS", "NOT_FINISHED");
					individualReport.getScriptedData().put("ERROR", "Corrupted individual report");
					return individualReport;
				}
				// Round the percentiles
				double pctl = Double.parseDouble(rd.getScoreData(scaleId + "_GP_PCTL"));
				int pOut = Math.floor(pctl + 0.5)
				if (pOut < 1)
					pOut = 1.0;
				else if (pOut > 99.0)
					pOut = 99.0;
				//println "    GP pctl=" + pOut;

				rd.getScriptedData().put(scaleId+"_GP_PCTL", pOut.toString());
				pctl = Double.parseDouble(rd.getScoreData(scaleId + "_SP_PCTL"));
				pOut = Math.floor(pctl + 0.5)
				if (pOut < 1.0)
					pOut = 1.0;
				else if (pOut > 99.0)
					pOut = 99.0;
				rd.getScriptedData().put(scaleId + "_SP_PCTL", pOut.toString());
				//println "    SP pctl=" + pOut;

				// Get the norms
				double mean = Double.parseDouble(rd.getScoreData(scaleId + "_GP_SCALE_MEAN"));
				double stDev = Double.parseDouble(rd.getScoreData(scaleId + "_GP_SCALE_STDEV"));
				Norm gpNorm = new Norm(mean,stDev);
				//println scaleId + " GP norm - mean=" + gpNorm.getMean() + ", SD=" + gpNorm.getStdDev();

				mean = stDev = 0.0;
				mean = Double.parseDouble(rd.getScoreData(scaleId + "_SP_SCALE_MEAN"));
				stDev = Double.parseDouble(rd.getScoreData(scaleId + "_SP_SCALE_STDEV"));
				Norm spNorm = new Norm(mean,stDev);
				//println scaleId + " SP norm - mean=" + spNorm.getMean() + ", SD=" + spNorm.getStdDev();
				
				// Calc the score position
				double score = Double.parseDouble(rd.getScoreData().get(scaleId));
				float scorPos = BWUtils.calcScorePos(score, gpNorm);

				rd.getScriptedData().put( scaleId + "_POSN", scorPos.toString());
				//println "        Position=" + scorPos;
				
				// Calc the cut-point positions
				float[] csPos = BWUtils.getCutPosArray(gpNorm, spNorm);
				//System.out.println("Cut Points for " + scaleId + ":");
				//for(int r= 0; r < csPos.length; r++)
				//{
				//	System.out.println("Idx[" + r + "]=" + csPos[r])
				//}
				rd.getScriptedData().put(scaleId + "_CP_3", csPos[0].toString());
				rd.getScriptedData().put(scaleId + "_CP_11", csPos[1].toString());
				rd.getScriptedData().put(scaleId + "_CP_23", csPos[2].toString());
				rd.getScriptedData().put(scaleId + "_CP_40", csPos[3].toString());
				rd.getScriptedData().put(scaleId + "_CP_60", csPos[4].toString());
				rd.getScriptedData().put(scaleId + "_CP_77", csPos[5].toString());
				rd.getScriptedData().put(scaleId + "_CP_89", csPos[6].toString());
				rd.getScriptedData().put(scaleId + "_CP_97", csPos[7].toString());
				//println "     CP_3: " + csPos[0];
				//println "     CP_11: " + csPos[1];
				//println "     CP_23: " + csPos[2];
				//println "     CP_40: " + csPos[3];
				//println "     CP_60: " + csPos[4];
				//println "     CP_77: " + csPos[5];
				//println "     CP_89: " + csPos[6];
				//println "     CP_97: " + csPos[7];
			}	// End of scale loop (index = j)
		}	// End of Dimension loop (index = i)
	
	*/
		//System.out.println(rd);	
		return individualReport;
	}	// End generate()
}