/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.reporting.report.vm;

import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;
import com.pdi.reporting.report.utils.BWUtils;
import com.pdi.scoring.Norm;
//
////used for debug
//import java.util.Iterator;

/**
 * Raven's (Form B)-- Code to create raw scale scores ("score()")
 * and to set up the required data for a report ("generate()")
 * 
 * @author gmyers
 * @author mbpanichi
 * @author kbeukelm
 *
 */
class RAVENS_B
{
	private IndividualReport individualReport;
	
	
	// Scoring key for the Ravens form B
	private static final int[] RAVB_KEY = new int[24];
	static
	{
		RAVB_KEY[1]  = 8
		RAVB_KEY[2]  = 7;
		RAVB_KEY[3]  = 1;
		RAVB_KEY[4]  = 8;
		RAVB_KEY[5]  = 4;
		RAVB_KEY[6]  = 3;
		RAVB_KEY[7]  = 1;
		RAVB_KEY[8]  = 6;
		RAVB_KEY[9]  = 5;
		RAVB_KEY[10] = 4;
		RAVB_KEY[11] = 4;
		RAVB_KEY[12] = 7;
		RAVB_KEY[13] = 8;
		RAVB_KEY[14] = 7;
		RAVB_KEY[15] = 2;
		RAVB_KEY[16] = 5;
		RAVB_KEY[17] = 6;
		RAVB_KEY[18] = 7;
		RAVB_KEY[19] = 8;
		RAVB_KEY[20] = 3;
		RAVB_KEY[21] = 6;
		RAVB_KEY[22] = 3;
		RAVB_KEY[23] = 2;
	}

	
	/*
	 * score - Rolls answer data up in proper manner to create raw scale scores
	 */
	public IndividualReport score()
	{	
		//get questions out
		//println "QUESTIONS HERE";
		// RAVENS_B
		// 23 questions
		int correct = 0;
		int totalIncorrect = 0;
		int unanswered = 0;
		//System.out.println("TIME TO SCORE!!!");
		for (int i=1; i <= 23; i++)
		{
			String ans = individualReport.getReportData().get(ReportingConstants.RC_RAVENS_B).getRawData().get("question_" + i);
			//System.out.println("Answer # " + i + " is " + ans);
			//println "i=" + i + ", ans=" + ans;
			if (ans == null || ans.length() == 0)
			{
				unanswered++;
				continue;
			}
			
			if (Integer.parseInt(ans) == RAVB_KEY[i])
			{
				correct++;
			} else {
				totalIncorrect++;
				
			}
		}
		
		// there are 3 "scale" scores for the Raven's B....
		individualReport.getReportData().get(ReportingConstants.RC_RAVENS_B).getScoreData().put("RAVENSB", correct.toString());
		individualReport.getReportData().get(ReportingConstants.RC_RAVENS_B).getScoreData().put("CORRECT_ANSWERS", correct.toString());
		individualReport.getReportData().get(ReportingConstants.RC_RAVENS_B).getScoreData().put("INCORRECT_ANSWERS", totalIncorrect.toString());
		individualReport.getReportData().get(ReportingConstants.RC_RAVENS_B).getScoreData().put("NUMBER_UNANSWERED", unanswered.toString());
		
		//finally return the individual report
		return individualReport;
		
	}	// End score()
	

	//*************************************************************************************//
	
	
	/*
	 * generate - Formats data for use by the UI to generate the report PDF
	 */
	public IndividualReport generate()
	{
/*
		// put out the display name
		String fn = individualReport.getDisplayData().get("FIRST_NAME");
		if (fn == null || fn.length() < 1)
		fn = "--";
		String ln = individualReport.getDisplayData().get("LAST_NAME");
		if (ln == null || ln.length() < 1)
		ln = "--";
		individualReport.addDisplayData("DISPLAY_NAME", fn + " " + ln);
		////println "DISPLAY_NAME=" + individualReport.getDisplayData("DISPLAY_NAME");
		
		//Get the ReportData object for the Raven's Short Form report 
		ReportData rd = individualReport.getReportData().get(ReportingConstants.RC_RAVENS_B);
		if(rd == null)
		{
			individualReport.getScriptedData().put("STATUS", "NOT_FINISHED");
			individualReport.getScriptedData().put("ERROR", "Could not find Ravens data");
			return individualReport;
		}

		// Calc the reference line positions and put them out
		float[] ref = BWUtils.getRefPosArray();
		rd.getScriptedData().put("REF_1", ref[0].toString());
		rd.getScriptedData().put("REF_3", ref[1].toString());
		rd.getScriptedData().put("REF_16", ref[2].toString());
		rd.getScriptedData().put("REF_50", ref[3].toString());
		rd.getScriptedData().put("REF_84", ref[4].toString());
		rd.getScriptedData().put("REF_97", ref[5].toString());
		rd.getScriptedData().put("REF_99", ref[6].toString());
		//println "  Did the ref lines..."
		
		// Round the percentiles
		if(rd.getScoreData("RAVENSB_GP_PCTL") == null) {
			individualReport.getScriptedData().put("STATUS", "NOT_FINISHED");
			individualReport.getScriptedData().put("ERROR", "No GP percentile");
			return individualReport; 
		}
		double pctl = Double.parseDouble(rd.getScoreData("RAVENSB_GP_PCTL"));
		int pOut = Math.floor(pctl + 0.5)
		if (pOut < 1)
			pOut = 1;
		else if (pOut > 99)
			pOut = 99;
		rd.getScriptedData().put("RAVENSB_GP_PCTL", pOut.toString());
		//println "  Did the GP pctl..."
		
		if(rd.getScoreData("RAVENSB_SP_PCTL") == null) {
			individualReport.getScriptedData().put("STATUS", "NOT_FINISHED");
			individualReport.getScriptedData().put("ERROR", "No SP percentile");
			return individualReport; 
		}
		pctl = Double.parseDouble(rd.getScoreData("RAVENSB_SP_PCTL"));
		pOut = Math.floor(pctl + 0.5)
		if (pOut < 1)
			pOut = 1;
		else if (pOut > 99)
			pOut = 99;
		rd.getScriptedData().put("RAVENSB_SP_PCTL", pOut.toString());
		//println "  Did the SP pctl..."
		
		// Get the norms

		if(rd.getScoreData("RAVENSB_GP_SCALE_MEAN") == null) {
			individualReport.getScriptedData().put("STATUS", "NOT_FINISHED");
			individualReport.getScriptedData().put("ERROR", "No GP norm data");
			return individualReport; 
		}
		double mean = Double.parseDouble(rd.getScoreData("RAVENSB_GP_SCALE_MEAN"));
		double stDev = Double.parseDouble(rd.getScoreData("RAVENSB_GP_SCALE_STDEV"));
		Norm gpNorm = new Norm(mean,stDev);
		//println "  GP norm - mean=" + spNorm.getMean() + ", SD=" + spNorm.getStdDev();
		
		if(rd.getScoreData("RAVENSB_SP_SCALE_MEAN") == null) {
			individualReport.getScriptedData().put("STATUS", "NOT_FINISHED");
			individualReport.getScriptedData().put("ERROR", "No SP norm data");
			return individualReport; 
		}
		mean = Double.parseDouble(rd.getScoreData("RAVENSB_SP_SCALE_MEAN"));
		stDev = Double.parseDouble(rd.getScoreData("RAVENSB_SP_SCALE_STDEV"));
		Norm spNorm = new Norm(mean,stDev);
		//println "  SP norm - mean=" + spNorm.getMean() + ", SD=" + spNorm.getStdDev();
				
		// Calc the score position

		if(rd.getScoreData("RAVENSB") == null) {
			individualReport.getScriptedData().put("STATUS", "NOT_FINISHED");
			individualReport.getScriptedData().put("ERROR", "No score data for Ravens");
			return individualReport; 
		}
		
		double score = Double.parseDouble(rd.getScoreData().get("RAVENSB"));
		float scorPos = BWUtils.calcScorePos(score, gpNorm);
		rd.getScriptedData().put("RAVENSB_POSN", scorPos.toString());

		// Calc the cut-point positions
		float[] csPos = BWUtils.getCutPosArray(gpNorm, spNorm);
		rd.getScriptedData().put("RAVENSB_CP_3", csPos[0].toString());
		rd.getScriptedData().put("RAVENSB_CP_11", csPos[1].toString());
		rd.getScriptedData().put("RAVENSB_CP_23", csPos[2].toString());
		rd.getScriptedData().put("RAVENSB_CP_40", csPos[3].toString());
		rd.getScriptedData().put("RAVENSB_CP_60", csPos[4].toString());
		rd.getScriptedData().put("RAVENSB_CP_77", csPos[5].toString());
		rd.getScriptedData().put("RAVENSB_CP_89", csPos[6].toString());
		rd.getScriptedData().put("RAVENSB_CP_97", csPos[7].toString());
		//System.out.println("Cut Points for " + scaleId + ":");
		//for(int r= 0; r < csPos.length; r++)
		//{
		//	System.out.println("Idx[" + r + "]=" + csPos[r])
		//}
		//Don't care what anyone else says, I was able to score a report, this is finished
		individualReport.scriptedData.put("STATUS", "FINISHED");						
		//println "  Ready to return..."	
*/		
		return individualReport;
	}	// End generate()
	
}
