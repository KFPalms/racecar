/*
 * Copyright (c) 2017 Korn Ferry.
 * All rights reserved.
 */
package com.pdi.reporting.report.vm

import com.pdi.reporting.report.IndividualReport

/**
 * This groovy script contains code to generate the report data required for use
 * by the UI when it is generating the ABYD EVAL GUIDE.
 
 * 
 * @author marnold
 */
public class ABYD_EVAL_GUIDE
{
	//
	// Static data.
	//
	private IndividualReport individualReport;


	// ------------- Data used in report data generation -------------
	

	//	
	// Static methods.
	//
	
	/*
	 * score - Rolls answer data up in proper manner to create raw scale scores.
	 * 
	 * NOTE:  This method probably won't be used but it is here as a place-holder
	 * 
	 * @returns An IndividualReport object
	 */
	public IndividualReport score()
	{
		//print out all the data
		 
		
		//finally return an empty IndividualReport object for now
		return individualReport;
		
	}	// End score()
	
	
	//*************************************************************************************//
	

	/*
	 * generate - Formats data for use by the UI to generate the report PDF
	 * 
	 * NOTE:  Not yet implemented
	 * 
	 * @returns An IndividualReport object
	 */
		public IndividualReport generate()
	{
		//System.out.println(individualReport.getReportData());
		
		// Return an empty IndividualReport object for now
		return individualReport;
	}	// End generate()
}
