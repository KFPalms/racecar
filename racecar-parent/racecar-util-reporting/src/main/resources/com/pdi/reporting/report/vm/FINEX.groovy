/*
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.reporting.report.vm;

import java.util.HashMap;

import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.constants.ReportingConstants;

/**
 * Financial Exercise -- This groovy script contains code to create the raw
 * scale scores from response data ("score()")and the code required to generate
 * the report data used by the UI when creating a report ("generate()").
 * NOTE:  There is no valid report generation code at this time.
 * 
 * @author kbeukelm
 */
class FinEx
{
	//
	// data.
	//
	private IndividualReport individualReport;

	// ------------- Data used in scoring -------------
	
	// Item scoring key
	private static int[] FIN_EX_ITEM_KEY = new int[14];
	static
	{
		// Fill the array with the correct answers.
		//		Note that the answers are present by question id.
		//		Answer 0 is bogus (place-holder only);
		FIN_EX_ITEM_KEY[0]  = 0;	// Bogus
		FIN_EX_ITEM_KEY[1]  = 3;  // C
		FIN_EX_ITEM_KEY[2]  = 1;  // A
		FIN_EX_ITEM_KEY[3]  = 3;  // C
		FIN_EX_ITEM_KEY[4]  = 4;  // D
		FIN_EX_ITEM_KEY[5]  = 3;  // C
		FIN_EX_ITEM_KEY[6]  = 2;  // B
		FIN_EX_ITEM_KEY[7]  = 1;  // A
		FIN_EX_ITEM_KEY[8]  = 2;  // B
		FIN_EX_ITEM_KEY[9]  = 2;  // B
		FIN_EX_ITEM_KEY[10] = 4;  // D
		FIN_EX_ITEM_KEY[11] = 3;  // C
		FIN_EX_ITEM_KEY[12] = 1;  // A
		FIN_EX_ITEM_KEY[13] = 4;  // D
	}
	
	// Behavior question list
	private static HashMap<String, int[]>  FIN_EX_BEHAVIORS = new HashMap<String, int[]>() ;
	static
	{
		// Fill the HashMap with behavior names and lists of question numbers.
		// We have to do it the stupid way because groovy doesn't like Java initializers
		int[] mpd = new int[3];
		mpd[0] = 1;
		mpd[1] = 5;
		mpd[2] = 9;
		FIN_EX_BEHAVIORS.put("FE_MPD", mpd);	// "Make prudent decisions..."
		int[] dac = new int[4];
		dac[0] = 2;
		dac[1] = 6;
		dac[2] = 10;
		dac[3] = 12;
		FIN_EX_BEHAVIORS.put("FE_DAC", dac);	// "Draw appropriate conclusions..."
				int[] afc = new int[4];
		afc[0] = 3;
		afc[1] = 7;
		afc[2] = 11;
		afc[3] = 13;
		FIN_EX_BEHAVIORS.put("FE_AFC", afc);	// "Accurately forecast costs..."
		int[] dam = new int[2];
		dam[0] = 4;
		dam[1] = 8;
		FIN_EX_BEHAVIORS.put("FE_DAM", dam);	// "Deliver appropriate messages..."
	}
	
	// Conversion tables from scores to PDI Behavior ratings.  We add up the correct responses
	// for the questions and convert that to a 1-5 scale using the appropriate array below.
	private static int[] RATE_2Q = new int[3];
	private static int[] RATE_3Q = new int[4];
	private static int[] RATE_4Q = new int[5];
	static
	{
		// Conversion for a 2 question behavior
		RATE_2Q[0] = 1;
		RATE_2Q[1] = 3;
		RATE_2Q[2] = 5;
		// Conversion for a 3 question behavior
		RATE_3Q[0] = 1;
		RATE_3Q[1] = 2;
		RATE_3Q[2] = 4;
		RATE_3Q[3] = 5;
		// Conversion for a 4 question behavior
		RATE_4Q[0] = 1;
		RATE_4Q[1] = 2;
		RATE_4Q[2] = 3;
		RATE_4Q[3] = 4;
		RATE_4Q[4] = 5;
												
	}
			
	
	//	
	// methods.
	//
	
	/*
	 * score - Rolls answer data up in proper manner to create raw scale scores.
	 * 
	 * @returns An IndividualReport object
	 */
	public IndividualReport score()
	{
		//println "DEBUG: Starting Finex scoring...";
		// Get the responses
		HashMap<String, String> responses = individualReport.getReportData().get(ReportingConstants.RC_FIN_EX).getRawData();
		
		// Get the answers
		int[] ans = getAnswers(responses);
		
		// Score the items
		int[] itemScores = applyAnswerKey(ans);
		
		// Score the behaviors
		scoreBehaviors(itemScores);
				
		// Return the updated IndividualReport object
		return individualReport;
	}	// End score()
	
	
	/*
	 * Gets the answers from a HashMap (called with the
	 * raw data array from ReportData).
	 * ASSUMES:
	 * 		Answers are labeled Q1 - Q14 
	 * 		Answers are ints, uppercase letters, or lowercase letters
	 * 		Output array will have 14 entries (0 - 14) - 0 is wasted.
	 * 		Missing entries will be present as zeros.
	 */
	private int[] getAnswers(HashMap<String, String> rDat)
	{
		int[] ret = new int[14];
		
		for(int i=1; i < ret.length; i++)
		{
			String ans = rDat.get("Q" + (i));
			if (ans == null || ans.isEmpty())
			{
				// Not a valid answer - not there or empty string
				ret[i] = 0;
				continue;
			}
			
			int cc = ans.charAt(0);
			ret[i]
			if (cc < 58)	// 0 - 9 ====> 48 - 57
			{
				// its a numeric
				ret[i] = Integer.parseInt(ans);
				continue;
			}
			if (cc < 91)	// A - Z ====> 65 - 90
			{
				// Letters A - Z (Upper case)
				ret[i] = (cc + 1) - 65;
				continue;
			}
			if (cc < 123)	// a - z ====> 97 - 122
			{
				// Letters a - z (Lower case)
				ret[i] = (cc + 1) - 97;
				continue
			}
			
			// If here, it's a way bad answer
			ret[i] = 0;
		}
		
		return ret;
	}
	
	
	/*
	 * Scores the answers.
	 * ASSUMES:
	 * 		Answers are numeric digits 1 - 4
	 * 		Valid score is 1 (correct) or 0 (incorrect)
	 * 		Output array will have 14 entries (0 - 300) - 0 is wasted.
	 */
	private int[] applyAnswerKey(int[] ans)
	{
		int[] ret = new int[14];
		
		for(int i=1; i < 14; i++)
		{
			ret[i] = (ans[i] == FIN_EX_ITEM_KEY[i]) ? 1 : 0;
		}
		
		return ret;
	}
	
	
	/*
	 * Scores the behaviors (roll-up).
	 */
	private void scoreBehaviors(int[] ans)
	{
		int correctCount = 0;
		int bScore;

		Iterator itr = FIN_EX_BEHAVIORS.entrySet().iterator();
		Map.Entry ent;
		while(itr.hasNext())
		{
			// process each behavior
			ent = itr.next();
			int[] ary = ent.getValue();
			bScore = 0;
			for (int i=0; i < ary.length; i++)
			{
				int q = ary[i];
				bScore += ans[q];
				if (ans[q] == 1)
				correctCount++;
			}
			
			// We have the accumulated score, turn it into a behavior score
			int score = 0;
			switch (ary.length)
			{
				case 2:
				score = RATE_2Q[bScore];
				break;
				case 3:
				score = RATE_3Q[bScore];
				break;
				case 4:
				score = RATE_4Q[bScore];
				break;
			}
			// Save it in the output
			individualReport.getReportData().get(ReportingConstants.RC_FIN_EX).getScoreData().put(ent.getKey(), score.toString());
			//println "DEBUG:  FinEx " + ent.getKey() + "scale score=" + score;
		}
		individualReport.getReportData().get(ReportingConstants.RC_FIN_EX).getScoreData().put("FE_CORRECT", correctCount.toString());
		
		return;
	}
	
	
	//*************************************************************************************//
	
	
	/*
	 * generate - Formats data for use by the UI to generate the report PDF
	 * 
	 * NOTE:  Implementation is in the ReportHelper.java  
	 *        This generate does nothing.
	 * 
	 * @returns An IndividualReport object
	 */
	public IndividualReport generate()
	{ 
		
		// just return the report object
		return individualReport;
	}	// End generate()
}
