/*
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.reporting.report.vm

import com.pdi.reporting.report.GroupReport
import com.pdi.scoring.Norm
import com.pdi.reporting.report.IndividualReport


/**
 * @author gmyers
 */
public class TLT_GROUP_SUMMARY_REPORT
{
	private GroupReport groupReport;
	
	private double transitionLevelMean;
	private double transitionLevelStdDev;
	private double gpiGenPopMean;
	private double gpiGenPopStdDev;
	private double gpiSpecPopMean;
	private double gpiSpecPopStdDev;
	
	/*
	 * parse
	 */
	private Double parse(String value)
	{
		Double d;
		try
		{
			d = Double.parseDouble(value);
		}
		catch(Exception e)
		{
			d = 0;
		}
		return d;
	}
	
	
	/*
	 * generate
	 */
	public GroupReport generate()
	{
		Norm norm = new Norm();
		transitionLevelMean = parse(groupReport.getDisplayData("TRANSITION_MEAN"));
		transitionLevelStdDev = parse(groupReport.getDisplayData("TRANSITION_STANDARD_DEVIATION"));
		
		gpiGenPopMean = parse(groupReport.getDisplayData("GEN_POP_NORM_MEAN"));
		gpiGenPopStdDev = parse(groupReport.getDisplayData("GEN_POP_NORM_STANDARD_DEVIATION"));
		
		gpiSpecPopMean = parse(groupReport.getDisplayData("SPEC_POP_NORM_MEAN"));
		gpiSpecPopStdDev = parse(groupReport.getDisplayData("SPEC_POP_NORM_STANDARD_DEVIATION"));
		
		groupReport.getIndividualReports().each { individualReport ->
			generate(individualReport);
		}

		return groupReport;
	}
	
	
	/*
	 * generate
	 */
	public IndividualReport generate(IndividualReport individualReport)
	{
		Norm norm = new Norm();
		//individualReport.getReportData().each {instrumentKey, instrumentValue ->
		//	println "*********************************************";
		//	println instrumentKey;
		//	println "*********************************************";
		//	instrumentValue.getScoreData().each {scoreKey, scoreValue -> 
		//		println scoreKey + " = " + scoreValue;
		//	}
		//} 
		Random randomGenerator = new Random();

		//FAKED
		individualReport.getScriptedData().put("TRANSITION_SUCCESS",(randomGenerator.nextInt(9)+ 1)+"");
		individualReport.getScriptedData().put("ESTIMATED_PERFORMANCE",(randomGenerator.nextInt(9)+ 1)+"");
		individualReport.getScriptedData().put("DERAILMENT_RISK",(randomGenerator.nextInt(9)+ 1)+"");
		
		//FAKED
		individualReport.getScriptedData().put("LEADERSHIP_INTEREST",(randomGenerator.nextInt(9)+ 1)+"");
		individualReport.getScriptedData().put("LEADERSHIP_EXPERIENCE",(randomGenerator.nextInt(9)+ 1)+"");
		individualReport.getScriptedData().put("LEADERSHIP_FOUNDATIONS",(randomGenerator.nextInt(9)+ 1)+"");

		return individualReport;
	}
}
