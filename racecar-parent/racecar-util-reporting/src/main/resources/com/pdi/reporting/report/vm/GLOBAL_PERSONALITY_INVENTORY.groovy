/*
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.reporting.report.vm

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import org.w3c.dom.Document;

import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.GroupReport
import com.pdi.reporting.report.IndividualReport
import com.pdi.reporting.report.ReportData;
import com.pdi.reporting.report.utils.BWUtils;
import com.pdi.scoring.Norm;
import com.sun.org.apache.xpath.internal.XPathAPI;


/**
 * GPI -- This groovy script contains code to create the raw scale scores
 * from response data ("score()")and the code required to generate the
 * report data used by the UI when creating a report ("generate()").
 * 
 * @author gmyers
 */
public class GLOBAL_PERSONALITY_INVENTORY
{

	/*
	 * ANYTHING THAT APPLIES TO ALL PARTICIPANTS MUST BE STATIC
	 */
	//Data
	private static Document data;

	
	//Global Scoring - usually applies to all instruments
	private static Object INSTRUMENT_SCORE_KEY = [:];
	
	//Global Scoring - specific to GPI
	private static double GPI_CP_COUNT = 0;
	private static Object GPI_RDI_RECODE = [:];
	private static Object GPI_RDI_CROSSPRODUCT = [:];
	private static Object GPI_RDI_Z = [:];

	//Global Reporting
	private static Object REPORT_DIMENSIONS = [:];

	/*
	 * ANYTHING SPECIFIC TO THIS PARTICIPANT/RUNTIME MUST NOT BE STATIC
	 */
	//Runtime Scoring - usually applies to all instruments
	private Object PARTICIPANT_INSTRUMENT_SCORE_DATA = [:];
	private Object PARTICIPANT_INSTRUMENT_RAW_DATA = [:];
	private Object PARTICIPANT_INSTRUMENT_WEIGHTED_DATA = [:];
	
	//Runtime reporting
	private IndividualReport individualReport;

	/**
	 * All reports should have an init method, this will be called on application startup to move data
	 */
	public static init() {
		if(GPI_CP_COUNT != 0) {
			return;
		}

		/**
		 * PHASE 1: Handle all normal scale scores, just about every instrument will have something like this
		 */
		Object scores = [
				"GPI_ADPT","GPI_AD","GPI_COMP",
				"GPI_CONS","GPI_DACH","GPI_DADV",
				"GPI_DUT","GPI_EGO","GPI_EC",
				"GPI_EMP","GPI_EL","GPI_IMPR", "GPI_IND",
				"GPI_INFL","GPI_INIT","GPI_INOV",
				"GPI_INTD","GPI_INTI","GPI_MAN",
				"GPI_MIC","GPI_NA","GPI_OPEN",
				"GPI_OPT","GPI_PA","GPI_RESP",
				"GPI_SO","GPI_AST","GPI_ST",
				"GPI_TC","GPI_TA","GPI_TF",
				"GPI_TR","GPI_VIS","GPI_WF",
				"GPI_RISK","GPI_SASI","GPI_SC"
				];
		
		scores.each { score ->
			INSTRUMENT_SCORE_KEY[score] = [:];
			XPathAPI.selectNodeList(data, "//dataset[@code='${score}']/reference").each {reference ->
				String question = XPathAPI.eval(reference, "@question");
				String value = XPathAPI.eval(reference, "@value");
				INSTRUMENT_SCORE_KEY[score][question] = value.equalsIgnoreCase("1");
			}
		}
		
		/**
		 * PHASE 2: Recode values
		 */
		XPathAPI.selectNodeList(data, "//dataset[@code='GPI_RDI_RECODE']/reference").each {
			String question = XPathAPI.eval(it, "@question");
			GPI_RDI_RECODE[question] = true;
		}
		
		/**
		 * PHASE 3: Cross Product
		 */
		GPI_CP_COUNT = Double.parseDouble(XPathAPI.eval(data, "//dataset[@code='GPI_CP_COUNT']/reference/@value").str());
		
		int i = 0;
		XPathAPI.selectNodeList(data, "//dataset[@code='GPI_RDI_CROSSPRODUCT']/reference").each {
			String question1 = XPathAPI.eval(it, "@question1");
			String question2 = XPathAPI.eval(it, "@question2");
			GPI_RDI_CROSSPRODUCT[i] = [:];
			GPI_RDI_CROSSPRODUCT[i].question1 = question1;
			GPI_RDI_CROSSPRODUCT[i].question2 = question2;
			i++;
		}
		
		/**
		 * PHASE 4: Z-Score
		 */
		XPathAPI.selectNodeList(data, "//dataset[@code='GPI_RDI_Z']/reference").each {
			String question = XPathAPI.eval(it, "@question");
			double mean = Double.parseDouble(XPathAPI.eval(it, "@mean").str());
			double stdev = Double.parseDouble(XPathAPI.eval(it, "@stdev").str());
			
			GPI_RDI_Z[question] = [:];
			GPI_RDI_Z[question].question = question;
			GPI_RDI_Z[question].mean = mean;
			GPI_RDI_Z[question].stdev = stdev;
		}
		
		//GPI_RDI_RECODE["test"] = [ Nat:24, Jules:25, Josh:17 ];
		
		
	}
	
	private double weightAnswer(questionAscending, rawValue) {
		double weightedValue = 0.0;
		if(questionAscending) {
			switch(rawValue) {
				case 1:
					break;
				case 2:
					weightedValue = 0.25;
					break;
				case 3:
					weightedValue = 0.5;
					break;
				case 4:
					weightedValue = 0.75;
					break;
				case 5:
					weightedValue = 1;
					break;
				default:
					break;
			}
			
		} else {
			switch(rawValue) {
				case 1:
					weightedValue = 1;
					break;
				case 2:
					weightedValue = 0.75;
					break;
				case 3:
					weightedValue = 0.5;
					break;
				case 4:
					weightedValue = 0.25;
					break;
				case 5:
					break;
				default:
					break;
			}
			
		}
		return weightedValue;
	}
	
	public ReportData score(ReportData rd) {
		//Load up the static data
		init();
		
		//Set the answer key
		PARTICIPANT_INSTRUMENT_RAW_DATA = rd.getRawData();
		
		//Loop through all of the scale scores
		INSTRUMENT_SCORE_KEY.each { scoreKey, scoreQuestions ->

			PARTICIPANT_INSTRUMENT_SCORE_DATA[scoreKey] = [:];
			double scoreValue = 0.0;
			scoreQuestions.each { questionCode, questionAscending -> 
				
				int rawValue = 0;

				if(PARTICIPANT_INSTRUMENT_RAW_DATA[questionCode] != null && PARTICIPANT_INSTRUMENT_RAW_DATA[questionCode] != "") {
					rawValue = Integer.parseInt(PARTICIPANT_INSTRUMENT_RAW_DATA[questionCode]);
				}
				double weightedValue = weightAnswer(questionAscending, rawValue);
				PARTICIPANT_INSTRUMENT_WEIGHTED_DATA[questionCode] = weightedValue;
				scoreValue += weightedValue;


			}
			PARTICIPANT_INSTRUMENT_SCORE_DATA[scoreKey] = scoreValue;
		}
		PARTICIPANT_INSTRUMENT_SCORE_DATA.each {scoreKey, scoreValue ->
			System.out.println(scoreKey + " = " + scoreValue);
			rd.getScoreData().put(scoreKey, scoreValue + "");
		}	
		
		//Start of RDI
		
		//Phase 1: Doing Recodes, z, mean, stdev
		Object PARTICIPANT_INSTRUMENT_DATA_RDI = [:];
		PARTICIPANT_INSTRUMENT_RAW_DATA.each { questionCode, valueString ->
			
			double weightedValue = PARTICIPANT_INSTRUMENT_WEIGHTED_DATA[questionCode];
			if(GPI_RDI_RECODE[questionCode]) {
				weightedValue = 1.0 - weightedValue;
			}
			Object zScore = GPI_RDI_Z[questionCode];
			//System.out.println("ZScore = " + GPI_RDI_Z[questionCode]);
			//System.out.println("Weighted = " + weightedValue);
			
			double zValue = (weightedValue - zScore.mean) / zScore.stdev;
			//System.out.println("zValue = " + zValue + " ( " + zScore.mean + " / " + zScore.stdev + " )");
			
			PARTICIPANT_INSTRUMENT_DATA_RDI[questionCode] = zValue;
			
			
		}
		double crossproduct = 0.0;
		double i = 0;
		GPI_RDI_CROSSPRODUCT.each { code, map ->
			crossproduct += PARTICIPANT_INSTRUMENT_DATA_RDI[map.question1] * PARTICIPANT_INSTRUMENT_DATA_RDI[map.question2];
			i++;
		}
		
		crossproduct = crossproduct / i;
		String percentile = calculatePercentileFromCrossproduct(crossproduct);
		rd.getScoreData().put("PGPI_RDI", percentile);
		return rd;
	}
	
	/*
	 * Convert the CVI to a percentile
	 * 
	 * Decided to use Chris' logic for this.  In V2 the numbers are the same but we found that
	 * The logic was (substantially) correct for pctl 1 - 39, but they duped 38 as 40 and 39 as 41
	 * and from 41 through 85 the percentiles were reading 2 points too high (HRA read 55 when the
	 * number used ws the one for research's 53). At HRA's 86th percentile, they skipped a number
	 * and now they read only 1 percentile too high (HRA's 90 was research's 89).
	 */
	private String calculatePercentileFromCrossproduct(double cvi)
	{
		int pctl = 0;

		if (cvi <= -0.16966985849919725)		pctl = 1;
		else if (cvi <= -0.13696030946656482)	pctl = 2;
		else if (cvi <= -0.1207554579437209)	pctl = 3;
		else if (cvi <= -0.11411821360639715)	pctl = 4;
		else if (cvi <= -0.10509966741050229)	pctl = 5;
		else if (cvi <= -0.09672028098476175)	pctl = 6;
		else if (cvi <= -0.089427468624583117)	pctl = 7;
		else if (cvi <= -0.082789644789247496)	pctl = 8;
		else if (cvi <= -0.078396059756110942)	pctl = 9;
		else if (cvi <= -0.074115507490735658)	pctl = 10;
		else if (cvi <= -0.069697982021272573)	pctl = 11;
		else if (cvi <= -0.065633438954249473)	pctl = 12;
		else if (cvi <= -0.063015423529638806)	pctl = 13;
		else if (cvi <= -0.060233195632737116)	pctl = 14;
		else if (cvi <= -0.058208212112545642)	pctl = 15;
		else if (cvi <= -0.056022995407653001)	pctl = 16;
		else if (cvi <= -0.05290037488237128)	pctl = 17;
		else if (cvi <= -0.049906654231385517)	pctl = 18;
		else if (cvi <= -0.048399103744713116)	pctl = 19;
		else if (cvi <= -0.046642393383809287)	pctl = 20;
		else if (cvi <= -0.044930300458579465)	pctl = 21;
		else if (cvi <= -0.043117852074069943)	pctl = 22;
		else if (cvi <= -0.041841326374401619)	pctl = 23;
		else if (cvi <= -0.039879610585240415)	pctl = 24;
		else if (cvi <= -0.03853737494969129)	pctl = 25;
		else if (cvi <= -0.036549836286672376)	pctl = 26;
		else if (cvi <= -0.035194738829403376)	pctl = 27;
		else if (cvi <= -0.034121915686083318)	pctl = 28;
		else if (cvi <= -0.033007074835552715)	pctl = 29;
		else if (cvi <= -0.031528384972134976)	pctl = 30;
		else if (cvi <= -0.030388045654790134)	pctl = 31;
		else if (cvi <= -0.029106520565240063)	pctl = 32;
		else if (cvi <= -0.027558161540817853)	pctl = 33;
		else if (cvi <= -0.025302633480912464)	pctl = 34;
		else if (cvi <= -0.023785116736692168)	pctl = 35;
		else if (cvi <= -0.023017086826841233)	pctl = 36;
		else if (cvi <= -0.021597563158133219)	pctl = 37;
		else if (cvi <= -0.02038144512265792)	pctl = 38;
		else if (cvi <= -0.019489596608230356)	pctl = 39;
		else if (cvi <= -0.018095753593356231)	pctl = 40;
		else if (cvi <= -0.016909197567306182)	pctl = 41;
		else if (cvi <= -0.015128029916176206)	pctl = 42;
		else if (cvi <= -0.01438846743350624)	pctl = 43;
		else if (cvi <= -0.013440473532661723)	pctl = 44;
		else if (cvi <= -0.01258706898006175)	pctl = 45;
		else if (cvi <= -0.011224380896694492)	pctl = 46;
		else if (cvi <= -0.010384944714149582)	pctl = 47;
		else if (cvi <= -0.0090853002503220089)	pctl = 48;
		else if (cvi <= -0.0083133523398350988)	pctl = 49;
		else if (cvi <= -0.0071563290072970097)	pctl = 50;
		else if (cvi <= -0.0059542512386922793)	pctl = 51;
		else if (cvi <= -0.0051148909442527458)	pctl = 52;
		else if (cvi <= -0.0040980845501925271)	pctl = 53;
		else if (cvi <= -0.0031133546877791332)	pctl = 54;
		else if (cvi <= -0.001946905238618373)	pctl = 55;
		else if (cvi <= -0.00083198742987670602)	pctl = 56;
		else if (cvi <= 0.00021061312940573695)	pctl = 57;
		else if (cvi <= 0.001456269553112365)	pctl = 58;
		else if (cvi <= 0.0025390389851302209)	pctl = 59;
		else if (cvi <= 0.0035359843716760027)	pctl = 60;
		else if (cvi <= 0.0050986126637216772)	pctl = 61;
		else if (cvi <= 0.0063752842205078569)	pctl = 62;
		else if (cvi <= 0.0076397931588243284)	pctl = 63;
		else if (cvi <= 0.0093644562400389056)	pctl = 64;
		else if (cvi <= 0.010834652283741754)	pctl = 65;
		else if (cvi <= 0.012260401224350598)	pctl = 66;
		else if (cvi <= 0.014058291348309005)	pctl = 67;
		else if (cvi <= 0.01556734242211867)	pctl = 68;
		else if (cvi <= 0.017306981306469543)	pctl = 69;
		else if (cvi <= 0.01951438448309448)	pctl = 70;
		else if (cvi <= 0.02109887131490213)	pctl = 71;
		else if (cvi <= 0.023440758045153838)	pctl = 72;
		else if (cvi <= 0.026238254520155151)	pctl = 73;
		else if (cvi <= 0.028090985397530441)	pctl = 74;
		else if (cvi <= 0.029591701891467571)	pctl = 75;
		else if (cvi <= 0.03173002283469701)	pctl = 76;
		else if (cvi <= 0.033999771699890234)	pctl = 77;
		else if (cvi <= 0.037195254950942946)	pctl = 78;
		else if (cvi <= 0.04065946916804699)	pctl = 79;
		else if (cvi <= 0.042975994834457298)	pctl = 80;
		else if (cvi <= 0.045217376315423871)	pctl = 81;
		else if (cvi <= 0.048396546689536774)	pctl = 82;
		else if (cvi <= 0.051575685005030722)	pctl = 83;
		else if (cvi <= 0.055090888926911608)	pctl = 84;
		else if (cvi <= 0.061611475069100745)	pctl = 85;
		else if (cvi <= 0.069153235991170042)	pctl = 86;
		else if (cvi <= 0.073579074990245019)	pctl = 87;
		else if (cvi <= 0.078428407547228213)	pctl = 88;
		else if (cvi <= 0.087107474211367664)	pctl = 89;
		else if (cvi <= 0.092506925560241413)	pctl = 90;
		else if (cvi <= 0.10221042641142068)	pctl = 91;
		else if (cvi <= 0.11018547350728503)	pctl = 92;
		else if (cvi <= 0.1220401730394526)		pctl = 93;
		else if (cvi <= 0.13743813537351005)	pctl = 94;
		else if (cvi <= 0.14945623926307075)	pctl = 95;
		else if (cvi <= 0.17460889833451426)	pctl = 96;
		else if (cvi <= 0.20936800964618374)	pctl = 97;
		else if (cvi <= 0.27281308086395406)	pctl = 98;
		else if (cvi >  0.27281308086395406)	pctl = 99;

		// String-ify it when we return
		return "" + pctl;
	}
	
	
}
