/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.reporting.report.vm

import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.IndividualReport
import com.pdi.reporting.report.ReportData;
import com.pdi.reporting.report.utils.BWUtils;
import com.pdi.scoring.Norm;

//used for debug
import java.util.Iterator;


/**
 * Raven's (Short Form)-- Code to create raw scale scores ("score()")
 * and to set up the required data for a report ("generate()")
 * 
 * @author gmyers
 * @author mbpanichi
 * @author kbeukelm
 *
 */
public class RAVENS_SF
{
	private static IndividualReport individualReport;

	// Scoring key for the Ravens short form
	private static final int[] RAV_KEY = new int[24];
	static
	{
		RAV_KEY[1] = 4;
		RAV_KEY[2] = 2;
		RAV_KEY[3] = 4;
		RAV_KEY[4] = 6;
		RAV_KEY[5] = 7;
		RAV_KEY[6] = 3;
		RAV_KEY[7] = 8;
		RAV_KEY[8] = 8;
		RAV_KEY[9] = 7;
		RAV_KEY[10] = 6;
		RAV_KEY[11] = 3;
		RAV_KEY[12] = 7;
		RAV_KEY[13] = 2;
		RAV_KEY[14] = 7;
		RAV_KEY[15] = 5;
		RAV_KEY[16] = 6;
		RAV_KEY[17] = 5;
		RAV_KEY[18] = 4;
		RAV_KEY[19] = 8;
		RAV_KEY[20] = 5;
		RAV_KEY[21] = 1;
		RAV_KEY[22] = 3;
		RAV_KEY[23] = 2;
	};


	/*
	 * score - Rolls answer data up in proper manner to create raw scale scores
	 */
	public static IndividualReport score()
	{
		//print out all the data
		//individualReport.getReportData().each {instrumentKey, instrumentValue ->
		//	//println "*********************************************";
		//	//println instrumentKey;
		//	//println "*********************************************";
		//	instrumentValue.getRawData().each {rawKey, rawValue ->
		//		//show all questions
		//		//println rawKey + " = " + rawValue;			
		//	}
		//} 
		
		//get questions out
		//println "QUESTIONS HERE";
		// RAVENS_SF
		// 23 questions
		int correct = 0;
		int totalIncorrect = 0;
		int unanswered = 0;
		
		for (int i=1; i <= 23; i++)
		{
			String ans = individualReport.getReportData().get(ReportingConstants.RC_RAVENS_SF).getRawData().get("question_" + i);
				//println "i=" + i + ", ans=" + ans;
			if (ans == null || ans.length() == 0)
			{
				unanswered++;
				continue;
			}
			
			if (Integer.parseInt(ans) == RAV_KEY[i])
			{
				correct++;
			} else {
				totalIncorrect++;
				
			}
		}
		
		// there are 3 "scale" scores for the Raven's SF....
		individualReport.getReportData().get(ReportingConstants.RC_RAVENS_SF).getScoreData().put("CORRECT_ANSWERS", correct.toString());
		individualReport.getReportData().get(ReportingConstants.RC_RAVENS_SF).getScoreData().put("INCORRECT_ANSWERS", totalIncorrect.toString());
		individualReport.getReportData().get(ReportingConstants.RC_RAVENS_SF).getScoreData().put("NUMBER_UNANSWERED", unanswered.toString());

		//finally return the individual report
		return individualReport;
		
	}	// End score()
	

	//*************************************************************************************//
	
	
	/*
	 * generate - Formats data for use by the UI to generate the report PDF
	 */
	public static IndividualReport generate()
	{
		// put out the display name
		String fn = individualReport.getDisplayData().get("FIRST_NAME");
		if (fn == null || fn.length() < 1)
			fn = "--";
		String ln = individualReport.getDisplayData().get("LAST_NAME");
		if (ln == null || ln.length() < 1)
			ln = "--";
		individualReport.addDisplayData("DISPLAY_NAME", fn + " " + ln);
		////println "DISPLAY_NAME=" + individualReport.getDisplayData("DISPLAY_NAME");

		//Get the ReportData object for the Raven's Short Form report 
		ReportData rd = individualReport.getReportData().get(ReportingConstants.RC_RAVENS_SF);
		
		//println "RD keys:"
		//for(Iterator<String> itr=individualReport.getReportData().keySet().iterator(); itr.hasNext(); )
		//{
		//	String key = itr.next();
		//	println "  key=" + key;
		//}
		
		// Calc the reference line positions and put them out
		float[] ref = BWUtils.getRefPosArray();
		rd.getScriptedData().put("REF_1", ref[0].toString());
		rd.getScriptedData().put("REF_3", ref[1].toString());
		rd.getScriptedData().put("REF_16", ref[2].toString());
		rd.getScriptedData().put("REF_50", ref[3].toString());
		rd.getScriptedData().put("REF_84", ref[4].toString());
		rd.getScriptedData().put("REF_97", ref[5].toString());
		rd.getScriptedData().put("REF_99", ref[6].toString());
		//println "  Did the ref lines..."

		// Round the percentile
		double pctl = Double.parseDouble(rd.getScoreData("RAVENSSF_SP_PCTL"));
		int pOut = Math.floor(pctl + 0.5)
		if (pOut < 1)
			pOut = 1;
		else if (pOut > 99)
			pOut = 99;
		rd.getScriptedData().put("RAVENSSF_SP_PCTL", pOut.toString());
		//println "  Did the SP pctl..."
		
		// Get the norm (SP only)
		double mean = Double.parseDouble(rd.getScoreData("RAVENSSF_SP_SCALE_MEAN"));
		double stDev = Double.parseDouble(rd.getScoreData("RAVENSSF_SP_SCALE_STDEV"));
		Norm spNorm = new Norm(mean,stDev);
		//println "  SP norm - mean=" + spNorm.getMean() + ", SD=" + spNorm.getStdDev();
		
		// Calc the score position
		double score = Double.parseDouble(rd.getScoreData().get("RAVENSSF"));
		float scorPos = BWUtils.calcScorePos(score, spNorm);
		rd.getScriptedData().put("RAVENSSF_POSN", scorPos.toString());

		//println "  Ready to return..."		
		return individualReport;
	}	// End generate()
}