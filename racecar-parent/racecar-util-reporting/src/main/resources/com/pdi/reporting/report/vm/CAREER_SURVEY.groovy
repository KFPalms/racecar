/*
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.reporting.report.vm

import com.pdi.reporting.report.GroupReport
import java.util.HashMap;
import java.util.Map;

import com.pdi.reporting.report.IndividualReport
import java.util.ArrayList;

import javax.swing.text.html.HTMLDocument.Iterator;

import com.pdi.reporting.constants.ReportingConstants;

import com.pdi.scoring.Norm

/**
 * Career Survey -- This groovy script contains code to create the raw scale
 * scores from response data ("score()")and the code required to generate
 * the report data used by the UI when creating a report ("generate()").
 * 
 * @author gmyers
 * @author mbpanichi
 */
public class CAREER_SURVEY
{
	//
	// data.
	//
	private IndividualReport individualReport;


//	// ------------- Data used in scoring -------------
//	
//	// Norms used in scoring
//	private Norm CAREER_GOALS_NORM = new Norm(3.5145, 0.62377);
//	private CAREER_DRIVERS_NORM = new Norm(2.7488,0.88267);
//	private LEARNING_ORIENTATION_NORM = new Norm(3.1826, 0.55244);
//	private EXPERIENCE_ORIENTATION_NORM	= new Norm(3.6382,0.44473);

/*
 * NOTE: We reversed the calculations on the questions from 5,4,3,2,1 to 1,2,3,4,5 to match Checkbox data
 * This needs to be validated for future instruments
 */
	//	
	// methods.
	//

	/*
	 * score - Rolls answer data up in proper manner to create raw scale scores.
	 * Career Survey is unique in that it calculates the percentiles from hard-coded norms
	 * 
	 * @returns An IndividualReport object
	 */
	public IndividualReport score()
	{
		//get questions out
		// CS - 18 questions
		//
		HashMap questions = new HashMap(); 
		// Get the questions, translating the score sting values to floats
		individualReport.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getRawData().each{ item ->
			Float val;
			
			if (item.getValue() == null || item.getValue().length() == 0)
			{
				val = new Float(0.0);
				questions.put(item.getKey(), val);
				
			} else {
				try{
							
							val = new Float(item.getValue());
							questions.put(item.getKey(), val);
						
					}catch(java.lang.NumberFormatException nfe){
					
						// just put in a zero for safety's sake
						val = new Float(0.0);
						questions.put(item.getKey(), val);
					}
			}
			
		}
		//println "Questions: " + questions;
		
		/***************************************************************
		 1) Calculate 'career goals' scale score		 
		 ***************************************************************/
		float score = 0.0;
		float tempScore = 0.0;
		float cumScore = 0.0;
		
		score = questions.get("Q1");
		if(score == 1)
		{
			tempScore += 1;
		}
		
		score = ((Float)questions.get("Q2")).floatValue();
		if(score == 2)
		{
			tempScore += 1;
		}
		
		if(tempScore == 2)
		{
			cumScore = 5;
		}else if (tempScore == 1)
		{
			cumScore = 3;
		}

		score = questions.get("Q3");		
		switch (score)
		{
			case 5:  cumScore += 5; break;
			case 4:  cumScore += 4; break;
			case 3:  cumScore += 3; break;
			case 2:  cumScore += 2; break;
			case 1:  cumScore += 1; break;
			default:  break; 
		}
		score = questions.get("Q4");		
		switch (score)
		{
			case 5:  cumScore += 5; break;
			case 4:  cumScore += 4; break;
			case 3:  cumScore += 3; break;
			case 2:  cumScore += 2; break;
			case 1:  cumScore += 1; break;
			default:  break; 
		}
		score = questions.get("Q5");		
		switch (score)
		{
			case 5:  cumScore += 5; break;
			case 4:  cumScore += 4; break;
			case 3:  cumScore += 3; break;
			case 2:  cumScore += 2; break;
			case 1:  cumScore += 1; break;
			default:  break; 
		}
		
		float careerGoalsScale = cumScore/4;
		individualReport.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().put("CAREER_GOALS", careerGoalsScale.toString());
//		individualReport.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().put("CAREER_GOALS_RAW", careerGoalsScale.toString());
//		
//		// Calc the percentile and save it
//		double careerGoalsZscore = CAREER_GOALS_NORM.calcZScore(careerGoalsScale);
//		double careerGoalsPctl = CAREER_GOALS_NORM.calcDoublePctlFromZ(careerGoalsZscore);
//
//	   individualReport.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().put("CAREER_GOALS", careerGoalsPctl.toString());
//	   //println "CG Raw score=" + careerGoalsScale;
//	   //println "CG Pctl score=" + careerGoalsPctl;
	
	
	/***************************************************************
		 2) Calculate 'career drivers' scale score		 
		 ***************************************************************/
		score = 0.0;
		tempScore = 0.0;
		cumScore = 0.0;
		
		// Only rankings are used at this point (6b and 7b);
		// ratings (6a and 7a) not yet used

		score = questions.get("Q_6b_3");		
		switch (score)
			{
			case 1:  cumScore += 1; break;
			case 2:  cumScore += 1; break;
			case 3:  cumScore += 1; break;
			case 4:  cumScore += 1; break;
			case 5:  cumScore += 1; break;
			case 6:  cumScore += 0; break;
			case 7:  cumScore += 0; break;
			case 8:  cumScore += 0; break;
			case 9:  cumScore += 0; break;
			default:  break; 
		}

		score = questions.get("Q_6b_4");		
		switch (score)
		{
			case 1:  cumScore += 1; break;
			case 2:  cumScore += 1; break;
			case 3:  cumScore += 1; break;
			case 4:  cumScore += 1; break;
			case 5:  cumScore += 1; break;
			case 6:  cumScore += 0; break;
			case 7:  cumScore += 0; break;
			case 8:  cumScore += 0; break;
			case 9:  cumScore += 0; break;
			default:  break; 
		}

		score = questions.get("Q_6b_6");		
		switch (score)
		{
			case 1:  cumScore += 1; break;
			case 2:  cumScore += 1; break;
			case 3:  cumScore += 1; break;
			case 4:  cumScore += 1; break;
			case 5:  cumScore += 1; break;
			case 6:  cumScore += 0; break;
			case 7:  cumScore += 0; break;
			case 8:  cumScore += 0; break;
			case 9:  cumScore += 0; break;
			default:  break; 
		}
		
		score = questions.get("Q_6b_7");		
		switch (score)
		{
			case 1:  cumScore += 1; break;
			case 2:  cumScore += 1; break;
			case 3:  cumScore += 1; break;
			case 4:  cumScore += 1; break;
			case 5:  cumScore += 1; break;
			case 6:  cumScore += 0; break;
			case 7:  cumScore += 0; break;
			case 8:  cumScore += 0; break;
			case 9:  cumScore += 0; break;
			default:  break; 
		}
		
		score = questions.get("Q_6b_9");		
		switch (score)
		{
			case 1:  cumScore += 1; break;
			case 2:  cumScore += 1; break;
			case 3:  cumScore += 1; break;
			case 4:  cumScore += 1; break;
			case 5:  cumScore += 1; break;
			case 6:  cumScore += 0; break;
			case 7:  cumScore += 0; break;
			case 8:  cumScore += 0; break;
			case 9:  cumScore += 0; break;
			default:  break; 
		}
		
		score = questions.get("Q_7b_1");		
		switch (score)
		{
			case 1:  cumScore += 1; break;
			case 2:  cumScore += 1; break;
			case 3:  cumScore += 1; break;
			case 4:  cumScore += 1; break;
			case 5:  cumScore += 1; break;
			case 6:  cumScore += 0; break;
			case 7:  cumScore += 0; break;
			case 8:  cumScore += 0; break;
			case 9:  cumScore += 0; break;
			default:  break; 
		}
		
		score = questions.get("Q_7b_4");		
		switch (score)
		{
			case 1:  cumScore += 1; break;
			case 2:  cumScore += 1; break;
			case 3:  cumScore += 1; break;
			case 4:  cumScore += 1; break;
			case 5:  cumScore += 1; break;
			case 6:  cumScore += 0; break;
			case 7:  cumScore += 0; break;
			case 8:  cumScore += 0; break;
			case 9:  cumScore += 0; break;
			default:  break; 
		}
		
		score = questions.get("Q_7b_5");		
		switch (score)
		{
			case 1:  cumScore += 1; break;
			case 2:  cumScore += 1; break;
			case 3:  cumScore += 1; break;
			case 4:  cumScore += 1; break;
			case 5:  cumScore += 1; break;
			case 6:  cumScore += 0; break;
			case 7:  cumScore += 0; break;
			case 8:  cumScore += 0; break;
			case 9:  cumScore += 0; break;
			default:  break; 
		}
		
		score = questions.get("Q_7b_7");		
		switch (score)
		{
			case 1:  cumScore += 1; break;
			case 2:  cumScore += 1; break;
			case 3:  cumScore += 1; break;
			case 4:  cumScore += 1; break;
			case 5:  cumScore += 1; break;
			case 6:  cumScore += 0; break;
			case 7:  cumScore += 0; break;
			case 8:  cumScore += 0; break;
			case 9:  cumScore += 0; break;
			default:  break; 
		}
		
		score = questions.get("Q_7b_9");		
		switch (score)
		{
			case 1:  cumScore += 1; break;
			case 2:  cumScore += 1; break;
			case 3:  cumScore += 1; break;
			case 4:  cumScore += 1; break;
			case 5:  cumScore += 1; break;
			case 6:  cumScore += 0; break;
			case 7:  cumScore += 0; break;
			case 8:  cumScore += 0; break;
			case 9:  cumScore += 0; break;
			default:  break; 
		}
		
		score = questions.get("Q_8_4");
		float score2 = questions.get("Q_8_2");
	  	if(score == 1)
		{
			switch (score2)
			{
				case 1:  cumScore += 0; break;
				case 2:  cumScore += 5; break;
				case 3:  cumScore += 4; break;
				case 4:  cumScore += 2; break;
				case 5:  cumScore += 0; break;
				default:  break; 
			}
		} else if(score == 2)
		{
			switch (score2)
			{
				case 1:  cumScore += 4; break;
				case 2:  cumScore += 0; break;
				case 3:  cumScore += 3; break;
				case 4:  cumScore += 1; break;
				case 5:  cumScore += 0; break;
				default:  break; 
			}	
		}

	  	tempScore = cumScore/3;
		
		individualReport.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().put("CAREER_DRIVERS", tempScore.toString());
//		individualReport.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().put("CAREER_DRIVERS_RAW", tempScore.toString());
//		
//		//{ Calc Career Drivers z-score - Hard coded GP norm of 10/12/06  }
//		//careerDriversNorm = new Norm(2.7488,0.88267);
//		double careerDriversZscore = CAREER_DRIVERS_NORM.calcZScore(tempScore);
//		double careerDriversPctl = CAREER_DRIVERS_NORM.calcDoublePctlFromZ(careerDriversZscore);	
//	
//		individualReport.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().put("CAREER_DRIVERS", careerDriversPctl.toString());
//		//System.out.println("Career Drivers Scale = " + tempScore);
//		//System.out.println("Career Drivers Normed = " + careerDriversPctl.toString());
		
		
		/***************************************************************
		 3) Calculate 'learning orientation' scale score 
		 ***************************************************************/
		score = 0.0;
		tempScore = 0.0;
		cumScore = 0.0;		
	
		for (int i = 9; i <= 15; i++)
		{	
			String qString = "H_" + i;
			score = questions.get(qString);		
			switch (score)
			{
				case 5:  cumScore += 5; break;
				case 4:  cumScore += 4; break;
				case 3:  cumScore += 3; break;
				case 2:  cumScore += 2; break;
				case 1:  cumScore += 1; break;
				default:  break; 
			}		
		}

		tempScore = cumScore/7;
		individualReport.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().put("LEARNING_ORIENT", tempScore.toString());
//		individualReport.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().put("LEARNING_ORIENT_RAW", tempScore.toString());
//		
//		//{ Calc Learning Orientation z-score - Hard coded GP norm of 10/12/06 }
//		// LearningOrientationNorm = new Norm(3.1826, 0.55244);
//		double learnOrientZscore = LEARNING_ORIENTATION_NORM.calcZScore(tempScore);
//		double learnOrientPctl = LEARNING_ORIENTATION_NORM.calcDoublePctlFromZ(learnOrientZscore);	
//
//		individualReport.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().put("LEARNING_ORIENT", learnOrientPctl.toString());
//		//println "LO Raw score=" + tempScore;
//		//println "LO Pctl score=" + learnOrientPctl;
		
		/***************************************************************
		4) Calculate 'experience orientation' scale score
		 ***************************************************************/
		score = 0.0;
		tempScore = 0.0;
		cumScore = 0.0;		
		float cumChal = 0.0;   //{ Cumulative Challenges ratings v1}
		float cumAssign = 0.0; //{ Cumulative Assignment rating v2}
		float totalAv = 0.0;   //{ Total of averages v3}	

		for (int i = 1; i <= 7; i++)
		{	
			String qString = "Q_16a_" + i;
			score = questions.get(qString)
//println "key=" + qString + ", score=" + score;
			switch (score)
			{
				case 5:  cumChal += 5; break;
				case 4:  cumChal += 4; break;
				case 3:  cumChal += 3; break;
				case 2:  cumChal += 2; break;
				case 1:  cumChal += 1; break;
				default:  break; 
			}
		}
		totalAv+=cumChal/7;
//println "cumChal=" + cumChal + ", totalAv=" + totalAv;


		for (int i = 1; i <= 7; i++)
		{	
			String qString = "Q_17a_" + i;
			score = questions.get(qString);		
//println "key=" + qString + ", score=" + score;
			switch (score)
			{
				case 5:  cumAssign += 5; break;
				case 4:  cumAssign += 4; break;
				case 3:  cumAssign += 3; break;
				case 2:  cumAssign += 2; break;
				case 1:  cumAssign += 1; break;
				default:  break; 
			}
		}
		totalAv+=cumAssign/7;
		//println "cumAssign=" + cumAssign + ", totalAv=" + totalAv;
		
		float experOrientScore = totalAv/2;
		individualReport.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().put("EXPERIENCE_ORIENT", experOrientScore.toString());
//		individualReport.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().put("EXPERIENCE_ORIENT_RAW", experOrientScore.toString());
//		
//		//{ Calc Experience Orientation z-score - Hard coded GP norm of 10/12/06  }
//		//experienceOrientationNorm = new Norm(3.6382,0.44473);
//		double experOrientZscore = EXPERIENCE_ORIENTATION_NORM.calcZScore(experOrientScore);
//		double experOrientPctl = EXPERIENCE_ORIENTATION_NORM.calcDoublePctlFromZ(experOrientZscore);	
//
//		individualReport.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().put("EXPERIENCE_ORIENT", experOrientPctl.toString());
//		//println "EO Raw score=" + experOrientScore;
//		//println "EO Pctl score=" + experOrientPctl;


		//finally return the individual report
		return individualReport;
	}	// End score()


	//*************************************************************************************//
	

	/*
	 * generate - Formats data for use by the UI to generate the report PDF
	 * 
	 * @returns An IndividualReport object
	 */
	public IndividualReport generate()
	{
		println "Career Survey 'generate' code not implemented at this time!!!" 
		// Nothing done here... No report generated yet
		return new IndividualReport();
	}	// End generate()

}