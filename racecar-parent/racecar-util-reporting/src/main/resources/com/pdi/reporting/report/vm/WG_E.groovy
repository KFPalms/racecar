/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.reporting.report.vm;

import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;
import com.pdi.reporting.report.utils.BWUtils;
import com.pdi.scoring.Norm;

////used for debug
//import java.util.Iterator;

/**
 * Watson Glaser II Form E (Global English)
 * Code to create raw scale scores ("score()") and to
 * set up the required data for a report ("generate()")
 * 
 * @author kbeukelm
 */
class WG_E
{
	private IndividualReport individualReport;
	
	// Scoring key for the Watson-Glaser II Form E
	private static final int[] WGE_KEY = new int[41];
	static
	{
		// Note that the first element ([0]) is ignored;
		// the index matches exactly the question number
		// the length is 1 more than the # of questions
		WGE_KEY[1]  = 5;
		WGE_KEY[2]  = 4;
		WGE_KEY[3]  = 5;
		WGE_KEY[4]  = 4;
		WGE_KEY[5]  = 4;
		WGE_KEY[6]  = 2;
		WGE_KEY[7]  = 1;
		WGE_KEY[8]  = 2;
		WGE_KEY[9]  = 2;
		WGE_KEY[10] = 1;
		WGE_KEY[11] = 2;
		WGE_KEY[12] = 1;
		WGE_KEY[13] = 2;
		WGE_KEY[14] = 1;
		WGE_KEY[15] = 2;
		WGE_KEY[16] = 2;
		WGE_KEY[17] = 2;
		WGE_KEY[18] = 1;
		WGE_KEY[19] = 2;
		WGE_KEY[20] = 2;
		WGE_KEY[21] = 2;
		WGE_KEY[22] = 1;
		WGE_KEY[23] = 2;
		WGE_KEY[24] = 2;
		WGE_KEY[25] = 2;
		WGE_KEY[26] = 2;
		WGE_KEY[27] = 1;
		WGE_KEY[28] = 2;
		WGE_KEY[29] = 2;
		WGE_KEY[30] = 1;
		WGE_KEY[31] = 1;
		WGE_KEY[32] = 2;
		WGE_KEY[33] = 1;
		WGE_KEY[34] = 1;
		WGE_KEY[35] = 2;
		WGE_KEY[36] = 1;
		WGE_KEY[37] = 1;
		WGE_KEY[38] = 2;
		WGE_KEY[39] = 2;
		WGE_KEY[40] = 1;
	};

	// Index values
	private static int CT = 0;	// Critical Thinking
	private static int RA = 1;	// Recognize Assumptions
	private static int EA = 2;	// Evaluate Arguments
	private static int DC = 3;	// Draw Conclusions

	private static final boolean[][] WGE_SCALES = new boolean[41][4]
	static
	{
		// Entry 0 is bogus - allows answers 1-40
		WGE_SCALES[0][CT]  = false;	WGE_SCALES[0][RA]  = false;	WGE_SCALES[0][EA]  = false;	WGE_SCALES[0][DC]  = false;
		WGE_SCALES[1][CT]  = true;	WGE_SCALES[1][RA]  = false;	WGE_SCALES[1][EA]  = false;	WGE_SCALES[1][DC]  = true;
		WGE_SCALES[2][CT]  = true;	WGE_SCALES[2][RA]  = false;	WGE_SCALES[2][EA]  = false;	WGE_SCALES[2][DC]  = true;
		WGE_SCALES[3][CT]  = true;	WGE_SCALES[3][RA]  = false;	WGE_SCALES[3][EA]  = false;	WGE_SCALES[3][DC]  = true;
		WGE_SCALES[4][CT]  = true;	WGE_SCALES[4][RA]  = false;	WGE_SCALES[4][EA]  = false;	WGE_SCALES[4][DC]  = true;
		WGE_SCALES[5][CT]  = true;	WGE_SCALES[5][RA]  = false;	WGE_SCALES[5][EA]  = false;	WGE_SCALES[5][DC]  = true;
		WGE_SCALES[6][CT]  = true;	WGE_SCALES[6][RA]  = true;	WGE_SCALES[6][EA]  = false;	WGE_SCALES[6][DC]  = false;
		WGE_SCALES[7][CT]  = true;	WGE_SCALES[7][RA]  = true;	WGE_SCALES[7][EA]  = false;	WGE_SCALES[7][DC]  = false;
		WGE_SCALES[8][CT]  = true;	WGE_SCALES[8][RA]  = true;	WGE_SCALES[8][EA]  = false;	WGE_SCALES[8][DC]  = false;
		WGE_SCALES[9][CT]  = true;	WGE_SCALES[9][RA]  = true;	WGE_SCALES[9][EA]  = false;	WGE_SCALES[9][DC]  = false;
		WGE_SCALES[10][CT] = true;	WGE_SCALES[10][RA] = true;	WGE_SCALES[10][EA] = false;	WGE_SCALES[10][DC] = false;
		WGE_SCALES[11][CT] = true;	WGE_SCALES[11][RA] = true;	WGE_SCALES[11][EA] = false;	WGE_SCALES[11][DC] = false;
		WGE_SCALES[12][CT] = true;	WGE_SCALES[12][RA] = true;	WGE_SCALES[12][EA] = false;	WGE_SCALES[12][DC] = false;
		WGE_SCALES[13][CT] = true;	WGE_SCALES[13][RA] = true;	WGE_SCALES[13][EA] = false;	WGE_SCALES[13][DC] = false;
		WGE_SCALES[14][CT] = true;	WGE_SCALES[14][RA] = true;	WGE_SCALES[14][EA] = false;	WGE_SCALES[14][DC] = false;
		WGE_SCALES[15][CT] = true;	WGE_SCALES[15][RA] = true;	WGE_SCALES[15][EA] = false;	WGE_SCALES[15][DC] = false;
		WGE_SCALES[16][CT] = true;	WGE_SCALES[16][RA] = true;	WGE_SCALES[16][EA] = false;	WGE_SCALES[16][DC] = false;
		WGE_SCALES[17][CT] = true;	WGE_SCALES[17][RA] = true;	WGE_SCALES[17][EA] = false;	WGE_SCALES[17][DC] = false;
		WGE_SCALES[18][CT] = true;	WGE_SCALES[18][RA] = false;	WGE_SCALES[18][EA] = false;	WGE_SCALES[18][DC] = true;
		WGE_SCALES[19][CT] = true;	WGE_SCALES[19][RA] = false;	WGE_SCALES[19][EA] = false;	WGE_SCALES[19][DC] = true;
		WGE_SCALES[20][CT] = true;	WGE_SCALES[20][RA] = false;	WGE_SCALES[20][EA] = false;	WGE_SCALES[20][DC] = true;
		WGE_SCALES[21][CT] = true;	WGE_SCALES[21][RA] = false;	WGE_SCALES[21][EA] = false;	WGE_SCALES[21][DC] = true;
		WGE_SCALES[22][CT] = true;	WGE_SCALES[22][RA] = false;	WGE_SCALES[22][EA] = false;	WGE_SCALES[22][DC] = true;
		WGE_SCALES[23][CT] = true;	WGE_SCALES[23][RA] = false;	WGE_SCALES[23][EA] = false;	WGE_SCALES[23][DC] = true;
		WGE_SCALES[24][CT] = true;	WGE_SCALES[24][RA] = false;	WGE_SCALES[24][EA] = false;	WGE_SCALES[24][DC] = true;
		WGE_SCALES[25][CT] = true;	WGE_SCALES[25][RA] = false;	WGE_SCALES[25][EA] = false;	WGE_SCALES[25][DC] = true;
		WGE_SCALES[26][CT] = true;	WGE_SCALES[26][RA] = false;	WGE_SCALES[26][EA] = false;	WGE_SCALES[26][DC] = true;
		WGE_SCALES[27][CT] = true;	WGE_SCALES[27][RA] = false;	WGE_SCALES[27][EA] = false;	WGE_SCALES[27][DC] = true;
		WGE_SCALES[28][CT] = true;	WGE_SCALES[28][RA] = false;	WGE_SCALES[28][EA] = false;	WGE_SCALES[28][DC] = true;
		WGE_SCALES[29][CT] = true;	WGE_SCALES[29][RA] = false;	WGE_SCALES[29][EA] = true;	WGE_SCALES[29][DC] = false;
		WGE_SCALES[30][CT] = true;	WGE_SCALES[30][RA] = false;	WGE_SCALES[30][EA] = true;	WGE_SCALES[30][DC] = false;
		WGE_SCALES[31][CT] = true;	WGE_SCALES[31][RA] = false;	WGE_SCALES[31][EA] = true;	WGE_SCALES[31][DC] = false;
		WGE_SCALES[32][CT] = true;	WGE_SCALES[32][RA] = false;	WGE_SCALES[32][EA] = true;	WGE_SCALES[32][DC] = false;
		WGE_SCALES[33][CT] = true;	WGE_SCALES[33][RA] = false;	WGE_SCALES[33][EA] = true;	WGE_SCALES[33][DC] = false;
		WGE_SCALES[34][CT] = true;	WGE_SCALES[34][RA] = false;	WGE_SCALES[34][EA] = true;	WGE_SCALES[34][DC] = false;
		WGE_SCALES[35][CT] = true;	WGE_SCALES[35][RA] = false;	WGE_SCALES[35][EA] = true;	WGE_SCALES[35][DC] = false;
		WGE_SCALES[36][CT] = true;	WGE_SCALES[36][RA] = false;	WGE_SCALES[36][EA] = true;	WGE_SCALES[36][DC] = false;
		WGE_SCALES[37][CT] = true;	WGE_SCALES[37][RA] = false;	WGE_SCALES[37][EA] = true;	WGE_SCALES[37][DC] = false;
		WGE_SCALES[38][CT] = true;	WGE_SCALES[38][RA] = false;	WGE_SCALES[38][EA] = true;	WGE_SCALES[38][DC] = false;
		WGE_SCALES[39][CT] = true;	WGE_SCALES[39][RA] = false;	WGE_SCALES[39][EA] = true;	WGE_SCALES[39][DC] = false;
		WGE_SCALES[40][CT] = true;	WGE_SCALES[40][RA] = false;	WGE_SCALES[40][EA] = true;	WGE_SCALES[40][DC] = false;
	};


	/*
	 * score - Rolls answer data up in proper manner to create raw scale scores
	 */
	public IndividualReport score()
	{
		int ct = 0;
		int ra = 0;
		int ea = 0;
		int dc = 0;
				
		//get questions out
		for (int i=1; i < WGE_KEY.length; i++)
		{
			String ans = individualReport.getReportData().get(ReportingConstants.RC_WG_E).getRawData().get("question_" + i);
			//println "i=" + i + ", ans=" + ans;

			if (ans != null && !ans.equalsIgnoreCase("") && !ans.equalsIgnoreCase("null") && Integer.parseInt(ans) == WGE_KEY[i])
			{
				// We have a correct answer... bump appropriate counters
				if (WGE_SCALES[i][CT])
					ct++;
				if (WGE_SCALES[i][RA])
					ra++;
				if (WGE_SCALES[i][EA])
					ea++;
				if (WGE_SCALES[i][DC])
					dc++;
			}
		}
		
		// there are 4 scale scores for the WG_E....
		individualReport.getReportData().get(ReportingConstants.RC_WG_E).getScoreData().put("WGE_CT", ct.toString());
		individualReport.getReportData().get(ReportingConstants.RC_WG_E).getScoreData().put("WGE_RA", ra.toString());
		individualReport.getReportData().get(ReportingConstants.RC_WG_E).getScoreData().put("WGE_EA", ea.toString());
		individualReport.getReportData().get(ReportingConstants.RC_WG_E).getScoreData().put("WGE_DC", dc.toString());

		//finally return the individual report
		return individualReport;
	}	// End score()
	
	
	//*************************************************************************************//
	
	
	/*
	 * generate - Formats data for use by the UI to generate the report PDF
	 */
	public IndividualReport generate()
	{
/*		
		////	System.out.println("WG_E not yet implemented");
		// put out the display name
		//String fn = individualReport.getDisplayData().get("FIRST_NAME");
		//if (fn == null || fn.length() < 1)
		//	fn = "--";
		//String ln = individualReport.getDisplayData().get("LAST_NAME");
		//if (ln == null || ln.length() < 1)
		//	ln = "--";
		//individualReport.addDisplayData("DISPLAY_NAME", fn + " " + ln);
		////println "DISPLAY_NAME=" + individualReport.getDisplayData("DISPLAY_NAME");
		
		//Get the ReportData object for the Watson-Glaser Form E (Watson-Glaser II) report 
		ReportData rd = individualReport.getReportData().get(ReportingConstants.RC_WG_E);
		if(rd == null)
		{
			individualReport.getScriptedData().put("STATUS", "NOT_FINISHED");
			individualReport.getScriptedData().put("ERROR", "Could not find Watson-Glaser II data");
			return individualReport;
		}
		
		// Calc the reference line positions and put them out
		float[] ref = BWUtils.getRefPosArray();
		rd.getScriptedData().put("REF_1", ref[0].toString());
		rd.getScriptedData().put("REF_3", ref[1].toString());
		rd.getScriptedData().put("REF_16", ref[2].toString());
		rd.getScriptedData().put("REF_50", ref[3].toString());
		rd.getScriptedData().put("REF_84", ref[4].toString());
		rd.getScriptedData().put("REF_97", ref[5].toString());
		rd.getScriptedData().put("REF_99", ref[6].toString());
		//println "  Did the ref lines..."
		System.out.println("Ref 50 = " + ref[3].toString());
		
		// Round the percentiles
		if(rd.getScoreData("WGE_CT_GP_PCTL") == null)
		{
			individualReport.getScriptedData().put("STATUS", "NOT_FINISHED");
			individualReport.getScriptedData().put("ERROR", "No GP percentile");
			return individualReport; 
		}
		double pctl = Double.parseDouble(rd.getScoreData("WGE_CT_GP_PCTL"));
		int pOut = Math.floor(pctl + 0.5)
		if (pOut < 1)
			pOut = 1;
		else if (pOut > 99)
			pOut = 99;
		rd.getScriptedData().put("WGE_GP_PCTL", pOut.toString());
		
		if(rd.getScoreData("WGE_CT_SP_PCTL") == null)
		{
			individualReport.getScriptedData().put("STATUS", "NOT_FINISHED");
			individualReport.getScriptedData().put("ERROR", "No SP percentile");
			return individualReport; 
		}
		pctl = Double.parseDouble(rd.getScoreData("WGE_CT_SP_PCTL"));
		pOut = Math.floor(pctl + 0.5)
		if (pOut < 1)
			pOut = 1;
		else if (pOut > 99)
			pOut = 99;
		rd.getScriptedData().put("WGE_SP_PCTL", pOut.toString());
		//println "  Did the SP pctl..."
		
		// Get the norms
		
		if(rd.getScoreData("WGE_CT_GP_SCALE_MEAN") == null) {
			individualReport.getScriptedData().put("STATUS", "NOT_FINISHED");
			individualReport.getScriptedData().put("ERROR", "No GP norm data");
			return individualReport; 
		}
		double mean = Double.parseDouble(rd.getScoreData("WGE_CT_GP_SCALE_MEAN"));
		double stDev = Double.parseDouble(rd.getScoreData("WGE_CT_GP_SCALE_STDEV"));
		Norm gpNorm = new Norm(mean,stDev);
		//println "  GP norm - mean=" + spNorm.getMean() + ", SD=" + spNorm.getStdDev();
		
		if(rd.getScoreData("WGE_CT_SP_SCALE_MEAN") == null)
		{
			individualReport.getScriptedData().put("STATUS", "NOT_FINISHED");
			individualReport.getScriptedData().put("ERROR", "No SP norm data");
			return individualReport; 
		}
		mean = Double.parseDouble(rd.getScoreData("WGE_CT_SP_SCALE_MEAN"));
		stDev = Double.parseDouble(rd.getScoreData("WGE_CT_SP_SCALE_STDEV"));
		Norm spNorm = new Norm(mean,stDev);
		//println "  SP norm - mean=" + spNorm.getMean() + ", SD=" + spNorm.getStdDev();
		
		// Calc the score position
		
		if(rd.getScoreData("WGE_CT") == null)
		{
			individualReport.getScriptedData().put("STATUS", "NOT_FINISHED");
			individualReport.getScriptedData().put("ERROR", "No score data for Ravens");
			return individualReport; 
		}
		
		double score = Double.parseDouble(rd.getScoreData().get("WGE_CT"));
		float scorPos = BWUtils.calcScorePos(score, gpNorm);
		rd.getScriptedData().put("WGE_POSN", scorPos.toString());
		
		// Calc the cut-point positions
		float[] csPos = BWUtils.getCutPosArray(gpNorm, spNorm);
		rd.getScriptedData().put("WGE_CP_3", csPos[0].toString());
		rd.getScriptedData().put("WGE_CP_11", csPos[1].toString());
		rd.getScriptedData().put("WGE_CP_23", csPos[2].toString());
		rd.getScriptedData().put("WGE_CP_40", csPos[3].toString());
		rd.getScriptedData().put("WGE_CP_60", csPos[4].toString());
		rd.getScriptedData().put("WGE_CP_77", csPos[5].toString());
		rd.getScriptedData().put("WGE_CP_89", csPos[6].toString());
		rd.getScriptedData().put("WGE_CP_97", csPos[7].toString());

		//System.out.println("Cut Points for " + scaleId + ":");
		//for(int r= 0; r < csPos.length; r++)
		//{
		//	System.out.println("Idx[" + r + "]=" + csPos[r])
		//}
		
		//println "  Ready to return..."		
		individualReport.scriptedData.put("STATUS", "FINISHED");
*/		
		return individualReport;
	}	// End generate()
	
}
