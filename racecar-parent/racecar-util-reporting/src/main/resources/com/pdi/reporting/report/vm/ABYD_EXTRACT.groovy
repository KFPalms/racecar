/*
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.reporting.report.vm 

import java.util.ArrayList;
import com.pdi.reporting.report.helpers.AbyDExtractFormatHelper;

import com.pdi.reporting.report.ExtractItem;
import com.pdi.reporting.report.GroupReport;


public class ABYD_EXTRACT
{ 
	private GroupReport groupReport;


	/*
	 * generate  -- deprecated -- moved to POI
	 */
	public GroupReport generate()
	{
		//println "--->Start the data formatting<---";
		
		// First set up the label order...
		ArrayList<ExtractItem> lst = AbyDExtractFormatHelper.setUpOrder(groupReport);
		
		// ...then generate the data strings
		//println "Gen the output strings";
		groupReport = AbyDExtractFormatHelper.genHtmlString(lst, groupReport);
		
		//println "Data formating complete";
		return groupReport;
	}	
}