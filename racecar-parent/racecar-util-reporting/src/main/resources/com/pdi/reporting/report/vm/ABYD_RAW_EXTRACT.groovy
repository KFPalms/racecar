/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.reporting.report.vm

import java.util.ArrayList;
import com.pdi.reporting.report.helpers.AbyDRawExtractFormatHelper;

import com.pdi.reporting.report.ExtractItem;
import com.pdi.reporting.report.GroupReport;


public class ABYD_RAW_EXTRACT
{
   private GroupReport groupReport;


   /*
	* generate -- deprecated -- moved to POI
	*/
   public GroupReport generate()
   {
	   println "--->Start the data formatting<---";
	   
	   // First set up the label order...
	   ArrayList<ExtractItem> lst = AbyDRawExtractFormatHelper.setUpOrder(groupReport);
	   
	   // ...then generate the data strings
	   println "Gen the output strings";
	   groupReport = AbyDRawExtractFormatHelper.genHtmlString(lst, groupReport);
	   
	   println "Data formating complete";
	   return groupReport;
   }
}