/*
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.reporting.report.vm

import java.text.SimpleDateFormat;
import java.util.HashMap;

import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.IndividualReport
import com.pdi.reporting.report.ReportData;
import com.pdi.scoring.Norm;


/**
 * @author gmyers
 */
public class TLT_INDIVIDUAL_SUMMARY_REPORT
{
	private IndividualReport individualReport;


	/*
	 * parse
	 */


	public IndividualReport generate()
	{
		TLT_INDIVIDUAL_SUMMARY_REPORT_SCORING tltScoring = new TLT_INDIVIDUAL_SUMMARY_REPORT_SCORING();
		tltScoring.individualReport = individualReport;
		return tltScoring.generate();
	}
}
