/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.instrument.constants;

public class InstrumentConstants
{
	//Generic instrument codes
	public static String IC_GPI = "GPI";
	public static String IC_LEI = "LEI";
	public static String IC_RAVENS_SF = "RAVENS_SF";
	public static String IC_RAVENS_B = "RAVENS_B";
	public static String IC_CAREER_SURVEY = "CAREER_SURVEY";
	public static String IC_WESMAN = "WESMAN_PCT";
	public static String IC_FIN_EX = "FINEX";
	public static String IC_CHQ = "CHQ";
	public static String IC_WG_E = "WG_E";

	//public static String IC_WATSON_A_SF = "WG_A_SF";	No longer Used

}
