package com.pdi.scoring.ut;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;

import com.pdi.properties.PropertyLoader;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.ReportData;
import com.pdi.scoring.request.ScoreRequest;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

/*
 * Scoring test for LEI
 */
public class ScoreLeiUT extends TestCase
{
	//
	// Static data
	//

	/*
	 * Response data for the LEI scoring test cases
	 */
	private static final HashMap<String, String[]> LEI_RESP = new HashMap<String, String[]>();
	static
   	{
		LEI_RESP.put("Q1",     new String[] {"2", "2", "1", "1", "2"});
		LEI_RESP.put("Q2",     new String[] {"2", "2", "1", "1", "2"});
		LEI_RESP.put("Q3",     new String[] {"1", "2", "2", "2", "2"});
		LEI_RESP.put("Q4",     new String[] {"1", "2", "1", "1", "1"});
		LEI_RESP.put("Q5",     new String[] {"1", "1", "1", "1", "2"});
		LEI_RESP.put("Q6",     new String[] {"2", "1", "1", "2", "4"});
		LEI_RESP.put("Q7",     new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q8",     new String[] {"2", "1", "2", "1", "1"});
		LEI_RESP.put("Q9",     new String[] {"2", "2", "2", "1", "1"});
		LEI_RESP.put("Q10",    new String[] {"3", "2", "2", "2", "2"});
		LEI_RESP.put("Q11",    new String[] {"2", "2", "2", "1", "1"});
		LEI_RESP.put("Q12",    new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q13",    new String[] {"4", "2", "1", "2", "2"});
		LEI_RESP.put("Q14",    new String[] {"4", "2", "1", "4", "4"});
		LEI_RESP.put("Q15",    new String[] {"3", "1", "1", "2", "2"});
		LEI_RESP.put("Q16",    new String[] {"4", "1", "2", "4", "4"});
		LEI_RESP.put("Q17",    new String[] {"4", "1", "1", "2", "1"});
		LEI_RESP.put("Q18",    new String[] {"3", "1", "1", "3", "2"});
		LEI_RESP.put("Q19",    new String[] {"4", "4", "2", "2", "3"});
		LEI_RESP.put("Q20",    new String[] {"2", "4", "3", "2", "4"});
		LEI_RESP.put("Q21",    new String[] {"1", "4", "1", "1", "2"});
		LEI_RESP.put("Q22",    new String[] {"2", "4", "2", "3", "2"});
		LEI_RESP.put("Q23",    new String[] {"1", "4", "2", "1", "2"});
		LEI_RESP.put("Q24",    new String[] {"3", "2", "1", "2", "2"});
		LEI_RESP.put("Q25",    new String[] {"4", "2", "3", "4", "1"});
		LEI_RESP.put("Q26",    new String[] {"4", "4", "2", "2", "2"});
		LEI_RESP.put("Q27",    new String[] {"2", "1", "1", "1", "1"});
		LEI_RESP.put("Q28",    new String[] {"4", "3", "4", "4", "4"});
		LEI_RESP.put("Q29",    new String[] {"4", "4", "3", "3", "3"});
		LEI_RESP.put("Q30",    new String[] {"1", "1", "2", "2", "3"});
		LEI_RESP.put("Q31",    new String[] {"2", "3", "2", "2", "2"});
		LEI_RESP.put("Q32",    new String[] {"2", "4", "3", "3", "4"});
		LEI_RESP.put("Q33",    new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_34_1", new String[] {"2", "3", "3", "2", "3"});
		LEI_RESP.put("Q_34_2", new String[] {"2", "2", "3", "2", "2"});
		LEI_RESP.put("Q_34_3", new String[] {"1", "1", "2", "2", "1"});
		LEI_RESP.put("Q_35_1", new String[] {"2", "2", "3", "1", "3"});
		LEI_RESP.put("Q_35_2", new String[] {"2", "1", "1", "2", "2"});
		LEI_RESP.put("Q_35_3", new String[] {"2", "1", "1", "1", "1"});
		LEI_RESP.put("Q_36_1", new String[] {"3", "2", "2", "2", "1"});
		LEI_RESP.put("Q_36_2", new String[] {"2", "1", "2", "3", "1"});
		LEI_RESP.put("Q_36_3", new String[] {"2", "1", "1", "1", "1"});
		LEI_RESP.put("Q_37_1", new String[] {"1", "2", "1", "1", "2"});
		LEI_RESP.put("Q_37_2", new String[] {"1", "1", "2", "1", "2"});
		LEI_RESP.put("Q_37_3", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_38_1", new String[] {"2", "1", "1", "1", "2"});
		LEI_RESP.put("Q_38_2", new String[] {"1", "1", "2", "2", "2"});
		LEI_RESP.put("Q_38_3", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_39_1", new String[] {"1", "2", "2", "3", "1"});
		LEI_RESP.put("Q_39_2", new String[] {"3", "1", "1", "2", "1"});
		LEI_RESP.put("Q_39_3", new String[] {"2", "1", "1", "1", "1"});
		LEI_RESP.put("Q_40_1", new String[] {"3", "2", "2", "1", "2"});
		LEI_RESP.put("Q_40_2", new String[] {"2", "2", "2", "3", "1"});
		LEI_RESP.put("Q_40_3", new String[] {"2", "2", "1", "1", "1"});
		LEI_RESP.put("Q_41_1", new String[] {"3", "2", "2", "3", "1"});
		LEI_RESP.put("Q_41_2", new String[] {"3", "1", "1", "3", "1"});
		LEI_RESP.put("Q_41_3", new String[] {"2", "1", "1", "1", "1"});
		LEI_RESP.put("Q_42_1", new String[] {"3", "1", "1", "1", "2"});
		LEI_RESP.put("Q_42_2", new String[] {"3", "1", "1", "1", "1"});
		LEI_RESP.put("Q_42_3", new String[] {"2", "1", "1", "1", "1"});
		LEI_RESP.put("Q_43_1", new String[] {"2", "3", "2", "1", "1"});
		LEI_RESP.put("Q_43_2", new String[] {"2",  "", "2", "2", "1"});
		LEI_RESP.put("Q_43_3", new String[] {"2",  "", "1", "1", "1"});
		LEI_RESP.put("Q_44_1", new String[] {"2", "2", "1", "1", "2"});
		LEI_RESP.put("Q_44_2", new String[] {"2", "2", "2", "2", "2"});
		LEI_RESP.put("Q_44_3", new String[] {"1",  "", "1", "1", "1"});
		LEI_RESP.put("Q_45_1", new String[] {"2", "2", "1", "1", "2"});
		LEI_RESP.put("Q_45_2", new String[] {"2", "1", "1", "2", "1"});
		LEI_RESP.put("Q_45_3", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_46_1", new String[] {"2", "1", "1", "1", "1"});
		LEI_RESP.put("Q_46_2", new String[] {"2", "1", "1", "2", "1"});
		LEI_RESP.put("Q_46_3", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_47_1", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_47_2", new String[] {"1", "1", "1", "2", "1"});
		LEI_RESP.put("Q_47_3", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_48_1", new String[] {"2", "3", "1", "1", "1"});
		LEI_RESP.put("Q_48_2", new String[] {"2", "2", "1", "1", "1"});
		LEI_RESP.put("Q_48_3", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_49_1", new String[] {"3", "1", "1", "1", "1"});
		LEI_RESP.put("Q_49_2", new String[] {"3", "1", "1", "2", "1"});
		LEI_RESP.put("Q_49_3", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_50_1", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_50_2", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_50_3", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_51_1", new String[] {"2", "1", "1", "1", "1"});
		LEI_RESP.put("Q_51_2", new String[] {"2", "1", "1", "2", "1"});
		LEI_RESP.put("Q_51_3", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_52_1", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_52_2", new String[] {"1", "1", "1", "2", "1"});
		LEI_RESP.put("Q_52_3", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_53_1", new String[] {"3", "1", "1", "1", "1"});
		LEI_RESP.put("Q_53_2", new String[] {"3", "1", "2", "3", "1"});
		LEI_RESP.put("Q_53_3", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_54_1", new String[] {"2", "1", "2", "2", "1"});
		LEI_RESP.put("Q_54_2", new String[] {"2", "1", "1", "2", "1"});
		LEI_RESP.put("Q_54_3", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_55_1", new String[] {"3", "1", "2", "1", "2"});
		LEI_RESP.put("Q_55_2", new String[] {"2", "1", "3", "2", "1"});
		LEI_RESP.put("Q_55_3", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_56_1", new String[] {"1", "1", "2", "1", "1"});
		LEI_RESP.put("Q_56_2", new String[] {"1", "1", "2", "2", "1"});
		LEI_RESP.put("Q_56_3", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_57_1", new String[] {"3", "2", "2", "1", "1"});
		LEI_RESP.put("Q_57_2", new String[] {"1", "2", "2", "3", "1"});
		LEI_RESP.put("Q_57_3", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_58_1", new String[] { "", "2", "2", "1", "2"});
		LEI_RESP.put("Q_58_2", new String[] { "", "1", "2", "3", "1"});
		LEI_RESP.put("Q_58_3", new String[] { "", "1", "1", "1", "1"});
		LEI_RESP.put("Q_59_1", new String[] {"2", "2", "2", "1", "2"});
		LEI_RESP.put("Q_59_2", new String[] {"2", "1", "1", "2", "1"});
		LEI_RESP.put("Q_59_3", new String[] {"2", "1", "1", "1", "1"});
		LEI_RESP.put("Q_60_1", new String[] {"2", "3", "2", "1", "2"});
		LEI_RESP.put("Q_60_2", new String[] {"2", "1", "3", "2", "1"});
		LEI_RESP.put("Q_60_3", new String[] {"2", "1", "1", "1", "1"});
		LEI_RESP.put("Q_61_1", new String[] {"1", "2", "1", "1", "1"});
		LEI_RESP.put("Q_61_2", new String[] {"2", "2", "1", "2", "1"});
		LEI_RESP.put("Q_61_3", new String[] {"1", "2", "1", "1", "1"});
		LEI_RESP.put("Q_62_1", new String[] {"2", "1", "1", "1", "1"});
		LEI_RESP.put("Q_62_2", new String[] {"2", "1", "1", "1", "1"});
		LEI_RESP.put("Q_62_3", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_63_1", new String[] {"3", "2", "2", "1", "3"});
		LEI_RESP.put("Q_63_2", new String[] {"3", "1", "1", "1", "1"});
		LEI_RESP.put("Q_63_3", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_64_1", new String[] {"2", "1", "1", "1", "1"});
		LEI_RESP.put("Q_64_2", new String[] {"2", "1", "1", "1", "1"});
		LEI_RESP.put("Q_64_3", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_65_1", new String[] {"2", "1", "1", "1", "1"});
		LEI_RESP.put("Q_65_2", new String[] {"2", "1", "1", "1", "1"});
		LEI_RESP.put("Q_65_3", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_66_1", new String[] {"2", "1", "1", "1", "1"});
		LEI_RESP.put("Q_66_2", new String[] {"2", "1", "1", "2", "1"});
		LEI_RESP.put("Q_66_3", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_67_1", new String[] {"2", "1", "1",  "", "2"});
		LEI_RESP.put("Q_67_2", new String[] {"2", "1", "1",  "", "1"});
		LEI_RESP.put("Q_67_3", new String[] {"2", "1", "1",  "", "1"});
		LEI_RESP.put("Q_68_1", new String[] {"2", "3", "2", "1", "1"});
		LEI_RESP.put("Q_68_2", new String[] {"2", "2", "1", "2", "1"});
		LEI_RESP.put("Q_68_3", new String[] {"3", "1", "1", "1", "1"});
		LEI_RESP.put("Q_69_1", new String[] {"2", "2", "2", "1", "1"});
		LEI_RESP.put("Q_69_2", new String[] {"2", "2", "1", "1", "1"});
		LEI_RESP.put("Q_69_3", new String[] {"2", "1", "1", "1", "1"});
		LEI_RESP.put("Q_70_1", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_70_2", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_70_3", new String[] {"2", "1", "1", "1", "1"});
		LEI_RESP.put("Q_71_1", new String[] {"1", "2", "1", "1", "1"});
		LEI_RESP.put("Q_71_2", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_71_3", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_72_1", new String[] {"2", "1", "1", "1", "1"});
		LEI_RESP.put("Q_72_2", new String[] {"2", "1", "1", "3", "1"});
		LEI_RESP.put("Q_72_3", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_73_1", new String[] {"1", "1", "2", "1", "1"});
		LEI_RESP.put("Q_73_2", new String[] {"1", "1", "2", "1", "1"});
		LEI_RESP.put("Q_73_3", new String[] {"2", "1", "1", "1", "1"});
		LEI_RESP.put("Q_74_1", new String[] {"2", "1", "2", "1", "1"});
		LEI_RESP.put("Q_74_2", new String[] {"2", "1", "2", "2", "1"});
		LEI_RESP.put("Q_74_3", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_75_1", new String[] {"2", "1", "1", "1", "1"});
		LEI_RESP.put("Q_75_2", new String[] {"2", "1", "1", "2", "1"});
		LEI_RESP.put("Q_75_3", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_76_1", new String[] {"1", "2", "1", "1", "2"});
		LEI_RESP.put("Q_76_2", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_76_3", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_77_1", new String[] {"1", "1", "2", "1", "1"});
		LEI_RESP.put("Q_77_2", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_77_3", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_78_1", new String[] {"1", "1", "2", "1", "1"});
		LEI_RESP.put("Q_78_2", new String[] {"1", "1", "2", "1", "1"});
		LEI_RESP.put("Q_78_3", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q_79_1", new String[] {"3", "3", "3", "2", "2"});
		LEI_RESP.put("Q_79_2", new String[] {"3", "2", "1", "1", "1"});
		LEI_RESP.put("Q_79_3", new String[] {"2", "1", "1", "1", "1"});
		LEI_RESP.put("Q_80_1", new String[] {"2", "1", "2", "1", "1"});
		LEI_RESP.put("Q_80_2", new String[] {"2", "1", "2", "1", "1"});
		LEI_RESP.put("Q_80_3", new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q81",    new String[] {"4", "2", "2", "1", "4"});
		LEI_RESP.put("Q82",    new String[] {"4", "1", "2", "2", "2"});
		LEI_RESP.put("Q83",    new String[] {"4", "1", "2", "2", "1"});
		LEI_RESP.put("Q84",    new String[] {"4", "2", "3", "4", "2"});
		LEI_RESP.put("Q85",    new String[] {"1", "1", "2", "1", "2"});
		LEI_RESP.put("Q86",    new String[] {"4", "3", "4", "3", "3"});
		LEI_RESP.put("Q87",    new String[] {"3", "4", "1", "4", "4"});
		LEI_RESP.put("Q88",    new String[] {"3", "4", "1", "1", "1"});
		LEI_RESP.put("Q89",    new String[] {"1", "1", "1", "1", "4"});
		LEI_RESP.put("Q90",    new String[] {"1", "1", "1", "4", "1"});
		LEI_RESP.put("Q91",    new String[] {"1", "1", "2", "1", "1"});
		LEI_RESP.put("Q92",    new String[] {"1", "1", "1", "4", "1"});
		LEI_RESP.put("Q93",    new String[] {"2", "2", "4", "1", "2"});
		LEI_RESP.put("Q94",    new String[] {"1", "1", "1", "2", "1"});
		LEI_RESP.put("Q95",    new String[] {"4", "4", "2", "4", "4"});
		LEI_RESP.put("Q96",    new String[] {"3", "4", "3", "4", "4"});
		LEI_RESP.put("Q97",    new String[] {"4", "4", "1", "4", "4"});
		LEI_RESP.put("Q98",    new String[] {"2", "2", "4", "4", "4"});
		LEI_RESP.put("Q99",    new String[] {"2", "1", "4", "3", "4"});
		LEI_RESP.put("Q100",   new String[] {"4", "2", "4", "4", "3"});
		LEI_RESP.put("Q101",   new String[] {"2", "3", "2", "3", "3"});
		LEI_RESP.put("Q102",   new String[] {"4", "3", "2", "2", "4"});
		LEI_RESP.put("Q103",   new String[] {"4", "1", "3", "4", "2"});
		LEI_RESP.put("Q104",   new String[] {"1", "1", "1", "1", "1"});
		LEI_RESP.put("Q105",   new String[] {"4", "2", "2", "4", "1"});
		LEI_RESP.put("Q106",   new String[] {"6", "6", "7", "7", "7"});
		LEI_RESP.put("Q107",   new String[] {"6", "5", "7", "6", "7"});
		LEI_RESP.put("Q108",   new String[] {"6", "5", "6", "6", "7"});
		LEI_RESP.put("Q109",   new String[] {"6", "5", "6", "6", "7"});
		LEI_RESP.put("Q110",   new String[] {"5", "6", "7", "6", "6"});
		LEI_RESP.put("Q111",   new String[] {"6", "4", "6", "5", "5"});
		LEI_RESP.put("Q112",   new String[] {"4", "2", "6", "4", "2"});
		LEI_RESP.put("Q113",   new String[] {"5", "4", "5", "6", "6"});
		LEI_RESP.put("Q114",   new String[] {"4", "3", "6", "6", "3"});
		LEI_RESP.put("Q115",   new String[] {"2", "3", "2", "6", "1"});
		LEI_RESP.put("Q116",   new String[] {"2", "2", "2", "2", "1"});
		LEI_RESP.put("Q117",   new String[] {"3", "6", "3", "2", "2"});
		LEI_RESP.put("Q118",   new String[] {"2", "3", "5", "4", "3"});
	};


	/*
	 * Reference data (expected scores) for the LEI test cases
	 */
	private static final HashMap<String, double[]> LEI_REF = new HashMap<String, double[]>();
	static
	{
		//---------WORK ORIENTATION SCORES----------
		LEI_REF.put("LEI_WO_AVOID", new double[] {    2.25,     3.50,     3.00,     3.50,     1.75});
		LEI_REF.put("LEI_WO_LEARN", new double[] {    5.80,     5.40,     6.60,     6.20,     6.80});
		LEI_REF.put("LEI_WO_PROVE", new double[] {    4.75,     3.25,     5.75,     5.25,     4.00});
		//---------SCALE SCORES----------
		LEI_REF.put("LEI3_SGY",     new double[] {  82.425,   51.825,   48.025,   58.775,   45.625});
		LEI_REF.put("LEI3_PGT",     new double[] {  94.000,   62.675,   66.950,   70.775,   53.950});
		LEI_REF.put("LEI3_BDV",     new double[] {  38.950,   25.325,   37.250,   30.025,   23.500});
		LEI_REF.put("LEI3_BGR",     new double[] {  42.675,   29.500,   30.875,   33.175,   23.125});
		LEI_REF.put("LEI3_PDT",     new double[] {  15.000,   11.075,   15.175,   15.550,    8.775});
		LEI_REF.put("LEI3_STA",     new double[] {  24.500,   14.450,   11.975,   12.875,   10.075});
		LEI_REF.put("LEI3_FM",      new double[] {  89.975,   56.825,   43.475,   71.875,   63.850});
		LEI_REF.put("LEI3_OPX",     new double[] {  68.775,   50.625,   41.675,   52.800,   51.975});
		LEI_REF.put("LEI3_FEX",     new double[] {  36.200,   32.825,   22.925,   33.650,   30.625});
		LEI_REF.put("LEI3_RPR",     new double[] {  93.375,   64.250,   66.925,   70.550,   54.975});
		LEI_REF.put("LEI3_PRB",     new double[] {  36.100,   39.375,   28.250,   31.650,   40.475});
		LEI_REF.put("LEI3_PPL",     new double[] { 175.100,  146.925,  130.550,  147.800,  160.800});
		LEI_REF.put("LEI3_FAL",     new double[] {  34.275,   23.525,   20.950,   21.550,   25.950});
		LEI_REF.put("LEI3_FIN",     new double[] {  74.475,   42.125,   36.650,   57.725,   44.100});
		LEI_REF.put("LEI3_STF",     new double[] {  65.050,   79.925,   52.825,   50.300,   70.850});
		LEI_REF.put("LEI3_HR",      new double[] {  66.625,   51.200,   43.675,   52.225,   41.950});
		LEI_REF.put("LEI3_NEG",     new double[] {  93.975,   72.100,   62.900,   81.550,   58.725});
		LEI_REF.put("LEI3_CMT",     new double[] {  37.100,   33.250,   25.775,   27.700,   32.450});
		LEI_REF.put("LEI3_VAS",     new double[] { 140.850,  100.900,   87.475,  100.850,   85.925});
		LEI_REF.put("LEI3_SDV",     new double[] {  69.600,   51.600,   73.500,   75.100,   75.100});
		LEI_REF.put("LEI3_DEV",     new double[] {  52.300,   69.300,   41.000,   56.100,   68.500});
		LEI_REF.put("LEI3_INN",     new double[] {  72.650,   28.000,   45.100,   56.050,   31.700});
		LEI_REF.put("LEI3_EXT",     new double[] {  42.850,   35.950,   35.200,   35.450,   33.450});
		//---------SUPER-SCALE SCORES----------
		LEI_REF.put("LEI3_GMT",     new double[] { 585.875,  399.375,  385.250,  450.050,  366.475});
		LEI_REF.put("LEI3_CHA",     new double[] { 385.000,  331.875,  269.225,  309.025,  342.175});
		LEI_REF.put("LEI3_RC",      new double[] { 338.550,  257.450,  219.825,  262.325,  219.050});
		LEI_REF.put("LEI3_PCR",     new double[] { 237.400,  184.850,  194.800,  222.700,  208.750});
		//---------OVERALL SCORE----------
		LEI_REF.put("LEI3_OAL",     new double[] {1546.825,	1173.550, 1069.100, 1244.100, 1136.450});
		//---------DEV SUG STANDARDIZATION----------
		LEI_REF.put("LEI_DS_SGY",   new double[] {-1.150106718, -2.124908413, -2.245962218, -1.903507375, -2.322417253});
		LEI_REF.put("LEI_DS_PGT",   new double[] {-1.392250329, -2.30831993,  -2.183301652, -2.071443193, -2.563474192});
		LEI_REF.put("LEI_DS_BDV",   new double[] {-1.607481437, -2.243777145, -1.686872461, -2.024284313, -2.329005744});
		LEI_REF.put("LEI_DS_BGR",   new double[] {-1.536584223, -2.153738055, -2.089329211, -1.981590781, -2.452360877});
		LEI_REF.put("LEI_DS_PDT",   new double[] {-1.957572907, -2.326810913, -1.941110066, -1.905832549, -2.54317968 });
		LEI_REF.put("LEI_DS_STA",   new double[] {-1.410062089, -2.327702703, -2.553688824, -2.471512053, -2.727173119});
		LEI_REF.put("LEI_DS_FM",    new double[] {-1.076966468, -2.11582576,  -2.534189909, -1.644186775, -1.895675337});
		LEI_REF.put("LEI_DS_OPX",   new double[] {-1.650822257, -2.332282046, -2.66831869,  -2.250619509, -2.281594954});
		LEI_REF.put("LEI_DS_FEX",   new double[] {-2.754931645, -2.909762364, -3.363932471, -2.871914855, -3.010689054});
		LEI_REF.put("LEI_DS_RPR",   new double[] {-1.202757125, -2.105018587, -2.022149938, -1.909851301, -2.392378203});
		LEI_REF.put("LEI_DS_PRB",   new double[] {-2.96542775,  -2.813652795, -3.32922421,  -3.171656317, -2.762674947});
		LEI_REF.put("LEI_DS_PPL",   new double[] {-1.403693164, -1.988928816, -2.329061338, -1.970753796, -1.700724924});
		LEI_REF.put("LEI_DS_FAL",   new double[] {-1.795830079, -2.441747281, -2.596466983, -2.56041579,  -2.296040377});
		LEI_REF.put("LEI_DS_FIN",   new double[] {-1.178582744, -2.31738656,  -2.510120745, -1.768226141, -2.247861443});
		LEI_REF.put("LEI_DS_STF",   new double[] {-2.105434026, -1.530532581, -2.577916055, -2.675504367, -1.881270774});
		LEI_REF.put("LEI_DS_HR",    new double[] {-2.150812599, -2.594505969, -2.810959298, -2.565022293, -2.860578168});
		LEI_REF.put("LEI_DS_NEG",   new double[] {-1.500601469, -2.06047964,  -2.295948402, -1.81861227,  -2.40280515 });
		LEI_REF.put("LEI_DS_CMT",   new double[] {-2.636212759, -2.792057966, -3.094640544, -3.01671794,  -2.824441386});
		LEI_REF.put("LEI_DS_VAS",   new double[] {-1.088327399, -1.945218995, -2.23317318,  -1.94629145,  -2.266419287});
		LEI_REF.put("LEI_DS_SDV",   new double[] {-2.049275165, -3.274348329, -1.783842646, -1.674947254, -1.674947254});
		LEI_REF.put("LEI_DS_DEV",   new double[] {-2.455343535, -1.418947753, -3.144241907, -2.223678595, -1.46771932 });
		LEI_REF.put("LEI_DS_INN",   new double[] { 1.172660583, -1.490308344, -0.470447904,  0.182620624, -1.269636787});
		LEI_REF.put("LEI_DS_EXT",   new double[] {-0.418169345, -1.094374755, -1.167875343, -1.143375147, -1.339376715});
		LEI_REF.put("LEI_DS_OAL",   new double[] {-1.673238256, -2.353559029, -2.543926733, -2.224976534, -2.421176471});
	};


	//
	// Constructor
	//
	public ScoreLeiUT(String name)
	{
		super(name);
	} 


	//
	// Test Main
	//
	public static void main(String[] args)
		throws Exception
	{
		junit.textui.TestRunner.run(ScoreLeiUT.class);
	}

	//
	// Tests
	//
	public void testGroovy()
	{
    	UnitTestUtils.start(this);
		// call groovy expressions from Java code
		Binding binding = new Binding();
		binding.setVariable("foo", new Integer(2));
		GroovyShell shell = new GroovyShell(binding);
		
		Object value = shell.evaluate("println 'Hello World!'; x = 123; return foo * 10");
		assertEquals(value.equals(new Integer(20)), true);
		assertEquals(binding.getVariable("x").equals(new Integer(123)), true);
		
		//System.out.println("The value of x is " + binding.getVariable("x"));
		//System.out.println("The returned value is " + value);
		UnitTestUtils.stop(this);
	}


	public void testProperties()
		throws Exception
	{
		UnitTestUtils.start(this);
		//Does the local property work?
		String applicationId = PropertyLoader.getProperty("com.pdi.reporting.application", "application.id");
		assertEquals(applicationId != null, true);
		UnitTestUtils.stop(this);
	}
	 

	/*
	 * Test the LEI
	 * @throws Exception
	 */  
	public void testLEI()
		throws Exception
	{
		UnitTestUtils.start(this);
		
		
		// iterate through the test cases
		//System.out.println("total cases=" + LEI_RESP.get("Q1").length);
		//for (int i=0; i < 1; i++)	// Test only 1
		Thread[] threads = new Thread[30];
		for (int i = 0; i < 30; i++) {
		  	Thread thread = new Thread(){
			    public void run(){
			    	try {
			    	for (int i=0; i < LEI_RESP.get("Q1").length; i++)
					{
						//System.out.println("Itr " + i);
						// Put the data into the ReportData object
						
						ReportData rd = new ReportData();
						
						for(Iterator<Map.Entry<String, String[]>> itr=LEI_RESP.entrySet().iterator(); itr.hasNext(); )
						{
							Map.Entry<String, String[]> ent = itr.next();
							if (ent.getValue()[i] == null || ent.getValue()[i].length() < 1)
							{
								// don't put the value in the ReportData raw data bucket
								continue;
							}
							
							// Move the String data to the RD
							rd.getRawData().put(ent.getKey(), ent.getValue()[i]);
						}
						//System.out.println("RD input:  " + rd.toString());

						rd = ScoreRequest.score(ReportingConstants.RC_LEI, rd);
						

						// Get the score data
						HashMap<String, String> retScores = rd.getScoreData();

						//// DEBUG:  Put out the scores
						//System.out.println("LEI:  Scores for iteration " + i + ":");
						//for(Iterator<Map.Entry<String, String>> itr=retScores.entrySet().iterator(); itr.hasNext(); )
						//{
						//	Map.Entry<String, String> ent = itr.next();
						//	String key = ent.getKey();
						//	String scor = retScores.get(key);
						//	System.out.println("  " + key + "=" + scor);
						//}

						// iterate through the expected response HashMap
						for(Iterator<Map.Entry<String, double[]>> itr=LEI_REF.entrySet().iterator(); itr.hasNext(); )
						{
							Map.Entry<String, double[]> ent = itr.next();
							String key = ent.getKey();
							String scor = retScores.get(key);
							//System.out.println("LEI:  Itr " + i + " - " + key + ":  expected " + ent.getValue()[i] + ", got " + scor);
							assertNotNull("LEI:  Itr " + i + ":  No score returned for scale " + key, scor);
							assertEquals("LEI:  Itr " + i + ":  Scale " + key +" scores don't match", ent.getValue()[i], Double.parseDouble(scor),0.00005);
						}
					}	// End of the outer "for" loop (# of test cases)
		    	} catch (Exception e) {
		    		e.printStackTrace();
		    	}
					
			    }
			  };
			 
			  	threads[i] = thread;
			  	thread.start();
			}
			for(Thread thread : threads) {
				thread.join();
			}
		
		UnitTestUtils.stop(this);
	}
}
