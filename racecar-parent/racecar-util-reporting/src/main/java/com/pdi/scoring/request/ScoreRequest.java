package com.pdi.scoring.request;

import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;
import com.pdi.reporting.request.ReportingRequest;
import com.pdi.reporting.response.ReportingResponse;

public class ScoreRequest {
	
	/**
	 * Fast way to generate a scored report data object
	 * 
	 * @param reportCode the report code (found in ReportingConstants
	 * @param reportData the report data containing the questions
	 * @return a reportData object with scores
	 * @throws Exception
	 */
	public static ReportData score(String reportCode, ReportData reportData) throws Exception {
		
		IndividualReport ir = new IndividualReport();
		ir.setReportCode(reportCode);
		ir.setReportType(ReportingConstants.REPORT_TYPE_SCORE);
		ir.getReportData().put(reportCode, reportData);

		ReportingRequest request = new ReportingRequest();
		request.addReport(ir);
		ReportingResponse response = request.generateReports();
	
		IndividualReport resp = (IndividualReport)response.getReports().get(0);
		
		
		return resp.getReportData().get(reportCode);
	}
}
