package com.pdi.scoring.ut;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;

import com.pdi.properties.PropertyLoader;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.ReportData;
import com.pdi.scoring.request.ScoreRequest;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

/*
 * Scoring test for The Financial Exercise
 */
public class ScoreFinExUT extends TestCase
{
	//
	// Static data
	//

	/*
	 * Response data for the Financial Exercise scoring test cases
	 * Hoked up by programmer... test happy path for every scoring combo
	 */
	private static final String[][] FIN_EX_RESP = new String[][]
   	{
		(new String[] { "",  "",  "",  "",  "",  "",  ""}), // Bogus - allows answers 1-13
		(new String[] {"3", "C", "c",  "", "3", "3", "3"}),	// Q1
		(new String[] {"1", "A", "a",  "", "1", "1", "1"}),	// Q2
		(new String[] {"3", "C", "c",  "", "3", "3", "3"}),	// Q3
		(new String[] {"4", "D", "d",  "", "4", "4", "4"}),	// Q4
		(new String[] {"3", "C", "c",  "",  "", "3", "3"}),	// Q5
		(new String[] {"2", "B", "b",  "",  "", "2", "2"}),	// Q6
		(new String[] {"1", "A", "a",  "",  "", "1", "1"}),	// Q7
		(new String[] {"2", "B", "b",  "",  "", "2", "2"}),	// Q8
		(new String[] {"2", "B", "b",  "",  "",  "", "2"}),	// Q9
		(new String[] {"4", "D", "d",  "",  "",  "", "4"}),	// Q10
		(new String[] {"3", "C", "c",  "",  "",  "", "3"}),	// Q11
		(new String[] {"1", "A", "a",  "",  "",  "",  ""}),	// Q12
		(new String[] {"4", "D", "d",  "",  "",  "",  ""}),	// Q13
	};


	/*
	 * Reference data (expected scores) for the Financial Exercise test cases
	 */
	private static final HashMap<String, int[]> FIN_EX_REF = new HashMap<String, int[]>();
	static
	{
		FIN_EX_REF.put("FE_MPD",		new int[] { 5,  5,  5,  1,  2,  4,  5});
		FIN_EX_REF.put("FE_DAC",		new int[] { 5,  5,  5,  1,  2,  3,  4});
		FIN_EX_REF.put("FE_AFC",		new int[] { 5,  5,  5,  1,  2,  3,  4});
		FIN_EX_REF.put("FE_DAM",		new int[] { 5,  5,  5,  1,  3,  5,  5});
		FIN_EX_REF.put("FE_CORRECT",	new int[] {13, 13, 13,  0,  4,  8, 11});
	};


	//
	// Constructor
	//
	public ScoreFinExUT(String name)
	{
		super(name);
	} 


	//
	// Test Main
	//
	public static void main(String[] args)
		throws Exception
	{
		junit.textui.TestRunner.run(ScoreFinExUT.class);
	}

	//
	// Tests
	//
	public void testGroovy()
	{
    	UnitTestUtils.start(this);
		// call groovy expressions from Java code
		Binding binding = new Binding();
		binding.setVariable("foo", new Integer(2));
		GroovyShell shell = new GroovyShell(binding);
		
		Object value = shell.evaluate("println 'Hello World!'; x = 123; return foo * 10");
		assertEquals(value.equals(new Integer(20)), true);
		assertEquals(binding.getVariable("x").equals(new Integer(123)), true);
		
		//System.out.println("The value of x is " + binding.getVariable("x"));
		//System.out.println("The returned value is " + value);
		UnitTestUtils.stop(this);
	}


	public void testProperties()
		throws Exception
	{
		UnitTestUtils.start(this);
		//Does the local property work?
		String applicationId = PropertyLoader.getProperty("com.pdi.reporting.application", "application.id");
		assertEquals(applicationId != null, true);
		UnitTestUtils.stop(this);
	}
	 

	/*
	 * Test the Financial Exercise
	 * @throws Exception
	 */  
	public void testFinEx()
		throws Exception
	{

		UnitTestUtils.start(this);

		// iterate through the test cases
		//for (int i=6; i < 7; i++)	// Test only 1
		for (int i=0; i < FIN_EX_RESP[0].length; i++)
		{
			//System.out.println("Itr " + i);
			// Put the data into the ReportData object
			ReportData rd = new ReportData();
			
			for(int j=1; j < FIN_EX_RESP.length; j++)
			{
				String str = FIN_EX_RESP[j][i];
				if (str == null || str.length() < 1)
				{
					// don't put the value in the ReportData raw data bucket
					continue;
				}
				
				String question = "Q" + j;
				rd.getRawData().put(question, str);
			}
			//System.out.println("RD input:  " + rd.toString());

			rd = ScoreRequest.score(ReportingConstants.RC_FIN_EX, rd);
			
			// Get the score data
			HashMap<String, String> retScores = rd.getScoreData();

			// iterate through the expected response HashMap
			for(Iterator<Map.Entry<String, int[]>> itr=FIN_EX_REF.entrySet().iterator(); itr.hasNext(); )
			{
				Map.Entry<String, int[]> ent = itr.next();
				String key = ent.getKey();
				String scor = retScores.get(key);
				//System.out.println("FinEx:  Itr " + i + " - " + key + ":  expected " + ent.getValue()[i] + ", got " + scor);
				assertNotNull("FinEx:  Itr " + i + ":  No score returned for scale " + key, scor);
				assertEquals("FinEx:  Itr " + i + ":  Scale " + key +" scores don't match", ent.getValue()[i], Double.parseDouble(scor),0.00005);
			}
		}	// End of the outer "for" loop (# of test cases)
		
		UnitTestUtils.stop(this);
	}
}
