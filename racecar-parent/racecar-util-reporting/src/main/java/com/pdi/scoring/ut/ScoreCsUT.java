package com.pdi.scoring.ut;

import java.util.HashMap;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;

import com.pdi.properties.PropertyLoader;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.ReportData;
import com.pdi.scoring.request.ScoreRequest;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

/*
 * Scoring test for Career Survey
 */
public class ScoreCsUT extends TestCase
{
	//
	// Static data
	//
	/*
	 * Response data for the Career Survey scoring test cases
	 */
	private static final String[][] CS_RESP = new String[][]
   	{
//	Old (V2) testing 
//		(new String[] { "",  "",  "",  "",  "",  "",  "" 		}), // Bogus - allows answers 1-82	
//		(new String[] {"1", "1", "1", "1", "2", "2"	,"2"		}),	// 1, Q1
//		(new String[] {"2", "2", "1",  "", "2", "1"	,"2"		}),	// 2, Q2
//		(new String[] {"4", "1", "2", "3", "2", "3"	,"4"		}),	// 3, Q3
//		(new String[] {"4", "3", "3", "4", "4", "3"	,"4"		}),	// 4, Q4
//		(new String[] {"4", "1", "2",  "", "1", "2"	,"4"		}),	// 5, Q5
//		(new String[] {"3", "3", "3", "3", "1", "2"	,"3"		}),	// 6, Q_6a_1
//		(new String[] {"2", "2", "3", "3", "1", "3"	,"1"		}),	// 7, Q_6a_2
//		(new String[] {"2", "2", "2", "2", "2", "1"	,"4"		}),	// 8, Q_6a_3
//		(new String[] {"2", "3", "1", "2", "3", "3"	,"4"		}),	// 9, Q_6a_4
//		(new String[] {"2", "3", "4", "1", "3", "4"	,"3"		}),	// 10, Q_6a_5
//		(new String[] {"3", "2", "1", "1", "1", "2"	,"2"		}),	// 11, Q_6a_6
//		(new String[] {"2", "2", "2", "2", "2", "1"	,"4"		}),	// 12, Q_6a_7
//		(new String[] {"3", "3", "2", "1", "3", "2"	,"5"		}),	// 13, Q_6a_8
//		(new String[] {"3", "3", "2", "2", "2", "4"	,"2"		}),	// 14, Q_6a_9
//		(new String[] {"8", "5", "8", "5", "4", "4"	,"3"		}),	// 15, Q_6b_1
//		(new String[] {"9", "1", "7", "8", "1", "6"	,"3"		}),	// 16, Q_6b_2
//		(new String[] {"1", "2", "6", "4", "2", "2"	,"6"		}),	// 17, Q_6b_3
//		(new String[] {"3", "8", "2", "7", "7", "8"	,"4"		}),	// 18, Q_6b_4
//		(new String[] {"4", "9", "9", "1", "8", "7"	,"9"		}),	// 19, Q_6b_5
//		(new String[] {"5", "3", "1", "3", "3", "3"	,"2"		}),	// 20, Q_6b_6
//		(new String[] {"2", "4", "5", "9", "5", "1"	,"7"		}),	// 21, Q_6b_7
//		(new String[] {"7", "6", "3", "2", "9", "5"	,"3"		}),	// 22, Q_6b_8
//		(new String[] {"6", "7", "4", "6", "6", "9"	,"8"		}),	// 23, Q_6b_9
//		(new String[] {"2", "2", "1", "1", "2", "3"	,"3"		}),	// 24, Q_7a_1
//		(new String[] {"4", "3", "3", "1", "4", "2"	,"4"		}),	// 25, Q_7a_2
//		(new String[] {"3", "3", "3", "3", "3", "2"	,"4"		}),	// 26, Q_7a_3
//		(new String[] {"2", "1", "2", "2", "1", "2"	,"2"		}),	// 27, Q_7a_4
//		(new String[] {"2", "2", "2", "2", "2", "3"	,"4"		}),	// 28, Q_7a_5
//		(new String[] {"4", "2", "4", "2", "1", "2"	,"1"		}),	// 29, Q_7a_6
//		(new String[] {"3", "1", "2", "2", "1", "1"	,"5"		}),	// 30, Q_7a_7
//		(new String[] {"4", "3", "4", "2", "2", "4"	,"5"		}),	// 31, Q_7a_8
//		(new String[] {"3", "2", "2", "1", "1", "1"	,"4"		}),	// 32, Q_7a_9
//		(new String[] {"1", "6", "1", "2", "7", "6"	,"8"		}),	// 33, Q_7b_1
//		(new String[] {"9", "7", "7", "6", "9", "3"	,"5"		}),	// 34, Q_7b_2
//		(new String[] {"6", "8", "6", "9", "6", "5"	,"9"		}),	// 35, Q_7b_3
//		(new String[] {"3", "1", "2", "5", "1", "4"	,"4"		}),	// 36, Q_7b_4
//		(new String[] {"2", "3", "4", "8", "3", "8"	,"6"		}),	// 37, Q_7b_5
//		(new String[] {"7", "5", "9", "7", "5", "7"	,"4"		}),	// 38, Q_7b_6
//		(new String[] {"5", "2", "5", "1", "4", "1"	,"7"		}),	// 39, Q_7b_7
//		(new String[] {"8", "9", "8", "4", "8", "9"	,"3"		}),	// 40, Q_7b_8
//		(new String[] {"4", "4", "3", "3", "2", "2"	,"3"		}),	// 41, Q_7b_9
//		(new String[] {"4", "5", "5", "3", "5", "3"	,"2"		}),	// 42, Q_8_1
//		(new String[] {"2", "1", "3", "1", "1", "1"	,"4"		}),	// 43, Q_8_2
//		(new String[] {"1", "4", "2", "2", "2", "5"	,"1"		}),	// 44, Q_8_3
//		(new String[] {"3", "3", "1", "5", "4", "4"	,"3"		}),	// 45, Q_8_4
//		(new String[] {"5", "2", "4", "4", "3", "2"	,"5"		}),	// 46, Q_8_5
//		(new String[] {"4", "3", "5", "5", "2", "2"	,"4"		}),	// 47, H_9
//		(new String[] {"4", "3", "5", "3", "4", "1"	,"4"		}),	// 48, H_10
//		(new String[] {"5", "4", "4",  "", "5", "1"	,"4"		}),	// 49, H_11
//		(new String[] {"4", "4", "4", "3", "3", "4"	,"2"		}),	// 50, H_12
//		(new String[] {"4", "2", "2", "3", "3", "1"	,"3"		}),	// 51, H_13
//		(new String[] {"3", "2", "2", "3", "2", "3"	,"3"		}),	// 52, H_14
//		(new String[] {"1", "2", "2",  "", "3", "2"	,"2"		}),	// 53, H_15
//		(new String[] {"3", "2", "3", "1", "2", "4"	,"4"		}),	// 54, Q_16a_1
//		(new String[] {"2", "1", "2", "2", "1", "1"	,"3"		}),	// 55, Q_16a_2
//		(new String[] {"4", "3", "2", "1", "2", "1"	,"4"		}),	// 56, Q_16a_3
//		(new String[] {"3", "2", "1", "2", "1", "2"	,"5"		}),	// 57, Q_16a_4
//		(new String[] {"3", "3", "3", "2", "2", "3"	,"4"		}),	// 58, Q_16a_5
//		(new String[] {"2", "2", "1", "3", "1", "4"	,"6"		}),	// 59, Q_16a_6
//		(new String[] {"2", "3", "3", "2", "3", "3"	,"5"		}),	// 60, Q_16a_7
//		(new String[] {"5", "4", "6", "7", "5", "7"	,"2"		}),	// 61, Q_16b_1
//		(new String[] {"1", "1", "3", "1", "1", "2"	,"4"		}),	// 62, Q_16b_2
//		(new String[] {"7", "6", "4", "5", "4", "1"	,"1"		}),	// 63, Q_16b_3
//		(new String[] {"4", "2", "1", "3", "2", "3"	,"3"		}),	// 64, Q_16b_4
//		(new String[] {"6", "7", "5", "6", "6", "4"	,"4"		}),	// 65, Q_16b_5
//		(new String[] {"2", "3", "2", "2", "3", "6"	,"4"		}),	// 66, Q_16b_6
//		(new String[] {"3", "5", "7", "4", "7", "5"	,"7"		}),	// 67, Q_16b_7
//		(new String[] {"1", "2", "3", "1", "4", "2"	,"4"		}),	// 68, Q_17a_1
//		(new String[] {"3", "2", "2", "2", "5", "1"	,"3"		}),	// 69, Q_17a_2
//		(new String[] {"3", "1", "1", "2", "1", "4"	,"3"		}),	// 70, Q_17a_3
//		(new String[] {"4", "2", "3", "3", "1", "4"	,"5"		}),	// 71, Q_17a_4
//		(new String[] {"2", "1", "1", "1", "1", "1"	,"3"		}),	// 72, Q_17a_5
//		(new String[] {"2", "2",  "", "2", "2", "2"	,"6"		}),	// 73, Q_17a_6
//		(new String[] {"3", "2", "2", "3", "1", "2"	,"4"		}),	// 74, Q_17a_7
//		(new String[] {"1", "5", "5", "2", "6", "5"	,"2"		}),	// 75, Q_17b_1
//		(new String[] {"6", "6", "4", "3", "7", "1"	,"4"		}),	// 76, Q_17b_2
//		(new String[] {"5", "1", "1", "4", "1", "6"	,"1"		}),	// 77, Q_17b_3
//		(new String[] {"7", "3", "7", "5", "2", "7"	,"5"		}),	// 78, Q_17b_4
//		(new String[] {"3", "2", "2", "1", "3", "2"	,"4"		}),	// 79, Q_17b_5
//		(new String[] {"2", "7", "6", "6", "5", "3"	,"5"		}),	// 80, Q_17b_6
//		(new String[] {"4", "4", "3", "7", "4", "4"	,"7"		}),	// 81, Q_17b_7
//		(new String[] {"2", "1", "3", "1", "1", "1"	,"1"		})	// 82, Q_18
//		//
//		//	New (Checkbox) testing
//		// First row (index 0) is bogus - allows answers 1-82
//		(new String[] { "",  "",  "",  "",  "",  "",  "",  "",""}),	// Checkbox		V2	
//		(new String[] {"2", "1", "2", "1", "1", "1", "1", "1","1"}),	// Q1			Q1
//		(new String[] {"2", "1", "1", "2", "2", "1", "2", "1","1"}),	// Q2			Q2
//		(new String[] {"2", "4", "5", "1", "5", "5", "4", "2","2"}),	// Q3			Q3
//		(new String[] {"3", "4", "5", "2", "5", "4", "4", "5","3"}),	// Q4			Q4
//		(new String[] {"4", "3", "4", "2", "4", "3", "4", "5","1"}),	// Q5			Q5
//		(new String[] {"1", "5", "4", "1", "4", "3", "3", "5","2"}),	// Q_6_1_a
//		(new String[] {"9", "2", "1", "9", "1", "1", "1", "1","1"}),	// Q_6_1_b
//		(new String[] {"1", "4", "5", "1", "5", "3", "3", "5","1"}),	// Q_6_2_a
//		(new String[] {"8", "3", "2", "8", "2", "2", "2", "2","1"}),	// Q_6_2_b
//		(new String[] {"1", "4", "5", "1", "5", "3", "3", "5","2"}),	// Q_6_3_a
//		(new String[] {"7", "4", "3", "7", "3", "3", "3", "3","1"}),	// Q_6_3_b
//		(new String[] {"1", "5", "4", "1", "5", "3", "3", "5","2"}),	// Q_6_4_a
//		(new String[] {"6", "1", "4", "6", "4", "5", "4", "4","1"}),	// Q_6_4_b
//		(new String[] {"1", "5", "4", "1", "5", "3", "3", "5","1"}),	// Q_6_5_a
//		(new String[] {"5", "5", "5", "5", "5", "4", "5", "5","3"}),	// Q_6_5_b
//		(new String[] {"1", "4", "5", "1", "5", "3", "3", "5","6"}),	// Q_6_6_a
//		(new String[] {"4", "6", "7", "4", "6", "7", "6", "6","5"}),	// Q_6_6_b
//		(new String[] {"1", "5", "4", "1", "5", "3", "3", "5","4"}),	// Q_6_7_a
//		(new String[] {"3", "7", "6", "3", "7", "8", "7", "7","8"}),	// Q_6_7_b
//		(new String[] {"1", "4", "5", "1", "5", "3", "3", "5","1"}),	// Q_6_8_a
//		(new String[] {"2", "8", "8", "2", "8", "6", "8", "8","9"}),	// Q_6_8_b
//		(new String[] {"1", "5", "5", "1", "5", "3", "3", "5","2"}),	// Q_6_9_a
//		(new String[] {"1", "9", "9", "1", "9", "9", "9", "9","7"}),	// Q_6_9_b
//		(new String[] {"1", "5", "5", "1", "5", "3", "4", "5","1"}),	// Q_7_1_a
//		(new String[] {"1", "1", "1", "9", "1", "1", "1", "1","1"}),	// Q_7_1_b
//		(new String[] {"1", "5", "5", "1", "5", "3", "4", "5","2"}),	// Q_7_2_a
//		(new String[] {"2", "2", "2", "8", "2", "2", "2", "2","1"}),	// Q_7_2_b
//		(new String[] {"1", "5", "5", "1", "5", "3", "4", "5","2"}),	// Q_7_3_a
//		(new String[] {"3", "3", "3", "7", "3", "3", "3", "3","2"}),	// Q_7_3_b
//		(new String[] {"1", "4", "5", "1", "5", "3", "4", "5","1"}),	// Q_7_4_a
//		(new String[] {"4", "4", "4", "6", "4", "4", "4", "4","1"}),	// Q_7_4_b
//		(new String[] {"1", "4", "5", "1", "4", "3", "4", "5","1"}),	// Q_7_5_a
//		(new String[] {"5", "5", "5", "5", "5", "5", "5", "5","5"}),	// Q_7_5_b
//		(new String[] {"1", "4", "5", "1", "4", "3", "4", "5","6"}),	// Q_7_6_a
//		(new String[] {"6", "6", "6", "4", "6", "6", "6", "6","7"}),	// Q_7_6_b
//		(new String[] {"1", "4", "5", "1", "4", "3", "4", "5","2"}),	// Q_7_7_a
//		(new String[] {"7", "7", "7", "3", "7", "7", "7", "7","8"}),	// Q_7_7_b
//		(new String[] {"1", "3", "5", "1", "4", "3", "4", "5","9"}),	// Q_7_8_a
//		(new String[] {"8", "8", "8", "2", "8", "8", "8", "8","1"}),	// Q_7_8_b
//		(new String[] {"1", "3", "5", "1", "4", "3", "4", "5","3"}),	// Q_7_9_a
//		(new String[] {"9", "9", "9", "1", "9", "9", "9", "9","4"}),	// Q_7_9_b
//		(new String[] {"1", "1", "1", "1", "1", "1", "5", "1","2"}),	// Q_8_a_ran
//		(new String[] {"2", "2", "2", "2", "2", "2", "4", "2","1"}),	// Q_8_b_ran
//		(new String[] {"3", "3", "3", "3", "3", "3", "3", "3","5"}),	// Q_8_c_ran
//		(new String[] {"4", "4", "4", "4", "4", "4", "2", "4","3"}),	// Q_8_d_ran
//		(new String[] {"5", "5", "5", "5", "5", "5", "1", "5","4"}),	// Q_8_e_ran
//		(new String[] {"3", "5", "5", "3", "5", "3", "2", "5","4"}),	// H_9
//		(new String[] {"3", "4", "5", "4", "5", "3", "3", "5","2"}),	// H_10
//		(new String[] {"4", "4", "5", "4", "5", "3", "4", "5","1"}),	// H_11
//		(new String[] {"2", "4", "5", "1", "5", "3", "4", "5","4"}),	// H_12
//		(new String[] {"3", "5", "3", "4", "3", "4", "5", "5","1"}),	// H_13
//		(new String[] {"3", "2", "2", "2", "3", "2", "4", "1","1"}),	// H_14
//		(new String[] {"3", "3", "1", "3", "2", "2", "2", "1","1"}),	// H_15
//		(new String[] {"1", "5", "5", "1", "5", "5", "5", "5","2"}),	// Q_16_1_a
//		(new String[] {"7", "1", "1", "1", "7", "1", "1", "1","1"}),	// Q_16_1_b
//		(new String[] {"1", "4", "5", "1", "5", "5", "5", "5","2"}),	// Q_16_2_a
//		(new String[] {"6", "2", "2", "2", "6", "2", "4", "2","1"}),	// Q_16_2_b
//		(new String[] {"1", "5", "5", "1", "5", "5", "5", "5","1"}),	// Q_16_3_a
//		(new String[] {"5", "3", "3", "3", "5", "3", "2", "3","1"}),	// Q_16_3_b
//		(new String[] {"1", "5", "5", "1", "5", "5", "5", "5","2"}),	// Q_16_4_a
//		(new String[] {"4", "4", "4", "4", "4", "4", "3", "4","5"}),	// Q_16_4_b
//		(new String[] {"1", "4", "5", "1", "5", "5", "5", "5","2"}),	// Q_16_5_a
//		(new String[] {"3", "5", "5", "5", "3", "5", "5", "5","6"}),	// Q_16_5_b
//		(new String[] {"1", "5", "4", "1", "5", "5", "5", "5","1"}),	// Q_16_6_a
//		(new String[] {"2", "6", "6", "6", "2", "6", "6", "6","3"}),	// Q_16_6_b
//		(new String[] {"1", "4", "4", "1", "5", "5", "5", "5","4"}),	// Q_16_7_a
//		(new String[] {"1", "7", "7", "7", "1", "7", "7", "7","7"}),	// Q_16_7_b
//		(new String[] {"1", "5", "5", "1", "5", "5", "2", "5","2"}),	// Q_17_1_a
//		(new String[] {"7", "1", "1", "7", "7", "1", "1", "1","1"}),	// Q_17_1_b
//		(new String[] {"1", "5", "5", "1", "5", "5", "3", "5","2"}),	// Q_17_2_a
//		(new String[] {"6", "2", "2", "6", "6", "2", "2", "2","3"}),	// Q_17_2_b
//		(new String[] {"1", "4", "5", "1", "5", "5", "3", "5","1"}),	// Q_17_3_a
//		(new String[] {"5", "3", "3", "5", "5", "3", "3", "3","1"}),	// Q_17_3_b
//		(new String[] {"1", "4", "5", "1", "5", "5", "2", "5","2"}),	// Q_17_4_a		Q_17a_4
//		(new String[] {"4", "4", "4", "4", "4", "4", "4", "4","4"}),	// Q_17_4_b		Q_17b_4
//		(new String[] {"1", "4", "5", "1", "5", "5", "2", "5","1"}),	// Q_17_5_a		Q_17a_5
//		(new String[] {"3", "5", "5", "3", "3", "5", "5", "5","5"}),	// Q_17_5_b		Q_17b_5
//		(new String[] {"1", "3", "4", "1", "5", "5", "3", "5","7"}),	// Q_17_6_a		Q_17a_6
//		(new String[] {"2", "6", "6", "2", "2", "6", "6", "6","3"}),	// Q_17_6_b		Q_17b_6
//		(new String[] {"1", "3", "4", "1", "5", "5", "3", "5","2"}),	// Q_17_7_a		Q_17a_7
//		(new String[] {"1", "7", "7", "1", "1", "7", "7", "7","6"}),	// Q_17_7_b		Q_17b_7
//		(new String[] {"2", "1", "2", "2", "1", "1", "2", "1","1"})		// Q_18			Q_18
//	};
		//
		//	New (Checkbox) testing
		// First row (index 0) is bogus - allows answers 1-82
		(new String[] { "",  "",  "",  "",  "",  "",  "",  "", ""}),	// Checkbox		V2	
		(new String[] {"2", "1", "2", "1", "1", "1", "1", "1", "1"}),	// Q1			Q1
		(new String[] {"2", "1", "1", "2", "2", "1", "2", "1", "1"}),	// Q2			Q2
		(new String[] {"2", "4", "5", "1", "5", "5", "4", "2", "2"}),	// Q3			Q3
		(new String[] {"3", "4", "5", "2", "5", "4", "4", "5", "3"}),	// Q4			Q4
		(new String[] {"4", "3", "4", "2", "4", "3", "4", "5", "2"}),	// Q5			Q5
		(new String[] {"1", "5", "4", "1", "4", "3", "3", "5", "3"}),	// Q_6_1_a
		(new String[] {"9", "2", "1", "9", "1", "1", "1", "1", "9"}),	// Q_6_1_b
		(new String[] {"1", "4", "5", "1", "5", "3", "3", "5", "3"}),	// Q_6_2_a
		(new String[] {"8", "3", "2", "8", "2", "2", "2", "2", "8"}),	// Q_6_2_b
		(new String[] {"1", "4", "5", "1", "5", "3", "3", "5", "3"}),	// Q_6_3_a
		(new String[] {"7", "4", "3", "7", "3", "3", "3", "3", "7"}),	// Q_6_3_b
		(new String[] {"1", "5", "4", "1", "5", "3", "3", "5", "3"}),	// Q_6_4_a
		(new String[] {"6", "1", "4", "6", "4", "5", "4", "4", "6"}),	// Q_6_4_b
		(new String[] {"1", "5", "4", "1", "5", "3", "3", "5", "3"}),	// Q_6_5_a
		(new String[] {"5", "5", "5", "5", "5", "4", "5", "5", "5"}),	// Q_6_5_b
		(new String[] {"1", "4", "5", "1", "5", "3", "3", "5", "3"}),	// Q_6_6_a
		(new String[] {"4", "6", "7", "4", "6", "7", "6", "6", "4"}),	// Q_6_6_b
		(new String[] {"1", "5", "4", "1", "5", "3", "3", "5", "3"}),	// Q_6_7_a
		(new String[] {"3", "7", "6", "3", "7", "8", "7", "7", "3"}),	// Q_6_7_b
		(new String[] {"1", "4", "5", "1", "5", "3", "3", "5", "3"}),	// Q_6_8_a
		(new String[] {"2", "8", "8", "2", "8", "6", "8", "8", "2"}),	// Q_6_8_b
		(new String[] {"1", "5", "5", "1", "5", "3", "3", "5", "9"}),	// Q_6_9_a
		(new String[] {"1", "9", "9", "1", "9", "9", "9", "9", "1"}),	// Q_6_9_b
		(new String[] {"1", "5", "5", "1", "5", "3", "4", "5", "2"}),	// Q_7_1_a
		(new String[] {"1", "1", "1", "9", "1", "1", "1", "1", "1"}),	// Q_7_1_b
		(new String[] {"1", "5", "5", "1", "5", "3", "4", "5", "2"}),	// Q_7_2_a
		(new String[] {"2", "2", "2", "8", "2", "2", "2", "2", "2"}),	// Q_7_2_b
		(new String[] {"1", "5", "5", "1", "5", "3", "4", "5", "3"}),	// Q_7_3_a
		(new String[] {"3", "3", "3", "7", "3", "3", "3", "3", "3"}),	// Q_7_3_b
		(new String[] {"1", "4", "5", "1", "5", "3", "4", "5", "2"}),	// Q_7_4_a
		(new String[] {"4", "4", "4", "6", "4", "4", "4", "4", "4"}),	// Q_7_4_b
		(new String[] {"1", "4", "5", "1", "4", "3", "4", "5", "2"}),	// Q_7_5_a
		(new String[] {"5", "5", "5", "5", "5", "5", "5", "5", "5"}),	// Q_7_5_b
		(new String[] {"1", "4", "5", "1", "4", "3", "4", "5", "2"}),	// Q_7_6_a
		(new String[] {"6", "6", "6", "4", "6", "6", "6", "6", "6"}),	// Q_7_6_b
		(new String[] {"1", "4", "5", "1", "4", "3", "4", "5", "2"}),	// Q_7_7_a
		(new String[] {"7", "7", "7", "3", "7", "7", "7", "7", "7"}),	// Q_7_7_b
		(new String[] {"1", "3", "5", "1", "4", "3", "4", "5", "2"}),	// Q_7_8_a
		(new String[] {"8", "8", "8", "2", "8", "8", "8", "8", "8"}),	// Q_7_8_b
		(new String[] {"1", "3", "5", "1", "4", "3", "4", "5", "4"}),	// Q_7_9_a
		(new String[] {"9", "9", "9", "1", "9", "9", "9", "9", "9"}),	// Q_7_9_b
		(new String[] {"1", "1", "1", "1", "1", "1", "5", "1", "1"}),	// Q_8_a_ran
		(new String[] {"2", "2", "2", "2", "2", "2", "4", "2", "2"}),	// Q_8_b_ran
		(new String[] {"3", "3", "3", "3", "3", "3", "3", "3", "3"}),	// Q_8_c_ran
		(new String[] {"4", "4", "4", "4", "4", "4", "2", "4", "4"}),	// Q_8_d_ran
		(new String[] {"5", "5", "5", "5", "5", "5", "1", "5", "5"}),	// Q_8_e_ran
		(new String[] {"3", "5", "5", "3", "5", "3", "2", "5", "4"}),	// H_9
		(new String[] {"3", "4", "5", "4", "5", "3", "3", "5", "4"}),	// H_10
		(new String[] {"4", "4", "5", "4", "5", "3", "4", "5", "4"}),	// H_11
		(new String[] {"2", "4", "5", "1", "5", "3", "4", "5", "2"}),	// H_12
		(new String[] {"3", "5", "3", "4", "3", "4", "5", "5", "2"}),	// H_13
		(new String[] {"3", "2", "2", "2", "3", "2", "4", "1", "4"}),	// H_14
		(new String[] {"3", "3", "1", "3", "2", "2", "2", "1", "4"}),	// H_15
		(new String[] {"1", "5", "5", "1", "5", "5", "5", "5", "5"}),	// Q_16_1_a
		(new String[] {"7", "1", "1", "1", "7", "1", "1", "1", "7"}),	// Q_16_1_b
		(new String[] {"1", "4", "5", "1", "5", "5", "5", "5", "5"}),	// Q_16_2_a
		(new String[] {"6", "2", "2", "2", "6", "2", "4", "2", "6"}),	// Q_16_2_b
		(new String[] {"1", "5", "5", "1", "5", "5", "5", "5", "5"}),	// Q_16_3_a
		(new String[] {"5", "3", "3", "3", "5", "3", "2", "3", "5"}),	// Q_16_3_b
		(new String[] {"1", "5", "5", "1", "5", "5", "5", "5", "5"}),	// Q_16_4_a
		(new String[] {"4", "4", "4", "4", "4", "4", "3", "4", "4"}),	// Q_16_4_b
		(new String[] {"1", "4", "5", "1", "5", "5", "5", "5", "5"}),	// Q_16_5_a
		(new String[] {"3", "5", "5", "5", "3", "5", "5", "5", "3"}),	// Q_16_5_b
		(new String[] {"1", "5", "4", "1", "5", "5", "5", "5", "5"}),	// Q_16_6_a
		(new String[] {"2", "6", "6", "6", "2", "6", "6", "6", "2"}),	// Q_16_6_b
		(new String[] {"1", "4", "4", "1", "5", "5", "5", "5", "5"}),	// Q_16_7_a
		(new String[] {"1", "7", "7", "7", "1", "7", "7", "7", "1"}),	// Q_16_7_b
		(new String[] {"1", "5", "5", "1", "5", "5", "2", "5", "4"}),	// Q_17_1_a
		(new String[] {"7", "1", "1", "7", "7", "1", "1", "1", "1"}),	// Q_17_1_b
		(new String[] {"1", "5", "5", "1", "5", "5", "3", "5", "4"}),	// Q_17_2_a
		(new String[] {"6", "2", "2", "6", "6", "2", "2", "2", "2"}),	// Q_17_2_b
		(new String[] {"1", "4", "5", "1", "5", "5", "3", "5", "4"}),	// Q_17_3_a
		(new String[] {"5", "3", "3", "5", "5", "3", "3", "3", "3"}),	// Q_17_3_b
		(new String[] {"1", "4", "5", "1", "5", "5", "2", "5", "4"}),	// Q_17_4_a		Q_17a_4
		(new String[] {"4", "4", "4", "4", "4", "4", "4", "4", "4"}),	// Q_17_4_b		Q_17b_4
		(new String[] {"1", "4", "5", "1", "5", "5", "2", "5", "4"}),	// Q_17_5_a		Q_17a_5
		(new String[] {"3", "5", "5", "3", "3", "5", "5", "5", "5"}),	// Q_17_5_b		Q_17b_5
		(new String[] {"1", "3", "4", "1", "5", "5", "3", "5", "4"}),	// Q_17_6_a		Q_17a_6
		(new String[] {"2", "6", "6", "2", "2", "6", "6", "6", "6"}),	// Q_17_6_b		Q_17b_6
		(new String[] {"1", "3", "4", "1", "5", "5", "3", "5", "4"}),	// Q_17_7_a		Q_17a_7
		(new String[] {"1", "7", "7", "1", "1", "7", "7", "7", "7"}),	// Q_17_7_b		Q_17b_7
		(new String[] {"2", "1", "2", "2", "1", "1", "2", "1", "2"})		// Q_18			Q_18
	};


//	/*
//	 * Reference data (expected percentiles) for the Career Survey test cases
//	 */
//	private static final HashMap<String, double[]> CS_REF = new HashMap<String, double[]>();
//	static
//	{
//		// Old (V2) scores
////		CS_REF.put("CAREER_GOALS",      new double[] {64.8, 94.3, 49.0,  0.8, 49.0,  5.2, 64.8});
////		CS_REF.put("CAREER_DRIVERS",    new double[] {61.2, 31.9, 96.4, 19.8, 31.9, 19.8, 11});
////		CS_REF.put("LEARNING_ORIENT",   new double[] { 8.6, 47.2, 13.5,  0.8, 27.8, 93.1, 67.2});
////		CS_REF.put("EXPERIENCE_ORIENT", new double[] {26.4, 79.2, 50.4, 83.5, 83.5, 44.0, 79.2});
//		// New (Check-box) scores
//		CS_REF.put("CAREER_GOALS",      new double[] {20.474, 49.073, 49.073,  5.193, 97.619, 64.711, 88.082, 64.711, 3.75});
//		CS_REF.put("CAREER_DRIVERS",    new double[] {19.813, 11.082, 11.082, 19.813, 11.082, 11.082, 19.813, 11.082, 2.33});
//		CS_REF.put("LEARNING_ORIENT",   new double[] {37.050, 88.891, 83.195, 37.050, 93.051, 27.780, 67.221, 88.891, 4});
//		CS_REF.put("EXPERIENCE_ORIENT", new double[] { 0.000, 92.739, 99.222,  0.000, 99.890, 99.890, 63.018, 99.890, 4.42});
//	};


	/*
	 * Reference data (expected scores) for the Career Survey test cases
	 */
	private static final HashMap<String, double[]> CS_RAW = new HashMap<String, double[]>();
	static
	{
		// New (Check-box) scores                           0      1      2      3      4      5      6      7
//		CS_RAW.put("CAREER_GOALS_RAW",      new double[] {3.000, 3.500, 3.500, 2.500, 4.750, 3.750, 4.250, 3.750});
//		CS_RAW.put("CAREER_DRIVERS_RAW",    new double[] {2.000, 1.667, 1.667, 2.000, 1.667, 1.667, 2.000, 1.667});
//		CS_RAW.put("LEARNING_ORIENT_RAW",   new double[] {3.000, 3.857, 3.714, 3.000, 4.000, 2.857, 3.429, 3.857});
//		CS_RAW.put("EXPERIENCE_ORIENT_RAW", new double[] {1.000, 4.286, 4.714, 1.000, 5.000, 5.000, 3.786, 5.000});
		CS_RAW.put("CAREER_GOALS",      new double[] {3.000, 3.500, 3.500, 2.500, 4.750, 3.750, 4.250, 3.750, 2.500});
		CS_RAW.put("CAREER_DRIVERS",    new double[] {2.000, 1.667, 1.667, 2.000, 1.667, 1.667, 2.000, 1.667, 2.000});
		CS_RAW.put("LEARNING_ORIENT",   new double[] {3.000, 3.857, 3.714, 3.000, 4.000, 2.857, 3.429, 3.857, 3.429});
		CS_RAW.put("EXPERIENCE_ORIENT", new double[] {1.000, 4.286, 4.714, 1.000, 5.000, 5.000, 3.786, 5.000, 4.500});
	};


	//
	// Constructor
	//
	public ScoreCsUT(String name)
	{
		super(name);
	} 


	//
	// Test Main
	//
	public static void main(String[] args)
		throws Exception
	{
		junit.textui.TestRunner.run(ScoreCsUT.class);
	}

	//
	// Tests
	//
	public void testGroovy()
	{
    	UnitTestUtils.start(this);
		// call groovy expressions from Java code
		Binding binding = new Binding();
		binding.setVariable("foo", new Integer(2));
		GroovyShell shell = new GroovyShell(binding);
		
		Object value = shell.evaluate("println 'Hello World!'; x = 123; return foo * 10");
		assertEquals(value.equals(new Integer(20)), true);
		assertEquals(binding.getVariable("x").equals(new Integer(123)), true);
		
		//System.out.println("The value of x is " + binding.getVariable("x"));
		//System.out.println("The returned value is " + value);
		UnitTestUtils.stop(this);
	}


	public void testProperties()
		throws Exception
	{
		UnitTestUtils.start(this);
		//Does the local property work?
		String applicationId = PropertyLoader.getProperty("com.pdi.reporting.application", "application.id");
		assertEquals(applicationId != null, true);
		UnitTestUtils.stop(this);
	}


	/*
	 * Test the CAREER SURVEY 
	 * @throws Exception
	 */
	public void testCAREER_SURVEY()
		throws Exception
	{

		UnitTestUtils.start(this);

		// iterate through all of the instances
		//System.out.println("Itr count = " + CS_RESP[0].length);
		for (int i=0; i < CS_RESP[0].length; i++)	// test 'em all
		////for (int i=0; i < 1; i++)	// test first 1
		{
			String question;
			ReportData rd = new ReportData();
			
			int cnt = 0;
			for (int j=1; j <= 82; j++)
			{
// Old order
//				// questions 1-5
//				if (j <= 5)
//				{
//					question = "Q" + j;
//					rd.getRawData().put(question, CS_RESP[j][i]);
//				} else if (j >= 6 && j <= 14)	// questions 6 & 7 ( radioRanking grid and drop-down ranking )
//				{
//					question = "Q_6a_" + (j - 5);
//					rd.getRawData().put(question, CS_RESP[j][i]);
//				} else if (j >= 15 && j <= 23)
//				{
//					question = "Q_6b_" + (j - 14);
//					rd.getRawData().put(question, CS_RESP[j][i]);
//				} else if (j >= 24 && j <= 32)
//				{
//					question = "Q_7a_" + (j - 23);
//					rd.getRawData().put(question, CS_RESP[j][i]);
//				} else if (j >= 33 && j <= 41)
//				{
//					question = "Q_7b_" + (j - 32);
//					rd.getRawData().put(question, CS_RESP[j][i]);
//				} else if (j >= 42 && j <= 46)	// question 8 ( select ranking dropdowns )
//				{
//					question = "Q_8_" + (j - 41);
//					rd.getRawData().put(question, CS_RESP[j][i]);
//				} else if (j >= 47 && j <= 53)	// questions 9-15  (MAY NEED TO DO SOME RENAMING HERE.....)
//				{
//					question = "H_" + (j - 38);
//					rd.getRawData().put(question, CS_RESP[j][i]);
//				} else if (j >= 54 && j <= 60)	// questions 16 & 17 ( radioRanking grid and drop-down ranking )
//				{
//					question = "Q_16a_" + (j - 53);
//					rd.getRawData().put(question, CS_RESP[j][i]);
//				} else if (j >= 61 && j <= 67)
//				{
//					question = "Q_16b_" + (j - 60);
//					rd.getRawData().put(question, CS_RESP[j][i]);
//				} else if (j >= 68 && j <= 74)
//				{
//					question = "Q_17a_" + (j - 67);
//					rd.getRawData().put(question, CS_RESP[j][i]);
//				} else if (j >= 75 && j <= 81)
//				{
//					question = "Q_17b_" + (j - 74);
//					rd.getRawData().put(question, CS_RESP[j][i]);
//				}  else if (j == 82)
//				{
//					question = "Q" + j;
//					rd.getRawData().put(question, CS_RESP[j][i]);
//				} else {
//					// do nothing
//				}

				// New (checkbox) order
				// questions 1-5
				if (j <= 5)
				{
					question = "Q" + j;
					rd.getRawData().put(question, CS_RESP[j][i]);
				} else if (j >= 6 && j <= 23)	// question 6 ( radioRanking grid and drop-down ranking )
				{
					if (j == 6)  cnt = 1;
					question = "Q_6" + ((j % 2) == 0 ? "a" : "b") + "_" + cnt;
					rd.getRawData().put(question, CS_RESP[j][i]);
					if ((j % 2) == 1)  cnt++;
				} else if (j >= 24 && j <= 41)	// question 7 ( radioRanking grid and drop-down ranking )
				{
					if (j == 24)  cnt = 1;
					question = "Q_7" + ((j % 2) == 0 ? "a" : "b") + "_" + cnt;
					rd.getRawData().put(question, CS_RESP[j][i]);
					if ((j % 2) == 1)  cnt++;
				} else if (j >= 42 && j <= 46)	// question 8 ( select ranking dropdowns )
				{
					question = "Q_8_" + (j - 41);
					rd.getRawData().put(question, CS_RESP[j][i]);
				} else if (j >= 47 && j <= 53)	// questions 9-15  (MAY NEED TO DO SOME RENAMING HERE.....)
				{
					question = "H_" + (j - 38);
					rd.getRawData().put(question, CS_RESP[j][i]);
				} else if (j >= 54 && j <= 67)	// question 16 ( radioRanking grid and drop-down ranking )
				{
					if (j == 54)  cnt = 1;
					question = "Q_16" + ((j % 2) == 0 ? "a" : "b") + "_" + cnt;
					rd.getRawData().put(question, CS_RESP[j][i]);
					if ((j % 2) == 1)  cnt++;
				} else if (j >= 68 && j <= 81)	// question 16 ( radioRanking grid and drop-down ranking )
				{
					if (j == 68)  cnt = 1;
					question = "Q_17" + ((j % 2) == 0 ? "a" : "b") + "_" + cnt;
					rd.getRawData().put(question, CS_RESP[j][i]);
					if ((j % 2) == 1)  cnt++;
				}  else if (j == 82)
				{
					question = "Q_18";
					rd.getRawData().put(question, CS_RESP[j][i]);
				} else {
					// do nothing
				}
			}
			//System.out.println("Itr " + i + ":  " + rd.rawToString());

			rd = ScoreRequest.score(ReportingConstants.RC_CAREER_SURVEY, rd);
			//System.out.println("Itr " + i + ":  " + rd.scoreToString());
			System.out.println(rd.getScoreData("CAREER_GOALS") + " , " + rd.getScoreData("CAREER_DRIVERS") + " , " + rd.getScoreData("LEARNING_ORIENT") + " , " + rd.getScoreData("EXPERIENCE_ORIENT"));

//			// Percentiles (probably should go away; percentiles shouldn't be calced in this scoring -- raw only))
//			assertEquals("Itr " + i + " Career Goals match failed.", CS_REF.get("CAREER_GOALS")[i], Float.parseFloat(rd.getScoreData("CAREER_GOALS")), 0.1);
//			assertEquals("Itr " + i + " Career Drivers match failed.", CS_REF.get("CAREER_DRIVERS")[i], Float.parseFloat(rd.getScoreData("CAREER_DRIVERS")), 0.1);
//			assertEquals("Itr " + i + " Learning Orientation match failed.", CS_REF.get("LEARNING_ORIENT")[i], Float.parseFloat(rd.getScoreData("LEARNING_ORIENT")), 0.1);
//			assertEquals("Itr " + i + " Experience Orientation match failed.", CS_REF.get("EXPERIENCE_ORIENT")[i], Float.parseFloat(rd.getScoreData("EXPERIENCE_ORIENT")), 0.1);

			// Raw scores (matches what other scoring routines are doing)
			assertEquals("Itr " + i + " Career Goals score match failed.", CS_RAW.get("CAREER_GOALS")[i], Float.parseFloat(rd.getScoreData("CAREER_GOALS")), 0.1);
			assertEquals("Itr " + i + " Career Drivers score match failed.", CS_RAW.get("CAREER_DRIVERS")[i], Float.parseFloat(rd.getScoreData("CAREER_DRIVERS")), 0.1);
			assertEquals("Itr " + i + " Learning Orientation score match failed.", CS_RAW.get("LEARNING_ORIENT")[i], Float.parseFloat(rd.getScoreData("LEARNING_ORIENT")), 0.1);
			assertEquals("Itr " + i + " Experience Orientation score match failed.", CS_RAW.get("EXPERIENCE_ORIENT")[i], Float.parseFloat(rd.getScoreData("EXPERIENCE_ORIENT")), 0.1);
			//System.out.println("Raw scores (Exp/Act)... Itr=" + i);
			//System.out.println("  CG:  " + CS_RAW.get("CAREER_GOALS")[i] + "/" + rd.getScoreData("CAREER_GOALS"));
			//System.out.println("  CD:  " + CS_RAW.get("CAREER_DRIVERS")[i] + "/" + rd.getScoreData("CAREER_DRIVERS"));
			//System.out.println("  LO:  " + CS_RAW.get("LEARNING_ORIENT")[i] + "/" + rd.getScoreData("LEARNING_ORIENT"));
			//System.out.println("  EO:  " + CS_RAW.get("EXPERIENCE_ORIENT")[i] + "/" + rd.getScoreData("EXPERIENCE_ORIENT"));
//			assertEquals("Itr " + i + " Career Goals raw match failed.", CS_RAW.get("CAREER_GOALS_RAW")[i], Float.parseFloat(rd.getScoreData("CAREER_GOALS_RAW")), 0.1);
//			assertEquals("Itr " + i + " Career Drivers raw match failed.", CS_RAW.get("CAREER_DRIVERS_RAW")[i], Float.parseFloat(rd.getScoreData("CAREER_DRIVERS_RAW")), 0.1);
//			assertEquals("Itr " + i + " Learning Orientation raw match failed.", CS_RAW.get("LEARNING_ORIENT_RAW")[i], Float.parseFloat(rd.getScoreData("LEARNING_ORIENT_RAW")), 0.1);
//			assertEquals("Itr " + i + " Experience Orientation raw match failed.", CS_RAW.get("EXPERIENCE_ORIENT_RAW")[i], Float.parseFloat(rd.getScoreData("EXPERIENCE_ORIENT_RAW")), 0.1);
////		System.out.println("Raw scores (Exp/Act)... Itr=" + i);
////		System.out.println("  CG:  " + CS_RAW.get("CAREER_GOALS_RAW")[i] + "/" + rd.getScoreData("CAREER_GOALS_RAW"));
////		System.out.println("  CD:  " + CS_RAW.get("CAREER_DRIVERS_RAW")[i] + "/" + rd.getScoreData("CAREER_DRIVERS_RAW"));
////		System.out.println("  LO:  " + CS_RAW.get("LEARNING_ORIENT_RAW")[i] + "/" + rd.getScoreData("LEARNING_ORIENT_RAW"));
////		System.out.println("  EO:  " + CS_RAW.get("EXPERIENCE_ORIENT_RAW")[i] + "/" + rd.getScoreData("EXPERIENCE_ORIENT_RAW"));
		}

		UnitTestUtils.stop(this);
	}
}
