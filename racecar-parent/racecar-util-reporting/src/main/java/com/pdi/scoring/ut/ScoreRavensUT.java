package com.pdi.scoring.ut;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;

import com.pdi.properties.PropertyLoader;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;
import com.pdi.reporting.request.ReportingRequest;
import com.pdi.reporting.response.ReportingResponse;
import com.pdi.scoring.request.ScoreRequest;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;


/*
 * Scoring test for Raven's (Short Form and Form B)
 */
public class ScoreRavensUT extends TestCase
{
	//
	// Static data
	//


	/*
	 * Response data for the Ravens Short Form scoring test cases
	 */
	private static final String[][] RAV_SF_RESP = new String[][]
	{
		(new String[] { "",  "",  "",  "",  ""}), // Bogus - allows answers 1-23
		(new String[] {"4", "4", "4", "4", "4"}),
		(new String[] {"3", "7", "5", "5", "2"}),
		(new String[] {"4", "4", "4", "4", "4"}),
		(new String[] {"6", "6", "6", "6", "3"}),
		(new String[] {"7", "4", "7", "7", "1"}),
		(new String[] {"3", "4", "5", "3", "3"}),
		(new String[] {"8", "8", "8", "8", "8"}),
		(new String[] {"8", "2", "1", "8", "1"}),
		(new String[] {"8", "8", "7", "7", "7"}),
		(new String[] {"6", "6", "3", "6", "6"}),
		(new String[] {"3", "8", "3", "3", "3"}),
		(new String[] {"7", "7", "7", "2", "5"}),
		(new String[] {"5", "6", "8", "3", "6"}),
		(new String[] {"5", "4", "3", "5", "7"}),
		(new String[] {"5", "4", "5",  "", "5"}),
		(new String[] {"7", "6", "7", "3", "6"}),
		(new String[] {"5", "7", "1", "5", "5"}),
		(new String[] {"8", "6", "2", "4", "4"}),
		(new String[] {"8", "8", "7", "8", "8"}),
		(new String[] {"5", "3", "2", "5", "6"}),
		(new String[] {"4", "3", "2", "1", "1"}),
		(new String[] {"1", "4", "1", "3", "1"}),
		(new String[] { "", "8", "4",  "", "6"})
	};

	/*
	 * Reference data (expected scores) for the Raven's Short Form test cases
	 */
	private static final HashMap<String, int[]> RAV_SF_REF = new HashMap<String, int[]>();
	static
	{
		RAV_SF_REF.put("CORRECT_ANSWERS",   new int[] {14,  8,  9, 16, 15});
		RAV_SF_REF.put("NUMBER_UNANSWERED", new int[] { 1,  0,  0,  2,  0});
		RAV_SF_REF.put("INCORRECT_ANSWERS", new int[] { 8, 15, 14,  5,  8});
	};


	/*
	 * Response data for the Ravens Form B scoring test cases
	 * This is "made up" data... replace it when we have real test cases
	 */
	private static final String[][] RAV_B_RESP = new String[][]
	{
		(new String[] { "",  "",  "",  "",  ""}), // Bogus - allows answers 1-23
		(new String[] {"8", "8",  "", "4", "8"}),
		(new String[] {"7", "7", "1", "3", "8"}),
		(new String[] {"1", "6",  "", "2", "1"}),
		(new String[] {"8", "5", "2", "1", "1"}),
		(new String[] {"4", "4",  "",  "", "4"}),
		(new String[] {"3", "3", "3", "1", "4"}),
		(new String[] {"1", "2",  "", "3", "1"}),
		(new String[] {"6", "1", "4",  "", "1"}),
		(new String[] {"5", "8",  "",  "", "5"}),
		(new String[] {"4", "7", "5", "4", "5"}),
		(new String[] {"4", "6",  "", "4",  ""}),
		(new String[] {"7", "5", "6",  "", "7"}),
		(new String[] {"8", "4",  "",  "", "7"}),
		(new String[] {"7", "3", "7", "7", "7"}),
		(new String[] {"2", "2",  "", "2", "7"}),
		(new String[] {"5", "1", "8",  "",  ""}),
		(new String[] {"6",  "",  "",  "", "6"}),
		(new String[] {"7", "1", "7", "7", "6"}),
		(new String[] {"8",  "",  "", "8", "8"}),
		(new String[] {"3", "2", "3",  "", "8"}),
		(new String[] {"6",  "",  "",  "", "8"}),
		(new String[] {"3", "3", "3", "3", "8"}),
		(new String[] {"2",  "",  "", "2",  ""})
	};

	/*
	 * Reference data (expected scores) for the Raven's Form B test cases
	 * This is "made up" data... replace it when we have real test cases
	 */
	private static final HashMap<String, int[]> RAV_B_REF = new HashMap<String, int[]>();
	static
	{
		RAV_B_REF.put("CORRECT_ANSWERS",   new int[] { 23,  6,  5,  8,  9});
		RAV_B_REF.put("NUMBER_UNANSWERED", new int[] {  0,  4, 12,  9,  3});
		RAV_B_REF.put("INCORRECT_ANSWERS", new int[] {  0, 13,  6,  6, 11});
	};


	//
	// Constructor
	//
	public ScoreRavensUT(String name)
	{
		super(name);
	} 


	//
	// Test Main
	//
	public static void main(String[] args)
		throws Exception
	{
		junit.textui.TestRunner.run(ScoreRavensUT.class);
	}

	//
	// Tests
	//
	public void testGroovy()
	{
    	UnitTestUtils.start(this);
		// call groovy expressions from Java code
		Binding binding = new Binding();
		binding.setVariable("foo", new Integer(2));
		GroovyShell shell = new GroovyShell(binding);
		
		Object value = shell.evaluate("println 'Hello World!'; x = 123; return foo * 10");
		assertEquals(value.equals(new Integer(20)), true);
		assertEquals(binding.getVariable("x").equals(new Integer(123)), true);
		
		//System.out.println("The value of x is " + binding.getVariable("x"));
		//System.out.println("The returned value is " + value);
		UnitTestUtils.stop(this);
	}


	public void testProperties()
		throws Exception
	{
		UnitTestUtils.start(this);
		//Does the local property work?
		String applicationId = PropertyLoader.getProperty("com.pdi.reporting.application", "application.id");
		assertEquals(applicationId != null, true);
		UnitTestUtils.stop(this);
	}


	/*    
	 * Test the RAVENS_SF
	 * @throws Exception
     */
	public void testRAVENS_SF()
		throws Exception
	{
		UnitTestUtils.start(this);

		for (int i=0; i < RAV_SF_RESP[0].length; i++)	// test 'em all
		////for (int i=0; i < 1; i++)	// test first 1
		{
			ReportData rd = new ReportData();

			// Insert the test response data for this iteration
			for(int j = 1; j <= 23; j++)
			{
				String question = "Q" + j;
				rd.getRawData().put(question, RAV_SF_RESP[j][i]);
			}
			////System.out.println(rd.toString());
			
			rd = ScoreRequest.score(ReportingConstants.RC_RAVENS_SF, rd);

			// Get the score data
			HashMap<String, String> retScores = rd.getScoreData();

			// iterate through the expected response HashMap
			for(Iterator<Map.Entry<String, int[]>> itr=RAV_SF_REF.entrySet().iterator(); itr.hasNext(); )
			{
				Map.Entry<String, int[]> ent = itr.next();
				String key = ent.getKey();
				String scor = retScores.get(key);
				//System.out.println("RavSF:  Itr " + i + " - " + key + ":  expected " + ent.getValue()[i] + ", got " + scor);
				assertNotNull("RavSF:  Itr " + i + ":  No score returned for scale " + key, scor);
				assertEquals("RavSF:  Itr " + i + ":  Scale " + key +" scores don't match", ent.getValue()[i], Integer.parseInt(scor));
			}

		}
		
		UnitTestUtils.stop(this);
	}


	/*    
	 * Test the RAVENS_B
	 * @throws Exception
     */
	public void testRAVENS_B()
		throws Exception
	{
//    	if(true) {
//    		return;
//    	}

		UnitTestUtils.start(this);
		
		IndividualReport ir = new IndividualReport();
		ir.setReportCode(ReportingConstants.RC_RAVENS_B);
		ir.setReportType(ReportingConstants.REPORT_TYPE_SCORE);

		for (int i=0; i < RAV_B_RESP[0].length; i++)	// test 'em all
		////for (int i=0; i < 1; i++)	// test first 1
		{
			ReportData rd = new ReportData();

			// Insert the test response data for this iteration
			for(int j = 1; j <= 23; j++)
			{
				String question = "question_" + j;
				rd.getRawData().put(question, RAV_B_RESP[j][i]);
			}
			////System.out.println(rd.toString());
			
			ir.getReportData().put(ReportingConstants.RC_RAVENS_B, rd);
			
			ReportingRequest request = new ReportingRequest();
			request.addReport(ir);
			ReportingResponse response = request.generateReports();

			// Check the results
			IndividualReport resp = (IndividualReport)response.getReports().get(0);
			//System.out.println("Itr " + i + ":  correct=" + resp.getReportData().get(ReportingConstants.RC_RAVENS_B).getScoreData("CORRECT_ANSWERS")
			//	+ ", incorrect=" + resp.getReportData().get(ReportingConstants.RC_RAVENS_B).getScoreData("INCORRECT_ANSWERS")
			//	+ ", unans=" + resp.getReportData().get(ReportingConstants.RC_RAVENS_B).getScoreData("NUMBER_UNANSWERED"));

			// Get the score data
			HashMap<String, String> retScores = resp.getReportData().get(ReportingConstants.RC_RAVENS_B).getScoreData();

			// iterate through the expected response HashMap
			for(Iterator<Map.Entry<String, int[]>> itr=RAV_B_REF.entrySet().iterator(); itr.hasNext(); )
			{
				Map.Entry<String, int[]> ent = itr.next();
				String key = ent.getKey();
				String scor = retScores.get(key);
				//System.out.println("RavB:  Itr " + i + " - " + key + ":  expected " + ent.getValue()[i] + ", got " + scor);
				assertNotNull("RavB:  Itr " + i + ":  No score returned for scale " + key, scor);
				assertEquals("RavB:  Itr " + i + ":  Scale " + key +" scores don't match", ent.getValue()[i], Integer.parseInt(scor));
			}

		}
		
		UnitTestUtils.stop(this);
	}
}
