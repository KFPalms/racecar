package com.pdi.scoring.ut;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;

import com.pdi.properties.PropertyLoader;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.ReportData;
import com.pdi.scoring.request.ScoreRequest;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;


/*
 * Scoring test for Watson-Glaser E (WG-A not scored in this paradigm)
 */
public class ScoreWgeUT extends TestCase
{
	//
	// Static data
	//

	/*
	 * Response data for the Watson-Glaser II Form E scoring test cases
	 * This is "made up" data... replace it when we have real test cases
	 */
	private static final String[][] WG_E_RESP = new String[][]
	{
		// 0 - All correct
		// 1 - All incorrect
		// 2 - RA only
		// 3 - EA only
		// 4 - DC only
		(new String[] { "",  "",  "",  "",  ""}),	// Bogus - allows answers 1-40
		(new String[] {"5", "1", "1", "1", "5"}),	// Q1
		(new String[] {"4", "1", "1", "1", "4"}),	// Q2
		(new String[] {"5", "1", "1", "1", "5"}),	// Q3
		(new String[] {"4", "1", "1", "1", "4"}),	// Q4
		(new String[] {"4", "1", "1", "1", "4"}),	// Q5
		(new String[] {"2", "1", "2", "1", "1"}),	// Q6
		(new String[] {"1", "2", "1", "2", "2"}),	// Q7
		(new String[] {"2", "1", "2", "1", "1"}),	// Q8
		(new String[] {"2", "1", "2", "1", "1"}),	// Q9
		(new String[] {"1", "2", "1", "2", "2"}),	// Q10
		(new String[] {"2", "1", "2", "1", "1"}),	// Q11
		(new String[] {"1", "2", "1", "2", "2"}),	// Q12
		(new String[] {"2", "1", "2", "1", "1"}),	// Q13
		(new String[] {"1", "2", "1", "2", "2"}),	// Q14
		(new String[] {"2", "1", "2", "1", "1"}),	// Q15
		(new String[] {"2", "1", "2", "1", "1"}),	// Q16
		(new String[] {"2", "1", "2", "1", "1"}),	// Q17
		(new String[] {"1", "2", "2", "2", "1"}),	// Q18
		(new String[] {"2", "1", "1", "1", "2"}),	// Q19
		(new String[] {"2", "1", "1", "1", "2"}),	// Q20
		(new String[] {"2", "1", "1", "1", "2"}),	// Q21
		(new String[] {"1", "2", "2", "2", "1"}),	// Q22
		(new String[] {"2", "1", "1", "1", "2"}),	// Q23
		(new String[] {"2", "1", "1", "1", "2"}),	// Q24
		(new String[] {"2", "1", "1", "1", "2"}),	// Q25
		(new String[] {"2", "1", "1", "1", "2"}),	// Q26
		(new String[] {"1", "2", "2", "2", "1"}),	// Q27
		(new String[] {"2", "1", "1", "1", "2"}),	// Q28
		(new String[] {"2", "1", "1", "2", "1"}),	// Q29
		(new String[] {"1", "2", "2", "1", "2"}),	// Q30
		(new String[] {"1", "2", "2", "1", "2"}),	// Q31
		(new String[] {"2", "1", "1", "2", "1"}),	// Q32
		(new String[] {"1", "2", "2", "1", "2"}),	// Q33
		(new String[] {"1", "2", "2", "1", "2"}),	// Q34
		(new String[] {"2", "1", "1", "2", "1"}),	// Q35
		(new String[] {"1", "2", "2", "1", "2"}),	// Q36
		(new String[] {"1", "2", "2", "1", "2"}),	// Q37
		(new String[] {"2", "1", "1", "2", "1"}),	// Q38
		(new String[] {"2", "1", "1", "2", "1"}),	// Q39
		(new String[] {"1", "2", "2", "1", "2"})	// Q40
	};

	/*
	 * Reference data (expected scores) for the Watson-Glaser II Form E test cases
	 * This is "made up" data... replace it when we have real test cases
	 */
	private static final HashMap<String, int[]> WG_E_REF = new HashMap<String, int[]>();
	static
	{
		WG_E_REF.put("WGE_CT",   new int[] { 40,  0, 12, 12, 16});
		WG_E_REF.put("WGE_RA",   new int[] { 12,  0, 12,  0,  0});
		WG_E_REF.put("WGE_EA",   new int[] { 12,  0,  0, 12,  0});
		WG_E_REF.put("WGE_DC",   new int[] { 16,  0,  0,  0, 16});
	};


	//
	// Constructor
	//
	public ScoreWgeUT(String name)
	{
		super(name);
	} 


	//
	// Test Main
	//
	public static void main(String[] args)
		throws Exception
	{
		junit.textui.TestRunner.run(ScoreWgeUT.class);
	}

	//
	// Tests
	//
	public void testGroovy()
	{
    	UnitTestUtils.start(this);
		// call groovy expressions from Java code
		Binding binding = new Binding();
		binding.setVariable("foo", new Integer(2));
		GroovyShell shell = new GroovyShell(binding);
		
		Object value = shell.evaluate("println 'Hello World!'; x = 123; return foo * 10");
		assertEquals(value.equals(new Integer(20)), true);
		assertEquals(binding.getVariable("x").equals(new Integer(123)), true);
		
		//System.out.println("The value of x is " + binding.getVariable("x"));
		//System.out.println("The returned value is " + value);
		UnitTestUtils.stop(this);
	}


	public void testProperties()
		throws Exception
	{
		UnitTestUtils.start(this);
		//Does the local property work?
		String applicationId = PropertyLoader.getProperty("com.pdi.reporting.application", "application.id");
		assertEquals(applicationId != null, true);
		UnitTestUtils.stop(this);
	}


	/*    
	 * Test the Watson-Glaser II Form E
	 * @throws Exception
     */
	public void testWgE()
		throws Exception
	{
		UnitTestUtils.start(this);

		
		
		for (int i=0; i < WG_E_RESP[0].length; i++)	// test 'em all
		////for (int i=0; i < 1; i++)	// test first 1
		{
			ReportData rd = new ReportData();

			// Insert the test response data for this iteration
			for(int j = 1; j < WG_E_RESP.length; j++)
			{
				String question = "question_" + j;
				rd.getRawData().put(question, WG_E_RESP[j][i]);
			}
			////System.out.println(rd.toString());
			
			rd = ScoreRequest.score(ReportingConstants.RC_WG_E, rd);
			
			// Get the score data
			HashMap<String, String> retScores = rd.getScoreData();

			// iterate through the expected response HashMap
			for(Iterator<Map.Entry<String, int[]>> itr=WG_E_REF.entrySet().iterator(); itr.hasNext(); )
			{
				Map.Entry<String, int[]> ent = itr.next();
				String key = ent.getKey();
				String scor = retScores.get(key);
				//System.out.println("WGE:  Itr " + i + " - " + key + ":  expected " + ent.getValue()[i] + ", got " + scor);
				assertNotNull("WGE:  Itr " + i + ":  No score returned for scale " + key, scor);
				assertEquals("WGE:  Itr " + i + ":  Scale " + key +" scores don't match", ent.getValue()[i], Integer.parseInt(scor));
			}
		}

		UnitTestUtils.stop(this);
	}


	//
	//
}
