package com.pdi.reporting;

import groovy.lang.GroovyObject;

import java.util.HashMap;
import org.w3c.dom.Document;

public class ApplicationData {
	private static ApplicationData singleton = null;
	
	//@SuppressWarnings("unchecked")
	private ApplicationData() {
	}


	public Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException();
	}

	
	private HashMap<String, Document> data = new HashMap<String, Document>();
	@SuppressWarnings("unchecked")
	private HashMap<String, Class> calculation = new HashMap<String, Class>();
	private HashMap<String, String> rendering = new HashMap<String, String>();
	
	
	private static synchronized ApplicationData getInstance() {
		if(ApplicationData.singleton == null) {
			ApplicationData.singleton = new ApplicationData();
		}
		return ApplicationData.singleton;
	}
	
	public static GroovyObject getCalculation(String code) {
		try {
			return (GroovyObject)ApplicationData.getInstance().calculation.get(code).newInstance();
		} catch(Exception e) {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	protected static void putCalculation(String code, Class cl) {
		try {
			ApplicationData.getInstance().calculation.put(code, cl);
		} catch(Exception e) {
		}
	}
	
	public static Document getData(String code) {
		return ApplicationData.getInstance().data.get(code);
	}
	
	protected static void putData(String code, Document doc) {
		ApplicationData.getInstance().data.put(code, doc);
	}
	
	public static String getRendering(String code) {
		return ApplicationData.getInstance().rendering.get(code);
	}
	
	protected static void putRendering(String code, String doc) {
		ApplicationData.getInstance().rendering.put(code, doc);
	}
	
	protected static void setId(String id) {
		ApplicationData.getInstance().id = id;
	}
	public static String getId() {
		return ApplicationData.getInstance().id;
	}
	private String id = "REPORTING-SCORING";
}

