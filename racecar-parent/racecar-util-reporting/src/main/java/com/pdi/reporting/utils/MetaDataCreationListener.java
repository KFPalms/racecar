package com.pdi.reporting.utils;


import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import org.xhtmlrenderer.pdf.DefaultPDFCreationListener;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfString;
import com.lowagie.text.pdf.PdfWriter;
import com.pdi.reporting.constants.ReportingConstants;

public class MetaDataCreationListener extends DefaultPDFCreationListener {
	
	// Mark Arnold May 15 2015
	// This class is used to get the watermark value
	// into the title metadata field of a pdf
	// This is set up to also add other values into the 
    // metadata tags if we choose do to so.
	
	
	 
	HashMap<String, String> metaTags = new HashMap<String, String>();
	
	public void getMetaData() {
		
		metaTags.put("title", ReportingConstants.WATERMARK);
				
        return;
    }
	
	
	 public void preOpen(ITextRenderer iTextRenderer) {
	   
		 for (Map.Entry<String, String> entry : this.metaTags.entrySet()) 
		 {
		 String key = entry.getKey();
		 PdfString val = new
				 PdfString(entry.getValue(), PdfObject.TEXT_UNICODE);
		 iTextRenderer.getWriter().setViewerPreferences(PdfWriter.DisplayDocTitle);
         if (key == null ? "title" == null : key.equals("title")) {
             iTextRenderer.getWriter().getInfo().put(PdfName.TITLE, val);
         } else if (key == null ? "author" == null : key.equals("author")) {
             iTextRenderer.getWriter().getInfo().put(PdfName.AUTHOR, val);
         } else if (key == null ? "subject" == null : key.equals("subject")) {
             iTextRenderer.getWriter().getInfo().put(PdfName.SUBJECT, val);
         } else if (key == null ? "creator" == null : key.equals("creator")) {
             iTextRenderer.getWriter().getInfo().put(PdfName.CREATOR, val);
         } else if (key == null ? "description" == null : key.equals("description")) {
             iTextRenderer.getWriter().getInfo().put(PdfName.DESC, val);
         } else if (key == null ? "keywords" == null : key.equals("keywords")) {
             iTextRenderer.getWriter().getInfo().put(PdfName.KEYWORDS, val);
         }
	           
	    }
	 }

}
