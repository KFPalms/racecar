/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.reporting.request;


import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyObject;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lowagie.text.Document;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfReader;
import com.pdi.properties.PropertyLoader;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.GroupReport;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.Report;
import com.pdi.reporting.report.ReportDataException;
import com.pdi.reporting.response.ReportingResponse;
import com.pdi.reporting.utils.ReportingUtils;
import com.pdi.webservice.NhnWebserviceUtils;

/**
 * Generic container request for submitting multiple reports
 * Contains an array of reports that are being requested
 * along with other useful information
 * 
 *
 * @author		Gavin Myers
 */
public class ReportingRequest
{
	private static final Logger log = LoggerFactory.getLogger(ReportingRequest.class);
	// 
	// Static data.
	//

	// Custom product code to trigger pdf concatenation for Aramco
	private static String ARAMCO_CODE = "ARAMCO";
	
	// Readiness Assessment project type name
	private static String RA_PROJECT_TYPE = "Readiness Assessment";
	
	// Assessment By Design project type name
	private static String ABYD_PROJECT_TYPE = "Assessment By Design - Online";
	
	// The name of the report to concatenate to the Aramco Readiness report
	private static String ARAMCO_REDI_RPT_NAME = "Aramco_Understanding_Readiness_Rpt.pdf";

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private ArrayList<Report> _reports;
	
	private Map<Integer, org.w3c.dom.Document> projectInfoMap = new HashMap<Integer, org.w3c.dom.Document>();

	//
	// Constructors.
	//
	public ReportingRequest()
	{
		
	}
	
	//
	// Instance methods.
	//
	
	/*
	 * Returns a report built on a request
	 * NOTE: DOES NOT THREAD, SIMPLY DOES JOB
	 * threading should be handled by the application
	 * calling this. This application assumes
	 * that NOW is the time to build the requested
	 * report regardless of size
	 */
//	@SuppressWarnings("unchecked")
	public ReportingResponse generateReports()
		throws Exception
	{
		log.debug("ReportingRequest- generateReports().......");
		StringBuffer allReports = new StringBuffer();
		allReports.append("<html>");
		@SuppressWarnings("unused")
		boolean generateAllReports = false;
		@SuppressWarnings("unused")
		int countIndividualSummaryReports = 0;
		@SuppressWarnings("unused")
		int counter = 0;
		if(_reports == null) {
			//throw new Exception("ERROR: 991004 - ReportingRequest.generateReports() no reports to generate! No participant has valid data, or there was an error while building the report list.");
			//String msg = "Participant has no valid instrument results, norms have not been selected, or an error was detected while gathering participant instrument information."; 
			String msg = "Possible causes:<ul><li>Participant has no valid instrument results</li><li>Norms have not been selected</li><li>An error was detected while gathering participant instrument information</li><li>A target level has not been set for the project</li></ul>"; 
			log.error("ReportingRequest:  {}", msg);	// Let the sysout log know
			throw new ReportDataException(msg);
		}
		
		// there is probably a better way to do this.
		// The AbyD extract is generating three complete reports (spreadsheets), each of which has all
		// the report data for each person in it. (i.e., 3 spreadsheets in the zip file, each with 
		// columns for all of the participants.  what i need is one spreadsheet with all 
		// the data in it.  so i'm doing this the cheaty way, and just removing the extraneous
		// reports.... 
		//if(_reports.get(0).getReportCode().equals("ABYD_EXTRACT") && _reports.size() > 1){			
		if(_reports.get(0).getReportCode().equals(ReportingConstants.REPORT_CODE_ABYD_RAW_EXTRACT) && _reports.size() > 1){			
			int size = _reports.size();
			for(int numRpts = size; numRpts > 1; numRpts--)
			{
				this._reports.remove(numRpts-1);
			}
			
		}

		//int idx = 0;
		for(Report report : _reports)
		{
			//idx++;
			if(report.getName() == null) {
				report.setName("ERROR_"+UUID.randomUUID());
			}
			log.debug("In report loop: rpt={}", report.getName());
			try
			{
				
				//Get the velocity engine going
				counter++;		
				//determine what report they want
				ReportRequestCodes codes = ReportingConstants.reportRequestMap.get(report.getReportCode());
				if (codes == null)
				{
					throw new IllegalStateException("No codes set for " + report.getReportCode() + ".  Unable to generate report.");
				}
				
	        	String groovyPath = "/com/pdi/reporting/report/vm/"+codes.getScriptId()+".groovy";    	     
				String reportPath = "/com/pdi/reporting/report/vm/"+codes.getReportTemplateId()+".html";
//				System.out.println("groovyPath .... " + groovyPath);
//				System.out.println("reportPath .... " + reportPath);
//				System.out.println("report.reportType  : " + report.getReportType());
//				if(report.getReportType() == ReportingConstants.REPORT_TYPE_GROUP_EXTRACT ||
//				   report.getReportType() == ReportingConstants.REPORT_TYPE_INDIVIDUAL_EXTRACT )
				if(report.getReportType().equalsIgnoreCase(ReportingConstants.REPORT_TYPE_GROUP_EXTRACT) &&
				   report.getReportCode().equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_RAW_EXTRACT) )
				{
					reportPath = "/com/pdi/reporting/report/vm/"+codes.getExtractTemplateId()+".html";
				}

	        	String groovy = ReportingUtils.pathToString(groovyPath);
	        	
				//add config data
	        	report.addConfigurationData("global.url", PropertyLoader.getProperty("com.pdi.reporting.application", "global.url"));

				if(groovy != null)
				{
		        	GroovyClassLoader gcl = new GroovyClassLoader();
					try
					{
//		        		GroovyClassLoader gcl = new GroovyClassLoader();
			        	@SuppressWarnings("rawtypes")
						Class groovyClass = gcl.parseClass(groovy);
			        	GroovyObject groovyObject = (GroovyObject) groovyClass.newInstance();
			        	Object[] args = {};
		        	
//						if(report.getReportType() == ReportingConstants.REPORT_TYPE_GROUP ||
//						   report.getReportType() == ReportingConstants.REPORT_TYPE_GROUP_DATA ||
//						   report.getReportType() == ReportingConstants.REPORT_TYPE_GROUP_EXTRACT)
//						{
						if(report.getReportType() == ReportingConstants.REPORT_TYPE_GROUP ||
						   report.getReportType() == ReportingConstants.REPORT_TYPE_GROUP_DATA	 ||
						   report.getReportType() == ReportingConstants.REPORT_TYPE_GROUP_EXTRACT)
						{
							//Group report
							GroupReport group = (GroupReport)report;
				        	groovyObject.setProperty("groupReport", group);
						} else {
							//Individual report
							IndividualReport individual = (IndividualReport)report;
				        	groovyObject.setProperty("individualReport", individual);
						}
	
						if(report.getReportType().equals(ReportingConstants.REPORT_TYPE_SCORE ))
						{
							groovyObject.invokeMethod("score", args);
						} else {
	
							groovyObject.invokeMethod("generate", args);
						}
						
						if(PropertyLoader.getProperty("com.pdi.reporting.application", "application.isDebugMode").equalsIgnoreCase("1")) {
							
							// Write to disk with FileOutputStream
							FileOutputStream f_out = new 
								FileOutputStream(codes.getReportTemplateId()+".report.data");
							ObjectOutputStream obj_out = new
								ObjectOutputStream (f_out);
							obj_out.writeObject ( report );
							
							obj_out.close();
						}
					}
					finally
					{
						gcl.close();
					}
				}
								
				if(report.getReportType().equalsIgnoreCase(ReportingConstants.REPORT_TYPE_GROUP) ||
				   report.getReportType().equalsIgnoreCase(ReportingConstants.REPORT_TYPE_INDIVIDUAL))
				{

					// Save the intermediate HTML if in debug mode
					if(PropertyLoader.getProperty("com.pdi.reporting.application", "application.isDebugMode").equalsIgnoreCase("1")) {
					
						//BufferedWriter out = new BufferedWriter(new FileWriter(codes.getReportTemplateId()+".html"));
						BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(codes.getReportTemplateId()+".html"),"UTF8"));

						out.write(ReportingUtils.renderHTML(reportPath,report));
						out.close();
						
					}

					//report.setData(ReportingUtils.renderTemplate(reportPath,report));
					ByteArrayOutputStream rptOs = ReportingUtils.renderTemplate(reportPath,report);
					
					// Code to append a pdf to another pdf
					// for the dashboard reports..
					//if (report.getReportCode().equals(ReportingConstants.REPORT_CODE_ABYD_FIT_REPORT)  ||
					//	  report.getReportCode().equals(ReportingConstants.REPORT_CODE_ABYD_READINESS_REPORT) ||
					//	  report.getReportCode().equals(ReportingConstants.REPORT_CODE_ABYD_DEVELOPMENT_REPORT))
					if (report.getReportCode().equals(ReportingConstants.REPORT_CODE_ABYD_READINESS_REPORT))
					{
						String projId = report.getConfigurationData("ProjId");
						boolean doIt = false;
						
						NhnWebserviceUtils nws = new NhnWebserviceUtils();
						org.w3c.dom.Document projectInfo = projectInfoMap.get(Integer.valueOf(projId));
						if (projectInfo == null){
							projectInfoMap.put(Integer.valueOf(projId), nws.fetchProjectInfo(projId));
							projectInfo = projectInfoMap.get(Integer.valueOf(projId));
						}

						
						
						// ...on a project that has a product code for Aramco...
						ArrayList<String> productCodes = nws.getProjectProductCodes(projectInfo);
						if (productCodes.contains(ARAMCO_CODE))
						{
							// ...and an RA project type
							String type = nws.getProjectTypeName(projectInfo);
							if (RA_PROJECT_TYPE.equalsIgnoreCase(type) || ABYD_PROJECT_TYPE.equalsIgnoreCase(type))
							{
								doIt = true;
							}
						}
						
						InputStream is = null;
						if (doIt)
						{
							// get the second report
							String aramcoTpltName = ARAMCO_REDI_RPT_NAME;
							is = ReportingRequest.class.getClassLoader().getResourceAsStream(aramcoTpltName);
							if (is == null || is.available() < 1)
							{
								System.out.println("Template (" + ARAMCO_REDI_RPT_NAME + ") is not available.  Concatenation skipped.");
								doIt = false;
							}
						}
						
						if (doIt)
						{
							// Convert the outputStream to an inputStream
							InputStream rptIs = new ByteArrayInputStream(rptOs.toByteArray());

							// Do the concatenate
							OutputStream os = new ByteArrayOutputStream();

							InputStream[] inp = new InputStream[2];
							inp[0] = rptIs;
							inp[1] = is;
							Document outDoc = new Document();
							//PdfCopy copy = new PdfCopy(outDoc, new FileOutputStream("OutputPDFDocument.pdf"));
							PdfCopy copy = new PdfCopy(outDoc, os);
							outDoc.open();

							PdfReader ReadInputPDF;
							
							int numberOfPages;
							  for (int i = 0; i < inp.length; i++) {
									ReadInputPDF = new PdfReader(inp[i]);
							        numberOfPages = ReadInputPDF.getNumberOfPages();
							        for (int page = 0; page < numberOfPages; )
							        {
							        	copy.addPage(copy.getImportedPage(ReadInputPDF, ++page));
							        }
							  }
							outDoc.close();

							rptOs = (ByteArrayOutputStream)os;
						}	// End of second if(doIt)
					}	// End of if Readiness report (the whole concatenation process)

					report.setData(rptOs);

//REMOVING THIS PER ABD-352 / NHN-1714					
//					if(report.getReportCode().equalsIgnoreCase(ReportingConstants.REPORT_CODE_TLT_INDIVIDUAL_SUMMARY)) 
//					{
//						// still have all data in _scriptedData... 
//						countIndividualSummaryReports ++;
//						if(countIndividualSummaryReports > 1){
//							//generateAllReports = true; //only set when there is more than one individual report (multiple participants selected)  
//							
//						}
//						String tempReport = ReportingUtils.renderHTML(reportPath,report);
//						
//						// testing
//						//	System.out.println("reporting request 180... tempreport html.  " + tempReport);
//						// end testing
//						
//						tempReport = tempReport.replace("<html>", "");
//						tempReport = tempReport.replace("</html>", "");
//						allReports.append(tempReport);
//						
//					}

				}
//				else if (report.getReportType() == ReportingConstants.REPORT_TYPE_GROUP_EXTRACT ||
//				 report.getReportType() == ReportingConstants.REPORT_TYPE_INDIVIDUAL_EXTRACT)
//		{
//
//			report.setData(ReportingUtils.renderExtract(reportPath,report));
//		
//		} 
				else if (report.getReportType().equalsIgnoreCase(ReportingConstants.REPORT_TYPE_GROUP_EXTRACT) &&
						 report.getReportCode().equalsIgnoreCase(ReportingConstants.REPORT_CODE_ABYD_RAW_EXTRACT) )
				{
					report.setData(ReportingUtils.renderExtract(reportPath,report));
				} 
				else
				{
					//This is a data request, no need to hog processor time with building a PDF
				}

			} catch(Exception e) {
				this._reports.remove(report);
				e.printStackTrace();
				break;
			}
		}
		//System.out.println("End of report loop: idx=" + idx + ", rptCnt=" + _reports.size());

//REMOVING THIS PER ABD-352 / NHN-1714
//		if(counter > 1 && generateAllReports) {
//			allReports.append("</html>");
//			//System.out.println(allReports);
//			try {
//				//TODO: This system for building a complete report package isn't elegant, it doesn't work for landscape/portrait combinations, I don't like messing with large strings
//				//if(this._reports.)
//				Report r = new Report();
//				r.setData(ReportingUtils.renderHTMLtoPDF(allReports.toString()));
//				r.setName("_All_Individual_Reports.pdf");
//				r.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);
//			r.setReportCode(ReportingConstants.REPORT_CODE_ALL_REPORTS);
//			//NOT ADDING THIS REPORT PER Jira task 	 ABD-352	-Keith 9/2010
//				this._reports.add(r);
//				/*
//				 * debug the all reports
//				BufferedWriter out = new BufferedWriter(new FileWriter("_All_Individual_Reports.html"));
//				out.write(allReports);
//				out.close();
//				*/
//			}
//			 catch(Exception e) {
//				e.printStackTrace();
//			}
//		}
		
		ReportingResponse response = new ReportingResponse();
		response.setReports(this.getReports());
		response.setResponseCode(ReportingConstants.RESPONSE_TYPE_COMPLETE);

		log.debug("Finished GenerateReports.  Returning ReportingResponse");
		return response;
	}

	//
	//	Instance variable access
	//
	
	public void addReport(Report r)
	{
		if(_reports == null)
		{
			_reports = new ArrayList<Report>();
		}
		_reports.add(r);
	}
	
	public void removeReport(Report r)
	{
		if(_reports == null)
		{
			return;
		}
		_reports.remove(r);
	}


	public void setReports(ArrayList<Report> reports)
	{
		_reports = reports;
	}


	public ArrayList<Report> getReports()
	{
		return _reports;
	}
}
