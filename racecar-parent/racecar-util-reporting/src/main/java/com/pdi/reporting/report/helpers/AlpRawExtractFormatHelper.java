/**
 * Copyright (c) 2017 Korn Ferry Hay Group
 * All rights reserved.
 */
package com.pdi.reporting.report.helpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.pdi.reporting.report.ExtractItem;
import com.pdi.reporting.report.GroupReport;
import com.pdi.reporting.report.IndividualReport;

public class AlpRawExtractFormatHelper
{
	//
	// Static Variables
	//
	private static List<String> TRAITS = new ArrayList<String>(Arrays.asList("Cr", "Ta", "Ss", "Cf", "Ri", "Ad",
			"Af", "Na", "Pe", "As", "Sa", "Ie", "Tr", "En", "Mi", "Co", "Hu", "In", "Du", "St", "Od"));
	private static List<String> DRIVERS = new ArrayList<String>(
			Arrays.asList("STRC", "CHAL", "POWR", "COLL", "BALA", "INDY"));

	//
	// Constructor
	//
	public AlpRawExtractFormatHelper()
	{
		// Default constructor
	}
	
	public ArrayList<ExtractItem> setUpOrder(GroupReport gr)
	{
		ArrayList<ExtractItem> eItems = new ArrayList<ExtractItem>();

//		// Everybody in the same extract should have the same Eval Guide and
//		// Integration Grid layout.  The data for them, however, is (redundantly)
//		// in the Individual Report display data.  Pick the first person's
//		// IR object and get IR for EG and IG counts.
//		
//		IndividualReport ir = gr.getIndividualReports().get(0);

		// Participant info
		eItems.add(new ExtractItem("Ppt", "Participant", ExtractItem.SRC_INDV, ExtractItem.DATA_STRING));
		
		// Responses
		for (int i = 1; i < 206; i++)
		{
			eItems.add(new ExtractItem("R" + i, "" + i, ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		}

		// Raw Traits (hatEta)
		for(int i = 0; i < TRAITS.size(); i++)
		{
			String str = TRAITS.get(i);
			eItems.add(new ExtractItem(str, str, ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		}
		
		// Ravens
		eItems.add(new ExtractItem("Ravens", "Ravens", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));

		// Raw Drivers (hatEtaDrivers)
		for(int i = 0; i < DRIVERS.size(); i++)
		{
			String str = DRIVERS.get(i);
			eItems.add(new ExtractItem(str, str, ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		}

		// Hipro
		eItems.add(new ExtractItem("Hipro", "HiPro", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));

		// Mobility
		eItems.add(new ExtractItem("Mob", "Mobility", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));

		//CLO
		eItems.add(new ExtractItem("Clo", "CLO", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));

		// Project info
		eItems.add(new ExtractItem("ProjId", "Project ID", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("ProjName", "Project Name", ExtractItem.SRC_INDV, ExtractItem.DATA_STRING));


		return eItems;
	}

}
