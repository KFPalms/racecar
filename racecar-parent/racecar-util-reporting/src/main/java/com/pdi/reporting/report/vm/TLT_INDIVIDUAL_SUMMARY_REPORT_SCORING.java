package com.pdi.reporting.report.vm;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;
import com.pdi.scoring.Norm;

public class TLT_INDIVIDUAL_SUMMARY_REPORT_SCORING {
	public IndividualReport individualReport;

	//
	// Static constants
	//

	/*
	 * parse
	 */
	private double parse(String value)
	{
		double d;
		try
		{
			d = Double.parseDouble(value);
		}
		catch(Exception e)
		{
			d = 0;
		}
		return d;
	}


	public IndividualReport generate()
	{
		try {
			return generate2();
		} catch (Exception e) {
			System.out.println("Error in generating a report, this is usually caused by bad data " + individualReport.toString());
			e.printStackTrace();
			individualReport.addScriptedData("STATUS", "NOT_FINISHED");
			individualReport.getScriptedData().put("ERROR", "Unable to generate report");
			return individualReport;
		}
	}
	public IndividualReport generate2() {
		//println "Input -- " + individualReport.displayDataToString();
		//System.out.println("TLT_INDIVIDUAL_SUMMARY_REPORT_SCORING  IndividualReport generate2().......");
		//Norm norm = new Norm();

		Calendar cal = Calendar.getInstance();
		// Date format changed per JIRA task TLT-186
		//SimpleDateFormat sdf = new SimpleDateFormat("MMMMM dd, yyyy");
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
		individualReport.addDisplayData("DATE", sdf.format(cal.getTime()));
		  
		//System.out.println("Groovy Started For: " + individualReport.getDisplayData("FIRST_NAME"));
		//individualReport.getReportData().each {instrumentKey, instrumentValue ->
		//	println "*********************************************";
		//	println instrumentKey;
		//	println "*********************************************";
		//	instrumentValue.getScoreData().each {scoreKey, scoreValue -> 
		//		println scoreKey + " = " + scoreValue;
		//	 
		//	}
		//} 
		Boolean fault = false;
		String errorString = "";
		if(individualReport.getReportData().get(ReportingConstants.RC_GPI) == null
			&& individualReport.getScriptedData().get(ReportingConstants.PART_COMPL_RPT).equals(ReportingConstants.COMPLETE_REPORT))
		{ 
			// They haven't finished the GPI
			individualReport.addScriptedData("STATUS", "NOT_FINISHED");
			errorString += "GPI has not been completed. <br/>";
			fault = true;
			//return individualReport;
		} 
		
		if(individualReport.getReportData().get(ReportingConstants.RC_LEI) == null
				&& individualReport.getScriptedData().get(ReportingConstants.PART_COMPL_RPT).equals(ReportingConstants.COMPLETE_REPORT))
		//if(individualReport.getReportData().get(ReportingConstants.RC_LEI) == null)
		{ 
			// They haven't finished the LEI
			individualReport.getScriptedData().put("STATUS", "NOT_FINISHED");
			errorString += "LEI has not been completed. <br/>";
			fault = true;
			//return individualReport;
		}  
		if(individualReport.getReportData().get(ReportingConstants.RC_CAREER_SURVEY) == null
				&& individualReport.getScriptedData().get(ReportingConstants.PART_COMPL_RPT).equals(ReportingConstants.COMPLETE_REPORT))
		//if(individualReport.getReportData().get(ReportingConstants.RC_CAREER_SURVEY) == null)
		{
			// They haven't finished the CS
			individualReport.getScriptedData().put("STATUS", "NOT_FINISHED");
			errorString += "Career Survey has not been completed. <br/>";
			fault = true;
			//return individualReport;
		}	
		if(fault){
			individualReport.getScriptedData().put("ERROR", errorString);
			return individualReport;
		}
		//System.out.println("All Report Data Found For: " + individualReport.getDisplayData("FIRST_NAME"));
		
		// This is supposed to catch the lack of norms - TLT Does not require norms
		/*
		if(individualReport.getDisplayData().get(ReportingConstants.DR_EGOCE+"_MEAN") == null)
		{
			// There are no GPI norms
			individualReport.getScriptedData().put("STATUS", "NOT_FINISHED");
			individualReport.getScriptedData().put("ERROR", "Norms are not present for this project, unable to generate report");
			return individualReport;
		}
		*/
		individualReport.getScriptedData().put("STATUS", "FINISHED");
				
		// Convert scale scores to PDI ratings (Looks like stanines, really)
		// Means and Standard deviations
		HashMap<String, String> tempScores = new HashMap<String, String>();

		//	CALC ZGEST (Cognitives -- Ravens... )
		boolean doRavSF = false;
		boolean doRavB = false;
		ReportData ravRd = individualReport.getReportData().get(ReportingConstants.RC_RAVENS_SF);
		doRavSF = (ravRd==null ? false : true);
		ravRd = individualReport.getReportData().get(ReportingConstants.RC_RAVENS_B);
		doRavB = (ravRd==null ? false : true);
		
		if (doRavSF && doRavB)
		{
			doRavSF = false;
			//doRavB = true;	// redundant... should already be true
		}
		
		if(new	Integer(individualReport.getDisplayData().get(ReportingConstants.RC_COGNITIVES_INCLUDED)).intValue() != 1)
		{
			// The Raven's is not assigned so don't include it...
			//tempScores.put(ReportingConstants.RAV_PSOLV, "0.0");
			tempScores.put(ReportingConstants.RAV_PSOLV, "");
			
		} else {
			//if(individualReport.getReportData().get(ReportingConstants.RC_RAVENS_SF) == null)
			if(! doRavSF && ! doRavB)
			{
				//System.out.println("the scored data is null... ");		
				individualReport.addScriptedData("STATUS", "NOT_FINISHED");
				//individualReport.getScriptedData().put("ERROR", "Ravens has not been completed, unable to generate report");
				individualReport.addDisplayData("RAVENS_MISSING", "MISSING");
				
				//tempScores.put(ReportingConstants.RAV_PSOLV, "0.0");
				//individualReport.getScriptedData().put("RAW_" + ReportingConstants.RAV_PSOLV, "0.0");
				//individualReport.getScriptedData().put("RAW_RAVENS_CORRECT", "0");
				tempScores.put(ReportingConstants.RAV_PSOLV, "");
				individualReport.getScriptedData().put("RAW_" + ReportingConstants.RAV_PSOLV, "");
				individualReport.getScriptedData().put("RAW_RAVENS_CORRECT", "");
			} else {
				// score the raven's
				double ravensScore = 0.0;
				if (doRavSF)
				{
					ravensScore = parse(individualReport.getReportData().get(ReportingConstants.RC_RAVENS_SF).getScoreData().get(ReportingConstants.RAVENS_SHORT));
				}
				if (doRavB)
				{
					ravensScore = parse(individualReport.getReportData().get(ReportingConstants.RC_RAVENS_B).getScoreData().get(ReportingConstants.RAVENS_B));
				}
				//System.out.println("Raven's raw score  " + ravensScore);
				individualReport.getScriptedData().put("RAW_RAVENS_CORRECT", ravensScore + "");
				//if (ravensScore != 0.0)
				//{
				//	ravensScore = (ravensScore - ReportingConstants.RAVENS_TLT_MEAN) / ReportingConstants.RAVENS_TLT_STDEV ;
				//}
				// Score of 0 is valid
				ravensScore = (ravensScore - ReportingConstants.RAVENS_TLT_MEAN) / ReportingConstants.RAVENS_TLT_STDEV ;

				//System.out.println("raven's initial normed score " + ravensScore);
				tempScores.put(ReportingConstants.RAV_PSOLV, ravensScore+ "");
				individualReport.getScriptedData().put("RAW_" + ReportingConstants.RAV_PSOLV, ravensScore+ "");
				
			}
		}
		
		//println "Ravens done... Start GPI"
		// CALC RAW PERSONALITY SCORES (GPI)
				
		// First, we need to reverse the score for two GPI scales.
		double gpiInd = parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_INDEPENDENCE));
		double gpiIndReversed = (gpiInd - 8) * (-1);
		double gpiNegAff = parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_NEG_AFFECT));
		double gpiNegAffReversed = (gpiNegAff - 7) * (-1);

		// Compute Raw Personality Scores
		// GPI_INTEN
		double gpiInten = 
			parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_INNOVATION)) +
			parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_THOUGHT_FOCUS)) +
			parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_VISION));
		tempScores.put(ReportingConstants.GPI_INTEN, gpiInten+ "");
		individualReport.getScriptedData().put("RAW_" + ReportingConstants.GPI_INTEN, gpiInten+ "");
		//System.out.println("79:gpiInten+ "" " + gpiInten+ "");
		//System.out.println("79:tempScores.get(ReportingConstants.GPI_INTEN) " + tempScores.get(ReportingConstants.GPI_INTEN));

			
		//GPI_ATD
		double gpiAtd = parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_ATTN_DETAIL));
		tempScores.put(ReportingConstants.GPI_ATD, gpiAtd+ "");
		individualReport.getScriptedData().put("RAW_" + ReportingConstants.GPI_ATD, gpiAtd+ "");
		//System.out.println(" ReportingConstants.GPI_ATD  " + tempScores.get(ReportingConstants.GPI_ATD));
		
		// GPI_IMIN
		double gpiImin = 
			parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_TAKE_CHARGE)) +
			parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_INFLUENCE)) + 
			parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_TRUST)) +
			parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_SOCIAL_ASTUTE));
		tempScores.put(ReportingConstants.GPI_IMIN, gpiImin+ "");
		individualReport.getScriptedData().put("RAW_" + ReportingConstants.GPI_IMIN, gpiImin+ "");
		
		//GPI_INEN
		double gpiInen = 
			parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_SOCIABILITY)) +
			parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_CONSIDERATION)) +
			parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_EMPATHY));
		tempScores.put(ReportingConstants.GPI_INEN, gpiInen+ "");
		individualReport.getScriptedData().put("RAW_" + ReportingConstants.GPI_INEN, gpiInen+ "");
		
		//GPI_ACHD	
		double gpiAchd = 
			parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_ENERGY_LEV)) +
			parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_DESIRE_ACHIEVE));
		tempScores.put(ReportingConstants.GPI_ACHD, gpiAchd+ "");
		individualReport.getScriptedData().put("RAW_" + ReportingConstants.GPI_ACHD, gpiAchd+ "");
		
		//GPI_ADVD
		double gpiAdvd = 
			parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_RISK_TAKING)) +
			parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_DESIRE_ADVANCE));
		tempScores.put(ReportingConstants.GPI_ADVD, gpiAdvd+ "");
		individualReport.getScriptedData().put("RAW_" + ReportingConstants.GPI_ADVD, gpiAdvd+ "");
		
		//GPI_COOR
		double gpiCoor = 
			parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_INTERDEPENDENCE)) +
			parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_DUTIFUL)) +
			gpiIndReversed;
		tempScores.put(ReportingConstants.GPI_COOR, gpiCoor+ "");
		individualReport.getScriptedData().put("RAW_" + ReportingConstants.GPI_COOR, gpiCoor+ "");
		
		//GPI_FLAD	
		double gpiFlad = 
			parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_ADAPTABILITY)) +
			gpiNegAffReversed +
			parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_OPTIMISM)) +
			parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_STRESS_TOL)) +
			parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_SELF_AWARE));
		tempScores.put(ReportingConstants.GPI_FLAD, gpiFlad+ "");
		individualReport.getScriptedData().put("RAW_" + ReportingConstants.GPI_FLAD, gpiFlad+ "");
		
		//println "Start Derail..."	
		//DR_DERAIL
		double drDerail = 
			parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_EGO)) +
			parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_MANIPULATION)) +
			parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_MICRO_MANAGE)) +
			parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_PASS_AGGRESS));
		tempScores.put(ReportingConstants.DR_DERAIL, drDerail+ "");
		individualReport.getScriptedData().put("RAW_" + ReportingConstants.DR_DERAIL, drDerail+ "");
		
		//DERAILMENT RISK SCORES:
		double gpiEgo = parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_EGO));
		tempScores.put(ReportingConstants.DR_EGOCE, gpiEgo+ "");
		individualReport.getScriptedData().put("RAW_" + ReportingConstants.DR_EGOCE, gpiEgo+ "");
		
		double gpiManipulation = parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_MANIPULATION));
		tempScores.put(ReportingConstants.DR_MANIP, gpiManipulation+ "");
		individualReport.getScriptedData().put("RAW_" + ReportingConstants.DR_MANIP, gpiManipulation+ "");
		
		double gpiMicroManage = parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_MICRO_MANAGE));
		tempScores.put(ReportingConstants.DR_MICRO, gpiMicroManage+ "");
		individualReport.getScriptedData().put("RAW_" + ReportingConstants.DR_MICRO, gpiMicroManage+ "");
		
		double gpiPassAggress = parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_PASS_AGGRESS));
		tempScores.put(ReportingConstants.DR_PASAG, gpiPassAggress+ "");
		individualReport.getScriptedData().put("RAW_" + ReportingConstants.DR_PASAG, gpiPassAggress+ "");
		
		//println "Start CS..."	
		// CAREER SURVEY SCORES 
		double csCareerGoals = parse(individualReport.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().get(ReportingConstants.CAREER_GOALS));
		tempScores.put(ReportingConstants.CS_LDRAS, csCareerGoals+ "");
		
		double csCareerDrivers = parse(individualReport.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().get(ReportingConstants.CAREER_DRIVERS));
		tempScores.put(ReportingConstants.CS_CRDRV, csCareerDrivers+ "");
		
		double csLearningOrient = parse(individualReport.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().get(ReportingConstants.LEARNING_ORIENT));
		tempScores.put(ReportingConstants.CS_LRNOR, csLearningOrient+ "");
		
		double csExperienceOrient = parse(individualReport.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().get(ReportingConstants.EXPERIENCE_ORIENT));
		tempScores.put(ReportingConstants.CS_EXPOR, csExperienceOrient+ "");
		
		//println "Start LEI..."	
		// CALC LEADERSHIP SCORES (LEI)
		//LEI_BUSOP
		double leiBusop =
			parse(individualReport.getReportData().get(ReportingConstants.RC_LEI).getScoreData().get(ReportingConstants.LEI_OPERATIONS)) +
			parse(individualReport.getReportData().get(ReportingConstants.RC_LEI).getScoreData().get(ReportingConstants.LEI_PROJ_MGMT_IMPL));
		tempScores.put(ReportingConstants.LEI_BUSOP,leiBusop+ "");
		individualReport.getScriptedData().put("RAW_" + ReportingConstants.LEI_BUSOP,leiBusop+ "");
		
		//System.out.println(individualReport.getReportData().get(ReportingConstants.RC_LEI).getScoreData().get(ReportingConstants.LEI_OPERATIONS));
		//System.out.println(individualReport.getReportData().get(ReportingConstants.RC_LEI).getScoreData().get(ReportingConstants.LEI_PROJ_MGMT_IMPL));
		//System.out.println("IND. REPT. leiBusop " + leiBusop+ "");
		
		//LEI_HTC
		double leiHtc = 
			parse(individualReport.getReportData().get(ReportingConstants.RC_LEI).getScoreData().get(ReportingConstants.LEI_DIFF_STAFFING)) +
			parse(individualReport.getReportData().get(ReportingConstants.RC_LEI).getScoreData().get(ReportingConstants.LEI_INHERITED_PROB));
		tempScores.put(ReportingConstants.LEI_HTC,leiHtc+ "");
		individualReport.getScriptedData().put("RAW_" + ReportingConstants.LEI_HTC,leiHtc+ "");
		
		//LEI_HVIS
		double leiHvis = 
			parse(individualReport.getReportData().get(ReportingConstants.RC_LEI).getScoreData().get(ReportingConstants.LEI_HI_CRIT_VISIBLE)) +
			parse(individualReport.getReportData().get(ReportingConstants.RC_LEI).getScoreData().get(ReportingConstants.LEI_HIGH_RISK));
		tempScores.put(ReportingConstants.LEI_HVIS,leiHvis+ "");
		individualReport.getScriptedData().put("RAW_" + ReportingConstants.LEI_HVIS,leiHvis+ "");
		
		//LEI_GTBUS
		double leiGtbus = 
			parse(individualReport.getReportData().get(ReportingConstants.RC_LEI).getScoreData().get(ReportingConstants.LEI_BUS_DEV_MARKTNG)) +
			parse(individualReport.getReportData().get(ReportingConstants.RC_LEI).getScoreData().get(ReportingConstants.LEI_BUS_GROWTH));
		tempScores.put(ReportingConstants.LEI_GTBUS,leiGtbus+ "");
		individualReport.getScriptedData().put("RAW_" + ReportingConstants.LEI_HVIS,leiHvis+ "");
		
		//LEI_PDEV
		double leiPdev = 
			parse(individualReport.getReportData().get(ReportingConstants.RC_LEI).getScoreData().get(ReportingConstants.LEI_SELF_DEV)) + 
			parse(individualReport.getReportData().get(ReportingConstants.RC_LEI).getScoreData().get(ReportingConstants.LEI_EXTRACURRICULAR));
		tempScores.put(ReportingConstants.LEI_PDEV, leiPdev+ "");
		individualReport.getScriptedData().put("RAW_" + ReportingConstants.LEI_PDEV, leiPdev+ "");
		
		// NOW CALC Z-SCORES FROM THE tempScores, 
		// and CALC PERCENTILES FROM Z'S,
		// finally, GET STANINES FROM PERCENTILE
		double zScore = 0.0;
		double percentScore = 0.0;
		int stanine = 0;
		double mean = 0.0;
		double stdev = 0.0;
		
		//println "Check /do cog Z..."
		// cognitive score
		//if( ! tempScores.get(ReportingConstants.RAV_PSOLV).equals("0.0")  )
		if( ! tempScores.get(ReportingConstants.RAV_PSOLV).equals("")  )
		{
			zScore =  parse(tempScores.get(ReportingConstants.RAV_PSOLV));
			// can i get the mean, std dev for RAV_PSOLV? or ZGEST?
			mean = parse(individualReport.getDisplayData(ReportingConstants.RAV_PSOLV+"_MEAN"));
			stdev = parse(individualReport.getDisplayData(ReportingConstants.RAV_PSOLV+"_STDEV"));
			
			//System.out.println("RAVEN'S COG FINAL CALCS...  ");
			//System.out.println("initial zscore: " + tempScores.get(ReportingConstants.RAV_PSOLV));			
			//System.out.println("     mean " + mean);
			//System.out.println("     stdev " + stdev);
			
			// do the zScore w/ the group ZGEST
			double  zScore2 = (zScore - mean)/stdev;
			
			//percentScore = Norm.calcDoublePctlFromZ(zScore);
			percentScore = Norm.calcDoublePctlFromZ(zScore2);
			//System.out.println("    percentScore " + percentScore);
			
			stanine = findStanineFromPctl( percentScore );			
			//System.out.println("     stanine " + stanine);
						
			individualReport.getScriptedData().put(ReportingConstants.RAV_PSOLV, stanine+ "");
		} else {
			individualReport.getScriptedData().put(ReportingConstants.RAV_PSOLV, "");
		}

		//println "Personality Z..."	
		// personality scores
		// NOTE:  yeah, could have done this in fewer lines, and more elegantly, but the number 
		// 		   of parens were driving me nutz... this is easier to follow...
		mean = parse(individualReport.getDisplayData(ReportingConstants.GPI_INTEN+"_MEAN"));
		stdev = parse(individualReport.getDisplayData(ReportingConstants.GPI_INTEN+"_STDEV"));
		if(tempScores.get(ReportingConstants.GPI_INTEN).equals("0.0"))
		{
			zScore = 0.0 - mean / stdev;
		} else {
			zScore = ( parse(tempScores.get(ReportingConstants.GPI_INTEN)) - mean  ) / stdev;	
		}
		percentScore = Norm.calcDoublePctlFromZ(zScore);
		stanine = findStanineFromPctl(percentScore);
		individualReport.getScriptedData().put(ReportingConstants.GPI_INTEN, stanine+ "");
		
		mean = parse(individualReport.getDisplayData(ReportingConstants.GPI_ATD+"_MEAN"));
		stdev = parse(individualReport.getDisplayData(ReportingConstants.GPI_ATD+"_STDEV"));
		if(tempScores.get(ReportingConstants.GPI_ATD).equals("0.0")){
			zScore = 0.0 - mean / stdev;
		} else {
			zScore =  ( parse(tempScores.get(ReportingConstants.GPI_ATD) ) - mean  ) / stdev;
		}		
		percentScore = Norm.calcDoublePctlFromZ(zScore);		
		stanine = findStanineFromPctl(percentScore);
		individualReport.getScriptedData().put(ReportingConstants.GPI_ATD, stanine+ "");
		//System.out.println("ReportingConstants.GPI_ATD " + percentScore+ "" + " | " + stanine+ "");
		
		mean = parse(individualReport.getDisplayData(ReportingConstants.GPI_IMIN+"_MEAN"));
		stdev = parse(individualReport.getDisplayData(ReportingConstants.GPI_IMIN+"_STDEV"));
		if(tempScores.get(ReportingConstants.GPI_IMIN).equals("0.0"))
		{
			zScore = 0.0 - mean / stdev;
		} else {
			zScore =  ( parse(tempScores.get(ReportingConstants.GPI_IMIN) ) - mean  ) / stdev;
		}
		percentScore = Norm.calcDoublePctlFromZ(zScore);
		stanine = findStanineFromPctl(percentScore);
		individualReport.getScriptedData().put(ReportingConstants.GPI_IMIN, stanine+ "");
		//System.out.println("ReportingConstants.GPI_IMIN " + percentScore+ "" + " | " + stanine+ "");
		
		mean = parse(individualReport.getDisplayData(ReportingConstants.GPI_INEN+"_MEAN"));
		stdev = parse(individualReport.getDisplayData(ReportingConstants.GPI_INEN+"_STDEV"));
		if(tempScores.get(ReportingConstants.GPI_INEN).equals("0.0"))
		{
			zScore = 0.0 - mean / stdev;
		} else {
			zScore =  ( parse(tempScores.get(ReportingConstants.GPI_INEN) ) - mean  ) / stdev;
		}		
		percentScore = Norm.calcDoublePctlFromZ(zScore);
		stanine = findStanineFromPctl(percentScore);
		individualReport.getScriptedData().put(ReportingConstants.GPI_INEN, stanine+ "");
		//System.out.println("ReportingConstants.GPI_INEN " + percentScore+ "" + " | " + stanine+ "");
				
		mean = parse(individualReport.getDisplayData(ReportingConstants.GPI_ACHD+"_MEAN"));
		stdev = parse(individualReport.getDisplayData(ReportingConstants.GPI_ACHD+"_STDEV"));
		if(tempScores.get(ReportingConstants.GPI_ACHD).equals("0.0"))
		{
			zScore = 0.0 - mean / stdev;
		} else {
			zScore =  ( parse(tempScores.get(ReportingConstants.GPI_ACHD) ) - mean  ) / stdev;
		}				
		percentScore = Norm.calcDoublePctlFromZ(zScore);
		stanine = findStanineFromPctl(percentScore);
		individualReport.getScriptedData().put(ReportingConstants.GPI_ACHD, stanine+ "");
		//System.out.println("ReportingConstants.GPI_ACHD " + percentScore+ "" + " | " + stanine+ "");
		
		mean = parse(individualReport.getDisplayData(ReportingConstants.GPI_ADVD+"_MEAN"));
		stdev = parse(individualReport.getDisplayData(ReportingConstants.GPI_ADVD+"_STDEV"));
		if(tempScores.get(ReportingConstants.GPI_ADVD).equals("0.0"))
		{
			zScore = 0.0 - mean / stdev;
		} else {
			zScore =  ( parse(tempScores.get(ReportingConstants.GPI_ADVD) ) - mean  ) / stdev;
		}		
		percentScore = Norm.calcDoublePctlFromZ(zScore);
		stanine = findStanineFromPctl(percentScore);
		individualReport.getScriptedData().put(ReportingConstants.GPI_ADVD, stanine+ "");		
		//System.out.println("ReportingConstants.GPI_ADVD " + percentScore+ "" + " | " + stanine+ "");
		
		mean = parse(individualReport.getDisplayData(ReportingConstants.GPI_COOR+"_MEAN"));
		stdev = parse(individualReport.getDisplayData(ReportingConstants.GPI_COOR+"_STDEV"));
		if(tempScores.get(ReportingConstants.GPI_COOR).equals("0.0"))
		{
			zScore = 0.0 - mean / stdev;
		} else {
			zScore =  ( parse(tempScores.get(ReportingConstants.GPI_COOR) ) - mean  ) / stdev;
		}		
		percentScore = Norm.calcDoublePctlFromZ(zScore);
		stanine = findStanineFromPctl(percentScore);
		individualReport.getScriptedData().put(ReportingConstants.GPI_COOR, stanine+ "");
		//System.out.println("ReportingConstants.GPI_COOR " + percentScore+ "" + " | " + stanine+ "");
		
		mean = parse(individualReport.getDisplayData(ReportingConstants.GPI_FLAD+"_MEAN"));
		stdev = parse(individualReport.getDisplayData(ReportingConstants.GPI_FLAD+"_STDEV"));
		if(tempScores.get(ReportingConstants.GPI_FLAD).equals("0.0"))
		{
			zScore = 0.0 - mean / stdev;
		} else {
			zScore =  ( parse(tempScores.get(ReportingConstants.GPI_FLAD) ) - mean  ) / stdev;
		}		
		percentScore = Norm.calcDoublePctlFromZ(zScore);
		stanine = findStanineFromPctl(percentScore);
		individualReport.getScriptedData().put(ReportingConstants.GPI_FLAD, stanine+ "");
		//System.out.println("ReportingConstants.GPI_FLAD " + percentScore+ "" + " | " + stanine+ "");
		
		//println "Derail stanine..."
		// Derail Risk calc
		mean = parse(individualReport.getDisplayData(ReportingConstants.DR_DERAIL+"_MEAN"));
		stdev = parse(individualReport.getDisplayData(ReportingConstants.DR_DERAIL+"_STDEV"));
		//println "Derail:  mean=" + mean + ", stDev=" + stdev + ", score=" + tempScores.get(ReportingConstants.DR_DERAIL)
		if(tempScores.get(ReportingConstants.DR_DERAIL).equals("0.0"))
		{
			zScore = 0.0 - mean / stdev;
		} else {
			zScore =  ( parse(tempScores.get(ReportingConstants.DR_DERAIL) ) - mean  ) / stdev;
		}		
		percentScore = Norm.calcDoublePctlFromZ(zScore);
		stanine = findStanineFromPctl(percentScore);
		individualReport.getScriptedData().put(ReportingConstants.DR_DERAIL, stanine+ "");
		String rStr;
		if (stanine == 9) {
			rStr = ReportingConstants.LBL_HIGH;	//"High"
		}  else if (stanine == 8) {
			rStr = ReportingConstants.LBL_MODERATE;	//"Moderate"
		}  else if (stanine == 7 || stanine == 6 || stanine == 5 || stanine == 4 )  {
			rStr = ReportingConstants.LBL_LOW;	//"Low"
		}  else if (stanine == 3 || stanine == 2 || stanine == 1)  {
			rStr = ReportingConstants.LBL_MINIMAL;	//"Minimal"
		}  else  {
			rStr  = ReportingConstants.LBL_BLANK;	// Should never happen
		}
		individualReport.getScriptedData().put(ReportingConstants.DR_RISK_TEXT, rStr);
		//println "z=" + zScore + ", pctl=" + percentScore + ", st9=" + stanine + ", desc=" + rStr
		//System.out.println("ReportingConstants.DR_DERAIL " + percentScore+ "" + " | " + stanine+ "");
		
		// Leadership scores
		mean = parse(individualReport.getDisplayData(ReportingConstants.LEI_HTC+"_MEAN"));
		stdev = parse(individualReport.getDisplayData(ReportingConstants.LEI_HTC+"_STDEV"));
		if(tempScores.get(ReportingConstants.LEI_HTC).equals("0.0"))
		{
			zScore = 0.0 - mean / stdev;
		} else {
			zScore =  ( parse(tempScores.get(ReportingConstants.LEI_HTC) ) - mean  ) / stdev;
		}
		percentScore = Norm.calcDoublePctlFromZ(zScore);
		stanine = findStanineFromPctl(percentScore);
		individualReport.getScriptedData().put(ReportingConstants.LEI_HTC, stanine+ "");		
		//System.out.println("ReportingConstants.LEI_HTC " + percentScore+ "" + " | " + stanine+ "");
		
		mean = parse(individualReport.getDisplayData(ReportingConstants.LEI_GTBUS+"_MEAN"));
		stdev = parse(individualReport.getDisplayData(ReportingConstants.LEI_GTBUS+"_STDEV"));
		if(tempScores.get(ReportingConstants.LEI_GTBUS).equals("0.0"))
		{
			zScore = 0.0 - mean / stdev;
		} else {
			zScore =  ( parse(tempScores.get(ReportingConstants.LEI_GTBUS) ) - mean  ) / stdev;
		}		
		percentScore = Norm.calcDoublePctlFromZ(zScore);
		stanine = findStanineFromPctl(percentScore);
		individualReport.getScriptedData().put(ReportingConstants.LEI_GTBUS, stanine+ "");		
		//System.out.println("ReportingConstants.LEI_GTBUS " + percentScore+ "" + " | " + stanine+ "");
		
		mean = parse(individualReport.getDisplayData(ReportingConstants.LEI_HVIS+"_MEAN"));
		stdev = parse(individualReport.getDisplayData(ReportingConstants.LEI_HVIS+"_STDEV"));
		if(tempScores.get(ReportingConstants.LEI_HVIS).equals("0.0"))
		{
			zScore = 0.0 - mean / stdev;
		} else {
			zScore =  ( parse(tempScores.get(ReportingConstants.LEI_HVIS) ) - mean  ) / stdev;
		}			
		percentScore = Norm.calcDoublePctlFromZ(zScore);
		stanine = findStanineFromPctl(percentScore);
		individualReport.getScriptedData().put(ReportingConstants.LEI_HVIS, stanine+ "");		

		mean = parse(individualReport.getDisplayData(ReportingConstants.LEI_BUSOP+"_MEAN"));
		stdev =parse(individualReport.getDisplayData(ReportingConstants.LEI_BUSOP+"_STDEV"));
		if(tempScores.get(ReportingConstants.LEI_BUSOP).equals("0.0"))
		{
			zScore = 0.0 - mean / stdev;
		} else {
			zScore =  ( parse(tempScores.get(ReportingConstants.LEI_BUSOP) ) - mean  ) / stdev;
		}
		percentScore = Norm.calcDoublePctlFromZ(zScore);
		//System.out.println("%" + percentScore + " Mean: " + mean + " STDEV: " + stdev + " SCORE: " + tempScores.get(ReportingConstants.LEI_BUSOP) + " Z: " + zScore);
		stanine = findStanineFromPctl(percentScore);
		individualReport.getScriptedData().put(ReportingConstants.LEI_BUSOP, stanine+ "");		
		//System.out.println("ReportingConstants.LEI_BUSOP " + percentScore+ "" + " | " + stanine+ "");
		
		mean = parse(individualReport.getDisplayData(ReportingConstants.LEI_PDEV+"_MEAN"));
		stdev = parse(individualReport.getDisplayData(ReportingConstants.LEI_PDEV+"_STDEV"));
		if(tempScores.get(ReportingConstants.LEI_PDEV).equals("0.0"))
		{
			zScore = 0.0 - mean / stdev;
		} else {
			zScore =  ( parse(tempScores.get(ReportingConstants.LEI_PDEV) ) - mean  ) / stdev;
		}		
		percentScore = Norm.calcDoublePctlFromZ(zScore);
		stanine = findStanineFromPctl(percentScore);
		individualReport.getScriptedData().put(ReportingConstants.LEI_PDEV, stanine+ "");		
		//System.out.println("ReportingConstants.LEI_PDEV " + percentScore+ "" + " | " + stanine+ "");
				
		// Derailment factors
		ArrayList<String> factorList = new ArrayList<String>();
		mean = parse(individualReport.getDisplayData(ReportingConstants.DR_EGOCE+"_MEAN"));
		stdev = parse(individualReport.getDisplayData(ReportingConstants.DR_EGOCE+"_STDEV"));
		if(tempScores.get(ReportingConstants.DR_EGOCE).equals("0.0"))
		{
			zScore = 0.0 - mean / stdev;
		} else {
			zScore =  ( parse(tempScores.get(ReportingConstants.DR_EGOCE) ) - mean  ) / stdev;
		}		
		percentScore = Norm.calcDoublePctlFromZ(zScore);
		stanine = findStanineFromPctl(percentScore);
		if (stanine == 9)
		{
			//drFactors += "<br />Ego-Centered";
			factorList.add(ReportingConstants.LBL_EGO_CNTRD);
		}
		
		individualReport.getScriptedData().put(ReportingConstants.DR_EGOCE, stanine+ "");		
		//System.out.println("ReportingConstants.DR_EGOCE " + percentScore+ "" + " | " + stanine+ "");
		

		mean = parse(individualReport.getDisplayData(ReportingConstants.DR_MANIP+"_MEAN"));
		stdev = parse(individualReport.getDisplayData(ReportingConstants.DR_MANIP+"_STDEV"));
		if(tempScores.get(ReportingConstants.DR_MANIP).equals("0.0"))
		{
			zScore = 0.0 - mean / stdev;
		} else {
			zScore =  ( parse(tempScores.get(ReportingConstants.DR_MANIP) ) - mean  ) / stdev;
		}		
		percentScore = Norm.calcDoublePctlFromZ(zScore);
		stanine = findStanineFromPctl(percentScore);
		if (stanine == 9)
		{
			//drFactors += "<br />Manipulative";
			factorList.add(ReportingConstants.LBL_MANIP);
		}

		individualReport.getScriptedData().put(ReportingConstants.DR_MANIP, stanine+ "");		
		//System.out.println("ReportingConstants.DR_MANIP " + percentScore+ "" + " | " + stanine+ "");
		
		mean = parse(individualReport.getDisplayData(ReportingConstants.DR_MICRO+"_MEAN"));
		stdev = parse(individualReport.getDisplayData(ReportingConstants.DR_MICRO+"_STDEV"));
		if(tempScores.get(ReportingConstants.DR_MICRO).equals("0.0"))
		{
			zScore = 0.0 - mean / stdev;
		} else {
			zScore =  ( parse(tempScores.get(ReportingConstants.DR_MICRO) ) - mean  ) / stdev;
		}		
		percentScore = Norm.calcDoublePctlFromZ(zScore);
		stanine = findStanineFromPctl(percentScore);
		if (stanine == 9)
		{
			//drFactors += "<br />Micro-Managing";
			factorList.add(ReportingConstants.LBL_MICRO_MANG);
		}
		individualReport.getScriptedData().put(ReportingConstants.DR_MICRO, stanine+ "");		
		//System.out.println("ReportingConstants.DR_MICRO " + percentScore+ "" + " | " + stanine+ "");

		mean = parse(individualReport.getDisplayData(ReportingConstants.DR_PASAG+"_MEAN"));
		stdev = parse(individualReport.getDisplayData(ReportingConstants.DR_PASAG+"_STDEV"));
		if(tempScores.get(ReportingConstants.DR_PASAG).equals("0.0"))
		{
			zScore = 0.0 - mean / stdev;
		} else {
			zScore =  ( parse(tempScores.get(ReportingConstants.DR_PASAG) ) - mean  ) / stdev;
		}		
		percentScore = Norm.calcDoublePctlFromZ(zScore);
		stanine = findStanineFromPctl(percentScore);
		if (stanine == 9)
		{
			//drFactors += "<br />Unwilling to Confront";
			factorList.add(ReportingConstants.LBL_UNWILLING_CONFRONT);
		}
		individualReport.getScriptedData().put(ReportingConstants.DR_PASAG, stanine+ "");
		//System.out.println("ReportingConstants.DR_PASAG " + percentScore+ "" + " | " + stanine+ "" );

		// Put out any Derailment factors
		individualReport.getScriptedData().put(ReportingConstants.DR_FACTOR_TEXT_COUNT, "" + factorList.size());
		int i = 0;
		for (String label : factorList)
		{
			individualReport.getScriptedData().put(ReportingConstants.DR_FACTOR_TEXT + i, label);
			i++;
		}
				
		// Career Survey components
		mean = parse(individualReport.getDisplayData(ReportingConstants.CS_LDRAS+"_MEAN"));
		stdev = parse(individualReport.getDisplayData(ReportingConstants.CS_LDRAS+"_STDEV"));
		if(tempScores.get(ReportingConstants.CS_LDRAS).equals("0.0"))
		{
			zScore = 0.0 - mean / stdev;
		} else {
			zScore =  ( parse(tempScores.get(ReportingConstants.CS_LDRAS) ) - mean  ) / stdev;
		}	
		percentScore = Norm.calcDoublePctlFromZ(zScore);
		stanine = findStanineFromPctl(percentScore);
		individualReport.getScriptedData().put(ReportingConstants.CS_LDRAS, stanine+ "");
		//System.out.println("ReportingConstants.CS_LDRAS " + percentScore+ "" + " | " + stanine+ "");
		
		mean = parse(individualReport.getDisplayData(ReportingConstants.CS_CRDRV+"_MEAN"));
		stdev = parse(individualReport.getDisplayData(ReportingConstants.CS_CRDRV+"_STDEV"));		
		if(  tempScores.get(ReportingConstants.CS_CRDRV).equals("0.0")   )
		{
			zScore = 0.0 - mean / stdev;
		} else {
			zScore =  ( parse(tempScores.get(ReportingConstants.CS_CRDRV) ) - mean  ) / stdev;			
		}		
		percentScore = Norm.calcDoublePctlFromZ(zScore);		
		stanine = findStanineFromPctl(percentScore);
		individualReport.getScriptedData().put(ReportingConstants.CS_CRDRV, stanine+ "");
		//System.out.println("ReportingConstants.CS_CRDRV " + percentScore+ "" + " | " + stanine+ "");
		
		mean = parse(individualReport.getDisplayData(ReportingConstants.CS_LRNOR+"_MEAN"));
		stdev = parse(individualReport.getDisplayData(ReportingConstants.CS_LRNOR+"_STDEV"));
		if(tempScores.get(ReportingConstants.CS_LRNOR).equals("0.0"))
		{
			zScore = 0.0 - mean / stdev;
		} else {
			zScore =  ( (parse(tempScores.get(ReportingConstants.CS_LRNOR)) ) - mean  ) / stdev;
		}		
		percentScore = Norm.calcDoublePctlFromZ(zScore);
		stanine = findStanineFromPctl(percentScore);
		individualReport.getScriptedData().put(ReportingConstants.CS_LRNOR, stanine+ "");
		//System.out.println("ReportingConstants.CS_LRNOR " + percentScore+ "" + " | " + stanine+ "");
		
		mean = parse(individualReport.getDisplayData(ReportingConstants.CS_EXPOR+"_MEAN"));
		stdev = parse(individualReport.getDisplayData(ReportingConstants.CS_EXPOR+"_STDEV"));
		if(tempScores.get(ReportingConstants.CS_EXPOR).equals("0.0"))
		{
			zScore = 0.0 - mean / stdev;
		} else {
			zScore =  ( parse(tempScores.get(ReportingConstants.CS_EXPOR)) - mean   ) / stdev;
		}			
		percentScore = Norm.calcDoublePctlFromZ(zScore);
		stanine = findStanineFromPctl(percentScore);
		individualReport.getScriptedData().put(ReportingConstants.CS_EXPOR, stanine+ "");
		//System.out.println("ReportingConstants.CS_EXPOR " + percentScore+ "" + " | " + stanine+ "");
		
		//println "End of TLT_INDIVIDUAL_SUMMARY."
		return individualReport;
	}


	/*
	 *  findStanineFromPctl determines the Stanine value for a given percentileScore.
	 *
	 * param - percentileScore:Number
	 * returns - aStanineNumber:Number
	 */	
	// Note that this is duplicated in the following places:
	//	ScoreUtils.getStanine()
	//	Norm.calcStanineFromPercent()
	//  TLT_GROUP_DETAIL_REPORT_SCORING.findStanineFromPctl()
	//  TLT_INDIVIDUAL_SUMMARY_REPORT_SCORING.findStanineFromPctl()  <-- this method
	// TODO Should it be consolidated?
	public int findStanineFromPctl(double percentileScore)
	{
		int stanine = 0;
		percentileScore = Math.round(percentileScore);
		
		if (percentileScore < 4)
		{
			stanine = 1;
		}
		else if (percentileScore > 3 && percentileScore < 11)
		{
			stanine = 2;
		}
		else if (percentileScore > 10 && percentileScore < 23)
		{
			stanine = 3;
		}
		else if (percentileScore > 22 && percentileScore < 40)
		{
			stanine = 4;
		}
		else if (percentileScore > 39 && percentileScore < 60)
		{
			stanine = 5;
		}
		else if (percentileScore > 59 && percentileScore < 77)
		{
			stanine = 6;
		}
		else if (percentileScore > 76 && percentileScore < 89)
		{
			stanine = 7;
		}
		else if (percentileScore > 88 && percentileScore < 97)
		{
			stanine = 8;
		}
		else if (percentileScore > 96)
		{
			stanine = 9;
		}
	
		return stanine;
	}	
}
