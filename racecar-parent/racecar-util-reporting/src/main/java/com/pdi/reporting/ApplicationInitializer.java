package com.pdi.reporting;

import groovy.lang.GroovyClassLoader;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.pdi.reporting.utils.ReportingUtils;
import com.pdi.xml.XMLUtils;

public class ApplicationInitializer {
	
	@SuppressWarnings("unchecked")
	public ApplicationInitializer() {
		//build up the scoring information
		try {
			DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
			Document doc = docBuilder.parse(new InputSource(new StringReader(ReportingUtils.pathToString("/com/pdi/reporting/scoring.xml"))));
			XPath xpath = XPathFactory.newInstance().newXPath(); 
			NodeList records = (NodeList) xpath.evaluate("//score", doc, XPathConstants.NODESET); 

			for(int i = 0; i < records.getLength(); i++)
			{
				Node node = records.item(i);
				if(ApplicationData.getCalculation(XMLUtils.getAttributeValue(node, "calculation")) == null) {
					String calculation = ReportingUtils.pathToString(XMLUtils.getAttributeValue(node, "calculation"));

					GroovyClassLoader gcl = new GroovyClassLoader();
					Class groovy = gcl.parseClass(calculation);
					ApplicationData.putCalculation(XMLUtils.getAttributeValue(node, "calculation"), groovy);
				}
				if(ApplicationData.getData(XMLUtils.getAttributeValue(node, "data")) == null) {
					Document data = docBuilder.parse(new InputSource(new StringReader(ReportingUtils.pathToString(XMLUtils.getAttributeValue(node, "data")))));
					ApplicationData.putData(XMLUtils.getAttributeValue(node, "data"), data);
				}		
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}