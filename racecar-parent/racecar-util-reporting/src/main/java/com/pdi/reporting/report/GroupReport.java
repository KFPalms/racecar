/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.reporting.report;
/**
 * Generic class for group report request
 * 
 * @author		Gavin Myers
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.pdi.reporting.constants.ReportingConstants;

public class GroupReport extends Report
{
	
	// 
	// Static data.
	//

	//
	// Static methods.
	//

	/**
	 * 
	 */
	private static final long serialVersionUID = -6577012377123846563L;
	//
	// Instance data.
	//
	private HashMap<String, String> _displayData = new HashMap<String, String>();
	private HashMap<String, String> _scriptedData = new HashMap<String, String>();	
	private HashMap<String, String> _labelData = new HashMap<String, String>();	
	private ArrayList<IndividualReport> _individualReports = new ArrayList<IndividualReport>();
	private int _transitionLevel = 0;
	private int _targetTransitionLevel = 0;
	private boolean multiProject = false;
	
	
	//
	// Constructors.
	//
	public GroupReport()
	{
		this.setReportType(ReportingConstants.REPORT_TYPE_GROUP);
	}
	

	//
	// Instance methods.
	//
	

	//****************************************************************
	public void addDisplayData(String key, String value)
	{
		_displayData.put(key, value);
	}
	
	public String getDisplayData(String key)
	{
		return _displayData.get(key);
	}
	
	public void setDisplayData(HashMap<String, String> displayData)
	{
		_displayData = displayData;
	}
	public HashMap<String, String> getDisplayData() {
		return _displayData;
	}
	
	//****************************************************************
	public void addScriptedData(String key, String value)
	{
		_scriptedData.put(key, value);
	}
	
	public String getScriptedData(String key)
	{
		return _scriptedData.get(key);
	}
	
	public void setScriptedData(HashMap<String, String> scriptedData)
	{
		_scriptedData = scriptedData;
	}
	
	public HashMap<String, String> getScriptedData()
	{
		return _scriptedData;
	}
	
	//****************************************************************
	public void addLabelData(String key, String value)
	{
		_labelData.put(key, value);
	}
	
	public String getLabelData(String key)
	{
		return _labelData.get(key);
	}
	
	public void setLabelData(HashMap<String, String> scriptedData)
	{
		_labelData = scriptedData;
	}
	
	public HashMap<String, String> getLabelData()
	{
		return _labelData;
	}

	//****************************************************************
	public void setIndividualReports(ArrayList<IndividualReport> individualReports)
	{
		_individualReports = individualReports;
	}

	public ArrayList<IndividualReport> getIndividualReports()
	{
		return _individualReports;
	}

	//****************************************************************
	public void setTransitionLevel(int value)
	{	
		_transitionLevel = value;
	}
	
	public int getTransitionLevel()
	{
		return _transitionLevel;
	}	
	
	//****************************************************************
	public void setTargetTransitionLevel(int value)
	{	
		_targetTransitionLevel = value;
	}

	public int getTargetTransitionLevel()
	{
		return _targetTransitionLevel;
	}	


	public String toString()
	{
		String ret = "GroupReport class:\n";
		ret += super.toString();
		ret += "  Trans. lvl=" + _transitionLevel + "\n";
		ret += "  Target trans. lvl=" + _targetTransitionLevel + "\n";
		ret += "  Display data:\n";
		for (Iterator<Map.Entry<String, String>> itr=_displayData.entrySet().iterator(); itr.hasNext();  )
		{
			Map.Entry<String, String> ent = itr.next();
			ret += "    " + ent.getKey() + "=" + ent.getValue() + "\n";
		}
		ret += "  Scripted data:\n";
		for (Iterator<Map.Entry<String, String>> itr=_scriptedData.entrySet().iterator(); itr.hasNext();  )
		{
			Map.Entry<String, String> ent = itr.next();
			ret += "    " + ent.getKey() + "=" + ent.getValue() + "\n";
		}
		ret += "  Individual reports:  NOT dumped at this time\n";
		ret += "GroupReport class ends...\n";
		
		return ret;
	}


	public boolean isMultiProject() {
		return multiProject;
	}


	public void setMultiProject(boolean multiProject) {
		this.multiProject = multiProject;
	}
}
