/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.reporting.request;

/*
 * Convenience class to hold data related to reports
 */
public class ReportRequestCodes
{
	//
	// Instance variables
	//
	private String _reportId;
 	private String _scriptId;		// name of .groovy script used to score or generate report data
 	private String _rptTmpltId;		// name of .html template used by Velocity to generate report
 	private String _textGroupId;	// Text Group identifier
 	private String _extTmpltId;		// name of .html template used to contain extract data
	
 	//
 	// Constructors
 	//
 	
 	/*
 	 * No parameter constructor is not allowed
 	 */
 	public ReportRequestCodes()
 		throws IllegalArgumentException
 	{
 		throw new IllegalArgumentException("'No parameter' constructor not allowed for the ReportRequestCodes class");
 	}
 
 	/*
 	 * Three parameter constructor (id, script, and report template)
 	 */
 	public ReportRequestCodes(String id, String script, String tmplt, String textGroup)
 		throws IllegalArgumentException
 	{
 		if (id == null || id.length() < 1)
 	 	{
 	 		throw new IllegalArgumentException("An id is required to instantiate the ReportRequestCodes class");
 	 	}
 		if (script == null || script.length() < 1)
 	 	{
 	 		throw new IllegalArgumentException("A script id string is required to instantiate the ReportRequestCodes class");
 	 	}
 		if (tmplt == null || tmplt.length() < 1)
 	 	{
 	 		throw new IllegalArgumentException("A report template id string is required to instantiate the ReportRequestCodes class");
 	 	}
 		
 		// text group parameter is allowed to be null but not
 		// an empty string... convert to null in that case.
 		if (textGroup != null && textGroup.length() < 1)
 			textGroup = null;

 		_reportId = id;
 		_scriptId = script;
 		_rptTmpltId = tmplt;
 		_textGroupId = textGroup;
 		_extTmpltId = null;
 	}
 
 	/*
 	 * Four parameter constructor (id, script, report template, text group and extract template)
 	 */
 	public ReportRequestCodes(String id, String script, String tmplt, String textGroup, String extrct)
 		throws IllegalArgumentException
 	{
 		if (id == null || id.length() < 1)
 	 	{
 	 		throw new IllegalArgumentException("An id is required to instantiate the ReportRequestCodes class");
 	 	}
 		if (script == null || script.length() < 1)
 	 	{
 	 		throw new IllegalArgumentException("A script id string is required to instantiate the ReportRequestCodes class");
 	 	}
 		if (tmplt == null || tmplt.length() < 1)
 	 	{
 	 		throw new IllegalArgumentException("A template id string is required to instantiate the ReportRequestCodes class");
 	 	}
 		if (extrct == null || extrct.length() < 1)
 	 	{
 	 		throw new IllegalArgumentException("An extract template id string is required to instantiate the ReportRequestCodes class");
 	 	}
 		
 		// text group parameter is allowed to be null but not
 		// an empty string... convert to null in that case.
 		if (textGroup != null && textGroup.length() < 1)
 			textGroup = null;

 		_reportId = id;
 		_scriptId = script;
 		_rptTmpltId = tmplt;
 		_textGroupId = textGroup;
 		_extTmpltId = extrct;
 	}
 	
 	//
 	// Instance methods
 	//
	
 	///////////////////////////////////////////
 	//          Setters and Getters          //
 	///////////////////////////////////////////
 	
 	/*****************************************/
 	public void setReportId(String value)
 	{
 		_reportId = value;
 	}

 	public String getReportId()
 	{
 		return _reportId;
 	}
 	
 	/*****************************************/
 	public void setScriptId(String value)
 	{
 		_scriptId = value;
 	}

 	public String getScriptId()
 	{
 		return _scriptId;
 	}
 	
 	/*****************************************/
 	public void setReportTemplateId(String value)
 	{
 		_rptTmpltId = value;
 	}

 	public String getReportTemplateId()
 	{
 		return _rptTmpltId;
 	}
 	
 	/*****************************************/
 	public void setTextGroupId(String value)
 	{
 		_textGroupId = value;
 	}

 	public String getTextGroupId()
 	{
 		return _textGroupId;
 	}
 	
 	/*****************************************/
 	public void setExtractTemplateId(String value)
 	{
 		_rptTmpltId = value;
 	}

 	public String getExtractTemplateId()
 	{
 		return _extTmpltId;
 	}
}
