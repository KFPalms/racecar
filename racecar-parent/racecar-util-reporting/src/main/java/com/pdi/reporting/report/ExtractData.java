/**
 * Copyright (c) 2014 Korn/Ferry International, Inc.
 * All rights reserved.
 */
package com.pdi.reporting.report;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ExtractData {
	
	private static final String	CLT_COMP_NAME_KEY = "CC_NAME";
	private static final String	CLT_COMP_SCORE_KEY = "CC_SCORE";
	private static final String	IG_COMP_NAME_KEY = "IGC_NAME";
	private static final String	IG_COMP_SCORE_KEY = "IGC_SCORE";
	private static final String CUST_FLD_NAME_KEY = "CF_NAME";
	private static final String CUST_FLD_VALUE_KEY = "CF_VALUE";
	
	public static final String SRT_GRP_NAME = "SortGroupName";
	public static final String SRT_DISP_NAME = "SortDispName";
	public static final String CHART_AXIS = "AxisLabels";
	
	public static final String INSUF_DATA = "-99";
	public static int INSUF_DATA_NUM = Integer.parseInt(INSUF_DATA);


	private static Map<String, Integer> labelMap = new HashMap<String, Integer>();
	{
		labelMap.put("IGDate",							 1);
		labelMap.put("ReportDate",						 2);		
		labelMap.put("ParticipantId",					 3);
		labelMap.put("ParticipantFname",				 4);
		labelMap.put("ParticipantLname",				 5);
		labelMap.put("ClientName",						 6);
		labelMap.put("ProjectName",						 7);	// Actually, DNA name
		labelMap.put("Model",							 8);
		labelMap.put("DRILeadSkillRating",				 9);
		labelMap.put("DRILeadSkillRatingNumber",		10);
		labelMap.put("DRILeadExperRating",				11);
		labelMap.put("DRILeadExperRatingNumber",		12);
		labelMap.put("DRILeadStyleRating",				13);
		labelMap.put("DRILeadStyleRatingNumber",		14);
		labelMap.put("DRILeadIntrstRating",				15);
		labelMap.put("DRILeadIntrstRatingNumber",		16);
		labelMap.put("DRIDerailmentRating",				17);
		labelMap.put("DRIDerailmentRatingNumber",	 	18);
		labelMap.put("DRILongTermRating",				19);
		labelMap.put("DRILongTermRatingNumber",			20);
		labelMap.put("DRIFitIndex",						21);
		labelMap.put("DRIFitRating",					22);
		labelMap.put("DRIReadinessRating",				23);
		labelMap.put("DRIReadinessRatingNumber",		24);
		labelMap.put("DRIDevelopmentFocus",				25);		
		labelMap.put("TLTLeadershipAspiration",			26);
		labelMap.put("TLTCareerDrivers",				27);
		labelMap.put("TLTLearningOrientation",			28);
		labelMap.put("TLTExperienceOrientation",		29);
		labelMap.put("TLTBusinessOperations",			30);
		labelMap.put("TLTHandlingToughChallenges",		31);
		labelMap.put("TLTHighVisibility",				32);
		labelMap.put("TLTGrowingTheBusiness",			33);
		labelMap.put("TLTPersonalDevelopment",			34);
		labelMap.put("TLTProblemSolving",				35);
		labelMap.put("TLTIntellectualEngagement",		36);
		labelMap.put("TLTAttentionToDetail",			37);
		labelMap.put("TLTImpactInfluence",				38);
		labelMap.put("TLTInterpersonalEngagement",		39);
		labelMap.put("TLTAchievementDrive",				40);
		labelMap.put("TLTAdvancementDrive",				41);
		labelMap.put("TLTCollectiveOrientation",		42);
		labelMap.put("TLTFlexibilityAdaptability",	 	43);
		labelMap.put("TLTLeadershipInterest",			44);
		labelMap.put("TLTLeadershipExperience",			45);
		labelMap.put("TLTLeadershipFoundations",		46);
		labelMap.put("TLTDerailmentRisk",				47);
		labelMap.put("TLTDerailmentRiskNumber",			48);
		labelMap.put("DRISkillsToLeverageOrgText",		49);
		labelMap.put("DRISkillsToLeveragePartText",		50);
		labelMap.put("DRISkillsToDevelopOrgText",		51);
		labelMap.put("DRISkillsToDevelopPartText",		52);
		labelMap.put("DRILeadershipSkillOrgText",		53);
		labelMap.put("DRILeadershipSkillPartText",		54);
		labelMap.put("DRILeadershipExpOrgText",			55);
		labelMap.put("DRILeadershipExpPartText",		56);
		labelMap.put("DRILeadershipStyleOrgText",		57);
		labelMap.put("DRILeadershipStylePartText",		58);
		labelMap.put("DRILeadershipInterestOrgText",	59);
		labelMap.put("DRILeadershipInterestPartText",	60);
		labelMap.put("DRIDerailmentOrgText",			61);
		labelMap.put("DRIDerailmentPartText",			62);
		labelMap.put("DRILongTermPotentialOrgText",		63);
		labelMap.put("DRIFitOrgText",					64);
		labelMap.put("DRIReadinessOrgText",				65);
		labelMap.put("DRIDevelopmentPartText",			66);
		labelMap.put("DRIPivotalPartText", 				67);
		labelMap.put("IG_TotalScore", 					68);
		labelMap.put("IG_dLCI", 						69);
		labelMap.put("WGE_CT_SP_NAME", 					70);
		labelMap.put("WGE_CT_SP_RATING",				71);
		labelMap.put("RAVENSB_SP_NAME", 				72);
		labelMap.put("RAVENSB_SP_RATING",				73);
		// Special indicators for the comp lists.  This only works because
		// the data comes in in data output (column) order
		labelMap.put(CLT_COMP_NAME_KEY,					74);
		labelMap.put(CLT_COMP_SCORE_KEY,				75);
		labelMap.put(IG_COMP_NAME_KEY,					76);
		labelMap.put(IG_COMP_SCORE_KEY,					77);
		
		// External (parameter) data to be placed
		labelMap.put(SRT_GRP_NAME,						78);
		labelMap.put(SRT_DISP_NAME,						79);
		labelMap.put(CHART_AXIS,						80);
		
		// PDA Pre-defined fields
		labelMap.put("PDA_JOB_TITLE",					81);
		labelMap.put("PDA_COUNTRY",						82);
		labelMap.put("PDA_EMP_ID",						83);
		labelMap.put("PDA_COST_CTR",					84);
		labelMap.put("PDA_PO_NBR",						85);
		labelMap.put("PDA_INT_EXT_CAND",				86);
		labelMap.put("PDA_TRGT_POS",					87);
		labelMap.put("PDA_TIM_ORG",						88);
		labelMap.put("PDA_TIP",							89);
		labelMap.put("PDA_BC_FNAME",					90);
		labelMap.put("PDA_BC_LNAME",					91);
		labelMap.put("PDA_BC_INV_EMAIL",				92);
		labelMap.put("PDA_BC_ADDR_1",					93);
		labelMap.put("PDA_BC_ADDR_2",					94);
		labelMap.put("PDA_BC_ADDR_3",					95);
		labelMap.put("PDA_BC_CITY",						96);
		labelMap.put("PDA_BC_STATE",					97);
		labelMap.put("PDA_BC_PCODE",					98);
		labelMap.put("PDA_BC_COUNTRY",					99);
		labelMap.put("PDA_SUPR_NAME",					100);
		labelMap.put("PDA_DIR_RPTS",					101);
		labelMap.put("PDA_JOB_LOC",						102);
		labelMap.put("PDA_HOME_PHONE",					103);
		labelMap.put("PDA_MOBL_PHONE",					104);
		// Special indicators for the custom field list.  This only works
		// because the data comes in in data output (column) order
		labelMap.put(CUST_FLD_NAME_KEY,					105);
		labelMap.put(CUST_FLD_VALUE_KEY,				106);

	}
	
	// class members
	private String igDate;
	private String reportDate;		
	private Integer pptId;
	private String pptFname;
	private String pptLname;
	private String clientName;
	private String projectName;	// Actually, DNA name
	private String model;
	
	//Instrument scores and associated data  Only cogs so far (2/2014)
	private String wgeSpNorm;
	private String ravSpNorm;
	
	private Float ravSpRating;
	private Float wgeSpRating;	// CT.  There are more but we don't use them
	
	// DRI data
	// Consultant Ratings
	private String skillRating;
	private Integer skillRatingNum;
	private String exprRating;
	private Integer exprRatingNum;
	private String styleRating;
	private Integer styleRatingNum;
	private String intRating;
	private Integer intRatingNum;
	private String derailRating;
	private Integer derailRatingNum;
	private String longTermRating;
	private Integer longTermRatingNum;
	// Fit
	private Integer fitIdx;
	private String fitRating;
	// Readiness
	private String readiRating;
	private Integer readiRatingNum;
	private String devFocus;
	//Competency names and scores.
	// There are 2 sets, one for the client defined list and one for the "standard" list
	// coming out of the Int. Grid.  Each set has two arrays, one for names and one for
	// scores.  The name/score list for each type is indexed the same so they are in
	// synch.  This works because the data coming in is doing so in output column order.
	private ArrayList<String> cCompNames = new ArrayList<String>();
	private ArrayList<Float> cCompScores = new ArrayList<Float>();
	private ArrayList<String> igCompNames = new ArrayList<String>();
	private ArrayList<Float> igCompScores = new ArrayList<Float>();
	
	private Float igTotalScore;
	private Integer igDlci;
	
	// TLT scores
	// Interest stanine scores
	private Integer tltLdrAsp;
	private Integer tltCarDrv;
	private Integer tltLrnOrnt;
	private Integer tltExpOrnt;
	// Experience stanine scores
	private Integer tltBusOps;
	private Integer tltTghChl;
	private Integer tltHiVis;
	private Integer tltGroBus;
	private Integer tltPerDev;
	// Foundations
	private Integer tltProbSolv;
	private Integer tltIntlEng;
	private Integer tltAttnDtl;
	private Integer tltImpInfl;
	private Integer tltIntpEng;
	private Integer tltAchDrv;
	private Integer tltAdvDrv;
	private Integer tltColOrnt;
	private Integer tltFlxAdpt;
	// Overall scores
	private String tltLdrInt;
	private String tltLdrExp;
	private String tltLdrFnd;
	// Derailment
	private String tltDrlRisk;
	private Integer tltDrlRiskNum;
	// Consultant Entered Text
	private String skillLevrgOrg;
	private String skillLevrgPpt;
	private String skillDevlpOrg;
	private String skillDevlpPpt;
	private String ldrshpSkillOrg;
	private String ldrshpSkillPpt;
	private String ldrshpExpOrg;
	private String ldrshpExpPpt;
	private String ldrshpStylOrg;
	private String ldrshpStylPpt;
	private String ldrshpIntOrg;
	private String ldrshpIntPpt;
	private String derailOrg;
	private String derailPpt;
	private String ltPotOrg;
	private String fitSumOrg;
	private String rediSumOrg;
	private String devSumPpt;
	private String pivOpsPpt;
	
	private String sortGroupName;
	private String sortItemName;
	private String axisLabels;
	
	private String pdaTitle;
	private String pdaCoutryRes;
	private String pdaEmpId;
	private String pdaCostCtr;
	private String pdaPoNbr;
	private String pdaIntExtCand;
	private String pdaTrgtPos;
	private String pdaTimOrg;
	private String pdaTimPos;
	private String pdaBcFname;
	private String pdaBcLname;
	private String pdaBcInvEmail;
	private String pdaBcAddr1;
	private String pdaBcAddr2;
	private String pdaBcAddr3;
	private String pdaBcCity;
	private String pdaBcState;
	private String pdaBcPcode;
	private String pdaBcCountry;
	private String pdaSuprName;
	private String pdaDirRpts;
	private String pdaJobLoc;
	private String pdaHomePhone;
	private String pdaMMobilePhone;

	// Custom Field names and values.
	// Like the competencies, the custom fields has two arrays, one for names
	// and one for values.  The name/value list for each type is indexed the
	// same so they are in synch.  This works because the data coming in is
	// doing so in output column order.
	private ArrayList<String> custFieldNames = new ArrayList<String>();
	private ArrayList<String> custFieldValues = new ArrayList<String>();

	
	// Generic accessors
	public void setAttribute(String key, String value, int type)
	{
		String strVal = null;
		Integer intVal = null;
		Float fltVal = null;
		
		// Skip stuff that needs to bwe skipped
		if (key.startsWith("PDA_") && key.endsWith("_HDG"))
		{
			// Move along folks... nothing to do here
			return;
		}
		
		// create the appropriate type, but only if there is something to do
		if (value != null && value.length() > 0)
		{
			// 1st handle "Insufficient Data" (transform it into a guard value)
			if (type == ExtractItem.DATA_INT  || type == ExtractItem.DATA_FLOAT)
			{
				if(value.equals("Insufficient Data"))
				{
					value = INSUF_DATA;
				}
			}
			
			boolean isNum = true;
			int iVal = 0;
			float fVal = 0.0f;
			switch (type)
			{
				case ExtractItem.DATA_INT:
					try {
						iVal = Integer.parseInt(value);
					} catch (NumberFormatException nfe) {
						isNum = false;
					}
					if (isNum)
						intVal = iVal;
					break;
				case ExtractItem.DATA_FLOAT:
					try {
						fVal = Float.parseFloat(value);
					} catch (NumberFormatException nfe) {
						isNum = false;
					}
					if (isNum)
						fltVal = (Float)fVal;
					break;
				case ExtractItem.DATA_STRING:
					strVal = value;
					break;
				default:
					// Report it but put out nulls so as not to fail downstream
					System.out.println("ExtractData error:  invalid data type (" + type + ") Defaulted to NULL");
					break;
			}
		}
		
		// Now stuff it into the appropriate spot
		// Messy
		if (key.startsWith("CC_SF"))
		{
			if (key.endsWith("Name"))
			{
				key = CLT_COMP_NAME_KEY;
			}
			else if (key.endsWith("Score"))
			{
				key = CLT_COMP_SCORE_KEY;
			}
		}
		if (key.startsWith("IG_Comp"))
		{
			if (key.endsWith("Name"))
			{
				key = IG_COMP_NAME_KEY;
			}
			else if (key.endsWith("Score"))
			{
				key = IG_COMP_SCORE_KEY;
			}
		}
		if (key.startsWith("CPDA_"))
		{
			if (key.endsWith("_CFNAME"))
			{
				key = CUST_FLD_NAME_KEY;
			}
			else if (key.endsWith("_CFVALUE"))
			{
				key = CUST_FLD_VALUE_KEY;
			}
		}
		
		// Now get the magic number that tells where to store it
		Integer idx = labelMap.get(key);
		if (idx == null)
		{
			// Do nothing but report the failure
			System.out.println("ExtractData error:  invalid store key type (" + key + ").  Data not in ExtractData.");
			return;
		}

		switch (idx.intValue())
		{
			case  1:		// IGDate
				this.setIgDate(strVal);
				break;
			case  2:		// ReportDate
				this.setReportDate(strVal);
				break;
			case  3:		// ParticipantId
				this.setPptId(intVal);
				break;
			case  4:		// ParticipantFname
				this.setPptFname(strVal);
				break;
			case  5:		// ParticipantFname
				this.setPptLname(strVal);
				break;
			case  6:		// ClientName
				this.setClientName(strVal);
				break;
			case  7:		// ProjectName
				this.setProjectName(strVal);
				break;
			case  8:		// Model
				this.setModel(strVal);
				break;
			case  9:		// DRILeadSkillRating
				this.setSkillRating(strVal);
				break;
			case 10:		// DRILeadSkillRatingNumber
				this.setSkillRatingNum(intVal);
				break;
			case 11:		// DRILeadExperRating
				this.setExprRating(strVal);
				break;
			case 12:		// DRILeadExperRatingNumber
				this.setExprRatingNum(intVal);
				break;
			case 13:		// DRILeadStyleRating
				this.setStyleRating(strVal);
				break;
			case 14:		// DRILeadStyleRatingNumber
				this.setStyleRatingNum(intVal);
				break;
			case 15:		// DRILeadIntrstRating
				this.setIntRating(strVal);
				break;
			case 16:		// DRILeadIntrstRatingNumber
				this.setIntRatingNum(intVal);
				break;
			case 17:		// DRIDerailmentRating
				this.setDerailRating(strVal);
				break;
			case 18:		// DRIDerailmentRatingNumber
				this.setDerailRatingNum(intVal);
				break;
			case 19:		// DRILongTermRating
				this.setLongTermRating(strVal);
				break;
			case 20:		// DRILongTermRatingNumber
				this.setLongTermRatingNum(intVal);
				break;
			case 21:		// DRIFitIndex
				this.setFitIdx(intVal);
				break;
			case 22:		// DRIFitRating
				this.setFitRating(strVal);
				break;
			case 23:		// DRIReadinessRating
				this.setReadiRating(strVal);
				break;
			case 24:		// DRIReadinessRatingNumber
				this.setReadiRatingNum(intVal);
				break;
			case 25:		// DRIDevelopmentFocus	
				this.setDevFocus(strVal);
				break;
			case 26:		// TLTLeadershipAspiration
				this.setTltLdrAsp(intVal);
				break;
			case 27:		// TLTCareerDrivers
				this.setTltCarDrv(intVal);
				break;
			case 28:		// TLTLearningOrientation
				this.setTltLrnOrnt(intVal);
				break;
			case 29:		// TLTExperienceOrientation
				this.setTltExpOrnt(intVal);
				break;
			case 30:		// TLTBusinessOperations
				this.setTltBusOps(intVal);
				break;
			case 31:		// TLTHandlingToughChallenges
				this.setTltTghChl(intVal);
				break;
			case 32:		// TLTHighVisibility
				this.setTltHiVis(intVal);
				break;
			case 33:		// TLTGrowingTheBusiness
				this.setTltGroBus(intVal);
				break;
			case 34:		// TLTPersonalDevelopment
				this.setTltPerDev(intVal);
				break;
			case 35:		// TLTProblemSolving
				this.setTltProbSolv(intVal);
				break;
			case 36:		// TLTIntellectualEngagement
				this.setTltIntlEng(intVal);
				break;
			case 37:		// TLTAttentionToDetail
				this.setTltAttnDtl(intVal);
				break;
			case 38:		// TLTImpactInfluence
				this.setTltImpInfl(intVal);
				break;
			case 39:		// TLTInterpersonalEngagement
				this.setTltIntpEng(intVal);
				break;
			case 40:		// TLTAchievementDrive
				this.setTltAchDrv(intVal);
				break;
			case 41:		// TLTAdvancementDrive
				this.setTltAdvDrv(intVal);
				break;
			case 42:		// TLTCollectiveOrientation
				this.setTltColOrnt(intVal);
				break;
			case 43:		// TLTFlexibilityAdaptability
				this.setTltFlxAdpt(intVal);
				break;
			case 44:		// TLTLeadershipInterest
				this.setTltLdrInt(strVal);
				break;
			case 45:		// TLTLeadershipExperience
				this.setTltLdrExp(strVal);
				break;
			case 46:		// TLTLeadershipFoundations
				this.setTltLdrFnd(strVal);
				break;
			case 47:		// TLTDerailmentRisk
				this.setTltDrlRisk(strVal);
				break;
			case 48:		// TLTDerailmentRiskNumber
				this.setTltDrlRiskNum(intVal);
				break;
			case 49:		// DRISkillsToLeverageOrgText
				this.setSkillLevrgOrg(strVal);
				break;
			case 50:		// DRISkillsToLeveragePartText
				this.setSkillLevrgPpt(strVal);
				break;
			case 51:		// DRISkillsToDevelopOrgText
				this.setSkillDevlpOrg(strVal);
				break;
			case 52:		// DRISkillsToDevelopPartText
				this.setSkillDevlpPpt(strVal);
				break;
			case 53:		// DRILeadershipSkillOrgText
				this.setLdrshpSkillOrg(strVal);
				break;
			case 54:		// DRILeadershipSkillPartText
				this.setLdrshpSkillPpt(strVal);
				break;
			case 55:		// DRILeadershipExpOrgText
				this.setLdrshpExpOrg(strVal);
				break;
			case 56:		// DRILeadershipExpPartText
				this.setLdrshpExpPpt(strVal);
				break;
			case 57:		// DRILeadershipStyleOrgText
				this.setLdrshpStylOrg(strVal);
				break;
			case 58:		// DRILeadershipStylePartText
				this.setLdrshpStylPpt(strVal);
				break;
			case 59:		// DRILeadershipInterestOrgText
				this.setLdrshpIntOrg(strVal);
				break;
			case 60:		// DRILeadershipInterestPartText
				this.setLdrshpIntPpt(strVal);
				break;
			case 61:		// DRIDerailmentOrgText
				this.setDerailOrg(strVal);
				break;
			case 62:		// DRIDerailmentPartText
				this.setDerailPpt(strVal);
				break;
			case 63:		// DRILongTermPotentialOrgText
				this.setLtPotOrg(strVal);
				break;
			case 64:		// DRIFitOrgText
				this.setFitSumOrg(strVal);
				break;
			case 65:		// DRIReadinessOrgText
				this.setRediSumOrg(strVal);
				break;
			case 66:		// DRIDevelopmentPartText
				this.setDevSumPpt(strVal);
				break;
			case 67:		// DRIPivotalPartText
				this.setPivOpsPpt(strVal);
				break;
			case 68:		// IG_TotalScore
				this.setIgTotalScore(fltVal);
				break;
			case 69:		// IG_dLCI
				this.setIgDlci(intVal);
				break;
			case 70:		// WGE_CT_SP_NAME
				this.setWgeSpNorm(strVal);
				break;
			case 71:		// WGE_CT_SP_RATING
				this.setWgeSpRating(fltVal);
				break;
			case 72:		// RAVENSB_SP_NAME
				this.setRavSpNorm(strVal);
				break;
			case 73:		// RAVENSB_SP_RATING
				this.setRavSpRating(fltVal);
				break;
			case 74:		// CLT_COMP_NAME_KEY
				this.getCCompNames().add(strVal);
				break;
			case 75:		// CLT_COMP_SCORE_KEY
				this.getCCompScores().add(fltVal);
				break;
			case 76:		// IG_COMP_NAME_KEY
				this.getIgCompNames().add(strVal);
				break;
			case 77:		// IG_COMP_SCORE_KEY
				this.getIgCompScores().add(fltVal);
				break;
			case 78:
				this.setSortGroupName(strVal);
				break;
			case 79:
				this.setSortItemName(strVal);
				break;
			case 80:
				this.setAxisLabels(strVal);
				break;
			case 81:		// Job title
				this.setPdaTitle(strVal);
				break;
			case 82:		// Country name (Residence)
				this.setPdaCoutryRes(strVal);
				break;
			case 83:		// Employee ID (Palms users ID)
				this.setPdaEmpId(strVal);
				break;
			case 84:		// Cost Center (assmt acctg)
				this.setPdaCostCtr(strVal);
				break;
			case 85:		// PO Number (assmt acctg)
				this.setPdaPoNbr(strVal);
				break;
			case 86:		// Internal/External candidate
				this.setPdaIntExtCand(strVal);
				break;
			case 87:		// Target Position
				this.setPdaTrgtPos(strVal);
				break;
			case 88:		// Time Organization
				this.setPdaTimOrg(strVal);
				break;
			case 89:		// Time in Position
				this.setPdaTimPos(strVal);
				break;
			case 90:		// Billing contact first name
				this.setPdaBcFname(strVal);
				break;
			case 91:		// Billing contact last name
				this.setPdaBcLname(strVal);
				break;
			case 92:		// Billing contact Invoice Email
				this.setPdaBcInvEmail(strVal);
				break;
			case 93:		// Billing contact Address 1
				this.setPdaBcAddr1(strVal);
				break;
			case 94:		// Billing contact Address 2
				this.setPdaBcAddr2(strVal);
				break;
			case 95:		// Billing contact Address 3
				this.setPdaBcAddr3(strVal);
				break;
			case 96:		// Billing contact city
				this.setPdaBcCity(strVal);
				break;
			case 97:		// Billing contact state
				this.setPdaBcState(strVal);
				break;
			case 98:		// Billing contact postal code
				this.setPdaBcPcode(strVal);
				break;
			case 99:		// Billing contact country
				this.setPdaBcCountry(strVal);
				break;
			case 100:		// Supervisor name
				this.setPdaSuprName(strVal);
				break;
			case 101:		// Has direct reports
				this.setPdaDirRpts(strVal);
				break;
			case 102:		// Job location
				this.setPdaJobLoc(strVal);
				break;
			case 103:		// Home phone
				this.setPdaHomePhone(strVal);
				break;
			case 104:		// Mobile phone
				this.setPdaMMobilePhone(strVal);
				break;
			case 105:		// CUST_FLD_NAME_KEY
				this.getCustFieldNames().add(strVal);
				break;
			case 106:		// CUST_FLD_VALUE_KEY
				this.getCustFieldValues().add(strVal);
				break;
			default:
				System.out.println("ExtractData error:  invalid set index (" + idx + ").  Data not in ExtractData.");
				break;
		}
		return;
	}
	
	public Object getAttribute(String key)
	{
		Object ret = null;

		// Now the data
		// Messy
		if (key.startsWith("CC_SF"))
		{
			if (key.endsWith("Name"))
			{
				key = CLT_COMP_NAME_KEY;
			}
			else if (key.endsWith("Score"))
			{
				key = CLT_COMP_SCORE_KEY;
			}
		}
		if (key.startsWith("IG_Comp"))
		{
			if (key.endsWith("Name"))
			{
				key = IG_COMP_NAME_KEY;
			}
			else if (key.endsWith("Score"))
			{
				key = IG_COMP_SCORE_KEY;
			}
		}
		if (key.startsWith("CPDA_"))
		{
			if (key.endsWith("_CFNAME"))
			{
				key = CUST_FLD_NAME_KEY;
			}
			else if (key.endsWith("_CFVALUE"))
			{
				key = CUST_FLD_VALUE_KEY;
			}
		}

		// Now get the magic number that tells where to store it
		Integer idx = labelMap.get(key);
		if (idx == null)
		{
			// Do nothing but report the failure
			System.out.println("ExtractData error:  invalid fetch key type (" + key + ").  Data not in ExtractData.");
			return null;
		}

		switch (idx.intValue())
		{
			case  1:		// IGDate
				ret = this.getIgDate();
				break;
			case  2:		// ReportDate
				ret = this.getReportDate();
				break;
			case  3:		// ParticipantId
				ret = this.getPptId();
				break;
			case  4:		// ParticipantFname
				ret = this.getPptFname();
				break;
			case  5:		// ParticipantFname
				ret = this.getPptLname();
				break;
			case  6:		// ClientName
				ret = this.getClientName();
				break;
			case  7:		// ProjectName
				ret = this.getProjectName();
				break;
			case  8:		// Model
				ret = this.getModel();
				break;
			case  9:		// DRILeadSkillRating
				ret = this.getSkillRating();
				break;
			case 10:		// DRILeadSkillRatingNumber
				ret = this.getSkillRatingNum();
				break;
			case 11:		// DRILeadExperRating
				ret = this.getExprRating();
				break;
			case 12:		// DRILeadExperRatingNumber
				ret = this.getExprRatingNum();
				break;
			case 13:		// DRILeadStyleRating
				ret = this.getStyleRating();
				break;
			case 14:		// DRILeadStyleRatingNumber
				ret = this.getStyleRatingNum();
				break;
			case 15:		// DRILeadIntrstRating
				ret = this.getIntRating();
				break;
			case 16:		// DRILeadIntrstRatingNumber
				ret = this.getIntRatingNum();
				break;
			case 17:		// DRIDerailmentRating
				ret = this.getDerailRating();
				break;
			case 18:		// DRIDerailmentRatingNumber
				ret = this.getDerailRatingNum();
				break;
			case 19:		// DRILongTermRating
				ret = this.getLongTermRating();
				break;
			case 20:		// DRILongTermRatingNumber
				ret = this.getLongTermRatingNum();
				break;
			case 21:		// DRIFitIndex
				ret = this.getFitIdx();
				break;
			case 22:		// DRIFitRating
				ret = this.getFitRating();
				break;
			case 23:		// DRIReadinessRating
				ret = this.getReadiRating();
				break;
			case 24:		// DRIReadinessRatingNumber
				ret = this.getReadiRatingNum();
				break;
			case 25:		// DRIDevelopmentFocus	
				ret = this.getDevFocus();
				break;
			case 26:		// TLTLeadershipAspiration
				ret = this.getTltLdrAsp();
				break;
			case 27:		// TLTCareerDrivers
				ret = this.getTltCarDrv();
				break;
			case 28:		// TLTLearningOrientation
				ret = this.getTltLrnOrnt();
				break;
			case 29:		// TLTExperienceOrientation
				ret = this.getTltExpOrnt();
				break;
			case 30:		// TLTBusinessOperations
				ret = this.getTltBusOps();
				break;
			case 31:		// TLTHandlingToughChallenges
				ret = this.getTltTghChl();
				break;
			case 32:		// TLTHighVisibility
				ret = this.getTltHiVis();
				break;
			case 33:		// TLTGrowingTheBusiness
				ret = this.getTltGroBus();
				break;
			case 34:		// TLTPersonalDevelopment
				ret = this.getTltPerDev();
				break;
			case 35:		// TLTProblemSolving
				ret = this.getTltProbSolv();
				break;
			case 36:		// TLTIntellectualEngagement
				ret = this.getTltIntlEng();
				break;
			case 37:		// TLTAttentionToDetail
				ret = this.getTltAttnDtl();
				break;
			case 38:		// TLTImpactInfluence
				ret = this.getTltImpInfl();
				break;
			case 39:		// TLTInterpersonalEngagement
				ret = this.getTltIntpEng();
				break;
			case 40:		// TLTAchievementDrive
				ret = this.getTltAchDrv();
				break;
			case 41:		// TLTAdvancementDrive
				ret = this.getTltAdvDrv();
				break;
			case 42:		// TLTCollectiveOrientation
				ret = this.getTltColOrnt();
				break;
			case 43:		// TLTFlexibilityAdaptability
				ret = this.getTltFlxAdpt();
				break;
			case 44:		// TLTLeadershipInterest
				ret = this.getTltLdrInt();
				break;
			case 45:		// TLTLeadershipExperience
				ret = this.getTltLdrExp();
				break;
			case 46:		// TLTLeadershipFoundations
				ret = this.getTltLdrFnd();
				break;
			case 47:		// TLTDerailmentRisk
				ret = this.getTltDrlRisk();
				break;
			case 48:		// TLTDerailmentRiskNumber
				ret = this.getTltDrlRiskNum();
				break;
			case 49:		// DRISkillsToLeverageOrgText
				ret = this.getSkillLevrgOrg();
				break;
			case 50:		// DRISkillsToLeveragePartText
				ret = this.getSkillLevrgPpt();
				break;
			case 51:		// DRISkillsToDevelopOrgText
				ret = this.getSkillDevlpOrg();
				break;
			case 52:		// DRISkillsToDevelopPartText
				ret = this.getSkillDevlpPpt();
				break;
			case 53:		// DRILeadershipSkillOrgText
				ret = this.getLdrshpSkillOrg();
				break;
			case 54:		// DRILeadershipSkillPartText
				ret = this.getLdrshpSkillPpt();
				break;
			case 55:		// DRILeadershipExpOrgText
				ret = this.getLdrshpExpOrg();
				break;
			case 56:		// DRILeadershipExpPartText
				ret = this.getLdrshpExpPpt();
				break;
			case 57:		// DRILeadershipStyleOrgText
				ret = this.getLdrshpStylOrg();
				break;
			case 58:		// DRILeadershipStylePartText
				ret = this.getLdrshpStylPpt();
				break;
			case 59:		// DRILeadershipInterestOrgText
				ret = this.getLdrshpIntOrg();
				break;
			case 60:		// DRILeadershipInterestPartText
				ret = this.getLdrshpIntPpt();
				break;
			case 61:		// DRIDerailmentOrgText
				ret = this.getDerailOrg();
				break;
			case 62:		// DRIDerailmentPartText
				ret = this.getDerailPpt();
				break;
			case 63:		// DRILongTermPotentialOrgText
				ret = this.getLtPotOrg();
				break;
			case 64:		// DRIFitOrgText
				ret = this.getFitSumOrg();
				break;
			case 65:		// DRIReadinessOrgText
				ret = this.getRediSumOrg();
				break;
			case 66:		// DRIDevelopmentPartText
				ret = this.getDevSumPpt();
				break;
			case 67:		// DRIPivotalPartText
				ret = this.getPivOpsPpt();
				break;
			case 68:		// IG_TotalScore
				ret = this.getIgTotalScore();
				break;
			case 69:		// IG_dLCI
				ret = this.getIgDlci();
				break;
			case 70:		// WGE_CT_SP_NAME
				ret = this.getWgeSpNorm();
				break;
			case 71:		// WGE_CT_SP_RATING
				ret = this.getWgeSpRating();
				break;
			case 72:		// RAVENSB_SP_NAME
				ret = this.getRavSpNorm();
				break;
			case 73:		// RAVENSB_SP_RATING
				ret = this.getRavSpRating();
				break;
			case 74:		// CLT_COMP_NAME_KEY
				ret = this.getCCompNames();
				break;
			case 75:		// CLT_COMP_SCORE_KEY
				ret = this.getCCompScores();
				break;
			case 76:		// IG_COMP_NAME_KEY
				ret = this.getIgCompNames();
				break;
			case 77:		// IG_COMP_SCORE_KEY
				ret = this.getIgCompScores();
				break;
			case 78:		// SRT_GRP_NAME
				ret = this.getSortGroupName();
				break;
			case 79:		// SRT_DISP_NAME
				ret = this.getSortItemName();
				break;
			case 80:		// CHART_AXIS
				ret = this.getAxisLabels();
				break;
			case 81:		// Job title
				ret = this.getPdaTitle();
				break;
			case 82:		// Country name (Residence)
				ret = this.getPdaCoutryRes();
				break;
			case 83:		// Employee ID (Palms users ID)
				ret = this.getPdaEmpId();
				break;
			case 84:		// Cost Center (assmt acctg)
				ret = this.getPdaCostCtr();
				break;
			case 85:		// PO Number (assmt acctg)
				ret = this.getPdaPoNbr();
				break;
			case 86:		// Internal/External candidate
				ret = this.getPdaIntExtCand();
				break;
			case 87:		// Target Position
				ret = this.getPdaTrgtPos();
				break;
			case 88:		// Time Organization
				ret = this.getPdaTimOrg();
				break;
			case 89:		// Time in Position
				ret = this.getPdaTimPos();
				break;
			case 90:		// Billing contact first name
				ret = this.getPdaBcFname();
				break;
			case 91:		// Billing contact last name
				ret = this.getPdaBcLname();
				break;
			case 92:		// Billing contact Invoice Email
				ret = this.getPdaBcInvEmail();
				break;
			case 93:		// Billing contact Address 1
				ret = this.getPdaBcAddr1();
				break;
			case 94:		// Billing contact Address 2
				ret = this.getPdaBcAddr2();
				break;
			case 95:		// Billing contact Address 3
				ret = this.getPdaBcAddr3();
				break;
			case 96:		// Billing contact city
				ret = this.getPdaBcCity();
				break;
			case 97:		// Billing contact state
				ret = this.getPdaBcState();
				break;
			case 98:		// Billing contact postal code
				ret = this.getPdaBcPcode();
				break;
			case 99:		// Billing contact country
				ret = this.getPdaBcCountry();
				break;
			case 100:		// Supervisor name
				ret = this.getPdaSuprName();
				break;
			case 101:		// Has direct reports
				ret = this.getPdaDirRpts();
				break;
			case 102:		// Job location
				ret = this.getPdaJobLoc();
				break;
			case 103:		// Home phone
				ret = this.getPdaHomePhone();
				break;
			case 104:		// Mobile phone
				ret = this.getPdaMMobilePhone();
				break;
			case 105:		// CUST_FLD_NAME_KEY
				ret = this.getCustFieldNames();
				break;
			case 106:		// CUST_FLD_VALUE_KEY
				ret = this.getCustFieldValues();
				break;
			default:
				System.out.println("ExtractData error:  invalid index (" + idx + ").  Data not in ExtractData.");
				break;
		}
		return ret;
	}

	
	// Accessors (getters and setters)
	public String getIgDate() {
		return igDate;
	}
	public void setIgDate(String igDate) {
		this.igDate = igDate;
	}
	public String getReportDate() {
		return reportDate;
	}
	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}
	public Integer getPptId() {
		return pptId;
	}
	public void setPptId(Integer pptId) {
		this.pptId = pptId;
	}
	public String getPptFname() {
		return pptFname;
	}
	public void setPptFname(String pptFname) {
		this.pptFname = pptFname;
	}
	public String getPptLname() {
		return pptLname;
	}
	public void setPptLname(String pptLname) {
		this.pptLname = pptLname;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getWgeSpNorm() {
		return wgeSpNorm;
	}
	public void setWgeSpNorm(String wgeSpNorm) {
		this.wgeSpNorm = wgeSpNorm;
	}
	public String getRavSpNorm() {
		return ravSpNorm;
	}
	public void setRavSpNorm(String ravSpNorm) {
		this.ravSpNorm = ravSpNorm;
	}
	public Float getRavSpRating() {
		return ravSpRating;
	}
	public void setRavSpRating(Float ravSpRating) {
		this.ravSpRating = ravSpRating;
	}
	public Float getWgeSpRating() {
		return wgeSpRating;
	}
	public void setWgeSpRating(Float wgeSpRating) {
		this.wgeSpRating = wgeSpRating;
	}
	public String getSkillRating() {
		return skillRating;
	}
	public void setSkillRating(String skillRating) {
		this.skillRating = skillRating;
	}
	public Integer getSkillRatingNum() {
		return skillRatingNum;
	}
	public void setSkillRatingNum(Integer skillRatingNum) {
		this.skillRatingNum = skillRatingNum;
	}
	public String getExprRating() {
		return exprRating;
	}
	public void setExprRating(String exprRating) {
		this.exprRating = exprRating;
	}
	public Integer getExprRatingNum() {
		return exprRatingNum;
	}
	public void setExprRatingNum(Integer exprRatingNum) {
		this.exprRatingNum = exprRatingNum;
	}
	public String getStyleRating() {
		return styleRating;
	}
	public void setStyleRating(String styleRating) {
		this.styleRating = styleRating;
	}
	public Integer getStyleRatingNum() {
		return styleRatingNum;
	}
	public void setStyleRatingNum(Integer styleRatingNum) {
		this.styleRatingNum = styleRatingNum;
	}
	public String getIntRating() {
		return intRating;
	}
	public void setIntRating(String intRating) {
		this.intRating = intRating;
	}
	public Integer getIntRatingNum() {
		return intRatingNum;
	}
	public void setIntRatingNum(Integer intRatingNum) {
		this.intRatingNum = intRatingNum;
	}
	public String getDerailRating() {
		return derailRating;
	}
	public void setDerailRating(String derailRating) {
		this.derailRating = derailRating;
	}
	public Integer getDerailRatingNum() {
		return derailRatingNum;
	}
	public void setDerailRatingNum(Integer derailRatingNum) {
		this.derailRatingNum = derailRatingNum;
	}
	public String getLongTermRating() {
		return longTermRating;
	}
	public void setLongTermRating(String longTermRating) {
		this.longTermRating = longTermRating;
	}
	public Integer getLongTermRatingNum() {
		return longTermRatingNum;
	}
	public void setLongTermRatingNum(Integer longTermRatingNum) {
		this.longTermRatingNum = longTermRatingNum;
	}
	public Integer getFitIdx() {
		return fitIdx;
	}
	public void setFitIdx(Integer fitIdx) {
		this.fitIdx = fitIdx;
	}
	public String getFitRating() {
		return fitRating;
	}
	public void setFitRating(String fitRating) {
		this.fitRating = fitRating;
	}
	public String getReadiRating() {
		return readiRating;
	}
	public void setReadiRating(String readiRating) {
		this.readiRating = readiRating;
	}
	public Integer getReadiRatingNum() {
		return readiRatingNum;
	}
	public void setReadiRatingNum(Integer readiRatingNum) {
		this.readiRatingNum = readiRatingNum;
	}
	public String getDevFocus() {
		return devFocus;
	}
	public void setDevFocus(String devFocus) {
		this.devFocus = devFocus;
	}
	public ArrayList<String> getCCompNames() {
		return cCompNames;
	}
	public void setCCompNames(ArrayList<String> cCompNames) {
		this.cCompNames = cCompNames;
	}
	public ArrayList<Float> getCCompScores() {
		return cCompScores;
	}
	public void setCCompScores(ArrayList<Float> cCompScores) {
		this.cCompScores = cCompScores;
	}
	public ArrayList<String> getIgCompNames() {
		return igCompNames;
	}
	public void setIgCompNames(ArrayList<String> igCompNames) {
		this.igCompNames = igCompNames;
	}
	public ArrayList<Float> getIgCompScores() {
		return igCompScores;
	}
	public void setIgCompScores(ArrayList<Float> igCompScores) {
		this.igCompScores = igCompScores;
	}

	public Float getIgTotalScore() {
		return igTotalScore;
	}
	public void setIgTotalScore(Float igTotalScore) {
		this.igTotalScore = igTotalScore;
	}
	public Integer getIgDlci() {
		return igDlci;
	}
	public void setIgDlci(Integer igDlci) {
		this.igDlci = igDlci;
	}
	public Integer getTltLdrAsp() {
		return tltLdrAsp;
	}
	public void setTltLdrAsp(Integer tltLdrAsp) {
		this.tltLdrAsp = tltLdrAsp;
	}
	public Integer getTltCarDrv() {
		return tltCarDrv;
	}
	public void setTltCarDrv(Integer tltCarDrv) {
		this.tltCarDrv = tltCarDrv;
	}
	public Integer getTltLrnOrnt() {
		return tltLrnOrnt;
	}
	public void setTltLrnOrnt(Integer tltLrnOrnt) {
		this.tltLrnOrnt = tltLrnOrnt;
	}
	public Integer getTltExpOrnt() {
		return tltExpOrnt;
	}
	public void setTltExpOrnt(Integer tltExpOrnt) {
		this.tltExpOrnt = tltExpOrnt;
	}
	public Integer getTltBusOps() {
		return tltBusOps;
	}
	public void setTltBusOps(Integer tltBusOps) {
		this.tltBusOps = tltBusOps;
	}
	public Integer getTltTghChl() {
		return tltTghChl;
	}
	public void setTltTghChl(Integer tltTghChl) {
		this.tltTghChl = tltTghChl;
	}
	public Integer getTltHiVis() {
		return tltHiVis;
	}
	public void setTltHiVis(Integer tltHiVis) {
		this.tltHiVis = tltHiVis;
	}
	public Integer getTltGroBus() {
		return tltGroBus;
	}
	public void setTltGroBus(Integer tltGroBus) {
		this.tltGroBus = tltGroBus;
	}
	public Integer getTltPerDev() {
		return tltPerDev;
	}
	public void setTltPerDev(Integer tltPerDev) {
		this.tltPerDev = tltPerDev;
	}
	public Integer getTltProbSolv() {
		return tltProbSolv;
	}
	public void setTltProbSolv(Integer tltProbSolv) {
		this.tltProbSolv = tltProbSolv;
	}
	public Integer getTltIntlEng() {
		return tltIntlEng;
	}
	public void setTltIntlEng(Integer tltIntlEng) {
		this.tltIntlEng = tltIntlEng;
	}
	public Integer getTltAttnDtl() {
		return tltAttnDtl;
	}
	public void setTltAttnDtl(Integer tltAttnDtl) {
		this.tltAttnDtl = tltAttnDtl;
	}
	public Integer getTltImpInfl() {
		return tltImpInfl;
	}
	public void setTltImpInfl(Integer tltImpInfl) {
		this.tltImpInfl = tltImpInfl;
	}
	public Integer getTltIntpEng() {
		return tltIntpEng;
	}
	public void setTltIntpEng(Integer tltIntpEng) {
		this.tltIntpEng = tltIntpEng;
	}
	public Integer getTltAchDrv() {
		return tltAchDrv;
	}
	public void setTltAchDrv(Integer tltAchDrv) {
		this.tltAchDrv = tltAchDrv;
	}
	public Integer getTltAdvDrv() {
		return tltAdvDrv;
	}
	public void setTltAdvDrv(Integer tltAdvDrv) {
		this.tltAdvDrv = tltAdvDrv;
	}
	public Integer getTltColOrnt() {
		return tltColOrnt;
	}
	public void setTltColOrnt(Integer tltColOrnt) {
		this.tltColOrnt = tltColOrnt;
	}
	public Integer getTltFlxAdpt() {
		return tltFlxAdpt;
	}
	public void setTltFlxAdpt(Integer tltFlxAdpt) {
		this.tltFlxAdpt = tltFlxAdpt;
	}
	public String getTltLdrInt() {
		return tltLdrInt;
	}
	public void setTltLdrInt(String tltLdrInt) {
		this.tltLdrInt = tltLdrInt;
	}
	public String getTltLdrExp() {
		return tltLdrExp;
	}
	public void setTltLdrExp(String tltLdrExp) {
		this.tltLdrExp = tltLdrExp;
	}
	public String getTltLdrFnd() {
		return tltLdrFnd;
	}
	public void setTltLdrFnd(String tltLdrFnd) {
		this.tltLdrFnd = tltLdrFnd;
	}
	public String getTltDrlRisk() {
		return tltDrlRisk;
	}
	public void setTltDrlRisk(String tltDrlRisk) {
		this.tltDrlRisk = tltDrlRisk;
	}
	public Integer getTltDrlRiskNum() {
		return tltDrlRiskNum;
	}
	public void setTltDrlRiskNum(Integer tltDrlRiskNum) {
		this.tltDrlRiskNum = tltDrlRiskNum;
	}
	public String getSkillLevrgOrg() {
		return skillLevrgOrg;
	}
	public void setSkillLevrgOrg(String skillLevrgOrg) {
		this.skillLevrgOrg = skillLevrgOrg;
	}
	public String getSkillLevrgPpt() {
		return skillLevrgPpt;
	}
	public void setSkillLevrgPpt(String skillLevrgPpt) {
		this.skillLevrgPpt = skillLevrgPpt;
	}
	public String getSkillDevlpOrg() {
		return skillDevlpOrg;
	}
	public void setSkillDevlpOrg(String skillDevlpOrg) {
		this.skillDevlpOrg = skillDevlpOrg;
	}
	public String getSkillDevlpPpt() {
		return skillDevlpPpt;
	}
	public void setSkillDevlpPpt(String skillDevlpPpt) {
		this.skillDevlpPpt = skillDevlpPpt;
	}
	public String getLdrshpSkillOrg() {
		return ldrshpSkillOrg;
	}
	public void setLdrshpSkillOrg(String ldrshpSkillOrg) {
		this.ldrshpSkillOrg = ldrshpSkillOrg;
	}
	public String getLdrshpSkillPpt() {
		return ldrshpSkillPpt;
	}
	public void setLdrshpSkillPpt(String ldrshpSkillPpt) {
		this.ldrshpSkillPpt = ldrshpSkillPpt;
	}
	public String getLdrshpExpOrg() {
		return ldrshpExpOrg;
	}
	public void setLdrshpExpOrg(String ldrshpExpOrg) {
		this.ldrshpExpOrg = ldrshpExpOrg;
	}
	public String getLdrshpExpPpt() {
		return ldrshpExpPpt;
	}
	public void setLdrshpExpPpt(String ldrshpExpPpt) {
		this.ldrshpExpPpt = ldrshpExpPpt;
	}
	public String getLdrshpStylOrg() {
		return ldrshpStylOrg;
	}
	public void setLdrshpStylOrg(String ldrshpStylOrg) {
		this.ldrshpStylOrg = ldrshpStylOrg;
	}
	public String getLdrshpStylPpt() {
		return ldrshpStylPpt;
	}
	public void setLdrshpStylPpt(String ldrshpStylPpt) {
		this.ldrshpStylPpt = ldrshpStylPpt;
	}
	public String getLdrshpIntOrg() {
		return ldrshpIntOrg;
	}
	public void setLdrshpIntOrg(String ldrshpIntOrg) {
		this.ldrshpIntOrg = ldrshpIntOrg;
	}
	public String getLdrshpIntPpt() {
		return ldrshpIntPpt;
	}
	public void setLdrshpIntPpt(String ldrshpIntPpt) {
		this.ldrshpIntPpt = ldrshpIntPpt;
	}
	public String getDerailOrg() {
		return derailOrg;
	}
	public void setDerailOrg(String derailOrg) {
		this.derailOrg = derailOrg;
	}
	public String getDerailPpt() {
		return derailPpt;
	}
	public void setDerailPpt(String derailPpt) {
		this.derailPpt = derailPpt;
	}
	public String getLtPotOrg() {
		return ltPotOrg;
	}
	public void setLtPotOrg(String ltPotOrg) {
		this.ltPotOrg = ltPotOrg;
	}
	public String getFitSumOrg() {
		return fitSumOrg;
	}
	public void setFitSumOrg(String fitSumOrg) {
		this.fitSumOrg = fitSumOrg;
	}
	public String getRediSumOrg() {
		return rediSumOrg;
	}
	public void setRediSumOrg(String rediSumOrg) {
		this.rediSumOrg = rediSumOrg;
	}
	public String getDevSumPpt() {
		return devSumPpt;
	}
	public void setDevSumPpt(String devSumPpt) {
		this.devSumPpt = devSumPpt;
	}
	public String getPivOpsPpt() {
		return pivOpsPpt;
	}
	public void setPivOpsPpt(String pivOpsPpt) {
		this.pivOpsPpt = pivOpsPpt;
	}
	public String getSortGroupName() {
		return sortGroupName;
	}
	public void setSortGroupName(String sortGroupName) {
		this.sortGroupName = sortGroupName;
	}
	public String getSortItemName() {
		return sortItemName;
	}
	public void setSortItemName(String sortItemName) {
		this.sortItemName = sortItemName;
	}
	public String getAxisLabels() {
		return axisLabels;
	}
	public void setAxisLabels(String axisLabels) {
		this.axisLabels = axisLabels;
	}
	
	
	public String getPdaTitle() {
		return this.pdaTitle;
	}
	public void setPdaTitle(String pdaTitle) {
		this.pdaTitle = pdaTitle;
	}
	public String getPdaCoutryRes() {
		return this.pdaCoutryRes;
	}
	public void setPdaCoutryRes(String pdaCoutryRes) {
		this.pdaCoutryRes = pdaCoutryRes;
	}
	public String getPdaEmpId() {
		return this.pdaEmpId;
	}
	public void setPdaEmpId(String pdaEmpId) {
		this.pdaEmpId = pdaEmpId;
	}
	public String getPdaCostCtr() {
		return this.pdaCostCtr;
	}
	public void setPdaCostCtr(String pdaCostCtr) {
		this.pdaCostCtr = pdaCostCtr;
	}
	public String getPdaPoNbr() {
		return this.pdaPoNbr;
	}
	public void setPdaPoNbr(String pdaPoNbr) {
		this.pdaPoNbr = pdaPoNbr;
	}
	public String getPdaIntExtCand() {
		return this.pdaIntExtCand;
	}
	public void setPdaIntExtCand(String pdaIntExtCand) {
		this.pdaIntExtCand = pdaIntExtCand;
	}
	public String getPdaTrgtPos() {
		return this.pdaTrgtPos;
	}
	public void setPdaTrgtPos(String pdaTrgtPos) {
		this.pdaTrgtPos = pdaTrgtPos;
	}
	public String getPdaTimOrg() {
		return this.pdaTimOrg;
	}
	public void setPdaTimOrg(String pdaTimOrg) {
		this.pdaTimOrg = pdaTimOrg;
	}
	public String getPdaTimPos() {
		return this.pdaTimPos;
	}
	public void setPdaTimPos(String pdaTimPos) {
		this.pdaTimPos = pdaTimPos;
	}
	public String getPdaBcFname() {
		return this.pdaBcFname;
	}
	public void setPdaBcFname(String pdaBcFname) {
		this.pdaBcFname = pdaBcFname;
	}
	public String getPdaBcLname() {
		return this.pdaBcLname;
	}
	public void setPdaBcLname(String pdaBcLname) {
		this.pdaBcLname = pdaBcLname;
	}
	public String getPdaBcInvEmail() {
		return this.pdaBcInvEmail;
	}
	public void setPdaBcInvEmail(String pdaBcInvEmail) {
		this.pdaBcInvEmail = pdaBcInvEmail;
	}
	public String getPdaBcAddr1() {
		return this.pdaBcAddr1;
	}
	public void setPdaBcAddr1(String pdaBcAddr1) {
		this.pdaBcAddr1 = pdaBcAddr1;
	}
	public String getPdaBcAddr2() {
		return this.pdaBcAddr2;
	}
	public void setPdaBcAddr2(String pdaBcAddr2) {
		this.pdaBcAddr2 = pdaBcAddr2;
	}
	public String getPdaBcAddr3() {
		return this.pdaBcAddr3;
	}
	public void setPdaBcAddr3(String pdaBcAddr3) {
		this.pdaBcAddr3 = pdaBcAddr3;
	}
	public String getPdaBcCity() {
		return this.pdaBcCity;
	}
	public void setPdaBcCity(String pdaBcCity) {
		this.pdaBcCity = pdaBcCity;
	}
	public String getPdaBcState() {
		return this.pdaBcState;
	}
	public void setPdaBcState(String pdaBcState) {
		this.pdaBcState = pdaBcState;
	}
	public String getPdaBcPcode() {
		return this.pdaBcPcode;
	}
	public void setPdaBcPcode(String pdaBcPcode) {
		this.pdaBcPcode = pdaBcPcode;
	}
	public String getPdaBcCountry() {
		return this.pdaBcCountry;
	}
	public void setPdaBcCountry(String pdaBcCountry) {
		this.pdaBcCountry = pdaBcCountry;
	}
	public String getPdaSuprName() {
		return this.pdaSuprName;
	}
	public void setPdaSuprName(String pdaSuprName) {
		this.pdaSuprName = pdaSuprName;
	}
	public String getPdaDirRpts() {
		return this.pdaDirRpts;
	}
	public void setPdaDirRpts(String pdaDirRpts) {
		this.pdaDirRpts = pdaDirRpts;
	}
	public String getPdaJobLoc() {
		return this.pdaJobLoc;
	}
	public void setPdaJobLoc(String pdaJobLoc) {
		this.pdaJobLoc = pdaJobLoc;
	}
	public String getPdaHomePhone() {
		return this.pdaHomePhone;
	}
	public void setPdaHomePhone(String pdaHomePhone) {
		this.pdaHomePhone = pdaHomePhone;
	}
	public String getPdaMMobilePhone() {
		return this.pdaMMobilePhone;
	}
	public void setPdaMMobilePhone(String pdaMMobilePhone) {
		this.pdaMMobilePhone = pdaMMobilePhone;
	}
	public ArrayList<String> getCustFieldNames() {
		return custFieldNames;
	}
	public void setCustFieldNames(ArrayList<String> custFieldNames) {
		this.custFieldNames = custFieldNames;
	}
	public ArrayList<String> getCustFieldValues() {
		return custFieldValues;
	}
	public void setCustFieldValues(ArrayList<String> custFieldValues) {
		this.custFieldValues = custFieldValues;
	}
	
	
	// -----------------------------------------------------------
	// Here starts the comparators for ExtractData
	// -----------------------------------------------------------
	// ID = 1, Fields sorted = Name

	private static class Sort1Comp implements Comparator<ExtractData>
	{
		@Override
		public int compare(ExtractData ed1, ExtractData ed2)
		{
			int comp = ed1.getPptLname().compareToIgnoreCase(ed2.getPptLname());
			return (comp != 0) ? comp : ed1.getPptFname().compareToIgnoreCase(ed2.getPptFname());
		}
	}
	
	// ID = 2, Fields sorted = Readiness, dLCI, Name
	private static class Sort2Comp implements Comparator<ExtractData>
	{
		@Override
		public int compare(ExtractData ed1, ExtractData ed2)
		{
			int comp;
			// Sort descending on readiness rating
			comp = ed2.getReadiRatingNum() - ed1.getReadiRatingNum();
			if (comp != 0)  return comp;
			// Sort descending on dLci rating
			comp = ed2.getIgDlci() - ed1.getIgDlci();
			if (comp != 0)  return comp;
			comp = ed1.getPptLname().compareToIgnoreCase(ed2.getPptLname());
			return (comp != 0) ? comp : ed1.getPptFname().compareToIgnoreCase(ed2.getPptFname());
		}
	}

	// ID = 3, Fields sorted = dLCI, Readiness, Name
	private static class Sort3Comp implements Comparator<ExtractData>
	{
		@Override
		public int compare(ExtractData ed1, ExtractData ed2)
		{
			int comp;
			// Sort descending on dLCI
			comp = ed2.getIgDlci() - ed1.getIgDlci();
			if (comp != 0)  return comp;
			// Sort descending on readiness
			comp = ed2.getReadiRatingNum() - ed1.getReadiRatingNum();
			if (comp != 0)  return comp;
			comp = ed1.getPptLname().compareToIgnoreCase(ed2.getPptLname());
			return (comp != 0) ? comp : ed1.getPptFname().compareToIgnoreCase(ed2.getPptFname());
		}
	}

	// ID = 4, Fields sorted = Fit, dLCI, Name
	private static class Sort4Comp implements Comparator<ExtractData>
	{
		@Override
		public int compare(ExtractData ed1, ExtractData ed2)
		{
			int comp;
			// Sort descending on Fit
			comp = ed2.getFitIdx() - ed1.getFitIdx();
			if (comp != 0)  return comp;
			// Sort descending on dLCI
			comp = ed2.getIgDlci() - ed1.getIgDlci();
			if (comp != 0)  return comp;
			comp = ed1.getPptLname().compareToIgnoreCase(ed2.getPptLname());
			return (comp != 0) ? comp : ed1.getPptFname().compareToIgnoreCase(ed2.getPptFname());
		}
	}

	// ID = 5, Fields sorted = dLCI, Fit, Name
	private static class Sort5Comp implements Comparator<ExtractData>
	{
		@Override
		public int compare(ExtractData ed1, ExtractData ed2)
		{
			int comp;
			// Sort descending on dLCI
			comp = ed2.getIgDlci() - ed1.getIgDlci();
			if (comp != 0)  return comp;
			// Sort descending on Fit
			comp = ed2.getFitIdx() - ed1.getFitIdx();
			if (comp != 0)  return comp;
			comp = ed1.getPptLname().compareToIgnoreCase(ed2.getPptLname());
			return (comp != 0) ? comp : ed1.getPptFname().compareToIgnoreCase(ed2.getPptFname());
		}
	}

	// ID = 6, Fields sorted = Readiness, Potential, Name
	private static class Sort6Comp implements Comparator<ExtractData>
	{
		@Override
		public int compare(ExtractData ed1, ExtractData ed2)
		{
			int comp;
			// Sort descending on Readiness
			comp = ed2.getReadiRatingNum() - ed1.getReadiRatingNum();
			if (comp != 0)  return comp;
			// Sort descending on Potential
			comp = ed2.getLongTermRatingNum() - ed1.getLongTermRatingNum();
			if (comp != 0)  return comp;
			comp = ed1.getPptLname().compareToIgnoreCase(ed2.getPptLname());
			return (comp != 0) ? comp : ed1.getPptFname().compareToIgnoreCase(ed2.getPptFname());
		}
	}

	// ID = 7, Fields sorted = Potential, Readiness, Name
	private static class Sort7Comp implements Comparator<ExtractData>
	{
		@Override
		public int compare(ExtractData ed1, ExtractData ed2)
		{
			int comp;
			// Sort descending on Potential
			comp = ed2.getLongTermRatingNum() - ed1.getLongTermRatingNum();
			if (comp != 0)  return comp;
			// Sort descending on Readiness
			comp = ed2.getReadiRatingNum() - ed1.getReadiRatingNum();
			if (comp != 0)  return comp;
			comp = ed1.getPptLname().compareToIgnoreCase(ed2.getPptLname());
			return (comp != 0) ? comp : ed1.getPptFname().compareToIgnoreCase(ed2.getPptFname());
		}
	}

	// ID = 8, Fields sorted = Readiness, Leadership Experience, Name
	private static class Sort8Comp implements Comparator<ExtractData>
	{
		@Override
		public int compare(ExtractData ed1, ExtractData ed2)
		{
			int comp;
			// Sort descending on Readiness
			comp = ed2.getReadiRatingNum() - ed1.getReadiRatingNum();
			if (comp != 0)  return comp;
			// Sort descending on Experience
			comp = ed2.getExprRatingNum() - ed1.getExprRatingNum();
			if (comp != 0)  return comp;
			comp = ed1.getPptLname().compareToIgnoreCase(ed2.getPptLname());
			return (comp != 0) ? comp : ed1.getPptFname().compareToIgnoreCase(ed2.getPptFname());
		}
	}

	// ID = 9, Fields sorted = dLCI, Leadership Experience, Name
	private static class Sort9Comp implements Comparator<ExtractData>
	{
		@Override
		public int compare(ExtractData ed1, ExtractData ed2)
		{
			int comp;
			// Sort descending on dLCI
			comp = ed2.getIgDlci() - ed1.getIgDlci();
			if (comp != 0)  return comp;
			// Sort descending on Experience
			comp = ed2.getExprRatingNum() - ed1.getExprRatingNum();
			if (comp != 0)  return comp;
			comp = ed1.getPptLname().compareToIgnoreCase(ed2.getPptLname());
			return (comp != 0) ? comp : ed1.getPptFname().compareToIgnoreCase(ed2.getPptFname());
		}
	}
	
	// ID = 10, Fields sorted = Readiness, Potential, Leadership Experience, Name
	private static class Sort10Comp implements Comparator<ExtractData>
	{
		@Override
		public int compare(ExtractData ed1, ExtractData ed2)
		{
			int comp;
			// Sort descending on Readiness
			comp = ed2.getReadiRatingNum() - ed1.getReadiRatingNum();
			if (comp != 0)  return comp;
			// Sort descending on Potential
			comp = ed2.getLongTermRatingNum() - ed1.getLongTermRatingNum();
			if (comp != 0)  return comp;
			// Sort descending on Experience
			comp = ed2.getExprRatingNum() - ed1.getExprRatingNum();
			if (comp != 0)  return comp;
			comp = ed1.getPptLname().compareToIgnoreCase(ed2.getPptLname());
			return (comp != 0) ? comp : ed1.getPptFname().compareToIgnoreCase(ed2.getPptFname());
		}
	}

	// ID = 11, Fields sorted = Readiness, Leadership Experience, Potential, Name
	private static class Sort11Comp implements Comparator<ExtractData>
	{
		@Override
		public int compare(ExtractData ed1, ExtractData ed2)
		{
			int comp;
			// Sort descending on Readiness
			comp = ed2.getReadiRatingNum() - ed1.getReadiRatingNum();
			if (comp != 0)  return comp;
			// Sort descending on Experience
			comp = ed2.getExprRatingNum() - ed1.getExprRatingNum();
			if (comp != 0)  return comp;
			// Sort descending on Potential
			comp = ed2.getLongTermRatingNum() - ed1.getLongTermRatingNum();
			if (comp != 0)  return comp;
			comp = ed1.getPptLname().compareToIgnoreCase(ed2.getPptLname());
			return (comp != 0) ? comp : ed1.getPptFname().compareToIgnoreCase(ed2.getPptFname());
		}
	}

	// ID = 12, Fields sorted = Fit, Potential, Name
	private static class Sort12Comp implements Comparator<ExtractData>
	{
		@Override
		public int compare(ExtractData ed1, ExtractData ed2)
		{
			int comp;
			// Sort descending on Fit
			comp = ed2.getFitIdx() - ed1.getFitIdx();
			if (comp != 0)  return comp;
			// Sort descending on Potential
			comp = ed2.getLongTermRatingNum() - ed1.getLongTermRatingNum();
			if (comp != 0)  return comp;
			comp = ed1.getPptLname().compareToIgnoreCase(ed2.getPptLname());
			return (comp != 0) ? comp : ed1.getPptFname().compareToIgnoreCase(ed2.getPptFname());
		}
	}

	// ID = 13, Fields sorted = Potential, Fit, Name
	private static class Sort13Comp implements Comparator<ExtractData>
	{
		@Override
		public int compare(ExtractData ed1, ExtractData ed2)
		{
			int comp;
			// Sort descending on Potential
			comp = ed2.getLongTermRatingNum() - ed1.getLongTermRatingNum();
			if (comp != 0)  return comp;
			// Sort descending on Fit
			comp = ed2.getFitIdx() - ed1.getFitIdx();
			if (comp != 0)  return comp;
			comp = ed1.getPptLname().compareToIgnoreCase(ed2.getPptLname());
			return (comp != 0) ? comp : ed1.getPptFname().compareToIgnoreCase(ed2.getPptFname());
		}
	}

	// ID = 14, Fields sorted = Fit, Leadership Experience, Name
	private static class Sort14Comp implements Comparator<ExtractData>
	{
		@Override
		public int compare(ExtractData ed1, ExtractData ed2)
		{
			int comp;
			// Sort descending on Fit
			comp = ed2.getFitIdx() - ed1.getFitIdx();
			if (comp != 0)  return comp;
			// Sort descending on Experience
			comp = ed2.getExprRatingNum() - ed1.getExprRatingNum();
			if (comp != 0)  return comp;
			comp = ed1.getPptLname().compareToIgnoreCase(ed2.getPptLname());
			return (comp != 0) ? comp : ed1.getPptFname().compareToIgnoreCase(ed2.getPptFname());
		}
	}

	// ID = 15, Fields sorted = Fit, Potential, Leadership Experience, Name
	private static class Sort15Comp implements Comparator<ExtractData>
	{
		@Override
		public int compare(ExtractData ed1, ExtractData ed2)
		{
			int comp;
			// Sort descending on Fit
			comp = ed2.getFitIdx() - ed1.getFitIdx();
			if (comp != 0)  return comp;
			// Sort descending on Potential
			comp = ed2.getLongTermRatingNum() - ed1.getLongTermRatingNum();
			if (comp != 0)  return comp;
			// Sort descending on Experience
			comp = ed2.getExprRatingNum() - ed1.getExprRatingNum();
			if (comp != 0)  return comp;
			comp = ed1.getPptLname().compareToIgnoreCase(ed2.getPptLname());
			return (comp != 0) ? comp : ed1.getPptFname().compareToIgnoreCase(ed2.getPptFname());
		}
	}

	// ID = 16, Fields sorted = Fit, Leadership Experience, Potential, Name
	private static class Sort16Comp implements Comparator<ExtractData>
	{
		@Override
		public int compare(ExtractData ed1, ExtractData ed2)
		{
			int comp;
			// Sort descending on Fit
			comp = ed2.getFitIdx() - ed1.getFitIdx();
			if (comp != 0)  return comp;
			// Sort descending on Experience
			comp = ed2.getExprRatingNum() - ed1.getExprRatingNum();
			if (comp != 0)  return comp;
			// Sort descending on Potential
			comp = ed2.getLongTermRatingNum() - ed1.getLongTermRatingNum();
			if (comp != 0)  return comp;
			comp = ed1.getPptLname().compareToIgnoreCase(ed2.getPptLname());
			return (comp != 0) ? comp : ed1.getPptFname().compareToIgnoreCase(ed2.getPptFname());
		}
	}

	// ID = 17, Fields sorted = Fit, Name
	private static class Sort17Comp implements Comparator<ExtractData>
	{
		@Override
		public int compare(ExtractData ed1, ExtractData ed2)
		{
			int comp;
			// Sort descending on Fit
			comp = ed2.getFitIdx() - ed1.getFitIdx();
			if (comp != 0)  return comp;
			comp = ed1.getPptLname().compareToIgnoreCase(ed2.getPptLname());
			return (comp != 0) ? comp : ed1.getPptFname().compareToIgnoreCase(ed2.getPptFname());
		}
	}

	// ID = 18, Fields sorted = IG Date, Name
	private static class Sort18Comp implements Comparator<ExtractData>
	{
		@Override
		public int compare(ExtractData ed1, ExtractData ed2)
		{
			// IG Date is in dd/mm/yyyy format
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Date d1;
			Date d2;
			try {
				d1 = sdf.parse(ed1.getIgDate());
				d2 = sdf.parse(ed2.getIgDate());
			} catch (ParseException e) {
				System.out.println("IG Date invalid (d1=" + ed1.getIgDate() + ", d2=" + ed2.getIgDate() + ").  Date ignored.");
				e.printStackTrace();
				d1 = d2 = new Date();	// Set to same date so that date is ignored in the sort.
			}
			int comp;
			comp = d1.compareTo(d2);
			if (comp != 0)  return comp;
			comp = ed1.getPptLname().compareToIgnoreCase(ed2.getPptLname());
			return (comp != 0) ? comp : ed1.getPptFname().compareToIgnoreCase(ed2.getPptFname());
		}
	}

	// ID = 19, Fields sorted = Readiness, Name
	private static class Sort19Comp implements Comparator<ExtractData>
	{
		@Override
		public int compare(ExtractData ed1, ExtractData ed2)
		{
			int comp;
			// Sort descending on Potential
			comp = ed2.getReadiRatingNum() - ed1.getReadiRatingNum();
			if (comp != 0)  return comp;
			comp = ed1.getPptLname().compareToIgnoreCase(ed2.getPptLname());
			return (comp != 0) ? comp : ed1.getPptFname().compareToIgnoreCase(ed2.getPptFname());
		}
	}

	// ID = 20, Fields sorted = Leadership Skill, Name
	private static class Sort20Comp implements Comparator<ExtractData>
	{
		@Override
		public int compare(ExtractData ed1, ExtractData ed2)
		{
			int comp;
			// Sort descending on Skill
			comp = ed2.getSkillRatingNum() - ed1.getSkillRatingNum();
			if (comp != 0)  return comp;
			comp = ed1.getPptLname().compareToIgnoreCase(ed2.getPptLname());
			return (comp != 0) ? comp : ed1.getPptFname().compareToIgnoreCase(ed2.getPptFname());
		}
	}

	// ID = 21, Fields sorted = Leadership Experience, Name
	private static class Sort21Comp implements Comparator<ExtractData>
	{
		@Override
		public int compare(ExtractData ed1, ExtractData ed2)
		{
			int comp;
			// Sort descending on Experience
			comp = ed2.getExprRatingNum() - ed1.getExprRatingNum();
			if (comp != 0)  return comp;
			comp = ed1.getPptLname().compareToIgnoreCase(ed2.getPptLname());
			return (comp != 0) ? comp : ed1.getPptFname().compareToIgnoreCase(ed2.getPptFname());
		}
	}

	// ID = 22, Fields sorted = Leadership Style, Name
	private static class Sort22Comp implements Comparator<ExtractData>
	{
		@Override
		public int compare(ExtractData ed1, ExtractData ed2)
		{
			int comp;
			// Sort descending on Style
			comp = ed2.getStyleRatingNum() - ed1.getStyleRatingNum();
			if (comp != 0)  return comp;
			comp = ed1.getPptLname().compareToIgnoreCase(ed2.getPptLname());
			return (comp != 0) ? comp : ed1.getPptFname().compareToIgnoreCase(ed2.getPptFname());
		}
	}

	// ID = 23, Fields sorted = Leadership Interest, Name
	private static class Sort23Comp implements Comparator<ExtractData>
	{
		@Override
		public int compare(ExtractData ed1, ExtractData ed2)
		{
			int comp;
			// Sort descending on Lnterest
			comp = ed2.getIntRatingNum() - ed1.getIntRatingNum();
			if (comp != 0)  return comp;
			comp = ed1.getPptLname().compareToIgnoreCase(ed2.getPptLname());
			return (comp != 0) ? comp : ed1.getPptFname().compareToIgnoreCase(ed2.getPptFname());
		}
	}

	// ID = 24, Fields sorted = Derailment, Name
	private static class Sort24Comp implements Comparator<ExtractData>
	{
		@Override
		public int compare(ExtractData ed1, ExtractData ed2)
		{
			int comp;
			// Sort descending on Derailment
			comp = ed2.getDerailRatingNum() - ed1.getDerailRatingNum();
			if (comp != 0)  return comp;
			comp = ed1.getPptLname().compareToIgnoreCase(ed2.getPptLname());
			return (comp != 0) ? comp : ed1.getPptFname().compareToIgnoreCase(ed2.getPptFname());
		}
	}

	// ID = 25, Fields sorted = Potential, Name
	private static class Sort25Comp implements Comparator<ExtractData>
	{
		@Override
		public int compare(ExtractData ed1, ExtractData ed2)
		{
			int comp;
			// Sort descending on Potential
			comp = ed2.getLongTermRatingNum() - ed1.getLongTermRatingNum();
			if (comp != 0)  return comp;
			comp = ed1.getPptLname().compareToIgnoreCase(ed2.getPptLname());
			return (comp != 0) ? comp : ed1.getPptFname().compareToIgnoreCase(ed2.getPptFname());
		}
	}
	// ID = 26, Fields sorted = IG Score, Name
	private static class Sort26Comp implements Comparator<ExtractData>
	{
		@Override
		public int compare(ExtractData ed1, ExtractData ed2)
		{
			float comp;
			// Sort descending on IG score
			comp = ed2.getIgTotalScore() - ed1.getIgTotalScore();
			if (comp != 0)
			{
				if (comp < 0)
				{
					return -1;
				}
				else
				{
					return 1;
				}
			}
			
			int iComp = ed1.getPptLname().compareToIgnoreCase(ed2.getPptLname());
			return (iComp != 0) ? iComp : ed1.getPptFname().compareToIgnoreCase(ed2.getPptFname());
		}
	}
	// ID = 27, Fields sorted = dLCI, Name
	private static class Sort27Comp implements Comparator<ExtractData>
	{
		@Override
		public int compare(ExtractData ed1, ExtractData ed2)
		{
			int comp;
			// Sort descending on dLCI
			comp = ed2.getIgDlci() - ed1.getIgDlci();
			if (comp != 0)  return comp;
			comp = ed1.getPptLname().compareToIgnoreCase(ed2.getPptLname());
			return (comp != 0) ? comp : ed1.getPptFname().compareToIgnoreCase(ed2.getPptFname());
		}
	}
	// -----------------------------------------------------------
	// End of the comparators
	// -----------------------------------------------------------
	
	
	/**
	 * Sorts extract data by identified criteria
	 * @param sortId
	 * @param edArray
	 * @return
	 */
	static public ArrayList<ExtractData> sortExtractData(int sortId, ArrayList<ExtractData> edArray)
	{
		// 
		switch (sortId)
		{
			case  1:	// Name
				Collections.sort(edArray, new Sort1Comp());
				break;
			case  2:	// Readiness, dLCI, Name
				Collections.sort(edArray, new Sort2Comp());
				break;
			case  3:	// dLCI, Readiness, Name
				Collections.sort(edArray, new Sort3Comp());
				break;
			case  4:	// Fit, dLCI, Name
				Collections.sort(edArray, new Sort4Comp());
				break;
			case  5:	// dLCI, Fit, Name
				Collections.sort(edArray, new Sort5Comp());
				break;
			case  6:	// Readiness, Potential, Name
				Collections.sort(edArray, new Sort6Comp());
				break;
			case  7:	// Potential, Readiness, Name
				Collections.sort(edArray, new Sort7Comp());
				break;
			case  8:	// Readiness, Leadership Experience, Name
				Collections.sort(edArray, new Sort8Comp());
				break;
			case  9:	// dLCI, Leadership Experience, Name
				Collections.sort(edArray, new Sort9Comp());
				break;
			case 10:	// Readiness, Potential, Leadership Experience, Name
				Collections.sort(edArray, new Sort10Comp());
				break;
			case 11:	// Readiness, Leadership Experience, Potential, Name
				Collections.sort(edArray, new Sort11Comp());
				break;
			case 12:	// Fit, Potential, Name
				Collections.sort(edArray, new Sort12Comp());
				break;
			case 13:	// Potential, Fit, Name
				Collections.sort(edArray, new Sort13Comp());
				break;
			case 14:	// Fit, Leadership Experience, Name
				Collections.sort(edArray, new Sort14Comp());
				break;
			case 15:	// Fit, Potential, Leadership Experience, Name
				Collections.sort(edArray, new Sort15Comp());
				break;
			case 16:	// Fit, Leadership Experience, Potential, Name
				Collections.sort(edArray, new Sort16Comp());
				break;
			case 17:	// Fit, Name
				Collections.sort(edArray, new Sort17Comp());
				break;
			case 18:	// IG Date, Name
				Collections.sort(edArray, new Sort18Comp());
				break;
			case 19:	// Readiness, Name
				Collections.sort(edArray, new Sort19Comp());
				break;
			case 20:	// Leadership Skill, Name
				Collections.sort(edArray, new Sort20Comp());
				break;
			case 21:	// Leadership Experience, Name
				Collections.sort(edArray, new Sort21Comp());
				break;
			case 22:	// Leadership Style, Name
				Collections.sort(edArray, new Sort22Comp());
				break;
			case 23:	// Leadership Interest, Name
				Collections.sort(edArray, new Sort23Comp());
				break;
			case 24:	// Derailment, Name
				Collections.sort(edArray, new Sort24Comp());
				break;
			case 25:	// Potential, Name
				Collections.sort(edArray, new Sort25Comp());
				break;
			case 26:	// IG Score, Name
				Collections.sort(edArray, new Sort26Comp());
				break;
			case 27:	// dLCI, Name
				Collections.sort(edArray, new Sort27Comp());
				break;
			default:
				System.out.println("Invalid sort ID (" + sortId + ").  Sort not performed.");
				break;
		}
		return edArray;
	}

}
