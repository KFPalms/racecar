/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.reporting.report;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


/**
 * Generic Report object used to store like data
 * that are used in both Group and Individual reports
 * 
 * @author		Gavin Myers
 */
public class Report implements java.io.Serializable
{ 
	/**
	 * 
	 */
	private static final long serialVersionUID = -8493832759447537884L;
	//
	// Instance variables
	//
	private String _reportType = "";
	private String _reportCode = "";
	private ByteArrayOutputStream _data;
	private String _name;	
	private HashMap<String, String> _configurationData = new HashMap<String, String>();
	
	
	//
	// Instance methods
	//

	/***********************************************/
	// The type of report (GROUP or INDIVIDUAL)
	public void setReportType(String value)
	{
		_reportType = value;
	}
	
	public String getReportType()
	{
		return _reportType;
	}
	
	/***********************************************/
	// The report code (GPI/LEI/WESMEN/RAVENS-SF)
	public void setReportCode(String value)
	{
		_reportCode = value;
	}
	
	public String getReportCode()
	{
		return _reportCode;
	}
	
	/***********************************************/
	// The real report data (generated)
	public void setData(ByteArrayOutputStream value)
	{
		_data = value;
	}
	
	public ByteArrayOutputStream getData()
	{
		return _data;
	}
	
	/***********************************************/
	// The actual real name of the report (example, TestReport_For_JohnDoe.pdf)
	public void setName(String value)
	{
		_name = value;
	}
	
	public String getName()
	{
		return _name;
	}
	
	/***********************************************/
	//The configuration data ("WEB_URL", "http://localhost:8080/pdi-web-participant")
	public void addConfigurationData(String key, String value)
	{
		_configurationData.put(key, value);
	}
	
	public String getConfigurationData(String key)
	{
		return _configurationData.get(key);
	}
	
	public void setConfigurationData(HashMap<String, String> value)
	{
		_configurationData = value;
	}
	
	public HashMap<String, String> getConfigurationData()
	{
		return _configurationData;
	}


	public String toString()
	{
		String ret = "Report class:\n";
		ret += "  Name=" + _name + "\n";
		ret += "  Type=" + _reportType + "\n";
		ret += "  Code=" + _reportCode + "\n";
		ret += "  Config Data:\n";
		for (Iterator<Map.Entry<String, String>> itr=_configurationData.entrySet().iterator(); itr.hasNext();  )
		{
			Map.Entry<String, String> ent = itr.next();
			ret += "    " + ent.getKey() + "=" + ent.getValue();
		}
		ret += "  DataStream purposely not dumped\n";
		ret += "Report class ends...\n";
		
		return ret;
	}
}
