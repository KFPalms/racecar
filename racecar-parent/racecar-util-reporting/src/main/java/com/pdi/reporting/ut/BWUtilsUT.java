package com.pdi.reporting.ut;

import com.pdi.reporting.report.utils.BWUtils;
import com.pdi.scoring.Norm;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

public class BWUtilsUT  extends TestCase
{
	//
	// static data
	//
	static private float[] REF_POINTS =  {0.0f, 9.6f, 28.6f, 50.0f, 71.4f, 90.4f, 100.0f};
	
	// Old reference points (5, 15, 30, 40, 60, 70, 85, 95)
	//static private float[] CUT_POINTS_1 = {36.1f, 49.2f, 60.2f, 66.0f, 76.9f, 82.8f, 93.8f, 106.8f};
	//static private float[] CUT_POINTS_2 = {-6.8f, 6.2f, 17.2f, 23.1f, 34.0f, 39.8f, 50.8f, 63.9f};
	
	// New reference points (3, 11, 23, 40, 60, 77, 89, 97)
	static private float[] CUT_POINTS_1 = {31.1f, 45.1f, 55.6f, 66.0f, 76.9f, 87.4f, 97.9f, 111.9f};
	static private float[] CUT_POINTS_2 = {-11.9f,  2.1f, 12.6f, 23.1f, 34.0f, 44.4f, 54.9f, 68.9f};

	// given the score, the value that returns 0.0 would be expected to return
	// -3.7 if rounding did not occur.  Likewise, the value that returns 100.0
	// would be expected to return 103.7 if rounding did not occur.
	static private double[] RAW_SCORE =  {10.0,   8.0,   6.0,  5.0,  12.0,  14.0,  15.0};
	static private float[] SCORE_POS = {50.0f, 28.5f, 7.0f, 0.0f, 71.5f, 93.0f, 100.0f};
	//
	// Constructor
	//
	public BWUtilsUT(String name)
	{
		super(name);
	}


	// Test reference points 
	public void testRefPoints()
		throws Exception
	{
		UnitTestUtils.start(this);
		
		float[] refAry = BWUtils.getRefPosArray();
		for (int i=0; i < refAry.length; i++)
		{
			//System.out.println("Ref " + i + " = " + refAry[i]);
			assertEquals("Error at idx " + i, REF_POINTS[i], refAry[i], 0.01d);
		}

		UnitTestUtils.stop(this);
	}


	// Test cut positions 
	public void testCutPoints()
		throws Exception
	{
		UnitTestUtils.start(this);
		
		Norm gp1 = new Norm(10, 2);
		Norm sp1 = new Norm(12, 2);
		float[] cutAry1 = BWUtils.getCutPosArray(gp1, sp1);
		for (int i=0; i < cutAry1.length; i++)
		{
			//System.out.println("Cut " + i + " = " + cutAry1[i]);  // + ", Ref=" + CUT_POINTS_1[i]);
			assertEquals("Error at idx " + i, CUT_POINTS_1[i], cutAry1[i], 0.01d);
		}
		
		// Phase 2
		Norm gp2 = new Norm(10, 2);
		Norm sp2 = new Norm(8, 2);
		float[] cutAry2 = BWUtils.getCutPosArray(gp2, sp2);
		for (int i=0; i < cutAry1.length; i++)
		{
			//System.out.println("Cut " + i + " = " + cutAry2[i]); // + ", Ref=" + CUT_POINTS_1[i]);
			assertEquals("Cutpoint Error at idx " + i, CUT_POINTS_2[i], cutAry2[i], 0.01d);
		}

		UnitTestUtils.stop(this);
	}


	// Test cut positions 
	public void testScoreCalc()
		throws Exception
	{
		UnitTestUtils.start(this);
		
		Norm gp1 = new Norm(10, 2);
		float pos;
		for (int i = 0; i < RAW_SCORE.length; i++)
		{
			pos = BWUtils.calcScorePos(RAW_SCORE[i], gp1);
			//System.out.println("i=" + i + ", Score=" + RAW_SCORE[i] + ", Pos=" + pos);
			assertEquals("Score error:  idx=" + i + ". ", SCORE_POS[i], pos, 0.01d);
		}


		UnitTestUtils.stop(this);
	}

}
