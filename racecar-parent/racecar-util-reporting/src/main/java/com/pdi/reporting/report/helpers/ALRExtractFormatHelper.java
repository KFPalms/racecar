/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.reporting.report.helpers;

import java.util.ArrayList;
import java.util.HashMap;

//import com.pdi.reporting.report.ExtractData;
import com.pdi.reporting.report.ExtractItem;
import com.pdi.reporting.report.GroupReport;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;

/*
 * ALRExtractFormatHelper provides a repository for methods used by the
 * ALR_EXTRACT.groovy  * scripts in the util-reporting (data formatting)
 * environment.
 *
 * The code was removed from the .groovy class because there appears to be
 * a limit on the size of a .groovy class and this code was approaching
 * that limit.
 *
 * This functionality was not combined with the extract helper because that
 * helper is inaccessible to reporting.
 */
public class ALRExtractFormatHelper {
	//
	// Static data
	//

	//
	// Statics used in extract output formatting
	//

	// Instrument names. These must exactly duplicate the ones in
	// ReportingConstants.RC_xx
	// Can't use those definitions in a static context
	private static String RC_GPI = "GPI"; // RC_GPI = "GPI"
	private static String RC_LEI = "LEI"; // RC_LEI = "LEI"
	private static String RC_CS = "CAREER_SURVEY"; // RC_CAREER_SURVEY =
													// "CAREER_SURVEY"
	private static String RC_WGE = "WG_E"; // RC_WG_E = "WG_E"
	private static String RC_RAVB = "RAVENS_B"; // RC_RAVENS_B = "RAVENS_B"
	private static String RC_FIN_EX = "FINEX"; // RC_FIN_EX = "FINEX";

	// Instrument List
	private static ArrayList<String> INST_LIST = new ArrayList<String>();
	static {

		INST_LIST.add(RC_WGE);
		INST_LIST.add(RC_RAVB);

	}

	// Instrument Names
	// Other instrument names - not presently used in extract anyway
	// If they do eventually get used, ensure that the names conform to Raj's
	// spec
	// "Raven's APM Short Form"
	// "Watson-Glaser A - Short Form"
	// "Personnel Classification Test - Alexander G. Wesman (Wesman)");
	// "Raven's Timed - Long Form"
	// "Watson-Glaser A - Long Form"
	// "Watson-Glaser C"
	private static HashMap<String, String> INST_NAMES = new HashMap<String, String>();
	static {
		// MOST REMOVED FOR THE HORIZONTAL VERSION OF THE EXTRACT

		INST_NAMES.put(RC_WGE, "Watson-Glaser II Critical Thinking Appraisal - Form E");
		INST_NAMES.put(RC_RAVB, "Raven's Advanced Progressive Matrices - Form B");

	}

	static public HashMap<String, String> INST_CODE_TO_RC_NAME = new HashMap<String, String>();
	static {
		INST_CODE_TO_RC_NAME.put("gpi", RC_GPI);
		INST_CODE_TO_RC_NAME.put("gpil", RC_GPI);
		INST_CODE_TO_RC_NAME.put("lei", RC_LEI);
		INST_CODE_TO_RC_NAME.put("cs", RC_CS);
		INST_CODE_TO_RC_NAME.put("cs2", RC_CS);
		INST_CODE_TO_RC_NAME.put("wg", RC_WGE);
		INST_CODE_TO_RC_NAME.put("rv", RC_RAVB);
		INST_CODE_TO_RC_NAME.put("finex", RC_FIN_EX);
	}

	// Career Survey Scales in order by SPSS label
	static public ArrayList<String> CS_SCALE_ORDER = new ArrayList<String>();
	static {
		CS_SCALE_ORDER.add("CAREER_GOALS");
		CS_SCALE_ORDER.add("CAREER_DRIVERS");
		CS_SCALE_ORDER.add("LEARNING_ORIENT");
		CS_SCALE_ORDER.add("EXPERIENCE_ORIENT");
	}

	// Watson-Glaser E Scales in order by SPSS label
	static public ArrayList<String> WGE_SCALE_ORDER = new ArrayList<String>();
	static {
		// There are 3 more scales (WGE_RA, WGE_EA, and WGE_DC), but we don't
		// use them at present
		WGE_SCALE_ORDER.add("WGE_CT");
	}

	// Ravens B Scales in order by SPSS label
	static public ArrayList<String> RAVB_SCALE_ORDER = new ArrayList<String>();
	static {
		RAVB_SCALE_ORDER.add("RAVENSB");
	}

	//
	// Static methods.
	//

	//
	// These static methods are used by the groovy script in reporting.
	// They are used to format the output data
	//

	/**
	 * Convenience method to allow non-analytics data to be gathered just as
	 * before
	 *
	 * @throws Exception
	 */
	public ArrayList<ExtractItem> setUpOrder(GroupReport gr) {
		return setUpOrder(gr, false);
	}

	/**
	 * Set up the output data order based upon the data previously gathered.
	 *
	 * Note that the order has changed from the original spec. Order is now per
	 * the email from Vidula dated 8/17/2010
	 *
	 * @throws Exception
	 */
	public ArrayList<ExtractItem> setUpOrder(GroupReport gr, boolean hasAnalytics) {
		ArrayList<ExtractItem> eItems = new ArrayList<ExtractItem>();

		// Everybody in the same extract should have the same Eval Guide and
		// Integration Grid layout. The data for them, however, is (redundantly)
		// in the Individual Report display data. Pick the first person's
		// IR object and get IR for EG and IG counts.

		// NOTE: This logic assumes that all of the participants are in the same
		// project/DNA. Stuff with GRP tags is picked up from the GroupReport
		// object and so the client project and DNA info has to be the same
		// for all participants. If the users ever want to pick from multiple
		// projects, the above named info will have to be collected into the
		// IndividualReport objects (will be redundant for people of the same
		// DNA).
		// IndividualReport ir = gr.getIndividualReports().get(0);

		// Section 1 - General identification
		eItems.add(new ExtractItem("ParticipantId", "Candidate ID", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("ParticipantFname", "Candidate First Name", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("ParticipantLname", "Candidate Last Name", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("CLIENT_NAME", "Client Name", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("IGDate", "IG Date (dd/mm/yyyy)", ExtractItem.SRC_INDV, ExtractItem.DATA_DATE));
		eItems.add(new ExtractItem("ReportDate", "Report Date", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("PROJECT_NAME", "Project Name", ExtractItem.SRC_INDV)); // Actually,
																							// DNA
																							// name
		eItems.add(new ExtractItem("MODEL_NAME", "Assessment Model", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("ExtractDate", "Extract Date", ExtractItem.SRC_GRP, ExtractItem.DATA_DATE));

		// Role
		eItems.add(new ExtractItem("CurrentRole", "Current or Most Recent Role", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("TargetRole", "Target Role", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("CurrentLevel", "Current or Most Recent Level", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("TARGET_LEVEL_NAME", "Target Level", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("TARGET_LEVEL_INDEX", "Target Level Code", ExtractItem.SRC_INDV));

		// Setup
		eItems.add(new ExtractItem("FixedOrTailored", "Fixed, Tailored/Custom", ExtractItem.SRC_INDV));

		eItems.add(new ExtractItem("Interview", "Interview", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("Testing", "Testing", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("BRE", "Business Review Exercise", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("DRM", "Direct Report Meeting", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("Peer", "Peer Meeting", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("Boss", "Boss Meeting", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("TownHall", "Town Hall Meeting", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("Customer", "Customer Meeting", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("StratFinEx", "Strategy and Financial Exercise", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("Top200", "Top 200", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("BrdMtg", "Board Meeting", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("Cfin", "CFIN", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("InBox", "In Box", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("Team", "Team Meeting", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("KF360", "KF360", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("AnalystMeeting", "Analyst Meeting", ExtractItem.SRC_INDV));

		eItems.add(new ExtractItem("DRIStatus", "Dashboard Report Input Status", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("Rpt_Note", "Setup Notes", ExtractItem.SRC_GRP));

		// eItems.add(new ExtractItem("SimulationConfiguration ", "Simulation
		// Configuration ", ExtractItem.SRC_GRP));

		// Recommendation
		eItems.add(new ExtractItem("ReportedRecommendationNumeric", "Reported Recommendation Numeric",
				ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("ReportedRecommendationClientLabel", "Reported Recommendation Client label",
				ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("ReportedRecommendationText", "Reported Recommendation Rating Text",
				ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("ReportedRecommendationOrganizationText",
				"Reported Recommendation Organization Text", ExtractItem.SRC_INDV));

		eItems.add(new ExtractItem("SuggestedRecommendationNumeric", "Suggested Recommendation Rating Numeric",
				ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("SuggestedRecommendationText", "Suggested Recommendation Rating Text",
				ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("SuggestedRecommendationOrganizationText",
				"Suggested Recommendation Organization Text", ExtractItem.SRC_INDV));

		// Development
		eItems.add(new ExtractItem("ReportedDevelopmentFocus", "Reported Development Focus Statement",
				ExtractItem.SRC_INDV));

		// Readiness
		eItems.add(new ExtractItem("ReportedReadinessNumeric", "Reported Readiness Rating Numeric",
				ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("LABEL_ALR_RS", "Reported Readiness Client label", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("ReportedReadinessText", "Reported Readiness Rating Text", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("ReportedReadinessOrganizationText", "Reported Readiness Organization Text",
				ExtractItem.SRC_INDV));

		eItems.add(new ExtractItem("SuggestedReadinessNumeric", "Suggested Readiness Rating Numeric",
				ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("SuggestedReadinessText", "Suggested Readiness Rating Text", ExtractItem.SRC_INDV));
		// eItems.add(new ExtractItem("SuggestedReadinessOrganizationText",
		// "Suggested Readiness Organization Text", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("ReadinessORAuth", "Readiness Rating Override Authorizer", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("ReadinessORJust", "Readiness Rating Override Justification", ExtractItem.SRC_INDV));

		eItems.add(new ExtractItem("DevelopmentParticipantText", "Development Summary Participant Text",
				ExtractItem.SRC_INDV));

		// Advancement
		eItems.add(new ExtractItem("ReportedAdvancementNumeric",
				"Reported Long Term Advancement Potential Rating Numeric", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("LABEL_LTADV", "Reported Long Term Advancement Potential Client label",
				ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("ReportedAdvancementText", "Reported Long Term Advancement Potential Rating Text",
				ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("DRILongTermPotentialOrgText",
				"Reported Long Term Advancement Potential Organization Text", ExtractItem.SRC_INDV));

		eItems.add(new ExtractItem("SuggestedAdvancementNumeric", "Suggested Long Term Advancement Potential Numeric",
				ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("SuggestedAdvancementText", "Suggested Long Term Advancement Potential Rating Text",
				ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("SuggestedAdvancementOrganizationText",
				"Suggested Long Term Advancement Potential Organization Text", ExtractItem.SRC_INDV));

		// Calculated candidate fit index
		eItems.add(new ExtractItem("CalculatedFitIndex", "Candidate Fit Index", ExtractItem.SRC_INDV,
				ExtractItem.DATA_INT));

		// Culture
		eItems.add(new ExtractItem("ReportedCultureNumeric", "Reported Culture Fit Rating Numeric",
				ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("LABEL_ALR_CF", "Reported Culture Fit Client label", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("ReportedCultureText", "Reported Culture Fit Rating Text", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("ReportedCultureOrganizationText", "Reported Culture Fit Organization Text",
				ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("ReportedCultureParticipantText", "Reported Culture Fit Participant Text",
				ExtractItem.SRC_INDV));

		eItems.add(new ExtractItem("SuggestedCultureNumeric", "Suggested Culture Fit Rating Numeric",
				ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("SuggestedCultureText", "Suggested Culture Fit Rating Text", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("CultureFitORAuth", "Culture Fit Rating Override Authorizer", ExtractItem.SRC_INDV));
		eItems.add(
				new ExtractItem("CultureFitORJust", "Culture Fit Rating Override Justification", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("SuggestedCultureOrganizationText", "Suggested Culture Fit Organization Text",
				ExtractItem.SRC_INDV));

		// Leadership Competencies
		eItems.add(new ExtractItem("ReportedCompetenciesNumeric", "Reported Leadership Competencies Rating Numeric",
				ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("LABEL_LS", "Reported Leadership Competencies Client label", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("ReportedCompetenciesText", "Reported Leadership Competencies Rating Text",
				ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("DRILeadershipSkillOrgText", "Reported Leadership Competencies Organization Text",
				ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("DRILeadershipSkillPartText", "Reported Leadership Competencies  Participant Text",
				ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("LABEL_SKIL_LEV", "Reported Leadership Competencies Strengths Client Label",
				ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("DRISkillsToLeverageOrgText",
				"Reported Leadership Competencies Strengths Organization Text", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("DRISkillsToLeveragePartText",
				"Reported Leadership Competencies Strengths Participant Text", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("LABEL_SKIL_DEV", "Reported Leadership Competencies Dev Needs Client Label",
				ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("ReportedCompetenciesNeedsOrganizationText",
				"Reported Leadership Competencies Dev Needs Organization Text", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("ReportedCompetenciesNeedsParticipantText",
				"Reported Leadership Competencies Dev Needs Participant Text", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("SuggestedCompetenciesNumeric", "Suggested Leadership Competencies Rating Numeric",
				ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("SuggestedCompetenciesText", "Suggested Leadership Competencies Rating Text",
				ExtractItem.SRC_INDV));

		// Leadership Experience
		eItems.add(new ExtractItem("ReportedExperienceNumeric", "Reported Leadership Experience Rating Numeric",
				ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(
				new ExtractItem("LABEL_LE_TLT", "Reported Leadership Experience Client label", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("ReportedExperienceText", "Reported Leadership Experience Rating Text",
				ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("DRILeadershipExpOrgText", "Reported Leadership Experience Organization Text",
				ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("DRILeadershipExpPartText", "Reported Leadership Experience Participant Text",
				ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("SuggestedExperienceNumeric", "Suggested Leadership Experience Rating Numeric",
				ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("SuggestedExperienceText", "Suggested Leadership Experience Rating Text",
				ExtractItem.SRC_INDV));

		// Traits
		eItems.add(new ExtractItem("ReportedTraitsNumeric", "Reported Leadership Traits Rating Numeric",
				ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("LABEL_LSA", "Reported Leadership Traits Client label", ExtractItem.SRC_INDV));
		eItems.add(
				new ExtractItem("ReportedTraitsText", "Reported Leadership Traits Rating Text", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("DRILeadershipStyleOrgText", "Reported Leadership Traits Organization Text",
				ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("DRILeadershipStylePartText", "Reported Leadership Traits Participant Text",
				ExtractItem.SRC_INDV));

		eItems.add(new ExtractItem("SuggestedTraitsNumeric", "Suggested Leadership Traits Rating Numeric",
				ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("SuggestedTraitsText", "Suggested Leadership Traits Rating Text",
				ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("SuggestedTraits", "Suggested Traits text", ExtractItem.SRC_INDV));

		// Leadership Drivers
		eItems.add(new ExtractItem("ReportedDriversNumeric", "Reported Leadership Drivers Rating Numeric",
				ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("LABEL_LI_TLT", "Reported Leadership Drivers Client label", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("ReportedDriversText", "Reported Leadership Drivers Rating Text",
				ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("DRILeadershipInterestOrgText", "Reported Leadership Drivers Organization Text",
				ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("DRILeadershipInterestPartText", "Reported Leadership Drivers Participant Text",
				ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("SuggestedDriversNumeric", "Suggested Leadership Drivers Rating Numeric",
				ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("SuggestedDriversText", "Suggested Leadership Drivers Rating Text",
				ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("SuggestedDrivers", "Suggested Leadership Drivers text", ExtractItem.SRC_INDV));

		// Derailment Risk
		eItems.add(new ExtractItem("ReportedDerailmentNumeric", "Reported Derailment Risk Rating Numeric",
				ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("LABEL_DR", "Reported Derailment Risk Client label", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("ReportedDerailmentText", "Reported Derailment Risk Rating Text",
				ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("DRIDerailmentOrgText", "Reported Derailment Risk Organization Text",
				ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("DRIDerailmentPartText", "Reported Derailment Risk Participant Text",
				ExtractItem.SRC_INDV));

		eItems.add(new ExtractItem("SuggestedDerailmentNumeric", "Suggested Derailment Risk Rating Numeric",
				ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("SuggestedDerailmentText", "Suggested Derailment Risk Rating Text",
				ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("SuggestedDerailmentOrganizationText", "Suggested Derailment Risk Organization Text",
				ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("SuggestedDerailmentParticipantText", "Suggested Derailment Risk Participant Text",
				ExtractItem.SRC_INDV));

		// Client Competency Rounded

		if (gr.getDisplayData("CC_SF_Count") == null) {
			System.out.println("No Client Competency data available");
		} else {
			int sfCnt = Integer.parseInt(gr.getDisplayData("CC_SF_Count"));

			int totalcomps = 0;
			for (int i = 1; i <= sfCnt; i++) {
				int compCnt = Integer.parseInt(gr.getDisplayData("CC_SF" + i + "_Comp_Count"));
				for (int j = 1; j <= compCnt; j++) {
					eItems.add(new ExtractItem("CC_SF" + i + "_Comp" + j + "_Name",
							"Client Competency " + (totalcomps + 1) + " consultant rounded", ExtractItem.SRC_GRP));
					eItems.add(new ExtractItem("CC_SF" + i + "_Comp" + j + "_Score",
							"Client Competency " + j + " consultant Entered Score", ExtractItem.SRC_INDV,
							ExtractItem.DATA_FLOAT));
					totalcomps++;
				}

			}

			// need 40 total columns
			if (totalcomps < 40) {
				int addCells = 40 - totalcomps;
				int addStart = 40 - addCells;
				for (int j = addStart + 1; j <= 40; j++) {
					eItems.add(new ExtractItem("CC_SFx_Comp" + j + "_Name",
							"Client Competency " + j + " consultant rounded", ExtractItem.SRC_GRP));
					eItems.add(new ExtractItem("CC_SFx_Comp" + j + "_Score", "Extra Client Competency",
							ExtractItem.SRC_INDV));
				}

			}
		}

		// Client Competency Mapping

		if (gr.getDisplayData("CC_SF_Count") == null) {
			System.out.println("No Client Competency data available");
		} else {
			int sfCnt2 = Integer.parseInt(gr.getDisplayData("CC_SF_Count"));
			int k = 1;
			int totalcomps2 = 0;
			for (int i = 1; i <= sfCnt2; i++) {
				int compCnt2 = Integer.parseInt(gr.getDisplayData("CC_SF" + i + "_Comp_Count"));
				for (int j = 1; j <= compCnt2; j++) {
					eItems.add(new ExtractItem("CC_SF" + i + "_Comp" + j + "_Name",
							"Client Competency " + (totalcomps2 + 1) + " mapping to KFLA", ExtractItem.SRC_GRP));
					eItems.add(new ExtractItem("CC_SF" + i + "_Comp" + j + "_Child" + k + "_Dna_Name", "Mapping",
							ExtractItem.SRC_GRP));
					totalcomps2++;
				}

			}

			// need 40 total columns
			if (totalcomps2 < 40) {
				int addCells = 40 - totalcomps2;
				int addStart = 40 - addCells;
				for (int j = addStart + 1; j <= 40; j++) {
					eItems.add(new ExtractItem("CC_SFx_Comp" + j + "_Name",
							"Client Competency " + j + " mapping to KFLA", ExtractItem.SRC_GRP));
					eItems.add(
							new ExtractItem("Comp" + j + "_Score", "Extra Competency Mapping", ExtractItem.SRC_INDV));
				}

			}
		}

		// Overall KFLA stats
		eItems.add(new ExtractItem("IG_TotalScore", "Overall KFLA Competency Average", ExtractItem.SRC_INDV,
				ExtractItem.DATA_FLOAT));
		eItems.add(new ExtractItem("dlciMean", "KFLA Configuration Specific Competency Norm Mean", ExtractItem.SRC_GRP,
				ExtractItem.DATA_FLOAT_3));
		eItems.add(new ExtractItem("dlciStdDev", "KFLA Configuration Specific Competency Norm Standard Deviation",
				ExtractItem.SRC_GRP, ExtractItem.DATA_FLOAT_3));
		eItems.add(new ExtractItem("wbVersion", "KFLA Configuration Design Workbook Version", ExtractItem.SRC_GRP));
		eItems.add(new ExtractItem("IG_dLCI", "Overall KFLA Configuration Specific Competency Percentile",
				ExtractItem.SRC_INDV, ExtractItem.DATA_INT));

		// KFLA Competency Rounded
		int KFLAccrCnt = 38;
		for (int i = 1; i <= KFLAccrCnt; i++) {
			eItems.add(new ExtractItem("KFLA_COMP" + i + "_Name", "KFLA Competency " + i + " consultant rounded",
					ExtractItem.SRC_INDV));
			eItems.add(
					new ExtractItem("IG_Comp" + i + "_FinalScore", "", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT));
			// eItems.add(new ExtractItem("IG_Comp" + KFLACompArray[i] +
			// "_FinalScore", "", ExtractItem.SRC_INDV,
			// ExtractItem.DATA_FLOAT));
		}

		// KFLA Competency Raw Average
		int KFLAcraCnt = 38;
		for (int i = 1; i <= KFLAcraCnt; i++) {
			eItems.add(new ExtractItem("KFLA_COMP" + i + "_Name", "KFLA Competency " + i + " raw average",
					ExtractItem.SRC_INDV));
			eItems.add(new ExtractItem("IG_Comp" + i + "_CalcScore", "", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT));

		}

		int InterviewKFLACnt = 38; // get real count from DB
		for (int i = 1; i <= InterviewKFLACnt; i++) {
			eItems.add(
					new ExtractItem("KFLA_COMP" + i + "_Name", "Interview KFLA Competency " + i, ExtractItem.SRC_INDV));
			eItems.add(new ExtractItem("EG_Interview_Comp" + i + "_Score", "", ExtractItem.SRC_INDV,
					ExtractItem.DATA_FLOAT));

		}

		// Testing KFLA Competency.
		// Could be data from Eval Guide, ALP scores or data from GPI/Cogs
		// (CEO), but we use the EG_ prefix to make it a little easier on
		// ourselves.

		int TestingKFAlPCnt = 38; // get real count from DB
		for (int i = 1; i <= TestingKFAlPCnt; i++) {
			eItems.add(
					new ExtractItem("KFLA_COMP" + i + "_Name", "Testing KFLA Competency " + i, ExtractItem.SRC_INDV));
			eItems.add(new ExtractItem("EG_Testing_Comp" + i + "_Score", "", ExtractItem.SRC_INDV,
					ExtractItem.DATA_FLOAT));

		}

		// Business Review Exercise KFLA Competency

		int BRECnt = 38;
		for (int i = 1; i <= BRECnt; i++) {
			eItems.add(new ExtractItem("KFLA_COMP" + i + "_Name", "Business Review Exercise KFLA Competency " + i,
					ExtractItem.SRC_INDV));
			eItems.add(new ExtractItem("EG_BRE_Comp" + i + "_Score", "", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT));

		}

		// Direct Report Meeting KFLA Competency

		int DRMCnt = 38;
		for (int i = 1; i <= DRMCnt; i++) {
			eItems.add(new ExtractItem("KFLA_COMP" + i + "_Name", "Direct Report Meeting KFLA Competency " + i,
					ExtractItem.SRC_INDV));
			eItems.add(new ExtractItem("EG_DRM_Comp" + i + "_Score", "", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT));

		}

		// Peer Meeting KFLA Competency

		int PeerCnt = 38;
		for (int i = 1; i <= PeerCnt; i++) {
			eItems.add(new ExtractItem("KFLA_COMP" + i + "_Name", "Peer Meeting KFLA Competency " + i,
					ExtractItem.SRC_INDV));
			eItems.add(
					new ExtractItem("EG_Peer_Comp" + i + "_Score", "", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT));

		}

		// Boss Meeting KFLA Competency

		int BossCnt = 38;
		for (int i = 1; i <= BossCnt; i++) {
			eItems.add(new ExtractItem("KFLA_COMP" + i + "_Name", "Boss Meeting KFLA Competency " + i,
					ExtractItem.SRC_INDV));
			eItems.add(
					new ExtractItem("EG_Boss_Comp" + i + "_Score", "", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT));

		}

		// Town Hall KFLA Competency

		int TownCnt = 38;
		for (int i = 1; i <= TownCnt; i++) {
			eItems.add(
					new ExtractItem("KFLA_COMP" + i + "_Name", "Town Hall KFLA Competency " + i, ExtractItem.SRC_INDV));
			eItems.add(new ExtractItem("EG_TownHall_Comp" + i + "_Score", "", ExtractItem.SRC_INDV,
					ExtractItem.DATA_FLOAT));

		}

		// Customer Meeting KFLA Competency

		int CustomerCnt = 38;
		for (int i = 1; i <= CustomerCnt; i++) {
			eItems.add(new ExtractItem("KFLA_COMP" + i + "_Name", "Customer Meeting KFLA Competency " + i,
					ExtractItem.SRC_INDV));
			eItems.add(new ExtractItem("EG_Customer_Comp" + i + "_Score", "", ExtractItem.SRC_INDV,
					ExtractItem.DATA_FLOAT));

		}

		// Strategy and Financial Exercise KFLA Competency

		int sfeCnt = 38;
		for (int i = 1; i <= sfeCnt; i++) {
			eItems.add(new ExtractItem("KFLA_COMP" + i + "_Name",
					"Strategy and Financial Exercise KFLA Competency " + i, ExtractItem.SRC_INDV));
			eItems.add(new ExtractItem("EG_StratFinEx_Comp" + i + "_Score", "", ExtractItem.SRC_INDV,
					ExtractItem.DATA_FLOAT));
		}

		// Top 200 KFLA Competency

		int t200Cnt = 38;
		for (int i = 1; i <= t200Cnt; i++) {
			eItems.add(
					new ExtractItem("KFLA_COMP" + i + "_Name", "Top 200 KFLA Competency " + i, ExtractItem.SRC_INDV));
			eItems.add(
					new ExtractItem("EG_Top200_Comp" + i + "_Score", "", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT));
		}

		// Board Meeting KFLA Competency

		int brdMtgCnt = 38;
		for (int i = 1; i <= brdMtgCnt; i++) {
			eItems.add(new ExtractItem("KFLA_COMP" + i + "_Name", "Board Meeting KFLA Competency " + i,
					ExtractItem.SRC_INDV));
			eItems.add(
					new ExtractItem("EG_BrdMtg_Comp" + i + "_Score", "", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT));
		}

		// CFIN KFLA Competency

		int cfinCnt = 38;
		for (int i = 1; i <= cfinCnt; i++) {
			eItems.add(new ExtractItem("KFLA_COMP" + i + "_Name", "CFIN KFLA Competency " + i, ExtractItem.SRC_INDV));
			eItems.add(
					new ExtractItem("EG_Cfin_Comp" + i + "_Score", "", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT));
		}

		// Inbox KFLA Competency

		int ibxCnt = 38;
		for (int i = 1; i <= ibxCnt; i++) {
			eItems.add(new ExtractItem("KFLA_COMP" + i + "_Name", "Inbox KFLA Competency " + i, ExtractItem.SRC_INDV));
			eItems.add(
					new ExtractItem("EG_Inbox_Comp" + i + "_Score", "", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT));
		}

		// Team Meeting KFLA Competency

		int teamCnt = 38;
		for (int i = 1; i <= teamCnt; i++) {
			eItems.add(new ExtractItem("KFLA_COMP" + i + "_Name", "Team Meeting KFLA Competency " + i,
					ExtractItem.SRC_INDV));
			eItems.add(
					new ExtractItem("EG_Team_Comp" + i + "_Score", "", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT));
		}

		// KF360 Competency.
		// This is an externally scored instrument put in with a special type
		// Eval. Guide;
		int KF360Cnt = 38;
		for (int i = 1; i <= KF360Cnt; i++) {
			eItems.add(new ExtractItem("KFLA_COMP" + i + "_Name", "KF360 Competency " + i, ExtractItem.SRC_INDV));
			eItems.add(
					new ExtractItem("EG_KF360_Comp" + i + "_Score", "", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT));
		}

		// Analyst Meeting Competency.
		int analystMeetingCnt = 38;
		for (int i = 1; i <= analystMeetingCnt; i++) {
			eItems.add(new ExtractItem("KFLA_COMP" + i + "_Name", "Analyst Meeting Competency " + i,
					ExtractItem.SRC_INDV));
			eItems.add(new ExtractItem("EG_AnalystMeeting_Comp" + i + "_Score", "", ExtractItem.SRC_INDV,
					ExtractItem.DATA_FLOAT));
		}

		// KFALP Scoring items
		// Uses new constructors where the key name and display name are the
		// same
		eItems.add(new ExtractItem("ParticipantID", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("DateExtractStarted", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("ReportTargetLevelCode", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("ReportTargetLevelText", ExtractItem.SRC_INDV));

		eItems.add(new ExtractItem("CurrentLevelOverrideValue", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("CurrentLevelOriginalValue", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));

		eItems.add(new ExtractItem("ParticipantSetupDate", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("KFALPInstrumentVersion", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));

		eItems.add(new ExtractItem("KFALPScoreVersion", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("KFALPNormVersion", ExtractItem.SRC_INDV, ExtractItem.DATA_INT)); // Currently
																										// no
																										// data

		eItems.add(new ExtractItem("KFALPSubmitted", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("RavensInstrumentVersion", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));

		eItems.add(new ExtractItem("RavensScoreVersion", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT));
		eItems.add(new ExtractItem("RavensNormVersion", ExtractItem.SRC_INDV)); // Currently
																				// no
																				// data

		eItems.add(new ExtractItem("RavensSubmitted", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("Experimental1", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("Experimental2", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("GrowthParticipant3", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("GrowthParticipant4", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("GrowthParticipant5", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("GrowthParticipant6", ExtractItem.SRC_INDV));

		eItems.add(new ExtractItem("signpostSum", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("signpostCount", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));

		eItems.add(new ExtractItem("agileChangeNorm", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("agileChangeRaw", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("agileChangeScore", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("agileMentalNorm", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("agileMentalRaw", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("agileMentalScore", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("agilePeopleNorm", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("agilePeopleRaw", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("agilePeopleScore", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("agileResultsNorm", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("agileResultsRaw", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("agileResultsScore", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("agileScore", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("agileTotal", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("awarenessSelfNorm", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("awarenessSelfRaw", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("awarenessSelfScore", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("awarenessSitNorm", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("awarenessSitRaw", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("awarenessSitScore", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("awarenessScore", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("awarenessTotal", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("capacityNorm", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("capacityRaw", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("capacityScore", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("derailerClosedNorm", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("derailerClosedRaw", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("derailerClosedScore", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("derailerMicroNorm", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("derailerMicroRaw", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("derailerMicroScore", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("derailerVolatileNorm", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("derailerVolatileRaw", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("derailerVolatileScore", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("derailerScore", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("derailerTotal", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("devEffort", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("kfarDerailerAvoidantNorm", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("kfarDerailerAvoidantRaw", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("kfarDerailerAvoidantScore", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("kfarDerailerSuspiciousNorm", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("kfarDerailerSuspiciousRaw", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("kfarDerailerSuspiciousScore", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("kfarDerailerRestrainedNorm", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("kfarDerailerRestrainedRaw", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("kfarDerailerRestrainedScore", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("driversAdvdriveAspiration", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("driversAdvdriveJump", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("driversAdvdriveNorm", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("driversAdvdriveRaw", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("driversAdvdriveThreeToFiveGoal", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("driversAdvdriveYearsCurrentRole", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("driversAdvdriveScore", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("driversCareerplanApproach", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("driversCareerplanFuncPref", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("driversCareerplanIndPref", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("driversCareerplanNorm", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("driversCareerplanOrgSPref", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("driversCareerplanOrgTPref", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("driversCareerplanRaw", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("driversCareerplanRolePref", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("driversCareerplanScore", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("driversCareerplanStrongPrefs", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("driversRoleprefNorm", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("driversRoleprefRaw", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("driversRoleprefScore", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("driversScore", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("driversTotal", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("experienceCoreActual", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("experienceCoreCore", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("experienceCoreExpected", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("experienceCoreHighest", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("experienceCoreNorm", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("experienceCorePace", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("experienceCoreScore", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("experienceCoreTenure", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("experienceCoreX", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("experienceKeyExpat", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("experienceKeyExpatXP", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("experienceKeynKey", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("experienceKeyNorm", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("experienceKeyRawKeyChallenge", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("experienceKeyScore", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("experiencePerspectiveExpNOrgCountry", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("experiencePerspectiveExpNOrgs", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("experiencePerspectiveExpOrgInd", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("experiencePerspectiveExpOrgSizes", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("experiencePerspectiveExpOrgTypes", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("experiencePerspectiveExpOrgsFunc", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("experiencePerspectiveExpOrgsRole", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("experiencePerspectiveNorm", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("experiencePerspectiveRaw", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("experiencePerspectiveScore", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("experienceScore", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("experienceTotal", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("traitsAssertNorm", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("traitsAssertRaw", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("traitsAssertScore", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("traitsFocusNorm", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("traitsFocusRaw", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("traitsFocusScore", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("traitsOptimismNorm", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("traitsOptimismRaw", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("traitsOptimismScore", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("traitsPersistNorm", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("traitsPersistRaw", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("traitsPersistScore", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("traitsToleranceNorm", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("traitsToleranceRaw", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("traitsToleranceScore", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("traitsScore", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("traitsTotal", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("WorkEngagement", ExtractItem.SRC_INDV)); // cal
																				// payload
																				// in
																				// future?
		eItems.add(new ExtractItem("OrganizationCommitment", ExtractItem.SRC_INDV)); // cal
																						// payload
																						// in
																						// future?
		eItems.add(new ExtractItem("TBD1", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("TBD2", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("TBD3", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("TBD4", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("PIQ1", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("PIQ2", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("PIQ3", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("PIQ4", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("PIQ5", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("PIQ6", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("PIQ7", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("PIQ8", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("PIQ9", ExtractItem.SRC_INDV));

		eItems.add(new ExtractItem("JF1", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("JF6", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("JF7", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("JF8", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("JF17", ExtractItem.SRC_INDV));

		for (int i = 18; i < 37; i++) {
			eItems.add(new ExtractItem("JF" + i, ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		}

		for (int i = 37; i < 43; i++) {
			eItems.add(new ExtractItem("JF" + i, ExtractItem.SRC_INDV));
		}

		eItems.add(new ExtractItem("Culture1", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("Culture2", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("Culture3", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("Culture4", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));

		for (int i = 1; i < 219; i++) {
			eItems.add(new ExtractItem("ALP" + i, ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		}

		for (int i = 1; i < 24; i++) {
			eItems.add(new ExtractItem("RAV" + i, ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
		}

		for (int i = 1; i < 10; i++) {
			eItems.add(new ExtractItem("DRIVER0" + i, ExtractItem.SRC_INDV));
		}

		eItems.add(new ExtractItem("DRIVER10", ExtractItem.SRC_INDV));

		for (int i = 1; i < 10; i++) {
			eItems.add(new ExtractItem("TRAIT0" + i, ExtractItem.SRC_INDV));
		}

		for (int i = 10; i < 43; i++) {
			eItems.add(new ExtractItem("TRAIT" + i, ExtractItem.SRC_INDV));
		}

		eItems.add(new ExtractItem("Cr", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("Ta", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("Ss", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("Cf", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("Ri", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("Ad", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("Af", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("Na", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("Pe", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("As", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("Sa", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("Ie", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("Tr", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("En", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("Mi", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("Co", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("Hu", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("In", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("Du", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("St", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("Od", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("STRC", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("CHAL", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("POWR", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("COLL", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("BALA", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));
		eItems.add(new ExtractItem("INDY", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_9));

		// Raw KF4D items
		eItems.add(new ExtractItem("r_aco", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_aex", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_bet", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_bre", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_bst", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_cfo", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_cin", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_col", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_com", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_cou", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_dqu", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_dre", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_dta", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_dwo", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_eac", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_ein", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_gpe", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_ips", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_itr", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_mab", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_mco", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_nle", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_nne", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_owp", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_per", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_rsf", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_sad", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_sdv", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_svi", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_vdi", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_sa", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_bala", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_chal", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_coll", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_indy", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_powr", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_strc", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_agr", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_af", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_hu", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_od", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_tr", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_exp", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_ad", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_cu", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_fo", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_ri", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_ta", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_pos", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_cp", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_op", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_ss", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_pre", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_as", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_em", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_in", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_so", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_stv", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_cf", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_cr", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_na", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));
		eItems.add(new ExtractItem("r_pe", ExtractItem.SRC_INDV, ExtractItem.DATA_FLOAT_5));

		// PDA ITEMS
		eItems.add(new ExtractItem("PDA_JOB_TITLE", "Current Job Title", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("PDA_COUNTRY", "Country of Residence", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("PDA_EMP_ID", "Employee ID", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("PDA_COST_CTR", "Cost Center", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("PDA_PO_NBR", "Purchase Order", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("PDA_INT_EXT_CAND", "Internal/External Candidate", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("PDA_TRGT_POS", "Target Position", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("PDA_TIM_ORG", "Tenure with organization", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("PDA_TIP", "Tenure in current position", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("PDA_BC_FNAME", "Billing Contact First Name", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("PDA_BC_LNAME", "BillingContact Last Name", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("PDA_BC_INV_EMAIL", "Billing (Invoice) Email Address", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("PDA_BC_ADDR_1", "Billing Address 1", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("PDA_BC_ADDR_2", "Billing Address 2", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("PDA_BC_ADDR_3", "Billing Address 3", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("PDA_BC_CITY", "Billing City", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("PDA_BC_STATE", "Billing State/Region/Province", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("PDA_BC_PCODE", "Billing Postal Code", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("PDA_BC_COUNTRY", "Billing Country", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("PDA_SUPR_NAME", "Boss Name", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("PDA_DIR_RPTS", "Has Direct Reports?", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("PDA_JOB_LOC", "Job Location", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("PDA_HOME_PHONE", "Home Phone", ExtractItem.SRC_INDV));
		eItems.add(new ExtractItem("PDA_MOBL_PHONE", "Mobile Phone", ExtractItem.SRC_INDV));

		if (gr.getDisplayData("IR_Rating_Ids") == null) {
			System.out.println("No Impact Rating data available");
		} else {
			for (String impactRatingId : gr.getDisplayData("IR_Rating_Ids").split(",")) {
				eItems.add(new ExtractItem(impactRatingId + "_Name", gr.getDisplayData(impactRatingId + "_Name"),
						ExtractItem.SRC_INDV));
				eItems.add(new ExtractItem(impactRatingId + "_Score", ExtractItem.SRC_INDV, ExtractItem.DATA_INT));
			}
		}
		return eItems;
	}

	/**
	 * Set up the output data order based upon the data previously gathered.
	 *
	 * Note that the order has changed from the original spec. Order is now per
	 * the email from Vidula dated 8/17/2010
	 *
	 * @throws Exception
	 */
	public static GroupReport genHtmlString(ArrayList<ExtractItem> lst, GroupReport gr) {
		// println "Start genHtmlString";
		String headerStr = null;

		// Loop through the individualReports generating the requisite string(s)
		for (IndividualReport ir : gr.getIndividualReports()) {
			// println "In IRloop...";
			if (headerStr == null) {
				// First time through... generate the header string
				// println "Do the header...";
				headerStr = "<table>";
				// loop through the extract items and put out the header string
				for (ExtractItem itm : lst) {
					headerStr += "<tr><td>";
					if (itm.getSrc() == ExtractItem.SRC_HDG) {
						headerStr += "<b>" + itm.getDispName() + "</b>";
					} else {
						headerStr += itm.getDispName();
					}
					headerStr += "</td></tr>";
				}
				headerStr += "</table>";
				// println "header done. string=" + headerStr;
			}

			// Add the header to this ir
			ir.addDisplayData("EXTRACT_HEADER_HTML", headerStr);

			// Now do the data
			String dataStr = "<table>";
			for (ExtractItem itm : lst) {
				dataStr += "<tr><td>";
				if (itm.getSrc() == ExtractItem.SRC_HDG) {
					// put nothing into the td for an individual's data
				} else if (itm.getSrc() == ExtractItem.SRC_GRP) {
					// System.out.println(" GRP Value=itm.getKey()="
					// +itm.getKey()+ " " + gr.getDisplayData(itm.getKey()));
					dataStr += gr.getDisplayData(itm.getKey()) == null ? "" : gr.getDisplayData(itm.getKey());
				} else if (itm.getSrc() == ExtractItem.SRC_INDV) {
					// System.out.println(" INDV
					// Value=itm.getKey()="+itm.getKey()+ " " +
					// ir.getScriptedData(itm.getKey()));
					dataStr += ir.getDisplayData(itm.getKey()) == null ? "" : ir.getDisplayData(itm.getKey());
				} else {
					// Must be an RDx label
					ReportData rd = null;
					// Get the appropriate ReportData object
					rd = ir.getReportData().get(itm.getRptKey());
					if (rd == null)
						rd = new ReportData();

					if (itm.getSrc() == ExtractItem.SRC_RDD) {
						// then put out the display data
						// System.out.println(" RDD Key=" + itm.getKey());
						// System.out.println(" RDD Value=" +
						// ir.getDisplayData(itm.getKey()));
						dataStr += rd.getDisplayData(itm.getKey()) == null ? "" : rd.getDisplayData(itm.getKey());
					} else if (itm.getSrc() == ExtractItem.SRC_RDS) {
						// then put out the score data
						// System.out.println(" RDS Key=" + itm.getKey());
						// System.out.println("1 RDS Value=" +
						// rd.getScoreData(itm.getKey()) == null ? "" :
						// rd.getScoreData(itm.getKey()));
						dataStr += rd.getScoreData(itm.getKey()) == null ? "" : rd.getScoreData(itm.getKey());
					}
				}
				dataStr += "</td></tr>";
			}
			dataStr += "</table>";

			ir.addDisplayData("EXTRACT_DATA_HTML", dataStr);
		}

		return gr;
	}

	//
	// Instance data.
	//

	//
	// Constructors.
	//

	//
	// Instance methods.
	//

}