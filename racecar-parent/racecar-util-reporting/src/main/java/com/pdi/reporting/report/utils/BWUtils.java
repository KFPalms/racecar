package com.pdi.reporting.report.utils;

import com.pdi.scoring.Norm;

/*
 * BWUtils contains a number of methods called when generating
 * bar-and-whisker reports or when generating a score position
 * based upon its norm.
 */
public class BWUtils
{
	//
	// Static constants
	//

	/*	The following array contains Z scores for the standard PDI reference
	 *	lines.  As above, each Z score represents a pair of lines.  Usually
	 *	associated with the Gen Pop or abitrary norm information.
	 *
	 *	0.412463 -> 66/34 pctl - no longer used
	 */
	private static final double[] REF_Z = new double[3];
	static {
		REF_Z[0] = 2.326348d;	// 99/1 pctl
		REF_Z[1] = 1.880794d;	// 97/3 pctl
		REF_Z[2] = 0.994458d;	// 84/16 pctl
	}
	
	/*	The following array contains Z scores for the standard PDI rating cut
	 *	percentiles.  Each Z score represents a percentile pair because the Z
	 *	score is symmetric about the mean (negative if below the mean, positive
	 *	if above the mean).  The absolute value is what is present in this
	 *	array.  The percentiles represented by each Z score are indicated in
	 *	the comments below.  Usually associated with the Spec Pop information.
	 */
	private static final double[] CUT_Z = new double[4];
	static
	{
		// Old values for PDI ratings
		//CUT_Z[0] = 1.644854d;	// 95/5 pctl
		//CUT_Z[1] = 1.036433d;	// 85/15 pctl
		//CUT_Z[2] = 0.524401d;	// 70/30 pctl
		//CUT_Z[3] = 0.253347d;	// 60/40 pctl
		
		// Values for bar-and-whisker/space-ship) cutpoints (PDI ratings/stanines)
		CUT_Z[0] = 1.880794d;	// 97/3 pctl
		CUT_Z[1] = 1.226528d;	// 89/11 pctl
		CUT_Z[2] = 0.738847d;	// 77/23 pctl
		CUT_Z[3] = 0.253347d;	// 60/40 pctl
		// Note that if the cut percentiles change, then the stanine cuts (in the Norm, ScoreUtils,
		// TLT_GROUP_DETAIL_REPORT_SCORING, and TLT_INDIVIDUAL_SUMMARY_REPORT_SCORING class must also change
	}
		
	// the Z score of the end point (1st/99th percentiles) of the drawing area
	private static double MAXMIN_Z = 2.326348;

	
	//
	// Static Methods
	//
	/*
	 *	The array based methods in this class calculate physical positions for
	 *  the percentile pairs represented by the Z scores used in the respective
	 *  methods.  The output of each will be an array of floats that will be
	 *  twice the size of the z-score array used + 1 (the position of the mean
	 *  will be included).  The values returned will be in ascending order by
	 *  percentile, assuming that the input array was arranged by ascending Z
	 *  score.  The position of the mean (50th percentile) is also returned at
	 *  its proper position in the array (the middle element).
	 *
	 *	The idea is to plot the relevant (cut or reference) scores calculated
	 *  from the operative norm into the graphical context.  In general, the
	 *  graphical context is based upon the GenPop norms.
	 *
	 *	In essence, these routines use the graphical information for the graphical
	 *  context to determine the relative position value of each cut score
	 *  (calculated by using the norm relevant to the population under examination
	 *  - the 'Special Population' norm).  Thus, the cut scores are mapped onto the
	 *  graphical context of the norm used to generate the graphical context
	 *  information (the General Population	norm).  If the same norm is used to
	 *  generate both the graphical context and this array, the graphical construct
	 *  will be centered in the 50th percentile (the mean) and will be symmetric about it.
	 *  
	 *  The output will map the entire space between the 1st percentile and the 99th
	 *  percentile as a percentage (to 1 decimal digit) of the space between them.
	 *  The actual calculations will take place at a precision of 1000 units and
	 *  will be rounded (by adding 1/2 to the final result).  We then apply the
	 *  Math.floor() function to get the integer below the final rounded result
	 *  and divide by 10 to get a percentage with a single digit of decimal precision.
	 *  
	 *  Note that there is also a simple method included for calculating the position
	 *  of the score in the graphical context.  Pass it a score and a norm and it
	 *  passes back a float representing the position of the score in the context.
	 *  Like all of the other values passed back it is calculated on a scale of 1 to
	 *  100 with a single decimal digit of precision.
	 */
	

	/*
	 * getRefPosArray
	 *		Generates the reference lines at 1st, 3rd, 16th, 50th, 84th, 97th, and
	 *		99th percentiles.  The reference lines are mapped to the general context
	 *		so will match the reference lines no matter what Gen. Pop. norm is used.
	 *
	 *		Note that all calculations take place at 10x precision (1000 units) and
	 *		are down-converted to a 100 unit scale (percentage) with a single decimal
	 *		digit of precision) when placed into the output array.
	 *
	 *	For each of the z scores in the selected zArray:
	 *		1) Calculate the operative score
	 *		2) Subtract it from the reference max score calculated earlier
	 *		3) Multiply it by the reference conversion factor
	 *		4) Round it
	 *		5) Place it in its proper position in the output array
	 *	Note that this is done twice for each reference Z score once above
	 *	the mean (for pctls above 50) and once below the mean (for
	 *	percentiles below 50).
	 *
	 *	@return	 An array of floats with the relative position of each
	 *           reference line
	 */
	public static float[] getRefPosArray()
	{
		double d;
		double score;
		
		// Output array is twice the size of input plus 1 for the mean,
		//so the max index value of the new array (0 to 2 x input size)

		float[] ary = new float[(REF_Z.length * 2) + 1];
		int maxI = REF_Z.length * 2;	
		
		// For reference, we don't need a "real" mean because the reference
		// lines will always reside in the same relative position in the
		// graphical context no matter what the norm is.
		Norm norm = new Norm(100.0, 25.0);	// create an arbitrary norm
		GraphicalContext gc = new GraphicalContext(norm);
		
		//	This will only work for curves that are symmetric about the mean.
		for (int i = 0; i < REF_Z.length; i++)
		{
			// Get the deflection from the mean
			d = REF_Z[i] * norm.getStdDev();

			// calc the high cut score,
			// Get rounded position, down convert, put into array
			score = norm.getMean() + d;
			ary[maxI-i] = (float)(Math.floor(gc.scorePos(score)) / 10.0);
			
			// calc the low cut score,
			// Get rounded position, down convert, put into array
			score = norm.getMean() - d;
			ary[i] = (float)(Math.floor(gc.scorePos(score)) / 10.0);
		}
		
		// Now calculate the graphical position of the mean; 50th pctl score is the mean
		// Get rounded position, down convert, put into array
		// Note that the mean goes into the middle of the output array at the position of the length of the input array
		score = norm.getMean();
		ary[REF_Z.length] = (float)(Math.floor(gc.scorePos(score)) / 10.0);
		
		return ary;
	}


	/**
	 * getCutPosArray
	 *		Generates the positions of the bar and whisker constructs as mapped on the
	 *		Gen. Pop. graphical context.  Gives positions in the graphical context (the
	 *		drawing space defined by the Gen. Pop. norm) for the 5th, 15th, 30th, 40th,
	 *		60th, 70th, 85th and 95th percentile points of the Spec. Pop. norm.
	 *
	 *		Note that all calculations take place at 10x precision (1000 units) and
	 *		are down-converted to a 100 unit scale (percentage) with a single decimal
	 *		digit of precision) when placed into the output array.
	 *
	 *		Note also that the scores may lie outside of the graphical context in
	 *		that that they may be greater than 100 or less than 0.0.
	 * 
	 * @param gpNorm - The Norm object containing the General Population norm.
	 * @param spNorm - The Norm object containing the Special Population norm.
	 * @return -An array of floats with the relative position of each of the
	 *          cut points
	 */
	public static float[] getCutPosArray(Norm gpNorm, Norm spNorm)
	{
		double d;	// Working bucket
		double score;	// Score at cut point

		// Output array is exactly twice the size of input (no mean),
		// so the max index will be the size of the output array - 1
		float[] ary = new float[(CUT_Z.length * 2)];
		int maxI = (CUT_Z.length * 2) - 1;

		// Get the graphical context
		GraphicalContext gc = new GraphicalContext(gpNorm);

		//	This will only work for curves that are symmetric about the mean.
		for (int i = 0; i < CUT_Z.length; i++)
		{
			// get the deflection from the mean
			d = CUT_Z[i] * spNorm.getStdDev();

			// calc the high cut score,
			// Get rounded position, down convert, put into array
			score = spNorm.getMean() + d;
			ary[maxI-i] = (float)(Math.floor(gc.scorePos(score)) / 10.0);

			// calc the low cut score,
			// Get rounded position, down convert, put into array
			score = spNorm.getMean() - d;
			ary[i] = (float)(Math.floor(gc.scorePos(score)) / 10.0);
		}
		
		return ary;
	}


	/*
	 * calcScorePos
	 *		Generates the position of the score in the GP graphical context
	 *
	 *		The norm passed in determines the graphical context in which the scores
	 *		are drawn.  That context is determined by the norm passed in (usually the
	 *		general population norm).  The position calculated within that graphical
	 *		context is expressed as a range of values from 0.0 (which coincides with
	 *		the 1st percentile marker) to 100.0 (which coincides with the 99th
	 *		percentile marker.  Therefore, there can be no scores that appear outside
	 *		of that context.  Scores at the extremes which would otherwise lie outside
	 *		the context will be truncated; if below 0.0 they will be set to 0.0 or if
	 *		above 100.0 they will be set to 100.0. 
	 *
	 *		The above assumptions make this method unsuitable for calculating
	 *		cutpoints for the bar-and-whisker/spaceship construct; those constructs
	 *		are calculated in different methods.
	 *
	 *		NOTE:  This calculation is used for score positioning only!
	 *		NOTE:  All calculations take place at 10x precision (1000 units)
	 *			   and are down-converted to a 100-point scale (percentage)
	 *			   with a single decimal digit of precision) when returned.
	 * 
	 * @param score The score for the scale
	 * @param norm The Gen. Pop. norm for the scale
	 * @return	 An array of floats with the relative position of each
	 *           reference line
	 */
	public static float calcScorePos(double score, Norm norm)
	{
		// Old (unrounded) calulation
		//GraphicalContext gc = new GraphicalContext(norm);
		//return (float)(Math.floor(gc.scorePos(score)) / 10.0);	// Get position value, down convert
		
		// New (truncated) calculation
		GraphicalContext gc = new GraphicalContext(norm);
		// Get position value, down convert
		double scor = (Math.floor(gc.scorePos(score)) / 10.0);
		if (scor < 0.0)
			scor = 0.0;
		else if (scor > 100.0)
			scor = 100.0;
		
		return (float) scor;
	}
	
	//
	//  Private class
	//
	
	/*
	 * This class acts as a holder for context information.  Creating
	 * this class creates the context information required to generate
	 * position information.  Because of this, the following are true
	 * for this class:
	 * 		- There no "empty" constructor for this class.
	 * 		- There are no publicly accessible getters or setters for
	 * 		  the class variables.  Only the position is needed, so
	 * 		  only the calulation for it is exposed.
	 * 
	 * The context is exactly 100 units wide when results are returned
	 * from the routines in the public class.  This results in positions
	 * that are percentages of the width of the context.  Note, however,
	 * that all calculations in the public class are done using a space
	 * that is 1000 units wide and rounded at return.  That means that
	 * the conversion units calulated within this private class will be
	 *  based upon a space that is 1000 units wide.
	 */
	private static class GraphicalContext
	{
		//
		// Instance variables
		//
		private double _maxScore;	// The maximum score in this context (99th percentile
		private double _minScore;	// The minimum score in this context (1st percentile
		private double _conv;		// The conversion factor of score point to graphical
									// points in this context

		//
		// Constructor
		//
		protected GraphicalContext(Norm norm)
		{
			// Calculate the min and max scores and conversion factor
			double d = MAXMIN_Z * norm.getStdDev();
			_maxScore = norm.getMean() + d;
			_minScore = norm.getMean() - d;
			_conv = 1000.0 / (_maxScore - _minScore);
		}
		
		
		//
		// Public methods
		//
		protected double scorePos(double score)
		{
			// Calc number of units & round it up 
			return ( ((score - _minScore) * _conv) + 0.5);	

		}
	}
}
