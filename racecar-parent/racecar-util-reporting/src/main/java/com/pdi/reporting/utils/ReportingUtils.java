/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.reporting.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
//import java.util.ArrayList;
import java.util.Properties;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.runtime.resource.loader.StringResourceLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xhtmlrenderer.pdf.ITextRenderer;
//import org.xhtmlrenderer.pdf.PDFCreationListener;

import com.lowagie.text.pdf.BaseFont;
//import com.lowagie.text.pdf.PdfName;
//import com.lowagie.text.pdf.PdfObject;
import com.pdi.properties.PropertyLoader;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.Report;
import com.pdi.string.StringUtils;

/**
 * Pulls a template from the resource
 * 
 *
 * @author		Gavin Myers
 */

public class ReportingUtils {
	
	private static final Logger log = LoggerFactory.getLogger(ReportingUtils.class);
	/**
	 * Creates a PDF from
	 * @param template the template's path
	 * @param report
	 * @param data
	 * @param savePath
	 * @throws Exception
	 */
	public static ByteArrayOutputStream renderTemplate(String templatePath, Report report) throws Exception {
		log.debug("Loading Template from path: {}", templatePath);
		Template template = loadTemplate(templatePath);
		
		if (report == null){
			log.error("Report is NULL");
		}
		
		//Fill the template with data
        VelocityContext context = new VelocityContext();
        context.put("report", report);

        StringWriter writer = new StringWriter();
        template.merge( context, writer );
        String content = writer.toString();
        //log.debug("Creating PDF from String: {}", content);
        ByteArrayOutputStream os = createPDF( content);
        return os;
	}
	
	/**
	 * Creates a PDF from
	 * @param template the template's path
	 * @param report
	 * @param data
	 * @param savePath
	 * @throws Exception
	 */
	public static ByteArrayOutputStream renderHTMLtoPDF(String html) throws Exception {
        ByteArrayOutputStream os = createPDF(html);
        return os;
	}
	
	/**
	 * Creates a String from
	 * @param template the template's path
	 * @param report
	 * @param data
	 * @param savePath
	 * @throws Exception
	 */
	public static String renderHTML(String templatePath, Report report) throws Exception {
		Template template = loadTemplate(templatePath);

		//Fill the template with data
        VelocityContext context = new VelocityContext();
        context.put("report", report);

        StringWriter writer = new StringWriter();
        
        template.merge( context, writer );
        String resp = writer.toString();
        
        //We don't really know until after everything has been rendered if this document is unicode or not
        //Ideally this would be set in a different location, and not be so brutal
        

        return resp;
	}
	
	public static ByteArrayOutputStream renderExtract(String templatePath, Report report) throws Exception {
		Template template = loadTemplate(templatePath);
		
		//Fill the template with data
        VelocityContext context = new VelocityContext();
        context.put("report", report);

        StringWriter writer = new StringWriter();
        template.merge( context, writer );
        
        ByteArrayOutputStream os = createXLS( writer.toString());
        return os;
	}
	
	/**
	 * Build a velocity template file that can be used to generate a report
	 * @param templatePath the path of the velocity file
	 * @return
	 * @throws Exception
	 */
	private static Template loadTemplate(String templatePath) throws Exception {
		Properties p = new Properties();
		p.setProperty("resource.loader", "string");
		p.setProperty("string.resource.loader.class","org.apache.velocity.runtime.resource.loader.StringResourceLoader");
		p.setProperty("input.encoding", "utf-8");
		p.setProperty("output.encoding", "utf-8");
		//System.out.println("  template path " + templatePath);
		Velocity.init(p);
		InputStream inStream = ReportingUtils.class.getResourceAsStream(templatePath);
		String templateString = inputStreamAsString(inStream);
		String finalHTML = "";
		for(String m : ReportingConstants.reportMacros.keySet()) {
			
			InputStream macStream = ReportingUtils.class.getResourceAsStream("/com/pdi/reporting/report/vm/macro/"+m+".html");
			String macCode = inputStreamAsString(macStream);
			finalHTML += macCode;
		}
		finalHTML += templateString;
		
		if(PropertyLoader.getProperty("com.pdi.reporting.application", "application.isDebugMode").equalsIgnoreCase("1")) {
			
			BufferedWriter out = new BufferedWriter(new FileWriter("velocity.html"));
			out.write(finalHTML);
			out.close();
		}
		
		
		StringResourceLoader.getRepository().putStringResource("VMTemplate", finalHTML);
		Template template = Velocity.getTemplate( "VMTemplate");
		
        return template;
	}
	
	public static String pathToString(String path) throws Exception {		
		InputStream inStream = ReportingUtils.class.getResourceAsStream(path);
		//System.out.println(ReportingUtils.class.getResourceAsStream("/com/pdi/data/reporting/report/vm/TLT_GROUP_SUMMARY_REPORT.groovy"));
		//System.out.println("Reporting utils: pathToString  "  + path );
		if(inStream == null)
			log.error("inStream == null  {}", path);
		String templateString = inputStreamAsString(inStream);

		return templateString;
	}
	
	/**
	 * Convert an input stream to a string
	 * @param stream
	 * @return
	 * @throws Exception
	 */
	private static String inputStreamAsString(InputStream stream)
	throws Exception {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(stream));
			StringBuilder sb = new StringBuilder();
			String line = null;
		
			while ((line = br.readLine()) != null) {
			sb.append(line + "\n");
			}
		
			br.close();
			return sb.toString();
		} catch (Exception e) {
			return null;
		}
	}
	
	

	/**
	 * builds the actual PDF file
	 * @param input
	 * @param savePath
	 * @throws Exception
	 */ 
    private static ByteArrayOutputStream createPDF(String input)
            throws Exception {
			ByteArrayOutputStream os = new ByteArrayOutputStream();


	//As of Java 1.4, you could also try something a little cleaner:
	input = input.trim().replaceFirst("^([\\W]+)<","<");
	input = input.replace("nullnullnullnull", "");
	//System.out.println("createPDF -- input string:   " + input);
			//using unicode fonts in every template now, no matter the lang selection.
			//if(!StringUtils.isISOString(input)) {
	        	//input = input.replace("http://localhost:8080/pdi-web-reporting/css/layout.css", "http://localhost:8080/pdi-web-reporting/css/layout_unicode.css");
	        	//input = input.replace("http://localhost:8080/pdi-web-reporting/css/styles.css", "http://localhost:8080/pdi-web-reporting/css/styles_unicode.css");
	        	//input = input.replace("pdi-web-reporting/css/layout.css", "pdi-web-reporting/css/layout_unicode.css");
	        	//input = input.replace("pdi-web-reporting/css/styles.css", "pdi-web-reporting/css/styles_unicode.css");
	       // }
			
			if(PropertyLoader.getProperty("com.pdi.reporting.application", "application.isDebugMode").equalsIgnoreCase("1")) {
				//Print out a schnaaazy html file
				//BufferedWriter out = new BufferedWriter(new FileWriter()); 
				BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("Velocity_PDF_FINALE.html"),"UTF8"));

				out.write(input); 
				out.close();
			}
			
	        ITextRenderer renderer = new ITextRenderer();
	        
	        MetaDataCreationListener mcl = new MetaDataCreationListener();
	        mcl.getMetaData();
	        renderer.setListener(mcl);
	        
	        //Object val = "T67y";
	       // renderer.getWriter().getInfo().put(PdfName.TITLE, (PdfObject) val);
	        if(!StringUtils.isISOString(input)) {
	        	renderer.getFontResolver().addFont ("C:\\WINDOWS\\Fonts\\ARIALUNI.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED );
	        }
	        //renderer.getFontResolver().addFont ("C:\\WINDOWS\\Fonts\\TIMES.TTF", BaseFont.IDENTITY_H, BaseFont.EMBEDDED );
	        //renderer.getFontResolver().addFont ("C:\\WINDOWS\\Fonts\\DejaVuSerif.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED );
	        log.trace("About to set input string to renderer: {}", input);
	        renderer.setDocumentFromString(input);
	        renderer.layout();
	       
	        renderer.createPDF(os);
	
	        return os;
    }
    
      
	/**
	 * builds the actual PDF file
	 * @param input
	 * @param savePath
	 * @throws Exception
	 */
    private static ByteArrayOutputStream createXLS(String input)
            throws Exception
	{
		//System.out.println("Raw XML:");
		//System.out.println(input);
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			
			os.write(input.getBytes()); 
	
	        return os;
            

    }
}
