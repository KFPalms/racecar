/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.reporting.constants;

import java.util.ArrayList;
import java.util.HashMap;

import com.pdi.reporting.request.ReportRequestCodes;

/**
 * Various constants used by Reporting
 * 
 * Contains the following:
 * 1) Various constants are GUIDS, this is to prevent people from hard coding 
 *    string values as these string values are not easy to hard code.  Report types,
 *    reporting functions, etc
 *    Built from the online GUID generator at
 *      http://www.guidgenerator.com/online-guid-generator.aspx
 * 2) Predefined strings that denote individual reports
 * 3) Collections of logically related data (in ReportRequestCodes objects)
 * 4) Collections of data for specific reports (scales, SPSS keys, and normsets)
 *
 * @author		Gavin Myers
 */
public class ReportingConstants
{
	// Watermark value for putting into documents
	//public static final String WATERMARK = "Korn Ferry 15846ER2UL9";
	public static final String WATERMARK = "Korn Ferry N44344Z3A7E3C0E7";
	
	// Data sources
	public static final String SRC_KFP = "kfp";
	public static final String SRC_KF4D = "kf4d";
	
	// Text labels.  Defined here for use throughout reporting.
	// Create a Definition for any label that has to be included in a program
	
	// Levels
	public static final String LBL_FLL = "FRST_LVL_LDR";
	public static final String LBL_MLL = "MID_LVL_LDR";
	public static final String LBL_SLL = "SR_LVL_LDR";
	
	// Derailment Risk
	public static final String LBL_BLANK = "BLANK";
	public static final String LBL_HIGH = "HIGH";
	public static final String LBL_MODERATE = "MODERATE";
	public static final String LBL_LOW = "LOW";
	public static final String LBL_MINIMAL = "MINIMAL";
	
	// Derailment Factors
	public static final String LBL_EGO_CNTRD = "EGO_CNTRD";
	public static final String LBL_MANIP = "MANIP";		// Manipulation, not Manipulative
	public static final String LBL_MICRO_MANG = "MICRO_MANG";
	public static final String LBL_UNWILLING_CONFRONT = "UNWILLING_CONFRONT";
	
	
	//Score Request
	public static String REPORT_TYPE_SCORE = "f44243193-f65d-35fb-vb4e-ed04b4ebd056";
	
	//Group vs Individual Reports
	public static String REPORT_TYPE_GROUP = "e644e193-665d-45fb-9b4e-ed04b4ebd056";
	public static String REPORT_TYPE_INDIVIDUAL = "c91d6456-d8fa-4707-a23c-bac901ad542d";
	
	//Extract report - returns an xls file (technically an html file)
	public static String REPORT_TYPE_GROUP_EXTRACT = "e224e193-665d-45fb-9b4e-ed04b4ebd056";
	public static String REPORT_TYPE_INDIVIDUAL_EXTRACT = "v21d6456-d8fa-4707-a23c-bac901ad542d";
	
	//Data report - this does not return a PDF/XLS object, simply a Java object with data
	public static String REPORT_TYPE_GROUP_DATA = "e2225193-665d-45fb-9b4e-ed04b4ebd056";
	public static String REPORT_TYPE_INDIVIDUAL_DATA = "v21616-d8fa-4707-a23c-bac901ad542d";
	
	//Response Codes
	public static String RESPONSE_TYPE_PENDING = "a2d77b5a-b9e3-4bb6-ba3a-d1a5d3e6da3e";
	public static String RESPONSE_TYPE_ERROR = "7204c351-94f4-430d-84f3-2062625b6ca5";
	public static String RESPONSE_TYPE_COMPLETE = "c5c8a857-23a3-49f9-b3d7-1f5f7bb22e02";

	// Target level types - These should exactly reflect the values in the
	//                      PALMS target_level_type table
	public static int TGT_LVL_FLL = 1;
	public static int TGT_LVL_MLL = 2;
	public static int TGT_LVL_SLL = 3;	// Note that BUL in A by D = SLL in TLT (In PALMS this is SLL)
	public static int TGT_LVL_SEA = 4;
	public static int TGT_LVL_BUL = 6;	// this is for the PALMS BUL level

	// Instrument Codes
	public static String IC_RAVENS =	"rv";		// Raven's B
	public static String IC_GPI =		"gpil";		// GPI (regular version)
	public static String IC_GPI_SHORT =	"gpi";		// GPI (short version)
	public static String IC_LEI =		"lei";		// LEI
	public static String IC_WGE =		"wg";		// Watson-Glaser E
	public static String IC_FINEX = 	"finex";	// Financial Exercise
	public static String IC_CS =		"cs";
	public static String IC_CS2 = 		"cs2";
	
	//Individual Report Codes
	public static String REPORT_CODE_INDIVIDUAL_GENERIC = "REPORT_CODE_INDIVIDUAL_GENERIC";
	//Group report Codes
	public static String REPORT_CODE_GROUP_GENERIC = "REPORT_CODE_GROUP_GENERIC";
	
	//All reports in the list as a single pdf
	public static String REPORT_CODE_ALL_REPORTS = "REPORT_CODE_ALL_REPORTS";
		
	//TLT and A By D Report Codes
	public static String REPORT_CODE_TLT_GROUP_DETAIL = "TLT_GROUP_DETAIL_REPORT";  // THIS IS THE GOOD ONE
	//public static String REPORT_CODE_TLT_GROUP_SUMMARY = "TLT_GROUP_SUMMARY_REPORT";	// Not a valid report
	public static String REPORT_CODE_TLT_INDIVIDUAL_SUMMARY = "TLT_INDIVIDUAL_SUMMARY_REPORT";
	public static String REPORT_CODE_TLT_KF_INDIVIDUAL_SUMMARY = "TLT_KF_INDIVIDUAL_SUMMARY_REPORT";
	public static String REPORT_CODE_TLT_EXTRACT = "TLT_EXTRACT_REPORT";
	public static String REPORT_CODE_TLT_EXTRACT_RAW = "TLT_EXTRACT_REPORT_RAW";
	
	public static String REPORT_CODE_ABYD_DEVELOPMENT_REPORT = "ABYD_DEVELOPMENT_REPORT";
	public static String REPORT_CODE_ABYD_FIT_REPORT = "ABYD_FIT_REPORT";
	public static String REPORT_CODE_ABYD_SUCCESS_PROFILE = "ABYD_SUCCESS_PROFILE_REPORT";
	public static String REPORT_CODE_ABYD_READINESS_REPORT = "ABYD_READINESS_REPORT";
	public static String REPORT_CODE_ABYD_CUSTOM_MODEL_REPORT = "ABYD_CUSTOM_MODEL_REPORT";
	// This one is the Custom Dashboard Report
	public static String REPORT_CODE_ABYD_CUSTOM_REPORT = "ABYD_CUSTOM_REPORT";

	public static String REPORT_CODE_ABYD_TRANSITION_SCALES = "ABYD_TRANSITION_SCALES";	
	
	public static String REPORT_CODE_FINEX_DETAIL = "FINEX_DETAIL_REPORT";
	public static String REPORT_CODE_GPI_GRAPHICAL = "GPI_GRAPHICAL_REPORT";
	public static String REPORT_CODE_LEI_GRAPHICAL = "LEI_GRAPHICAL_REPORT";
	public static String REPORT_CODE_RAV_SF_GRAPHICAL = "RAV_SF_GRAPHICAL_REPORT";
	public static String REPORT_CODE_RAV_B_GRAPHICAL = "RAV_B_GRAPHICAL_REPORT";
	public static String REPORT_CODE_WGE_GRAPHICAL = "WGE_GRAPHICAL_REPORT";
	public static String REPORT_CODE_CHQ_DETAIL = "CHQ_DETAIL_REPORT";
	// Replaced by WGE_GRAPHICAL REPORT.
	//public static String REPORT_CODE_WGE_DETAIL = "WGE_DETAIL_REPORT";
	
	public static String REPORT_CODE_ABYD_EXTRACT = "ABYD_EXTRACT";
	public static String REPORT_CODE_ABYD_RAW_EXTRACT = "ABYD_RAW_EXTRACT";
	public static String REPORT_CODE_ASSESSMENT_EXTRACT = "ASSESSMENT_EXTRACT";
	public static String REPORT_CODE_ABYD_ANALYTICS_EXTRACT = "ABYD_ANALYTICS_EXTRACT";
	
	public static String REPORT_CODE_ALR_EXTRACT =  "ALR_REPORT_EXTRACT";
	
	public static String REPORT_CODE_ALR_EXTRACT_V2 =  "ALR_REPORT_EXTRACT_V2";
	
	public static String REPORT_CODE_KFALP_RAW_EXTRACT =  "KFALP_RAW_EXTRACT";
	
	// Eval Guide
	public static String REPORT_CODE_EVAL_GUIDE = "ABYD_EVAL_GUIDE";
	
	// Integration Grid
	public static String REPORT_CODE_INT_GRID_REVIEW = "ABYD_INT_GRID_REVIEW";
	public static String REPORT_CODE_INT_GRID_SNAPSHOT = "ABYD_INT_GRID_SNAPSHOT";

	// The Traits and Behavior reports
	public static String REPORT_CODE_ALR_TRAITS = "ALR_TRAITS";
	public static String REPORT_CODE_ALR_DRIVERS = "ALR_DRIVERS";
	public static String REPORT_CODE_KF4D_TRAITS = "KF4D_TRAITS";
	public static String REPORT_CODE_KF4D_DRIVERS = "KF4D_DRIVERS";
	public static String REPORT_CODE_KF4D_RISK = "KF4D_RISK";
	
	// Target report codes
	public static String REPORT_CODE_ABYD_TARGET_PPT = "ABYD_TARGET_LP_PART_REPORT";
	public static String REPORT_CODE_ABYD_TARGET_ORG = "ABYD_TARGET_LP_ORG_REPORT";
	
	// REPORT_TYPE -- used mainly (as of 08-08-2011) for the TLT_GROUP_DETAIL_REPORT.html
	public static String REPORT_TYPE_TLT = "TLT";
	public static String REPORT_TYPE_ABYD = "ABYD";
	
	//
	//public static String REPORT_CODE_IBLVA_PARTICIPANT_REVIEW = "IBLVA_PARTICIPANT_REVIEW";	// Not needed (yet)
	public static String REPORT_CODE_IBLVA_CONSULTANT_REVIEW = "IBLVA_CONSULTANT_REVIEW";
	
	public static String REPORT_CODE_NIS_CONSULTANT_REVIEW = "NIS_CONSULTANT_REVIEW";
	public static String REPORT_CODE_ISMLL_CONSULTANT_REVIEW = "ISMLL_CONSULTANT_REVIEW";
	public static String REPORT_CODE_ISBUL_CONSULTANT_REVIEW = "ISBUL_CONSULTANT_REVIEW";
	public static String REPORT_CODE_ISZ_CONSULTANT_REVIEW = "ISZ_CONSULTANT_REVIEW";
	public static String REPORT_CODE_EIS_CONSULTANT_REVIEW = "EIS_CONSULTANT_REVIEW";
	public static String REPORT_CODE_BRE2_CONSULTANT_REVIEW = "BRE2_CONSULTANT_REVIEW";
	public static String REPORT_CODE_BRESEA_CONSULTANT_REVIEW = "BRESEA_CONSULTANT_REVIEW";
	public static String REPORT_CODE_SFECEO_CONSULTANT_REVIEW = "SFECEO_CONSULTANT_REVIEW";
	public static String REPORT_CODE_BREMED_CONSULTANT_REVIEW = "BREMED_CONSULTANT_REVIEW";

	// Externasl report codes
	public static String REPORT_CODE_KF4D_TDLA = "TDLAReport";
	public static String REPORT_CODE_KF4D_TD = "TDReport";
	public static String REPORT_CODE_KF4D_LA = "LAReport";
	public static String REPORT_CODE_KF4D_ENTERPRISE_RISK = "EnterpriseRiskFactorsReport";
	public static String REPORT_CODE_POTENTIAL = "POTENTIAL_REPORT";
	
	public static String[] PALMS_REPORT_CODE_LIST = {REPORT_CODE_KF4D_TDLA, REPORT_CODE_KF4D_TD, REPORT_CODE_KF4D_LA, REPORT_CODE_KF4D_ENTERPRISE_RISK, REPORT_CODE_POTENTIAL};
	
	//Generic report codes
	public static String RC_GPI = "GPI";
	public static String RC_LEI = "LEI";
	public static String RC_RAVENS_SF = "RAVENS_SF";
	public static String RC_RAVENS_B = "RAVENS_B";
	public static String RC_CAREER_SURVEY = "CAREER_SURVEY";
	public static String RC_WESMAN = "WESMAN_PCT";
	public static String RC_WATSON_A_SF = "WG_A_SF";
	public static String RC_FIN_EX = "FINEX";
	public static String RC_CHQ = "CHQ";
	public static String RC_WG_E = "WG_E";
	
	public static String RC_COGNITIVES_INCLUDED = "COGNITIVES_INCLUDED";
	
	// used to determine if all data is required for a report, 
	// or if a report can be written with only partial data
	public static String PART_COMPL_RPT = "PART_COMPL_RPT";
	public static String PARTIAL_REPORT = "PARTIAL";
	public static String COMPLETE_REPORT = "COMPLETE";
	
	// currently this mean/stdev is the same as the
	// Raven's instrument global norm.  eventually
	// the two will diverge.
	public static double RAVENS_TLT_MEAN = 14.3;
	public static double RAVENS_TLT_STDEV = 4.1;
	
	
	
	// these are mapped to the nhn database project_type_id's
	public static int TLT_WITH_COGS = 1;  	//	TLT
	public static int TLT_NO_COGS = 2;		//	TLT
	public static int ABYD_ONLINE = 3;		// ABYD
	public static int ASSESSMENT = 4;		// ASMT
	public static int TARGET_FULL_LP = 5;		
	public static int TARGET_EXTENDED_LP = 6;		
	public static int TARGET_ASIA_INDIA_LP = 7;		
	public static int TARGET_CANADA_LP = 8;		
	//  project_type_id	name		     			code
	//	12				MLL LVA - Development		MLLLVA
	public static int MLL_LVA = 12;		//MLLLVA
	
	public static int TLT_LA_NO_COGS = 17;		// TLT_LAG
	public static int TLT_LA_WITH_COGS = 18;	// TLT_LAG_COG
	

	public static HashMap<String, ReportRequestCodes> reportRequestMap = 
												new HashMap<String, ReportRequestCodes>();
	
	public static HashMap<String, String> reportMacros = 
		new HashMap<String, String>();

	//TODO: This should all be database driven (? or xml, or something that can be dynamic)
	static
	{
		
		reportMacros.put("MACRO_CSS", "MACRO_CSS");
		reportMacros.put("MACRO_TABLES", "MACRO_TABLES");
		reportMacros.put("MACRO_PAGE", "MACRO_PAGE");
		reportMacros.put("MACRO_ABYD", "MACRO_ABYD");
		
		// Setups for reports																// Report Code (ID)						//  Script						// Template							// Text Group     		// Extract Template (optional)
		reportRequestMap.put(REPORT_CODE_INDIVIDUAL_GENERIC,		new ReportRequestCodes(REPORT_CODE_INDIVIDUAL_GENERIC,			"CHQ",							 REPORT_CODE_INDIVIDUAL_GENERIC,	null));
		reportRequestMap.put(REPORT_CODE_GROUP_GENERIC,				new ReportRequestCodes(REPORT_CODE_GROUP_GENERIC,				REPORT_CODE_GROUP_GENERIC,		 REPORT_CODE_GROUP_GENERIC,			null));
		
		reportRequestMap.put(REPORT_CODE_TLT_GROUP_DETAIL,			new ReportRequestCodes(REPORT_CODE_TLT_GROUP_DETAIL,        	"TLT_GROUP_DETAIL_REPORT",		"TLT_GROUP_DETAIL_REPORT",			"TLT_GRP_DETAIL_RPT"));
		//reportRequestMap.put(REPORT_CODE_TLT_GROUP_SUMMARY,			new ReportRequestCodes(REPORT_CODE_TLT_GROUP_SUMMARY,       	"TLT_GROUP_SUMMARY_REPORT",		"TLT_GROUP_SUMMARY_REPORT",			"TLT_GRP_DETAIL_RPT"));
		reportRequestMap.put(REPORT_CODE_TLT_INDIVIDUAL_SUMMARY,	new ReportRequestCodes(REPORT_CODE_TLT_INDIVIDUAL_SUMMARY,  	"TLT_INDIVIDUAL_SUMMARY_REPORT", "TLT_INDIVIDUAL_SUMMARY_REPORT",	"TLT_IND_SUMMARY_RPT"));
		reportRequestMap.put(REPORT_CODE_TLT_KF_INDIVIDUAL_SUMMARY,	new ReportRequestCodes(REPORT_CODE_TLT_KF_INDIVIDUAL_SUMMARY,  	"TLT_INDIVIDUAL_SUMMARY_REPORT", "TLT_KF_INDIVIDUAL_SUMMARY_REPORT","TLT_IND_SUMMARY_RPT"));
		reportRequestMap.put(REPORT_CODE_TLT_EXTRACT,				new ReportRequestCodes(REPORT_CODE_TLT_EXTRACT,  				"TLT_EXTRACT", 			  		 "NO_TEMPLATE",						"TLT_GRP_DETAIL_RPT",	"TLT_EXTRACT"));
		
		//reportRequestMap.put(REPORT_CODE_ABYD_SUCCESS_PROFILE,		new ReportRequestCodes(REPORT_CODE_ABYD_SUCCESS_PROFILE, 		"ABYD_REPORT",   				"ABYD_SUCCESS_PROFILE_REPORT",		null));
		reportRequestMap.put(REPORT_CODE_ABYD_DEVELOPMENT_REPORT,	new ReportRequestCodes(REPORT_CODE_ABYD_DEVELOPMENT_REPORT, 	"ABYD_REPORT",     				"ABYD_DEVELOPMENT_REPORT",			"ABYD_DEV_RPT"));
		reportRequestMap.put(REPORT_CODE_ABYD_FIT_REPORT,			new ReportRequestCodes(REPORT_CODE_ABYD_FIT_REPORT, 			"ABYD_REPORT",       			"ABYD_FIT_REPORT",					"ABYD_FIT_RPT"));
		reportRequestMap.put(REPORT_CODE_ABYD_CUSTOM_MODEL_REPORT,	new ReportRequestCodes(REPORT_CODE_ABYD_CUSTOM_MODEL_REPORT,	"ABYD_REPORT",       			"ABYD_CUSTOM_MODEL_REPORT",			"GENERIC_REPORT_TEXT"));
		reportRequestMap.put(REPORT_CODE_ABYD_READINESS_REPORT,		new ReportRequestCodes(REPORT_CODE_ABYD_READINESS_REPORT, 		"ABYD_REPORT",       			"ABYD_READINESS_REPORT",			"ABYD_READ_RPT"));
		reportRequestMap.put(REPORT_CODE_ABYD_TRANSITION_SCALES,	new ReportRequestCodes(REPORT_CODE_ABYD_TRANSITION_SCALES, 		"TLT_GROUP_DETAIL_REPORT",      "TLT_GROUP_DETAIL_REPORT",			"TLT_GRP_DETAIL_RPT"));
		// This is the Custom Dashboard Report
		reportRequestMap.put(REPORT_CODE_ABYD_CUSTOM_REPORT,		new ReportRequestCodes(REPORT_CODE_ABYD_CUSTOM_REPORT,			"ABYD_REPORT",       			"ABYD_CUSTOM_REPORT",				"ABYD_FIT_RPT"));
		
		reportRequestMap.put(REPORT_CODE_FINEX_DETAIL, 				new ReportRequestCodes(REPORT_CODE_FINEX_DETAIL, 				"FINEX", 						"FINX_SUMMARY_REPORT",				"ABYD_DEV_RPT"));
		reportRequestMap.put(REPORT_CODE_GPI_GRAPHICAL,				new ReportRequestCodes(REPORT_CODE_GPI_GRAPHICAL,           	RC_GPI,							"GPI_GRAPHICAL_REPORT",				"GENERIC_REPORT_TEXT"));
		reportRequestMap.put(REPORT_CODE_LEI_GRAPHICAL,				new ReportRequestCodes(REPORT_CODE_LEI_GRAPHICAL, 				RC_LEI,	                    	"LEI_GRAPHICAL_REPORT",				"GENERIC_REPORT_TEXT"));
		reportRequestMap.put(REPORT_CODE_RAV_SF_GRAPHICAL,			new ReportRequestCodes(REPORT_CODE_RAV_SF_GRAPHICAL,        	"CHQ",                 	 		"RAV_SF_GRAPHICAL_REPORT",			"GENERIC_REPORT_TEXT"));
		reportRequestMap.put(REPORT_CODE_RAV_B_GRAPHICAL,			new ReportRequestCodes(REPORT_CODE_RAV_B_GRAPHICAL,				"RAVENS_B",						"RAV_B_GRAPHICAL_REPORT",			"GENERIC_REPORT_TEXT"));
		reportRequestMap.put(REPORT_CODE_WGE_GRAPHICAL,				new ReportRequestCodes(REPORT_CODE_WGE_GRAPHICAL,				"WG_E",							"WGE_GRAPHICAL_REPORT",				"GENERIC_REPORT_TEXT"));
		
		reportRequestMap.put(REPORT_CODE_CHQ_DETAIL,				new ReportRequestCodes(REPORT_CODE_CHQ_DETAIL,					"CHQ",					 		"CHQ_DETAIL_REPORT",				"GENERIC_REPORT_TEXT"));
		// Replaced by REPORT_CODE_WGE_GRAPHICAL
		//reportRequestMap.put(REPORT_CODE_WGE_DETAIL,				new ReportRequestCodes(REPORT_CODE_WGE_DETAIL,					"WG_E",					 		"WGA_GRAPHICAL_REPORT",				null));
		
		// Eval Guide
		reportRequestMap.put(REPORT_CODE_EVAL_GUIDE,			new ReportRequestCodes(REPORT_CODE_EVAL_GUIDE,				"ABYD_EVAL_GUIDE",						"ABYD_EVAL_GUIDE",			"GENERIC_REPORT_TEXT"));
		
		// Integration Grid
		reportRequestMap.put(REPORT_CODE_INT_GRID_REVIEW,			new ReportRequestCodes(REPORT_CODE_INT_GRID_REVIEW,	"ABYD_INT_GRID_REVIEW",						"ABYD_INT_GRID_REVIEW",			"GENERIC_REPORT_TEXT"));
		reportRequestMap.put(REPORT_CODE_INT_GRID_SNAPSHOT,			new ReportRequestCodes(REPORT_CODE_INT_GRID_SNAPSHOT,	"ABYD_INT_GRID_SNAPSHOT",						"ABYD_INT_GRID_SNAPSHOT",			"GENERIC_REPORT_TEXT"));
		
		//Setups for Target reports
		reportRequestMap.put(REPORT_CODE_ABYD_TARGET_PPT,			new ReportRequestCodes(REPORT_CODE_ABYD_TARGET_PPT,				"ABYD_REPORT",					"TARGET_DASHBOARD_REPORT",			"TARGET_DSHBD"));
		reportRequestMap.put(REPORT_CODE_ABYD_TARGET_ORG,			new ReportRequestCodes(REPORT_CODE_ABYD_TARGET_ORG,				"ABYD_REPORT",					"TARGET_DASHBOARD_REPORT",			"TARGET_DSHBD"));
		
		// Setups for extracts
		reportRequestMap.put(REPORT_CODE_ABYD_EXTRACT,				new ReportRequestCodes(REPORT_CODE_ABYD_EXTRACT,				REPORT_CODE_ABYD_EXTRACT,		 "NO_TEMPLATE",						null,					REPORT_CODE_ABYD_EXTRACT));
		reportRequestMap.put(REPORT_CODE_ABYD_RAW_EXTRACT,			new ReportRequestCodes(REPORT_CODE_ABYD_RAW_EXTRACT,			REPORT_CODE_ABYD_RAW_EXTRACT,	 "NO_TEMPLATE",						null,					REPORT_CODE_ABYD_RAW_EXTRACT));
		reportRequestMap.put(REPORT_CODE_ASSESSMENT_EXTRACT, 		new ReportRequestCodes(REPORT_CODE_ASSESSMENT_EXTRACT,			REPORT_CODE_ASSESSMENT_EXTRACT,	 "NO_TEMPLATE",						null,					REPORT_CODE_ASSESSMENT_EXTRACT));
		reportRequestMap.put(REPORT_CODE_TLT_EXTRACT_RAW, 			new ReportRequestCodes(REPORT_CODE_TLT_EXTRACT_RAW,				REPORT_CODE_TLT_EXTRACT_RAW,	 "NO_TEMPLATE",						null,					REPORT_CODE_TLT_EXTRACT_RAW));
		reportRequestMap.put(REPORT_CODE_KFALP_RAW_EXTRACT,         new ReportRequestCodes(REPORT_CODE_KFALP_RAW_EXTRACT,			REPORT_CODE_KFALP_RAW_EXTRACT,	 "NO_TEMPLATE",						null,					REPORT_CODE_KFALP_RAW_EXTRACT));
		// Analytics Extract uses the same script and extract as "standard" extract for now (same data extracted)
		reportRequestMap.put(REPORT_CODE_ABYD_ANALYTICS_EXTRACT,	new ReportRequestCodes(REPORT_CODE_ABYD_ANALYTICS_EXTRACT,		REPORT_CODE_ABYD_EXTRACT,		 "NO_TEMPLATE",						null,					REPORT_CODE_ABYD_EXTRACT));
		reportRequestMap.put(REPORT_CODE_ALR_EXTRACT,				new ReportRequestCodes(REPORT_CODE_ALR_EXTRACT,					REPORT_CODE_ALR_EXTRACT,		 "NO_TEMPLATE",						null,					REPORT_CODE_ALR_EXTRACT));
		reportRequestMap.put(REPORT_CODE_ALR_TRAITS,				new ReportRequestCodes(REPORT_CODE_ALR_TRAITS,					REPORT_CODE_ALR_TRAITS,			 "NO_TEMPLATE",						null,					REPORT_CODE_ALR_TRAITS));
		reportRequestMap.put(REPORT_CODE_ALR_DRIVERS,				new ReportRequestCodes(REPORT_CODE_ALR_DRIVERS,					REPORT_CODE_ALR_DRIVERS,		 "NO_TEMPLATE",						null,					REPORT_CODE_ALR_DRIVERS));
		reportRequestMap.put(REPORT_CODE_KF4D_TRAITS,				new ReportRequestCodes(REPORT_CODE_KF4D_TRAITS,					REPORT_CODE_KF4D_TRAITS,		 "NO_TEMPLATE",						null,					REPORT_CODE_KF4D_TRAITS));
		reportRequestMap.put(REPORT_CODE_KF4D_DRIVERS,				new ReportRequestCodes(REPORT_CODE_KF4D_DRIVERS,				REPORT_CODE_KF4D_DRIVERS,		 "NO_TEMPLATE",						null,					REPORT_CODE_KF4D_DRIVERS));
		reportRequestMap.put(REPORT_CODE_KF4D_RISK,					new ReportRequestCodes(REPORT_CODE_KF4D_RISK,					REPORT_CODE_KF4D_RISK,			 "NO_TEMPLATE",						null,					REPORT_CODE_KF4D_RISK));
		
		
		// Setups for scoring - We might be able to use one of the reporting
		// setups above, but they are defined separately for clarity.
		reportRequestMap.put(RC_GPI, 								new ReportRequestCodes(RC_GPI,									RC_GPI,							 "GPI_GRAPHICAL_REPORT",			null));
		reportRequestMap.put(RC_LEI, 								new ReportRequestCodes(RC_LEI,									RC_LEI,							 "NO_TEMPLATE",						null));
		reportRequestMap.put(RC_RAVENS_SF, 							new ReportRequestCodes(RC_RAVENS_SF,							RC_RAVENS_SF,					 "NO_TEMPLATE",						null));
		reportRequestMap.put(RC_RAVENS_B, 							new ReportRequestCodes(RC_RAVENS_B,								RC_RAVENS_B,					 "RAV_B_GRAPHICAL_REPORT",			null));
		reportRequestMap.put(RC_CAREER_SURVEY, 						new ReportRequestCodes(RC_CAREER_SURVEY,						RC_CAREER_SURVEY,				 "NO_TEMPLATE",						null));
		reportRequestMap.put(RC_FIN_EX, 							new ReportRequestCodes("FinEx",									RC_FIN_EX,						 "NO_TEMPLATE",						null));
		reportRequestMap.put(RC_WG_E, 								new ReportRequestCodes(RC_WG_E,									RC_WG_E,						 "WGE_GRAPHICAL_REPORT",			null));
		reportRequestMap.put(RC_CHQ, 								new ReportRequestCodes(RC_CHQ,									RC_CHQ,							 "NO_TEMPLATE",						null));
		
	};


	/*
	*  Career Survey scales
	*/
	public static String CAREER_GOALS			=	"CAREER_GOALS"; 			//	Career Goals
	public static String CAREER_DRIVERS			=	"CAREER_DRIVERS";			//	Career Drivers
	public static String LEARNING_ORIENT		=	"LEARNING_ORIENT";			//	Learning Orientation
	public static String EXPERIENCE_ORIENT		=	"EXPERIENCE_ORIENT";		//	Experience orientation
	
	/*
	*  GPI Scales 
	*/
	// The commented out definitions are V3 constants... V2 constants (SPSS codes) are found below
	// I didn't see the scale names marked with *** in in any of the XML, so not completely sure they're right.
//	public static String GPI_ADAPTABILITY		=	"F_Adaptability";				//	Adaptability
//	public static String GPI_ATTN_DETAIL		=	"F_Attention to Detail";		//	Attention to Detail
//	public static String GPI_CONSIDERATION		=	"F_Consideration";				//	Consideration
//	public static String GPI_DESIRE_ACHIEVE		=	"F_Desire for Achievement";		//	Desire for Achievement
//	public static String GPI_DESIRE_ADVANCE		=	"F_Desire for Advancement";		//	Desire for Advancement
//	public static String GPI_DUTIFUL			=	"F_Dutifulness";				//	Dutifulness ***
//	public static String GPI_EGO				=	"F_Ego-centered";				//	Ego-centered
//	public static String GPI_EMPATHY			=	"F_Empathy";					//	Empathy
//	public static String GPI_ENERGY_LEV			=	"F_Energy Level";				//	Energy Level
//	public static String GPI_INDEPENDENCE		=	"F_Independence";				//	Independence
//	public static String GPI_INFLUENCE			=	"F_Influence";					//	Influence
//	public static String GPI_INNOVATION			=	"F_Innovation/Creativity";		//	Innovation/Creativity
//	public static String GPI_INTERDEPENDENCE	=	"F_Interdependence";			//	Interdependence ***
//	public static String GPI_MANIPULATION		=	"F_Manipulation";				//	Manipulation
//	public static String GPI_MICRO_MANAGE		=	"F_Micro-managing";				//	Micro-managing
//	public static String GPI_NEG_AFFECT			=	"F_Negative Affectivity";		//	Negative Affectivity
//	public static String GPI_OPTIMISM			=	"F_Optimism";					//	Optimism
//	public static String GPI_PASS_AGGRESS		=	"F_Passive-aggressive";			//	Passive-aggressive
//	public static String GPI_RISK_TAKING		=	"F_Risk-taking";				//	Risk-taking
//	public static String GPI_SELF_AWARE			=	"F_Self-awareness/Insight";		//	Self-awareness/Insight
//	public static String GPI_SOCIABILITY		=	"F_Sociability";				//	Sociability
//	public static String GPI_SOCIAL_ASTUTE		=	"F_Social Astuteness";			//	Social Astuteness
//	public static String GPI_STRESS_TOL			=	"F_Stress Tolerance";			//	Stress Tolerance
//	public static String GPI_TAKE_CHARGE		=	"F_Taking Charge";				//	Taking Charge
//	public static String GPI_THOUGHT_FOCUS		=	"F_Thought Focus";				//	Thought Focus
//	public static String GPI_TRUST				=	"F_Trust";						//	Trust
//	public static String GPI_VISION				=	"F_Vision";						//	Vision

	// The following are GPI scales not covered in the above V3 list;
	//	note that the labels were made up to make the V2 list complete:
	//		GPI_COMPETITIVE		Competitive
	//		GPI_EM_CONTROL		Emotional Control
	//		GPI_IMPRESSING		Impressing
	//		GPI_INITIATIVE		Initiative
	//		GPI_INTIMIDATING	Intimidating
	//		GPI_OPENNESS		Openness
	//		GPI_RESPONSIBILITY	Responsibility
	//		GPI_SELF_CONF		Self-confidence
	//		GPI_THOUGHT_AGIL	Thought Agility

	//					 Key						Value (SPSS)
	public static String GPI_ADAPTABILITY		=	"GPI_ADPT";		//	Adaptability
	public static String GPI_ATTN_DETAIL		=	"GPI_AD";		//	Attention to Detail
	public static String GPI_COMPETITIVE		=	"GPI_COMP";		//	Competitive
	public static String GPI_CONSIDERATION		=	"GPI_CONS";		//	Consideration
	public static String GPI_DESIRE_ACHIEVE		=	"GPI_DACH";		//	Desire for Achievement
	public static String GPI_DESIRE_ADVANCE		=	"GPI_DADV";		//	Desire for Advancement
	public static String GPI_DUTIFUL			=	"GPI_DUT";		//	Dutifulness	
	public static String GPI_EGO				=	"GPI_EGO";		//	Ego-centered
	public static String GPI_EM_CONTROL			=	"GPI_EC";		//	Emotional Control
	public static String GPI_EMPATHY			=	"GPI_EMP";		//	Empathy
	public static String GPI_ENERGY_LEV			=	"GPI_EL";		//	Energy Level
	public static String GPI_IMPRESSING			=	"GPI_IMPR";		//	Impressing
	public static String GPI_INDEPENDENCE		=	"GPI_IND";		//	Independence
	public static String GPI_INFLUENCE			=	"GPI_INFL";		//	Influence
	public static String GPI_INITIATIVE			=	"GPI_INIT";		//	Initiative
	public static String GPI_INNOVATION			=	"GPI_INOV";		//	Innovation/Creativity
	public static String GPI_INTERDEPENDENCE	=	"GPI_INTD";		//	Interdependence
	public static String GPI_INTIMIDATING		=	"GPI_INTI";		//	Intimidating
	public static String GPI_MANIPULATION		=	"GPI_MAN";		//	Manipulation
	public static String GPI_MICRO_MANAGE		=	"GPI_MIC";		//	Micro-managing
	public static String GPI_NEG_AFFECT			=	"GPI_NA";		//	Negative Affectivity
	public static String GPI_OPENNESS			=	"GPI_OPEN";		//	Openness
	public static String GPI_OPTIMISM			=	"GPI_OPT";		//	Optimism
	public static String GPI_PASS_AGGRESS		=	"GPI_PA";		//	Passive-aggressive
	public static String GPI_RESPONSIBILITY		=	"GPI_RESP";		//	Responsibility
	public static String GPI_RISK_TAKING		=	"GPI_RISK";		//	Risk-taking
	public static String GPI_SELF_AWARE			=	"GPI_SASI";		//	Self-awareness/Self-insight
	public static String GPI_SELF_CONF			=	"GPI_SC";		//	Self-confidence
	public static String GPI_SOCIABILITY		=	"GPI_SO";		//	Sociability
	public static String GPI_SOCIAL_ASTUTE		=	"GPI_AST";		//	Social Astuteness
	public static String GPI_STRESS_TOL			=	"GPI_ST";		//	Stress Tolerance
	public static String GPI_TAKE_CHARGE		=	"GPI_TC";		//	Taking Charge
	public static String GPI_THOUGHT_AGIL		=	"GPI_TA";		//	Thought Agility
	public static String GPI_THOUGHT_FOCUS		=	"GPI_TF";		//	Thought Focus
	public static String GPI_TRUST				=	"GPI_TR";		//	Trust
	public static String GPI_VISION				=	"GPI_VIS";		//	Vision
	public static String GPI_WORK_FOCUS			=	"GPI_WF";		//	Work Focus
	
	// 	LEI Scales
	// The commented out definitions are V3 constants... V2 constants (SPSS codes) are found below
//	public static String LEI_PROJ_MGMT_IMPL		=	"F_Scale 02 - Project Management and Implementation";	//  Scale 02 - Project Management and Implementation -
//	public static String LEI_BUS_DEV_MARKTNG  	= 	"F_Scale 03 - Business Development and Marketing,FACTOR";	//  Scale 03 - Business Development and Marketing Scal
//	public static String LEI_BUS_GROWTH      	= 	"F_Scale 04 - Business Growth Scale - BGROW"; 			//  Scale 04 - Business Growth Scale - BGROW
//	public static String LEI_OPERATIONS		 	=  	"F_Scale 08 - Operations Scale - OPEXP"; 				//	Scale 08 - Operations Scale - OPEXP
//	public static String LEI_INHERITED_PROB		=	"F_Scale 11 - Inherited Problems and Challenges Scale"; //	Scale 11 - Inherited Problems and Challenges Scale
//	public static String LEI_DIFF_STAFFING		=	"F_Scale 15 - Difficult Staffing Situations Scale - S";	//	Scale 15 - Difficult Staffing Situations Scale - S
//	public static String LEI_HIGH_RISK			=	"F_Scale 16 - High Risk Situations Scale - HIRISK";		//	Scale 16 - High Risk Situations Scale - HIRISK
//	public static String LEI_HI_CRIT_VISIBLE	=	"F_Scale 19 - Highly Critical/Visible Assignments or";	//	Scale 19 - Highly Critical/Visible Assignments or
//	public static String LEI_SELF_DEV			=	"F_Scale 20 - Self-Development Scale - SELFDEV";		//	Scale 20 - Self-Development Scale - SELFDEV
//	public static String LEI_EXTRACURRICULAR 	=	"F_Scale 23 - Extracurricular Activities Scale - EXTR"; //	Scale 23 - Extracurricular Activities Scale - EXTR

	// LEI super-scales and Overall are not included here
	public static String LEI_STRATEGY_DEV		=	"LEI3_SGY";	//  Scale 01 - Strategy Development
	public static String LEI_PROJ_MGMT_IMPL		=	"LEI3_PGT";	//  Scale 02 - Project Management and Implementation
	public static String LEI_BUS_DEV_MARKTNG  	= 	"LEI3_BDV";	//  Scale 03 - Business Development and Marketing
	public static String LEI_BUS_GROWTH      	= 	"LEI3_BGR"; //  Scale 04 - Business Growth
	public static String LEI_PRODUCT_DEV      	= 	"LEI3_PDT"; //  Scale 05 - Product Development
	public static String LEI_BUS_START_UP      	= 	"LEI3_STA"; //  Scale 06 - Start-up Business
	public static String LEI_FIN_MGMT	      	= 	"LEI3_FM";  //  Scale 07 - Financial Management
	public static String LEI_OPERATIONS		 	=  	"LEI3_OPX"; //	Scale 08 - Operations
	public static String LEI_SPT_FUNCTIONS	 	=  	"LEI3_FEX"; //	Scale 09 - Support Functions
	public static String LEI_EXT_RELATIONS	 	=  	"LEI3_RPR"; //	Scale 10 - External Relations
	public static String LEI_INHERITED_PROB		=	"LEI3_PRB";	//	Scale 11 - Inherited Problems and Challenges
	public static String LEI_INTP_CHAL_SIT		=	"LEI3_PPL";	//	Scale 12 - Interpersonally Challenging Situations
	public static String LEI_DOWN_FAIL			=	"LEI3_FAL";	//	Scale 13 - Downturn and/or Failures
	public static String LEI_DIFF_FINANCIAL		=	"LEI3_FIN";	//	Scale 14 - Difficult Financial Situations
	public static String LEI_DIFF_STAFFING		=	"LEI3_STF";	//	Scale 15 - Difficult Staffing Situations
	public static String LEI_HIGH_RISK			=	"LEI3_HR";	//	Scale 16 - High Risk Situations
	public static String LEI_CRIT_NEG			=	"LEI3_NEG";	//	Scale 17 - Critical Negotiations
	public static String LEI_CRISIS_MGMT		=	"LEI3_CMT";	//	Scale 18 - Crisis Management
	public static String LEI_HI_CRIT_VISIBLE	=	"LEI3_VAS";	//	Scale 19 - Highly Critical/Visible Assignments
	public static String LEI_SELF_DEV			=	"LEI3_SDV";	//	Scale 20 - Self-Development Scale
	public static String LEI_DEV_OTHERS			=	"LEI3_DEV";	//	Scale 21 - Development of Others
	public static String LEI_CROSS_CULT			=	"LEI3_INN";	//	Scale 22 - International/Cross-Cultural
	public static String LEI_EXTRACURRICULAR 	=	"LEI3_EXT";	//	Scale 23 - Extracurricular Activities


	/*
	*  Raven's Short Form Scales 
	*  NOTE:  I haven't seen any XML w/ scored results, to grab the construct string....
	*/
	public static String RAVENS_SHORT	=	"RAVENSSF";	//	Harcourt Raven APM Short Form


	/*
	*  Raven's B Scales 
	*  NOTE:  I haven't seen any XML w/ scored results, to grab the construct string....
	*/
	public static String RAVENS_B	=	"RAVENSB";	//	Harcourt Raven APM Form B


	/*
	*  Career Survey
	*/
	public static String CS_CAREER_GOALS	=	"CAREER_GOALS";
	public static String CS_CAREER_DRIVERS	=	"CAREER_DRIVERS";
	public static String CS_LEARN_ORIENT	=	"LEARNING_ORIENT";
	public static String CS_EXP_ORIENT		=	"EXPERIENCE_ORIENT";
	

	/*
	 * TLT Individual Report 
	 * 
	 * Displayed component scores
	 * 
	 */
	// (Interest)
	public static String CS_LDRAS		=	"CAREER_GOALS";			//  Leadership Aspiration
	public static String CS_CRDRV		=	"CAREER_DRIVERS";		//  Career Drivers
	public static String CS_LRNOR		=	"LEARNING_ORIENT";		//  Learning Orientation
	public static String CS_EXPOR		=	"EXPERIENCE_ORIENT";	//  Experience Orientation
	
	// (Experience)
	public static String LEI_BUSOP		=	"LEI_OPERATIONS";		//  Business Operations
	public static String LEI_HTC		=	"LEI_SIT_TOUGH_CHAL";	//  Handling Tough Challenges
	public static String LEI_HVIS		=	"LEI_HIGH_VIS";			//  High Visibility
	public static String LEI_GTBUS		=	"LEI_BD_GROW_BUS";		//  Growing the Business
	public static String LEI_PDEV		=	"LEI_PERSONAL";			//  Personal Development
	
	// (Foundations)
	public static String RAV_PSOLV		=	"ZGEST";				//  Problem Solving
	
	public static String GPI_INTEN		=	"INTELLECTUAL_ENGMT";	//  Intellectual Engagement
	public static String GPI_ATD		=	"DETAIL_ORIENT";		//  Attention to Detail
	public static String GPI_IMIN		=	"FACIL_LEADERSHP";		//  Impact / Influence
	public static String GPI_INEN		=	"INTER_PERSONAL_ENGMT";	//  Interpersonal Engagement
	public static String GPI_ACHD		=	"DRIVE";				//  Achievement Drive
	public static String GPI_ADVD		=	"INDIV_ORIENT";			//  Advancement Drive
	public static String GPI_COOR		=	"COLLECTIVE_ORIENT";	//  Collective Orientation
	public static String GPI_FLAD		=	"POSITIVITY";			//  Flexibility / Adaptability	
	
	// (Derailers)
	public static String DR_DERAIL		=	"DERAIL_FACTORS";		//  Derailment Risk stanine
	public static String DR_RISK_TEXT	=	"DERAIL_RISK_TEXT";		//  Derailment Risk name
	public static String DR_FACTOR_TEXT_COUNT	=	"DERAIL_FACTOR_TEXT_COUNT";	//	Derailment Factor count
	public static String DR_FACTOR_TEXT	=	"DERAIL_FACTOR_TEXT_";	//	Derailment Factor starter
	
	/*
	*  Derailment Risks
	*/
	//
	/*
	 * update pdi_norm set labelKey = 'GPI_EGOCENTRIC' where labelKey = 'GPI_EGO'
	 * 
	 */
	public static String DR_EGOCE		=	"GPI_EGOCENTRIC";	//	Risk Egocentric
	public static String DR_MANIP		=	"GPI_MANIPULATION";	//	Risk Manipulation
	public static String DR_MICRO		=	"GPI_MICRO_MANAGE";	//	Risk MicroManaging
	public static String DR_PASAG		=	"GPI_PASS_AGGRESS";	//	Risk Passive Aggressive

	/*
	*  TLT Group Score NormSets
	*/	
	public static String TPOT2_GROUP_COMPONENT_Z_SCORE		=	"TPOT2_Group_component_z_score";
	public static String TPOT2_GROUP_LOGODDS				=	"TPOT2_Group_Logodds"; 
	public static String TPOT2_GROUP_PERFORMANCE			=	"TPOT2_Group_Performance";
	public static String TPOT2_GROUP_LOGODDS_FROM_ZSCOR		=	"TPOT2_Group_Logodds_From_Zscor";
	public static String TPOT2_GROUP_PERFORMANCE_FROM_Z 	=	"TPOT2_Group_Performance_From_Z";
	public static String LOGODDS_CONSTANT					= 	"LOGODDS_CONSTANT";
	//Overall TPOT scores
	public static String OV_OVERALL 						= 	"OV_OVERALL";
	public static String POTENTIAL_LOGODD					=   "_POTENTIAL_LOGODD";
	public static String POTENTIAL_STANINE                  =   "_POTENTIAL_STANINE";
	public static String PERF_EST_SCORE                     =   "_PERF_EST_SCORE";
	public static String PERF_EST_STANINE                   =   "_PERF_EST_STANINE";

	
	/*   this is an array list of 
	 * 	 LEI scales used in the 
	 *   Assessment Extract
	 */
	public static String [] lei_scales_for_assessment_xtract = new String [23];
	static{
		lei_scales_for_assessment_xtract[0]="LEI3_SGY_SP_RATING";	// Scale 01 - Strategy Development
		lei_scales_for_assessment_xtract[1]="LEI3_PGT_SP_RATING";  	// Scale 02 - Project Management and Implementation				
		lei_scales_for_assessment_xtract[2]="LEI3_BDV_SP_RATING";  	// Scale 03 - Business Development and Marketing				
		lei_scales_for_assessment_xtract[3]="LEI3_BGR_SP_RATING";  	// Scale 04 - Business Growth	
		lei_scales_for_assessment_xtract[4]="LEI3_PDT_SP_RATING"; 	// Scale 05 - Product Development
		lei_scales_for_assessment_xtract[5]="LEI3_STA_SP_RATING"; 	// Scale 06 - Start-up Business
		lei_scales_for_assessment_xtract[6]="LEI3_FM_SP_RATING";  	// Scale 07 - Financial Management		
		lei_scales_for_assessment_xtract[7]="LEI3_OPX_SP_RATING";  	// Scale 08 - Operations
		lei_scales_for_assessment_xtract[8]="LEI3_FEX_SP_RATING"; 	// Scale 09 - Support Functions
		lei_scales_for_assessment_xtract[9]="LEI3_RPR_SP_RATING"; 	// Scale 10 - External Relations		
		lei_scales_for_assessment_xtract[10]="LEI3_PRB_SP_RATING";  // Scale 11 - Inherited Problems and Challenges	
		lei_scales_for_assessment_xtract[11]="LEI3_PPL_SP_RATING";	// Scale 12 - Interpersonally Challenging Situations
		lei_scales_for_assessment_xtract[12]="LEI3_FAL_SP_RATING";	// Scale 13 - Downturn and/or Failures
		lei_scales_for_assessment_xtract[13]="LEI3_FIN_SP_RATING";	// Scale 14 - Difficult Financial Situations		
		lei_scales_for_assessment_xtract[14]="LEI3_STF_SP_RATING";  // Scale 15 - Difficult Staffing Situations				
		lei_scales_for_assessment_xtract[15]="LEI3_HR_SP_RATING";  	// Scale 16 - High Risk Situations
		lei_scales_for_assessment_xtract[16]="LEI3_NEG_SP_RATING";	// Scale 17 - Critical Negotiations
		lei_scales_for_assessment_xtract[17]="LEI3_CMT_SP_RATING";	// Scale 18 - Crisis Management		
		lei_scales_for_assessment_xtract[18]="LEI3_VAS_SP_RATING";  // Scale 19 - Highly Critical/Visible Assignments				
		lei_scales_for_assessment_xtract[19]="LEI3_SDV_SP_RATING";  // Scale 20 - Self-Development	
		lei_scales_for_assessment_xtract[20]="LEI3_DEV_SP_RATING";	// Scale 21 - Development of Others
		lei_scales_for_assessment_xtract[21]="LEI3_INN_SP_RATING";	// Scale 22 - International/Cross-Cultural		
		lei_scales_for_assessment_xtract[22]="LEI3_EXT_SP_RATING"; 	// Scale 23 - Extracurricular Activities				
	};
	
	public static String [] lei_scales_text_for_assessment_xtract = new String [23];
	static{
		lei_scales_text_for_assessment_xtract[0]="Strategy Development";	// Scale 01 - Strategy Development
		lei_scales_text_for_assessment_xtract[1]="Project Management and Implementation";  	// Scale 02 - Project Management and Implementation				
		lei_scales_text_for_assessment_xtract[2]="Business Development and Marketing";  	// Scale 03 - Business Development and Marketing				
		lei_scales_text_for_assessment_xtract[3]="Business Growth";  	// Scale 04 - Business Growth	
		lei_scales_text_for_assessment_xtract[4]="Product Development"; 	// Scale 05 - Product Development
		lei_scales_text_for_assessment_xtract[5]="Start-up Business"; 	// Scale 06 - Start-up Business
		lei_scales_text_for_assessment_xtract[6]="Financial Management";  	// Scale 07 - Financial Management		
		lei_scales_text_for_assessment_xtract[7]="Operations";  	// Scale 08 - Operations
		lei_scales_text_for_assessment_xtract[8]="Support Functions"; 	// Scale 09 - Support Functions
		lei_scales_text_for_assessment_xtract[9]="External Relations"; 	// Scale 10 - External Relations		
		lei_scales_text_for_assessment_xtract[10]="Inherited Problems and Challenges";  // Scale 11 - Inherited Problems and Challenges	
		lei_scales_text_for_assessment_xtract[11]="Interpersonally Challenging Situations";	// Scale 12 - Interpersonally Challenging Situations
		lei_scales_text_for_assessment_xtract[12]="Downturn and/or Failures";	// Scale 13 - Downturn and/or Failures
		lei_scales_text_for_assessment_xtract[13]="Difficult Financial Situations";	// Scale 14 - Difficult Financial Situations		
		lei_scales_text_for_assessment_xtract[14]="Difficult Staffing Situations";  // Scale 15 - Difficult Staffing Situations				
		lei_scales_text_for_assessment_xtract[15]="High Risk Situations";  	// Scale 16 - High Risk Situations
		lei_scales_text_for_assessment_xtract[16]="Critical Negotiations";	// Scale 17 - Critical Negotiations
		lei_scales_text_for_assessment_xtract[17]="Crisis Management";	// Scale 18 - Crisis Management		
		lei_scales_text_for_assessment_xtract[18]="Highly Critical/Visible Assignments";  // Scale 19 - Highly Critical/Visible Assignments				
		lei_scales_text_for_assessment_xtract[19]="Self-Development";  // Scale 20 - Self-Development	
		lei_scales_text_for_assessment_xtract[20]="Development of Others";	// Scale 21 - Development of Others
		lei_scales_text_for_assessment_xtract[21]="International/Cross-Cultural";	// Scale 22 - International/Cross-Cultural		
		lei_scales_text_for_assessment_xtract[22]="Extracurricular Activities"; 	// Scale 23 - Extracurricular Activities				
	};
	
	/*   this is an array list of 
	 * 	 GPI scales used in the 
	 *   Assessment Extract
	 */
	public static String []  gpi_scales_for_assessment_xtract = new String [37];
	static
	{
		gpi_scales_for_assessment_xtract[0]="GPI_TA_SP_RATING";			// Thought Agility		
		gpi_scales_for_assessment_xtract[1]="GPI_INOV_SP_RATING";		// Innovation/Creativity		
		gpi_scales_for_assessment_xtract[2]="GPI_TF_SP_RATING";			// Thought Focus		
		gpi_scales_for_assessment_xtract[3]="GPI_VIS_SP_RATING";	  	// Vision		
		gpi_scales_for_assessment_xtract[4]="GPI_AD_SP_RATING";	  		// Attention to Detail		
		gpi_scales_for_assessment_xtract[5]="GPI_WF_SP_RATING";	  		// Work Focus		
		gpi_scales_for_assessment_xtract[6]="GPI_TC_SP_RATING";	 		// Taking Charge		
		gpi_scales_for_assessment_xtract[7]="GPI_INFL_SP_RATING";	  	// Influence		
		gpi_scales_for_assessment_xtract[8]="GPI_EGO_SP_RATING";	  	// Ego-centered		
		gpi_scales_for_assessment_xtract[9]="GPI_MAN_SP_RATING";	  	// Manipulation		
		gpi_scales_for_assessment_xtract[10]="GPI_MIC_SP_RATING";	  	// Micro-managing		
		gpi_scales_for_assessment_xtract[11]="GPI_INTI_SP_RATING";	  	// Intimidating		
		gpi_scales_for_assessment_xtract[12]="GPI_PA_SP_RATING";	  	// Passive-aggressive		
		gpi_scales_for_assessment_xtract[13]="GPI_SO_SP_RATING";	  	// Sociability		
		gpi_scales_for_assessment_xtract[14]="GPI_CONS_SP_RATING";	  	// Consideration		
		gpi_scales_for_assessment_xtract[15]="GPI_EMP_SP_RATING";	  	// Empathy		
		gpi_scales_for_assessment_xtract[16]="GPI_TR_SP_RATING";	  	// Trust		
		gpi_scales_for_assessment_xtract[17]="GPI_AST_SP_RATING";	  	// Social Astuteness		
		gpi_scales_for_assessment_xtract[18]="GPI_EL_SP_RATING";	  	// Energy Level		
		gpi_scales_for_assessment_xtract[19]="GPI_INIT_SP_RATING";	  	// Initiative		
		gpi_scales_for_assessment_xtract[20]="GPI_DACH_SP_RATING";	  	// Desire for Achievement		
		gpi_scales_for_assessment_xtract[21]="GPI_ADPT_SP_RATING";	  	// Adaptability		
		gpi_scales_for_assessment_xtract[22]="GPI_OPEN_SP_RATING";	 	// Openness		
		gpi_scales_for_assessment_xtract[23]="GPI_NA_SP_RATING";	  	// Negative Affectivity		
		gpi_scales_for_assessment_xtract[24]="GPI_OPT_SP_RATING";	  	// Optimism		
		gpi_scales_for_assessment_xtract[25]="GPI_EC_SP_RATING";	  	// Emotional Control		
		gpi_scales_for_assessment_xtract[26]="GPI_ST_SP_RATING";	  	// Stress Tolerance		
		gpi_scales_for_assessment_xtract[27]="GPI_SC_SP_RATING";	  	// Self-confidence		
		gpi_scales_for_assessment_xtract[28]="GPI_IMPR_SP_RATING";	  	// Impressing		
		gpi_scales_for_assessment_xtract[29]="GPI_SASI_SP_RATING";	  	// Self-awareness/Insight		
		gpi_scales_for_assessment_xtract[30]="GPI_IND_SP_RATING";	  	// Independence		
		gpi_scales_for_assessment_xtract[31]="GPI_COMP_SP_RATING";	  	// Competitive		
		gpi_scales_for_assessment_xtract[32]="GPI_RISK_SP_RATING";	  	// Risk-taking		
		gpi_scales_for_assessment_xtract[33]="GPI_DADV_SP_RATING";	  	// Desire for Advancement		
		gpi_scales_for_assessment_xtract[34]="GPI_INTD_SP_RATING";	  	// Interdependence		
		gpi_scales_for_assessment_xtract[35]="GPI_DUT_SP_RATING";	  	// Dutifulness		
		gpi_scales_for_assessment_xtract[36]="GPI_RESP_SP_RATING";	  	// Responsibility	

	};

	
	public static String []  gpi_scales_text_for_assessment_xtract = new String [37];
	static
	{
		gpi_scales_text_for_assessment_xtract[0]= "Thought Agility";		// Thought Agility		
		gpi_scales_text_for_assessment_xtract[1]= "Innovation/Creativity";	// Innovation/Creativity		
		gpi_scales_text_for_assessment_xtract[2]= "Thought Focus";		// Thought Focus		
		gpi_scales_text_for_assessment_xtract[3]= "Vision";	  		// Vision		
		gpi_scales_text_for_assessment_xtract[4]= "Attention to Detail";	// Attention to Detail		
		gpi_scales_text_for_assessment_xtract[5]= "Work Focus";		// Work Focus		
		gpi_scales_text_for_assessment_xtract[6]= "Taking Charge";	 	// Taking Charge		
		gpi_scales_text_for_assessment_xtract[7]= "Influence";	  	// Influence		
		gpi_scales_text_for_assessment_xtract[8]= "Ego-centered";	  	// Ego-centered		
		gpi_scales_text_for_assessment_xtract[9]= "Manipulation";	  	// Manipulation		
		gpi_scales_text_for_assessment_xtract[10]="Micro-managing";	  	// Micro-managing		
		gpi_scales_text_for_assessment_xtract[11]="Intimidating";	  	// Intimidating		
		gpi_scales_text_for_assessment_xtract[12]="Passive-aggressive";	 // Passive-aggressive		
		gpi_scales_text_for_assessment_xtract[13]="Sociability";	  	// Sociability		
		gpi_scales_text_for_assessment_xtract[14]="Consideration";	  	// Consideration		
		gpi_scales_text_for_assessment_xtract[15]="Empathy";	  		// Empathy		
		gpi_scales_text_for_assessment_xtract[16]="Trust";	  		// Trust		
		gpi_scales_text_for_assessment_xtract[17]="Social Astuteness";	// Social Astuteness		
		gpi_scales_text_for_assessment_xtract[18]="Energy Level";	  	// Energy Level		
		gpi_scales_text_for_assessment_xtract[19]="Initiative";	  	// Initiative		
		gpi_scales_text_for_assessment_xtract[20]="Desire for Achievement";	// Desire for Achievement		
		gpi_scales_text_for_assessment_xtract[21]="Adaptability";	  	// Adaptability		
		gpi_scales_text_for_assessment_xtract[22]="Openness";	 	// Openness		
		gpi_scales_text_for_assessment_xtract[23]="Negative Affectivity";	// Negative Affectivity		
		gpi_scales_text_for_assessment_xtract[24]="Optimism";	  		// Optimism		
		gpi_scales_text_for_assessment_xtract[25]="Emotional Control";	// Emotional Control		
		gpi_scales_text_for_assessment_xtract[26]="Stress Tolerance";	// Stress Tolerance		
		gpi_scales_text_for_assessment_xtract[27]="Self-confidence";  	// Self-confidence		
		gpi_scales_text_for_assessment_xtract[28]="Impressing";	  	// Impressing		
		gpi_scales_text_for_assessment_xtract[29]="Self-awareness/Insight";	// Self-awareness/Insight		
		gpi_scales_text_for_assessment_xtract[30]="Independence";	  	// Independence		
		gpi_scales_text_for_assessment_xtract[31]="Competitive";	  	// Competitive		
		gpi_scales_text_for_assessment_xtract[32]="Risk-taking";	  	// Risk-taking		
		gpi_scales_text_for_assessment_xtract[33]="Desire for Advancement";	// Desire for Advancement		
		gpi_scales_text_for_assessment_xtract[34]="Interdependence";	  	// Interdependence		
		gpi_scales_text_for_assessment_xtract[35]="Dutifulness";	  	// Dutifulness		
		gpi_scales_text_for_assessment_xtract[36]="Responsibility";	  	// Responsibility
	}
	
	
	/*
	 * Arraylist of Predefined fields from PDA (lrm database)
	 */
	public static String [] pda_predef_tags = new String [24];
	static{
		pda_predef_tags[0]="PDA_JOB_TITLE";		// Job title - mandatory
		pda_predef_tags[1]="PDA_COUNTRY";  		// Country name - mandatory			
		pda_predef_tags[2]="PDA_EMP_ID";  		// Employee ID (Palms users ID)			
		pda_predef_tags[3]="PDA_COST_CTR";  	// Cost Center (assmt acctg)
		pda_predef_tags[4]="PDA_PO_NBR"; 	 	// PO Number (assmt acctg)
		pda_predef_tags[5]="PDA_INT_EXT_CAND";  // Internal/External candidate
		pda_predef_tags[6]="PDA_TRGT_POS";  	// Target Position			
		pda_predef_tags[7]="PDA_TIM_ORG";  		// Time Organization
		pda_predef_tags[8]="PDA_TIP";  			// Time in Position
		pda_predef_tags[9]="PDA_BC_FNAME";		// Billing contact first name
		pda_predef_tags[10]="PDA_BC_LNAME";		// Billing contact last name
		pda_predef_tags[11]="PDA_BC_INV_EMAIL";	// Billing contact Invoice Email
		pda_predef_tags[12]="PDA_BC_ADDR_1";	// Billing contact Address 1
		pda_predef_tags[13]="PDA_BC_ADDR_2";	// Billing contact Address 2
		pda_predef_tags[14]="PDA_BC_ADDR_3";	// Billing contact Address 3
		pda_predef_tags[15]="PDA_BC_CITY";		// Billing contact city
		pda_predef_tags[16]="PDA_BC_STATE";		// Billing contact state
		pda_predef_tags[17]="PDA_BC_PCODE";		// Billing contact postal code
		pda_predef_tags[18]="PDA_BC_COUNTRY";	// Billing contact country
		pda_predef_tags[19]="PDA_SUPR_NAME";	// Supervisor name
		pda_predef_tags[20]="PDA_DIR_RPTS";		// Has direct reports
		pda_predef_tags[21]="PDA_JOB_LOC";		// Job location
		pda_predef_tags[22]="PDA_HOME_PHONE";	// Home phone
		pda_predef_tags[23]="PDA_MOBL_PHONE";	// Mobile
	};
	
	// "Thinking" dimension with scales
//	DIMENSIONS[0][0] = "Thinking";
//	DIMENSIONS[0][1] = new ArrayList();
//	((ArrayList) DIMENSIONS[0][1]).add("GPI_TA");	// GPI_TA - Thought Agility
//	((ArrayList) DIMENSIONS[0][1]).add("GPI_INOV");	// GPI_INOV - Innovation/Creativity
//	((ArrayList) DIMENSIONS[0][1]).add("GPI_TF");	// GPI_TF - Thought Focus
//	((ArrayList) DIMENSIONS[0][1]).add("GPI_VIS");	// GPI_VIS - Vision
	
//	DIMENSIONS[0][0] = "Thinking";
//	ArrayList<String> al0 = new ArrayList<String>();
//	al0.add("GPI_TA");		// GPI_TA - Thought Agility
//	al0.add("GPI_INOV");	// GPI_INOV - Innovation/Creativity
//	al0.add("GPI_TF");		// GPI_TF - Thought Focus
//	al0.add("GPI_VIS");		// GPI_VIS - Vision
//	DIMENSIONS[0][1] = al0;
	// ------------- Data used in data generation -------------
	
	// List of LEI dimensions and the scales that form them
	public static Object[][] DIMENSIONS = new Object[5][2];
	static {
		// "Overall" dimension with scales
		DIMENSIONS[0][0] = "Overall";
		ArrayList<String> a01 = new ArrayList<String>();
		a01.add("LEI3_OAL");	// LEI3_OAL - Overall
		a01.add("LEI3_GMT");	// LEI3_GMT - General Management Experiences Super Scale
		a01.add("LEI3_CHA");	// LEI3_CHA - Adversity Super Scale
		a01.add("LEI3_RC");	// LEI3_RC - Risky/Critical Super Scale
		a01.add("LEI3_PCR");	// LEI3_PCR - Personal & Career Related Experiences Super Scale
		DIMENSIONS[0][1] = a01;
		
		// "General Management Experience" dimension with scales
		DIMENSIONS[1][0] = "General Management Experience";		
		ArrayList<String> a11 = new ArrayList<String>();
		a11.add("LEI3_SGY");	// LEI3_SGY - Strategy Development
		a11.add("LEI3_PGT");	// LEI3_PGT -Project Management & Implementation
		a11.add("LEI3_BDV");	// LEI3_BDV - Business Development & Marketing
		a11.add("LEI3_BGR");	// LEI3_BGR - Business Growth
		a11.add("LEI3_PDT");	// LEI3_PDT - Product Development
		a11.add("LEI3_STA");	// LEI3_STA - Start-up Business
		a11.add("LEI3_FM");		// LEI3_FM  - Financial Management
		a11.add("LEI3_OPX");	// LEI3_OPX - Operations
		a11.add("LEI3_FEX");	// LEI3_FEX - Support Functions
		a11.add("LEI3_RPR");	// LEI3_RPR - External Relations
		DIMENSIONS[1][1] = a11;

		
		// "Adversity" dimension with scales
		DIMENSIONS[2][0] = "Adversity";		
		ArrayList<String> a21 = new ArrayList<String>();
		a21.add("LEI3_PRB");	// LEI3_PRB - Inherited Problems & Challenges
		a21.add("LEI3_PPL");	// LEI3_PPL - Interpersonnally Challenging Situations
		a21.add("LEI3_FAL");	// LEI3_FAL - Downturns and/or Failures
		a21.add("LEI3_FIN");	// LEI3_FIN - Difficult Financial Situations
		a21.add("LEI3_STF");	// LEI3_STF - Difficult Staffing Situations
		DIMENSIONS[2][1] = a21;
		
		// "Risky/Critical Experiences" dimension with scales
		DIMENSIONS[3][0] = "Risky/Critical Experiences";
		ArrayList<String> a31 = new ArrayList<String>();
		a31.add("LEI3_HR");	// LEI3_HR - High Risk Situations
		a31.add("LEI3_NEG");	// LEI3_NEG - Critical Negotiations
		a31.add("LEI3_CMT");	// LEI3_CMT - Crisis Management
		a31.add("LEI3_VAS");	// LEI3_VAS - Highly Critical/Visible Assignments or Initiatives
		DIMENSIONS[3][1] = a31;
		
		// "Personal and Career Related Experiences" dimension with scales
		DIMENSIONS[4][0] = "Personal and Career Related Experiences";	
		ArrayList<String> a41 = new ArrayList<String>();
		a41.add("LEI3_SDV");	// LEI3_SDV - Self-Development
		a41.add("LEI3_DEV");	// LEI3_DEV - Development of Others
		a41.add("LEI3_INN");	// LEI3_INN - International/Cross Cultural
		a41.add("LEI3_EXT");	// LEI3_EXT - Extracurricular Activities
		DIMENSIONS[4][1] = a41;
		
		
	}	
	
	/*   this is an array list of 
	 * 	 questions for the CHQ Report
	 *   Being pulled out of Groovy and into java
	 *   and altered to use the spss values instead
	 *   of question numbers
	 */
	public static String [] CHQquestionArray = new String [206];
	static{
		CHQquestionArray[0]="PARTICIPANT_NAME";
		CHQquestionArray[1]="ORGANIZATION";
		CHQquestionArray[2]="JOB_TITLE";
		CHQquestionArray[3]="EMAIL";
		
		CHQquestionArray[4]="EDUC";  // EDUC_01 - _04
		CHQquestionArray[5]="EDUC_GRID";  // instruction txt
		CHQquestionArray[6]="UNIV";  // UNIV_1 - _10	
		CHQquestionArray[7]="UN1DTBEG";  // UN1DTBEG to UN10DTBEG
		CHQquestionArray[8]="UN1DTEND";  // UN1DTEND to UN10DTEND
		CHQquestionArray[9]="UN_GRAD1";  // to UN_GRAD10
		CHQquestionArray[10]="UNI_DEG1";  // to UNI_DEG10
		CHQquestionArray[11]="UNI_MAJ1";  // UNI_MAJ10
		
		CHQquestionArray[12]="CURLOC";  // CURLOC_222, etc.
		CHQquestionArray[13]="CURLOC_OTHER_000";
		CHQquestionArray[14]="CULT";	// CULT_222, etc.
		CHQquestionArray[15]="CULT_000_OTHER";
		
		CHQquestionArray[16]="INDUST";	// INDUST_01
		CHQquestionArray[17]="INDUST_OTHER";
		CHQquestionArray[18]="FUNC";  // FUNC_01	
		CHQquestionArray[19]="FUNC_19_OTHER";
		
		CHQquestionArray[20]="ORGSIZE";  // ORGSIZE_04		
		CHQquestionArray[21]="MGRRESP";  		
		CHQquestionArray[22]="MGDIR";
		CHQquestionArray[23]="MGTENURE";
		CHQquestionArray[24]="MGLEV";		
		CHQquestionArray[25]="SAL_CURR";
		CHQquestionArray[26]="SCURR_OTHR";
		CHQquestionArray[27]="SALARY_E";
		
		CHQquestionArray[28]="WORK_HIST_GRID";
		CHQquestionArray[29]="JOB"; 	// JOB_1 to _30 
		CHQquestionArray[30]="JBCTY";  //JBCTY_1 to _30
		CHQquestionArray[31]="POSTN";  // POSTN_1 to _30
		CHQquestionArray[32]="JBDTFR"; // JBDTFR_1 to _30
		CHQquestionArray[33]="JBDTTO";  // JBDTTO_1 to _30
		CHQquestionArray[34]="JBRESP"; // JBRESP_1 to _30
		
		CHQquestionArray[35]="CURR_CHAL";
		CHQquestionArray[36]="GR_ACCOM";
		CHQquestionArray[37]="LIK_BEST";
		CHQquestionArray[38]="LIK_LEST";
		CHQquestionArray[39]="LEAD_EXP";
		
		CHQquestionArray[40]="CRER_MVE";
		CHQquestionArray[41]="RELOCATE";
		CHQquestionArray[42]="REL_LOC";
		CHQquestionArray[43]="REL_CHNG";
		CHQquestionArray[44]="NEXT_POS";
		CHQquestionArray[45]="GOAL_3YR";
		CHQquestionArray[46]="GOAL_10YR";
		CHQquestionArray[47]="PERS_BAR";
		
		CHQquestionArray[48]="STRENGTHS";
		CHQquestionArray[49]="WEAKNESS";
		CHQquestionArray[50]="OTHR_INT";
		CHQquestionArray[51]="IMPRV_ATT";
		CHQquestionArray[52]="IDEAL_JB";
		CHQquestionArray[53]="IMP";  	// _ADVC  _FAST  _FRND... 
		CHQquestionArray[54]="OTHR_COM";
		
		CHQquestionArray[55]="ERLYR";	 // ERLYR_111
		CHQquestionArray[56]="ERLYR_000_OTHER";
		CHQquestionArray[57]="CITZ";	// CITZ_111
		CHQquestionArray[58]="CITZ_000_OTHER";
		CHQquestionArray[59]="LANG";	// LANG_111
		CHQquestionArray[60]="LANG_OTHER_99";
		CHQquestionArray[61]="YRWKFRC";
		CHQquestionArray[62]="YOB";
		CHQquestionArray[63]="SEX";
		CHQquestionArray[64]="ETHNIC";	// ETHNIC_01
		CHQquestionArray[65]="ETHNIC_99";

	}
	
	
	public static HashMap<String, String> COUNTRY_CODES = 
		new HashMap<String, String>();

	//TODO: This should all be database driven (? or xml, or something that can be dynamic)
	static
	{		
		COUNTRY_CODES.put("32", "Argentina");
		COUNTRY_CODES.put("36", "Australia");
		COUNTRY_CODES.put("40", "Austria");
		COUNTRY_CODES.put("50", "Bangladesh");
		COUNTRY_CODES.put("56", "Belgium");
		COUNTRY_CODES.put("68", "Bolivia");
		COUNTRY_CODES.put("76", "Brazil");
		COUNTRY_CODES.put("96", "Brunei");
		COUNTRY_CODES.put("116", "Cambodia");
		COUNTRY_CODES.put("124", "Canada");
		COUNTRY_CODES.put("156", "China (People's Republic)");
		COUNTRY_CODES.put("170", "Colombia");
		COUNTRY_CODES.put("203", "Czech Republic");
		COUNTRY_CODES.put("208", "Denmark");
		COUNTRY_CODES.put("218", "Ecuador");
		COUNTRY_CODES.put("818", "Egypt");
		COUNTRY_CODES.put("246", "Finland");
		COUNTRY_CODES.put("250", "France");
		COUNTRY_CODES.put("276", "Germany");
		COUNTRY_CODES.put("300", "Greece");
		COUNTRY_CODES.put("344", "Hong Kong");
		COUNTRY_CODES.put("348", "Hungary");
		COUNTRY_CODES.put("356", "India");
		COUNTRY_CODES.put("360", "Indonesia");
		COUNTRY_CODES.put("372", "Ireland");
		COUNTRY_CODES.put("376", "Israel");
		COUNTRY_CODES.put("380", "Italy");
		COUNTRY_CODES.put("392", "Japan");
		COUNTRY_CODES.put("418", "Laos");
		COUNTRY_CODES.put("458", "Malaysia");
		COUNTRY_CODES.put("484", "Mexico");
		COUNTRY_CODES.put("528", "Netherlands");
		COUNTRY_CODES.put("554", "New Zealand");
		COUNTRY_CODES.put("566", "Nigeria");
		COUNTRY_CODES.put("578", "Norway");
		COUNTRY_CODES.put("512", "Oman");
		COUNTRY_CODES.put("586", "Pakistan");
		COUNTRY_CODES.put("600", "Paraguay");
		COUNTRY_CODES.put("604", "Peru");
		COUNTRY_CODES.put("608", "Philippines");
		COUNTRY_CODES.put("616", "Poland");
		COUNTRY_CODES.put("620", "Portugal");
		COUNTRY_CODES.put("642", "Romania");
		COUNTRY_CODES.put("643", "Russia");
		COUNTRY_CODES.put("682", "Saudi Arabia");
		COUNTRY_CODES.put("702", "Singapore");
		COUNTRY_CODES.put("703", "Slovakia");
		COUNTRY_CODES.put("710", "South Africa");
		COUNTRY_CODES.put("410", "South Korea");
		COUNTRY_CODES.put("724", "Spain");
		COUNTRY_CODES.put("144", "Sri Lanka");
		COUNTRY_CODES.put("752", "Sweden");
		COUNTRY_CODES.put("756", "Switzerland");
		COUNTRY_CODES.put("158", "Taiwan");
		COUNTRY_CODES.put("764", "Thailand");
		COUNTRY_CODES.put("792", "Turkey");
		COUNTRY_CODES.put("784", "United Arab Emirates");
		COUNTRY_CODES.put("826", "United Kingdom");
		COUNTRY_CODES.put("840", "United States");
		COUNTRY_CODES.put("858", "Uruguay");
		COUNTRY_CODES.put("862", "Venezuela");
		COUNTRY_CODES.put("704", "Vietnam");
		COUNTRY_CODES.put("999", "Other");
	};
	
	
	public static HashMap<String, String> EDUC_CODES = 
		new HashMap<String, String>();
	static
	{	
		// single-select :  based on value:  code = EDUC  value = 1-5
		EDUC_CODES.put("1", "Some secondary/high school");
		EDUC_CODES.put("2", "Secondary/high school graduate (Diploma, Baccalaureate, A-Levels, Abitur)");
		EDUC_CODES.put("3", "Undergraduate degree (B.A., B.S., B.Sc., Maitrise)");
		EDUC_CODES.put("4", "Postgraduate degree (M.A., M.S., M.B.A., M.Sc., M.Phil.)");
		EDUC_CODES.put("5", "Doctorate/professional (Ph.D., M.D.)");
	};

	
	public static HashMap<String, String> INDUST_CODES = 
		new HashMap<String, String>();
	static
	{	
		// single-select :  code:  INDUST   value:  5
		INDUST_CODES.put("1", "Some secondary/high school");
		INDUST_CODES.put("1", "Aerospace");
		INDUST_CODES.put("2", "Agriculture, Livestock,  &amp; Fishing");
		INDUST_CODES.put("3", "Automotive");
		INDUST_CODES.put("4", "Banking, Financial Services,  &amp; Insurance");
		INDUST_CODES.put("5", "Biotechnology  &amp; Pharmaceuticals");
		INDUST_CODES.put("6", "Business Services (e.g., IT, engineering, financial, consulting; printing/publishing; advertising)");
		INDUST_CODES.put("7", "Chemicals, Plastics,  &amp; Rubber Products");
		INDUST_CODES.put("8", "Computer  &amp; Electronics (e.g., software, audio  &amp; video, hardware, semiconductors, networking)");
		INDUST_CODES.put("9", "Construction  &amp; Engineering");
		INDUST_CODES.put("10", "Container  &amp; Packaging");
		INDUST_CODES.put("11", "Education  &amp; Training");
		INDUST_CODES.put("14", "Food, Beverages,  &amp; Tobacco");
		INDUST_CODES.put("15", "Foundations/Nonprofit (not including hospitals)");
		INDUST_CODES.put("16", "Government");
		INDUST_CODES.put("17", "Health Care");
		INDUST_CODES.put("18", "Housing  &amp; Real Estate");
		INDUST_CODES.put("19", "Industrial  &amp; Farm Equipment");
		INDUST_CODES.put("20", "Mining  &amp; Metals");
		INDUST_CODES.put("21", "Multi-Industry Holding (corporate staff)");
		INDUST_CODES.put("22", "Oil  &amp; Gas (production, equipment, services, refinement, pipelines)");
		INDUST_CODES.put("23", "Personal Services");
		INDUST_CODES.put("24", "Restaurants  &amp; Food Services");
		INDUST_CODES.put("25", "Telecommunications (e.g., cable, communication networks)");
		INDUST_CODES.put("26", "Textiles Manufacturing (e.g., apparel, footwear)");
		INDUST_CODES.put("27", "Transportation, Freight,  &amp; Logistics (e.g., airlines, railways, trucking)");
		INDUST_CODES.put("28", "Travel  &amp; Leisure (e.g., hotels, recreation)");
		INDUST_CODES.put("29", "Utilities  &amp; Energy (except oil &amp; gas)");
		INDUST_CODES.put("30", "Waste Management Services, Water Treatment,  &amp; Environmental Services");
		INDUST_CODES.put("31", "Wholesale/Retail Trade (e.g., general merchandise, specialty retail)");
		INDUST_CODES.put("32", "Wood, Paper,  &amp; Forestry");
		INDUST_CODES.put("999", "Other");
	};

	public static HashMap<String, String> FUNC_CODES = 
		new HashMap<String, String>();
	static
	{	
		// MULTI-select :  code:  FUNC   value:  5
		FUNC_CODES.put("1", "Administrative Services/Support");  // FUNC_01
		FUNC_CODES.put("11", "Business Unit General Management/P&amp;L Management");
		FUNC_CODES.put("3", "Corporate Administration");
		FUNC_CODES.put("13", "Corporate Communications &amp; Public Relations");
		FUNC_CODES.put("18", "Customer Service");
		FUNC_CODES.put("4", "Engineering");
		FUNC_CODES.put("5", "Finance/Accounting/Treasury");
		FUNC_CODES.put("6", "Human Resources/O.D./T&amp;D");
		FUNC_CODES.put("7", "Information Technology");
		FUNC_CODES.put("8", "Legal");
		FUNC_CODES.put("12", "Logistics/Distribution/Warehouse/Transportation");
		FUNC_CODES.put("9", "Manufacturing/Production");
		FUNC_CODES.put("10", "Marketing/Advertising");
		FUNC_CODES.put("21", "Product Support");
		FUNC_CODES.put("20", "Professional Services Delivery");
		FUNC_CODES.put("14", "Purchasing &amp; Supplier Management");
		FUNC_CODES.put("15", "Real Estate/Facilities Management/Asset Management");
		FUNC_CODES.put("16", "Research &amp; Development/Product Development");
		FUNC_CODES.put("17", "Sales");
		FUNC_CODES.put("999", "Other");
	
	};

	public static HashMap<String, String> MGLEV_CODES = 
		new HashMap<String, String>();
	static
	{	
		// single SELECT :  code:  MGLEV   value:  1	
		MGLEV_CODES.put("8", "Chief Executive Officer");
		MGLEV_CODES.put("7", "Senior/Top Business Group Executive (enterprise-wide leader of a business group: Business Group President, Business Group EVP)");
		MGLEV_CODES.put("6", "Senior/Top Functional Executive (enterprise-wide leader of a function: EVP of HR, CFO)");
		MGLEV_CODES.put("5", "Business Unit Leader (e.g., General Manager, Country Manager)");
		MGLEV_CODES.put("4", "Functional Leader (within a business unit or head of a corporate sub-function)");
		MGLEV_CODES.put("3", "Mid-level Leader (manager of managers/supervisors)");
		MGLEV_CODES.put("2", "First-line Leader");
		MGLEV_CODES.put("1", "Individual Contributor");

	};
	
	public static HashMap<String, String> ORGSIZE_CODES = 
		new HashMap<String, String>();
	static
	{	
		// single-select :  code:  FUNC   value:  5
		ORGSIZE_CODES.put("4", "less than 1,000");  // ORGSIZE_04
		ORGSIZE_CODES.put("5", "1,000 - 4,999"); 
		ORGSIZE_CODES.put("6", "5,000 - 9,999"); 
		ORGSIZE_CODES.put("7", "10,000 - 24,999"); 
		ORGSIZE_CODES.put("8", "25,000 - 49,999"); 
		ORGSIZE_CODES.put("9", "50,000 - 99,999"); 
		ORGSIZE_CODES.put("10", "100,000 or more"); 
	};
	
	public static HashMap<String, String> SAL_CURR_CODES = 
		new HashMap<String, String>();
	static
	{	
		// single-select :  code:  SAL_CURR   value:  5
		//SAL_CURR_CODES.put("1", "Euro"); 
		//SAL_CURR_CODES.put("2", "U.S. Dollar"); 
		//SAL_CURR_CODES.put("3", "Australian Dollar"); 
		//SAL_CURR_CODES.put("4", "Hong Kong Dollar"); 
		//SAL_CURR_CODES.put("5", "Singapore Dollar"); 
		//SAL_CURR_CODES.put("6", "British Pound"); 
		//SAL_CURR_CODES.put("7", "Japanese Yen"); 
		//SAL_CURR_CODES.put("8", "Chinese Yuan"); 
		//SAL_CURR_CODES.put("9", "Swedish Kronas"); 
		//SAL_CURR_CODES.put("10", "Swiss Franc"); 
		//SAL_CURR_CODES.put("11", "Indian Rupee"); 
		//SAL_CURR_CODES.put("12", "UAE Dirham"); 
		//SAL_CURR_CODES.put("13", "Canadian Dollar"); 
		//SAL_CURR_CODES.put("999", "Other"); 
		SAL_CURR_CODES.put("1", "Euro"); 
		SAL_CURR_CODES.put("2", "U.S. Dollar"); 
		SAL_CURR_CODES.put("3", "Australian Dollar"); 
		SAL_CURR_CODES.put("4", "Hong Kong Dollar"); 
		SAL_CURR_CODES.put("5", "Singapore Dollar"); 
		SAL_CURR_CODES.put("6", "British Pound"); 
		SAL_CURR_CODES.put("7", "Japanese Yen"); 
		SAL_CURR_CODES.put("8", "Chinese Yuan"); 
		SAL_CURR_CODES.put("9", "Swedish Kronas"); 
		SAL_CURR_CODES.put("10", "Other");		// <--- Note this departure form past cutom
		SAL_CURR_CODES.put("11", "Swiss Franc"); 
		SAL_CURR_CODES.put("12", "Indian Rupee"); 
		SAL_CURR_CODES.put("13", "UAE Dirham"); 
		SAL_CURR_CODES.put("14", "Canadian Dollar"); 
	};
	
	// rank 5 factors....
	public static HashMap<String, String> IMP_CODES = 
		new HashMap<String, String>();
	static
	{	
		// MULTI SELECT :  code:  IMP_   value:  5
	    IMP_CODES.put("IMP_ADVC", "Advancement opportunities (promotions)");
	    IMP_CODES.put("IMP_FAST", "Fast-paced environment with lots of change");
	    IMP_CODES.put("IMP_FRND", "Friendly workplace with good relations with coworkers, vendors, and/or clients");
	    IMP_CODES.put("IMP_ACCM", "Sense of personal accomplishment");
	    IMP_CODES.put("IMP_PERF", "Hard-driving, performance-based culture");
	    IMP_CODES.put("IMP_AUTO", "Autonomy in carrying out my responsibilities");
	    IMP_CODES.put("IMP_MISN", "Belief in the mission of the organization");
	    IMP_CODES.put("IMP_AUTH", "Expert or authority status");
	    IMP_CODES.put("IMP_FRMG", "Fair and consistent managers");
	    IMP_CODES.put("IMP_INFL", "Influence on the direction of the organization");
	    IMP_CODES.put("IMP_MONY", "Monetary compensation");
	    IMP_CODES.put("IMP_CRTV", "Opportunity to be creative");
	    IMP_CODES.put("IMP_RESP", "Responsibility for the performance of others and the results of the unit");
	    IMP_CODES.put("IMP_CHL", "Stimulating, challenging work");
	    IMP_CODES.put("IMP_DEV", "Training or development opportunities");
	    IMP_CODES.put("IMP_VAR", "Variety in job duties");
	    IMP_CODES.put("IMP_RECG", "Visibility and recognition");
	    IMP_CODES.put("IMP_STBL", "Working in a stable job with little change");
	    IMP_CODES.put("IMP_BAL", "Work-life balance");  
	};
		
	public static HashMap<String, String> LANG_CODES = 
		new HashMap<String, String>();
	static
	{	
		// single SELECT :  code:  LANG   value:  21	
		LANG_CODES.put("1", "Afrikaans");
        LANG_CODES.put("2", "Arabic");
        LANG_CODES.put("3", "Bahasa Indonesia");
        LANG_CODES.put("4", "Bengali");
		LANG_CODES.put("12", "Chinese (Cantonese)");
		LANG_CODES.put("5", "Chinese (Mandarin, dialect)");
		LANG_CODES.put("6", "Czech");
		LANG_CODES.put("7", "Danish");
		LANG_CODES.put("8", "Dutch");
		LANG_CODES.put("9", "English");
		LANG_CODES.put("10", "Egyptian");
		LANG_CODES.put("11", "Filipino");
		LANG_CODES.put("13", "Finnish");
		LANG_CODES.put("14", "French");
		LANG_CODES.put("19", "German");
		LANG_CODES.put("18", "Greek");
		LANG_CODES.put("15", "Hebrew");
		LANG_CODES.put("16", "Hindi"); 
		LANG_CODES.put("17", "Hungarian");
		LANG_CODES.put("20", "Irish Gaelic");
		LANG_CODES.put("21", "Italian");
		LANG_CODES.put("22", "Japanese");
		LANG_CODES.put("23", "Khmer");
		LANG_CODES.put("24", "Korean");
		LANG_CODES.put("25", "Lao");
		LANG_CODES.put("26", "Malay");
		LANG_CODES.put("27", "Maori");
		LANG_CODES.put("28", "Norwegian");
		LANG_CODES.put("29", "Polish");
		LANG_CODES.put("30", "Portuguese");
		LANG_CODES.put("31", "Quechua");
		LANG_CODES.put("32", "Romanian");
		LANG_CODES.put("33", "Romansh");
		LANG_CODES.put("34", "Russian");
		LANG_CODES.put("35", "Sinhala");
		LANG_CODES.put("36", "Slovak"); 
		LANG_CODES.put("37", "Spanish");
		LANG_CODES.put("38", "Swedish");
		LANG_CODES.put("39", "Tamil");
		LANG_CODES.put("40", "Thai");
		LANG_CODES.put("41", "Turkish");
		LANG_CODES.put("42", "Urdu");
		LANG_CODES.put("43", "Vietnamese");
	};

	public static HashMap<String, String> ETHNIC_CODES = 
		new HashMap<String, String>();
	static
	{	
		// single SELECT :  code:  LANG   value:  21	
		ETHNIC_CODES.put("5", "White/Caucasian");
		ETHNIC_CODES.put("1", "Black/African American");
		ETHNIC_CODES.put("99", "Other");
		ETHNIC_CODES.put("3", "Asian/Pacific Islander");
		ETHNIC_CODES.put("2", "Aleutian/Native American");
		ETHNIC_CODES.put("4", "Hispanic/Latino");
		ETHNIC_CODES.put("7", "Multi-ethnic background");
	};
	
	public static HashMap<String, String> GENDER_CODES = 
		new HashMap<String, String>();
	static
	{	
		// single SELECT :  code:  LANG   value:  21	
		GENDER_CODES.put("1", "Male");
		GENDER_CODES.put("2", "Female");
	};
	
	public static HashMap<String, String> YESNO_CODES = 
		new HashMap<String, String>();
	static
	{	
		// single SELECT :  code:  LANG   value:  21	
		YESNO_CODES.put("1", "Yes");
		YESNO_CODES.put("0", "No");
	};
	
	
}
