/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.reporting.report;

import java.util.HashMap;

import com.pdi.reporting.constants.ReportingConstants;

/**
 * Generic Report object used to store like data
 * that are used in both Group and Individual reports
 * 
 * @author		Gavin Myers
 */
public class IndividualReport extends Report implements Comparable<Object>
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	/**
	 * 
	 */
	private static final long serialVersionUID = -5200177743388129782L;
	//
	// Instance data.
	//
	private HashMap<String, String> _displayData = new HashMap<String, String>();
	private HashMap<String, ReportData>  _reportData  = new HashMap<String, ReportData>();
	private HashMap<String, String> _scriptedData = new HashMap<String, String>();
	private HashMap<String, String> _labelData = new HashMap<String, String>();	
	private HashMap<String, Double> _scriptedGroupData = new HashMap<String, Double>();
	private int _rank = 0;
	//
	// Constructors.
	//
	public IndividualReport()
	{
		this.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);
	}

	
	@SuppressWarnings("unchecked")
	public IndividualReport clone()
	{
		IndividualReport ret = new IndividualReport();
		// Do the superclass fields (defined in Report)
		ret.setReportType(this.getReportType());
		ret.setReportCode(this.getReportCode());
		ret.setData(this.getData());	// Technically this isn't cloned
		ret.setName(this.getName());
		ret.setConfigurationData((HashMap<String, String>) this.getConfigurationData().clone());

		// Now do the ones defined in IR
		ret.setDisplayData((HashMap<String, String>) _displayData.clone());
		ret.setReportData((HashMap<String, ReportData>) _reportData.clone());
		ret.setScriptedData((HashMap<String, String>) _scriptedData.clone());
		ret.setLabelData((HashMap<String, String>) _labelData.clone());	
		ret.setScriptedGroupData((HashMap<String, Double>) _scriptedGroupData.clone());
		ret.setRank(_rank);
		
		return ret;
	}

	
	public void addDisplayData(String key, String value)
	{
		_displayData.put(key, value);
	}
	public String getDisplayData(String key)
	{
		return _displayData.get(key);
	}
	public void setDisplayData(HashMap<String, String> displayData)
	{
		_displayData = displayData;
	}
	public HashMap<String, String> getDisplayData()
	{
		return _displayData;
	}

	
	public void setReportData(HashMap<String, ReportData> reportData)
	{
		_reportData = reportData;
	}
	public HashMap<String, ReportData> getReportData()
	{
		return _reportData;
	}
	
	
	public void addScriptedData(String key, String value)
	{
		_scriptedData.put(key, value);
	}
	public String getScriptedData(String key)
	{
		return _scriptedData.get(key);
	}
	public void setScriptedData(HashMap<String, String> scriptedData)
	{
		_scriptedData = scriptedData;
	}
	public HashMap<String, String> getScriptedData()
	{
		return _scriptedData;
	}
	
	//****************************************************************
	public void addLabelData(String key, String value)
	{
		_labelData.put(key, value);
	}
	
	public String getLabelData(String key)
	{
		return _labelData.get(key);
	}
	
	public void setLabelData(HashMap<String, String> scriptedData)
	{
		_labelData = scriptedData;
	}
	
	public HashMap<String, String> getLabelData()
	{
		return _labelData;
	}

	public void addScriptedGroupData(String key, Double value)
	{
		_scriptedGroupData.put(key, value);
	}
	public Double getScriptedGroupData(String key)
	{
		return _scriptedGroupData.get(key);
	}
	public void setScriptedGroupData(HashMap<String, Double> value)
	{
		_scriptedGroupData = value;
	}
	public HashMap<String, Double> getScriptedGroupData()
	{
		return _scriptedGroupData;
	}

	/*
	 * Convenience method used in the velocity template
	 */
	public int parseInt(String str)
	{
		int ret;
		try
		{
			ret = Integer.parseInt(str);
		}
		catch(Exception e)
		{
			ret = 0;
		}

		return ret;
	}
	
	public double parseDouble(String str) {
		if(str == null) {
			return 0;
		}
		return Double.parseDouble(str);
	}

	/*
	//@Override
	public int compareTo(Object obj) {
		return ((IndividualReport)obj).getRank() - this.getRank();
	}*/


	public void setRank(int rank) {
		_rank = rank;
	}


	public int getRank() {
		return _rank;
	}


	public int compareTo(Object obj) {
		// TODO Auto-generated method stub
		return ((IndividualReport)obj).getRank() - this.getRank();
	}
	
	public String toString()
	{
		String result = "IndividulaReport object dump:\n";
		
		result += "  Display Data:\n";
		for(String key : this.getDisplayData().keySet())
		{
			result += ("    " + key + " = " + this.getDisplayData(key) + "\n");
		}
		
		result += "  Report Data:\n";
		for(String key : this.getReportData().keySet())
		{
			//System.out.println("Key=" + key);
			// TODO clean up the groovy scripts so they don't do this
			// We need this because the groovy scripts (well TLT_GROUP_DETAIL_REPORT
			// anyway) puts String data into a HashMap defined as <String, ReportData>
			// causing java to fail.
			if (key.equals(ReportingConstants.RC_GPI)  ||
				key.equals(ReportingConstants.RC_LEI) ||
				key.equals(ReportingConstants.RC_RAVENS_SF) ||
				key.equals(ReportingConstants.RC_RAVENS_B) ||
				key.equals(ReportingConstants.RC_CAREER_SURVEY) ||
				key.equals(ReportingConstants.RC_WESMAN) ||
				key.equals(ReportingConstants.RC_WATSON_A_SF) ||
				key.equals(ReportingConstants.RC_FIN_EX) ||
				key.equals(ReportingConstants.RC_CHQ) ||
				key.equals(ReportingConstants.RC_WG_E)
				//key.equals(ReportingConstants.RC_WG_E)  ||
				//key.equals(ReportingConstants.RC_EXTRACT)
				)
			{
				ReportData rd = this.getReportData().get(key);
				result += "    " + "ReportData for " + key + ":\n" + rd.toString();
			} else {
				System.out.println(key + " does not equal anything!");
				ReportData rd = this.getReportData().get(key);
				result += "    " + "ReportData for " + key + ":\n" + rd.toString();
			}
		}
		
		result += "  Scripted Data:\n";
		for(String key : this.getScriptedData().keySet())
		{
			result += ("    " + key + " = " + this.getScriptedData(key) + "\n");
		}
		
		result += "  Scripted Group Data:\n";
		for(String key : this.getScriptedGroupData().keySet())
		{
			result += ("    " + key + " = " + this.getScriptedGroupData(key) + "\n");
		}
		
//		result += "Configuration data (superclass):\n";
//		for(String key : this.getConfigurationData().keySet()) {
//			result += (key + " = " + this.getConfigurationData(key));
//		}
		
		return result;
	}


	public String displayDataToString()
	{
		String result = "IndividulaReport displayData dump:\n";

		for(String key : this.getDisplayData().keySet())
		{
			result += ("    " + key + " = " + this.getDisplayData(key) + "\n");
		}
		
		return result;
	}


	public String displayDataStarting(String str)
	{
		String result = "IndividulaReport displayData (starting with '" + str + "') dump:\n";

		for(String key : this.getDisplayData().keySet())
		{
			if (key.startsWith(str))
			{
				result += ("    " + key + " = " + this.getDisplayData(key) + "\n");
			}
		}
		
		return result;
	}
}
