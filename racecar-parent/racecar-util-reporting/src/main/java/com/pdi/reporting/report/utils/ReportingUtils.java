package com.pdi.reporting.report.utils;

import com.pdi.reporting.report.GroupReport;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;
import com.pdi.xml.XMLUtils;

public class ReportingUtils
{
	public static IndividualReport xmlEscapeString(IndividualReport r)
	{
		// Escape no longer needed
		//The only thing that should contain free text is display data
		for(Object k : r.getDisplayData().keySet())
		{
			if(r.getDisplayData().get(k) instanceof String)
			{
				String v = r.getDisplayData().get(k);
				v = XMLUtils.xmlEscapeString(v);
				r.getDisplayData().put(k.toString(), v);
			}
		}

		
		for(ReportData rd : r.getReportData().values())
		{
			for(String k : rd.getDisplayData().keySet())
			{
				if(rd.getDisplayData().get(k) instanceof String)
				{
					String v = rd.getDisplayData().get(k);
					v = XMLUtils.xmlEscapeString(v);
					rd.getDisplayData().put(k, v);
				}
			}
		}

		return r;
	}

	
	public static GroupReport xmlEscapeString(GroupReport r)
	{
		// Escape no longer needed
		for(Object k : r.getDisplayData().keySet())
		{
			if(r.getDisplayData().get(k) instanceof String)
			{
				String v = r.getDisplayData().get(k);
				v = XMLUtils.xmlEscapeString(v);
				r.getDisplayData().put(k.toString(), v);
			}
		}
		for(IndividualReport ir : r.getIndividualReports())
		{
			ir = ReportingUtils.xmlEscapeString(ir);
		}

		return r;
	}

}
