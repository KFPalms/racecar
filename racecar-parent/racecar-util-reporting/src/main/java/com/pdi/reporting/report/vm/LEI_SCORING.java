package com.pdi.reporting.report.vm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;

@SuppressWarnings("unchecked")
public class LEI_SCORING {
	//
	// Static Constants
	//
	private static ArrayList<String> LEI_SCALES = new ArrayList<String>();
	static
	{
		LEI_SCALES.add("LEI3_SGY");	//  Scale 01 - Strategy Development
		LEI_SCALES.add("LEI3_PGT");	//  Scale 02 - Project Management and Implementation
		LEI_SCALES.add("LEI3_BDV");	//  Scale 03 - Business Development and Marketing
		LEI_SCALES.add("LEI3_BGR"); //  Scale 04 - Business Growth
		LEI_SCALES.add("LEI3_PDT");	//  Scale 05 - Product Development
		LEI_SCALES.add("LEI3_STA");	//  Scale 06 - Start-up Business
		LEI_SCALES.add("LEI3_FM");	//  Scale 07 - Financial Management
		LEI_SCALES.add("LEI3_OPX");	//	Scale 08 - Operations
		LEI_SCALES.add("LEI3_FEX");	//	Scale 09 - Support Functions
		LEI_SCALES.add("LEI3_RPR");	//	Scale 10 - External Relations
		LEI_SCALES.add("LEI3_PRB");	//	Scale 11 - Inherited Problems and Challenges
		LEI_SCALES.add("LEI3_PPL");	//	Scale 12 - Interpersonally Challenging Situations
		LEI_SCALES.add("LEI3_FAL");	//	Scale 13 - Downturn and/or Failures
		LEI_SCALES.add("LEI3_FIN");	//	Scale 14 - Difficult Financial Situations
		LEI_SCALES.add("LEI3_STF");	//	Scale 15 - Difficult Staffing Situations
		LEI_SCALES.add("LEI3_HR");	//	Scale 16 - High Risk Situations
		LEI_SCALES.add("LEI3_NEG");	//	Scale 17 - Critical Negotiations
		LEI_SCALES.add("LEI3_CMT");	//	Scale 18 - Crisis Management
		LEI_SCALES.add("LEI3_VAS");	//	Scale 19 - Highly Critical/Visible Assignments
		LEI_SCALES.add("LEI3_SDV");	//	Scale 20 - Self-Development Scale
		LEI_SCALES.add("LEI3_DEV");	//	Scale 21 - Development of Others
		LEI_SCALES.add("LEI3_INN");	//	Scale 22 - International/Cross-Cultural
		LEI_SCALES.add("LEI3_EXT");	//	Scale 23 - Extracurricular Activities
	}
	
	// 
	// Static methods
	//
	//This first set is the DevSug scores (not widely used)
	/*
	 * Calculate Scale 1 - ??? -- LEI_DS_SGY
	 */
	protected static float calcDss1(float score)
	{
		return (score - (float)118.528) / (float)31.391;
	}
	
	
	/*
	 * Calculate Scale 2 - PMGT -- LEI_DS_PGT
	 */
	protected static float calcDss2(float score)
	{
		return (score - (float)141.608) / (float)34.195;
	}
	
	
	/*
	 * Calculate Scale 3 - BDEV -- LEI_DS_BDV
	 */
	protected static float calcDss3(float score)
	{
		return (score - (float)73.371) / (float)21.413;
	}
	
	
	/*
	 * Calculate Scale 4 - BGROW -- LEI_DS_BGR
	 */
	protected static float calcDss4(float score)
	{
		return (score - (float)75.478) / (float)21.348;
	}
	
	
	/*
	 * Calculate Scale 5 - PRODUCT -- LEI_DS_PDT
	 */
	protected static float calcDss5(float score)
	{
		return (score - (float)35.809) / (float)10.630;
	}
	
	
	/*
	 * Calculate Scale 6 - START -- LEI_DS_STA
	 */
	protected static float calcDss6(float score)
	{
		return (score - (float)39.943) / (float)10.952;
	}
	
	
	/*
	 * Calculate Scale 7 - FM -- LEI_DS_FM
	 */
	protected static float calcDss7(float score)
	{
		return (score - (float)124.341) / (float)31.910;
	}
	
	
	/*
	 * Calculate Scale 8 - OPEXP -- LEI_DS_OPX
	 */
	protected static float calcDss8(float score)
	{
		return (score - (float)112.743) / (float)26.634;
	}
	
	
	/*
	 * Calculate Scale 9 - FCNEXP -- LEI_DS_FEX
	 */
	protected static float calcDss9(float score)
	{
		return (score - (float)96.252) / (float)21.798;
	}
	
	
	/*
	 * Calculate Scale 10 - REPRE -- LEI_DS_RPR
	 */
	protected static float calcDss10(float score)
	{
		return (score - (float)132.200) / (float)32.280;
	}
	
	
	/*
	 * Calculate Scale 11 - PROB -- LEI_DS_PRB
	 */
	protected static float calcDss11(float score)
	{
		return (score - (float)100.088) / (float)21.578;
	}
	
	
	/*
	 * Calculate Scale 12 - PEOPLE -- LEI_DS_PPL
	 */
	protected static float calcDss12(float score)
	{
		return (score - (float)242.678) / (float)48.143;
	}
	
	
	/*
	 * Calculate Scale 13 - FAILURE -- LEI_DS_FAL
	 */
	protected static float calcDss13(float score)
	{
		return (score - (float)64.163) / (float)16.643;
	}
	
	
	/*
	 * Calculate Scale 14 - FINSIT -- LEI_DS_FIN
	 */
	protected static float calcDss14(float score)
	{
		return (score - (float)107.955) / (float)28.407;
	}
	
	
	/*
	 * Calculate Scale 15 - STAFF -- LEI_DS_STF
	 */
	protected static float calcDss15(float score)
	{
		return (score - (float)119.526) / (float)25.874;
	}
	
	
	/*
	 * Calculate Scale 16 - HIRISK -- LEI_DS_HR
	 */
	protected static float calcDss16(float score)
	{
		return (score - (float)141.398) / (float)34.765;
	}
	
	
	/*
	 * Calculate Scale 17 - NEGOT -- LEI_DS_NEG
	 */
	protected static float calcDss17(float score)
	{
		return (score - (float)152.605) / (float)39.071;
	}
	
	
	/*
	 * Calculate Scale 18 - CMGMT -- LEI_DS_CMT
	 */
	protected static float calcDss18(float score)
	{
		return (score - (float)102.225) / (float)24.704;
	}
	
	
	/*
	 * Calculate Scale 19 - VISASS -- LEI_DS_VAS
	 */
	protected static float calcDss19(float score)
	{
		return (score - (float)191.590) / (float)46.622;
	}
	
	
	/*
	 * Calculate Scale 20 - SELFDEV -- LEI_DS_SDV
	 */
	protected static float calcDss20(float score)
	{
		return (score - (float)99.710) / (float)14.693;
	}
	
	
	/*
	 * Calculate Scale 21 - DEV -- LEI_DS_DEV
	 */
	protected static float calcDss21(float score)
	{
		return (score - (float)92.575) / (float)16.403;
	}
	
	
	/*
	 * Calculate Scale 22 - INTERN -- LEI_DS_INN
	 */
	protected static float calcDss22(float score)
	{
		return (score - (float)52.988) / (float)16.767;
	}
	
	
	/*
	 * Calculate Scale 23 - EXTRA -- LEI_DS_EXT
	 */
	protected static float calcDss23(float score)
	{
		return (score - (float)47.117) / (float)10.204;
	}
	
	
	/*
	 * Calculate Overall - OVERALL -- LEI_DS_OAL
	 */
	protected static float calcDssOverall(float score)
	{
		return (score - (float)2464.889) / (float)548.675;
	}
	
	/*
	 * This first section does all of the scale rollup calculations
	 */
	
	/*
	 * Calculate Scale 1 - Strategy Development (LEI3_SGY)
	 * NOTE: This is assuming that the answer values for the questions are
	 *       1 - 4 for the first section, and 1 - 4 for the third section
	 */	
	protected static float calcScale1(HashMap<String, Float> questions)
	{
		float scaleScore = (float)(float)0.0;
		
		//{Section 1} 	
		float score = (float)questions.get("Q1");
		if (score < 1.0 || score > 4.0)  score = (float)(float)1.0;
		scaleScore += (float)1.2 * score;
		
		score = (float)questions.get("Q18");	
		if (score < 1.0 || score > 4.0)  score = (float)(float)1.0;
		scaleScore += (float)1.3 * score;
		
		//{Section 2}  
		scaleScore += (float)1.6 * questions.get("Q39");
		scaleScore += (float)2.8 * questions.get("Q40"); 
		scaleScore += (float)2.4 * questions.get("Q41"); 
		scaleScore += (float)2.3 * questions.get("Q43");
		scaleScore += (float)1.2 * questions.get("Q56"); 
		scaleScore += (float)1.1 * questions.get("Q65");
		scaleScore += (float)1.0 * questions.get("Q67");
		scaleScore += (float)1.6 * questions.get("Q69");
		scaleScore += (float)1.7 * questions.get("Q70");
		scaleScore += (float)2.3 * questions.get("Q73");
		scaleScore += (float)1.8 * questions.get("Q74");
		scaleScore += (float)1.6 * questions.get("Q75");
		scaleScore += (float)1.9 * questions.get("Q76");
		scaleScore += (float)1.3 * questions.get("Q77");
		
		//{Section 3} 
		score = (float)questions.get("Q81");	
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.1 * score;
		
		score = (float)questions.get("Q88");	
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.4 * score;
		
		score = (float)questions.get("Q89");	
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.2 * score;
		
		score = (float)questions.get("Q92");	
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.9 * score;
		
		score = (float)questions.get("Q94");	
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.1 * score;
		
		return scaleScore;
	}
	
	
	/*
	 * Calculate Scale 02 - Project Management and Implementation Scale (LEI3_PGT)
	 */
	protected static float calcScale2(HashMap<String, Float> questions)
	{
		float scaleScore = (float)0.0;
		
		//{Section 1}
		float score = (float)questions.get("Q8");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.0 * score;
	
		score = (float)questions.get("Q24");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.6 * score;
	
		//{Section 2} 
		scaleScore += (float)1.5 * questions.get("Q34");
		scaleScore += (float)1.4 * questions.get("Q35");
		scaleScore += (float)1.7 * questions.get("Q36");
		scaleScore += (float)1.4 * questions.get("Q38");
		scaleScore += (float)2.1 * questions.get("Q42");
		scaleScore += (float)1.4 * questions.get("Q43");
		scaleScore += (float)1.8 * questions.get("Q53");
		scaleScore += (float)1.7 * questions.get("Q54");
		scaleScore += (float)1.1 * questions.get("Q55");
		scaleScore += (float)1.0 * questions.get("Q56");
		scaleScore += (float)1.7 * questions.get("Q57");
		scaleScore += (float)1.9 * questions.get("Q58");
		scaleScore += (float)1.5 * questions.get("Q59");
		scaleScore += (float)1.7 * questions.get("Q60");
		scaleScore += (float)1.2 * questions.get("Q61");
		scaleScore += (float)1.0 * questions.get("Q68");
		scaleScore += (float)1.5 * questions.get("Q69");
		scaleScore += (float)1.3 * questions.get("Q70");
		scaleScore += (float)2.5 * questions.get("Q71");
		scaleScore += (float)1.1 * questions.get("Q72");
		scaleScore += (float)1.2 * questions.get("Q74");
		scaleScore += (float)1.2 * questions.get("Q75");
		scaleScore += (float)1.4 * questions.get("Q76");
		
		//{Section 3} 
		score = (float)questions.get("Q87");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.0 * score;
		
		score = (float)questions.get("Q88");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.0 * score;
				
		score = (float)questions.get("Q91");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)3.0 * score;
		
		return scaleScore;
	}


	//	/*******************************************************************
	//	 3.)Calculate Scale 03 - Business Development and Marketing Scale (LEI3_BDV)
	//	 ********************************************************************/	
	protected static float calcScale3(HashMap<String, Float> questions)
	{
		float scaleScore = (float)0.0;
		
		//{No Section 1 items} 
	
		//{Section 2} 
		scaleScore += (float)1.9 * questions.get("Q56");
		scaleScore += (float)1.1 * questions.get("Q58");
		scaleScore += (float)1.7 * questions.get("Q67");
		scaleScore += (float)1.0 * questions.get("Q68");
		scaleScore += (float)1.0 * questions.get("Q69");
		scaleScore += (float)1.4 * questions.get("Q70");
		scaleScore += (float)2.9 * questions.get("Q73");
		scaleScore += (float)1.0 * questions.get("Q75");
		scaleScore += (float)1.0 * questions.get("Q77");
		scaleScore += (float)1.0 * questions.get("Q78");
		scaleScore += (float)1.2 * questions.get("Q80");
		
		//{Section 3} 
		float score = (float)questions.get("Q92");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.0 * score;
	
		score = (float)questions.get("Q93");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.8 * score;

		return scaleScore;
	}


	/*
	 * Calculate Scale 04 - Business Growth Scale (LEI3_BGR)
	 */
	protected static float calcScale4(HashMap<String, Float> questions)
	{
		float scaleScore = (float)0.0;
		
		//{Section 1}
		float score = (float)questions.get("Q1");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.0 * score;

		//{Section 2} 
		scaleScore += (float)1.1 * questions.get("Q41");
		scaleScore += (float)1.2 * questions.get("Q56");
		scaleScore += (float)1.1 * questions.get("Q58");
		scaleScore += (float)1.2 * questions.get("Q67");
		scaleScore += (float)1.2 * questions.get("Q68");
		scaleScore += (float)1.6 * questions.get("Q69");
		scaleScore += (float)1.8 * questions.get("Q70");
		scaleScore += (float)2.0 * questions.get("Q73");
		scaleScore += (float)1.1 * questions.get("Q74");
		scaleScore += (float)1.0 * questions.get("Q76");
	
		//{Section 3} 
		score = (float)questions.get("Q88");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.1 * score;
	
		score = (float)questions.get("Q92");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.7 * score;
	
		score = (float)questions.get("Q93");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.6 * score;

		return scaleScore;
	}

	
	/*
	 * Calculate Scale 05 - Product Development Scale (LEI3_PDT)
	//	 ********************************************************************/	
	protected static float calcScale5(HashMap<String, Float> questions)
	{
		float scaleScore = (float)0.0;
		
		// {No Section 1 items} 

		//{Section 2} 
		scaleScore += (float)1.7 * questions.get("Q53");
		scaleScore += (float)1.6 * questions.get("Q55");
		scaleScore += (float)2.7 * questions.get("Q57");
		scaleScore += (float)1.1 * questions.get("Q58");
		scaleScore += (float)1.0 * questions.get("Q75");
	
		//{No Section 3 items} 

		return scaleScore;
	}

	
	/*
	 * Calculate Scale 06 - Start-up Business Scale (LEI3_STA)
	 */
	protected static float calcScale6(HashMap<String, Float> questions)
	{
		float scaleScore = (float)0.0;

		// {No Section 1 items} 

		//{Section 2} 
		scaleScore += (float)1.9 * questions.get("Q68");
		scaleScore += (float)2.0 * questions.get("Q69");
		scaleScore += (float)2.6 * questions.get("Q70");
		scaleScore += (float)1.2 * questions.get("Q74");
		scaleScore += (float)1.0 * questions.get("Q75");
		scaleScore += (float)1.1 * questions.get("Q76");
	
		//{No Section 3 items} 

		return scaleScore;
	}


	/*
	 * Calculate Scale 07 - Financial Management Scale (LEI3_FM)
	 */
	protected static float calcScale7(HashMap<String, Float> questions)
	{
		float scaleScore = (float)0.0;
		
		//{Section 1}
		float score = (float)questions.get("Q1");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.5 * score;

		score = (float)questions.get("Q9");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.3 * score;
	
		score = (float)questions.get("Q14");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.5 * score;
	
		score = (float)questions.get("Q15");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.8 * score;
	
		score = (float)questions.get("Q16");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.1 * score;
	
		score = (float)questions.get("Q17");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.6 * score;
	
		score = (float)questions.get("Q18");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.3 * score;
	
		//{Section 2} 
		scaleScore += (float)1.0 * questions.get("Q45");
		scaleScore += (float)1.8 * questions.get("Q51");
		scaleScore += (float)1.0 * questions.get("Q61");
		scaleScore += (float)2.0 * questions.get("Q62");
		scaleScore += (float)1.6 * questions.get("Q63");
		scaleScore += (float)1.3 * questions.get("Q65");
		scaleScore += (float)1.5 * questions.get("Q69");
		scaleScore += (float)1.4 * questions.get("Q70");
		scaleScore += (float)1.7 * questions.get("Q71");
		scaleScore += (float)2.0 * questions.get("Q72");
		scaleScore += (float)1.1 * questions.get("Q75");
		scaleScore += (float)2.0 * questions.get("Q80");
	
		//{Section 3} 
		score = (float)questions.get("Q87");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.1 * score;
	
		score = (float)questions.get("Q88");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.7 * score;
	
		score = (float)questions.get("Q89");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.6 * score;
	
		score = (float)questions.get("Q92");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.7 * score;
	
		score = (float)questions.get("Q94");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.7 * score;

		return scaleScore;
	}

	
	/*
	 * Calculate Scale 08 - Operations Scale (LEI3_OPX)
	//	 ********************************************************************/	
	protected static float calcScale8(HashMap<String, Float> questions)
	{
		float scaleScore = (float)0.0;
		
		//{Section 1}
		float score = (float)questions.get("Q1");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.0 * score;
	
		score = (float)questions.get("Q16");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.1 * score;
	
		//{Section 2} 
		scaleScore += (float)2.3 * questions.get("Q42");
		scaleScore += (float)1.2 * questions.get("Q55");
		scaleScore += (float)1.0 * questions.get("Q57");
		scaleScore += (float)1.6 * questions.get("Q59");
		scaleScore += (float)1.2 * questions.get("Q60");
		scaleScore += (float)1.2 * questions.get("Q68");
		scaleScore += (float)1.1 * questions.get("Q69");
		scaleScore += (float)1.5 * questions.get("Q70");
		scaleScore += (float)1.7 * questions.get("Q71");
		
		//{Section 3} 
		score = (float)questions.get("Q86");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.3 * score;
	
		score = (float)questions.get("Q87");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)3.0 * score;
	
		score = (float)questions.get("Q88");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.4 * score;
	
		score = (float)questions.get("Q89");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.9 * score;
	
		score = (float)questions.get("Q90");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.4 * score;
	
		score = (float)questions.get("Q91");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.9 * score;

		return scaleScore;
	}

	
	/*
	 * Calculate Scale 09 - Support Functions Scale (LEI3_FEX)
	 */
	protected static float calcScale9(HashMap<String, Float> questions)
	{
		float scaleScore = (float)0.0;
		
		//{Section 1}
		float score = (float)questions.get("Q16");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.6 * score;
	
		score = (float)questions.get("Q19");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.2 * score;
	
		score = (float)questions.get("Q22").floatValue();		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.0 * score;
	
		//{Section 2} 
		scaleScore +=1.6 * questions.get("Q54");
		scaleScore +=1.3 * questions.get("Q60");
		scaleScore +=1.3 * questions.get("Q61");
		scaleScore +=1.0 * questions.get("Q71");
	
		//{Section 3} 
		score = (float)questions.get("Q86");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.0 * score;
	
		score = (float)questions.get("Q87");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.0 * score;
	
		score = (float)questions.get("Q88");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.1 * score;
	
		score = (float)questions.get("Q89");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.0 * score;
	
		score = (float)questions.get("Q90");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.0 * score;

		return scaleScore;
	}

	
	/*
	 * Calculate Scale 10 - External Relations Scale (LEI3_RPR)
	 */
	protected static float calcScale10(HashMap<String, Float> questions)
	{
		float scaleScore = (float)0.0;
		
		//{Section 1}
		float score = (float)questions.get("Q13");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.5 * score;
	
		score = (float)questions.get("Q25");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.5 * score;
	
		score = (float)questions.get("Q26");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.7 * score;
	
		score = (float)questions.get("Q27");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.7 * score;
	
		//{Section 2} 
		scaleScore += (float)1.6 * questions.get("Q35");
		scaleScore += (float)1.3 * questions.get("Q45");
		scaleScore += (float)1.6 * questions.get("Q46");
		scaleScore += (float)2.3 * questions.get("Q47");
		scaleScore += (float)1.5 * questions.get("Q48");
		scaleScore += (float)1.9 * questions.get("Q49");
		scaleScore += (float)1.9 * questions.get("Q50");
		scaleScore += (float)1.6 * questions.get("Q52");
		scaleScore += (float)1.0 * questions.get("Q66");
		scaleScore += (float)1.6 * questions.get("Q74");
		scaleScore += (float)1.6 * questions.get("Q75");
		scaleScore += (float)1.1 * questions.get("Q76");
		scaleScore += (float)2.7 * questions.get("Q77");
		scaleScore += (float)2.9 * questions.get("Q78");
		scaleScore += (float)1.1 * questions.get("Q79");
		scaleScore += (float)1.4 * questions.get("Q80");
	
		//{Section 3} 
		score = (float)questions.get("Q86");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.2 * score;
	
		score = (float)questions.get("Q93");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.6 * score;
	
		score = (float)questions.get("Q94");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.4 * score;
	
		score = (float)questions.get("Q104");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.2 * score;

		return scaleScore;
	}
	
	
	/*
	 * Calculate Scale 11 - Inherited Problems and Challenges Scale (LEI3_PRB)
	 */
	protected static float calcScale11(HashMap<String, Float> questions)
	{
		float scaleScore = (float)0.0;

		//{Section 1}
		float score = (float)questions.get("Q1");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.5 * score;

		score = (float)questions.get("Q2");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)3.0 * score;
		
		score = (float)questions.get("Q3");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.5 * score;

		score = (float)questions.get("Q4");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.4 * score;

		score = (float)questions.get("Q5");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.6 * score;

		score = (float)questions.get("Q22");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.2 * score;

		//{Section 2} 
		scaleScore += (float)1.1 * questions.get("Q65");
		scaleScore += (float)1.0 * questions.get("Q75");
		scaleScore += (float)1.1 * questions.get("Q76");
		scaleScore += (float)1.4 * questions.get("Q77");

		//{Section 3} 
		score = (float)questions.get("Q81");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.1* score;

		score = (float)questions.get("Q85");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.1 * score;

		score = (float)questions.get("Q94");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.0 * score;

		score = (float)questions.get("Q97");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.0 * score;

		return scaleScore;
	}

	
	/*
	 * Calculate Scale 12 - Interpersonally Challenging Situations Scale (LEI3_PPL)
	 */
	protected static float calcScale12(HashMap<String, Float> questions)
	{
		float scaleScore = (float)0.0;

		//{Section 1}
		float score = (float)questions.get("Q2");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.8 * score;

		score = (float)questions.get("Q3");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.6 * score;

		score = (float)questions.get("Q4");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.6 * score;

		score = (float)questions.get("Q5");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.0 * score;

		score = (float)questions.get("Q6");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.6 * score;

		score = (float)questions.get("Q7");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.3 * score;

		score = (float)questions.get("Q8");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.4 * score;

		score = (float)questions.get("Q10");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.3 * score;

		score = (float)questions.get("Q11");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.6 * score;

		score = (float)questions.get("Q12");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.3 * score;

		score = (float)questions.get("Q13");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.1 * score;

		score = (float)questions.get("Q15");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.3 * score;

		score = (float)questions.get("Q16");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.5 * score;

		score = (float)questions.get("Q17");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.1 * score;

		score = (float)questions.get("Q20");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.8 * score;

		score = (float)questions.get("Q21");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.3 * score;

		score = (float)questions.get("Q22");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.2 * score;

		score = (float)questions.get("Q23");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.3 * score;

		//{Section 2} 
		scaleScore +=1.1 * questions.get("Q34");
		scaleScore +=1.0 * questions.get("Q35");
		scaleScore +=1.2 * questions.get("Q37");
		scaleScore +=1.1 * questions.get("Q44");
		scaleScore +=1.1 * questions.get("Q47");
		scaleScore +=1.7 * questions.get("Q48");
		scaleScore +=1.0 * questions.get("Q49");
		scaleScore +=1.0 * questions.get("Q50");
		scaleScore +=1.3 * questions.get("Q52");
		scaleScore +=1.9 * questions.get("Q64");
		scaleScore +=1.2 * questions.get("Q65");
		scaleScore +=1.4 * questions.get("Q70");
		scaleScore +=1.6 * questions.get("Q74");
		scaleScore +=1.6 * questions.get("Q75");
		scaleScore +=1.5 * questions.get("Q76");
		scaleScore +=1.4 * questions.get("Q78");

		//{Section 3} 
		score = (float)questions.get("Q81");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.0 * score;

		score = (float)questions.get("Q82");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.5 * score;

		score = (float)questions.get("Q83");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.1 * score;

		score = (float)questions.get("Q84");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.6 * score;

		score = (float)questions.get("Q85");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.5 * score;

		score = (float)questions.get("Q89");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.2 * score;

		score = (float)questions.get("Q92");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.0 * score;

		score = (float)questions.get("Q93");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.0 * score;

		score = (float)questions.get("Q94");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.2 * score;

		score = (float)questions.get("Q95");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.4 * score;

		score = (float)questions.get("Q96");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.2 * score;

		score = (float)questions.get("Q97");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.4 * score;

		score = (float)questions.get("Q101");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)3.0 * score;

		score = (float)questions.get("Q102");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.9 * score;

		score = (float)questions.get("Q103");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.8 * score;

		score = (float)questions.get("Q104");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.2 * score;

		return scaleScore;
	}

	
	/*
	 * Calculate Scale 13 - Down-turn and/or Failures Scale (LEI3_FAL)
	 */
	protected static float calcScale13(HashMap<String, Float> questions)
	{
		float scaleScore = (float)0.0;
				
		//{Section 1}
		float score = (float)questions.get("Q2");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.0 * score;

		score = (float)questions.get("Q22");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.7 * score;

		//{Section 2} 
		scaleScore +=2.1 * questions.get("Q38");
		scaleScore +=1.3 * questions.get("Q63");
		scaleScore +=2.0 * questions.get("Q64");
		scaleScore +=2.1 * questions.get("Q65");
		scaleScore +=1.3 * questions.get("Q66");
		scaleScore +=1.6 * questions.get("Q67");
		scaleScore +=1.0 * questions.get("Q77");

		//{Section 3} 
		score = (float)questions.get("Q81");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.5 * score;
		
		return scaleScore;
	}

	
	/*
	 * Calculate Scale 14 - Difficult Financial Situations Scale (LEI3_FIN)
	 */
	protected static float calcScale14(HashMap<String, Float> questions)
	{
		float scaleScore = (float)0.0;

		//{Section 1}
		float score = (float)questions.get("Q9");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.1 * score;

		score = (float)questions.get("Q14");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.0 * score;

		score = (float)questions.get("Q15");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.6 * score;

		score = (float)questions.get("Q17");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.6 * score;

		score = (float)questions.get("Q22");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.2 * score;

		//{Section 2} 
		scaleScore += (float)1.0 * questions.get("Q36");
		scaleScore += (float)1.8 * questions.get("Q51");
		scaleScore += (float)1.1 * questions.get("Q61");
		scaleScore += (float)2.1 * questions.get("Q62");
		scaleScore += (float)2.2 * questions.get("Q63");
		scaleScore += (float)1.9 * questions.get("Q64");
		scaleScore += (float)1.2 * questions.get("Q65");
		scaleScore += (float)1.6 * questions.get("Q66");
		scaleScore += (float)1.8 * questions.get("Q67");
		scaleScore += (float)1.5 * questions.get("Q72");
		scaleScore += (float)1.4 * questions.get("Q76");

		//{Section 3} 
		score = (float)questions.get("Q81");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.3 * score;

		score = (float)questions.get("Q90");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.1 * score;

		score = (float)questions.get("Q92");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.6 * score;

		score = (float)questions.get("Q94");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.4 * score;

		return scaleScore;
	}

	
	/*
	 * Calculate Scale 15 - Difficult Staffing Situations Scale (LEI3_STF)
	 */
	protected static float calcScale15(HashMap<String, Float> questions)
	{
		float scaleScore = (float)0.0;

		//{Section 1}
		float score = (float)questions.get("Q2");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.1 * score;

		score = (float)questions.get("Q3");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.3 * score;

		score = (float)questions.get("Q4");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.6 * score;

		score = (float)questions.get("Q5");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.8 * score;

		score = (float)questions.get("Q20");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.8 * score;

		score = (float)questions.get("Q21");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.7 * score;

		score = (float)questions.get("Q22");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.3 * score;

		score = (float)questions.get("Q23");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.2 * score;

		//{Section 2} 
		scaleScore +=1.2 * questions.get("Q48");
		scaleScore +=2.0 * questions.get("Q64");
		scaleScore +=1.1 * questions.get("Q68");
		scaleScore +=1.0 * questions.get("Q69");
		scaleScore +=1.4 * questions.get("Q70");
		scaleScore +=1.1 * questions.get("Q74");
		scaleScore +=1.0 * questions.get("Q75");
		scaleScore +=1.8 * questions.get("Q76");

		//{Section 3} 
		score = (float)questions.get("Q81");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.8 * score;

		score = (float)questions.get("Q85");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.8 * score;

		score = (float)questions.get("Q89");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.0 * score;

		score = (float)questions.get("Q95");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.0 * score;

		score = (float)questions.get("Q102");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.6 * score;

		return scaleScore;
	}
	
	
	/*
	 * Calculate Scale 16 - High Risk Situations Scale (LEI3_HR)
	 */
	protected static float calcScale16(HashMap<String, Float> questions)
	{
		float scaleScore = (float)0.0;

		//{Section 1}
		float score = (float)questions.get("Q5");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.0 * score;

		score = (float)questions.get("Q9");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.9 * score;

		score = (float)questions.get("Q13");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.0 * score;

		score = (float)questions.get("Q21");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.0 * score;

		score = (float)questions.get("Q22");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.0 * score;

		score = (float)questions.get("Q24");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.2 * score;

		//{Section 2} 
		scaleScore +=2.4 * questions.get("Q36");
		scaleScore +=1.3 * questions.get("Q38");
		scaleScore +=1.0 * questions.get("Q39");
		scaleScore +=1.4 * questions.get("Q44");
		scaleScore +=1.1 * questions.get("Q45");
		scaleScore +=1.1 * questions.get("Q51");
		scaleScore +=1.7 * questions.get("Q52");
		scaleScore +=1.2 * questions.get("Q62");
		scaleScore +=1.1 * questions.get("Q65");
		scaleScore +=1.2 * questions.get("Q69");
		scaleScore +=1.0 * questions.get("Q70");
		scaleScore +=1.4 * questions.get("Q72");
		scaleScore +=1.4 * questions.get("Q74");
		scaleScore +=1.3 * questions.get("Q75");
		scaleScore +=1.5 * questions.get("Q76");
		scaleScore +=1.0 * questions.get("Q78");

		//{Section 3} 
		score = (float)questions.get("Q88");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.2 * score;

		score = (float)questions.get("Q92");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.0 * score;

		return scaleScore;
	}
	
	
	/*
	 * Calculate Scale 17 - Critical Negotiations Scale (LEI3_NEG)
	 */
	protected static float calcScale17(HashMap<String, Float> questions)
	{
		float scaleScore = (float)0.0;

		//{Section 1}
		float score = (float)questions.get("Q10");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.6 * score;

		score = (float)questions.get("Q11");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.9 * score;

		score = (float)questions.get("Q12");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.1 * score;

		score = (float)questions.get("Q13");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.4 * score;

		score = (float)questions.get("Q21");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.0 * score;

		score = (float)questions.get("Q22");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.0 * score;

		score = (float)questions.get("Q27");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.0 * score;

		//{Section 2} 
		scaleScore += (float)1.0 * questions.get("Q39");
		scaleScore += (float)3.0 * questions.get("Q45");
		scaleScore += (float)2.9 * questions.get("Q46");
		scaleScore += (float)2.5 * questions.get("Q47");
		scaleScore += (float)2.4 * questions.get("Q48");
		scaleScore += (float)2.4 * questions.get("Q49");
		scaleScore += (float)2.6 * questions.get("Q50");
		scaleScore += (float)1.2 * questions.get("Q51");
		scaleScore += (float)1.6 * questions.get("Q52");
		scaleScore += (float)1.3 * questions.get("Q61");
		scaleScore += (float)1.1 * questions.get("Q62");
		scaleScore += (float)2.9 * questions.get("Q66");
		scaleScore += (float)1.0 * questions.get("Q69");
		scaleScore += (float)1.2 * questions.get("Q71");
		scaleScore += (float)2.1 * questions.get("Q72");
		scaleScore += (float)1.8 * questions.get("Q74");
		scaleScore += (float)1.6 * questions.get("Q75");
		scaleScore += (float)1.5 * questions.get("Q76");
		scaleScore += (float)1.9 * questions.get("Q78");

		//{Section 3} 
		score = (float)questions.get("Q90");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.0 * score;

		score = (float)questions.get("Q93");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.3 * score;

		return scaleScore;
	}
	
	
	/*
	 * Calculate Scale 18 - Critical Negotiations Scale (LEI3_CMT)
	 */
	protected static float calcScale18(HashMap<String, Float> questions)
	{
		float scaleScore = (float)0.0;

		//{Section 1}
		float score = (float)questions.get("Q2");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.1 * score;

		score = (float)questions.get("Q4");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.1 * score;

		score = (float)questions.get("Q5");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.8 * score;

		score = (float)questions.get("Q9");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.1 * score;

		score = (float)questions.get("Q12");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.1 * score;

		score = (float)questions.get("Q22");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.2 * score;

		score = (float)questions.get("Q24");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.2 * score;

		//{Section 2} 
		scaleScore += (float)2.8 * questions.get("Q44");
		scaleScore += (float)1.2 * questions.get("Q52");
		scaleScore += (float)1.2 * questions.get("Q62");
		scaleScore += (float)1.0 * questions.get("Q64");
		scaleScore += (float)1.2 * questions.get("Q65");
		scaleScore += (float)1.0 * questions.get("Q76");
		scaleScore += (float)1.1 * questions.get("Q77");
		
		//{Section 3} 
		score = (float)questions.get("Q81");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.0 * score;

		return scaleScore;
	}
	
	
	/*
	 * Calculate Scale 19 - Highly Critical/Visible Assignments or Initiatives Scale (LEI3_VAS)
	 */
	protected static float calcScale19(HashMap<String, Float> questions)
	{
		float scaleScore = (float)0.0;
									
		//{Section 1}
		float score = (float)questions.get("Q1");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.1 * score;

		score = (float)questions.get("Q2");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.1 * score;

		score = (float)questions.get("Q5");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.7 * score;

		score = (float)questions.get("Q9");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.4 * score;

		score = (float)questions.get("Q10");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.1 * score;
		
		score = (float)questions.get("Q11");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.4 * score;

		score = (float)questions.get("Q12");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.0 * score;

		score = (float)questions.get("Q13");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.1 * score;

		score = (float)questions.get("Q22");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.0 * score;

		score = (float)questions.get("Q24");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.2 * score;

		score = (float)questions.get("Q25");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.7 * score;

		score = (float)questions.get("Q27");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.0 * score;

		//{Section 2} 
		scaleScore += (float)1.2 * questions.get("Q35");
		scaleScore += (float)1.9 * questions.get("Q36");
		scaleScore += (float)1.1 * questions.get("Q38");
		scaleScore += (float)1.2 * questions.get("Q39");
		scaleScore += (float)1.0 * questions.get("Q40");
		scaleScore += (float)1.2 * questions.get("Q41");
		scaleScore += (float)1.4 * questions.get("Q43");
		scaleScore += (float)1.8 * questions.get("Q44");
		scaleScore += (float)2.1 * questions.get("Q45");
		scaleScore += (float)1.1 * questions.get("Q48");
		scaleScore += (float)1.4 * questions.get("Q50");
		scaleScore += (float)1.2 * questions.get("Q51");
		scaleScore += (float)1.7 * questions.get("Q52");
		scaleScore += (float)1.0 * questions.get("Q59");
		scaleScore += (float)1.0 * questions.get("Q61");
		scaleScore += (float)1.2 * questions.get("Q62");
		scaleScore += (float)1.0 * questions.get("Q63");
		scaleScore += (float)1.6 * questions.get("Q64");
		scaleScore += (float)1.4 * questions.get("Q65");
		scaleScore += (float)1.2 * questions.get("Q66");
		scaleScore += (float)1.0 * questions.get("Q68");
		scaleScore += (float)1.3 * questions.get("Q69");
		scaleScore += (float)1.0 * questions.get("Q70");
		scaleScore += (float)1.1 * questions.get("Q71");
		scaleScore += (float)1.6 * questions.get("Q72");
		scaleScore += (float)1.7 * questions.get("Q74");
		scaleScore += (float)1.7 * questions.get("Q75");
		scaleScore += (float)2.0 * questions.get("Q76");
		scaleScore += (float)1.7 * questions.get("Q77");
		scaleScore += (float)1.8 * questions.get("Q78");
		
		//{Section 3} 
		score = (float)questions.get("Q81");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.2 * score;

		score = (float)questions.get("Q88");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.4 * score;

		score = (float)questions.get("Q89");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.1 * score;

		score = (float)questions.get("Q92");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.0 * score;

		score = (float)questions.get("Q94");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.2 * score;

		return scaleScore;
	}
	
	
	/*
	 * Calculate Scale 20 - Highly Critical/Visible Assignments or Initiatives Scale ()
	 */
	protected static float calcScale20(HashMap<String, Float> questions)
	{
		float scaleScore = (float)0.0;

		//{Section 1}
		float score = (float)questions.get("Q6");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.6 * score;

		score = (float)questions.get("Q8");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.1 * score;

		score = (float)questions.get("Q28");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.5 * score;

		score = (float)questions.get("Q29");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.6 * score;

		score = (float)questions.get("Q30");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.5 * score;

		score = (float)questions.get("Q31");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.6 * score;

		score = (float)questions.get("Q33");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.5 * score;

		//{No Section 2 items}

		//{Section 3}
		score = (float)questions.get("Q82");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.1 * score;

		score = (float)questions.get("Q98");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)3.0 * score;

		score = (float)questions.get("Q99");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.9 * score;

		score = (float)questions.get("Q100");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.6 * score;

		score = (float)questions.get("Q105");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.0 * score;

		return scaleScore;
	}
	
	
	/*
	 * Calculate Scale 21 - Development of Others Scale (LEI3_DEV)
	 */
	protected static float calcScale21(HashMap<String, Float> questions)
	{
		float scaleScore = (float)0.0;

		//{Section 1}
		float score = (float)questions.get("Q20");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.7 * score;

		score = (float)questions.get("Q22");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.0 * score;

		score = (float)questions.get("Q23");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.6 * score;

		score = (float)questions.get("Q32");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.6 * score;

		score = (float)questions.get("Q33");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.3 * score;

		//{No Section 2 items} 

		//{Section 3} 
		score = (float)questions.get("Q89");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.0 * score;

		score = (float)questions.get("Q95");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)3.0 * score;

		score = (float)questions.get("Q96");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)3.0 * score;

		score = (float)questions.get("Q97");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.8 * score;

		score = (float)questions.get("Q102");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.4 * score;

		return scaleScore;
	}

	
	/*
	 * Calculate Scale 22 - International/Cross-Cultural Scale (LEI3_INN)
	 */
	protected static float calcScale22(HashMap<String, Float> questions)
	{
		float scaleScore = (float)0.0;

		//{Section 1}
		float score = (float)questions.get("Q8");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.6 * score;

		//{Section 2}
		scaleScore += (float)1.9 * questions.get("Q47");
		scaleScore += (float)2.2 * questions.get("Q70");
		scaleScore += (float)2.7 * questions.get("Q75");
		
		//{Section 3} 
		score = (float)questions.get("Q82");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)3.0 * score;

		score = (float)questions.get("Q83");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)3.0 * score;

		score = (float)questions.get("Q84");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.5 * score;

		score = (float)questions.get("Q103");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)3.0 * score;

		score = (float)questions.get("Q105");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.3 * score;

		return scaleScore;
	}


	/*
	 * Calculate Scale 23 - Extracurricular Activities Scale (LEI3_EXT)
	 */
	protected static float calcScale23(HashMap<String, Float> questions)
	{
		float scaleScore = (float)0.0;

		//{Section 1}
		float score = (float)questions.get("Q28");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.4 * score;

		score = (float)questions.get("Q29");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.1 * score;

		score = (float)questions.get("Q30");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.5 * score;

		score = (float)questions.get("Q31");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.6 * score;

		score = (float)questions.get("Q32");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.1 * score;

		score = (float)questions.get("Q33");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.4 * score;

		//{Section 2} 
		scaleScore += (float)3.0 * questions.get("Q79");
		scaleScore += (float)2.4 * questions.get("Q80");
		
		//{Section 3} 
		score = (float)questions.get("Q94");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.0 * score;

		score = (float)questions.get("Q104");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)2.7 * score;

		score = (float)questions.get("Q105");		
		if (score < 1.0 || score > 4.0)  score = (float)1.0;
		scaleScore += (float)1.2 * score;

		return scaleScore;
	}
	
	
	/*
	 * 
	 */
	//@SuppressWarnings("unchecked")
	public static IndividualReport addRptDevSugList(IndividualReport ir)
	{
		
		ReportData rd = ir.getReportData().get(ReportingConstants.RC_LEI);
		if (rd == null)
		{
			// Should not happen
			return ir;
		}
		
		// Get the lowest 7 scale gp percentiles
		ArrayList<ScaleScore> ss = new ArrayList<ScaleScore>();

		// Get all of the scales & scores...
		for (String scl : LEI_SCALES)
		{
			String key = scl + "_GP_PCTL";
			String pctlStr = rd.getScriptedData().get(key);
			if (pctlStr == null)
			{
				// If there is no percentile, skip it
				continue;
			}
			ss.add(new ScaleScore(new Double(pctlStr), scl));
		}
		// ...and sort ascending
		Collections.sort(ss, SCORE_ORDER);
		
		// Report them in the IR
		for (int i = 0; i < 7; i++)
		{
			if (i >= ss.size())
			{
				//Done (shouldn't happen)
				break;
			}
			
			rd.getScriptedData().put("DS_" + (i + 1), ss.get(i).getSpss());
			//System.out.println("DS_"+ (i + 1) + "=" + ss.get(i).getSpss());
		}

		// return the updated IR
		return ir;
	}

	private static final Comparator<ScaleScore> SCORE_ORDER = new Comparator()
	{
		public int compare(Object o1, Object o2)
		{
			ScaleScore scale1 = (ScaleScore)o1;
			ScaleScore scale2 = (ScaleScore)o2;
			
			if ( scale1.getPctl() == scale2.getPctl() )
			{
				// If the same value, alphabetize
				return ( scale1.getSpss().toString().compareTo(scale2.getSpss().toString()) );
			}

			return (scale1.getPctl().compareTo(scale2.getPctl()));
		}
	};
	
	private static class ScaleScore
	{
		Double _pctl;
		String _spss;
		
		public ScaleScore(Double pctl, String key)
		{
			//_pctl = pctl;
			//_spss = key;
			setPctl(pctl);
			setSpss(key);
		}
		
		public void setPctl(Double value)
		{
			_pctl = value;
		}
		
		public Double getPctl()
		{
			return _pctl;
		}
		
		// Always set by the constuctor
		public void setSpss(String value)
		{
			_spss = value;
		}
		
		public String getSpss()
		{
			return _spss;
		}
	}
}
