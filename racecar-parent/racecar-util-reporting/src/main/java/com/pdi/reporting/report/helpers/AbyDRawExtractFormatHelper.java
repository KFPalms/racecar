/**
 * Copyright (c) 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.reporting.report.helpers;

import java.util.ArrayList;
import java.util.HashMap;

import com.pdi.reporting.report.ExtractItem;
import com.pdi.reporting.report.GroupReport;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;
import com.pdi.xml.XMLUtils;

/*
 * AbyDRawExtractFormatHelper provides a repository for methods used by the
 * ABYD_RAW_EXTRACT.groovy scripts in the util-reporting (data formatting)
 * environment.
 *
 * The code was removed from the .groovy class because there appears to be
 * a limit on the size of a .groovy class and this code was approaching
 * that limit.
 *
 * This functionality was not combined with the extract helper because that
 * helper is inaccessible to reporting.
 */
public class AbyDRawExtractFormatHelper
{
	//
	//  Static data
	//

	//
	// Statics used in extract output formatting
	//

	// Constants for the ExtractItem data source
	private static int GRP = 00;	// GroupReport
	private static int INDV = 10;	// IndividualReport
	private static int RDD = 20;	// ReportData in IR (Display stuff)
	private static int RDS = 21;	// ReportData in IR (Score stuff)
	private static int HDG = -1;	// Heading

	// Instrument names.  These must exactly duplicate the ones in ReportingConstants.RC_xx
	// Can't use those definitions in a static context
	private static String RC_GPI = "GPI";			// RC_GPI = "GPI"
	private static String RC_LEI = "LEI";			// RC_LEI = "LEI"
	private static String RC_CS = "CAREER_SURVEY";	// RC_CAREER_SURVEY = "CAREER_SURVEY"
	private static String RC_WGE = "WG_E";			// RC_WG_E = "WG_E"
	private static String RC_RAVB = "RAVENS_B";		// RC_RAVENS_B = "RAVENS_B"
	private static String RC_FIN_EX = "FINEX";		// RC_FIN_EX = "FINEX";

	// these are from the Reporting Constants after the tlt migration...
	// so am adding them to the existing list...
	// probably , should remove dups.
	//public static String RC_RAVENS_SF = "RAVENS_SF";
	//public static String RC_CAREER_SURVEY = "CAREER_SURVEY";
	//public static String RC_WG_E = "WG_E";

	//  Instrument List
	private static ArrayList<String> INST_LIST = new ArrayList<String>();
	static
	{
		INST_LIST.add(RC_GPI);
		INST_LIST.add(RC_LEI);
		INST_LIST.add(RC_CS);
		INST_LIST.add(RC_WGE);
		INST_LIST.add(RC_RAVB);
		INST_LIST.add(RC_FIN_EX);
		//INST_LIST.add(RC_RAVENS_SF);
		//INST_LIST.add(RC_CAREER_SURVEY);
		//INST_LIST.add(RC_WG_E);

	}

	// Instrument Names
	// Other instrument names - not presently used in extract anyway
	// If they do eventually get used, ensure that the names conform to Raj's spec
	//	"Raven's APM Short Form"
	//	"Watson-Glaser A - Short Form"
	//	"Personnel Classification Test - Alexander G. Wesman (Wesman)");
	//	"Raven's Timed - Long Form"
	//	"Watson-Glaser A - Long Form"
	//	"Watson-Glaser C"
	private static HashMap<String, String> INST_NAMES = new HashMap<String, String>();
	static
	{
		INST_NAMES.put(RC_GPI,	"Global Personality Inventory");
		INST_NAMES.put(RC_LEI,	"Leadership Experience Inventory");
		INST_NAMES.put(RC_CS,	"Career Survey");
		INST_NAMES.put(RC_WGE,	"Watson-Glaser II Critical Thinking Appraisal - Form E");
		INST_NAMES.put(RC_RAVB,	"Raven's Advanced Progressive Matrices - Form B");
		INST_NAMES.put(RC_FIN_EX, "Financial Exercise");
	}

    static public HashMap<String, String> INST_CODE_TO_RC_NAME = new HashMap<String, String>();
	static
	{
		INST_CODE_TO_RC_NAME.put("gpi", RC_GPI);
		INST_CODE_TO_RC_NAME.put("gpil", RC_GPI);
		INST_CODE_TO_RC_NAME.put("lei", RC_LEI);
		INST_CODE_TO_RC_NAME.put("cs", RC_CS);
		INST_CODE_TO_RC_NAME.put("cs2", RC_CS);
		INST_CODE_TO_RC_NAME.put("wg", RC_WGE);
		INST_CODE_TO_RC_NAME.put("rv", RC_RAVB);
		INST_CODE_TO_RC_NAME.put("finex", RC_FIN_EX);
	}

	// GPI Scales in order by SPSS label
	static public ArrayList<String> GPI_SCALE_ORDER = new ArrayList<String>();
	static
	{
		GPI_SCALE_ORDER.add("GPI_TA");	// Thought Agility
		GPI_SCALE_ORDER.add("GPI_INOV");	// Innovation/Creativity
		GPI_SCALE_ORDER.add("GPI_TF");		// Thought Focus
		GPI_SCALE_ORDER.add("GPI_VIS");		// Vision
		GPI_SCALE_ORDER.add("GPI_AD");		// Attention to Detail
		GPI_SCALE_ORDER.add("GPI_WF");		// Work Focus
		GPI_SCALE_ORDER.add("GPI_TC");		// Taking Charge
		GPI_SCALE_ORDER.add("GPI_INFL");	// Influence
		GPI_SCALE_ORDER.add("GPI_EGO");		// Ego-centered
		GPI_SCALE_ORDER.add("GPI_MAN");		// Manipulation
		GPI_SCALE_ORDER.add("GPI_MIC");		// Micro-managing
		GPI_SCALE_ORDER.add("GPI_INTI");	// Intimidating
		GPI_SCALE_ORDER.add("GPI_PA");		// Passive-aggressive
		GPI_SCALE_ORDER.add("GPI_SO");		// Sociability
		GPI_SCALE_ORDER.add("GPI_CONS");	// Consideration
		GPI_SCALE_ORDER.add("GPI_EMP");		// Empathy
		GPI_SCALE_ORDER.add("GPI_TR");		// Trust
		GPI_SCALE_ORDER.add("GPI_AST");		// Social Astuteness
		GPI_SCALE_ORDER.add("GPI_EL");		// Energy Level
		GPI_SCALE_ORDER.add("GPI_INIT");	// Initiative
		GPI_SCALE_ORDER.add("GPI_DACH");	// Desire for Achievement
		GPI_SCALE_ORDER.add("GPI_ADPT");	// Adaptability
		GPI_SCALE_ORDER.add("GPI_OPEN");	// Openness
		GPI_SCALE_ORDER.add("GPI_NA");		// Negative Affectivity
		GPI_SCALE_ORDER.add("GPI_OPT");		// Optimism
		GPI_SCALE_ORDER.add("GPI_EC");		// Emotional Control
		GPI_SCALE_ORDER.add("GPI_ST");		// Stress Tolerance
		GPI_SCALE_ORDER.add("GPI_SC");		// Self-confidence
		GPI_SCALE_ORDER.add("GPI_IMPR");	// Impressing
		GPI_SCALE_ORDER.add("GPI_SASI");	// Self-awareness/Insight
		GPI_SCALE_ORDER.add("GPI_IND");		// Independence
		GPI_SCALE_ORDER.add("GPI_COMP");	// Competitive
		GPI_SCALE_ORDER.add("GPI_RISK");	// Risk-taking
		GPI_SCALE_ORDER.add("GPI_DADV");	// Desire for Advancement
		GPI_SCALE_ORDER.add("GPI_INTD");	// Interdependence
		GPI_SCALE_ORDER.add("GPI_DUT");		// Dutifulness
		GPI_SCALE_ORDER.add("GPI_RESP");	// Responsibility
	}


	// LEI Scales in order by SPSS label
	static public ArrayList<String> LEI_SCALE_ORDER = new ArrayList<String>();
	static
	{
		LEI_SCALE_ORDER.add("LEI3_OAL");		// Overall
		LEI_SCALE_ORDER.add("LEI3_GMT");		// Super-Scale - General Management Experiences
		LEI_SCALE_ORDER.add("LEI3_CHA");		// Super-Scale - Adversity
		LEI_SCALE_ORDER.add("LEI3_RC");			// Super-Scale - Risky/Critical Experiences
		LEI_SCALE_ORDER.add("LEI3_PCR");		// Super-Scale - Personal and Career Related Experiences
		LEI_SCALE_ORDER.add("LEI3_SGY");		// Scale 01 - Strategy Development
		LEI_SCALE_ORDER.add("LEI3_PGT");		// Scale 02 - Project Management and Implementation
		LEI_SCALE_ORDER.add("LEI3_BDV");		// Scale 03 - Business Development and Marketing
		LEI_SCALE_ORDER.add("LEI3_BGR");		// Scale 04 - Business Growth
		LEI_SCALE_ORDER.add("LEI3_PDT");		// Scale 05 - Product Development
		LEI_SCALE_ORDER.add("LEI3_STA");		// Scale 06 - Start-up Business
		LEI_SCALE_ORDER.add("LEI3_FM");			// Scale 07 - Financial Management
		LEI_SCALE_ORDER.add("LEI3_OPX");		// Scale 08 - Operations
		LEI_SCALE_ORDER.add("LEI3_FEX");		// Scale 09 - Support Functions
		LEI_SCALE_ORDER.add("LEI3_RPR");		// Scale 10 - External Relations
		LEI_SCALE_ORDER.add("LEI3_PRB");		// Scale 11 - Inherited Problems and Challenges
		LEI_SCALE_ORDER.add("LEI3_PPL");		// Scale 12 - Interpersonally Challenging Situations
		LEI_SCALE_ORDER.add("LEI3_FAL");		// Scale 13 - Downturn and/or Failures
		LEI_SCALE_ORDER.add("LEI3_FIN");		// Scale 14 - Difficult Financial Situations
		LEI_SCALE_ORDER.add("LEI3_STF");		// Scale 15 - Difficult Staffing Situations
		LEI_SCALE_ORDER.add("LEI3_HR");			// Scale 16 - High Risk Situations
		LEI_SCALE_ORDER.add("LEI3_NEG");		// Scale 17 - Critical Negotiations
		LEI_SCALE_ORDER.add("LEI3_CMT");		// Scale 18 - Crisis Management
		LEI_SCALE_ORDER.add("LEI3_VAS");		// Scale 19 - Highly Critical/Visible Assignments
		LEI_SCALE_ORDER.add("LEI3_SDV");		// Scale 20 - Self-Development
		LEI_SCALE_ORDER.add("LEI3_DEV");		// Scale 21 - Development of Others
		LEI_SCALE_ORDER.add("LEI3_INN");		// Scale 22 - International/Cross-Cultural
		LEI_SCALE_ORDER.add("LEI3_EXT");		// Scale 23 - Extracurricular Activities
	}


	// Career Survey Scales in order by SPSS label
	static public ArrayList<String> CS_SCALE_ORDER = new ArrayList<String>();
	static
	{
		CS_SCALE_ORDER.add("CAREER_GOALS");
		CS_SCALE_ORDER.add("CAREER_DRIVERS");
		CS_SCALE_ORDER.add("LEARNING_ORIENT");
		CS_SCALE_ORDER.add("EXPERIENCE_ORIENT");
	}


	// Watson-Glaser E Scales in order by SPSS label
	static public ArrayList<String> WGE_SCALE_ORDER = new ArrayList<String>();
	static
	{
		// There are 3 more scales (WGE_RA, WGE_EA, and WGE_DC), but we don't
		// use them at present
		WGE_SCALE_ORDER.add("WGE_CT");
	}


	// Ravens B Scales in order by SPSS label
	static public ArrayList<String> RAVB_SCALE_ORDER = new ArrayList<String>();
	static
	{
		RAVB_SCALE_ORDER.add("RAVENSB");
	}

	// FinEx Scales in order by SPSS label
	static public ArrayList<String> FINEX_SCALE_ORDER = new ArrayList<String>();
	static
	{
		FINEX_SCALE_ORDER.add("FE_MPD");
		FINEX_SCALE_ORDER.add("FE_AFC");
		FINEX_SCALE_ORDER.add("FE_DAM");
		FINEX_SCALE_ORDER.add("FE_DAC");
	}

	//
	// Static methods.
	//


	//
	// These static methods are used by the groovy script in reporting.
	// They are used to format the output data
	//

	/**
	 * Set up the output data order based upon the data previously gathered.
	 *
	 * Note that the order has changed from the original spec.  Order is now
	 *  per the email from Vidula dated 8/17/2010
	 *
	 * @throws Exception
	 */
	public static ArrayList<ExtractItem> setUpOrder(GroupReport gr)
	{
		ArrayList<ExtractItem> eItems = new ArrayList<ExtractItem>();

		// Everybody in the same extract should have the same Eval Guide and
		// Integration Grid layout.  The data for them, however, is (redundantly)
		// in the Individual Report display data.  Pick the first person's
		// IR object and get IR for EG and IG counts.

		// NOTE:  This logic assumes that all of the participants are in the same
		//		  project/DNA.  Stuff with GRP tags is picked up from the GroupReport
		//		  object and so the client project and DNA info has to be the same
		//		  for all participants.  If the users ever want to pick from multiple
		//		  projects, the above named info will have to be collected into the
		//		  IndividualReport objects (will be redundant for people of the same DNA).
		IndividualReport ir = gr.getIndividualReports().get(0);

		// Section 1 - General identification
		eItems.add(new ExtractItem("ParticipantId", "Candidate ID", INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("ParticipantFname", "Candidate First Name", INDV));
		eItems.add(new ExtractItem("ParticipantLname", "Candidate Last Name", INDV));
		eItems.add(new ExtractItem("ClientName", "Client Name", GRP));
		eItems.add(new ExtractItem("ProjectName", "Project Name", GRP));	// Actually, DNA name
		eItems.add(new ExtractItem("Model", "Assessment Model", GRP));
		// Section 2 - Additional DNA items
		eItems.add(new ExtractItem("dlciMean", "Synthetic Norm Mean", GRP));
		eItems.add(new ExtractItem("dlciStdDev", "Synthetic Norm Standard Deviation", GRP));
		eItems.add(new ExtractItem("DnaNotes", "DNA Notes", GRP));

		// Instrument scores
		//System.out.println("INST_LIST class=" + INST_LIST.getClass().getName());
		//System.out.println("INST_LIST size=" + INST_LIST.size());
		for (String key : INST_LIST)
		{
			//System.out.println("Instrument=" + key);
			eItems.add(new ExtractItem("", INST_NAMES.get(key) + " Instrument Data:", HDG));

			ArrayList<String> order;
			String prefix;
			if(key.equals(RC_GPI))
			{
				order = GPI_SCALE_ORDER;
				prefix = "GPI";
			}
			else if(key.equals(RC_LEI))
			{
				order = LEI_SCALE_ORDER;
				prefix = "LEI";
			}
			else if(key.equals(RC_CS))
			{
				order = CS_SCALE_ORDER;
				prefix = "CAR";
			}
			else if(key.equals(RC_WGE))
			{
				order = WGE_SCALE_ORDER;
				prefix = "WGE_CT";
			}
			else if(key.equals(RC_RAVB))
			{
				order = RAVB_SCALE_ORDER;
				prefix = "RAV";
			}
			else if(key.equals(RC_FIN_EX))
			{
				order = FINEX_SCALE_ORDER;
				prefix = "FINEX";
			}
			else
			{
				order = new ArrayList<String>();	// So it won' t blow up
				prefix = "";
			}

			// Put out the Norms
			if (! key.equals(RC_FIN_EX))
			{
				if(key.equals(RC_RAVB)){
					//System.out.println("RAVENSB" + "_GP_NAME   || " +  key + "  || General Population Norm Name");
					eItems.add(new ExtractItem("RAVENSB" + "_GP_NAME", key, "General Population Norm Name", RDS));
				}else{
					eItems.add(new ExtractItem(prefix + "_GP_NAME", key, "General Population Norm Name", RDS));
				}
			}
			if (! key.equals(RC_CS) && ! key.equals(RC_FIN_EX))
			{
				if(key.equals(RC_RAVB)){
					//System.out.println("RAVENSB" + "_GP_NAME  || " +  key + "  ||  General Population Norm Name");
					eItems.add(new ExtractItem("RAVENSB" + "_SP_NAME", key, "Special Population Norm Name", RDS));
				}else{
					eItems.add(new ExtractItem(prefix + "_SP_NAME",key, "Special Population Norm Name", RDS));
				}
			}


			// Put out the scale score info
			for (String scale : order)
			{
				eItems.add(new ExtractItem(scale + "_SCALE_NAME", key, scale + " Display Name", RDD));
				eItems.add(new ExtractItem(scale, key, scale + " Raw Score", RDS, ExtractItem.DATA_FLOAT));
				// FINEX doesn't have a norm, so the GP & SP rating should probably be screened out...
				eItems.add(new ExtractItem(scale + "_GP_RATING", key, scale + " Gen Pop Rating", RDS, ExtractItem.DATA_FLOAT));
				if (! key.equals(RC_CS))
				{
					eItems.add(new ExtractItem(scale + "_SP_RATING", key, scale + " Spec Pop Rating", RDS, ExtractItem.DATA_FLOAT));
				}
			}
			if(key.equals(RC_GPI))
			{
				eItems.add(new ExtractItem("PGPI_RDI", key, "GPI RDI", RDS, ExtractItem.DATA_FLOAT));
			}
		}

		// TLT scores
		eItems.add(new ExtractItem("", "TLT Scores:", HDG));
		//Interest stanine scores
		eItems.add(new ExtractItem("TLTLeadershipAspiration", "\"Leadership Aspiration\" Stanine", INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("TLTCareerDrivers", "\"Career Drivers\" Stanine", INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("TLTLearningOrientation", "\"Learning Orientation\" Stanine", INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("TLTExperienceOrientation", "\"Experience Orientation\" Stanine", INDV, ExtractItem.DATA_INT));
		// Experience stanine scores
		eItems.add(new ExtractItem("TLTBusinessOperations", "\"Business Operations\" Stanine", INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("TLTHandlingToughChallenges", "\"Handling Tough Challenges\" Stanine", INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("TLTHighVisibility", "\"High Visibility\" Stanine", INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("TLTGrowingTheBusiness", "\"Growing the Business\" Stanine", INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("TLTPersonalDevelopment", "\"Personal Development\" Stanine", INDV, ExtractItem.DATA_INT));
		// Foundations
		eItems.add(new ExtractItem("TLTProblemSolving", "\"Problem Solving\" Stanine", INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("TLTIntellectualEngagement", "\"Intellectual Engagement\" Stanine", INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("TLTAttentionToDetail", "\"Attention to Detail\" Stanine", INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("TLTImpactInfluence", "\"Impact/Influence\" Stanine", INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("TLTInterpersonalEngagement", "\"Interpersonal Engagement\" Stanine", INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("TLTAchievementDrive", "\"Achievement Drive\" Stanine", INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("TLTAdvancementDrive", "\"Advancement Drive\" Stanine", INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("TLTCollectiveOrientation", "\"Collective Orientation\" Stanine", INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("TLTFlexibilityAdaptability", "\"Flexibility/Adaptability\" Stanine", INDV, ExtractItem.DATA_INT));
		// Overall scores
		eItems.add(new ExtractItem("TLTLeadershipInterest", "\"Leadership Interest\"", INDV));
		eItems.add(new ExtractItem("TLTLeadershipExperience", "\"Leadership Experience\"", INDV));
		eItems.add(new ExtractItem("TLTLeadershipFoundations", "\"Leadership Foundations\"", INDV));
		// Derailment
		eItems.add(new ExtractItem("TLTDerailmentRisk", "\"Derailment Risk\"", INDV));

		// DRI data
		eItems.add(new ExtractItem("", "Dashboard Report Data:", HDG));
		// Consultant Ratings
		eItems.add(new ExtractItem("DRILeadSkillRating", "Consultant Leadership Competencies Rating", INDV));
		eItems.add(new ExtractItem("DRILeadExperRating", "Consultant Leadership Experience Rating", INDV));
		eItems.add(new ExtractItem("DRILeadStyleRating", "Consultant Leadership Style Rating", INDV));
		eItems.add(new ExtractItem("DRILeadIntrstRating", "Consultant Leadership Interest Rating", INDV));
		eItems.add(new ExtractItem("DRIDerailmentRating", "Consultant Derailment Rating", INDV));
		eItems.add(new ExtractItem("DRILongTermRating", "Consultant Long Term Potential Rating", INDV));
		// Fit
		eItems.add(new ExtractItem("DRIFitIndex", "Fit Index", INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("DRIFitRating", "Fit Recommendation", INDV));
		// Readiness
		eItems.add(new ExtractItem("DRIReadinessRating", "Readiness Recomendation", INDV));
		eItems.add(new ExtractItem("DRIDevelopmentFocus", "Development Focus Statement", INDV));
		// Consultant Entered Text
		eItems.add(new ExtractItem("DRISkillsToLeverageOrgText", "\"Skills to Leverage\" Text (Organization Facing)", INDV));
		eItems.add(new ExtractItem("DRISkillsToLeveragePartText", "\"Skills to Leverage\" Text (Participant Facing)", INDV));
		eItems.add(new ExtractItem("DRISkillsToDevelopOrgText", "\"Skills to Develop\" Text (Organization Facing)", INDV));
		eItems.add(new ExtractItem("DRISkillsToDevelopPartText", "\"Skills to Develop\" Text (Participant Facing)", INDV));
		eItems.add(new ExtractItem("DRILeadershipSkillOrgText", "\"Leadership Competency\" Text (Organization Facing)", INDV));
		eItems.add(new ExtractItem("DRILeadershipSkillPartText", "\"Leadership Competency\" Text (Participant Facing)", INDV));
		eItems.add(new ExtractItem("DRILeadershipExpOrgText", "\"Leadership Experience\" Text (Organization Facing)", INDV));
		eItems.add(new ExtractItem("DRILeadershipExpPartText", "\"Leadership Experience\" Text (Participant Facing)", INDV));
		eItems.add(new ExtractItem("DRILeadershipStyleOrgText", "\"Leadership Style\" Text (Organization Facing)", INDV));
		eItems.add(new ExtractItem("DRILeadershipStylePartText", "\"Leadership Style\" Text (Participant Facing)", INDV));
		eItems.add(new ExtractItem("DRILeadershipInterestOrgText", "\"Leadership Interest\" Text (Organization Facing)", INDV));
		eItems.add(new ExtractItem("DRILeadershipInterestPartText", "\"Leadership Interest\" Text (Participant Facing)", INDV));
		eItems.add(new ExtractItem("DRIDerailmentOrgText", "\"Derailment Risk\" Text (Organization Facing)", INDV));
		eItems.add(new ExtractItem("DRIDerailmentPartText", "\"Derailment Risk\" Text (Participant Facing)", INDV));
		eItems.add(new ExtractItem("DRILongTermPotentialOrgText", "\"Long-term Advancement Potential\" Text (Organization Facing)", INDV));
		eItems.add(new ExtractItem("DRIFitOrgText", "\"Fit Summary\" Text (Organization Facing)", INDV));
		eItems.add(new ExtractItem("DRIReadinessOrgText", "\"Readiness Summary\" Text (Organization Facing)", INDV));
		eItems.add(new ExtractItem("DRIDevelopmentPartText", "\"Development Summary\" Text (Participant Facing)", INDV));
		eItems.add(new ExtractItem("DRIPivotalPartText", "\"Pivotal Opportunities\" Text (Participant Facing)", INDV));
		eItems.add(new ExtractItem("Rpt_Note", "Report Input Note", GRP));

		// Client competency (Report Model) data
		eItems.add(new ExtractItem("", "Client Competencies:", HDG));
		// Build the structure tags using the counts
		if (gr.getDisplayData("CC_SF_Count") == null)
		{
			System.out.println("No Client Competency data available");
		}
		else
		{
			int sfCnt = Integer.parseInt(gr.getDisplayData("CC_SF_Count"));

			for (int i=1; i <= sfCnt; i++)
			{
				eItems.add(new ExtractItem("CC_SF" + i + "_Name", "Superfactor " + i + " Name", GRP));
				int compCnt = Integer.parseInt(gr.getDisplayData("CC_SF" + i + "_Comp_Count"));
				for (int j=1; j <= compCnt; j++)
				{
					eItems.add(new ExtractItem("CC_SF" + i + "_Comp" + j + "_Name", "Superfactor " + i + " Competency " + j + " Name", GRP));
					eItems.add(new ExtractItem("CC_SF" + i + "_Comp" + j + "_Essential", "Superfactor " + i + " Competency " + j + " isEssential", GRP));
					eItems.add(new ExtractItem("CC_SF" + i + "_Comp" + j + "_Score", "Superfactor " + i + ", Competency " + j + " Score", INDV, ExtractItem.DATA_FLOAT));

					int childCnt = Integer.parseInt(gr.getDisplayData("CC_SF" + i + "_Comp" + j + "_Child_Count"));
					for (int k=1; k <= childCnt; k++)
					{
						eItems.add(new ExtractItem("CC_SF" + i + "_Comp" + j + "_Child" + k + "_Dna_Name", "Superfactor " + i + ", Competency " + j + ", Child " + k + " DNA Name", GRP));
						eItems.add(new ExtractItem("CC_SF" + i + "_Comp" + j + "_Child" + k + "_Dna_Essential", "Superfactor " + i + ", Competency " + j + ", Child " + k + " DNA isEssential", GRP));
					}
				}
			}
		}

		// Integration Grid data
		eItems.add(new ExtractItem("", "Integration Grid Data:", HDG));
		// Get the IG competency data...
		if (ir.getDisplayData("IG_Comp_Count") == null)
		{
			System.out.println("No IG data available");
		}
		else
		{
			int igCnt = Integer.parseInt(ir.getDisplayData("IG_Comp_Count"));
			for (int i=1; i <= igCnt; i++)
			{
				eItems.add(new ExtractItem("IG_Comp" + i + "_Name", "Competency " + i + " Name", INDV));
				eItems.add(new ExtractItem("IG_Comp" + i + "_CalcScore", "Competency " + i + " Calculated Score", INDV, ExtractItem.DATA_FLOAT));
				eItems.add(new ExtractItem("IG_Comp" + i + "_FinalScore", "Competency " + i + " Consultant Entered Score", INDV, ExtractItem.DATA_FLOAT));
				eItems.add(new ExtractItem("IG_Comp" + i + "_Note", "Competency " + i + " Note", INDV));
			}
			// ... And the IG overall data
			eItems.add(new ExtractItem("IG_TotalScore", "Integration Grid Total Score", INDV, ExtractItem.DATA_FLOAT));
			eItems.add(new ExtractItem("IG_dLCI", "Integration Grid dLCI", INDV, ExtractItem.DATA_INT));
			eItems.add(new ExtractItem("IG_WorkingNotes", "Integration Grid Working Notes", INDV));
		}

		// Eval Guide data
		eItems.add(new ExtractItem("", "Eval. Guide Data:", HDG));
		// Get the EG count and loop through it
		if (ir.getDisplayData("EG_Count") == null)
		{
			System.out.println("No EG data available");
		}
		else
		{
			int egCnt = Integer.parseInt(ir.getDisplayData("EG_Count"));
			for (int i=1; i <= egCnt; i++)
			{
				// Put out the name
				eItems.add(new ExtractItem("EG" + i + "_Name", "Eval. Guide " + i + " Name", INDV));
				// Put out the competency data
				int egCompCnt = Integer.parseInt(ir.getDisplayData("EG" + i + "_Comp_Count"));
				for(int j=1; j <= egCompCnt; j++)
				{
					eItems.add(new ExtractItem("EG" + i + "_Comp" + j + "_Name", "Eval. Guide " + i + " Competency " + j + " Name", INDV));
					eItems.add(new ExtractItem("EG" + i + "_Comp" + j + "_Score", "Eval. Guide " + i + " Competency " + j + " Score", INDV, ExtractItem.DATA_FLOAT));
					eItems.add(new ExtractItem("EG" + i + "_Comp" + j + "_Notes", "Eval. Guide " + i + " Competency " + j + " Notes", INDV));
				}
				// Put out the summary data and text
				eItems.add(new ExtractItem("EG" + i + "_OPR", "Eval. Guide " + i + " Overall Performance", INDV, ExtractItem.DATA_INT));
				eItems.add(new ExtractItem("EG" + i + "_BriefDesc", "Eval. Guide " + i + " Overview", INDV));
				eItems.add(new ExtractItem("EG" + i + "_KeyStrengths", "Eval. Guide " + i + " Strengths", INDV));
				eItems.add(new ExtractItem("EG" + i + "_KeyNeeds", "Eval. Guide " + i + " Development Needs", INDV));
				eItems.add(new ExtractItem("EG" + i + "_themesFromPeers", "Eval. Guide " + i + " Themes From Peers", INDV));
				eItems.add(new ExtractItem("EG" + i + "_themesFromDirectReports", "Eval. Guide " + i + " Themes From Direct Reports", INDV));
				eItems.add(new ExtractItem("EG" + i + "_themesFromBoss", "Eval. Guide " + i + " Themes From Boss", INDV));
				eItems.add(new ExtractItem("EG" + i + "_CoachNotes", "Eval. Guide " + i + " Real-time Coaching Notes", INDV));
			}
		}

		// IG test column data, per NHN-2680
		eItems.add(new ExtractItem("", "IG Testing Competency Score Data:", HDG));

		int igColumnCount = Integer.parseInt(ir.getDisplayData("IGTestColCnt"));
		for(int k=1; k <= igColumnCount; k++){
			eItems.add(new ExtractItem("IGTestCol_CompName_" + k, "IGTestCol_CompName_" + k, INDV));
			eItems.add(new ExtractItem("IGTestCol_Score_" + k, "IGTestCol_Score_" + k, INDV, ExtractItem.DATA_FLOAT));
		}


		// Numerical values for mapped strings found previously
		eItems.add(new ExtractItem("", "Numeric Mappings:", HDG));
		eItems.add(new ExtractItem("DRILeadSkillRatingNumber", "Consultant Leadership Competencies Rating Number", INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("DRILeadExperRatingNumber", "Consultant Leadership Experience Rating Number", INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("DRILeadStyleRatingNumber", "Consultant Leadership Style Rating Number", INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("DRILeadIntrstRatingNumber", "Consultant Leadership Interest Rating Number", INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("DRIDerailmentRatingNumber", "Consultant Derailment Rating Number", INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("DRILongTermRatingNumber", "Consultant Long Term Potential Rating Number", INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("DRIReadinessRatingNumber", "Readiness Recommendation Number", INDV, ExtractItem.DATA_INT));
		eItems.add(new ExtractItem("TLTDerailmentRiskNumber", "\"Derailment Risk\" Number", INDV, ExtractItem.DATA_INT));

		return eItems;
	}


	/**
	 * DEPRECATED AS OF JAN 2013 - REPLACED WITH POI GENERATED XCEL
	 *
	 * Set up the output data order based upon the data previously gathered.
	 *
	 * Note that the order has changed from the original spec.  Order is now
	 *  per the email from Vidula dated 8/17/2010
	 *
	 * @throws Exception
	 */
	public static GroupReport genHtmlString(ArrayList<ExtractItem> lst, GroupReport gr)
	{
//		//println "Start genHtmlString";
//		String headerStr = null;
//
//		// Loop through the individualReports generating the requisite string(s)
//		for (IndividualReport ir : gr.getIndividualReports())
//		{
//			//println "In IRloop...";
//			if (headerStr == null)
//			{
//				// First time through... generate the header string
//				//println "Do the header...";
//				headerStr = "<table>";
//				// loop through the extract items and put out the header string
//				for(ExtractItem itm : lst)
//				{
//					headerStr += "<tr><td>";
//					if (itm.getSrc() == HDG)
//					{
//						headerStr += "<b>" + itm.getDispName() + "</b>";
//					}
//					else
//					{
//						headerStr += itm.getDispName();
//					}
//					headerStr += "</td></tr>";
//				}
//				headerStr += "</table>";
//				//println "header done.  string=" + headerStr;
//			}
//
//			// Add the header to this ir
//			ir.addDisplayData("EXTRACT_HEADER_HTML", headerStr);
//
//			// Now do the data
//			String dataStr = "<table>";
//			for(ExtractItem itm : lst)
//			{
//				dataStr += "<tr><td>";
//				if (itm.getSrc() == HDG)
//				{
//					// put nothing into the td for an individual's data
//				}
//				else if (itm.getSrc() == GRP)
//				{
//					//System.out.println("  GRP Value=itm.getKey()=" +itm.getKey()+ " " + gr.getDisplayData(itm.getKey()));
//					dataStr += gr.getDisplayData(itm.getKey()) == null ? "" : XMLUtils.xmlSsEscapeString(gr.getDisplayData(itm.getKey()));
//				}
//				else if (itm.getSrc() == INDV)
//				{
//					//System.out.println("  INDV Value=itm.getKey()="+itm.getKey()+ "  " + ir.getScriptedData(itm.getKey()));
//					dataStr += ir.getDisplayData(itm.getKey()) == null ? "" : XMLUtils.xmlSsEscapeString(ir.getDisplayData(itm.getKey()));
//				}
//				else
//				{
//					// Must be an RDx label
//					ReportData rd = null;
//					// Get the appropriate ReportData object
//					rd = ir.getReportData().get(itm.getRptKey());
//					if (rd == null)
//						rd = new ReportData();
//
//					if (itm.getSrc() == RDD)
//					{
//						// then put out the display data
//						//System.out.println("  RDD Key=" + itm.getKey());
//						//System.out.println("  RDD Value=" + ir.getDisplayData(itm.getKey()));
//						dataStr += rd.getDisplayData(itm.getKey()) == null ? "" : XMLUtils.xmlSsEscapeString(rd.getDisplayData(itm.getKey()));
//					}
//					else if (itm.getSrc() == RDS)
//					{
//						// then put out the score data
//						//System.out.println("  RDS Key=" + itm.getKey());
//						//System.out.println("1  RDS Value=" +  rd.getScoreData(itm.getKey()) == null ? "" : rd.getScoreData(itm.getKey()));
//						dataStr += rd.getScoreData(itm.getKey()) == null ? "" : XMLUtils.xmlSsEscapeString(rd.getScoreData(itm.getKey()));
//						}
//				}
//				dataStr += "</td></tr>";
//			}
//			dataStr += "</table>";
//
//			ir.addDisplayData("EXTRACT_DATA_HTML", dataStr);
//		}

		return gr;
	}


	//
	// Instance data.
	//

	//
	// Constructors.
	//


	//
	// Instance methods.
	//

}