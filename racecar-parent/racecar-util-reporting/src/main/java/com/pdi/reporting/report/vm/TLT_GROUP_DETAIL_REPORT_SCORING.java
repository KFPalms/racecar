package com.pdi.reporting.report.vm;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.GroupReport;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;
import com.pdi.scoring.Norm;

public class TLT_GROUP_DETAIL_REPORT_SCORING {
	public GroupReport groupReport;

//	private double transitionLevelMean;
//	private double transitionLevelStdDev;
//	private double gpiGenPopMean;
//	private double gpiGenPopStdDev;
//	private double gpiSpecPopMean;
//	private double gpiSpecPopStdDev;

	public GroupReport getGroupReport(){
		return groupReport;
	}
	public void setGroupReport(GroupReport value){
		groupReport = value;
	}

	/*
	 * parse
	 */

	private Double parse(String value)
	{
		Double d;
		try
		{
			d = Double.parseDouble(value);
		}
		catch(Exception e)
		{
			d = (double)0;
		}
		return d;
	}

	/*
	 * generate
	 */
	public GroupReport generate()
	{
		//System.out.println("TLT_GROUP_DETAIL_REPORT_SCORING.java    GroupReport generate()  1 ");
		if(groupReport.getDisplayData().containsKey("STATUS") && groupReport.getDisplayData().get("STATUS").equalsIgnoreCase("FINISHED"))
		{
			//System.out.println("We've run this report");
			return groupReport;
		}

		//Calendar cal = Calendar.getInstance();
		//SimpleDateFormat sdf = new SimpleDateFormat("MMMMM dd, yyyy");
		//SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		//groupReport.addDisplayData("TODAY_DATE", sdf.format(cal.getTime()));
		java.util.Date date = new java.util.Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
		groupReport.getDisplayData().put("TODAY_DATE", sdf.format(date));
	  	/*
		 * *****************************************************
		 * GENERATE INDIVIDUAL REPORT DATA....
		 * (this process creates all the individual report data
		 *   to be used in generating the group score data)
		 * *******************************************************
		 */
		for(IndividualReport individualReport : groupReport.getIndividualReports()) {
			generate(individualReport);
			//System.out.println("generate: " + individualReport.getDisplayData().get("PART_ID"));
	    }
		//System.out.println("Done generating individual reports");

		/*
		 * *************************************************
		 * GENERATE GROUP SCORING DATA...
		 * (using the individual report data just obtained)
		 * **************************************************
		 */
		//call once for each participant....
		for(IndividualReport individualReport : groupReport.getIndividualReports()) {
			generateGroupData(individualReport);
			//System.out.println("generateGroupData: " + individualReport.getDisplayData().get("PART_ID"));
		}
		//System.out.println("Done generating group data");

		/*
		 * *************************************************
		 * GENERATE RANK...
		 * (determine the order of each participant)
		 * **************************************************
		 */
		//call once for each participant....

		//System.out.println("Done generating rank");
		//System.out.println("GroupReport generate()  4 ")


		//System.out.println("GroupReport generate()  5 ")

		return groupReport;
	}


	/*
	 *  FOR THE MOMENT, THIS WAS BEEN COPIED EXACTLY FROM THE INDIVIDUAL REPORT
	 *  GROOVY SCRIPT.... JUST TO MAKE IT WORK... WILL CREATE INTERESTING NEW
	 *  CLASS AND METHODS LATER....  OR NOT, IF TOO MANY CHANGES
	 */
	public void generate(IndividualReport individualReport)
	{
		//System.out.println( "- TLT_GROUP_DETAIL_REPORT_SCORING.java  - Starting GENERATE INDIVIDUAL REPORT... ");

		//Norm norm = new Norm();

		if(individualReport.getReportData() == null)
		{
			individualReport.getScriptedData().put("STATUS", "NOT_FINISHED");
			individualReport.getScriptedData().put("ERROR", "Corrupted individual report");
			return;
		}


		if(individualReport.getReportData() == null)
		{
			individualReport.getScriptedData().put("STATUS", "NOT_FINISHED");
			individualReport.getScriptedData().put("ERROR", "No report data is available, unable to generate report");
			return;

		}

		/*
		 * If this is a TRANSITIONS_SCALES report, we can use partial data
		 * check the flag.... 
		 * 
		 */		
		if(individualReport.getScriptedData().get(ReportingConstants.PART_COMPL_RPT).equals(ReportingConstants.PARTIAL_REPORT)) {
			// let it just go through....
			
		}else {
			// return with error if not complete data set
			if(individualReport.getDisplayData().get("GPI_MISSING") == "MISSING" 
					&& individualReport.getScriptedData().get(ReportingConstants.PART_COMPL_RPT) == ReportingConstants.COMPLETE_REPORT)
			{
				//System.out.println("- tltgroupdetailreportscoring.java  -no gpi ");
				// They haven't finished the GPI
				individualReport.addScriptedData("STATUS", "NOT_FINISHED");
				individualReport.getScriptedData().put("ERROR", "GPI has not been completed, unable to generate report");
				//return ; //Group Report! we're fine with partial or no data
			}

			if(individualReport.getDisplayData().get("LEI_MISSING") == "MISSING" 
					&& individualReport.getScriptedData().get(ReportingConstants.PART_COMPL_RPT) == ReportingConstants.COMPLETE_REPORT)
			{
				//System.out.println("- tltgroupdetailreportscoring.java  - no lei ");
				// They haven't finished the GPI
				individualReport.addScriptedData("STATUS", "NOT_FINISHED");
				individualReport.getScriptedData().put("ERROR", "LEI has not been completed, unable to generate report");
				//return ;//Group Report! we're fine with partial or no data

			}
			String partialComp = individualReport.getScriptedData().get(ReportingConstants.PART_COMPL_RPT);
			if(individualReport.getDisplayData().get("CS_MISSING") == "MISSING" 
					&& partialComp == ReportingConstants.COMPLETE_REPORT)
			{
				//System.out.println("- tltgroupdetailreportscoring.java  - no cs ");
				// They haven't finished the Career Survey
				individualReport.addScriptedData("STATUS", "NOT_FINISHED");
				individualReport.getScriptedData().put("ERROR", "Career Survey has not been completed, unable to generate report");
				//return ;//Group Report! we're fine with partial or no data

			}

			// This is supposed to catch the lack of norms - TLT Does not require norms
			/*
			if(individualReport.getDisplayData().get(ReportingConstants.DR_EGOCE+"_MEAN") == null)
			{
				//System.out.println("- tltgroupdetailreportscoring.java  - no norms set ");
				// There are no GPI norms
				individualReport.getScriptedData().put("STATUS", "NOT_FINISHED");
				individualReport.getScriptedData().put("ERROR", "Norms are not present for this project, unable to generate report");
				return ;
			}
			*/
		}
	
		individualReport.getScriptedData().put("STATUS", "FINISHED");

		//Set<String> ks = individualReport.getReportData().keySet();
		//System.out.println("--->Groovy Keys:");
		//for(java.util.Iterator<String> itr= ks.iterator(); itr.hasNext();  )
		//{
		//	System.out.println("--->Key=" + itr.next());
		//}

		// Convert scale scores to PDI ratings (stanines not ratings)
		// Means and Standard deviations
		HashMap<String, String> tempScores = new HashMap<String, String>();

		//	CALC ZGEST (Cognitives -- Ravens... )
		boolean doRavSF = false;
		boolean doRavB = false;
		ReportData ravRd = individualReport.getReportData().get(ReportingConstants.RC_RAVENS_SF);
		doRavSF = (ravRd==null ? false : true);
		ravRd = individualReport.getReportData().get(ReportingConstants.RC_RAVENS_B);
		doRavB = (ravRd==null ? false : true);

		if (doRavSF && doRavB)
		{
			doRavSF = false;
			//doRavB = true;	// redundant... should already be true
		}
		//println "doRavSF=" + doRavSF + ", doRavB=" + doRavB;
		//System.out.println("individualReport.getDisplayData().get(ReportingConstants.RC_COGNITIVES_INCLUDED) " + individualReport.getDisplayData().get(ReportingConstants.RC_COGNITIVES_INCLUDED));
		//System.out.println("228 TLT GROUP DETAIL RPT SCORING JAVA  " + individualReport.getReportCode());
		
		if(individualReport.getReportCode().equals(ReportingConstants.REPORT_CODE_ABYD_TRANSITION_SCALES) ){
			//Per code in the report helper for AbyD, Hardcode Cogs Included to 1
			individualReport.getDisplayData().put(ReportingConstants.RC_COGNITIVES_INCLUDED, "1");
		}
		if(new	Integer(individualReport.getDisplayData().get(ReportingConstants.RC_COGNITIVES_INCLUDED)).intValue() != 1)
		{
			//System.out.println("HOW THE HECK ARE COGNITIVES NOT INCLUDED???? " + individualReport.getDisplayData().get(ReportingConstants.RC_COGNITIVES_INCLUDED));
			//println "Cogs NOT included..."
			// They haven't been assigned either Raven's, so don't include it...
			if (doRavSF  &&
			    groupReport.getScriptedData().get(ReportingConstants.RC_RAVENS_SF) == null)
			{
				groupReport.getScriptedData().put(ReportingConstants.RC_RAVENS_SF, "");
			}
			if (doRavB  &&
			    groupReport.getScriptedData().get(ReportingConstants.RC_RAVENS_B) == null)
			{
				groupReport.getScriptedData().put(ReportingConstants.RC_RAVENS_B, "");
			}

			//tempScores.put(ReportingConstants.RAV_PSOLV, "0.0");
			//individualReport.getScriptedData().put("RAW_" + ReportingConstants.RAV_PSOLV, "0.0");
			tempScores.put(ReportingConstants.RAV_PSOLV, "");
			individualReport.getScriptedData().put("RAW_" + ReportingConstants.RAV_PSOLV, "");
		} else {
			//println "Cogs ARE included..."

			if (! doRavSF && ! doRavB)
			{
				//System.out.println("NO RAVENS SF OR RAVENS B DATA FOR THIS PARTICIPANT");
				individualReport.addDisplayData("RAVENS_MISSING", "MISSING");

				//println "No Ravens Data..."
				// No data for either Raven's
				//tempScores.put(ReportingConstants.RAV_PSOLV, "0.0");
				//individualReport.getScriptedData().put("RAW_" + ReportingConstants.RAV_PSOLV, "0.0");
				//individualReport.getScriptedData().put("RAW_RAVENS_CORRECT", "0");
				tempScores.put(ReportingConstants.RAV_PSOLV, "");
				individualReport.getScriptedData().put("RAW_" + ReportingConstants.RAV_PSOLV, "");
				individualReport.getScriptedData().put("RAW_RAVENS_CORRECT", "");
			} else {
				//println "Got Ravens Data..."
				// score the raven's
				double ravensScore = 0.0;
				if (doRavSF)
				{
					ravensScore = parse(individualReport.getReportData().get(ReportingConstants.RC_RAVENS_SF).getScoreData().get(ReportingConstants.RAVENS_SHORT));
					groupReport.getScriptedData().put(ReportingConstants.RC_RAVENS_SF, "true");
				}
				if (doRavB)
				{
					//System.out.println("COGNITIVES INCLUDED IS ONE, WE NEED TO CALC IT NOW "  + individualReport.getReportData().get(ReportingConstants.RC_RAVENS_B).getScoreData().get(ReportingConstants.RAVENS_B));
					ravensScore = parse(individualReport.getReportData().get(ReportingConstants.RC_RAVENS_B).getScoreData().get(ReportingConstants.RAVENS_B));
					groupReport.getScriptedData().put(ReportingConstants.RC_RAVENS_B, "true");
				}
				//System.out.println("Raven's raw score  " + ravensScore);
				individualReport.getScriptedData().put("RAW_RAVENS_CORRECT", ravensScore + "");
				
				//if(ravensScore != 0.0) {
				//	ravensScore = (ravensScore - ReportingConstants.RAVENS_TLT_MEAN) / ReportingConstants.RAVENS_TLT_STDEV ;
				//}
				// Score of 0 is valid
				ravensScore = (ravensScore - ReportingConstants.RAVENS_TLT_MEAN) / ReportingConstants.RAVENS_TLT_STDEV ;

				//System.out.println("raven's initial normed score (treated as raw downstream) " + ravensScore);
				tempScores.put(ReportingConstants.RAV_PSOLV, ravensScore+ "");
				individualReport.getScriptedData().put("RAW_" + ReportingConstants.RAV_PSOLV, ravensScore+ "");
			}
		}

		// CALC RAW PERSONALITY SCORES (GPI)
		if(	individualReport.getReportData().get(ReportingConstants.RC_GPI) != null )
		{
			// First, we need to reverse the score for two GPI scales.
			double gpiInd = parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_INDEPENDENCE));
			double gpiIndReversed = (gpiInd - 8) * (-1);
			double gpiNegAff = parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_NEG_AFFECT));
			double gpiNegAffReversed = (gpiNegAff - 7) * (-1);
			// Compute Raw Personality Scores
			// GPI_INTEN
			double gpiInten =
				parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_INNOVATION)) +
				parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_THOUGHT_FOCUS)) +
				parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_VISION));
			tempScores.put(ReportingConstants.GPI_INTEN, gpiInten+ "");
			individualReport.getScriptedData().put("RAW_" + ReportingConstants.GPI_INTEN, gpiInten+ "");
			//System.out.println("243:gpiInten+ "" " + gpiInten+ "");
			//System.out.println("301 individualReport.getScriptedData().get(RAW_+ ReportingConstants.GPI_INTEN " + individualReport.getScriptedData().get("RAW_" + ReportingConstants.GPI_INTEN));

			//GPI_ATD
			double gpiAtd = parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_ATTN_DETAIL));
			tempScores.put(ReportingConstants.GPI_ATD, gpiAtd+ "");
			individualReport.getScriptedData().put("RAW_" + ReportingConstants.GPI_ATD, gpiAtd+ "");
			//System.out.println(" ReportingConstants.GPI_ATD  " + tempScores.get(ReportingConstants.GPI_ATD));

			// GPI_IMIN
			double gpiImin =
				parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_TAKE_CHARGE)) +
				parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_INFLUENCE)) +
				parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_TRUST)) +
				parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_SOCIAL_ASTUTE));
			tempScores.put(ReportingConstants.GPI_IMIN, gpiImin+ "");
			individualReport.getScriptedData().put("RAW_" + ReportingConstants.GPI_IMIN, gpiImin+ "");

			//GPI_INEN
			double gpiInen =
				parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_SOCIABILITY)) +
				parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_CONSIDERATION)) +
				parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_EMPATHY));
			tempScores.put(ReportingConstants.GPI_INEN, gpiInen+ "");
			individualReport.getScriptedData().put("RAW_" + ReportingConstants.GPI_INEN, gpiInen+ "");

			//GPI_ACHD
			double gpiAchd =
				parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_ENERGY_LEV)) +
				parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_DESIRE_ACHIEVE));
			tempScores.put(ReportingConstants.GPI_ACHD, gpiAchd+ "");
			individualReport.getScriptedData().put("RAW_" + ReportingConstants.GPI_ACHD, gpiAchd+ "");

			//GPI_ADVD
			double gpiAdvd =
				parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_RISK_TAKING)) +
				parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_DESIRE_ADVANCE));
			tempScores.put(ReportingConstants.GPI_ADVD, gpiAdvd+ "");
			individualReport.getScriptedData().put("RAW_" + ReportingConstants.GPI_ADVD, gpiAdvd+ "");
			//System.out.println("ADVD:  risk=" + individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_RISK_TAKING) +
			//    ", desAdvance=" + individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_DESIRE_ADVANCE) +
			//	", raw=" + gpiAdvd);

			//GPI_COOR
			double gpiCoor =
				parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_INTERDEPENDENCE)) +
				parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_DUTIFUL)) +
				gpiIndReversed;
			tempScores.put(ReportingConstants.GPI_COOR, gpiCoor+ "");
			individualReport.getScriptedData().put("RAW_" + ReportingConstants.GPI_COOR, gpiCoor+ "");
			//System.out.println("COOR:  interdep=" + individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_INTERDEPENDENCE) +
			//", dutiful=" + individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_DUTIFUL) +
			//", gpiIndReversed=" + gpiIndReversed +
			//", raw=" + gpiCoor);

			//GPI_FLAD
			double gpiFlad =
				parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_ADAPTABILITY)) +
				gpiNegAffReversed +
				parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_OPTIMISM)) +
				parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_STRESS_TOL)) +
				parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_SELF_AWARE));
			tempScores.put(ReportingConstants.GPI_FLAD, gpiFlad+ "");
			individualReport.getScriptedData().put("RAW_" + ReportingConstants.GPI_FLAD, gpiFlad+ "");

			//DR_DERAIL
			double drDerail =
				parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_EGO)) +
				parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_MANIPULATION)) +
				parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_MICRO_MANAGE)) +
				parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_PASS_AGGRESS));
			tempScores.put(ReportingConstants.DR_DERAIL, drDerail+ "");
			individualReport.getScriptedData().put("RAW_" + ReportingConstants.DR_DERAIL, drDerail+ "");
			//System.out.println("RAW_DERAIL_SCORE=" + drDerail);

			//DERAILMENT RISK SCORES:
			double gpiEgo = parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_EGO));
			tempScores.put(ReportingConstants.DR_EGOCE, gpiEgo+ "");
			individualReport.getScriptedData().put("RAW_" + ReportingConstants.DR_EGOCE, gpiEgo+ "");

			double gpiManipulation = parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_MANIPULATION));
			tempScores.put(ReportingConstants.DR_MANIP, gpiManipulation+ "");
			individualReport.getScriptedData().put("RAW_" + ReportingConstants.DR_MANIP, gpiManipulation+ "");

			double gpiMicroManage = parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_MICRO_MANAGE));
			tempScores.put(ReportingConstants.DR_MICRO, gpiMicroManage+ "");
			individualReport.getScriptedData().put("RAW_" + ReportingConstants.DR_MICRO, gpiMicroManage+ "");

			double gpiPassAggress = parse(individualReport.getReportData().get(ReportingConstants.RC_GPI).getScoreData().get(ReportingConstants.GPI_PASS_AGGRESS));
			tempScores.put(ReportingConstants.DR_PASAG, gpiPassAggress+ "");
			individualReport.getScriptedData().put("RAW_" + ReportingConstants.DR_PASAG, gpiPassAggress+ "");
		}

		if( individualReport.getReportData().get(ReportingConstants.RC_CAREER_SURVEY) != null)
		{
			// CAREER SURVEY SCORES
			double csCareerGoals = parse(individualReport.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().get(ReportingConstants.CAREER_GOALS));
			tempScores.put(ReportingConstants.CS_LDRAS, csCareerGoals+ "");
			individualReport.getScriptedData().put("RAW_" + ReportingConstants.CS_LDRAS, csCareerGoals+ "");

			double csCareerDrivers = parse(individualReport.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().get(ReportingConstants.CAREER_DRIVERS));
			tempScores.put(ReportingConstants.CS_CRDRV, csCareerDrivers+ "");
			individualReport.getScriptedData().put("RAW_" + ReportingConstants.CS_CRDRV, csCareerDrivers+ "");

			double csLearningOrient = parse(individualReport.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().get(ReportingConstants.LEARNING_ORIENT));
			tempScores.put(ReportingConstants.CS_LRNOR, csLearningOrient+ "");
			individualReport.getScriptedData().put("RAW_" + ReportingConstants.CS_LRNOR, csLearningOrient+ "");

			double csExperienceOrient = parse(individualReport.getReportData().get(ReportingConstants.RC_CAREER_SURVEY).getScoreData().get(ReportingConstants.EXPERIENCE_ORIENT));
			tempScores.put(ReportingConstants.CS_EXPOR, csExperienceOrient+ "");
			individualReport.getScriptedData().put("RAW_" + ReportingConstants.CS_EXPOR, csExperienceOrient+ "");
		}

		// CALC LEADERSHIP SCORES (LEI)
		if( individualReport.getReportData().get(ReportingConstants.RC_LEI) != null)
		{
			//LEI_BUSOP
			double leiBusop =
				parse(individualReport.getReportData().get(ReportingConstants.RC_LEI).getScoreData().get(ReportingConstants.LEI_OPERATIONS)) +
				parse(individualReport.getReportData().get(ReportingConstants.RC_LEI).getScoreData().get(ReportingConstants.LEI_PROJ_MGMT_IMPL));
			tempScores.put(ReportingConstants.LEI_BUSOP,leiBusop+ "");
			individualReport.getScriptedData().put("RAW_" + ReportingConstants.LEI_BUSOP,leiBusop+ "");

			//LEI_HTC
			double leiHtc =
				parse(individualReport.getReportData().get(ReportingConstants.RC_LEI).getScoreData().get(ReportingConstants.LEI_DIFF_STAFFING)) +
				parse(individualReport.getReportData().get(ReportingConstants.RC_LEI).getScoreData().get(ReportingConstants.LEI_INHERITED_PROB));
			tempScores.put(ReportingConstants.LEI_HTC,leiHtc+ "");
			individualReport.getScriptedData().put("RAW_" + ReportingConstants.LEI_HTC,leiHtc+ "");

			//LEI_HVIS
			double leiHvis =
				parse(individualReport.getReportData().get(ReportingConstants.RC_LEI).getScoreData().get(ReportingConstants.LEI_HI_CRIT_VISIBLE)) +
				parse(individualReport.getReportData().get(ReportingConstants.RC_LEI).getScoreData().get(ReportingConstants.LEI_HIGH_RISK));
			tempScores.put(ReportingConstants.LEI_HVIS,leiHvis+ "");
			individualReport.getScriptedData().put("RAW_" + ReportingConstants.LEI_HVIS,leiHvis+ "");

			//LEI_GTBUS
			double leiGtbus =
				parse(individualReport.getReportData().get(ReportingConstants.RC_LEI).getScoreData().get(ReportingConstants.LEI_BUS_DEV_MARKTNG)) +
				parse(individualReport.getReportData().get(ReportingConstants.RC_LEI).getScoreData().get(ReportingConstants.LEI_BUS_GROWTH));
			tempScores.put(ReportingConstants.LEI_GTBUS,leiGtbus+ "");
			individualReport.getScriptedData().put("RAW_" + ReportingConstants.LEI_GTBUS,leiGtbus+ "");

			//LEI_PDEV
			double leiPdev =
				parse(individualReport.getReportData().get(ReportingConstants.RC_LEI).getScoreData().get(ReportingConstants.LEI_SELF_DEV)) +
				parse(individualReport.getReportData().get(ReportingConstants.RC_LEI).getScoreData().get(ReportingConstants.LEI_EXTRACURRICULAR));
			tempScores.put(ReportingConstants.LEI_PDEV, leiPdev+ "");
			individualReport.getScriptedData().put("RAW_" + ReportingConstants.LEI_PDEV, leiPdev+ "");
		}

		//println "Done with RAW calcs..."

		// NOW CALC Z-SCORES FROM THE tempScores,
		// and CALC PERCENTILES FROM Z'S,
		// finally, GET STANINES FROM PERCENTILE

		double zScore = 0.0;
		double percentScore = 0.0;
		int stanine = 0;
		double mean = 0.0;
		double stdev = 0.0;

		// cognitive score
		//System.out.println("RAV_PSOLV=" + tempScores.get(ReportingConstants.RAV_PSOLV));
		//if( ! tempScores.get(ReportingConstants.RAV_PSOLV).equals("0.0")  )
		if( ! tempScores.get(ReportingConstants.RAV_PSOLV).equals("")  )
		{
			zScore =  parse(tempScores.get(ReportingConstants.RAV_PSOLV));
			mean = parse(individualReport.getDisplayData(ReportingConstants.RAV_PSOLV + "_MEAN"));
			stdev = parse(individualReport.getDisplayData(ReportingConstants.RAV_PSOLV + "_STDEV"));
			//System.out.println("RAVEN'S COG FINAL CALCS...  ");
			//System.out.println("initial zscore: " + tempScores.get(ReportingConstants.RAV_PSOLV));
			//System.out.println("     mean " + mean);
			//System.out.println("     stdev " + stdev);

			// do the zScore w/ the group ZGEST
			double  zScore2 = (zScore - mean)/stdev;
			percentScore = Norm.calcDoublePctlFromZ(zScore2);
			stanine = findStanineFromPctl( percentScore );
			//System.out.println("    RAV_PSOLV percentScore " + percentScore);
			//System.out.println("    RAV_PSOLV stanine " + stanine);

			individualReport.getScriptedData().put(ReportingConstants.RAV_PSOLV, stanine+ "");
		} else
		{
			// UI will put in an empty box if the stanine is an empty string
			individualReport.getScriptedData().put(ReportingConstants.RAV_PSOLV, "");
		}

		// personality scores
		if( individualReport.getReportData().get(ReportingConstants.RC_GPI) != null)			
		{
			// NOTE:  yeah, could have done this in fewer lines, and more elegantly, but the number
			// 		   of parens were driving me nutz... this is easier to follow...
			mean = parse(individualReport.getDisplayData(ReportingConstants.GPI_INTEN+"_MEAN"));
			stdev = parse(individualReport.getDisplayData(ReportingConstants.GPI_INTEN+"_STDEV"));
			if(tempScores.get(ReportingConstants.GPI_INTEN).equals("0.0"))
			{
				zScore = (0.0 - mean) / stdev;
			} else {
				zScore = ( parse(tempScores.get(ReportingConstants.GPI_INTEN)) - mean ) / stdev;
			}
			percentScore = Norm.calcDoublePctlFromZ(zScore);
			stanine = findStanineFromPctl(percentScore);
			individualReport.getScriptedData().put(ReportingConstants.GPI_INTEN, stanine+ "");

			mean = parse(individualReport.getDisplayData(ReportingConstants.GPI_ATD+"_MEAN"));
			stdev = parse(individualReport.getDisplayData(ReportingConstants.GPI_ATD+"_STDEV"));
			if(tempScores.get(ReportingConstants.GPI_ATD).equals("0.0"))
			{
				zScore = (0.0 - mean) / stdev;
			} else {
				zScore =  ( parse(tempScores.get(ReportingConstants.GPI_ATD) ) - mean ) / stdev;
			}
			percentScore = Norm.calcDoublePctlFromZ(zScore);
			stanine = findStanineFromPctl(percentScore);
			individualReport.getScriptedData().put(ReportingConstants.GPI_ATD, stanine+ "");
			//System.out.println("ReportingConstants.GPI_ATD " + percentScore+ "" + " | " + stanine+ "");

			mean = parse(individualReport.getDisplayData(ReportingConstants.GPI_IMIN+"_MEAN"));
			stdev = parse(individualReport.getDisplayData(ReportingConstants.GPI_IMIN+"_STDEV"));
			if(tempScores.get(ReportingConstants.GPI_IMIN).equals("0.0"))
			{
				zScore = (0.0 - mean) / stdev;
			} else {
				zScore =  ( parse(tempScores.get(ReportingConstants.GPI_IMIN) ) - mean  ) / stdev;
			}
			percentScore = Norm.calcDoublePctlFromZ(zScore);
			stanine = findStanineFromPctl(percentScore);
			individualReport.getScriptedData().put(ReportingConstants.GPI_IMIN, stanine+ "");
			//System.out.println("ReportingConstants.GPI_IMIN " + percentScore+ "" + " | " + stanine+ "");

			mean = parse(individualReport.getDisplayData(ReportingConstants.GPI_INEN+"_MEAN"));
			stdev = parse(individualReport.getDisplayData(ReportingConstants.GPI_INEN+"_STDEV"));
			if(tempScores.get(ReportingConstants.GPI_INEN).equals("0.0"))
			{
				zScore = (0.0 - mean) / stdev;
			} else {
				zScore =  ( parse(tempScores.get(ReportingConstants.GPI_INEN) ) - mean ) / stdev;
			}
			percentScore = Norm.calcDoublePctlFromZ(zScore);
			stanine = findStanineFromPctl(percentScore);
			individualReport.getScriptedData().put(ReportingConstants.GPI_INEN, stanine+ "");
			//System.out.println("ReportingConstants.GPI_INEN " + percentScore+ "" + " | " + stanine+ "");

			mean = parse(individualReport.getDisplayData(ReportingConstants.GPI_ACHD+"_MEAN"));
			stdev = parse(individualReport.getDisplayData(ReportingConstants.GPI_ACHD+"_STDEV"));
			if(tempScores.get(ReportingConstants.GPI_ACHD).equals("0.0"))
			{
				zScore = (0.0 - mean) / stdev;
			} else {
				zScore =  ( parse(tempScores.get(ReportingConstants.GPI_ACHD) ) - mean ) / stdev;
			}
			percentScore = Norm.calcDoublePctlFromZ(zScore);
			stanine = findStanineFromPctl(percentScore);
			individualReport.getScriptedData().put(ReportingConstants.GPI_ACHD, stanine+ "");
			//System.out.println("ReportingConstants.GPI_ACHD " + percentScore+ "" + " | " + stanine+ "");

			mean = parse(individualReport.getDisplayData(ReportingConstants.GPI_ADVD+"_MEAN"));
			stdev = parse(individualReport.getDisplayData(ReportingConstants.GPI_ADVD+"_STDEV"));
			if(tempScores.get(ReportingConstants.GPI_ADVD).equals("0.0"))
			{
				zScore = (0.0 - mean) / stdev;
			} else {
				zScore =  ( parse(tempScores.get(ReportingConstants.GPI_ADVD) ) - mean ) / stdev;
			}
			percentScore = Norm.calcDoublePctlFromZ(zScore);
			stanine = findStanineFromPctl(percentScore);
			individualReport.getScriptedData().put(ReportingConstants.GPI_ADVD, stanine+ "");
			//System.out.println("GPI_ADVD:  mean=" + mean + ", stdv=" + stdev + ", zscore=" + zScore + ", pctl=" + percentScore + ", stanine=" + stanine);

			mean = parse(individualReport.getDisplayData(ReportingConstants.GPI_COOR+"_MEAN"));
			stdev = parse(individualReport.getDisplayData(ReportingConstants.GPI_COOR+"_STDEV"));
			if(tempScores.get(ReportingConstants.GPI_COOR).equals("0.0"))
			{
				zScore = (0.0 - mean) / stdev;
			} else {
				zScore =  ( parse(tempScores.get(ReportingConstants.GPI_COOR) ) - mean ) / stdev;
			}
			percentScore = Norm.calcDoublePctlFromZ(zScore);
			stanine = findStanineFromPctl(percentScore);
			individualReport.getScriptedData().put(ReportingConstants.GPI_COOR, stanine+ "");
			//System.out.println("GPI_COOR:  mean=" + mean + ", stdv=" + stdev + ", zscore=" + zScore + ", pctl=" + percentScore + ", stanine=" + stanine);

			mean = parse(individualReport.getDisplayData(ReportingConstants.GPI_FLAD+"_MEAN"));
			stdev = parse(individualReport.getDisplayData(ReportingConstants.GPI_FLAD+"_STDEV"));
			if(tempScores.get(ReportingConstants.GPI_FLAD).equals("0.0"))
			{
				zScore = (0.0 - mean) / stdev;
			} else {
				zScore =  ( parse(tempScores.get(ReportingConstants.GPI_FLAD) ) - mean ) / stdev;
			}
			percentScore = Norm.calcDoublePctlFromZ(zScore);
			stanine = findStanineFromPctl(percentScore);
			individualReport.getScriptedData().put(ReportingConstants.GPI_FLAD, stanine+ "");
			//System.out.println("ReportingConstants.GPI_FLAD " + percentScore+ "" + " | " + stanine+ "");

			mean = parse(individualReport.getDisplayData(ReportingConstants.DR_DERAIL+"_MEAN"));
			stdev = parse(individualReport.getDisplayData(ReportingConstants.DR_DERAIL+"_STDEV"));
			if(tempScores.get(ReportingConstants.DR_DERAIL).equals("0.0"))
			{
				zScore = (0.0 - mean) / stdev;
			} else {
				zScore =  ( parse(tempScores.get(ReportingConstants.DR_DERAIL) ) - mean ) / stdev;
			}
			percentScore = Norm.calcDoublePctlFromZ(zScore);
			stanine = findStanineFromPctl(percentScore);
			individualReport.getScriptedData().put(ReportingConstants.DR_DERAIL, stanine+ "");
			//System.out.println("DR Stanine=" + individualReport.getScriptedData().get(ReportingConstants.DR_DERAIL));
			//System.out.println("ReportingConstants.DR_DERAIL " + percentScore+ "" + " | " + stanine+ "");
		}else {
			//System.out.println("GPI is null, setting data to 0.0");
			// all the stanines for gpi are null
			individualReport.getScriptedData().put(ReportingConstants.GPI_INTEN, "0.0");
			individualReport.getScriptedData().put(ReportingConstants.GPI_ATD, "0.0");
			individualReport.getScriptedData().put(ReportingConstants.GPI_IMIN, "0.0");
			individualReport.getScriptedData().put(ReportingConstants.GPI_INEN, "0.0");
			individualReport.getScriptedData().put(ReportingConstants.GPI_ACHD, "0.0");
			individualReport.getScriptedData().put(ReportingConstants.GPI_ADVD, "0.0");
			individualReport.getScriptedData().put(ReportingConstants.GPI_COOR,"0.0");
			individualReport.getScriptedData().put(ReportingConstants.GPI_FLAD, "0.0");
			individualReport.getScriptedData().put(ReportingConstants.DR_DERAIL, "0.0");
		}


		// Leadership scores
		if(individualReport.getReportData().get(ReportingConstants.RC_LEI) != null)
		{
			//System.out.println("leadership scores - 618");
			mean = parse(individualReport.getDisplayData(ReportingConstants.LEI_HTC+"_MEAN"));
			stdev = parse(individualReport.getDisplayData(ReportingConstants.LEI_HTC+"_STDEV"));
			if(tempScores.get(ReportingConstants.LEI_HTC).equals("0.0"))
			{
				zScore = (0.0 - mean) / stdev;
			} else {
				zScore =  ( parse(tempScores.get(ReportingConstants.LEI_HTC) ) - mean ) / stdev;
			}
			percentScore = Norm.calcDoublePctlFromZ(zScore);
			stanine = findStanineFromPctl(percentScore);
			individualReport.getScriptedData().put(ReportingConstants.LEI_HTC, stanine+ "");
			//System.out.println("ReportingConstants.LEI_HTC " + percentScore+ "" + " | " + stanine+ "");

			mean = parse(individualReport.getDisplayData(ReportingConstants.LEI_GTBUS+"_MEAN"));
			stdev = parse(individualReport.getDisplayData(ReportingConstants.LEI_GTBUS+"_STDEV"));
			if(tempScores.get(ReportingConstants.LEI_GTBUS).equals("0.0"))
			{
				zScore = (0.0 - mean) / stdev;
			} else {
				zScore =  ( parse(tempScores.get(ReportingConstants.LEI_GTBUS) ) - mean ) / stdev;
			}
			percentScore = Norm.calcDoublePctlFromZ(zScore);
			stanine = findStanineFromPctl(percentScore);
			individualReport.getScriptedData().put(ReportingConstants.LEI_GTBUS, stanine+ "");
			//System.out.println("ReportingConstants.LEI_GTBUS " + percentScore+ "" + " | " + stanine+ "");

			mean = parse(individualReport.getDisplayData(ReportingConstants.LEI_HVIS+"_MEAN"));
			stdev = parse(individualReport.getDisplayData(ReportingConstants.LEI_HVIS+"_STDEV"));
			if(tempScores.get(ReportingConstants.LEI_HVIS).equals("0.0"))
			{
				zScore = (0.0 - mean) / stdev;
			} else {
				zScore =  ( parse(tempScores.get(ReportingConstants.LEI_HVIS) ) - mean ) / stdev;
			}
			percentScore = Norm.calcDoublePctlFromZ(zScore);
			stanine = findStanineFromPctl(percentScore);
			individualReport.getScriptedData().put(ReportingConstants.LEI_HVIS, stanine+ "");

			mean = parse(individualReport.getDisplayData(ReportingConstants.LEI_BUSOP+"_MEAN"));
			stdev =parse(individualReport.getDisplayData(ReportingConstants.LEI_BUSOP+"_STDEV"));
			if(tempScores.get(ReportingConstants.LEI_BUSOP).equals("0.0"))
			{
				zScore = (0.0 - mean) / stdev;
			} else {
				zScore =  ( parse(tempScores.get(ReportingConstants.LEI_BUSOP) ) - mean ) / stdev;
			}
			percentScore = Norm.calcDoublePctlFromZ(zScore);
			//System.out.println("01-05-2011 - 660 - %" + percentScore + " Mean: " + mean + " STDEV: " + stdev + " SCORE: " + tempScores.get(ReportingConstants.LEI_BUSOP) + " Z: " + zScore);
			stanine = findStanineFromPctl(percentScore);
			individualReport.getScriptedData().put(ReportingConstants.LEI_BUSOP, stanine+ "");
			//System.out.println("ReportingConstants.LEI_BUSOP " + percentScore+ "" + " | " + stanine+ "");

			mean = parse(individualReport.getDisplayData(ReportingConstants.LEI_PDEV+"_MEAN"));
			stdev = parse(individualReport.getDisplayData(ReportingConstants.LEI_PDEV+"_STDEV"));
			if(tempScores.get(ReportingConstants.LEI_PDEV).equals("0.0"))
			{
				zScore = (0.0 - mean) / stdev;
			} else {
				zScore =  ( parse(tempScores.get(ReportingConstants.LEI_PDEV) ) - mean ) / stdev;
			}
			percentScore = Norm.calcDoublePctlFromZ(zScore);
			stanine = findStanineFromPctl(percentScore);
			individualReport.getScriptedData().put(ReportingConstants.LEI_PDEV, stanine+ "");
			//System.out.println("ReportingConstants.LEI_PDEV " + percentScore+ "" + " | " + stanine+ "");
		}else {
			//set all scores to 0.0
			individualReport.getScriptedData().put(ReportingConstants.LEI_HTC, "0.0");
			individualReport.getScriptedData().put(ReportingConstants.LEI_GTBUS, "0.0");
			individualReport.getScriptedData().put(ReportingConstants.LEI_HVIS, "0.0");
			individualReport.getScriptedData().put(ReportingConstants.LEI_BUSOP, "0.0");
			individualReport.getScriptedData().put(ReportingConstants.LEI_PDEV, "0.0");
		}

		// Derailment factors
		if( individualReport.getReportData().get(ReportingConstants.RC_GPI) != null)
		{

			mean = parse(individualReport.getDisplayData(ReportingConstants.DR_EGOCE+"_MEAN"));
			stdev = parse(individualReport.getDisplayData(ReportingConstants.DR_EGOCE+"_STDEV"));
			if(tempScores.get(ReportingConstants.DR_EGOCE).equals("0.0"))
			{
				zScore = (0.0 - mean) / stdev;
			} else {
				zScore =  ( parse(tempScores.get(ReportingConstants.DR_EGOCE) ) - mean ) / stdev;
			}
			percentScore = Norm.calcDoublePctlFromZ(zScore);
			stanine = findStanineFromPctl(percentScore);
			individualReport.getScriptedData().put(ReportingConstants.DR_EGOCE, stanine+ "");

			mean = parse(individualReport.getDisplayData(ReportingConstants.DR_MANIP+"_MEAN"));
			stdev = parse(individualReport.getDisplayData(ReportingConstants.DR_MANIP+"_STDEV"));
			if(tempScores.get(ReportingConstants.DR_MANIP).equals("0.0"))
			{
				zScore = (0.0 - mean) / stdev;
			} else {
				zScore =  ( parse(tempScores.get(ReportingConstants.DR_MANIP) ) - mean ) / stdev;
			}
			percentScore = Norm.calcDoublePctlFromZ(zScore);
			stanine = findStanineFromPctl(percentScore);
			individualReport.getScriptedData().put(ReportingConstants.DR_MANIP, stanine+ "");

			mean = parse(individualReport.getDisplayData(ReportingConstants.DR_MICRO+"_MEAN"));
			stdev = parse(individualReport.getDisplayData(ReportingConstants.DR_MICRO+"_STDEV"));
			if(tempScores.get(ReportingConstants.DR_MICRO).equals("0.0"))
			{
				zScore = (0.0 - mean) / stdev;
			} else {
				zScore =  ( parse(tempScores.get(ReportingConstants.DR_MICRO) ) - mean ) / stdev;
			}
			percentScore = Norm.calcDoublePctlFromZ(zScore);
			stanine = findStanineFromPctl(percentScore);
			individualReport.getScriptedData().put(ReportingConstants.DR_MICRO, stanine+ "");
			//System.out.println("ReportingConstants.DR_MICRO " + percentScore+ "" + " | " + stanine+ "");

			mean = parse(individualReport.getDisplayData(ReportingConstants.DR_PASAG+"_MEAN"));
			stdev = parse(individualReport.getDisplayData(ReportingConstants.DR_PASAG+"_STDEV"));
			if(tempScores.get(ReportingConstants.DR_PASAG).equals("0.0"))
			{
				zScore = (0.0 - mean) / stdev;
			} else {
				zScore =  ( parse(tempScores.get(ReportingConstants.DR_PASAG) ) - mean  ) / stdev;
			}
			percentScore = Norm.calcDoublePctlFromZ(zScore);
			stanine = findStanineFromPctl(percentScore);
			individualReport.getScriptedData().put(ReportingConstants.DR_PASAG, stanine+ "");
			//System.out.println("ReportingConstants.DR_PASAG " + percentScore+ "" + " | " + stanine+ "" );

		}

		if( individualReport.getReportData().get(ReportingConstants.RC_CAREER_SURVEY) != null)
		{
			// Career Survey components
			mean = parse(individualReport.getDisplayData(ReportingConstants.CS_LDRAS+"_MEAN"));
			stdev = parse(individualReport.getDisplayData(ReportingConstants.CS_LDRAS+"_STDEV"));
			if(tempScores.get(ReportingConstants.CS_LDRAS).equals("0.0"))
			{
				zScore = (0.0 - mean) / stdev;
			} else {
				zScore =  ( parse(tempScores.get(ReportingConstants.CS_LDRAS) ) - mean ) / stdev;
			}
			percentScore = Norm.calcDoublePctlFromZ(zScore);
			stanine = findStanineFromPctl(percentScore);
			individualReport.getScriptedData().put(ReportingConstants.CS_LDRAS, stanine+ "");


			mean = parse(individualReport.getDisplayData(ReportingConstants.CS_CRDRV+"_MEAN"));
			stdev = parse(individualReport.getDisplayData(ReportingConstants.CS_CRDRV+"_STDEV"));
			if(  tempScores.get(ReportingConstants.CS_CRDRV).equals("0.0")   )
			{
				zScore = (0.0 - mean) / stdev;
			} else {
				zScore =  ( parse(tempScores.get(ReportingConstants.CS_CRDRV) ) - mean ) / stdev;
			}
			percentScore = Norm.calcDoublePctlFromZ(zScore);
			stanine = findStanineFromPctl(percentScore);
			individualReport.getScriptedData().put(ReportingConstants.CS_CRDRV, stanine+ "");
			//System.out.println("ReportingConstants.CS_CRDRV " + percentScore+ "" + " | " + stanine+ "");

			mean = parse(individualReport.getDisplayData(ReportingConstants.CS_LRNOR+"_MEAN"));
			stdev = parse(individualReport.getDisplayData(ReportingConstants.CS_LRNOR+"_STDEV"));
			if(tempScores.get(ReportingConstants.CS_LRNOR).equals("0.0"))
			{
				zScore = (0.0 - mean) / stdev;
			} else {
				zScore =  ( (parse(tempScores.get(ReportingConstants.CS_LRNOR)) ) - mean ) / stdev;
			}
			percentScore = Norm.calcDoublePctlFromZ(zScore);
			stanine = findStanineFromPctl(percentScore);
			individualReport.getScriptedData().put(ReportingConstants.CS_LRNOR, stanine+ "");
			//System.out.println("ReportingConstants.CS_LRNOR " + percentScore+ "" + " | " + stanine+ "");

			mean = parse(individualReport.getDisplayData(ReportingConstants.CS_EXPOR+"_MEAN"));
			stdev = parse(individualReport.getDisplayData(ReportingConstants.CS_EXPOR+"_STDEV"));
			if(tempScores.get(ReportingConstants.CS_EXPOR).equals("0.0"))
			{
				zScore = (0.0 - mean) / stdev;
			} else {
				zScore =  ( parse(tempScores.get(ReportingConstants.CS_EXPOR)) - mean  ) / stdev;
			}
			percentScore = Norm.calcDoublePctlFromZ(zScore);
			stanine = findStanineFromPctl(percentScore);
			individualReport.getScriptedData().put(ReportingConstants.CS_EXPOR, stanine+ "");

		}else {
			//set all scores to 0.0
			individualReport.getScriptedData().put(ReportingConstants.CS_LDRAS, "0.0");
			individualReport.getScriptedData().put(ReportingConstants.CS_CRDRV, "0.0");
			individualReport.getScriptedData().put(ReportingConstants.CS_LRNOR, "0.0");
			individualReport.getScriptedData().put(ReportingConstants.CS_EXPOR, "0.0");

		}

		//System.out.println("824   TLT GROUP DETAIL REPORT.JAVA    End generate(IndividualReport)..........");

	}	// End generate(IndividualReport)


	/*
	 * generateGroupData
	 */
	public void generateGroupData(IndividualReport individualReport)
	{
		if(individualReport.getScriptedData().get("STATUS") == "NOT_FINISHED")
		{
			return;
		}

		/*
		 * 	get the zscore normset
		 *		calc the zscores by
		 *		going through each participant's participations, doing scores by component...
		 */
		// sending the individual report, and the group norm data
		calcZscores(individualReport, groupReport.getDisplayData());
		//System.out.println("Step 2: User passed Calc Z");

		/*
		 * 	get the logodds normset
		 *		calc the logods
		 *			going through each participant's participations, weighting scores by component...
		 * 	get the overall potential normset
		 *		calc overall potential
		 *
		 */
		calcPotential(individualReport, groupReport.getDisplayData());
		//System.out.println("Steps 3 & 4: User passed ov pot");

		/*
		 *  Calc raw performance estimate scores, then the overall performance estimate score
		 */
		calcPerformanceEstimate(individualReport, groupReport.getDisplayData());
		//System.out.println("Steps 5 & 6: User ov perf est");

		/*
		 *  Calc leadership
		 */
		calcLeadership(individualReport);
		//System.out.println("Step 7: User leadership");

		return;
	}


	/*
	 * The calcZscores() function calculates Z-Scores for each raw component score:
	 * cognitive, career goals, personality, and leadership.
	 *
	 *
	 * param -
	 */
	public void calcZscores(IndividualReport individualReport, HashMap<String, String> groupNorms)
	{
		//System.out.println("calcZscores(IndividualReport individualReport, HashMap<String, String> groupNorms) 3 ")

		// Cognitive score (zgest) (Ravens')
		// Looking at the old .as code, they simply copy the raw score value (after the
		// first norming) to the group z score
		String scoreStr = individualReport.getScriptedData().get("RAW_" + ReportingConstants.RAV_PSOLV);
		individualReport.getScriptedData().put("GZ_" + ReportingConstants.RAV_PSOLV, scoreStr);
		//System.out.println("RAV_PSOLV GZ: " + scoreStr);

		double rawScore = 0.0;
		double mean = 0.0;
		double stdev = 0.0;
		double score = 0.0;

		// Personality scores
		rawScore = parse(individualReport.getScriptedData().get("RAW_" + ReportingConstants.GPI_INTEN));
		if(rawScore == 0.0)
		{
			score = 0.0;
		} else {
			mean = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_COMPONENT_Z_SCORE+ "_" + ReportingConstants.GPI_INTEN + "_MEAN"));
			stdev = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_COMPONENT_Z_SCORE+ "_" + ReportingConstants.GPI_INTEN + "_STDEV"));
			score = calcZscore(rawScore, mean, stdev);
		}
		individualReport.getScriptedData().put("GZ_" + ReportingConstants.GPI_INTEN, score+ "");
		//System.out.println("GPI_INTEN GZ:  " + score + (score == 0.0 ? " (No Z calc)" : " (mean=" + mean + ", stdv=" + stdev +", raw=" + rawScore + ")"));

		rawScore = parse(individualReport.getScriptedData().get("RAW_" + ReportingConstants.GPI_ATD));
		if(rawScore == 0.0)
		{
			score = 0.0;
		} else {
			mean = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_COMPONENT_Z_SCORE+ "_" + ReportingConstants.GPI_ATD + "_MEAN"));
			stdev = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_COMPONENT_Z_SCORE+ "_" + ReportingConstants.GPI_ATD + "_STDEV"));
			score = calcZscore(rawScore, mean, stdev);
		}
		individualReport.getScriptedData().put("GZ_" + ReportingConstants.GPI_ATD, score+ "");
		//System.out.println("GPI_ATD GZ:  " + score + (score == 0.0 ? " (No Z calc)" : " (mean=" + mean + ", stdv=" + stdev +", raw=" + rawScore + ")"));

		rawScore = parse(individualReport.getScriptedData().get("RAW_" + ReportingConstants.GPI_IMIN));
		if(rawScore == 0.0)
		{
			score = 0.0;
		} else {
			mean = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_COMPONENT_Z_SCORE+ "_" + ReportingConstants.GPI_IMIN + "_MEAN"));
			stdev = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_COMPONENT_Z_SCORE+ "_" + ReportingConstants.GPI_IMIN + "_STDEV"));
			score = calcZscore(rawScore, mean, stdev);
		}
		individualReport.getScriptedData().put("GZ_" + ReportingConstants.GPI_IMIN, score+ "");
		//System.out.println("GPI_IMIN GZ:  " + score + (score == 0.0 ? " (No Z calc)" : " (mean=" + mean + ", stdv=" + stdev +", raw=" + rawScore + ")"));

		rawScore = parse(individualReport.getScriptedData().get("RAW_" + ReportingConstants.GPI_INEN));
		if(rawScore == 0.0)
		{
			score = 0.0;
		} else {
			mean = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_COMPONENT_Z_SCORE+ "_" + ReportingConstants.GPI_INEN + "_MEAN"));
			stdev = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_COMPONENT_Z_SCORE+ "_" + ReportingConstants.GPI_INEN + "_STDEV"));
			score = calcZscore(rawScore, mean, stdev);
		}
		individualReport.getScriptedData().put("GZ_" + ReportingConstants.GPI_INEN, score+ "");
		//System.out.println("GPI_INEN GZ:  " + score + (score == 0.0 ? " (No Z calc)" : " (mean=" + mean + ", stdv=" + stdev +", raw=" + rawScore + ")"));

		rawScore = parse(individualReport.getScriptedData().get("RAW_" + ReportingConstants.GPI_ACHD));
		if(rawScore == 0.0)
		{
			score = 0.0;
		} else {
			mean = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_COMPONENT_Z_SCORE+ "_" + ReportingConstants.GPI_ACHD + "_MEAN"));
			stdev = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_COMPONENT_Z_SCORE+ "_" + ReportingConstants.GPI_ACHD + "_STDEV"));
			score = calcZscore(rawScore, mean, stdev);
		}
		individualReport.getScriptedData().put("GZ_" + ReportingConstants.GPI_ACHD, score+ "");
		//System.out.println("GPI_ACHD GZ:  " + score + (score == 0.0 ? " (No Z calc)" : " (mean=" + mean + ", stdv=" + stdev +", raw=" + rawScore + ")"));

		rawScore = parse(individualReport.getScriptedData().get("RAW_" + ReportingConstants.GPI_ADVD));
		if(rawScore == 0.0)
		{
			score = 0.0;
		} else {
			mean = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_COMPONENT_Z_SCORE+ "_" + ReportingConstants.GPI_ADVD + "_MEAN"));
			stdev = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_COMPONENT_Z_SCORE+ "_" + ReportingConstants.GPI_ADVD + "_STDEV"));
			score = calcZscore(rawScore, mean, stdev);
		}
		individualReport.getScriptedData().put("GZ_" + ReportingConstants.GPI_ADVD, score+ "");
		//System.out.println("GPI_ADVD GZ:  " + score + (score == 0.0 ? " (No Z calc)" : " (mean=" + mean + ", stdv=" + stdev +", raw=" + rawScore + ")"));

		rawScore = parse(individualReport.getScriptedData().get("RAW_" + ReportingConstants.GPI_COOR));
		if(rawScore == 0.0)
		{
			score = 0.0;
		} else {
			mean = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_COMPONENT_Z_SCORE+ "_" + ReportingConstants.GPI_COOR + "_MEAN"));
			stdev = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_COMPONENT_Z_SCORE+ "_" + ReportingConstants.GPI_COOR + "_STDEV"));
			score = calcZscore(rawScore, mean, stdev);
		}
		individualReport.getScriptedData().put("GZ_" + ReportingConstants.GPI_COOR, score+ "");
		//System.out.println("GPI_COOR GZ:  " + score + (score == 0.0 ? " (No Z calc)" : " (mean=" + mean + ", stdv=" + stdev +", raw=" + rawScore + ")"));

		rawScore = parse(individualReport.getScriptedData().get("RAW_" + ReportingConstants.GPI_FLAD));
		if(rawScore == 0.0)
		{
			score = 0.0;
		} else {
			mean = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_COMPONENT_Z_SCORE+ "_" + ReportingConstants.GPI_FLAD + "_MEAN"));
			stdev = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_COMPONENT_Z_SCORE+ "_" + ReportingConstants.GPI_FLAD + "_STDEV"));
			score = calcZscore(rawScore, mean, stdev);
		}
		individualReport.getScriptedData().put("GZ_" + ReportingConstants.GPI_FLAD, score+ "");
		//System.out.println("GPI_FLAD GZ:  " + score + (score == 0.0 ? " (No Z calc)" : " (mean=" + mean + ", stdv=" + stdev +", raw=" + rawScore + ")"));

		// Leadership scores
		rawScore = parse(individualReport.getScriptedData().get("RAW_" + ReportingConstants.LEI_HTC));
		if(rawScore == 0.0)
		{
			score = 0.0;
		} else {
			mean = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_COMPONENT_Z_SCORE+ "_" + ReportingConstants.LEI_HTC + "_MEAN"));
			stdev = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_COMPONENT_Z_SCORE+ "_" + ReportingConstants.LEI_HTC + "_STDEV"));
			score = calcZscore(rawScore, mean, stdev);
		}
		individualReport.getScriptedData().put("GZ_" + ReportingConstants.LEI_HTC, score+ "");
		//System.out.println("  LEI_HTC GZ:  " + score + (score == 0.0 ? " (No Z calc)" : " (mean=" + mean + ", stdv=" + stdev +", raw=" + rawScore + ")"));

		rawScore = parse(individualReport.getScriptedData().get("RAW_" + ReportingConstants.LEI_GTBUS));
		if(rawScore == 0.0)
		{
			score = 0.0;
		} else {
			mean = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_COMPONENT_Z_SCORE+ "_" + ReportingConstants.LEI_GTBUS + "_MEAN"));
			stdev = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_COMPONENT_Z_SCORE+ "_" + ReportingConstants.LEI_GTBUS + "_STDEV"));
			score = calcZscore(rawScore, mean, stdev);
		}
		individualReport.getScriptedData().put("GZ_" + ReportingConstants.LEI_GTBUS, score+ "");
		//System.out.println("  LEI_GTBUS GZ:  " + score + (score == 0.0 ? " (No Z calc)" : " (mean=" + mean + ", stdv=" + stdev +", raw=" + rawScore + ")"));

		rawScore = parse(individualReport.getScriptedData().get("RAW_" + ReportingConstants.LEI_HVIS));
		if(rawScore == 0.0)
		{
			score = 0.0;
		} else {
			mean = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_COMPONENT_Z_SCORE+ "_" + ReportingConstants.LEI_HVIS + "_MEAN"));
			stdev = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_COMPONENT_Z_SCORE+ "_" + ReportingConstants.LEI_HVIS + "_STDEV"));
			score = calcZscore(rawScore, mean, stdev);
		}
		individualReport.getScriptedData().put("GZ_" + ReportingConstants.LEI_HVIS, score+ "");
		//System.out.println("  LEI_HVIS GZ:  " + score + (score == 0.0 ? " (No Z calc)" : " (mean=" + mean + ", stdv=" + stdev +", raw=" + rawScore + ")"));

		rawScore = parse(individualReport.getScriptedData().get("RAW_" + ReportingConstants.LEI_BUSOP));
		if(rawScore == 0.0)
		{
			score = 0.0;
		} else {
			mean = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_COMPONENT_Z_SCORE+ "_" + ReportingConstants.LEI_BUSOP + "_MEAN"));
			stdev = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_COMPONENT_Z_SCORE+ "_" + ReportingConstants.LEI_BUSOP + "_STDEV"));
			score = calcZscore(rawScore, mean, stdev);
		}
		individualReport.getScriptedData().put("GZ_" + ReportingConstants.LEI_BUSOP, score+ "");
		//System.out.println("  LEI_BUSOP GZ:  " + score + (score == 0.0 ? " (No Z calc)" : " (mean=" + mean + ", stdv=" + stdev +", raw=" + rawScore + ")"));

		rawScore = parse(individualReport.getScriptedData().get("RAW_" + ReportingConstants.LEI_PDEV));
		if(rawScore == 0.0)
		{
			score = 0.0;
		} else {
			mean = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_COMPONENT_Z_SCORE+ "_" + ReportingConstants.LEI_PDEV + "_MEAN"));
			stdev = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_COMPONENT_Z_SCORE+ "_" + ReportingConstants.LEI_PDEV + "_STDEV"));
			score = calcZscore(rawScore, mean, stdev);
		}
		individualReport.getScriptedData().put("GZ_" + ReportingConstants.LEI_PDEV, score+ "");
		//System.out.println("  LEI_PDEV GZ:  " + score + (score == 0.0 ? " (No Z calc)" : " (mean=" + mean + ", stdv=" + stdev +", raw=" + rawScore + ")"));

		// Derailment factors
		rawScore = parse(individualReport.getScriptedData().get("RAW_" + ReportingConstants.DR_DERAIL));
		if(rawScore == 0.0)
		{
			score = 0.0;
		} else {
			mean = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_COMPONENT_Z_SCORE+ "_" + ReportingConstants.DR_DERAIL + "_MEAN"));
			stdev = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_COMPONENT_Z_SCORE+ "_" + ReportingConstants.DR_DERAIL + "_STDEV"));
			score = calcZscore(rawScore, mean, stdev);
		}
		individualReport.getScriptedData().put("GZ_" + ReportingConstants.DR_DERAIL, score+ "");
		//println "  DR raw score=" + rawScore
		//println "  DR Group Z key=" + ReportingConstants.TPOT2_GROUP_COMPONENT_Z_SCORE+ "_" + ReportingConstants.DR_DERAIL
		//println "  DR Group Z mean=" + groupNorms.get(ReportingConstants.TPOT2_GROUP_COMPONENT_Z_SCORE+ "_" + ReportingConstants.DR_DERAIL + "_MEAN")
		//println "  DR Group Z std=" + groupNorms.get(ReportingConstants.TPOT2_GROUP_COMPONENT_Z_SCORE+ "_" + ReportingConstants.DR_DERAIL + "_STDEV")
		//System.out.println("  DR_DERAIL GZ:  " + score + (score == 0.0 ? " (No Z calc)" : " (mean=" + mean + ", stdv=" + stdev +", raw=" + rawScore + ")"));
	}


	/*
	 * calcZscore
	 */
	private double calcZscore(double rawScore, double mean, double stddev)
	{
		//System.out.println("calcZscore(double rawScore, double mean, double stddev) 4 ")
		double score = 0.0;
		score = (rawScore - mean) / stddev;
		return score;
	}


	/*
	 * The calcPotential() function calculates Raw score overall potential (logodds),
	 * then rolls the scores up for the Overall Potential score.
	 *
	 * NOTE:  Assuming the caller of this function has already retrieved one of these normsets
	 * and is passing one of them to this function:
	 *
	 * Individual Potential scores
	 * displayName						scoringStepName						transitionLevel
	 * TPOT2_Group_Logodds_From_Zscor	TPOT2_Group_Logodds_From_Zscor		1, 2, 3, 4, OR 5
	 *
	 * Overall Potential
	 * displayName						scoringStepName						transitionLevel
	 * TPOT2_Group_Logodds				TPOT2_Group_Logodds					1, 2, 3, 4, OR 5

	 *
	 * param - normset:Normset
	 */
	public void calcPotential(IndividualReport individualReport, HashMap<String, String> groupNorms)
	{
		double zScore = 0.0;
		double weight = 0.0;
		double score = 0.0;

		ArrayList<Double> weightedScores = new ArrayList<Double>();

		// Cognitive score (zgest)
		if (individualReport.getScriptedData().get("GZ_" + ReportingConstants.RAV_PSOLV).length() > 0)
		{
			// Only do this if there is a zgest score
			zScore = parse(individualReport.getScriptedData().get("GZ_" + ReportingConstants.RAV_PSOLV));
			weight = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_LOGODDS_FROM_ZSCOR + "_" + ReportingConstants.RAV_PSOLV + "_WEIGHT"));
			score = calcLogodd(zScore, weight);
			weightedScores.add(new Double(score));
			//System.out.println("LO (RAV_PSOLV) GZ:  " + score + (score == 0.0 ? " (No weighting)" : " (weight=" + weight + ", zscore=" + zScore + ")"));
		}
		// Personality scores
		zScore = parse(individualReport.getScriptedData().get("GZ_" + ReportingConstants.GPI_INTEN));
		weight = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_LOGODDS_FROM_ZSCOR + "_" + ReportingConstants.GPI_INTEN + "_WEIGHT"));
		score = calcLogodd(zScore, weight);
		weightedScores.add(new Double(score));
		//System.out.println("LO (GPI_INTEN) GZ:  " + score + (score == 0.0 ? " (No weighting)" : " (weight=" + weight + ", zscore=" + zScore + ")"));

		zScore = parse(individualReport.getScriptedData().get("GZ_" + ReportingConstants.GPI_ATD));
		weight = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_LOGODDS_FROM_ZSCOR + "_" + ReportingConstants.GPI_ATD + "_WEIGHT"));
		score = calcLogodd(zScore, weight);
		weightedScores.add(new Double(score));
		//System.out.println("LO (GPI_ATD) GZ:  " + score + (score == 0.0 ? " (No weighting)" : " (weight=" + weight + ", zscore=" + zScore + ")"));

		zScore = parse(individualReport.getScriptedData().get("GZ_" + ReportingConstants.GPI_IMIN));
		weight = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_LOGODDS_FROM_ZSCOR + "_" + ReportingConstants.GPI_IMIN + "_WEIGHT"));
		score = calcLogodd(zScore, weight);
		weightedScores.add(new Double(score));
		//System.out.println("LO (GPI_IMIN) GZ:  " + score + (score == 0.0 ? " (No weighting)" : " (weight=" + weight + ", zscore=" + zScore + ")"));

		zScore = parse(individualReport.getScriptedData().get("GZ_" + ReportingConstants.GPI_INEN));
		weight = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_LOGODDS_FROM_ZSCOR + "_" + ReportingConstants.GPI_INEN + "_WEIGHT"));
		score = calcLogodd(zScore, weight);
		weightedScores.add(new Double(score));
		//System.out.println("LO (GPI_INEN) GZ:  " + score + (score == 0.0 ? " (No weighting)" : " (weight=" + weight + ", zscore=" + zScore + ")"));

		zScore = parse(individualReport.getScriptedData().get("GZ_" + ReportingConstants.GPI_ACHD));
		weight = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_LOGODDS_FROM_ZSCOR + "_" + ReportingConstants.GPI_ACHD + "_WEIGHT"));
		score = calcLogodd(zScore, weight);
		weightedScores.add(new Double(score));
		//System.out.println("LO (GPI_ACHD) GZ:  " + score + (score == 0.0 ? " (No weighting)" : " (weight=" + weight + ", zscore=" + zScore + ")"));

		zScore = parse(individualReport.getScriptedData().get("GZ_" + ReportingConstants.GPI_ADVD));
		weight = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_LOGODDS_FROM_ZSCOR + "_" + ReportingConstants.GPI_ADVD + "_WEIGHT"));
		score = calcLogodd(zScore, weight);
		weightedScores.add(new Double(score));
		//System.out.println("LO (GPI_ADVD) GZ:  " + score + (score == 0.0 ? " (No weighting)" : " (weight=" + weight + ", zscore=" + zScore + ")"));

		zScore = parse(individualReport.getScriptedData().get("GZ_" + ReportingConstants.GPI_COOR));
		weight = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_LOGODDS_FROM_ZSCOR + "_" + ReportingConstants.GPI_COOR + "_WEIGHT"));
		score = calcLogodd(zScore, weight);
		weightedScores.add(new Double(score));
		//System.out.println("LO (GPI_COOR) GZ:  " + score + (score == 0.0 ? " (No weighting)" : " (weight=" + weight + ", zscore=" + zScore + ")"));

		zScore = parse(individualReport.getScriptedData().get("GZ_" + ReportingConstants.GPI_FLAD));
		weight = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_LOGODDS_FROM_ZSCOR + "_" + ReportingConstants.GPI_FLAD + "_WEIGHT"));
		score = calcLogodd(zScore, weight);
		weightedScores.add(new Double(score));
		//System.out.println("LO (GPI_FLAD) GZ:  " + score + (score == 0.0 ? " (No weighting)" : " (weight=" + weight + ", zscore=" + zScore + ")"));


		// Leadership scores
		zScore = parse(individualReport.getScriptedData().get("GZ_" + ReportingConstants.LEI_HTC));
		weight = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_LOGODDS_FROM_ZSCOR + "_" + ReportingConstants.LEI_HTC + "_WEIGHT"));
		score = calcLogodd(zScore, weight);
		weightedScores.add(new Double(score));
		//System.out.println("LO (LEI_HTC) GZ:  " + score + (score == 0.0 ? " (No weighting)" : " (weight=" + weight + ", zscore=" + zScore + ")"));

		zScore = parse(individualReport.getScriptedData().get("GZ_" + ReportingConstants.LEI_GTBUS));
		weight = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_LOGODDS_FROM_ZSCOR + "_" + ReportingConstants.LEI_GTBUS + "_WEIGHT"));
		score = calcLogodd(zScore, weight);
		weightedScores.add(new Double(score));
		//System.out.println("LO (LEI_GTBUS) GZ:  " + score + (score == 0.0 ? " (No weighting)" : " (weight=" + weight + ", zscore=" + zScore + ")"));

		zScore = parse(individualReport.getScriptedData().get("GZ_" + ReportingConstants.LEI_HVIS));
		weight = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_LOGODDS_FROM_ZSCOR + "_" + ReportingConstants.LEI_HVIS + "_WEIGHT"));
		score = calcLogodd(zScore, weight);
		weightedScores.add(new Double(score));
		//System.out.println("LO (LEI_HVIS) GZ:  " + score + (score == 0.0 ? " (No weighting)" : " (weight=" + weight + ", zscore=" + zScore + ")"));

		zScore = parse(individualReport.getScriptedData().get("GZ_" + ReportingConstants.LEI_BUSOP));
		weight = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_LOGODDS_FROM_ZSCOR + "_" + ReportingConstants.LEI_BUSOP + "_WEIGHT"));
		score = calcLogodd(zScore, weight);
		weightedScores.add(new Double(score));
		//System.out.println("LO (LEI_BUSOP) GZ:  " + score + (score == 0.0 ? " (No weighting)" : " (weight=" + weight + ", zscore=" + zScore + ")"));

		zScore = parse(individualReport.getScriptedData().get("GZ_" + ReportingConstants.LEI_PDEV));
		weight = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_LOGODDS_FROM_ZSCOR + "_" + ReportingConstants.LEI_PDEV + "_WEIGHT"));
		score = calcLogodd(zScore, weight);
		weightedScores.add(new Double(score));
		//System.out.println("LO (LEI_PDEV) GZ:  " + score + (score == 0.0 ? " (No weighting)" : " (weight=" + weight + ", zscore=" + zScore + ")"));

		// Derailment factors
		zScore = parse(individualReport.getScriptedData().get("GZ_" + ReportingConstants.DR_DERAIL));
		weight = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_LOGODDS_FROM_ZSCOR + "_" + ReportingConstants.DR_DERAIL + "_WEIGHT"));
		score = calcLogodd(zScore, weight);
		weightedScores.add(new Double(score));
		//System.out.println("LO (DR_DERAIL) GZ:  " + score + (score == 0.0 ? " (No weighting)" : " (weight=" + weight + ", zscore=" + zScore + ")"));

		// Add the weighted Z-Scores and the transition level constant value.
		double logoddSum;

		// Transition Logodd constant
		double transitionLevelConstant = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_LOGODDS_FROM_ZSCOR + "_" + ReportingConstants.LOGODDS_CONSTANT + "_WEIGHT"));
		//println "transitionLevelConstant=" + transitionLevelConstant
		logoddSum = transitionLevelConstant;
		for (int i = 0; i < weightedScores.size(); i++)
		{
			Double groupWeightedZScore = weightedScores.get(i);
			logoddSum += groupWeightedZScore;
		}
		//System.out.println("logoddSum ; " + logoddSum);

		// add sum to report
		individualReport.getScriptedData().put(ReportingConstants.OV_OVERALL + ReportingConstants.POTENTIAL_LOGODD, logoddSum+ "");

		// OK now calc the overall postential score
		// Note that the stanines could end up a little odd if there
		// are significant portions of the LEI or GPI not completed
		double overallScore = logoddSum;
		double mean = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_LOGODDS + "__MEAN"));
		double stdev = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_LOGODDS + "__STDEV"));

		double potentialLogoddZScore = 	( overallScore - mean ) / stdev;
		double potentialLogoddPctl = Norm.calcIntPctlFromZ( potentialLogoddZScore );
		int potentialStanine = findStanineFromPctl( potentialLogoddPctl );
		// set overall performance score...
		individualReport.getScriptedData().put(ReportingConstants.OV_OVERALL + ReportingConstants.POTENTIAL_STANINE, potentialStanine+ "");
		//System.out.println("OV POT for " + individualReport.displayData.get("LAST_NAME") + ", " + individualReport.displayData.get("FIRST_NAME") + ":");
		//System.out.println("         mean=" + mean + ", stdev=" + stdev + ", score=" + overallScore);
		//System.out.println("         potentialLogoddZScore :  " + potentialLogoddZScore);
		//System.out.println("         potentialLogoddPctl :  " + potentialLogoddPctl);
		//System.out.println("         potentialStanine :  " + potentialStanine);
 	}


	/*
	 * calcLogodd determines a Logodd for a given score value and norm.
	 *
	 * param - aConstructName:String
	 * param - aComponentName:String
	 * param - aNormset:Normset
	 * returns - Logodds:Number
	 */
	public double calcLogodd(double zScore, double weight)
	{
		//System.out.println("calcLogodd(double zScore, double weight)  6 ")

		double groupWeightedZScore = zScore * weight;
		return groupWeightedZScore;
	}


	/*
	 * The calcRawPerformanceEstimates() function calculates Raw PerformanceEstimate scores.
	 *
	 * NOTE:  Assuming the caller of this function has already retrieved one of these normsets
	 * and is passing one of them to this function:
	 *
	 * Raw scores
	 * displayName						scoringStepName						transitionLevel
	 * TPOT2_Group_Performance_From_Z	TPOT2_Group_Performance_From_Z		6, 7, OR 8
	 *
	 * Overall scores
	 * displayName						scoringStepName						transitionLevel
	 * TPOT2_Group_Performance			TPOT2_Group_Performance				6, 7, OR 8
	 *
	 * param - project:Project
	 * param - normset:Normset
	 */
	public void calcPerformanceEstimate(IndividualReport individualReport, HashMap<String, String> groupNorms)
	{
		double groupZScore = 0.0;
		double weight = 0.0;
		double score = 0.0;

		ArrayList<Double> perfEstimatedScores = new ArrayList<Double>();
		ArrayList<Double> perfEstimatedScoresNoCog = new ArrayList<Double>();

		// Cognitive score (zgest)
		if (individualReport.getScriptedData().get("GZ_" + ReportingConstants.RAV_PSOLV).length() > 0)
		{
			// Only do this if there is a score
			groupZScore = parse(individualReport.getScriptedData().get("GZ_" + ReportingConstants.RAV_PSOLV));
			weight = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_PERFORMANCE_FROM_Z + "_" + ReportingConstants.RAV_PSOLV + "_WEIGHT"));
			score = calcRawPerformanceEstimate(groupZScore, weight);
			perfEstimatedScores.add(new Double(score));
			perfEstimatedScoresNoCog.add(new Double(score));
			//System.out.println("RAV_PSOLV perf:  " + score + (score == 0.0 ? " (No weighting)" : " (weight=" + weight + ", zscore=" + groupZScore + ")"));
		}
		
		// Personality scores
		groupZScore = parse(individualReport.getScriptedData().get("GZ_" + ReportingConstants.GPI_INTEN));
		weight = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_PERFORMANCE_FROM_Z + "_" + ReportingConstants.GPI_INTEN + "_WEIGHT"));
		score = calcRawPerformanceEstimate(groupZScore, weight);
		perfEstimatedScores.add(new Double(score));
		perfEstimatedScoresNoCog.add(new Double(score));
		//System.out.println("GPI_INTEN perf:  " + score + (score == 0.0 ? " (No weighting)" : " (weight=" + weight + ", zscore=" + groupZScore + ")"));

		groupZScore = parse(individualReport.getScriptedData().get("GZ_" + ReportingConstants.GPI_ATD));
		weight = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_PERFORMANCE_FROM_Z + "_" + ReportingConstants.GPI_ATD + "_WEIGHT"));
		score = calcRawPerformanceEstimate(groupZScore, weight);
		perfEstimatedScores.add(new Double(score));
		perfEstimatedScoresNoCog.add(new Double(score));
		//System.out.println("GPI_ATD perf:  " + score + (score == 0.0 ? " (No weighting)" : " (weight=" + weight + ", zscore=" + groupZScore + ")"));

		groupZScore = parse(individualReport.getScriptedData().get("GZ_" + ReportingConstants.GPI_IMIN));
		weight = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_PERFORMANCE_FROM_Z + "_" + ReportingConstants.GPI_IMIN + "_WEIGHT"));
		score = calcRawPerformanceEstimate(groupZScore, weight);
		perfEstimatedScores.add(new Double(score));
		perfEstimatedScoresNoCog.add(new Double(score));
		//System.out.println("GPI_IMIN perf:  " + score + (score == 0.0 ? " (No weighting)" : " (weight=" + weight + ", zscore=" + groupZScore + ")"));

		groupZScore = parse(individualReport.getScriptedData().get("GZ_" + ReportingConstants.GPI_INEN));
		weight = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_PERFORMANCE_FROM_Z + "_" + ReportingConstants.GPI_INEN + "_WEIGHT"));
		score = calcRawPerformanceEstimate(groupZScore, weight);
		perfEstimatedScores.add(new Double(score));
		perfEstimatedScoresNoCog.add(new Double(score));
		//System.out.println("GPI_INEN perf:  " + score + (score == 0.0 ? " (No weighting)" : " (weight=" + weight + ", zscore=" + groupZScore + ")"));

		groupZScore = parse(individualReport.getScriptedData().get("GZ_" + ReportingConstants.GPI_ACHD));
		weight = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_PERFORMANCE_FROM_Z + "_" + ReportingConstants.GPI_ACHD + "_WEIGHT"));
		score = calcRawPerformanceEstimate(groupZScore, weight);
		perfEstimatedScores.add(new Double(score));
		perfEstimatedScoresNoCog.add(new Double(score));
		//System.out.println("GPI_ACHD perf:  " + score + (score == 0.0 ? " (No weighting)" : " (weight=" + weight + ", zscore=" + groupZScore + ")"));

		groupZScore = parse(individualReport.getScriptedData().get("GZ_" + ReportingConstants.GPI_ADVD));
		weight = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_PERFORMANCE_FROM_Z + "_" + ReportingConstants.GPI_ADVD + "_WEIGHT"));
		score = calcRawPerformanceEstimate(groupZScore, weight);
		perfEstimatedScores.add(new Double(score));
		perfEstimatedScoresNoCog.add(new Double(score));
		//System.out.println("GPI_ADVD perf:  " + score + (score == 0.0 ? " (No weighting)" : " (weight=" + weight + ", zscore=" + groupZScore + ")"));

		groupZScore = parse(individualReport.getScriptedData().get("GZ_" + ReportingConstants.GPI_COOR));
		weight = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_PERFORMANCE_FROM_Z + "_" + ReportingConstants.GPI_COOR + "_WEIGHT"));
		score = calcRawPerformanceEstimate(groupZScore, weight);
		perfEstimatedScores.add(new Double(score));
		perfEstimatedScoresNoCog.add(new Double(score));
		//System.out.println("GPI_COOR perf:  " + score + (score == 0.0 ? " (No weighting)" : " (weight=" + weight + ", zscore=" + groupZScore + ")"));

		groupZScore = parse(individualReport.getScriptedData().get("GZ_" + ReportingConstants.GPI_FLAD));
		weight = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_PERFORMANCE_FROM_Z + "_" + ReportingConstants.GPI_FLAD + "_WEIGHT"));
		score = calcRawPerformanceEstimate(groupZScore, weight);
		perfEstimatedScores.add(new Double(score));
		perfEstimatedScoresNoCog.add(new Double(score));
		//System.out.println("GPI_FLAD perf:  " + score + (score == 0.0 ? " (No weighting)" : " (weight=" + weight + ", zscore=" + groupZScore + ")"));

		// Leadership scores
		// NOTE:  For the IC->FLL transition level ("1"), there is insufficient LEI data
		//        to use the leadership components. Exclude the five leadership components
		//        when computing estimated performance.
		//System.out.println("TransitionLevel() = " + groupReport.getTransitionLevel());
		if ( groupReport.getTransitionLevel() != 1)
		{
			//println "---------> We got cogs <---------"
			groupZScore = parse(individualReport.getScriptedData().get("GZ_" + ReportingConstants.LEI_HTC));
			weight = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_PERFORMANCE_FROM_Z + "_" + ReportingConstants.LEI_HTC + "_WEIGHT"));
			score = calcRawPerformanceEstimate(groupZScore, weight);
			perfEstimatedScores.add(new Double(score));
			//System.out.println("LEI_HTC perf:  " + score + (score == 0.0 ? " (No weighting)" : " (weight=" + weight + ", zscore=" + groupZScore + ")"));

			groupZScore = parse(individualReport.getScriptedData().get("GZ_" + ReportingConstants.LEI_GTBUS));
			weight = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_PERFORMANCE_FROM_Z + "_" + ReportingConstants.LEI_GTBUS + "_WEIGHT"));
			score = calcRawPerformanceEstimate(groupZScore, weight);
			perfEstimatedScores.add(new Double(score));
			//System.out.println("LEI_GTBUS perf:  " + score + (score == 0.0 ? " (No weighting)" : " (weight=" + weight + ", zscore=" + groupZScore + ")"));

			groupZScore = parse(individualReport.getScriptedData().get("GZ_" + ReportingConstants.LEI_HVIS));
			weight = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_PERFORMANCE_FROM_Z + "_" + ReportingConstants.LEI_HVIS + "_WEIGHT"));
			score = calcRawPerformanceEstimate(groupZScore, weight);
			perfEstimatedScores.add(new Double(score));
			//System.out.println("LEI_HVIS perf:  " + score + (score == 0.0 ? " (No weighting)" : " (weight=" + weight + ", zscore=" + groupZScore + ")"));

			groupZScore = parse(individualReport.getScriptedData().get("GZ_" + ReportingConstants.LEI_BUSOP));
			weight = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_PERFORMANCE_FROM_Z + "_" + ReportingConstants.LEI_BUSOP + "_WEIGHT"));
			score = calcRawPerformanceEstimate(groupZScore, weight);
			perfEstimatedScores.add(new Double(score));
			//System.out.println("LEI_BUSOP perf:  " + score + (score == 0.0 ? " (No weighting)" : " (weight=" + weight + ", zscore=" + groupZScore + ")"));

			groupZScore = parse(individualReport.getScriptedData().get("GZ_" + ReportingConstants.LEI_PDEV));
			weight = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_PERFORMANCE_FROM_Z + "_" + ReportingConstants.LEI_PDEV + "_WEIGHT"));
			score = calcRawPerformanceEstimate(groupZScore, weight);
			perfEstimatedScores.add(new Double(score));
			//System.out.println("LEI_PDEV perf:  " + score + (score == 0.0 ? " (No weighting)" : " (weight=" + weight + ", zscore=" + groupZScore + ")"));
		}

		// Derailment factors
		groupZScore = parse(individualReport.getScriptedData().get("GZ_" + ReportingConstants.DR_DERAIL));
		weight = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_PERFORMANCE_FROM_Z + "_" + ReportingConstants.DR_DERAIL + "_WEIGHT"));
		score = calcRawPerformanceEstimate(groupZScore, weight);
		perfEstimatedScores.add(new Double(score));
		perfEstimatedScoresNoCog.add(new Double(score));
		//System.out.println("DR_DERAIL perf:  " + score + (score == 0.0 ? " (No weighting)" : " (weight=" + weight + ", zscore=" + groupZScore + ")"));

		// Add the weighted Z-Scores and the target level constant value.
		// Start with the target level constant
		double perfEstSum = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_PERFORMANCE_FROM_Z + "_" + ReportingConstants.LOGODDS_CONSTANT + "_WEIGHT"));
		//println "TargetTransitionLevel constant=" + perfEstSum
		//println "TransitionLevel number= " + groupReport.getTransitionLevel();
		if ( groupReport.getTransitionLevel() == 1)
		{
			// NOTE:  For the IC->FLL transition level ("1"), there is insufficient LEI
			//        data to use the leadership components. Exclude the five leadership
			//        components when computing estimated performance.
			for(int i = 0; i < perfEstimatedScoresNoCog.size(); i++ )
			{
				perfEstSum += (perfEstimatedScoresNoCog.get(i));
			}
		} else {
			for(int i = 0; i < perfEstimatedScores.size(); i++ )
			{
				perfEstSum += perfEstimatedScores.get(i);
			}
		}

		individualReport.getScriptedData().put(ReportingConstants.OV_OVERALL + ReportingConstants.PERF_EST_SCORE, perfEstSum+ "");
		//System.out.println("perfEstSum = " + perfEstSum);

		// Now calc the overall perf est
		double perfEstScore = perfEstSum;
		double mean = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_PERFORMANCE + "__MEAN"));
		double stdev = parse(groupNorms.get(ReportingConstants.TPOT2_GROUP_PERFORMANCE + "__STDEV"));

		double performanceEstZScore = (perfEstScore - mean ) / stdev;
		double performanceEstPctl = Norm.calcIntPctlFromZ( performanceEstZScore );
		int performanceEstStanine = findStanineFromPctl( performanceEstPctl );
		//System.out.println("PERF_EST for " + individualReport.displayData.get("LAST_NAME") + ", " + individualReport.displayData.get("FIRST_NAME") + ":");
		//System.out.println("          mean=" + mean + ", stdev=" + stdev + ", perfEstScore=" + perfEstScore);
		//System.out.println("          performanceEstZScore : " + performanceEstZScore);
		//System.out.println("          performanceEstPctl : " + performanceEstPctl);
		//System.out.println("          performanceEstStanine : " + performanceEstStanine);

		// add score to report
		individualReport.getScriptedData().put(ReportingConstants.OV_OVERALL + ReportingConstants.PERF_EST_STANINE, performanceEstStanine+ "");
	}


	/*
	 * calcRawPerformanceEstimate determines ...
	 *
	 * param - aConstructName:String
	 * param - aComponentName:String
	 * param - aNormset:Normset
	 */
	public double calcRawPerformanceEstimate(double groupZScore, double weight)
	{

		double groupWeightedPerfEstZScore = groupZScore * weight;

		if (Double.isNaN(groupWeightedPerfEstZScore))
		{
			groupWeightedPerfEstZScore = 0.0;
		}
		return groupWeightedPerfEstZScore;
	}


	/*
	 * The calcLeadership() function determines the three Leadership on/off values.
	 *
	 * param - project:Project
	 *
	 */
	public void calcLeadership(IndividualReport individualReport)
	{
		if(individualReport.getScriptedGroupData() == null)
		{
			return;
		}
		// Leadership Interest
		//println "LE_INT precursors:  CS_LDRAS=" + individualReport.getScriptedData().get(ReportingConstants.CS_LDRAS) +
		//							", CS_CRDRV=" + individualReport.getScriptedData().get(ReportingConstants.CS_CRDRV)
		if ( parse(individualReport.getScriptedData().get(ReportingConstants.CS_LDRAS)) > 3 &&
			 parse(individualReport.getScriptedData().get(ReportingConstants.CS_CRDRV)) > 3 )
		{
			individualReport.getScriptedGroupData().put("LE_INTER_FINALSCORE", new Double(1));
			//println "inter = 1"
		} else {
			individualReport.getScriptedGroupData().put("LE_INTER_FINALSCORE", new Double(0));
			//println "inter = 0"
		}

		// Leadership Experience
		//System.out.println(">>>>>>>>>>Transition Level=" + groupReport.getTargetTransitionLevel());
		if (groupReport.getTargetTransitionLevel() == 6)
		{
			// FLL
			if ( parse(individualReport.getScriptedData().get(ReportingConstants.LEI_BUSOP)) > 3 &&
				 parse(individualReport.getScriptedData().get(ReportingConstants.LEI_GTBUS)) > 3 )
			{
				individualReport.getScriptedGroupData().put("LE_EXPER_FINALSCORE", new Double(1));
			} else {
				individualReport.getScriptedGroupData().put("LE_EXPER_FINALSCORE", new Double(0));
			}
		}
		else if (groupReport.getTargetTransitionLevel() == 7)
		{
			// MLL
			if ( parse(individualReport.getScriptedData().get(ReportingConstants.LEI_BUSOP)) > 3 &&
				 parse(individualReport.getScriptedData().get(ReportingConstants.LEI_HTC)) > 3 &&
			     parse(individualReport.getScriptedData().get(ReportingConstants.LEI_GTBUS)) > 3 )
			{
				individualReport.getScriptedGroupData().put("LE_EXPER_FINALSCORE", new Double(1));
			} else {
				individualReport.getScriptedGroupData().put("LE_EXPER_FINALSCORE", new Double(0));
			}
		}
		else if (groupReport.getTargetTransitionLevel() == 8 )
		{
			// SR EXEC/BUL
			if ( parse(individualReport.getScriptedData().get(ReportingConstants.LEI_BUSOP)) > 3.0 &&
			parse(individualReport.getScriptedData().get(ReportingConstants.LEI_HTC)) > 3.0 &&
			parse(individualReport.getScriptedData().get(ReportingConstants.LEI_HVIS)) > 3.0 &&
			parse(individualReport.getScriptedData().get(ReportingConstants.LEI_GTBUS)) > 3.0 )
			{
				individualReport.getScriptedGroupData().put("LE_EXPER_FINALSCORE", new Double(1));
			} else {
				individualReport.getScriptedGroupData().put("LE_EXPER_FINALSCORE", new Double(0));
			}
		}
		else if (groupReport.getTargetTransitionLevel() == 9 )
		{
			// SEA - Currently (4/28/10) same as BUL
			if ( parse(individualReport.getScriptedData().get(ReportingConstants.LEI_BUSOP)) > 3.0 &&
			parse(individualReport.getScriptedData().get(ReportingConstants.LEI_HTC)) > 3.0 &&
			parse(individualReport.getScriptedData().get(ReportingConstants.LEI_HVIS)) > 3.0 &&
			parse(individualReport.getScriptedData().get(ReportingConstants.LEI_GTBUS)) > 3.0 )
			{
				individualReport.getScriptedGroupData().put("LE_EXPER_FINALSCORE", new Double(1));
			} else {
				individualReport.getScriptedGroupData().put("LE_EXPER_FINALSCORE", new Double(0));
			}
			//System.out.println("      SEA Experience flag=" + individualReport.getScriptedGroupData().get("LE_EXPER_FINALSCORE"));
			//System.out.println("         Components: BUSOP=" + individualReport.getScriptedData().get(ReportingConstants.LEI_BUSOP) + 
			//		", HTC=" + individualReport.getScriptedData().get(ReportingConstants.LEI_HTC) +
			//		", HVIS=" + individualReport.getScriptedData().get(ReportingConstants.LEI_HVIS) +
			//		", GTBUS=" + individualReport.getScriptedData().get(ReportingConstants.LEI_GTBUS));
		} else {
			//System.out.println("      Invalid transition level - Experience flag=Default (0)");
			individualReport.getScriptedGroupData().put("LE_EXPER_FINALSCORE", new Double(0));
		}

		// Leadership Foundations
		int ldrFoundationCount = 0;
		if (parse(individualReport.getScriptedData().get(ReportingConstants.RAV_PSOLV)) > 2.0 &&
			parse(individualReport.getScriptedData().get(ReportingConstants.RAV_PSOLV)) < 8.0 )
		{
			ldrFoundationCount += 1;
		}

		if ( parse(individualReport.getScriptedData().get(ReportingConstants.GPI_INTEN)) > 2.0 &&
			parse(individualReport.getScriptedData().get(ReportingConstants.GPI_INTEN)) < 8.0 )
		{
			ldrFoundationCount += 1;
		}

		if ( parse(individualReport.getScriptedData().get(ReportingConstants.GPI_ATD)) > 2.0 &&
			parse(individualReport.getScriptedData().get(ReportingConstants.GPI_ATD)) < 8.0 )
		{
			ldrFoundationCount += 1;
		}

		if ( parse(individualReport.getScriptedData().get(ReportingConstants.GPI_IMIN)) > 2.0 &&
			parse(individualReport.getScriptedData().get(ReportingConstants.GPI_IMIN)) < 8.0 )
		{
			ldrFoundationCount += 1;
		}

		if ( parse(individualReport.getScriptedData().get(ReportingConstants.GPI_INEN)) > 2.0 &&
			parse(individualReport.getScriptedData().get(ReportingConstants.GPI_INEN)) < 8.0 )
		{
			ldrFoundationCount += 1;
		}

		if ( parse(individualReport.getScriptedData().get(ReportingConstants.GPI_ACHD)) > 2.0 &&
			parse(individualReport.getScriptedData().get(ReportingConstants.GPI_ACHD)) < 8.0 )
		{
			ldrFoundationCount += 1;
		}

		if ( parse(individualReport.getScriptedData().get(ReportingConstants.GPI_ADVD)) > 2.0 &&
			parse(individualReport.getScriptedData().get(ReportingConstants.GPI_ADVD)) < 8.0 )
		{
			ldrFoundationCount += 1;
		}

		if ( parse(individualReport.getScriptedData().get(ReportingConstants.GPI_COOR)) > 2.0 &&
			parse(individualReport.getScriptedData().get(ReportingConstants.GPI_COOR)) < 8.0 )
		{
			ldrFoundationCount += 1;
		}

		if ( parse(individualReport.getScriptedData().get(ReportingConstants.GPI_FLAD)) > 2.0 &&
			parse(individualReport.getScriptedData().get(ReportingConstants.GPI_FLAD)) < 8.0 )
		{
			ldrFoundationCount += 1;
		}

		if (ldrFoundationCount > 6 || 
				(new	Integer(individualReport.getDisplayData().get(ReportingConstants.RC_COGNITIVES_INCLUDED)).intValue() != 1 && ldrFoundationCount > 5))
		{
			individualReport.getScriptedGroupData().put("LE_FOUND_FINALSCORE", new Double(1));
		} else {
			individualReport.getScriptedGroupData().put("LE_FOUND_FINALSCORE", new Double(0));
		}

	}


	/*
	 *  findStanineFromPctl determines the Stanine value for a given percentileScore.
	 *
	 * param - percentileScore:Number
	 * returns - aStanineNumber:Number
	 */
	// TODO reconcile this with the one in Norm... better yet, call the one in Norm
	// note that this is duplicated in the following places:
	//	ScoreUtils.getStanine()
	//	Norm.calcStanineFromPercent()
	//  TLT_GROUP_DETAIL_REPORT_SCORING.findStanineFromPctl()  <-- this method
	//  TLT_INDIVIDUAL_SUMMARY_REPORT_SCORING.findStanineFromPctl()
	// TODO Should it be consolidated?
	public int findStanineFromPctl(double percentileScore)
	{
		int stanine = 0;
		percentileScore = Math.round(percentileScore);

		if (percentileScore < 4)
		{
			stanine = 1;
		}
		else if (percentileScore > 3 && percentileScore < 11)
		{
			stanine = 2;
		}
		else if (percentileScore > 10 && percentileScore < 23)
		{
			stanine = 3;
		}
		else if (percentileScore > 22 && percentileScore < 40)
		{
			stanine = 4;
		}
		else if (percentileScore > 39 && percentileScore < 60)
		{
			stanine = 5;
		}
		else if (percentileScore > 59 && percentileScore < 77)
		{
			stanine = 6;
		}
		else if (percentileScore > 76 && percentileScore < 89)
		{
			stanine = 7;
		}
		else if (percentileScore > 88 && percentileScore < 97)
		{
			stanine = 8;
		}
		else if (percentileScore > 96)
		{
			stanine = 9;
		}

		return stanine;
	}
	
	
}
