/**
 * Copyright (c) 2011, 2012 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.reporting.report;

public class ExtractItem
{
	//
	// Static definitions
	//
	
	// Constants for the ExtractItem data source
	public static int SRC_GRP = 00;	// GroupReport
	public static int SRC_INDV = 10;	// IndividualReport
	public static int SRC_RDD = 20;	// ReportData in IR (Display stuff)
	public static int SRC_RDS = 21;	// ReportData in IR (Score stuff)
	public static int SRC_HDG = -1;	// Heading - Not strictly a source, but...
	public static int SRC_PARM = 99;	// Parameter (external) data placed into the extract
	
	// Constants for ExtractItem data types
	//public static int DATA_BLANK = 0;
	public static final int DATA_INT = 1;
	public static final int DATA_FLOAT = 2;
	public static final int DATA_STRING = 3;
	public static final int DATA_DATE = 4;
	public static final int DATA_FLOAT_3 = 5;
	public static final int DATA_FLOAT_9 = 6;
	public static final int DATA_FLOAT_5 = 7;
	
	//
	// Instance variables
	//
	private String _key;
	private String _rptKey;
	private String _dispName;
	private int _src;
	private int _cellType = DATA_STRING;
	
	//
	// Constructors
	//
	
	// 3 value... no rptKey, defaults to String type
	public ExtractItem(String key, String dName, int src)
	{
		_key = key;
		_dispName = dName;
		_src = src;
	}
	
	// 4 value... with rptKey, defaults to String type
	public ExtractItem(String key, String rKey, String dName, int src)
	{
		_key = key;
		_rptKey = rKey;
		_dispName = dName;
		_src = src;
	}
	
	// 4 value... no rptKey
	public ExtractItem(String key, String dName, int src, int type)
	{
		_key = key;
		_dispName = dName;
		_src = src;
		_cellType = type;
	}
	
	// 5 value... with rptKey, defaults to String type
	public ExtractItem(String key, String rKey, String dName, int src, int type)
	{
		_key = key;
		_rptKey = rKey;
		_dispName = dName;
		_src = src;
		_cellType = type;
	}
	
	// constructors primarily for kfalp data.  Drops display name parameter... key = display name
	
	// 2 value... no rptKey, defaults to String type
	public ExtractItem(String key,int src)
	{
		_key = key;
		_dispName = key;
		_src = src;
	}
	
	// 3 value... no rptKey
	public ExtractItem(String key, int src, int type)
	{
		_key = key;
		_dispName = key;
		_src = src;
		_cellType = type;
	}
	
	//
	// Instance methods
	//
	
//	public void setKey(String value)
//	{
//		_key = value;
//	}
	public String getKey()
	{
		return _key;
	}
	
//	public void setRptKey(String value)
//	{
//		_rptKey = value;
//	}
	public String getRptKey()
	{
		return _rptKey;
	}
	
//	public void setDispName(String value)
//	{
//		_dispName = value;
//	}
	public String getDispName()
	{
		return _dispName;
	}
	
	// Use the declared statics (GRP, INDV, etc.) only
//	public void setSrc(int value)
//	{
//		_src = value;
//	}
	public int getSrc()
	{
		return _src;
	}
	
	// ----------------------
	public void setCellType(int value)
	{
		_cellType = value;
	}
	
	public int getCellType()
	{
		return _cellType;
	}
}
