/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.reporting.report;

//import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Individual report data object, used to set scored, raw and
 * display data
 * @author		Gavin Myers
 */
public class ReportData implements java.io.Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4924173778607869827L;
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private HashMap<String, String> _scoreData = new HashMap<String, String>();
	private HashMap<String, String> _rawData = new HashMap<String, String>();
	private HashMap<String, String> _displayData = new HashMap<String, String>();
	private HashMap<String, String> _scriptedData = new HashMap<String, String>();

	//
	// Constructors.
	//
	public ReportData() {
		
	}
	

	//
	// Instance methods.
	//
	
	//
	// Getters and setters of various levels of data
	//
	
	/************************************************************/
	// Add an entry to the scoreData map
	public void addScoreData(String key, String value)
	{
		_scoreData.put(key, value);
	}
	
	// Get an item from the scoreData map
	public String getScoreData(String key)
	{
		return _scoreData.get(key);
	}
	
	// Replace the whole scoreData map
	public void setScoreData(HashMap<String, String> scoreData)
	{
		_scoreData = scoreData;
	}
	
	// Return the whole scoreData map
	public HashMap<String, String> getScoreData()
	{
		return _scoreData;
	}
	
	/************************************************************/
	// Add an entry to the rawData map
	public void addRawData(String key, String value)
	{
		_rawData.put(key, value);
	}
	
	// Get an entry from the rawData map
	public String getRawData(String key)
	{
		return _rawData.get(key);
	}
	
	// Replace the whole rawData map
	public void setRawData(HashMap<String, String> rawData)
	{
		_rawData = rawData;
	}
	
	// Return the whole rawData map
	public HashMap<String, String> getRawData()
	{
		return _rawData;
	}
	
	/************************************************************/
	// Add an entry to the displayData map
	public void addDisplayData(String key, String value)
	{
		_displayData.put(key, value);
	}
	
	// Fetch an entry from the displayData map
	public String getDisplayData(String key)
	{
		return _displayData.get(key);
	}
	
	// Replace the whole displayData map
	public void setDisplayData(HashMap<String, String> displayData)
	{
		_displayData = displayData;
	}
	
	// Return the whole displayData map
	public HashMap<String, String> getDisplayData()
	{
		return _displayData;
	}
	
	/************************************************************/
	// Add an entry to the scriptedData map
	public void addScriptedData(String key, String value)
	{
		_scriptedData.put(key, value);
	}
	
	// Fetch an entry from the scriptedData map
	public String getScriptedData(String key)
	{
		return _scriptedData.get(key);
	}
	
	// Replace the whole scriptedData map
	public void setScriptedData(HashMap<String, String> scriptedData)
	{
		_scriptedData = scriptedData;
	}
	
	// Return the scriptedData map
	public HashMap<String, String> getScriptedData()
	{
		return _scriptedData;
	}


	public String toString()
	{
		String ret = "ReportData:\n";
		
		ret += "  Score Data:\n";
		for (Iterator<Map.Entry<String, String>> itr=_scoreData.entrySet().iterator(); itr.hasNext();  )
		{
			Map.Entry<String, String> ent = itr.next();
			ret += "    " + ent.getKey() + " = " + ent.getValue() + "\n";
		}
		
		ret += "  Raw Data:\n";
		for (Iterator<Map.Entry<String, String>> itr=_rawData.entrySet().iterator(); itr.hasNext();  )
		{
			Map.Entry<String, String> ent = itr.next();
			ret += "    " + ent.getKey() + " = " + ent.getValue() + "\n";
		}
		
		ret += "  Display Data:\n";
		for (Iterator<Map.Entry<String, String>> itr=_displayData.entrySet().iterator(); itr.hasNext();  )
		{
			Map.Entry<String, String> ent = itr.next();
			ret += "    " + ent.getKey() + " = " + ent.getValue() + "\n";
		}
		
		ret += "  Scripted Data:\n";
		for (Iterator<Map.Entry<String, String>> itr=_scriptedData.entrySet().iterator(); itr.hasNext();  )
		{
			Map.Entry<String, String> ent = itr.next();
			ret += "    " + ent.getKey() + " = " + ent.getValue() + "\n";
		}

		return ret;
	}

	
	public String scoreToString()
	{
		String ret = "Score Data:\n";
		for (Iterator<Map.Entry<String, String>> itr=_scoreData.entrySet().iterator(); itr.hasNext();  )
		{
			Map.Entry<String, String> ent = itr.next();
			ret += "    " + ent.getKey() + " = " + ent.getValue() + "\n";
		}

		return ret;
	}

	
	public String rawToString()
	{
		String ret = "Raw Data:\n";
		for (Iterator<Map.Entry<String, String>> itr=_rawData.entrySet().iterator(); itr.hasNext();  )
		{
			Map.Entry<String, String> ent = itr.next();
			ret += "    " + ent.getKey() + " = " + ent.getValue() + "\n";
		}

		return ret;
	}

	
	public String displayToString()
	{
		String ret = "Display Data:\n";
		for (Iterator<Map.Entry<String, String>> itr=_displayData.entrySet().iterator(); itr.hasNext();  )
		{
			Map.Entry<String, String> ent = itr.next();
			ret += "    " + ent.getKey() + " = " + ent.getValue() + "\n";
		}

		return ret;
	}
}
