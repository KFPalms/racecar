package com.pdi.reporting.ut;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;

import java.io.FileOutputStream;
import java.io.OutputStream;

import com.pdi.properties.PropertyLoader;
import com.pdi.reporting.constants.ReportingConstants;
import com.pdi.reporting.report.IndividualReport;
import com.pdi.reporting.report.ReportData;
import com.pdi.reporting.request.ReportingRequest;
import com.pdi.reporting.response.ReportingResponse;
import com.pdi.ut.UnitTestUtils;

import junit.framework.TestCase;

public class ReportingUT extends TestCase
{
	//
	// Static data
	//

	//
	// Constructor
	//
	public ReportingUT(String name)
	{
		super(name);
	} 


	//
	// Test Main
	//
	public static void main(String[] args)
		throws Exception
	{
		junit.textui.TestRunner.run(ReportingUT.class);
	}

	//
	// Tests
	//
	public void testGroovy()
	{
		UnitTestUtils.start(this);
		// call groovy expressions from Java code
		Binding binding = new Binding();
		binding.setVariable("foo", new Integer(2));
		GroovyShell shell = new GroovyShell(binding);
		
		Object value = shell.evaluate("println 'Hello World!'; x = 123; return foo * 10");
		assertEquals(value.equals(new Integer(20)), true);
		assertEquals(binding.getVariable("x").equals(new Integer(123)), true);
		
		//System.out.println("The value of x is " + binding.getVariable("x"));
		//System.out.println("The returned value is " + value);
		UnitTestUtils.stop(this);
	}


	
	public void testProperties()
		throws Exception
	{
		UnitTestUtils.start(this);
		//Does the local property work?
		String applicationId = PropertyLoader.getProperty("com.pdi.reporting.application", "application.id");
		assertEquals(applicationId != null, true);
		UnitTestUtils.stop(this);
	}



    /*
     * Tests the individual reports
     * 
     */
	public void testAbyDDevelopmentReport() throws Exception {
		UnitTestUtils.start(this);
    	
//		//Step 1: Build your report request
//       	 
//    	{		// AbyD Development Report
//    		ReportingRequest request = new ReportingRequest();
//    	
//			//Step 2.75 Fill your report with data (individual)
//			IndividualReport individual1 = new IndividualReport();
//			individual1.setReportCode(ReportingConstants.REPORT_CODE_ABYD_FIT_REPORT);
//			individual1.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);
//			ReportData data = new ReportData();
//			data.addDisplayData("CLIENT_NAME", "TARGET");
//			data.addDisplayData("PARTICIPANT_NAME", "Joe the Individual");
//			data.addRawData("Q1", "1"); 
//			data.addRawData("Q2", "2"); 
//			data.addRawData("Q3", "3");
//			data.addRawData("Q4", "4");
//			data.addRawData("Q5", "5");
//			data.addScoreData("SCALE_1", "1");
//			data.addScoreData("SCALE_2", "2");
//			//individual1.setReportData(data);
//			request.addReport(individual1); 
// 
//			//Step 4: Get the response object
//			ReportingResponse response = request.generateReports();
//		
//			//Step 5: Do something
//
//		
//			//Step 6: give it back to the caller
//			OutputStream outputStream = new FileOutputStream ("AbyD_DevelopmentReport.pdf");
//			response.toFile().writeTo(outputStream);
//
//    	}
    	
//    	{		// AbyD Fitness report
//        	ReportingRequest request = new ReportingRequest();
//        	
//    		//Step 2.75 Fill your report with data (individual)
//    		IndividualReport individual1 = new IndividualReport();
//    		individual1.setReportCode(ReportingConstants.REPORT_CODE_ABYD_FIT_REPORT);
//    		individual1.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);
//    		ReportData data = new ReportData();
//    		data.addDisplayData("CLIENT_NAME", "TARGET");
//    		data.addDisplayData("PARTICIPANT_NAME", "Joe the Individual");
//    		data.addRawData("Q1", "1"); 
//    		data.addRawData("Q2", "2"); 
//    		data.addRawData("Q3", "3");
//    		data.addRawData("Q4", "4");
//    		data.addRawData("Q5", "5");
//    		data.addScoreData("SCALE_1", "1");
//    		data.addScoreData("SCALE_2", "2");
//    		//individual1.setReportData(data);
//    		request.addReport(individual1);
//     
//    		//Step 4: Get the response object
//    		ReportingResponse response = request.generateReports();
//    		
//    		//Step 5: Do something
//
//    		
//    		//Step 6: give it back to the caller
//    		OutputStream outputStream = new FileOutputStream ("AbyD_FitReport.pdf");
//    		response.toFile().writeTo(outputStream);
//    	}
    	
//    	{		// AbyD Readiness Report
//        	ReportingRequest request = new ReportingRequest();
//        	
//    		//Step 2.75 Fill your report with data (individual)
//    		IndividualReport individual1 = new IndividualReport();
//    		individual1.setReportCode(ReportingConstants.REPORT_CODE_ABYD_READINESS_REPORT);
//    		individual1.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);
//    		ReportData data = new ReportData();
//    		data.addDisplayData("CLIENT_NAME", "TARGET");
//    		data.addDisplayData("PARTICIPANT_NAME", "Joe the Individual");
//    		data.addRawData("Q1", "1"); 
//    		data.addRawData("Q2", "2"); 
//    		data.addRawData("Q3", "3");
//    		data.addRawData("Q4", "4");
//    		data.addRawData("Q5", "5");
//    		data.addScoreData("SCALE_1", "1");
//    		data.addScoreData("SCALE_2", "2");
//    		//individual1.setReportData(data);
//    		request.addReport(individual1);
//     
//    		//Step 4: Get the response object
//    		ReportingResponse response = request.generateReports();
//    		
//    		//Step 5: Do something
//
//    		
//    		//Step 6: give it back to the caller
//    		OutputStream outputStream = new FileOutputStream ("AbyD_ReadinessReport.pdf");
//    		response.toFile().writeTo(outputStream);
//    	}
    	
//    	{		// CHQ Report
//        	ReportingRequest request = new ReportingRequest();
//        	
//    		//Step 2.75 Fill your report with data (individual)
//    		IndividualReport individual1 = new IndividualReport();
//    		individual1.setReportCode(ReportingConstants.REPORT_CODE_CHQ_DETAIL);
//    		individual1.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);
//    		ReportData data = new ReportData();
//    		data.addDisplayData("CLIENT_NAME", "TARGET");
//    		data.addDisplayData("PARTICIPANT_NAME", "Joe the Individual");
//    		data.addRawData("Q1", "1"); 
//    		data.addRawData("Q2", "2"); 
//    		data.addRawData("Q3", "3");
//    		data.addRawData("Q4", "4");
//    		data.addRawData("Q5", "5");
//    		data.addScoreData("SCALE_1", "1");
//    		data.addScoreData("SCALE_2", "2");
//    		//individual1.setReportData(data);
//    		request.addReport(individual1);
//     
//    		//Step 4: Get the response object
//    		ReportingResponse response = request.generateReports();
//    		
//    		//Step 5: Do something
//
//    		
//    		//Step 6: give it back to the caller
//    		OutputStream outputStream = new FileOutputStream ("IndividualReport_CHQ_Detail.pdf");
//    		response.toFile().writeTo(outputStream);
//    	}
    	
    	{		// LEI Report
        	ReportingRequest request = new ReportingRequest();
        	
    		//Step 2.75 Fill your report with data (individual)
    		IndividualReport individual1 = new IndividualReport();
    		individual1.setReportCode(ReportingConstants.REPORT_CODE_LEI_GRAPHICAL);
    		individual1.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);
    		ReportData data = new ReportData();
    		data.addDisplayData("CLIENT_NAME", "TARGET");
    		data.addDisplayData("PARTICIPANT_NAME", "Joe the Individual");
    		data.addRawData("Q1", "1"); 
    		data.addRawData("Q2", "2"); 
    		data.addRawData("Q3", "3");
    		data.addRawData("Q4", "4");
    		data.addRawData("Q5", "5");
    		data.addScoreData("SCALE_1", "1");
    		data.addScoreData("SCALE_2", "2");
    		//individual1.setReportData(data);
    		request.addReport(individual1);
     
    		//Step 4: Get the response object
    		ReportingResponse response = request.generateReports();
    		
    		//Step 5: Do something

    		
    		//Step 6: give it back to the caller
    		OutputStream outputStream = new FileOutputStream ("IndividualReport_LEI_Detail.pdf");
    		response.toFile().writeTo(outputStream);
    	}
    	
//    	{		// GPI report
//        	ReportingRequest request = new ReportingRequest();
//        	
//    		//Step 2.75 Fill your report with data (individual)
//    		IndividualReport individual1 = new IndividualReport();
//    		individual1.setReportCode(ReportingConstants.REPORT_CODE_GPI_GRAPHICAL);
//    		individual1.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);
//    		ReportData data = new ReportData();
//    		data.addDisplayData("CLIENT_NAME", "TARGET");
//    		data.addDisplayData("PARTICIPANT_NAME", "Joe the Individual");
//    		data.addRawData("Q1", "1"); 
//    		data.addRawData("Q2", "2"); 
//    		data.addRawData("Q3", "3");
//    		data.addRawData("Q4", "4");
//    		data.addRawData("Q5", "5");
//    		data.addScoreData("SCALE_1", "1");
//    		data.addScoreData("SCALE_2", "2");
//    		//individual1.setReportData(data);
//    		request.addReport(individual1);
//     
//    		//Step 4: Get the response object
//    		ReportingResponse response = request.generateReports();
//    		
//    		//Step 5: Do something
//
//    		
//    		//Step 6: give it back to the caller
//    		OutputStream outputStream = new FileOutputStream ("IndividualReport_GPI_Detail.pdf");
//    		response.toFile().writeTo(outputStream);
//    	}


//    	{		// Raven's Short Form report
//        	ReportingRequest request = new ReportingRequest();
//        	
//    		//Step 2.75 Fill your report with data (individual)
//    		IndividualReport individual1 = new IndividualReport();
//    		individual1.setReportCode(ReportingConstants.REPORT_CODE_RAV_SF_GRAPHICAL);
//    		individual1.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);
//    		ReportData data = new ReportData();
//    		data.addDisplayData("CLIENT_NAME", "TARGET");
//    		data.addDisplayData("PARTICIPANT_NAME", "Joe the Individual");
//    		data.addRawData("Q1", "1"); 
//    		data.addRawData("Q2", "2"); 
//    		data.addRawData("Q3", "3");
//    		data.addRawData("Q4", "4");
//    		data.addRawData("Q5", "5");
//    		data.addScoreData("SCALE_1", "1");
//    		data.addScoreData("SCALE_2", "2");
//    		//individual1.setReportData(data);
//    		request.addReport(individual1);
//     
//    		//Step 4: Get the response object
//    		ReportingResponse response = request.generateReports();
//    		
//    		//Step 5: Do something
//
//    		
//    		//Step 6: give it back to the caller
//    		OutputStream outputStream = new FileOutputStream ("IndividualReport_RavensSF_Detail.pdf");
//    		response.toFile().writeTo(outputStream);
//    	}


//    	{		// Raven's B report
//    	ReportingRequest request = new ReportingRequest();
//    	
//		//Step 2.75 Fill your report with data (individual)
//		IndividualReport individual1 = new IndividualReport();
//		individual1.setReportCode(ReportingConstants.REPORT_CODE_RAV_B_GRAPHICAL);
//		individual1.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);
//		ReportData data = new ReportData();
//		data.addDisplayData("CLIENT_NAME", "TARGET");
//		data.addDisplayData("PARTICIPANT_NAME", "Joe the Individual");
//		data.addRawData("Q1", "1"); 
//		data.addRawData("Q2", "2"); 
//		data.addRawData("Q3", "3");
//		data.addRawData("Q4", "4");
//		data.addRawData("Q5", "5");
//		data.addScoreData("SCALE_1", "1");
//		data.addScoreData("SCALE_2", "2");
//		//individual1.setReportData(data);
//		request.addReport(individual1);
// 
//		//Step 4: Get the response object
//		ReportingResponse response = request.generateReports();
//		
//		//Step 5: Do something
//
//		
//		//Step 6: give it back to the caller
//		OutputStream outputStream = new FileOutputStream ("IndividualReport_RavensB_Detail.pdf");
//		response.toFile().writeTo(outputStream);
//	}


//		{		// WG E (WG II) Report
//			ReportingRequest request = new ReportingRequest();
//
//			//	Fill your report with data (individual)
//			IndividualReport ir = new IndividualReport();
//			ir.setReportCode(ReportingConstants.REPORT_CODE_WGE_GRAPHICAL);
//			ir.setReportType(ReportingConstants.REPORT_TYPE_INDIVIDUAL);
//			
//			ir.getDisplayData().put("PARTICIPANT_NAME", "Testy Testbogin");
//			ir.getDisplayData().put("ORGANIZATION", "My Company");
//			ir.getDisplayData().put("PROJECT_NAME", "My Project");
//			ir.getDisplayData().put("ADMIN_DATE", "Today");
//			
//			request.addReport(ir);
//			
//			ReportData rd = new ReportData();
//			rd.addScoreData("WGE_CT","25");
//			rd.addScoreData("WGE_GP_NAME", "General Population");
//			rd.addScoreData("WGE_CT_GP_SCALE_MEAN", "21.2");
//			rd.addScoreData("WGE_CT_GP_SCALE_STDEV", "4.9");
//			rd.addScoreData("WGE_CT_GP_PCTL", "78.09808808955421");
//			rd.addScoreData("WGE_SP_NAME", "NA Executive");
//			rd.addScoreData("WGE_CT_SP_SCALE_STDEV", "4.3");
//			rd.addScoreData("WGE_CT_SP_SCALE_MEAN", "31");
//			rd.addScoreData("WGE_CT_SP_PCTL", "3.1945395186425563");
//			ir.getReportData().put(ReportingConstants.RC_WG_E, rd);
//
//			ir.getDisplayData().put("WGE_GP_NAME", ir.getReportData().get(ReportingConstants.RC_WG_E).getScoreData("WGE_GP_NAME"));
//			ir.getDisplayData().put("WGE_SP_NAME", ir.getReportData().get(ReportingConstants.RC_WG_E).getScoreData("WGE_SP_NAME"));
//
//			//Step 4: Get the response object
//			ReportingResponse response = request.generateReports();
//		
//			//Step 5: Do something
//
//		
//			//Step 6: give it back to the caller
//			OutputStream outputStream = new FileOutputStream ("IndividualReport_WGE_Graphical.pdf");
//			response.toFile().writeTo(outputStream);
//    	}

    	UnitTestUtils.stop(this);
    }
}
