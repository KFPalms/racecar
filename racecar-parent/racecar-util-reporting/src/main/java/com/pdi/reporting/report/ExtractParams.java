package com.pdi.reporting.report;

/*
 * Convenience class to hold extraneous parameters used in extracts
 */
public class ExtractParams {
	
	//
	// Instance variables
	//
	
	// General purpose buckets
	private String reportCode = null;
	
	// Used in analytics extract
	private Integer sortId = null;
	private String sortGroupName = null;
	private String sortDispName = null;
	private Boolean showName = null;
	private Boolean showLci = null;
	private String axisLabels = null;
	
	//
	// Constructors
	//
	// 1-parameter contructor (report Name
	public ExtractParams(String reportCode)
	{
		this.reportCode = reportCode;
	}
	
	//
	// Instance methods
	//
	public void setReportCode(String value)
	{
		this.reportCode = value;
	}
	public String getReportCode()
	{
		return this.reportCode;
	}
	
	public void setSortId(Integer value)
	{
		this.sortId = value;
	}
	public Integer getSortId()
	{
		return this.sortId;
	}
	
	public void setSortGroupName(String value)
	{
		this.sortGroupName = value;
	}
	public String getSortGroupName()
	{
		return this.sortGroupName;
	}
	
	public void setSortDispName(String value)
	{
		this.sortDispName = value;
	}
	public String getSortDispName()
	{
		return this.sortDispName;
	}
	
	public void setShowName(Boolean value)
	{
		this.showName = value;
	}
	public Boolean getShowName()
	{
		return this.showName;
	}
	
	public void setShowLci(Boolean value)
	{
		this.showLci = value;
	}
	public Boolean getShowLci()
	{
		return this.showLci;
	}
	
	public void setAxisLabels(String value)
	{
		this.axisLabels = value;
	}
	public String getAxisLabels()
	{
		return this.axisLabels;
	}
}