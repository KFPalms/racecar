/**
 * Copyright (c) 2011 Personnel Decisions International Corp. d.b.a. as PDI Ninth House
 * All rights reserved.
 */
package com.pdi.reporting.report;

/*
 * Convenience class to differentiate error.
 */
public class ReportDataException extends Exception
{
	static final long serialVersionUID = 0L;
	
	public ReportDataException(String msg)
	{ super(msg); }
}
