/**
 * Copyright (c) 2009 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.reporting.response;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Random;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipOutputStream;

import com.pdi.reporting.report.Report;

/**
 * Returns the report generated URL, or attached binary file, or a status
 * 
 * @author		Gavin Myers
 */
public class ReportingResponse
{
	//
	// Static data.
	//

	//
	// Static methods.
	//

	//
	// Instance data.
	//
	private ArrayList<Report> _reports;
	private String _responseCode;

	//
	// Constructors.
	//
	public ReportingResponse()
	{
		
	}

	//
	// Instance methods.
	//
	
	/**
	 * Converts the pack of reports to either a zip file, or a single pdf
	 */
	public ByteArrayOutputStream toFile()
		throws Exception
	{
	    ByteArrayOutputStream bos = new ByteArrayOutputStream();
	    if(_reports.size() > 1)
	    {
	    	
	    	//It's a bunch of PDFs, return a zip
		    ZipOutputStream zipFile = new ZipOutputStream(bos);
		    for(Report r : _reports)
		    {
		    	try {
			    	String fileName = r.getName();
			    	ZipEntry zipEntry = new ZipEntry(fileName);
			    	zipFile.putNextEntry(zipEntry);
			    	zipFile.write(r.getData().toByteArray());
		    	} catch (ZipException ze) {
		    		//duplicate name, not sure if it's the 1st, 2nd, but it probably isn't the 10,000th
		    		Random generator = new Random();
		    		int randomIndex = generator.nextInt( 10000 ) * 10000;
			    	String fileName = r.getName();
			    	fileName = fileName.replace(".", "["+randomIndex+"].");
			    	ZipEntry zipEntry = new ZipEntry(fileName);
			    	zipFile.putNextEntry(zipEntry);
			    	zipFile.write(r.getData().toByteArray());
		    	}
		    }
		    zipFile.close();
	    } else {
	    	//It's just a pdf, return a pdf
	    	bos = _reports.get(0).getData();
	    }
	    return bos;
	    
	}
	
	// Getters and setters for instance data
	//Response type
	public void setResponseCode(String value)
	{
		_responseCode = value;
	}
	public String getResponseCode()
	{
		return _responseCode;
	}
	
	// reports
	public void addReport(Report r)
	{
		if(_reports == null)
		{
			_reports = new ArrayList<Report>();
		}
		_reports.add(r);
	}
	
	public void removeReport(Report r)
	{
		if(_reports == null)
		{
			return;
		}
		_reports.remove(r);
	}
	
	public void setReports(ArrayList<Report> value)
	{
		_reports = value;
	}
	public ArrayList<Report> getReports()
	{
		return _reports;
	}

}
