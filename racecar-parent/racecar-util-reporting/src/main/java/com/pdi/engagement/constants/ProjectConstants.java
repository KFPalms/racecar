/**
 * Copyright (c) 2010 Personnel Decisions International, Inc.
 * All rights reserved.
 */
package com.pdi.engagement.constants;

public class ProjectConstants
{
	//Generic Project type codes
	public static String EC_TYPE_ABYD_TESTING = "AbyD_Test";					// AbyD Testing Components
	public static String EC_TYPE_ABYD_CONSULTANT = "AbyD_Consult";				// AbyD Consultant Components
	public static String EC_TYPE_TLT = "TLT";									// TalentView of Transitions
	public static String EC_TYPE_ASSESSMENT = "Assessment";						// Assessment
	
	// Not used so much
	public static String EC_TYPE_LEADERSHIP_PROFILE = "Ldr_Prof";				// Leadership Profile
	public static String EC_TYPE_LEADERSHIP_PROFILE_SHORT = "Ldr_Prof_Short";	// Leadership Profile - Short
	public static String EC_TYPE_ASSESSMENT_REPORTING = "AsmtReporting";		// Assessments Reporting
	public static String EC_TYPE_TLR = "TLR";									// TalentView of Readiness
	public static String EC_TYPE_TLR_MS = "TVOR_BUL_MS";						// TalentView of Readiness - Microsoft BUL Path
	public static String EC_TYPE_TPOT = "TPOT";									// TPOT-OLD
	public static String EC_TYPE_TCM = "TCM";									// TCM
	public static String EC_TYPE_JAV = "JAV";									// TCM JAV
	public static String EC_TYPE_BUL_ASMT = "BUL";								// BUL Assessment
	public static String EC_TYPE_SAC_PRE = "SacPre";							// SAC Pre-work
	public static String EC_TYPE_SAC_PROC = "SacProctored";						// BUL Assessment Path
	
	// Project Product codes
	// Must be defined the same as on the Flex side
	public static int EC_PROD_A_BY_D = 1;       // in v2, is 25, 26
	public static int EC_PROD_TLT = 2;          // in v2 is 23
	public static int EC_PROD_ASSESSMENT = 3;   // in v2, anything other than the above

}
